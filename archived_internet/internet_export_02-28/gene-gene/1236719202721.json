{
    "dcr_id": "1236719202721",
    "title": {
        "en": "The Glomeromycetes in vitro Collection - Inoculum propagation",
        "fr": "La collection in vitro de Glom\u00e9romyc\u00e8tes - Propagation de l'inoculum"
    },
    "modified": "2021-06-14",
    "issued": "2009-06-17",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1366750690747",
    "dc_date_created": "2009-06-17",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/sci_Ottawa/WORKAREA/sci_Ottawa/templatedata/comm-comm/gene-gene/data/03/ginco_propagation_1236719202721_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/sci_Ottawa/WORKAREA/sci_Ottawa/templatedata/comm-comm/gene-gene/data/03/ginco_propagation_1236719202721_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "The Glomeromycetes in vitro Collection - Inoculum propagation",
        "fr": "La collection in vitro de Glom\u00e9romyc\u00e8tes - Propagation de l'inoculum"
    },
    "breadcrumb": {
        "en": "The Glomeromycetes in vitro Collection - Inoculum propagation",
        "fr": "La collection in vitro de Glom\u00e9romyc\u00e8tes - Propagation de l'inoculum"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-06-17",
            "fr": "2009-06-17"
        },
        "modified": {
            "en": "2021-06-14",
            "fr": "2021-06-14"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "The Glomeromycetes in vitro Collection - Inoculum propagation",
            "fr": "La collection in vitro de Glom\u00e9romyc\u00e8tes - Propagation de l'inoculum"
        },
        "subject": {
            "en": "mushrooms",
            "fr": "champignons"
        },
        "dcterms:subject": {
            "en": "entomology;educational resources",
            "fr": "herbier;air;\u00e9valuation"
        },
        "description": {
            "en": "inoculum propagationin vivo propagation in pot cultures (figure 15: in vivo propagation in pot cultures) in vitro propagation on excised roots (figure 16: in vitro propagation on excised roots)",
            "fr": null
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " inoculum propagationin vivo propagation; vitro collection; in vitro propagation; excised roots;pot cultures",
            "fr": null
        },
        "audience": {
            "en": "scientists",
            "fr": "scientifiques"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Eastern Cereal and Oilseed Research Centre;Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Centre de recherches de l'Est sur les c\u00e9r\u00e9ales et les ol\u00e9agineux;Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Glomeromycetes in vitro Collection - Inoculum propagation",
        "fr": "La collection in vitro de Glom\u00e9romyc\u00e8tes - Propagation de l'inoculum"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <nav>\n<h2 class=\"hidden\">Previous-next document navigation - top</h2>\n<p class=\"text-right\"><a href=\"/eng/?id=1236785110466\" title=\"Glomeromycete Images\">Previous</a> | <a href=\"/eng/?id=1236786816381\">Table of Contents</a> | <a href=\"the-glomeromycetes-in-vitro-collection-inoculum-preservation/?id=1236718789448\" title=\"The Glomeromycetes in vitro Collection - Inoculum preservation\">Next</a></p>\n</nav>\n\n<figure><img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/ginco/ginco-05-2008-an_p18_0001_eng.jpg\" alt=\"Description of this image follows.\">\n<figcaption>\n<details><summary> Description - In vivo propagation in pot culture</summary>\n<p>Propagation's methodology of <abbr title=\"arbuscular mycorrhizal\">AM</abbr> fungi in pot culture (in vivo): the starting <abbr title=\"arbuscular mycorrhizal\">AM</abbr> inoculum, provided under the form of gel plugs (left), is deposited at the proximity of the roots of a plantlet (centre). Once the roots get colonized, the spores and the <abbr title=\"arbuscular mycorrhizal\">AM</abbr> mycelium network are differentiated throughout the rhizosphere (right).</p>\n</details>\n</figcaption></figure>\n\n<figure class=\"mrgn-tp-lg\"><img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/ginco/ginco-05-2008-an_p18_0002_eng.jpg\" alt=\"Description of this image follows.\"><figcaption>\n<details><summary> Description - In vitro propagation on excised roots</summary>\n<p>Propagation's methodology of <abbr title=\"arbuscular mycorrhizal\">AM</abbr> fungi: a gel plug extracted from an in vitro culture of excised roots (1 and 2) is replaced by an inoculum gel plug transfered from a starting inoculum preserved at 4<abbr title=\"degrees Celsius\">\u00b0C</abbr> in an Eppendorf (3). The resulting product is a monoxenic cultures of the <abbr title=\"arbuscular mycorrhizal\">AM</abbr> fungi (4).</p>\n</details>\n</figcaption>\n</figure>\n\n<nav>\n<h2 class=\"hidden\">Previous-next document navigation - bottom</h2>\n<p class=\"text-right\"><a href=\"/eng/?id=1236785110466\" title=\"Glomeromycete Images\">Previous</a> | <a href=\"/eng/?id=1236786816381\">Table of Contents</a> | <a href=\"/eng/?id=1236718789448\" title=\"The Glomeromycetes in vitro Collection - Inoculum preservation\">Next</a></p>\n</nav>\n\r\n    ",
        "fr": "\r\n    <nav>\n<h2 class=\"hidden\">Navigation du document - pr\u00e9c\u00e9dente-suivante - haut</h2>\n<p class=\"text-right\"><a title=\"Images de Glom\u00e9romyc\u00e8tes\" href=\"/fra/?id=1236785110466\">Pr\u00e9c\u00e9dente</a> | <a href=\"/fra/?id=1236786816381\">Table des mati\u00e8res</a> | <a title=\"La collection in vitro de Glom\u00e9romyc\u00e8tes - Conservation de l'inoculum\" href=\"/fra/?id=1236718789448\">Suivante</a></p>\n</nav>\n\n<figure>\n<img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/ginco/ginco-05-2008-an_p18_0001_fra.jpg\" alt=\"La description de cette image suit.\"><figcaption><details><summary> Description - Propagation in vivo en cultures en pots</summary>\n<p>Propagation en pots de champignons <abbr title=\"mycorhiziens arbusculaires\">MA</abbr> (in vivo)\u00a0: l'inoculum de d\u00e9part, sous forme de rondelles de g\u00e9lose (gauche), est d\u00e9pos\u00e9 \u00e0 proximit\u00e9 des racines d'une plantule (centre). Une fois les racines colonis\u00e9es, spores et r\u00e9seau myc\u00e9lien se diff\u00e9rencient dans la rhizosph\u00e8re (droite).</p>\n</details></figcaption></figure>\n\n<figure class=\"mrgn-tp-lg\"><img src=\"https://www.agr.gc.ca/resources/prod/img/science/images/ginco/ginco-05-2008-an_p18_0002_fra.jpg\" alt=\"La description de cette image suit.\"><figcaption><details><summary> Description - Propagation in vitro avec racines excis\u00e9es</summary>\n<p>Propagation in vitro des champignons <abbr title=\"mycorhiziens arbusculaires\">MA</abbr>\u00a0: une rondelle de g\u00e9lose pr\u00e9lev\u00e9e d'une culture in vitro de racine (1 et 2) est remplac\u00e9e par une rondelle d'inoculum provenant d'un pilulier d'inoculum conserv\u00e9 \u00e0 4<abbr title=\"degr\u00e9s Celsius\">\u00b0C</abbr> (3). Il en r\u00e9sulte une culture monox\u00e9nique de champignon <abbr title=\"mycorhiziens arbusculaires\">MA</abbr> (4).</p>\n</details></figcaption></figure>\n\n<nav>\n<h2 class=\"hidden\">Navigation du document - pr\u00e9c\u00e9dente-suivante - bas</h2>\n<p class=\"text-right\"><a title=\"Images de Glom\u00e9romyc\u00e8tes\" href=\"/fra/?id=1236785110466\">Pr\u00e9c\u00e9dente</a> | <a href=\"/fra/?id=1236786816381\">Table des mati\u00e8res</a> | <a title=\"La collection in vitro de Glom\u00e9romyc\u00e8tes - Conservation de l'inoculum\" href=\"/fra/?id=1236718789448\">Suivante</a></p>\n</nav>\n\r\n    "
    }
}