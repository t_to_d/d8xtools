{
    "dcr_id": "1476285727422",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Leucanthemum vulgare</i> (Ox-eye daisy)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Leucanthemum vulgare</i> (Marguerite blanche)"
    },
    "html_modified": "7-11-2017",
    "modified": "6-11-2017",
    "issued": "12-10-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_leucanthemum_vulgare_1476285727422_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_leucanthemum_vulgare_1476285727422_fra"
    },
    "parent_ia_id": "1333136685768",
    "ia_id": "1476285727734",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Leucanthemum vulgare</i> (Ox-eye daisy)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Leucanthemum vulgare</i> (Marguerite blanche)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Leucanthemum vulgare</i> (Ox-eye daisy)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Leucanthemum vulgare</i> (Marguerite blanche)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476285727734",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/leucanthemum-vulgare/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/leucanthemum-vulgare/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Leucanthemum vulgare (Ox-eye daisy)",
            "fr": "Semence de mauvaises herbe : Leucanthemum vulgare (Marguerite blanche)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Leucanthemum vulgare, Asteraceae, Chrysanthemum leucanthemum, Ox-eye daisy",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Leucanthemum vulgare, Asteraceae, Chrysanthemum leucanthemum, Marguerite blanche"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-06",
            "fr": "2017-11-06"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Leucanthemum vulgare (Ox-eye daisy)",
        "fr": "Semence de mauvaises herbe : Leucanthemum vulgare (Marguerite blanche)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Synonym</h2>\n<p><i lang=\"la\">Chrysanthemum leucanthemum</i></p>\n\n<h2>Common Name</h2>\n<p>Ox-eye daisy</p>\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 3 and Noxious, Class 5 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.\n</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in all Canadian provinces and territories (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to Europe and western temperate Asia (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Naturalized in east and southern Africa, eastern temperate and southern tropical Asia, Australia, New Zealand, North America and parts of South America (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Achene length: 2.0 - 3.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width: 0.8 - 1.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene elongate, ribbed, tapered at base</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Achene has 10 prominent ribs with smooth interspaces</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene dark brown with light brown ribs</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>A short and blunt floral style remnant is on top of the achene</li>\n<li>The rib interspaces frequently have short, white streaks</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated land (rare), hay fields, pastures, gardens, lawns, rangeland,   native grasslands, forest openings, abandoned croplands, roadways,   railway embankments and disturbed areas (Clements et al. 2004<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, Darbyshire 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Ox-eye daisy was introduced to North America as an ornamental and seed   contaminant in the late 1700s. By 1800, it was well established in many   areas of North America (Clements et al. 2004<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n<p>Reproduction occurs mainly through seeds but plants can also sprout from rhizomes (Clements et al. 2004<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). A single plant may produce several thousand seeds per season that can remain viable in the soil for 5 to 40 years (Clements et al. 2004<sup id=\"fn3d-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Shasta daisy (<i lang=\"la\">Leucanthemum</i> x <i lang=\"la\">superbum</i>)</h3>\n<ul>\n<li>Shasta daisy achenes have a similar shape, dark colour with streaks between the ribs and top peg as ox-eye daisy.</li>\n\n<li>Shasta daisy (length: 3.0 - 5.0 <abbr title=\"millimetres\">mm</abbr>; width 1.0 - 1.5 <abbr title=\"millimetres\">mm</abbr>) is generally larger than ox-eye daisy. The ribs tend to be darker than ox-eye daisy.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_04cnsh_1475604132956_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Ox-eye daisy (<i lang=\"la\">Leucanthemum vulgare</i>) achenes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_01cnsh_1475604023106_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Ox-eye daisy (<i lang=\"la\">Leucanthemum vulgare</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_02cnsh_1475604047790_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Ox-eye daisy (<i lang=\"la\">Leucanthemum vulgare</i>) achene, side view\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_03cnsh_1475604090641_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Ox-eye daisy (<i lang=\"la\">Leucanthemum vulgare</i>) top of the achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leuc_vulg_copyright_1475603930529_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Ox-eye daisy (<i lang=\"la\">Leucanthemum vulgare</i>) achene\n</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_superbum_01cnsh_1475603954906_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Shasta daisy (<i lang=\"la\">Leucanthemum</i> x <i lang=\"la\">superbum</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_superbum_02cnsh_1475603983441_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Shasta daisy (<i lang=\"la\">Leucanthemum</i> x <i lang=\"la\">superbum</i>) achenes\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Clements, D. R., Cole, D. E., Darbyshire, S., King, J. and McClay, A. 2004</strong>. The biology of Canadian weeds. 128. <i lang=\"la\">Leucanthemum vulgare</i> Lam. Canadian Journal of Plant Science 84: 343-363.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Synonyme</h2>\n<p><i lang=\"la\">Chrysanthemum leucanthemum</i></p>\n\n<h2>Nom commun</h2>\n<p>Marguerite blanche</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie\u00a03, et mauvaise herbe nuisible, cat\u00e9gorie\u00a05 de de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente dans l'ensemble des provinces et des territoires du Canada (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Europe et de l'Asie temp\u00e9r\u00e9e occidentale (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Naturalis\u00e9e en Afrique australe et orientale, dans les zones temp\u00e9r\u00e9es   de l'Asie de l'Est et dans les zones tropicales de l'Asie du Sud, en   Australie, en Nouvelle-Z\u00e9lande, en Am\u00e9rique du Nord et dans certaines   parties d'Am\u00e9rique du Sud (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'ak\u00e8ne\u00a0: 2,0 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de l'ak\u00e8ne\u00a0: 0,8 \u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne allong\u00e9, c\u00f4tel\u00e9 et r\u00e9tr\u00e9ci \u00e0 la base</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>L'ak\u00e8ne a dix c\u00f4tes saillantes et des espaces intercostaux lisses</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>L'ak\u00e8ne est brun fonc\u00e9 et les c\u00f4tes sont brun clair</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Un vestige de style floral court et arrondi est visible sur le sommet de l'ak\u00e8ne</li>\n<li>Les espaces intercostaux sont fr\u00e9quemment orn\u00e9s de courtes stries blanches</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Terrains cultiv\u00e9s (rare), pr\u00e9s de fauche, p\u00e2turages, jardins, pelouses,   parcours, prairies naturelles, clairi\u00e8res, terres agricoles abandonn\u00e9es,   bords des routes et des voies ferr\u00e9es et terrains perturb\u00e9s (Clements et al., 2004<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; Darbyshire, 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La marguerite blanche a \u00e9t\u00e9 introduite en Am\u00e9rique du Nord comme plante   ornementale et dans de la semence contamin\u00e9e \u00e0 la fin du 18e\u00a0si\u00e8cle. En 1800, elle \u00e9tait bien \u00e9tablie dans de nombreuses r\u00e9gions d'Am\u00e9rique du Nord (Clements et al., 2004<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n<p>La multiplication se fait principalement par la production de graines, mais peut aussi se faire \u00e0 partir des rhizomes (Clements et al., 2004<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>). Une seule plante peut produire plusieurs milliers de graines par saison qui peuvent demeurer viables dans le sol de 5\u00a0\u00e0 40\u00a0ans (Clements et al., 2004<sup id=\"fn3d-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Grande marguerite (<i lang=\"la\">Leucanthemum</i> x <i lang=\"la\">superbum</i>)</h3>\n<ul>\n<li>Les ak\u00e8nes de la grande marguerite ressemblent \u00e0 ceux de la marguerite blanche par leur forme, leur couleur fonc\u00e9e, leurs stries intercostales et la pr\u00e9sence d'une base stylaire sur leur sommet.</li>\n\n<li>L'ak\u00e8ne de la grande marguerite (longueur\u00a0: 3,0 \u00e0 5,0 <abbr title=\"millim\u00e8tre\">mm</abbr>; largeur\u00a0: 1,0 \u00e0 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr>) est g\u00e9n\u00e9ralement plus gros que celui de la marguerite blanche. Ses c\u00f4tes sont g\u00e9n\u00e9ralement plus fonc\u00e9es que celles de la marguerite blanche.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_04cnsh_1475604132956_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Marguerite blanche (<i lang=\"la\">Leucanthemum vulgare</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_01cnsh_1475604023106_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Marguerite blanche (<i lang=\"la\">Leucanthemum vulgare</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_02cnsh_1475604047790_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Marguerite blanche (<i lang=\"la\">Leucanthemum vulgare</i>) ak\u00e8ne, vue de c\u00f4t\u00e9\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_vulgare_03cnsh_1475604090641_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Marguerite blanche (<i lang=\"la\">Leucanthemum vulgare</i>) sommet de l'ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leuc_vulg_copyright_1475603930529_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Marguerite blanche (<i lang=\"la\">Leucanthemum vulgare</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_superbum_01cnsh_1475603954906_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Grande marguerite (<i lang=\"la\">Leucanthemum</i> x <i lang=\"la\">superbum</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_leucanthemum_superbum_02cnsh_1475603983441_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Grande marguerite (<i lang=\"la\">Leucanthemum</i> x <i lang=\"la\">superbum</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Clements, D. R., Cole, D. E., Darbyshire, S., King, J. and McClay, A. 2004</strong>. The biology of Canadian weeds. 128. <i lang=\"la\">Leucanthemum vulgare</i> Lam. Canadian Journal of Plant Science 84: 343-363.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}