{
    "dcr_id": "1523294923364",
    "title": {
        "en": "Biosecurity Measures for Poultry Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille"
    },
    "html_modified": "2024-01-26 2:24:06 PM",
    "modified": "2018-05-07",
    "issued": "2018-05-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/biosec_measures_poultry_1523294923364_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/biosec_measures_poultry_1523294923364_fra"
    },
    "ia_id": "1523294923754",
    "parent_ia_id": "1519235183948",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1519235183948",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biosecurity Measures for Poultry Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille"
    },
    "label": {
        "en": "Biosecurity Measures for Poultry Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille"
    },
    "templatetype": "content page 1 column",
    "node_id": "1523294923754",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1519235033628",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/standards-and-principles/transportation/poultry/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/transport/volaille/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biosecurity Measures for Poultry Transportation",
            "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille"
        },
        "description": {
            "en": "Transporters have a crucial role in preventing or reducing the spread of diseases during transportation. Diseases in poultry can lead to poultry health, human health, and economic consequences.  This brochure highlights key biosecurity measures to protect poultry from contracting or spreading diseases during transportation and minimize costs due to outbreak of diseases.",
            "fr": "Les transporteurs jouent un r\u00f4le crucial dans la pr\u00e9vention et la r\u00e9duction de la propagation de maladies au cours du transport. La pr\u00e9sence de maladies chez la volaille peut avoir des cons\u00e9quences sur la sant\u00e9 de la volaille, sur la sant\u00e9 humaine et sur l'\u00e9conomie. Le pr\u00e9sent d\u00e9pliant d\u00e9crit les principales mesures de bios\u00e9curit\u00e9 \u00e0 adopter pour emp\u00eacher que la volaille contracte ou propage des maladies durant le transport ainsi que pour r\u00e9duire le plus possible les co\u00fbts d\u00e9coulant d'une \u00e9closion de maladie."
        },
        "keywords": {
            "en": "biosecurity, animal health, diseases, standards, principles, measures, poultry, transportation",
            "fr": "bios\u00e9curit\u00e9, sant\u00e9 des animaux, maladies, normes, principes, mesures, volaille, transport"
        },
        "dcterms.subject": {
            "en": "livestock,imports,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-05-07",
            "fr": "2018-05-07"
        },
        "modified": {
            "en": "2018-05-07",
            "fr": "2018-05-07"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biosecurity Measures for Poultry Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<section class=\"mwsdownload-link section col-xs-12 col-sm-12 col-md-12 col-lg-10\">\n<div class=\"well well-sm gc-dwnld\">\n\n<div class=\"row\">\n\n<div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\">\n\n<p><a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_poultry_1525476449618_eng.pdf\">\n<img class=\"img-responsive mrgn-lft-sm mrgn-tp-sm gc-dwnld-img\" src=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/images-images/bt-thumbnails-poultry_1525476195697_eng.jpg\" alt=\"PDF thumbnail: Biosecurity Measures for Poultry Transportation\"></a></p>\n</div>\n\n<div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_poultry_1525476449618_eng.pdf\">\n<span>Biosecurity Measures for Poultry Transportation</span><br>\n<span class=\"gc-dwnld-info\"><abbr title=\"Portable Document Format\">PDF</abbr> (267\u00a0<abbr title=\"kilobytes\">KB</abbr>)</span></a>\n</p>\n\n</div>\n\n</div>\n\n</div>\n</section>\n</div>\n\n\n<p>Transporters have a crucial role in preventing or reducing the spread of diseases during transportation. Diseases in poultry can lead to poultry health, human health, and economic consequences. This fact sheet highlights key biosecurity measures to protect poultry from contracting or spreading diseases during transportation and minimize costs due to outbreak of diseases.</p>\n\n<h2>What is biosecurity?</h2>\n\n<p>Biosecurity is a set of measures used to reduce the chance of the introduction and spread of disease-causing organisms and pests. During transportation, poultry can be at risk of contracting infectious disease from exposure to infected poultry, other animals, carcasses and pests (such as rodents and insects) or contact with manure, bodily fluids, dander and aerosols of infected poultry or contaminated trailers, equipment, and people. A chain of strict biosecurity measures is recommended to maintain the health and well-being of poultry at the place of loading, during transportation and at the destination site.</p>\n\n<h2>Why is biosecurity important?</h2>\n\n<p>The transportation and movement of poultry, particularly those that are infected, provide an opportunity to spread infection/diseases such as Salmonella <abbr title=\"sub-species\">spp.</abbr> and avian influenza. The impact and cost of a disease outbreak can far exceed the cost of implementing biosecurity measures to minimize the risk of introduction and spread of infectious diseases. The lack of biosecurity measures during transportation may result in significant financial losses for the transporters and poultry industry.</p>\n\n<h2>Who is responsible for biosecurity?</h2>\n\n<p>Producers, caretakers, transporters and everyone else involved in the transportation of poultry are responsible for ensuring animal health and welfare. It is recommended that biosecurity measures be implemented at all times during loading, on the road and during unloading. Always discuss biosecurity with the producers/bird owners before transportation.</p>\n\n<h2>General Biosecurity Recommendations</h2>\n\n<ul>\n<li>Plan your transportation event.</li>\n<li>Determine and follow biosecurity protocols for departure and destination sites. Respect and follow posted biosecurity signs. For sites where multi-age production occurs, biosecurity to protect the remaining flocks is of utmost importance. </li>\n<li>When accessing sites, avoid muddy or manure-contaminated pathways. Drive slowly to minimize contamination of the undercarriage of the trailer.</li>\n<li>Do not cross-contaminate between the transport unit and the sites.</li>\n<li>Whenever possible, select routes that avoid locations where a disease is identified. Follow disease outbreak updates provided by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, <abbr title=\"United States Department of Agriculture\">USDA</abbr>, and provincial/territorial, or state governments.</li>\n<li>It is recommended that transporters be aware of signs of disease and fitness for transport.</li>\n</ul>\n\n<h2>Biosecurity Best Practices</h2>\n\n<h3>Only load poultry onto clean trailers</h3>\n\n<p>Dirty trailers can spread disease. To reduce the risk of spreading disease:</p>\n\n<ul>\n<li>Clean and disinfect trailers between transportation events.</li>\n<li>Dispose of organic matter (manure, poultry waste, <abbr title=\"et cetera\">etc.</abbr>) at designated locations.</li>\n<li>Use effective and compatible cleaning (detergents, degreasers) and disinfection products; consult specialists as needed. Follow manufacturer's recommended water temperature, concentration and contact time. Ensure that trailer surfaces are visually clean and free of all organic matter before applying the disinfectant.</li>\n<li>Before loading, inspect the trailer to ensure that it is dry and clean.</li>\n</ul>\n\n<h3>Avoid contact with other animals</h3>\n\n<p>Poultry can appear healthy yet still shed pathogens (<abbr title=\"for example\">e.g.</abbr> bacteria, viruses, and fungi) that can put other poultry at risk of an infection. To reduce this risk:</p>\n\n<ul>\n<li>Transport healthy birds. It is in the interest of the producers/flock owners that their birds are appropriately vaccinated at the origin site to prevent transmission of common diseases.</li>\n<li>Avoid parking close to the barn exhaust fans, if possible. Avoid contact with barns and flocks that will remain in production on site.</li>\n<li>Whenever possible, choose a route through areas of lowest poultry density.</li>\n<li>Whenever possible at stops, park away from other poultry and livestock trailers.</li>\n<li>It is recommended that identified sick flocks or flocks from farms with identified illness be transported last, followed by complete cleaning and disinfection.</li>\n</ul>\n\n<h3>People and equipment</h3>\n\n<p>People, crates, modules, dollies, and other equipment that have come into contact with diseased poultry or their environment can pose a risk to poultry. To minimize this risk:</p>\n\n<ul>\n<li>Clean and disinfect crates, modules, and dollies before loading them on a clean and disinfected trailer. It is recommended that equipment used be free of all organic matter before applying disinfectant.</li>\n<li>Do not share equipment between flocks, farms, or with other drivers.</li>\n<li>It is recommended that the catching crew, transporter, and other personnel that come into contact with the poultry being transported wear protective clean clothing and footwear.</li>\n<li>Always clean and sanitize hands before and after contact with poultry, other animals or equipment.</li>\n<li>Discourage people not involved with the transport from coming into contact with the poultry in the shipment, equipment, and trailer.</li>\n<li>Keep the truck cab clean to prevent cross-contamination between transportation events. Disinfect, if necessary. For example, remove dirty coveralls and footwear before entering the cab.</li>\n</ul>\n\n\n<p>For more information, visit the <a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/transportation/eng/1519235033628/1519235183948\">biosecurity standard for livestock, poultry and deadstock transportation</a> on the <a href=\"/eng/1297964599443/1297965645317\">Canadian Food Inspection Agency website</a> at Inspection.gc.ca and <a href=\"http://www.animalhealth.ca/aspx/public/\">Canadian Animal Health Coalition</a> website at Animalhealth.ca.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n\n<section class=\"mwsdownload-link section col-xs-12 col-sm-12 col-md-12 col-lg-10\">\n<div class=\"well well-sm gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\">\n\n<p>\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_poultry_1525476449618_fra.pdf\">\n<img class=\"img-responsive mrgn-lft-sm mrgn-tp-sm gc-dwnld-img\" src=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/images-images/bt-thumbnails-poultry_1525476195697_fra.jpg\" alt=\"Image de PDF : Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille\"></a></p>\n</div>\n\n<div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_poultry_1525476449618_fra.pdf\">\n<span>Mesures de bios\u00e9curit\u00e9 pour le transport de la volaille</span><br>\n<span class=\"gc-dwnld-info\"><abbr title=\"Format de document portable\">PDF</abbr> (347\u00a0<abbr title=\"kilo-octets\">ko</abbr>)</span></a>\n</p>\n\n</div>\n</div>\n</div>\n\n</section>\n</div>\n\n\n<p>Les transporteurs jouent un r\u00f4le crucial dans la  pr\u00e9vention et la r\u00e9duction de la propagation de maladies au cours du transport.  La pr\u00e9sence de maladies chez la volaille peut avoir des cons\u00e9quences sur la sant\u00e9 de la volaille, sur la sant\u00e9 humaine et sur l'\u00e9conomie. Le pr\u00e9sent fiche d'information d\u00e9crit les principales mesures de bios\u00e9curit\u00e9 \u00e0 adopter pour emp\u00eacher  que la volaille contracte ou propage des maladies durant le transport ainsi que  pour r\u00e9duire le plus possible les co\u00fbts d\u00e9coulant d'une \u00e9closion de maladie.</p>\n\n<h2>Qu'est-ce que la bios\u00e9curit\u00e9?</h2>\n\n<p>La bios\u00e9curit\u00e9 est un ensemble de mesures  utilis\u00e9es afin de r\u00e9duire les possibilit\u00e9s d'introduction et de propagation  d'organismes infectieux et nuisibles. Au cours des activit\u00e9s de transport, les  volailles peuvent contracter une maladie infectieuse de par leur exposition \u00e0  de la volaille infect\u00e9e, \u00e0 d'autres animaux, \u00e0 des carcasses d'animaux et \u00e0 des  organismes nuisibles (tels que des rongeurs ou des insectes) ou par le contact  avec du fumier, des liquides corporels, des particules animales et des a\u00e9rosols  provenant de volaille infect\u00e9e ou de remorques, de pi\u00e8ces d'\u00e9quipement ou de  personnes contamin\u00e9es. Il est recommand\u00e9 qu'une s\u00e9rie de mesures de bios\u00e9curit\u00e9  rigoureuses soit \u00e9tablie afin de maintenir la sant\u00e9 et le bien-\u00eatre des  volailles au lieu de chargement, lors du transport ainsi qu'au site de  destination.</p>\n\n<h2>Pourquoi la bios\u00e9curit\u00e9 est-elle importante?</h2>\n\n<p>Le transport et les d\u00e9placements de la volaille, en particulier celle qui est infect\u00e9e, permettent la propagation d'infections  et de maladies telles que Salmonella <abbr title=\"sous-esp\u00e8ce\">spp.</abbr> et l'influenza aviaire. Les r\u00e9percussions et les co\u00fbts d'une \u00e9closion de  maladie peuvent d\u00e9passer largement les co\u00fbts de mise en \u0153uvre de mesures de  bios\u00e9curit\u00e9 visant \u00e0 r\u00e9duire les risques d'introduction et de propagation de  maladies infectieuses. L'absence de mesures de bios\u00e9curit\u00e9 durant les activit\u00e9s  de transport peut entra\u00eener des pertes financi\u00e8res importantes pour les  transporteurs ainsi que pour l'industrie avicole. </p>\n\n<h2>Qui est responsable de la bios\u00e9curit\u00e9?</h2>\n\n<p>Les producteurs, les pr\u00e9pos\u00e9s aux soins des  animaux, les transporteurs et tous ceux qui jouent un r\u00f4le dans le transport  des volailles ont la responsabilit\u00e9 d'assurer la sant\u00e9 et le bien-\u00eatre des  animaux. Il est recommand\u00e9 que des mesures de bios\u00e9curit\u00e9 soient mises en \u0153uvre  en tout temps au cours du chargement, sur la route et au cours du d\u00e9chargement.  Discutez toujours des mesures de bios\u00e9curit\u00e9 avec les producteurs ou  propri\u00e9taires de volailles avant le transport.</p>\n\n<h2>Recommandations g\u00e9n\u00e9rales en mati\u00e8re de bios\u00e9curit\u00e9</h2>\n<ul>\n<li>Planifiez  votre activit\u00e9 de transport.</li>\n<li>D\u00e9terminez  et respectez les protocoles de bios\u00e9curit\u00e9 des sites d'origine et de  destination. Respectez les affiches sur la bios\u00e9curit\u00e9 qui sont en place. Pour  les \u00e9tablissements dans lesquels il y a une production de volailles d'\u00e2ges  diff\u00e9rents, la prise de mesures de bios\u00e9curit\u00e9 visant \u00e0 prot\u00e9ger les troupeaux  restants est d'une importance cruciale.</li>\n<li>Lorsque  vous acc\u00e9dez \u00e0 un site, \u00e9vitez les voies d'acc\u00e8s boueuses ou contamin\u00e9es par du  fumier. Conduisez lentement afin de contaminer le moins possible le ch\u00e2ssis de  la remorque.</li>\n<li>\u00c9liminez  tout risque de contamination crois\u00e9e entre l'unit\u00e9 de transport et les sites.</li>\n<li>\n<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, l'<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr> et les gouvernements des provinces et des \u00c9tats.</li>\n<li>Il est recommand\u00e9 que les transporteurs puissent  reconna\u00eetre les signes d'une maladie et d\u00e9terminer si un animal est apte au  transport.</li>\n</ul>\n\n<h2>Pratiques exemplaires en mati\u00e8re de bios\u00e9curit\u00e9</h2>\n\n<h3>Chargez la volaille uniquement dans des  remorques propres</h3>\n\n<p>Des remorques sales peuvent propager des  maladies. Pour att\u00e9nuer ce risque\u00a0:</p>\n\n<ul>\n<li>Nettoyez  et d\u00e9sinfectez les remorques apr\u00e8s chaque activit\u00e9 de transport. </li>\n<li>\u00c9liminez  toutes les mati\u00e8res organiques  (fumier, d\u00e9chets de volaille, <abbr title=\"et cetera\">etc.</abbr>) \u00e0 un  endroit d\u00e9sign\u00e9. </li>\n<li>Utilisez  des produits nettoyants (d\u00e9tergents et d\u00e9graissants) et d\u00e9sinfectants efficaces  et compatibles; consultez des sp\u00e9cialistes au besoin. Suivez les  recommandations du fabricant concernant la temp\u00e9rature de l'eau, la  concentration du produit et le temps de contact. Veillez \u00e0 ce que les surfaces de la  remorque soient visuellement propres et exemptes de mati\u00e8res organiques avant  l'application du d\u00e9sinfectant.</li>\n<li>Avant  le chargement, inspectez la remorque afin de vous assurer qu'elle est s\u00e8che et  propre.</li>\n</ul>\n\n<h3>\u00c9vitez tout contact avec d'autres animaux</h3>\n\n<p>Des volailles qui semblent en sant\u00e9 peuvent tout  de m\u00eame excr\u00e9ter des pathog\u00e8nes (<abbr title=\"par exemple\">p.\u00a0ex.</abbr>, des bact\u00e9ries, virus et champignons)  qui pourraient infecter d'autres volailles. Afin de r\u00e9duire ce risque\u00a0:</p>\n\n<ul>\n<li>Transportez  des oiseaux en sant\u00e9. Il est dans l'int\u00e9r\u00eat des producteurs ou des  propri\u00e9taires de troupeaux que leurs oiseaux soient ad\u00e9quatement vaccin\u00e9s au  site d'origine pour pr\u00e9venir la transmission de maladies courantes.</li>\n<li>\u00c9vitez  de vous stationner pr\u00e8s des ventilateurs d'\u00e9vacuation du poulailler, dans la  mesure du possible. \u00c9vitez tout contact avec les poulaillers et les  troupeaux qui resteront en  production sur le site. </li>\n<li>Dans  la mesure du possible, choisissez un itin\u00e9raire traversant les zones \u00e0 plus  faible densit\u00e9 de volaille.</li>\n<li>Lors  des arr\u00eats, si cela est possible, stationnez-vous loin des autres remorques  contenant des volailles ou du b\u00e9tail.</li>\n<li>Il est  recommand\u00e9 que les troupeaux identifi\u00e9s comme malades ou les troupeaux de  fermes dans lesquelles une maladie a \u00e9t\u00e9 d\u00e9cel\u00e9e soient transport\u00e9s en dernier,  et que tout soit ensuite nettoy\u00e9 et d\u00e9sinfect\u00e9 compl\u00e8tement.</li>\n</ul>\n\n<h3>Personnes et \u00e9quipement</h3>\n\n<p>Les personnes, palettes, modules, chariots et  autres \u00e9quipements qui sont entr\u00e9s en contact avec des volailles infect\u00e9es ou  avec leur environnement peuvent poser un risque pour vos volailles. Afin de  minimiser ces risques, veuillez respecter les consignes suivantes\u00a0:</p>\n\n<ul>\n<li>Nettoyez  et d\u00e9sinfectez les palettes, les modules et les chariots avant  de les charger sur une remorque propre et d\u00e9sinfect\u00e9e. Il est recommand\u00e9 que tout l'\u00e9quipement  utilis\u00e9 soit exempt de mati\u00e8re organique avant l'application d'un d\u00e9sinfectant. </li>\n<li>Ne  partagez pas votre \u00e9quipement avec d'autres troupeaux, fermes ou avec d'autres  conducteurs. </li>\n<li>Il  est recommand\u00e9 que les \u00e9quipes de capture, les transporteurs et les autres  membres du personnel qui entrent en contact avec la volaille transport\u00e9e  portent des v\u00eatements et des chaussures de protection  propres. </li>\n<li>Lavez  et d\u00e9sinfectez toujours vos mains avant et apr\u00e8s \u00eatre entr\u00e9 en contact avec la  volaille, d'autres animaux ou de l'\u00e9quipement. </li>\n<li>Dissuadez toute personne qui ne prend pas part \u00e0 l'activit\u00e9  de transport d'entrer en contact avec les volailles du  chargement, l'\u00e9quipement et la remorque.</li>\n<li>Gardez la cabine du camion propre afin d'\u00e9viter la contamination crois\u00e9e  entre les activit\u00e9s de transport. D\u00e9sinfectez, au besoin. Par  exemple, retirez les chaussures et v\u00eatements sales avant d'entrer dans la  cabine.</li>\n</ul>\n\n<p>Pour plus de renseignements, veuillez consulter les <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/transport/fra/1519235033628/1519235183948\">normes en mati\u00e8re de bios\u00e9curit\u00e9 pour le transport du b\u00e9tail, de la volaille et des carcasses d'animaux</a> sur le <a href=\"/fra/1297964599443/1297965645317\">site Web de l'Agence canadienne d'inspection des aliments</a> \u00e0 l'adresse Inspection.gc.ca et sur celui de la <a href=\"http://www.animalhealth.ca/aspx/public/index.aspx?languageid=fr\">Coalition canadienne pour la sant\u00e9 des animaux</a> \u00e0 l'adresse Animalhealth.ca.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}