{
    "dcr_id": "1477322471001",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Silene vulgaris</i> (Bladder campion)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Silene vulgaris</i> (Sil\u00e8ne enfl\u00e9)"
    },
    "html_modified": "2024-01-26 2:23:28 PM",
    "modified": "2017-11-06",
    "issued": "2017-11-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_silene_vulgaris_1477322471001_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_silene_vulgaris_1477322471001_fra"
    },
    "ia_id": "1477322471358",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Silene vulgaris</i> (Bladder campion)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Silene vulgaris</i> (Sil\u00e8ne enfl\u00e9)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Silene vulgaris</i> (Bladder campion)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Silene vulgaris</i> (Sil\u00e8ne enfl\u00e9)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1477322471358",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/silene-vulgaris/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/silene-vulgaris/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Silene vulgaris (Bladder campion)",
            "fr": "Semence de mauvaises herbe : Silene vulgaris (Sil\u00e8ne enfl\u00e9)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Silene vulgaris, Caryophyllaceae, Bladder campion",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, esp\u00e8ces, Silene vulgaris, Caryophyllaceae, Sil\u00e8ne enfl\u00e9"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-06",
            "fr": "2017-11-06"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Silene vulgaris (Bladder campion)",
        "fr": "Semence de mauvaises herbe : Silene vulgaris (Sil\u00e8ne enfl\u00e9)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Caryophyllaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Bladder campion</p>\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 3 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs throughout Canada except in <abbr title=\"Northwest Territories\">NT</abbr> and <abbr title=\"Nunavut\">NU</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). Very common in the eastern provinces (Darbyshire 2003<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, Europe and temperate Asia and introduced in   North America and South America, South Africa, Japan, Australia and New   Zealand (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). In the United States, found in most states except for some in the south (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Natural Resources Conservation Service\">NRCS</abbr> 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 1.0 - 1.8 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 1.0 - 1.3 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed angular D-shaped, compressed, with the hilum in the middle of the flat edge</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed covered in tubercles arranged in rows</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed grey to greyish-black</li>\n<li>Immature seeds are reddish</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hilum flanked on each side by two long, raised pads with rectangular cells on their surface</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, old fields, pastures, hay fields, gardens, shores, gravel pits, roadsides, disturbed areas (Royer and Dickinson 1999<sup id=\"fn5a-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>, Darbyshire 2003<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>, eFloras 2016<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). Not a serious weed in cultivated fields but can become weedy in zero to minimum-till fields (Royer and Dickinson 1999<sup id=\"fn5b-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Introduction of bladder campion into North America appears to have   occurred in the late 18th or early 19th century via ship ballast or   contaminated clover seed (Taylor and Keller 2007<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote\u00a0</span>7</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Night-flowering catchfly (<i lang=\"la\">Silene noctiflora</i>)</h3>\n<ul>\n<li>Similar D-shape, size and pads near hilum as bladder campion.</li>\n\n<li>Bladder campion seed tends to be greyish in colour, has two large pads near the hilum, tubercles in rows, and is angular. Night-flowering catchfly seed may have a yellowish bloom, or is reddish in colour. The hilum is bordered by two small pads, tubercles randomly arranged and the seed is rounded.</li>\n</ul>\n<h3>White cockle (<i lang=\"la\">Silene latifolia</i> <abbr title=\"subspecies\">subsp.</abbr> <i lang=\"la\">alba</i>)</h3>\n<ul>\n<li>Similar D-shape and size as bladder campion.</li>\n\n<li>Bladder campion seed is usually greyish. The hilum is bordered by two large pads, with surface tubercles in rows and is angular. White cockle seed is usually covered in a whitish bloom, has an 'open mouth' hilum, with an inflated shape and random tubercle arrangement.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_05cnsh_1476893528028_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Bladder campion (<i lang=\"la\">Silene vulgaris</i>) seeds</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_img5_1509981029416_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Bladder campion (<i lang=\"la\">Silene vulgaris</i>) seed, hilum view</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_01cnsh_1476893383810_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Bladder campion (<i lang=\"la\">Silene vulgaris</i>) seed</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_02cnsh_1476893463771_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Bladder campion (<i lang=\"la\">Silene vulgaris</i>) seed, profile</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_04cnsh_1476893494916_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Bladder campion (<i lang=\"la\">Silene vulgaris</i>) seed, hilum (close-up view)</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_noctiflora_05cnsh_1476893327321_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Night-flowering catchfly (<i lang=\"la\">Silene noctiflora</i>) seeds</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_noctiflora_04cnsh_1476893292190_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Night-flowering catchfly (<i lang=\"la\">Silene noctiflora</i>) seed, hilum (close-up view)</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_latifolia_05cnsh_1476893213354_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: White cockle (<i lang=\"la\">Silene latifolia</i> <abbr title=\"subspecies\">subsp.</abbr> <i lang=\"la\">alba</i>) seeds</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_latifolia_01cnsh_1476893155644_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: White cockle (<i lang=\"la\">Silene latifolia</i> <abbr title=\"subspecies\">subsp.</abbr> <i lang=\"la\">alba</i>) seed, hilum view</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Natural Resources Conservation Service\">NRCS</abbr>. 2016</strong>. The PLANTS Database. National Plant Data Center, Baton Rouge, LA USA, http://plants.usda.gov [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Royer, F. and Dickinson, R. 1999</strong>. Weeds of Canada and the Northern United States. The University of Alberta Press/Lone Pine Publishing, Edmonton, Alberta.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>eFloras. 2016</strong>. Electronic Floras. Missouri Botanical Garden, St. Louise, MO &amp; Harvard University Herbaria, Cambridge, MA., http://www.efloras.org [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 7</dt>\n<dd id=\"fn7\">\n<p><strong>Taylor, D. R. and Keller, S. R. 2007</strong>. Historical Range Expansion Determines the Phylogenetic Diversity Introduced During Contemporary Species Invasion. Evolution: Volume 61 (2): 334-345.</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>7<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Caryophyllaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Sil\u00e8ne enfl\u00e9</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie 3 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong>Pr\u00e9sente dans l'ensemble du Canada, sauf dans les <abbr title=\"Territoires du Nord-Ouest\">T.N-O.</abbr> et au <abbr title=\"Nunavut\">Nt</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>). Tr\u00e8s commune dans les provinces de l'Est (<span lang=\"en\">Darbyshire</span>, 2003<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Afrique septentrionale, d'Europe et de l'Asie temp\u00e9r\u00e9e et introduite en Am\u00e9rique du Nord et en Am\u00e9rique du Sud, en Afrique du Sud, au Japon, en Australie et en Nouvelle-Z\u00e9lande (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>). Aux \u00c9tats-Unis, elle est pr\u00e9sente dans la plupart des \u00c9tats, sauf dans certains \u00c9tats du Sud (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Natural Resources Conservation Service\">NRCS</abbr>, 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 1,0 \u00e0 1,8 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 1,0 \u00e0 1,3 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine anguleuse en forme de D, comprim\u00e9e; hile situ\u00e9 au milieu du c\u00f4t\u00e9 plat</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine recouverte de tubercules dispos\u00e9s en rang\u00e9es</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine de couleur grise \u00e0 noir gris\u00e2tre</li>\n<li>Graines immatures rouge\u00e2tres</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hile flanqu\u00e9 de chaque c\u00f4t\u00e9 de deux longs coussinets soulev\u00e9s avec des cellules rectangulaires \u00e0 leur surface</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, champs abandonn\u00e9s, p\u00e2turages, pr\u00e9s de fauche, jardins, rivages, gravi\u00e8res, bords de chemin, terrains perturb\u00e9s (<span lang=\"en\">Royer</span> et <span lang=\"en\">Dickinson</span>, 1999<sup id=\"fn5a-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>; <span lang=\"en\">Darbyshire</span>, 2003<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>; eFloras, 2016<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>). N'est pas une mauvaise herbe importante dans les champs cultiv\u00e9s, mais elle peut devenir probl\u00e9matique dans les syst\u00e8mes culturaux de semis direct ou de travail r\u00e9duit du sol (<span lang=\"en\">Royer</span> et <span lang=\"en\">Dickinson</span>, 1999<sup id=\"fn5b-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le sil\u00e8ne enfl\u00e9 se serait introduit en Am\u00e9rique du Nord \u00e0 la fin du 18e\u00a0si\u00e8cle ou au d\u00e9but du 19e\u00a0si\u00e8cle dans les eaux de ballast de navires ou dans de la semence de tr\u00e8fle contamin\u00e9e (<span lang=\"en\">Taylor</span> et <span lang=\"en\">Keller</span>, 2007<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>7</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Sil\u00e8ne noctiflore (<i lang=\"la\">Silene noctiflora</i>)</h3>\n<ul>\n<li>La graine du sil\u00e8ne noctiflore ressemble \u00e0 celle du sil\u00e8ne enfl\u00e9 par son contour en forme de D, ses dimensions et la pr\u00e9sence de coussinets pr\u00e8s du hile.</li>\n\n<li>La graine du sil\u00e8ne enfl\u00e9 est g\u00e9n\u00e9ralement gris\u00e2tre, est flanqu\u00e9e de deux gros coussinets pr\u00e8s du hile, a des tubercules dispos\u00e9s en rang\u00e9es et est anguleuse. La graine du sil\u00e8ne noctiflore peut avoir une pruine jaun\u00e2tre ou \u00eatre de couleur rouge\u00e2tre. Le hile est bord\u00e9 de deux petits coussinets, les tubercules sont dispos\u00e9s al\u00e9atoirement et la graine est arrondie.</li>\n</ul>\n<h3>Lychnide blanche (<i lang=\"la\">Silene latifolia</i> <abbr title=\"sous-esp\u00e8ces\">subsp.</abbr> <i lang=\"la\">alba</i>)</h3>\n<ul>\n<li>La graine de la lychnide blanche ressemble \u00e0 celle du sil\u00e8ne enfl\u00e9 par sa forme en D et ses dimensions.</li>\n\n<li>La graine du sil\u00e8ne enfl\u00e9 est g\u00e9n\u00e9ralement gris\u00e2tre. Le hile est bord\u00e9 de deux gros coussinets avec des tubercules \u00e0 la surface dispos\u00e9s en rang\u00e9es et est anguleux. La graine de la lychnide blanche est g\u00e9n\u00e9ralement recouverte d'une pruine blanch\u00e2tre, son hile a l'air d'une bouche ouverte et est enfl\u00e9 et la disposition des tubercules est al\u00e9atoire.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_05cnsh_1476893528028_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Sil\u00e8ne enfl\u00e9 (<i lang=\"la\">Silene vulgaris</i>) graines</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_img5_1509981029416_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Sil\u00e8ne enfl\u00e9 (<i lang=\"la\">Silene vulgaris</i>) graines, vue hile</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_01cnsh_1476893383810_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Sil\u00e8ne enfl\u00e9 (<i lang=\"la\">Silene vulgaris</i>) graine</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_02cnsh_1476893463771_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Sil\u00e8ne enfl\u00e9 (<i lang=\"la\">Silene vulgaris</i>) graine, profil</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_vulgaris_04cnsh_1476893494916_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Sil\u00e8ne enfl\u00e9 (<i lang=\"la\">Silene vulgaris</i>) graine, hile (vue rapproch\u00e9e) </figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_noctiflora_05cnsh_1476893327321_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable: Sil\u00e8ne noctiflore (<i lang=\"la\">Silene noctiflora</i>) graines</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_noctiflora_04cnsh_1476893292190_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable: Sil\u00e8ne noctiflore (<i lang=\"la\">Silene noctiflora</i>) graine, hile (vue rapproch\u00e9e)</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_latifolia_05cnsh_1476893213354_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable: Lychnide blanche (<i lang=\"la\">Silene latifolia</i> <abbr title=\"sous-esp\u00e8ces\">subsp.</abbr> <i lang=\"la\">alba</i>) graines</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_silene_latifolia_01cnsh_1476893155644_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable: Lychnide blanche (<i lang=\"la\">Silene latifolia</i> <abbr title=\"sous-esp\u00e8ces\">subsp.</abbr> <i lang=\"la\">alba</i>) graine, vue hile</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Natural Resources Conservation Service\">NRCS</abbr>. 2016</strong>. The PLANTS Database. National Plant Data Center, Baton Rouge, LA USA, http://plants.usda.gov [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Royer, F. and Dickinson, R. 1999</strong>. Weeds of Canada and the Northern United States. The University of Alberta Press/Lone Pine Publishing, Edmonton, Alberta.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>eFloras. 2016</strong>. Electronic Floras. Missouri Botanical Garden, St. Louise, MO &amp; Harvard University Herbaria, Cambridge, MA., http://www.efloras.org [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n\n<dt>Note de bas de page 7</dt>\n<dd id=\"fn7\">\n<p lang=\"en\"><strong>Taylor, D. R. and Keller, S. R. 2007</strong>. Historical Range Expansion Determines the Phylogenetic Diversity Introduced During Contemporary Species Invasion. Evolution: Volume 61 (2): 334-345.</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>7</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}