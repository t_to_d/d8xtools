{
    "dcr_id": "1335968583875",
    "title": {
        "en": "Plant Breeders' Rights... What are they?",
        "fr": "En quoi consiste la protection des obtentions v\u00e9g\u00e9tales?"
    },
    "html_modified": "2024-01-26 2:21:07 PM",
    "modified": "2015-04-22",
    "issued": "2015-04-13 13:15:44",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_bred_whatinformation_1335968583875_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_bred_whatinformation_1335968583875_fra"
    },
    "ia_id": "1335969867075",
    "parent_ia_id": "1299169455265",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299169455265",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Plant Breeders' Rights... What are they?",
        "fr": "En quoi consiste la protection des obtentions v\u00e9g\u00e9tales?"
    },
    "label": {
        "en": "Plant Breeders' Rights... What are they?",
        "fr": "En quoi consiste la protection des obtentions v\u00e9g\u00e9tales?"
    },
    "templatetype": "content page 1 column",
    "node_id": "1335969867075",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299169386050",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plant-breeders-rights/overview/",
        "fr": "/varietes-vegetales/protection-des-obtentions-vegetales/apercu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Plant Breeders' Rights... What are they?",
            "fr": "En quoi consiste la protection des obtentions v\u00e9g\u00e9tales?"
        },
        "description": {
            "en": "Plant breeders\u2019 rights are a form of intellectual property rights that allow plant breeders to protect new varieties of plants. Other forms of intellectual property protection include patents, trademarks and copyrights.",
            "fr": "La protection des obtentions v\u00e9g\u00e9tales (POV) est une forme de protection des droits de propri\u00e9t\u00e9 intellectuelle qui permet aux s\u00e9lectionneurs de v\u00e9g\u00e9taux de prot\u00e9ger les nouvelles vari\u00e9t\u00e9s v\u00e9g\u00e9tales. Parmi les autres formes de protection de la propri\u00e9t\u00e9 intellectuelle, mentionnons les brevets, les marques de fabrique et les droits d\u2019auteur."
        },
        "keywords": {
            "en": "Plant Breeders Rights, Three Part Process Cost of Protection, International Union, Protection of New Varieties of Plants, Plant Biosafety Variety Registration",
            "fr": "protection des obtentions v&#233;g&#233;tales, Liste des vari&#233;t&#233;s, Bios&#233;curit&#233; v&#233;g&#233;tale, droits dobtentions v&#233;g&#233;tales, Bulletin des vari&#233;t&#233;s v&#233;g&#233;tales"
        },
        "dcterms.subject": {
            "en": "retail trade,plants,products",
            "fr": "commerce de d\u00e9tail,plante,produit"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Breeders' Rights Office",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Bureau de la protection des obtentions v\u00e9g\u00e9tales"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-13 13:15:44",
            "fr": "2015-04-13 13:15:44"
        },
        "modified": {
            "en": "2015-04-22",
            "fr": "2015-04-22"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Plant Breeders' Rights... What are they?",
        "fr": "En quoi consiste la protection des obtentions v\u00e9g\u00e9tales?"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Plant breeders' rights are a form of intellectual property rights that allow plant breeders to protect new varieties of plants. Other forms of intellectual property protection include patents, trademarks and copyrights.</p>\n<p>When plant breeders' rights are granted, the breeder gets exclusive rights in relation to propagating material of their new plant variety.</p>\n<h2>What is a variety?</h2>\n<p>A plant variety is a group of plants within a single species or sub-species that</p>\n<ul>\n<li>can be distinguished from other plant groups of that same species by at least one characteristic, and</li>\n<li>can be reproduced unchanged into the next generation.</li>\n<li>is homogeneous in that all plants in the group look the same.</li>\n</ul>\n<p>For instance, most of us recognize that there are many different varieties of apples such as Granny Smith, McIntosh and Red Delicious. Although they are all the same species, each variety can be distinguished from the others by the fruit shape, size or colour and other similar types of characteristics.</p>\n<h2>What is propagating material?</h2>\n<p>Propagating material is the part of the plant that is used to reproduce the variety. For example, many field crops such as corn and wheat, and vegetables such as beans and cucumbers, are grown from seed. Consequently, the propagating material for these crops is the seed.</p>\n<p>Other plants are reproduced vegetatively from plant cuttings or budwood. This includes many ornamental plants, such as roses and spirea, and most fruit, such as apples and strawberries. Even though these types of plants commonly produce seeds, it is the cuttings that are considered to be the propagating material.</p>\n<h2>What is plant breeding?</h2>\n<p>Plant breeding has existed for thousands of years. It is the science of working with the genetics of plants to produce new plants with desired characteristics. Today's breeders can be gardeners, farmers, or professionals who work for government research centres, universities, private companies or industry associations.</p>\n<p>The techniques of plant breeding range from simply selecting certain plants for multiplication, to deliberate crossing of individual plants to develop new varieties with a combination of desired characteristics. Modern technology has expanded the scope of plant breeding to also include complex molecular techniques.</p>\n<h2>What are the benefits of plant breeders' rights for the breeder?</h2>\n<p>A plant breeder who obtains plant breeders' rights usually collects royalties each time propagating material of the protected plant variety is sold, similar to the way an author collects royalties on a copyrighted book.</p>\n<h2>What are the benefits of plant breeders' rights for Canadians?</h2>\n<p>An effective plant breeders' rights system encourages investment in plant breeding programs which result in the development and promotion of new, improved and innovative plant varieties.</p>\n<p>A strong plant breeding program can</p>\n<ul>\n<li>increase yields in crop varieties</li>\n<li>improve quality in new varieties, leading to higher value products with increased marketability</li>\n<li>improve nutritional content in new varieties, leading to human health benefits,</li>\n<li>enhance disease resistance and stress tolerance, resulting in reduced losses</li>\n<li>reduce the need for fertilizers and pesticides, leading to environmental benefits</li>\n<li>provide a wider selection of plant varieties for landscaping, cut flowers and potted plants</li>\n</ul>\n<h2>Who administers plant breeders' rights?</h2>\n<p>Since 1990, Canada has had legislation called the <em>Plant Breeders' Rights Act.</em> This law is administered by the Canadian Food Inspection Agency's Plant Breeders' Rights Office.</p>\n<p>Canada's Plant Breeders' Rights program is voluntary: a breeder may choose to protect a new variety or not. The grant of a plant breeder's right <strong>does not</strong> overrule any other mandatory regulatory requirements.</p>\n<p>Plant breeders' rights that are granted in Canada only apply in Canada. To protect a variety in another country, the breeder must make a separate application in that jurisdiction.</p>\n<h2>Can a variety of any type of plant be protected by plant breeders' rights?</h2>\n<p>In Canada, varieties of all plant species are eligible for protection. Algae, fungi and bacteria cannot be protected under plant breeders' rights.</p>\n<h2>Can heritage varieties be protected by plant breeders' rights?</h2>\n<p>In most cases, heritage varieties are not eligible for protection by plant breeders' rights because they do not meet the requirement of being \"new\".</p>\n<h2>How is a plant variety evaluated?</h2>\n<p>Before being granted plant breeders' rights, new plant varieties must be evaluated in comparative growing trials to confirm that they are</p>\n<ul>\n<li>distinct from all varieties of common knowledge, and</li>\n<li>sufficiently uniform and stable in their characteristics.</li>\n</ul>\n<h2>Exceptions to plant breeders' rights</h2>\n<p>Plant breeders' rights do not restrict anyone from using the protected variety for</p>\n<ul>\n<li>private and non-commercial purposes,</li>\n<li>experimental purposes,</li>\n<li>breeding and developing new plant varieties, and</li>\n<li>storing and saving seed harvested from a protected variety for planting by farmers on their own land.</li>\n</ul>\n<h2>How long do plant breeders' rights last?</h2>\n<p>Once granted, plant breeders' rights can last for up to 25 years in the case of a variety of tree and vine (including their rootstocks), and 20 years in the case of all other varieties of plants. The breeder maintains their rights by paying an annual fee. The breeder can surrender the rights anytime before the end of the 20 or 25 year period.</p>\n<p>You can contact us by mail at:</p>\n<p>Plant Breeders' Rights Office<br> Canadian Food Inspection Agency<br> 59 Camelot Drive, Ottawa, Ontario K1A 0Y9</p>\n<p></p>\n<ul>\n<li><a href=\"/plant-varieties/plant-breeders-rights/overview/guide/eng/1409074255127/1409074255924\">Guide to Plant Breeders' Rights</a></li>\n<li><a href=\"/plant-varieties/plant-breeders-rights/overview/impact-upov/eng/1409072983279/1409073464633\">Questions and Answers: The Impact of the International Union for the Protection of New Varieties of Plants (UPOV) Conventions on Plant Breeders' Rights in Canada</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La protection des obtentions v\u00e9g\u00e9tales (POV) est une forme de protection des droits de propri\u00e9t\u00e9 intellectuelle qui permet aux obtenteurs de v\u00e9g\u00e9taux de prot\u00e9ger les nouvelles vari\u00e9t\u00e9s v\u00e9g\u00e9tales. Parmi les autres formes de protection de la propri\u00e9t\u00e9 intellectuelle, mentionnons les brevets, les marques de fabrique et les droits d'auteur.</p>\n<p>Lorsque les droits de <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> sont accord\u00e9s, l'obtenteur obtient des droits exclusifs en ce qui concerne le mat\u00e9riel de multiplication de sa nouvelle vari\u00e9t\u00e9 v\u00e9g\u00e9tale.</p>\n<h2>Qu'est ce qu'une vari\u00e9t\u00e9?</h2>\n<p>Une vari\u00e9t\u00e9 v\u00e9g\u00e9tale est un groupe de v\u00e9g\u00e9taux d'une seule esp\u00e8ce ou sous esp\u00e8ce qui\u00a0:</p>\n<ul>\n<li>se distinguent des autres groupes de la m\u00eame esp\u00e8ce par au moins une caract\u00e9ristique;</li>\n<li>peuvent \u00eatre reproduits jusqu'\u00e0 la nouvelle g\u00e9n\u00e9ration sans \u00eatre modifi\u00e9s ;</li>\n<li>est homog\u00e8ne de telle sorte que toutes les plantes dans le groupe se resemble.</li>\n</ul>\n<p>Par exemple, la plupart d'entre nous reconnaissent qu'il existe de nombreuses vari\u00e9t\u00e9s diff\u00e9rentes de pommes, comme les <span lang=\"en\">Granny Smith</span>, les <span lang=\"en\">McIntosh</span>, et les <span lang=\"en\">Red Delicious</span>. Bien qu'il s'agisse de fruits de la m\u00eame esp\u00e8ce, chaque vari\u00e9t\u00e9 se distingue des autres par la forme ou la taille du fruit, la couleur et d'autres caract\u00e9ristiques semblables.</p>\n<h2>Qu'est ce que le mat\u00e9riel de multiplication?</h2>\n<p>Le mat\u00e9riel de multiplication est la partie d'un v\u00e9g\u00e9tal qui est utilis\u00e9e pour reproduire une vari\u00e9t\u00e9. Par exemple, bon nombre de cultures de grande production, notamment le ma\u00efs et le bl\u00e9, et de l\u00e9gumes, comme les f\u00e8ves et les concombres, sont cultiv\u00e9s \u00e0 partir de semences. Par cons\u00e9quent, les semences constituent le mat\u00e9riel de multiplication de ces cultures.</p>\n<p>D'autres v\u00e9g\u00e9taux sont reproduits par multiplication v\u00e9g\u00e9tative \u00e0 partir de boutures ou de bois de greffe, notamment de nombreuses plantes ornementales, comme les roses et les spir\u00e9es, et la plupart des fruits, comme les pommes et les fraises. Bien que ces types de v\u00e9g\u00e9taux produisent g\u00e9n\u00e9ralement des graines, on consid\u00e8re que les boutures constituent le mat\u00e9riel de multiplication.</p>\n<h2>Qu'est ce que l'obtention des v\u00e9g\u00e9taux?</h2>\n<p>L'obtention des v\u00e9g\u00e9taux existe depuis des milliers d'ann\u00e9es. Il s'agit de la science de la manipulation g\u00e9n\u00e9tique des v\u00e9g\u00e9taux en vue de produire de nouveaux v\u00e9g\u00e9taux poss\u00e9dant certaines caract\u00e9ristiques souhait\u00e9es. Les obtenteurs d'aujourd'hui peuvent \u00eatre des jardiniers, des agriculteurs ainsi que des professionnels qui travaillent pour des centres de recherche gouvernementaux, des universit\u00e9s, des entreprises priv\u00e9es ou des associations de l'industrie.</p>\n<p>Les techniques d'obtention des v\u00e9g\u00e9taux vont de la simple s\u00e9lection de certains v\u00e9g\u00e9taux aux fins de reproduction au croisement d\u00e9lib\u00e9r\u00e9 de diff\u00e9rents v\u00e9g\u00e9taux en vue de cr\u00e9er de nouvelles vari\u00e9t\u00e9s poss\u00e9dant une combinaison de caract\u00e9ristiques souhait\u00e9es. La technologie moderne a permis d'accro\u00eetre la port\u00e9e de cette activit\u00e9 de mani\u00e8re \u00e0 ce qu'elle comprenne \u00e9galement des techniques mol\u00e9culaires complexes.</p>\n<h2>Quels avantages les obtenteurs tirent ils de la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>?</h2>\n<p>Un obtenteur qui b\u00e9n\u00e9ficie de la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> per\u00e7oit habituellement des redevances chaque fois que du mat\u00e9riel de multiplication de la vari\u00e9t\u00e9 v\u00e9g\u00e9tale prot\u00e9g\u00e9e est vendu, un peu comme un auteur per\u00e7oit des redevances pour un livre prot\u00e9g\u00e9 par le droit d'auteur.</p>\n<h2>Quels avantages les Canadiens tirent ils de la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>?</h2>\n<p>Un syst\u00e8me efficace de <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> favorise des investissements dans les programmes d'obtention des v\u00e9g\u00e9taux qui ont pour r\u00e9sultat la cr\u00e9ation et la promotion de nouvelles vari\u00e9t\u00e9s v\u00e9g\u00e9tales am\u00e9lior\u00e9es et innovatrices.</p>\n<p>Un programme solide de s\u00e9lection des v\u00e9g\u00e9taux peut procurer des avantages comme\u00a0:</p>\n<ul>\n<li>un rendement plus \u00e9lev\u00e9 des vari\u00e9t\u00e9s de cultures;</li>\n<li>une qualit\u00e9 accrue des nouvelles vari\u00e9t\u00e9s se traduisant par des produits de plus grandes valeur et commercialit\u00e9;</li>\n<li>des avantages pour la sant\u00e9 humaine en raison du contenu nutritionnel am\u00e9lior\u00e9 des nouvelles vari\u00e9t\u00e9s;</li>\n<li>des pertes moins \u00e9lev\u00e9es gr\u00e2ce \u00e0 l'augmentation de la r\u00e9sistance aux maladies et de la tol\u00e9rance au stress;</li>\n<li>des avantages pour l'environnement en raison du besoin moindre en engrais et en pesticides;</li>\n<li>l'acc\u00e8s \u00e0 un plus grand choix de vari\u00e9t\u00e9s v\u00e9g\u00e9tales pour l'am\u00e9nagement paysager ou la production de fleurs coup\u00e9es et de plantes en pot.</li>\n</ul>\n<h2>Qui administre la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>?</h2>\n<p>Depuis 1990, le Canada dispose d'une loi intitul\u00e9e <em>Loi sur la protection des obtentions v\u00e9g\u00e9tales</em>, laquelle est administr\u00e9e par le Bureau de la protection des obtentions v\u00e9g\u00e9tales de l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<p>La participation au programme de <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> du Canada est facultative. En effet, un obteneur peut choisir de prot\u00e9ger ou non une nouvelle vari\u00e9t\u00e9. L'octroi de droits de <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> aux obteneur <strong>ne</strong> pr\u00e9vaut <strong>pas</strong> sur les autres exigences r\u00e9glementaires obligatoires.</p>\n<p>Les droits de <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> octroy\u00e9s au Canada ne sont valides qu'au pays. Un obteneur qui souhaite prot\u00e9ger une m\u00eame vari\u00e9t\u00e9 dans un autre pays doit y pr\u00e9senter une demande distincte.</p>\n<h2>Est ce que toute vari\u00e9t\u00e9 v\u00e9g\u00e9tale peut faire l'objet d'une <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>?</h2>\n<p>Au Canada, des vari\u00e9t\u00e9s de toutes les esp\u00e8ces v\u00e9g\u00e9tales peuvent \u00eatre prot\u00e9g\u00e9es, sauf les algues, les champignons et les bact\u00e9ries.</p>\n<h2>Les vari\u00e9t\u00e9s anciennes peuvent elles faire l'objet d'une <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>?</h2>\n<p>Dans la plupart des cas, les vari\u00e9t\u00e9s anciennes ne peuvent \u00eatre prot\u00e9g\u00e9es en vertu de la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>, car elles ne satisfont pas \u00e0 l'exigence selon laquelle il doit s'agir de \u00ab nouvelles \u00bb vari\u00e9t\u00e9s.</p>\n<h2>Comment la vari\u00e9t\u00e9 v\u00e9g\u00e9tale est elle \u00e9valu\u00e9e?</h2>\n<p>Avant que les nouvelles vari\u00e9t\u00e9s v\u00e9g\u00e9tales puissent faire l'objet d'une <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>, il faut les \u00e9valuer par le biais d'essais de culture comparatif afin de confirmer\u00a0:</p>\n<ul>\n<li>qu'elles se distinguent de toutes les vari\u00e9t\u00e9s notoirement connue;</li>\n<li>que leurs caract\u00e9ristiques sont suffisamment homog\u00e8nes et stables.</li>\n</ul>\n<h2>Exceptions \u00e0 la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr></h2>\n<p>La <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr> n'emp\u00eache personne d'utiliser une vari\u00e9t\u00e9 prot\u00e9g\u00e9e \u00e0 de nombreuses fins, notamment\u00a0:</p>\n<ul>\n<li>priv\u00e9es et non commerciales;</li>\n<li>exp\u00e9rimentales;</li>\n<li>de cr\u00e9er de nouvelles vari\u00e9t\u00e9s ;</li>\n<li>de conserver et d'utiliser des semences r\u00e9colt\u00e9es des vari\u00e9t\u00e9s prot\u00e9g\u00e9es pour la cultivation et plantation par un agriculteur sur ses propre terre.</li>\n</ul>\n<h2>Quelle est la dur\u00e9e de la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>?</h2>\n<p>Une fois que les droits de protection ont \u00e9t\u00e9 octroy\u00e9s, ils peuvent demeurer en vigueur pendant 25 ans pour une vari\u00e9t\u00e9 d'arbre et de vigne (ainsi que leurs porte-greffes), et de 20 ans pour toutes les autres vari\u00e9t\u00e9 v\u00e9g\u00e9tale. Pour continuer de se pr\u00e9valoir de la <abbr title=\"protection des obtentions v\u00e9g\u00e9tales\">POV</abbr>, un obtenteur doit payer des frais annuels. Il peut choisir de renoncer \u00e0 ces droits \u00e0 n'importe quel moment avant la fin de la p\u00e9riode de 20 ou 25 ans.</p>\n<p>Vous pouvez communiquer avec nous par la poste \u00e0 l'adresse suivante\u00a0:</p>\n<p>Bureau de la protection des obtentions v\u00e9g\u00e9tales<br> Agence canadienne d'inspection des aliments<br> 59, promenade Camelot, Ottawa (Ontario) K1A 0Y9</p>\n<ul>\n<li><a href=\"/varietes-vegetales/protection-des-obtentions-vegetales/apercu/guide/fra/1409074255127/1409074255924\">Guide sur les droits d'obtentions v\u00e9g\u00e9tales au Canada</a></li>\n<li><a href=\"/varietes-vegetales/protection-des-obtentions-vegetales/apercu/repercussions-upov/fra/1409072983279/1409073464633\">Questions et r\u00e9ponses\u00a0: R\u00e9percussions de la Convention de l'Union internationale pour la protection des obtentions v\u00e9g\u00e9tales (UPOV) sur la protection des obtentions v\u00e9g\u00e9tales au Canada</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}