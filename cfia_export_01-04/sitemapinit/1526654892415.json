{
    "dcr_id": "1526654892166",
    "title": {
        "en": "Food-specific requirements and guidance \u2013 Fresh fruits or vegetables",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Fruits ou l\u00e9gumes frais"
    },
    "html_modified": "13-6-2018",
    "modified": "27-6-2022",
    "issued": "18-5-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/food_sfcr_specific_fresh_fruits_veg_1526654892166_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/food_sfcr_specific_fresh_fruits_veg_1526654892166_fra"
    },
    "parent_ia_id": "1521720158588",
    "ia_id": "1526654892415",
    "parent_node_id": "1521720158588",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food-specific requirements and guidance \u2013 Fresh fruits or vegetables",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Fruits ou l\u00e9gumes frais"
    },
    "label": {
        "en": "Food-specific requirements and guidance \u2013 Fresh fruits or vegetables",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Fruits ou l\u00e9gumes frais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1526654892415",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1521720158031",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-guidance-by-commodity/fresh-fruits-or-vegetables/",
        "fr": "/exigences-et-documents-d-orientation-relatives-a-c/fruits-ou-legumes-frais/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food-specific requirements and guidance \u2013 Fresh fruits or vegetables",
            "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Fruits ou l\u00e9gumes frais"
        },
        "description": {
            "en": "Before you review information related specifically to fresh fruits or vegetables, read the information on General food requirements and guidance.",
            "fr": "Avant de passer en revue les renseignements portant pr\u00e9cis\u00e9ment sur les fruits ou les l\u00e9gumes frais, veuillez lire les Exigences et lignes directrices g\u00e9n\u00e9rales sur les aliments."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, food, Safe Food for Canadians Regulations, SFCR, Food-specific requirements and guidance, Fresh fruits, vegetables, Part 6, Division 6",
            "fr": "Agence canadienne d'inspection des aliments, aliments, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, exigences propres aux fruits ou l\u00e9gumes frais, lignes directrices, Fruits frais, l\u00e9gumes frais, Partie 6, section 6"
        },
        "dcterms.subject": {
            "en": "food,food labeling,information,food inspection,agri-food products,regulations,food safety,food processing",
            "fr": "aliment,\u00e9tiquetage des aliments,information,inspection des aliments,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2022-06-27",
            "fr": "2022-06-27"
        },
        "type": {
            "en": "legislation and regulations,reference material",
            "fr": "l\u00e9gislation et r\u00e8glements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food-specific requirements and guidance \u2013 Fresh fruits or vegetables",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Fruits ou l\u00e9gumes frais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1526992762174\"></div>\n\n<section class=\"alert alert-info\">\n<h2>Information</h2>\n<p class=\"mrgn-tp-0\">Before you review information related specifically to fresh fruits or vegetables, read the information on <a href=\"/eng/1512149634601/1512149659119\">General food requirements and guidance</a>. The <i>Safe Food for Canadians Regulations</i> (SFCR) set out many requirements that apply across food commodities, along with the specific requirements highlighted here.</p>\n\n<p>Additional requirements for <a href=\"/importing-food-plants-or-animals/food-imports/eng/1526656151226/1526656151476\">Food imports</a>\n<a href=\"/exporting-food-plants-or-animals/food-exports/eng/1323723342834/1323723662195\">Food exports</a> may apply also to your business.</p>\n</section>\n\n<h2>Know the requirements</h2>\n<p><a href=\"/food-guidance-by-commodity/fresh-fruits-or-vegetables/regulatory-requirements/eng/1527197515042/1527197515323\">Regulatory requirements: Fresh fruits or vegetables</a><br>\n</p>\n\n<p><a href=\"/eng/1572624590741/1572624591273\">Video presentation for the fresh fruit or vegetable sector</a><br>\n</p>\n\n<p><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-2/eng/1519996239002/1519996303947\">Canadian Grade Compendium Volume 2, Fresh Fruit or Vegetables</a><br>\n</p>\n\n<p><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-9/eng/1520647701525/1520647702274?chap=1#s1c1\">Grade Names for Imported Food</a><br>\n</p>\n\n<p><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/fresh-fruits-or-vegetables-imported-from-the-u-s-/eng/1520007339733/1520007340669\">Grade Standard Requirements for Fresh Fruits or Vegetables Imported from the United States</a><br>\n</p>\n\n<p><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/units-of-measurement-for-the-net-quantity-declarat/eng/1521819171564/1521819242968\">Units of Measurement for the Net Quantity Declaration of Certain Foods</a><br>\n</p>\n\n<p><a href=\"/exporting-food-plants-or-animals/food-exports/food-specific-export-requirements/eng/1503941030653/1503941059640\">Food-specific export requirements\u00a0\u2013 Fresh fruit and vegetables</a><br>\n</p>\n\n<p><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/personal-use-exemption/eng/1520439688578/1520439689098\">Maximum Quantity Limits for Personal Use Exemption</a><br>\n</p>\n\n<h2>Preventive control recommendations</h2>\n\n<p><a href=\"/preventive-controls/fresh-fruits-or-vegetables/eng/1526650999897/1526651000163\">Preventive controls for food\u00a0\u2013 Fresh fruits or vegetables</a><br>\n</p>\n\n<h2>Related information</h2>\n\n<p><a href=\"/food-licences/food-business-activities/eng/1524074697160/1524074697425\">Food business activities that require a license under the SFCR</a><br>\n</p>\n\n<p><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/fresh-fruits-and-vegetables-businesses/eng/1547477730165/1547477730492\">Fact sheet: fresh fruits and vegetables businesses</a><br>\n</p>\n\n\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1526992762174\"></div>\n\n\n<section class=\"alert alert-info\">\n<h2>Information</h2>\n<p class=\"mrgn-tp-0\">Avant de passer en revue les renseignements portant pr\u00e9cis\u00e9ment sur les fruits ou les l\u00e9gumes frais, veuillez lire les <a href=\"/fra/1512149634601/1512149659119\">Exigences et documents d'orientation g\u00e9n\u00e9rales sur les aliments</a>. Le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC) \u00e9nonce de nombreuses exigences qui s'appliquent \u00e0 tous les produits alimentaires, de m\u00eame que les exigences propres aux fruits ou l\u00e9gumes frais soulign\u00e9es dans cette section.</p>\n\n<p>D'autres exigences concernant les <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/fra/1526656151226/1526656151476\">importations d'aliments</a> et les <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/fra/1323723342834/1323723662195\">exportations d'aliments</a> pourraient \u00e9galement s'appliquer \u00e0 votre entreprise.</p>\n</section>\n\n<h2>Conna\u00eetre les exigences</h2>\n<p><a href=\"/exigences-et-documents-d-orientation-relatives-a-c/fruits-ou-legumes-frais/exigences-reglementaires/fra/1527197515042/1527197515323\">Exigences r\u00e9glementaires\u00a0: Fruits ou l\u00e9gumes frais</a><br>\n</p>\n\n<p><a href=\"/fra/1572624590741/1572624591273\">Pr\u00e9sentation vid\u00e9o pour le secteur des fruits ou l\u00e9gumes frais</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1519996239002/1519996303947\">Recueil des normes canadiennes de classification, Volume 2, Fruits ou l\u00e9gumes frais</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1520647701525/1520647702274?chap=1#s1c1\">Noms de cat\u00e9gories applicables aux aliments import\u00e9s</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/fruits-ou-les-legumes-frais-importes-des-etats-uni/fra/1520007339733/1520007340669\">Exigences de normes de cat\u00e9gories pour les fruits ou les l\u00e9gumes frais import\u00e9s des \u00c9tats-Unis</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/les-unites-de-mesure-pour-la-declaration-de-quanti/fra/1521819171564/1521819242968\">Les unit\u00e9s de mesure pour la d\u00e9claration de quantit\u00e9 nette de certains aliments</a><br>\n</p>\n\n<p><a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences-d-exportation-particulieres-aux-produits/fra/1503941030653/1503941059640\">Exigences d'exportation particuli\u00e8res \u00e0 un produit alimentaire\u00a0\u2013 Fruits et l\u00e9gumes frais</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/exemption-pour-usage-personnel/fra/1520439688578/1520439689098\">Les quantit\u00e9s maximales pour l'exemption pour usage personnel</a><br>\n</p>\n\n<h2>Contr\u00f4les pr\u00e9ventifs recommand\u00e9s</h2>\n\n<p><a href=\"/controles-preventifs/fruits-ou-legumes-frais/fra/1526650999897/1526651000163\">Contr\u00f4les pr\u00e9ventifs recommand\u00e9s pour les aliments\u00a0\u2013 Fruits ou l\u00e9gumes frais</a><br>\n</p>\n\n<h2>Renseignements connexes</h2>\n\n<p><a href=\"/licences-pour-aliments/activites-du-secteur-alimentaire/fra/1524074697160/1524074697425\">Activit\u00e9s du secteur alimentaire qui n\u00e9cessitent une licence aux termes du RSAC</a><br>\n</p>\n\n<p><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/entreprises-de-fruits-et-legumes-frais/fra/1547477730165/1547477730492\">Fiche d'information\u00a0: entreprises de fruits et l\u00e9gumes frais</a><br>\n</p>\n\n\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "chat_wizard": true,
    "success": true
}