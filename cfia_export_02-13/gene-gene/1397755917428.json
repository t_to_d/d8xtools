{
    "dcr_id": "1397755917428",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Pueraria montana</i> (Kudzu)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Pueraria montana</i> (Kudzu)"
    },
    "html_modified": "2024-02-13 9:49:50 AM",
    "modified": "2017-09-29",
    "issued": "2014-06-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_Pueraria_montana_1397755917428_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_Pueraria_montana_1397755917428_fra"
    },
    "ia_id": "1397755957679",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Pueraria montana</i> (Kudzu)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Pueraria montana</i> (Kudzu)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Pueraria montana</i> (Kudzu)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Pueraria montana</i> (Kudzu)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1397755957679",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/pueraria-montana/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/pueraria-montana/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Pueraria montana (Kudzu)",
            "fr": "Semence de mauvaises herbe : Pueraria montana (Kudzu)"
        },
        "description": {
            "en": "Fact sheet for Pueraria montana.",
            "fr": "Fiche de renseignements pour Pueraria montana."
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, import, US, grader, Pueraria montana, Kudzu",
            "fr": "fiche de renseignements, envahissantes, semence envahissantes, semence, importation, \u00c9tats-Unis, calibreuse, Pueraria montana, Kudzu"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants",
            "fr": "cultures,grain,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-06-12",
            "fr": "2014-06-12"
        },
        "modified": {
            "en": "2017-09-29",
            "fr": "2017-09-29"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Pueraria montana (Kudzu)",
        "fr": "Semence de mauvaises herbe : Pueraria montana (Kudzu)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/kudzu/eng/1331750489827/1331750551292\">Invasive Plant - Kudzu (<i lang=\"la\">Pueraria montana</i>)</a></p>\n\n<h2>Family</h2>\n<p><i lang=\"la\">Fabaceae</i></p>\n\n<h2>Common name</h2>\n<p>Kudzu</p>\n\n<h2>Regulation</h2>\n<p>Prohibited Noxious, Class 1 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>. All imported and domestic seed must be free of Prohibited Noxious weed seeds.</p>\n\n<p>Listed on the <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">List of Pests Regulated by Canada</a> established under the <a href=\"/english/reg/jredirect2.shtml?plavega\"><i>Plant Protection Act</i></a>.</p>\n\n<h2>Distribution</h2>\n\n<p><strong>Canadian:</strong> One known population in southern <abbr title=\"Ontario\">ON</abbr> under official control.</p>\n\n<p><strong>Worldwide:</strong> Native to eastern and southeastern Asia and introduced to southern Australia, South America, South Africa, New Zealand, central and eastern Europe and the United States (Lindgren et al. 2013<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). There are severe infestations in the southeastern United States (<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> 2014<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 3.1 - 5.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 2.5 - 3.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed thickness: 2.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Oval or D-shaped seed; compressed</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Surface texture</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed smooth with a satiny sheen</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Colour</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Seed usually reddish-brown or brown with black mottles; some seeds are a uniform colour</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Other features</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Pale ring of tissue around hilum, located in the middle of narrow edge</li>\n</ul>\n\n<h2>Habitat and crop association</h2>\n\n<p>Abandoned fields and field edges, grasslands, pastures, natural forests, plantations, fencerows, roadsides, riverbanks and urban areas (<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> 2012<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, 2014<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>General information</h2>\n\n<p>Kudzu was planted as a soil stabilizer in the 1930s and 40s in the southeastern United States, where it persists today (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). In China and Japan, it is cultivated for forage, starch, food and medicine (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<p>In recent years it has also been considered for biofuel use (Sage et al. 2009<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). In its native range, kudzu grows best in areas with mild winters, hot summers and at least 100 <abbr title=\"centimetres\">cm</abbr> of annual precipitation (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). It is known for its rapid vegetative growth and for quickly overwhelming native species (Forseth Jr. and Innis 2004<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n<h2>Similar species</h2>\n<h3>Sainfoin (<i lang=\"la\">Onobrychis viciifolia</i>)</h3>\n<ul>\n<li>Similar size, D- shape, reddish-brown colour and raised tissue around hilum.</li>\n\n<li>Sainfoin seeds do not have mottles and the raised hilum tissue is thin.</li>\n\n<li>The single-seeded legume is often shed with the seed inside, while kudzu's legume is many-seeded and splits at maturity.</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img5_1504023320277_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) seeds\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img2_1397671767371_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) seeds</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img1_1397671695840_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) seed\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img3_1397671840669_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) seed, hilum view\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img4_1397671873310_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Sainfoin (<i lang=\"la\">Onobrychis viciifolia</i>) legume and seeds\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img6_1504021721318_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Sainfoin (<i lang=\"la\">Onobrychis viciifolia</i>) seed\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Lindgren, C. J., Castro, K. L., Coiner, H. A., Nurse, R. E. and Darbyshire, S. J. 2013.</strong> The biology of invasive alien plants in Canada. 12. Pueraria montana var. <i lang=\"la\">lobata</i> (Willd.) Sanjappa &amp; Predeep. Canadian Journal of Plant Science 93 (1): 71-95.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2014.</strong> Invasive Plants - Fact Sheets. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>., http://www.inspection.gc.ca/plants/plant-pests-invasive-species/invasive-plants/fact-sheets/eng/1331614724083/1331614823132 [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2012</strong>. Invasive Plant Field Guide. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016.</strong> Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Sage, R. F., Coiner, H. A., Way, D. A., Runion, G. B., Prior, S. A., Torbert, H. A., Sicher, R. and Ziska, L. 2009.</strong> Kudzu (<i lang=\"la\">Pueraria montana</i> (Lour.) Merr. var. <i lang=\"la\">lobata</i>): A new source of carbohydrate for bioethanol production. Biomass and Bioenergy 33: 57-61.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Forseth Jr., I. N. and Innis, A. F. 2004.</strong> Kudzu (<i lang=\"la\">Pueraria montana</i>): History, physiology, and ecology combine to make a major ecosystem threat. Critical Reviews in Plant Sciences 23: 401-413.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/kudzu/fra/1331750489827/1331750551292\">Plante envahissante - Kudzu (<i lang=\"la\">Pueraria montana</i>)</a></p>\n\n<h2>Famille</h2>\n<p><i lang=\"la\">Fabaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Kudzu</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible interdite, cat\u00e9gorie 1 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>. Toutes les semences canadiennes et import\u00e9es doivent \u00eatre exemptes de mauvaises herbes nuisibles interdites.</p>\n<p>Figure sur la <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Liste des parasites r\u00e9glement\u00e9s par le Canada</a>, \u00e9tablie en vertu de la <a href=\"/francais/reg/jredirect2.shtml?plavega\"><i>Loi sur la protection des v\u00e9g\u00e9taux</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> Une seule population est rapport\u00e9e dans le Sud de l'<abbr title=\"Ontario\">Ont.</abbr> et elle fait l'objet d'un contr\u00f4le officiel.</p>\n\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Indig\u00e8ne de l'Asie orientale et de l'Asie du Sud-Est et introduite dans le Sud de l'Australie, en Am\u00e9rique du Sud, en Afrique du Sud, en Nouvelle-Z\u00e9lande, en Europe centrale et orientale et aux \u00c9tats-Unis (Lindgren et al., 2013<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>). Il y a de graves infestations dans le Sud-Est des \u00c9tats-Unis (<abbr lang=\"en\" title=\"Canadian Food Inspection Agency\">CFIA</abbr>, 2014<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 3,1 \u00e0 5,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 2,5 \u00e0 3,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>\u00c9paisseur de la graine\u00a0: 2,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine de forme elliptique ou en D; comprim\u00e9e</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine lisse avec un lustre satin\u00e9</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Les graines sont g\u00e9n\u00e9ralement d'une couleur brun rouge\u00e2tre ou brune avec des marbrures noires; certaines sont d'une couleur uniforme</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Le hile est entour\u00e9 d'un p\u00e2le anneau tissulaire et il est situ\u00e9 au milieu du bord mince de la graine</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n\n<p>Champs abandonn\u00e9s, bords de champs, prairies, p\u00e2turages, for\u00eats naturelles, plantations, haies, bords de chemin, berges de rivi\u00e8res et zones urbaines (<abbr lang=\"en\" title=\"Canadian Food Inspection Agency\">CFIA</abbr>, 2012<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>; <abbr lang=\"en\" title=\"Canadian Food Inspection Agency\">CFIA</abbr>, 2014<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n\n<p>Dans les ann\u00e9es 1930 et 1940, le kudzu \u00e9tait plant\u00e9 dans le Sud-Est des \u00c9tats-Unis pour stabiliser le sol, et on le trouve encore dans ces r\u00e9gions aujourd'hui (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>). En Chine et au Japon, il est cultiv\u00e9 comme plante fourrag\u00e8re, alimentaire, m\u00e9dicinale et amidonni\u00e8re (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>).</p>\n\n<p>Ces derni\u00e8res ann\u00e9es, le kudzu a fait l'objet d'\u00e9tudes relativement \u00e0 son potentiel pour la production de biocarburants (Sage et al., 2009<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>). Dans son aire d'indig\u00e9nat, le kudzu pr\u00e9f\u00e8re les r\u00e9gions aux hivers doux et aux \u00e9t\u00e9s tr\u00e8s chauds qui re\u00e7oivent au moins 100 <abbr title=\"centim\u00e8tres\">cm</abbr> de pr\u00e9cipitations annuelles (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>). La plante est connue pour sa forte croissance v\u00e9g\u00e9tative et sa capacit\u00e9 \u00e0 supplanter rapidement les esp\u00e8ces indig\u00e8nes (Forseth Jr et Innis, 2004<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Sainfoin (<i lang=\"la\">Onobrychis viciifolia</i>)</h3> \n<ul>\n<li>Les graines du sainfoin ressemblent \u00e0 celles du kudzu par leurs dimensions, leur forme en D, leur couleur brun rouge\u00e2tre et les tissus saillants autour de leur hile.</li>\n\n<li>Les graines du sainfoin ne sont pas marbr\u00e9es et les tissus saillants autour de leur hile sont minces.</li>\n\n<li>La gousse unis\u00e9min\u00e9e du sainfoin se d\u00e9tache souvent avec la semence \u00e0 l'int\u00e9rieur, alors que celle du kudzu est multis\u00e9min\u00e9e et s'ouvre \u00e0 maturit\u00e9.</li>\n</ul>\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img5_1504023320277_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) graines\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img2_1397671767371_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) graines</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img1_1397671695840_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) graine\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img3_1397671840669_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Kudzu (<i lang=\"la\">Pueraria montana</i>) graine, vue hile\n</figcaption>\n</figure>\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img4_1397671873310_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Sainfoin (<i lang=\"la\">Onobrychis viciifolia</i>) legume et graines\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_Pueraria_montana_img6_1504021721318_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Sainfoin (<i lang=\"la\">Onobrychis viciifolia</i>) graine\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Lindgren, C. J., Castro, K. L., Coiner, H. A., Nurse, R. E. and Darbyshire, S. J. 2013.</strong> The biology of invasive alien plants in Canada. 12. Pueraria montana var. <i lang=\"la\">lobata</i> (Willd.) Sanjappa &amp; Predeep. Canadian Journal of Plant Science 93 (1): 71-95.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2014.</strong> Invasive Plants - Fact Sheets. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>., http://www.inspection.gc.ca/plants/plant-pests-invasive-species/invasive-plants/fact-sheets/eng/1331614724083/1331614823132 [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2012</strong>. Invasive Plant Field Guide. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016.</strong> Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Sage, R. F., Coiner, H. A., Way, D. A., Runion, G. B., Prior, S. A., Torbert, H. A., Sicher, R. and Ziska, L. 2009.</strong> Kudzu (<i lang=\"la\">Pueraria montana</i> (Lour.) Merr. var. <i lang=\"la\">lobata</i>): A new source of carbohydrate for bioethanol production. Biomass and Bioenergy 33: 57-61.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Forseth Jr., I. N. and Innis, A. F. 2004.</strong> Kudzu (<i lang=\"la\">Pueraria montana</i>): History, physiology, and ecology combine to make a major ecosystem threat. Critical Reviews in Plant Sciences 23: 401-413.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n</dl>\n</aside>\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}