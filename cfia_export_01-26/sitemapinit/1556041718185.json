{
    "dcr_id": "1556041717935",
    "title": {
        "en": "Producers: Protect pigs from African swine fever",
        "fr": "Producteurs\u00a0: Prot\u00e9ger les porcs contre la peste porcine africaine"
    },
    "html_modified": "2024-01-26 2:24:39 PM",
    "modified": "2022-08-04",
    "issued": "2019-04-25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/producer_protection_asf_1556041717935_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/producer_protection_asf_1556041717935_fra"
    },
    "ia_id": "1556041718185",
    "parent_ia_id": "1306983373952",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1306983373952",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Producers: Protect pigs from African swine fever",
        "fr": "Producteurs\u00a0: Prot\u00e9ger les porcs contre la peste porcine africaine"
    },
    "label": {
        "en": "Producers: Protect pigs from African swine fever",
        "fr": "Producteurs\u00a0: Prot\u00e9ger les porcs contre la peste porcine africaine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1556041718185",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306983245302",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/producers/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/producteurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Producers: Protect pigs from African swine fever",
            "fr": "Producteurs\u00a0: Prot\u00e9ger les porcs contre la peste porcine africaine"
        },
        "description": {
            "en": "Whether you're a small-scale or large-scale producer, on-farm biosecurity is critical to preventing foreign animal diseases like African swine fever (ASF) from developing and spreading.",
            "fr": "Que vous soyez un producteur d'un petit ou de grand \u00e9levage, la bios\u00e9curit\u00e9 \u00e0 la ferme est essentielle pour la pr\u00e9vention de la contraction et de la propagation de maladies animales exotiques comme la peste porcine africaine (PPA)."
        },
        "keywords": {
            "en": "Reportable Diseases Regulations, African swine fever, African swine, contagious viral disease of swine, reportable disease, Government of Canada, producer, on-farm biosecurity, swine feed",
            "fr": "R\u00e9glementation relative aux maladies \u00e0 d\u00e9claration obligatoire, Peste porcine africaine, Porc africain, maladie virale contagieuse du porc, maladie \u00e0 d\u00e9claration obligatoire, gouvernement du Canada, producteur, bios\u00e9curit\u00e9 \u00e0 la ferme, aliments du porc"
        },
        "dcterms.subject": {
            "en": "animal,inspection,animal inspection,animal diseases,animal health",
            "fr": "animal,inspection,inspection des animaux,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-04-25",
            "fr": "2019-04-25"
        },
        "modified": {
            "en": "2022-08-04",
            "fr": "2022-08-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Producers: Protect pigs from African swine fever",
        "fr": "Producteurs\u00a0: Prot\u00e9ger les porcs contre la peste porcine africaine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Whether you're a small-scale or large-scale producer, on-farm biosecurity is critical to preventing foreign animal diseases like African swine fever (ASF) from developing and spreading. Careful sourcing of animals, products and by-products, including feed and feed ingredients, is important.</p>\n\n<p>People visiting your farm could also be carrying ASF on their contaminated clothing, equipment or in their food if they were recently in an area that was infected with ASF.</p>\n\n<p>Producers should make sure that visitors or workers on their farms follow strict biosecurity measures before coming back onto their premises. Review the <a href=\"/animal-health/terrestrial-animals/biosecurity/eng/1299868055616/1320534707863\">national biosecurity standards</a> and <a href=\"/animal-health/terrestrial-animals/biosecurity/tools/checklist/eng/1362944949857/1362945111651\">assess your biosecurity practices</a> to mitigate the risk of ASF entering your farm.</p>\n\n<p>Refer to our <a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/fact-sheet/eng/1306993248674/1306993787261\">frequently asked questions about ASF</a> to inform yourself about this animal disease.</p>\n\n<h2>Awareness toolkit</h2>\n\n<p>Everyone has a role to play in reducing the risk of animal diseases. Do your part by sharing this toolkit with your network. You'll find helpful videos, fact sheets, posters and images to help spread the word, not the animal disease.</p>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/close-your-gate-on-african-swine-fever/spot-the-signs-of-african-swine-fever-asf-/eng/1611086348221/1611086348456\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/asf_biosecurity_know_the_signs_360x203_1611323297872_eng.jpg\" class=\"img-responsive thumbnail\" alt=\"Spot the signs of African swine fever (ASF)\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/close-your-gate-on-african-swine-fever/spot-the-signs-of-african-swine-fever-asf-/eng/1611086348221/1611086348456\">Infographic: Spot the signs of African swine fever (ASF)</a></strong></p>\n<p>No matter the size of your farm or how many pigs you have, African swine fever (ASF) could affect you.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/on-farm-biosecurity/eng/1547479486729/1547479538543\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/on-farm_bio_asf_1556041285195_eng.png\" class=\"img-responsive thumbnail\" alt=\"On-farm biosecurity\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/on-farm-biosecurity/eng/1547479486729/1547479538543\">Infographic: On-farm biosecurity</a></strong></p>\n<p>ASF can have a devastating effect on a swine herd. On-farm biosecurity is key to prevent diseases from developing and spreading.</p>\n</div>\n</div>\n\n<h2>Awareness tools from third parties</h2>\n\n<ul>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">World Organisation for Animal Health awareness tools</a></li>\n<li><a href=\"https://www.youtube.com/watch?time_continue=7&amp;v=eyQ4t1wHl2M\">Video: African swine fever\u00a0\u2013\u00a0How to stay one step ahead</a></li>\n<li><a href=\"http://www.cfsph.iastate.edu/DiseaseInfo/disease-images.php?name=african-swine-fever&amp;lang=in\">Disease images of African swine fever</a></li>\n<li><a href=\"https://oahn.ca/wp-content/uploads/2018/11/ASF-AND-FEED.pdf\">What's hitching a ride in your feed?\u00a0\u2013\u00a0PDF (444 kb)</a></li>\n<li><a href=\"https://trello-attachments.s3.amazonaws.com/5c501da3a4ba4c2c50ce308d/5c5024e01dfe360dabc406a7/d7499579a4a73e1022c1b5001292d4c6/EN_Poster_ASF_CommercialFarms.pdf\">Commercial pig farmers: Don't be the carrier of a deadly pig disease\u00a0\u2013\u00a0PDF (213 kb)</a></li>\n<li><a href=\"https://www.ahwcouncil.ca/african-swine-fever-surveillance-information\">CanSpotASF</a></li>\n</ul>\n\n<h2>Resources for producers</h2>\n\n<h3 class=\"h5\">National</h3>\n\n<ul>\n<li><a href=\"https://www.cpc-ccp.com/biosecurity\">National Swine Farm-Level Biosecurity Standard</a></li>\n<li><a href=\"https://www.anacan.org/feed-industry/public-resources/national-biosecurity-guide-for-the-livestock-and-poultry-feed-sector/\">National Biosecurity Guide for the Livestock and Poultry Feed Sector</a></li>\n<li><a href=\"https://www.cpc-ccp.com/default\">Canadian Pork Council</a></li>\n<li><a href=\"https://www.anacan.org/anac/index.html\">Animal Nutrition Association of Canada</a></li>\n</ul>\n\n<h3 class=\"h5\">Nova Scotia</h3>\n\n<ul>\n<li><a href=\"http://porknovascotia.ca/wp-content/uploads/NS-Emergency-Management-Producer-Handbook.pdf\">Preparing the Nova Scotia Livestock Sector for Disease-Related Sector-Wide Emergencies\u00a0\u2013\u00a0Producer Handbook\u00a0\u2013\u00a0PDF (4,377 kb)</a></li>\n</ul>\n\n<h3 class=\"h5\">Quebec</h3>\n\n<ul>\n<li><a href=\"http://www.leseleveursdeporcsduquebec.com/actualites/?cat=10\"><span lang=\"fr\">Les \u00c9leveurs de porcs du Qu\u00e9bec\u00a0: Actualit\u00e9s\u00a0\u2013\u00a0Maladies porcines</span> (in French only)</a></li>\n</ul>\n\n<h3 class=\"h5\">Ontario</h3>\n\n<ul>\n<li><a href=\"https://oahn.ca/reports/swine-producer-report-q3-2018/\">Ontario Animal Health Network's Swine Producer Report\u00a0\u2013\u00a0Q3 2018</a></li>\n<li><a href=\"http://www.swinehealthontario.ca/Portals/15/documents/resources/ASF/SHO%20OAHN%20ASF%20Fact%20Sheet.pdf?ver=2018-10-01-165101-033\">Ontario Animal Health Network and Swine Health Ontario's African Swine Fever Update\u00a0\u2013\u00a0PDF (185 kb)</a></li>\n</ul>\n\n<h3 class=\"h5\">Manitoba</h3>\n\n<ul>\n<li><a href=\"https://www.manitobapork.com/swine-health/foreign-animal-disease-fad-preparedness\">Manitoba Pork's Foreign Animal Disease Preparedness</a></li>\n<li><a href=\"https://www.manitobapork.com/images/producers/pdfs/animalhealth/AHEM-MB-Pork-Producer-Handbook-2018.pdf\">Producer Handbook\u00a0\u2013\u00a0Preparing the Manitoba Pork Sector for Disease-Related Sector-Wide Emergencies\u00a0\u2013\u00a0PDF (3,823 kb)</a></li>\n</ul>\n\n<h3 class=\"h5\">Saskatchewan</h3>\n\n<ul>\n<li><a href=\"https://www.saskpork.com/swine-health/?rq=african%20swine%20fever\">Saskatchewan Pork's Swine Health</a></li>\n</ul>\n\n<h3 class=\"h5\">Alberta</h3>\n\n<ul>\n<li><a href=\"https://www.albertapork.com/2018/09/20/town-hall-asf/\">Alberta Pork's keeping ASF out of Canada update</a></li>\n<li><a href=\"https://www.albertapork.com/our-producer-services/biosecurity/preparing-disease-related-emergencies/\">Preparing the Alberta Pork Sector for Disease-Related Sector-Wide Emergencies\u00a0\u2013\u00a0Producer Handbook</a></li>\n</ul>\n\n<h3 class=\"h5\">British Columbia</h3>\n\n<ul>\n<li><a href=\"https://www2.gov.bc.ca/assets/gov/farming-natural-resources-and-industry/agriculture-and-seafood/farm-management/emergency-management/bc_pork_emergency_management_guide.pdf\">Emergency Management Guide for BC Pork Producers\u00a0\u2013\u00a0PDF (1,700 kb)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Que vous soyez un producteur d'un petit ou de grand \u00e9levage, la bios\u00e9curit\u00e9 \u00e0 la ferme est essentielle pour la pr\u00e9vention de la contraction et de la propagation de maladies animales exotiques comme la peste porcine africaine (PPA). La s\u00e9lection soigneuse des sources des animaux, des produits et des sous-produits, y compris des aliments et des ingr\u00e9dients pour les aliments du b\u00e9tail, est importante.</p>\n\n<p>La PPA peut \u00eatre introduite \u00e0 une ferme par des gens qui ont visit\u00e9 d'autres fermes dans une r\u00e9gion infect\u00e9e par la PPA. Cela comprend le transport d'aliments, de v\u00eatements ou d'\u00e9quipement infect\u00e9s sur les lieux des fermes.</p>\n\n<p>Les producteurs doivent veiller \u00e0 ce que les visiteurs et les travailleurs respectent des mesures rigoureuses en mati\u00e8re de bios\u00e9curit\u00e9 avant de ne pouvoir retourner sur les lieux. Passez en revue les <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/fra/1299868055616/1320534707863\">normes nationales de bios\u00e9curit\u00e9</a> et <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/outils/liste-de-verification/fra/1362944949857/1362945111651\">\u00e9valuez vos pratiques de bios\u00e9curit\u00e9</a> afin d'att\u00e9nuer les risques d'introduction de la PPA \u00e0 votre ferme.</p>\n\n<p>Veuillez consulter notre <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fiche-de-renseignements/fra/1306993248674/1306993787261\">foire aux questions au sujet de la PPA</a> pour obtenir des renseignements au sujet de cette maladie animale.</p>\n\n<h2>Trousse d'outils de sensibilisation</h2>\n\n<p>Nous avons tous un r\u00f4le \u00e0 jouer dans la r\u00e9duction des risques associ\u00e9s aux maladies animales. Faites votre part en partageant cette trousse d'outils dans votre r\u00e9seau. Vous y retrouverez des vid\u00e9os utiles, des fiches de renseignements, des affiches et des images pour vous aider \u00e0 transmettre des renseignements\u00a0\u2013\u00a0plut\u00f4t que des maladies animales.</p>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fermez-votre-barriere-a-la-peste-porcine-africaine/reperez-les-signes-de-la-peste-porcine-africaine-p/fra/1611086348221/1611086348456\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/asf_biosecurity_know_the_signs_360x203_1611323297872_fra.jpg\" class=\"img-responsive thumbnail\" alt=\"Rep\u00e9rez les signes de la peste porcine africaine (PPA)\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fermez-votre-barriere-a-la-peste-porcine-africaine/reperez-les-signes-de-la-peste-porcine-africaine-p/fra/1611086348221/1611086348456\">Infographie\u00a0: Rep\u00e9rez les signes de la peste porcine africaine (PPA)</a></strong></p>\n<p>Peu importe la taille de votre ferme ou le nombre de vos porcs, la peste porcine africaine (PPA) pourrait vous affecter.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/biosecurite-pour-les-fermes/fra/1547479486729/1547479538543\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/on-farm_bio_asf_1556041285195_fra.png\" class=\"img-responsive thumbnail\" alt=\"Bios\u00e9curit\u00e9 pour les fermes\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/biosecurite-pour-les-fermes/fra/1547479486729/1547479538543\">Infographie\u00a0: Bios\u00e9curit\u00e9 pour les fermes</a></strong></p>\n<p>La PPA peut avoir un effet d\u00e9vastateur sur les troupeaux de porcs. La bios\u00e9curit\u00e9 \u00e0 la ferme est essentielle pour aider \u00e0 pr\u00e9venir le d\u00e9veloppement et la propagation de maladies.</p>\n</div>\n</div>\n\n<h2>Autres outils de sensibilisation provenant de tiers</h2>\n\n<ul>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">Outils de sensibilisation de l'Organisation mondiale de la sant\u00e9 animale (OIE)</a></li>\n<li><a href=\"https://www.youtube.com/watch?time_continue=7&amp;v=eyQ4t1wHl2M\">Vid\u00e9o\u00a0: <span lang=\"en\">African swine fever\u00a0\u2013\u00a0How to stay one step ahead</span> (en anglais seulement)</a></li>\n<li><a href=\"http://www.cfsph.iastate.edu/DiseaseInfo/disease-images-fr.php?name=classical-swine-fever&amp;lang=fr\">Photos des signes cliniques de la peste porcine africaine</a></li>\n<li><a href=\"https://oahn.ca/wp-content/uploads/2018/11/ASF-AND-FEED.pdf\"><span lang=\"en\">What's hitching a ride in your feed</span>? (en anglais seulement)\u00a0\u2013\u00a0PDF (444 ko)</a></li>\n<li><a href=\"https://trello-attachments.s3.amazonaws.com/5c501da3a4ba4c2c50ce308d/5c5024e01dfe360dabc406a7/d7499579a4a73e1022c1b5001292d4c6/EN_Poster_ASF_CommercialFarms.pdf\">\u00c9leveurs de porcs commerciaux\u00a0: Ne soyez pas le transporteur d'une maladie mortelle pour les porcs</a> (en anglais seulement)\u00a0\u2013\u00a0PDF (213 ko)</li>\n<li><a href=\"https://www.ahwcouncil.ca/fr-african-swine-fever-surveillance-information\">CanaVeillePPA</a></li>\n</ul>\n\n<h2>Ressources pour les producteurs</h2>\n\n<h3 class=\"h5\">Nationales</h3>\n\n<ul>\n<li><a href=\"https://www.cpc-ccp.com/francais/biosecurity\">Norme nationale de bios\u00e9curit\u00e9 pour les fermes porcines</a></li>\n<li><a href=\"https://www.anacan.org/fr/industrie-de-lalimentation-animale/ressources-publiques/guide-national-de-biosecurite-pour-le-secteur-de-lalimentation-du-betail-et-de-la-volaille/\">Guide de bios\u00e9curit\u00e9 pour l'industrie canadienne de l'alimentation animale</a></li>\n<li><a href=\"https://www.cpc-ccp.com/francais/default\">Conseil canadien du porc</a></li>\n<li><a href=\"https://www.anacan.org/fr/anac/index.html\">Association de nutrition animale du Canada</a></li>\n</ul>\n\n<h3 class=\"h5\">Nouvelle-\u00c9cosse</h3>\n\n<ul>\n<li><a href=\"http://porknovascotia.ca/wp-content/uploads/NS-Emergency-Management-Producer-Handbook.pdf\"><span lang=\"en\">Preparing the Nova Scotia Livestock Sector for Disease-Related Sector-Wide Emergencies\u00a0\u2013\u00a0Producer Handbook</span> (en anglais seulement)\u00a0\u2013\u00a0PDF (4\u00a0377 ko)</a></li>\n</ul>\n\n<h3 class=\"h5\">Qu\u00e9bec</h3>\n\n<ul>\n<li><a href=\"http://www.leseleveursdeporcsduquebec.com/actualites/?cat=10\">Les \u00c9leveurs de porcs du Qu\u00e9bec\u00a0: Actualit\u00e9s\u00a0\u2013\u00a0Maladies porcines</a></li>\n</ul>\n\n<h3 class=\"h5\">Ontario</h3>\n\n<ul>\n<li><a href=\"https://oahn.ca/reports/swine-producer-report-q3-2018/\"><span lang=\"en\">Ontario Animal Health Network's Swine Producer Report</span>\u00a0\u2013\u00a0Q3\u00a02018 (en anglais seulement)</a></li>\n<li><a href=\"http://www.swinehealthontario.ca/Portals/15/documents/resources/ASF/SHO%20OAHN%20ASF%20Fact%20Sheet.pdf?ver=2018-10-01-165101-033\"><span lang=\"en\">Ontario Animal Health Network and Swine Health Ontario's African Swine Fever Update</span>\u00a0\u2013\u00a0(en anglais seulement) PDF (185\u00a0ko)</a></li>\n</ul>\n\n<h3 class=\"h5\">Manitoba</h3>\n\n<ul>\n<li><a href=\"https://www.manitobapork.com/swine-health/foreign-animal-disease-fad-preparedness\"><span lang=\"en\">Manitoba Pork's Foreign Animal Disease Preparedness</span> (en anglais seulement)</a></li>\n<li><a href=\"https://www.manitobapork.com/images/producers/pdfs/animalhealth/AHEM-MB-Pork-Producer-Handbook-2018.pdf\"><span lang=\"en\">Producer Handbook\u00a0\u2013\u00a0Preparing the Manitoba Pork Sector for Disease-Related Sector-Wide Emergencies</span>\u00a0\u2013\u00a0(en anglais seulement) PDF (3\u00a0823 kb)</a></li>\n</ul>\n\n<h3 class=\"h5\">Saskatchewan</h3>\n\n<ul>\n<li><a href=\"https://www.saskpork.com/swine-health/?rq=african%20swine%20fever\"><span lang=\"en\">Saskatchewan Pork's Swine Health</span> (en anglais seulement)</a></li>\n</ul>\n\n<h3 class=\"h5\">Alberta</h3>\n\n<ul>\n<li><a href=\"https://www.albertapork.com/2018/09/20/town-hall-asf/\"><span lang=\"en\">Alberta Pork's keeping ASF out of Canada update</span> (en anglais seulement)</a></li>\n<li><a href=\"https://www.albertapork.com/our-producer-services/biosecurity/preparing-disease-related-emergencies/\"><span lang=\"en\">Preparing the Alberta Pork Sector for Disease-Related Sector-Wide Emergencies\u00a0\u2013\u00a0Producer Handbook</span> (en anglais seulement)</a></li>\n</ul>\n\n<h3 class=\"h5\">Colombie-Britannique</h3>\n\n<ul>\n<li><a href=\"https://www2.gov.bc.ca/assets/gov/farming-natural-resources-and-industry/agriculture-and-seafood/farm-management/emergency-management/bc_pork_emergency_management_guide.pdf\"><span lang=\"en\">Emergency Management Guide for BC Pork Producers</span>\u00a0\u2013\u00a0(en anglais seulement) PDF (1\u00a0700\u00a0ko)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}