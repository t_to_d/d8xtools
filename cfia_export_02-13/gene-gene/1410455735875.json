{
    "dcr_id": "1410455735875",
    "title": {
        "en": "Service standard for a minor amendment",
        "fr": "Norme de service pour la modification mineure"
    },
    "html_modified": "2024-02-13 9:50:14 AM",
    "modified": "2019-06-03",
    "issued": "2015-04-30",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_reg_service_standards_fertilizer_minor_1410455735875_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_reg_service_standards_fertilizer_minor_1410455735875_fra"
    },
    "ia_id": "1410455736407",
    "parent_ia_id": "1396056725411",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1396056725411",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Service standard for a minor amendment",
        "fr": "Norme de service pour la modification mineure"
    },
    "label": {
        "en": "Service standard for a minor amendment",
        "fr": "Norme de service pour la modification mineure"
    },
    "templatetype": "content page 1 column",
    "node_id": "1410455736407",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1396056700668",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/acts-and-regulations/service-standards/fertilizer-and-supplement-registration/minor-amendment/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/normes-de-service/engrais-ou-de-supplement-enregistrement/modification-mineure/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Service standard for a minor amendment",
            "fr": "Norme de service pour la modification mineure"
        },
        "description": {
            "en": "A Minor Amendment will be completed in 135 days.",
            "fr": "Modification mineur sera termin\u00e9e en 135 jours."
        },
        "keywords": {
            "en": "acts, regulations, administration, enforcement, service standards, Fertilizer and Supplement Registration, Fertilizers Act and Regulations, Minor Amendment",
            "fr": "lois, r\u00e8glementaires, assurer, contr\u00f4ler l'application, normes de service, engrais ou de suppl\u00e9ment enregistrement, Loi sur les engrais et du R\u00e8glement, nouvel enregistrement, Modification mineur"
        },
        "dcterms.subject": {
            "en": "fertilizers,legislation,standards,regulation,food safety",
            "fr": "engrais,l\u00e9gislation,norme,r\u00e9glementation,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-30",
            "fr": "2015-04-30"
        },
        "modified": {
            "en": "2019-06-03",
            "fr": "2019-06-03"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Service standard for a minor amendment",
        "fr": "Norme de service pour la modification mineure"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>A minor amendment will be completed in 135 days.</p>\n\n<h2>Performance target</h2>\n<p>The target for achieving this standard is set at 90%.</p>\n\n<h2>Performance results</h2>\n<p>Annual performance:</p>\n<ul>\n<li>2018-19:  95% of 39 submissions processed within the service standard.</li>\n</ul>\n\n<h2>Applying for a minor amendment</h2>\n\n<p>Fertilizer and supplement products imported into and/or sold in Canada are regulated under the authority of the federal <i>Fertilizers Act</i> and <i>Regulations</i> administered by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. Pursuant to the Act and Regulations all products must be: safe with respect to plant, animal, human health, and the environment, and properly labelled as to avoid misrepresentation in the marketplace. Compliance with these standards is verified by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> as part of an assessment and registration process, or through the administration of marketplace monitoring programs that include product inspection, sample analysis, and label verification.</p>\n<p>The process for submitting an application consists of pre-screening and up to 3 review streams.</p>\n<ol>\n<li><strong>Pre-screening</strong>\n<p>All submissions are screened to determine whether the required information has been submitted. The pre-screening process is divided into 2 phases: 1) the completeness check and 2) the first response.</p></li>\n<li><strong>Review streams</strong>\n<p>During the review stage, evaluators review the submission's information against the requirements of the Fertilizers Act and Regulations and associated policies. All administrative requirements are also dealt with during the review. Depending on the submission type and the complexity of the submission, the file may be required to go through more than 1 type of assessment.</p></li>\n</ol>\n<p>Instructions on submitting information for registration and contacts can be found in <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-122/eng/1305609994431/1307910971122\">T-4-122 - Service Delivery Standards for Fertilizer and Supplement Registration - Related Submissions under the <i>Fertilizers Act</i> and <i>Regulations</i></a></p>\n\n<h2>Service feedback</h2>\n<p>If you have a service complaint, complete a <a href=\"/eng/1299860523723/1299860643049#form-formulaire\">Feedback Form</a> or contact:</p>\n<p>Fertilizer Safety Section<br>\n<abbr title=\"care of\">c/o</abbr> Pre-market Application Submissions Office (PASO)<br>\n<br>\n<br>\n<abbr title=\"Ontario\">ON</abbr> <span class=\"nowrap\">K1A 0Y9</span><br>\n<br>\n<br>\n</p>\n\n<p>or, alternatively:</p>\n\n<p>The <a href=\"/eng/1333027171445/1333039662703\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Complaints and Appeals Office</a></p>\r\n<h2>For more information</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/federal-regulatory-management.html\">Federal regulatory management and modernization</a></li>\n<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/federal-regulatory-management/government-wide-forward-regulatory-plans.html\">Government-Wide Forward Regulatory Plans</a></li>\n<li><a href=\"/about-the-cfia/cfia-2025/rcc/eng/1427420901391/1427420973247\">The Canada-United States Regulatory Cooperation Council</a></li>\n</ul>\n<p>To learn about upcoming or ongoing consultations on proposed federal regulations, visit the <a href=\"http://gazette.gc.ca/accueil-home-eng.html\"><i>Canada Gazette</i></a> and <a href=\"https://www.canada.ca/en/government/system/consultations/consultingcanadians.html\">Consulting with Canadians</a> websites.</p>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Modification mineur sera termin\u00e9e en 135 jours.</p>\n\n<h2>Cible de rendement</h2>\n<p>L'objectif pour la r\u00e9alisation de cette norme est fix\u00e9 \u00e0 <span class=\"nowrap\">90 %</span>.</p>\n\n<h2>Information sur le rendement</h2>\n<p>Rendement annuel\u00a0:</p>\n<ul>\n<li>2018-2019\u00a0: l'Agence a trait\u00e9 l'int\u00e9gralit\u00e9 (95<i></i>%) des 39 demandes dans le cadre de la norme de service.</li>\n</ul>\n\n<h2>Demande de modification mineure</h2>\n\n<p>Les engrais et suppl\u00e9ments import\u00e9s et vendus au Canada sont r\u00e9gis en vertu de la <i>Loi sur les engrais</i> et le <i>R\u00e8glement</i> connexe, dont l'application est assur\u00e9e par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Conform\u00e9ment \u00e0 la Loi et au R\u00e8glement, tous les produits doivent \u00eatre\u00a0: s\u00e9curitaires pour les plantes, les animaux, les humains et l'environnement; et bien \u00e9tiquet\u00e9s afin d'\u00e9viter de pr\u00e9senter, sur le march\u00e9, des informations trompeuses. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> v\u00e9rifie que ces normes sont respect\u00e9es dans le cadre de l'\u00e9valuation avant la mise en march\u00e9 et le processus d'enregistrement ou par diff\u00e9rents programmes de surveillance du march\u00e9, y compris l'inspection des produits, l'analyse des \u00e9chantillons et la v\u00e9rification des \u00e9tiquettes.</p>\n<p>Le processus de pr\u00e9sentation d'une demande se compose de flux pr\u00e9-examen et d'un maximum de 3 \u00e9valuations.</p>\n<ol>\n<li><strong>Examen initial</strong>\n<p>Toutes les soumissions sont examin\u00e9es afin de d\u00e9terminer si l'information requise \u00e0 \u00e9t\u00e9 envoy\u00e9e. L'\u00e9tape de l'examen initial de la soumission est divis\u00e9e en 2 \u00e9tapes\u00a0: 1) V\u00e9rification de la compl\u00e9tude de la soumission et 2) La premi\u00e8re r\u00e9ponse.</p></li>\n<li><strong>Volets d'\u00e9valuation</strong>\n<p>Au cours de l'examen, les \u00e9valuateurs v\u00e9rifient les renseignements de la demande en fonction des exigences de la <i>Loi sur les engrais</i> et du <i>R\u00e8glement</i> connexe et les politiques associ\u00e9es. Toutes les exigences administratives sont \u00e9galement trait\u00e9es au cours de cette \u00e9tape. Selon le type de demande et la complexit\u00e9 de celle-ci, il se peut que le dossier doive faire l'objet de plus 1 type d'\u00e9valuation.</p></li>\n</ol>\n<p>Renseignements sur la soumission de l'information pour l'enregistrement et les contacts peuvent \u00eatre trouv\u00e9s au <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-122/fra/1305609994431/1307910971122\">T-4-122 - Normes de prestation de services, s'appliquant aux demandes d'enregistrement des engrais et suppl\u00e9ments r\u00e9gies par la <i>Loi</i> et le <i>R\u00e8glement sur les engrais</i></a>.</p>\n\n<h2>M\u00e9canisme de r\u00e9troaction</h2>\n<p>Si vous avez une plainte de service remplir le <a href=\"/fra/1299860523723/1299860643049#form-formulaire\">formulaire de r\u00e9troaction</a> ou communiquez avec\u00a0:</p>\n\n<p>Section de l'innocuit\u00e9 des engrais<br>\n<abbr title=\"au soin\">a/s</abbr> du Bureau de pr\u00e9sentation des demandes pr\u00e9alables \u00e0 la mise en march\u00e9 (BPDPM)<br>\n<br>\n<br>\n<abbr title=\"Ontario\">ON</abbr> <span class=\"nowrap\">K1A 0Y9</span><br>\n<br>\n<br>\n</p>\n\n<p>ou, alternativement\u00a0: </p>\n\n<p>Le <a href=\"/fra/1333027171445/1333039662703\">Bureau des plaintes et des appels de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a></p>\r\n<h2>Pour de plus amples renseignements</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/gestion-reglementation-federale.html\">Gestion et modernisation de la r\u00e9glementation f\u00e9d\u00e9rale</a></li>\n<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/gestion-reglementation-federale/plans-prospectif-reglementation-echelle-gouvernement.html\">Plans prospectifs de la r\u00e9glementation \u00e0 l'\u00e9chelle du gouvernement</a></li>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/ccr/fra/1427420901391/1427420973247\">Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation Canada\u2013\u00c9tats-Unis</a></li>\n</ul>\n<p>Pour de plus amples renseignements concernant les consultations actuelles ou \u00e0 venir sur les projets de r\u00e8glement f\u00e9d\u00e9raux, veuillez consulter la <a href=\"http://gazette.gc.ca/accueil-home-fra.html\"><i>Gazette du Canada</i></a> ou le site <a href=\"https://www.canada.ca/fr/gouvernement/systeme/consultations/consultationdescanadiens.html\">Web Consultations aupr\u00e8s des Canadiens</a>.</p>\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}