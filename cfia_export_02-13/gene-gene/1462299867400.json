{
    "dcr_id": "1462299867400",
    "title": {
        "en": "Buying organic food: labels and advertising can help",
        "fr": "Acheter des aliments <span class=\"nowrap\">biologiques :</span> les \u00e9tiquettes et la publicit\u00e9 peuvent vous aider"
    },
    "html_modified": "2024-02-13 9:51:25 AM",
    "modified": "2023-07-12",
    "issued": "2023-07-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_ctre_organic_1462299867400_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_ctre_organic_1462299867400_fra"
    },
    "ia_id": "1462299912625",
    "parent_ia_id": "1526652186496",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "Organic foods",
        "fr": "Aliments biologiques"
    },
    "activity": {
        "en": "Certifying organic products|Labelling",
        "fr": "Certification de produits biologiques|\u00c9tiquetage"
    },
    "commodity": {
        "en": "Honey|Grain-based foods|Fruits and vegetables, processed products|Dairy products|Fruits and vegetables, fresh|Fish and seafood|Egg, processed products|Meat products and food animals|Other|Maple products",
        "fr": "Miel|Aliments \u00e0 base de grains|Fruits et l\u00e9gumes, produits  transform\u00e9s|Produits laitiers|Fruits et l\u00e9gumes, frais|Poissons et fruits de mer|Oeufs, produits transform\u00e9s|Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande|Autre|Produits d'\u00e9rable"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1526652186496",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Buying organic food: labels and advertising can help",
        "fr": "Acheter des aliments <span class=\"nowrap\">biologiques :</span> les \u00e9tiquettes et la publicit\u00e9 peuvent vous aider"
    },
    "label": {
        "en": "Buying organic food: labels and advertising can help",
        "fr": "Acheter des aliments <span class=\"nowrap\">biologiques :</span> les \u00e9tiquettes et la publicit\u00e9 peuvent vous aider"
    },
    "templatetype": "content page 1 column",
    "node_id": "1462299912625",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526652186199",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/organic-products/buying-organic-food/",
        "fr": "/produits-biologiques/acheter-des-aliments-biologiques/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Buying organic food: labels and advertising can help",
            "fr": "Acheter des aliments biologiques : les \u00e9tiquettes et la publicit\u00e9 peuvent vous aider"
        },
        "description": {
            "en": "The amount of certified organic content in a product determines how organic information can be displayed.",
            "fr": "Le pourcentage de contenu certifi\u00e9 biologique dans un produit d\u00e9termine les all\u00e9gations relatives au contenu biologique qui peuvent \u00eatre affich\u00e9es."
        },
        "keywords": {
            "en": "food safety tips, labels, food packaging, storage, food handling, specific products, risks, organic food",
            "fr": "Conseils sur la salubrit\u00e9 des aliments, \u00e9tiquettes, emballage, entreposage, produits, risques sp\u00e9cifiques, \u00e8tiquetage, aliments biologiques"
        },
        "dcterms.subject": {
            "en": "agri-food products,best practices,consumer protection,consumers,food safety",
            "fr": "produit agro-alimentaire,meilleures pratiques,protection du consommateur,consommateur,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-07-12",
            "fr": "2023-07-12"
        },
        "modified": {
            "en": "2023-07-12",
            "fr": "2023-07-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Buying organic food: labels and advertising can help",
        "fr": "Acheter des aliments biologiques : les \u00e9tiquettes et la publicit\u00e9 peuvent vous aider"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The amount of certified organic content in a product determines how organic information can be displayed.</p>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<h2 class=\"h3\">Single ingredient (for example, strawberries), or multi-ingredient foods (for example, strawberry jam) with 95% or more organic content</h2>\n<ul>\n<li>Product can be labelled or advertised as organic</li>\n<li>Canada organic logo can be on product packaging</li>\n</ul>\n</div>\n<div class=\"col-sm-3\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/food_ctre_organic_img1_1688497825295_eng.png\" class=\"img-responsive mrgn-tp-lg\" alt=\"\"></p>\n</div>\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<h2 class=\"h3\">Multi-ingredient foods with 70% to less than 95% organic content</h2>\n<ul>\n<li>Product can be labelled with \"Contains X% organic ingredients\"</li>\n<li>Canada organic logo is not allowed</li>\n</ul>\n</div>\n<div class=\"col-sm-3\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/food_ctre_organic_img2_1688497875371_eng.png\" class=\"img-responsive mrgn-tp-lg\" alt=\"\"></p>\n</div>\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<h2 class=\"h3\">Multi-ingredient foods with less than 70% organic content</h2>\n<ul>\n<li>May only identify organic content in the list of ingredients</li>\n<li>No claims about organic content can be on the product packaging</li>\n<li>Canada organic logo is not allowed</li>\n</ul>\n</div>\n<div class=\"col-sm-3\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/food_ctre_organic_img3_1688497897245_eng.png\" class=\"img-responsive mrgn-tp-lg\" alt=\"\"></p>\n</div>\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le pourcentage de contenu certifi\u00e9 biologique dans un produit d\u00e9termine les all\u00e9gations relatives au contenu biologique qui peuvent \u00eatre affich\u00e9es.</p>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<h2 class=\"h3\">Aliment \u00e0 ingr\u00e9dient unique (par exemple, fraises) ou \u00e0 ingr\u00e9dients multiples (par exemple, confiture aux fraises) dont le contenu biologique est \u00e9gal ou sup\u00e9rieur \u00e0 95\u00a0%</h2>\n<ul>\n<li>Le terme \u00ab\u00a0biologique\u00a0\u00bb peut \u00eatre utilis\u00e9 sur l'\u00e9tiquette ou dans la publicit\u00e9 du produit</li>\n<li>Le logo Biologique Canada peut figurer sur l'emballage du produit</li>\n</ul>\n</div>\n<div class=\"col-sm-3\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/food_ctre_organic_img1_1688497825295_fra.png\" class=\"img-responsive mrgn-tp-lg\" alt=\"\"></p>\n</div>\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<h2 class=\"h3\">Aliment \u00e0 ingr\u00e9dients multiples dont le contenu biologique est \u00e9gal ou sup\u00e9rieur \u00e0 70\u00a0% et inf\u00e9rieur \u00e0 95\u00a0%</h2>\n<ul>\n<li>Le produit peut porter sur son \u00e9tiquette la mention \u00ab\u00a0Contient X\u00a0% d'ingr\u00e9dients biologiques\u00a0\u00bb</li>\n<li>L'utilisation du logo Biologique Canada est interdite</li>\n</ul>\n</div>\n<div class=\"col-sm-3\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/food_ctre_organic_img2_1688497875371_fra.png\" class=\"img-responsive mrgn-tp-lg\" alt=\"\"></p>\n</div>\n</div>\n\n<div class=\"clear\"></div>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<h2 class=\"h3\">Aliment \u00e0 ingr\u00e9dients multiples dont le contenu biologique est inf\u00e9rieur \u00e0 70\u00a0%</h2>\n<ul>\n<li>Le contenu biologique peut uniquement figurer dans la liste d'ingr\u00e9dients</li>\n<li>Aucune all\u00e9gation sur le contenu biologique ne peut figurer sur l'emballage du produit</li>\n<li>L'utilisation du logo Biologique Canada est interdite</li>\n</ul>\n</div>\n<div class=\"col-sm-3\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/food_ctre_organic_img3_1688497897245_fra.png\" class=\"img-responsive mrgn-tp-lg\" alt=\"\"></p>\n</div>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}