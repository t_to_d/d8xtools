{
    "dcr_id": "1336319434490",
    "title": {
        "en": "Use of Shipping Marks",
        "fr": "Utilisation des marques d'exp\u00e9dition"
    },
    "html_modified": "2024-01-26 2:21:07 PM",
    "modified": "2018-10-30",
    "issued": "2012-05-06 11:50:39",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexd_1336319434490_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexd_1336319434490_fra"
    },
    "ia_id": "1336319505953",
    "parent_ia_id": "1536170495320",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food",
        "fr": "Importation d\u2019aliments"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1536170495320",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Use of Shipping Marks",
        "fr": "Utilisation des marques d'exp\u00e9dition"
    },
    "label": {
        "en": "Use of Shipping Marks",
        "fr": "Utilisation des marques d'exp\u00e9dition"
    },
    "templatetype": "content page 1 column",
    "node_id": "1336319505953",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1536170455757",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/use-of-shipping-marks/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/utilisation-des-marques-d-expedition/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Use of Shipping Marks",
            "fr": "Utilisation des marques d'exp\u00e9dition"
        },
        "description": {
            "en": "General policy on the use of shipping marks on cartons and the use of shipping marks under alternative packaging procedures.",
            "fr": "Politique g\u00e9n\u00e9rale sur l'utilisation des marques d'exp\u00e9dition sur des contenants et l'utilisation des marques d'exp\u00e9dition avec d'autres types d'emballage."
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, 1990, Meat Hygiene Manual of Procedures, manual, meat inspection, procedures, import, countries, Official Meat Inspection Certificates, OMIC, shiping marks",
            "fr": "Loi sur l'inspection des viandes, R\u00e8glement de 1990 sur l'inspection des viandes, Manuel des m\u00e9thodes de l'hygi\u00e8ne des viandes, inspection des viandes , proc\u00e9dures, importation, pays, certificats officiels d'inspection des viandes, COIV, marques d'exp\u00e9dition"
        },
        "dcterms.subject": {
            "en": "imports,inspection,food inspection,handbooks,standards,policy,meat",
            "fr": "importation,inspection,inspection des aliments,manuel,norme,politique,viande"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Meat Programs Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division des programmes des viandes"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-06 11:50:39",
            "fr": "2012-05-06 11:50:39"
        },
        "modified": {
            "en": "2018-10-30",
            "fr": "2018-10-30"
        },
        "type": {
            "en": "reference material,standard",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,norme"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Use of Shipping Marks",
        "fr": "Utilisation des marques d'exp\u00e9dition"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n<ul class=\"list-unstyled mrgn-lft-0\">\n<li><a href=\"#a1\">General policy</a></li>\n<li><a href=\"#a2\">Use of shipping marks under alternative packaging procedures</a></li>\n</ul>\n<h2 id=\"a1\">General policy</h2>\n<p>Shipping marks are used to identify all shipping containers (cartons) within an imported shipment to the appropriate Official Meat Inspection Certificate (OMIC). <strong>Each shipping container in each imported lot must be clearly marked with an appropriate shipping mark</strong>. In the case of shipments from the United States, the United States Department of Agriculture (USDA) export stamp, bearing the appropriate export certificate number, is the shipping mark and does not have to be entered in the shipping marks column on the export certificate.</p>\n<p>The shipping marks can be specifically generated numbers or they can represent the appropriate <abbr title=\"Official Meat Inspection Certificate\">OMIC</abbr> number. They must not be repeated in the next twelve (12) months on any OMIC from the same exporting country. There may be more than one shipping mark on an <abbr title=\"Official Meat Inspection Certificate\">OMIC</abbr>, but there may not be two <abbr title=\"Official Meat Inspection Certificates\">OMICs</abbr> with the same shipping mark.</p>\n<p>The shipping marks must be entered on the <abbr title=\"Official Meat Inspection Certificate\">OMIC</abbr>, in box 11, \"shipping marks\" on certificates from any country other than the United States whether they are specifically generated numbers or whether they represent the <abbr title=\"Official Meat Inspection Certificate\">OMIC</abbr> number. The establishment numbers cannot be used as a shipping mark.</p>\n<p>Where the individual stamping of the retail containers would not be practical (for example, small retail containers not containerized in larger containers, or products in tray packs), the alternative packaging procedure may be used. The alternative procedure allows for the pallet to be considered as the shipping container.</p>\n<h2 id=\"a2\">Use of shipping marks under alternative packaging procedures</h2>\n<h3>Use of pallets as shipping containers</h3>\n<h4>Policy</h4>\n<p>Palletized, consumer packaged, fully marked and labelled meat and poultry products, intended to move as an intact unit to retail distribution, may be imported with the shipping marks and shipping container label applied to the outside of the pallet, rather than to individual tray packs or cartons.</p>\n<h4>Alternative packaging procedures for fully marked and labelled retail products</h4>\n<h5>Packaging and Palletizing</h5>\n<p>Fully marked and labelled, packaged products are placed in cartons or trays for retail sale as a unit. The trays may be stretch wrapped in groups or individually. The trays should be sufficiently sturdy and high enough to allow handling during import inspection sample selection.</p>\n<p>The trays or cartons are then palletized and subsequently stretch wrapped (or covered by corrugated material). The wrapped pallet is considered as one shipping container for import certification purposes.</p>\n<p>Only one type of product may be assembled on each pallet. Product type is interpreted as a meat product packaged in one container type and size, one product formula and originating from one processing establishment.</p>\n<h5>Labelling</h5>\n<p>When a pallet is identified as a shipping container, one main shipping label is required on the side of the pallet. This label can be in the form of a placard underneath the pallet stretch wrap or as an adhesive label.</p>\n<p>The pallet label must display in a <strong>prominent and legible manner</strong>, all mandatory information required on a shipping container and shipping marks.</p>\n<p>The shipping mark or export stamp (in the case of United States products) must be applied to the placard or shipping container labels of the stretch wrapped pallet. Trays and cartons do not need to be marked with the shipping mark/export stamp. However, if the entire pallet does not move as an intact unit to retail distribution, then the individual cartons or trays will be considered shipping cartons and shall have to meet the mandatory labelling requirements, including the shipping marks.</p>\n<h5> Certification</h5>\n<p>All production codes present on the retail package (such as date codes imprinted on the packages, or the entire production code required to be permanently marked on cans or other containers of hermetically sealed meat products) for each type of product in the shipment must be listed on the foreign country's export <abbr title=\"Official Meat Inspection Certificate\">OMIC</abbr>. This will allow for a production code based recall, should the need arise.</p>\n<p>Box 12 of the <abbr title=\"Official Meat Inspection Certificate\">OMIC</abbr> (number and kind of packages) will identify the number of pallets in the shipment, number of cartons or trays, the number of each individual unit / carton or / tray, the size of the units and all production codes. Example: 1 pallet (25 trays X 6 cans/tray X 250\u00a0<abbr title=\"millilitres\">m</abbr>l) production codes: 00000, 00001 and 00002.</p>\n<p>In the event that production codes are missing, incorrect or completely illegible on a health certificate, product shall not be permitted to move as an intact unit into Canada. The shipment can be presented under normal import re-inspection procedures, provided the shipping marks are affixed to the individual cartons or trays. This must be done by an official of the foreign inspection system. If this is not possible, the shipment will be refused entry.</p>\n<h4>Importer responsibility</h4>\n<p>The importer is responsible for assuring that the full pallet will be distributed to the retail distribution level as an intact unit. If not, each individual unit that is distributed must be marked with the appropriate labelling features and shipping marks. If a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> official determines that a company or importer violates the provisions of this program, the foreign establishment shall be removed from the program. The foreign establishment that has been suspended from the program must submit a letter, through their competent authority to the CFIA, requesting reinstatement to the program. This correspondence must provide details of corrective actions that have been taken to prevent future violations.</p>\n<h4>Import establishment responsibility</h4>\n<p>The import establishment is responsible for presenting the lot in a manner that each individual unit within the lot will have an equal chance of being selected as a sample.</p>\n<p>As the meat products are subject to normal sampling and import inspection procedures, the import inspection establishment must provide facilities to draw the random sample, re-shrink wrap, re-stack and reapply the placard or the label to the pallets from which the necessary samples were drawn.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n<ul class=\"list-unstyled mrgn-lft-0\">\n<li><a href=\"#a1\">Politique g\u00e9n\u00e9rale</a></li>\n<li><a href=\"#a2\">Utilisation des marques d'exp\u00e9dition avec d'autres types d'emballage</a></li>\n</ul>\n\n<h2 id=\"a1\">Politique g\u00e9n\u00e9rale</h2>\n<p>Les marques d'exp\u00e9dition servent \u00e0 \u00e9tablir une correspondance entre tous les contenants d'exp\u00e9dition (bo\u00eetes) d'un envoi de produits import\u00e9s et le Certificat officiel d'inspection des viandes (COIV) appropri\u00e9. <strong>Chaque contenant d'exp\u00e9dition du lot import\u00e9 doit \u00eatre clairement identifi\u00e9 au moyen d'une marque d'exp\u00e9dition appropri\u00e9e.</strong> Dans le cas d'envois en provenance des \u00c9tats-Unis, le timbre d'exportation du <span lang=\"en\" xml:lang=\"en\">United States Department of Agriculture</span> (USDA) portant le num\u00e9ro du certificat d'exportation appropri\u00e9 fait office de marque d'exp\u00e9dition, et cette derni\u00e8re n'a pas \u00e0 \u00eatre inscrite dans la colonne r\u00e9serv\u00e9e \u00e0 cette fin sur le certificat d'exportation.</p>\n<p>Une marque d'exp\u00e9dition peut \u00eatre compos\u00e9e de chiffres sp\u00e9cialement g\u00e9n\u00e9r\u00e9s ou repr\u00e9senter le num\u00e9ro du <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr> appropri\u00e9. Une marque ne doit pas \u00eatre r\u00e9p\u00e9t\u00e9e au cours des douze (12) mois qui suivent sa premi\u00e8re utilisation sur aucun <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr> provenant du m\u00eame pays exportateur. Il peut y avoir plusieurs marques d'exp\u00e9dition sur un certificat, mais deux <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr> ne peuvent jamais porter la m\u00eame marque d'exp\u00e9dition.</p>\n<p>Les marques d'exp\u00e9dition doivent \u00eatre inscrites \u00e0 la case <abbr title=\"num\u00e9ro\">no</abbr>\u00a011 \u00ab\u00a0Marques d'exp\u00e9dition\u00a0\u00bb du <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr> pour tous les pays autres que les \u00c9tats-Unis. Le num\u00e9ro de l'\u00e9tablissement ne devrait pas \u00eatre utilis\u00e9 comme une marque d'exp\u00e9dition.</p>\n<p>Lorsque l'estampillage de chaque contenant de d\u00e9tail n'est pas chose pratique (par exemple, petits contenants de d\u00e9tail non conteneuris\u00e9s dans de gros contenants ou produits conditionn\u00e9s en barquettes), on peut apposer les marques d'exp\u00e9dition sur d'autres types d'emballage. Ainsi, les palettes peuvent faire office de contenants d'exp\u00e9dition.</p>\n<h2>Utilisation des marques d'exp\u00e9dition avec d'autres types d'emballage</h2>\n<h3>Utilisation de palettes comme contenants d'exp\u00e9dition</h3>\n<h4>Politique</h4>\n<p>Pour ce qui est des produits de viande et de volaille import\u00e9s qui sont d\u00fbment marqu\u00e9s et \u00e9tiquet\u00e9s, emball\u00e9s en portions-consommateurs, palletis\u00e9s et destin\u00e9s \u00e0 \u00eatre distribu\u00e9s \u00e0 titre d'une unit\u00e9 compl\u00e8te dans le commerce de d\u00e9tail, les marques d'exp\u00e9dition et l'\u00e9tiquette d'identification du contenant d'exp\u00e9dition peuvent \u00eatre appos\u00e9es sur l'ext\u00e9rieur de la palette, plut\u00f4t que sur les bo\u00eetes ou les barquettes.</p>\n<h4>Autres m\u00e9thodes d'emballage pour les produits de d\u00e9tail d\u00fbment marqu\u00e9s et \u00e9tiquet\u00e9s</h4>\n<h5> Emballage et palletisation</h5>\n<p>Les produits emball\u00e9s d\u00fbment marqu\u00e9s et \u00e9tiquet\u00e9s sont plac\u00e9s dans des bo\u00eetes ou des barquettes destin\u00e9es \u00e0 \u00eatre distribu\u00e9es dans le commerce de d\u00e9tail \u00e0 titre d'une unit\u00e9 compl\u00e8te. Les barquettes peuvent \u00eatre emball\u00e9es sous film \u00e9tirable de fa\u00e7on individuelle ou group\u00e9es. Elles doivent \u00eatre suffisamment solides et hautes pour qu'on puisse facilement les manipuler durant la s\u00e9lection des \u00e9chantillons aux fins de l'inspection des importations.</p>\n<p>Les bo\u00eetes ou les barquettes sont ensuite palletis\u00e9es et emball\u00e9es sous film \u00e9tirable (ou recouvertes d'un mat\u00e9riau ondul\u00e9). La palette emball\u00e9e sous film \u00e9tirable est consid\u00e9r\u00e9e comme \u00e9tant un seul contenant d'exp\u00e9dition aux fins de la certification des importations.</p>\n<p>Un seul type de produit peut \u00eatre plac\u00e9 sur chaque palette. Un type de produit peut \u00eatre interpr\u00e9t\u00e9 comme un produit de viande emball\u00e9 dans un m\u00eame type et format de contenant, fabriqu\u00e9 selon une m\u00eame formule de composition et provenant d'un m\u00eame \u00e9tablissement de transformation.</p>\n<h5>\u00c9tiquetage</h5>\n<p>Lorsqu'une palette est identifi\u00e9e comme contenant d'exp\u00e9dition, une seule \u00e9tiquette d'identification doit \u00eatre appos\u00e9e sur un de ses c\u00f4t\u00e9s. Il peut s'agir d'une \u00e9tiquette plac\u00e9e sous le film \u00e9tirable ou d'une \u00e9tiquette adh\u00e9sive.</p>\n<p>L'\u00e9tiquette d'identification de la palette doit s'afficher <strong>d'une fa\u00e7on \u00e9minente et lisible</strong> toute l'information obligatoire requise concernant un contenant d'exp\u00e9dition et les marques d'exp\u00e9dition.</p>\n<p>La marque d'exp\u00e9dition ou le timbre d'exportation (dans le cas de produits en provenance des \u00c9tats-Unis) doit \u00eatre appos\u00e9 sur l'\u00e9tiquette plac\u00e9e sous le film \u00e9tirable ou sur l'\u00e9tiquette adh\u00e9sive de la palette, plut\u00f4t que sur les bo\u00eetes/barquettes individuelles. Cependant, si la palette n'est pas destin\u00e9e \u00e0 \u00eatre achemin\u00e9e dans le commerce de d\u00e9tail au titre d'une unit\u00e9 compl\u00e8te, les bo\u00eetes ou barquettes individuelles sont consid\u00e9r\u00e9es comme \u00e9tant des contenants d'exp\u00e9dition et doivent donc satisfaire aux exigences en mati\u00e8re d'\u00e9tiquetage obligatoire, y compris les exigences relatives aux marques d'exp\u00e9dition.</p>\n<h5>Certification</h5>\n<p>Tous les codes de production pr\u00e9sents sur les produits emball\u00e9s pour la vente au d\u00e9tail (tels que les codes de date imprim\u00e9s sur les emballages ou les codes de production complets devant \u00eatre appos\u00e9s d'une mani\u00e8re permanente sur les bo\u00eetes ou autres contenants pour produits de viande ferm\u00e9s herm\u00e9tiquement) doivent figurer sur le <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr> du pays \u00e9tranger, et ce, pour chaque type de produit que renferme l'envoi. On pourra ainsi effectuer, au besoin, un rappel en fonction des codes de production.</p>\n<p>La case <abbr title=\"num\u00e9ro\">no</abbr>\u00a012 du <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr> (Nombre et nature des colis) doit indiquer le nombre de palettes de l'envoi, le nombre de bo\u00eetes ou de barquettes, le nombre d'unit\u00e9s individuelles par bo\u00eete ou barquette, le format des unit\u00e9s et tous les codes de production. Exemple : 1 palette (25\u00a0barquettes X <span class=\"noWrap\">6 bo\u00eetes/barquette</span> X 250\u00a0<abbr title=\"millilitres\">ml</abbr>), codes de production\u00a0: 00000, 00001 et 00002.</p>\n<p>Les codes de production sont manquants, inexacts ou compl\u00e8tement illisibles sur le certificat sanitaire, l'envoi ne pourra \u00eatre achemin\u00e9 au Canada au titre d'une unit\u00e9 compl\u00e8te. Il pourra cependant \u00eatre soumis aux proc\u00e9dures habituelles de r\u00e9inspection \u00e0 l'importation, sous r\u00e9serve que les marques d'exp\u00e9dition requises soient appos\u00e9es sur les bo\u00eetes ou barquettes individuelles. Cela doit \u00eatre fait par un repr\u00e9sentant officiel du syst\u00e8me d'inspection \u00e9tranger. Si cela n'est pas possible, l'entr\u00e9e de l'envoi sera refus\u00e9e.</p>\n<h4>Responsabilit\u00e9 de l'importateur</h4>\n<p>L'importateur doit s'assurer qu'une palette intacte est distribu\u00e9e dans le commerce de d\u00e9tail au titre d'une unit\u00e9 compl\u00e8te. Dans le cas contraire, chaque unit\u00e9 individuelle destin\u00e9e \u00e0 la distribution doit porter les dispositifs de l'\u00e9tiquetage et les marques d'exp\u00e9dition appropri\u00e9es. Si un repr\u00e9sentant de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d\u00e9termine qu'une compagnie ou un importateur ne respecte pas les conditions de ce programme, l'\u00e9tablissement \u00e9tranger sera suspendu de celui-ci. En pareil cas, l'\u00e9tablissement \u00e9tranger suspendu doit pr\u00e9senter une demande de r\u00e9admission au programme, au moyen d'une lettre adress\u00e9e, par l'entremise de son repr\u00e9sentant officiel \u00e0 l'ACIA. Cette lettre doit fournir des d\u00e9tails sur les actions correctives prises pour \u00e9viter \u00eatre en infraction \u00e0 l'avenir.</p>\n<h4>Responsabilit\u00e9 de l'\u00e9tablissement importateur</h4>\n<p>L'\u00e9tablissement importateur est responsable de pr\u00e9senter le lot de telle mani\u00e8re que chacune des unit\u00e9s individuelles ait la m\u00eame probabilit\u00e9 d'\u00eatre s\u00e9lectionn\u00e9e en tant qu'\u00e9chantillon.</p>\n<p>Comme les produits de viande sont soumis aux proc\u00e9dures habituelles d'inspection et d'\u00e9chantillonnage \u00e0 l'importation, l'\u00e9tablissement importateur doit fournir les installations n\u00e9cessaires pour pr\u00e9lever les \u00e9chantillons au hasard ainsi que pour emballer sous film \u00e9tirable, empiler et \u00e9tiqueter de nouveau les palettes sur lesquelles on aura pr\u00e9lev\u00e9 les \u00e9chantillons requis.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}