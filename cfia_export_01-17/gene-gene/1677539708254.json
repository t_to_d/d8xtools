{
    "dcr_id": "1677539708254",
    "title": {
        "en": "Meet Anas Zaman, CFIA junior communications officer",
        "fr": "Voici Anas Zaman, agent des communications junior de l'ACIA"
    },
    "html_modified": "28-2-2023",
    "modified": "27-2-2023",
    "issued": "27-2-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/ins_meet_anas_zaman_1677539708254_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/ins_meet_anas_zaman_1677539708254_fra"
    },
    "parent_ia_id": "1565298312402",
    "ia_id": "1677539708622",
    "parent_node_id": "1565298312402",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Meet Anas Zaman, CFIA junior communications officer",
        "fr": "Voici Anas Zaman, agent des communications junior de l'ACIA"
    },
    "label": {
        "en": "Meet Anas Zaman, CFIA junior communications officer",
        "fr": "Voici Anas Zaman, agent des communications junior de l'ACIA"
    },
    "templatetype": "content page 1 column",
    "node_id": "1677539708622",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298312168",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/science-and-innovation/anas-zaman/",
        "fr": "/inspecter-et-proteger/science-et-innovation/anas-zaman/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Meet Anas Zaman, CFIA junior communications officer",
            "fr": "Voici Anas Zaman, agent des communications junior de l'ACIA"
        },
        "description": {
            "en": "For Generation Z (including me), choosing a career path and entering the workforce can be intimidating. Here's how a co-op student opportunity at the CFIA opened my eyes to the fascinating world of communications in the Government of Canada.",
            "fr": "Pour la g\u00e9n\u00e9ration Z (moi y compris), choisir un cheminement de carri\u00e8re et entrer sur le march\u00e9 du travail peuvent \u00eatre intimidants. Voici comment une occasion pour \u00e9tudiants du programme coop\u00e9ratif \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) m'a ouvert les yeux sur le monde fascinant des communications au sein du gouvernement du Canada."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, CFIA, Anas Zaman",
            "fr": "Agence canadienne d'inspection des aliments, ACIA, Anas Zaman"
        },
        "dcterms.subject": {
            "en": "imports,food inspection,animal diseases,animal health",
            "fr": "importation,inspection des aliments,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-02-28",
            "fr": "2023-02-28"
        },
        "modified": {
            "en": "2023-02-27",
            "fr": "2023-02-27"
        },
        "type": {
            "en": "news publication",
            "fr": "publication d'information"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Meet Anas Zaman, CFIA junior communications officer",
        "fr": "Voici Anas Zaman, agent des communications junior de l'ACIA"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_meet_anas_zaman_600x600_1677535765259_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>My name is Anas Zaman and I am a junior communications officer at the Canadian Food Inspection Agency (CFIA). Throughout my undergraduate studies, I worked part-time as a student at the CFIA. Upon graduation, I continued working in the position\u2014and I am loving every minute of it.</p>\n\n<p>This is not a job I always knew I wanted. In fact, even going into my first year of university, I was still not 100% sure what route to take with my life. I think a lot of students struggle with that. It's hard making life decisions at the ripe old age of 18!</p>\n\n<p>Even though I was nervous to commit to one field of study, I stuck with it and earned my Bachelor of Communications and Media Studies at Carleton University while getting hands-on work experience and making an impact at the CFIA.</p>\n\n<h2>Diversity by default</h2>\n\n<p>Around my third year of university, my eyes really opened to the power of communications and made me realize this was the correct subject for me. It's a fun, unique and creative way of sharing news, updates and information with Canadians.</p>\n\n<p>But communicating effectively means you need to understand different identities, cultures and groups in order to best reach them. Embracing diverse perspectives, backgrounds and life experiences is something I became passionate about.</p>\n\n<p>My peers and I, via our university projects, put theory into practice by helping various organizations and businesses in Ottawa achieve their missions through solid communication strategies and tactics. I quickly learned that the medium is the message. At that point, I knew that I wanted to craft important, meaningful messages as much as possible in my future career.</p>\n\n<p>When I found out about a student placement opportunity at the CFIA, the stars aligned. The position would allow me to get hands-on experience on a wide range of files, including food safety, plant health, animal health, innovation and internal communications that directly impact Canadians, industry and other stakeholders.</p>\n\n<h2>Finding balance (and cool files) as a part-time student</h2>\n\n<p>Beyond working on products like news releases, dashboards, strategic plans and articles, I saw another side of communications\u2014and discovered that public affairs in the Government of Canada is actually a fun and vibrant environment!</p>\n\n<p>Starting this student placement and first \"office job\" in a pandemic made it tricky to get the \"office life\" experience, but everyone was welcoming and made me feel valued. My colleagues offered career tips and advice, and insights on the world of communications in general. The Branch also had the initiative to get to know me and foster an environment that made me feel like a colleague and not a student. I thankfully had a lot of flexibility to juggle school and work, as my team was very accommodating to my academic schedule. I got to work on real-life communications issues while completing my courses and essays.</p>\n\n<p>I even got to work directly with the CFIA's Vice-President of Communications and Public Affairs on a few files, and worked on creating a new project! Not only did this allow me to work with senior management, but it also allowed me, as a student, to sit in meetings with the President and other Vice-Presidents within the Agency. This was a wonderful opportunity for a student, and showed me that the Agency values input and perspective from employees and students alike.</p>\n\n<h2>Working for all Canadians</h2>\n\n<p>Creating products for underrepresented communities in Canada has been one of the most gratifying aspects of working at the CFIA so far. For example, I've worked on a series called <i>Beyond Barriers</i>, which has opened a safe space for storytelling and conversation with employees about the important work and life experiences of our colleagues living with disabilities.</p>\n\n<p>In my perspective, this is one of the best things about the field of communications: highlighting and uplifting the voices and experiences of marginalized and underrepresented groups. The fact that this is part of my job only makes me love it more.</p>\n\n<p>I understand the experience of being marginalized and not having a voice to speak my concerns. In this role, I can see the impact of my contributions and the lens I bring to my files, whether the communications are intended for CFIA staff or the general public.</p>\n\n<p>At the end of the day, working at the CFIA means we are working for Canadians. This is only the start of my career, but I'll keep striving to make sure people from all walks of life are heard and meaningfully represented whenever I get the chance.</p>\n\n<p>For now, I know I'm in the right place!</p>\n\n<h2>Learn more</h2>\n\n<ul>\n<li><a href=\"/eng/1568731957675/1568743720700#2022\">Do you have what it takes to be a superhero?</a></li>\n<li><a href=\"/about-cfia/job-opportunities/eng/1299857348736/1299857657230\">Job opportunities at the CFIA</a></li>\n<li><a href=\"/about-cfia/job-opportunities/hr-information/vet-student-corner/eng/1324410496461/1324410579558\">Veterinary student corner</a></li>\n<li><a href=\"https://www.canada.ca/en/services/jobs/opportunities/student.html\">Youth and student employment</a> (Canada.ca)</li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_meet_anas_zaman_600x600_1677535765259_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>Je m'appelle Anas Zaman et je suis agent des communications junior \u00e0 l'Agence canadienne d'inspection des aliments (ACIA). Tout au long de mes \u00e9tudes de premier cycle, j'ai travaill\u00e9 \u00e0 temps partiel comme \u00e9tudiant \u00e0 l'ACIA. Apr\u00e8s avoir obtenu mon dipl\u00f4me, j'ai continu\u00e9 \u00e0 travailler dans ce poste \u2013 et j'adore chaque minute.</p>\n\n<p>Je n'ai pas toujours su que je voulais ce type de travail. En fait, m\u00eame en entrant dans ma premi\u00e8re ann\u00e9e d'universit\u00e9, je n'\u00e9tais toujours pas certain \u00e0 100 % de la voie \u00e0 suivre pour le reste de ma vie. Je pense que beaucoup d'\u00e9tudiants sont confront\u00e9s \u00e0 \u00e7a. Il est difficile de prendre des d\u00e9cisions de vie \u00e0 l'\u00e2ge de 18 ans!</p>\n\n<p>M\u00eame si j'\u00e9tais nerveux de m'engager dans un domaine d'\u00e9tudes, j'ai pers\u00e9v\u00e9r\u00e9 et j'ai obtenu mon baccalaur\u00e9at en communications et \u00e9tude des m\u00e9dias \u00e0 l'Universit\u00e9 Carleton, tout en acqu\u00e9rant une exp\u00e9rience de travail pratique et en ayant un impact \u00e0 l'ACIA.</p>\n\n<h2>Diversit\u00e9 par d\u00e9faut</h2>\n\n<p>Vers ma troisi\u00e8me ann\u00e9e d'universit\u00e9, mes yeux se sont vraiment ouverts sur la r\u00e9alit\u00e9 du pouvoir des communications et m'ont fait r\u00e9aliser que c'\u00e9tait le sujet qui me convenait. C'est une fa\u00e7on amusante, unique et cr\u00e9ative de partager des nouvelles, des mises \u00e0 jour et des renseignements avec les Canadiens.</p>\n\n<p>Toutefois, communiquer efficacement signifie que vous devez comprendre divers groupes, identit\u00e9s et cultures, afin de les atteindre de la mani\u00e8re la plus efficace. Je me suis passionn\u00e9 pour la diversit\u00e9 des perspectives, des origines et des exp\u00e9riences de vie.</p>\n\n<p>Mes coll\u00e8gues et moi-m\u00eame, \u00e0 travers mes projets universitaire, avons mis la th\u00e9orie en pratique en aidant divers organismes et entreprises \u00e0 Ottawa \u00e0 accomplir leurs missions gr\u00e2ce \u00e0 des strat\u00e9gies et des tactiques de communication solides. J'ai rapidement appris que le support est le message. \u00c0 ce moment-l\u00e0, j'ai su que je voulais, autant que possible, r\u00e9diger des messages importants et significatifs dans le cadre de ma carri\u00e8re future.</p>\n\n<p>Lorsque j'ai d\u00e9couvert qu'il y avait une possibilit\u00e9 de placement \u00e9tudiant \u00e0 l'ACIA, les \u00e9toiles \u00e9taient align\u00e9es. Ce poste me permettait d'acqu\u00e9rir une exp\u00e9rience pratique dans un large \u00e9ventail de dossiers, dont la salubrit\u00e9 des aliments, la protection des v\u00e9g\u00e9taux, la sant\u00e9 des animaux, l'innovation et les communications internes qui touchent directement les Canadiens, l'industrie et d'autres intervenants.</p>\n\n<h2>Trouver l'\u00e9quilibre (et des dossiers int\u00e9ressants) en tant qu'\u00e9tudiant \u00e0 temps partiel</h2>\n\n<p>En plus de travailler sur des produits comme des communiqu\u00e9s de presse, des tableaux de bord, des plans strat\u00e9giques et des articles, j'ai vu un autre aspect des communications et j'ai d\u00e9couvert que les affaires publiques du gouvernement du Canada sont en fait un environnement amusant et dynamique!</p>\n\n<p>Commencer ce stage d'\u00e9tudiant et le premier \u00ab\u00a0emploi de bureau\u00a0\u00bb dans le contexte d'une pand\u00e9mie a rendu difficile l'obtention de l'exp\u00e9rience de \u00ab\u00a0vie de bureau\u00a0\u00bb, mais tout le monde \u00e9tait accueillant et m'a fait sentir appr\u00e9ci\u00e9. Mes coll\u00e8gues ont offert des trucs et conseils de carri\u00e8re, et des id\u00e9es sur le monde des communications en g\u00e9n\u00e9ral. La direction g\u00e9n\u00e9rale a \u00e9galement pris l'initiative de me conna\u00eetre et de favoriser un environnement qui me faisait me sentir comme un coll\u00e8gue et non comme un \u00e9tudiant. Heureusement, j'ai eu beaucoup de souplesse pour pouvoir jongler entre l'\u00e9cole et le travail, car mon \u00e9quipe \u00e9tait tr\u00e8s compr\u00e9hensive quant \u00e0 mon horaire de cours. Je me suis mis \u00e0 travailler sur des enjeux de communication r\u00e9els, tout en poursuivant mes cours et mes essais.</p>\n\n<p>J'ai m\u00eame travaill\u00e9 directement avec la vice-pr\u00e9sidente des communications et affaires publiques de l'ACIA sur quelques dossiers et j'ai travaill\u00e9 \u00e0 la cr\u00e9ation d'un nouveau projet! Non seulement cela m'a permis de travailler avec la haute direction, mais cela m'a \u00e9galement permis, en tant qu'\u00e9tudiant, de participer \u00e0 des r\u00e9unions avec la pr\u00e9sidente et d'autres vice-pr\u00e9sidents de l'Agence. Il s'agissait d'une excellente occasion pour un \u00e9tudiant, et cela m'a montr\u00e9 que l'Agence valorise les commentaires et les points de vue des employ\u00e9s et des \u00e9tudiants.</p>\n\n<h2>Travailler pour tous les Canadiens</h2>\n\n<p>La cr\u00e9ation de produits pour les collectivit\u00e9s sous-repr\u00e9sent\u00e9es au Canada a \u00e9t\u00e9 l'un des aspects les plus gratifiants du travail \u00e0 l'ACIA jusqu'\u00e0 pr\u00e9sent. Par exemple, j'ai travaill\u00e9 sur une s\u00e9rie intitul\u00e9e <i>Au-del\u00e0 des obstacles</i>, qui a ouvert un espace s\u00e9curitaire pour raconter des histoires et discuter avec les employ\u00e9s au sujet des exp\u00e9riences de travail et de vie importantes de nos coll\u00e8gues qui vivent avec des handicaps.</p>\n\n<p>Selon moi, c'est l'une des meilleures choses dans le domaine des communications : mettre en lumi\u00e8re et encourager les voix et les exp\u00e9riences des groupes marginalis\u00e9s et sous-repr\u00e9sent\u00e9s. Cela fait partie de mon travail et me le fait aimer davantage.</p>\n\n<p>Je comprends l'exp\u00e9rience d'\u00eatre marginalis\u00e9 et de ne pas avoir une voix pour exprimer mes pr\u00e9occupations. Dans ce r\u00f4le, je peux voir l'impact de mes contributions et la perspective que j'apporte \u00e0 mes dossiers, que les communications soient destin\u00e9es au personnel de l'ACIA ou au grand public.</p>\n\n<p>En fin de compte, travailler \u00e0 l'ACIA signifie que nous travaillons pour les Canadiens. Ce n'est que le d\u00e9but de ma carri\u00e8re, mais je continuerai \u00e0 m'efforcer de faire en sorte que les gens de tous les horizons soient entendus et repr\u00e9sent\u00e9s de fa\u00e7on significative chaque fois que j'en aurai l'occasion.</p>\n\n<p>Pour l'instant, je sais que je suis au bon endroit!</p>\n\n<h2>En savoir plus</h2>\n\n<ul>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/cartes-a-echanger-de-l-acia/fra/1568731957675/1568743720700\">Avez-vous l'\u00e9toffe d'un superh\u00e9ros?</a></li>\n<li><a href=\"/a-propos-de-l-acia/possibilites-d-emploi/fra/1299857348736/1299857657230\">Possibilit\u00e9s d'emploi \u00e0 l'ACIA</a></li>\n<li><a href=\"https://www.canada.ca/fr/services/emplois/opportunites/etudiants.html\">Emplois pour les \u00e9tudiants</a> (Canada.ca)</li>\n</ul>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}