{
    "dcr_id": "1360803210541",
    "title": {
        "en": "How to Make an Access to Information Request",
        "fr": "Comment faire une demande d'acc\u00e8s \u00e0 l'information"
    },
    "html_modified": "2024-02-13 9:48:36 AM",
    "modified": "2019-06-12",
    "issued": "2015-04-23 09:19:57",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/atip_access_request_1360803210541_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/atip_access_request_1360803210541_fra"
    },
    "ia_id": "1360803290065",
    "parent_ia_id": "1299780252921",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299780252921",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "How to Make an Access to Information Request",
        "fr": "Comment faire une demande d'acc\u00e8s \u00e0 l'information"
    },
    "label": {
        "en": "How to Make an Access to Information Request",
        "fr": "Comment faire une demande d'acc\u00e8s \u00e0 l'information"
    },
    "templatetype": "content page 1 column",
    "node_id": "1360803290065",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299780176509",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/atip/access-to-information-request/",
        "fr": "/a-propos-de-l-acia/transparence/aiprp/demande-d-acces-a-l-information/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "How to Make an Access to Information Request",
            "fr": "Comment faire une demande d'acc\u00e8s \u00e0 l'information"
        },
        "description": {
            "en": "Records that may be available through an access to information request include correspondence applications certificates reports not normally published by the CFIA (for example, reports prepared by consultants) Exemptions and exclusions to access to information requests There are certain types of information that may not be released under the Access to Information Act",
            "fr": "Il faut envoyer les demandes \u00e0 ladresse suivante : Bureau de lacc\u00e8s \u00e0 linformation et de la protection des renseignements personnels Services de coordination et de soutien \u00e0 la haute direction Secr\u00e9tariat des services int\u00e9gr\u00e9s Agence canadienne dinspection des aliments 1400, chemin Merivale, tour 1, pi\u00e8ce 0-149 Ottawa (Ontario) K1A 0Y9 Frais Lorsque vous soumettez une demande au titre de la Loi sur lacc\u00e8s \u00e0 linformation , il faut inclure un ch\u00e8que ou un mandat-poste : dun montant de 5,00 $ libell\u00e9 \u00e0 lordre du receveur g\u00e9n\u00e9ral"
        },
        "keywords": {
            "en": "Information Act, information request",
            "fr": "demande dacc&#232;s, adresse suivante, titre de la Loi, protection des renseignements personne, Web du Secr&#233;tariat du Conseil du Tr&#233;sor"
        },
        "dcterms.subject": {
            "en": "access to information,applicants,information,administrative services,computer services",
            "fr": "acc\u00e8s \u00e0 l'information,demandeur,information,services administratifs,services informatiques"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Integrity and Redress Secretariat",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Secr\u00e9tariat de l'int\u00e9grit\u00e9 et des recours"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-23 09:19:57",
            "fr": "2015-04-23 09:19:57"
        },
        "modified": {
            "en": "2019-06-12",
            "fr": "2019-06-12"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "How to Make an Access to Information Request",
        "fr": "Comment faire une demande d'acc\u00e8s \u00e0 l'information"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Records available through the access to information process</h2>\n\n<p>Under the <a href=\"/english/reg/jredirect2.shtml?aia\"><i>Access to Information Act</i></a>, you can request any material under the control of the CFIA, regardless of its medium or form. <strong>Records that may be available</strong> through an access to information request include</p>\n\n<ul>\n<li>correspondence</li>\n<li>applications</li>\n<li>certificates</li>\n<li>reports not normally published by the CFIA (for example, reports prepared by consultants)</li>\n</ul>\n\n<h2>Exemptions and exclusions to access to information requests</h2>\n\n<p>There are certain types of <strong>information that may not be released</strong> under the <i>Access to Information Act</i>. These are called \"exemptions and exclusions.\"</p>\n\n<p>Exemptions protect certain types of information that could cause harm if released.</p>\n\n<p><strong>Exemptions</strong> are classified as</p>\n\n<ul>\n<li>mandatory (the information <strong>must</strong> be withheld), or</li>\n<li>discretionary (the information <strong>may</strong> be withheld).</li>\n</ul>\n\n<p>Some examples of exemptions are</p>\n\n<ul>\n<li>trade secrets of a third party</li>\n<li>personal information</li>\n<li>information on national security</li>\n</ul>\n\n<p>You can find a <strong>complete description of exemptions</strong> in Sections 13\u201323 of the <a href=\"/english/reg/jredirect2.shtml?aia\"><i>Access to Information Act</i></a>.</p>\n\n<p><strong>Exclusions</strong> are records not covered by the Act. These include</p>\n\n<ul>\n<li>published material</li>\n<li>material available for purchase</li>\n<li>museum material</li>\n<li>confidences of the Queen's Privy Council</li>\n</ul>\n\n<h2>Making an access to information request at the CFIA</h2>\n\n<p>Using the <abbr title=\"Access to Information and Privacy\">ATIP</abbr> Online Request service is a convenient way to get requests submitted more quickly and easily, while simplifying the application process. Apply online today to save time and postage.</p>\n\n<p class=\"mrgn-lft-lg\"><img alt=\"computer mouse\" class=\"mrgn-bttm-0\" height=\"20\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/mouse_icon_1389725962310_eng.gif\" width=\"20\"><a href=\"https://atip-aiprp.tbs-sct.gc.ca/en/Home/Privacy\">Online Request</a></p>\n\n<p class=\"mrgn-lft-xl\"><strong>Or</strong></p>\n\n<p>Your requests must be <strong>made in writing</strong>. They also must include sufficient detail to allow staff to find and retrieve the information requested.</p>\n\n<p>The \"<a href=\"https://www.tbs-sct.canada.ca/tbsf-fsct/350-57-eng.asp\"><strong>Access to Information Request Form</strong></a>\", along with detailed instructions, is available on the Treasury Board of Canada Secretariat's website.</p>\n\n<p>You should send your requests to the following address:</p>\n\n<p>Access to Information and Privacy Office<br>\n<br>\n<br>\n<br>\n</p>\n\n<h2>Fees</h2>\n\n<p>When you submit a request under the <i>Access to Information Act</i>, you must also send a <strong>cheque or money order</strong></p>\n\n<ul>\n<li>in the amount of $5.00, and</li>\n<li>payable to the Receiver General for Canada.</li>\n</ul>\n\n<p>Payments can also be made by <strong>credit card</strong> by contacting the CFIA ATIP Office.</p>\n\n<p>This fee helps cover the administrative costs associated with processing your request.</p>\n\n<h2>Time required</h2>\n\n<p>In general, an access to information request takes <strong>30 calendar days</strong> from the time the CFIA receives the request to the release of the documents.</p>\n\n<p>However, an extension may be required</p>\n\n<ul>\n<li>to allow for consultation with third parties and other departments, or</li>\n<li>if the request is for a large number of records that need a lengthy search that interferes with the operation of the government institution.</li>\n</ul>\n\n<h2>For more information on access to information</h2>\n\n<p>The CFIA's Access to Information and Privacy (ATIP) Office manages access to information requests. You can <strong>contact</strong> the office</p>\n\n<ul>\n<li>by phone at 613-773-5555</li>\n<li>by email at <a href=\"mailto:ATIP-CFIA-AIPRP@inspection.gc.ca\" class=\"nowrap\">ATIP-CFIA-AIPRP@inspection.gc.ca</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Documents pouvant \u00eatre communiqu\u00e9s par le processus de demande d'acc\u00e8s \u00e0 l'information</h2>\n\n<p>La demande d'information faite au titre de la <a href=\"/francais/reg/jredirect2.shtml?aia\"><i>Loi sur l'acc\u00e8s \u00e0 l'information</i></a> peut porter sur tout mat\u00e9riel relevant de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, quel qu'en soit le format. Au nombre des <strong>documents pouvant \u00eatre communiqu\u00e9s</strong> en r\u00e9ponse \u00e0 une demande d'acc\u00e8s \u00e0 l'information, il y a\u00a0:</p>\n\n<ul>\n<li>les textes de correspondance;</li>\n<li>les demandes;</li>\n<li>les certificats;</li>\n<li>les rapports que l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> ne diffuse pas d'ordinaire (<abbr title=\"par exemple\">p. ex</abbr>, rapports pr\u00e9par\u00e9s par des experts-conseils).</li>\n</ul>\n\n<h2>Exceptions et exclusions pour les demandes d'acc\u00e8s \u00e0 l'information</h2>\n\n<p>En vertu de la <i>Loi sur l'acc\u00e8s \u00e0 l'information</i>, certains types <strong>d'\u00e9l\u00e9ments d'information ne peuvent \u00eatre diffus\u00e9s</strong>. On les appelle les \u00ab\u00a0exceptions\u00a0\u00bb et les \u00ab\u00a0exclusions\u00a0\u00bb.</p>\n\n<p>L'exception permet de prot\u00e9ger les \u00e9l\u00e9ments d'information de divers types dont la divulgation pourrait entra\u00eener un pr\u00e9judice.</p>\n\n<p><strong>L'exception</strong> est\u00a0:</p>\n\n<ul>\n<li>soit de nature obligatoire (l'institution <strong>est tenue</strong> de prot\u00e9ger l'information ou d'en refuser la divulgation);</li>\n<li>soit de nature discr\u00e9tionnaire (l'institution <strong>a le droit</strong> de refuser la divulgation de l'information demand\u00e9e).</li>\n</ul>\n\n<p>Voici des exemples d'exceptions\u00a0:</p>\n\n<ul>\n<li>les secrets industriels de tiers;</li>\n<li>les renseignements personnels;</li>\n<li>l'information sur la s\u00e9curit\u00e9 nationale.</li>\n</ul>\n\n<p>La <strong>description compl\u00e8te des exceptions</strong> se trouve dans les articles 13 \u00e0 23 de la <a href=\"/francais/reg/jredirect2.shtml?aia\"><i>Loi sur l'acc\u00e8s \u00e0 l'information</i></a>.</p>\n\n<p><strong>L'exclusion</strong> est l'\u00e9l\u00e9ment d'information non vis\u00e9 par la <i>Loi sur l'acc\u00e8s \u00e0 l'information</i>. Il s'agit de l'information contenue notamment\u00a0:</p>\n\n<ul>\n<li>dans les documents publi\u00e9s;</li>\n<li>dans les documents mis en vente;</li>\n<li>dans les documents de mus\u00e9e;</li>\n<li>dans les documents confidentiels du Conseil priv\u00e9 de la Reine.</li>\n</ul>\n\n<h2>Faire une demande d'acc\u00e8s \u00e0 l'information aupr\u00e8s de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></h2>\n\n<p>Utiliser le service de demande d'<abbr title=\"l'acc\u00e8s \u00e0 l'information et de protection des renseignements personnels\">AIPRP</abbr> en ligne est une fa\u00e7on pratique de soumettre les demandes facilement et rapidement, tout en simplifiant le processus de demande. Soumettez votre demande en ligne aujourd'hui, et gagnez du temps et \u00e9conomisez les frais postaux.</p>\n\n<p class=\"mrgn-lft-lg\"><img alt=\"souris d'ordinateur\" class=\"mrgn-bttm-0\" height=\"20\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/mouse_icon_1389725962310_fra.gif\" width=\"20\"><a href=\"https://atip-aiprp.tbs-sct.gc.ca/fr/Bienvenue/Confidentialite\">Demande en ligne</a></p>\n\n<p class=\"mrgn-lft-xl\"><strong>Ou</strong></p>\n\n<p>Il faut faire la demande <strong>par \u00e9crit</strong>. La demande doit contenir tous les d\u00e9tails n\u00e9cessaires pour que le personnel puisse trouver et extraire les renseignements demand\u00e9s.</p>\n\n<p>Le <a href=\"https://www.tbs-sct.canada.ca/tbsf-fsct/350-57-fra.asp\"><strong>Formulaire de demande d'acc\u00e8s \u00e0 l'information</strong></a>, accompagn\u00e9 d'instructions d\u00e9taill\u00e9es, se trouve sur le site Web du Secr\u00e9tariat du Conseil du Tr\u00e9sor du Canada.</p>\n\n<p>Il faut envoyer les demandes \u00e0 l'adresse suivante\u00a0:</p>\n\n<p>Bureau de l'acc\u00e8s \u00e0 l'information et de la protection des renseignements personnels<br>\n<br>\n<br>\n<br>\n</p>\n\n<h2>Frais</h2>\n\n<p>Lorsque vous soumettez une demande au titre de la <i>Loi sur l'acc\u00e8s \u00e0 l'information</i>, il faut inclure <strong>un ch\u00e8que ou un mandat-poste</strong>\u00a0:</p>\n\n<ul>\n<li>d'un montant de 5,00\u00a0$</li>\n<li>libell\u00e9 \u00e0 l'ordre du receveur g\u00e9n\u00e9ral du Canada.</li>\n</ul>\n\n<p>Vous pouvez \u00e9galement acquitter les frais par <strong>carte de cr\u00e9dit</strong> en communiquant avec le BAIPRP de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n\n<h2>Temps de r\u00e9ponse</h2>\n\n<p>L'Agence dispose habituellement de <strong>30 jours civils</strong> pour r\u00e9pondre \u00e0 la demande d'acc\u00e8s \u00e0 l'information, \u00e0 partir de la date de r\u00e9ception de la demande jusqu'au jour de communication des documents.</p>\n\n<p>Par contre, il peut y avoir prolongation du temps de r\u00e9ponse\u00a0:</p>\n\n<ul>\n<li>afin de permettre la consultation de tiers et d'autres minist\u00e8res ou organismes;</li>\n<li>si le grand nombre de documents demand\u00e9s n\u00e9cessite une longue recherche qui entraverait de fa\u00e7on s\u00e9rieuse le fonctionnement de l'institution.</li>\n</ul>\n\n<h2>Pour en savoir plus sur l'acc\u00e8s \u00e0 l'information</h2>\n\n<p>C'est le Bureau de l'acc\u00e8s \u00e0 l'information et de la protection des renseignements personnels (BAIPRP) de l'Agence qui s'occupe de la gestion des demandes d'acc\u00e8s \u00e0 l'information. Vous pouvez <strong>communiquer</strong> avec le BAIPRP\u00a0:</p>\n\n<ul>\n<li>par t\u00e9l\u00e9phone en composant le 613-773-5555;</li>\n<li>par courriel \u00e0 l'adresse suivante\u00a0: <a href=\"mailto:ATIP-CFIA-AIPRP@inspection.gc.ca\" class=\"nowrap\">ATIP-CFIA-AIPRP@inspection.gc.ca</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}