{
    "dcr_id": "1462308015175",
    "title": {
        "en": "Frequently Asked Questions (FAQs)<br />  Amendments to the <i>Introduced Forest Pest Compensation Regulations</i>",
        "fr": "Foire aux questions (FAQ)<br />  Modifications au <i>R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits</i>"
    },
    "html_modified": "4-5-2016",
    "modified": "26-5-2016",
    "issued": "4-5-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_pest_regs_1462308015175_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_pest_regs_1462308015175_fra"
    },
    "parent_ia_id": "1419029097256",
    "ia_id": "1462308066207",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Frequently Asked Questions (FAQs)<br />  Amendments to the <i>Introduced Forest Pest Compensation Regulations</i>",
        "fr": "Foire aux questions (FAQ)<br />  Modifications au <i>R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits</i>"
    },
    "label": {
        "en": "Frequently Asked Questions (FAQs)<br />  Amendments to the <i>Introduced Forest Pest Compensation Regulations</i>",
        "fr": "Foire aux questions (FAQ)<br />  Modifications au <i>R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1462308066207",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1419029096537",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/acts-and-regulations/list-of-acts-and-regulations/frequently-asked-questions/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/foire-aux-questions/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Frequently Asked Questions (FAQs)  Amendments to the Introduced Forest Pest Compensation Regulations",
            "fr": "Foire aux questions (FAQ)  Modifications au R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits"
        },
        "description": {
            "en": "The FAQs below are meant to provide Canadians and businesses with general information about changes to Introduced Forest Pest Compensation Regulations that came into effect on April 19, 2016.",
            "fr": "La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les modifications port\u00e9s au R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits qui est entr\u00e9 en vigueur le 19 avril 2016."
        },
        "keywords": {
            "en": "Frequently Asked Questions, FAQs, Introduced Forest Pest Compensation Regulations, Emerald Ash Borer, EAB, Brown Spruce Longhorn Beetle, BSLB, Asian longhorned beetle, ALHB",
            "fr": "Foire aux questions, FAQ, R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits, agrile du fr\u00eane, AF, longicorne brun de l'\u00e9pinette, LBE, longicorne asiatique, LA"
        },
        "dcterms.subject": {
            "en": "insects,plants,regulation",
            "fr": "insecte,plante,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Government of Canada,Canadian Food Inspection Agency"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-05-04",
            "fr": "2016-05-04"
        },
        "modified": {
            "en": "2016-05-26",
            "fr": "2016-05-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Frequently Asked Questions (FAQs) Amendments to the Introduced Forest Pest Compensation Regulations",
        "fr": "Foire aux questions (FAQ) Modifications au R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The <abbr title=\"Frequently Asked Questions\">FAQs</abbr> below are meant to provide Canadians and businesses with general information about changes to <i>Introduced Forest Pest Compensation Regulations</i> that came into effect on April 19, 2016. On that date, the regulations also formally became known as the <i>Asian Long-horned Beetle Compensation Regulations</i>.</p>\n<h2 class=\"h5\">What is the purpose of this regulation?</h2>\n<p>This regulation will allow compensation to be provided to property owners whose tree(s) have been ordered removed by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> between April 1, 2013 and March 31, 2019 to control and eliminate the Asian longhorned beetle (ALHB).</p>\n<p>The amendments also make the regulations consistent with the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s current strategy to manage the Emerald Ash Borer (EAB) and Brown Spruce Longhorn Beetle (BSLB).</p>\n<h2 class=\"h5\">What are the key elements of this regulation?</h2>\n<p>Property owners may receive a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Notice to Dispose for trees known to host <abbr title=\"Asian longhorned beetle\">ALHB</abbr> until March 31, 2019 and may submit their compensation claims for removing those trees no later than December 31, 2020.</p>\n<p>To apply to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for compensation, property owners must have:</p>\n<ul>\n<li>received a \"Notice to Dispose\" issued by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> between April 1, 2013 and \u00a0March 31, 2019, requiring the removal of one or more host trees on their property;</li>\n<li>had the host tree(s) removed; and</li>\n<li>purchased (a) non-host replacement tree(s) and planted it (them) on the same property.</li>\n</ul>\n<p>The regulation also repeals subsections 2(2) and 2(3) of the <i>Introduced Forest Pest Compensation Regulations</i> to no longer include eradication and its related compensation as a pest management strategy for <abbr title=\"Emerald Ash Borer\">EAB</abbr> and <abbr title=\"Brown Spruce Longhorn Beetle\">BSLB</abbr>.</p>\n<h2 class=\"h5\">How does this regulation affect Canadian businesses?</h2>\n<p>All property owners within the <abbr title=\"Asian longhorned beetle\">ALHB</abbr> regulated area, including businesses, must comply with movement restrictions of wood and wood products. Compensation may encourage the public to report signs of the <abbr title=\"Asian longhorned beetle\">ALHB</abbr>. Failure to eradicate <abbr title=\"Asian longhorned beetle\">ALHB</abbr> could have devastating effects on Canada's forests, and related industries, including the Canadian hardwood market.</p>\n<h2 class=\"h5\">What is the timeline for implementation?</h2>\n<p>The regulations came into force April 19, 2016.</p>\n<h2 class=\"h5\">Where can I get more information?</h2>\n<p>For more information on this regulation and/or the <a href=\"/plant-health/invasive-species/insects/asian-longhorned-beetle/eng/1337792721926/1337792820836\"><abbr title=\"Asian longhorned beetle\">ALHB</abbr></a> please visit the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> website.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les modifications port\u00e9s au <i>R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits</i> qui est entr\u00e9 en vigueur le 19 avril 2016. Lors de cette date, le r\u00e8glement est aussi devenu connu sous le nom du <i>R\u00e8glement sur l'indemnisation longicorne asiatique.</i></p>\n<h2 class=\"h5\">Quel est le but de ce r\u00e8glement?</h2>\n<p>Ce r\u00e8glement permettra l'octroi d'une indemnisation aux propri\u00e9taires dont les arbres ont \u00e9t\u00e9 vis\u00e9s par un arr\u00eat\u00e9 de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> concernant leur enl\u00e8vement entre le 1<sup>er</sup> avril 2013 et le 31 mars 2019 afin de contr\u00f4ler et d'\u00e9liminer le longicorne asiatique (LA).</p>\n<p>Les modifications sont \u00e9galement apport\u00e9es au r\u00e8glement conform\u00e9ment \u00e0 la strat\u00e9gie actuelle de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> afin de d\u00e9 g\u00e9rer l'agrile du fr\u00eane (AF) et le longicorne brun de l'\u00e9pinette (LBE).</p>\n<h2 class=\"h5\">Quels sont les \u00e9l\u00e9ments cl\u00e9s de ce r\u00e8glement?</h2>\n<p>Les propri\u00e9taires peuvent recevoir un avis d'\u00e9limination concernant des arbres connus pour \u00eatre des h\u00f4tes du longicorne asiatique jusqu'au 31 mars 2019 et peuvent pr\u00e9senter leurs demandes d'indemnisation concernant l'enl\u00e8vement de ces arbres au plus tard le 31\u00a0d\u00e9cembre\u00a02020.</p>\n<p>Pour pr\u00e9senter une demande d'indemnisation \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, les propri\u00e9taires doivent\u00a0:</p>\n<ul>\n<li>avoir re\u00e7u un \u00ab\u00a0avis d'\u00e9limination\u00a0\u00bb d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> entre le 1<sup>er</sup> avril 2013 et le 31 mars 2019, vous obligeant \u00e0 faire abattre les arbres h\u00f4tes sur votre propri\u00e9t\u00e9;</li>\n<li>avoir fait abattre les arbres h\u00f4tes;</li>\n<li>avoir achet\u00e9 a) des arbres de remplacement qui ne sont pas des h\u00f4tes et les avoir plant\u00e9s sur la m\u00eame propri\u00e9t\u00e9.</li>\n</ul>\n<p>Le r\u00e8glement abroge \u00e9galement les paragraphes 2(2) et 2(3) du <i>R\u00e8glement sur l'indemnisation relative aux parasites forestiers introduits </i>afin de ne plus comprendre l'\u00e9radication et l'indemnisation connexe \u00e0 titre de strat\u00e9gie de gestion des parasites concernant l'<abbr title=\"agrile du fr\u00eane\">AF</abbr> et le <abbr title=\"longicorne brun de l'\u00e9pinette\">LBE</abbr>.</p>\n<h2 class=\"h5\">Quels effets ce r\u00e8glement a-t-il sur les entreprises canadiennes?</h2>\n<p>Tous les propri\u00e9taires \u00e0 l'int\u00e9rieur de la zone r\u00e9glement\u00e9e concernant le longicorne asiatique, y compris les entreprises, doivent se conformer aux restrictions relatives au transport de bois et de produits du bois. L'indemnisation peut encourager le public \u00e0 signaler les signes de longicorne asiatique. Le d\u00e9faut d'\u00e9radiquer le longicorne asiatique pourrait avoir des effets d\u00e9vastateurs sur les for\u00eats du Canada, les industries connexes, y compris le march\u00e9 des feuillus canadien.</p>\n<h2 class=\"h5\">Quel est le calendrier de mise en \u0153uvre?</h2>\n<p>Le r\u00e8glement est entr\u00e9 en vigueur le 19 avril 2016.</p>\n<h2 class=\"h5\">O\u00f9 puis-je obtenir plus de renseignements?</h2>\n<p>Pour plus de renseignements sur ce r\u00e8glement et/ou le <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-asiatique/fra/1337792721926/1337792820836\">longicorne asiatique</a>, veuillez visiter le site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}