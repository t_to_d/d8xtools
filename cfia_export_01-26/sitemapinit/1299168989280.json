{
    "dcr_id": "1299168913252",
    "title": {
        "en": "Invasive species",
        "fr": "Esp\u00e8ces envahissantes"
    },
    "html_modified": "2024-01-26 2:20:24 PM",
    "modified": "2024-01-12",
    "issued": "2011-03-03 11:15:15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_pest_1299168913252_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_pest_1299168913252_fra"
    },
    "ia_id": "1299168989280",
    "parent_ia_id": "1299162708850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299162708850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Invasive species",
        "fr": "Esp\u00e8ces envahissantes"
    },
    "label": {
        "en": "Invasive species",
        "fr": "Esp\u00e8ces envahissantes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1299168989280",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299162629094",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Invasive species",
            "fr": "Esp\u00e8ces envahissantes"
        },
        "description": {
            "en": "Certain insects, plants, snails, slugs and micro-organisms can harm plants when they spread to new areas of the country.",
            "fr": "Certains insectes, v\u00e9g\u00e9taux, escargots, limaces et micro-organismes peuvent nuire aux v\u00e9g\u00e9taux lorsqu'ils se propagent dans de nouvelles r\u00e9gions du pays."
        },
        "keywords": {
            "en": "Invasive species, Invasive insects, Nematodes, snails and other, Plant diseases, Report invasive species, Invasive plants, Unsolicited seeds",
            "fr": "Esp\u00e8ces envahissantes, Insectes envahissants, N\u00e9matodes, escargots et autres, Maladies v\u00e9g\u00e9tales, Signaler une esp\u00e8ce envahissante, V\u00e9g\u00e9taux envahissants, Semences non sollicit\u00e9es"
        },
        "dcterms.subject": {
            "en": "exports,imports,insects,plant diseases,plants",
            "fr": "exportation,importation,insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-03-03 11:15:15",
            "fr": "2011-03-03 11:15:15"
        },
        "modified": {
            "en": "2024-01-12",
            "fr": "2024-01-12"
        },
        "type": {
            "en": "reference material,home page",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,page d'accueil"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Invasive species",
        "fr": "Esp\u00e8ces envahissantes"
    },
    "body": {
        "en": "        \r\n<div class=\"row profile\">\n<div class=\"col-md-6\">\n\n\n<p>Certain insects, plants, snails, slugs and micro-organisms can harm plants when they spread to new areas of the country.</p>\n\n<p>They can cause serious damage to Canada's economy and the environment when they invade farm land, forests, parks and other natural areas.</p>\n<section class=\"followus\">\n<h2>Follow:</h2>\n<ul>\n<li><a href=\"https://www.facebook.com/CFIACanada\" class=\"facebook\"><span class=\"wb-inv\">Facebook</span></a></li>\n<li><a href=\"https://twitter.com/InspectionCan\" class=\"twitter\" rel=\"external\"><span class=\"wb-inv\">Twitter</span></a></li>\n<li><a href=\"https://www.youtube.com/channel/UCaaeZ7z9HAK0-TGLBFGk4gw\" class=\"youtube\"><span class=\"wb-inv\">YouTube</span></a></li>\n<li><a href=\"https://www.linkedin.com/company/canadian-food-inspection-agency\" class=\"linkedin\" rel=\"external\"><span class=\"wb-inv\">LinkedIn</span></a></li>\n<li><a href=\"https://www.instagram.com/cfia_canada/\" class=\"instagram\" rel=\"external\"><span class=\"wb-inv\">Instagram</span></a></li>\n<li><a href=\"https://notification.inspection.canada.ca/\" class=\"email\" rel=\"external\"><span class=\"wb-inv\">Email subscription</span></a></li>\n</ul>\n</section>\n</div>\n\n<section class=\"col-sm-6 col-md-6 mrgn-tp-sm hidden-sm hidden-xs\">\n<div class=\"wb-tabs carousel-s2\">\n<ul role=\"tablist\">\n\n<li class=\"active\"><a href=\"#panel6\"><img alt=\"Check your RV, camper or trailer\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/carousel-check_rv_camper_trailer_for_invasive_pests_1694547602345_eng.jpg\"></a></li>\n\n<li><a href=\"#panel4\"><img alt=\"Don't let the spotted lanternfly in!\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_1_1629505528829_eng.jpg\"></a></li>\n\n<li><a href=\"#panel5\"><img alt=\"Stop the LDD Moth\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_2_1629505530704_eng.jpg\"></a></li>\n\n\n</ul>\n\n<div class=\"tabpanels\">\n\n<div class=\"in fade\" id=\"panel6\" role=\"tabpanel\">\n<figure><a href=\"/plant-health/invasive-species/stop-the-spread/tips-for-checking-your-rv/eng/1694461237260/1694461237885\"><img alt=\"Check your RV, camper or trailer\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/carousel-check_rv_camper_trailer_for_invasive_pests_1694547602345_eng.jpg\">\n<figcaption>\n<p>Check your RV, camper or trailer</p>\n</figcaption></a>\n</figure>\n</div>\n\n<div class=\"out fade\" id=\"panel4\" role=\"tabpanel\">\n<figure><a href=\"/plant-health/invasive-species/insects/spotted-lanternfly/spotted-lanternfly/eng/1433365581428/1433365581959\"><img alt=\"Don't let the spotted lanternfly in!\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_1_1629505528829_eng.jpg\">\n<figcaption>\n<p>Don't let the spotted lanternfly in!</p>\n</figcaption></a>\n</figure>\n</div>\n\n<div class=\"out fade\" id=\"panel5\" role=\"tabpanel\">\n<figure><a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/moving-across-canada/eng/1629692231648/1629692233085\"><img alt=\"Stop the LDD Moth\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_2_1629505530704_eng.jpg\">\n<figcaption>\n<p>Stop the LDD Moth</p>\n</figcaption></a>\n</figure>\n</div>\n\n</div>\n</div>\n</section>\n</div>\n\n<div class=\"row\">\n<div class=\"col-md-4 col-xs-12 pull-right\">\n<section class=\"lnkbx\">\n<h2>Most requested</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/stop-the-spread/eng/1655945133110/1655945931559\">Reporting plant pests</a></li>\n\n<li><a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/moving-across-canada/eng/1629692231648/1629692233085\">Moving across Canada: Don't take the LDD moth with you</a></li>\n\n<li><a href=\"https://canadainvasives.ca/programs/be-plant-wise/\">Plant Wise (Canadian Council on Invasive Species)</a></li>\n</ul>\n</section>\n\n<section class=\"lnkbx\">\n<h2>More information for</h2>\n\n<ul>\n<li><a href=\"/exporting-food-plants-or-animals/exports/eng/1299164931224/1299165527958\">Plant exporters</a></li>\n\n<li><a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/eng/1299168480001/1299168593866\">Plant importers</a></li>\n\n<li><a href=\"/plant-health/invasive-species/buying-selling-and-trading/eng/1537451230024/1537451230445\">Online buyers and sellers</a> <span class=\"nowrap\">(e-commerce)</span></li>\n</ul>\n</section>\n</div>\n\n<section class=\"gc-srvinfo col-md-8 pull-left\">\n<h2>Services and information</h2>\n\n<div class=\"wb-eqht row\">\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/insects/eng/1307077188885/1307078272806\">Invasive insects</a></h3>\n\n<p>Facts, photos, how to identify, pest cards, requirements.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/eng/1321563900456/1321564231938\">Nematodes, snails and other</a></h3>\n\n<p>Facts, requirements, Newfoundland and Labrador soil restrictions.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/plant-diseases/eng/1322807235798/1322807340310\">Plant diseases</a></h3>\n\n<p>Facts, photos, how to identify, pest cards, requirements.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/stop-the-spread/eng/1655945133110/1655945931559\">Report invasive species</a></h3>\n\n<p>Tell us if you think you've found an invasive plant pest.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/invasive-plants/eng/1306601411551/1306601522570\">Invasive plants</a></h3>\n\n<p>Facts, photos, requirements, field guides, reports, weeds, tools.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/seeds/disposing-of-unsolicited-seeds/eng/1624489903715/1624490577542\">Unsolicited seeds</a></h3>\n\n<p>What to do if you receive seeds you did not order.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/domestic-plant-protection-measures/eng/1629843699782/1629901542997\">Domestic plant protection measures</a></h3>\n\n<p>Information on CFIA regulations in Canada, requirements.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/plant-health/invasive-species/plant-import/invertebrates-and-micro-organisms/eng/1528728938166/1528729097278\">Importing and handling invertebrates and micro-organisms</a></h3>\n\n<p>Processes, authorizations, requirements.</p>\n</div>\n</div>\n</section>\n</div>\n\n<section class=\"gc-prtts\">\n<h2>Features</h2>\n\n<div class=\"row\">\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\"><a href=\"/plant-health/invasive-species/stop-the-spread/eng/1655945133110/1655945931559\">\n<figure>\n<figcaption>Stop the spread</figcaption>\n<img alt=\"Stop the spread\" class=\"img-responsive thumbnail mrgn-bttm-sm\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/stop_the_spread_censored_360x203_1656357519242_eng.png\">\n<p>Destroy and report invasive species to keep them out of Canada.</p>\n</figure></a>\n</div>\n\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/about-cfia/transparency/consultations-and-engagement/expanding-regulated-areas-for-box-tree-moth/eng/1704730654533/1704730655236\">\n<figure>\n<figcaption>Box tree moth consultation</figcaption>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/feature_boxtree_consultation_1705016689772_eng.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Share your thoughts about expanding the regulated area by February\u00a010\u00a02024.</p>\n</figure>\n</a>\n</div>\n\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\"><a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">\n<figure>\n<figcaption>Don't move firewood</figcaption>\n<img alt=\"Don't move firewood\" class=\"img-responsive thumbnail mrgn-bttm-sm\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_feature_2_1629505532297_eng.jpg\">\n<p>Invasive species can hide on and in firewood. Help protect our forests. Buy local. Burn local.</p>\n</figure></a>\n</div>\n</div>\n</section>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n<div class=\"row profile\">\n<div class=\"col-md-6\">\n\n\n<p>Certains insectes, v\u00e9g\u00e9taux, escargots, limaces et micro-organismes peuvent nuire aux v\u00e9g\u00e9taux lorsqu'ils se propagent dans de nouvelles r\u00e9gions du pays.</p>\n\n<p>Ils peuvent causer de graves dommages \u00e0 l'\u00e9conomie canadienne et \u00e0 l'environnement lorsqu'ils envahissent les terres agricoles, les for\u00eats, les parcs et d'autres zones naturelles.</p>\n<section class=\"followus\">\n<h2>Suivez\u00a0:</h2>\n<ul>\n<li><a href=\"https://www.facebook.com/ACIACanada\" class=\"facebook\"><span class=\"wb-inv\">Facebook</span></a></li>\n<li><a href=\"https://twitter.com/InspectionCanFR\" class=\"twitter\" rel=\"external\"><span class=\"wb-inv\">Twitter</span></a></li>\n<li><a href=\"https://www.youtube.com/channel/UCP36FKXgcCspS6bi-USKC5A\" class=\"youtube\"><span class=\"wb-inv\">YouTube</span></a></li>\n<li><a href=\"https://www.linkedin.com/company/canadian-food-inspection-agency\" class=\"linkedin\" rel=\"external\"><span class=\"wb-inv\">LinkedIn</span></a></li>\n<li><a href=\"https://www.instagram.com/acia_canada/\" class=\"instagram\" rel=\"external\"><span class=\"wb-inv\">Instagram</span></a></li>\n<li><a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\" class=\"email\" rel=\"external\"><span class=\"wb-inv\">Abonnement par courriel</span></a></li>\n</ul>\n</section>\n</div>\n\n<section class=\"col-sm-6 col-md-6 mrgn-tp-sm hidden-sm hidden-xs\">\n<div class=\"wb-tabs carousel-s2\">\n<ul role=\"tablist\">\n\n<li class=\"active\"><a href=\"#panel6\"><img alt=\"Inspectez votre VR, fourgonnette de camping ou roulotte\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/carousel-check_rv_camper_trailer_for_invasive_pests_1694547602345_fra.jpg\"></a></li>\n\n<li><a href=\"#panel4\"><img alt=\"Ne laissez pas entrer le fulgore tachet\u00e9!\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_1_1629505528829_fra.jpg\"></a></li>\n\n<li><a href=\"#panel5\"><img alt=\"Arr\u00eatez la spongieuse europ\u00e9enne\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_2_1629505530704_fra.jpg\"></a></li>\n\n</ul>\n\n<div class=\"tabpanels\">\n\n<div class=\"in fade\" id=\"panel6\" role=\"tabpanel\">\n<figure><a href=\"/protection-des-vegetaux/especes-envahissantes/arreter-la-propagation/conseils-pour-inspecter-votre-vr/fra/1694461237260/1694461237885\"><img alt=\"Inspectez votre VR, fourgonnette de camping ou roulotte\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/carousel-check_rv_camper_trailer_for_invasive_pests_1694547602345_fra.jpg\">\n<figcaption>\n<p>Inspectez votre VR, fourgonnette de camping ou roulotte</p>\n</figcaption></a>\n</figure>\n</div>\n\n\n<div class=\"out fade\" id=\"panel4\" role=\"tabpanel\">\n<figure><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/le-fulgore-tachete/le-fulgore-tachete/fra/1433365581428/1433365581959\"><img alt=\"Ne laissez pas entrer le fulgore tachet\u00e9!\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_1_1629505528829_fra.jpg\">\n<figcaption>\n<p>Ne laissez pas entrer le fulgore tachet\u00e9!</p>\n</figcaption></a>\n</figure>\n</div>\n\n<div class=\"out fade\" id=\"panel5\" role=\"tabpanel\">\n<figure><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/demenagement-au-canada/fra/1629692231648/1629692233085\"><img alt=\"Arr\u00eatez la spongieuse europ\u00e9enne\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_spash_2_1629505530704_fra.jpg\">\n<figcaption>\n<p>Arr\u00eatez la spongieuse europ\u00e9enne</p>\n</figcaption></a>\n</figure>\n</div>\n\n\n</div>\n</div>\n</section>\n</div>\n\n<div class=\"row\">\n<div class=\"col-md-4 col-xs-12 pull-right\">\n<section class=\"lnkbx\">\n<h2>En demande</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/arreter-la-propagation/fra/1655945133110/1655945931559\">D\u00e9claration des phytoravageurs</a></li>\n\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/demenagement-au-canada/fra/1629692231648/1629692233085\">D\u00e9m\u00e9nagement au Canada\u00a0: n'emportez pas la spongieuse nord-am\u00e9ricaine avec vous</a></li>\n\n<li><a href=\"https://canadainvasives.ca/programmes/sachez-reconnaitre-les-plantes/\">Sachez reconna\u00eetre les plantes (Conseil canadien sur les esp\u00e8ces envahissantes)</a></li>\n</ul>\n</section>\n\n<section class=\"lnkbx\">\n<h2>Autres renseignements pour les</h2>\n\n<ul>\n<li><a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportation-des-vegetaux/fra/1299164931224/1299165527958\">Exportateurs de v\u00e9g\u00e9taux</a></li>\n\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/fra/1299168480001/1299168593866\">Importateurs de v\u00e9g\u00e9taux</a></li>\n\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/achat-vente-et-commerce/fra/1537451230024/1537451230445\">Acheteurs et vendeurs en ligne</a> <span class=\"nowrap\">(Commerce \u00e9lectronique)</span></li>\n</ul>\n</section>\n</div>\n\n<section class=\"gc-srvinfo col-md-8 pull-left\">\n<h2>Services et renseignements</h2>\n\n<div class=\"wb-eqht row\">\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/fra/1307077188885/1307078272806\">Insectes envahissants</a></h3>\n\n<p>Faits, photos, comment d\u00e9tecter, cartes de phytoravageurs, exigences.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/fra/1321563900456/1321564231938\">N\u00e9matodes, escargots et autres</a></h3>\n\n<p>Faits, exigences, restrictions de sol frappant Terre-Neuve-et-Labrador.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/fra/1322807235798/1322807340310\">Maladies v\u00e9g\u00e9tales</a></h3>\n\n<p>Faits, photos, comment d\u00e9tecter, cartes de phytoravageurs, exigences.</p>\n\n\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/arreter-la-propagation/fra/1655945133110/1655945931559\">Signaler une esp\u00e8ce envahissante</a></h3>\n\n<p>Dites-nous si vous pensez avoir trouv\u00e9 un phytoravageur envahissant.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/fra/1306601411551/1306601522570\">V\u00e9g\u00e9taux envahissants</a></h3>\n\n<p>Faits, photos, exigences, guides des champs, rapports, mauvaises herbes, outils.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/semences/elimination-de-semences-non-sollicitees/fra/1624489903715/1624490577542\">Semences non sollicit\u00e9es</a></h3>\n\n<p>Que faire si vous avez re\u00e7u des semences que vous n'avez pas command\u00e9es.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/mesures-de-protection-des-vegetaux-en-territoire-c/fra/1629843699782/1629901542997\">Mesures de protection des v\u00e9g\u00e9taux en territoire canadien</a></h3>\n\n<p>Information sur la r\u00e8glementation de l'ACIA au Canada, exigences.</p>\n</div>\n\n<div class=\"col-md-6\">\n<h3><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/invertebres-et-de-microorganismes/fra/1528728938166/1528729097278\">Importation et manipulation d'invert\u00e9br\u00e9s et de microorganismes</a></h3>\n\n<p>Processus, autorisations, exigences.</p>\n</div>\n</div>\n</section>\n</div>\n\n<section class=\"gc-prtts\">\n<h2>En vedette</h2>\n\n<div class=\"row\">\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\"><a href=\"/protection-des-vegetaux/especes-envahissantes/arreter-la-propagation/fra/1655945133110/1655945931559\">\n<figure>\n<figcaption>Arr\u00eater la propagation</figcaption>\n<br>\n<img alt=\"Arr\u00eater la propagation\" class=\"img-responsive thumbnail mrgn-bttm-sm\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/stop_the_spread_censored_360x203_1656357519242_fra.png\">\n<p>D\u00e9truire et signaler les esp\u00e8ces envahissantes afin de les garder hors du Canada.</p>\n</figure></a>\n</div>\n\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/agrandir-les-zones-reglementees-pour-la-pyrale-du-/fra/1704730654533/1704730655236\">\n<figure>\n<figcaption>Consultation  sur la pyrale du buis</figcaption><br>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/feature_boxtree_consultation_1705016689772_fra.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Partagez vos commentaires sur l'agrandissement de la zone r\u00e9glement\u00e9e d'ici le 10\u00a0f\u00e9vrier\u00a02024</p>\n</figure>\n</a>\n</div>\n\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\"><a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">\n<figure>\n<figcaption>Ne d\u00e9placez pas le bois de chauffage</figcaption>\n<img alt=\"Ne d\u00e9placez pas le bois de chauffage\" class=\"img-responsive thumbnail mrgn-bttm-sm\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_feature_2_1629505532297_fra.jpg\">\n<p>Les esp\u00e8ces envahissantes peuvent se cacher sur et dans le bois de chauffage. Aidez \u00e0 prot\u00e9ger nos for\u00eats. Achetez local. Br\u00fblez local.</p>\n</figure></a>\n</div>\n</div>\n</section>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}