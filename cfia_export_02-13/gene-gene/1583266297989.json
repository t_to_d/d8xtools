{
    "dcr_id": "1583266297989",
    "title": {
        "en": "How do we use genomics in our research on plant pathogens?",
        "fr": "Comment utilisons-nous la g\u00e9nomique dans le cadre de nos recherches sur les agents phytopathog\u00e8nes?"
    },
    "html_modified": "2024-02-13 9:54:27 AM",
    "modified": "2019-08-28",
    "issued": "2020-03-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_chronicle_mar_2020_mr_dna_1583266297989_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_chronicle_mar_2020_mr_dna_1583266297989_fra"
    },
    "ia_id": "1583266298348",
    "parent_ia_id": "1565298312402",
    "chronicletopic": {
        "en": "Science and Innovation|Plant Health",
        "fr": "Science et Innovation|Sant\u00e9 des plantes"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Industry|Academia|Canadians",
        "fr": "Industrie|Acad\u00e9mie|Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565298312402",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "How do we use genomics in our research on plant pathogens?",
        "fr": "Comment utilisons-nous la g\u00e9nomique dans le cadre de nos recherches sur les agents phytopathog\u00e8nes?"
    },
    "label": {
        "en": "How do we use genomics in our research on plant pathogens?",
        "fr": "Comment utilisons-nous la g\u00e9nomique dans le cadre de nos recherches sur les agents phytopathog\u00e8nes?"
    },
    "templatetype": "content page 1 column",
    "node_id": "1583266298348",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298312168",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/science-and-innovation/how-do-we-use-genomics-in-our-research-on-plant-pa/",
        "fr": "/inspecter-et-proteger/science-et-innovation/comment-utilisons-nous-la-genomique-dans-le-cadre-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "How do we use genomics in our research on plant pathogens?",
            "fr": "Comment utilisons-nous la g\u00e9nomique dans le cadre de nos recherches sur les agents phytopathog\u00e8nes?"
        },
        "description": {
            "en": "My kids always ask me, Dad what is your job? My response is simple, I work with DNA. I use DNA to identify diseases affecting plants.",
            "fr": "Mes enfants me demandent toujours, \u00ab papa, c'est quoi ton m\u00e9tier? \u00bb Ma r\u00e9ponse est simple : \u00ab Je travaille avec l'ADN. J'utilise l'ADN pour identifier les maladies des plantes. \u00bb"
        },
        "keywords": {
            "en": "The CFIA Chronicle, C360, genomics, research, plant pathogens, August 2019",
            "fr": "Les chroniques de l'ACIA, C360, g\u00e9nomique, recherches, agents phytopathog\u00e8nes, Ao\u00fbt 2019"
        },
        "dcterms.subject": {
            "en": "food inspection,food safety,government information,government publications,regulation,sciences,scientific research",
            "fr": "inspection des aliments,salubrit\u00e9 des aliments,information gouvernementale,publication gouvernementale,r\u00e9glementation,sciences,recherche scientifique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernment du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-03-06",
            "fr": "2020-03-06"
        },
        "modified": {
            "en": "2019-08-28",
            "fr": "2019-08-28"
        },
        "type": {
            "en": "news publication,reference material",
            "fr": "publication d'information\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "How do we use genomics in our research on plant pathogens?",
        "fr": "Comment utilisons-nous la g\u00e9nomique dans le cadre de nos recherches sur les agents phytopathog\u00e8nes?"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<p>By Guillaume Bilodeau and Emily Giroux</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img alt=\"Mr DNA.\" class=\"img-thumbnail img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_chronicle_mar_2020_mr_dna_600x600_1583259989263_eng.jpg\">\n</div>\n\n\n<p>This blog post was originally published to <a href=\"http://science.gc.ca/eic/site/063.nsf/eng/h_97679.html\">Cultivating Science</a> on science.gc.ca.</p>\n\n\n<blockquote>\n<p>My kids always ask me, \"Dad what is your job? What do you do at work?\"</p>\n\n<p>My response is simple, \"I work with DNA. I use DNA to identify diseases affecting plants.\"</p>\n\n<p>\"DNA as in Mr DNA in the Jurassic Park movie?!\" they ask.</p>\n\n<p>\"Yes, like Mr DNA. I try to find differences in the DNA of organisms so I can develop markers to help us detect particular plant disease-causing fungi.\"</p>\n</blockquote>\n\n\n<p><a href=\"http://science.gc.ca/eic/site/063.nsf/eng/97894.html\">Read the full blog post</a>.</p>\n\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<p>Par Guillaume Bilodeau et Emily Giroux</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img alt=\"Monsieur ADN.\" class=\"img-thumbnail img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_chronicle_mar_2020_mr_dna_600x600_1583259989263_fra.jpg\">\n</div>\n\n\n<p>Ce billet de blogue est d'abord paru dans <a href=\"http://science.gc.ca/eic/site/063.nsf/fra/h_97679.html\">Cultiver la science</a> sur science.gc.ca.</p>\n\n\n<blockquote>\n<p>Mes enfants me demandent toujours, \u00ab\u00a0papa, c'est quoi ton m\u00e9tier? Qu'est-ce que tu fais quand tu travailles?\u00a0\u00bb</p>\n\n<p>Ma r\u00e9ponse est simple\u00a0: \u00ab\u00a0Je travaille avec l'ADN. J'utilise l'ADN pour identifier les maladies des plantes.\u00a0\u00bb</p>\n\n<p>Alors, ils me demandent \u00ab\u00a0ADN comme monsieur ADN dans le film Parc jurassique?!\u00a0\u00bb</p>\n\n<p>\u00ab\u00a0Oui, comme monsieur ADN. J'essaie de trouver des diff\u00e9rences dans l'ADN d'organismes pour pouvoir cr\u00e9er des marqueurs qui nous aideront \u00e0 d\u00e9tecter certains champignons pathog\u00e8nes pr\u00e9cis.\u00a0\u00bb</p>\n</blockquote>\n\n\n<p><a href=\"http://science.gc.ca/eic/site/063.nsf/fra/97894.html\">Lisez le billet de blogue complet</a>.</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}