{
    "dcr_id": "1584933171585",
    "title": {
        "en": "T-4-<span class='wb-invisible'> </span>128\u00a0\u2013 Customer formula fertilizers",
        "fr": "T-4-<span class='wb-invisible'> </span>128\u00a0\u2013 Engrais pr\u00e9par\u00e9s selon la formule du client"
    },
    "html_modified": "13-11-2020",
    "modified": "2-11-2023",
    "issued": "23-3-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/fert_tmemo_4-128_customer_formula_ferts_1584933171585_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/fert_tmemo_4-128_customer_formula_ferts_1584933171585_fra"
    },
    "parent_ia_id": "1299873786929",
    "ia_id": "1584933171975",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "T-4-<span class='wb-invisible'> </span>128\u00a0\u2013 Customer formula fertilizers",
        "fr": "T-4-<span class='wb-invisible'> </span>128\u00a0\u2013 Engrais pr\u00e9par\u00e9s selon la formule du client"
    },
    "label": {
        "en": "T-4-<span class='wb-invisible'> </span>128\u00a0\u2013 Customer formula fertilizers",
        "fr": "T-4-<span class='wb-invisible'> </span>128\u00a0\u2013 Engrais pr\u00e9par\u00e9s selon la formule du client"
    },
    "templatetype": "content page 1 column",
    "node_id": "1584933171975",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1299873703612",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/trade-memoranda/t-4-128/",
        "fr": "/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-128/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "T-4- 128\u00a0\u2013 Customer formula fertilizers",
            "fr": "T-4- 128\u00a0\u2013 Engrais pr\u00e9par\u00e9s selon la formule du client"
        },
        "description": {
            "en": "The purpose of this document is to outline regulatory requirements (including exemptions from registration) for customer formula fertilizers regulated under the Fertilizers Act and Regulations.",
            "fr": "L'objet du pr\u00e9sent document est de d\u00e9crire les exigences r\u00e9glementaires (incluant les exemptions d'enregistrement) pour les engrais pr\u00e9par\u00e9s selon la formule du client en vertu de la Loi sur les engrais et son r\u00e8glement d'application."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizers Regulations, fertilizers, supplements, trade memoranda, t-memo, labelling, import, registration, requirements, growing, media, containing fertilizers or supplements, T-4-128, Customer formula fertilizers",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, engrais, suppl\u00e9ments, circulaire \u00e0 la profession, circulaire, s\u00e9curit\u00e9, importation, enregistrement, exigences, relatives, milieux, culture, contenant des engrais ou des suppl\u00e9ments, T-4-128, Engrais pr\u00e9par\u00e9s selon la formule du client"
        },
        "dcterms.subject": {
            "en": "imports,inspection,plants,policy,regulation",
            "fr": "importation,inspection,plante,politique,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-11-13",
            "fr": "2020-11-13"
        },
        "modified": {
            "en": "2023-11-02",
            "fr": "2023-11-02"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "T-4- 128\u00a0\u2013 Customer formula fertilizers",
        "fr": "T-4- 128\u00a0\u2013 Engrais pr\u00e9par\u00e9s selon la formule du client"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The 3 year regulatory transition period (October 26, 2020 to October 26, 2023) has now ended. As a result, regulated parties, including all manufacturers, importers, distributors and sellers of fertilizers and supplements must adhere to the amended <a href=\"/english/reg/jredirect2.shtml?ferengr\"><i>Fertilizers Regulations</i></a>. There are few notable exceptions for some product categories. Learn more about the <a href=\"/plant-health/fertilizers/notices-to-industry/2023-05-03/eng/1682633127754/1682633128598\">implementation of the amended <i>Fertilizers Regulations</i></a>.</p>\n</div>\n\n<h2>1. Purpose</h2>\n\n<p>The purpose of this document is to outline regulatory requirements (including exemptions from registration) for customer formula fertilizers regulated under the <i>Fertilizers Act</i> and Regulations.</p>\n\n<h2>2. Exemption from registration</h2>\n\n<div class=\"well pull-right col-sm-4 mrgn-lft-md mrgn-bttm-md\">\n<p><strong>Customer formula fertilizer</strong> means a fertilizer prepared in accordance with a written formula that sets out the name, amount and guaranteed analysis of each ingredient and the signature of the person for whose use for fertilizing purposes it has been prepared;</p>\n</div>\n\n<p>\"Customer formula fertilizers\" (see adjacent text box) are exempt from registration under the <i>Fertilizers Act</i>. To satisfy the conditions of the exemption, a customer formula fertilizer must:</p>\n\n<ul>\n<li>be prepared in response to a written request from the person who will use the product for fertilizing purposes\u00a0\u2013 it cannot be prepared in advance of that request in anticipation of a demand existing within the Canadian marketplace; and</li>\n<li>not contain seeds or growing media.</li>\n</ul>\n\n<p>A customer formula fertilizer that contains a pesticide registered under the <i>Pest Control Products Act</i> (PCPA) for the purpose of the fertilizer mixture, a micronutrient, or both, is exempt from registration. In the case of a customer formula fertilizer that contains a pesticide, the pesticide must comply with the requirements of the PCPA in respect of its approved use (including application rate, target crop, etc.).</p>\n\n<p>Under the <i>Fertilizers Act</i>, a fertilizer  that contains a supplement is regulated as a fertilizer. When a mixture of fertilizer(s) and  supplement(s) meets the definition of a customer formula fertilizer, it is  exempt from registration. A customer formula fertilizer may only contain supplements that are  either:</p>\n\n<ol class=\"lst-lwr-alph\">\n<li>exempt from registration; or</li>\n<li>registered for use in fertilizer mixtures and labelled  with directions for use that are consistent with those of the customer formula  fertilizer.</li>\n</ol>\n\n<h2>3. Standards</h2>\n\n<p>All customer-formula fertilizers must adhere to the safety standards outlined in <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-93/eng/1305611387327/1305611547479\">Trade memorandum T-4-93</a> with respect to the product's composition and contaminant levels.</p>\n\n<h2>4. Labelling requirements</h2>\n\n<p>General labelling requirements as well as labelling requirements specific to customer formula fertilizers with and without intentionally incorporated micronutrients or pesticides are provided in <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-130/eng/1604424185581/1604424268008\">Trade memorandum T-4-130</a>.</p>\n\n<h2>5. Contact information</h2>\n\n<p>Fertilizer Safety Section<br>Canadian Food Inspection Agency<br>Phone: 1-855-212-7695<br>Email: <a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\" class=\"nowrap\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n<p>La p\u00e9riode de transition r\u00e9glementaire de 3 ans (26 octobre 2020 au 26 octobre 2023) est termin\u00e9e. Par cons\u00e9quent, les parties r\u00e9glement\u00e9es, y compris tous les fabricants, importateurs, distributeurs et vendeurs d'engrais et de suppl\u00e9ments, doivent adh\u00e9rer au <a href=\"/francais/reg/jredirect2.shtml?ferengr\"><i>R\u00e8glement sur les engrais</i></a> modifi\u00e9. Il existe quelques exceptions notables pour certaines cat\u00e9gories de produits. Apprenez-en davantage sur la <a href=\"/protection-des-vegetaux/engrais/avis-a-l-industrie/2023-05-03/fra/1682633127754/1682633128598\">mise en \u0153uvre de la modification du <i>R\u00e8glement sur les engrais</i></a>.</p>\n</div>\n\n<h2>1. Objet</h2>\n\n<p>L'objet du pr\u00e9sent document est de d\u00e9crire les exigences r\u00e9glementaires (incluant les exemptions d'enregistrement) pour les engrais pr\u00e9par\u00e9s selon la formule du client en vertu de la <i>Loi sur les engrais</i> et son r\u00e8glement d'application.</p>\n\n<h2>2. Exemption d'enregistrement</h2>\n\n<div class=\"well col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<p>Un <strong>engrais pr\u00e9par\u00e9 selon la formule du client</strong> d\u00e9signe un engrais pr\u00e9par\u00e9 selon une formule \u00e9crite qui pr\u00e9cise le nom, la quantit\u00e9 et l'analyse garantie de chaque composant actif et qui porte la signature de la personne \u00e0 l'usage de laquelle il a \u00e9t\u00e9 pr\u00e9par\u00e9.</p>\n</div>\n\n<p>Les \u00ab\u00a0engrais pr\u00e9par\u00e9s selon la formule du client\u00a0\u00bb (voir zone de texte ci-contre) sont exempts d'enregistrement en vertu de la <i>Loi sur les engrais</i>. Pour respecter les conditions de l'exemption, l'engrais pr\u00e9par\u00e9 selon la formule du client\u00a0:</p>\n\n<ul>\n<li>doit \u00eatre pr\u00e9par\u00e9 \u00e0 la suite d'une demande \u00e9crite de la personne qui utilisera le produit \u00e0 des fins de fertilisation\u00a0\u2013 il ne peut pas \u00eatre pr\u00e9par\u00e9 avant que la demande ait \u00e9t\u00e9 re\u00e7ue pour anticiper une demande existante sur le march\u00e9 canadien; et</li>\n<li>ne doit contenir ni semences ni milieu de culture.</li>\n</ul>\n\n<p>Un engrais pr\u00e9par\u00e9 selon la formule du client qui contient soit un antiparasitaire enregistr\u00e9 en vertu de la <i>Loi sur les produits antiparasitaires</i> (LPA) pour \u00eatre utilis\u00e9 dans les m\u00e9langes d'engrais, ou soit un oligo-\u00e9l\u00e9ment ou les deux est exempt d'enregistrement. Dans le cas d'un engrais pr\u00e9par\u00e9 selon la formule du client qui contient un antiparasitaire, cet antiparasitaire doit \u00eatre conforme aux exigences de la LPA en ce qui concerne son utilisation approuv\u00e9e (incluant le taux d'application, la culture cible, etc.).</p>\n\n<p>En vertu de la <i>Loi sur les engrais</i>, un engrais contenant un suppl\u00e9ment est r\u00e9glement\u00e9 comme un engrais. Lorsqu'un m\u00e9lange d'engrais et de suppl\u00e9ments r\u00e9pond \u00e0 la d\u00e9finition d'un engrais pr\u00e9par\u00e9 selon la formule du client, il est exempt\u00e9 d'enregistrement. Un engrais pr\u00e9par\u00e9 selon la formule du client ne peut contenir que des suppl\u00e9ments qui sont\u00a0:</p>\n\n<ol class=\"lst-lwr-alph\">\n<li>exempt d'enregistrement; ou</li>\n<li>enregistr\u00e9 pour l'utilisation dans les m\u00e9langes d'engrais et \u00e9tiquet\u00e9 avec un mode d'emploi conforme \u00e0 l'engrais pr\u00e9par\u00e9 selon la formule du client.</li>\n</ol>\n\n<h2>3. Normes</h2>\n\n<p>Tous les engrais pr\u00e9par\u00e9s selon la formule du client doivent respecter les normes \u00e9nonc\u00e9es dans la <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-93/fra/1305611387327/1305611547479\">Circulaire \u00e0 la profession T-4-93</a> en ce qui concerne le contenu du produit et les concentrations de contaminants.</p>\n\n<h2>4. Exigences relatives \u00e0 l'\u00e9tiquetage</h2>\n\n<p>Les exigences g\u00e9n\u00e9rales en mati\u00e8re d'\u00e9tiquetage ainsi que les exigences en mati\u00e8re d'\u00e9tiquetage propres aux engrais pr\u00e9par\u00e9s selon la formule du client avec ou sans ajout intentionnel d'oligo-\u00e9l\u00e9ments ou d'antiparasitaires sont d\u00e9crites dans la <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-130/fra/1604424185581/1604424268008\">circulaire \u00e0 la profession T-4-130</a>.</p>\n\n<h2>5. Coordonn\u00e9es</h2>\n\n<p>Section de l'innocuit\u00e9 des engrais<br>Agence canadienne d'inspection des aliments<br>T\u00e9l\u00e9phone\u00a0: 1-855-212-7695<br>Courriel\u00a0: <a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\" class=\"nowrap\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}