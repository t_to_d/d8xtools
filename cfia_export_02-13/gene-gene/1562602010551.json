{
    "dcr_id": "1562602010551",
    "title": {
        "en": "Creating a preventive control plan under the <i>Safe Food for Canadians Regulations</i> (video)",
        "fr": "Cr\u00e9er un plan de contr\u00f4le pr\u00e9ventif en vertu du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>"
    },
    "html_modified": "2024-02-13 9:54:00 AM",
    "modified": "2020-07-02",
    "issued": "2019-07-10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/preventive_control_plan_1562602010551_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/preventive_control_plan_1562602010551_fra"
    },
    "ia_id": "1562602139451",
    "parent_ia_id": "1431014369144",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1431014369144",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Creating a preventive control plan under the <i>Safe Food for Canadians Regulations</i> (video)",
        "fr": "Cr\u00e9er un plan de contr\u00f4le pr\u00e9ventif en vertu du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>"
    },
    "label": {
        "en": "Creating a preventive control plan under the <i>Safe Food for Canadians Regulations</i> (video)",
        "fr": "Cr\u00e9er un plan de contr\u00f4le pr\u00e9ventif en vertu du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1562602139451",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1431014368582",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-industry/toolkit-for-food-businesses/developing-your-pcp/creating-a-preventive-control-plan/",
        "fr": "/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/elaborer-un-pcp/creer-un-plan-de-controle-preventif/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Creating a preventive control plan under the Safe Food for Canadians Regulations (video)",
            "fr": "Cr\u00e9er un plan de contr\u00f4le pr\u00e9ventif en vertu du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
        },
        "description": {
            "en": "A Preventive Control Plan demonstrates how hazards and risks to food are addressed. In this video, we look at how a business would prepare a preventive control plan.",
            "fr": "Un plan de contr\u00f4le pr\u00e9ventif d\u00e9montre comment les dangers et les risques pour les aliments sont abord\u00e9s. Dans cette vid\u00e9o, nous regardons comment une entreprise ferait pour pr\u00e9parer un plan de contr\u00f4le pr\u00e9ventif."
        },
        "keywords": {
            "en": "Safe Food, SFCR, Food Safety, Preventive Control Plan, PCP, Food Regulations",
            "fr": "Aliments salubres, RSAC, PCP, R\u00e9glementation sur les aliments"
        },
        "dcterms.subject": {
            "en": "communications,food inspection,food safety,government information,government publications,regulations",
            "fr": "communications,inspection des aliments,salubrit\u00e9 des aliments,information gouvernementale,publication gouvernementale,r\u00e9glementations"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-07-10",
            "fr": "2019-07-10"
        },
        "modified": {
            "en": "2020-07-02",
            "fr": "2020-07-02"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Creating a preventive control plan under the Safe Food for Canadians Regulations (video)",
        "fr": "Cr\u00e9er un plan de contr\u00f4le pr\u00e9ventif en vertu du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>A Preventive Control Plan demonstrates how hazards and risks to food are addressed. In this video, we look at how a business would prepare a preventive control plan.</p>\n\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://youtu.be/rOW4woHff5M\"}'>\n<video title=\"Traceability requirements under the SFCR\">\n<source type=\"video/youtube\" src=\"https://youtu.be/rOW4woHff5M\">\n</source></video>\n\n<figcaption>\n<details id=\"inline-transcript\">\n<summary>Creating a Preventive Control Plan (PCP) under SFCR\u00a0\u2013\u00a0Transcript</summary>\n\n<p class=\"mrgn-tp-md\">[Anna's pasta sauce factory is in front of a mountainous landscape. Anna is standing in front of the factory holding a tablet in one hand and her pasta sauce in the other hand.]</p>\n\n<p>Anna owns a pasta sauce business. Under the <i>Safe Food for Canadians Regulations</i> she needs a Preventive Control Plan.</p>\n<p>[Text on screen: <i>Safe Food for Canadians Regulations</i>]</p>\n<p>[Text on screen: Preventive Control Plan (PCP)]</p>\n<p>[The background changes to a family of six that is sitting around a dining table with salad and pasta on the table.]</p>\n<p>This plan is a written document that outlines how a business makes sure that the food it makes is safe and fit for us to eat.</p>\n<p>[Text on screen: Technical knowledge and scientific knowledge]</p>\n<p>To develop this plan, Anna needs:</p>\n<ul>\n<li>Technical knowledge of how her pasta sauce is made</li>\n<li>Scientific knowledge of food safety</li>\n</ul>\n<p>[Anna is holding a tablet and hands it to one of her staff members. Another staff member appears and she hands the tablet to her.]</p>\n<p>Anna can develop it herself or have her employees do it. She can also bring in outside experts if she or her team lack the time or expertise to develop one.</p>\n<p>[Text on screen: Key things to remember]</p>\n<p>Here are the key things Anna will need to do when developing her PCP.</p>\n<p>[Text on screen: Hazard analysis]</p>\n<p>[Images of biological, chemical, and physical hazard awareness signs appear. A picture of tomato, garlic and basil leaves appear below on the left of the screen and a picture of the factory making the pasta sauce appears on the left.</p>\n<p>Anna will need to identify any biological, chemical, or physical hazards that would pose a risk to the safety of her pasta sauce.</p>\n<p>[The background changes to images of pathogens that fill the screen.]</p>\n<p>She would do this by considering the hazards associated with each ingredient and each step involved in making the sauce.</p>\n<p>[Anna's twenty eight pasta sauce jars are all labelled and lined up in a refrigerator.]</p>\n<p>For example, Anna knows that pathogens can grow in the sauce during the storage step if the pH is not acidic enough, so she writes this in her hazard analysis.</p>\n<p>[Text on screen: Control the hazards]</p>\n<p>Anna will need to describe the control measures she will use so that any hazard she has identified will be reduced to a safe level.</p>\n<p>[Images of biological, chemical, and physical hazard awareness signs appear. The images rotate over to an image of a checkmark after each hazard is mentioned.]</p>\n<p>[Text on screen: Evidence]</p>\n<p>She will need evidence that these control measures are effective.</p>\n<p>[Anna's pasta sauce appears in a sauce pot, with a jar of vinegar next to it.]</p>\n<p>For example, Anna adds vinegar to the pasta sauce to acidify it.</p>\n<p>[The background changes to images of pathogens that fill the screen.]</p>\n<p>She has proof that acidification will prevent the growth of any pathogens in her pasta sauce during storage.</p>\n<p>[Text on screen: Critical control points]</p>\n<p>[Anna's tablet appears on the left with a list of various steps with hazard images labelled next to them in the middle of the tablet screen.]</p>\n<p>Anna will need to determine whether any of the steps she takes are critical to the product's safety, and if so, she will identify these as critical control points in her plan.</p>\n<p>[Anna's tablet appears on the left again and the word \"step 1\" is bolded.]</p>\n<p>[The titles \"Minimum cooking temperature, Monitor and corrective action\" all appear one by one on the tablet.]</p>\n<p>For each critical control point, she will describe what criteria must be met for the food to be safe (such as a minimum cooking temperature), how she will monitor that this is met, and what corrective action she will take if this is not met.</p>\n<p>[The background changes to an overview of the pasta sauce in the pot, and a thermometer hanging in it on the right side.]</p>\n<p>For example, Anna monitors and records the time and temperature of the cooking step to make sure that any pathogens in the sauce are reduced to a safe level. If the time or temperature is not met, Anna would cook the sauce again or dispose of it.</p>\n<p>[Anna's pasta sauce in the pot gets poured out to the right side of the page.]</p>\n<p>[Text on screen: Verify the PCP is working]</p>\n<p>Anna will need to write a procedure for how she will make sure that her plan has been followed as written and results in a safe product.</p>\n<p>[Anna's tablet appears on the right, showing an image of her pasta sauce in the centre of the tablet with the title \"Procedure\" labelled above it.]</p>\n<p>[The background changes to Anna's pasta sauce that appears at the bottom of the page and one of her staff's arm reaches in with a pH test strip that dips into the top of the pasta sauce jar. When the pH strip is lifted out, the colour appears to be yellow on the strip and the pH number \"4.6\" appears on the left side of the jar.]</p>\n<p>For example, Anna's procedure includes testing the pH of the pasta sauce to make sure that it is below 4.6.</p>\n<p>[Text on screen: Consumer protection measures]</p>\n<p>[A family of six sits around a dining table enjoying their pasta meal. The screen zooms into the pasta on the table.</p>\n<p>[The background changes to Anna's pasta sauce jar that appears on the left side and the titles \"Labelling\" and \"Net quantity\" appear on the right side of the pasta sauce jar.]</p>\n<p>In addition to making sure her product is safe, Anna's plan will also need to describe how she will make sure she is meeting consumer protection requirements for things like labelling and net quantity.</p>\n<p>[The background changes to Anna's pasta sauce jar and gets weighed on a scale in the middle of the screen.]</p>\n<p>For example, Anna weighs a jar from every batch to make sure that the volume declared on the label matches what is in the jar.</p>\n<p>[Text on screen: Implement the PCP and keep it updated]</p>\n<p>[Three of Anna's staff appear on the bottom left side of the screen, and an image of a tablet appears in the middle with the title \"Preventive control plan\" labelled in the centre of the tablet and the word \"2 years\" pops up on the right hand side.]</p>\n<p>[The tablet then rotates into a notepad image, and then rotates back into a tablet image.]</p>\n<p>Anna needs to put her plan into practice by making sure her team is well trained, by following the plan as written, and by keeping records of preventive control plan-related activities for at least two years. Anna can keep her plan as a printed document in her facility or electronically.</p>\n<p>[A calendar appears with various circled dates in red.]</p>\n<p>[A picture of tomato, garlic and basil leaves appear on the right of the screen and a picture of the facility machine washing the tomatoes appears below.]</p>\n<p>It is important that Anna maintain her plan by checking it periodically to make sure it is up to date and revising it as needed. She also needs to update her plan whenever there is a problem or something changes, like a new ingredient or a new piece of equipment is introduced.</p>\n<p>Depending on your product, you may not need a written PCP if your business makes less than $100 thousand dollars per year; however, you will need to ensure preventive controls are in place.</p>\n<p>[Text on screen: For more information: inspection.gc.ca/SafeFood]</p>\n<p>For more information about the Safe Food for Canadian Regulations and keeping a traceability record, visit inspection.gc.ca/SafeFood.</p>\n<p>[End of video]</p>\n</details>\n\n</figcaption>\n</figure>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n<p>Un plan de contr\u00f4le pr\u00e9ventif d\u00e9montre comment les dangers et les risques pour les aliments sont abord\u00e9s. Dans cette vid\u00e9o, nous regardons comment une entreprise ferait pour pr\u00e9parer un plan de contr\u00f4le pr\u00e9ventif.</p>\n\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://youtu.be/IW6aiWU4w7Y\"}'>\n<video title=\"Exigences en mati\u00e8re de tra\u00e7abilit\u00e9 en vertu du RSAC\">\n<source type=\"video/youtube\" src=\"https://youtu.be/IW6aiWU4w7Y\">\n</source></video>\n\n<figcaption>\n<details id=\"inline-transcript\">\n<summary>Cr\u00e9er un plan de contr\u00f4le pr\u00e9ventif (PCP) en vertu du RSAC\u00a0\u2013 Transcription</summary>\n\n<p class=\"mrgn-tp-md\">[L'usine de fabrication de sauce pour p\u00e2tes d'Anna est situ\u00e9e devant un paysage montagneux. Anna se tient devant l'usine, avec une tablette dans une main et un pot de sauce pour p\u00e2tes dans l'autre.]</p>\n\n<p>Anna est propri\u00e9taire d'une entreprise de fabrication de sauce pour p\u00e2tes. Conform\u00e9ment au <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>, elle doit \u00e9tablir un <i>Plan de contr\u00f4le pr\u00e9ventif</i> (PCP).</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>]</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: Le Plan de contr\u00f4le pr\u00e9ventif (PCP)]</p>\n<p>[Une famille de six personnes assises autour d'une table appara\u00eet \u00e0 l'\u00e9cran. Une salade et des p\u00e2tes sont pos\u00e9es sur la table.]</p>\n<p>Le PCP est un document \u00e9crit qui explique en d\u00e9tail comment une entreprise s'assure d'offrir aux consommateurs des aliments salubres et propres \u00e0 la consommation.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: Les connaissances techniques et scientifiques]</p>\n<p>Pour r\u00e9diger ce plan, Anna doit poss\u00e9der\u00a0:</p>\n<ul>\n<li>une connaissance technique de la fa\u00e7on dont sa sauce est pr\u00e9par\u00e9e;</li>\n<li>une connaissance scientifique de la salubrit\u00e9 alimentaire.</li>\n</ul>\n<p>[Anna a une tablette dans les mains, qu'elle tend \u00e0 l'un de ses employ\u00e9s. Une autre employ\u00e9e appara\u00eet \u00e0 l'\u00e9cran et Anna lui remet la tablette \u00e0 son tour.]</p>\n<p>Elle peut s'en charger elle-m\u00eame ou confier la t\u00e2che \u00e0 son personnel. Elle peut \u00e9galement faire appel \u00e0 des experts externes si elle ou son \u00e9quipe n'ont pas le temps ou l'expertise n\u00e9cessaires \u00e0 la r\u00e9daction d'un tel document.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: Les principaux \u00e9l\u00e9ments \u00e0 garder en t\u00eate.]</p>\n<p>Voici les principaux \u00e9l\u00e9ments qu'Anna doit examiner lors de l'\u00e9laboration de son PCP.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: L'analyse des dangers]</p>\n<p>[Des panneaux de mise en garde contre les dangers biologiques, chimiques ou physiques apparaissent \u00e0 l'\u00e9cran. Une photo repr\u00e9sentant une tomate, une gousse d'ail et des feuilles de basilic appara\u00eet sous les panneaux, sur la gauche de l'\u00e9cran. La photo de l'usine de fabrication de sauce pour p\u00e2tes appara\u00eet sur la gauche.]</p>\n<p>Anna doit d\u00e9terminer les dangers biologiques, chimiques ou physiques qui risquent de compromettre la salubrit\u00e9 de sa sauce pour p\u00e2tes.</p>\n<p>[L'arri\u00e8re-plan change et des images d'agents pathog\u00e8nes remplissent l'\u00e9cran.]</p>\n<p>Pour ce faire, elle doit prendre en compte les dangers associ\u00e9s \u00e0 chacun des ingr\u00e9dients qu'elle utilise ainsi qu'\u00e0 chacune des \u00e9tapes de production de la sauce.</p>\n<p>[Vingt-huit pots de sauce pour p\u00e2tes sont \u00e9tiquet\u00e9s et align\u00e9s dans un r\u00e9frig\u00e9rateur.]</p>\n<p>Par exemple, Anna sait que des agents pathog\u00e8nes peuvent se multiplier dans la sauce si, durant l'entreposage, le pH est trop \u00e9lev\u00e9. Elle note donc cette information dans son analyse des dangers.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: La ma\u00eetrise des dangers]</p>\n<p>Anna doit d\u00e9crire les mesures de contr\u00f4le qu'elle mettra en place pour que les dangers pr\u00e9alablement d\u00e9crits soient r\u00e9duits \u00e0 un niveau s\u00fbr.</p>\n<p>[Des panneaux de mise en garde contre les dangers biologiques, chimiques ou physiques apparaissent \u00e0 l'\u00e9cran. \u00c0 mesure que les dangers sont \u00e9nonc\u00e9s, les panneaux font une rotation et sont remplac\u00e9s par l'image d'un crochet.]</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: La preuve]</p>\n<p>Elle devra aussi faire la preuve que ses mesures de contr\u00f4les sont efficaces.</p>\n<p>[Une marmite de sauce pour p\u00e2tes appara\u00eet \u00e0 l'\u00e9cran. Un contenant de vinaigre est pos\u00e9 \u00e0 proximit\u00e9.]</p>\n<p>Par exemple, Anna ajoute du vinaigre dans sa sauce pour p\u00e2tes afin d'abaisser le pH sous 4,6.</p>\n<p>[L'arri\u00e8re-plan change et des images d'agents pathog\u00e8nes remplissent l'\u00e9cran.]</p>\n<p>Elle d\u00e9tient des preuves montrant que le fait de maintenir un pH de 4,6 ou moins pr\u00e9vient la multiplication d'agents pathog\u00e8nes dans sa sauce lors de l'entreposage.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: Les points critiques \u00e0 ma\u00eetriser]</p>\n<p>[La tablette d'Anna appara\u00eet sur la gauche de l'\u00e9cran. On y aper\u00e7oit une liste dont les diverses \u00e9tapes sont assorties d'images de dangers s'affichant au milieu de l'\u00e9cran de la tablette.]</p>\n<p>Anna doit d\u00e9terminer si l'une ou l'autre des \u00e9tapes de production sont d\u00e9terminantes pour la salubrit\u00e9 du produit. Si c'est le cas, elle doit indiquer ces \u00e9tapes dans son PCP comme \u00e9tant des points critiques \u00e0 ma\u00eetriser.</p>\n<p>[La tablette d'Anna appara\u00eet \u00e0 nouveau sur la gauche de l'\u00e9cran. On y voit l'\u00e9nonc\u00e9 \u00ab\u00a0\u00c9tape\u00a01\u00a0\u00bb en gras.]</p>\n<p>[Les \u00e9nonc\u00e9s \u00ab\u00a0Temp\u00e9rature de cuisson minimale\u00a0\u00bb, \u00ab\u00a0Mesures de suivi\u00a0\u00bb et \u00ab\u00a0Mesures correctives\u00a0\u00bb apparaissent un \u00e0 un sur l'\u00e9cran de la tablette.]</p>\n<p>Pour chacun des points critiques \u00e0 ma\u00eetriser, elle doit d\u00e9crire les limites critiques, la fa\u00e7on dont elle s'assurera<strong> </strong>que ces limites sont respect\u00e9es et les mesures correctives qui seront mises en place si elles ne le sont pas.</p>\n<p>[L'arri\u00e8re-plan change et on voit une marmite de sauce pour p\u00e2tes. Un thermom\u00e8tre est accroch\u00e9 du c\u00f4t\u00e9 int\u00e9rieur droit de la marmite.]</p>\n<p>Par exemple, Anna surveille et inscrit le temps et la temp\u00e9rature de cuisson afin que la pr\u00e9sence \u00e9ventuelle d'agents pathog\u00e8nes dans la sauce soit r\u00e9duite \u00e0 un niveau s\u00fbr. Si le temps ou la temp\u00e9rature de cuisson prescrits ne sont pas respect\u00e9s, Anna devra faire cuire la sauce de nouveau ou la jeter.</p>\n<p>[Du c\u00f4t\u00e9 droit de l'\u00e9cran, on voit la sauce contenue dans la marmite qui est d\u00e9vers\u00e9e.]</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: La v\u00e9rification de l'efficacit\u00e9 du PCP]</p>\n<p>Anna doit d\u00e9finir une proc\u00e9dure expliquant de quelle fa\u00e7on elle v\u00e9rifiera que son PCP est respect\u00e9 \u00e0 la lettre et qu'il permet la production de produits salubres.</p>\n<p>[La tablette d'Anna appara\u00eet sur la droite de l'\u00e9cran. Au centre de l'\u00e9cran de la tablette, on aper\u00e7oit sa sauce pour p\u00e2tes. La mention \u00ab\u00a0Proc\u00e9dure\u00a0\u00bb est affich\u00e9e au-dessus de la sauce.]</p>\n<p>[L'arri\u00e8re-plan change et un pot de sauce pour p\u00e2tes appara\u00eet au bas de l'\u00e9cran. On aper\u00e7oit la main d'un des employ\u00e9s d'Anna qui plonge l'extr\u00e9mit\u00e9 d'une bandelette d'analyse du pH dans le pot de sauce pour p\u00e2tes. Lorsque la bandelette ressort du pot, elle semble \u00eatre jaune, et le nombre 4,6 (indiquant le pH) appara\u00eet \u00e0 la gauche du pot.]</p>\n<p>Par exemple, la proc\u00e9dure d'Anna comprend un test de pH de la sauce pour p\u00e2tes dont le r\u00e9sultat doit \u00eatre inf\u00e9rieur \u00e0 4,6.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: Les mesures de protection des consommateurs]</p>\n<p>[Six membres d'une famille sont assis autour d'une table et d\u00e9gustent un plat de p\u00e2tes. Le plan se rapproche des p\u00e2tes qui sont sur la table.]</p>\n<p>[L'arri\u00e8re-plan change et un pot de sauce pour p\u00e2tes appara\u00eet sur la gauche. Les mentions \u00ab\u00a0\u00c9tiquetage\u00a0\u00bb et \u00ab\u00a0Quantit\u00e9 nette\u00a0\u00bb apparaissent \u00e0 droite du pot de sauce.]</p>\n<p>En plus de garantir que le produit est s\u00fbr, le PCP d'Anna doit expliquer comment elle s'assurera de satisfaire aux exigences de protection des consommateurs, notamment en ce qui concerne l'\u00e9tiquetage et la quantit\u00e9 nette.</p>\n<p>[L'arri\u00e8re-plan change et un pot de sauce pour p\u00e2tes appara\u00eet \u00e0 l'\u00e9cran. Il est pes\u00e9 sur une balance positionn\u00e9e au milieu de l'\u00e9cran.]</p>\n<p>Par exemple, Anna v\u00e9rifie un contenant tir\u00e9 de chaque lot pour confirmer que le volume inscrit sur l'\u00e9tiquette correspond bel et bien au contenu.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: L'application et la mise \u00e0 jour du PCP]</p>\n<p>[Trois employ\u00e9s d'Anna apparaissent en bas \u00e0 gauche de l'\u00e9cran. Une tablette appara\u00eet ensuite au centre de l'\u00e9cran. Au centre de l'\u00e9cran de celle-ci, on peut lire la mention \u00ab\u00a0Plan de contr\u00f4le pr\u00e9ventif\u00a0\u00bb, puis l'\u00e9nonc\u00e9 \u00ab\u00a02 ans\u00a0\u00bb s'affiche du c\u00f4t\u00e9 droit.]</p>\n<p>[L'image de la tablette fait une rotation et se transforme en image d'un bloc-notes. Cette nouvelle image fait une rotation \u00e0 son tour et redevient une image de tablette.]</p>\n<p>Anna doit mettre son PCP en pratique en s'assurant que son personnel est bien form\u00e9, en suivant \u00e0 la lettre le contenu du PCP et en conservant la trace pendant au moins deux (2) ans de toutes les activit\u00e9s relatives au PCP. Elle peut en garder une version imprim\u00e9e dans son \u00e9tablissement ou le conserver au moyen d'un programme informatique.</p>\n<p>[Un calendrier appara\u00eet \u00e0 l'\u00e9cran. Certaines dates sont encercl\u00e9es en rouge.]</p>\n<p>[Une photo repr\u00e9sentant une tomate, une gousse d'ail et des feuilles de basilic appara\u00eet sur la droite de l'\u00e9cran. Un appareil nettoyant les tomates dans l'usine appara\u00eet au-dessous.]</p>\n<p>Il est essentiel qu'Anna revoie r\u00e9guli\u00e8rement son PCP afin de s'assurer qu'il est \u00e0 jour, et elle doit le modifier au besoin. Elle doit \u00e9galement l'actualiser si un probl\u00e8me survient ou quelque chose change, comme l'utilisation d'un nouvel ingr\u00e9dient ou l'introduction de nouvel \u00e9quipement.</p>\n<p>En fonction du produit que vous fabriquez, il se peut qu'un PCP \u00e9crit ne soit pas requis si les revenus annuels de votre entreprise sont inf\u00e9rieurs \u00e0 100\u00a0000\u00a0$. Vous devrez n\u00e9anmoins vous assurer que des mesures de contr\u00f4le pr\u00e9ventif sont en place.</p>\n<p>[Texte \u00e0 l'\u00e9cran\u00a0: Pour en savoir plus\u00a0: http://www.inspection.gc.ca/aliments/fra/1299092387033/1299093490225]</p>\n<p>Pour en savoir plus sur le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> et sur la fa\u00e7on de tenir les registres de tra\u00e7abilit\u00e9, visitez le http://www.inspection.gc.ca/aliments/fra/1299092387033/1299093490225.</p>\n<p>[Fin de la vid\u00e9o]</p>\n</details>\n\n</figcaption>\n</figure>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "success": true,
    "chat_wizard": true
}