{
    "dcr_id": "1553782504366",
    "title": {
        "en": "How to use the AIRS Verification Service",
        "fr": "Comment utiliser le Service de v\u00e9rification du SARI"
    },
    "html_modified": "2024-01-26 2:24:37 PM",
    "modified": "2019-03-28",
    "issued": "2019-03-28",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_imports_avs_faq_1553782504366_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_imports_avs_faq_1553782504366_fra"
    },
    "ia_id": "1553782504620",
    "parent_ia_id": "1553708673301",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1553708673301",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "How to use the AIRS Verification Service",
        "fr": "Comment utiliser le Service de v\u00e9rification du SARI"
    },
    "label": {
        "en": "How to use the AIRS Verification Service",
        "fr": "Comment utiliser le Service de v\u00e9rification du SARI"
    },
    "templatetype": "content page 1 column",
    "node_id": "1553782504620",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1553708673052",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/airs/avs/airs-verification-service/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/sari/svs/service-de-verification-du-sari/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "How to use the AIRS Verification Service",
            "fr": "Comment utiliser le Service de v\u00e9rification du SARI"
        },
        "description": {
            "en": "How to use the AIRS validation service",
            "fr": "Comment utiliser le service de validation du SARI"
        },
        "keywords": {
            "en": "imports, commercial imports, AIRS Validation Service, AVS, Frequently Asked Questions, FAQ",
            "fr": "importation, importateurs commerciaux, Service de validation de SARI, AVS, SVS Foire aux questions, FAQ"
        },
        "dcterms.subject": {
            "en": "imports,inspection",
            "fr": "importation,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Operations Strategy and Delivery",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Strat\u00e9gie et prestation des Op\u00e9rations"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-03-28",
            "fr": "2019-03-28"
        },
        "modified": {
            "en": "2019-03-28",
            "fr": "2019-03-28"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "How to use the AIRS Verification Service",
        "fr": "Comment utiliser le Service de v\u00e9rification du SARI"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<h2>On this page</h2>\n\n<ul class=\"list-unstyled mrgn-lft-0 mrgn-bttm-lg\">\n<li><a href=\"#a1\">Is there a fee associated with AIRS Verification Service?</a></li>\n<li><a href=\"#a2\">What is a \"web key\" and how do I get one?</a></li>\n<li><a href=\"#a3\">Does a web key expire?</a></li>\n<li><a href=\"#a4\">What if I lose my web key or what if my web key has been compromised?</a></li>\n<li><a href=\"#a5\">When is AVS available?</a></li>\n<li><a href=\"#a6\">How do I send information to AVS?</a></li>\n<li><a href=\"#a7\">How many lines can AVS process in one transaction?</a></li>\n<li><a href=\"#a8\">Why can AVS only process 400\u00a0lines when EDI processes 999\u00a0lines?</a></li>\n<li><a href=\"#a9\">Why has CFIA provided AVS and not an AIRS data dump?</a></li>\n<li><a href=\"#a10\">If I use AVS do I still need to use AIRS?</a></li>\n</ul>\n\n\n\n<div class=\"mrgn-bttm-lg\" id=\"eg-tog1\">\n<div class=\"btn-group mrgn-tp-md mrgn-bttm-md\">\n<button type=\"button\" class=\"btn btn-default wb-toggle\" data-toggle='{\"selector\": \"details\", \"parent\": \"#eg-tog1\", \"print\": \"on\", \"type\": \"on\"}'>Expand All</button>\n<button type=\"button\" class=\"btn btn-default wb-toggle\" data-toggle='{\"selector\": \"details\", \"parent\": \"#eg-tog1\", \"type\": \"off\"}'>Collapse All</button>\n</div>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a1\">Is there a fee  associated with AIRS Verification Service (AVS)?</h2>\n</summary>\n\n<p>AVS is a free service provided to industry by CFIA.  Although this is a free service, all AVS users must be registered with CFIA and  a web key is required to gain access the service.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a2\">What is a \"web key\" and how do I get one?</h2>\n</summary>\n\n<p>A web key is your unique identifier and acts as your access key to AVS. Web keys should not be shared with anyone. Web keys are obtained by contacting the CFIA AVS primary contact.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a3\">Does a web key expire?</h2>\n</summary>\n\n<p>No. A web key does not expire; it can, however, be disabled by CFIA.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a4\">What if I lose my web key or what if my web key has been compromised?</h2>\n</summary>\n\n<p>If you misplace your web key, or if your web key has been compromised, please contact the CFIA AVS contact immediately to deactivate your old web key and to activate a new web key for your brokerage.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a5\">When is AVS available?</h2>\n</summary>\n\n<p>AVS officially launched on <span class=\"nowrap\">January 4, 2010</span>. As with CFIA's AIRS Query tool, AVS is available 24\u00a0hours a day, 7\u00a0days a week.<br>\n<a href=\"/eng/1297964599443/1297965645317\">www.inspection.gc.ca</a>\u00a0*</p>\n \n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a6\">How do I send information to AVS?</h2>\n</summary>\n<p>AVS uses XML formatting to exchange information. The specifications, including format for transmission, have been outlined in the technical specifications document which is available by contacting the CFIA AVS primary contact.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a7\">How many lines can AVS process in one transaction?</h2>\n</summary>\n\n<p>AVS is programmed to accept up to 400\u00a0lines per transaction.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a8\">Why can AVS only process 400\u00a0lines when EDI processes 999\u00a0lines?</h2>\n</summary>\n\n<p>AVS cannot produce response without timing out beyond 400\u00a0lines. In order to maintain optimum performance, the limit on an AVS submission must remain at 400\u00a0commodities or less.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a9\">Why has CFIA provided AVS and not an AIRS data dump?</h2>\n</summary>\n\n<p>Due to the frequency of changes to AIRS requirements, importers/brokers would be required to constantly download updates. There is an insurmountable number of possible combinations in AIRS and the size of the file for an AIRS data dump would be in excess of 5,106,636,534\u00a0kb. It would be necessary to re-download this file after every AIRS publication and therefore was deemed unfeasible at this time.</p>\n\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a10\">If I use AVS do I still need to use AIRS?</h2>\n</summary>\n\n<p>Yes. AIRS houses CFIA import requirements whereas AVS will only validate codes sent in an XML message. AVS will determine which line in a transaction is missing, or which line contains invalid information. AIRS will provide the most up to date codes necessary for an EDI transaction. It is strongly suggested that AIRS be consulted in order to determine CFIA import</p>\n\n</details>\n\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<h2>Sur cette page</h2>\n\n<ul class=\"list-unstyled mrgn-lft-0 mrgn-bttm-lg\">\n<li><a href=\"#a1\">Est-ce qu'il y a un frais associ\u00e9 avec le Service de v\u00e9rification du SARI (SVS)?</a></li>\n<li><a href=\"#a2\">Qu'est-ce qu'une \"cl\u00e9 Web\" et comment en obtenir une?</a></li>\n<li><a href=\"#a3\">Est-ce que la cl\u00e9 Web cesse d'\u00eatre en vigueur?</a></li>\n<li><a href=\"#a4\">Que dois-je faire si je perds ma cl\u00e9 Web ou si ma cl\u00e9 Web est compromise?</a></li>\n<li><a href=\"#a5\">Quand est-ce que le SVS sera disponible?</a></li>\n<li><a href=\"#a6\">Comment est-ce que j'envoies l'information au SVS?</a></li>\n<li><a href=\"#a7\">Combien de lignes est-ce que le SVS peut traiter dans une transaction?</a></li>\n<li><a href=\"#a8\">Pourquoi est-ce que le SVS traite seulement 400\u00a0lignes alors que l'EDI traite 999\u00a0lignes?</a></li>\n<li><a href=\"#a9\">Pourquoi est-ce que l'ACIA offre le SVS et non un transfert complet de donn\u00e9es du SARI?</a></li>\n<li><a href=\"#a10\">Si j'utilise le SVS, est-ce que je dois encore utiliser le SARI?</a></li>\n</ul>\n\n\n\n<div class=\"mrgn-bttm-lg\" id=\"eg-tog1\">\n<div class=\"btn-group mrgn-tp-md mrgn-bttm-md\">\n<button class=\"btn btn-default wb-toggle\" type=\"button\" data-toggle='{\"selector\": \"details\", \"parent\": \"#eg-tog1\", \"print\": \"on\", \"type\": \"on\"}'>Afficher tout</button>\n<button class=\"btn btn-default wb-toggle\" type=\"button\" data-toggle='{\"selector\": \"details\", \"parent\": \"#eg-tog1\", \"type\": \"off\"}'>R\u00e9duire tout</button>\n</div>\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a1\">Est-ce qu'il y a un frais associ\u00e9 avec le Service de v\u00e9rification du SARI (SVS)?</h2>\n</summary>\n<p>Le SVS est un service qui est offert gratuitement par l'ACIA. M\u00eame si le service est gratuit, tous les utilisateurs du SVS doivent \u00eatre enregistr\u00e9s avec l'ACIA et une cl\u00e9 Web est n\u00e9cessaire pour obtenir l'acc\u00e8s.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a2\">Qu'est-ce qu'une \"cl\u00e9 Web\" et comment en obtenir une?</h2>\n</summary>\n<p>La cl\u00e9 Web est votre identificateur unique et agit comme votre cl\u00e9 d'acc\u00e8s au SVS. La cl\u00e9 Web ne doit pas \u00eatre partag\u00e9e. Veuillez communiquer avec le premier point de contact du SVS de l'ACIA pour obtenir votre cl\u00e9 Web.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a3\">Est-ce que la cl\u00e9 Web cesse d'\u00eatre en vigueur?</h2>\n</summary>\n<p>Non. La cl\u00e9 Web n'expire pas; cependant elle peut \u00eatre d\u00e9sactiv\u00e9e par l'ACIA.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a4\">Que dois-je faire si je perds ma cl\u00e9 Web ou si ma cl\u00e9 Web est compromise?</h2>\n</summary>\n<p>Si vous perdez votre cl\u00e9 Web ou si elle est compromise, Veuillez communiquer avec le premier point de contact du SVS de l'ACIA imm\u00e9diatement pour la d\u00e9sactiver et pour en activer une nouvelle pour votre bureau de courtier.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a5\">Quand est-ce que le SVS sera disponible?</h2>\n</summary>\n<p>Le SVS a \u00e9t\u00e9 lanc\u00e9 officiellement le <span class=\"nowrap\">4 janvier 2010</span>. Comme l'outil de recherche du SARI de l'ACIA, le SVS est disponible 24\u00a0heures par jour, 7\u00a0jours par semaine.<br>\n<a href=\"/eng/1297964599443/1297965645317\">www.inspection.gc.ca</a>\u00a0*</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a6\">Comment est-ce que j'envoies l'information au SVS?</h2>\n</summary>\n<p>Le SVS utilise un format en XML pour l'\u00e9change d'information. Les sp\u00e9cifications, y compris le format pour la transmission de donn\u00e9es, sont d\u00e9crites dans le document des sp\u00e9cifications techniques. Pour obtenir ce document, Veuillez communiquer avec le premier point de contact du SVS de l'ACIA.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a7\">Combien de lignes est-ce que le SVS peut traiter dans une transaction?</h2>\n</summary>\n<p>Le SVS peut accepter un maximum de 400\u00a0lignes par transaction.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a8\">Pourquoi est-ce que le SVS traite seulement 400\u00a0lignes alors que l'EDI traite 999\u00a0lignes?</h2>\n</summary>\n<p>Le SVS ne peut produire une r\u00e9ponse instantan\u00e9e au del\u00e0 de 400\u00a0lignes sans temporiser. Afin de garder une performance optimale, le SVS doit rester \u00e0 400\u00a0commodit\u00e9s ou moins.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a9\">Pourquoi est-ce que l'ACIA offre le SVS et non un transfert complet de donn\u00e9es du SARI?</h2>\n</summary>\n\n<p>Due \u00e0 la fr\u00e9quence des modifications aux exigences du SARI, les importateurs/courtiers auraient constamment besoin de transf\u00e9rer les mises \u00e0 jours. Il y a un nombre insurmontable de combinaisons possibles dans le SARI et la taille du fichier pour un transfert complet de donn\u00e9es serait de plus de 5,106,636,534\u00a0ko. Il serait n\u00e9cessaire de transf\u00e9rer ce fichier apr\u00e8s chaque publication du SARI. Il a donc \u00e9t\u00e9 d\u00e9termin\u00e9 que ceci n'\u00e9tait pas possible en ce moment.</p>\n</details>\n\n\n\n<details>\n<summary class=\"well well-sm\">\n<h2 class=\"h5\" id=\"a10\">Si j'utilise le SVS, est-ce que je dois encore utiliser le SARI?</h2>\n</summary>\n<p>Oui. Les conditions d'importation de l'ACIA se retrouvent dans le SARI. Le SVS va seulement valider les codes envoy\u00e9s par message XML. Le SVS d\u00e9terminera quelle ligne de transaction est manquante ou quelle ligne contient de l'information invalide. Le SARI donnera les codes les plus courants qui sont n\u00e9cessaires pour une transaction par EDI. Il est donc fortement sugg\u00e9r\u00e9 de consulter le SARI afin de d\u00e9terminer les conditions d'importation de l'ACIA.</p>\n</details>\n\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}