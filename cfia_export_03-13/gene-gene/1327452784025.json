{
    "dcr_id": "1327452784025",
    "title": {
        "en": "Potato mop-top virus (PMTV) - Fact Sheet",
        "fr": "Virus du sommet touffu de la pomme de terre (VSTPT) - Fiche de renseignements"
    },
    "html_modified": "2024-03-12 3:55:33 PM",
    "modified": "2014-02-20",
    "issued": "2012-01-25 22:26:21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_pmtv_factsheet_1327452784025_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_pmtv_factsheet_1327452784025_fra"
    },
    "ia_id": "1327452899956",
    "parent_ia_id": "1327447959911",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1327447959911",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Potato mop-top virus (PMTV) - Fact Sheet",
        "fr": "Virus du sommet touffu de la pomme de terre (VSTPT) - Fiche de renseignements"
    },
    "label": {
        "en": "Potato mop-top virus (PMTV) - Fact Sheet",
        "fr": "Virus du sommet touffu de la pomme de terre (VSTPT) - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1327452899956",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1327447877833",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/pmtv/fact-sheet/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/vstpt/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Potato mop-top virus (PMTV) - Fact Sheet",
            "fr": "Virus du sommet touffu de la pomme de terre (VSTPT) - Fiche de renseignements"
        },
        "description": {
            "en": "Potato mop-top virus (PMTV) causes an economically important disease of potato. Serious yield and quality reductions can occur in some cultivars.",
            "fr": "Le virus du sommet touffu de la pomme de terre (PSTPT) cause une maladie d'importance \u00e9conomique chez les pommes de terre. Des baisses importantes de rendement et de qualit\u00e9 peuvent \u00eatre observ\u00e9es chez certains cultivars."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, sampling, Potato mop-top virus, PMTV, Potato, virus, fungi, mop-top, spraing",
            "fr": "phytoravageurs, enqu&#234;tes phytosanitaires, phytoparasites justiciables de quarantaine, Virus du sommet touffu de la pomme de terre, la pomme de terre, pomme de terre, virus, myc&#232;tes, pr&#233;l&#232;vement"
        },
        "dcterms.subject": {
            "en": "insects,inspection,plant diseases,plants",
            "fr": "insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health Science Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des sciences de la sant\u00e9 des v\u00e9g\u00e9taux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-25 22:26:21",
            "fr": "2012-01-25 22:26:21"
        },
        "modified": {
            "en": "2014-02-20",
            "fr": "2014-02-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Potato mop-top virus (PMTV) - Fact Sheet",
        "fr": "Virus du sommet touffu de la pomme de terre (VSTPT) - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Background</h2>\n<p>Potato mop-top virus (PMTV) causes an economically important disease of potato. Serious yield and quality reductions can occur in some cultivars. The powdery scab fungus (<i lang=\"la\">Spongospora subterranea</i>) is a soil-borne organism and is the only known vector of potato mop-top virus. The distribution of <i lang=\"la\"><abbr title=\"Spongospora\">S.</abbr> subterranea</i> in Canada is sporadic, but is more common in coastal areas where the cooler weather and wetter soils favour its development.</p>\n<h2>Distribution</h2>\n<ul>\n<li>Asia</li>\n<li>Northern Europe</li>\n<li>North America</li>\n<li>South America</li>\n</ul>\n<h2>Biology</h2>\n<p><abbr title=\"Potato mop-top virus\">PMTV</abbr> survives in the soil within dormant resting spores of its fungal vector <i lang=\"la\"><abbr title=\"Spongospora\">S.</abbr> subterranea</i> . These infected viable resting spores can persist for up to 18 years in the soil.</p>\n<p>High levels of soil moisture and cool soil temperatures (12-20\u00b0<abbr title=\"Celsius\">C</abbr>) stimulate resting spores of the fungus to germinate and release virus-carrying zoospores. Zoospores can move only very short distances in soil and require free water for movement to occur. Zoospores introduce the virus into the potato plant when they infect the roots, stolons and/or young tubers. Systemic movement of the virus within the plant is generally slow and erratic. The critical period for infection and development of powdery scab on tubers is early in the growth cycle, at stolon formation and tuber set, a period that lasts about 3-4 weeks. Tubers which have matured beyond this period are resistant to infection by zoospores. Little or no spread occurs in areas where soil temperatures are above 20\u00b0<abbr title=\"Celsius\">C</abbr>, or where moisture is lacking. Since roots can be infected by <i lang=\"la\"><abbr title=\"Spongospora\">S.</abbr> subterranea</i> , even cultivars noted for some resistance to powdery scab may still be infected with <abbr title=\"Potato mop-top virus\">PMTV</abbr>.</p>\n<p>When <abbr title=\"Potato mop-top virus\">PMTV</abbr>-infected tubers are planted as seed, the virus is passed on as a secondary infection to only limited numbers of progeny tubers (30 - 50%). Therefore, spread via the obligate vector, powdery scab, is the most important means of transmission.</p>\n<h2>Symptoms On Potato</h2>\n<h3>Shoots and foliage</h3>\n<p>Symptoms may occur on the foliage of plants produced from infected tubers, and on stems, although not all stems produced will show symptoms. Foliage symptoms develop best at temperatures between 5 and 15\u00b0<abbr title=\"Celsius\">C</abbr>, particularly during the early stages of plant growth. The most common symptom is the development of aucuba patterns on the stems which consist of bright yellow blotches and ring or line patterns on lower or middle leaves. Less commonly, a second type of symptom may be observed, consisting of pale, V-shaped, chlorotic chevrons, usually on the leaflets of young upper leaves, and ultimately resulting in a distinct mosaic in the upper leaves. A third type of symptom consists of extreme shortening of internodes accompanied by crowding or bunching of foliage, described as a <strong>\"mop-top\"</strong>. Some of the smaller leaves may have wavy or rolled margins and the overall effect is a dwarfed and bunched growth habit.</p>\n<h3>Tubers</h3>\n<p>Although tubers are sometimes free of symptoms, in other cases raised, superficial concentric rings of 1 - 5 <abbr title=\"centimetres\">cm</abbr> in diameter are present on the surface, radiating out from the point where the fungal vector first introduced the virus. Tubers may also develop brown necrotic lines, arcs and rings in the flesh, centred on the stolon attachment site. These symptoms are known as <strong>spraing</strong>. Spraing symptoms may not be present in tubers at the time of harvesting but can develop in storage, particularly if the storage temperature fluctuates. In other instances, depending on prevailing environmental conditions prior to harvest, spraing symptoms may occur at high levels at the time of harvest and increase very slowly during storage.</p>\n<p>Visual inspection of plants or tubers may be inconclusive for <abbr title=\"Potato mop-top virus\">PMTV</abbr> because the symptoms are similar to responses caused by other viruses or physiological conditions. In addition, foliar and tuber symptoms may not be present. The most reliable methods for accurately determining the presence of <abbr title=\"Potato mop-top virus\">PMTV</abbr> include: isolation of the virus using soil and bait plant methods, and detection through the use of indicator plants</p>\n\n<section>\n<div class=\"wb-lbx lbx-gal\">\n<ul class=\"list-inline lst-spcd mrgn-tp-md\">\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 1</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image1_1327448805400_eng.jpg\" title=\"Figure 1 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 1 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image1small_1327448846789_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 2</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image2_1327448911849_eng.jpg\" title=\"figure 2 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"figure 2 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image2small_1327448977097_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 3</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image3_1327449030757_eng.jpg\" title=\"Figure 3 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 3 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image3small_1327449087875_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 4</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image4_1327449132281_eng.jpg\" title=\"Figure 4 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 4 - Foliar symptoms (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image4small_1327449188274_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 5</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image5_1327449231101_eng.jpg\" title=\"Figure 5 - Spraing (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 5 - Spraing (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image5small_1327449271598_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 6</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image6_1327449397829_eng.jpg\" title=\"Figure 6 - Cracking (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 6 - Cracking (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image6small_1327449439276_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 7</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image7_1327449491945_eng.jpg\" title=\"Figure 7 - damage to Spraing (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 7 - damage to Spraing (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image7small_1327449538456_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 8</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image8_1327449583310_eng.jpg\" title=\"Figure 8 - Necrotic lines (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 8 - Necrotic lines (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image8small_1327449635222_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt mrgn-bttm-sm\"><figure><figcaption>Figure 9</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image9_1327449740823_eng.jpg\" title=\"Figure 9 - Necrotic lines (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\"><img alt=\"Figure 9 - Necrotic lines (Scottish Agricultural Science Agency) \u00a9 Crown Copyright\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image9small_1327449790670_eng.jpg\" class=\"img-responsive\"></a></figure></li>\n</ul>\n</div>\n</section>\n\n<p>Photo Credits: Scottish Agricultural Science Agency (SASA) \u00a9 Crown Copyright</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Contexte</h2>\n<p>Le virus du sommet touffu de la pomme de terre cause une maladie d'importance \u00e9conomique chez les pommes de terre. Des baisses importantes de rendement et de qualit\u00e9 peuvent \u00eatre observ\u00e9es chez certains cultivars. Le champignon de la gale poudreuse (<i lang=\"la\">Spongospora subterranea</i>) est un organisme terricole et le seul vecteur connu du virus du sommet touffu de la pomme de terre. La distribution de <i lang=\"la\"><abbr title=\"Spongospora\">S.</abbr> subterranea</i> au Canada est sporadique, mais il est plus fr\u00e9quent dans les r\u00e9gions c\u00f4ti\u00e8res o\u00f9 le temps plus frais et les sols plus humides favorise son d\u00e9veloppement.</p>\n<h2>Distribution</h2>\n<ul>\n<li>Asie</li>\n<li>L'Europe Nordique</li>\n<li>Am\u00e9rique du Nord</li>\n<li>Am\u00e9rique du Sud</li>\n</ul>\n<h2>Biologie</h2>\n<p>Le virus du sommet touffu de la pomme de terre survit dans le sol \u00e0 l'int\u00e9rieur des spores de r\u00e9serve dormantes de son vecteur fongique <span lang=\"la\"><em><abbr title=\"Spongospora\">S.</abbr> subterranea</em></span>. Ces spores de r\u00e9serve infect\u00e9es viables peuvent persister jusqu'\u00e0 18 ann\u00e9es dans le sol.</p>\n<p>Une teneur en eau \u00e9lev\u00e9e et des temp\u00e9ratures fra\u00eeches (12\u00a0-\u00a020\u00a0\u00b0<abbr title=\"Celsius\">C</abbr>) du sol stimulent la germination des spores de r\u00e9serve du champignon et la lib\u00e9ration des zoospores portant le virus. Les zoospores ne peuvent se d\u00e9placer que sur de tr\u00e8s courtes distances dans le sol et ont besoin d'eau mobile pour ce faire. Les zoospores introduisent le virus dans le plant de pomme de terre lorsqu'elles infectent les racines, les stolons ou les jeunes tubercules. Le mouvement syst\u00e9mique du virus \u00e0 l'int\u00e9rieur de la plante est g\u00e9n\u00e9ralement lent et erratique. La p\u00e9riode critique d'infection et l'apparition de gale poudreuse sur les tubercules se fait t\u00f4t au cours du cycle de croissance, au moment de la formation des stolons et de la tub\u00e9risation, p\u00e9riode qui dure de 3 \u00e0 4 semaines. Les tubercules qui ont d\u00e9pass\u00e9 cette \u00e9tape de maturit\u00e9 sont r\u00e9sistants \u00e0 l'infection par les zoospores. La propagation est faible ou inexistante dans les r\u00e9gions o\u00f9 la temp\u00e9rature du sol d\u00e9passe 20\u00b0<abbr title=\"Celsius\">C</abbr> ou en l'absence d'eau. Comme <i lang=\"la\"><abbr title=\"Spongospora\">S.</abbr> subterranea</i> peut infecter les racines, le virus du sommet touffu de la pomme de terre peut s'attaquer aux cultivars qui montrent une certaine r\u00e9sistance \u00e0 la gale poudreuse.</p>\n<p>Lorsque des tubercules infect\u00e9s par le virus du sommet touffu de la pomme de terre sont plant\u00e9s comme semences, le virus n'est transmis comme infection secondaire qu'\u00e0 un nombre limit\u00e9 de tubercules de la descendance (30 - 50%). Par cons\u00e9quent, la propagation par l'interm\u00e9diaire du vecteur obligatoire, \u00e0 savoir la gale poudreuse, est le mode de transmission le plus important.</p>\n<h2>Sympt\u00f4mes sur la pomme de terre</h2>\n<h3>Tiges et feuillage</h3>\n<p>Les sympt\u00f4mes peuvent se manifester sur le feuillage des plantes produites \u00e0 partir de tubercules infect\u00e9s et sur les tiges, m\u00eame si ce ne sont pas toutes les tiges qui montrent des sympt\u00f4mes. Les sympt\u00f4mes sur le feuillage s'observent surtout lorsque la temp\u00e9rature se situe entre 5\u00a0et 15\u00a0\u00b0<abbr title=\"Celsius\">C</abbr>, plus particuli\u00e8rement aux stades pr\u00e9coces de la croissance de la plante. Le sympt\u00f4me le plus commun est l'apparition de marques ressemblant \u00e0 l'aucuba sur les tiges. Il s'agit de taches jaune vif et en anneaux ou lignes sur les feuilles inf\u00e9rieures ou m\u00e9dianes. Un deuxi\u00e8me type de sympt\u00f4mes est observ\u00e9 \u00e0 l'occasion. Il consiste en chevrons chlorotiques p\u00e2les en forme de V habituellement sur les folioles des jeunes feuilles sup\u00e9rieures et pr\u00e9sente \u00e9ventuellement une mosa\u00efque distincte sur les feuilles sup\u00e9rieures. Un troisi\u00e8me type de sympt\u00f4me prend la forme d'un raccourcissement extr\u00eame des entre-noeuds accompagn\u00e9 par un tassement ou groupement du feuillage d\u00e9crit sous le nom de <strong>\u00ab\u00a0sommet touffu\u00a0\u00bb</strong>. Les bords de certaines feuilles plus petites peuvent \u00eatre ondul\u00e9es ou enroul\u00e9es et l'effet global est un type de d\u00e9veloppement nanifi\u00e9 et group\u00e9.</p>\n<h3>Tubercules</h3>\n<p>M\u00eame si les tubercules sont parfois exempts de sympt\u00f4mes, dans d'autres cas, des anneaux concentriques superficiels en relief, mesurant de 1 \u00e0 5 <abbr title=\"centim\u00e8tres\">cm</abbr> de diam\u00e8tre, sont pr\u00e9sents \u00e0 la surface, rayonnant du point o\u00f9 le vecteur fongique a d'abord introduit le virus. Les tubercules peuvent \u00e9galement montrer des lignes, des arcs et des anneaux n\u00e9crotiques bruns dans la chair, centr\u00e9s sur le site d'insertion du stolon. Ces sympt\u00f4mes sont appel\u00e9s <strong>n\u00e9crose annulaire</strong>. La n\u00e9crose annulaire n'est pas n\u00e9cessairement pr\u00e9sente chez les tubercules au moment de la r\u00e9colte, mais peut appara\u00eetre pendant l' entreposage, plus particuli\u00e8rement si la temp\u00e9rature y fluctue. Dans d'autres cas, d\u00e9pendant des conditions environnementales qui pr\u00e9valent avant la r\u00e9cole, la n\u00e9crose annulaire peut \u00eatre abondante au moment de la r\u00e9colte et progresser tr\u00e8s lentement pendant l'entreposage.</p>\n<p>L'inspection visuelle des plants ou tubercules peut ne pas mener \u00e0 un diagnostic probant de virus du sommet touffu de la pomme de terre parce que les sympt\u00f4mes sont semblables aux r\u00e9actions caus\u00e9es par d'autres virus ou troubles physiologiques. De surcro\u00eet, il est possibles que le feuillage et les tubercules ne montrent aucun sympt\u00f4me. Les m\u00e9thodes les plus fiables pour d\u00e9terminer la pr\u00e9sence pr\u00e9cise du virus du sommet touffu de la pomme de terre sont les suivantes : isolement du virus \u00e0 l'aide de m\u00e9thodes sol et de plante-app\u00e2t d\u00e9tection gr\u00e2ce \u00e0 l'utilisation de plantes indicatrices.</p>\n\n<section>\n<div class=\"wb-lbx lbx-gal\">\n<ul class=\"list-inline lst-spcd mrgn-tp-md\">\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 1</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image1_1327448805400_fra.jpg\" title=\"Figure 1 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 1 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image1small_1327448846789_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 2</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image2_1327448911849_fra.jpg\" title=\"Figure 2 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 2 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image2small_1327448977097_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 3</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image3_1327449030757_fra.jpg\" title=\"Figure 3 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 3 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image3small_1327449087875_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 4</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image4_1327449132281_fra.jpg\" title=\"Figure 4 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 4 - Sympt\u00f4mes foliaires (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image4small_1327449188274_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 5</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image5_1327449231101_fra.jpg\" title=\"Figure 5 - N\u00e9crose annulaire (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 5 - N\u00e9crose annulaire (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image5small_1327449271598_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 6</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image6_1327449397829_fra.jpg\" title=\"Figure 6 - Fendillements (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 6 - Fendillements (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image6small_1327449439276_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 7</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image7_1327449491945_fra.jpg\" title=\"Figure 7 - N\u00e9crose annulaire (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 7 - N\u00e9crose annulaire (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image7small_1327449538456_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 8</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image8_1327449583310_fra.jpg\" title=\"Figure 8 - Lignes n\u00e9crotiques (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 8 - Lignes n\u00e9crotiques (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image8small_1327449635222_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n\n<li class=\"thumbnail hght-inhrt\"><figure><figcaption>Figure 9</figcaption>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image9_1327449740823_fra.jpg\" title=\"Figure 9 - Lignes n\u00e9crotiques (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\"><img alt=\"Figure 9 - Lignes n\u00e9crotiques (Scottish Agricultural Science Agency \u00a9 droit d'auteur de la Couronne)\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_pmtv_factsheet_image9small_1327449790670_fra.jpg\" class=\"img-responsive\"></a></figure></li>\n</ul>\n</div>\n</section>\n\n<p>R\u00e9f\u00e9rences photographiques :\u00a0 <span lang=\"en\">Scottish Agricultural Science Agency (SASA) \u00a9 Crown Copyright</span></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}