{
    "dcr_id": "1255445339954",
    "title": {
        "en": "About the Agriculture and Agri-Food Canada National Collection of Vascular Plants",
        "fr": "\u00c0 propos de la Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada"
    },
    "modified": "2020-10-05",
    "issued": "2009-10-13",
    "templatetype": "content page 1 column",
    "page_type": "1",
    "node_id": "1366750813144",
    "dc_date_created": "2009-10-13",
    "managing_branch": "PAB",
    "parent_node_id": "1366750763150",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/sci_Ottawa/WORKAREA/sci_Ottawa/templatedata/comm-comm/gene-gene/data/10/sci_eorc_vph_about_1255445339954_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/sci_Ottawa/WORKAREA/sci_Ottawa/templatedata/comm-comm/gene-gene/data/10/sci_eorc_vph_about_1255445339954_fr"
    },
    "type_name": "sitemapinit",
    "has_dcr_id": true,
    "label": {
        "en": "About the Agriculture and Agri-Food Canada National Collection of Vascular Plants",
        "fr": "\u00c0 propos de la Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada"
    },
    "breadcrumb": {
        "en": "About the Agriculture and Agri-Food Canada National Collection of Vascular Plants",
        "fr": "\u00c0 propos de la Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-10-13",
            "fr": "2009-10-13"
        },
        "modified": {
            "en": "2020-10-05",
            "fr": "2020-10-05"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "About the Agriculture and Agri-Food Canada National Collection of Vascular Plants",
            "fr": "\u00c0 propos de la Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada"
        },
        "subject": {
            "en": "plant production;plant breeding;herbs",
            "fr": "herbes;production v\u00e9g\u00e9tale;plantes-g\u00e9n\u00e9tique"
        },
        "dcterms:subject": {
            "en": "cereals;scientific research;biological diversity",
            "fr": "b\u00e2timent;diversit\u00e9 biologique"
        },
        "description": {
            "en": "Collecting voucher specimens is important as they provide the essential tools to document unknown and changing North American flora which includes unknown taxa, unknown distributions, newly established aliens and changes in status and occurrence.",
            "fr": "La collecte de sp\u00e9cimens de r\u00e9f\u00e9rence rev\u00eat d'autant plus d'importance qu'ils constituent des outils essentiels pour illustrer la flore nord-am\u00e9ricaine inconnue et changeante, qui comprend des taxons inconnus, des distributions inconnues, des esp\u00e8ces exotiques \u00e9tablies depuis peu et des changements au niveau de leur statut et de leur occurrence."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " standard reference; early records; newly established aliens; essential foundation;Victorin's Gentian",
            "fr": " milliers de sp\u00e9cimens de r\u00e9f\u00e9rence; premi\u00e8re culture; prairies canadiennes; Premi\u00e8re Guerre mondiale;gentiane de Victorin"
        },
        "audience": {
            "en": "scientists;general public;educators",
            "fr": "scientifiques;grand public;\u00e9ducateurs"
        },
        "type": {
            "en": "educational material",
            "fr": "mat\u00e9riel didactique"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "About the Agriculture and Agri-Food Canada National Collection of Vascular Plants",
        "fr": "\u00c0 propos de la Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <nav>\n<h2 class=\"hidden\">Previous-next document navigation - top</h2>\n<p class=\"text-right\"><a href=\"?id=1251393521021\" title=\"The AAFC National Collection of Vascular Plants\">Table of Contents</a></p>\n</nav>\n\n<p>Agriculture and Agri-Food Canada's (AAFC) National Collection of Vascular Plants, includes thousands of voucher specimens. \"Vouchers\" are specimens preserved as reference material used in a research project. Such specimens allow researchers to see exactly what material was used and to examine it to answer future questions that may arise. For instance, the use of <abbr title=\"Deoxyribonucleic acid\">DNA</abbr> to confirm and establish identity is an example of the kind of use people did not dream of 20\u00a0years ago.</p>\n\n<p>Collecting voucher specimens is important as they provide the essential tools to document unknown and changing North American flora which includes unknown taxa, unknown distributions, newly established aliens and changes in status and occurrence.</p>\n\n<p>Most of the specimens in the collection are from Canada (about 62%), but the north temperate region of the world is generally well represented (United States of America 20%, Europe 10%). The collection is well developed in the areas of pest species and crop relatives reflecting the priorities of AAFC. The herbarium includes about 50,000 plant species or about 20% of the world total. It also contains 4,000 type specimens which are used by scientists to define the correct use of plant names. These specimens are permanently available for future study and provide an essential foundation for science.</p>\n\n<p>When adequately protected from moisture and pests, dried plant specimens can last for hundreds of years in the herbarium. Shown here is a specimen of an attractive blue-flowered plant called <a href=\"#img1\">Victorin's Gentian</a>. It was collected by Anne Mary Perceval around 1820. Mrs. Perceval was one of many people who collected plants for Sir William Jackson Hooker who provided the standard reference on Canadian plants for the period of 1840-1883. Today, Victorin's Gentian is a very rare plant confined to the estuary of the St. Lawrence River and considered threatened. The early records from herbarium specimens suggest that it was always a very rare and restricted plant.</p>\n\n<p id=\"img1\"><img src=\"https://www.agr.gc.ca/resources/prod/img/science/ecorc/images/gentiana2.jpg\" alt=\"The Herbarium specimen of Victorin's Gentian which was collected around 1820\"></p>\n\n<p>Also shown is a representative herbarium specimen of <a href=\"#img2\">Marquis Wheat</a>, a cultivar bred in 1892 at AAFC's Central Experimental Farm by <a href=\"#img3\">Sir Charles E. Saunders</a>. This variety was a major contribution to the Canadian economy and made possible a vast increase in wheat production on the Canadian prairies. It has been described as \"one of the world's greatest feats in economic crop breeding\". By 1911, it was the first 200 million bushel crop in the West's history. During the First World War in 1915, it made up 90% of the wheat shipped by Canada to France, and was essential at that time since U-boats had cut off supplies from Australia and Argentina. This variety was cultivated on 8 million hectares of the Prairie Provinces in 1928 and at that time accounted for 90% of the area devoted to spring wheat in western Canada. Marquis has been replaced by even earlier maturing varieties that are resistant to more recently evolved strains of rust fungus. Herbarium specimens of Marquis Wheat are still available to researchers studying the evolution of wheat in an effort to maintain a superior crop.</p>\n\n<p id=\"img2\"><img src=\"https://www.agr.gc.ca/resources/prod/img/science/ecorc/images/marquis_wheatv2.jpg\" alt=\"A Herbarium specimen of Marquis wheat including a label indicating name of plant, locality of origin, habitat, date collected, collector(s)\"></p>\n\n<p id=\"img3\"><img src=\"https://www.agr.gc.ca/resources/prod/img/science/ecorc/images/charles_saunders_my2006-rev.jpg\" alt=\"Charles Saunders as a young researcher in wheat field\"></p>\n\n<p><a href=\"?id=1251393521021\">Back to the Agriculture and Agri-Food Canada National Collection of Vascular Plants</a></p>\n\n<nav>\n<h2 class=\"hidden\">Previous-next document navigation - top</h2>\n<p class=\"text-right\"><a href=\"?id=1251393521021\" title=\"The AAFC National Collection of Vascular Plants\">Table of Contents</a></p>\n</nav>\n\r\n    ",
        "fr": "\r\n    <nav>\n<h2 class=\"hidden\">Navigation dans le document : page suivante - page pr\u00e9c\u00e9dente - bas</h2>\n<p class=\"text-right\"><a title=\"Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada\" href=\"?id=1251393521021\">Table des mati\u00e8res</a> </p></nav>\n\n<p>La Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada (AAC) comprend des milliers de sp\u00e9cimens de r\u00e9f\u00e9rence. Les sp\u00e9cimens \u00ab\u00a0de r\u00e9f\u00e9rence\u00a0\u00bb sont conserv\u00e9s comme mat\u00e9riels de r\u00e9f\u00e9rence qu'on utilise dans le cadre d'un projet de recherche. Ces sp\u00e9cimens permettent aux chercheurs de savoir exactement quels mat\u00e9riels ont \u00e9t\u00e9 utilis\u00e9s et de les examiner pour r\u00e9pondre aux questions qui pourraient se poser \u00e0 l'avenir. Par exemple, l'utilisation de l'<abbr title=\"acides d\u00e9soxyribonucl\u00e9iques\">ADN</abbr> pour confirmer et d\u00e9terminer l'identit\u00e9 est un exemple du type d'utilisation dont les gens ne r\u00eavaient m\u00eame pas il y a une vingtaine d'ann\u00e9es.</p>\n\n<p>La collecte de sp\u00e9cimens de r\u00e9f\u00e9rence rev\u00eat d'autant plus d'importance qu'ils constituent des outils essentiels pour illustrer la flore nord-am\u00e9ricaine inconnue et changeante, qui comprend des taxons inconnus, des distributions inconnues, des esp\u00e8ces exotiques \u00e9tablies depuis peu et des changements au niveau de leur statut et de leur occurrence.</p> \n\n<p>La plupart des sp\u00e9cimens de cette collection proviennent du Canada (environ 62\u00a0%), m\u00eame si la r\u00e9gion temp\u00e9r\u00e9e septentrionale du globe est g\u00e9n\u00e9ralement bien repr\u00e9sent\u00e9e (\u00c9tats-Unis, 20\u00a0%; Europe, 10\u00a0%). La collection est vaste dans les secteurs des esp\u00e8ces nuisibles et apparent\u00e9es aux cultures, ce qui refl\u00e8te les priorit\u00e9s d'AAC. L'herbier comporte environ 50\u00a0000 esp\u00e8ces de v\u00e9g\u00e9taux, soit environ 20\u00a0% du total mondial. On y trouve \u00e9galement 4\u00a0000 sp\u00e9cimens types dont les scientifiques se servent pour d\u00e9terminer l'emploi exact des noms de plantes. Ces sp\u00e9cimens sont disponibles en permanence pour les \u00e9tudes futures et pour servir de fondement essentiel aux sciences.</p>\n\n<p>Lorsqu'ils sont bien prot\u00e9g\u00e9s contre l'humidit\u00e9 et les ravageurs, les sp\u00e9cimens v\u00e9g\u00e9taux peuvent durer des centaines d'ann\u00e9es dans l'herbier. Dans l'herbier on trouve un sp\u00e9cimen d'une jolie plante aux fleurs bleues appel\u00e9e <a href=\"#img1\">gentiane de Victorin</a>. Celui-ci a \u00e9t\u00e9 recueilli par Anne Mary Perceval vers 1820. M<sup>me</sup> Perceval \u00e9tait l'une des nombreuses personnes qui collectionnaient des plantes pour Sir William Jackson Hooker, qui a produit un livre de r\u00e9f\u00e9rence sur les plantes canadiennes qui a servi entre 1840 et 1883. De nos jours, la gentiane de Victorin est une plante extr\u00eamement rare dont l'aire est confin\u00e9e \u00e0 la zone intertidale de l'estuaire du Saint-Laurent et qui est menac\u00e9e d'extinction. Les archives des sp\u00e9cimens de l'herbier incitent \u00e0 penser qu'il s'est toujours agi d'une plante tr\u00e8s rare et \u00e0 aire de r\u00e9partition limit\u00e9e.</p>\n\n<p><img id=\"img1\" alt=\"Une gentiane de Victorin recueilli par Anne Mary Perceval vers 1820.\" src=\"https://www.agr.gc.ca/resources/prod/img/science/ecorc/images/gentiana2.jpg\"></p>\n\n<p>On voit illustr\u00e9 ici un sp\u00e9cimen d'herbier repr\u00e9sentatif du <a href=\"#img2\">bl\u00e9 Marquis</a>, un cultivar s\u00e9lectionn\u00e9 en 1892 \u00e0 la Ferme exp\u00e9rimentale centrale d'AAC par <a href=\"#img3\">Sir Charles E. Saunders</a> (voir ci-apr\u00e8s). Cette vari\u00e9t\u00e9 a apport\u00e9 une pr\u00e9cieuse contribution \u00e0 l'\u00e9conomie canadienne et a permis une hausse colossale de la production de bl\u00e9 dans les prairies canadiennes. Elle a \u00e9t\u00e9 d\u00e9crite comme l'\u00ab\u00a0un des plus grands exploits du monde dans l'am\u00e9lioration g\u00e9n\u00e9tique des cultures sur le plan \u00e9conomique\u00a0\u00bb. En 1911, elle a \u00e9t\u00e9 la premi\u00e8re culture \u00e0 d\u00e9passer la barre des 200\u00a0millions de boisseaux dans l'histoire de l'Ouest. Durant la Premi\u00e8re Guerre mondiale, en 1915, elle repr\u00e9sentait jusqu'\u00e0 90\u00a0% du bl\u00e9 exp\u00e9di\u00e9 en France par le Canada, et \u00e9tait indispensable \u00e0 cette \u00e9poque \u00e9tant donn\u00e9 que les sous-marins allemands avaient interrompu les approvisionnements en provenance d'Australie et d'Argentine. Cette vari\u00e9t\u00e9 \u00e9tait cultiv\u00e9e sur 20\u00a0millions d'acres dans les provinces des Prairies en 1928 et, \u00e0 l'\u00e9poque, elle repr\u00e9sentait 90\u00a0% de la superficie consacr\u00e9e \u00e0 la culture du bl\u00e9 de printemps dans l'Ouest du Canada. Le bl\u00e9 Marquis a \u00e9t\u00e9 remplac\u00e9 par des vari\u00e9t\u00e9s qui parviennent encore plus t\u00f4t \u00e0 maturit\u00e9 et qui r\u00e9sistent aux souches plus r\u00e9centes de la rouille. Les sp\u00e9cimens d'herbier du bl\u00e9 Marquis sont toujours accessibles aux chercheurs qui \u00e9tudient l'\u00e9volution du bl\u00e9 afin de maintenir une culture sup\u00e9rieure.</p>\n\n<p><img id=\"img2\" alt=\"Un sp\u00e9cimen d'herbier du bl\u00e9 Marquis avec \u00e9tiquette de r\u00e9colte comprenant le nom latin du sp\u00e9cimen, la localit\u00e9 de r\u00e9colte, l'habitat, la date de r\u00e9colte, le r\u00e9colteur et le d\u00e9terminateur\" src=\"https://www.agr.gc.ca/resources/prod/img/science/ecorc/images/marquis_wheatv2.jpg\"></p>\n\n<p><img id=\"img3\" alt=\"Le chercheur Charles Saunders dans un champ de bl\u00e9\" src=\"https://www.agr.gc.ca/resources/prod/img/science/ecorc/images/charles_saunders_my2006-rev.jpg\"></p> \n\n<p><a href=\"?id=1251393521021\">Retournez \u00e0 la Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada</a></p>\n\n<nav>\n<h2 class=\"hidden\">Navigation dans le document : page suivante - page pr\u00e9c\u00e9dente - bas</h2>\n<p class=\"text-right\"><a title=\"Collection nationale de plantes vasculaires d'Agriculture et Agroalimentaire Canada\" href=\"?id=1251393521021\">Table des mati\u00e8res</a></p>\n</nav>\n\r\n    "
    }
}