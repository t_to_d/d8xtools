{
    "dcr_id": "1657557647452",
    "title": {
        "en": "Livestock feed approval or registration: How to apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Comment pr\u00e9senter une demande"
    },
    "html_modified": "2024-03-12 3:59:19 PM",
    "modified": "2022-07-20",
    "issued": "2022-07-19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/4_how_apply_lvstck_fd_apprvl_1657557647452_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/4_how_apply_lvstck_fd_apprvl_1657557647452_fra"
    },
    "ia_id": "1657557647827",
    "parent_ia_id": "1627997833424",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1627997833424",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Livestock feed approval or registration: How to apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Comment pr\u00e9senter une demande"
    },
    "label": {
        "en": "Livestock feed approval or registration: How to apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Comment pr\u00e9senter une demande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1657557647827",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1627997831971",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/approval-and-registration/livestock-feed/",
        "fr": "/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Livestock feed approval or registration: How to apply",
            "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Comment pr\u00e9senter une demande"
        },
        "description": {
            "en": "Animals, Animal Health",
            "fr": "Animals, Animal Health"
        },
        "keywords": {
            "en": "Animals, Animal Health, livestock, feed, how to apply, application, regulation",
            "fr": "aliments, animaux de ferme, comment de pr\u00e9senter une demande, informations, entreprise"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "modified": {
            "en": "2022-07-20",
            "fr": "2022-07-20"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Livestock feed approval or registration: How to apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Comment pr\u00e9senter une demande"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657223639320/1657223788955\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657306963131/1657306963740\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657309218484/1657309218702\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item active\" href=\"\">4. How to apply</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657558817043/1657558817433\">5. After you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657559631753/1657559632175\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<div class=\"row\">\n<div class=\"col-md-8\">\n\t\n<p>Once you <a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657309218484/1657309218702#a1\">get your information ready</a>, you can fill in and pay for your application for approval or registration through the electronic submission platform, My CFIA.</p>\n\t\n<p>A person having designated signing authority must send the online application form through My CFIA.</p>\n\t\n<p>When a Canadian agent signature is required, the CFIA will contact the applicant to get the signature if needed. This step will happen once the CFIA accepts and logs the submission in the queue for review by the CFIA.</p>\n\t\n<p>The CFIA will only process your payment once the application is accepted to be logged in for evaluation.</p>\n\t\n<p class=\"mrgn-tp-lg mrgn-bttm-lg\"><a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\" class=\"btn btn-primary\">Apply using My\u00a0CFIA</a></p>\n\t\n<p>If you are unable to sign up for My CFIA, you can submit your completed form <a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5933/eng/1616795005347/1616795104307\">CFIA/ACIA\u00a05933\u00a0\u2013 Application for Feed Approval or Registration under the <i>Feeds Act</i> and <i>Feeds Regulations</i></a> and submission package by mail to:</p>\n\t\n<p>Pre-Market Application Submissions Office (PASO)<br>\n<br>\n<br>\n<span class=\"nowrap\">59 Camelot Drive</span><br>\n<span class=\"nowrap\">Ottawa, ON K1A 0Y9</span><br>\nCanada\n</p>\n\t\n</div>\n\t\n<div class=\"col-md-4 pull-right mrgn-tp-0 mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Additional information</h2>\n</header>\n<div class=\"panel-body\">\n<p>Before contacting the CFIA with any questions, review <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/eng/1329109265932/1329109385432\">RG-1 Regulatory guidance\u00a0: Feed registration procedures and labelling standards</a>. If you still can't find what you're looking for after reviewing the guidance, you can then contact the CFIA.</p>\n<p>For questions about administrative processes, email the Pre-market Application Submissions Office (PASO) at: <a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">cfia.paso-bpdpm.acia@inspection.gc.ca</a>.</p>\n<p>For questions of a technical nature, email the Animal Feed Program (AFP) at: <a href=\"mailto:cfia.afp-paa.acia@inspection.gc.ca\">cfia.afp-paa.acia@inspection.gc.ca</a>.</p>\n</div>\n</section>\n</div>\n</div>\n\t\n\t\n<h3>Payment options (mailed applications)</h3>\n\t\n<p>Enclose the applicable application fee, based on the service requested and the feed evaluation category with your application.</p>\n\t\n<p>Fees are payable in Canadian funds by:</p>\n\n<ul class=\"lst-spcd\">\n<li>on-account option (preferred)\n<ul>\n<li>if you don't have a valid CFIA account number and wish to apply for one:\n<ul>\n<li>complete an <a href=\"/eng/1328823628115/1328823702784#c0015\">Application for Credit (CFIA/ACIA\u00a00015)</a> form</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>Visa\u2122</li>\n<li>Mastercard\u2122</li>\n<li>American Express\u2122</li>\n<li>cheque (from a Canadian bank) or money order payable to the Receiver General for Canada</li>\n</ul>\n\t\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657309218484/1657309218702\" rel=\"prev\">Previous: Before you apply</a></li>\n<li class=\"next\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657558817043/1657558817433\" rel=\"next\">Next: After you apply</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657223639320/1657223788955\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657306963131/1657306963740\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657309218484/1657309218702\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item active\" href=\"\">4. Comment pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657558817043/1657558817433\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657559631753/1657559632175\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\t\n<div class=\"row\">\n<div class=\"col-md-8\">\n\n<p>Une fois <a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657309218484/1657309218702#a1\">vos informations pr\u00e9par\u00e9es</a>, vous pouvez remplir et payer votre demande d'approbation ou d'enregistrement par le biais de la plateforme de soumission \u00e9lectronique, Mon ACIA.</p>\n\t\n<p>Une personne ayant le pouvoir de signature d\u00e9sign\u00e9 doit envoyer le formulaire de demande en ligne par le biais de Mon ACIA.</p>\n\t\n<p>Lorsqu'une signature d'un agent canadien est requise, l'ACIA contactera le requ\u00e9rant pour obtenir la signature si n\u00e9cessaire. Cette \u00e9tape aura lieu une fois que l'ACIA aura accept\u00e9 et consign\u00e9 la demande dans la file d'attente pour \u00e9valuation par l'ACIA.</p>\n\n<p>L'ACIA va seulement traiter votre paiement une fois que la demande est accept\u00e9e pour \u00eatre \u00e9valu\u00e9e.</p>\n\t\n<p>L'ACIA ne traitera votre paiement qu'une fois que la demande aura \u00e9t\u00e9 accept\u00e9e en vue de son \u00e9valuation.</p>\n\t\n<p class=\"mrgn-tp-lg mrgn-bttm-lg\"><a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\" class=\"btn btn-primary\">Pr\u00e9senter une demande via Mon ACIA</a></p>\n\n<p>Si vous ne pouvez pas vous inscrire \u00e0 Mon ACIA, vous pouvez soumettre votre formulaire <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5933/fra/1616795005347/1616795104307\">CFIA/ACIA\u00a05933\u00a0\u2013 Demande d'approbation ou d'enregistrement pour les aliments du b\u00e9tail en vertu de la <i>Loi relative aux aliments du b\u00e9tail</i> et du <i>R\u00e8glement sur les aliments du b\u00e9tail</i></a> rempli et votre dossier de soumission par voie postale \u00e0 l'adresse suivante\u00a0:</p>\n\t\n<p>Bureau de pr\u00e9sentation de demandes pr\u00e9alables \u00e0 la mise en march\u00e9 (BPDPM)<br>\n<br>\n<br>\n<br>\n<span class=\"nowrap\">Ottawa (Ontario) K1A 0Y9</span><br>\n</p>\n</div>\n\t\n<div class=\"col-md-4 pull-right mrgn-tp-0 mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Informations suppl\u00e9mentaires</h2>\n</header>\n<div class=\"panel-body\">\n<p>Avant de communiquer avec l'ACIA pour toute question, veuillez consulter le <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/fra/1329109265932/1329109385432\">RG-1 Directives r\u00e9glementaires: Proc\u00e9dures d'enregistrement et normes d'\u00e9tiquetage des aliments pour animaux</a>. Si vous ne trouvez toujours pas ce que vous cherchez apr\u00e8s avoir examin\u00e9 les directives, vous pouvez alors contacter l'ACIA.</p>\n<p>Pour toute question concernant les processus administratifs, veuillez envoyer un courriel au Bureau de pr\u00e9sentation de demandes pr\u00e9alables \u00e0 la mise en march\u00e9 (BPDPM) \u00e0 l'adresse suivante\u00a0: <a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">cfia.paso-bpdpm.acia@inspection.gc.ca</a>.</p>\n<p>Pour toute question de nature technique, veuillez envoyer un courriel au PAA \u00e0 l'adresse suivante: <a href=\"mailto:cfia.afp-paa.acia@inspection.gc.ca\">cfia.afp-paa.acia@inspection.gc.ca</a>.</p>\n</div>\n</section>\n</div>\n</div>\n\n\t\n<h2>Options de paiement (demandes envoy\u00e9es par voie postale)</h2>\n\t\n<p>Veuillez joindre \u00e0 votre demande les frais applicables en fonction du service demand\u00e9 et de la cat\u00e9gorie d'\u00e9valuation des aliments pour animaux.</p>\n\t\n<p>Les frais sont payables en dollars canadiens par\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>L'option du compte de paiement (m\u00e9thode recommand\u00e9e)\n<ul>\n<li>Si vous n'avez pas un num\u00e9ro de compte valide de l'ACIA et que vous souhaitez en obtenir un\u00a0:\n<ul>\n<li>Veuillez remplir une <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/fra/1328823628115/1328823702784?form=c0015\">Demande de cr\u00e9dit (ACIA/CFIA\u00a00015)</a></li>\n</ul></li>\n</ul></li>\n<li>Visa\u2122</li>\n<li>Mastercard\u2122</li>\n<li>American Express\u2122</li>\n<li>Ch\u00e8que (provenant d'une banque Canadienne) ou par mandat postal \u00e0 l'ordre du receveur g\u00e9n\u00e9ral du Canada</li>\n</ul>\n\t\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657309218484/1657309218702\" rel=\"prev\">Pr\u00e9c\u00e9dent\u00a0: Avant de pr\u00e9senter une demande</a></li>\n<li class=\"next\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657558817043/1657558817433\" rel=\"next\">Suivant\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}