{
    "dcr_id": "1348157925518",
    "title": {
        "en": "Meals ",
        "fr": "Repas "
    },
    "html_modified": "2024-03-12 3:55:48 PM",
    "modified": "2014-03-24",
    "issued": "2012-09-20 12:18:47",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/labeti_decisions_meals_1348157925518_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/labeti_decisions_meals_1348157925518_fra"
    },
    "ia_id": "1394902356457",
    "parent_ia_id": "1625662978189",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling",
        "fr": "\u00c9tiquetage"
    },
    "commodity": {
        "en": "Dairy products|Egg, processed products|Egg, shell|Fish and seafood|Fruits and vegetables, fresh|Fruits and vegetables, processed products|Maple products|Meat products and food animals|Other|Grain-based foods|Honey",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande|Autre|Fruits et l\u00e9gumes, frais|Fruits et l\u00e9gumes, produits  transform\u00e9s|Oeufs, en coquille|Oeufs, produits transform\u00e9s|Poissons et fruits de mer|Produits d'\u00e9rable|Produits laitiers|Aliments \u00e0 base de grains|Miel"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1625662978189",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Meals ",
        "fr": "Repas "
    },
    "label": {
        "en": "Meals ",
        "fr": "Repas "
    },
    "templatetype": "content page 1 column",
    "node_id": "1394902356457",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1625662827512",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/common-name/meals/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/nom-usuel/repas/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Meals",
            "fr": "Repas"
        },
        "description": {
            "en": "Question and decision on the foods that have to meet the compositional requirements for a prepackaged meal as set out in Division 1 of the Food and Drug Regulations.",
            "fr": "Question et d\u00e9cision sur les repas qui doivent respecter les normes de composition \u00e9tablies pour les repas pr\u00e9emball\u00e9s dans le Titre 1 du R\u00e8glement sur les aliments et drogues."
        },
        "keywords": {
            "en": "labels, labelling, advertising, consumers, claims, nutrition, composition, quality, quantity, origin, meals",
            "fr": "&#233;tiquette, &#233;tiquetage, publicit&#233;, consumers, all&#233;gations, nutritive, composition, qualit&#233;, quantit&#233;, origine, repas"
        },
        "dcterms.subject": {
            "en": "retail trade,consumers,food labeling,food inspection,consumer protection",
            "fr": "commerce de d\u00e9tail,consommateur,\u00e9tiquetage des aliments,inspection des aliments,protection du consommateur"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-09-20 12:18:47",
            "fr": "2012-09-20 12:18:47"
        },
        "modified": {
            "en": "2014-03-24",
            "fr": "2014-03-24"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Meals",
        "fr": "Repas"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What foods have to meet the compositional requirements for a prepackaged meal as set out in Division 1 of the <i>Food and Drug Regulations</i>?</h2>\n<p> A prepackaged food that is represented as a meal is required to meet the compositional requirements for a prepackaged meal as set out in section B.01.001 of the <i>Food and Drug Regulations</i>. Specifically, a \"prepackaged meal\" is a prepackaged selection of food for one individual that requires no preparation other than heating and that contains at least one serving, as described in <i>Canada's Food Guide to Healthy Eating</i>, published in 1992 by the Department of Supply and Services by authority of the Minister of National Health and Welfare, of</p>\n<ul>\n<li>meat, fish, poultry, legumes, nuts, seeds, eggs or milk or milk products other than butter, cream, sour cream, ice cream, ice milk and sherbert, and </li>\n<li>vegetables, fruit or grain products.</li>\n</ul>\n<p>Foods described using the words \"meal\", \"breakfast\", \"lunch\", \"dinner\", and similar terms are usually considered to be represented as a meal for the purposes of Division\u00a01. Also, a brand name which suggests that the product is a meal, by including the name of a meal as part of the brand name, must meet the requirements for a meal.</p>\n<p> Products described as an \"<span lang=\"fr\">entr\u00e9e</span>\" do not have to meet the requirements for a meal. Therefore, if an <span lang=\"fr\">entre\u00e9</span> is packed under a brand name which includes the name of a meal, <abbr title=\"for example\">e.g.</abbr>, \"Lunch\", it is necessary to indicate clearly, in the brand name or in conjunction with the brand name, that it is an <span lang=\"fr\">entre\u00e9</span>, <abbr title=\"for example\">e.g.</abbr>, \"Lunch (naming the brand) <span lang=\"fr\">Entre\u00e9</span>\", in order to exempt it from meeting the requirements of a meal.</p>\n<p> Some foods are often referred to generically in conversation as \"TV dinners\". They are not subject to Division\u00a01. However, in the unlikely event that they are labelled using the words \"TV Dinner\", they would be subject to the requirements of Division\u00a01.</p>\n<p>\"Junior Dinners\" are designed for very young children. They are not considered meals as described above and, as such, are not subject to these requirements. </p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Quels sont les aliments qui sont r\u00e9gis par les normes de composition \u00e9tablies pour les repas pr\u00e9emball\u00e9s dans le Titre 1 du <i>R\u00e8glement sur les aliments et drogues</i>?</h2>\n<p> Un aliment pr\u00e9emball\u00e9 qui est repr\u00e9sent\u00e9 comme un repas doit respecter les normes de composition des repas pr\u00e9emball\u00e9s \u00e9tablies \u00e0 l'article B.01.001 du <i>R\u00e8glement sur les aliments et drogues</i>. Plus pr\u00e9cis\u00e9ment, un \u00ab\u00a0repas pr\u00e9emball\u00e9\u00a0\u00bb est un choix pr\u00e9emball\u00e9 d'aliments destin\u00e9 \u00e0 une seule personne, qui ne requiert aucune autre pr\u00e9paration que le r\u00e9chauffage et qui contient au moins les portions suivantes, selon la description qui en est donn\u00e9e dans la publication intitul\u00e9e <i>Guide alimentaire canadien pour manger sainement</i>, autoris\u00e9e par le ministre de la Sant\u00e9 nationale et du Bien-\u00eatre social et publi\u00e9e en 1992 par le minist\u00e8re des Approvisionnements et Services\u00a0:</p>\n<ul>\n<li>une portion de viande, poisson, volaille, l\u00e9gumineuses, noix, graines, oeufs ou lait ou produits du lait autres que le beurre, la cr\u00e8me, la cr\u00e8me sure, la cr\u00e8me glac\u00e9e, le lait glac\u00e9 et le sorbet;</li>\n<li>une portion de l\u00e9gumes, fruits ou produits c\u00e9r\u00e9aliers.</li>\n</ul>\n<p>Les aliments d\u00e9crits par les mots \u00ab\u00a0repas\u00a0\u00bb, \u00ab\u00a0petit d\u00e9jeuner\u00a0\u00bb, \u00ab\u00a0d\u00eener\u00a0\u00bb, \u00ab\u00a0souper\u00a0\u00bb ou par des termes similaires sont habituellement consid\u00e9r\u00e9s comme \u00e9tant repr\u00e9sent\u00e9s comme des repas aux fins du Titre 1. De m\u00eame, toute appellation commerciale comportant le nom d'un repas et laissant ainsi croire que le produit est un repas doit satisfaire aux exigences prescrites pour les repas.</p>\n<p> Les produits d\u00e9sign\u00e9s comme \u00ab\u00a0plat principal\u00a0\u00bb n'ont pas \u00e0 satisfaire aux exigences prescrites pour les repas. Par cons\u00e9quent, si un plat principal porte sur son emballage une appellation commerciale qui contient le nom d'un repas, <abbr title=\"par exemple\">p.\u00a0ex.</abbr>, \u00ab\u00a0d\u00eener\u00a0\u00bb, il est n\u00e9cessaire d'indiquer, de fa\u00e7on claire, dans l'appellation commerciale ou \u00e0 proximit\u00e9 de l'appellation commerciale, qu'il s'agit d'un plat principal, <abbr title=\"par exemple\">p.\u00a0ex.</abbr> \u00ab\u00a0plat principal pour le d\u00eener (nom de l'appellation commerciale)\u00a0\u00bb afin de l'exempter du respect des exigences applicables aux repas.</p>\n<p> Dans le langage courant, on emploie souvent l'expression \u00ab\u00a0<span lang=\"en\">TV Dinner</span> (repas pr\u00e9par\u00e9)\u00a0\u00bb pour d\u00e9signer certains aliments. Le Titre\u00a01 ne r\u00e9git pas cette cat\u00e9gorie d'aliments. Cependant, si jamais l'expression \u00ab\u00a0<span lang=\"en\">TV Dinner</span> (repas pr\u00e9par\u00e9)\u00a0\u00bb devait \u00eatre employ\u00e9e sur l'\u00e9tiquette, les aliments en question seraient vis\u00e9s par les exigences du Titre\u00a01.</p>\n<p> Les \u00ab\u00a0repas pour enfants en bas \u00e2ge\u00a0\u00bb sont destin\u00e9s aux tr\u00e8s jeunes enfants. Ils ne sont pas consid\u00e9r\u00e9s comme des repas conform\u00e9ment \u00e0 la description susmentionn\u00e9e et, en tant que tels, ne sont pas r\u00e9gis par ces exigences. </p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}