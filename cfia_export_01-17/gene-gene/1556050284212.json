{
    "dcr_id": "1556050284212",
    "title": {
        "en": "Pig owners: Protecting pigs from African swine fever",
        "fr": "Propri\u00e9taires de porcs\u00a0: Prot\u00e9ger vos porcs contre la peste porcine africaine"
    },
    "html_modified": "25-4-2019",
    "modified": "9-3-2022",
    "issued": "25-4-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/pigownrs_prtct_asf_1556050284212_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/pigownrs_prtct_asf_1556050284212_fra"
    },
    "parent_ia_id": "1306983373952",
    "ia_id": "1556050284482",
    "parent_node_id": "1306983373952",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pig owners: Protecting pigs from African swine fever",
        "fr": "Propri\u00e9taires de porcs\u00a0: Prot\u00e9ger vos porcs contre la peste porcine africaine"
    },
    "label": {
        "en": "Pig owners: Protecting pigs from African swine fever",
        "fr": "Propri\u00e9taires de porcs\u00a0: Prot\u00e9ger vos porcs contre la peste porcine africaine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1556050284482",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306983245302",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/pig-owners/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/proprietaires-de-porcs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pig owners: Protecting pigs from African swine fever",
            "fr": "Propri\u00e9taires de porcs\u00a0: Prot\u00e9ger vos porcs contre la peste porcine africaine"
        },
        "description": {
            "en": "All varieties of pigs, whether kept as pets, companion animals or farm animals, are susceptible to African swine fever (ASF).",
            "fr": "Tous les porcs, qu'ils soient gard\u00e9s comme animaux de compagnie, animaux familiers ou animaux d'\u00e9levage, peuvent contracter la peste porcine africaine (PPA)."
        },
        "keywords": {
            "en": "Reportable Diseases Regulations, African swine fever, African swine, contagious viral disease of swine, reportable disease, Government of Canada, varieties of pigs, pets, companion animals, farm animals",
            "fr": "R\u00e9glementation en mati\u00e8re de maladies \u00e0 d\u00e9claration obligatoire, Peste porcine africaine, Porc africain, maladie virale contagieuse du porc, maladie \u00e0 d\u00e9claration obligatoire, gouvernement du Canada, vari\u00e9t\u00e9s de porcs, animaux domestiques, animaux de compagnie, animaux de ferme"
        },
        "dcterms.subject": {
            "en": "animal,inspection,animal inspection,animal diseases,animal health",
            "fr": "animal,inspection,inspection des animaux,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-04-25",
            "fr": "2019-04-25"
        },
        "modified": {
            "en": "2022-03-09",
            "fr": "2022-03-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pig owners: Protecting pigs from African swine fever",
        "fr": "Propri\u00e9taires de porcs\u00a0: Prot\u00e9ger vos porcs contre la peste porcine africaine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>All varieties of pigs, whether kept as pets, companion animals or farm animals, are susceptible to African swine fever (ASF).</p>\n\n<p>To help protect pigs from ASF, pig owners should be aware of the following:</p>\n\n<ul>\n<li>if you notice that your pig is showing signs of ASF, isolate the pig immediately</li>\n<li>you must report any suspicion of ASF <a href=\"/eng/1300462382369/1300462438912\">to your local CFIA veterinarian</a> immediately</li>\n<li>if you travel with a pig outside of Canada, it may not be allowed back to Canada</li>\n</ul>\n\n<p>All pigs are susceptible to the same serious diseases, regardless of type or where they live. This is one of the reasons why the Canadian Food Inspection Agency (CFIA) does not distinguish between a pig kept as a personal pet and a pig raised for food.</p>\n\n<p>If you plan to travel outside of the country and return with your Canadian origin pig, make sure that you are aware of the <a href=\"/animal-health/terrestrial-animals/exports/porcine/eng/1405795282433/1405795283542\" title=\"Porcine - Animal Health Certificates for Export\">export requirements</a> and of the <a href=\"/eng/1516899032767/1516899033337\">import requirements</a> for return to Canada. Import conditions for breeding pigs must be met, including testing and quarantine when returning to Canada after travelling.</p>\n\n<p>Refer to our <a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/fact-sheet/eng/1306993248674/1306993787261\">frequently asked questions about ASF</a> to inform yourself about ASF.</p>\n\n<h2>Awareness toolkit</h2>\n\n<p>Everyone has a role to play in reducing the risk of animal diseases. Do your part by sharing this toolkit with your network. You'll find helpful videos, fact sheets, posters and images to help spread the word, not the animal disease.</p>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/importing-food-plants-or-animals/pets/infographic-what-you-need-to-know-if-you-plan-to-t/eng/1516994052131/1516994052834\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/travelling_pigs_asf_1556033937376_eng.jpg\" class=\"img-responsive thumbnail\" alt=\"Infographic: What you need to know if you plan to travel with a pig\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/importing-food-plants-or-animals/pets/infographic-what-you-need-to-know-if-you-plan-to-t/eng/1516994052131/1516994052834\">Infographic: What you need to know if you plan to travel with a pig</a></strong></p>\n<p>All varieties of pigs, whether kept as pets, companion animals or farm animals, are susceptible to the same serious diseases.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/economic-impact/eng/1546889082597/1546889294408\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/economic_impact_asf_1556033444182_eng.png\" class=\"img-responsive thumbnail\" alt=\"Infographic: Economic impact\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/economic-impact/eng/1546889082597/1546889294408\">Infographic: Economic impact</a></strong></p>\n<p>African swine fever poses a significant risk to the Canadian pork industry and the Canadian economy.</p>\n</div>\n</div>\n\n<h2>Awareness tools from third parties</h2>\n\n<ul>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">World Organisation for Animal Health awareness tools </a></li>\n<li><a href=\"https://www.youtube.com/watch?time_continue=7&amp;v=eyQ4t1wHl2M\">Video: African swine fever\u00a0\u2013\u00a0How to stay one step ahead</a></li>\n</ul>\n\n<h2>Resources for pet pig owners</h2>\n\n<ul>\n<li><a href=\"http://www.cfsph.iastate.edu/DiseaseInfo/disease-images.php?name=african-swine-fever&amp;lang=in\">Disease images of African swine fever</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/reporting-animal-diseases/eng/1363844638670/1363844792607\">Reporting animal diseases</a></li>\n<li><a href=\"https://www.canadianveterinarians.net/\">Canadian Veterinary Medical Association</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Tous les porcs, qu'ils soient gard\u00e9s comme animaux de compagnie, animaux familiers ou animaux d'\u00e9levage, peuvent contracter la peste porcine africaine (PPA).</p>\n\n<p>Pour vous aider \u00e0 prot\u00e9ger vos porcs contre la PPA, nous rappelons aux propri\u00e9taires de porcs les points suivants\u00a0:</p>\n\n<ul>\n<li>si vous constatez que votre porc montre des signes de PPA, isolez-le imm\u00e9diatement;</li>\n<li>signalez tout cas soup\u00e7onn\u00e9 de PPA <a href=\"/fra/1300462382369/1300462438912\">\u00e0 votre v\u00e9t\u00e9rinaire de l'ACIA local</a>;</li>\n<li>si vous avez l'intention de voyager avec un porc \u00e0 l'ext\u00e9rieur du Canada, il est possible que vous ne puissiez le ramener au Canada.</li>\n</ul>\n\n<p>Tous les porcs sont susceptibles de contracter les m\u00eames maladies animales, peu importe o\u00f9 ils vivent. C'est pour cette raison que l'Agence canadienne d'inspection des aliments (ACIA) ne fait aucune distinction entre un porc gard\u00e9 comme animal de compagnie et un porc \u00e9lev\u00e9 \u00e0 des fins d'alimentation.</p>\n\n<p>Si vous pr\u00e9voyez voyager \u00e0 l'ext\u00e9rieur du Canada et y revenir avec votre porc d'origine canadienne, assurez-vous de conna\u00eetre les <a href=\"/sante-des-animaux/animaux-terrestres/exportation/porcin/fra/1405795282433/1405795283542\">conditions d'exportation</a> ainsi que les <a href=\"/fra/1516899032767/1516899033337\">conditions d'importation</a> pour revenir au Canada. Les conditions d'importation doivent \u00eatre respect\u00e9es pour les porcs de reproduction, y compris des analyses de d\u00e9pistage et la mise en quarantaine lorsqu'ils reviennent au Canada apr\u00e8s avoir voyag\u00e9 \u00e0 l'\u00e9tranger.</p>\n\n<p>Veuillez consulter notre <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fiche-de-renseignements/fra/1306993248674/1306993787261\">foire aux questions au sujet de la PPA</a> pour obtenir des renseignements au sujet de cette maladie animale.</p>\n\n<h2>Trousse d'outils de sensibilisation</h2>\n\n<p>Nous avons tous un r\u00f4le \u00e0 jouer dans la r\u00e9duction des risques associ\u00e9s aux maladies animales. Faites votre part en partageant cette trousse d'outils dans votre r\u00e9seau. Vous y retrouverez des vid\u00e9os utiles, des fiches de renseignements, des affiches et des images pour vous aider \u00e0 transmettre des renseignements\u00a0\u2013\u00a0plut\u00f4t que des maladies animales.</p>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/infographie/fra/1516994052131/1516994052834\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/travelling_pigs_asf_1556033937376_fra.jpg\" class=\"img-responsive thumbnail\" alt=\"Infographie\u00a0: Ce que vous devez savoir si vous pr\u00e9voyez voyager avec un cochon\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/infographie/fra/1516994052131/1516994052834\">Infographie\u00a0: Ce que vous devez savoir si vous pr\u00e9voyez voyager avec un cochon</a></strong></p>\n<p>Toutes les races de cochons, qu'il s'agisse d'animaux de compagnie ou de ferme, sont vuln\u00e9rables aux m\u00eames maladies graves.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/impact-economique/fra/1546889082597/1546889294408\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/economic_impact_asf_1556033444182_fra.png\" class=\"img-responsive thumbnail\" alt=\"Infographie : Impact \u00e9conomique\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/impact-economique/fra/1546889082597/1546889294408\">Infographie\u00a0: Impact \u00e9conomique</a></strong></p>\n<p>La peste porcine africaine pose un risque significatif \u00e0 l'industrie porcine canadienne et \u00e0 l'\u00e9conomie canadienne.</p>\n</div>\n</div>\n\n<h2>Autres outils de sensibilisation provenant de tiers</h2>\n\n<ul>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">Outils de sensibilisation de l'Organisation mondiale de la sant\u00e9 animale (OIE) </a></li>\n<li><a href=\"https://www.youtube.com/watch?time_continue=7&amp;v=eyQ4t1wHl2M\">Vid\u00e9o\u00a0: <span lang=\"en\">African swine fever\u00a0\u2013\u00a0How to stay one step ahead</span> (en anglais seulement) </a></li>\n</ul>\n\n<h2>Ressources pour les propri\u00e9taires de cochons de compagnie</h2>\n\n<ul>\n<li><a href=\"http://www.cfsph.iastate.edu/DiseaseInfo/disease-images-fr.php?name=classical-swine-fever&amp;lang=fr\">Photos des signes cliniques de la peste porcine africaine</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/declarez-les-maladies-animales/fra/1363844638670/1363844792607\">D\u00e9clarez les maladies animales</a></li>\n<li><a href=\"https://www.veterinairesaucanada.net/\">Association canadienne des m\u00e9decins v\u00e9t\u00e9rinaires</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}