{
    "dcr_id": "1545964031883",
    "title": {
        "en": "Completing a portal task in My CFIA",
        "fr": "Effectuer une t\u00e2che de portail dans Mon ACIA"
    },
    "html_modified": "2024-02-13 9:53:33 AM",
    "modified": "2019-01-21",
    "issued": "2019-01-02",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/completing_a_portal_task_1545964031883_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/completing_a_portal_task_1545964031883_fra"
    },
    "ia_id": "1545964134108",
    "parent_ia_id": "1545974178498",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1545974178498",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Completing a portal task in My CFIA",
        "fr": "Effectuer une t\u00e2che de portail dans Mon ACIA"
    },
    "label": {
        "en": "Completing a portal task in My CFIA",
        "fr": "Effectuer une t\u00e2che de portail dans Mon ACIA"
    },
    "templatetype": "content page 1 column",
    "node_id": "1545964134108",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1545974154373",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/my-cfia-account/user-guidance/completing-a-portal-task-in-my-cfia/",
        "fr": "/a-propos-de-l-acia/compte-mon-acia/orientation-de-l-utilisateur/effectuer-une-tache-de-portail-dans-mon-acia/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Completing a portal task in My CFIA",
            "fr": "Effectuer une t\u00e2che de portail dans Mon ACIA"
        },
        "description": {
            "en": "After you've submitted a new service request in My CFIA, you may receive a portal task notification. A portal task notification is an email message sent to you if the CFIA needs more information before processing a service request that you submitted through My CFIA.",
            "fr": "Apr\u00e8s que vous avez pr\u00e9sent\u00e9 une nouvelle demande de service dans Mon ACIA, vous pouvez recevoir une \u00ab\u00a0notification de t\u00e2che de portail\u00a0\u00bb. Une notification de t\u00e2che de portail est un message courriel qui vous est envoy\u00e9 si l'ACIA a besoin de plus de renseignements avant de traiter votre demande de service pr\u00e9sent\u00e9e par l'interm\u00e9diaire de Mon ACIA."
        },
        "keywords": {
            "en": "My CFIA, service, business, online, electronic, secure, completing a portal task",
            "fr": "Mon ACIA, services, \u00e9lectroniques, op\u00e9rations, en ligne, Effectuer une t\u00e2che de portail"
        },
        "dcterms.subject": {
            "en": "government communications,government information,administrative services",
            "fr": "communications gouvernementales,information gouvernementale,services administratifs"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-02",
            "fr": "2019-01-02"
        },
        "modified": {
            "en": "2019-01-21",
            "fr": "2019-01-21"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "educators,business,government,general public",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Completing a portal task in My CFIA",
        "fr": "Effectuer une t\u00e2che de portail dans Mon ACIA"
    },
    "body": {
        "en": "        \r\n        \n<p>After you've submitted a new service request in My CFIA, you may receive a \"portal task notification\". A portal task notification is an email message sent to you if the CFIA needs more information before processing a service request that you submitted through My CFIA.</p>\n<p>Portal task notifications are sent to the person who submitted the application for the service and may look like this:</p>\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md small\">\n<section>\n<p lang=\"fr\">Le texte fran\u00e7ais suit le texte anglais</p>\n<p>A service request requires your attention.</p>\n<p>You may log in and access the service request task here\u00a0: http://www.inspection.gc.ca/about-the-cfia/my-cfia/eng/1482204298243/1482204318353. Please provide the information or detail as appropriate to avoid request processing delays.</p>\n<p>If you have any questions about the task you may contact the CFIA at:<br>\n</p>\n<p>Thank you,<br>\n</p>\n\n\n<p lang=\"fr\">Une demande de service n\u00e9cessite votre attention.</p>\n\n<p lang=\"fr\">Vous pouvez vous connecter et acc\u00e9der \u00e0 la demande ici\u00a0: http://www.inspection.gc.ca/au-sujet-de-l-acia/mon-acia/fra/1482204298243/1482204318353. Veuillez fournir l'information ou les renseignements d\u00e9taill\u00e9s pour \u00e9viter des retards dans le traitement de la demande.</p>\n\n\n<p lang=\"fr\">Si vous avez des questions \u00e0 ce sujet, vous pouvez communiquer avec l'ACIA en composant le num\u00e9ro de t\u00e9l\u00e9phone suivant\u00a0:<br>\n</p>\n<p lang=\"fr\">Merci.<br>\n</p>\n\n\n</section>\n</div>\n<p>To complete the portal task, you must <a href=\"https://services.inspection.gc.ca/setLangEn?url=https%3A%2F%2Fservices.inspection.gc.ca%2Fmycfia-monacia%2F\">sign in to your My CFIA account</a>.</p>\n<p>Once signed in, make sure your validated party associated with the service request is selected by clicking the white drop down box under \"Party Administration\", clicking on the proper validated party and clicking \"Select\":</p>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Screen capture of the My CFIA dashboard page, showing how to select the correct party from the drop down list.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img1_1545961420892_eng.jpg\">\n<p>Now scroll down to the \"Service Request\" section of your party profile dashboard, where you will be presented with all previously submitted service requests. This section should look similar to this:</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Screen capture of the Service Request Dashboard. Description follows.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img2_1545961435322_eng.jpg\">\n<details>\n<summary>Description of Service Request Dashboard screen capture</summary>\n<p>On the Service request tab: Welcome to your Service Request Dashboard. On this page you can view your current or past service requests and create new service requests.</p>\n<p>Link: New Service Request</p>\n<p>Link: Search Service Requests</p>\n<p>Showing results for Service Requests for: NutriFood1 (Validated Party)</p>\n<p>Search results appear in a table that includes the following columns:</p>\n<ul>\n<li>Request ID (hyperlinked)</li>\n<li>Application Name</li>\n<li>Service Request Type</li>\n<li>Created On</li>\n<li>Date of Submission</li>\n<li>Status</li>\n<li>Status Reason</li>\n<li>Creator/Submitter</li>\n<li>You can sort the table by column heading or filter using keywords.</li>\n</ul>\n\n<p>You may scroll through your previous service requests or use the filter to find the required service request. Service requests requiring your attention are listed under \"Status Reason\" as <strong>'On Hold \u2013 Awaiting Information from Applicant'</strong>.</p>\n</details>\n</figure>\n\n<p>Click on the \"Request ID\" link of the service request that requires your attention as directed below:</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Screen capture of a service request in the Service Request Dashboard. Description follows.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img3_1545961470827_eng.jpg\">\n\n<details>\n<summary>Description of Service Request in the Service Request Dashboard</summary>\n<p>Showing results for Service Requests for: NutriFood1 (Validated Party)</p>\n<p>Service Request appears in table with following columns:</p>\n<ul>\n<li>Request ID (hyperlinked) with an arrow pointing to the Request ID result</li>\n<li>Application Name</li>\n<li>Service Request Type</li>\n<li>Created On</li>\n<li>Date of Submission</li>\n<li>Status</li>\n<li>Status Reason</li>\n<li>Creator/Submitter</li>\n<li>You can sort the table by column heading or filter using keywords.</li>\n</ul>\n</details>\n</figure>\n\n<p>After clicking the \"Request ID\" link, further details related to your service request will be displayed. Tasks requiring your attention are marked as 'Incomplete' under \"Status\". Scroll down to the tasks section on this page, and click the \"<strong>Action/Edit</strong>\" link, as indicated below:</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Screen capture of task required listed on the task required dashboard. Description follows.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img4_1545961484758_eng.jpg\">\n<details>\n<summary>Description of Tasks required in the Tasks Dashboard</summary>\n<p>Showing results for Tasks required for \"Status: Active\" requests</p>\n<p>Tasks appear in table with the following columns:</p>\n<p>Name</p>\n<p>Description</p>\n<p>Status (example shows 'Incomplete' status circled)</p>\n<p>Action/Edit (hyperlinked) with an arrow pointing to the link</p>\n</details>\n</figure>\n\n<p>You will then be presented with the \"Task Information\" page.</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Screen capture of Task Information. Description follows.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img5_1545961580886_eng.jpg\">\n<details>\n<summary>On this page, you will find information on what needs to be completed before your service request can be processed.</summary>\n<ol>\n<li>The <strong>Description</strong> box provides information as to what is required from you to proceed with the application.</li>\n<li>To send additional documents to the CFIA for review, click on the <strong>Upload Documents</strong> button.</li>\n<li>You may provide additional information by adding comments to the <strong>Comments</strong> section.</li>\n<li>After you have provided the missing information, click on <strong>Mark Task Complete</strong> to complete the portal task.</li>\n</ol>\n</details>\n</figure>\n<p>Once the task has been completed, the CFIA will review the information and continue with processing your application. We may reach out to you again should additional information be required.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Apr\u00e8s que vous avez pr\u00e9sent\u00e9 une nouvelle demande de service dans Mon ACIA, vous pouvez recevoir une \u00ab\u00a0notification de t\u00e2che de portail\u00a0\u00bb. Une notification de t\u00e2che de portail est un message courriel qui vous est envoy\u00e9 si l'ACIA a besoin de plus de renseignements avant de traiter votre demande de service pr\u00e9sent\u00e9e par l'interm\u00e9diaire de Mon ACIA.</p>\n<p>Les notifications de t\u00e2che de portail sont envoy\u00e9es \u00e0 la personne qui a pr\u00e9sent\u00e9 la demande pour le service et elles peuvent ressembler \u00e0 ceci\u00a0:</p>\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md small\">\n<section>\n<p lang=\"en\">English text will follow French text.</p>\n<p>Une demande de service n\u00e9cessite votre attention.</p>\n<p>Vous pouvez vous connecter et acc\u00e9der \u00e0 la demande ici\u00a0: http://www.inspection.gc.ca/au-sujet-de-l-acia/mon-acia/fra/1482204298243/1482204318353. Veuillez fournir l'information ou les renseignements d\u00e9taill\u00e9s pour \u00e9viter des retards dans le traitement de la demande.</p>\n<p>Si vous avez des questions \u00e0 ce sujet, vous pouvez communiquer avec l'ACIA en composant le num\u00e9ro de t\u00e9l\u00e9phone suivant\u00a0:<br>\n</p>\n<p>Merci.<br>\n</p>\n\n\n<p lang=\"en\">A service request requires your attention.</p>\n\n<p lang=\"en\">You may log in and access the service request task here\u00a0: http://www.inspection.gc.ca/about-the-cfia/my-cfia/eng/1482204298243/1482204318353. Please provide the information or detail as appropriate to avoid request processing delays.</p>\n<p lang=\"en\">If you have any questions about the task you may contact the CFIA at:<br>\n</p>\n<p lang=\"en\">Thank you,<br>\n</p>\n</section>\n\n</div>\n\n<p>Pour terminer la t\u00e2che de portail, vous devez vous <a href=\"https://services.inspection.gc.ca/setLangFr?url=https%3A%2F%2Fservices.inspection.gc.ca%2Fmycfia-monacia%2F\">connecter sur votre compte Mon ACIA</a>.</p>\n<p>Une fois connect\u00e9, assurez-vous que votre partie valid\u00e9e associ\u00e9e \u00e0 la demande de service est s\u00e9lectionn\u00e9e en cliquant sur le menu d\u00e9roulant blanc sous \u00ab\u00a0Administration de la partie\u00a0\u00bb, cliquez sur la bonne partie valid\u00e9e et cliquez sur \u00ab\u00a0S\u00e9lectionner\u00a0\u00bb.</p>\n<p><img class=\"img-responsive mrgn-bttm-xl\" alt=\"Capture d'\u00e9cran de la page du tableau de bord de Mon ACIA, montrant la mani\u00e8re de s\u00e9lectionner la bonne partie dans la liste du menu d\u00e9roulant.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img1_1545961420892_fra.jpg\">\n</p>\n<p>Maintenant, d\u00e9filez vers le bas jusqu'\u00e0 la section appel\u00e9e \u00ab\u00a0Demande de service\u00a0\u00bb du tableau de bord de votre profil de partie, o\u00f9 on vous pr\u00e9sentera toutes les demandes de service pr\u00e9sent\u00e9es auparavant. Cette section devrait ressembler \u00e0 ceci\u00a0:</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Capture d'\u00e9cran du tableau de bord des demandes de service. La description suit.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img2_1545961435322_fra.jpg\">\n<details>\n<summary>Description de la capture d'\u00e9cran du tableau de bord des demandes de service</summary>\n<p>Sur l'onglet de demande de service\u00a0: Bienvenue \u00e0 votre tableau de bord de demande de service. Sur cette page, vous pouvez voir vos demandes de services actuelles ou pass\u00e9es ainsi que cr\u00e9er de nouvelles demandes de service.</p>\n<p>Lien\u00a0: Nouvelle demande de service</p>\n<p>Lien\u00a0: Recherche de demande de service</p>\n<p>Montrer les r\u00e9sultats pour les demandes de services pour\u00a0: NutriFood1 (Partie valid\u00e9e)</p>\n<p>Les r\u00e9sultats de la recherche apparaissent dans un tableau qui contient les colonnes suivantes\u00a0:</p>\n<ul>\n<li>ID de la demande (hyperlien)</li>\n<li>Nom de l'application</li>\n<li>Type de demande de service</li>\n<li>Cr\u00e9\u00e9 le</li>\n<li>Date de pr\u00e9sentation</li>\n<li>\u00c9tat</li>\n<li>Raison de l'\u00e9tat</li>\n<li>Cr\u00e9ateur ou pr\u00e9sentateur</li>\n<li>Vous pouvez trier le tableau par titre de colonne ou filtrer en utilisant des mots-cl\u00e9s.</li>\n</ul>\n\n<p>Vous pouvez faire d\u00e9filer vos demandes de services pr\u00e9c\u00e9dentes ou utiliser le filtre pour trouver la demande de service recherch\u00e9e. Les demandes de service n\u00e9cessitant votre attention sont \u00e9num\u00e9r\u00e9es sous la \u00ab\u00a0raison de l'\u00e9tat\u00a0\u00bb comme \u00e9tant <strong>\u00ab\u00a0En suspens \u2013 en attente de renseignements du demandeur\u00a0\u00bb</strong>.</p>\n</details>\n</figure>\n\n<p>Cliquez sur le lien \u00ab\u00a0ID de la demande\u00a0\u00bb de la demande de service n\u00e9cessitant votre attention comme indiqu\u00e9 ci-dessous\u00a0:</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Capture d'\u00e9cran d'une demande de service dans le tableau de bord des demandes de service. La description suit.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img3_1545961470827_fra.jpg\">\n\n<details>\n<summary>Description d'une demande de service dans le tableau de bord des demandes de service</summary>\n<p>Montrer les r\u00e9sultats pour les demandes de services pour\u00a0: NutriFood1 (Partie valid\u00e9e)</p>\n<p>La demande de service appara\u00eet dans le tableau qui contient les colonnes suivantes\u00a0:</p>\n<ul>\n<li>ID de la demande (hyperlien) avec une fl\u00e8che pointant vers le r\u00e9sultat de l'ID de la demande</li>\n<li>Nom de l'application</li>\n<li>Type de demande de service</li>\n<li>Cr\u00e9\u00e9 le</li>\n<li>Date de pr\u00e9sentation</li>\n<li>\u00c9tat</li>\n<li>Raison de l'\u00e9tat</li>\n<li>Cr\u00e9ateur ou pr\u00e9sentateur</li>\n<li>Vous pouvez trier le tableau par titre de colonne ou filtrer en utilisant des mots-cl\u00e9s.</li>\n</ul>\n</details>\n</figure>\n\n<p>Apr\u00e8s avoir cliqu\u00e9 sur le lien \u00ab\u00a0ID de la demande\u00a0\u00bb, de plus amples d\u00e9tails concernant votre demande de service seront affich\u00e9s. Les t\u00e2ches exigeant votre attention sont marqu\u00e9es comme \u00ab\u00a0incompl\u00e8te\u00a0\u00bb sous la colonne appel\u00e9e \u00ab\u00a0\u00c9tat\u00a0\u00bb. Faites d\u00e9filer vers le bas vers la section des t\u00e2ches sur cette page et cliquez sur le lien \u00ab\u00a0<strong>Action/modification\u00a0\u00bb</strong> comme indiqu\u00e9 ci-dessous\u00a0:</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Capture d'\u00e9cran de la t\u00e2che n\u00e9cessaire \u00e9num\u00e9r\u00e9e dans le tableau de bord des t\u00e2ches n\u00e9cessaires. La description suit.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img4_1545961484758_fra.jpg\">\n<details>\n<summary>Description des t\u00e2ches n\u00e9cessaires dans le tableau de bord des t\u00e2ches</summary>\n<p>Montrer les r\u00e9sultats pour les t\u00e2ches pour les demandes indiqu\u00e9es comme \u00ab\u00a0\u00c9tat\u00a0: actif\u00a0\u00bb</p>\n<p>La t\u00e2che appara\u00eet dans un tableau qui contient les colonnes suivantes\u00a0:</p>\n<p>Nom</p>\n<p>Description</p>\n<p>\u00c9tat (l'exemple montre l'\u00e9tat \u00ab\u00a0incomplet\u00a0\u00bb encercl\u00e9)</p>\n<p>Action/modification (hyperlien) avec une fl\u00e8che pointant vers le lien</p>\n</details>\n</figure>\n\n<p>Vous verrez appara\u00eetre la page \u00ab\u00a0Renseignements sur la t\u00e2che\u00a0\u00bb.</p>\n<figure>\n<img class=\"img-responsive mrgn-bttm-xl\" alt=\"Capture d'\u00e9cran des renseignements sur la t\u00e2che. La description suit.\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/completing_a_portal_task_img5_1545961580886_fra.jpg\">\n<details>\n<summary>Sur cette page, vous trouverez de l'information sur ce qui doit \u00eatre termin\u00e9 avant que votre demande de service ne soit trait\u00e9e.</summary>\n<ol>\n<li>L'encadr\u00e9 de <strong>Description</strong> fournit de l'information sur ce qui est exig\u00e9 de vous pour poursuivre avec la demande.</li>\n<li>Pour envoyer des documents suppl\u00e9mentaires \u00e0 l'ACIA pour examen, cliquez sur le bouton <strong>T\u00e9l\u00e9charger des documents</strong>.</li>\n<li>Vous pouvez fournir de l'information suppl\u00e9mentaire en ajoutant des commentaires dans la section <strong>Commentaires</strong>.</li>\n<li>Apr\u00e8s avoir fourni l'information manquante, cliquez sur <strong>Marquer la t\u00e2che termin\u00e9e</strong> pour terminer la t\u00e2che de portail.</li>\n</ol>\n</details>\n</figure>\n<p>Une fois la t\u00e2che termin\u00e9e, l'ACIA examinera l'information et continuera le traitement de votre demande. Nous pouvons communiquer avec vous \u00e0 nouveau si de l'information suppl\u00e9mentaire est n\u00e9cessaire.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}