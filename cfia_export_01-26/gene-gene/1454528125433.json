{
    "dcr_id": "1454528125433",
    "title": {
        "en": "Olive Oil Compliance",
        "fr": "Conformit\u00e9 aux exigences relatives \u00e0 l'huile d'olive"
    },
    "html_modified": "2024-01-26 2:23:08 PM",
    "modified": "2016-02-03",
    "issued": "2016-02-19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/olive_oil_compliance_1454528125433_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/olive_oil_compliance_1454528125433_fra"
    },
    "ia_id": "1454528283159",
    "parent_ia_id": "1626115225080",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling|Meeting a standard of identity",
        "fr": "\u00c9tiquetage|Respect d\u2019une norme d\u2019identit\u00e9"
    },
    "commodity": {
        "en": "Other",
        "fr": "Autre"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1626115225080",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Olive Oil Compliance",
        "fr": "Conformit\u00e9 aux exigences relatives \u00e0 l'huile d'olive"
    },
    "label": {
        "en": "Olive Oil Compliance",
        "fr": "Conformit\u00e9 aux exigences relatives \u00e0 l'huile d'olive"
    },
    "templatetype": "content page 1 column",
    "node_id": "1454528283159",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1626115110031",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/fats-and-oils/olive-oil/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/matieres-grasses-et-huiles/huile-d-olive/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Olive Oil Compliance",
            "fr": "Conformit\u00e9 aux exigences relatives \u00e0 l'huile d'olive"
        },
        "description": {
            "en": "Products represented or sold as olive oil or pure olive oil are subject to the standard of composition for olive oil. Importers, distributors, and retailers of olive oil are responsible for ensuring compliance with the legal requirements.",
            "fr": "Les produits pr\u00e9sent\u00e9s ou vendus  comme huile d'olive ou huile d'olive pure sont assujettis \u00e0 la norme de composition de ce produit. Les importateurs, distributeurs et d\u00e9taillants d'huile d'olive doivent se conformer aux exigences de la loi."
        },
        "keywords": {
            "en": "label, labelling, advertising, retail food, decisions, nutrition, claims, Food and Drugs Act and Regulations, Consumer Packaging and Labelling Act and Regulations, fats, fatty acids, importation, distribution, olive oil",
            "fr": "\u00e9tiquette, \u00e9tiquetage, annonce, aliments au d\u00e9tail, d\u00e9cisions, nutrition, all\u00e9gations, Loi sur les aliments et drogues, R\u00e8glement sur les aliments, Loi sur l'emballage et l'\u00e9tiquetage des produits de consommation, mati\u00e8res grasses, acide gras, importateurs, distributeurs huile d'olive"
        },
        "dcterms.subject": {
            "en": "retail trade,consumers,food labeling,food inspection,handbooks,consumer protection",
            "fr": "commerce de d\u00e9tail,consommateur,\u00e9tiquetage des aliments,inspection des aliments,manuel,protection du consommateur"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Domestic Food Safety Systems & Meat Hygiene Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des syst\u00e8mes de la salubrit\u00e9 alimentaire et de l'hygi\u00e8ne des viandes"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-02-19",
            "fr": "2016-02-19"
        },
        "modified": {
            "en": "2016-02-03",
            "fr": "2016-02-03"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Olive Oil Compliance",
        "fr": "Conformit\u00e9 aux exigences relatives \u00e0 l'huile d'olive"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The purpose of this document is two-fold. First, it is intended to remind Canadian olive oil importers, distributors, and retailers about the Canadian regulatory requirements and policies affecting the importation, distribution, and sale of olive oil. Second, it is intended to affirm their legal responsibility to ensure full compliance with these requirements.</p>\n<h2>Canadian Regulations</h2>\n<p>All food products sold in Canada must meet the general labelling and compositional requirements of the federal <i>Food and Drugs Act</i> (FDA) and <i>Food and Drug Regulations</i> (FDR).</p>\n<p>Products that are represented or sold as olive oil or pure olive oil are subject to the standard of composition for this product, as stated in Section B.09.003 of the <abbr title=\"Food and Drug Regulations\">FDR</abbr>. This standard states, in part:</p>\n<h2>Olive Oil or Sweet Oil</h2>\n<p>(a) shall be the oil obtained from the fruit of the olive tree (<i lang=\"la\">Olea europaea</i> <abbr title=\"Linnaeus\">L</abbr>);</p>\n<p>The addition of vegetable oil(s) or of olive pomace oil to a product being represented as olive oil, is not permitted. This is considered adulteration and a fraudulent practice that violates the regulations and subsection 5 (1) of the Act which prohibits false or misleading statements or claims about a food.</p>\n<p>In addition to meeting the requirements of Section B.09.003 of the <abbr title=\"Food and Drug Regulations\">FDR</abbr>, products being represented or sold as Virgin or Extra Virgin Olive oil are expected to meet the standards and definitions of the International Olive Oil Council. These standards require, among other things, oils to be cold pressed products that do not contain any refined olive oil, and make a distinction between \"virgin\" and \"extra virgin\" olive oils based on free fatty acid content.</p>\n<h2>Penalty for Violations</h2>\n<p>Failure to comply with the above requirements in respect to the importation, distribution, or sale of olive oil will be considered a potential violation of subsection 5(1) of the <i>Food and Drugs Act</i>. Violations are subject to enforcement action under the Act, up to and including prosecution. Penalties are provided under the Act of up to $50,000 and/or imprisonment for up to six months on summary conviction or $250,000 and/or imprisonment for up to three years for conviction on indictment.</p>\n<h2>Obligations of Industry</h2>\n<p>Importers, distributors, and retailers of olive oil are responsible for ensuring compliance with the legal requirements. It is recommended that members of the industry seek assurance from their suppliers or through their own analytical efforts that the products they receive and offer for sale meet these requirements.</p>\n<h2>Surveillance Activities of the Canadian Food Inspection Agency</h2>\n<p>When testing olive oil for authenticity, the Canadian Food Inspection Agency (CFIA) uses analytical methods capable of detecting adulteration at very low levels.</p>\n<p>Although the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> has an on-going olive oil testing program, the Canadian olive oil industry is encouraged to increase its own testing efforts. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> requests industry's assistance to ensure that Canadian regulatory requirements are complied with and that consumers are protected from misrepresentation.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Ce document a deux objectifs\u00a0: (1) rappeler aux importateurs, aux distributeurs et aux d\u00e9taillants canadiens d'huile d'olive les exigences r\u00e9glementaires et les politiques en vigueur au Canada concernant l'importation, la distribution et la vente d'huile d'olive; (2) affirmer qu'ils sont l\u00e9galement justiciables, afin d'assurer la pleine conformit\u00e9 \u00e0 ces exigences.</p>\n<h2>R\u00e8glements en vigueur au Canada</h2>\n<p>Tous les produits alimentaires vendus au Canada doivent satisfaire aux exigences g\u00e9n\u00e9rales d'\u00e9tiquetage et de composition \u00e9nonc\u00e9es dans la <i>Loi sur les aliments et drogues</i> (LAD) et dans le <i>R\u00e8glement sur les  aliments et drogues</i> (RAD).</p>\n<p>Les produits pr\u00e9sent\u00e9s comme huile d'olive ou huile d'olive pure ou vendus comme telle sont assujettis \u00e0 la norme de composition \u00e9nonc\u00e9e dans l'article\u00a0B.09.003 du <i>R\u00e8glement sur les aliments et drogues </i> (RAD), lequel porte que\u00a0:</p>\n<h2>Huile d'olive ou huile douce</h2>\n<p>a) doit \u00eatre l'huile obtenue du fruit de l'olivier (<i lang=\"la\">Olea\u00a0europaea</i>\u00a0<abbr title=\"Linnaeus\">L</abbr>);</p>\n<p>Est interdit l'ajout d'une ou de plusieurs huiles v\u00e9g\u00e9tales ou de l'huile de grignons d'olive \u00e0 un produit pr\u00e9sent\u00e9 comme \u00e9tant de l'huile d'olive. Cela est consid\u00e9r\u00e9 comme une adult\u00e9ration et une pratique frauduleuse qui contrevient au r\u00e8glement et au paragraphe\u00a05(1) de la <i>Loi sur les aliments et drogues</i>, qui interdit les all\u00e9gations ou les d\u00e9clarations fausses, trompeuses ou mensong\u00e8res sur un aliment.</p>\n<p>En plus de satisfaire aux exigences de l'article\u00a0B.09.003 du <i>R\u00e8glement sur les aliments et drogues</i>, les produits pr\u00e9sent\u00e9s ou vendus sous le nom d'huile d'olive vierge ou d'huile d'olive extra vierge doivent satisfaire aux normes et aux d\u00e9finitions du Conseil ol\u00e9icole international. Entre autres, ces normes exigent que ces huiles soient des produits press\u00e9s \u00e0 froid, qui ne renferment aucune huile d'olive raffin\u00e9e, et \u00e9tablissent une distinction entre l'huile vierge et l'huile extra vierge, distinction qui se fait sur la teneur en acides gras libres.</p>\n<h2>Sanctions des infractions</h2>\n<p>La non-conformit\u00e9 aux exigences qui pr\u00e9c\u00e8dent sur l'importation, la distribution ou la vente d'huile d'olive sera consid\u00e9r\u00e9e comme une infraction possible au paragraphe\u00a05(1) de la Loi. Les infractions sont passibles des sanctions pr\u00e9vues par la loi, y compris de poursuites\u00a0: amende d'au plus 50\u00a0000\u00a0$ ou emprisonnement de six\u00a0mois, ou les deux\u00a0peines \u00e0 la fois, sur d\u00e9claration sommaire de culpabilit\u00e9; amende d'au plus 250\u00a0000\u00a0$ ou emprisonnement de trois\u00a0ans, ou les deux\u00a0peines \u00e0 la fois, en cas de d\u00e9claration de culpabilit\u00e9 sur acte d'accusation.</p>\n<h2>Obligations de l'industrie</h2>\n<p>Les importateurs, les distributeurs et les d\u00e9taillants d'huile d'olive sont tenus de s'assurer qu'ils se conforment aux exigences de la loi. Il est recommand\u00e9 aux membres de l'industrie d'obtenir de leurs fournisseurs l'assurance que les produits qu'ils re\u00e7oivent et vendent satisfont \u00e0 ces exigences ou de s'en assurer eux-m\u00eames par leurs propres services d'analyse.</p>\n<h2>Surveillance exerc\u00e9e par l'Agence canadienne d'inspection des aliments</h2>\n<p>Lorsqu'elle contr\u00f4le l'authenticit\u00e9 de l'huile d'olive, l'Agence canadienne d'inspection des aliments (ACIA) utilise des m\u00e9thodes d'analyse capables de d\u00e9celer de tr\u00e8s faibles taux d'adult\u00e9ration.</p>\n<p>M\u00eame si l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> s'est dot\u00e9e d'un programme permanent de contr\u00f4le de l'huile d'olive, l'industrie canadienne de l'huile d'olive est incit\u00e9e \u00e0 accentuer ses propres efforts de contr\u00f4le. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> demande le concours de l'industrie pour assurer le respect des r\u00e8glements canadiens et la protection des consommateurs contre les fausses repr\u00e9sentations.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}