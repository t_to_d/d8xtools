{
    "dcr_id": "1303156723465",
    "title": {
        "en": "African Horse Sickness",
        "fr": "Peste \u00e9quine"
    },
    "html_modified": "2024-03-12 3:55:13 PM",
    "modified": "2012-12-27",
    "issued": "2011-04-18 15:58:45",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_africequin_index_1303156723465_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_africequin_index_1303156723465_fra"
    },
    "ia_id": "1303791257036",
    "parent_ia_id": "1303768544412",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1303768544412",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "African Horse Sickness",
        "fr": "Peste \u00e9quine"
    },
    "label": {
        "en": "African Horse Sickness",
        "fr": "Peste \u00e9quine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1303791257036",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1303768471142",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/african-horse-sickness/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-equine/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "African Horse Sickness",
            "fr": "Peste \u00e9quine"
        },
        "description": {
            "en": "African swine fever (ASF) is a contagious viral disease of swine. The main symptoms of ASF are fever and hemorrhage, or internal bleeding.",
            "fr": "La peste porcine africaine (PPA) est une virose contagieuse des porcs. Les principaux sympt\u00f4mes de la PPA sont la fi\u00e8vre et des h\u00e9morragies internes."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, African swine fever, swine, pigs",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, peste \u00e9quine, cochon, porcs,"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,regulation,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,r\u00e9glementation,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Terrestrial Animal Health Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux terrestres"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-04-18 15:58:45",
            "fr": "2011-04-18 15:58:45"
        },
        "modified": {
            "en": "2012-12-27",
            "fr": "2012-12-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "African Horse Sickness",
        "fr": "Peste \u00e9quine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>African horse sickness (AHS) is an insect-borne disease affecting horses and related species such as mules, donkeys, and zebras. Horses are most severely affected by the disease. <abbr title=\"African horse sickness\">AHS</abbr> is caused by the <abbr title=\"African horse sickness\">AHS</abbr> virus, of which there are nine different strains. Any of the strains can cause disease with severity ranging from a mild fever to sudden death. <abbr title=\"African horse sickness\">AHS</abbr> causes fever and impairment of the respiratory and/or circulatory systems. The sickness can have a sudden onset and is highly fatal.</p>\n<p>In Canada, African horse sickness is a <a href=\"/animal-health/terrestrial-animals/diseases/reportable/eng/1303768471142/1303768544412\">reportable disease</a> under the <i>Health of Animals Act</i>, and all cases must be reported to the Canadian Food Inspection Agency (CFIA).</p>\n<h2>What information is available?</h2>\n<ul><li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-horse-sickness/countries-recognized-as-free-from-the-disease/eng/1306904228382/1306990647593\" title=\"African Horse Sickness - Countries that Canada recognizes as being free from the disease  \">Countries recognized as free from the disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-horse-sickness/fact-sheet/eng/1303160765186/1305050091685\" title=\"African Horse Sickness Fact Sheet\">Fact Sheet</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La peste \u00e9quine est transmise par des insectes qui s'attaquent aux chevaux et aux esp\u00e8ces apparent\u00e9es, comme les mulets, les \u00e2nes et les z\u00e8bres, mais elle touche plus durement les chevaux. Elle est caus\u00e9e par un virus dont il existe neuf souches diff\u00e9rentes qui toutes provoquent une maladie plus ou moins grave, allant d'une simple fi\u00e8vre \u00e0 une mort soudaine. La peste \u00e9quine se manifeste par de la fi\u00e8vre et des troubles des syst\u00e8mes respiratoire et/ou circulatoire. La maladie appara\u00eet soudainement et elle est tr\u00e8s souvent mortelle.</p>\n<p>Au Canada, la peste \u00e9quine est une <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fra/1303768471142/1303768544412\">maladie \u00e0 d\u00e9claration obligatoire</a> en vertu du <i>Loi sur la sant\u00e9 des animaux</i>, et tous les cas doivent \u00eatre d\u00e9clar\u00e9s \u00e0 l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition?</h2>\n<ul><li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-equine/fiche-de-renseignements/fra/1303160765186/1305050091685\" title=\"Fiche de renseignements - Peste \u00e9quine\">Fiche de renseignements</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-equine/pays-reconnus-indemnes-de-la-maladie/fra/1306904228382/1306990647593\" title=\"Peste \u00e9quine - Pays reconnus par le Canada comme \u00e9tant indemnes de la maladie\">Pays reconnus indemnes de la maladie</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}