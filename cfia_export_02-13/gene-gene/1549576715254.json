{
    "dcr_id": "1549576715254",
    "title": {
        "en": "The Saskatoon Laboratory",
        "fr": "Le laboratoire de Saskatoon"
    },
    "html_modified": "2024-02-13 9:53:39 AM",
    "modified": "2023-08-23",
    "issued": "2019-02-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_scnc_labratrs_ssktn_1549576715254_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_scnc_labratrs_ssktn_1549576715254_fra"
    },
    "ia_id": "1549576742564",
    "parent_ia_id": "1494878085588",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1494878085588",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The Saskatoon Laboratory",
        "fr": "Le laboratoire de Saskatoon"
    },
    "label": {
        "en": "The Saskatoon Laboratory",
        "fr": "Le laboratoire de Saskatoon"
    },
    "templatetype": "content page 1 column",
    "node_id": "1549576742564",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1494878032804",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-laboratories/saskatoon/",
        "fr": "/les-sciences-et-les-recherches/nos-laboratoires/saskatoon/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The Saskatoon Laboratory",
            "fr": "Le laboratoire de Saskatoon"
        },
        "description": {
            "en": "The Saskatoon Laboratory consists of three specialized centres located at two sites on the University of Saskatchewan campus and a third location at the Innovation Place Research Park.",
            "fr": "Le laboratoire de Saskatoon comprend trois centres sp\u00e9cialis\u00e9s, dont deux situ\u00e9s sur le campus de l\u2019Universit\u00e9 de la Saskatchewan et le troisi\u00e8me \u00e0 l\u2019Innovation Place Research Park."
        },
        "keywords": {
            "en": "Saskatoon Laboratory, laboratory",
            "fr": "laboratoire de Saskatoon, laboratoire"
        },
        "dcterms.subject": {
            "en": "education,guidelines",
            "fr": "\u00e9ducation,lignes directrices"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des communications strat\u00e9giques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2023-08-23",
            "fr": "2023-08-23"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "audience": {
            "en": "government,scientists",
            "fr": "gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Saskatoon Laboratory",
        "fr": "Le laboratoire de Saskatoon"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_ssktoon_text_1556837310330_eng.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">CFIA science laboratory brochure: The Saskatoon Laboratory <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2,400&nbsp;kb)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n-->\n\n<p>The Saskatoon Laboratory is located on the traditional territories of the Niitsitapi (Blackfoot) and the people of the Treaty 6 region, including the Cree, Nakoda and Saulteaux People. The area is also home to the M\u00e9tis Nation of Saskatchewan, Western Region IIa.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">About the Canadian Food Inspection Agency</h2></summary>\n\n<p>The Canadian Food Inspection Agency (CFIA) is a science-based regulator with a mandate to safeguard the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">food</a> supply, protect the health of <a href=\"/plant-health/eng/1299162629094/1299162708850\">plants</a> and <a href=\"/animal-health/eng/1299155513713/1299155693492\">animals</a>, and support market access. The Agency relies on high-quality, timely and relevant science as the basis of its program design and regulatory decision-making. Scientific activities inform the Agency's understanding of risks, provide evidence for developing mitigation measures, and confirm the effectiveness of these measures.</p>\n\n<p>CFIA scientific activities include laboratory testing, research, surveillance, test method development, risk assessments and expert scientific advice. Agency scientists maintain strong partnerships with universities, industry, and federal, provincial and international counterparts to effectively carry out the CFIA's mandate.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_saskatoon_1498061326055_eng.jpg\" class=\"img-responsive\" alt=\"Photograph - Saskatoon Laboratory building\">\n</div>\n<p>The Saskatoon Laboratory is located on the University of Saskatchewan campus and at the Innovation Place Research Park. The laboratory speciality areas are:</p>\n<ul>\n<li>Centre for Seed Science and Technology</li>\n<li>Centre for Veterinary Drug Residues</li>\n<li>Centre for Food-borne and Animal Parasitology</li>\n</ul>\n\n<p>The scientists at the Saskatoon laboratory are experts in parasitology, veterinary drug residues and related compounds, as well as seed science.</p>\n\n<h2>What we do</h2>\n\n<h3>Centre for Seed Science and Technology</h3>\n\n<p>This centre specializes in the quality assessment of seed and grain, as well as identifying crop and weed seeds, by using plant physiology, morphology and taxonomy.</p>\n\n<h4>National Seed Herbarium</h4>\n\n<ul>\n<li>Manage and expand Canada's only national reference collection for seed identification</li>\n<li>Provide seed identification services and specimen references to CFIA and commercial seed testing labs</li>\n</ul>\n\n<h4>Accreditation programs and proficiency testing</h4>\n\n<ul>\n<li>Provide accreditation or authorization for alternative service delivery of diagnostic tests that support CFIA seed, grains, oilseeds and invasive plant programs</li>\n<li>Evaluation of analysts and officially recognized foreign seed testing laboratories</li>\n<li>Deliver proficiency testing panels annually to Canadian and international labs</li>\n</ul>\n\n<h4>Research</h4>\n\n<ul>\n<li>Improve and publish official Canadian seed testing methods</li>\n<li>Develop and validate testing methods for seed and grain</li>\n<li>Develop seed identification methods and training resources</li>\n</ul>\n\n<h4>Diagnostic testing</h4>\n\n<ul>\n<li>Perform routine diagnostic testing, monitoring and surveillance of seed and grain to protect consumers and support trade</li>\n</ul>\n\n<h3>Centre for Veterinary Drug Residues</h3>\n\n<p>This is a well-equipped, modern analytical chemistry facility that conducts research and testing for drug residue in foods of animal origin.</p>\n\n<h4>Research</h4>\n\n<ul>\n<li>Methods of analysis for new drugs</li>\n<li>Methods of analysis for drugs of multiple classes with multiple residues</li>\n<li>Identify marker residues and perform drug residue depletion studies</li>\n<li>Identify unknown compounds</li>\n</ul>\n\n<h4>Testing of drug residues</h4>\n<ul class=\"list-unstyled\">\n<li>Drug residues such as:\n<ul>\n<li>antimicrobials</li>\n<li>growth promoters</li>\n<li>hormones and steroids</li>\n<li>banned substances</li>\n<li>other veterinary drugs and contaminants</li>\n</ul>\n</li>\n</ul>\n\n<h4>Proficiency testing</h4>\n\n<ul>\n<li>Provider of proficiency testing samples in a wide range of drugs</li>\n</ul>\n\n<h4>Key technologies</h4>\n\n<ul>\n<li>Tandem and high resolution mass spectrometry: a multistep process for sorting ions and accurately measuring mass</li>\n<li>Liquid and gas chromatography: uses a liquid or gas to separate a material for molecular analysis</li>\n</ul>\n\n<h3>Centre for Food-borne and Animal Parasitology</h3>\n\n<p>This national centre specializes in parasites of significance to food safety, animal health and/or trade.</p>\n\n\n<h4>Proficiency testing (PT)</h4>\n\n<ul>\n<li>Provide <i lang=\"la\">Trichinella</i> proficiency samples and related oversight to support alternative service delivery of CFIA domestic and export programs</li>\n</ul>\n\n<h4>Research</h4>\n\n<ul>\n<li>Develop and validate testing methods</li>\n<li>Detection and control of food-borne and animal parasites</li>\n<li>Molecular characterization</li>\n</ul>\n\n<h4>Diagnostic testing</h4>\n\n<ul>\n<li>Perform routine diagnostic testing, monitoring and surveillance of food-borne and animal parasites to protect consumers and livestock, and to support trade</li>\n<li>Implement a variety of techniques, from traditional parasitological (such as microscopic examination, culture) to serological (blood serum) and molecular detection methods</li>\n</ul>\n\n<h3>Reference Laboratory</h3>\n\n<p>The Saskatoon Laboratory is designated as a <a href=\"/science-and-research/our-laboratories/cfia-reference-laboratories-and-collaborating-cent/eng/1654548734146/1654548734678\">reference laboratory</a> by the World Organisation for Animal Health (WOAH; founded as Office International des \u00c9pizooties (OIE)) with expertise in trichinellosis (a disease caused by the roundworm <i lang=\"la\">Trichinella</i>).</p>\n<p>WOAH Reference Laboratories are designated to pursue the scientific and technical problems that relate to a specific disease or topic. Its role is to function as a centre of expertise and standardisation of diagnostic techniques for its designated disease.</p>\n<p>The reference laboratory is involved in cutting-edge research and training of highly skilled personnel from countries all over the world.</p>\n\n<h3>WOAH Collaborating Centre</h3>\n<p>The Saskatoon Laboratory is also designated as a <a href=\"/science-and-research/our-laboratories/cfia-reference-laboratories-and-collaborating-cent/eng/1654548734146/1654548734678\">collaborating centre</a> by the WOAH with expertise in food-borne zoonotic parasites (transmitted between animals and people).</p>\n\n\n<h2>Quality management</h2>\n\n<p>All CFIA laboratories are accredited in accordance with the International Standard ISO/IEC 17025, <i>General requirements for the competence of testing and calibration laboratories</i>. The Standards Council of Canada (SCC) provides accreditation for routine testing, test method development and non-routine testing, as identified on the laboratory's Scope of Accreditation on the <a href=\"https://www.scc.ca/en/accreditation/laboratories/canadian-food-inspection-agency\" title=\"Standards Council of Canada\">SCC</a> website. Accreditation formally verifies the CFIA's competence to produce accurate and reliable results. The results are supported by the development, validation and implementation of scientific methods, conducted by highly qualified personnel, using reliable products, services, and equipment, in a quality controlled environment. Participation in international proficiency testing programs further demonstrates that our testing is comparable to laboratories across Canada and around the world.</p>\n\n<h2>Physical address</h2>\n\n<p>Saskatoon Laboratory<br>\n<br>\n<br>\n</p>\n\n<h2>More information</h2>\n\n<ul>\n<li><a href=\"/inspect-and-protect/science-and-innovation/dr-ruojing-wang/eng/1572617316547/1572617316922\">Podcast with Dr.\u00a0Ruojing Wang, Head of the National Seed Herbarium</a></li>\n</ul>\n    \n<p>Learn about other <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_ssktoon_text_1556837310330_fra.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">Brochure du laboratoire scientifique de l'ACIA&nbsp;: Le laboratoire de Saskatoon <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2 400&nbsp;ko)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n-->\n<p>Le laboratoire de Saskatoon est situ\u00e9 sur les territoires traditionnels des Niitsitapi (Pieds-Noirs) et des peuples de la r\u00e9gion vis\u00e9e par le Trait\u00e9 no 6, notamment les Cris des plaines, les Nakoda et les Saulteux. La r\u00e9gion abrite \u00e9galement la M\u00e9tis Nation of Saskatchewan, Western Region IIa.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">\u00c0 propos de l'Agence canadienne d'inspection des aliments</h2></summary>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est un organisme de r\u00e9glementation \u00e0 vocation scientifique dont le mandat est de pr\u00e9server l'<a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">approvisionnement alimentaire</a>, de prot\u00e9ger la sant\u00e9 des <a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">v\u00e9g\u00e9taux</a> et des <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">animaux</a> et de favoriser l'acc\u00e8s aux march\u00e9s. L'Agence s'appuie sur des donn\u00e9es scientifiques de grande qualit\u00e9, opportunes et pertinentes pour concevoir ses programmes et prendre ses d\u00e9cisions r\u00e9glementaires. Les activit\u00e9s scientifiques permettent \u00e0 l'Agence de mieux comprendre les risques, de fournir des preuves pour \u00e9laborer des mesures d'att\u00e9nuation et de confirmer l'efficacit\u00e9 de ces mesures.</p>\n\n<p>Les activit\u00e9s scientifiques de l'ACIA comprennent les essais en laboratoire, la recherche, la surveillance, l'\u00e9laboration de m\u00e9thodes d'essai, l'\u00e9valuation des risques et les conseils d'experts scientifiques. Les scientifiques de l'Agence entretiennent des partenariats solides avec les universit\u00e9s, l'industrie et leurs homologues f\u00e9d\u00e9raux, provinciaux et internationaux afin de remplir efficacement le mandat de l'ACIA.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_saskatoon_1498061326055_fra.jpg\" class=\"img-responsive\" alt=\"Photographe - Laboratoire de Saskatoon\">\n</div>\n\n<p>Le laboratoire de Saskatoon est situ\u00e9 sur le campus de l'Universit\u00e9 de la Saskatchewan et au parc de recherche Innovation Place. Les domaines de sp\u00e9cialit\u00e9 du laboratoire sont les suivants\u00a0:</p>\n\n<ul>\n<li>le Centre pour la science et la technologie des semences;</li>\n<li>le Centre pour les r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires; et</li>\n<li>le Centre de parasitologie animale et d'origine alimentaire.</li>\n</ul>\n\n<p>Les scientifiques du laboratoire de Saskatoon sont des experts en parasitologie, en d\u00e9tection de r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires et compos\u00e9s connexes, ainsi qu'en sciences des semences.</p>\n\n<h2>Nos activit\u00e9s</h2>\n<h3>Centre pour la science et la technologie des semences</h3>\n<p>Ce centre se sp\u00e9cialise dans l'\u00e9valuation de la qualit\u00e9 des semences et des c\u00e9r\u00e9ales, ainsi que dans l'identification des semences de cultures et de mauvaises herbes, en utilisant la physiologie, la morphologie et la taxonomie des plantes.</p>\n<h4>Herbier national de semences</h4>\n<ul>\n<li>G\u00e9rer et d\u00e9velopper la seule collection de r\u00e9f\u00e9rence nationale du Canada pour l'identification des semences.</li>\n<li>Fournir des services d'identification de semences et des r\u00e9f\u00e9rences de sp\u00e9cimens \u00e0 l'ACIA et aux laboratoires commerciaux d'essais de semences.</li>\n</ul>\n<h4>Programmes d'accr\u00e9ditation et essais d'aptitude</h4>\n<ul>\n<li>Fournir l'accr\u00e9ditation ou l'autorisation pour la prestation de services alternatifs pour les tests de diagnostic qui appuient les programmes de l'ACIA sur les semences, les grains, les ol\u00e9agineux et les plantes envahissantes.</li>\n<li>\u00c9valuation des analystes et des laboratoires d'essais de semences \u00e9trangers officiellement reconnus.</li>\n<li>Fournir annuellement des panels d'essais d'aptitude aux laboratoires canadiens et internationaux.</li>\n</ul>\n<h4>Recherche</h4>\n<ul>\n<li>Am\u00e9liorer et publier les m\u00e9thodes d'essai canadiennes officielles des semences.</li>\n<li>\u00c9laborer et valider des m\u00e9thodes d'essai pour les semences et les c\u00e9r\u00e9ales.</li>\n<li>D\u00e9velopper des m\u00e9thodes d'identification des semences et des ressources de formation.</li>\n</ul>\n<h4>Essais diagnostiques</h4>\n<ul>\n<li>Effectuer des tests de diagnostic de routine, le suivi et la surveillance des semences et des c\u00e9r\u00e9ales pour prot\u00e9ger les consommateurs et soutenir le commerce.</li>\n</ul>\n<h3>Centre pour les r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires</h3>\n<p>Il s'agit d'une installation moderne et bien \u00e9quip\u00e9e de chimie analytique qui effectue des recherches et des essais sur les r\u00e9sidus de m\u00e9dicaments dans les aliments d'origine animale.</p>\n<h4>Recherche</h4>\n<ul>\n<li>M\u00e9thodes d'analyse pour les nouveaux m\u00e9dicaments.</li>\n<li>M\u00e9thodes d'analyse pour les m\u00e9dicaments de classes multiples avec des r\u00e9sidus multiples.</li>\n<li>Identification des r\u00e9sidus marqueurs et r\u00e9alisation d'\u00e9tudes d'\u00e9limination des r\u00e9sidus de m\u00e9dicaments.</li>\n<li>Identification des compos\u00e9s inconnus.</li>\n</ul>\n<h4>Analyse des r\u00e9sidus de m\u00e9dicaments</h4>\n<p>R\u00e9sidus de m\u00e9dicaments tels que\u00a0:</p>\n<ul>\n<li>antimicrobiens</li>\n<li>stimulateurs de croissance</li>\n<li>hormones et st\u00e9ro\u00efdes</li>\n<li>substances interdites</li>\n<li>autres m\u00e9dicaments v\u00e9t\u00e9rinaires et contaminants</li>\n</ul>\n<h4>Essais d'aptitude</h4>\n<ul>\n<li>Fournisseur d'\u00e9chantillons pour les essais d'aptitude d'une large gamme de m\u00e9dicaments.</li>\n</ul>\n<h4>Technologies cl\u00e9s</h4>\n<ul>\n<li>Spectrom\u00e9trie de masse en tandem et \u00e0 haute r\u00e9solution : un processus en plusieurs \u00e9tapes pour trier les ions et mesurer la masse avec pr\u00e9cision.</li>\n<li>Chromatographie en phases liquide et gazeuse : utiliser un liquide ou un gaz pour s\u00e9parer un mati\u00e8re en vue d'une analyse mol\u00e9culaire.</li>\n</ul>\n<h4>Centre de parasitologie animale et d'origine alimentaire</h4>\n<p>Ce centre national est sp\u00e9cialis\u00e9 dans les parasites ayant une importance pour la salubrit\u00e9 alimentaire, la sant\u00e9 animale ou le commerce.</p>\n<h4>Essais d'aptitude</h4>\n<p>Fournir des \u00e9chantillons d'aptitude pour Trichinella et la surveillance connexe pour soutenir la prestation de services alternatifs des programmes nationaux et d'exportation de l'ACIA.</p>\n<h4>Recherche</h4>\n<ul>\n<li>\u00c9laboration et validation de m\u00e9thodes d'essai.</li>\n<li>D\u00e9tection et contr\u00f4le des parasites d'origine alimentaire et animale.</li>\n<li>Caract\u00e9risation mol\u00e9culaire.</li>\n</ul>\n<h4>Tests de diagnostic</h4>\n<ul>\n<li>R\u00e9aliser les tests de diagnostic de routine, le suivi et la surveillance des parasites d'origine alimentaire et animale afin de prot\u00e9ger les consommateurs et le b\u00e9tail, et de soutenir le commerce.</li>\n<li>Mettre en \u0153uvre une vari\u00e9t\u00e9 de techniques, allant de la parasitologie traditionnelle (par exemple, examen microscopique, culture) aux m\u00e9thodes de d\u00e9tection s\u00e9rologique (s\u00e9rum sanguin) et mol\u00e9culaire.</li>\n</ul>\n\n<h3>Laboratoire de r\u00e9f\u00e9rence</h3> \n<p>Le laboratoire de Saskatoon est d\u00e9sign\u00e9 comme <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/laboratoires-de-reference-et-centres-collaborateur/fra/1654548734146/1654548734678\">laboratoire de r\u00e9f\u00e9rence</a> par l'Organisation mondiale de la sant\u00e9 animale (OMSA; fond\u00e9e sous le nom d'Office international des \u00e9pizooties (OIE)) pour son expertise en mati\u00e8re de trichinellose (une maladie caus\u00e9e par le ver rond <i lang=\"la\">Trichinella</i>).</p>\n<p>Les laboratoires de r\u00e9f\u00e9rence de l'OMSA sont d\u00e9sign\u00e9s pour \u00e9tudier les probl\u00e8mes scientifiques et techniques li\u00e9s \u00e0 une maladie ou \u00e0 un sujet sp\u00e9cifique. Leur r\u00f4le est de fonctionner comme un centre d'expertise et de normalisation des techniques de diagnostic pour la maladie d\u00e9sign\u00e9e.</p>\n<p>Le laboratoire de r\u00e9f\u00e9rence est impliqu\u00e9 dans la recherche de pointe et la formation de personnel hautement qualifi\u00e9 provenant de pays du monde entier.</p>\n<h3>Centre collaborateur OMSA</h3>\n<p>Le laboratoire de Saskatoon est \u00e9galement d\u00e9sign\u00e9 comme <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/laboratoires-de-reference-et-centres-collaborateur/fra/1654548734146/1654548734678\">centre collaborateur</a> par l'OMSA pour son expertise en mati\u00e8re de parasites zoonotiques d'origine alimentaire (transmis entre les animaux et les humains).</p>\n\n<h3>Gestion de la qualit\u00e9</h3>\n\n<p>Tous les laboratoires de l'ACIA sont accr\u00e9dites conform\u00e9ment \u00e0 la norme ISO/IEC 17025, <i>Exigences g\u00e9n\u00e9rales concernant la comp\u00e9tence des laboratoires d'\u00e9talonnage et d'essais</i>. Le Conseil canadien des normes (CCN) accorde l'accr\u00e9ditation pour les essais de routine, l'\u00e9laboration de m\u00e9thodes d'essai et les essais non routiniers, comme l'indique la port\u00e9e d'accr\u00e9ditation du laboratoire sur le site Web du <a href=\"https://www.scc.ca/fr/accreditation/laboratoires/canadian-food-inspection-agency-2\" title=\"Conseil canadien des normes\">CCN</a>. L'accr\u00e9ditation v\u00e9rifie officiellement la comp\u00e9tence de l'ACIA \u00e0 produire des r\u00e9sultats pr\u00e9cis et fiables. Ces r\u00e9sultats sont \u00e9tay\u00e9s par l'\u00e9laboration, la validation et la mise en \u0153uvre de m\u00e9thodes scientifiques, men\u00e9es par un personnel hautement qualifi\u00e9, \u00e0 l'aide de produits, de services et d'\u00e9quipement fiables, dans un environnement de qualit\u00e9 contr\u00f4l\u00e9e. La participation \u00e0 des programmes internationaux d'essais d'aptitude d\u00e9montre en outre que nos analyses sont comparables \u00e0 celles de laboratoires du Canada et du monde entier.</p>\n\n<h2>Adresse physique</h2>\n\n<p>Laboratoire de Saskatoon<br>\n<br>\n<br>\n</p>\n\n<h2>Plus d'informations</h2>\n<ul>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/dre-ruojing-wang/fra/1572617316547/1572617316922\">Balado avec M.\u00a0Ruojing Wang, chef de l'Herbier national des semences</a></li>\n</ul>\n\n<p>Renseignez-vous sur les autres <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a>.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}