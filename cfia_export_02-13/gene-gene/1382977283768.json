{
    "dcr_id": "1382977283768",
    "title": {
        "en": "Regulatory Requirements for Labelling Weed Species in Ground Cover Mixtures",
        "fr": "Exigences r\u00e8glementaires relatives \u00e0 l\u2019\u00e9tiquetage des mauvaises herbes dans les m\u00e9langes de semences de plantes couvre-sol"
    },
    "html_modified": "2024-02-13 9:49:25 AM",
    "modified": "2014-07-18",
    "issued": "2013-10-28 12:21:25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/labelling_weed_species_mixtures_1382977283768_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/labelling_weed_species_mixtures_1382977283768_fra"
    },
    "ia_id": "1382977442299",
    "parent_ia_id": "1364064312276",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Guidance Documents|Industry Advisories|Labelling and Composition|Core Requirements",
        "fr": "Documents d'orientation|Avis de l'industrie|\u00c9tiquetage et composition|Base d'\u00e9tiquetage"
    },
    "commodity": {
        "en": "Labelling|Seeds",
        "fr": "\u00c9tiquetage|Semences"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1364064312276",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Regulatory Requirements for Labelling Weed Species in Ground Cover Mixtures",
        "fr": "Exigences r\u00e8glementaires relatives \u00e0 l\u2019\u00e9tiquetage des mauvaises herbes dans les m\u00e9langes de semences de plantes couvre-sol"
    },
    "label": {
        "en": "Regulatory Requirements for Labelling Weed Species in Ground Cover Mixtures",
        "fr": "Exigences r\u00e8glementaires relatives \u00e0 l\u2019\u00e9tiquetage des mauvaises herbes dans les m\u00e9langes de semences de plantes couvre-sol"
    },
    "templatetype": "content page 1 column",
    "node_id": "1382977442299",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1364064277710",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-industry-advisories/weed-species/",
        "fr": "/protection-des-vegetaux/semences/avis-a-l-industrie-des-semences/mauvaises-herbes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Regulatory Requirements for Labelling Weed Species in Ground Cover Mixtures",
            "fr": "Exigences r\u00e8glementaires relatives \u00e0 l\u2019\u00e9tiquetage des mauvaises herbes dans les m\u00e9langes de semences de plantes couvre-sol"
        },
        "description": {
            "en": "As ground cover mixtures are typically made up of a number of grasses and fine-seeded species are intended for use over large areas and sometimes in remote locations, it is prudent to control the risk of introduction of weedy species through the pathway of seed of ground cover mixtures federally and provincially.",
            "fr": "Les m\u00e9langes de semences de plantes couvre-sol, g\u00e9n\u00e9ralement compos\u00e9s de gramin\u00e9es et d\u2019esp\u00e8ces \u00e0 semences fines, sont con\u00e7us pour \u00eatre utilis\u00e9s sur de vastes zones et parfois dans des r\u00e9gions \u00e9loign\u00e9es. Il est donc prudent, tant au niveau f\u00e9d\u00e9ral que provincial, de limiter le risque d\u2019introduction d\u2019esp\u00e8ces de mauvaises herbes via les graines faisant partie des m\u00e9langes de semences de plantes couvre-sol."
        },
        "keywords": {
            "en": "plants, plant health, Labelling, Requirements, Weed",
            "fr": "v\u00e9g\u00e9taux, la sant\u00e9 des v\u00e9g\u00e9taux, Exigences \u00e9tiquetage, mauvaises, herbes"
        },
        "dcterms.subject": {
            "en": "inspection,labelling,weeds",
            "fr": "inspection,\u00e9tiquetage,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-10-28 12:21:25",
            "fr": "2013-10-28 12:21:25"
        },
        "modified": {
            "en": "2014-07-18",
            "fr": "2014-07-18"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Regulatory Requirements for Labelling Weed Species in Ground Cover Mixtures",
        "fr": "Exigences r\u00e8glementaires relatives \u00e0 l\u2019\u00e9tiquetage des mauvaises herbes dans les m\u00e9langes de semences de plantes couvre-sol"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=17&amp;ga=13#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2>Background</h2>\n<p>In order to protect seed purchasers, the agricultural system, and the environment in Canada, the Canadian Food Inspection Agency (the CFIA) administers national regulations specifying standards for seed quality and the labelling of seed in Canada. In addition, the <i><a href=\"/english/reg/jredirect2.shtml?wsoagm\">Weed Seeds Order</a></i> (WSO) lists the species of plants, the seeds of which are considered weed seeds for the purposes of the <i><a href=\"/english/reg/jredirect2.shtml?seesema\">Seeds Act</a></i>. Provincial governments also make regulations for the control of weeds and weed seeds within their jurisdictions.</p>\n<p>As ground cover mixtures are typically made up of a number of grasses and fine-seeded species and are intended for use over large areas, sometimes in remote locations, it is prudent to control the risk of introduction of weedy species through the pathway of seed of ground cover mixtures federally and provincially.</p>\n<h2>Provincial Weed Seed Regulations</h2>\n<p>Provincial governments have their own legislative and regulatory mechanisms for the control of weeds and weed seeds. For instance, the province of Alberta's <i>Weed Control Regulation</i> establishes a list of prohibited noxious and noxious weeds for Alberta. Provincial regulations also provide for municipalities to designate plants as weeds within their jurisdictions.</p>\n<h2>Federal Weed Seed Regulations</h2>\n<p>The federal <i><a href=\"/english/reg/jredirect2.shtml?seesemr\">Seeds Regulations</a></i> (the Regulations) specify that seed may not contain any seeds of species identified on the federal <abbr title=\"Weed Seeds Order\">WSO</abbr> as a prohibited noxious weed. Weed seeds are further identified in the <abbr title=\"Weed Seeds Order\">WSO</abbr> as primary noxious, secondary noxious and noxious weed seeds. These designations are applied in the grading of seed where quality standards specify the maximum number and species of weed seeds permitted in a sample of a seed lot intended for import into Canada and/or sale in Canada.</p>\n<p>Section 29 of the Regulations describes the requirements for labelling ground cover mixtures and subparagraph 29(2)(<i>c</i>)(<abbr title=\"2\">ii</abbr>) is intended to provide a linkage between the national and provincial regulations for weeds and weed seeds.</p>\n<ul>\n<li>Subparagraph 29(2)(<i>c</i>)(<abbr title=\"1\">i</abbr>) of the Regulations specifies that the label on seed of ground cover mixtures must indicate the name of each kind or species of seed in the mixture that constitutes five per cent or more by weight of the mixture.</li>\n<li>Subparagraph 29(2)(<i>c</i>)(<abbr title=\"2\">ii</abbr>) requires that if seed of a species listed in any provincial legislation as a noxious or restricted weed is intentionally added to the mixture, at any percentage, the label must indicate these species. </li>\n<li>Subparagraph 29(2)(<i>c</i>)(<abbr title=\"2\">ii</abbr>) also indicates that if the seller chooses to not label the information required in subparagraph 29(2)(<i>c</i>)(<abbr title=\"1\">i</abbr>) (the components that make up five percent or more), this information must be provided in writing to the purchaser upon request. </li>\n</ul>\n<p>In all cases, however, the seller is required to indicate the provincially regulated noxious and restricted weeds on the label, whether or not the information on the components of the mixture that make up five percent or more appears on the label.</p>\n<p>This protects the purchaser of the seed and satisfies provincial government controls on the introduction of undesirable weeds. The requirement to label species that are on <strong>any</strong> provincial list ensures that the weedy species are identified to the purchaser no matter the province to which the seed is distributed.</p>\n<table class=\"table table-bordered\">\n<caption>Table 1 Labelling Ground Cover Mixtures -- At-A-Glance</caption> \n<thead>\n<tr>\n<th class=\"active\">Regulation</th> <th class=\"active\">Condition at Time of Sale</th> <th class=\"active\">Labelling Requirement</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th class=\"nowrap\">29(2)(<i>c</i>)(<abbr title=\"1\">i</abbr>)</th>\n<td>Information on components making up five per cent or more by weight is stated on the label</td>\n<td>The name of each kind or species in the mix that constitutes five percent or more by weight</td>\n</tr>\n<tr>\n<th class=\"nowrap\">29(2)(<i>c</i>)(<abbr title=\"2\">ii</abbr>)</th>\n<td>Information on components making up five per cent or more by weight is <strong>not</strong> stated on the label</td>\n<td>Information provided to purchaser upon request includes the name of each kind or species in the mix that constitutes five percent or more by weight</td>\n</tr>\n<tr>\n<th class=\"nowrap\">29(2)(<i>c</i>)(<abbr title=\"2\">ii</abbr>)</th>\n<td>Mixture intentionally contains species that are noxious or restricted weeds in <strong>any</strong> province</td>\n<td>The names of species that have been intentionally added to the mixture at <strong>any</strong> percentage which are noxious or restricted weeds in <strong>any</strong> province.</td>\n</tr>\n</tbody>\n</table>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=17&amp;ga=13#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2>Contexte</h2>\n<p>Afin de prot\u00e9ger les acheteurs de semences, le syst\u00e8me agricole et l'environnement du Canada, l'Agence canadienne d'inspection des aliments (ACIA) administre une r\u00e9glementation nationale pr\u00e9cisant les normes de qualit\u00e9 et d'\u00e9tiquetage des semences. De plus, l'<i><a href=\"/francais/reg/jredirect2.shtml?wsoagm\">Arr\u00eat\u00e9 sur les graines des mauvaises herbes</a></i> (l'Arr\u00eate) \u00e9num\u00e8re les esp\u00e8ces v\u00e9g\u00e9tales dont les graines sont consid\u00e9r\u00e9es comme \u00e9tant des graines de mauvaises herbes aux fins de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>. Les gouvernements provinciaux adoptent \u00e9galement des r\u00e8glements pour lutter contre les mauvaises herbes et les graines de ces derni\u00e8res, dans leur territoire respectif.</p>\n<p>Comme les m\u00e9langes de semences de plantes couvre-sol sont g\u00e9n\u00e9ralement compos\u00e9s d'un certain nombre d'esp\u00e8ces de gramin\u00e9es et d'esp\u00e8ces \u00e0 semences fines, et qu'il sont con\u00e7us pour \u00eatre utilis\u00e9s sur de vastes zones, parfois dans des r\u00e9gions \u00e9loign\u00e9es, il est prudent, tant au niveau f\u00e9d\u00e9ral que provincial, de limiter le risque d'introduction d'esp\u00e8ces de mauvaises herbes par la voie d'entr\u00e9e que constituent les graines faisant partie des m\u00e9langes de semences de plantes couvre-sol.</p>\n<h2>R\u00e8glements provinciaux sur les graines de mauvaises herbes</h2>\n<p>Les gouvernements provinciaux ont leurs propres m\u00e9canismes l\u00e9gislatifs et r\u00e9glementaires de lutte contre les mauvaises herbes et leurs graines. Par exemple, le <i lang=\"en\">Weed Control Regulation</i> de l'Alberta dresse une liste des mauvaises herbes nuisibles et des mauvaises herbes nuisibles interdites en Alberta. Les r\u00e8glements provinciaux pr\u00e9voient \u00e9galement que les municipalit\u00e9s d\u00e9signent les esp\u00e8ces v\u00e9g\u00e9tales dont les graines sont des mauvaises herbes, dans leur territoire respectif.</p>\n<h2>R\u00e8glement f\u00e9d\u00e9ral sur les graines de mauvaises herbes</h2>\n<p>Le <i><a href=\"/francais/reg/jredirect2.shtml?seesemr\">R\u00e8glement sur les semences</a></i> (le R\u00e8glement) f\u00e9d\u00e9ral pr\u00e9cise que la semence ne peut pas contenir de graines d'esp\u00e8ces identifi\u00e9es dans l'Arr\u00eat\u00e9 comme graines de mauvaises herbes interdites. Dans l'Arr\u00eat\u00e9, les graines de mauvaises herbes sont en outre r\u00e9pertori\u00e9es comme graines de mauvaises herbes nuisibles principales, graines de mauvaises herbes nuisibles secondaires ou graines de mauvaises herbes nuisibles. Ces d\u00e9signations sont utilis\u00e9es dans la classification de la semence lorsque les normes de qualit\u00e9 pr\u00e9cisent les esp\u00e8ces de graines de mauvaises herbes et le nombre maximum permis dans un \u00e9chantillon d'un lot de semences destin\u00e9 \u00e0 \u00eatre import\u00e9 ou vendu au Canada.</p>\n<p>L'article\u00a029 du R\u00e8glement d\u00e9crit les exigences relatives \u00e0 l'\u00e9tiquetage des m\u00e9langes de semences de plantes couvre-sol et le sous-alin\u00e9a \u00a029(2)<i>c</i>)(<abbr title=\"2\">ii</abbr>) a pour but de faire le lien entre les r\u00e8glements nationaux et provinciaux sur les semences et sur les mauvaises herbes et les graines de ces derni\u00e8res.\u00a0</p>\n<ul>\n<li>Le sous-alin\u00e9a 29(2)<i>c</i>)(<abbr title=\"1\">i</abbr>) du R\u00e8glement stipule que les exigences concernant l'\u00e9tiquetage du m\u00e9lange de semences de plantes couvre-sol doit comporter le nom de chaque sorte ou esp\u00e8ce de semence faisant partie du m\u00e9lange, qui constitue, en poids, cinq pour cent ou plus du m\u00e9lange.</li>\n<li>Le sous-alin\u00e9a 29(2)<i>c</i>)(<abbr title=\"2\">ii</abbr>) stipule que lorsque des semences d'esp\u00e8ces d\u00e9clar\u00e9es mauvaises herbes nuisibles ou r\u00e9glement\u00e9es par une province sont ajout\u00e9es intentionnellement au m\u00e9lange, \u00e0 n'importe quel pourcentage, l'\u00e9tiquette doit faire mention de ces esp\u00e8ces.</li>\n<li>Le sous-alin\u00e9a 29(2)<i>c</i>)(<abbr title=\"2\">ii</abbr>) stipule \u00e9galement que lorsque le vendeur choisit de ne pas faire figurer sur l'\u00e9tiquette les renseignements \u00e9nonc\u00e9s au sous-alin\u00e9a\u00a029(2)<i>c</i>)(i) (semences faisant partie du m\u00e9lange, qui constituent, en poids, cinq pour cent ou plus du m\u00e9lange), il doit fournir ces renseignements par \u00e9crit aux acheteurs qui en font la demande.</li>\n</ul>\n<p>Dans tous les cas, toutefois, le vendeur est tenu de faire figurer sur l'\u00e9tiquette les semences qui sont d\u00e9clar\u00e9es mauvaises herbes nuisibles ou r\u00e9glement\u00e9es par une ou plusieurs provinces, que les renseignements sur les semences qui constituent cinq pour cent ou plus du m\u00e9lange figurent ou non sur l'\u00e9tiquette.</p>\n<p>Cela prot\u00e8ge l'acheteur de semences et satisfait aux exigences de lutte des gouvernements provinciaux contre l'introduction de mauvaises herbes ind\u00e9sirables. L'imposition de porter sur l'\u00e9tiquette les esp\u00e8ces \u00e9num\u00e9r\u00e9es parmi les mauvaises herbes dans l'un ou l'autre des diff\u00e9rent textes l\u00e9gislatifs provinciaux permet de s'assurer que les esp\u00e8ces de mauvaises herbes sont identifi\u00e9es pour l'acheteur, peu importe dans quelle province la semence sera distribu\u00e9e.</p>\n<table class=\"table table-bordered\">\n<caption>Tableau 1 \u00c9tiquetage des m\u00e9langes de semences de plantes couvre sol \u2013 Vue d'ensemble</caption>\n<thead>\n<tr><th class=\"active\">R\u00e8glement</th><th class=\"active\">Condition au moment de la vente</th><th class=\"active\">Exigences relatives \u00e0 l'\u00e9tiquetage</th></tr>\n</thead>\n<tbody>\n<tr><th class=\"nowrap\">29(2)<i>c</i>)(<abbr title=\"1\">i</abbr>)</th>\n<td>L'\u00e9tiquette comporte les renseignements sur les semences faisant partie du m\u00e9lange, qui constituent, en poids, cinq pour cent ou plus du m\u00e9lange.</td>\n<td>Le nom de chaque sorte ou esp\u00e8ce de semence dans le m\u00e9lange, qui constitue, en poids, cinq pour cent ou plus du m\u00e9lange.</td>\n</tr>\n<tr><th class=\"nowrap\">29(2)<i>c</i>)(<abbr title=\"2\">ii</abbr>)</th>\n<td>L'\u00e9tiquette <strong>ne comporte pas</strong> les renseignements sur les semences faisant partie du m\u00e9lange, qui constituent, en poids, cinq pour cent ou plus du m\u00e9lange.</td>\n<td>Les renseignements fournis aux acheteurs qui en font la demande comprennent le nom de chaque sorte ou esp\u00e8ce de semence, qui constitue, en poids, cinq pour cent ou plus du m\u00e9lange.</td>\n</tr>\n<tr><th class=\"nowrap\">29(2)<i>c</i>)(<abbr title=\"2\">ii</abbr>)</th>\n<td>Le m\u00e9lange contient intentionnellement des esp\u00e8ces d\u00e9clar\u00e9es mauvaises herbes nuisibles ou r\u00e9glement\u00e9es par <strong>une ou plusieurs</strong> provinces.</td>\n<td>Les noms des esp\u00e8ces qui ont \u00e9t\u00e9 ajout\u00e9es intentionnellement, <strong>quel qu'en soit</strong> le pourcentage, qui sont d\u00e9clar\u00e9es nuisibles ou r\u00e9glement\u00e9es par <strong>une ou plusieurs</strong> provinces.</td>\n</tr>\n</tbody>\n</table>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}