{
    "dcr_id": "1591283962065",
    "title": {
        "en": "White coating on chocolate (chocolate bloom)",
        "fr": "Couche blanche sur le chocolat (blanchiment du chocolat)"
    },
    "html_modified": "16-7-2020",
    "modified": "27-7-2020",
    "issued": "4-6-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/white_coating_on_chocolate_chocolate_bloom_1591283962065_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/white_coating_on_chocolate_chocolate_bloom_1591283962065_fra"
    },
    "parent_ia_id": "1592232381996",
    "ia_id": "1591284132019",
    "parent_node_id": "1592232381996",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "White coating on chocolate (chocolate bloom)",
        "fr": "Couche blanche sur le chocolat (blanchiment du chocolat)"
    },
    "label": {
        "en": "White coating on chocolate (chocolate bloom)",
        "fr": "Couche blanche sur le chocolat (blanchiment du chocolat)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1591284132019",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1592232381574",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/commonly-occurring-issues-in-food/chocolate-bloom/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/problemes-courants-dans-les-aliments/blanchiment-du-chocolat/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "White coating on chocolate (chocolate bloom)",
            "fr": "Couche blanche sur le chocolat (blanchiment du chocolat)"
        },
        "description": {
            "en": "Occasionally, you may find a hazy, white coating on your chocolate. This is called chocolate bloom. Although it may affect the taste of chocolate, it is harmless and safe to eat.",
            "fr": "De temps \u00e0 autre, vous trouverez peut-\u00eatre une couche blanche floue sur votre chocolat. C'est ce qu'on appelle le blanchiment du chocolat. M\u00eame si cela peut alt\u00e9rer le go\u00fbt du chocolat, cela reste inoffensif et sans danger \u00e0 manger."
        },
        "keywords": {
            "en": "food safety tips, labels, food recalls, food borne illess, food packaging, storage, food handling, specific products, risks, white coating on chocolate, chocolate bloom",
            "fr": "Conseils sur la salubrit&#233; des aliments, &#233;tiquettes, Rappels d&#39;aliments, toxi-infections alimentaire, emballage, entreposage, Produits, risques sp&#233;cifiques, &#201;tiquetage, Couche blanche sur le chocolat, blanchiment du chocolat"
        },
        "dcterms.subject": {
            "en": "food,consumers,labelling,food labeling,government information,inspection,food inspection,consumer protection,government publication,food safety",
            "fr": "aliment,consommateur,\u00e9tiquetage,\u00e9tiquetage des aliments,information gouvernementale,inspection,inspection des aliments,protection du consommateur,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-07-16",
            "fr": "2020-07-16"
        },
        "modified": {
            "en": "2020-07-27",
            "fr": "2020-07-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "White coating on chocolate (chocolate bloom)",
        "fr": "Couche blanche sur le chocolat (blanchiment du chocolat)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Occasionally, you may find a hazy, white coating on your chocolate. This is called chocolate bloom. Although it may affect the taste of chocolate, it is harmless and safe to eat.</p>\n\n<div class=\"pull-right mrgn-lft-md col-sm-4\"> <p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/white_coating_on_chocolate_chocolate_bloom_img1_1591808829348_eng.jpg\" class=\"img-responsive\" alt=\"hazy, white coating on chocolate\"></p> </div>\n\n<h2>On this page</h2>\n<ul>\n<li><a href=\"#a1\">How chocolate bloom occurs</a></li>\n<li><a href=\"#a2\">Product safety</a></li>\n<li><a href=\"#a3\">How to avoid chocolate bloom</a></li>\n</ul>\n<h2 id=\"a1\">How chocolate bloom occurs</h2>\n<p>Two things can cause chocolate bloom:</p>\n<ul>\n<li>If chocolate is exposed to high temperatures, the cocoa butter inside melts and separates from the rest of the ingredients. As a result, it settles on the surface in a hazy, white coating</li>\n<li>If there is an excess of moisture, it will cause the sugar in the chocolate to crystalize, which creates a white, speckled or spotted appearance on the surface of the chocolate</li>\n</ul>\n<h2 id=\"a2\">Product safety</h2>\n<p>Although chocolate bloom may affect the taste of the chocolate, it does not pose a health risk. Chocolate that has bloom can still be eaten or used for baking.</p>\n<h2 id=\"a3\">How to avoid chocolate bloom</h2>\n<p>You can avoid chocolate bloom by storing your chocolate in a consistently cool and dry place.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>De temps \u00e0 autre, vous trouverez peut-\u00eatre une couche blanche floue sur votre chocolat. C'est ce qu'on appelle le blanchiment du chocolat. M\u00eame si cela peut alt\u00e9rer le go\u00fbt du chocolat, cela reste inoffensif et il peut \u00eatre consomm\u00e9 sans danger.</p>\n\n<div class=\"pull-right mrgn-lft-md col-sm-4\"> <p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/white_coating_on_chocolate_chocolate_bloom_img1_1591808829348_fra.jpg\" class=\"img-responsive\" alt=\"une couche blanche floue sur chocolat\"></p> </div>\n\n<h2>Sur cette page</h2>\n<ul>\n<li><a href=\"#a1\">Comment se produit le blanchiment du chocolat</a></li>\n<li><a href=\"#a2\">Salubrit\u00e9 des produits</a></li>\n<li><a href=\"#a3\">Comment \u00e9viter le blanchiment du chocolat</a></li>\n</ul>\n<h2 id=\"a1\">Comment se produit le blanchiment du chocolat</h2>\n<p>Deux choses peuvent provoquer le blanchiment du chocolat\u00a0:</p>\n<ul>\n<li>Si le chocolat est expos\u00e9 \u00e0 des temp\u00e9ratures \u00e9lev\u00e9es, le beurre de cacao qui se trouve \u00e0 l'int\u00e9rieur fond et se s\u00e9pare du reste des ingr\u00e9dients. Il se d\u00e9pose alors \u00e0 la surface, en une couche blanche floue.</li>\n<li>S'il y a un exc\u00e8s d'humidit\u00e9, le sucre dans le chocolat se cristallise, ce qui cr\u00e9e un aspect blanc, mouchet\u00e9 ou tachet\u00e9 \u00e0 la surface du chocolat.</li>\n</ul>\n<h2 id=\"a2\">Salubrit\u00e9 des produits</h2>\n<p>M\u00eame si le blanchiment du chocolat peut alt\u00e9rer le go\u00fbt du chocolat, cela ne pr\u00e9sente aucun risque pour la sant\u00e9. Le chocolat qui a blanchi peut encore \u00eatre mang\u00e9 ou utilis\u00e9 pour la cuisson.</p>\n<h2 id=\"a3\">Comment \u00e9viter le blanchiment du chocolat</h2>\n<p>Vous pouvez \u00e9viter le blanchiment du chocolat en conservant votre chocolat dans un endroit toujours frais et sec.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}