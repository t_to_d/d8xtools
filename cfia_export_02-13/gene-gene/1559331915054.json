{
    "dcr_id": "1559331915054",
    "title": {
        "en": "Questions and answers: Regulation of nitrification and urease inhibitors",
        "fr": "Questions et r\u00e9ponses\u00a0: R\u00e9glementation des inhibiteurs de la nitrification et de l'ur\u00e9ase"
    },
    "html_modified": "2024-02-13 9:53:55 AM",
    "modified": "2019-06-03",
    "issued": "2019-06-03",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/questions_and_answers_regulation_of_nitrification_and_urease_inhibitors_1559331915054_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/questions_and_answers_regulation_of_nitrification_and_urease_inhibitors_1559331915054_fra"
    },
    "ia_id": "1559332199234",
    "parent_ia_id": "1559332134244",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1559332134244",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and answers: Regulation of nitrification and urease inhibitors",
        "fr": "Questions et r\u00e9ponses\u00a0: R\u00e9glementation des inhibiteurs de la nitrification et de l'ur\u00e9ase"
    },
    "label": {
        "en": "Questions and answers: Regulation of nitrification and urease inhibitors",
        "fr": "Questions et r\u00e9ponses\u00a0: R\u00e9glementation des inhibiteurs de la nitrification et de l'ur\u00e9ase"
    },
    "templatetype": "content page 1 column",
    "node_id": "1559332199234",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1559331988417",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/notices-to-industry/2019-06-07/questions-and-answers/",
        "fr": "/protection-des-vegetaux/engrais/avis-a-l-industrie/2019-06-07/questions-et-reponses/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and answers: Regulation of nitrification and urease inhibitors",
            "fr": "Questions et r\u00e9ponses\u00a0: R\u00e9glementation des inhibiteurs de la nitrification et de l'ur\u00e9ase"
        },
        "description": {
            "en": "What are nitrification and urease inhibitors? Nitrification and urease inhibitors are nitrogen management products that can be applied with many forms of nitrogen-based fertilizers.",
            "fr": "Qu'est-ce que les inhibiteurs de la nitrification et de l'ur\u00e9ase? Les inhibiteurs de la nitrification et de l'ur\u00e9ase sont des produits de gestion de l'azote qui peuvent \u00eatre appliqu\u00e9s avec de nombreuses formes d'engrais \u00e0 base d'azote."
        },
        "keywords": {
            "en": "plant protection, fertilizers, biotechnology, microbial supplements, novel supplements, supplements, nitrification and urease inhibitors, Questions and answers",
            "fr": "protection des v&#233;g&#233;taux, engrais, biotechnologie, suppl&#233;ments microbiens, suppl&#233;ments nouveaux, suppl&#233;ments, inhibiteurs de la nitrification et de l'ur\u00e9ase, Questions et r\u00e9ponses"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,environment,inspection,plants",
            "fr": "cultures,engrais,environnement,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-06-03",
            "fr": "2019-06-03"
        },
        "modified": {
            "en": "2019-06-03",
            "fr": "2019-06-03"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and answers: Regulation of nitrification and urease inhibitors",
        "fr": "Questions et r\u00e9ponses\u00a0: R\u00e9glementation des inhibiteurs de la nitrification et de l'ur\u00e9ase"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What are nitrification and urease inhibitors?</h2>\n<p>Nitrification and urease inhibitors are nitrogen management products that can be applied with many forms of nitrogen-based fertilizers. Nitrification and urease inhibitors belong to a class of products knowns as nitrogen stabilizers. These products increase nitrogen-use efficiency by reducing losses through volatilization, leaching, run-off and denitrification allowing for more of the applied nitrogen fertilizer to be available to the crop. Urease inhibitors act on the enzyme urease, which is endemic in fields and an abundant soil enzyme. Nitrification inhibitors delay the conversion of ammonium to nitrate by temporarily inhibiting the ammonium monooxygenase enzyme within the Nitrosomonas bacterium.</p>\n<h2 class=\"h5\">Why is the regulatory oversight of nitrification and urease inhibitors changing?</h2>\n<p>A review of the <i>Fertilizers Act</i> and the <i>Pest Control Products Act </i>revealed overlapping authorities in the regulation of nitrification inhibitors that act on soil bacteria. \u00a0To improve efficiencies and clarify the regulatory pathway to market, the Canadian Food Inspection Agency (CFIA) and Health Canada's Pest Management Regulatory Agency (PMRA) agreed that both nitrification and urease inhibitors will be regulated by the CFIA alone, as soil supplements.</p>\n<h2 class=\"h5\">How did the PMRA and the CFIA arrive at this decision?</h2>\n<p>The PMRA determined that the scientific risk assessments conducted as part of the CFIA's registration process adequately address the health and environmental risks and benefits associated with these products. Consequently, the <i>Pest Control Products Regulations</i> (PCPR) will be amended to exempt nitrification inhibitors from the provisions of the <i>Pest Control Products Act</i>.</p>\n<h2 class=\"h5\">What type of data is required to demonstrate the environmental benefits of urease and nitrification inhibitors?</h2>\n<p>The CFIA in consultation with the impacted industry have developed baseline data requirements to substantiate environmental protection end points. Examples of admissible data include but are not limited to nitrogen speciation curves, volatilization data [ammonia (NH3), nitrous oxyde ( N2O)], nitrate leaching data, etc.\u00a0 In case of volatilization data for nitrous oxide (N2O), both direct and indirect measurement of emissions is required. These data requirements are intentionally outcome-based (that is, they do not prescribe specific methods or approaches) in order to offer companies the flexibility in substantiating the environmental protection claims.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que les inhibiteurs de la nitrification et de l'ur\u00e9ase?</h2>\n<p>Les inhibiteurs de la nitrification et de l'ur\u00e9ase sont des produits de gestion de l'azote qui peuvent \u00eatre appliqu\u00e9s avec de nombreuses formes d'engrais \u00e0 base d'azote. Les inhibiteurs de la nitrification et de l'ur\u00e9ase appartiennent \u00e0 une classe de produits connus sous le nom de stabilisateurs de l'azote. Ces produits am\u00e9liorent l'efficacit\u00e9 de l'azote en r\u00e9duisant les pertes dues \u00e0 la volatilisation, au lessivage, au ruissellement et \u00e0 la d\u00e9nitrification permettant ainsi au sol d'absorber davantage d'engrais azot\u00e9 appliqu\u00e9. Les inhibiteurs de l'ur\u00e9ase agissent sur l'ur\u00e9ase, une enzyme que l'on retrouve en abondance dans le sol et partout et dans les champs, alors que les inhibiteurs de la nitrification retardent la conversion de l'ammonium en nitrates en inhibant temporairement l'enzyme ammoniac monooxygenase au moyen de la bact\u00e9rie Nitrosomonas.</p>\n<h2 class=\"h5\">Pourquoi modifie-t-on la surveillance r\u00e9glementaire des inhibiteurs de la nitrification et de l'ur\u00e9ase?</h2>\n<p>Un examen de la <i>Loi sur les engrais</i> et de la <i>Loi sur les produits antiparasitaires</i> a r\u00e9v\u00e9l\u00e9 un chevauchement des pouvoirs en mati\u00e8re de r\u00e9glementation des inhibiteurs de la nitrification agissant sur des bact\u00e9ries du sol. Par souci d'efficacit\u00e9 et de clart\u00e9 concernant le parcours r\u00e9glementaire en vue de la commercialisation, l'Agence canadienne d'inspection des aliments (ACIA) et l'Agence de r\u00e9glementation de la lutte antiparasitaire (ARLA) de Sant\u00e9 Canada ont convenu que les inhibiteurs de la nitrification et de l'ur\u00e9ase seraient r\u00e9glement\u00e9s uniquement par l'ACIA en tant que suppl\u00e9ments.</p>\n<h2 class=\"h5\">Comment l'ARLA et l'ACIA en sont-elles arriv\u00e9es \u00e0 cette d\u00e9cision?</h2>\n<p>L'ARLA a d\u00e9termin\u00e9 que les \u00e9valuations des risques scientifiques r\u00e9alis\u00e9es dans le cadre du processus d'enregistrement de l'ACIA permettent d'\u00e9valuer ad\u00e9quatement les risques et les avantages pour la sant\u00e9 et l'environnement associ\u00e9s \u00e0 ces produits. Par cons\u00e9quent, le <i>R\u00e8glement sur les produits antiparasitaires</i> (RPA) sera modifi\u00e9 de mani\u00e8re \u00e0 soustraire les inhibiteurs de la nitrification de l'application des dispositions de la <i>Loi sur les produits antiparasitaires</i>.</p>\n<h2 class=\"h5\">Quel type de donn\u00e9es doit-on fournir pour d\u00e9montrer les avantages du point de vue de l'environnement des inhibiteurs de la nitrification et de l'ur\u00e9ase?</h2>\n<p>L'ACIA, en consultation avec l'industrie touch\u00e9e, a \u00e9labor\u00e9 des exigences en mati\u00e8re de donn\u00e9es de base pour \u00e9tayer les points finaux de la protection de l'environnement. Parmi les donn\u00e9es admissibles, mentionnons, entre autres, les courbes de sp\u00e9ciation de l'azote, les donn\u00e9es sur la volatilisation [de l'ammoniac (NH3) de l'oxyde nitreux (N2O)] et les donn\u00e9es sur le lessivage des nitrates. En ce qui concerne les donn\u00e9es sur la volatilisation de l'oxyde nitreux (N2O), une mesure des \u00e9missions directes et indirectes est exig\u00e9e. Ces exigences en mati\u00e8re de donn\u00e9es sont d\u00e9lib\u00e9r\u00e9ment ax\u00e9es sur les r\u00e9sultats (si aucune m\u00e9thode ou approche particuli\u00e8re n'est prescrite) afin que les entreprises b\u00e9n\u00e9ficient d'une certaine latitude lorsqu'elles doivent justifier les all\u00e9gations en mati\u00e8re de protection de l'environnement.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}