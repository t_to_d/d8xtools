{
    "dcr_id": "1548470439499",
    "title": {
        "en": "Making grapes into wine\u00a0\u2013 it's di-vine!",
        "fr": "Du raisin au vin, tout passe d'abord par la vigne!"
    },
    "html_modified": "2024-01-26 2:24:31 PM",
    "modified": "2019-02-08",
    "issued": "2019-02-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/making_grapes_into_wine_its_di-vine_1548470439499_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/making_grapes_into_wine_its_di-vine_1548470439499_fra"
    },
    "ia_id": "1548470439733",
    "parent_ia_id": "1495210111055",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1495210111055",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Making grapes into wine\u00a0\u2013 it's di-vine!",
        "fr": "Du raisin au vin, tout passe d'abord par la vigne!"
    },
    "label": {
        "en": "Making grapes into wine\u00a0\u2013 it's di-vine!",
        "fr": "Du raisin au vin, tout passe d'abord par la vigne!"
    },
    "templatetype": "content page 1 column",
    "node_id": "1548470439733",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1495210110540",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-research-and-publications/making-grapes-into-wine/",
        "fr": "/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/du-raisin-au-vin/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Making grapes into wine\u00a0\u2013 it's di-vine!",
            "fr": "Du raisin au vin, tout passe d'abord par la vigne!"
        },
        "description": {
            "en": "After a long week of balancing home-life, work-life and all the other kinds of\u00a0\u2013 life we juggle, it's easy to understand why a glass of wine tastes so good.",
            "fr": "Apr\u00e8s une longue semaine \u00e0 jongler avec la vie de famille, le travail et toutes ces autres choses avec lesquelles il faut composer, on comprend facilement pourquoi un verre de vin a si bon go\u00fbt."
        },
        "keywords": {
            "en": "science, food, animal, plant, laboratories, veterinarians, scientists, research, grapes, wine, divine",
            "fr": "science, aliments, animaux, v\u00e9g\u00e9taux, laboratoires, v\u00e9t\u00e9rinaires, scientistes, recherche, raisin, vin, d'abord par la vigne"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Science",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Science"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2019-02-08",
            "fr": "2019-02-08"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Making grapes into wine\u00a0\u2013 it's di-vine!",
        "fr": "Du raisin au vin, tout passe d'abord par la vigne!"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>After a long week of balancing home-life, work-life and all the other kinds of\u00a0\u2013 life we juggle, it's easy to understand why a glass of wine tastes so good. The big question is\u00a0\u2013 Which One To Choose?!? It can feel like all of the work goes into choosing the right wine, making us quick to forget the amount of work that goes in to getting the grapes in the bottle, on to the shelves and into your glass.</p>\n\n<p>Canada's wine industry is a $1.2\u00a0billion industry, with exports worth $133\u00a0million in 2016 (and increasing fast). That's a lot of grapes! It may not seem like it at first glance, but the Canadian Food Inspection Agency (CFIA) has a big role to play in the protection of those grapes. Nestled on the coast of Vancouver Island is one of 13\u00a0CFIA laboratories, the Sidney Centre for Plant Health. This lab's location is picture perfect\u00a0\u2013 but it is its isolation from major production areas that helps those in the lab prevent the possible spread of diseases and viruses, and the area also offers the best possible climate for growing diverse fruit and ornamental crops. This isolation makes the Centre the first stop for certain plants\u00a0\u2013 including grapevines\u00a0\u2013 entering Canada from over 30\u00a0different countries. When someone wants to grow grape varieties from countries that are not yet authorized for import, the process starts at the Centre for Plant Health, the only post-entry quarantine program for grapevines in Canada. When grapevines from those countries arrive at the Centre for Plant Health, the diagnostic testing team immediately begins the lengthy process of testing the grapevines for threatening viruses while keeping the grapevines under strict quarantine. After preliminary tests, it is sometimes possible to identify whether a grapevine is carrying a virus. Other times, more extensive testing is required. The importer can have the grapevine enter a virus elimination program. The vine is placed through a heat therapy and microshoot tip culture regime. Once the program is complete and the vine is determined to be virus-free, cuttings from the grapevine are declared safe to hit Canadian soil.</p>\n\n<p>In parallel with this \"grow and show\" method of virus detection, the scientists at CFIA are working in collaboration with partners to develop advanced genomics-based detection tools that will clear grapevines on a shorter timeline (up to 3\u00a0years versus weeks). The development and use of these tools has been, and will continue to be, a key component of the innovative science delivered at the Centre for Plant Health. Collaboration with other scientists\u00a0\u2013 including universities, other research organizations, and industry partners are an important part in enabling the Center to support national priorities and initiatives. These new tools will reduce the logistical cost of vine imports to Canada.</p>\n\n<p>But the work for the Sidney Centre doesn't stop there: it also offers an array of scientific expertise that keeps Canadian vineyards growing and your wine flowing! Once a grapevine has gone through the virus elimination program, it is kept in Canada's only repository of confirmed virus-tested vines located at the Sidney Centre for Plant Health. Cuttings of an accepted vine in the repository are then distributed to its importer making it possible to produce many plants from just one cutting! In simpler terms, the clean plants can be cloned over and over to produce vineyards-worth of virus-free grapevines. Voil\u00e0! A new grape variety to grow in Canada is born.</p>\n\n<p>The dedication of those working at the Centre shines through as they safeguard plants, small fruit (e.g. berries), tree fruit and vines. Their passion for what they do allows Canada's wineries to stay at the top of their game. The Centre for Plant Health is essential for healthy vineyards and a growing wine industry. Without the everyday work of the Centre, we wouldn't be able to enjoy that perfect glass grown right here in Canada. So, next time you are about to take a sip of Canadian-made wine, remember to toast the critical and passionate behind the scenes work of the scientists at the Sidney Centre for Plant Health. Cheers!</p>\n\n<h2>Want to know more?</h2>\n\n<ul class=\"lst-spcd\">\n<li>Listen to our podcast with<a href=\"/inspect-and-protect/science-and-innovation/dr-emilie-larocque/eng/1572621040261/1573240128498\"> Anna-Mary Schmidt, Head of Grapevine Diagnostics, Sidney Laboratory</a></li>\n\n<li>Found out more about the <a href=\"https://www.canada.ca/en/food-inspection-agency/news/2018/04/government-of-canada-invests-80-million-in-the-centre-for-plant-health.html\">$80\u00a0million investment in the Centre for Plant Health</a></li>\n\n<li><strong>Find out what is happening at other</strong> <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a></li>\n\n<li><a href=\"/plant-health/eng/1299162629094/1299162708850\">Learn about CFIA plant health programs</a></li>\n\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Apr\u00e8s une longue semaine \u00e0 jongler avec la vie de famille, le travail et toutes ces autres choses avec lesquelles il faut composer, on comprend facilement pourquoi un verre de vin a si bon go\u00fbt. La grande question est\u00a0\u2013 Lequel Choisir?!? Il peut sembler que l'essentiel consiste \u00e0 choisir le bon vin, mais on oublie facilement tout le travail n\u00e9cessaire pour produire le raisin qui se retrouve en bouteille, sur les rayons, puis finalement dans notre verre.</p>\n\n<p>Au Canada, l'industrie vinicole g\u00e9n\u00e8re 1,2\u00a0milliard de dollars, et les exportations ont atteint 133\u00a0millions de dollars en 2016 (et connaissent une hausse rapide). \u00c7a repr\u00e9sente beaucoup de raisin! On ne s'en doute pas n\u00e9cessairement, mais l'Agence canadienne d'inspection des aliments (ACIA) joue un r\u00f4le consid\u00e9rable pour la protection de ce raisin. Le Centre de protection des v\u00e9g\u00e9taux de <span lang=\"en\">Sidney</span>, l'un des 13 laboratoires de l'ACIA, est perch\u00e9 sur la c\u00f4te de l'\u00eele de Vancouver. Ce laboratoire se trouve dans un d\u00e9cor de r\u00eave, mais c'est son isolement des principales r\u00e9gions productrices qui est important, car il lui permet de pr\u00e9venir les risques de dispersion des organismes pathog\u00e8nes et des virus. De plus, la r\u00e9gion offre un climat id\u00e9al pour la production d'une vari\u00e9t\u00e9 de plantes fruiti\u00e8res et de plantes ornementales. L'isolement du Centre en fait l'un des premiers arr\u00eats pour certains v\u00e9g\u00e9taux qui entrent au Canada en provenance de plus de 30\u00a0pays, notamment les vignes. Lorsqu'une personne d\u00e9sire importer des vignes d'un pays d'o\u00f9 l'importation n'est pas encore autoris\u00e9e, le processus d\u00e9bute au Centre de protection des v\u00e9g\u00e9taux, qui est responsable du seul programme de quarantaine post-entr\u00e9e pour les vignes au Canada. Lorsque les vignes provenant de ces pays arrivent au Centre de protection des v\u00e9g\u00e9taux, l'\u00e9quipe d'\u00e9preuves diagnostiques commence imm\u00e9diatement le long processus de d\u00e9pistage des virus mena\u00e7ants tout en conservant ces v\u00e9g\u00e9taux selon des mesures de quarantaine rigoureuses. Apr\u00e8s des essais pr\u00e9liminaires, il est parfois possible de d\u00e9terminer si une vigne est porteuse d'un virus. Parfois, des essais pouss\u00e9s sont n\u00e9cessaires. L'importateur peut demander que les vignes soient soumises \u00e0 un programme d'\u00e9limination des virus. Les vignes subissent alors une thermoth\u00e9rapie et passent par un r\u00e9gime de culture de m\u00e9rist\u00e8mes apicaux. Une fois que le programme est termin\u00e9 et qu'on peut \u00e9tablir que les vignes sont exemptes de virus et s\u00fbres, des boutures peuvent \u00eatre mises en terre en sol canadien.</p>\n\n<p>En parall\u00e8le de cette m\u00e9thode de culture pour la d\u00e9tection des virus, les scientifiques de l'ACIA collaborent avec des partenaires en vue de mettre au point des outils de d\u00e9tection avanc\u00e9s fond\u00e9s sur la g\u00e9nomique, qui permettront de mettre les vignes en circulation plus rapidement (jusqu'\u00e0 3\u00a0ans par rapport \u00e0 quelques semaines). La mise au point et l'utilisation de ces outils est et continuera d'\u00eatre une composante cl\u00e9 des activit\u00e9s scientifiques novatrices du Centre de protection des v\u00e9g\u00e9taux. La collaboration avec des scientifiques d'autres milieux, y compris les universit\u00e9s, d'autres organisations de recherche et les partenaires de l'industrie, joue un r\u00f4le important pour permettre au Centre de contribuer aux priorit\u00e9s et aux initiatives nationales. Ces nouveaux outils r\u00e9duiront les co\u00fbts logistiques associ\u00e9s \u00e0 l'importation de vignes au Canada.</p>\n\n<p>Toutefois, le travail du Centre de <span lang=\"en\">Sidney</span> ne se limite pas \u00e0 cela\u00a0: le Centre offre \u00e9galement divers services scientifiques qui permettent aux vignes canadiennes de bien pousser et \u00e0 vos coupes de demeurer bien remplies! Une fois que les vignes sont pass\u00e9es par le programme d'\u00e9limination des virus, elles sont conserv\u00e9es dans la seule banque de vignes test\u00e9es \u00e0 l'\u00e9gard des virus au Canada, qui se trouve au Centre de protection des v\u00e9g\u00e9taux de Sidney. Lorsqu'une vigne est accept\u00e9e, des boutures sont pr\u00e9lev\u00e9es sur les sujets conserv\u00e9s dans la banque et remises \u00e0 l'importateur, et de nombreuses plantes peuvent \u00eatre produites \u00e0 partir d'une seule bouture! En termes simples, les vignes exemptes de virus peuvent \u00eatre clon\u00e9es \u00e0 r\u00e9p\u00e9tition pour produire suffisamment de plantes pour cr\u00e9er un vignoble. Voil\u00e0! Une nouvelle vari\u00e9t\u00e9 de vigne pouvant \u00eatre cultiv\u00e9e au Canada a vu le jour!</p>\n\n<p>Le d\u00e9vouement des employ\u00e9s du Centre se refl\u00e8te dans la pr\u00e9servation des v\u00e9g\u00e9taux, notamment les plantes \u00e0 petits fruits, les arbres fruitiers et les vignes. Leur passion pour leur travail permet aux vignerons canadiens de demeurer au sommet de leur art. Le Centre de protection des v\u00e9g\u00e9taux est essentiel au maintien de la sant\u00e9 des vignobles et \u00e0 la croissance de l'industrie vinicole. Sans le travail quotidien du Centre, nous ne pourrions pas savourer ces vins parfaits produits ici au Canada. Alors, la prochaine fois que vous vous appr\u00eatez \u00e0 prendre une gorg\u00e9e de vin canadien, rappelez-vous de lever votre verre au travail essentiel et passionn\u00e9 que les scientifiques du Centre de protection des v\u00e9g\u00e9taux de <span lang=\"en\">Sidney</span> accomplissent dans l'ombre. Sant\u00e9!</p>\n\n<h2>Souhaitez-vous en savoir plus?</h2>\n\n<ul class=\"lst-spcd\">\n\n<li>\u00c9coutez notre balado avec <a href=\"/inspecter-et-proteger/science-et-innovation/dre-emilie-larocque/fra/1572621040261/1573240128498\">Anna-Mary Schmidt, chef des Services de diagnostic de la vigne du laboratoire de Sidney</a></li>\n\n<li>Apprenez-en plus sur les <a href=\"https://www.canada.ca/fr/agence-inspection-aliments/nouvelles/2018/04/le-gouvernement-du-canada-investit80millions-de-dollars-dans-le-centre-pour-la-protection-des-vegetaux.html\">investissements de 80\u00a0millions de dollars au Centre de protection des v\u00e9g\u00e9taux</a></li>\n\n<li><strong>D\u00e9couvrez ce qui se passe dans d'autres</strong> <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a></li>\n\n<li><a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">D\u00e9couvrez plus sur les programmes de la sant\u00e9 des v\u00e9g\u00e9taux de l'ACIA</a></li>\n\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}