{
    "dcr_id": "1331732275924",
    "title": {
        "en": "Chinese yam - <i lang=\"la\">Dioscorea polystachya</i>",
        "fr": "Igname de Chine - <i lang=\"la\">Dioscorea polystachya</i>"
    },
    "html_modified": "2012-03-14 09:37",
    "modified": "9-8-2016",
    "issued": "14-3-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_dioscorea_1331732275924_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_dioscorea_1331732275924_fra"
    },
    "parent_ia_id": "1331614823132",
    "ia_id": "1331732474608",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Chinese yam - <i lang=\"la\">Dioscorea polystachya</i>",
        "fr": "Igname de Chine - <i lang=\"la\">Dioscorea polystachya</i>"
    },
    "label": {
        "en": "Chinese yam - <i lang=\"la\">Dioscorea polystachya</i>",
        "fr": "Igname de Chine - <i lang=\"la\">Dioscorea polystachya</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331732474608",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1331614724083",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/chinese-yam/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/igname-de-chine/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Chinese yam - Dioscorea polystachya",
            "fr": "Igname de Chine - Dioscorea polystachya"
        },
        "description": {
            "en": "Chinese yam is native to eastern Asia, where it is cultivated for its edible roots or tubers and used in traditional Chinese medicine.",
            "fr": "L?igname de Chine est une plante indig\u00e8ne de l?Asie orientale, o\u00f9 elle est cultiv\u00e9e pour ses racines ou ses tubercules comestibles et sert \u00e0 la fabrication de rem\u00e8des traditionnels chinois."
        },
        "keywords": {
            "en": "invasive plants, plant protection, Plant Protection Act, weed control, chinese yam, dioscorea polystachya",
            "fr": "plantes envahissantes, protection des v\u00e9g\u00e9taux, Loi sur la protection des v\u00e9g\u00e9taux, contre les mauvaises herbes, dioscorea polystachya, igname de chine"
        },
        "dcterms.subject": {
            "en": "crops,environment,inspection,plants,weeds,environmental protection",
            "fr": "cultures,environnement,inspection,plante,plante nuisible,protection de l'environnement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-14 09:37:58",
            "fr": "2012-03-14 09:37:58"
        },
        "modified": {
            "en": "2016-08-09",
            "fr": "2016-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Chinese yam - Dioscorea polystachya",
        "fr": "Igname de Chine - Dioscorea polystachya"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/plant-health/seeds/seed-testing-and-grading/seeds-identification/dioscorea-polystachya/eng/1397668090275/1397668126629\">Weed Seed - Chinese Yam (<i lang=\"la\">Dioscorea polystachya</i>)</a></p>\n<p>Chinese yam is native  to eastern Asia, where it is cultivated for its edible roots or tubers and used  in traditional Chinese medicine. It is a climbing vine that rapidly invades  undisturbed habitats, reduces biodiversity and damages the branches of large  trees and shrubs. Manual removal of this plant is possible for small isolated  patches; however, multiple herbicide treatments are required for larger  infestations.</p>\n<h2>Where it's found</h2>\n<p>Established  populations of Chinese yam have not been found in Canada. Since its  introduction into North America, it has spread throughout the eastern United  States. Chinese yam is found in many habitats including forests, ravines,  mountain slopes, along rivers and in disturbed areas.</p>\n<h2>What it looks like</h2>\n<p>Chinese yam is a  climbing vine. Its hairless stems are purplish-red and its leaves are broad and  heart-shaped. Its flowers are small, yellowish and have a cinnamon-like  fragrance.</p>\n<h2>How it spreads</h2>\n<p>Intentional  introduction as an ornamental garden plant has been the most significant route  of entry to North America. Within North America, Chinese yam reproduces through  small above-ground bulbs called bulbils, which are spread by rodents gathering  and feeding on them.</p>\n<h2>Legislation</h2>\n<p>Chinese yam is regulated as a pest in Canada under the <i>Plant Protection Act</i>. Importation and domestic movement of regulated plants and their propagative parts is prohibited.</p>\n<h2>What you can do about it</h2>\n<ul>\n<li>Avoid planting  invasive plants in your garden.</li>\n<li>Use clean,  high-quality seed that is certified if possible.</li>\n<li>Declare all plants  and related products when returning to Canada.</li>\n<li>Contact your local  Canadian Food Inspection Agency (CFIA) office if you suspect you have found  this invasive plant. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will follow up and determine if further action is  needed.</li>\n</ul>\n<p>Learn more about <a href=\"/eng/1328325263410/1328325333845\">invasive species</a>.</p>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Chinese yam leaves\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_dioscoreab_1331667993117_eng.jpg\"><figcaption>Chinese yam leaves<br>Attribution: James H. Miller, United States Department of Agriculture\u2014Forest Service (Bugwood.org)</figcaption></figure>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Chinese yam aerial tubers\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_dioscoreac_1331668102068_eng.jpg\"><figcaption>Chinese yam aerial tubers<br> Attribution: Chris Evans, River to River Cooperative Weed Management Area (CWMA) (Bugwood.org)</figcaption></figure>\n\n<figure><img alt=\"Chinese yam foliage\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_dioscoreaa_1331667914541_eng.jpg\"><figcaption>Chinese yam foliage<br> Attribution: Chris Evans, River to River <abbr title=\"Cooperative Weed Management Area\">CWMA</abbr> (Bugwood.org)</figcaption></figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/dioscorea-polystachya/fra/1397668090275/1397668126629\">Semence de mauvaises herbe - Igname de Chine (<i lang=\"la\">Dioscorea polystachya</i>)</a></p>\n<p>L'igname de Chine est  une plante indig\u00e8ne de l'Asie orientale, o\u00f9 elle est cultiv\u00e9e pour ses racines  ou ses tubercules comestibles et sert \u00e0 la fabrication de rem\u00e8des traditionnels  chinois. Cette vigne grimpante envahit rapidement les habitats non perturb\u00e9s,  r\u00e9duit la biodiversit\u00e9 et endommage les branches des grands arbres et des  arbustes. Il est possible de l'enlever \u00e0 la main sur de petites superficies  isol\u00e9es, mais les grandes infestations exigent de nombreux traitements aux  herbicides.</p>\n<h2>O\u00f9 trouve-t-on cette esp\u00e8ce?</h2>\n<p>On n'a trouv\u00e9 aucune  population \u00e9tablie d'ignames de Chine au Canada. Depuis son introduction en  Am\u00e9rique du Nord, elle s'est propag\u00e9e dans tout l'est des \u00c9tats-Unis. Cette  esp\u00e8ce pousse dans de nombreux habitats, notamment les for\u00eats et les ravins,  les pentes des montagnes, le long des rivi\u00e8res et les r\u00e9gions perturb\u00e9es.</p>\n<h2>\u00c0 quoi ressemble-t-elle?</h2>\n<p>L'igname de Chine est  une vigne grimpante. Ses hampes glabres sont d'un pourpre rouge\u00e2tre, et ses  larges feuilles sont en forme de c\u0153ur. Ses petites fleurs jaun\u00e2tres ont une  senteur qui rappelle celle de la cannelle.</p>\n<h2>Comment se propage-t-elle?</h2>\n<p>En Am\u00e9rique du Nord,  l'esp\u00e8ce a surtout \u00e9t\u00e9 introduite volontairement comme plante ornementale. Elle  s'y reproduit \u00e0 l'aide de petits bulbes de surface appel\u00e9s bulbilles, que les  rongeurs dispersent en les recueillant et en s'en nourrissant.</p>\n<h2>Lois</h2>\n<p>L'igname de Chine est r\u00e8glement\u00e9 comme \u00e9tant des organismes nuisibles en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>. L'importation et la circulation sur le territoire canadien de plantes r\u00e9glement\u00e9s et de leurs parties servant \u00e0 la multiplication sont interdites.</p>\n<h2>Que pouvez-vous faire?</h2>\n<ul>\n<li>\u00c9vitez de planter des  plantes envahissantes dans votre jardin.</li>\n<li>Utilisez des semences  propres, de haute qualit\u00e9 et si possible certifi\u00e9es.</li>\n<li>Si vous avez voyag\u00e9,  d\u00e9clarez tout v\u00e9g\u00e9tal et mat\u00e9riel connexe \u00e0 votre retour au Canada.</li>\n<li>Communiquez avec  votre bureau local de l'Agence canadienne d'inspection des aliments (ACIA) si  vous croyez avoir aper\u00e7u cette plante envahissante. L'<abbr title=\"Agence canadienne d\u2019inspection des aliments\">ACIA</abbr> fera un suivi et  d\u00e9terminera si d'autres mesures s'imposent.</li>\n</ul>\n\n<p>En savoir plus sur les <a href=\"/fra/1328325263410/1328325333845\">esp\u00e8ces envahissantes</a>.</p>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Feuilles de l'igname de Chine\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_dioscoreab_1331667993117_fra.jpg\"><figcaption>Feuilles de l'igname de Chine<br> Source\u00a0: James H. Miller, d\u00e9partement de l'Agriculture des \u00c9tats-Unis \u2014 Service des for\u00eats (Bugwood.org)</figcaption> </figure> \n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Tubercules a\u00e9riens de l'igname de Chine\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_dioscoreac_1331668102068_fra.jpg\"><figcaption>Tubercules a\u00e9riens de l'igname de Chine<br> Source\u00a0: Chris Evans, <span lang=\"en\">River to River Cooperative Weed Management Area (CWMA)(Bugwood.org)</span></figcaption> </figure> \n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Feuillage de l'igname de Chine\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_dioscoreaa_1331667914541_fra.jpg\"><figcaption>Feuillage de l'igname de Chine<br>Source\u00a0:<span lang=\"en\">Chris Evans, River to River <abbr title=\"Cooperative Weed Management Area\">CWMA</abbr> (Bugwood.org)</span></figcaption> </figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}