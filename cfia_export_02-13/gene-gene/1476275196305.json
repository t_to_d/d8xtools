{
    "dcr_id": "1476275196305",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Conium maculatum</i> (Poison hemlock)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Conium maculatum</i> (Cigu\u00eb macul\u00e9e)"
    },
    "html_modified": "2024-02-13 9:51:47 AM",
    "modified": "2017-10-16",
    "issued": "2017-10-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_conium_maculatum_1476275196305_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_conium_maculatum_1476275196305_fra"
    },
    "ia_id": "1476275196788",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Conium maculatum</i> (Poison hemlock)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Conium maculatum</i> (Cigu\u00eb macul\u00e9e)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Conium maculatum</i> (Poison hemlock)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Conium maculatum</i> (Cigu\u00eb macul\u00e9e)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476275196788",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/conium-maculatum/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/conium-maculatum/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Conium maculatum (Poison hemlock)",
            "fr": "Semence de mauvaises herbe : Conium maculatum (Cigu\u00eb macul\u00e9e)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Conium maculatum, Apiaceae, Poison hemlock",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Conium maculatum, Apiaceae, Cigu\u00eb macul\u00e9e"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-10-16",
            "fr": "2017-10-16"
        },
        "modified": {
            "en": "2017-10-16",
            "fr": "2017-10-16"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Conium maculatum (Poison hemlock)",
        "fr": "Semence de mauvaises herbe : Conium maculatum (Cigu\u00eb macul\u00e9e)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Poison hemlock</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"Alberta\">AB</abbr>, <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Nova Scotia\">NS</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr>, <abbr title=\"Saskatchewan\">SK</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, Europe and western and central Asia. Introduced to North and South America, southern Africa, Australia, New Zealand and Micronesia (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Biennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Schizocarp, divided into 2 mericarps</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Mericarp length: 2.0 - 4.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Mericarp width: 1.5 - 2.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Mericarp thickness 1.3 - 2.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Oval to teardrop-shaped mericarp, flat on the ventral side</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Mericarp dull surface, wrinkled between the wavy ribs</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Mericarp greyish-brown to brown with straw-yellow ribs</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Musty smell if mericarp surface is punctured</li>\n<li>Five ribs on the dorsal side; no ribs on the ventral side of the mericarp</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, pastures, roads and disturbed areas (Darbyshire 2003<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). Invades a variety of crops, including cereal and vegetable crops, as well as orchards, often by encroachment from field edges (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Poison hemlock was brought to the United States as a garden plant in the 19th century (DiTomaso and Healy 2007<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). Seed may have spread by mud, boots and other human clothing, as well as by machinery and transported soil (Ministry of Agriculture, Food, and Fisheries 2002<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n<p>Poison hemlock grows in lowlands on dry to moist soils and can tolerate poorly drained soils (Ministry of Agriculture, Food, and Fisheries 2002<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). It is poisonous to livestock (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Wild carrot (<i lang=\"la\">Daucus carota</i> <abbr title=\"subspecies\">subsp.</abbr> <i lang=\"la\">carota</i>)</h3>\n<ul>\n<li>The mericarps of wild carrot and poison hemlock are a similar size, oval - teardrop shape and grey-brown colour.</li>\n\n<li>Wild carrot mericarps may lack their typical long wings and hairs if processed, immature or damaged.</li>\n\n<li>Wild carrot mericarps may still retain small hairs on the straight ribs, the surface between the ribs is smooth, and thin ribs are on the ventral side. Poison hemlock does not have hairs, ribs are wavy and the surface is wrinkled. The ventral side of the mericarp does not have ribs.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_05cnsh_1475595655103_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Poison hemlock (<i lang=\"la\">Conium maculatum</i>) mericarps\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_01cnsh_1475595357453_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Poison hemlock (<i lang=\"la\">Conium maculatum</i>) mericarp, outer side\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_02cnsh_1475595491871_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Poison hemlock (<i lang=\"la\">Conium maculatum</i>) mericarp, inner side\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_03cnsh_1475595594282_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Poison hemlock (<i lang=\"la\">Conium maculatum</i>) mericarp, side view\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_04cnsh_1475595624516_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Poison hemlock (<i lang=\"la\">Conium maculatum</i>) mericarp, top-down view \n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_2_copyright_1475595527147_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Poison hemlock (<i lang=\"la\">Conium maculatum</i>) mericarps\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_daucus_carota_05cnsh_1475597042684_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Wild carrot (<i lang=\"la\">Daucus carota</i>) mericarps</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_daucus_carota_01cnsh_1475596943436_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Wild carrot (<i lang=\"la\">Daucus carota</i>) mericarp, outer side</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_daucus_carota_02cnsh_1475596964363_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Wild carrot (<i lang=\"la\">Daucus carota</i>) mericarp, inner side</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>DiTomaso, J. M. and Healy, E. A. 2007</strong>. Weeds of California and Other Western States. Vol. 1. 834 pp. University of California, CA.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Ministry of Agriculture, Food, and Fisheries. 2002</strong>. Guide to the Weeds in British Columbia, https://www.for.gov.bc.ca/hra/Plants/weedsbc/GuidetoWeeds.pdf [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Cigu\u00eb macul\u00e9e</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente en <abbr title=\"Alberta\">Alb.</abbr>, en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr>, en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr>, au <abbr title=\"Qu\u00e9bec\">Qc</abbr> et en <abbr title=\"Saskatchewan\">Sask.</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Afrique septentrionale, d'Europe et de l'Asie occidentale et centrale. Introduite en Am\u00e9rique du Nord et en Am\u00e9rique du Sud, en Afrique australe, en Australie, en Nouvelle-Z\u00e9lande et en Micron\u00e9sie (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Bisannuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Schizocarpe, divis\u00e9 en deux m\u00e9ricarpes</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur du m\u00e9ricarpe\u00a0: 2,0 \u00e0 4,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur du m\u00e9ricarpe\u00a0: 1,5 \u00e0 2,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>\u00c9paisseur du m\u00e9ricarpe\u00a0: 1,3 \u00e0 2,0  <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe d'elliptique \u00e0 larmiforme, face ventrale plate</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Surface du m\u00e9ricarpe mate, rid\u00e9e entre les c\u00f4tes sinueuses</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe brun gris\u00e2tre \u00e0 brun avec des c\u00f4tes jaune paille</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Une odeur de moisi se d\u00e9gage si l'on perfore la surface du m\u00e9ricarpe</li>\n<li>Cinq c\u00f4tes sur la face dorsale; aucune c\u00f4te sur la face ventrale du m\u00e9ricarpe</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, p\u00e2turages, chemins et terrains perturb\u00e9s (Darbyshire, 2003<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>). Elle envahit diverses cultures, notamment les c\u00e9r\u00e9ales et les l\u00e9gumes, ainsi que les vergers, souvent \u00e0 partir des bordures du champ (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La cigu\u00eb macul\u00e9e a \u00e9t\u00e9 introduite aux \u00c9tats-Unis comme plante de jardin au 19<sup>e</sup> si\u00e8cle (DiTomaso et Healy, 2007<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>). La semence peut \u00eatre dispers\u00e9e par la boue, les bottes et d'autres tenues vestimentaires, ainsi que par la machinerie et le sol d\u00e9plac\u00e9 (Ministry of Agriculture, Food, and Fisheries, 2002<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>).</p>\n<p>La cigu\u00eb macul\u00e9e pousse dans les basses terres dans des sols secs \u00e0 humides et elle peut tol\u00e9rer les sols mal drain\u00e9s  (Ministry of Agriculture, Food, and Fisheries, 2002<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>). Elle est toxique pour le b\u00e9tail (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Carotte sauvage (<i lang=\"la\">Daucus carota</i> <abbr title=\"subspecies\">subsp.</abbr> <i lang=\"la\">carota</i>)</h3>\n<ul>\n<li>Les m\u00e9ricarpes de la carotte sauvage ressemblent \u00e0 ceux de la cigu\u00eb macul\u00e9e par leurs dimensions, leur forme elliptique \u00e0 larmiforme et leur couleur brun-gris.</li>\n\n<li>Les m\u00e9ricarpes de la carotte sauvage peuvent \u00eatre d\u00e9barrass\u00e9s de leurs longues ailes et de leurs poils caract\u00e9ristiques s'ils ont \u00e9t\u00e9 trait\u00e9s ou endommag\u00e9s ou s'ils sont immatures.</li>\n\n<li>Les m\u00e9ricarpes de la carotte sauvage peuvent demeurer courtement pubescents sur les c\u00f4tes rectilignes, la surface entre les c\u00f4tes est lisse, et la face ventrale est orn\u00e9e de minces c\u00f4tes. Ceux de la cigu\u00eb macul\u00e9e sont glabres, pr\u00e9sentent des c\u00f4tes sinueuses et une surface rid\u00e9e. La face ventrale du m\u00e9ricarpe n'est pas c\u00f4tel\u00e9e.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_05cnsh_1475595655103_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Cigu\u00eb macul\u00e9e (<i lang=\"la\">Conium maculatum</i>) m\u00e9ricarpes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_01cnsh_1475595357453_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Cigu\u00eb macul\u00e9e (<i lang=\"la\">Conium maculatum</i>) m\u00e9ricarpe, la face ext\u00e9rieure\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_02cnsh_1475595491871_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Cigu\u00eb macul\u00e9e (<i lang=\"la\">Conium maculatum</i>) m\u00e9ricarpe, la face int\u00e9rieure\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_03cnsh_1475595594282_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Cigu\u00eb macul\u00e9e (<i lang=\"la\">Conium maculatum</i>) m\u00e9ricarpe, vue de c\u00f4t\u00e9\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_04cnsh_1475595624516_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Cigu\u00eb macul\u00e9e (<i lang=\"la\">Conium maculatum</i>) m\u00e9ricarpe, vue du dessus\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_conium_maculatum_2_copyright_1475595527147_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Cigu\u00eb macul\u00e9e (<i lang=\"la\">Conium maculatum</i>) m\u00e9ricarpes\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_daucus_carota_05cnsh_1475597042684_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Carotte sauvage (<i lang=\"la\">Daucus carota</i>) m\u00e9ricarpes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_daucus_carota_01cnsh_1475596943436_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Carotte sauvage (<i lang=\"la\">Daucus carota</i>) m\u00e9ricarpe, la face ext\u00e9rieure</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_daucus_carota_02cnsh_1475596964363_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Carotte sauvage (<i lang=\"la\">Daucus carota</i>) m\u00e9ricarpe, la face int\u00e9rieure</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>DiTomaso, J. M. and Healy, E. A. 2007</strong>. Weeds of California and Other Western States. Vol. 1. 834 pp. University of California, CA.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Ministry of Agriculture, Food, and Fisheries. 2002</strong>. Guide to the Weeds in British Columbia, https://www.for.gov.bc.ca/hra/Plants/weedsbc/GuidetoWeeds.pdf [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}