{
    "dcr_id": "1526653268334",
    "title": {
        "en": "Food-specific requirements and guidance \u2013 Dairy products",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Produits laitiers"
    },
    "html_modified": "2024-02-13 9:52:53 AM",
    "modified": "2023-03-02",
    "issued": "2018-06-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_specific_dairy_1526653268334_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_specific_dairy_1526653268334_fra"
    },
    "ia_id": "1526653268554",
    "parent_ia_id": "1521720158588",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1521720158588",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food-specific requirements and guidance \u2013 Dairy products",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Produits laitiers"
    },
    "label": {
        "en": "Food-specific requirements and guidance \u2013 Dairy products",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1526653268554",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1521720158031",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-guidance-by-commodity/dairy/",
        "fr": "/exigences-et-documents-d-orientation-relatives-a-c/produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food-specific requirements and guidance \u2013 Dairy products",
            "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Produits laitiers"
        },
        "description": {
            "en": "Before you review information related specifically to dairy products, read the information on General food requirements and guidance.",
            "fr": "Avant de passer en revue les renseignements portant pr\u00e9cis\u00e9ment sur les produits laitiers, veuillez lire le document Exigences et lignes directrices g\u00e9n\u00e9rales sur les aliments."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, food, Safe Food for Canadians Regulations, SFCR, dairy-specific requirements, guidance, Dairy products, Part 6, Division 2",
            "fr": "Agence canadienne d'inspection des aliments, aliments, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, exigences propres aux produits laitiers, lignes directrices, Produits laitiers, Partie 6, section 2"
        },
        "dcterms.subject": {
            "en": "food,food labeling,information,food inspection,agri-food products,regulations,food safety,food processing",
            "fr": "aliment,\u00e9tiquetage des aliments,information,inspection des aliments,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2023-03-02",
            "fr": "2023-03-02"
        },
        "type": {
            "en": "legislation and regulations,reference material",
            "fr": "l\u00e9gislation et r\u00e8glements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food-specific requirements and guidance \u2013 Dairy products",
        "fr": "Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013 Produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1526992762174\"></div>\n\n<section class=\"alert alert-info\">\n<h2>Information</h2>\n<p class=\"mrgn-tp-0\">Before you review information related specifically to dairy products, read the information on <a href=\"/eng/1512149634601/1512149659119\">General food requirements and guidance</a>. The Safe Food for Canadians Regulations (SFCR) set out many requirements that apply across food commodities, along with the dairy-specific requirements highlighted here.</p>\n\n<p>Additional requirements for <a href=\"/importing-food-plants-or-animals/food-imports/eng/1526656151226/1526656151476\">Food imports</a>\n<a href=\"/exporting-food-plants-or-animals/food-exports/eng/1323723342834/1323723662195\">Food exports</a> may apply also to your business.</p>\n</section>\n\n<h2>Know the requirements</h2>\n\n<p><a href=\"/food-guidance-by-commodity/dairy/regulatory-requirements/eng/1521739667281/1521739699003\">Regulatory requirement: Dairy products</a><br>\n</p>\n\n<p><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-standards-of-identity-volume-1/eng/1521473554991/1521473555532\">Canadian Standards of Identity Volume 1, Dairy Products</a><br>\n</p>\n\n<p><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-4/eng/1521118213588/1521118214322\">Canadian Grade Compendium Volume 4, Dairy Products</a><br>\n</p>\n\n<p><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-9/eng/1520647701525/1520647702274?chap=1#s2c1\">Grade Designations for Imported Food</a><br>\n</p>\n\n<p><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/units-of-measurement-for-the-net-quantity-declarat/eng/1521819171564/1521819242968\">Units of Measurement for the Net Quantity Declaration of Certain Foods</a><br>\n</p>\n\n<p><a href=\"/eng/1507333026490/1507333072859#a1\">Food-specific export requirements\u00a0\u2013 Dairy</a><br>\n</p>\n\n<p><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/personal-use-exemption/eng/1520439688578/1520439689098\">Maximum Quantity Limits for Personal Use Exemption</a><br>\n</p>\n\n<h2>Preventive control recommendations</h2>\n\n<p><a href=\"/preventive-controls/dairy-products/eng/1534954777758/1534954778164\">Preventive controls for dairy products</a><br>\n</p>\n\n<h2>Related information</h2>\n\n<p><a href=\"/food-licences/food-business-activities/eng/1524074697160/1524074697425\">Food business activities that require a licence under the SFCR</a><br>\n</p>\n\n\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1526992762174\"></div>\n\n\n<section class=\"alert alert-info\">\n<h2>Information</h2>\n<p class=\"mrgn-tp-0\">Avant de passer en revue les renseignements portant pr\u00e9cis\u00e9ment sur les produits laitiers, veuillez lire le document <a href=\"/fra/1512149634601/1512149659119\">Exigences et documents d'orientation g\u00e9n\u00e9rales sur les aliments</a>. Le R\u00e8glement sur la salubrit\u00e9 des aliments au Canada (RSAC) \u00e9nonce de nombreuses exigences qui s'appliquent \u00e0 tous les produits alimentaires, de m\u00eame que les exigences propres aux produits laitiers soulign\u00e9es dans cette section.</p>\n\n<p>D'autres exigences concernant les <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/fra/1526656151226/1526656151476\">importations d'aliments</a> et les <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/fra/1323723342834/1323723662195\">exportations d'aliments</a> pourraient \u00e9galement s'appliquer \u00e0 votre entreprise.</p>\n</section>\n\n<h2>Conna\u00eetre les exigences</h2>\n\n<p><a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-laitiers/exigences-reglementaires/fra/1521739667281/1521739699003\">Exigences r\u00e9glementaires\u00a0: Produits laitiers</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/normes-d-identite-canadiennes-volume-1/fra/1521473554991/1521473555532\">Normes d'identit\u00e9 canadiennes, Volume\u00a01, Produits laitiers</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1521118213588/1521118214322\">Recueil des normes canadiennes de classification, Volume\u00a04, Produits laitiers</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1520647701525/1520647702274?chap=1#s2c1\">D\u00e9signations de cat\u00e9gories applicables aux aliments import\u00e9s</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/les-unites-de-mesure-pour-la-declaration-de-quanti/fra/1521819171564/1521819242968\">Les unit\u00e9s de mesure pour la d\u00e9claration de quantit\u00e9 nette de certains aliments</a><br>\n</p>\n\n<p><a href=\"/fra/1507333026490/1507333072859#a1\">Produits laitiers\u00a0\u2013 Exigences d'exportation propres \u00e0 un produit alimentaire</a><br>\n</p>\n\n<p><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/exemption-pour-usage-personnel/fra/1520439688578/1520439689098\">Les quantit\u00e9s maximales pour l'exemption pour usage personnel</a><br>\n</p>\n\n\n<h2>Contr\u00f4les pr\u00e9ventifs recommand\u00e9s</h2>\n\n<p><a href=\"/controles-preventifs/produits-laitiers/fra/1534954777758/1534954778164\">Contr\u00f4les pr\u00e9ventifs recommand\u00e9s pour les aliments \u2013 Produits laitiers</a><br>\n</p>\n\n\n<h2>Renseignements connexes</h2>\n\n<p><a href=\"/licences-pour-aliments/activites-du-secteur-alimentaire/fra/1524074697160/1524074697425\">Activit\u00e9s du secteur alimentaire qui n\u00e9cessitent une licence aux termes du RSAC</a><br>\n</p>\n\n\n\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "success": true,
    "chat_wizard": true
}