{
    "dcr_id": "1323993172781",
    "title": {
        "en": "Foot-and-Mouth Disease",
        "fr": "Fi\u00e8vre aphteuse"
    },
    "html_modified": "2011-12-15 18:53",
    "modified": "20-11-2023",
    "issued": "15-12-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_fmdfa_index_1323993172781_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_fmdfa_index_1323993172781_fra"
    },
    "parent_ia_id": "1303768544412",
    "ia_id": "1323993252010",
    "parent_node_id": "1303768544412",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Foot-and-Mouth Disease",
        "fr": "Fi\u00e8vre aphteuse"
    },
    "label": {
        "en": "Foot-and-Mouth Disease",
        "fr": "Fi\u00e8vre aphteuse"
    },
    "templatetype": "content page 1 column",
    "node_id": "1323993252010",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1303768471142",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Foot-and-Mouth Disease",
            "fr": "Fi\u00e8vre aphteuse"
        },
        "description": {
            "en": "Foot-and-Mouth Disease is a severe, highly communicable viral disease of cattle and swine. It also affects sheep, goats, deer and other cloven-hoofed ruminants.",
            "fr": "La fi\u00e8vre aphteuse est une virose grave hautement transmissible, qui affecte les bovins et les porcs. Le virus peut aussi s'attaquer aux ovins, aux caprins, aux cerfs et \u00e0 d'autres ruminants artiodactyles.."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, Foot-and-Mouth Disease, FMD, cattle, pigs, sheep goats, deer, ruminants",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, fi\u00e8vre aphteuse, bovins, porcs, ovins, caprins, cerfs, ruminants"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-12-15 18:53:08",
            "fr": "2011-12-15 18:53:08"
        },
        "modified": {
            "en": "2023-11-20",
            "fr": "2023-11-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Foot-and-Mouth Disease",
        "fr": "Fi\u00e8vre aphteuse"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Foot-and-Mouth Disease is a severe, highly communicable viral disease of cattle and swine. It also affects sheep, goats, deer and other cloven-hoofed ruminants. The disease is characterized by fever and blister-like sores on the tongue and lips, in the mouth, on the teats and between the hooves. Many affected animals recover, but the disease leaves them weakened and debilitated. Horses are not susceptible to this disease.</p>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/travellers/eng/1355873582260/1355874711310\">Information for Travellers</a></li>\n</ul>\n<p>In Canada, <abbr title=\"Foot-and-mouth disease\">FMD</abbr> is a <a href=\"/animal-health/terrestrial-animals/diseases/reportable/eng/1303768471142/1303768544412\">reportable disease</a> under the <i>Health of Animals Act</i>, and all suspected cases must be reported to the Canadian Food Inspection Agency (CFIA).</p>\n\n<h2>Available information</h2>\n<p>Prevention and Outbreak Response</p>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/eng/1299868055616/1320534707863\">Animal Biosecurity</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/fact-sheet/eng/1330481689083/1330481803452\">Fact Sheet on FMD</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/questions-and-answers/eng/1355875603810/1355876586587\">Questions and Answers - Response to <abbr title=\"Foot-and-mouth disease\">FMD</abbr></a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/if-your-animals-may-be-infected/eng/1355870935544/1355871049411\">What to expect if your animals may be infected</a></li>\n\n</ul>\n<p>Imports</p>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/countries-recognized-as-free-of-the-disease/eng/1330483635966/1330483942804\">Country Freedom Recognition</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/avians/eng/1332018771882/1332032810000\">Avians imported into Canada from countries with Foot-and-Mouth Disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/horses/eng/1332033335585/1332033552830\">Horses imported into Canada from countries with Foot-and-Mouth Disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2010-17/eng/1430185177295/1430185231054\">Import of used equipment and things from non-designated countries</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La fi\u00e8vre aphteuse est une maladie virale grave hautement transmissible qui affecte les bovins et les porcs. Le virus peut aussi infecter les ovins, les caprins, les cerfs et d'autres ruminants artiodactyles. La maladie se caract\u00e9rise par de la fi\u00e8vre et la pr\u00e9sence de l\u00e9sions semblables \u00e0 des cloques sur la langue et les l\u00e8vres, dans la gueule sur les mamelles et entre les onglons. M\u00eame si bon nombre des animaux atteints se r\u00e9tablissent, ils demeurent affaiblies. Les chevaux ne sont pas sensibles \u00e0 cette maladie.</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/voyageurs/fra/1355873582260/1355874711310\">Information pour les voyageurs</a></li>\n</ul>\n<p>Au Canada, la fi\u00e8vre aphteuse est une maladie \u00e0 <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fra/1303768471142/1303768544412\">d\u00e9claration obligatoire</a> en vertu de la <i>Loi sur la sant\u00e9 des animaux</i>, et tous les cas suspects doivent \u00eatre d\u00e9clar\u00e9s \u00e0 l'Agence canadienne d'inspection des aliments (ACIA).</p>\n\n<h2>Renseignements disponibles</h2>\n<p>Pr\u00e9vention et intervention en cas d'\u00e9closion</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/fra/1299868055616/1320534707863\">Bios\u00e9curit\u00e9 animale</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/fiche-de-renseignements/fra/1330481689083/1330481803452\">Fiche de renseignements</a></li>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/questions-et-reponses/fra/1355875603810/1355876586587\">Questions et r\u00e9ponses - R\u00e9ponse \u00e0 la fi\u00e8vre aphteuse</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/si-vos-animaux-sont-potentiellement-infectes/fra/1355870935544/1355871049411\">Ce \u00e0 quoi vous attendre lorsque vos animaux sont potentiellement infect\u00e9s</a></li>\n</ul>\n<p>Importation</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/pays-reconnus-etant-indemnes-de-la-maladie/fra/1330483635966/1330483942804\">Pays reconnus indemnes</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/chevaux/fra/1332033335585/1332033552830\">Chevaux import\u00e9s au Canada en provenance de pays o\u00f9 s\u00e9vit la fi\u00e8vre aphteuse</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/general/2010-17/fra/1430185177295/1430185231054\">Importation de choses et de pi\u00e8ces d'\u00e9quipement usag\u00e9es de pays non d\u00e9sign\u00e9s</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/volaille/fra/1332018771882/1332032810000\">Volaille import\u00e9e au Canada en provenance de pays o\u00f9 s\u00e9vit la fi\u00e8vre aphteuse</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}