{
    "dcr_id": "1663956320830",
    "title": {
        "en": "Prohibition order in relation to secondary control zones in respect of rabies caused by canine-variant viruses",
        "fr": "Ordonnance d'interdiction relative aux zones de contr\u00f4le secondaires en lien avec le variant canin du virus de la rage"
    },
    "html_modified": "2024-02-13 9:55:57 AM",
    "modified": "2022-09-27",
    "issued": "2022-09-27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/prohibition_order_prohibited_rabies_entry_1663956320830_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/prohibition_order_prohibited_rabies_entry_1663956320830_fra"
    },
    "ia_id": "1663956321158",
    "parent_ia_id": "1356152541083",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1356152541083",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Prohibition order in relation to secondary control zones in respect of rabies caused by canine-variant viruses",
        "fr": "Ordonnance d'interdiction relative aux zones de contr\u00f4le secondaires en lien avec le variant canin du virus de la rage"
    },
    "label": {
        "en": "Prohibition order in relation to secondary control zones in respect of rabies caused by canine-variant viruses",
        "fr": "Ordonnance d'interdiction relative aux zones de contr\u00f4le secondaires en lien avec le variant canin du virus de la rage"
    },
    "templatetype": "content page 1 column",
    "node_id": "1663956321158",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1356138388304",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/rabies/prohibition-order/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/rage/ordonnance-d-interdiction/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Prohibition order in relation to secondary control zones in respect of rabies caused by canine-variant viruses",
            "fr": "Ordonnance d'interdiction relative aux zones de contr\u00f4le secondaires en lien avec le variant canin du virus de la rage"
        },
        "description": {
            "en": "Secondary control zone for rabies caused by canine-variant viruses in Canada \u2013 Declaration Schedule",
            "fr": "Zone de contr\u00f4le secondaire en lien avec le variant canin du virus de la rage au Canada \u2013 Annexe de d\u00e9claration"
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, rabies, warm-blooded animals, humans, pets, vaccination",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire,maladie, rage, animaux \u00e0 sang chaud, humains, animaux domestiques, vacciner"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,livestock,regulation,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,b\u00e9tail,r\u00e9glementation,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-09-27",
            "fr": "2022-09-27"
        },
        "modified": {
            "en": "2022-09-27",
            "fr": "2022-09-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Prohibition order in relation to secondary control zones in respect of rabies caused by canine-variant viruses",
        "fr": "Ordonnance d'interdiction relative aux zones de contr\u00f4le secondaires en lien avec le variant canin du virus de la rage"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Whereas:</p>\n\n<p>On June 28, 2022, secondary control zones were declared by the delegate of the Minister of Agriculture and Agri-Food (the \"Minister\"), Dr. Siddika Mithani, President of the Canadian Food Inspection Agency, under subsection 27.1(2) of the <i>Health of Animals Act</i><sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1a\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup> (\"the act\"), in respect of rabies caused by canine-variant viruses;</p>\n\n<p>On September 28, 2022, an order was made under subsection 27.1(3) of that act designating animals as being capable of being affected or contaminated by rabies caused by canine-variant viruses (\"Designation order\");</p>\n\n<p>The Minister has the power, under subsection 27.1(4) of that act to, by order, prohibit the removing from, moving within or taking into a secondary control zone a designated animal;</p>\n\n<p>As an inspector, I have the authority under section 33 of that act to exercise the authority of the Minister under subsection 27.1(4) of that act;</p>\n\n<p>Therefore, by virtue of this order, I prohibit the taking into, moving within or removing from, any of the secondary control zones, an animal designated under the designation order that originates from a country listed in the attached Schedule:</p>\n\n<p>Dated at Ottawa, September 28, 2022<br>Dr. Mary Jane Ireland, Inspector</p>\n\n<h2>Schedule</h2>\n\n<h3>High-risk countries for rabies caused by canine-variant viruses</h3>\n\n<h4>Africa</h4>\n\n<ul>\n<li>Algeria, Angola</li>\n<li>Benin, Botswana, Burkina Faso, Burundi</li>\n<li>Cameroon, Central Africa Republic, Chad, Comoros, C\u00f4te D'Ivoire (Ivory Coast)</li>\n<li>Democratic Republic of the Congo, Djibouti</li>\n<li>Egypt </li>\n<li>Equatorial Guinea, Eritrea, Eswatini (Swaziland), Ethiopia</li>\n<li>Gabon, Gambia, Ghana, Guinea, Guinea-Bissau</li>\n<li>Kenya</li>\n<li>Lesotho, Liberia, Libya</li>\n<li>Madagascar, Malawi, Mali, Mauritania, Morocco, Mozambique</li>\n<li>Namibia, Niger, Nigeria</li>\n<li>Republic of Congo, Rwanda</li>\n<li>Sao Tome and Principe, Senegal, Sierra Leone, Somalia, South Africa, South Sudan, Sudan</li>\n<li>Tanzania (including Zanzibar), Togo, Tunisia</li>\n<li>Uganda</li>\n<li>Western Sahara</li>\n<li>Zambia, Zimbabwe</li>\n</ul>\n\n<h4>Americas and Caribbean</h4>\n\n<ul>\n<li>Belize, Bolivia, Brazil</li>\n<li>Colombia, Cuba</li>\n<li>Dominican Republic</li>\n<li>Ecuador, El Salvador</li>\n<li>Guatemala, Guyana</li>\n<li>Haiti, Honduras</li>\n<li>Peru</li>\n<li>Suriname</li>\n<li>Venezuela</li>\n</ul>\n\n<h4>Asia and the Middle East, Eastern Europe </h4>\n\n<ul>\n<li>Afghanistan, Armenia, Azerbaijan</li>\n<li>Bangladesh, Belarus, Brunei</li>\n<li>Cambodia, China (mainland only)</li>\n<li>Georgia</li>\n<li>India, Indonesia, Iran, Iraq</li>\n<li>Jordan</li>\n<li>Kazakhstan, Kuwait, Kyrgyzstan</li>\n<li>Laos, Lebanon</li>\n<li>Malaysia, Moldova, Mongolia, Myanmar (Burma)</li>\n<li>Nepal, North Korea</li>\n<li>Oman</li>\n<li>Pakistan, Philippines</li>\n<li>Qatar</li>\n<li>Russia</li>\n<li>Saudi Arabia, Sri Lanka, Syria</li>\n<li>Tajikistan, Thailand, T\u00fcrkiye, Turkmenistan</li>\n<li>Ukraine, United Arab Emirates, Uzbekistan</li>\n<li>Vietnam</li>\n<li>Yemen</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\" class=\"wb-inv\">Footnote</h2>\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1a\">\n<p>S.C. 1990, c. 21.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Attendu que\u00a0:</p>\n\n<p>Le 28 juin 2022, en vertu du paragraphe\u00a027.1(2) de la <i>Loi sur la sant\u00e9 des animaux</i><sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup> (la \u00ab\u00a0loi\u00a0\u00bb), la d\u00e9l\u00e9gu\u00e9e de la ministre de l'Agriculture et de l'Agroalimentaire (la \u00ab\u00a0ministre\u00a0\u00bb), Dre Siddika Mithani, pr\u00e9sidente de l'Agence canadienne d'inspection des aliments, a d\u00e9clar\u00e9 des zones de contr\u00f4le secondaires en lien avec le variant canin du virus de la rage;</p>\n\n<p>Le 28 septembre 2022, une ordonnance a \u00e9t\u00e9 prise en vertu du paragraphe\u00a027.1(3) de cette loi, d\u00e9signant les animaux susceptibles d'\u00eatre contamin\u00e9s par le variant canin du virus de la rage (\u00ab\u00a0Ordonnance de d\u00e9signation\u00a0\u00bb);</p>\n\n<p>En vertu du paragraphe\u00a027.1(4) de la loi, la ministre peut, par ordonnance, interdire l'entr\u00e9e, la sortie ou le d\u00e9placement dans toute zone de contr\u00f4le secondaire d'animaux d\u00e9sign\u00e9s;</p>\n\n<p>En tant qu'inspecteur, j'ai le pouvoir, en vertu de l'article\u00a033 de cette loi, d'exercer les pouvoirs conf\u00e9r\u00e9s \u00e0 la ministre en vertu du paragraphe\u00a027.1(4) de cette loi;</p>\n\n<p>Par cons\u00e9quent, en vertu de la pr\u00e9sente ordonnance, j'interdis l'entr\u00e9e, la sortie ou le d\u00e9placement dans l'une de ces zones de contr\u00f4le secondaires d'un animal d\u00e9sign\u00e9 aux termes de l'Ordonnance de d\u00e9signation qui provient d'un pays figurant dans l'annexe\u00a0ci-jointe\u00a0:</p>\n\n<p>Le 28 septembre 2022, \u00e0 Ottawa.<br>Dre Mary Jane Ireland, Inspectrice</p>\n\n<h2>Annexe</h2>\n\n<h3>Pays consid\u00e9r\u00e9s \u00e0 haut risque pour le variant canin du virus de la rage</h3>\n\n<h4>Afrique</h4>\n\n<ul>\n<li>Alg\u00e9rie, Angola</li>\n<li>B\u00e9nin, Botswana, Burkina\u00a0Faso, Burundi</li>\n<li>Cameroun, R\u00e9publique centrafricaine, Tchad, Comores, C\u00f4te d'Ivoire</li>\n<li>R\u00e9publique d\u00e9mocratique du Congo, Djibouti</li>\n<li>\u00c9gypte</li>\n<li>Guin\u00e9e \u00e9quatoriale, \u00c9rythr\u00e9e, Eswatini (Swaziland), \u00c9thiopie</li>\n<li>Gabon, Gambie, Ghana, Guin\u00e9e, Guin\u00e9e-Bissau</li>\n<li>Kenya</li>\n<li>Lesotho, Liberia, Libye</li>\n<li>Madagascar, Malawi, Mali, Mauritanie, Maroc, Mozambique</li>\n<li>Namibie, Niger, Nigeria</li>\n<li>R\u00e9publique du Congo, Rwanda</li>\n<li>Sao Tom\u00e9-et-Principe, S\u00e9n\u00e9gal, Sierra Leone, Somalie, Afrique du Sud, Soudan du Sud, Soudan</li>\n<li>Tanzanie (y compris Zanzibar), Togo, Tunisie</li>\n<li>Ouganda</li>\n<li>Sahara occidental</li>\n<li>Zambie, Zimbabwe</li>\n</ul>\n\n<h4>Am\u00e9rique et Cara\u00efbes</h4>\n\n<ul>\n<li>Belize, Bolivie, Br\u00e9sil</li>\n<li>Colombie, Cuba</li>\n<li>R\u00e9publique dominicaine</li>\n<li>\u00c9quateur, El Salvador</li>\n<li>Guatemala, Guyane</li>\n<li>Ha\u00efti, Honduras</li>\n<li>P\u00e9rou</li>\n<li>Suriname</li>\n<li>Venezuela</li>\n</ul>\n\n<h4>Asie et Moyen-Orient, Europe de l'Est</h4>\n\n<ul>\n<li>Afghanistan, Arm\u00e9nie, Azerba\u00efdjan</li>\n<li>Bangladesh, B\u00e9larus, Brunei</li>\n<li>Cambodge, Chine (continentale seulement)</li>\n<li>G\u00e9orgie</li>\n<li>Inde, Indon\u00e9sie, Iran, Irak</li>\n<li>Jordanie</li>\n<li>Kazakhstan, Kowe\u00eft, Kirghizistan</li>\n<li>Laos, Liban</li>\n<li>Malaisie, Moldavie, Mongolie, Myanmar (Birmanie)</li>\n<li>N\u00e9pal, Cor\u00e9e du Nord</li>\n<li>Oman</li>\n<li>Pakistan, Philippines</li>\n<li>Qatar</li>\n<li>Russie</li>\n<li>Arabie Saoudite, Sri Lanka, Syrie</li>\n<li>Tadjikistan, Tha\u00eflande,, T\u00fcrkiye, Turkm\u00e9nistan</li>\n<li>Ukraine, \u00c9mirats arabes unis, Ouzb\u00e9kistan</li>\n<li>Vietnam</li>\n<li>Y\u00e9men</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\" class=\"wb-inv\">Note de bas de page</h2>\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p>L.C. 1990, ch. 21.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}