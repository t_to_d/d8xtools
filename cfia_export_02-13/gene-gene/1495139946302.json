{
    "dcr_id": "1495139946302",
    "title": {
        "en": "Herd mark identification for groups of pigs",
        "fr": "Marque de troupeau relative aux groupes de porcs"
    },
    "html_modified": "2024-02-13 9:52:12 AM",
    "modified": "2017-07-10",
    "issued": "2017-05-30",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_trace_herd_mark_1495139946302_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_trace_herd_mark_1495139946302_fra"
    },
    "ia_id": "1495139946661",
    "parent_ia_id": "1300461804752",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Traceability",
        "fr": "Tra\u00e7abilit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1300461804752",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Herd mark identification for groups of pigs",
        "fr": "Marque de troupeau relative aux groupes de porcs"
    },
    "label": {
        "en": "Herd mark identification for groups of pigs",
        "fr": "Marque de troupeau relative aux groupes de porcs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1495139946661",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300461751002",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/traceability/herd-mark/",
        "fr": "/sante-des-animaux/animaux-terrestres/tracabilite/marque-de-troupeau/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Herd mark identification for groups of pigs",
            "fr": "Marque de troupeau relative aux groupes de porcs"
        },
        "description": {
            "en": "A herd mark is an identification number unique to a group of pigs that originate from the same premises.",
            "fr": "Une marque de troupeau est un num\u00e9ro d\u2019identification unique \u00e0 un groupe de porcs provenant des m\u00eames installations."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, adminstrators, traceability, surveillance, animal health, disease, detection, identification, herd mark, pigs",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, programme d'identification du b\u00e9tail, tra\u00e7abilit\u00e9, surveillance, sant\u00e9 animal, maladie, d\u00e9tection, marque de troupeau, porcs"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,standards,regulation,animal health",
            "fr": "b\u00e9tail,inspection,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-05-30",
            "fr": "2017-05-30"
        },
        "modified": {
            "en": "2017-07-10",
            "fr": "2017-07-10"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Herd mark identification for groups of pigs",
        "fr": "Marque de troupeau relative aux groupes de porcs"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/eng/1672964761819\"></div>\n\n<p>A herd mark is an identification number unique to a group of pigs that originate from the same premises. A herd mark is used for approved tattoo applied to pigs and, under certain circumstances, may be printed on ear tags approved for pigs. If you wish to obtain a herd mark for a premises under your management that houses pigs, please contact your provincial pork producer association:</p>\n\n<p><a href=\"http://www.albertapork.com\">Alberta Pork</a><br>\n</p>\n<p><a href=\"http://www.saskpork.com\" title=\"obtain a herd mark - Sask Pork\">Sask Pork</a><br> \n</p> \n\n<p><a href=\"http://www.manitobapork.com\" title=\"obtain a herd mark - Manitoba Pork Council\">Manitoba Pork Council</a><br> \n</p> \n\n<p><a href=\"http://www.ontariopork.on.ca\" title=\"obtain a herd mark - Ontario Pork\">Ontario Pork</a><br> \n</p> \n\n<p><span lang=\"fr\"><a href=\"http://www.leseleveursdeporcsduquebec.com/\" title=\"obtain a herd mark - Les \u00c9leveurs de porcs du Qu\u00e9bec\">Les \u00c9leveurs de porcs du Qu\u00e9bec</a></span><br>\n</p> \n\n<p><a href=\"http://www.porcnbpork.nb.ca\" title=\"obtain a herd mark - Porc NB Pork\"><span lang=\"fr\">Porc</span> <abbr title=\"New Brunswick\">NB</abbr> Pork</a><br> \n</p> \n\n<p><a href=\"http://www.peipork.com\" title=\"obtain a herd mark - PEI Pork\"><abbr title=\"Prince Edward Island\">PEI</abbr> Pork</a><br> \n</p> \n\n<p><a href=\"http://www.porknovascotia.ca\" title=\"obtain a herd mark - Pork Nova Scotia\">Pork Nova Scotia</a><br> \n</p>\n\n<p>For producers in British Columbia and Newfoundland and Labrador, contact the <a href=\"http://www.cpc-ccp.com\">Canadian Pork Council</a> at 1-613-236-9239)</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/fra/1672964761819\"></div>\n\n<p>Une marque de troupeau est un num\u00e9ro d'identification unique \u00e0 un groupe de porcs provenant des m\u00eames installations. Une marque de troupeau est utilis\u00e9e lorsqu'un tatouage approuv\u00e9 est appliqu\u00e9 aux porcs et dans certaines circonstances, peut \u00eatre imprim\u00e9e sur des \u00e9tiquettes d'oreille approuv\u00e9es destin\u00e9es aux porcs. Si vous souhaitez obtenir une marque de troupeau relative \u00e0 des installations dont vous assurez la gestion o\u00f9 sont gard\u00e9s des porcs, veuillez communiquer avec votre association provinciale des \u00e9leveurs de porcs\u00a0:</p>\n\n<p><a href=\"http://www.albertapork.com\"><span lang=\"en\">Alberta Pork</span></a><br>\n</p>\n\n<p><a href=\"http://www.saskpork.com\" title=\"obtenir une marque de troupeau - Sask Pork\"><span lang=\"en\">Sask Pork</span></a><br> \n</p> \n\n<p><a href=\"http://www.manitobapork.com\" title=\"obtenir une marque de troupeau - Manitoba Pork Council\"><span lang=\"en\">Manitoba Pork Council</span></a><br>\n</p> \n\n<p><a href=\"http://www.ontariopork.on.ca\" title=\"obtenir une marque de troupeau - Ontario Pork\">Ontario <span lang=\"en\">Pork</span></a><br> \n</p> \n\n<p><a href=\"http://www.leseleveursdeporcsduquebec.com/\" title=\"obtenir une marque de troupeau - Les \u00c9leveurs de porcs du Qu\u00e9bec\">Les \u00c9leveurs de porcs du Qu\u00e9bec</a><br>\n</p> \n\n<p><a href=\"http://www.porcnbpork.nb.ca/fr/\" title=\"obtenir une marque de troupeau - Porc NB Pork\">Porc <span lang=\"en\"><abbr title=\"New Brunswick\">NB</abbr> Pork</span></a><br>\n</p>\n\n<p><a href=\"http://www.peipork.com\" title=\"obtenir une marque de troupeau - PEI Pork\"><span lang=\"en\"><abbr title=\"Prince Edward Island\">PEI</abbr> Pork</span></a><br> \n</p> \n\n<p><a href=\"http://www.porknovascotia.ca\" title=\"obtenir une marque de troupeau - Pork Nova Scotia\"><span lang=\"en\">Pork Nova Scotia</span></a><br> \n</p>\n\n<p>Pour les producteurs de Colombie-Britannique et Terre-Neuve-et-Labrador, veuillez communiquer avec le <a href=\"http://www.cpc-ccp.com\">Conseil canadien du porc</a> \u00e0 1-613-236-9239</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}