{
    "dcr_id": "1329760438133",
    "title": {
        "en": "Questions and answers",
        "fr": "Questions et r\u00e9ponses"
    },
    "html_modified": "2024-02-13 9:47:20 AM",
    "modified": "2020-03-05",
    "issued": "2015-04-13 08:47:14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_questions_1329760438133_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_questions_1329760438133_fra"
    },
    "ia_id": "1329760617115",
    "parent_ia_id": "1306601522570",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1306601522570",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and answers",
        "fr": "Questions et r\u00e9ponses"
    },
    "label": {
        "en": "Questions and answers",
        "fr": "Questions et r\u00e9ponses"
    },
    "templatetype": "content page 1 column",
    "node_id": "1329760617115",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306601411551",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/questions-and-answers/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/questions-et-reponses/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and answers",
            "fr": "Questions et r\u00e9ponses"
        },
        "description": {
            "en": "Invasive plants - Frequently asked questions",
            "fr": "Plantes envahissantes - Foire aux questions"
        },
        "keywords": {
            "en": "invasive plants, Invasive Plants Policy, regulatory action, detection, surveys, mapping, management programs, monitoring, regulations, frequently asked questions, FAQ",
            "fr": "Plantes envahissantes, Politique sur les plantes envahissantes, mesures r&#233;glementaires, d&#233;tection, surveillance, mapping, Programme de surveillance et de gestion, r&#232;glement, Foire aux questions, FAQ"
        },
        "dcterms.subject": {
            "en": "inspection,plants",
            "fr": "inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-13 08:47:14",
            "fr": "2015-04-13 08:47:14"
        },
        "modified": {
            "en": "2020-03-05",
            "fr": "2020-03-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and answers",
        "fr": "Questions et r\u00e9ponses"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Invasive plants are plant species that can be harmful when introduced to new environments. These plants can reproduce quickly and thrive in different habitats.</p>\n\n<p>Invasive plants can grow in natural areas (forests, grasslands and wetlands), managed areas (cultivated fields, gardens, lawns and pastures), and areas where the soil and vegetation have been disturbed (ditches, rights of way and roadsides).</p>\n\n<h2>Why are invasive plants a problem?</h2>\n\n<p>A plant that looks harmless can invade agricultural and natural areas, causing serious damage to Canada's economy and environment. Invasive plants in crops and pastures cost an estimated $2.2 billion each year by reducing crop yields and quality, and increasing costs of weed control and harvesting.</p>\n\n<p>In natural areas, invasive plants can negatively affect ecosystems. They create an imbalance in nature by competing for the same resources that native species need to survive.</p>\n\n<p>The resulting changed landscapes can impact recreational activities and tourism, as well as aesthetic and property values. Human health can also be affected as some invasive plants are toxic, or cause skin reactions or allergies. In some cases, animal health may also be affected.</p>\n\n<h2>How do invasive plants spread?</h2>\n\n<p>Some invasive plants are brought into Canada for agricultural, horticultural or medicinal purposes. Others are introduced when people are not aware a plant is invasive and they introduce it to a new area.</p>\n\n<p>Invasive plants and their seeds can be dispersed naturally by animals, birds, water and wind. Humans can unintentionally move them with soil or as contaminants in forage, grain, seed and other plant products. They can also be carried on clothing, equipment and vehicles, including cars, farm machinery and railway cars.</p>\n\n<h2>What is being done to protect Canada from invasive plants?</h2>\n\n<p>Federal, provincial and municipal governments are working to stop the spread of invasive plants.</p>\n\n<p>As Canada's plant protection agency, the Canadian Food Inspection Agency (CFIA) monitors plants around the world and the potential impacts on plant resources if certain invasive plants became established here.</p>\n\n<p>The CFIA regulates various invasive plant species under the <i>Plant Protection Act</i> and <i>Seeds Act</i>. When an invasive plant species is regulated by the CFIA, there are restrictions on the import, sale and movement of these species into and within Canada.</p>\n\n<h2>What invasive plants are currently regulated by the CFIA?</h2>\n\n<p>Currently, the following plants are regulated by the CFIA under the <i>Plant Protection Act</i>:</p>\n\n<ul>\n<li>Broomrape (<i lang=\"la\">Orobanche</i> spp.)</li>\n<li>Dodder (<i lang=\"la\">Cuscuta</i> spp.)</li>\n<li>Witchweed (<i lang=\"la\">Striga</i> spp.)</li>\n<li>Woolly cup grass (<i lang=\"la\">Eriochloa villosa</i>)</li>\n</ul>\n<h2>How can I help?</h2>\n\n<p>Everyone can play a part in protecting Canada's environment from invasive plants. These are some of the steps you can take to help stop the spread of invasive plants in Canada:</p>\n\n<ul>\n<li>be informed about invasive plants</li>\n<li>avoid planting invasive plants in your garden</li>\n<li>ensure machinery, vehicles and tools are free of soil and plant parts before moving them from one area to another</li>\n<li>avoid moving seeds and plant material on your clothes, footwear, camping gear and vehicles</li>\n<li>declare all plants and related products when returning to Canada</li>\n<li>use clean, high-quality seed that is certified if possible</li>\n<li>use clean grain, hay and straw</li>\n<li>maintain healthy and diverse pastures</li>\n<li>contact your local CFIA office if you suspect you have found an invasive plant. The CFIA will follow up and determine if further action is needed.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les plantes envahissantes sont des esp\u00e8ces v\u00e9g\u00e9tales qui peuvent \u00eatre nuisibles lorsqu'elles sont introduites dans de nouveaux milieux. Ces plantes peuvent se reproduire rapidement et cro\u00eetre dans diff\u00e9rents types de milieux.</p>\n\n<p>Les plantes envahissantes peuvent pousser dans les zones naturelles (mar\u00e9cages, for\u00eats et prairies), les zones cultiv\u00e9es (champs cultiv\u00e9s, jardins, pelouses et p\u00e2turages) et les zones o\u00f9 le sol et la v\u00e9g\u00e9tation ont \u00e9t\u00e9 perturb\u00e9s (foss\u00e9s, emprises et le long des routes).</p>\n\n<h2>Pourquoi les plantes envahissantes constituent-elles un probl\u00e8me?</h2>\n\n<p>Une plante qui para\u00eet inoffensive peut envahir les zones agricoles et naturelles, causant ainsi d'importants dommages \u00e0 l'\u00e9conomie et \u00e0 l'environnement du Canada. La pr\u00e9sence de plantes envahissantes dans les cultures et les p\u00e2turages co\u00fbte au Canada environ 2,2 milliards de dollars par ann\u00e9e, en raison des pertes de productivit\u00e9 et de qualit\u00e9, ainsi que des frais croissants li\u00e9s \u00e0 la lutte contre les mauvaises herbes et \u00e0 la r\u00e9colte.</p>\n\n<p>Dans les zones naturelles, les plantes envahissantes peuvent avoir un effet nuisible sur les \u00e9cosyst\u00e8mes. Elles cr\u00e9ent un d\u00e9s\u00e9quilibre naturel en faisant concurrence pour les m\u00eames ressources essentielles \u00e0 la survie des esp\u00e8ces indig\u00e8nes.</p>\n\n<p>Les paysages alt\u00e9r\u00e9s par ces plantes peuvent avoir une incidence sur les activit\u00e9s r\u00e9cr\u00e9atives et le tourisme, ainsi que sur l'aspect esth\u00e9tique et la valeur des propri\u00e9t\u00e9s. Certaines plantes envahissantes sont toxiques et d'autres peuvent avoir des effets sur la sant\u00e9 humaine, comme des r\u00e9actions cutan\u00e9es ou des allergies. La sant\u00e9 des animaux peut aussi \u00eatre atteinte dans certains cas.</p>\n\n<h2>Comment les plantes envahissantes se propagent-elles?</h2>\n\n<p>Certaines plantes envahissantes sont introduites au Canada \u00e0 des fins agricoles, horticoles ou m\u00e9dicales. D'autres sont transport\u00e9es d'une r\u00e9gion \u00e0 l'autre par des personnes qui ne savent pas qu'il s'agit d'une plante envahissante.</p>\n\n<p>Les plantes envahissantes et leurs graines peuvent \u00eatre dispers\u00e9es naturellement par les animaux, les oiseaux, l'eau et le vent. Les humains peuvent les d\u00e9placer involontairement avec du sol ou en tant que contaminants dans le fourrage, le grain, les semences et d'autres produits v\u00e9g\u00e9taux. Elles peuvent aussi \u00eatre transport\u00e9es sur les v\u00eatements, l'\u00e9quipement et les v\u00e9hicules, y compris les voitures, la machinerie agricole et les wagons.</p>\n\n<h2>Que fait-on pour prot\u00e9ger le Canada contre les plantes envahissantes?</h2>\n\n<p>Les gouvernements f\u00e9d\u00e9ral et provinciaux et les administrations municipales s'emploient \u00e0 mettre fin \u00e0 la propagation des plantes envahissantes.</p>\n\n<p>En tant qu'agence de protection des v\u00e9g\u00e9taux du Canada, l'Agence canadienne d'inspection des aliments (ACIA) surveille les plantes partout dans le monde ainsi que les r\u00e9percussions que pourraient avoir certaines plantes envahissantes sur les ressources v\u00e9g\u00e9tales du Canada si elles s'\u00e9tablissaient au pays.</p>\n\n<p>L'ACIA r\u00e9glemente diverses esp\u00e8ces de plantes envahissantes en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> et la <i>Loi sur les semences</i>. Lorsqu'une esp\u00e8ce envahissante est r\u00e9glement\u00e9e par l'ACIA, des restrictions s'appliquent \u00e0 l'importation, \u00e0 la vente et au d\u00e9placement de ces esp\u00e8ces vers le Canada et \u00e0 l'int\u00e9rieur du pays.</p>\n\n<h2>Quelles sont les plantes r\u00e9glement\u00e9es par l'ACIA sous la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>?</h2>\n\n<p>\u00c0 l'heure actuelle, les plantes \u00e9num\u00e9r\u00e9es ci-apr\u00e8s sont r\u00e9glement\u00e9es par l'ACIA\u00a0:</p>\n\n<ul>\n<li>Cuscute (<i lang=\"la\">Cuscuta</i> spp.)</li>\n<li>\u00c9riochlo\u00e9 velue (<i lang=\"la\">Eriochloa villosa</i>)</li>\n<li>Orobanche (<i lang=\"la\">Orobanche</i> spp.)</li>\n<li>Striga (<i lang=\"la\">Striga</i> spp.)</li>\n</ul>\n<h2>Comment puis-je aider?</h2>\n\n<p>Nous pouvons tous contribuer \u00e0 la protection de l'environnement contre les plantes envahissantes. Voici quelques-unes des mesures que vous pouvez prendre pour aider \u00e0 emp\u00eacher la propagation des plantes envahissantes au Canada\u00a0:</p>\n\n<ul>\n<li>renseignez-vous sur les plantes envahissantes</li>\n<li>\u00e9vitez de planter des plantes envahissantes dans votre jardin</li>\n<li>assurez-vous que la machinerie, les v\u00e9hicules et les outils sont libres de sol et de mat\u00e9riel v\u00e9g\u00e9tal avant de les d\u00e9placer d'un endroit \u00e0 l'autre</li>\n<li>\u00e9vitez de transporter des graines et du mat\u00e9riel v\u00e9g\u00e9tal sur vos v\u00eatements, vos chaussures, votre mat\u00e9riel de camping et vos v\u00e9hicules</li>\n<li>d\u00e9clarez tout v\u00e9g\u00e9tal et produit connexe \u00e0 votre retour au Canada</li>\n<li>utilisez des semences propres et de qualit\u00e9 qui sont certifi\u00e9es, dans la mesure du possible</li>\n<li>utilisez des grains, du foin et de la paille propres</li>\n<li>entretenez des p\u00e2turages sains et diversifi\u00e9s</li>\n<li>communiquez avec votre bureau local de l'ACIA si vous croyez avoir trouv\u00e9 cette plante envahissante. L'ACIA fera un suivi et d\u00e9terminera si d'autres mesures s'imposent.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}