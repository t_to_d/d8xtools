{
    "dcr_id": "1329836269430",
    "title": {
        "en": "Spongy moth (Lymantria dispar dispar)",
        "fr": "La spongieuse (Lymantria dispar dispar)"
    },
    "html_modified": "2024-01-26 2:20:59 PM",
    "modified": "2022-07-11",
    "issued": "2012-02-21 09:57:51",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_lymdis_european_index_1329836269430_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_lymdis_european_index_1329836269430_fra"
    },
    "ia_id": "1329836504450",
    "parent_ia_id": "1329833208596",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1329833208596",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Spongy moth (Lymantria dispar dispar)",
        "fr": "La spongieuse (Lymantria dispar dispar)"
    },
    "label": {
        "en": "Spongy moth (Lymantria dispar dispar)",
        "fr": "La spongieuse (Lymantria dispar dispar)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1329836504450",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329833100787",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/spongy-moth/spongy-moth/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Spongy moth (Lymantria dispar dispar)",
            "fr": "La spongieuse (Lymantria dispar dispar)"
        },
        "description": {
            "en": "The LDD moth (Lymantria dispar dispar) is an invasive pest that can destroy trees by eating their leaves.",
            "fr": "La spongieuse nord-am\u00e9ricaine (Lymantria dispar dispar) est un ravageur envahissant qui peut d\u00e9truire les arbres en mangeant leurs feuilles."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, Lymantria dispar dispar, LDD moth",
            "fr": "phytoravageurs, enqu\u00eates phytosanitaires, phytoparasites justiciables de quarantaine, La spongieuse nord-am\u00e9ricaine, Lymantria dispar dispar"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-21 09:57:51",
            "fr": "2012-02-21 09:57:51"
        },
        "modified": {
            "en": "2022-07-11",
            "fr": "2022-07-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Spongy moth (Lymantria dispar dispar)",
        "fr": "La spongieuse (Lymantria dispar dispar)"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\n<h2 class=\"h3\">Changes to common names</h2>\n<p>The common names for some insects are under review internationally. Spongy moth is the new common name for <i lang=\"la\">Lymantria dispar dispar</i>, previously known as European gypsy moth, EGM or LDD moth. This change does not affect the French name.</p>\n\n<p>The common name for the group of moths (<i lang=\"la\">Lymantria dispar asiatica</i>, <i lang=\"la\">Lymantria dispar japonica</i>, <i lang=\"la\">Lymantria umbrosa</i>, <i lang=\"la\">Lymantria postalba</i> and <i lang=\"la\">Lymantria albescens</i>) referred to as AGM (formerly Asian gypsy moth) has changed to Flighted Spongy Moth Complex (FSMC).</p>\n</section>\n\n<div class=\"mrgn-bttm-lg\"></div>\n\n<div class=\"col-sm-4 col-md-2 mrgn-bttm-md\">\n<img alt=\"Image of two LDD moths. The male is brown and the female is white.\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_lymantria_dispar_dispar_moth_1629815173366_eng.jpg\" class=\"img-responsive\">\n</div>\n\n<p>The spongy moth (<i lang=\"la\">Lymantria dispar dispar</i>) is an invasive pest that can destroy trees by eating their leaves. It is found in Ontario, Quebec, New Brunswick, Nova Scotia and Prince Edward Island. The spongy moth has been detected in British Columbia, Alberta, Saskatchewan and Manitoba, although introductions of the insect have been detected and eradicated.</p>\n\n<h2>What to do</h2>\n\n<ul>\n<li>Inspect trees and things stored outside, including your vehicle and patio furniture</li>\n<li>Look closely at things like tarps, yard and garden items, and sports equipment (including the underside)</li>\n<li>It's important not to accidentally transport these pests to new parts of the country, so if you're travelling or going camping, check your camper, trailer, or RV before making the trip</li>\n<li>Remove or scrape off egg masses, larvae, caterpillars and moths</li>\n<li>Destroy them by soaking them in hot soapy water for at least 2 days</li>\n<li>Put the destroyed insects in your household garbage</li>\n</ul>\n\n<p>If you find the spongy moth in western Canada or Newfoundland and Labrador, please <a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">report it</a>.</p>\n\n<h2>What information is available</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/spongy-moth/fact-sheet/eng/1330355335187/1335975909100\">Pest fact sheet</a></li>\n<li><a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">Don't move firewood</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/moving-across-canada/eng/1629692231648/1629692233085\">Moving across Canada: Don't take the spongy moth with you</a></li>\n</ul>\n\n<h2>Directives</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/imports/d-08-04/eng/1323752901318/1323753560467\">D-08-04: Plant protection import requirements for plants and plant parts for planting</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/d-02-12/eng/1488215831209/1488215831755\">D-02-12: Phytosanitary import requirements for non-processed wood and other wooden products, bamboo and bamboo products, originating from all areas other than the continental United States</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/d-01-12/eng/1323828428558/1323828505539\">D-01-12: Phytosanitary Requirements for the Importation and Domestic Movement of Firewood</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/d-98-09/eng/1323885774950/1323886065560\">D-98-09: Comprehensive policy to control the spread of <i lang=\"la\">Lymantria dispar</i>, in Canada and the United States</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-98-08/eng/1323963831423/1323964732223\">D-98-08: Entry Requirements for Wood Packaging Material into Canada</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-95-08/eng/1322413085880/1322413275292\">D-95-08: Phytosanitary import requirements for fresh temperate fruits and tree nuts</a></li>\n</ul>\n\n<h2>Notices</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/spongy-moth/notice-to-industry-2021-03-19-/eng/1615926058034/1615926326849\">Notice to industry\u00a0\u2013 New compliance measures for facilities moving LDD moth host nursery stock outside of a regulated area</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/archived-notice-to-industry-2017-05-17-/eng/1494960678944/1494960679834\">Notice to industry\u00a0\u2013 Update to regulated zones for spongy moth</a></li>\n</ul>\n\n<h2>Additional information</h2>\n\n<ul>\n<li><a href=\"https://www.aphis.usda.gov/aphis/ourfocus/planthealth/plant-pest-and-disease-programs\">United States Department of Agriculture\u00a0\u2013 Animal and Plant Health Inspection Service</a></li>\n<li><a href=\"https://www2.gov.bc.ca/gov/content/industry/forestry/managing-our-forest-resources/forest-health/invasive-forest-pests/spongy-moth\">British Columbia Ministry of Forests, Land, Natural Resource Operations and Rural Development (FLNRORD)</a></li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\n<h2 class=\"h3\">Changements aux noms communs anglais</h2>\n\n<p>Les noms usuels de certains insectes sont \u00e0 l'\u00e9tude \u00e0 l'\u00e9chelle internationale. Le nom de la spongieuse (<i lang=\"la\">Lymantria dispar dispar</i>, spongieuse europ\u00e9enne ou spongieuse nord-am\u00e9ricaine) a chang\u00e9 en anglais. Cette modification n'affecte pas le nom fran\u00e7ais.</p>\n<p>Le nom commun anglais pour le groupe de papillons de nuit, <i lang=\"la\">Lymantria dispar asiatica</i>, <i lang=\"la\">Lymantria dispar japonica</i>, <i lang=\"la\">Lymantria umbrosa</i>, <i lang=\"la\">Lymantria postalba</i> et <i lang=\"la\">Lymantria albescens</i>, connu sous la forme abr\u00e9g\u00e9e \u00ab\u00a0AGM moth\u00a0\u00bb, a \u00e9t\u00e9 modifi\u00e9 pour \u00ab\u00a0Flighted Spongy Moth Complex (FSMC)\u00a0\u00bb.</p>\n</section>\n\n<div class=\"mrgn-bttm-lg\"></div>\n\n<div class=\"col-sm-4 col-md-2 mrgn-bttm-md\">\n<img alt=\"Photo d'une spongieuse europ\u00e9enne m\u00e2le brun et d'une femelle blanche.\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_species_lymantria_dispar_dispar_moth_1629815173366_fra.jpg\" class=\"img-responsive\">\n</div>\n\n<p>La spongieuse (<i lang=\"la\">Lymantria dispar dispar</i>) est un ravageur envahissant qui peut d\u00e9truire les arbres en mangeant leurs feuilles. On la trouve en Ontario, au Qu\u00e9bec, au Nouveau-Brunswick, en Nouvelle-\u00c9cosse et \u00e0 l'\u00cele-du-Prince-\u00c9douard. La spongieuse a \u00e9t\u00e9 d\u00e9tect\u00e9e en Colombie-Britannique, en Alberta, en Saskatchewan et au Manitoba, bien que son introduction ait \u00e9t\u00e9 d\u00e9tect\u00e9e et que l'insecte ait \u00e9t\u00e9 \u00e9radiqu\u00e9.</p>\n\n<h2>Quoi faire</h2>\n\n<ul>\n<li>Inspectez les arbres ainsi que les objets entrepos\u00e9s \u00e0 l'ext\u00e9rieur, y compris votre v\u00e9hicule et vos meubles d'ext\u00e9rieur. </li>\n<li>Examinez de pr\u00e8s vos articles, comme les b\u00e2ches, les \u00e9quipements de jardinage et les \u00e9quipements d'ext\u00e9rieur, ainsi que l'\u00e9quipement de sport (y compris leur dessous). </li>\n<li>Il est important de ne pas transporter accidentellement ces ravageurs vers de nouvelles r\u00e9gions du pays. Si vous voyagez ou allez camper, inspectez votre auto-caravane, votre roulotte ou votre v\u00e9hicule r\u00e9cr\u00e9atif, avant de vous mettre en route. </li>\n<li>Enlevez ou grattez les masses d'\u0153ufs, les larves, les chenilles et les papillons de nuit. </li>\n<li>D\u00e9truisez-les en les trempant dans de l'eau chaude savonneuse, pendant au moins 2 jours. </li>\n<li>Jetez \u00e0 la poubelle les insectes que vous avez ainsi d\u00e9truits. </li>\n</ul>\n\n<p>Si vous d\u00e9tectez la pr\u00e9sence de la spongieuse dans l'ouest du Canada ou \u00e0 Terre-Neuve-et-Labrador, veuillez <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">le signaler</a>.</p>\n\n<h2>Renseignements disponibles</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/fiche-de-renseignements/fra/1330355335187/1335975909100\">Fiche de renseignements sur le ravageur</a></li>\n<li><a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">Ne d\u00e9placez pas le bois de chauffage</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/demenagement-au-canada/fra/1629692231648/1629692233085\">D\u00e9m\u00e9nagement au Canada\u00a0: n'emportez pas la spongieuse avec vous</a></li>\n</ul>\n\n<h2>Directives</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-08-04/fra/1323752901318/1323753560467\">D-08-04\u00a0: Exigences phytosanitaires r\u00e9gissant l'importation de v\u00e9g\u00e9taux et de parties de v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-02-12/fra/1488215831209/1488215831755\">D-02-12\u00a0: Exigences phytosanitaires pour l'importation de produits en bois non transform\u00e9 et autres produits de bois, bambou et produits de bambou, en provenance de toutes les r\u00e9gions du monde autres que la zone continentale des \u00c9tats-Unis</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-01-12/fra/1323828428558/1323828505539\">D-01-12\u00a0: Exigences phytosanitaires r\u00e9gissant l'importation et le transport en territoire canadien de bois de chauffage</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-98-09/fra/1323885774950/1323886065560\">D-98-09\u00a0: Politique globale de lutte contre la propagation de la spongieuse, <i lang=\"la\">Lymantria dispar</i> au Canada et aux \u00c9tats-Unis</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-98-08/fra/1323963831423/1323964732223\">D-98-08\u00a0: Exigences relatives \u00e0 l'entr\u00e9e au Canada des mat\u00e9riaux d'emballage en bois</a></li>\n<li><a href=\"/fra/1323819186585/1323819458696\">D-96-12\u00a0: Programme de certification des serres aux fins de l'exportation de plantes de serre vers les \u00c9tats-Unis</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-95-08/fra/1322413085880/1322413275292\">D-95-08\u00a0: Exigences phytosanitaires d'importation pour les fruits temp\u00e9r\u00e9s frais et les noix</a></li>\n</ul>\n\n<h2>Avis</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/avis-a-l-industrie-2021-03-19-/fra/1615926058034/1615926326849\">Avis \u00e0 l'industrie\u00a0\u2013 nouvelles mesures de conformit\u00e9 pour les \u00e9tablissements qui d\u00e9placent du mat\u00e9riel de p\u00e9pini\u00e8re h\u00f4te de la spongieuse \u00e0 l'ext\u00e9rieur d'une zone r\u00e9glement\u00e9e</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/archivee-avis-a-l-industrie-2017-05-17-/fra/1494960678944/1494960679834\">Avis \u00e0 l'industrie\u00a0\u2013 Mise \u00e0 jour des zones r\u00e9glement\u00e9es \u00e0 l'\u00e9gard de la spongieuse</a></li>\n</ul>\n\n<h2>Renseignements suppl\u00e9mentaires</h2>\n\n<ul>\n<li><a href=\"https://www.aphis.usda.gov/aphis/ourfocus/planthealth/plant-pest-and-disease-programs\">D\u00e9partement de l'Agriculture des \u00c9tats-Unis \u2013 Service de l'inspection sanitaire animale et v\u00e9g\u00e9tale (en anglais seulement)</a></li>\n<li><a href=\"https://www2.gov.bc.ca/gov/content/industry/forestry/managing-our-forest-resources/forest-health/invasive-forest-pests/spongy-moth\">Minist\u00e8re des for\u00eats, des terres, de l'exploitation des ressources naturelles et du d\u00e9veloppement rural de la Colombie-Britannique (en anglais seulement)</a></li>\n</ul>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}