{
    "dcr_id": "1646347538968",
    "title": {
        "en": "Fertilizer or supplement registration: After you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "html_modified": "2024-03-12 3:59:13 PM",
    "modified": "2022-08-09",
    "issued": "2022-08-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-after_you_apply_1646347538968_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-after_you_apply_1646347538968_fra"
    },
    "ia_id": "1646347539296",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fertilizer or supplement registration: After you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "label": {
        "en": "Fertilizer or supplement registration: After you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646347539296",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/after-you-apply/",
        "fr": "/protection-des-vegetaux/engrais/apres-avoir-presente-une-demande/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fertilizer or supplement registration: After you apply",
            "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
        },
        "description": {
            "en": "The pre-screening process is divided into 2 phases: 1. completeness check 2. first response.",
            "fr": "Le processus d'examen initial se divise en 2 parties\u00a0: 1. La v\u00e9rification de la compl\u00e9tude de la soumission 2. La premi\u00e8re r\u00e9ponse."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizer regulations, Fertilizer, supplement, registration, After you apply, new",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, Enregistrement, engrais, suppl\u00e9ment,\u00a0Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande, nouveau"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy",
            "fr": "cultures,engrais,inspection,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "modified": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fertilizer or supplement registration: After you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/overview/eng/1646095692928/1646095693725\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646102406709/1646102407100\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/before-you-apply/eng/1646274247575/1646274247919\">3. Before you apply</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/how-to-apply/eng/1646282540949/1646282541291\">4. How to apply</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">5. After you apply</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646359107928/1646359108272\">6. Amending and renewing</a></li>\n\n</ul>\n</div>\n</div>\n\n<h2>Pre-screening</h2>\n\n<h3>First response</h3>\n\n<p>The first response is a non-technical scan after the submission has entered the Canadian Food Inspection Agency's (CFIA) file tracking system. During this step, the CFIA verifies that the applicant has included all of the mandatory information in their submission.</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Additional information</h3>\n</header>\n<div class=\"panel-body\">\n<p>For more information about the file review procedures, refer to <a href=\"/eng/1305609994431/1307910971122#a5\">file submission management process</a>.</p>\n</div>\n</section>\n</div>\n\n\n\n<p>This scan does not evaluate the quality of the information submitted. Safety reviews don't begin until an application reaches the first review stage.</p>\n\n<h4>First response letter</h4>\n\n<p>The CFIA will outline any deficiencies identified during this step for the applicant via a first response letter (electronic notification within My\u00a0CFIA).</p>\n\n<p>The applicant must respond within\u00a030 days, addressing the deficiencies. The CFIA will close the application and will refund the associated safety fees; the CFIA won't refund application fees, if either:</p>\n\n\n<ul>\n<li>an applicant's response doesn't fully address each deficiency</li>\n<li>the CFIA doesn't receive an applicant's response by the stated deadline</li>\n</ul>\n\n<p>If no outstanding information requirements exist, the CFIA will:</p>\n\n<ul>\n<li>enter the application into the first review queue</li>\n<li>inform the applicant that their application is in the queue</li>\n<li>send the applicant the deadline (calendar date) for the CFIA's first review</li>\n</ul>\n\n<h2>Technical review of applications for permissions</h2>\n\n<p>During the technical review stages, evaluators review the information in the submission against the requirements of the <i>Fertilizers Act</i> and regulations and associated policies. Evaluators also deal with all administrative requirements during the technical review.</p>\n\n<p>The application review (both for label compliance and safety) is divided into 3 possible iterative reviews (not all of which are mandatory):</p>\n\n<ul>\n<li>first review</li>\n<li>second review</li>\n<li>third review</li>\n</ul>\n\n<h2>Results of the first or second review</h2>\n\n<p>The CFIA may:</p>\n\n<ul>\n<li>register the product under the <i>Fertilizers Act</i></li>\n<li>send the applicant an electronic notification within My\u00a0CFIA detailing deficiencies, identifying any missing information or necessary label changes</li>\n<li>deny the application if the CFIA identifies safety issues that could not be addressed without substantive changes to the product</li>\n</ul>\n\n<p>The applicant must respond by the stated deadline and adequately address each deficiency in their response.</p>\n\n<p>If the CFIA receives all the requested information on or before the stated deadline, evaluators will move the application into the next review queue.</p>\n\n<p>If the response is incomplete or the CFIA receives the response after the stated deadline, the CFIA will:</p>\n\n<ul>\n<li>close (deny) the application</li>\n<li>not refund fees</li>\n</ul>\n<h2>Results of the third review</h2>\n\n<p>Once the third review is complete, if the applicant hasn't adequately addressed all of the questions or deficiencies, the CFIA will close the application.</p>\n\n<p>If the applicant addresses the requirements identified in the second review letter (electronic notification within My\u00a0CFIA), the CFIA will register the product.</p>\n\n<h2>Marketplace monitoring</h2>\n\n<p>All products regulated under the <i>Fertilizers Act</i> and regulations are subject to marketplace monitoring.</p>\n\n<p>Marketplace monitoring includes:</p>\n\n<ul>\n<li>product inspections</li>\n<li>sampling</li>\n<li>testing</li>\n<li>label verification</li>\n</ul>\n\n<p>Non-compliant products may be subject to regulatory action. Regulatory action can include product detention (stop sale) and in cases of serious or repeated non-compliance, prosecution.</p>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/plant-health/fertilizers/how-to-apply/eng/1646282540949/1646282541291\" rel=\"prev\">Previous</a></li>\n\n<li class=\"next\"><a href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646359107928/1646359108272\" rel=\"next\">Next</a></li>\n\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apercu/fra/1646095692928/1646095693725\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646102406709/1646102407100\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646274247575/1646274247919\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646282540949/1646282541291\">4. Comment pr\u00e9senter une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646359107928/1646359108272\">6. Modifier et renouveler</a></li>\n\n</ul>\n</div>\n</div>\n\n\n<h2>Examen initial</h2>\n\n<h3>La premi\u00e8re r\u00e9ponse</h3>\n\n<p>La premi\u00e8re r\u00e9ponse est un balayage non technique apr\u00e8s l'entr\u00e9e de la soumission dans le syst\u00e8me de suivi des dossiers de l'Agence canadienne d'inspection des aliments (ACIA). Au cours de cette \u00e9tape, l'ACIA v\u00e9rifie que le requ\u00e9rant a inclus toutes les informations obligatoires dans sa soumission.</p>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h4 class=\"panel-title\">Informations suppl\u00e9mentaires</h4>\n</header>\n<div class=\"panel-body\">\n<p>Pour en savoir plus sur le processus de gestion des demandes, veuillez consulter le <a href=\"/fra/1305609994431/1307910971122#a5\">processus de gestion des demandes</a>.</p>\n</div>\n</section>\n</div>\n\n<p>Cet \u00e9valuation ne permet pas d'\u00e9valuer la qualit\u00e9 des informations soumises. Les \u00e9valuations de s\u00e9curit\u00e9 ne commencent pas avant qu'une demande n'atteigne le premier stade d'\u00e9valuation.</p>\n\n<h4>Lettre de premi\u00e8re r\u00e9ponse</h4>\n\n<p>L'ACIA indique au requ\u00e9rant les lacunes identifi\u00e9es au cours de cette \u00e9tape par le biais d'une lettre de premi\u00e8re r\u00e9ponse (notification \u00e9lectronique dans Mon\u00a0ACIA).</p>\n\n<p>Le requ\u00e9rant doit r\u00e9pondre dans les\u00a030\u00a0jours, en corrigeant les lacunes. L'ACIA fermera la demande et remboursera les frais de s\u00e9curit\u00e9 associ\u00e9s; l'ACIA ne remboursera pas les frais de demande, le cas \u00e9ch\u00e9ant\u00a0:</p>\n\n<ul>\n<li>la r\u00e9ponse du requ\u00e9rant ne r\u00e9pond pas enti\u00e8rement \u00e0 chaque lacune</li>\n<li>l'ACIA ne re\u00e7oit pas la r\u00e9ponse d'un requ\u00e9rant dans le d\u00e9lai fix\u00e9</li>\n</ul>\n\n<p>Si aucun renseignement suppl\u00e9mentaire n'est exig\u00e9, l'ACIA\u00a0:</p>\n\n<ul>\n<li>Entre la demande dans la file d'attente de la premi\u00e8re \u00e9valuation</li>\n<li>Informe le requ\u00e9rant que sa demande est entr\u00e9e dans la file d'attente</li>\n<li>Envoie \u00e9galement la date limite (date du calendrier) de la premi\u00e8re \u00e9valuation de l'ACIA.</li>\n</ul>\n\n<h2>\u00c9valuation technique des demandes d'autorisation</h2>\n\n<p>Pendant les \u00e9tapes de l'\u00e9valuation technique, les \u00e9valuateurs v\u00e9rifient les renseignements de la demande en fonction des exigences de la <i>Loi sur les engrais</i>, de son r\u00e8glement d'application et des politiques connexes. Toutes les exigences administratives sont \u00e9galement trait\u00e9es au cours de cette \u00e9tape.</p>\n\n<p>L'\u00e9valuation de la demande (tant pour la conformit\u00e9 de l'\u00e9tiquette que l'innocuit\u00e9) se divise en\u00a03\u00a0\u00e9valuations it\u00e9ratives possibles (qui ne sont pas toutes obligatoires)\u00a0:</p>\n\n<ul>\n<li>Premi\u00e8re \u00e9valuation</li>\n<li>Deuxi\u00e8me \u00e9valuation</li>\n<li>Troisi\u00e8me \u00e9valuation</li>\n</ul>\n\n<h2>R\u00e9sultats de la premi\u00e8re ou deuxi\u00e8me \u00e9valuation</h2>\n\n<p>L'ACIA peut soit\u00a0:</p>\n\n<ul>\n<li>Enregistrer le produit en vertu de la <i>Loi sur les engrais</i></li>\n<li>Envoyer au requ\u00e9rant une notification \u00e9lectronique dans Mon\u00a0ACIA d\u00e9taillant les lacunes, en indiquant toute information manquante ou tout changement d'\u00e9tiquette n\u00e9cessaire</li>\n<li>Refuser la demande si l'ACIA identifie des probl\u00e8mes de s\u00e9curit\u00e9 qui ne peuvent \u00eatre r\u00e9solus sans apporter des modifications importantes au produit.</li>\n</ul>\n\n<p>Le requ\u00e9rant doit r\u00e9pondre dans le d\u00e9lai fix\u00e9 et traiter de mani\u00e8re ad\u00e9quate chaque lacune dans sa r\u00e9ponse.</p>\n\n<p>Si l'ACIA re\u00e7oit tous les renseignements demand\u00e9s au plus tard \u00e0 la date limite indiqu\u00e9e, les \u00e9valuateurs feront passer la demande dans la prochaine file d'attente des \u00e9valuations.</p>\n\n<p>Si la r\u00e9ponse est incompl\u00e8te ou si l'ACIA re\u00e7oit la r\u00e9ponse apr\u00e8s la date limite indiqu\u00e9e, l'ACIA\u00a0:</p>\n\n<ul>\n<li>Fermera (rejettera) la demande</li>\n<li>Ne remboursera pas les frais</li>\n</ul>\n\n<h2>R\u00e9sultats de la troisi\u00e8me \u00e9valuation</h2>\n\n<p>Une fois la troisi\u00e8me \u00e9valuation termin\u00e9e si le requ\u00e9rant n'a pas correctement r\u00e9pondu \u00e0 toutes les questions ou lacunes, l'ACIA fermera la demande.</p>\n\n<p>Si le requ\u00e9rant r\u00e9pond aux exigences identifi\u00e9es dans la deuxi\u00e8me lettre d'examen (notification \u00e9lectronique dans Mon\u00a0ACIA), l'ACIA enregistrera le produit.</p>\n\n<h2>Surveillance du march\u00e9</h2>\n\n<p>Tous les produits r\u00e9glement\u00e9s en vertu de la <i>Loi sur les engrais</i> et de son r\u00e8glement d'application peuvent \u00eatre soumis \u00e0 une surveillance du march\u00e9.</p>\n\n<p>Une surveillance du march\u00e9 comprend\u00a0:</p>\n\n<ul>\n<li>des inspections de produits</li>\n<li>un \u00e9chantillonnage</li>\n<li>des tests</li>\n<li>une v\u00e9rification des \u00e9tiquettes</li>\n</ul>\n\n<p>Les produits non conformes peuvent faire l'objet de mesures r\u00e9glementaires. Les mesures r\u00e9glementaires peuvent inclure la d\u00e9tention du produit (arr\u00eat de la vente) et, en cas de non-conformit\u00e9 grave ou r\u00e9p\u00e9t\u00e9e, des poursuites judiciaires.</p>\n\n\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646282540949/1646282541291\" rel=\"prev\">Pr\u00e9c\u00e9dent</a></li>\n\n<li class=\"next\"><a href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646359107928/1646359108272\" rel=\"next\">Suivant</a></li>\n\n</ul>\n</nav>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}