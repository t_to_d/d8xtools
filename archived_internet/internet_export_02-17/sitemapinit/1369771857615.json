{
    "dcr_id": "1344639901615",
    "title": {
        "en": "Planning your shelterbelt",
        "fr": "Planification de votre brise-vent"
    },
    "modified": "2014-12-23",
    "issued": "2012-10-30",
    "templatetype": "content page 1 column",
    "page_type": "1",
    "node_id": "1369771857615",
    "dc_date_created": "2012-10-30",
    "managing_branch": "PAB",
    "parent_node_id": "1354137773742",
    "label": {
        "en": "Planning Your Shelterbelt",
        "fr": "Planification de votre brise-vent"
    },
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/08/agroforest_shltrblt_plan_1344639901615_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/08/agroforest_shltrblt_plan_1344639901615_fr"
    },
    "type_name": "sitemapinit",
    "has_dcr_id": true,
    "breadcrumb": {
        "en": "Planning Your Shelterbelt",
        "fr": "Planification de votre brise-vent"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "meta": {
        "issued": {
            "en": "2012-10-30",
            "fr": "2012-10-30"
        },
        "modified": {
            "en": "2014-12-23",
            "fr": "2014-12-23"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Planning your shelterbelt",
            "fr": "Planification de votre brise-vent"
        },
        "subject": {
            "en": "livestock;programs",
            "fr": "b\u00e9tail;agriculture;programme"
        },
        "description": {
            "en": "Decide what shelterbelt design you will need and the area available to plant it in. The design should match the equipment that you will use to prepare the site plant the trees and most importantly control weeds after the shelterbelt has been planted.",
            "fr": "D\u00e9terminer \u00e9galement les autres conditions du site qui pourraient \u00eatre am\u00e9lior\u00e9es par un brise-vent et les facteurs qui pourraient nuire \u00e0 son efficacit\u00e9.\u00c9valuation du paysage - Cerner les aspects li\u00e9s aux ressources environnantes qui pourraient nuire au brise-vent ou qui pourraient \u00eatre compromis par sa pr\u00e9sence."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "several years;complete shelterbelt plan;Landscape assessment;planning objectives;shelterbelt design",
            "fr": "assortiment d'essences d'arbres;r\u00e9gion de la rivi\u00e8re de la Paix;liste de possibilit\u00e9s;plantation du brise-vent;objectifs de planification"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "publication",
            "fr": "plan d'activit\u00e9s"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "css": {
        "fr": null
    },
    "js": {
        "en": null,
        "fr": null
    },
    "body": {
        "en": "<!-- D&eacute;but du contenu -->\r\n    <p>It is important to plan your shelterbelt. Planning involves reviewing what you have and determining what you will need. Choose plants that will grow well in your location. Decide what shelterbelt design you will need and the area available to plant it in. The design should match the equipment that you will use to prepare the site, plant the trees and most importantly, control weeds after the shelterbelt has been planted.</p>\n<p>As you plan your shelterbelt, keep the following considerations in mind:</p>\n<ul>\n<li>Locate the shelterbelt where it will be most effective.</li>\n<li>Design the shelterbelt to fit the available space and to meet your objectives. The design must take into account proper spacing to allow for optimum tree growth and the use of maintenance equipment.</li>\n<li>Select tree and shrub species that are well adapted to your soil and climatic conditions.</li>\n<li>Prepare the planting site and fence areas to exclude livestock.</li>\n<li>Arrange for planting labour and equipment to plant the trees.</li>\n<li>Provide care and protection for young seedlings.</li>\n<li>Control weeds after shelterbelt establishment.</li>\n</ul>\n \n<h2>Determine Objectives</h2>\n<p>Before you design and plant a shelterbelt, ask yourself what you want to accomplish. Initially, you may have only a general idea of the problems on your site that can be solved with a shelterbelt. This provides a starting point, but you should conduct both a site and a landscape assessment.</p>\n<ul>\n<li>Site assessment - Identify initial areas of concern and verify needs. Also, identify other conditions that could be improved by, or limit the effectiveness of a shelterbelt at your site.</li>\n<li>Landscape assessment - Identify resource conditions and problems in the surrounding area that could affect, or be affected by, a shelterbelt.</li>\n</ul>\n \n<h2>Develop Alternatives and Select One</h2>\n<p>Developing a shelterbelt design may require creating a few alternatives to consider and choosing the most suitable. A complete shelterbelt plan will indicate its location, size, and tree and shrub composition. Also include management and maintenance options.</p>\n\n\n<h2>Implement and Monitor</h2>\n<p>Small shelterbelt projects may only require minimal planning. However large or more complicated projects will require more detailed planning to organize numerous activities, people, and equipment.</p>\n<p>After the shelterbelt has been planted, monitor how well each of your planning objectives is being met. It may take several years to achieve some of your goals. For these, regular monitoring can help you determine the progress in meeting your objectives.</p>\n\n<h2>Modify if Needed</h2>\n<p>The planning process is a learning process. New information often leads to better assessments of problems and limitations, changing priorities, and new or modified objectives. If monitoring suggests that the planning objectives are not being met, modify your plan.</p>\n\n\n\n\r\n    <!-- Fin du contenu -->",
        "fr": "\r\n    \r\n    <!-- MainContentStart -->\r\n    <!-- D&eacute;but du contenu -->\r\n    <p>La planification est une \u00e9tape importante de l'\u00e9tablissement des brise-vent. Elle consiste \u00e0 faire un \u00e9tat des lieux et \u00e0 d\u00e9terminer ce dont vous aurez besoin. Vous devez choisir des essences qui poussent bien dans votre r\u00e9gion et d\u00e9cider quel type de brise-vent conviendra et \u00e0 quel endroit le planter. Le plan de conception doit tenir compte de l'\u00e9quipement que vous utiliserez pour pr\u00e9parer le terrain, planter les arbres et, surtout, pour mener la lutte contre les mauvaises herbes apr\u00e8s la plantation du brise-vent.</p>\n<p>Lorsque vous planifiez votre brise-vent, pensez aux points suivants\u00a0: </p>\n<ul>\n<li>Le brise-vent doit \u00eatre plant\u00e9 l\u00e0 o\u00f9 il sera le plus efficace.</li><li>Concevez votre brise-vent pour qu'il s'ins\u00e8re dans l'espace disponible et qu'il r\u00e9ponde \u00e0 vos objectifs. Il faut pr\u00e9voir l'espacement ad\u00e9quat pour permettre la croissance optimale des arbres et le passage du mat\u00e9riel d'entretien.</li><li>Choisissez des essences d'arbres et d'arbustes adapt\u00e9es \u00e0 votre sol et aux conditions climatiques locales.</li><li>Lorsque vous pr\u00e9parez la bande de plantation, pr\u00e9voyez des cl\u00f4tures afin d'en exclure le b\u00e9tail.</li><li>Tenez compte des besoins en main-d'\u0153uvre et en mat\u00e9riel pour la plantation des arbres.</li><li>Prenez soin des jeunes plants et pr\u00e9voyez des moyens de protection.</li><li>Pr\u00e9voyez un programme de lutte contre les mauvaises apr\u00e8s l'\u00e9tablissement du brise-vent.</li></ul> \n \n<h2>D\u00e9finir les objectifs</h2>\n<p>Avant de concevoir et de planter un brise-vent, r\u00e9fl\u00e9chissez \u00e0 ce que vous voulez accomplir. Au d\u00e9part, vous n'aurez peut-\u00eatre qu'une id\u00e9e g\u00e9n\u00e9rale des probl\u00e8mes sur votre site auxquels un brise-vent pourrait apporter une solution. Il s'agit d'un point de d\u00e9part, mais il faudra proc\u00e9der \u00e0 une \u00e9valuation du terrain et du paysage environnant.</p>\n<ul> <li>\u00c9valuation des lieux - Cernez les principaux probl\u00e8mes et v\u00e9rifiez les besoins. D\u00e9terminer \u00e9galement les autres conditions du site qui pourraient \u00eatre am\u00e9lior\u00e9es par un brise-vent et les facteurs qui pourraient nuire \u00e0 son efficacit\u00e9.</li><li>\u00c9valuation du paysage - Cerner les aspects li\u00e9s aux ressources environnantes qui pourraient nuire au brise-vent ou qui pourraient \u00eatre compromis par sa pr\u00e9sence.</li></ul>\n \n<h2>Dresser une liste de possibilit\u00e9s et en choisir une</h2>\n<p>La conception d'un brise-vent peut n\u00e9cessiter de dresser une liste de possibilit\u00e9s diff\u00e9rentes et de choisir la plus appropri\u00e9e. Le plan d'ensemble du brise-vent comprendra son emplacement et sa grandeur et l'assortiment d'essences d'arbres et d'arbustes, ainsi que les options de conduite culturale et d'entretien.</p>\n \n\n<h2>Mise en \u0153uvre et suivi</h2><p>Pour les petits projets de plantation de brise-vent, une planification minimale suffira le plus souvent. Toutefois, les projets plus vastes ou \u00e9labor\u00e9s n\u00e9cessiteront une planification d\u00e9taill\u00e9e des nombreux travaux n\u00e9cessaires et des besoins en main-d'\u0153uvre et en mat\u00e9riel.</p><p>Apr\u00e8s la plantation du brise-vent, il faudra surveiller la fa\u00e7on dont chacun des objectifs de planification ont \u00e9t\u00e9 respect\u00e9s. Il faudra attendre plusieurs ann\u00e9es pour voir certains de ces objectifs se concr\u00e9tiser. Un suivi r\u00e9gulier permettra de d\u00e9terminer les progr\u00e8s r\u00e9alis\u00e9s \u00e0 cet \u00e9gard.</p>\n \n<h2>Modifier si n\u00e9cessaire</h2><p>Le processus de planification en est aussi un d'apprentissage. De nouvelles informations permettent souvent de mieux \u00e9valuer les probl\u00e8mes et les contraintes et de modifier les priorit\u00e9s et les objectifs. Si votre surveillance de suivi r\u00e9v\u00e8le que les objectifs de planification n'ont pas \u00e9t\u00e9 atteints, il faudra modifier le plan.</p>\n \n\n\n \n\r\n    <!-- Fin du contenu -->\r\n\t\r\n\t\r\n\t\r\n  "
    }
}