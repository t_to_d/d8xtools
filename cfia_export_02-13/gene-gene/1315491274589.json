{
    "dcr_id": "1315491274589",
    "title": {
        "en": "Swine Vesicular Disease - Fact Sheet",
        "fr": "Maladie v\u00e9siculeuse du porc - Fiche de renseignements "
    },
    "html_modified": "2024-02-13 9:47:02 AM",
    "modified": "2014-03-26",
    "issued": "2011-09-08 10:14:38",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_swinevesic_fact_1315491274589_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_swinevesic_fact_1315491274589_fra"
    },
    "ia_id": "1315491569672",
    "parent_ia_id": "1315488608966",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1315488608966",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Swine Vesicular Disease - Fact Sheet",
        "fr": "Maladie v\u00e9siculeuse du porc - Fiche de renseignements "
    },
    "label": {
        "en": "Swine Vesicular Disease - Fact Sheet",
        "fr": "Maladie v\u00e9siculeuse du porc - Fiche de renseignements "
    },
    "templatetype": "content page 1 column",
    "node_id": "1315491569672",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1315488431571",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/swine-vesicular-disease/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/maladie-vesiculeuse-du-porc/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Swine Vesicular Disease - Fact Sheet",
            "fr": "Maladie v\u00e9siculeuse du porc - Fiche de renseignements"
        },
        "description": {
            "en": "Fact Sheet - Swine vesicular disease is an acute, contagious viral disease of pigs characterised by fever and vesicles (fluid-filled blisters) in the mouth and on the snout, feet, and teats.",
            "fr": "Fiche de renseignements - La maladie v\u00e9siculeuse du porc (MVP) est une virose aigu\u00eb contagieuse du porc caract\u00e9ris\u00e9e par de la fi\u00e8vre et la pr\u00e9sence de v\u00e9sicules (ampoules remplies de fluide) dans la bouche, ainsi que sur le museau, les pattes et les mamelles."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, Vesicular stomatitis, swine vesicular disease, swine, fact sheet",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, maladie v\u00e9siculeuse du porc, porcs, fiche de renseignements"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-09-08 10:14:38",
            "fr": "2011-09-08 10:14:38"
        },
        "modified": {
            "en": "2014-03-26",
            "fr": "2014-03-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Swine Vesicular Disease - Fact Sheet",
        "fr": "Maladie v\u00e9siculeuse du porc - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is swine vesicular disease?</h2>\n<p>Swine vesicular disease (SVD) is a contagious viral disease of pigs characterized by fever and vesicles (fluid-filled blisters) in the mouth and on the snout, feet and teats.</p>\n<p>The vesicular lesions are undistinguishable to the naked eye from those caused by foot-and-mouth disease (FMD). The illness varies in severity but is rarely fatal.</p>\n<h2 class=\"h5\">Is <abbr title=\"Swine vesicular disease\">SVD</abbr> a risk to human health?</h2>\n<p>No. The disease does not affect humans. However, rare cases of accidental human infection have been reported in laboratory personnel working with the virus.</p>\n<h2 class=\"h5\">What are the clinical signs of <abbr title=\"Swine vesicular disease\">SVD</abbr>?</h2>\n<p>The visible signs of this disease may easily be confused with those of <abbr title=\"foot-and-mouth disease\">FMD</abbr> and include:</p>\n<ul>\n<li>fever and loss of appetite;</li>\n<li>sudden appearance of lameness in several animals in close contact;</li>\n<li>limping, an uncomfortable appearance, or a refusal to move on hard surfaces;</li>\n<li>vesicles (fluid-filled blisters) on the snout, feet, mouth, tongue and teats;</li>\n<li>vesicular ruptures, leaving erosive lesions; and</li>\n<li>foot pads may become loosened or a loss of hoof may occur.</li>\n</ul>\n<p>Young animals are more severely affected. Recovery occurs usually within one to three weeks, with little to no mortality.</p>\n<h2 class=\"h5\">Where is <abbr title=\"Swine vesicular disease\">SVD</abbr> found?</h2>\n<p><abbr title=\"Swine vesicular disease\">SVD</abbr> was first described in Italy in 1966, and it is still present in limited locations, resulting in occasional clinical cases.</p>\n<p><abbr title=\"Swine vesicular disease\">SVD</abbr> has never been found in Canada.</p>\n<h2 class=\"h5\">How is <abbr title=\"Swine vesicular disease\">SVD</abbr> transmitted and spread?</h2>\n<p>The disease is spread primarily through contact with infected swine, or through a contaminated environment, as the virus is extremely resistant in the environment. The virus can also survive in pork and processed pork products for extended periods of time, so the feeding of contaminated food scraps can result in transmission of the virus.</p>\n<h2 class=\"h5\">How is <abbr title=\"Swine vesicular disease\">SVD</abbr> diagnosed?</h2>\n<p>Laboratory tests are necessary to confirm a diagnosis and rule out the possibility of other vesicular diseases such as <abbr title=\"foot-and-mouth disease\">FMD</abbr>.</p>\n<h2 class=\"h5\">How is <abbr title=\"Swine vesicular disease\">SVD</abbr> treated?</h2>\n<p>There is no treatment for <abbr title=\"Swine vesicular disease\">SVD</abbr> and there are no vaccines available.</p>\n<h2 class=\"h5\">What is done to protect Canadian livestock from <abbr title=\"Swine vesicular disease\">SVD</abbr>?</h2>\n<p>The Canadian Food Inspection Agency (CFIA) imposes strict regulations on the import of animals and animal products from countries where <abbr title=\"Swine vesicular disease\">SVD</abbr> is known to occur. These regulations are enforced through port-of-entry inspections done either by the Canada Border Services Agency or the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p><abbr title=\"Swine vesicular disease\">SVD</abbr> is a \"reportable disease\" under the <i>Health of Animals Act</i>. This means that all suspected cases must be reported to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for immediate investigation by inspectors.</p>\n<h2 class=\"h5\">How would the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> respond to an outbreak of <abbr title=\"Swine vesicular disease\">SVD</abbr> in Canada?</h2>\n<p>Canada's emergency response strategy to an outbreak of <abbr title=\"Swine vesicular disease\">SVD</abbr> would be to:</p>\n<ul>\n<li>eradicate the disease; and</li>\n<li>re-establish Canada's disease-free status as quickly as possible.</li>\n</ul>\n<p>In an effort to eradicate <abbr title=\"Swine vesicular disease\">SVD</abbr>, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would use its \"stamping out\" policy, which includes:</p>\n<ul>\n<li>the humane destruction of all infected and exposed animals;</li>\n<li>surveillance and tracing of potentially infected or exposed animals;</li>\n<li>strict quarantine and animal movement controls to prevent spread;</li>\n<li>strict decontamination of infected premises; and</li>\n<li>zoning to define infected and disease-free areas.</li>\n</ul>\n<p>Owners whose animals are ordered destroyed may be eligible for compensation.</p>\n\r\n<h2 class=\"font-medium black\">Additional information</h2>\n<ul>\n<li><a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a></li>\n</ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que la maladie v\u00e9siculeuse du porc?</h2>\n<p>La maladie v\u00e9siculeuse du porc est une virose contagieuse du porc caract\u00e9ris\u00e9e par de la fi\u00e8vre et la pr\u00e9sence de v\u00e9sicules (ampoules remplies de liquide) dans la bouche, ainsi que sur le museau, les pattes et les mamelles.</p>\n<p>Il est impossible de distinguer \u00e0 l'\u0153il nu ces l\u00e9sions v\u00e9siculeuses de celles caus\u00e9es par la fi\u00e8vre aphteuse. La maladie peut \u00eatre b\u00e9nigne ou grave, mais rarement mortelle.</p>\n<h2 class=\"h5\">La maladie v\u00e9siculeuse du porc pr\u00e9sente-t-elle un risque pour la sant\u00e9 humaine?</h2>\n<p>Non. Cette maladie n'affecte pas les humains. Toutefois, on a signal\u00e9 de rares cas d'infection accidentelle chez du personnel de laboratoire travaillant avec le virus.</p>\n<h2 class=\"h5\">Quels sont les signes cliniques de la maladie v\u00e9siculeuse du porc?</h2>\n<p>Les signes visibles de cette maladie peuvent facilement \u00eatre confondus avec ceux de la fi\u00e8vre aphteuse et comprennent les suivants\u00a0:</p>\n<ul>\n<li>fi\u00e8vre et perte d'app\u00e9tit;</li>\n<li>apparition soudaine de boiterie chez plusieurs animaux en contact \u00e9troit;</li>\n<li>claudication, inconfort visible ou refus de se d\u00e9placer sur des surfaces dures;</li>\n<li>v\u00e9sicules (ampoules remplies de liquide) sur le museau, les pattes, la gueule, la langue et les mamelles;</li>\n<li>ruptures des v\u00e9sicules laissant des l\u00e9sions \u00e9rosives;</li>\n<li>fourchettes l\u00e2ches ou perte de sabots.</li>\n</ul>\n<p>Les jeunes porcs sont touch\u00e9s plus durement. Le r\u00e9tablissement survient habituellement au bout d'une \u00e0 trois semaines, et le taux de mortalit\u00e9 est tr\u00e8s faible, voire nul.</p>\n<h2 class=\"h5\">Dans quelles r\u00e9gions la maladie v\u00e9siculeuse du porc s\u00e9vit-elle?</h2>\n<p>La maladie v\u00e9siculeuse du porc a \u00e9t\u00e9 d\u00e9crite pour la premi\u00e8re fois en Italie en 1966, et elle demeure pr\u00e9sente dans des endroits limit\u00e9s, causant \u00e0 l'occasion des cas cliniques.</p>\n<p>La maladie v\u00e9siculeuse du porc n'a jamais \u00e9t\u00e9 observ\u00e9e au Canada.</p>\n<h2 class=\"h5\">Comment la maladie v\u00e9siculeuse du porc est-elle transmise et comment se propage-t-elle?</h2>\n<p>La maladie se propage principalement par contact avec des porcs infect\u00e9s, ou dans un environnement contamin\u00e9, car le virus est extr\u00eamement r\u00e9sistant dans l'environnement. Le virus peut \u00e9galement survivre dans le porc et les produits du porc transform\u00e9s pendant de longues p\u00e9riodes. La distribution d'aliments destin\u00e9s aux rebuts contamin\u00e9s aux animaux peut donc entra\u00eener la transmission du virus.</p>\n<h2 class=\"h5\">Comment la maladie v\u00e9siculeuse du porc est-elle diagnostiqu\u00e9e?</h2>\n<p>Il est n\u00e9cessaire de proc\u00e9der \u00e0 des analyses en laboratoire pour confirmer un diagnostic et \u00e9carter la possibilit\u00e9 d'autres maladies v\u00e9siculeuses comme la fi\u00e8vre aphteuse.</p>\n<h2 class=\"h5\">Quel est le traitement utilis\u00e9 contre la maladie v\u00e9siculeuse du porc?</h2>\n<p>Il n'existe ni traitement, ni vaccin contre la maladie v\u00e9siculeuse du porc.</p>\n<h2 class=\"h5\">Que fait-on pour prot\u00e9ger le b\u00e9tail canadien contre une \u00e9closion de maladie v\u00e9siculeuse du porc?</h2>\n<p>L'Agence canadienne d'inspection des aliments (ACIA) impose des exigences r\u00e9glementaires rigoureuses qui s'appliquent aux animaux et aux produits animaux import\u00e9s de pays o\u00f9 la maladie v\u00e9siculeuse du porc a \u00e9t\u00e9 signal\u00e9e. C'est l'Agence des services frontaliers du Canada ou l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> qui se charge de faire respecter ces exigences gr\u00e2ce aux inspections effectu\u00e9es aux points d'entr\u00e9e.</p>\n<p>La maladie v\u00e9siculeuse du porc est une \u00ab maladie \u00e0 d\u00e9claration obligatoire \u00bb en vertu de la <i>Loi sur la sant\u00e9 des animaux</i>. Cela signifie que tous les cas suspects doivent \u00eatre d\u00e9clar\u00e9s \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et font l'objet d'une enqu\u00eate imm\u00e9diate par des inspecteurs.</p>\n<h2 class=\"h5\">Quelles mesures prendrait l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en cas d'\u00e9closion de maladie v\u00e9siculeuse du porc au Canada?</h2>\n<p>La strat\u00e9gie d'intervention d'urgence du Canada en cas d'\u00e9closion de maladie v\u00e9siculeuse du porc vise \u00e0\u00a0:</p>\n<ul>\n<li>\u00e9radiquer la maladie;</li>\n<li>r\u00e9tablir le plus rapidement possible le statut sanitaire du Canada comme pays exempt de maladie v\u00e9siculeuse du porc.</li>\n</ul>\n<p>Pour tenter d'\u00e9radiquer la maladie v\u00e9siculeuse du porc, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> appliquerait sa politique d'abattage sanitaire qui comprend ce qui suit\u00a0:</p>\n<ul>\n<li>la destruction sans cruaut\u00e9 de tous les animaux infect\u00e9s et expos\u00e9s au virus;</li>\n<li>la surveillance et le retra\u00e7age de tous les animaux potentiellement infect\u00e9s ou expos\u00e9s au virus;</li>\n<li>des mises en quarantaine et des mesures de contr\u00f4le des d\u00e9placements des animaux rigoureuses pour emp\u00eacher la propagation du virus;</li>\n<li>la d\u00e9contamination compl\u00e8te des lieux contamin\u00e9s;</li>\n<li>le zonage pour circonscrire les r\u00e9gions contamin\u00e9es et celles exemptes de la maladie.</li>\n</ul>\n<p>Les propri\u00e9taires d'animaux dont on a ordonn\u00e9 la destruction <a href=\"/sante-des-animaux/animaux-terrestres/maladies/indemnisation/fra/1313712524829/1313712773700\">pourraient \u00eatre indemnis\u00e9s</a>.</p>\n\r\n<h2 class=\"font-medium black\">Renseignements additionnels</h2>\n\n<ul>\n<li><a href=\"/fra/1300462382369/1300462438912\">Bureaux de sant\u00e9 animale</a></li>\n</ul> \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}