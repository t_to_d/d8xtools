{
    "dcr_id": "1303439340158",
    "title": {
        "en": "Nipah Virus Disease - Fact Sheet",
        "fr": "Fiche de renseignements - Virus Nipah"
    },
    "html_modified": "2011-04-21 22:29",
    "modified": "28-3-2014",
    "issued": "21-4-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_nipah_fact_1303439340158_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_nipah_fact_1303439340158_fra"
    },
    "parent_ia_id": "1306092780481",
    "ia_id": "1306100144027",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Nipah Virus Disease - Fact Sheet",
        "fr": "Fiche de renseignements - Virus Nipah"
    },
    "label": {
        "en": "Nipah Virus Disease - Fact Sheet",
        "fr": "Fiche de renseignements - Virus Nipah"
    },
    "templatetype": "content page 1 column",
    "node_id": "1306100144027",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1303433426078",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/immediately-notifiable/nipah-virus/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/virus-nipah/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Nipah Virus Disease - Fact Sheet",
            "fr": "Fiche de renseignements - Virus Nipah"
        },
        "description": {
            "en": "Nipah virus can infect pigs and other animals and can be transmitted from pigs to humans. It is considered a paramyxovirus - a group of viruses that induce a wide range of distinct illnesses.",
            "fr": "Le virus Nipah peut infecter les porcs et d'autres animaux et peut \u00eatre transmis des porcs aux humains. Il fait partie des paramyxovirus, un groupe de virus qui causent un grand nombre de maladies distinctes."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, immediately notifiable disease, Nipah virus, humans, pigs, fruit bats, fever, cough, respiratory distress, trembling, muscle spasms, weakness,lack of coordination",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, maladie \u00e0 notification imm\u00e9diate, virus Nipah, humains, porcs, roussettes, fi\u00e8vre, toux forte et aboyante;, d\u00e9tresse respiratoire, tremblements, spasmes musculaire, faiblesse, manque de coordination"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-04-21 22:29:02",
            "fr": "2011-04-21 22:29:02"
        },
        "modified": {
            "en": "2014-03-28",
            "fr": "2014-03-28"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Nipah Virus Disease - Fact Sheet",
        "fr": "Fiche de renseignements - Virus Nipah"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is Nipah virus disease?</h2>\n<p>Nipah virus is a disease that can affect both animals and humans.</p>\n<p>The virus was first identified in swine, and people who worked with them. It has subsequently been identified in dogs, cats, horses, and goats.</p>\n<p>The fruit bat, also know as the \"flying fox\" is the natural host for the virus. The bat also hosts the Hendra virus.</p>\n<h2 class=\"h5\">Is Nipah virus disease a risk to human health?</h2>\n<p>The risk to Canadians is considered to be low as there are no species of fruit bats in Canada. However, people working with swine in Southeast Asia should be aware of the risk.</p>\n<p>In humans, the symptoms of Nipah virus resemble influenza and can include:</p>\n<ul>\n<li>fever;</li>\n<li>severe headaches;</li>\n<li>muscle pain;</li>\n<li>dizziness; and</li>\n<li>vomiting.</li>\n</ul>\n<p>More serious symptoms affecting the central nervous system include coma, seizures and the inability to breathe. The disease may progress to encephalitis (inflammation of the brain) or meningitis, with a 40 per cent fatality rate.</p>\n<h2 class=\"h5\">What are the clinical signs of Nipah virus disease in swine?</h2>\n<p>Nipah virus affects both the nervous system and the respiratory system. The clinical signs in swine include:</p>\n<ul>\n<li>fever;</li>\n<li>a loud, barking cough;</li>\n<li>respiratory distress (open mouthed breathing, rapid and laboured respiration);</li>\n<li>trembling;</li>\n<li>muscle spasms;</li>\n<li>weakness in the hind limbs; and</li>\n<li>a lack of coordination.</li>\n</ul>\n<p>In sows and boars particularly, the following signs can be observed:</p>\n<ul>\n<li>agitation;</li>\n<li>head pressing;</li>\n<li>increased salivation and nasal discharge;</li>\n<li>seizures; and</li>\n<li>sudden death.</li>\n</ul>\n<p>Abortions have been reported in affected sows.</p>\n<p>The illness can affect 100 per cent of the herd, but mortality is generally less than five per cent, except in piglets, where it is higher.</p>\n<h2 class=\"h5\">Where is Nipah virus found?</h2>\n<p>It was first described in Malaysia in 1999, resulting in illness in swine and a number of associated human fatalities in people who worked closely with the sick pigs. Since 1999, there have been additional human outbreaks in Singapore, Bangladesh and India; the Singapore outbreak is the only one linked to swine exposure.</p>\n<p>The virus is likely endemic in Southeast Asia, which is the natural habitat of the fruit bat.</p>\n<p>The Nipah virus has never been found in Canada.</p>\n<h2 class=\"h5\">How is Nipah virus transmitted and spread?</h2>\n<p>There is uncertainty about how the virus is spread in humans and pigs.</p>\n<p>The illness and mortality in humans was initially associated with exposure to the excretions and secretions of infected pigs; however more recent outbreaks have occurred without animal illness.</p>\n<h2 class=\"h5\">How is Nipah virus disease diagnosed?</h2>\n<p>Testing of lung and brain samples confirms diagnosis in pigs.</p>\n<p>Because Nipah virus is a zoonotic disease, samples must be collected, handled and transported with appropriate biosecurity precautions.</p>\n<h2 class=\"h5\">How is Nipah virus disease treated?</h2>\n<p>There is no treatment or preventative vaccine currently available.</p>\n<h2 class=\"h5\">What is done to protect Canadian livestock from the Nipah virus disease?</h2>\n<p>The Canadian Food Inspection Agency (CFIA) imposes strict regulations on the import of animals and animal products from countries where Nipah virus is known to occur. These regulations are enforced through port-of-entry inspections done either by the Canada Border Services Agency or the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p>Nipah virus is a \"reportable disease\" under the Health of Animals Act. This means that all suspected cases must be reported to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for immediate investigation by inspectors.</p>\n<h2 class=\"h5\">How would the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> respond to an outbreak of Nipah virus disease?</h2>\n<p>Canada's emergency response strategy in the event of an outbreak of Nipah virus would be to:</p>\n<ul>\n<li>eradicate the disease; and</li>\n<li>re-establish Canada's disease-free status as quickly as possible.</li>\n</ul>\n<p>In an effort to eradicate the Nipah virus, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would use its \"stamping out\" policy, which includes:</p>\n<ul>\n<li>the humane destruction of all infected and exposed animals;</li>\n<li>surveillance and tracing of potentially infected or exposed animals;</li>\n<li>strict quarantine and animal movement controls to prevent spread;</li>\n<li>strict decontamination of infected premises; and</li>\n<li>zoning to define infected and disease-free areas.</li>\n</ul>\n<p>Owners whose animals are ordered destroyed may be <a href=\"/animal-health/terrestrial-animals/diseases/compensation/eng/1313712524829/1313712773700\">eligible for compensation</a>.</p>\n<p>Canada's National Center for Foreign Animal Disease in Winnipeg is part of an international collaborative effort to study Nipah virus and is working on the development of a rapid diagnostic capability.</p>\n\r\n<h2 class=\"font-medium black\">Additional information</h2>\n<ul>\n<li><a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a></li>\n</ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que le virus Nipah?</h2>\n<p>Le virus Nipah est une maladie qui peut s'attaquer aux animaux et aux humains.</p>\n<p>La pr\u00e9sence du virus a d'abord \u00e9t\u00e9 d\u00e9cel\u00e9e chez les porcs, puis chez les humains qui travaillaient avec eux. Le virus a ensuite \u00e9t\u00e9 d\u00e9couvert chez les chiens, les chats, les chevaux et les ch\u00e8vres.</p>\n<p>La roussette est l'h\u00f4te naturel du virus. La chauve-souris est \u00e9galement l'h\u00f4te du virus Hendra.</p>\n<h2 class=\"h5\">Le virus Nipah pr\u00e9sente-t-il un risque pour la sant\u00e9 humaine?</h2>\n<p>Le risque pour les Canadiens est jug\u00e9 faible, car aucune esp\u00e8ce de roussette n'est pr\u00e9sente au Canada. Cependant, les personnes travaillant avec des porcs en Asie du Sud-Est devraient \u00eatre conscientes du risque qu'elles courent.</p>\n<p>Chez les humains, les sympt\u00f4mes du virus Nipah ressemblent \u00e0 ceux de l'influenza et peuvent comprendre\u00a0:</p>\n<ul>\n<li>de la fi\u00e8vre;</li>\n<li>des maux de t\u00eate intenses;</li>\n<li>des douleurs musculaires;</li>\n<li>des \u00e9tourdissements;</li>\n<li>des vomissements.</li>\n</ul>\n<p>Parmi les sympt\u00f4mes plus graves affectant le syst\u00e8me nerveux central, mentionnons le coma, les convulsions et l'incapacit\u00e9 de respirer. La maladie peut d\u00e9g\u00e9n\u00e9rer en enc\u00e9phalite (inflammation du cerveau) ou en m\u00e9ningite. Le taux de mortalit\u00e9 est d'environ 40 pour cent.</p>\n<h2 class=\"h5\">Quels sont les signes cliniques du virus Nipah chez les porcs?</h2>\n<p>Le virus Nipah affecte le syst\u00e8me nerveux central et le syst\u00e8me respiratoire. Chez les porcs, les signes cliniques comprennent les suivants\u00a0:</p>\n<ul>\n<li>fi\u00e8vre;</li>\n<li>toux forte et aboyante;</li>\n<li>d\u00e9tresse respiratoire (respiration haletante, rapide et laborieuse);</li>\n<li>tremblements;</li>\n<li>spasmes musculaires;</li>\n<li>faiblesse des pattes arri\u00e8re;</li>\n<li>manque de coordination.</li>\n</ul>\n<p>Chez les truies et les verrats, on peut observer les signes suivants\u00a0:</p>\n<ul>\n<li>agitation;</li>\n<li>mouvement d'appui de la t\u00eate;</li>\n<li>salivation et \u00e9coulement nasal accrus;</li>\n<li>convulsions;</li>\n<li>mort subite.</li>\n</ul>\n<p>Des avortements ont \u00e9t\u00e9 signal\u00e9s chez des truies infect\u00e9es.</p>\n<p>La maladie peut infecter la totalit\u00e9 du troupeau, mais, en r\u00e8gle g\u00e9n\u00e9rale, le taux de mortalit\u00e9 est inf\u00e9rieur \u00e0 cinq pour cent, sauf chez les porcelets, pour lesquels le taux de mortalit\u00e9 est plus \u00e9lev\u00e9.</p>\n<h2 class=\"h5\">Dans quelles r\u00e9gions le virus Nipah s\u00e9vit-il?</h2>\n<p>Le virus Nipah a d'abord \u00e9t\u00e9 d\u00e9crit en 1999 en Malaisie, o\u00f9 il a rendu malade les porcs et fait un certain nombre de victimes humaines parmi des personnes qui travaillaient en contact \u00e9troit avec des porcs malades. Depuis, d'autres \u00e9closions sont survenues chez les humains \u00e0 Singapour, au Bangladesh et en Inde. Seule l'\u00e9closion de Singapour est li\u00e9e \u00e0 l'exposition aux porcs.</p>\n<p>Le virus est probablement end\u00e9mique en Asie du Sud-Est, r\u00e9gion servant d'habitat naturel \u00e0 la roussette.</p>\n<p>Le virus Nipah n'a jamais \u00e9t\u00e9 observ\u00e9 au Canada.</p>\n<h2 class=\"h5\">Comment le virus Nipah est-il transmis et comment se propage-t-il?</h2>\n<p>La fa\u00e7on dont le virus s'est propag\u00e9 chez les humains et les porcs soul\u00e8ve de l'incertitude.</p>\n<p>La maladie et la mortalit\u00e9 chez les humains ont d'abord \u00e9t\u00e9 associ\u00e9es \u00e0 l'exposition aux excr\u00e9tions et aux s\u00e9cr\u00e9tions de porcs infect\u00e9s. Cependant, des \u00e9closions sont survenues plus r\u00e9cemment en l'absence de maladie animale.</p>\n<h2>Comment le virus Nipah est-il diagnostiqu\u00e9?</h2>\n<p>L'analyse d'\u00e9chantillons de poumon et de cervelle permet de confirmer le diagnostic chez les porcs.</p>\n<p>Puisque le virus Nipah est une zoonose, il faut prendre les mesures de bios\u00e9curit\u00e9 appropri\u00e9es lors du pr\u00e9l\u00e8vement, de la manipulation et du transport des \u00e9chantillons.</p>\n<h2 class=\"h5\">Quel est le traitement utilis\u00e9 contre le virus Nipah?</h2>\n<p>Il n'existe ni traitement, ni vaccin pr\u00e9ventif contre le virus Nipah.</p>\n<h2 class=\"h5\">Que fait-on pour prot\u00e9ger le b\u00e9tail canadien contre le virus Nipah?</h2>\n<p>L'Agence canadienne d'inspection des aliments (ACIA) impose des exigences r\u00e9glementaires rigoureuses qui s'appliquent aux animaux et aux produits animaux import\u00e9s de pays o\u00f9 le virus Nipah a \u00e9t\u00e9 signal\u00e9. C'est l'Agence des services frontaliers du Canada ou l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> qui se charge de faire respecter ces exigences gr\u00e2ce aux inspections effectu\u00e9es aux points d'entr\u00e9e.</p>\n<p>Le virus Nipah est une \u00ab maladie \u00e0 d\u00e9claration obligatoire \u00bb en vertu de la Loi sur la sant\u00e9 des animaux. Cela signifie que tous les cas suspects doivent \u00eatre d\u00e9clar\u00e9s \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et font l'objet d'une enqu\u00eate imm\u00e9diate par des inspecteurs.</p>\n<h2 class=\"h5\">Quelles mesures prendrait l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en cas d'\u00e9closion du virus Nipah?</h2>\n<p>La strat\u00e9gie d'intervention d'urgence du Canada en cas d'\u00e9closion du virus Nipah vise \u00e0\u00a0:</p>\n<ul>\n<li>\u00e9radiquer la maladie;</li>\n<li>r\u00e9tablir le plus rapidement possible le statut sanitaire du Canada comme pays exempt du virus Nipah.</li>\n</ul>\n<p>Pour tenter d'\u00e9radiquer le virus Nipah, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> appliquerait sa politique d'abattage sanitaire qui comprend ce qui suit\u00a0:</p>\n<ul>\n<li>la destruction sans cruaut\u00e9 de tous les animaux infect\u00e9s et expos\u00e9s au virus;</li>\n<li>la surveillance et le retra\u00e7age de tous les animaux potentiellement infect\u00e9s ou expos\u00e9s au virus;</li>\n<li>des mises en quarantaine et des mesures de contr\u00f4le des d\u00e9placements des animaux rigoureuses pour emp\u00eacher la propagation du virus;</li>\n<li>la d\u00e9contamination compl\u00e8te des lieux contamin\u00e9s;</li>\n<li>le zonage pour circonscrire les r\u00e9gions contamin\u00e9es et celles exemptes de la maladie.</li>\n</ul>\n<p>Les propri\u00e9taires d'animaux dont on a ordonn\u00e9 la destruction <a href=\"/sante-des-animaux/animaux-terrestres/maladies/indemnisation/fra/1313712524829/1313712773700\">pourraient \u00eatre indemnis\u00e9s</a>.</p>\n<p>Le Centre national des maladies animales exotiques du Canada \u00e0 Winnipeg fait partie d'un effort international conjoint pour l'\u00e9tude du virus Nipah. Le Centre travaille \u00e0 l'\u00e9laboration d'une m\u00e9thode de diagnostic rapide.</p>\n\r\n<h2 class=\"font-medium black\">Renseignements additionnels</h2>\n\n<ul>\n<li><a href=\"/fra/1300462382369/1300462438912\">Bureaux de sant\u00e9 animale</a></li>\n</ul> \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}