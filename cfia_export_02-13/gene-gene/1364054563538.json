{
    "dcr_id": "1364054563538",
    "title": {
        "en": "Document Procedures for  the National Import Service Centre (NISC)",
        "fr": "Proc\u00e9dures relatives aux documents pour le Centre de service national \u00e0 l'importation (CSNI) "
    },
    "html_modified": "2024-02-13 9:48:46 AM",
    "modified": "2014-11-20",
    "issued": "2013-03-23 12:02:46",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_imports_commercial_documents_1364054563538_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_imports_commercial_documents_1364054563538_fra"
    },
    "ia_id": "1364054606558",
    "parent_ia_id": "1364059265637",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1364059265637",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Document Procedures for  the National Import Service Centre (NISC)",
        "fr": "Proc\u00e9dures relatives aux documents pour le Centre de service national \u00e0 l'importation (CSNI) "
    },
    "label": {
        "en": "Document Procedures for  the National Import Service Centre (NISC)",
        "fr": "Proc\u00e9dures relatives aux documents pour le Centre de service national \u00e0 l'importation (CSNI) "
    },
    "templatetype": "content page 1 column",
    "node_id": "1364054606558",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1364059150360",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/nisc/document-procedures/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/csni/procedures-relatives-aux-documents/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Document Procedures for  the National Import Service Centre (NISC)",
            "fr": "Proc\u00e9dures relatives aux documents pour le Centre de service national \u00e0 l'importation (CSNI)"
        },
        "description": {
            "en": "hood, food health",
            "fr": "aliments, la sant\u00e9 des aliments"
        },
        "keywords": {
            "en": "food, food health",
            "fr": "aliments, la sant\u00e9 des aliments"
        },
        "dcterms.subject": {
            "en": "inspection",
            "fr": "inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-03-23 12:02:46",
            "fr": "2013-03-23 12:02:46"
        },
        "modified": {
            "en": "2014-11-20",
            "fr": "2014-11-20"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Document Procedures for the National Import Service Centre (NISC)",
        "fr": "Proc\u00e9dures relatives aux documents pour le Centre de service national \u00e0 l'importation (CSNI)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) is reminding brokers/importers to adhere to the following guidelines when submitting documentation to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s National Import Service Centre (NISC).</p>\n<p>Import-related documents should be faxed <strong>only once</strong> to the <abbr title=\"National Import Service Centre\">NISC</abbr>, along with the transmission of your Electronic Data Interchange (EDI) submission. Please refrain from re-faxing documents unless directed to do so by the <abbr title=\"National Import Service Centre\">NISC</abbr>. All faxes are now to be sent to the new centralized fax number at 1-613-773-9999.  Refer to <a href=\"/importing-food-plants-or-animals/food-imports/nisc/submit-a-paper-declaration-to-the-nisc/eng/1416522119765/1416522354282\">Procedures for Faxing documents</a> for more information.</p>\n<p>When using the <a href=\"/eng/1328823628115/1328823702784#c5272\">Request for Documentation Review (CFIA/ACIA\u00a05272)</a>:</p>\n<ul>\n<li>Use this form as the first document of a fax submission. Do not use your company fax cover page.</li>\n<li>Use the current version of the form.</li>\n<li>Ensure all information on the form is typed.</li>\n<li>Do not provide a bar code and/or a label transaction code on the form.</li>\n<li>Be sure to include the transaction number on the form.</li>\n<li>Do not affix any labels or stickers to the form.</li>\n<li>Should a single fax contain multiple requests, please ensure form 5272 separates each request.</li>\n</ul>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is committed to maintaining established service standards with minimal disruption to clients. In order to ensure operations continue as smoothly as possible the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is asking that all importers follow these guidelines.</p>\n<p>The National Import Service Centre processes import request documentation / data sent by the importing community across Canada and provides frontline screening to ensure that imported products meet Canadian requirements. Regulatory compliance and the safety of food and agricultural commodities imported into Canada are a priority for the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p>National Import Service Centre<br> 7:00 <abbr lang=\"la\" title=\"Ante Meridiem\">a.m.</abbr> to 03:00 <abbr lang=\"la\" title=\"Ante Meridiem\">a.m.</abbr> (Eastern Time)<br> Telephone and <abbr title=\"Electronic Data Interchange\">EDI</abbr>: 1-800-835-4486 (Canada or <abbr title=\"United States of America\">U.S.A.</abbr>)<br> 1-289-247-4099 (local calls and all other countries)<br> Facsimile: 1-613-773-9999</p>\n<p class=\"text-right small\">Originally issued March 16, 2012 (Notice to Industry)</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) rappelle aux courtiers et aux importateurs de respecter les lignes directrices qui suivent lorsqu'ils transmettent des documents au Centre de service national \u00e0 l'importation (CSNI) de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Les documents relatifs \u00e0 l'importation doivent \u00eatre t\u00e9l\u00e9copi\u00e9s <strong>une seule fois</strong> au <abbr title=\"Centre de service national \u00e0 l'importation\">CSNI</abbr> et au m\u00eame moment que l'envoi de leur demande au moyen de l'\u00c9change de donn\u00e9es informatis\u00e9es (EDI). Veuillez vous abstenir de t\u00e9l\u00e9copier plus d'une fois des documents, \u00e0 moins d'avis contraire par le <abbr title=\"Centre de service national \u00e0 l'importation\">CSNI</abbr>. Toutes les t\u00e9l\u00e9copies doivent dor\u00e9navant \u00eatre envoy\u00e9es au nouveau num\u00e9ro de t\u00e9l\u00e9copieur centralis\u00e9 (1\u2011613\u2011773\u20119999).  R\u00e9f\u00e9rez-vous \u00e0 <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/csni/soumettre-une-declaration-papier-au-csni/fra/1416522119765/1416522354282\">Marche \u00e0 suivre pour t\u00e9l\u00e9copier des documents au Centre de service national \u00e0 l'importation</a> pour plus d'information.</p>\n<p>Lorsque vous utilisez le formulaire <a href=\"/fra/1328823628115/1328823702784#c5272\">Demande de r\u00e9vision de documents (CFIA/ACIA\u00a05272)</a>\u00a0:</p>\n<ul>\n<li>t\u00e9l\u00e9copiez le formulaire en premier. N'utilisez pas la page couverture des envois par t\u00e9l\u00e9copieur de votre entreprise;</li>\n<li>utilisez la nouvelle version du formulaire;</li>\n<li>assurez\u2011vous que tous les renseignements figurant sur le formulaire sont dactylographi\u00e9s;</li>\n<li>ne fournissez ni codes \u00e0 barres ni code de transaction d'une \u00e9tiquette sur le formulaire;</li>\n<li>assurez\u2011vous d'inclure le num\u00e9ro de transaction sur le formulaire;</li>\n<li>n'apposez ni \u00e9tiquette ni autocollant sur le formulaire;</li>\n<li>si un envoi par t\u00e9l\u00e9copieur contient plus d'une demande, assurez\u2011vous qu'un formulaire\u00a05272 s\u00e9pare chacune d'elle.</li>\n</ul>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> est d\u00e9termin\u00e9e \u00e0 maintenir les normes de service \u00e9tablies tout en minimisant les perturbations au service \u00e0 la client\u00e8le. C'est pourquoi, afin que les activit\u00e9s se poursuivent le plus harmonieusement possible, l'Agence demande \u00e0 tous les importateurs de respecter ces lignes directrices.</p>\n<p>Le <abbr title=\"Centre de service national \u00e0 l'importation\">CSNI</abbr> est charg\u00e9 de traiter les documents et les donn\u00e9es qui accompagnent une demande d'importation envoy\u00e9e par un importateur au Canada et d'effectuer un tri de premi\u00e8re ligne pour v\u00e9rifier la conformit\u00e9 des produits import\u00e9s avec les exigences canadiennes. La conformit\u00e9 \u00e0 la r\u00e9glementation et la salubrit\u00e9 des aliments et produits agricoles import\u00e9s au Canada sont des priorit\u00e9s de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Centre de service national \u00e0 l'importation<br> De\u00a07\u00a0<abbr title=\"heures\">h</abbr>\u00a0\u00e0\u00a03\u00a0<abbr title=\"heures\">h</abbr> (heure de l'Est)<br> T\u00e9l\u00e9phone et <abbr title=\"\u00c9change de donn\u00e9es informatis\u00e9es\">EDI</abbr>\u00a0: 1\u2011800\u2011835\u20114486 (Canada ou \u00c9tats\u2011Unis)<br> 1\u2011289\u2011247\u20114099 (appels locaux et autres pays)<br> T\u00e9l\u00e9copieur\u00a0:1\u2011613\u2011773\u20119999</p>\n<p class=\"text-right small\">Premi\u00e8re publication 16 mars, 2012 (Avis \u00e0 l'industrie)</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}