Several scripts for various levels/steps of export/import processing.

This is an example of a different interwoven project.


Initial export from teamsite starts with a table of values exported via sql to an excel spreadsheet
PREVIOUS SQL FROM AAFC
```
    select
        "METADATA_DCR_ID",
        "METADATA_DCR_LANG_CODE",
        "DC_TITLE_NAME",
/*       "DC_DESC",
        "AAFC_KEYWORD_NAME", */
        "DCTERMS_MODIFIED_DATE",
        "DCTERMS_ISSUED_DATE",
--        "DCR_NAME",
        "DCR_TYPE_NAME",
        "NODE_ID",
        "DCR_LAYOUT_NAME",
        "CREATED_DATE",
        "LAST_UPDATE_DATE" 
    from
        "AMAP"."AMAP_METADATA_DCR"
    where
                METADATA_DCR_LANG_CODE='eng' OR METADATA_DCR_LANG_CODE='fra'
```
**NEW SQL FOR CFIA**

```
SELECT DISTINCT md_item.wcmsid, md_item.lang, title.md_value as "Title", dateissued.md_value as "Date Issued", datemodified.md_value as "Date Modified", datecreated.md_value as "Date Created", filepath.md_value as "File Path", iaparent.md_value as "IA Parent ID", iapath.md_value as "IA ID" FROM md_item 
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 11
        and lang = 'eng')
        title ON (md_item.wcmsid = title.wcmsid)
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 1
        and lang = 'eng')
        dateissued ON (md_item.wcmsid = dateissued.wcmsid)
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 3
        and lang = 'eng')
        datemodified ON (md_item.wcmsid = datemodified.wcmsid)   
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 58
        and lang = 'eng')
        datecreated ON (md_item.wcmsid = datecreated.wcmsid)  
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 19
        and lang = 'eng')
        filepath ON (md_item.wcmsid = filepath.wcmsid)
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 34
        and lang = 'ia')
        iaparent ON (md_item.wcmsid = iaparent.wcmsid)
LEFT JOIN (select wcmsid, md_value from md_item
        where type_id = 36
        and lang = 'ia')
        iapath ON (md_item.wcmsid = iapath.wcmsid)
WHERE filepath.md_value NOT LIKE '%DAM%'
AND filepath.md_value NOT LIKE '%navi-navi%'
AND filepath.md_value NOT LIKE '%recall-rappel%'
AND lang = 'eng'
ORDER BY datemodified.md_value ASC;
```

SKIP THIS SQL PART (above), handled by Kevin Lambert, and processed by Joseph Olstad.  Duplicate rows are removed by Joseph Olstad using google sheets.
I take the english with ia sheet , sort all rows by column A, very first thing, then I use google sheets to remove duplicate rows based on the column A value.  I repeat for french with ia sheet.   Then I replace column B 'eng' in french with ia and paste 'fra' into all rows.  Then I create a new sheet, paste in all the english with ia rows, then right after paste all the french with i a rows, then I sort again by column A in descending order , always descending order..  Then I save the document and open it with libre office and I generate a csv from the new sheet using tab separated , there's a specific setting I use for the csv export:

Jeu de caractères : Unicode (UTF-8)
Séparateur de Champ {Tabulation}
Separateur de chaine de caractères: "
Checked options:
Enregistrer le contenu de la cellule comme affiché
Mettre entre guillemets toutes les cellules de texte

 and then the results are combined and exported to tab delimited csv (prior to combining english/french must perform duplicate row processing based on the first column for both with_ia sheets (google sheets for this) then combine).  The expected output has been processed, the header must be as shown in this file as stored in this project under: **z_cfia_raw/export_with_ia_en_and_fr.csv**

From Here, export the excel to csv tab seperated with " identifiers the header row is adjusted removing most of the prefixes (as follows):

#BEFORE (AAFC)
"dcr_id"        "lang"  "title" "modified"      "issued"        "type_name"     "node_id"       "layout_name" "created"        "updated"

#NEW for CFIA:
"dcr_id"	"langcode"	"title"	"html_modified"	"modified"	"issued"	"teamsite_location"	"parent_ia_id"	"ia_id"

#EXPORT SCRIPTS WERE WRITTEN TO WORK WITH PHP 7.4 but recently upgraded to work with PHP 8.0, to export you must switch to PHP 8.0, example as follows:

```
 ╭─◀ ☕ j envy-4700u ▶ ~/zCFIA/d8xtools ▶ 📂26  📃10 🔗2 ▶ 🔀 master ▶
 ╰❯ $ sudo update-alternatives --config php
[sudo] Mot de passe de j : 
Il existe 5 choix pour l'alternative php (qui fournit /usr/bin/php).

  Sélection   Chemin                Priorité  État
------------------------------------------------------------
  0            /usr/bin/php.default   100       mode automatique
  1            /usr/bin/php.default   100       mode manuel
  2            /usr/bin/php7.4        74        mode manuel
* 3            /usr/bin/php8.0        80        mode manuel
  4            /usr/bin/php8.1        81        mode manuel
  5            /usr/bin/php8.2        82        mode manuel

Appuyez sur <Entrée> pour conserver la valeur par défaut[*] ou choisissez le numéro sélectionné :3

 ╭─◀ ☕ j envy-4700u ▶ ~/zCFIA/d8xtools ▶ 📂26  📃10 🔗2 ▶ 🔀 master ▶
 ╰❯ $ php --version
PHP 8.0.30 (cli) (built: Sep  2 2023 08:05:13) ( NTS )
Copyright (c) The PHP Group
Zend Engine v4.0.30, Copyright (c) Zend Technologies
    with Zend OPcache v8.0.30, Copyright (c), by Zend Technologies
```

**- STEP 1)**

Then a script is run to convert these csv rows into json files:

```
cd /some/folder
git clone https://gitlab.com/agrcms/d8xtools.git
cd d8xtools
php csv_to_json_export.php z_cfia_raw/export_with_ia_en_and_fr.csv 2>&1 | tee step1_csv_processing.log
```

the above script writes to the cfia_export_current folder on december 11th , the month and day will change every day.

now that those are converted to json records, we run the updatejsons script on it as follows:


**- STEP 2)**

how to run the `update_json.php` script (can be run from outside the VPN network (for internet))

```
cd d8xtools;
# The first one is of type sitemap
php update_json.php /home/j/zCFIA/d8xtools/cfia_export_current/sitemapinit sitemap 2>&1 | tee step2a_export_sitemapinit.log
# The next ones will be of type teamsite
php update_json.php /home/j/zCFIA/d8xtools/cfia_export_current/pros-judi teamsite 2>&1 | tee step2b_export_pros-judi.log
php update_json.php /home/j/zCFIA/d8xtools/cfia_export_current/forms-formes teamsite 2>&1 | tee step2c_export_forms-formes.log
php update_json.php /home/j/zCFIA/d8xtools/cfia_export_current/gene-gene teamsite 2>&1 | tee step2d_export_gene-gene.log
php update_json.php /home/j/zCFIA/d8xtools/cfia_export_current/prod-judi teamsite 2>&1 | tee step2e_export_prod-judi.log
php update_json.php /home/j/zCFIA/d8xtools/cfia_export_current/manuals-manuels teamsite 2>&1 | tee step2e_export_manuals-manuels.log

#In the above commands please substitute 12-11 for the current month and current day.
teamsite content json entities are written into json files in the cfia_export_current subfolders
```

#Note: json cleanup step, add this to the update_json.php script:

`find . -type f -exec sed -i 's/\"-&gt;\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n/\"/g' {} +`

**- STEP 3 (work in progress, not ready yet)**
## ********  END OF UPDATED README.MD ******************************************
## ******** TODO, update the next part of the readme and scripts for CFIA ******
## ********       DEPRECATED DOCUMENTATION BELOW , please ignore ***************
Now for the sitemap

The source sitemap is in xml

I have created a script to convert it to json: 

```
cd internetx/xml2json
php teamsite_sitemap_xml2json.php > test99.json
```

IMPORTANT FIX corrige test99.json:

 a) vim search and replace wierd "0": "" pattern 

`:%s/,$\n\s*"0": ""//g`


and then from json to flatten it into json entities with this script:

`php internetx/process_sitemap.php`

sitemap json entities will be written into the `export` folder

`php update_json.php /some/folder/export sitemap`

sitemap json entities will be updated into the export folder with meta data and body content.


Troubleshooting: locate files with less than 800 bytes, have the update_json.php script scripts re-run the export (page reader)

```
mkdir run_json_update_again;
find cfia_export -type f -size -800c -exec echo cp\ {}\ run_json_update_again/. \;
```

#IMPORTING

```
cd docroot
drush scr ../d8xtools/drupal_export_import/import.php /var/www/clients/client3/web65/web/d8xtools/export_sitemap_02-10 sitemapinit
#make sure a folder or symlink called sitemap exists in /path/to/export that contains the json files, filename.json is dcrid.json ex: 123456789012.json
drush --root=/var/www/clients/client3/web65/web/docroot /path/to/export sitemap
#make sure a folder or symlink called page exists in /path/to/export that contains the json files, filename.json is dcrid.json ex: 123456789012.json
drush --root=/var/www/clients/client3/web65/web/docroot /path/to/export page
```

Drupal Bug:

When you run any import or update script, you receive the error:

```
**Drupal\Componet\Plugin\Exception\PlugninNOtFoundException: The "EntityHasField" plugin does not exist.
Valid plugin IDs for Drupal\Core\ValidationConstraintManger are: Callback...
In SqlContentEntiyStorage.php line 846:
The "EntityHasField" plugin does not exist. **
```

Workaround:

Go to `/en/admin/content`, add any fake node. After that, run the script agian.

 Image processing and link processing IMPORT STEPS:

```
cd /var/www/path/to/web/d8xtools/convertTeamsiteIdToDrupal/;
php exportContentURL.php /var/www/path/to/web/d8xtools/convertTeamsiteIdToDrupal/export;
php retrieveFileContentFromAgriSource.php export; mv exportcontent ../../docroot/html/sites/default/files/legacy
cd /var/www/path/to/web/docroot;

drush scr ../d8xtools/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL;
drush scr ../d8xtools/convertTeamsiteIdToDrupal/updateContentURL.php /d8/d8xtools/convertTeamsiteIdToDrupal/export/
```
