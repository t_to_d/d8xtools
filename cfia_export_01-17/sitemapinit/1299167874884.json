{
    "dcr_id": "1299167784070",
    "title": {
        "en": "Horticulture",
        "fr": "Horticulture"
    },
    "html_modified": "2011-03-03 10:59",
    "modified": "26-7-2023",
    "issued": "3-3-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/plan_hort_1299167784070_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/plan_hort_1299167784070_fra"
    },
    "parent_ia_id": "1299162708850",
    "ia_id": "1299167874884",
    "parent_node_id": "1299162708850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Horticulture",
        "fr": "Horticulture"
    },
    "label": {
        "en": "Horticulture",
        "fr": "Horticulture"
    },
    "templatetype": "content page 1 column",
    "node_id": "1299167874884",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299162629094",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/horticulture/",
        "fr": "/protection-des-vegetaux/horticulture/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Horticulture",
            "fr": "Horticulture"
        },
        "description": {
            "en": "The CFIA establishes and maintains policies and standards for the horticulture industry to prevent the introduction and spread of regulated pests into Canada.",
            "fr": "L'ACIA \u00e9tablit et maintient les directives et les normes destin\u00e9es \u00e0 l'industrie horticole afin de pr\u00e9venir l'introduction et la propagation d'organismes nuisibles r\u00e9glement\u00e9s justiciables de quarantaine au Canada."
        },
        "keywords": {
            "en": "Plant Health Act, Plant Protection Regulations, horticulture, imports, exports, pests",
            "fr": "Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, horticulture, importation, exportation, organismes nuisibles"
        },
        "dcterms.subject": {
            "en": "crops,vegetable crops,fruits,imports,insects,inspection,vegetables,plants",
            "fr": "cultures,cultures mara\u00eech\u00e8res,fruit,importation,insecte,inspection,l\u00e9gume,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-03-03 10:59:19",
            "fr": "2011-03-03 10:59:19"
        },
        "modified": {
            "en": "2023-07-26",
            "fr": "2023-07-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Horticulture",
        "fr": "Horticulture"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) establishes and maintains policies and standards for the horticulture industry to prevent the introduction and spread of regulated pests into Canada.</p>\n\n<h2 id=\"a1\">General information and resources on Horticulture pathways and commodities</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/eng/1312230684400/1312231127174\">Plant protection policy directives</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/pest-risk-management/eng/1304820847590/1304820997079\">Pest risk management documents</a></li>\n<li><a href=\"/plant-health/soil/eng/1542127489815/1542127490190\">Soil and soil-related matter</a></li>\n<li><a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">Automated Import Reference System (AIRS) - Canadian Food Inspection Agency (canada.ca)</a>\n<ul>\n<li>The Automated Import Reference System (AIRS) shows the import requirements for CFIA regulated commodities</li>\n</ul>\n</li>\n<li><a href=\"/plant-health/invasive-species/directives/glossary/eng/1304730588212/1304730789969\">Plant health glossary of terms</a></li>\n\n</ul>\n\n\n<h2 id=\"a2\">Import, export, and domestic information</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/plant-import/primer/eng/1324568450671/1324569734910\">Importing plants and plant products: what you need to know</a></li>\n<li><a href=\"/plant-health/horticulture/horticulture-plant-list/eng/1419017863407/1419017907742\">Horticulture plant list (HPL)</a>\n<ul>\n<li>The HPL was compiled by the Canadian Food Inspection Agency (CFIA) as a list of frequently requested plants which have common, basic phytosanitary import requirements</li>\n</ul>\n</li>\n<li><a href=\"/plant-health/horticulture/exports/eng/1300383870830/1300383922268\">Export requirements</a> for horticulture products</li>\n<li>Import requirements for horticulture products\n<ul>\n<li><a href=\"/plant-health/horticulture/how-we-evaluate/eng/1425496755404/1425496838700\">Requesting a Pest Risk Analysis</a></li>\n<li><a href=\"/plant-health/horticulture/netherlands-bulb-list/eng/1508256806587/1508256807508\">Netherland Bulb List</a></li>\n<li><a href=\"/plant-health/horticulture/treatment-schedules/eng/1501526269211/1501526560731\">Treatment schedules for horticulture commodities</a></li>\n<li><a href=\"/plant-health/horticulture/importing-and-horticulture/eng/1466643835510/1466643911736\">Trial importation periods for fresh fruits and vegetables</a></li>\n<li><a href=\"/plant-health/horticulture/systems-approach/eng/1431458956662/1431459266630\">Systems approach for production of plant products</a></li>\n</ul>\n</li>\n<li><a href=\"/plant-health/invasive-species/domestic-plant-protection-measures/eng/1629843699782/1629901542997\">Domestic plant protection measures</a>\n<ul>\n<li>Information on CFIA regulations and domestic movement restrictions within Canada</li>\n</ul>\n</li>\n<li>How to apply for a permit to import:\n<ul>\n<li><a href=\"/about-cfia/find-a-form/form-cfia-acia-5256/eng/1491930450091/1491930450578\">Plant health import application form</a></li>\n<li><a href=\"/about-cfia/my-cfia/eng/1482204298243/1482204318353\">MyCFIA</a></li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a3\">Pest information</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">Pests regulated by Canada</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-pest-surveillance/eng/1344466499681/1344466638872#p4\">Pest fact sheets</a>\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/eng/1307077188885/1307078272806\">Insects</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/eng/1322807235798/1322807340310\">Plant diseases</a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/eng/1331614724083/1331614823132\">Invasive plants</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/eng/1321563900456/1321564231938\">Nematodes and others</a></li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a4\">Communication</h2>\n\n<p>Notices issued to inform stakeholders of updates to program administration, available reference and guidance information, and changes to the Act or Regulations</p>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/pest-risk-management/rmd-21-02/notice-to-industry/eng/1653420323729/1653420324010\">Recommendation to prevent movement of 'Concorde', 'Royal Cloak' and 'Tara' Emerald Carousel barberry cultivars, into Alberta, Saskatchewan and Manitoba</a></li>\n<li><a href=\"/about-cfia/transparency/consultations-and-engagement/import-and-domestic-directives/eng/1682616619810/1682616743632\">Share your thoughts: Consultation on proposed import and domestic directives for propagative and fresh decorative fruit tree material </a></li>\n<li><a href=\"/plant-health/invasive-species/insects/box-tree-moth/2023-07-12/eng/1689093614563/1689094186895\">Notice to Industry\u00a0\u2013\u00a0Ministerial Order to prevent the spread of box tree moth from the province of Ontario</a></li>\n</ul>\n\n<h2 id=\"a5\">Additional information</h2>\n\n<ul>\n<li><a href=\"https://agriculture.canada.ca/en/canadas-agriculture-sectors/horticulture\">Agriculture and Agri-Food Canada - horticulture sector</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-protection-organizations/eng/1312732291086/1312732417251\">Plant protection organizations</a></li>\n<li><a href=\"/plant-health/invasive-species/biosecurity/eng/1323475203667/1323475279124\">Plant health biosecurity standards and guides</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-pest-surveillance/eng/1344466499681/1344466638872\">Plant pest surveillance</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) \u00e9tablit et maintient les directives et les normes destin\u00e9es \u00e0 l'industrie horticole afin de pr\u00e9venir l'introduction et la propagation d'organismes nuisibles r\u00e9glement\u00e9s justiciables de quarantaine au Canada.</p>\n\n<!--<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Informations et ressources g&#233;n&#233;rales sur les produits et voies d'entr&#233;e du programme de l'Horticulture</a></li>\n<li><a href=\"#a2\">Informations sur l'importation, l'exportation et les mesures en territoire canadien</a></li>\n<li><a href=\"#a3\">Informations sp&#233;cifiques sur les ravageurs</a></li>\n<li><a href=\"#a4\">La communication</a></li>\n<li><a href=\"#a5\">Autres renseignements</a></li>\n</ul>-->\n\n<h2 id=\"a1\">Informations et ressources g\u00e9n\u00e9rales sur les produits et voies d'entr\u00e9e du programme de l'Horticulture</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/fra/1312230684400/1312231127174\">Directives sur la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/gestion-des-risques-phytosanitaire/fra/1304820847590/1304820997079\">Documents de gestion du risque phytosanitaire</a></li>\n<li><a href=\"/protection-des-vegetaux/terre/fra/1542127489815/1542127490190\">Terre et mati\u00e8res connexes \u00e0 la terre</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a>\n<ul>\n<li>Le syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI) indique les exigences d'importation pour les produits r\u00e9glement\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA)</li>\n</ul>\n</li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/glossaire/fra/1304730588212/1304730789969\">Glossaire de la protection des v\u00e9g\u00e9taux</a></li>\n\n</ul>\n\n\n<h2 id=\"a2\">Informations sur l'importation, l'exportation et les mesures en territoire canadien</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/guide-d-introduction/fra/1324568450671/1324569734910\">Importation de v\u00e9g\u00e9taux et de produits v\u00e9g\u00e9taux\u00a0: Ce que vous devez savoir</a></li>\n<li><a href=\"/protection-des-vegetaux/horticulture/liste-des-vegetaux-de-l-horticulture/fra/1419017863407/1419017907742\">Liste des v\u00e9g\u00e9taux de l'horticulture</a>\n<ul>\n<li>Dress\u00e9e par l'Agence canadienne d'inspection des aliments (ACIA), la liste des v\u00e9g\u00e9taux de l'horticulture est compos\u00e9e des v\u00e9g\u00e9taux qui sont fr\u00e9quemment demand\u00e9s et qui sont sujets aux m\u00eames exigences phytosanitaires d'importation de base.</li>\n</ul>\n</li>\n<li><a href=\"/protection-des-vegetaux/horticulture/exportation/fra/1300383870830/1300383922268\">Exigences r\u00e9gissant l'exportation</a> des produits horticoles</li>\n<li>Exigences r\u00e9gissant l'importation des produits horticoles\n<ul>\n<li><a href=\"/protection-des-vegetaux/horticulture/comment-nous-evaluons/fra/1425496755404/1425496838700\">Demander une analyse du risque phytosanitaire</a></li>\n<li><a href=\"/protection-des-vegetaux/horticulture/liste-des-bulbes-des-pays-bas/fra/1508256806587/1508256807508\">Liste des bulbes des Pays-Bas</a></li>\n<li><a href=\"/protection-des-vegetaux/horticulture/tables-de-traitement/fra/1501526269211/1501526560731\">Tables de traitement pour les produits de l'horticulture</a></li>\n<li><a href=\"/protection-des-vegetaux/horticulture/importation-et-horticulture/fra/1466643835510/1466643911736\">P\u00e9riodes d'importation \u00e0 titre d'essai pour les fruits et l\u00e9gumes frais</a></li>\n<li><a href=\"/protection-des-vegetaux/horticulture/approche-systemique/fra/1431458956662/1431459266630\">Approche syst\u00e9mique pour la production de produits v\u00e9g\u00e9taux</a></li>\n</ul>\n</li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/mesures-de-protection-des-vegetaux-en-territoire-c/fra/1629843699782/1629901542997\">Mesures de protection des v\u00e9g\u00e9taux en territoire canadien</a>\n<ul>\n<li>Information sur les r\u00e8glements de l'ACIA et les restrictions de d\u00e9placement int\u00e9rieur au Canada</li>\n</ul>\n</li>\n<li>Comment demander un permis d'importation\u00a0:\n<ul>\n<li><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5256/fra/1491930450091/1491930450578\">Formulaire demande de permis pour importer des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"/a-propos-de-l-acia/mon-acia/fra/1482204298243/1482204318353\">Mon ACIA</a></li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a3\">Informations sp\u00e9cifiques sur les ravageurs</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Parasites r\u00e9glement\u00e9s par le Canada</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/surveillance-phytosanitaire/fra/1344466499681/1344466638872\">Fiches de renseignements sur les phytoravageurs</a>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/fra/1307077188885/1307078272806\">Insectes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/fra/1322807235798/1322807340310\">Maladie des plantes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/fra/1331614724083/1331614823132\">Plantes envahissantes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/fra/1321563900456/1321564231938\">N\u00e9matodes et autres</a></li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a4\">La communication</h2>\n\n<p>Avis publi\u00e9s pour informer les intervenants de mises \u00e0 jour de l'administration de programmes, d'informations de r\u00e9f\u00e9rence et d'orientation disponibles et de modifications apport\u00e9es \u00e0 la loi ou au r\u00e8glement</p>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/gestion-des-risques-phytosanitaire/dgr-21-02/avis-a-l-industrie/fra/1653420323729/1653420324010\">Avis \u00e0 l'industrie\u00a0: Recommandation pour emp\u00eacher le d\u00e9placement des cultivars d'\u00e9pine-vinette 'Concorde', '<span lang=\"en\">Royal Cloak</span>' et '<span lang=\"en\">Tara' Emerald Carousel</span> en Alberta, en Saskatchewan et au Manitoba</a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/directives-nationales-et-d-importation/fra/1682616619810/1682616743632\">Faites-nous part de vos commentaires\u00a0: Consultation sur les propositions de directives nationales et d'importation propos\u00e9es pour le mat\u00e9riel d'arbres fruitiers destin\u00e9 \u00e0 la multiplication ou \u00e0 des fins ornementales</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/pyrale-du-buis/2023-07-12/fra/1689093614563/1689094186895\">Avis \u00e0 l'industrie\u00a0\u2013\u00a0Arr\u00eat\u00e9 minist\u00e9riel pour emp\u00eacher la propagation de la pyrale du buis depuis la province de l'Ontario</a></li>\n</ul>\n\n<h2 id=\"a5\">Autres renseignements</h2>\n\n<ul>\n<li><a href=\"https://agriculture.canada.ca/fr/secteurs-agricoles-du-canada/horticulture\">Agriculture et agroalimentaire Canada - Secteur de l'horticulture</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/organisations-de-la-protection-des-vegetaux/fra/1312732291086/1312732417251\">Organisations de la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/biosecurite/fra/1323475203667/1323475279124\">Normes et guides de bios\u00e9curit\u00e9 de la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/surveillance-phytosanitaire/fra/1344466499681/1344466638872\">Surveillance phytosanitaire</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}