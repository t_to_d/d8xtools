{
    "dcr_id": "1590085149461",
    "title": {
        "en": "Ministerial Exemption process to allow inter-provincial trade of food from establishments that are not federally licensed",
        "fr": "Processus d'exemption minist\u00e9rielle pour permettre le commerce interprovincial d'aliments provenant d'\u00e9tablissements qui ne sont pas agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral"
    },
    "html_modified": "21-5-2020",
    "modified": "1-2-2023",
    "issued": "21-5-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/temporary_ministerial_exemption_during_covid-19_1590085149461_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/temporary_ministerial_exemption_during_covid-19_1590085149461_fra"
    },
    "parent_ia_id": "1584462704709",
    "ia_id": "1590085150008",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Ministerial Exemption process to allow inter-provincial trade of food from establishments that are not federally licensed",
        "fr": "Processus d'exemption minist\u00e9rielle pour permettre le commerce interprovincial d'aliments provenant d'\u00e9tablissements qui ne sont pas agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral"
    },
    "label": {
        "en": "Ministerial Exemption process to allow inter-provincial trade of food from establishments that are not federally licensed",
        "fr": "Processus d'exemption minist\u00e9rielle pour permettre le commerce interprovincial d'aliments provenant d'\u00e9tablissements qui ne sont pas agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral"
    },
    "templatetype": "content page 1 column",
    "node_id": "1590085150008",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1584462704366",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/covid-19/cfia-information-for-industry/ministerial-exemption/",
        "fr": "/covid-19/informations-de-l-acia-pour-l-industrie/exemption-ministerielle/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Ministerial Exemption process to allow inter-provincial trade of food from establishments that are not federally licensed",
            "fr": "Processus d'exemption minist\u00e9rielle pour permettre le commerce interprovincial d'aliments provenant d'\u00e9tablissements qui ne sont pas agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral"
        },
        "description": {
            "en": "Sending original paper export certificates and related annexes or any other Canadian Food Inspection Agency (CFIA) issued export documents to foreign country border control points continues to be the preferred practice.",
            "fr": "Les certificats d'exportation, leurs annexes y aff\u00e9rentes, et les autres documents d'exportation \u00e9mis par l'Agence canadienne d'inspection des aliments (ACIA) sur papier demeurent la pratique privil\u00e9gi\u00e9e pour les envoyer aux points de contr\u00f4le frontalier des pays \u00e9trangers."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, Update on 2019 novel coronavirus, COVID-19, Interim guidance, Sending food export certificates to foreign countries",
            "fr": "Agence canadienne d'inspection des aliments, Mise \u00e0 jour au sujet du nouveau coronavirus 2019, COVID-19, Directive int\u00e9rimaire, Envoi de certificats d'exportation d'aliments vers des pays \u00e9trangers"
        },
        "dcterms.subject": {
            "en": "communications,government communications,exports,health indicators,government information,safety,health care",
            "fr": "communications,communications gouvernementales,exportation,indicateur de sant\u00e9,information gouvernementale,s\u00e9curit\u00e9,soins de sant\u00e9"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-05-21",
            "fr": "2020-05-21"
        },
        "modified": {
            "en": "2023-02-01",
            "fr": "2023-02-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "educators,business,government,general public,media,scientists",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,grand public,m\u00e9dia,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Ministerial Exemption process to allow inter-provincial trade of food from establishments that are not federally licensed",
        "fr": "Processus d'exemption minist\u00e9rielle pour permettre le commerce interprovincial d'aliments provenant d'\u00e9tablissements qui ne sont pas agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-bttm-lg\">2022-04-08</p>\n\n<p>CFIA has developed a Ministerial Exemption (ME) process to permit interprovincial movement of food from establishments that are not federally licensed if such trade becomes necessary to alleviate food shortages.</p>\n\n<p>To enable inter-provincial movement of provincially-inspected food, an ME could be issued to exempt food businesses from certain requirements of the <i>Safe Food for Canadians Act</i> (SFCA) which prohibits the sending or conveying from one province to another of a food commodity that does not meet the requirements of the regulations.</p>\n\n<p>The <i>Food and Drugs Act</i> (FDA) and <i>Food and Drug Regulations</i> (FDR) as well as other relevant federal, provincial and territorial laws related to food continue to apply, despite a Ministerial Exemption being granted. Food labels must be truthful and not misleading and the information should continue to be provided in both official languages.</p>\n\n<h2>Non-meat products</h2>\n<p>Providing food cannot be sourced from SFC licence holders or provincial/territorial food businesses within the province/territory, and it has been determined that interprovincial trade is necessary to alleviate the food shortage of non-meat products, please send an email at: <a href=\"mailto:natasha.richard@inspection.gc.ca\" class=\"nowrap\">natasha.richard@inspection.gc.ca</a> to obtain further details.</p>\n\n<h2>Meat products</h2>\n<p>To initiate the ME process, food businesses experiencing a shortage of meat products are asked to reach out to their Provincial or Territorial Authority to inform them of the food shortage, determine if the situation requires the issuance of an ME by the CFIA, and obtain all information about the application process.</p>\n\n\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Contact information for meat product Ministerial Exemption applications</caption>\n<thead>\n<tr class=\"active\">\n<th scope=\"col\">Provincial or Territorial Jurisdiction</th>\n<th scope=\"col\">Contact</th>\n<th scope=\"col\">Title</th>\n<th scope=\"col\">Email</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th scope=\"row\">British Columbia</th>\n<td>Gavin Last</td>\n<td>Executive Director, Food Safety and Inspection</td>\n<td><a href=\"mailto:bcmeatinspection@gov.bc.ca\" class=\"nowrap\">bcmeatinspection@gov.bc.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Alberta</th>\n<td>Leah Sheffield</td>\n<td>Executive Director, Food Safety Branch</td>\n<td><a href=\"mailto:Leah.Sheffield@gov.ab.ca\" class=\"nowrap\">Leah.Sheffield@gov.ab.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Saskatchewan</th>\n<td>Chris Smith</td>\n<td>Food Safety Specialist, Livestock Branch</td>\n<td><a href=\"mailto:chris.smith@gov.sk.ca\" class=\"nowrap\">chris.smith@gov.sk.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Manitoba</th>\n<td>Ketie Sandhu</td>\n<td>Director of Food Safety\u00a0&amp; Inspection</td>\n<td><a href=\"mailto:Ketie.sandhu@gov.mb.ca\" class=\"nowrap\">Ketie.sandhu@gov.mb.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Ontario</th>\n<td>Raquel Roy</td>\n<td>Program Specialist</td>\n<td><a href=\"mailto:raquel.roy@ontario.ca\" class=\"nowrap\">raquel.roy@ontario.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Quebec</th>\n<td lang=\"fr\">S\u00e9bastien Cloutier</td>\n<td>Coordonnateur des relations FPT en salubrit\u00e9 des aliments</td>\n<td><a href=\"mailto:PEM@mapaq.gouv.qc.ca\" class=\"nowrap\">PEM@mapaq.gouv.qc.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">New Brunswick</th>\n<td>Roxane Plaisance</td>\n<td>Project Executive</td>\n<td><a href=\"mailto:Roxane.Plaisance@gnb.ca\" class=\"nowrap\">Roxane.Plaisance@gnb.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Nova Scotia</th>\n<td>Marion MacAulay</td>\n<td>A/Executive Director, Agriculture and Food Operations</td>\n<td><a href=\"mailto:Marion.MacAulay@novascotia.ca\" class=\"nowrap\">Marion.MacAulay@novascotia.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Prince Edward Island</th>\n<td>Dwight Thompson</td>\n<td>Legislative\u00a0&amp; Program Specialist</td>\n<td><a href=\"mailto:dkthompson@gov.pe.ca\" class=\"nowrap\">dkthompson@gov.pe.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Newfoundland and Labrador</th>\n<td>Leona Raymond</td>\n<td>Food Safety Technician</td>\n<td><a href=\"mailto:leonaraymond@gov.nl.ca\" class=\"nowrap\">leonaraymond@gov.nl.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Northwest Territories</th>\n<td>Joel Holder</td>\n<td>Director, Economic Diversification Industry, Tourism and Investment</td>\n<td><a href=\"mailto:Joel_Holder@gov.nt.ca\" class=\"nowrap\">Joel_Holder@gov.nt.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Nunavut</th>\n<td>Zoya Martin</td>\n<td>Director, Fisheries and Sealing</td>\n<td><a href=\"mailto:zmartin@gov.nu.ca\" class=\"nowrap\">zmartin@gov.nu.ca</a></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-bttm-lg\">2022-04-08</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) a \u00e9labor\u00e9 un processus d'exemption minist\u00e9rielle pour permettre la circulation interprovinciale des aliments provenant d'\u00e9tablissements qui ne sont pas agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral si ce commerce devient n\u00e9cessaire pour att\u00e9nuer les p\u00e9nuries d'aliments.</p>\n\n<p>Pour permettre la circulation interprovinciale d'aliments inspect\u00e9s par les provinces, une exemption minist\u00e9rielle pourrait \u00eatre \u00e9mise afin d'exempter les entreprises alimentaires de certaines exigences de la <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> (LSAC) qui interdisent l'envoi ou le transport d'une province \u00e0 une autre d'un produit alimentaire qui ne satisfait pas aux exigences de la r\u00e9glementation.</p>\n\n<p>La <i>Loi sur les aliments et drogues</i> (LAD) et le <i>R\u00e8glement sur les aliments et drogues</i> (RAD) ainsi que d'autres lois f\u00e9d\u00e9rales, provinciales et territoriales pertinentes relatives aux aliments continuent de s'appliquer, malgr\u00e9 l'octroi d'une exemption minist\u00e9rielle. Les \u00e9tiquettes des aliments doivent \u00eatre v\u00e9ridiques et non trompeuses, et l'information doit continuer d'\u00eatre inscrite dans les deux langues officielles.</p>\n\n<h2>Produits non carn\u00e9s</h2>\n<p>Si les aliments ne peuvent pas provenir d'\u00e9tablissements titulaires de licence pour la salubrit\u00e9 des aliments au Canada ou d'entreprises alimentaires provinciales ou territoriales dans la province ou le territoire et s'il a \u00e9t\u00e9 d\u00e9termin\u00e9 que le commerce interprovincial est n\u00e9cessaire pour att\u00e9nuer la p\u00e9nurie de produits non carn\u00e9s, veuillez envoyer un courriel \u00e0 <a href=\"mailto:natasha.richard@inspection.gc.ca\" class=\"nowrap\">natasha.richard@inspection.gc.ca</a> pour obtenir de plus amples renseignements.</p>\n\n<h2>Produits carn\u00e9s</h2>\n<p>Pour amorcer le processus d'exemption minist\u00e9rielle, les entreprises alimentaires qui connaissent une p\u00e9nurie de produits carn\u00e9s sont pri\u00e9es de communiquer avec leur autorit\u00e9 provinciale ou territoriale pour l'informer de la p\u00e9nurie d'aliments, d\u00e9terminer si la situation n\u00e9cessite l'\u00e9mission d'une telle exemption par l'ACIA, et obtenir tous les renseignements pertinents sur le processus de demande.</p>\n\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\"> \n<caption class=\"text-left\">Coordonn\u00e9es pour l'envoi des demandes d'exemption minist\u00e9rielle pour les produits carn\u00e9s</caption>\n<thead>\n<tr class=\"active\">\n<th scope=\"col\">Administration provinciale ou territoriale</th>\n<th scope=\"col\">Personne-ressource</th>\n<th scope=\"col\">Titre</th>\n<th scope=\"col\">Courriel</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th scope=\"row\">Colombie-Britannique</th>\n<td>Gavin Last</td>\n<td>Directeur ex\u00e9cutive, Salubrit\u00e9 et inspection des aliments</td>\n<td><a href=\"mailto:bcmeatinspection@gov.bc.ca\" class=\"nowrap\">bcmeatinspection@gov.bc.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Alberta</th>\n<td>Leah Sheffield</td>\n<td>Directrice g\u00e9n\u00e9rale, Direction g\u00e9n\u00e9rale de la salubrit\u00e9 des aliments</td>\n<td><a href=\"mailto:Leah.Sheffield@gov.ab.ca\" class=\"nowrap\">Leah.Sheffield@gov.ab.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Saskatchewan</th>\n<td>Chris Smith</td>\n<td>Sp\u00e9cialiste de la salubrit\u00e9 des aliments, Direction g\u00e9n\u00e9rale des animaux d'\u00e9levage</td>\n<td><a href=\"mailto:chris.smith@gov.sk.ca\" class=\"nowrap\">chris.smith@gov.sk.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Manitoba</th>\n<td>Ketie Sandhu</td>\n<td>Directrice de la salubrit\u00e9 et de l'inspection des aliments</td>\n<td><a href=\"mailto:Ketie.sandhu@gov.mb.ca\" class=\"nowrap\">Ketie.sandhu@gov.mb.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Ontario</th>\n<td>Raquel Roy</td>\n<td>Sp\u00e9cialiste des programmes</td>\n<td><a href=\"mailto:raquel.roy@ontario.ca\" class=\"nowrap\">raquel.roy@ontario.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Qu\u00e9bec</th>\n<td>S\u00e9bastien Cloutier</td>\n<td>Coordonnateur des relations FPT en salubrit\u00e9 des aliments</td>\n<td><a href=\"mailto:PEM@mapaq.gouv.qc.ca\" class=\"nowrap\">PEM@mapaq.gouv.qc.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Nouveau-Brunswick</th>\n<td>Roxane Plaisance</td>\n<td>Directrice de projet</td>\n<td><a href=\"mailto:Roxane.Plaisance@gnb.ca\" class=\"nowrap\">Roxane.Plaisance@gnb.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Nouvelle-\u00c9cosse</th>\n<td>Marion MacAulay</td>\n<td>Directrice ex\u00e9cutive p.i., Op\u00e9rations agricoles et alimentaires</td>\n<td><a href=\"mailto:Marion.MacAulay@novascotia.ca\" class=\"nowrap\">Marion.MacAulay@novascotia.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">\u00cele-du-Prince-\u00c9douard</th>\n<td>Dwight Thompson</td>\n<td>Sp\u00e9cialiste des lois et des programmes</td>\n<td><a href=\"mailto:dkthompson@gov.pe.ca\" class=\"nowrap\">dkthompson@gov.pe.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Terre-Neuve-et-Labrador</th>\n<td>Leona Raymond</td>\n<td>Technicienne en salubrit\u00e9 des aliments</td>\n<td><a href=\"mailto:leonaraymond@gov.nl.ca\" class=\"nowrap\">leonaraymond@gov.nl.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Territoires du Nord-Ouest</th>\n<td>Joel Holder</td>\n<td>Directeur de la diversification \u00e9conomique, Industrie, Tourisme et Investissement</td>\n<td><a href=\"mailto:Joel_Holder@gov.nt.ca\" class=\"nowrap\">Joel_Holder@gov.nt.ca</a></td>\n</tr>\n<tr>\n<th scope=\"row\">Nunavut</th>\n<td>Zoya Martin</td>\n<td>Directrice, P\u00eache et chasse au phoque</td>\n<td><a href=\"mailto:zmartin@gov.nu.ca\" class=\"nowrap\">zmartin@gov.nu.ca</a></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}