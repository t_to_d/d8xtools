{
    "dcr_id": "1449632962119",
    "title": {
        "en": "Identifying and Preventing the Spread of the Emerald Ash Borer",
        "fr": "Identification de l'agrile du fr\u00eane et mesures \u00e0 prendre pour pr\u00e9venir sa propagation"
    },
    "html_modified": "14-12-2015",
    "modified": "27-8-2019",
    "issued": "17-5-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_agrpla_identify_1449632962119_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_agrpla_identify_1449632962119_fra"
    },
    "parent_ia_id": "1337273975030",
    "ia_id": "1449632962930",
    "parent_node_id": "1337273975030",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Identifying and Preventing the Spread of the Emerald Ash Borer",
        "fr": "Identification de l'agrile du fr\u00eane et mesures \u00e0 prendre pour pr\u00e9venir sa propagation"
    },
    "label": {
        "en": "Identifying and Preventing the Spread of the Emerald Ash Borer",
        "fr": "Identification de l'agrile du fr\u00eane et mesures \u00e0 prendre pour pr\u00e9venir sa propagation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1449632962930",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1337273882117",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/emerald-ash-borer/identification/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/identification/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Identifying and Preventing the Spread of the Emerald Ash Borer",
            "fr": "Identification de l'agrile du fr\u00eane et mesures \u00e0 prendre pour pr\u00e9venir sa propagation"
        },
        "description": {
            "en": "The beetle has a metallic green back and an emerald green underside. Ranging from 8.5 to 14.0 mm long and 3.1 to 3.4 mm wide, the beetle is fairly small and difficult to spot. Due to its small size, detection may be easier by looking for S-shaped lines formed by EAB larva or unhealthy ash trees rather than the insect itself.",
            "fr": "Les adultes sont vert m\u00e9tallique sur le dessus et vert \u00e9meraude en-dessous. Du fait de leur taille relativement modeste (8,5 \u00e0 14,0 mm de longueur sur 3,1 \u00e0 3,4 mm de largeur), ils sont difficiles \u00e0 d\u00e9tecter, et il est souvent plus facile de rep\u00e9rer les galeries sinueuses for\u00e9es par les larves ou les fr\u00eanes infest\u00e9s que l'insecte lui-m\u00eame."
        },
        "keywords": {
            "en": "Emerald Ash Borer Approved Facility Co, Phytosanitary Requirements, Management System Manual, spread of the emerald ash borer, Childrens Activity",
            "fr": "bois de chauffage, Agrile du fr\u00eane, Exigences phytosanitaires, protection des v\u00e9g\u00e9taux, Exigences relatives"
        },
        "dcterms.subject": {
            "en": "inspection,pests,plants",
            "fr": "inspection,organisme nuisible,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-12-14",
            "fr": "2015-12-14"
        },
        "modified": {
            "en": "2019-08-27",
            "fr": "2019-08-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Identifying and Preventing the Spread of the Emerald Ash Borer",
        "fr": "Identification de l'agrile du fr\u00eane et mesures \u00e0 prendre pour pr\u00e9venir sa propagation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Identifying <abbr title=\"Emerald Ash Borer\">EAB</abbr></h2>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<p>The beetle has a metallic green back and an emerald green underside. Ranging from 8.5 to 14.0 <abbr title=\"millimetre\">mm</abbr> long and 3.1 to 3.4 <abbr title=\"millimetre\">mm</abbr> wide, the beetle is fairly small and difficult to spot. Due to its small size, detection may be easier by looking for S-shaped lines formed by <abbr title=\"Emerald Ash Borer\">EAB</abbr> larva or unhealthy ash trees rather than the insect itself.</p>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/emerald-ash-borer/activity/eng/1337362866708/1337362981745\">Emerald Ash Borer (EAB): Find the beetle! (Children's Activity)</a></li>\n</ul>\n<img alt=\"Emerald Ash Borer. Actual size: approximately 8.5 to 14 millimetres. Exit hole: approximately 3.5 millimetres.\" class=\"img-responsive center-block\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_agrpla_identify_image2_1449636976897_eng.jpg\">\n</div>\n<div class=\"col-sm-3\">\n<img alt=\"S-shaped lines S-shaped galleries (or tunnels) found beneath the bark of an ash that has been infested by the emerald ash borer\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_agrpla_identify_image1_1449636862656_eng.jpg\">\n</div>\n\n</div>\n\n<div class=\"clearfix\"> </div>\n\n<h2>Help Prevent the Spread</h2>\n\n<div class=\"pull-right mrgn-lft-md\">\n<p><img alt=\"Don't Move Firewood\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/firewood_poster_sm_1371312417946_eng.jpg\"></p>\n</div>\n\n<p>Slowing the spread of the emerald ash borer will protect Canada's environment and forest resources. It also helps keep international markets open to the forest industry and nurseries in non-regulated parts of Canada.</p>\n\n<p>The emerald ash borer is most commonly spread through the movement of firewood and other infested ash wood products. By moving infested firewood, this invasive species is transported quickly across large areas. Help prevent the spread of pests: <a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">Don't Move Firewood</a>.</p>\n\n<p>Federal regulatory measures prohibit the movement of <a href=\"/plant-health/invasive-species/insects/emerald-ash-borer/regulated-articles/eng/1337286621795/1337286747578\">specific materials</a> including any ash material and firewood of all species from <a href=\"/plant-health/invasive-species/directives/forest-products/d-03-08/areas-regulated/eng/1347625322705/1347625453892\">specific areas of Manitoba, Ontario, Quebec, New Brunswick and Nova Scotia</a>. Anyone violating these restrictions is subject to a fine and/or prosecution.</p>\n\n<div class=\"clearfix\"> </div>\n\n<h2>The Importance of Ash Trees</h2>\n\n<p>Ash trees are an important part of Canada's landscape. They can be found on city streets, woodlots, and in forests across Canada. Not only do ash trees contribute to a city's aesthetic value, but ash wood is used to make furniture, hardwood floors, baseball bats, tool handles, electric guitars, hockey sticks and other materials that require high strength and resilience. Learn how to <a href=\"http://tidcf.nrcan.gc.ca/en/trees/identification/broadleaf/8/Fraxinus\">identify ash trees</a>.</p>\n\n<p>If there is a high presence of the emerald ash borer in your area, and many ash trees have been destroyed by the beetle, try replanting with <a href=\"/plant-health/invasive-species/insects/emerald-ash-borer/recommended-alternatives/eng/1337363806469/1337363875644\">recommended alternatives to ash trees</a>.</p>\n\n<h2>Life Cycle of <abbr title=\"Emerald Ash Borer\">EAB</abbr></h2>\n\n<p>Immature emerald ash borers feed on tree leaves, resulting in abnormal slits in the leaves. Their eggs are laid on the bark of branches of the tree on which they establish themselves. Their larvae bore through the bark, feeding on the inner bark and sapwood, eventually forming flat, 6 <abbr title=\"millimetre\">mm</abbr>, S-shaped galleries which are filled with a fine brownish coating. The larva can grow from 2 to 5 <abbr title=\"centimetres\">cm</abbr> long and the width of the S-shaped gallery increases throughout its life span.</p>\n\n<p><strong>Sign up to receive email updates on <abbr title=\"Emerald Ash Borer\">EAB</abbr> <a href=\"https://notification.inspection.canada.ca/\">Email Notification Services</a>.</strong></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Identification de l'agrile du fr\u00eane</h2>\n\n<div class=\"row\">\n<div class=\"col-sm-9\">\n<p>Les adultes sont vert m\u00e9tallique sur le dessus et vert \u00e9meraude en-dessous. Du fait de leur taille relativement modeste (8,5 \u00e0 14,0 <abbr title=\"millim\u00e8tre\">mm</abbr> de longueur sur 3,1 \u00e0 3,4 <abbr title=\"millim\u00e8tre\">mm</abbr> de largeur), ils sont difficiles \u00e0 d\u00e9tecter, et il est souvent plus facile de rep\u00e9rer les galeries sinueuses for\u00e9es par les larves ou les fr\u00eanes infest\u00e9s que l'insecte lui-m\u00eame.</p>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/activite/fra/1337362866708/1337362981745\">Agrile du fr\u00eane\u00a0: Trouvez l'insecte! (activit\u00e9 pour les enfants)</a></li>\n</ul>\n<img alt=\"Agrile du fr\u00eane.  Taille r\u00e9elle : environ 8,5 \u00e0 14 millimetres. Trou d'\u00e9mergence : environ 3,5 millimetres\" class=\"img-responsive center-block\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_agrpla_identify_image2_1449636976897_fra.jpg\">\n</div>\n<div class=\"col-sm-3\">\n<img alt=\"Les galeries (ou tunnels) en forme de \u00ab S \u00bb que l'on trouve sous l'\u00e9corce d'un fr\u00eane infest\u00e9 par l'agrile du fr\u00eane.\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_agrpla_identify_image1_1449636862656_fra.jpg\">\n</div>\n\n</div>\n\n<div class=\"clearfix\"> </div>\n\n<h2>Aidez-nous \u00e0 pr\u00e9venir la propagation de l'agrile du fr\u00eane</h2>\n\n<div class=\"pull-right mrgn-lft-md\">\n<p><img alt=\"Ne d\u00e9placez pas de bois de chauffage\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/firewood_poster_sm_1371312417946_fra.jpg\"></p>\n</div>\n\n<p>En freinant la propagation de l'agrile du fr\u00eane, vous contribuerez \u00e0 prot\u00e9ger l'environnement et les ressources foresti\u00e8res du Canada. Vous aiderez \u00e9galement \u00e0 maintenir l'acc\u00e8s aux march\u00e9s internationaux pour l'industrie foresti\u00e8re et les p\u00e9pini\u00e8res dans les zones non r\u00e9glement\u00e9es du Canada.</p>\n\n<p>La propagation de l'agrile du fr\u00eane est le plus souvent favoris\u00e9e par le d\u00e9placement de bois de chauffage et d'autres produits du fr\u00eane. Le d\u00e9placement de bois de chauffage infest\u00e9 peut permettre au ravageur de se d\u00e9placer rapidement sur de grandes distances. Aidez-nous \u00e0 pr\u00e9venir la propagation des ravageurs\u00a0: <a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">Ne d\u00e9placez pas de bois de chauffage</a>.</p>\n\n<p>Des mesures r\u00e9glementaires f\u00e9d\u00e9rales interdisent le d\u00e9placement de <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/produits-reglementes/fra/1337286621795/1337286747578\">mati\u00e8res pr\u00e9cises</a>, y compris les produits du fr\u00eane et le bois de chauffage de toutes les essences, en provenance <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-03-08/zones-reglementees/fra/1347625322705/1347625453892\">de zones r\u00e9glement\u00e9es au Manitoba, en Ontario, au Qu\u00e9bec, au Nouveau-Brunswick et au Nouvelle-\u00c9cosse</a>. Quiconque contrevient \u00e0 ces restrictions commet une infraction et est passible d'une amende ou s'expose \u00e0 des poursuites judiciaires.</p>\n\n<h2>Importance des fr\u00eanes</h2>\n\n<p>Les fr\u00eanes occupent une place importante du paysage du Canada. Ils sont couramment plant\u00e9s en ville comme arbres d'avenue et forment une composante importante des bois\u00e9s et des for\u00eats \u00e0 l'\u00e9chelle du Canada. Les fr\u00eanes contribuent \u00e0 la valeur esth\u00e9tique des paysages urbains. Le bois de fr\u00eane est \u00e9galement utilis\u00e9 pour la fabrication de meubles, de planchers de bois franc, de b\u00e2tons de baseball et de hockey, de manches d'outils, de guitares \u00e9lectriques et d'autres objets qui doivent \u00eatres solides et r\u00e9sistants. Apprenez comment <a href=\"http://aimfc.rncan.gc.ca/fr/arbres/identification/feuillus/8/Fraxinus\">identifier les fr\u00eanes</a>.</p>\n\n<p>Si l'agrile du fr\u00eane est abondant dans votre r\u00e9gion et y a d\u00e9j\u00e0 d\u00e9truit de nombreux fr\u00eanes, consultez la liste des <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/essences-recommandees/fra/1337363806469/1337363875644\">essences recommand\u00e9es pour remplacer les fr\u00eanes</a> avant de proc\u00e9der \u00e0 la plantation de nouveaux arbres.</p>\n\n\n<h2>Cycle vital de l'agrile du fr\u00eane</h2>\n\n\n<p>Les agriles du fr\u00eane immatures d\u00e9coupent des encoches dans le feuillage des fr\u00eanes en s'alimentant. Les femelles d\u00e9posent leurs \u0153ufs sur l'\u00e9corce des branches des arbres h\u00f4tes. \u00c0 l'\u00e9closion, les larves s'enfoncent dans l'\u00e9corce pour s'alimenter entre l'\u00e9corce interne et l'aubier, forant au fil de leur progression une galerie sinueuse et plate de 6 <abbr title=\"millim\u00e8tre\">mm</abbr> de largeur remplie d'une fine sciure brun\u00e2tre. Les larves peuvent atteindre 2 \u00e0 5 <abbr title=\"centim\u00e8tre\">cm</abbr> de longueur, et la largeur de leur galerie sinueuse augmente au fil de leur croissance.</p>\n\n<p><strong>Inscrivez-vous pour recevoir par <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">courriel des informations</a> \u00e0 jour sur l'agrile du fr\u00eane</strong>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}