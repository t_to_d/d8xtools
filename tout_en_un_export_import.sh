#!/bin/bash
drupalsite=$1
d8xtools=`pwd`

catch() {
  echo "Error $1 occurred on $2";
  if [ $2 == 87 ]; then
    echo "Ignore this error, doesn't hurt anything\n";
  fi
  if [ $2 == 84 ]; then
    echo "Ignoring error on line $2 , it's a known issue, all tables restored except one view and there's a script that adds the missing view back (search_node_url).\n";
    sleep 1;
  fi
  if [ $2 == 97 ] || [ $2 == 16 ]; then
    echo "run the gene-gene import again for the imports\n";sleep 5;
    drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current page 2>&1 | tee ${docroot}/etape5a_import_gene-gene.log
  fi
  if [ $2 == 94 ] || [ $2 == 37 ]; then
    d8xtools=$3;
    docroot=$4;
    cd $docroot;
    echo "$2 Vas-y fait l'étape manuel. Error Workaround. Manually: Go to site, create a node, delete that node";
    i=30;
    echo "chrono ${i} sec\n";
    for (( i=30; i>0; i--)); do
      sleep 1 &
      printf "  $i \r"
      wait
    done
    cd $docroot;
    echo -n "Vas-y fait l'étape manuel. Error Workaround. Manually: Go to site, create a node, delete that node ensuite pese 1 [ENTER]: "
    un_et_pret=1;#read -n 1 un_et_pret
#    echo "drush scr delete_all_nodes.php";
#          drush scr delete_all_nodes.php;
      if [ "$un_et_pret" == "1" ]; then
        echo "run the sitemap init import again for the IA (information architecture)\n";sleep 1;
        drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current sitemapinit 2>&1 | tee ${docroot}/etape4a_import_sitemapinit.log
      else
        echo "exit;"
        exit;
      fi
    else
      echo "erreur sur $2";
    sleep 2;
  fi
}
trap 'catch $? $LINENO $d8xtools $docroot' ERR

if [[ "$d8xtools" == *d8xtools ]]; then
  echo "d8xtools start export scripts\n";
elif [[ "$d8xtools" == *docroot ]]; then
  cd ../d8xtools;
  d8xtools=`pwd`
else
  echo "oops, not docroot , nor d8xtools";
  exit;
fi

cd ../docroot;
docroot=`pwd`;
cd $d8xtools;
#ignore this#rm -rf ${d8xtools}/cfia_export_current/gene-gene;
#ignore this#rm -rf ${d8xtools}/cfia_export_current/forms-formes;
#ignore this#rm -rf ${d8xtools}/cfia_export_current/manuals-manuels;
#ignore this#rm -rf ${d8xtools}/cfia_export_current/pros-judi;

php csv_to_json_export.php z_cfia_raw/export_with_ia_en_and_fr.csv 2>&1 | tee ${docroot}/etape1_csv_processing.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 1;
php update_json.php ${d8xtools}/cfia_export_current/sitemapinit sitemap 2>&1 | tee ${docroot}/etape2_update_json_sitemap.log
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 1;
cd $d8xtools;
php update_json.php ${d8xtools}/cfia_export_current/page teamsite 2>&1 | tee ${docroot}/etape3_update_json_page.log
php update_json.php ${d8xtools}/cfia_export_current/forms-formes teamsite 2>&1 | tee ${docroot}/etape3_update_json_forms-formes.log
php update_json.php ${d8xtools}/cfia_export_current/manuals-manuels teamsite 2>&1 | tee ${docroot}/etape3_update_json_manuals-manuels.log
php update_json.php ${d8xtools}/cfia_export_current/pros-judi teamsite 2>&1 | tee ${docroot}/etape3_update_json_pros-judi.log
bash cleanup.sh;
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 1;

#git checkout cfia_export_current/sitemapinit/1354137773675.json
cd $docroot;
drush sqlq 'drop view if exists search_node_url;';
#drush sql-drop -y;
#rm wxtdx.sql -f;
#tar -pxzf wxtdx.sql.tgz
#cat ${docroot}/wxtdx.sql | drush sqlc 2>/dev/null;
#drush pmu warmer -y;
#composer install;
#drush pmu content_moderation_notifications safedelete -y 2>/dev/null;
#drush cr;drush scr delete_all_nodes.php
drush sapi-dis guidance_finder; drush sapi-dis inspect_and_protect;
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current sitemapinit | tee ${docroot}/etape4_import_sitemap.log
echo "import sitemapinit done, continue";
sleep 3
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current page | tee ${docroot}/etape5aa_import_gene-gene.log
echo "REVIEW import report above for gene-gene.";
echo "sleep 3, about to start importing pros-judi.";
sleep 3;
rm ${d8xtools}/cfia_export_current/page
pushd ${d8xtools}/cfia_export_current/;
ln -s pros-judi page;
popd;
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current page 2>&1 | tee ${docroot}/etape5b_import_pros-judi.log
echo "REVIEW import report above for pros-judi.";
echo "sleep 3, about to start importing forms-formes.";
sleep 3;
rm ${d8xtools}/cfia_export_current/page
pushd ${d8xtools}/cfia_export_current/;
ln -s forms-formes page;
popd;
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current page 2>&1 | tee ${docroot}/etape5c_import_forms-formes.log
echo "REVIEW import report above for forms-formes.";
echo "sleep 3, about to start importing manuals-manuels.";
sleep 3;
rm ${d8xtools}/cfia_export_current/page
pushd ${d8xtools}/cfia_export_current/;
ln -s manuals-manuels page;
popd;
drush scr ${d8xtools}/drupal_export_import/import.php ${d8xtools}/cfia_export_current page 2>&1 | tee ${docroot}/etape5d_import_manuals-manuels.log
rm ${d8xtools}/cfia_export_current/page
pushd ${d8xtools}/cfia_export_current/;
ln -s gene-gene page;
popd;
cd $docroot;
drush cfia-post-migrate:navi-navi -y;
drush scr ${d8xtools}/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL page 2>&1 | tee ${docroot}/etape6a_convert_teamsiteId_to_drupal_full_meal_deal.log
sleep 1
drush cr;
sleep 1
drush pathauto:aliases-generate create canonical_entities:node -y;
sleep 1
echo "REVIEW ABOVE, NEXT STEP NOW";
sleep 3;

cd ${d8xtools};

rm -rf convertTeamsiteIdToDrupal/export;
rm -rf convertTeamsiteIdToDrupal/exportcontent;
mkdir convertTeamsiteIdToDrupal/export;
mkdir convertTeamsiteIdToDrupal/exportcontent;

cp -pr cfia_export_current/pros-judi/*json ${d8xtools}/convertTeamsiteIdToDrupal/export;
cp -pr cfia_export_current/forms-formes/*json ${d8xtools}/convertTeamsiteIdToDrupal/export;
cp -pr cfia_export_current/gene-gene/*json ${d8xtools}/convertTeamsiteIdToDrupal/export;
cp -pr cfia_export_current/manuals-manuels/*json ${d8xtools}/convertTeamsiteIdToDrupal/export;
cp -pr cfia_export_current/sitemapinit/*json ${d8xtools}/convertTeamsiteIdToDrupal/export;

cd ${d8xtools}/convertTeamsiteIdToDrupal;

php exportContentURL.php ${d8xtools}/convertTeamsiteIdToDrupal/export 2>&1 | tee ${docroot}/etape7_exportContentURL.log
rm -rf ${d8xtools}/convertTeamsiteIdToDrupal/export/1*.json;#plus besoin.
php retrieveFileContentFromCFIAInternet.php export 2>&1 | tee ${docroot}/etape8_retrieveFileContentFromCFIAInternet.log
#script ci-dessus va créer exportcontent avec les docx doc pdf png jpg etc dedans.
rm -rf ${docroot}/html/sites/default/files/legacy
mv ${d8xtools}/convertTeamsiteIdToDrupal/exportcontent ${docroot}/html/sites/default/files/legacy

cd ${docroot};
drush scr ${d8xtools}/convertTeamsiteIdToDrupal/updateContentURL.php ${d8xtools}/convertTeamsiteIdToDrupal/export/ 2>&1 | tee ${docroot}/etape9_updateContentURL.log
rm -rf ${d8xtools}/convertTeamsiteIdToDrupal/export/sites;

# comment out the verify term translations step, not sure if we need these yet, maybe.
cd ${docroot};
#drush scr ../d8xtools/drupal_export_import/importers/taxonomy/verifyAndAddMissingTermTranslations.php 2>&1 | tee ${docroot}/etape99_verifyAndAddMissingTermTranslations.log

echo "--------------------"
echo ""
git checkout html/sites/default/files/legacy;
echo "drush scr ../d8xtools/convertTeamsiteIdToDrupal/special_title_import_file.php 2>&1 | tee ${docroot}/etape10_special_title.log";
      drush scr ../d8xtools/convertTeamsiteIdToDrupal/special_title_import_file.php 2>&1 | tee ${docroot}/etape10_special_title.log
echo "drush scr ../d8xtools/convertTeamsiteIdToDrupal/special_title_import_file_2.php 2>&1 | tee ${docroot}/etape10b_special_title2.log";
      drush scr ../d8xtools/convertTeamsiteIdToDrupal/special_title_import_file_2.php 2>&1 | tee ${docroot}/etape10b_special_title2.log
cd ${d8xtools}/convertTeamsiteIdToDrupal;
rm -rf export/sitemapinit;
#begin retrieving social media icons for navi-nav blocks.
pushd ${docroot}/html/sites/default/files/legacy/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_linkedin30x30_1647230511213_eng.png
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_twitter_1647217319679_eng.png
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_twitter_1647217319679_fra.png
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_twitter30x30_1647230476448_eng.png
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_facebook30x30_1647230447415_eng.png
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/home_banner_slide_04_1707841920235_eng.jpg;
wget https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/home_banner_slide_03_1707841919251_fra.jpg
popd
#end of media icons for navi-nav blocks.
#Begin prep of assets archive.
pushd ${docroot}/html/sites/default/files
tar -pczf ../../../../files_contents.tgz campaign legacy embed_buttons library-definitions media-icons styles translations private php default-avatar.png
popd;
cd $docroot;
#Restore checked-in assets.
git checkout html/sites/default/files/legacy;
sleep 1
drush sapi-en guidance_finder; drush sapi-en inspect_and_protect;
sleep 1
#drush sapi-i guidance_finder
sleep 1
drush ucrt simona.zoccolan --mail=simona.zoccolan@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" simona.zoccolan
drush urol "editor" simona.zoccolan
drush urol "reviewer" simona.zoccolan
drush urol "pab_admin" simona.zoccolan
drush uublk simona.zoccolan
drush ucrt arman.forouzan --mail=arman.forouzan@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" arman.forouzan
drush urol "editor" arman.forouzan
drush urol "reviewer" arman.forouzan
drush urol "pab_admin" arman.forouzan
drush uublk arman.forouzan
drush ucrt gerald.batten --mail=gerald.batten@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" gerald.batten
drush urol "editor" gerald.batten
drush urol "reviewer" gerald.batten
drush urol "pab_admin" gerald.batten
drush uublk gerald.batten
#drush ucrt david.leonhardt --mail=david.leonhardt@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush upwd david.leonhardt 'Changetolongerphraseplease99!'
drush urol "creator" david.leonhardt
drush urol "editor" david.leonhardt
drush urol "reviewer" david.leonhardt
drush urol "pab_admin" david.leonhardt
drush uublk david.leonhardt
drush ucrt dominic.tetreault --mail=dominic.tetreault@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" dominic.tetreault
drush urol "editor" dominic.tetreault
drush urol "reviewer" dominic.tetreault
drush urol "pab_admin" dominic.tetreault
drush uublk dominic.tetreault
drush ucrt padraighariel.sinasac --mail=padraighariel.sinasac@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" padraighariel.sinasac
drush urol "editor" padraighariel.sinasac
drush urol "reviewer" padraighariel.sinasac
drush urol "pab_admin" padraighariel.sinasac
drush uublk padraighariel.sinasac
drush ucrt marc.obonsawin --mail=marc.obonsawin@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" marc.obonsawin
drush urol "editor" marc.obonsawin
drush urol "reviewer" marc.obonsawin
drush urol "pab_admin" marc.obonsawin
drush uublk marc.obonsawin
drush ucrt fernando.gigliopereira --mail=fernando.gigliopereira@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" fernando.gigliopereira
drush urol "editor" fernando.gigliopereira
drush urol "reviewer" fernando.gigliopereira
drush urol "pab_admin" fernando.gigliopereira
drush urol "administrator" fernando.gigliopereira
drush uublk fernando.gigliopereira
drush ucrt france.durocher --mail=france.durocher@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" france.durocher
drush urol "editor" france.durocher
drush urol "reviewer" france.durocher
drush urol "pab_admin" france.durocher
drush uublk france.durocher
drush ucrt murray.wambolt --mail=murray.wambolt@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" murray.wambolt
drush urol "editor" murray.wambolt
drush urol "reviewer" murray.wambolt
drush urol "pab_admin" murray.wambolt
drush uublk murray.wambolt
drush ucrt domenic.divincenzo --mail=domenic.divincenzo@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" domenic.divincenzo
drush urol "editor" domenic.divincenzo
drush urol "reviewer" domenic.divincenzo
drush urol "pab_admin" domenic.divincenzo
drush uublk domenic.divincenzo
drush ucrt tomomi.sakajiri --mail=tomomi.sakajiri@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" tomomi.sakajiri
drush urol "editor" tomomi.sakajiri
drush urol "reviewer" tomomi.sakajiri
drush urol "pab_admin" tomomi.sakajiri
drush uublk tomomi.sakajiri
drush ucrt anthony.furano --mail=anthony.furano@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" anthony.furano
drush urol "editor" anthony.furano
drush urol "reviewer" anthony.furano
drush urol "pab_admin" anthony.furano
drush uublk anthony.furano
drush ucrt viraj.nasit --mail=viraj.nasit@inspection.gc.ca --password='Changetolongerphraseplease99!'
drush urol "creator" viraj.nasit
drush urol "editor" viraj.nasit
drush urol "reviewer" viraj.nasit
drush urol "pab_admin" viraj.nasit
drush uublk viraj.nasit
drush ucrt smulvihill --mail=smulvih2@gmail.com --password='test123'
drush urol "administrator" smulvihill
drush urol "pab_admin" smulvihill
drush uublk smulvihill
sleep 1
drush sqlq 'drop view if exists search_node_url';
drush wd-del all -y;
drush cr;
drush sql-dump > wxtdx_fresh_install_with_imports.sql
tar -pczf wxtdx_fresh_install_with_imports.sql.tgz wxtdx_fresh_install_with_imports.sql
