{
    "dcr_id": "1362593586494",
    "title": {
        "en": "Infection with <em lang=\"la\">Haplosporidium nelsoni</em>",
        "fr": "Infection \u00e0 <em lang=\"la\">Haplosporidium nelsoni</em>"
    },
    "html_modified": "2024-01-26 2:21:44 PM",
    "modified": "2013-12-06",
    "issued": "2013-03-06 13:13:07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_haplosporidium_nelsoni_factsheet_1362593586494_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_haplosporidium_nelsoni_factsheet_1362593586494_fra"
    },
    "ia_id": "1362593745912",
    "parent_ia_id": "1362585762217",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1362585762217",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Infection with <em lang=\"la\">Haplosporidium nelsoni</em>",
        "fr": "Infection \u00e0 <em lang=\"la\">Haplosporidium nelsoni</em>"
    },
    "label": {
        "en": "Infection with <em lang=\"la\">Haplosporidium nelsoni</em>",
        "fr": "Infection \u00e0 <em lang=\"la\">Haplosporidium nelsoni</em>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1362593745912",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1362585610878",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/haplosporidium-nelsoni/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/haplosporidium-nelsoni/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Infection with Haplosporidium nelsoni",
            "fr": "Infection \u00e0 Haplosporidium nelsoni"
        },
        "description": {
            "en": "Bonamia ostreae causes disease in molluscs. It is a cause of mortality in juvenile and adult oysters.",
            "fr": "Haplosporidium nelsoni cause la maladie chez les hu\u00eetres. Il cause la mortalit\u00e9 chez les hu\u00eetres juv\u00e9niles et adultes."
        },
        "keywords": {
            "en": "haplosporidium nelsoni, multinuclear sphere, MSX, reportable disease, aquatics, oysters, parasite",
            "fr": "haplosporidium nelsoni, sph\u00e8re X multinucl\u00e9e, MSX, maladie d\u00e9clarable, aquatique, hu\u00eetres, parasite"
        },
        "dcterms.subject": {
            "en": "crustaceans,inspection,infectious diseases,molluscs",
            "fr": "crustac\u00e9,inspection,maladie infectieuse,mollusque"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-03-06 13:13:07",
            "fr": "2013-03-06 13:13:07"
        },
        "modified": {
            "en": "2013-12-06",
            "fr": "2013-12-06"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Infection with Haplosporidium nelsoni",
        "fr": "Infection \u00e0 Haplosporidium nelsoni"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> causes multinuclear sphere\u00a0X (MSX) disease in cultured and wild American oysters.</p>\n<h2 class=\"h5\">What species of molluscs can be infected by <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>Each species of molluscs may have several common names but only one common name is listed.</p>\n<p>Species susceptible to <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> that exist in the natural environment in Canada include:</p>\n<ul>\n<li><span lang=\"la\"><i>Crassostrea gigas</i></span> (Pacific cupped oyster)</li>\n<li><span lang=\"la\"><i>Crassostrea virginica</i></span> (American oyster)</li>\n</ul>\n<h2 class=\"h5\">Is <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> a risk to human health?</h2>\n<p>No. The causal agent of <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> is not a risk to human health.</p>\n<h2 class=\"h5\">What are the signs of <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> causes disease in oysters. It is a cause of mortality in juvenile and adult oysters.</p>\n<ul>\n<li>Oysters that are <strong>over</strong> 2\u00a0years old are particularly affected.</li>\n<li>Mortality rates can reach 90% to 95% in older oysters.</li>\n</ul>\n<p>\u00a0Affected molluscs may exhibit any of the following signs:</p>\n<ul>\n<li>behaviour \t\t\t \n<ul>\n<li>valves slow to close when disturbed</li>\n</ul>\n</li>\n<li>appearance \t\t\t \n<ul>\n<li>decreased rate of growth</li>\n<li>no new shell growth</li>\n<li>extensive fouling along the inside peripheral left valve</li>\n<li>juvenile oysters may have pale digestive glands</li>\n<li>oysters appear thin and watery</li>\n<li>receding of the mantle</li>\n<li>raised yellow-brown spots on internal valve surfaces</li>\n</ul>\n</li>\n</ul>\n<h2 class=\"h5\">Is <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> found in Canada?</h2>\n<p>Yes. In Canada, <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> has been found in Nova Scotia and British Columbia.</p>\n<h2 class=\"h5\">How is <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> spread?</h2>\n<p>The complete life cycle of <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> is not known. Therefore, how infection is spread between molluscs is unknown.</p>\n<p>However, it is likely that people can spread <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> by moving infected molluscs and contaminated equipment.</p>\n<h2 class=\"h5\">How is <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> diagnosed?</h2>\n<p>Diagnosis of an infection with <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> requires the identification of the parasite through laboratory tests. Not all infected molluscs show signs of disease.</p>\n<h2 class=\"h5\">How is <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> treated?</h2>\n<p>There are no treatment options currently available for <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>.</p>\n<h2 class=\"h5\">What measures can be taken to prevent the introduction and spread of <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>If you frequently handle or work with molluscs, be aware of the clinical signs of <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>.</p>\n<p>Do not import live infected molluscs into Canada.</p>\n<ul>\n<li>An import permit is required from the Canadian Food Inspection Agency (CFIA) for certain species of molluscs as of December\u00a02011.</li>\n<li>People bringing molluscs into Canada should check other federal, provincial, and/or territorial requirements before entering the country.</li>\n</ul>\n<p>Do not introduce live molluscs from another country into the natural waters of Canada.</p>\n<ul>\n<li>People releasing molluscs into the natural waters or rearing facilities within Canada should check if federal or provincial and/or territorial permits are required.</li>\n</ul>\n<p>If you frequently handle or work with molluscs, be aware of where <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> occurs in your area.</p>\n<ul>\n<li>A federal, provincial and/or territorial permit or licence may be required to relocate molluscs within Canada.</li>\n</ul>\n<p>Do not use molluscs that were bought in a grocery store as bait for catching fish or other aquatic animals.</p>\n<p>Shells that are removed from molluscs or unwanted molluscs that you harvested or bought for your consumption should be disposed of in your municipal garbage.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> recommends that you do not visit Canadian aquaculture sites, zoos or aquariums for 14\u00a0days if you have travelled to another country and</p>\n<ul>\n<li>visited an aquaculture site, or</li>\n<li>had contact with wild molluscs.</li>\n</ul>\n<p>Wash and disinfect the footwear you wore to the site or when you had contact with wild molluscs. Also wash your clothing thoroughly and dry it at a high temperature.</p>\n<h2 class=\"h5\">What is done to protect Canadian aquatic animals from <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> is a reportable disease in Canada. This means that anyone who owns or works with aquatic animals, who knows of or suspects <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> in their molluscs, is required by law to notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p>If <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> is found in Canada, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would control its spread by implementing disease response activities. These may include</p>\n<ul>\n<li>controlling the movements of infected animals that people own or work with</li>\n<li>humanely destroying infected animals</li>\n<li>cleaning and disinfecting</li>\n</ul>\n<p>The control measures chosen would depend on the situation.</p>\n<h2 class=\"h5\">What do I do if I think molluscs that I am raising or keeping have <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>If you suspect that molluscs you are raising or keeping may have <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>, you are required under the <i>Health of Animals Act</i> to immediately notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> of your suspicion.</p>\r\n<h2 class=\"font-medium black\">How do I get more information</h2>\n<p>For more information about reportable diseases, visit the <a href=\"/animal-health/aquatic-animals/eng/1299155892122/1320536294234\">Aquatic Animal Health</a> page, contact your <a href=\"/eng/1300462382369/1365216058692\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Animal Health Office</a>,  or your <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area office:</p>\n\n<ul><li>Atlantic: 506-777-3939</li>\n<li>Quebec: 514-283-8888</li>\n<li>Ontario: 226-217-8555</li>\n<li>West: 587-230-2200</li></ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que <span lang=\"la\"><i><span lang=\"la\"><i>Haplosporidium nelsoni</i></span></i></span>?</h2>\n<p><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> cause la maladie MSX (<span lang=\"en\">Multinuclear Sphere\u00a0X</span>, sph\u00e8re\u00a0X multinucl\u00e9e) chez les hu\u00eetres am\u00e9ricaines cultiv\u00e9es et sauvages.</p>\n<h2 class=\"h5\">Quelles esp\u00e8ces de mollusques peuvent \u00eatre infect\u00e9es par <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>Chaque esp\u00e8ce de mollusque peut avoir plusieurs noms communs, mais la pr\u00e9sente liste n'en retient qu'un seul.</p>\n<p>Parmi les esp\u00e8ces sensibles \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> qui sont pr\u00e9sentes dans l'environnement naturel au Canada, on retrouve\u00a0:</p>\n<ul>\n<li><span lang=\"la\"><i>Crassostrea gigas</i></span> (hu\u00eetre creuse du Pacifique)</li>\n<li><span lang=\"la\"><i>Crassostrea virginica</i></span> (hu\u00eetre am\u00e9ricaine)</li>\n</ul>\n<h2 class=\"h5\"><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> pr\u00e9sente-t-il un risque pour la sant\u00e9 humaine?</h2>\n<p>Non. L'agent causal <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> ne pose aucun risque pour la sant\u00e9 humaine.</p>\n<h2 class=\"h5\">Quels sont les signes d'une infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> cause la maladie chez les hu\u00eetres. Il cause la mortalit\u00e9 chez les hu\u00eetres juv\u00e9niles et adultes.</p>\n<ul>\n<li>Les hu\u00eetres \u00e2g\u00e9es de plus de 2\u00a0ans sont particuli\u00e8rement touch\u00e9es.</li>\n<li>Le taux de mortalit\u00e9 peut atteindre 90 \u00e0 95\u00a0% chez les hu\u00eetres plus \u00e2g\u00e9es.</li>\n</ul>\n<p>Voici les signes que peuvent pr\u00e9senter les mollusques infect\u00e9s.</p>\n<ul>\n<li>Comportement \t\t\t \n<ul>\n<li>Valves qui se referment lentement lorsque les individus sont perturb\u00e9s</li>\n</ul>\n</li>\n<li>Apparence \t\t\t \n<ul>\n<li>Diminution du taux de croissance</li>\n<li>Aucune croissance de la coquille</li>\n<li>Encrassement important sur la p\u00e9riph\u00e9rie interne de la valve gauche</li>\n<li>Glande digestive parfois p\u00e2le chez les hu\u00eetres juv\u00e9niles</li>\n<li>Sujets minces et aqueux en apparence</li>\n<li>R\u00e9tr\u00e9cissement du manteau</li>\n<li>Taches brun-jaune sur\u00e9lev\u00e9es sur la surface interne des valves.</li>\n</ul>\n</li>\n</ul>\n<h2 class=\"h5\"><span lang=\"la\"><i>Haplosporidium nelsoni</i></span> est-il pr\u00e9sent au Canada?</h2>\n<p>Oui. Au Canada, on a d\u00e9tect\u00e9 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> en Nouvelle-\u00c9cosse et en Colombie-Britannique.</p>\n<h2 class=\"h5\">Comment <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> se propage-t-il?</h2>\n<p>On ne conna\u00eet pas le cycle vital complet de <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>. Par cons\u00e9quent, on ne sait pas comment l'infection se propage d'un mollusque \u00e0 l'autre.</p>\n<p>Toutefois, il est probable que les \u00eatres humains puissent propager <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> en d\u00e9pla\u00e7ant des mollusques et de l'\u00e9quipement contamin\u00e9.</p>\n<h2 class=\"h5\">Comment l'infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> est-elle diagnostiqu\u00e9e?</h2>\n<p>Pour diagnostiquer une infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>, il faut effectuer des \u00e9preuves de laboratoire. Les mollusques infect\u00e9s ne pr\u00e9senteront pas tous des signes de la maladie.</p>\n<h2 class=\"h5\">Comment l'infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> est-elle trait\u00e9e?</h2>\n<p>\u00c0 l'heure actuelle, il n'existe pas de traitement pour l'infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>.</p>\n<h2 class=\"h5\">Quelles mesures peut-on prendre pour pr\u00e9venir l'introduction et la propagation de <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>Si vous travaillez souvent avec des mollusques, ou si vous les manipulez, sachez reconna\u00eetre les sympt\u00f4mes de l'infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>.</p>\n<p>N'importez pas de mollusques infect\u00e9s vivants au Canada.</p>\n<ul>\n<li>Depuis d\u00e9cembre\u00a02011, un permis d'importation est requis par l'Agence canadienne d'inspection des aliments (ACIA) pour certaines esp\u00e8ces de mollusques.</li>\n<li>Les personnes qui rapportent des mollusques au Canada devraient v\u00e9rifier les autres exigences f\u00e9d\u00e9rales, provinciales et territoriales avant d'importer des poissons au Canada ou d'entrer au Canada avec ces mollusques.</li>\n</ul>\n<p>N'introduisez pas de mollusques vivants provenant d'un autre pays ou d'une autre province dans les plans d'eau naturels du Canada.</p>\n<ul>\n<li>Les personnes qui rel\u00e2chent des mollusques dans des plans d'eau naturels ou des \u00e9tablissements de grossissement du Canada devraient v\u00e9rifier si elles ont besoin d'un permis f\u00e9d\u00e9ral ou provincial ou territorial.</li>\n</ul>\n<p>Si vous travaillez souvent avec des mollusques, ou si vous les manipulez, soyez inform\u00e9 des endroits o\u00f9 s\u00e9vit l'infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> dans votre r\u00e9gion.</p>\n<ul>\n<li>Le d\u00e9placement de mollusques d'un endroit \u00e0 l'autre au Canada peut n\u00e9cessiter une licence ou un permis f\u00e9d\u00e9ral, provincial ou territorial.</li>\n</ul>\n<p>N'utilisez pas de mollusques achet\u00e9s au supermarch\u00e9 comme app\u00e2ts pour attraper des poissons ou d'autres animaux aquatiques.</p>\n<p>Les coquilles retir\u00e9es des mollusques ou les mollusques r\u00e9colt\u00e9s ou achet\u00e9s aux fins de consommation dont vous voulez vous d\u00e9barrasser doivent \u00eatre jet\u00e9s dans vos ordures.\u00a0</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> vous recommande de ne pas visiter de sites aquacoles, de zoos ou d'aquariums canadiens pendant 14\u00a0jours si vous avez voyag\u00e9 dans un autre pays et si vous\u00a0:</p>\n<ul>\n<li>avez visit\u00e9 un site aquacole;</li>\n<li>\u00eates entr\u00e9 en contact avec des mollusques sauvages.</li>\n</ul>\n<p>Lavez et d\u00e9sinfectez les chaussures que vous portiez pour vous rendre au site ou lorsque vous \u00eates entr\u00e9 en contact avec des mollusques sauvages. Lavez \u00e9galement soigneusement vos v\u00eatements et s\u00e9chez-les \u00e0 temp\u00e9rature \u00e9lev\u00e9e.</p>\n<h2 class=\"h5\">Quelles sont les mesures prises pour prot\u00e9ger les animaux aquatiques du Canada contre <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>Au Canada, l'infection \u00e0 <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> est une maladie d\u00e9clarable. Par cons\u00e9quent, quiconque poss\u00e8de des animaux aquatiques ou travaille avec de tels animaux et soup\u00e7onne ou d\u00e9c\u00e8le la pr\u00e9sence de <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> chez les animaux qu'il poss\u00e8de ou avec lesquels il travaille est tenu par la loi d'en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Si on d\u00e9c\u00e8le la pr\u00e9sence de <span lang=\"la\"><i>Haplosporidium nelsoni</i></span> au Canada, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> contr\u00f4lera sa propagation en mettant en \u0153uvre des activit\u00e9s d'intervention en cas de maladie, par exemple :</p>\n<ul>\n<li>contr\u00f4ler les d\u00e9placements des animaux infect\u00e9s que les personnes poss\u00e8dent ou avec lesquels elles travaillent;</li>\n<li>d\u00e9truire sans cruaut\u00e9 les animaux infect\u00e9s;</li>\n<li>nettoyer et d\u00e9sinfecter.</li>\n</ul>\n<p>Les mesures de contr\u00f4le retenues d\u00e9pendront de la situation.</p>\n<h2 class=\"h5\">Que dois-je faire si je crois que les mollusques que j'\u00e9l\u00e8ve ou que je garde sont infect\u00e9s par <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>?</h2>\n<p>Si vous avez des motifs de soup\u00e7onner qu'un mollusque que vous \u00e9levez ou gardez est infect\u00e9 par <span lang=\"la\"><i>Haplosporidium nelsoni</i></span>, vous devez en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> imm\u00e9diatement, conform\u00e9ment \u00e0 la <i>Loi sur la sant\u00e9 des animaux</i>.</p>\r\n<h2 class=\"font-medium black\">Comment puis-je obtenir davantage de renseignements</h2>\n<p>Pour de plus amples renseignements sur les maladies d\u00e9clarables, consultez la page <a href=\"/sante-des-animaux/animaux-aquatiques/fra/1299155892122/1320536294234\">Sant\u00e9 des animaux aquatiques</a>,  communiquez avec votre <a href=\"/fra/1300462382369/1365216058692\">bureau local de sant\u00e9 des animaux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou  le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de votre centre op\u00e9rationnel\u00a0: </p>\n<ul><li>Atlantique : 506-777-3939</li>\n<li>Qu\u00e9bec : 514-283-8888</li>\n<li>Ontario : 226-217-8555</li>\n<li>Ouest : 587-230-2200</li></ul>\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}