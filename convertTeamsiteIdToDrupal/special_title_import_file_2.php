<?php

use Drush\Drush;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

global $import_override;
$import_override = TRUE;

global $importedFileNameArray;
$importedFileNameArray = array();

 print_r( $argv );
 echo $_SERVER['argv'][3] . "\n";
$export_root = $_SERVER['argv'][3];
echo $export_root . " =EXPORT_ROOT **********************************************************************************************************\n";

$count = 0;
$fileCount = 0;
$successCount = 0;
$failCount = 0;

$directory = 'legacy/special_title825x200.jpg';
$fullpath = DRUPAL_ROOT . '/sites/default/files/' . $directory; 
 Drush::output()->writeln("directory: " . $directory);
  echo 'public://' . $directory;
  if ($file = \Drupal\file\Entity\File::create(['uri' => 'public://' . $directory, 'status' => 1, 'uid' => 1])) {
    echo 'TEST ??? public://' . $directory;
    $existing_files = \Drupal::entityTypeManager()
      ->getStorage('file')
      ->loadByProperties([
      'uri' => 'public://' . $directory,
    ]);
    if (count($existing_files)) {
      $existing = reset($existing_files);
      $file->fid = $existing->id();
      $file->setOriginalId($existing->id());
      $file->setFilename($existing->getFilename());
      $file->save();
    }
    else {
      $file->save();
    }
  }
  $database = \Drupal::database();
  $sql = "select fid, uuid from file_managed where uri like :filename_pattern";
  $result = $database->query($sql, [':filename_pattern' => '%pecial_title825x200.jpg']);
  $fid = 0;
  if ($result) {
    while ($row = $result->fetchAssoc()) {
      if (!isset($row['fid']) || is_null($row['fid'])) {
        return FALSE;
      }
      $uuid = $row['uuid'];
      $fid = $row['fid'];
    }
  }
  if ($fid) {
    $file = \Drupal\file\Entity\File::load($fid);
  }

  //$file = file_save_data($file_data, 'public://' . $directory, FILE_EXISTS_REPLACE);
  // print_r($directory);
  echo "\ntest\n";
  echo "\nfullpath $fullpath\n";
  echo "\ndirectory $directory\n";
  // print_r($file);

  $path_parts = pathinfo($fullpath);
  $filename = $path_parts['basename'];
  $fileExtension = $path_parts['extension'];
  $bundleType = '';
  switch (strtolower($fileExtension)) {
       case "doc":
       case "docx":
       case "json":
       case "pdf":
       case "ppt":
       case "xls":
       case "xlsm":
       case "xlsx":
          $bundleType = 'document';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            'title'       => $filename,
            'field_document' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
          break;
       case "gif":
       case "jfif":
       case "jpg":
       case "jpeg":
       case "png":
       case "tif":
          $bundleType = 'image';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            /*'alt'       => $filename,*/ // WCAG says no , filename should not be same as title attribute. agrcms/d8#179 gitlab.com
            'image' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
          break;
       case "mp3":
          $bundleType = 'audio_file';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            /*'title'       => $filename,*/ // WCAG says no , filename should not be same as title attribute. agrcms/d8#179 gitlab.com.
            'field_media_audio_file' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
       case "mp4":
       case "wmv":
          $bundleType = 'video_file';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            /*'title'       => $filename,*/ // WCAG says no , filename should not be same as title attribute. agrcms/d8#179 gitlab.com.
            'field_media_video_file' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
          break;


  }


