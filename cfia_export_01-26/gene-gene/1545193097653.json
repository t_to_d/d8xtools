{
    "dcr_id": "1545193097653",
    "title": {
        "en": "Controls on contamination in red meat and poultry establishments",
        "fr": "Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge et les \u00e9tablissements de volaille"
    },
    "html_modified": "2024-01-26 2:24:28 PM",
    "modified": "2019-01-09",
    "issued": "2018-12-21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_meat_food_control_contamination_1545193097653_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_meat_food_control_contamination_1545193097653_fra"
    },
    "ia_id": "1545193144984",
    "parent_ia_id": "1523875902549",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Making food - Slaughter",
        "fr": "Production d'aliments - Abattage"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1523875902549",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Controls on contamination in red meat and poultry establishments",
        "fr": "Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge et les \u00e9tablissements de volaille"
    },
    "label": {
        "en": "Controls on contamination in red meat and poultry establishments",
        "fr": "Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge et les \u00e9tablissements de volaille"
    },
    "templatetype": "content page 1 column",
    "node_id": "1545193144984",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1523875902268",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-guidance-by-commodity/meat-products-and-food-animals/controls-on-contamination/",
        "fr": "/exigences-et-documents-d-orientation-relatives-a-c/produits-de-viande-et-animaux-pour-alimentation-hu/controles-de-la-contamination/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Controls on contamination in red meat and poultry establishments",
            "fr": "Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge et les \u00e9tablissements de volaille"
        },
        "description": {
            "en": "This document provides guidance to the licence holders on different controls that should be used to identify and control fecal, ingesta and milk contamination, as applicable, in red meat and poultry establishments.",
            "fr": "Lignes directrices sur les diff\u00e9rents contr\u00f4les qui devraient \u00eatre utilis\u00e9s pour identifier et contr\u00f4ler la contamination f\u00e9cale, par de l'ingesta et du lait, lorsque applicable, dans les \u00e9tablissements de viande rouge et de volaille."
        },
        "keywords": {
            "en": "controls on contamination, controls, contamination, red meat establishments",
            "fr": "Contr\u00f4les de la contamination, Contr\u00f4les, contamination, \u00e9tablissements de viande rouge"
        },
        "dcterms.subject": {
            "en": "food,agri-food industry,food inspection,food safety,meat",
            "fr": "aliment,industrie agro-alimentaire,inspection des aliments,salubrit\u00e9 des aliments,viande"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-12-21",
            "fr": "2018-12-21"
        },
        "modified": {
            "en": "2019-01-09",
            "fr": "2019-01-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Controls on contamination in red meat and poultry establishments",
        "fr": "Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge et les \u00e9tablissements de volaille"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\n<!--<div class=\"alert alert-info\">\n-->\n<h2>On this page</h2>\n<ul class=\"list-unstyled mrgn-lft-0\">\n<li><a href=\"#a1\">Controls on contamination in red meat establishments</a></li>\n<li><a href=\"#a2\">Controls on contamination in poultry establishments</a></li>\n</ul>\n\n\n\n<h2 id=\"a1\">Controls on contamination in red meat establishments</h2>\n<p>A licence holder cannot identify as edible any meat product that is contaminated. Possible contamination of the meat products is prevented by ensuring hygienic procedures during dressing and evisceration. When contamination inadvertently occurs, it will be promptly identified and hygienically removed by the operator, as described in the <a href=\"/food-guidance-by-commodity/meat-products-and-food-animals/standards-to-identify-a-meat-product-as-edible/eng/1526680768561/1526680768858\">Standards to identify a meat product as edible</a>. Please consult <a href=\"/food-guidance-by-commodity/meat-products-and-food-animals/dressing-procedures-and-edible-parts/eng/1544042033189/1544042477514\">Dressing procedures and Preparation of edible parts</a> to ensure that you implement the best hygienic practices to achieve this. </p>\n<p>Considering that the appearance of feces, ingesta and milk is affected by many factors such as the type of animal (ruminant vs non ruminant), diet, age of animal, the following descriptions are guidelines and are not exclusive.</p>\n<p> Size is relatively unimportant in identifying fecal, ingesta or milk contamination, however, defects that are less than 3.0 mm in their greatest dimension can be difficult to characterize with certainty when using only the naked eye. If the observer is not able to clearly identify a defect as being of gastrointestinal origin or milk, it may be classified as extraneous or foreign material.</p>\n<p> Foreign material will be identified as feces or ingesta when both color and texture is consistent with feces or ingesta. The color of fecal or ingesta contamination may vary between yellow, green, or brown, in the case of cattle; tan to dark brown, in the case of swine; and brown to black, in the case of sheep and goats. </p>\n<p> Fecal or ingesta contamination can have a fibrous or plant-like texture and may contain plant-like material. Sheep and goat feces and ingesta may be tarry. Swine feces and ingesta may include identifiable grain particles. </p>\n<p> Milk may be identified based on two factors: color and consistency. The colour of milk ranges from clear to white to light yellow. The consistency of milk ranges from watery to ropy or curdy. Milk, if present, tends to be found on the midline, during or after removal of mammary glands (udder).</p>\n<p> Fecal, ingesta and milk may contain pathogens that render meat products adulterated. Accordingly, operators must demonstrate, through their Preventive Control Plan (PCP) and subsequently by carcass evaluation, that carcasses are free of visible fecal, ingesta and milk (as applicable to the species slaughtered) after final trimming but prior to final carcass washing. Licence holders that operate under the <a href=\"/food-guidance-by-commodity/meat-products-and-food-animals/msip-hog/eng/1546630264657/1546630409389\">Modernised Slaughter Inspection Program (MSIP) Hog</a> will also need to ensure that they meet the requirements of that program.</p>\n<p>In alignment with <a href=\"/preventive-controls/preventive-control-plans/regulatory-requirements/eng/1526502822129/1526502868878\">PCP requirements</a> related to critical control points, in the event that fecal, ingesta or milk is detected at the evaluation, all carcasses produced since the last successful evaluation must be identified by the operator and subjected to procedures that will ensure the identified carcasses are free of visible fecal, ingesta and milk.</p>\n<p> Also upon detection of visible fecal, ingesta or milk during this evaluation, the operator must investigate the root cause of the deviation and implement effective corrective actions. Records of these evaluations (including detection of fecal, ingesta or milk); root cause investigations and their corrective actions; and procedures to identify and return potentially affected carcasses to freedom from visible fecal, ingesta and milk defects must be maintained in an auditable format. </p>\n<p> The operator should re-evaluate their Preventive Control Plan to incorporate carcass evaluation (after final trimming but prior to final carcass washing) and fecal, ingesta and milk control procedures, as appropriate.</p>\n\n\n<h2 id=\"a2\">Controls on contamination in poultry establishments</h2>\n<p>Contaminated carcass, cavity and viscera are not acceptable for human consumption and have to be:</p>\n<ul>\n<li>removed from the line for defect removal, or</li>\n<li>removed online using an approved on-line procedure, or</li>\n<li>rejected.</li>\n</ul>\n\n<p>Contamination can come from five (5) sources:</p>\n\n<ol>\n\n<li>Fecal contamination: Any identifiable stain and/or material determined to be from the lower gastrointestinal tract. There is zero tolerance for fecal contamination.\n\n<ul class=\"list-unstyled\">\n<li>The color of feces may range from varying shades of yellow to green, brown, and white.</li>\n<li>The consistency of feces is characteristically semi-solid to a paste.</li>\n<li>The composition of feces may or may not include plant material.</li>\n</ul>\n</li>\n<li>Ingesta: Identifiable stain and/or dry particles and/or liquid (aggregate) covering a minimum area &gt; 5\u00a0mm (internal and external). The undigested liquid or solid contents of the crop, gizzard or proventriculus. Dry and localized ingesta covering an area of a dime or less or a few isolated grains will not be counted as a defect. The colour of the ingesta may vary depending on diet, and can be green, yellow, or brown. The consistency may vary from solid to granular to slimy.</li>\n<li>Bile contamination: Caused by perforation of the gallbladder during evisceration which results in a green discolouration of affected tissue.</li>\n<li>Extraneous material: Grease stains or other foreign material which cannot be removed on-line or off-line.</li>\n<li> Intestine / cloaca: Intestine / Cloaca: Refers to a length of intestine/cloaca attached to the carcass or inside the cavity.</li>\n</ol>\n \n \n<p>Contamination in poultry establishments usually happens during the evisceration process. It has different causes:</p>\n\n<ul>\n<li>diarrhea</li>\n<li>improper fasting prior to slaughter</li>\n<li>poor work techniques (manual evisceration)</li>\n<li>poor adjustment of the evisceration equipment (mechanical evisceration)</li>\n<li>Variation in bird size within the flock</li>\n</ul>\n\n<p>Licence holders must ensure their Preventive Control Plan addresses the following:</p>\n\n<ol class=\"lst-lwr-alpha\">\n<li>all the contamination from the carcasses, carcass parts and viscera is removed</li>\n<li>if the contamination is not removed, the carcasses, carcass parts and viscera are condemned/rejected</li>\n<li>investigation of the root cause of the deviation and implementation of effective corrective actions</li>\n<li>if a disease is detected during defect detection, the carcass and viscera are submitted for secondary examination</li>\n</ol>\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>Sur cette page</h2>\n<ul class=\"list-unstyled mrgn-lft-0\">\n<li><a href=\"#a1\">Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge</a></li>\n<li><a href=\"#a2\">Contr\u00f4les de la contamination dans les \u00e9tablissements de volaille</a></li>\n</ul>\n\n\n<h2 id=\"a1\">Contr\u00f4les de la contamination dans les \u00e9tablissements de viande rouge</h2>\n\n<p>Un titulaire de licence ne peut pas identifier comme comestible tout produit de viande qui est  contamin\u00e9. La contamination possible des produits de viande est \u00e9vit\u00e9e en assurant l'application de proc\u00e9dures hygi\u00e9niques lors de l'habillage et de l'\u00e9visc\u00e9ration. Lorsque qu'une contamination se produit involontairement, celle-ci sera rapidement identifi\u00e9e et retir\u00e9e de fa\u00e7on hygi\u00e9nique par l'exploitant, tel que d\u00e9crit dans les <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-de-viande-et-animaux-pour-alimentation-hu/normes-pour-designer-un-produit-de-viande-comestib/fra/1526680768561/1526680768858\">Normes pour d\u00e9signer un produit de viande comestible</a>. Veuillez consulter les <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-de-viande-et-animaux-pour-alimentation-hu/procedures-d-habillage-de-parties-comestibles/fra/1544042033189/1544042477514\">Proc\u00e9dures d'habillage et de pr\u00e9paration de parties comestibles</a> afin de vous assurer de mettre en \u0153uvre les meilleures pratiques hygi\u00e9niques pour y parvenir. </p>\n<p>\u00c9tant donn\u00e9 que l'apparence de la mati\u00e8re f\u00e9cale, de l'ingesta et du lait est affect\u00e9e par plusieurs facteurs dont le type d'animal (ruminant vs non-ruminant), le r\u00e9gime alimentaire, l'\u00e2ge de l'animal, les descriptions qui suivent sont des guides et ne sont pas exclusifs.</p>\n<p>La taille est relativement peu importante dans l'identification de la contamination f\u00e9cale, de l'ingesta ou du lait, cependant des d\u00e9fauts inf\u00e9rieurs \u00e0 3,0 mm dans leur plus grande dimension peuvent \u00eatre difficiles \u00e0 caract\u00e9riser avec certitude \u00e0 l'\u0153il nu. Si l'observateur n'est pas capable d'identifier clairement un d\u00e9faut comme \u00e9tant d'origine gastro-intestinale ou li\u00e9 au lait, il peut le classer en tant que mati\u00e8re \u00e9trang\u00e8re.</p>\n<p>Les mati\u00e8res \u00e9trang\u00e8res seront identifi\u00e9es comme de la mati\u00e8re f\u00e9cale ou de l'ingesta lorsque la couleur et la texture sont compatibles avec la mati\u00e8re f\u00e9cale ou l'ingesta. Dans le cas des bovins, la couleur de la contamination  par de la mati\u00e8re f\u00e9cale ou par de l'ingesta est jaune, verte ou brune; dans le cas des porcs, c'est beige \u00e0 brun fonc\u00e9; et dans le cas des moutons et des ch\u00e8vres, c'est brun \u00e0 noir. </p>\n<p>La contamination par de la mati\u00e8re f\u00e9cale ou par de l'ingesta peut avoir une texture fibreuse ou ressemblant \u00e0 des v\u00e9g\u00e9taux et peut contenir du mat\u00e9riel qui ressemble \u00e0 des v\u00e9g\u00e9taux. La mati\u00e8re f\u00e9cale et l'ingesta de moutons et de ch\u00e8vres peut para\u00eetre comme du goudron. La mati\u00e8re f\u00e9cale et l'ingesta de porcs peut contenir des particules identifiables de grains.</p>\n<p> Le lait peut \u00eatre identifi\u00e9 en fonction de deux facteurs: la couleur et la consistance. La couleur du lait varie du clair au blanc au jaune p\u00e2le. La consistance du lait varie d'aqueux \u00e0 visqueux \u00e0 caill\u00e9. Le lait, si pr\u00e9sent, a tendance \u00e0 se retrouver sur la ligne m\u00e9diane, pendant ou apr\u00e8s le retrait des glandes mammaires (pis).</p>\n<p>La mati\u00e8re f\u00e9cale, l'ingesta et le lait peuvent contenir des agents pathog\u00e8nes qui falsifient les produits de viande En cons\u00e9quence, les exploitants doivent d\u00e9montrer, au moyen de leur Plan de Contr\u00f4le Pr\u00e9ventif (PCP) et par la suite gr\u00e2ce \u00e0 l'\u00e9valuation des carcasses, que les carcasses sont exemptes de toute contamination visible par de la mati\u00e8re f\u00e9cale, de l'ingesta et du lait (tels qu'applicable aux esp\u00e8ces abattus) apr\u00e8s le parage finale de la carcasse, mais avant le lavage final. Les titulaires de licence qui op\u00e8rent sous le <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-de-viande-et-animaux-pour-alimentation-hu/pmia-porc/fra/1546630264657/1546630409389\">Programme modernis\u00e9 de l'inspection de l'abattage (PMIA) Porc</a> devront aussi veiller \u00e0 rencontrer les exigences de ce programme.</p>\n<p>En lien avec les <a href=\"/controles-preventifs/plans-de-controle-preventif/exigences-reglementaires/fra/1526502822129/1526502868878\">exigences du PCP</a> relatifs aux points de contr\u00f4le critiques, dans le cas o\u00f9 une contamination par de la mati\u00e8re f\u00e9cale, de l'ingesta ou du lait est d\u00e9tect\u00e9e lors de l'\u00e9valuation, toutes les carcasses produites depuis la derni\u00e8re \u00e9valuation r\u00e9ussie doivent \u00eatre identifi\u00e9es par l'exploitant et assujetties \u00e0 des proc\u00e9dures qui garantiront que les carcasses identifi\u00e9es sont exemptes de contamination visible par de la mati\u00e8re f\u00e9cale, de l'ingesta et du lait.</p>\n<p>Lors de la d\u00e9tection de contamination visible par de la mati\u00e8re f\u00e9cale, de l'ingesta ou du lait au cours de cette \u00e9valuation, l'exploitant doit \u00e9galement enqu\u00eater sur la cause profonde de l'\u00e9cart et mettre en \u0153uvre des actions correctives efficaces. Les dossiers de ces \u00e9valuations (y compris la d\u00e9tection de contamination par de la mati\u00e8re f\u00e9cale, de l'ingesta ou du lait); les enqu\u00eates sur les causes profondes et leurs actions correctives; ainsi que les proc\u00e9dures pour identifier et rendre les carcasses potentiellement affect\u00e9es exemptes de contamination visible par de la mati\u00e8re f\u00e9cale, de l'ingesta et du lait doivent \u00eatre maintenus dans un format v\u00e9rifiable.</p>\n<p>L'exploitant devrait r\u00e9\u00e9valuer son PCP afin d'y incorporer l'\u00e9valuation des carcasses (apr\u00e8s le parage final mais avant le lavage final des carcasses) ainsi que les proc\u00e9dures de contr\u00f4le pour la contamination par de la mati\u00e8re f\u00e9cale, de l'ingesta et du lait, le cas \u00e9ch\u00e9ant.</p>\n\n\n<h2 id=\"a2\">Contr\u00f4les de la contamination dans les \u00e9tablissements de volaille</h2>\n<p>Les carcasses, cavit\u00e9s et visc\u00e8res contamin\u00e9s ne sont pas acceptables pour l'alimentation humaine et doivent \u00eatre\u00a0: </p>\n<ul>\n<li>retir\u00e9s de la cha\u00eene pour l'enl\u00e8vement du d\u00e9faut, ou</li>\n<li>enlev\u00e9s sur la cha\u00eene en utilisant une proc\u00e9dure approuv\u00e9e, ou</li>\n<li>rejet\u00e9s.</li>\n</ul>\n\n<p>La contamination peut provenir de cinq (5) sources:</p>\n\n<ol>\n\n<li>Contamination f\u00e9cale : Comprend toute tache et/ou mati\u00e8re qui peut \u00eatre identifi\u00e9e comme provenant du tractus gastro-intestinal inf\u00e9rieur (intestins). C'est tol\u00e9rance z\u00e9ro pour la contamination f\u00e9cal.\n\n<ul class=\"list-unstyled\">\n<li>La couleur de la mati\u00e8re \u00e9cale peut varier entre des tons de jaune, vert, brun et blanc.</li>\n<li>La consistance de la mati\u00e8re f\u00e9cale est de fa\u00e7on caract\u00e9ristique semi-solide \u00e0 p\u00e2teux.</li>\n<li>La composition de la mati\u00e8re f\u00e9cale peut inclure ou non du mat\u00e9riel v\u00e9g\u00e9tal.</li>\n</ul>\n</li>\n \n<li>Ingesta : Tache et/ou particules s\u00e8ches et/ou liquide (agr\u00e9gats) couvrant une superficie minimale de &gt;5mm (interne et externe). Comprend le contenu non dig\u00e9r\u00e9 liquide ou solide du jabot, du g\u00e9sier ou du proventricule. Des ingesta secs et localis\u00e9s ne d\u00e9passant pas la dimension d'une pi\u00e8ce de dix cents ou quelques grains isol\u00e9s ne seront pas compt\u00e9s comme un d\u00e9faut. La couleur des ingesta varie en fonction du r\u00e9gime alimentaire et peut \u00eatre verte, jaune ou brune. La consistance peut varier de solide \u00e0 granulaire \u00e0 gluante.</li>\n<li>Contamination par la bile: Caus\u00e9e par la perforation de la v\u00e9sicule biliaire durant l'\u00e9visc\u00e9ration, cette contamination donne aux tissus affect\u00e9s une couleur verte.</li>\n<li>Mati\u00e8res \u00e9trang\u00e8res : taches de graisse ou autres mati\u00e8res \u00e9trang\u00e8res qui ne peuvent pas \u00eatre enlev\u00e9es sur la cha\u00eene ou hors cha\u00eene.</li>\n<li>Intestin / cloaque : Toute portion identifiable de la partie terminale du tractus intestinal/cloaque qui demeure attach\u00e9e \u00e0 la carcasse ou \u00e0 l'int\u00e9rieur de la cavit\u00e9.</li>\n</ol>\n\n\n<p>Dans les \u00e9tablissements de volaille, la contamination se produit principalement durant le processus d'\u00e9visc\u00e9ration. Ses causes sont les suivantes :</p>\n<ul>\n<li>diarrh\u00e9e</li>\n<li>je\u00fbne inad\u00e9quat avant l'abattage</li>\n<li>mauvaise technique de travail (\u00e9visc\u00e9ration manuelle)</li>\n<li>mauvais r\u00e9glage des appareils servant \u00e0 l'\u00e9visc\u00e9ration (\u00e9visc\u00e9ration m\u00e9canique)</li>\n<li>variation de la taille des volailles dans le troupeau</li>\n</ul>\n\n<p>Les titulaires de licence doivent s'assurer que leur PCP traite les points suivants:</p>\n<ol class=\"lst-lwr-alpha\">\n<li>toute contamination des carcasses, des parties et des visc\u00e8res est enlev\u00e9e</li>\n<li>si la contamination n'est pas enlev\u00e9e, les carcasses, les parties de carcasses et les visc\u00e8res sont condamn\u00e9s/rejet\u00e9s</li>\n<li>l'enqu\u00eate sur la cause profonde de la d\u00e9viation et la mise en \u0153uvre d'actions correctives efficaces</li>\n<li>si une maladie est d\u00e9tect\u00e9e au cours de la d\u00e9tection de d\u00e9fauts, la carcasse et les visc\u00e8res sont soumis \u00e0 un examen secondaire.</li>\n</ol>\n\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}