{
    "dcr_id": "1475065583538",
    "title": {
        "en": "Export of Dogs and Cats to the United States of America",
        "fr": "Exportation de chiens et de chats aux \u00c9tats-Unis d'Am\u00e9rique"
    },
    "html_modified": "2024-02-13 9:51:45 AM",
    "modified": "2021-06-01",
    "issued": "2016-09-30",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_certif_dogs_cats_1475065583538_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_certif_dogs_cats_1475065583538_fra"
    },
    "ia_id": "1475065648302",
    "parent_ia_id": "1321281361100",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1321281361100",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Export of Dogs and Cats to the United States of America",
        "fr": "Exportation de chiens et de chats aux \u00c9tats-Unis d'Am\u00e9rique"
    },
    "label": {
        "en": "Export of Dogs and Cats to the United States of America",
        "fr": "Exportation de chiens et de chats aux \u00c9tats-Unis d'Am\u00e9rique"
    },
    "templatetype": "content page 1 column",
    "node_id": "1475065648302",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1321265624789",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/pets/united-states-of-america/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/animaux-de-compagnie/etats-unis-d-amerique/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Export of Dogs and Cats to the United States of America",
            "fr": "Exportation de chiens et de chats aux \u00c9tats-Unis d'Am\u00e9rique"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) has no role in the export of dogs and cats to the United States of America (USA), including Hawaii.",
            "fr": "L\u2019Agence canadienne d\u2019inspection des aliments (ACIA) ne joue aucun r\u00f4le dans l\u2019exportation des chiens et chats aux \u00c9tats-Unis d\u2019Am\u00e9rique, incluant Hawaii."
        },
        "keywords": {
            "en": "dogs, cats, pets, rabies, requirements, quarantine",
            "fr": "chiens, chats, animaux de compagnie, rage, exigences, quarantine"
        },
        "dcterms.subject": {
            "en": "animal health,exports",
            "fr": "sant\u00e9 animale,exportation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne de l'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-09-30",
            "fr": "2016-09-30"
        },
        "modified": {
            "en": "2021-06-01",
            "fr": "2021-06-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Export of Dogs and Cats to the United States of America",
        "fr": "Exportation de chiens et de chats aux \u00c9tats-Unis d'Am\u00e9rique"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The Canadian Food Inspection Agency (CFIA) has no role in the export of dogs and cats to the United States of America (USA), including Hawaii.  The completion and issuance of any required documentation (e.g. vaccination certificate, export health certificate) is the responsibility of a licensed Canadian veterinarian and official endorsement (i.e. stamp and signature) by a CFIA veterinarian is not required.</p>\n\n<p>All the information for exporting dogs and cats to the USA is available on the <a href=\"https://www.aphis.usda.gov/aphis/pet-travel/bring-pet-into-the-united-states\">United States Department of Agriculture Animal and Plant Health Inspection Service (USDA APHIS)</a> website.</p>\n\n<p>Dogs and cats entering the USA may also be subject to requirements from other federal agencies and state authorities. It is the exporter\u2019s responsibility to ensure that animals comply with all federal and state requirements.  If you have additional questions about exporting a dog or cat to the USA, please contact the appropriate federal and/or state authorities in the USA.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'Agence canadienne d'inspection des aliments (ACIA) ne joue aucun r\u00f4le dans l'exportation des chiens et chats aux \u00c9tats-Unis d'Am\u00e9rique, incluant Hawaii. Toute documentation n\u00e9cessaire pour l'exportation comme les certificats de vaccination et d'exportation doivent \u00eatre compl\u00e9t\u00e9s par un v\u00e9t\u00e9rinaire canadien licenci\u00e9. L'endossement official (estampillage et signature) par un v\u00e9t\u00e9rinaire de l'ACIA n'est pas requis.</p>\n<p>Toute l'information pour l'exportation de chiens et de chats aux \u00c9tats-Unis d'Am\u00e9rique est disponible en anglais seulement sur le site web du <a href=\"https://www.aphis.usda.gov/aphis/pet-travel/bring-pet-into-the-united-states\">Service d'inspection sanitaire des animaux et des plantes (APHIS) du D\u00e9partement d'Agriculture des \u00c9tats-Unis (USDA) (en anglais seulement)</a></p>\n\n<p>Les chiens et chats export\u00e9s aux \u00c9tats-Unis peuvent devoir rencontrer des conditions additionnelles impos\u00e9es par les autorit\u00e9s d'autres agences f\u00e9d\u00e9rales ou d'\u00e9tats. C'est la responsabilit\u00e9 de l'exportateur de s'assurer que les animaux satisfont \u00e0 toutes ces conditions. Pour toute question additionnelle se rapportant \u00e0 l'exportation de chiens et chats aux \u00c9tats-Unis, veuillez contacter les autorit\u00e9s am\u00e9ricaines f\u00e9d\u00e9rales et/ou d'\u00e9tat appropri\u00e9es.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}