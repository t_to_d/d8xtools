{
    "dcr_id": "1323672020149",
    "title": {
        "en": "Consultative groups",
        "fr": "Groupes consultatifs"
    },
    "html_modified": "2024-02-13 9:47:08 AM",
    "modified": "2021-05-31",
    "issued": "2015-03-31 10:41:29",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_agen_consult_groups_1323672020149_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_agen_consult_groups_1323672020149_fra"
    },
    "ia_id": "1323672093126",
    "parent_ia_id": "1566414728048",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1566414728048",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Consultative groups",
        "fr": "Groupes consultatifs"
    },
    "label": {
        "en": "Consultative groups",
        "fr": "Groupes consultatifs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1323672093126",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1566414558282",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/consultations-and-engagement/consultative-groups/",
        "fr": "/a-propos-de-l-acia/transparence/consultations-et-participation/groupes-consultatifs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Consultative groups",
            "fr": "Groupes consultatifs"
        },
        "description": {
            "en": "The Expert Advisory Committee provides the Canadian Food Inspection Agency (CFIA) with objective professional and technical advice on key issues related to the CFIA's three business lines: food safety, animal health and plant health.",
            "fr": "Le Comit\u00e9 consultatif d'experts donne \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) des conseils techniques et professionnels objectifs sur des dossiers cl\u00e9s li\u00e9s \u00e0 ses trois secteurs d'activit\u00e9\u00a0: la salubrit\u00e9 des aliments, la sant\u00e9 des animaux et la protection des v\u00e9g\u00e9taux."
        },
        "keywords": {
            "en": "Consumer Association Roundtable, Expert Advisory Comittee, Ministerial Advisory Board, concultative groups, stakeholders",
            "fr": "Comit\u00e9 consultatif d\u2019experts, Table ronde des groupes de consommateurs, Comit\u00e9 consultatif du ministre, groupes consultatifs, parties int\u00e9ress\u00e9es"
        },
        "dcterms.subject": {
            "en": "consumers,ethics,inspection,best practices",
            "fr": "consommateur,\u00e9thique,inspection,meilleures pratiques"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Human Resources",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidence, Ressources humaines"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-31 10:41:29",
            "fr": "2015-03-31 10:41:29"
        },
        "modified": {
            "en": "2021-05-31",
            "fr": "2021-05-31"
        },
        "type": {
            "en": "organizational description",
            "fr": "description de l'organisation"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Consultative groups",
        "fr": "Groupes consultatifs"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<ul>\n<li><a href=\"#a1\">Expert Advisory Committee</a></li>\n<li><a href=\"#a2\">Ministerial Advisory Board</a></li>\n<li><a href=\"#a3\">Sector Engagement Tables</a></li>\n</ul>\n\n<h2 id=\"a1\">Expert Advisory Committee</h2> \n\n<p>The Expert Advisory Committee provides the Canadian Food Inspection Agency (CFIA) with objective professional and technical advice on key issues related to the CFIA's three business lines: food safety, animal health and plant health.</p>\n\n<p>The committee could be engaged to:</p>\n\n<ul>\n<li>link into academic and scientific research</li>\n<li>comment on proposals for CFIA program and policy frameworks</li>\n<li>comment on CFIA priorities and risk management strategies</li>\n<li>provide insight into industry and international trends and developments</li>\n<li>advise on selected issues as requested by the CFIA</li>\n</ul>\n\n<p>The committee reports to the Agency through the CFIA's President and the Executive Vice President, who are the chair and vice-chair of the committee.</p>\n<p class=\"h3\"><a href=\"/about-the-cfia/transparency/consultations-and-engagement/consultative-groups/expert-advisory-committee/terms-of-reference-eac/eng/1580492882062/1580492882390\">Terms of reference</a></p>\n\n<h3>Membership</h3>\n\n<p>The current membership of the Expert  Advisory Committee is:<strong></strong></p>\n<p><a href=\"/eng/1579713552799/1579713553315#aa1\">Marilyn Allen </a><br>\n<a href=\"/eng/1579713552799/1579713553315#aa2\">Andrew Morse</a><br>\n<a href=\"/eng/1579713552799/1579713553315#aa3\">Simone Rudge</a><br>\n<a href=\"/eng/1579713552799/1579713553315#aa4\"><span lang=\"fr\">Ren\u00e9 Lallier</span></a><br>\n<a href=\"/eng/1579713552799/1579713553315#aa5\">Dr. Samuel K. Asiedu</a></p>\n\n<h2 id=\"a2\">Ministerial Advisory Board</h2>\n\n<p>The Ministerial Advisory Board (MAB) is established under Section 10 of the <i>Canadian Food Inspection Agency Act</i>.</p>\n\n<p>The mandate of the Ministerial Advisory Board is to advise the Minister of Health on:</p>\n\n<ul>\n<li>issues relevant to Canadian Food Inspection Agency (CFIA) activities</li>\n<li>the Minister's responsibilities for the overall direction of the CFIA</li>\n</ul>\n\n<p>The Board will focus on discussing a variety of topics, including:</p>\n\n<ul>\n<li>the strategic direction and priorities of the CFIA as reflected in its key planning documents</li>\n<li>the policy and program frameworks that govern CFIA operations</li>\n<li>the effects of developments in science and testing methods</li>\n<li>the systems for communicating and consulting with stakeholders</li>\n</ul>\n\n<p>The Board will consider issues requested by the Minister, and the Minister may direct the Board to consider and provide advice on any issue consistent with the mandate of the Board.</p>\n\n<p>The Board shall provide impartial and independent advisory services and make recommendations in support of the Board's mandate.</p>\n\n<p>The Board may provide advice to the Minister of Agriculture and Agri-Food on issues relating to her authorities.</p>\n<p class=\"h3\"><a href=\"/about-the-cfia/transparency/consultations-and-engagement/consultative-groups/ministerial-advisory-board/terms-of-reference-mab/eng/1580488067324/1580488067668\">Terms of reference</a></p>\n\n<h3>Membership</h3>\n\n<p>The current membership of the Ministerial Advisory Board is:</p>\n<ul class=\"list-unstyled\">\n<li><a href=\"/eng/1556654287531/1556654287794#a1\">Nancy Croitoru, Chair</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a2\">Alastair Cribb</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a3\">JoAnne L. Buth</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a4\">Dr. David Chalack D.V.M.</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a5\">Keith Mussar</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a6\">Suzanne T. Roy</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a7\">Ruth Salmon</a></li>\n<li><a href=\"/eng/1556654287531/1556654287794#a8\">Charles Murphy</a></li>\n</ul>\n\n<h2 id=\"a3\">Sector Engagement Tables</h2>\n\n<p>In 2021\u201322, Agriculture and Agri-Food Canada will launch the Sector  Engagement Tables (formerly the Value Chain Roundtables) to facilitate  strategic industry\u2013government collaboration on key issues facing the sector.  The model includes:</p>\n\n<ul>\n<li>5 sector advancement tables (Animal Protein,  Field Crops, Horticulture, Seafood, and Food Processing) </li>\n<li>4 thematic tables (Agile Regulations, Consumer  Demand and Market Trends, Skills Development, and Sustainability)</li>\n<li>fora for underrepresented groups in agriculture and  agri-food, namely youth, women, and Indigenous peoples</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<ul>\n<li><a href=\"#a1\">Comit\u00e9 consultatif d'experts</a></li>\n<li><a href=\"#a2\">Comit\u00e9 consultative du ministre</a></li>\n<li><a href=\"#a3\">Tables de consultation du secteur</a></li>\n</ul>\n\n<h2 id=\"a1\">Comit\u00e9 consultatif d'experts</h2>\n\n<p>Le Comit\u00e9 consultatif d'experts donne \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) des conseils techniques et professionnels objectifs sur des dossiers cl\u00e9s li\u00e9s \u00e0 ses trois secteurs d'activit\u00e9\u00a0: la salubrit\u00e9 des aliments, la sant\u00e9 des animaux et la protection des v\u00e9g\u00e9taux.</p>\n\n<p>Le comit\u00e9 pourrait\u00a0:</p>\n\n<ul>\n<li>participer aux travaux de recherche universitaires et scientifiques</li>\n<li>commenter les propositions de cadres de politiques et de programmes de l'ACIA</li>\n<li>commenter les priorit\u00e9s et les strat\u00e9gies de gestion des risques de l'Agence</li>\n<li>donner un aper\u00e7u des tendances et des r\u00e9alit\u00e9s nouvelles de l'industrie et \u00e0 l'\u00e9chelle internationale</li>\n<li>donner des conseils sur certaines questions \u00e0 la demande de l'ACIA</li>\n</ul>\n\n<p>Le comit\u00e9 rel\u00e8ve du pr\u00e9sident et de la premi\u00e8re vice-pr\u00e9sidente de l'ACIA, qui agiront \u00e0 titre de pr\u00e9sident et de vice-pr\u00e9sidente du comit\u00e9.</p>\n<p class=\"h3\"><a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/groupes-consultatifs/comite-consultatif-d-experts/mandat-cce/fra/1580492882062/1580492882390\">Mandat</a></p>\n\n<h3>Membres</h3>\n\n<p>L'adh\u00e9sion actuelle au comit\u00e9 consultatif minist\u00e9riel est de\u00a0:</p>\n<p><a href=\"/fra/1579713552799/1579713553315#aa1\">Marilyn Allen </a><br>\n<a href=\"/fra/1579713552799/1579713553315#aa2\">Andrew Morse</a><br>\n<a href=\"/fra/1579713552799/1579713553315#aa3\">Simone Rudge</a><br>\n<a href=\"/fra/1579713552799/1579713553315#aa4\"><span lang=\"fr\">Ren\u00e9 Lallier</span></a><br>\n<a href=\"/fra/1579713552799/1579713553315#aa5\">D<sup>r</sup> Samuel K. Asiedu</a></p>\n<h2 id=\"a2\">Comit\u00e9 consultative du ministre</h2>\n\n<p>Le Comit\u00e9 consultatif du ministre (le Comit\u00e9) est cr\u00e9\u00e9 en vertu de l'article\u00a010 de la <i>Loi sur l'Agence canadienne d'inspection des aliments</i>.</p>\n\n<p>Le Comit\u00e9 consultatif du ministre a pour mandat de conseiller la ministre de la Sant\u00e9 sur\u00a0:</p>\n\n<ul>\n<li>toute question relative aux activit\u00e9s de l'ACIA</li>\n<li>les responsabilit\u00e9s de la ministre en ce qui a trait \u00e0 l'orientation g\u00e9n\u00e9rale de l'ACIA</li>\n</ul>\n\n<p>Le Comit\u00e9 se penchera sur diverses questions, notamment\u00a0:</p>\n\n<ul>\n<li>l'orientation strat\u00e9gique et les priorit\u00e9s de l'ACIA telles que d\u00e9crites dans les principaux documents de planification</li>\n<li>les cadres de travail des politiques et programmes qui r\u00e9gissent les op\u00e9rations de l'ACIA</li>\n<li>les effets des progr\u00e8s scientifiques et des nouvelles m\u00e9thodes d'essai</li>\n<li>les syst\u00e8mes de consultation et de communication avec les intervenants</li>\n</ul>\n\n<p>Le Comit\u00e9 examinera les questions soulev\u00e9es par la ministre et cette derni\u00e8re pourra demander au Comit\u00e9 d'examiner toute question relevant du mandat du Comit\u00e9 et de donner des conseils \u00e0 ce sujet.</p>\n\n<p>Le Comit\u00e9 fournira des services consultatifs impartiaux et ind\u00e9pendants et fera des recommandations soutenant le mandat du Comit\u00e9.</p>\n\n<p>Le Comit\u00e9 peut donner des conseils au ministre de l'Agriculture et Agroalimentaire Canada sur des questions li\u00e9es \u00e0 ses pouvoirs.</p>\n<p class=\"h3\"><a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/groupes-consultatifs/comite-consultatif-ministeriel/mandat-ccm/fra/1580488067324/1580488067668\">Mandat</a></p>\n\n<h3>Membres</h3>\n\n<p>L'adh\u00e9sion actuelle au comit\u00e9 consultatif minist\u00e9riel est de\u00a0:</p>\n<ul class=\"list-unstyled\">\n<li><a href=\"/fra/1556654287531/1556654287794#a1\">Nancy Croitoru, Chair</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a2\">Alastair Cribb</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a3\">JoAnne L. Buth</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a4\">D<sup>r</sup> David Chalack D.V.M.</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a5\">Keith Mussar</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a6\">Suzanne T. Roy</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a7\">Ruth Salmon</a></li>\n<li><a href=\"/fra/1556654287531/1556654287794#a8\">Charles Murphy</a></li>\n</ul>\n<h2 id=\"a3\">Tables de consultation du secteur</h2>\n\n<p>En 2021-2022, l'Agriculture et Agroalimentaire Canada poursuivra le lancement les tables de consultation du secteur (anciennement les tables rondes sur les cha\u00eenes des valeurs) pour faciliter la collaboration strat\u00e9gique entre l'industrie et le gouvernement sur les questions cl\u00e9s auxquelles le secteur est confront\u00e9. Le mod\u00e8le comprend\u00a0:</p>\n\n<ul>\n<li>5 tables d'avancement sectoriel (prot\u00e9ines animales, grandes cultures, horticulture, produits de la mer et transformation alimentaire)</li>\n<li>4 tables th\u00e9matiques (r\u00e9glementation agile, demande des consommateurs et tendances du march\u00e9, d\u00e9veloppement des comp\u00e9tences et durabilit\u00e9)</li>\n<li>des forums pour les groupes sous-repr\u00e9sent\u00e9s de l'agriculture et l'agroalimentaire, \u00e0 savoir les jeunes, les femmes et les peuples autochtones</li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}