{
    "dcr_id": "1330891097865",
    "title": {
        "en": "Fertilizer program overview",
        "fr": "Aper\u00e7u du programme des engrais"
    },
    "html_modified": "2024-02-13 9:47:23 AM",
    "modified": "2023-10-25",
    "issued": "2012-03-04 14:58:19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_fert_overview_1330891097865_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_fert_overview_1330891097865_fra"
    },
    "ia_id": "1330891293225",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fertilizer program overview",
        "fr": "Aper\u00e7u du programme des engrais"
    },
    "label": {
        "en": "Fertilizer program overview",
        "fr": "Aper\u00e7u du programme des engrais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1330891293225",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/program-overview/",
        "fr": "/protection-des-vegetaux/engrais/apercu-du-programme/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fertilizer program overview",
            "fr": "Aper\u00e7u du programme des engrais"
        },
        "description": {
            "en": "Fertilizers (plant nutrients) and supplements (products other than fertilizers that improve the physical condition of the soil or aid plant growth and crop yield) when imported and sold in Canada are regulated by the Canadian Food inspection Agency under the Fertilizers Act and Fertilizers Regulations.",
            "fr": "Les engrais (nutriments pour v\u00e9g\u00e9taux) et les suppl\u00e9ments (produits autres que les engrais qui enrichie les sols ou \u00e0 favoriser la croissance des plantes ou la productivit\u00e9 des r\u00e9coltes) lorsqu'ils sont import\u00e9s ou vendus au Canada sont r\u00e9glement\u00e9s par l'Agence canadienne d'inspection des aliments en vertu de la Loi sur les engrais et R\u00e8glement sur les engrais."
        },
        "keywords": {
            "en": "plant protection, fertilizers, biotechnology, microbial supplements, novel supplements, supplements, Fertilizer Program Overview",
            "fr": "protection des v\u00e9g\u00e9taux, engrais, biotechnologie, suppl\u00e9ments microbiens, suppl\u00e9ments nouveaux, suppl\u00e9ments, Survol du programme des engrais"
        },
        "dcterms.subject": {
            "en": "crops,environment,fertilizers,inspection,plants",
            "fr": "cultures,environnement,engrais,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-04 14:58:19",
            "fr": "2012-03-04 14:58:19"
        },
        "modified": {
            "en": "2023-10-25",
            "fr": "2023-10-25"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fertilizer program overview",
        "fr": "Aper\u00e7u du programme des engrais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The 3 year regulatory transition period (October 26, 2020 to October 26, 2023) has now ended. As a result, regulated parties, including all manufacturers, importers, distributors and sellers of fertilizers and supplements must adhere to the amended <a href=\"/english/reg/jredirect2.shtml?ferengr\"><i>Fertilizers Regulations</i></a>. There are few notable exceptions for some product categories. Learn more about the <a href=\"/plant-health/fertilizers/notices-to-industry/2023-05-03/eng/1682633127754/1682633128598\">implementation of the amended <i>Fertilizers Regulations</i></a>.</p>\n</div>\n\n\n<h2>Program description and mandate</h2>\n\n<p>Fertilizers (plant nutrients) and supplements (products other than fertilizers that improve the physical condition of the soil or aid plant growth and crop yield) when imported and sold in Canada are regulated by the Canadian Food inspection Agency under the <a href=\"/english/reg/jredirect2.shtml?ferenga\"><i>Fertilizers Act</i></a> and <a href=\"/english/reg/jredirect2.shtml?ferengr\"><i>Fertilizers Regulations</i></a>.</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) administers a number of acts and regulations that, together, are designed to safeguard our food supply, protect the health of animals and plants and enhance the well-being of Canada's people, environment and economy.</p>\n\n<h3>Product coverage</h3>\n\n<p>The <i>Fertilizers Act</i> and Regulations requires that all regulated fertilizer and supplement products be safe for humans, plants, animals, and the environment. They must also be properly labelled to enable safe and appropriate use. The mandate of the CFIA's Fertilizer Program covers a wide range of products sold for agricultural, commercial, and home and garden purposes. Regulated products include:</p>\n\n<ul>\n<li>farm fertilizers,</li>\n<li>micronutrients,</li>\n<li>lawn and garden products as well as</li>\n<li>supplements\n<ul>\n<li>water holding polymers,</li>\n<li>microbial inoculants,</li>\n<li>abiotic stress protectants,</li>\n<li>liming materials, and</li>\n<li>waste-derived materials such as composts and municipal biosolids</li>\n</ul>\n</li>\n</ul>\n\n<p>While the CFIA regulates all fertilizers and supplements that are <strong>imported</strong> or <strong>sold</strong> in Canada, the manufacture, proper use and safe disposal of these products are controlled by provincial and municipal rules and regulations. The CFIA works together with provinces and municipalities to ensure that all fertilizers and supplements meet the highest standards for safety.</p>\n\n<p>Some fertilizers and most supplements require registration and a comprehensive pre-market assessment <strong>prior</strong> to their import or sale in Canada. Products that are exempt from registration are still subject to regulation and must meet all the prescribed standards at the time of sale or import.</p>\n\n<h3>Pre-market assessments</h3>\n\n<p>The CFIA's pre-market assessment consists of a detailed, science-based evaluation of product safety information and labelling. These assessments focus on evaluation of product safety towards humans, plants, animals and the environment. To assess a product, the Agency requires supporting information, which varies in scope depending on the nature of the product. The basic supporting information includes the product label, the manufacturing method, and a complete list of ingredients and source materials. For certain products additional information may be required. This includes:</p>\n\n<ul>\n<li>detailed description of the physical and chemical properties of each ingredient</li>\n<li>results of analytical tests that show freedom from biological and chemical contaminants, and/or</li>\n<li>a toxicological data package derived from either laboratory studies or scientific publications</li>\n</ul>\n\n<p>Safety assessments are conducted by a team of highly qualified and trained evaluators. Safety evaluators examine all ingredients in a fertilizer or supplement including the active components as well as the formulants, carriers, additives, potential contaminants and by-products that might be released into the environment as a result of product's use and application to soil. The CFIA also examines unintended and potentially adverse effects of the application of the product. This includes bystander and worker exposure (for example retailer, farmer, home owner), safety of food crops grown on land that has been treated with the product, impacts on animals and plants other than the target crop species, and ecosystem effects including impact on soil, biodiversity, leaching to waterways, etc.</p>\n\n<h3>Label verification</h3>\n\n<p>All products submitted to the CFIA for registration or approval undergo a thorough label verification to ensure that displayed information complies with the standards prescribed by the <i>Fertilizers Act</i> and Regulations. Evaluators verify that requisite information such as:</p>\n\n<ul>\n<li>guaranteed analysis,</li>\n<li>directions for use,</li>\n<li>company/manufacturer contact information,</li>\n<li>appropriate units of measurement, and</li>\n<li>mandatory cautionary statements, correctly appear and are clearly legible on the label</li>\n</ul>\n\n<p>All mandatory information must appear in both official languages and shall be printed conspicuously, legibly and indelibly in English and in French with both versions being in equal prominence and in close proximity to each other.</p>\n\n<h3>Marketplace monitoring</h3>\n\n<p>The CFIA also monitors fertilizer and supplement products that are already available in the marketplace to verify their compliance with the prescribed standards. Across the country, the CFIA inspectors visit facilities, sample products and review labels. These efforts are focused on verifying that products satisfy the safety standards for biological and chemical contaminants (pathogens, heavy metals, pesticide residues, and dioxins and furans). Products found to be non-compliant are subject to regulatory action, which may include product detention (stop sale) and, in severe cases, prosecution.</p>\n\n<h2>Index of Trade Memoranda</h2>\n\n<p>The <a href=\"/plant-health/fertilizers/trade-memoranda/eng/1299873703612/1299873786929\">Fertilizer Trade Memoranda</a> contain product-specific information on regulatory requirements for fertilizers and supplements regulated under the <i>Fertilizers Act</i>.</p>\n\n<h2>Contact information</h2>\n\n<p>Fertilizer Safety Section<br>\n<br>\n<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n<p>La p\u00e9riode de transition r\u00e9glementaire de 3 ans (26 octobre 2020 au 26 octobre 2023) est termin\u00e9e. Par cons\u00e9quent, les parties r\u00e9glement\u00e9es, y compris tous les fabricants, importateurs, distributeurs et vendeurs d'engrais et de suppl\u00e9ments, doivent adh\u00e9rer au <a href=\"/francais/reg/jredirect2.shtml?ferengr\"><i>R\u00e8glement sur les engrais</i></a> modifi\u00e9. Il existe quelques exceptions notables pour certaines cat\u00e9gories de produits. Apprenez-en davantage sur la <a href=\"/protection-des-vegetaux/engrais/avis-a-l-industrie/2023-05-03/fra/1682633127754/1682633128598\">mise en \u0153uvre de la modification du <i>R\u00e8glement sur les engrais</i></a>.</p>\n</div>\n\n<h2>Description du programme et mandat</h2>\n\n<p>Les engrais (nutriments pour v\u00e9g\u00e9taux) et les suppl\u00e9ments (produits autres que les engrais qui enrichie les sols ou \u00e0 favoriser la croissance des plantes ou la productivit\u00e9 des r\u00e9coltes) lorsqu'ils sont import\u00e9s ou vendus au Canada sont r\u00e9glement\u00e9s par l'Agence canadienne d'inspection des aliments en vertu de la <a href=\"/francais/reg/jredirect2.shtml?ferenga\"><i>Loi sur les engrais</i></a> et <a href=\"/francais/reg/jredirect2.shtml?ferengr\"><i>R\u00e8glement sur les engrais</i></a>.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) applique un certain nombre de lois et de r\u00e8glements f\u00e9d\u00e9raux qui, en synergie, servent \u00e0 prot\u00e9ger notre approvisionnement alimentaire, la sant\u00e9 des animaux et des v\u00e9g\u00e9taux, et \u00e0 am\u00e9liorer le bien-\u00eatre des Canadiens, l'environnement et l'\u00e9conomie.</p>\n\n<h3>Produits vis\u00e9s</h3>\n\n<p>La <i>Loi sur les engrais</i> et son r\u00e8glement d'application exigent que tous les engrais et suppl\u00e9ments r\u00e9glement\u00e9s soient sans risque pour les humains, les animaux, les v\u00e9g\u00e9taux et l'environnement. Ils doivent aussi \u00eatre ad\u00e9quatement \u00e9tiquet\u00e9s pour permettre leur usage s\u00fbr et appropri\u00e9. Le mandat du Programme des engrais de l'ACIA couvre une vaste gamme de produits vendus \u00e0 des fins agricoles, commerciales, et comme produits domestiques ou de jardinage. Les produits vis\u00e9s comprennent les\u00a0:</p>\n\n<ul>\n<li>engrais agricoles,</li>\n<li>engrais d'oligo-\u00e9l\u00e9ments,</li>\n<li>engrais pour pelouses et jardins,</li>\n<li>suppl\u00e9ments\n<ul>\n<li>polym\u00e8res retenant l'eau,</li>\n<li>inoculants microbiens,</li>\n<li>protecteurs contre les stress abiotiques,</li>\n<li>produits chaulant et</li>\n<li>produits d\u00e9riv\u00e9s de d\u00e9chets tels les composts et les biosolides municipaux trait\u00e9s.</li>\n</ul>\n</li>\n</ul>\n\n<p>Bien que l'ACIA r\u00e9glemente tous les engrais et suppl\u00e9ments qui sont <strong>import\u00e9s</strong> ou <strong>vendus</strong> au Canada, la fabrication, l'utilisation ad\u00e9quate et l'\u00e9limination sans risque de ces produits sont r\u00e9gies par les lois et les r\u00e8glements provinciaux et municipaux. L'ACIA collabore avec les provinces et municipalit\u00e9s pour faire en sorte que tous les engrais et suppl\u00e9ments respectent les normes les plus \u00e9lev\u00e9es en mati\u00e8re d'innocuit\u00e9.</p>\n\n<p>Certains engrais et la plupart des suppl\u00e9ments doivent \u00eatre enregistr\u00e9s, et n\u00e9cessitent une \u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 <strong>avant</strong> leur importation ou leur vente au Canada. Les produits qui sont exempt\u00e9s de l'enregistrement restent n\u00e9anmoins assujettis \u00e0 la r\u00e9glementation et doivent \u00eatre conformes \u00e0 toutes les normes prescrites au moment de la vente ou de l'importation.</p>\n\n<h3>\u00c9valuation pr\u00e9alable \u00e0 la mise en march\u00e9</h3>\n\n<p>L'\u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 r\u00e9alis\u00e9e par l'ACIA consiste d'une part en une \u00e9valuation scientifique exhaustive des renseignements sur l'innocuit\u00e9 du produit, et d'autre part en la v\u00e9rification des \u00e9tiquettes. L'\u00e9valuation de l'innocuit\u00e9 d'un produit se concentre tour \u00e0 tour sur son innocuit\u00e9 par rapport aux humains, aux plantes, aux animaux et \u00e0 l'environnement. Afin d'\u00e9valuer l'innocuit\u00e9 d'un produit, l'Agence demande au requ\u00e9rant de fournir les informations qui montrent que ce produit est s\u00e9curitaire. L'importance des informations requises varie selon la nature du produit. Les renseignements justificatifs de base que le requ\u00e9rant doit fournir incluent l'\u00e9tiquette du produit, le proc\u00e9d\u00e9 de fabrication et une liste compl\u00e8te des ingr\u00e9dients incluant leur source et leur proportion. Pour certains produits, des renseignements additionnels peuvent \u00eatre n\u00e9cessaires. Cela inclut\u00a0:</p>\n\n<ul>\n<li>une description d\u00e9taill\u00e9e des propri\u00e9t\u00e9s physiques et chimiques de chaque ingr\u00e9dient,</li>\n<li>les r\u00e9sultats d'analyses qui montrent l'absence de contaminants biologiques et chimiques, ou</li>\n<li>un ensemble de donn\u00e9es toxicologiques d\u00e9coulant d'\u00e9tudes en laboratoire ou de publications scientifiques.</li>\n</ul>\n\n<p>Les \u00e9valuations de l'innocuit\u00e9 sont men\u00e9es par une \u00e9quipe d'\u00e9valuateurs hautement qualifi\u00e9s qui ont re\u00e7u une formation approfondie. Les \u00e9valuateurs de l'innocuit\u00e9 examinent tous les ingr\u00e9dients pr\u00e9sents dans les engrais ou les suppl\u00e9ments, dont les \u00e9l\u00e9ments actifs, les produits de formulation, les vecteurs, les additifs, les contaminants potentiels et les sous-produits qui pourraient \u00eatre diss\u00e9min\u00e9s dans l'environnement \u00e0 la suite de l'\u00e9pandage du produit. L'ACIA examine \u00e9galement les effets non d\u00e9sir\u00e9s et potentiellement dangereux qui sont li\u00e9s \u00e0 l'utilisation des produits \u00e9valu\u00e9s. Il s'agit notamment de l'exposition des tiers et des travailleurs (p. ex. d\u00e9taillants, agriculteurs, propri\u00e9taires fonciers), de la salubrit\u00e9 des cultures vivri\u00e8res cultiv\u00e9es sur des terres qui ont \u00e9t\u00e9 trait\u00e9es avec le produit et des r\u00e9percussions sur les animaux et les v\u00e9g\u00e9taux autres que la culture vis\u00e9e ainsi que sur les \u00e9cosyst\u00e8mes (les effets sur le sol et la biodiversit\u00e9, le lessivage dans des cours d'eau, etc.).</p>\n\n<h3>V\u00e9rification des \u00e9tiquettes</h3>\n\n<p>Tous les produits soumis \u00e0 l'ACIA pour l'enregistrement ou l'approbation (produits exempt\u00e9s de l'enregistrement) subissent un examen approfondi de l'\u00e9tiquette afin de v\u00e9rifier que les renseignements affich\u00e9s sont conformes aux normes prescrites par la <i>Loi sur les engrais</i> et son r\u00e8glement d'application. Les \u00e9valuateurs v\u00e9rifient que les renseignements exig\u00e9s, tel que\u00a0:</p>\n\n<ul>\n<li>l'analyse garantie,</li>\n<li>le mode d'emploi,</li>\n<li>les coordonn\u00e9es de l'entreprise/du fabricant,</li>\n<li>les unit\u00e9s de mesure appropri\u00e9es et</li>\n<li>les \u00e9nonc\u00e9s de mise en garde sont conformes et clairement lisibles sur l'\u00e9tiquette.</li>\n</ul>\n\n<p>Toutes les informations obligatoires doivent appara\u00eetre dans les deux langues officielles et doivent \u00eatre imprim\u00e9es de mani\u00e8re visible, lisible et ind\u00e9l\u00e9bile en anglais et en fran\u00e7ais, les deux versions \u00e9tant \u00e9galement en \u00e9vidence et \u00e0 proximit\u00e9 l'une de l'autre.</p>\n\n<h3>Surveillance du march\u00e9</h3>\n\n<p>L'ACIA surveille \u00e9galement les engrais et les suppl\u00e9ments qui sont d\u00e9j\u00e0 disponibles sur le march\u00e9, afin de v\u00e9rifier leur conformit\u00e9 aux normes prescrites. Les inspecteurs de l'ACIA visitent les installations, \u00e9chantillonnent les produits et examinent les \u00e9tiquettes \u00e0 l'\u00e9chelle du pays. Ils veillent tout particuli\u00e8rement \u00e0 d\u00e9terminer si les produits respectent les normes de salubrit\u00e9 relatives aux contaminants biologiques et chimiques (agents pathog\u00e8nes, m\u00e9taux lourds, r\u00e9sidus de pesticides, dioxines et furannes, etc.). Des mesures de r\u00e9glementation sont prises dans le cas des produits non conformes, dont leur r\u00e9tention (arr\u00eat de la vente) et, dans les cas graves, des poursuites judiciaires sont intent\u00e9es.</p>\n\n<h2>Glossaire des circulaires \u00e0 la profession</h2>\n\n<p>Les <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/fra/1299873703612/1299873786929\">circulaires \u00e0 la profession</a> contiennent des renseignements sp\u00e9cifiques au produit sur les exigences r\u00e9glementaires applicables aux engrais et aux suppl\u00e9ments r\u00e9glement\u00e9s en vertu de la <i>Loi sur les engrais</i>.</p>\n\n<h2>Coordonn\u00e9es</h2>\n\n<p>Section de l'innocuit\u00e9 des engrais<br>\n<br>\n<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}