{
    "dcr_id": "1299780188624",
    "title": {
        "en": "Mandate",
        "fr": "Mandat"
    },
    "html_modified": "2024-02-13 9:46:37 AM",
    "modified": "2019-12-12",
    "issued": "2015-04-09 11:48:42",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_agen_value_1299780188624_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_agen_value_1299780188624_fra"
    },
    "ia_id": "1319164463699",
    "parent_ia_id": "1323224814073",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1323224814073",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Mandate",
        "fr": "Mandat"
    },
    "label": {
        "en": "Mandate",
        "fr": "Mandat"
    },
    "templatetype": "content page 1 column",
    "node_id": "1319164463699",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323224617636",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/organizational-structure/mandate/",
        "fr": "/a-propos-de-l-acia/structure-organisationnelle/mandat/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Mandate",
            "fr": "Mandat"
        },
        "description": {
            "en": "Our Values outlines the values and supporting principles CFIA has adopted to help guide our decisions and services and our staff in carrying out our mandate of safeguarding the Canadian public, environment and economy.",
            "fr": "L'ACIA vise avant tout \u00e0 att\u00e9nuer les risques li\u00e9s \u00e0 la salubrit\u00e9 des aliments."
        },
        "keywords": {
            "en": "Values, principles, Mandate",
            "fr": "principes, valeurs"
        },
        "dcterms.subject": {
            "en": "statements,ethics,government information,inspection,human resources",
            "fr": "d\u00e9claration,\u00e9thique,information gouvernementale,inspection,ressources humaines"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-09 11:48:42",
            "fr": "2015-04-09 11:48:42"
        },
        "modified": {
            "en": "2019-12-12",
            "fr": "2019-12-12"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Mandate",
        "fr": "Mandat"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Mitigating risks to food safety is the CFIA's highest priority,  and the health and safety of Canadians is the driving force behind the design  and development of CFIA programs. The CFIA, in collaboration and partnership  with industry, consumers, and federal, provincial and municipal organizations,  continues to work towards protecting Canadians from preventable health risks  related to food and zoonotic diseases.</p>\n<p>The current and future economic prosperity of the  Canadian agriculture and forestry sectors relies on a healthy and sustainable  animal and plant resource base. As such, the CFIA is continually improving its  program design and delivery in the animal health and plant resource areas in  order to minimize and manage risks. In an effort to protect the natural  environment from invasive animal and plant diseases and plant pests, the CFIA  also performs extensive work related to the protection of environmental  biodiversity.</p>\n<h2>Vision</h2>\n<p>To excel as a science-based regulator, trusted and respected by Canadians and the international community.</p>\n<h2>Mission</h2>\n<p>Dedicated to safeguarding food, animals and plants, which enhances   the health and well-being of Canada's people, environment and economy.</p>\n<h2>Statement of values</h2>\n\n<p>As employees of the Canadian Food Inspection Agency:</p>\n<ul>\n<li class=\"mrgn-bttm-md\">We value scientific rigour and professional and technical competence. These play a crucial role in our decision making. We do not manipulate science to achieve a desired outcome but acknowledge that other factors must be taken into account in this decision making.</li>\n<li class=\"mrgn-bttm-md\">The reputation and credibility of the Agency are vital to our ability to deliver our mandate. As such, we behave, internally and externally, in a way that trust is preserved.</li>\n<li class=\"mrgn-bttm-md\">We are proud of the contributions we make to the quality of life of Canadians. We value dedication and responsiveness from all employees day to day and, particularly, during an emergency.</li>\n<li class=\"mrgn-bttm-md\">We value competent, qualified and motivated personnel, whose efforts drive the results of the Agency.</li>\n<li class=\"mrgn-bttm-md\">To develop effective policies and strategies, we value the perspectives of the stakeholders who are affected by our decisions.</li>\n<li class=\"mrgn-bttm-md\">We maintain our regulatory independence from all external stakeholders. We have the courage to make difficult and potentially unpopular decisions and recommendations, free from personal bias.</li>\n<li class=\"mrgn-bttm-md\">We are committed to our physical and psychological well-being.</li>\n</ul>\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/about-the-cfia/organizational-structure/mandate/statement-of-values/eng/1319478952479/1319479599378\">Statement of values (full text)</a></li>\n<li><a href=\"/about-the-cfia/organizational-structure/mandate/relationship/eng/1319480989283/1319481252700\">The Canadian Food Inspection Agency and its regulated parties, stakeholders and partners: an ethical relationship </a></li>\n<li><a href=\"/about-the-cfia/organizational-structure/mandate/responsible-conduct/eng/1319825850028/1319826099857\">Policy on the responsible conduct of research and development</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'ACIA vise avant tout \u00e0 att\u00e9nuer les risques li\u00e9s \u00e0 la salubrit\u00e9 des aliments. La sant\u00e9 et la s\u00e9curit\u00e9 des Canadiens sont sous-jacents la conception et de l'\u00e9laboration des programmes de l'Agence. En collaboration et en partenariat avec l'industrie, les consommateurs ainsi que des organismes f\u00e9d\u00e9raux, provinciaux et municipaux, l'ACIA poursuit ses efforts pour prot\u00e9ger les Canadiens contre les risques \u00e9vitables pour la sant\u00e9 li\u00e9s aux aliments et aux zoonoses.</p>\n<p>La prosp\u00e9rit\u00e9 \u00e9conomique actuelle et future des secteurs agricole et forestier du Canada repose sur des ressources animales et v\u00e9g\u00e9tales saines et durables. C'est pourquoi l'ACIA am\u00e9liore continuellement la conception et l'ex\u00e9cution de ses programmes dans les secteurs de la sant\u00e9 des animaux et des ressources v\u00e9g\u00e9tales dans le but d'att\u00e9nuer et de g\u00e9rer les risques. En vue de prot\u00e9ger l'environnement naturel des maladies animales et v\u00e9g\u00e9tales, et des phytoravageurs envahissants, elle travaille de fa\u00e7on intensive \u00e0 prot\u00e9ger la biodiversit\u00e9. </p>\n<h2>Vision</h2>\n<p>Exceller en tant qu'organisme de r\u00e9glementation \u00e0 vocation  scientifique fiable et respect\u00e9 des Canadiens et de la communaut\u00e9  internationale.</p>\n<h2>Mission</h2>\n<p>Veiller \u00e0 la sant\u00e9 et au bien-\u00eatre des Canadiens, \u00e0 l'environnement  et \u00e0 l'\u00e9conomie en pr\u00e9servant la salubrit\u00e9 des aliments, la sant\u00e9 des  animaux et la protection des v\u00e9g\u00e9taux.</p>\n<h2>\u00c9nonc\u00e9 des valeurs</h2>\n\n<p>En tant qu'employ\u00e9s de l'Agence canadienne d'inspection des aliments\u00a0:</p>\n<ul>\n<li class=\"mrgn-bttm-md\">Nous accordons beaucoup d'importance \u00e0 la rigueur scientifique et aux comp\u00e9tences professionnelles et techniques. Elles jouent d'ailleurs un r\u00f4le crucial dans notre processus d\u00e9cisionnel. Nous n'admettons aucun compromis \u00e0 la d\u00e9marche scientifique pour obtenir les conclusions souhait\u00e9es. Nous sommes conscients n\u00e9anmoins que ce processus englobe d'autres facteurs.</li>\n<li class=\"mrgn-bttm-md\">La r\u00e9putation et la cr\u00e9dibilit\u00e9 de l'Agence sont essentielles \u00e0 sa capacit\u00e9 de r\u00e9aliser son mandat. Nous nous comportons de mani\u00e8re \u00e0 pr\u00e9server la relation de confiance que nous entretenons tant \u00e0 l'interne qu'\u00e0 l'externe.</li>\n<li class=\"mrgn-bttm-md\">Nous sommes fiers de notre contribution \u00e0 la qualit\u00e9 de vie des Canadiens. Nous appr\u00e9cions le d\u00e9vouement et la capacit\u00e9 d'intervention de tous les employ\u00e9s, au quotidien, et particuli\u00e8rement dans les situations d'urgence.</li>\n<li class=\"mrgn-bttm-md\">Nous appr\u00e9cions la comp\u00e9tence, les qualifications et la motivation du personnel, dont les efforts contribuent aux r\u00e9sultats de l'Agence.</li>\n<li class=\"mrgn-bttm-md\">Afin d'\u00e9laborer des politiques et des strat\u00e9gies efficaces, nous tenons compte du point de vue des intervenants touch\u00e9s par nos d\u00e9cisions.</li>\n<li class=\"mrgn-bttm-md\">En tant qu'organisme de r\u00e9glementation, nous veillons \u00e0 prot\u00e9ger notre ind\u00e9pendance face aux intervenants externes. Nous avons le courage de prendre des d\u00e9cisions difficiles, susceptibles de d\u00e9plaire, et de formuler des recommandations sans \u00e9gard \u00e0 notre opinion.</li>\n<li class=\"mrgn-bttm-md\">Nous nous engageons \u00e0 assurer notre bien-\u00eatre physique et psychologique.</li>\n</ul>\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/structure-organisationnelle/mandat/enonce-des-valeurs/fra/1319478952479/1319479599378\">\u00c9nonc\u00e9 des valeurs (le texte int\u00e9gral)</a></li>\n<li><a href=\"/a-propos-de-l-acia/structure-organisationnelle/mandat/relation/fra/1319480989283/1319481252700\">L'Agence canadienne d\u2019inspection des aliments et ses parties r\u00e9glement\u00e9es, intervenants et partenaires : Une relation fond\u00e9e sur l'\u00e9thique</a></li>\n<li><a href=\"/a-propos-de-l-acia/structure-organisationnelle/mandat/conduite-responsable/fra/1319825850028/1319826099857\">Politique sur la conduite responsable de Recherche et de d\u00e9veloppement</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}