{
    "dcr_id": "1591993057681",
    "title": {
        "en": "Ethanol in non-alcoholic fermented beverages",
        "fr": "\u00c9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es"
    },
    "html_modified": "18-6-2020",
    "modified": "18-6-2020",
    "issued": "15-6-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/notice_to_industry_ethanol_in_non_alcoholic_1591993057681_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/notice_to_industry_ethanol_in_non_alcoholic_1591993057681_fra"
    },
    "parent_ia_id": "1544505212400",
    "ia_id": "1591993058244",
    "parent_node_id": "1544505212400",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Ethanol in non-alcoholic fermented beverages",
        "fr": "\u00c9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es"
    },
    "label": {
        "en": "Ethanol in non-alcoholic fermented beverages",
        "fr": "\u00c9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es"
    },
    "templatetype": "content page 1 column",
    "node_id": "1591993058244",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1544505197742",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-import-notices-for-industry/notice-2020-06-11/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/avis-d-importation-d-aliments-pour-l-industrie/avis-2020-06-11/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Ethanol in non-alcoholic fermented beverages",
            "fr": "\u00c9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es"
        },
        "description": {
            "en": "On June 11, 2020, Health Canada published an information sheet for consumers on Ethanol in non-alcoholic fermented beverages.",
            "fr": "Le 11 juin 2020, Sant\u00e9 Canada a publi\u00e9 une fiche d\u2019information pour les consommateurs sur l\u2019\u00e9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es."
        },
        "keywords": {
            "en": "Safe Food for Canadians Regulations, SFCR, SFC licence, food import declarations, licence number, Ethanol in non-alcoholic fermented beverages, kombucha",
            "fr": "R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, SAC, licence, d\u00e9clarations d'importation d'aliments, \u00e9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es, kombucha"
        },
        "dcterms.subject": {
            "en": "food inspection,policy,agri-food products,food safety",
            "fr": "inspection des aliments,politique,produit agro-alimentaire,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-06-18",
            "fr": "2020-06-18"
        },
        "modified": {
            "en": "2020-06-18",
            "fr": "2020-06-18"
        },
        "type": {
            "en": "guide,reference material",
            "fr": "guide,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Ethanol in non-alcoholic fermented beverages",
        "fr": "\u00c9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>On June 11, 2020, Health Canada published an information sheet for consumers on <a href=\"https://www.canada.ca/en/health-canada/services/publications/food-nutrition/ethanol-non-alcoholic-fermented-beverages.html\">Ethanol in non-alcoholic fermented beverages</a>. Fermented beverages such as kombucha, kefir and some soft drinks like ginger beer can contain low levels of ethanol (alcohol). Although these products are not made to be alcoholic, their ethanol content can vary depending on the fermentation process, distribution and storage conditions.</p>\n\n<p>Companies selling fermented beverages are responsible for making sure their foods comply with all labelling and consumer protection requirements of the <i>Safe Food for Canadians Regulations</i> and <i>Food and Drug Regulations</i>, as well as any provincial or territorial requirements. </p>\n\n<p>Under the <a href=\"/english/reg/jredirect2.shtml?drga\"><i>Food and Drugs Act</i></a> and its regulations, beverages with 1.1% alcohol by volume (ABV) or greater are required to declare the alcohol content on the label. </p>\n\n<p>Provinces and territories oversee the control, distribution and sale of alcoholic beverages in their own jurisdictions and may have additional requirements. For example, in most provinces and territories a beverage containing &gt; 0.5% ABV would be considered to be an alcoholic product and subject to controlled sale.</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) continues to support food businesses in complying with the regulations, and takes enforcement action when any food is not in compliance with applicable legislation. Labelling requirements for alcoholic beverages can be found on the CFIA's website.</p>\n\n<p>If you have further questions contact your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a> for more information.</p>\n\n<h2>Supporting information</h2>\n<ul>\n<li>Health Canada information sheet re: <a href=\"https://www.canada.ca/en/health-canada/services/publications/food-nutrition/ethanol-non-alcoholic-fermented-beverages.html\">Ethanol in non-alcoholic fermented beverages</a></li>\n<li><a href=\"/food-labels/labelling/industry/eng/1383607266489/1383607344939\">Industry Labelling Tool</a></li>\n<li><a href=\"/eng/1392909001375/1392909133296\">Labelling requirements for alcoholic beverages</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Le 11 juin 2020, Sant\u00e9 Canada a publi\u00e9 une fiche d'information pour les consommateurs sur <a href=\"https://www.canada.ca/fr/sante-canada/services/publications/aliments-et-nutrition/ethanol-boissons-fermentees-non-alcoolisees.html\">l'\u00e9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es</a>. Les boissons ferment\u00e9es, telles que le kombucha, le k\u00e9fir et certaines boissons gazeuses comme la bi\u00e8re de gingembre peuvent contenir un faible taux d'\u00e9thanol (alcool). Bien que ces produits ne soient pas faits pour \u00eatre alcooliques, leur taux d'\u00e9thanol peut varier en fonction du processus de fermentation, et des conditions de distribution et d'entreposage.</p>\n\n<p>Il incombe aux entreprises qui vendent des boissons ferment\u00e9es de s'assurer qu'elles sont conformes non seulement \u00e0 toutes les exigences relatives \u00e0 l\u2019\u00e9tiquetage et \u00e0 la protection des consommateurs du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> et du <i>R\u00e8glement sur les aliments et drogues</i>, mais aussi \u00e0 toute exigence provinciale ou territoriale.</p>\n\n<p>En vertu de la <a href=\"/francais/reg/jredirect2.shtml?drga\"><i>Loi sur les aliments et drogues</i></a> et de son r\u00e8glement d'application, il est obligatoire de d\u00e9clarer sur l'\u00e9tiquette le contenu d'alcool de toute boisson dont le titre alcoom\u00e9trique volumique (TAV) est d'au moins 1,1\u00a0%.</p>\n\n<p>Les provinces et les territoires supervisent le contr\u00f4le, la distribution et la vente de boissons alcoolis\u00e9es sur leur propre territoire et peuvent imposer des exigences suppl\u00e9mentaires. Par exemple, dans la majorit\u00e9 des provinces et des territoires, une boisson dont le TAV est sup\u00e9rieur \u00e0 0,5 % serait consid\u00e9r\u00e9e comme un produit alcoolis\u00e9 et ferait l'objet d\u2019une vente contr\u00f4l\u00e9e.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) continue d'aider les entreprises alimentaires \u00e0 se conformer \u00e0 la r\u00e9glementation. Elle prend des mesures d'application de la loi lorsque tout aliment n'est pas conforme aux lois pertinentes. Vous trouverez les exigences en mati\u00e8re d'\u00e9tiquetage pour les boissons alcoolis\u00e9es sur le site Web de l'ACIA.</p>\n\n<p>Si vous avez d'autres questions, veuillez communiquer avec votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l\u2019ACIA</a> pour obtenir de plus amples renseignements.</p>\n\n<h2>Renseignements \u00e0 l'appui</h2>\n<ul>\n<li>Fiche d'information de Sant\u00e9 Canada concernant <a href=\"https://www.canada.ca/fr/sante-canada/services/publications/aliments-et-nutrition/ethanol-boissons-fermentees-non-alcoolisees.html\">l'\u00e9thanol dans les boissons ferment\u00e9es non alcoolis\u00e9es</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/fra/1383607266489/1383607344939\">Outil d'\u00e9tiquetage pour l'industrie</a></li>\n<li><a href=\"/fra/1392909001375/1392909133296\">Exigences en mati\u00e8re d'\u00e9tiquetage des boissons alcoolis\u00e9es</a></li></ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}