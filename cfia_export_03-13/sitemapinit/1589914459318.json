{
    "dcr_id": "1589914459022",
    "title": {
        "en": "Food inspection guidance: sample collection",
        "fr": "Orientation d'inspection des aliments : collecte d'\u00e9chantillons"
    },
    "html_modified": "2024-03-12 3:58:12 PM",
    "modified": "2023-03-30",
    "issued": "2020-06-02",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sample_collection_procedures_1589914459022_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sample_collection_procedures_1589914459022_fra"
    },
    "ia_id": "1589914459318",
    "parent_ia_id": "1542313762789",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1542313762789",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food inspection guidance: sample collection",
        "fr": "Orientation d'inspection des aliments : collecte d'\u00e9chantillons"
    },
    "label": {
        "en": "Food inspection guidance: sample collection",
        "fr": "Orientation d'inspection des aliments : collecte d'\u00e9chantillons"
    },
    "templatetype": "content page 1 column",
    "node_id": "1589914459318",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1542313686757",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/",
        "fr": "/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food inspection guidance: sample collection",
            "fr": "Orientation d'inspection des aliments : collecte d'\u00e9chantillons"
        },
        "description": {
            "en": "This page provides operational guidance to conduct activities related to permissions. It contains food and commodity-specific guidance for export certification, point of entry violations, import clearance procedures and other inspection activities.",
            "fr": "Cette page contient des documents d'orientation op\u00e9rationnelle n\u00e9cessaires \u00e0 la conduite d'activit\u00e9s reli\u00e9es aux permissions. Elle contient des documents d'orientation op\u00e9rationnelle sp\u00e9cifiques aux aliments et \u00e0 certains produits pour la certification \u00e0 l'exportation, les infractions aux point d'entr\u00e9s, les proc\u00e9dures de rel\u00e2che \u00e0 l'importation et d'autres activit\u00e9s d'inspection."
        },
        "keywords": {
            "en": "Standard permissions procedures, SPP",
            "fr": "Proc\u00e9dures de permissions standardis\u00e9es, PPS"
        },
        "dcterms.subject": {
            "en": "food inspection,inspection,processing",
            "fr": "inspection des aliments,inspection,traitement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-06-02",
            "fr": "2020-06-02"
        },
        "modified": {
            "en": "2023-03-30",
            "fr": "2023-03-30"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food inspection guidance: sample collection",
        "fr": "Orientation d'inspection des aliments : collecte d'\u00e9chantillons"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>Find operational guidance for planned, as required and official food samples which are submitted to the lab for analysis. This may include samples required for national sampling plans, compliance verification, complaints and investigations, surveys and surveillance.</p>\n<h2>Common inspection guidance</h2>\n<p>The following guidance is common to all sample collection inspection tasks as well as <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/preventive-control-inspection/eng/1589914617060/1589914617395\">preventive control inspection</a> and <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/commodity-inspection/eng/1589914517149/1589914517466\">commodity inspection</a> tasks</p>\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/standard-inspection-process/eng/1545435489013/1545435489265\">Standard inspection process</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/biosecurity-for-inspection-activities/eng/1555089971916/1555089972210\">Biosecurity for inspection activities</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/calibration-procedures/eng/1540301411823/1540302234709\">Calibration procedures for common equipment used by the CFIA Inspectorate</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/electronic-notes/eng/1542727784002/1542727784262\">Electronic notes</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/sanitation-decontamination-of-mobile-devices/eng/1543250752218/1543250911678\">Sanitation and/or decontamination of mobile devices for routine operations</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/digital-recording-devices/eng/1543525010666/1543525010934\">Use of digital recording (camera, video, audio) devices during inspections</a></li>\n</ul>\n<h2>Sample collection inspection</h2>\n<h3>Food sample collection guidance</h3>\n<p>The following guidance is common to all food and environmental sample collection inspection tasks</p>\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/food-sample-collection/eng/1540234969218/1540235089869\">Operational guideline: Food sample collection</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/planned-food-sample-collection/eng/1623253523858/1623253524529\">Operational procedure: Planned food sample collection</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/as-required-food-sample-collection/eng/1653062252765/1653062253358\">Operational procedure: As required food sample collection</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/official-food-sample-collection/eng/1653064227947/1653064228166\">Operational procedure: Official food sample collection</a></li>\n</ul>\n<h3>Commodity specific sample collection</h3>\n<h4>Fish and seafood</h4>\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/developing-a-mbmcp/eng/1540570455383/1540570455664\">Developing a marine bio-toxin monitoring control plan</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/main-page/eng/1542739573232/1542739655742\">Implementation of Memorandum of Understanding with samplers for the delivery of the Canadian Shellfish Sanitation Program</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/monitoring-for-marine-biotoxins-in-geoduck/eng/1541104736952/1541104737206\">Monitoring for marine bio-toxins in Geoduck in British Columbia</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Trouvez des orientations op\u00e9rationnelles pour la collecte d'\u00e9chantillons d'aliments planifi\u00e9s, ceux pr\u00e9lev\u00e9s au besoins et les \u00e9chantillons officiels qui sont soumis au laboratoire pour analyse. Cela peut inclure les \u00e9chantillons requis pour les plans d'\u00e9chantillonnage nationaux, la v\u00e9rification de la conformit\u00e9, les plaintes et les investigations, les enqu\u00eates et la surveillance.</p>\n<h2>Orientation d'inspection commune</h2>\n<p>Les documents d'orientation suivants sont communs \u00e0 toutes les t\u00e2ches d'inspection pour la collecte d'\u00e9chantillons ainsi qu'aux taches d'<a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/preventive-control-inspection/eng/1589914617060/1589914617395\">inspection de contr\u00f4le pr\u00e9ventif</a> et d'<a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/commodity-inspection/eng/1589914517149/1589914517466\">inspection des produits</a>.</p>\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/processus-d-inspection-standardise/fra/1545435489013/1545435489265\">Processus d'inspection standardis\u00e9</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/biosecurite-pour-les-activites-d-inspection/fra/1555089971916/1555089972210\">Bios\u00e9curit\u00e9 pour les activit\u00e9s d'inspection</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/procedures-d-etalonnage/fra/1540301411823/1540302234709\">Proc\u00e9dure d'\u00e9talonnage des instruments couramment utilis\u00e9s par les inspecteurs de l'ACIA</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/notes-electroniques/fra/1542727784002/1542727784262\">Notes \u00e9lectroniques</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/desinfection-des-appareils-mobiles-utilises/fra/1543250752218/1543250911678\">D\u00e9sinfection et/ou d\u00e9contamination des appareils mobiles utilis\u00e9s pour les op\u00e9rations courantes</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/des-appareils-d-enregistrement-numerique/fra/1543525010666/1543525010934\">Utilisation des appareils d'enregistrement num\u00e9rique (photographie, vid\u00e9os, enregistrements audio) au cours des inspections</a></li>\n</ul>\n<h2>Collecte d'\u00e9chantillons</h2>\n<h3>Orientation pour la collecte d'\u00e9chantillons d'aliments</h3>\n<p>Les documents d'orientation suivants sont communs \u00e0 toutes les t\u00e2ches d'inspection de collecte d'\u00e9chantillons alimentaires et environnementaux.</p>\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/collecte-d-echantillons-d-aliments/fra/1540234969218/1540235089869\">Orientation op\u00e9rationnelle\u00a0: Collecte d'\u00e9chantillons d'aliments</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/collecte-d-echantillons-planifies-d-aliments/fra/1623253523858/1623253524529\">Proc\u00e9dure op\u00e9rationnelle\u00a0:  Collecte d'\u00e9chantillons planifi\u00e9s d'aliments</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/collecte-d-echantillons-d-aliments-au-besoin/fra/1653062252765/1653062253358\">Proc\u00e9dure op\u00e9rationnelle\u00a0: Collecte d'\u00e9chantillons d'aliments au besoin</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/collecte-d-echantillons-officiels-d-aliments/fra/1653064227947/1653064228166\">Proc\u00e9dure op\u00e9rationnelle\u00a0: Collecte d'\u00e9chantillons officiels d'aliments</a></li>\n</ul>\n<h3>Collecte d'\u00e9chantillons sp\u00e9cifiques \u00e0 certains produits</h3>\n<h4>Poisson et fruits de mer</h4>\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/elaboration-d-un-pccsm/fra/1540570455383/1540570455664\">\u00c9laboration d'un plan de contr\u00f4le de la surveillance des bio-toxines marines</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/page-principale/fra/1542739573232/1542739655742\">Mise en \u0153uvre des protocoles d'entente conclus avec les \u00e9chantillonneurs pour la prestation du Programme canadien de contr\u00f4le de la salubrit\u00e9 des mollusques</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/surveillance-des-biotoxines-marines-dans-les-panop/fra/1541104736952/1541104737206\">Surveillance des biotoxines marines dans les panopes en Colombie-Britannique</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}