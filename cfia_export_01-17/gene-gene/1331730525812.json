{
    "dcr_id": "1331730525812",
    "title": {
        "en": "African-rue - <i lang=\"la\">Peganum harmala</i>",
        "fr": "Rue sauvage - <i lang=\"la\">Peganum harmala</i>"
    },
    "html_modified": "2012-03-14 09:08",
    "modified": "9-8-2016",
    "issued": "14-3-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_peganum_1331730525812_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_peganum_1331730525812_fra"
    },
    "parent_ia_id": "1331614823132",
    "ia_id": "1331730623392",
    "parent_node_id": "1331614823132",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "African-rue - <i lang=\"la\">Peganum harmala</i>",
        "fr": "Rue sauvage - <i lang=\"la\">Peganum harmala</i>"
    },
    "label": {
        "en": "African-rue - <i lang=\"la\">Peganum harmala</i>",
        "fr": "Rue sauvage - <i lang=\"la\">Peganum harmala</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331730623392",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331614724083",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/african-rue/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/rue-sauvage/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "African-rue - Peganum harmala",
            "fr": "Rue sauvage - Peganum harmala"
        },
        "description": {
            "en": "African-rue is persistent, dominant and difficult to control in suitable habitats such as dry rangelands.",
            "fr": "Toxique pour les humains et le b\u00e9tail, la rue sauvage est une plante persistante, dominante et difficile \u00e0 \u00e9liminer dans des habitats propices tels que les parcours naturels secs."
        },
        "keywords": {
            "en": "invasive plants, plant protection, Plant Protection Act, weed control, African-rue, peganum harmala",
            "fr": "plantes envahissantes, protection des v\u00e9g\u00e9taux, Loi sur la protection des v\u00e9g\u00e9taux, contre les mauvaises herbes, rue sauvage, peganum harmala"
        },
        "dcterms.subject": {
            "en": "crops,environment,inspection,plants,weeds,environmental protection",
            "fr": "cultures,environnement,inspection,plante,plante nuisible,protection de l'environnement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-14 09:08:48",
            "fr": "2012-03-14 09:08:48"
        },
        "modified": {
            "en": "2016-08-09",
            "fr": "2016-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "African-rue - Peganum harmala",
        "fr": "Rue sauvage - Peganum harmala"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/plant-health/seeds/seed-testing-and-grading/seeds-identification/peganum-harmala/eng/1397749468442/1397749511997\">Weed Seed - African-rue (<i lang=\"la\">Peganum harmala</i>)</a></p>\n<p>African-rue is  persistent, dominant and difficult to control in suitable habitats such as dry  rangelands. It is toxic to humans and livestock. Since the plants are  unpalatable and toxic, heavily infested rangelands lose much of their forage  value.</p>\n<h2>Where it's found</h2>\n<p>African-rue has not  been found in Canada. It is native to the desert regions of northern Africa,  Asia and southern and eastern Europe. Populations introduced into North America  are currently concentrated in Arizona, New Mexico and Texas. African-rue is  found mainly in dry grasslands and saline waste areas, but is also common along  roadsides, field edges and in degraded pastures. It prefers disturbed  environments.</p>\n<h2>What it looks like</h2>\n<p>African-rue is an  erect, bushy, perennial, herbaceous plant. It grows 30-80\u00a0<abbr title=\"centimetres\">cm</abbr> tall. Its leaves  are dark green and are about 2-5\u00a0<abbr title=\"centimetres\">cm</abbr> long. The flowers are white and about 2.5\u00a0<abbr title=\"centimetres\">cm</abbr> in diameter.</p>\n<h2>How it spreads</h2>\n<p>People plant  African-rue for medicinal purposes and may unintentionally move seeds and  pieces of rootstock with vehicles and machinery. The natural spread of  African-rue is primarily by seed\u2014animals disperse the seeds in their droppings  after feeding on the plant.</p>\n<h2>What you can do about it</h2>\n<ul>\n<li>Maintain healthy and  diverse pastures.</li>\n<li>Use clean,  high-quality seed that is certified if possible.</li>\n<li>Ensure machinery,  vehicles and tools are free of soil and plant parts before moving them from one  area to another.</li>\n<li>Contact your local  Canadian Food Inspection Agency (CFIA) office if you suspect you have found  this invasive plant. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will follow up and determine if further action is  needed.</li>\n</ul>\n\n<p>Learn more about <a href=\"/eng/1328325263410/1328325333845\">invasive species</a>.\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Whole African-rue plant\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_peganuma_1331667762184_eng.jpg\"><figcaption>Whole African-rue plant<br> <a href=\"http://twig.tamu.edu/keyindex.htm\">Weeds of Field Crops and Pastures</a></figcaption></figure>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"African-rue flowers\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_peganumb_1331667801246_eng.jpg\"><figcaption>African-rue flowers<br> <a href=\"http://twig.tamu.edu/keyindex.htm\">Weeds of Field Crops and Pastures</a></figcaption></figure>\n\n<figure><img alt=\"Single African-rue flower with leaves\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_peganumc_1331667845261_eng.jpg\"><figcaption>Single African-rue flower with leaves<br> Attribution: Russ Kleinman</figcaption></figure>\r\n        \r\n\r\n\r\n\r\n</p><div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/peganum-harmala/fra/1397749468442/1397749511997\">Semence de mauvaises herbe - Harmal (<i lang=\"la\">Peganum harmala</i>)</a></p>\n<p>Toxique pour les  humains et le b\u00e9tail, la rue sauvage est une plante persistante, dominante et  difficile \u00e0 \u00e9liminer dans des habitats propices tels que les parcours naturels  secs. Puisque les plants ont mauvais go\u00fbt, les parcours naturels lourdement  infest\u00e9s perdent une bonne partie de leur valeur fourrag\u00e8re.</p>\n<h2>O\u00f9 trouve-t-on cette esp\u00e8ce?</h2>\n<p>Personne n'a encore  d\u00e9couvert de rue sauvage au Canada. Originaires des r\u00e9gions d\u00e9sertiques du nord  de l'Afrique, de l'Asie ainsi que du sud et de l'est de l'Europe, ses  populations introduites en Am\u00e9rique du Nord se concentrent actuellement en  <span lang=\"en\">Arizona</span>, au Nouveau-Mexique et au Texas.</p>\n<p>La rue sauvage pousse  surtout dans les p\u00e2turages secs et les terrains salins en friche, et on la voit  souvent le long des routes, en bordure des champs et dans les p\u00e2turages  d\u00e9grad\u00e9s. Elle pr\u00e9f\u00e8re les milieux perturb\u00e9s.</p>\n<h2>\u00c0 quoi ressemble-t-elle?</h2>\n<p>La rue sauvage est  une herbac\u00e9e vivace buissonnante et droite qui atteint de 30 \u00e0 80\u00a0<abbr title=\"centim\u00e8tres\">cm</abbr> de  hauteur. Ses feuilles vert fonc\u00e9 font de 2 \u00e0 5\u00a0<abbr title=\"centim\u00e8tres\">cm</abbr>, et ses fleurs blanches ont  un diam\u00e8tre d'environ 2,5\u00a0<abbr title=\"centim\u00e8tres\">cm</abbr>.</p>\n<h2>Comment se propage-t-elle?</h2>\n<p>Les gens  plantent la rue sauvage pour ses propri\u00e9t\u00e9s m\u00e9dicinales et peuvent par m\u00e9garde  d\u00e9placer les graines et du mat\u00e9riel racinaire avec leurs v\u00e9hicules et leur  machinerie. La dispersion naturelle de la rue sauvage se  fait avant tout par semis; les animaux r\u00e9pandent les graines dans leurs  d\u00e9jections apr\u00e8s s'\u00eatre nourris de la plante.</p>\n<h2>Que pouvez-vous faire?</h2>\n<ul>\n<li>Entretenez des  p\u00e2turages sains et diversifi\u00e9s.</li>\n<li>Utilisez des semences  propres, de haute qualit\u00e9 et si possible certifi\u00e9es.</li>\n<li>Assurez-vous que la  machinerie, les v\u00e9hicules et les outils sont exempts de terre et de mat\u00e9riel  v\u00e9g\u00e9tal avant de les d\u00e9placer d'un endroit \u00e0 l'autre.</li>\n<li>Communiquez avec  votre bureau local de l'Agence canadienne d'inspection des aliments (ACIA) si  vous croyez avoir aper\u00e7u cette plante envahissante. L'<abbr title=\"Agence canadienne d\u2019inspection des aliments\">ACIA</abbr> fera un suivi et  d\u00e9terminera si d'autres mesures s'imposent.</li>\n</ul>\n\n<p>En savoir plus sur les <a href=\"/fra/1328325263410/1328325333845\">esp\u00e8ces envahissantes</a>.</p>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Plant complet de la rue sauvage\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_peganuma_1331667762184_fra.jpg\"><br><figcaption>Plant complet de la rue sauvage<br><a href=\"http://twig.tamu.edu/keyindex.htm\"><span lang=\"en\">Weeds of Field Crops and Pastures</span> (en anglais seulement)</a></figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Fleurs de la rue sauvage\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_peganumb_1331667801246_fra.jpg\"><br><figcaption>Fleurs de la rue sauvage<br><a href=\"http://twig.tamu.edu/keyindex.htm\"><span lang=\"en\">Weeds of Field Crops and Pastures</span> (en anglais seulement)</a></figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Fleur et feuilles de la rue sauvage\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_peganumc_1331667845261_fra.jpg\"><br><figcaption>Fleur et feuilles de la rue sauvage<br> Source\u00a0: Russ Kleinman</figcaption>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}