{
    "dcr_id": "1643140920906",
    "title": {
        "en": "Transport of lactating animals",
        "fr": "Transport d'animaux en lactation"
    },
    "html_modified": "2024-03-12 3:59:12 PM",
    "modified": "2022-02-01",
    "issued": "2022-02-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/transp_lact_animals_graphic_1643140920906_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/transp_lact_animals_graphic_1643140920906_fra"
    },
    "ia_id": "1643140921812",
    "parent_ia_id": "1300460096845",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1300460096845",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Transport of lactating animals",
        "fr": "Transport d'animaux en lactation"
    },
    "label": {
        "en": "Transport of lactating animals",
        "fr": "Transport d'animaux en lactation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1643140921812",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300460032193",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/humane-transport/transport-of-lactating-animals/",
        "fr": "/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/transport-d-animaux-en-lactation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Transport of lactating animals",
            "fr": "Transport d'animaux en lactation"
        },
        "description": {
            "en": "Anyone transporting a lactating animal in Canada needs to meet specific requirements to make sure the animal is fit for the intended journey.",
            "fr": "Quiconque transporte un animal en lactation au Canada doit satisfaire \u00e0 des exigences sp\u00e9cifiques afin de s'assurer que l'animal est apte \u00e0 tol\u00e9rer le voyage pr\u00e9vu."
        },
        "keywords": {
            "en": "Animals, Animal Health, Livestock, transport, Canada",
            "fr": "Animaux, Sant\u00e9 des animaux, Transport, b\u00e9tail, Canada"
        },
        "dcterms.subject": {
            "en": "animal,animal diseases,animal health,animal inspection,inspection,livestock,regulations,transport",
            "fr": "animal,maladie animale,sant\u00e9 animale,inspection des animaux,inspection,b\u00e9tail,r\u00e9glementations,transport"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-02-01",
            "fr": "2022-02-01"
        },
        "modified": {
            "en": "2022-02-01",
            "fr": "2022-02-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Transport of lactating animals",
        "fr": "Transport d'animaux en lactation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/transp_lact_animals_graphic_1643141746594_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Transport of lactating animals in Portable document format\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Transport of lactating animals</span> <span class=\"gc-dwnld-info nowrap\">(PDF, 189 kb)</span></p>\n</div>\n</div>\n</div></a>\n</div>\n</div>\n\n<figure class=\"mrgn-bttm-lg\"> \n<img alt=\"Transport of lactating animals - description follows\" class=\"img-responsive\" src=\"/DAM/DAM-animals-animaux/STAGING/images-images/transp_lact_animals_graphic_1643141388469_eng.jpg\"> \n<details> \n<summary>Transport of lactating animals - Text version</summary>\n<p>Anyone transporting a lactating animal in Canada needs to meet specific requirements to make sure the animal is fit for the intended journey.</p>\n<h2 class=\"h5\">Step 1: assess the lactating animal before transporting it.</h2>\n<p>Is the animal comfortable, with no signs of pain or udder engorgement? (signs of engorgement can include hardness, redness, pain, heat of the udder)</p>\n<ul>\n<li><strong>If no</strong>, the animal is considered unfit and must not be transported. Animals with udder/mammary engorgement are considered unfit for transport.</li>\n<li><strong>If yes</strong>, move on to Step 2.</li>\n</ul>\n<h2 class=\"h5\">Step 2: assess the risk factors related to lactation during transport.</h2>\n<p>Are any of the following practices being used to prevent mammary engorgement if the animal has not been dried off?</p>\n<ul>\n<li>Reaching her final destination before becoming engorged</li>\n<li>Travelling with her suckling offspring</li>\n<li>Being milked en-route</li>\n</ul>\n<ul class=\"mrgn-tp-lg\">\n<li><strong>If no or unknown</strong>, the animal is considered compromised and must be transported as such. Maximum of 12 hours without feed, water, or rest. The animal is compromised and will need to be milked regularly to prevent engorgement.</li>\n<li><strong>If yes</strong>, animal may be transported. Maximum of 36 hours without feed, water or rest for ruminants. Move on to Step 3.</li>\n</ul>\n<h2 class=\"h5\">Step 3: Monitor en-route.</h2>\n<p>Is the animal still okay? Keep an eye on animals in heavy lactation since they are at higher risk of becoming engorged or feeling pain; monitor accordingly!</p>\n<ul>\n<li><strong>If no</strong>, take the following actions because this animal has become compromised or unfit en-route:\n<ul>\n<li>take measures to prevent suffering</li>\n<li>go directly to the nearest suitable place for care, humane killing or humanely kill in conveyance</li>\n</ul>\n</li>\n<li><strong>If yes</strong>, continue the transport journey and continue to monitor.</li>\n</ul>\n<h2 class=\"h5\">Definitions</h2>\n<p><strong>Unfit:</strong> an unfit animal can only be transported directly to a place to receive veterinary care and under the recommendation of a veterinarian.</p>\n<p><strong>Compromised:</strong> a compromised animal can only be transported directly for slaughter or to a place to receive care for its condition. It cannot be transported to an assembly centre or auction market.</p>\n<h2 class=\"h5\">Requirements if transporting an unfit or compromised animal</h2>\n<ul>\n<li>Isolate the animal</li>\n<li>Load or unload the animal alone without negotiating interior ramps</li>\n<li>Take special measures to prevent unnecessary suffering</li>\n</ul>\n</details>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/transp_lact_animals_graphic_1643141746594_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Transport d'animaux en lactation en format PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Transport d'animaux en lactation</span> <span class=\"gc-dwnld-info nowrap\">(PDF, 105 ko)</span></p>\n</div>\n</div>\n</div></a>\n</div>\n</div>\n\n<figure class=\"mrgn-bttm-lg\"> \n<img alt=\"Transport d'animaux en lactation - description ci-dessous\" class=\"img-responsive\" src=\"/DAM/DAM-animals-animaux/STAGING/images-images/transp_lact_animals_graphic_1643141388469_fra.jpg\"> \n<details> \n<summary>Transport d'animaux en lactation - Version textuelle</summary>\n<p>Quiconque transporte un animal en lactation au Canada doit satisfaire \u00e0 des exigences sp\u00e9cifiques afin de s'assurer que l'animal est apte \u00e0 tol\u00e9rer le voyage pr\u00e9vu.</p>\n<h2 class=\"h5\">1\u00e8re \u00e9tape: \u00e9valuer l'animal en lactation avant de le transporter.</h2>\n<p>Est-ce que l'animal est confortable, sans signe de douleur ou d'engorgement mammaire? (les signes d'engorgement peuvent inclure une mamelle dure, rouge, douloureuse, chaude)</p>\n<ul>\n<li><strong>Si non</strong>, l'animal est consid\u00e9r\u00e9 comme inapte et ne doit pas \u00eatre transport\u00e9. Les animaux pr\u00e9sentant un engorgement mammaire sont consid\u00e9r\u00e9s inaptes au transport.</li>\n<li><strong>Si oui</strong>, passez \u00e0 la 2e \u00e9tape.</li>\n</ul>\n<h2 class=\"h5\">2e \u00e9tape: \u00e9valuez les facteurs de risques associ\u00e9s \u00e0 la lactation pendant le transport.</h2>\n<p>Est-ce que l'une des pratiques suivantes sera utilis\u00e9e pour pr\u00e9venir l'engorgement mammaire si l'animal n'a pas \u00e9t\u00e9 tari avant son transport?</p>\n<ul>\n<li>Atteinte de sa destination finale avant de devenir engorg\u00e9</li>\n<li>Voyage avec sa prog\u00e9niture se nourrissant \u00e0 la mamelle</li>\n<li>Traite en cours de route</li>\n</ul>\n<ul class=\"mrgn-tp-lg\">\n<li><strong>Si non ou ne sait pas</strong>, l'animal est fragilis\u00e9 et doit \u00eatre transport\u00e9 comme tel. Maximum de 12 h sans aliment, eau ni repos. L'animal est fragilis\u00e9 et devra \u00eatre trait r\u00e9guli\u00e8rement afin d'emp\u00eacher tout engorgement.</li>\n<li><strong>Si oui</strong>, allez-y! Maximum de 36 h sans aliment, eau ni repos pour les ruminants. Passez \u00e0 la 3e \u00e9tape.</li>\n</ul>\n<h2 class=\"h5\">3e \u00e9tape: surveillez en cours de transport.</h2>\n<p>Est que l'animal est OK? Soyez particuli\u00e8rement attentif aux animaux en p\u00e9riode de forte lactation car ils sont plus \u00e0 risque de devenir engorg\u00e9s ou en douleur; surveillez-les en cons\u00e9quence!</p>\n<ul>\n<li><strong>Si non</strong>, prenez les mesures suivantes car cet animal est devenu fragilis\u00e9 ou inapte en cours de route:\n<ul>\n<li>prenez des mesures pour \u00e9viter les souffrances</li>\n<li>allez directement au lieu appropri\u00e9 le plus proche o\u00f9 l'animal pourra recevoir des soins, ou \u00eatre tu\u00e9 sans cruaut\u00e9, ou tuez-le sans cruaut\u00e9 dans le v\u00e9hicule.</li>\n</ul>\n</li>\n<li><strong>Si oui</strong>, poursuivez le voyage.</li>\n</ul>\n<h2 class=\"h5\">D\u00e9finitions</h2>\n<p><strong>Inapte:</strong> un animal inapte peut seulement \u00eatre transport\u00e9 directement \u00e0 un endroit o\u00f9 il recevra des soins v\u00e9t\u00e9rinaires et sous la recommandation d'un(e) v\u00e9t\u00e9rinaire.</p>\n<p><strong>Fragilis\u00e9:</strong> un animal fragilis\u00e9 peut uniquement \u00eatre transport\u00e9 directement pour fin d'abattage ou \u00e0 un endroit o\u00f9 il pourra recevoir les soins requis pour son \u00e9tat. Il ne peut \u00eatre transport\u00e9 \u00e0 un centre de rassemblement ou un march\u00e9 de vente aux ench\u00e8res (encan).</p>\n<h2 class=\"h5\">Exigences lors du transport d'un animal inapte ou fragilis\u00e9</h2>\n<ul>\n<li>Isolez l'animal;</li>\n<li>Embarquez et d\u00e9barquez l'animal individuellement sans n\u00e9gocier de rampes int\u00e9rieures;</li>\n<li>Mettez en place des mesures sp\u00e9ciales afin d'\u00e9viter des souffrances inutiles \u00e0 l'animal.</li>\n</ul>\n</details>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}