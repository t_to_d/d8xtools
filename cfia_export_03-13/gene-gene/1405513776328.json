{
    "dcr_id": "1405513776328",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Solanum carolinense</i> (Horse nettle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Solanum carolinense</i> (Morelle de la Caroline)"
    },
    "html_modified": "2024-03-12 3:56:47 PM",
    "modified": "2017-11-01",
    "issued": "2014-11-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_solanum_carolinense_1405513776328_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_solanum_carolinense_1405513776328_fra"
    },
    "ia_id": "1405513777296",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Solanum carolinense</i> (Horse nettle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Solanum carolinense</i> (Morelle de la Caroline)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Solanum carolinense</i> (Horse nettle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Solanum carolinense</i> (Morelle de la Caroline)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1405513777296",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/solanum-carolinense/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/solanum-carolinense/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Solanum carolinense (Horse nettle)",
            "fr": "Semence de mauvaises herbe : Solanum carolinense (Morelle de la Caroline)"
        },
        "description": {
            "en": "Fact sheet for Horse nettle (Solanum carolinense)",
            "fr": "Fiche de renseignements pour Morelle de Caroline (Solanum carolinense)"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Horse nettle, Solanum carolinense",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Morelle de Caroline, Solanum carolinense"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants,weeds",
            "fr": "cultures,grain,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-11-06",
            "fr": "2014-11-06"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Solanum carolinense (Horse nettle)",
        "fr": "Semence de mauvaises herbe : Solanum carolinense (Morelle de la Caroline)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Solanaceae</i></p>\n\n<h2>Synonym</h2>\n<p>Ball nettle</p>\n\n<h2>Common Name</h2>\n<p>Horse nettle</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"Ontario\">ON</abbr> and <abbr title=\"Quebec\">QC</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n\n<p><strong>Worldwide:</strong> Native to eastern North America, from Ontario to northern Mexico, and introduced in Japan and India (Bassett and Munro 1986<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>, <abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of Life Cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or Fruit Type</h2>\n<p>Seed</p>\n\n<h2>Identification Features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 1.7 - 3.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 1.4 - 2.1 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed thickness: 0.2 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Shape</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Oval shaped seed, flattened</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Faint pattern of wavy-edged cells on seed surface can be seen under magnification</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed yellow to orange</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Hilum is a slit along the narrow edge of seed and is 0.5 <abbr title=\"millimetres\">mm</abbr> long</li>\n</ul>\n\n<h2>Habitat and Crop Association</h2>\n\n<p>Cultivated fields, pastures, gardens, nurseries, riverbanks, roadsides and disturbed areas (Bassett and Munro 1986<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>, Darbyshire 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). Most frequently found in corn and grain fields, and to a lesser extent in pastures, alfalfa, potatoes, soybeans and tomatoes (Bassett and Munro 1986<sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>General Information</h2>\n\n<p>Horse nettle may produce up to 5000 seeds per plant. The seeds are apparently spread through livestock manure and the berries and horizontal roots are spread as a result of harvesting operations (Bassett and Munro 1986<sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n<h2>Similar Species</h2>\n\n<h3>Silverleaf nightshade (<i lang=\"la\">Solanum elaeagnifolium</i>)</h3>\n<ul>\n<li>Silverleaf nightshade seeds are a similar flattened oval shape, and smooth surface as horse nettle seeds.</li>\n\n<li>Silverleaf nightshade seeds (length: 3.0 - 4.0 <abbr title=\"millimetres\">mm</abbr>; width: 2.3 - 3.5 <abbr title=\"millimetres\">mm</abbr>) are generally larger than horse nettle seeds, brown coloured and the surface pattern is long, wavy lines around the outside of the seed.</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img3_1405428600852_eng.jpg\" alt=\"\">\n<figcaption>Horse nettle (<i lang=\"la\">Solanum carolinense</i>) seeds\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img1_1405428532914_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Horse nettle (<i lang=\"la\">Solanum carolinense</i>) seed </figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img2_1405428563617_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Horse nettle (<i lang=\"la\">Solanum carolinense</i>) seed\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img4_1509564855642_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Horse nettle (<i lang=\"la\">Solanum carolinense</i>) seed\n</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_elaeagnifolium_img1_1405428665617_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Silverleaf nightshade (<i lang=\"la\">Solanum eleagnifolium</i>) seeds</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_Solanum_elaeagnifolium_img1_1397672204312_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Silverleaf nightshade (<i lang=\"la\">Solanum eleagnifolium</i>) seed</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong>Bassett, I. J. and Munro, D. B. 1986.</strong> The biology of Canadian weeds. 78. Solanum carolinense L. and S. rostratum Dunal. Canadian Journal of Plant Science 66: 977-991.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Solanaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Morelle de la Caroline</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> Pr\u00e9sente en <abbr title=\"Ontario\">Ont.</abbr> et au <abbr title=\"Qu\u00e9bec\">Qc</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Indig\u00e8ne de l'Est de l'Am\u00e9rique du Nord, depuis l'Ontario jusque dans le Nord du Mexique, et introduite au Japon et en Inde (Bassett et Munro, 1986<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>; <abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 1,7 \u00e0 3,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 1,4 \u00e0 2,1 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>\u00c9paisseur de la graine\u00a0: 0,2 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Graine elliptique, aplatie</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Motif peu marqu\u00e9 de cellules \u00e0 bords sinueux sur la surface de la graine, visible sous grossissement</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Graine de couleur jaune \u00e0 orange</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Le hile est une mince fente sur le bord \u00e9troit de la graine et mesure 0,5 <abbr title=\"millim\u00e8tre\">mm</abbr> de longueur</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n\n<p>Champs cultiv\u00e9s, p\u00e2turages, jardins, p\u00e9pini\u00e8res, berges de rivi\u00e8res, bords de chemin et terrains perturb\u00e9s (Bassett et Munro, 1986<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>; Darbyshire, 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>). Plus fr\u00e9quemment observ\u00e9e dans le ma\u00efs et les c\u00e9r\u00e9ales, et dans une moindre mesure, dans les p\u00e2turages et les cultures de luzerne, de pommes de terre, de soja et de tomate (Bassett et Munro, 1986<sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n\n<p>La morelle de Caroline peut produire jusqu'\u00e0 5000 graines par plante. Les graines sont apparemment dispers\u00e9es par l\u2019\u00e9pandage de fumier, tandis que les fruits et les racines horizontales le sont au moment de la r\u00e9colte (Bassett et Munro, 1986<sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n\n<h3>Morelle jaune (<i lang=\"la\">Solanum elaeagnifolium</i>)</h3>\n<ul>\n<li>Les graines de la morelle jaune ressemblent \u00e0 celles de la morelle de la Caroline par leur forme elliptique aplatie et leur surface lisse.</li>\n\n<li>Les graines de la morelle jaune (longueur\u00a0: 3,0 \u00e0 4,0 <abbr title=\"millim\u00e8tre\">mm</abbr>; largeur\u00a0: 2,3 \u00e0 3,5 <abbr title=\"millim\u00e8tre\">mm</abbr>) sont g\u00e9n\u00e9ralement plus larges que celles de la morelle de la Caroline, de couleur brune et le motif de la surface est constitu\u00e9 de longues lignes sinueuses sur le pourtour de la graine.</li>\n</ul>\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img3_1405428600852_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Morelle de Caroline (<i lang=\"la\">Solanum carolinense</i>), graines\n</figcaption>\n</figure>\n\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img1_1405428532914_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Morelle de Caroline (<i lang=\"la\">Solanum carolinense</i>), graine</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img2_1405428563617_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Morelle de Caroline (<i lang=\"la\">Solanum carolinense</i>), graine</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_carolinense_img4_1509564855642_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Morelle de Caroline (<i lang=\"la\">Solanum carolinense</i>), graine</figcaption>\n</figure>\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_solanum_elaeagnifolium_img1_1405428665617_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Morelle jaune (<i lang=\"la\">Solanum eleagnifolium</i>), graines\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_Solanum_elaeagnifolium_img1_1397672204312_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Morelle jaune (<i lang=\"la\">Solanum eleagnifolium</i>), graine\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>Bassett, I. J. and Munro, D. B. 1986.</strong> The biology of Canadian weeds. 78. Solanum carolinense L. and S. rostratum Dunal. Canadian Journal of Plant Science 66: 977-991.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}