{
    "dcr_id": "1658109930314",
    "title": {
        "en": "Reporting humane animal transport concerns",
        "fr": "Signalement des pr\u00e9occupations li\u00e9es au transport sans cruaut\u00e9 des animaux"
    },
    "html_modified": "19-7-2022",
    "modified": "19-7-2022",
    "issued": "15-7-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/rep_humane_anim_transport_concerns_1658109930314_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/rep_humane_anim_transport_concerns_1658109930314_fra"
    },
    "parent_ia_id": "1300460096845",
    "ia_id": "1658109930689",
    "parent_node_id": "1300460096845",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Reporting humane animal transport concerns",
        "fr": "Signalement des pr\u00e9occupations li\u00e9es au transport sans cruaut\u00e9 des animaux"
    },
    "label": {
        "en": "Reporting humane animal transport concerns",
        "fr": "Signalement des pr\u00e9occupations li\u00e9es au transport sans cruaut\u00e9 des animaux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1658109930689",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300460032193",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/humane-transport/reporting-humane-animal-transport-concerns/",
        "fr": "/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/signalement-des-preoccupations-liees-au-transport-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Reporting humane animal transport concerns",
            "fr": "Signalement des pr\u00e9occupations li\u00e9es au transport sans cruaut\u00e9 des animaux"
        },
        "description": {
            "en": "The transport of animals is regulated to prevent injury or suffering of all animals.",
            "fr": "Le transport des animaux est r\u00e9glement\u00e9 afin d'\u00e9viter les blessures ou les souffrances de tous les animaux."
        },
        "keywords": {
            "en": "Animals, Animal Health, transportation, Reporting, humane, animal transport, concerns",
            "fr": "Animaux, Sant\u00e9 des animaux, transport, Signalement, pr\u00e9occupations li\u00e9es, transportdes animaux, sans cruaut\u00e9"
        },
        "dcterms.subject": {
            "en": "animal,inspection,animal health,transport",
            "fr": "animal,inspection,sant\u00e9 animale,transport"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "modified": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Reporting humane animal transport concerns",
        "fr": "Signalement des pr\u00e9occupations li\u00e9es au transport sans cruaut\u00e9 des animaux"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=34#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section>\n<p>The transport of animals is regulated to prevent injury or suffering of all animals. All those involved in transporting or causing animals to be transported, must ensure that every animal being loaded is fit for the trip and treated humanely.</p>\n\n<h2>If you see something, say something</h2>\n\n<p>If you believe you have witnessed someone who is not following the regulations regarding loading, transporting and unloading animals (such as rough handling, inadequate weather protection, or overcrowding), do the following:</p>\n\n<ul>\n<li>gather as many details as possible (for example, date, time, location, license plate number)\n<ul>\n<li>report this information to the CFIA as soon as possible by contacting your <a href=\"/eng/1300462382369/1300462438912\">local CFIA office</a></li>\n</ul>\n</li>\n</ul>\n\n<h2>Additional resources</h2>\n\n<ul>\n\n<li><a href=\"/about-cfia/media-relations/prosecution-bulletins/eng/1298575869119/1299852705293\">CFIA's prosecution bulletins</a></li>\n<li><a href=\"/about-cfia/transparency/regulatory-transparency-and-openness/compliance-and-enforcement/amps/eng/1324319195211/1324319534243\">CFIA's Administrative Monetary Penalties</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/eng/1300460032193/1300460096845\">Humane transport and animal welfare</a></li>\n<li><a href=\"/importing-food-plants-or-animals/pets/eng/1326600389775/1326600500578\">Bringing pets to Canada: rules for importing or travelling with animals</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/eng/1300388920375/1300388985791\">Exporting terrestrial animals and animal products</a></li>\n<li><a href=\"http://www.nfacc.ca/codes-of-practice\">Codes of practice for the care and handling of farm animals</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/humane-transport-overview/eng/1363740981698/1363741144174\">Legal requirements</a> (humane transportation overview)</li>\n<li><a href=\"/importing-food-plants-or-animals/pets/transporting-pets/eng/1383445120844/1383445222475\">Transporting pets</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/cold-weather/eng/1394551047242/1394551048336\">Transporting animals during cold weather</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/transporting-animals/eng/1374601368429/1374601895769\">Transport animals during hot and humid weather</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=34#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Le transport des animaux est r\u00e9glement\u00e9 afin d'\u00e9viter les blessures ou les souffrances de tous les animaux. Tous ceux impliqu\u00e9s dans le transport d'animaux ou occasionnant le transport d'animaux doivent s'assurer que tous les animaux embarqu\u00e9s sont aptes \u00e0 effectuer le trajet et sont trait\u00e9s sans cruaut\u00e9.</p>\n\n<h2>Si vous \u00eates t\u00e9moin de quelque chose, dites-le nous</h2>\n\n<p>Voici la marche \u00e0 suivre si vous pensez avoir \u00e9t\u00e9 t\u00e9moin d'un incident de non-respect de la r\u00e9glementation en mati\u00e8re d'embarquement, de transport ou de d\u00e9barquement des animaux (par exemple\u00a0: manipulation brutale, protection insuffisante contre les intemp\u00e9ries ou entassement, etc.)\u00a0:</p>\n\n<ul>\n<li>notez autant de d\u00e9tails que possible (date, heure, endroit, num\u00e9ro de la plaque d'immatriculation);\n<ul>\n<li>transmettez ces renseignements \u00e0 l'ACIA le plus rapidement possible en communiquant avec <a href=\"/fra/1300462382369/1300462438912\">votre bureau local de l'ACIA</a>.</li>\n</ul>\n</li>\n</ul>\n\n<h2>Autres ressources</h2>\n\n<ul>\n\n<li><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/bulletins-judiciaires/fra/1298575869119/1299852705293\">Bulletins judiciaires publi\u00e9s par l'ACIA</a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/conformite-et-application-de-la-loi/sap/fra/1324319195211/1324319534243\">Sanctions administratives p\u00e9cuniaires inflig\u00e9es par l'ACIA</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/fra/1300460032193/1300460096845\">Transport sans cruaut\u00e9 et bien-\u00eatre des animaux</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/fra/1326600389775/1326600500578\">Apporter des animaux de compagnie au Canada\u00a0: exigences d'importation et de voyage pour les animaux</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/fra/1300388920375/1300388985791\">Exportation d'animaux terrestres et de produits d'origine animale</a></li>\n<li><a href=\"http://www.nfacc.ca/codes-de-pratiques\">Codes de pratiques pour les soins et la manipulation des animaux d'\u00e9levage</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/survol-du-transport-sans-cruaute-des-animaux/fra/1363740981698/1363741144174\">Exigences l\u00e9gales</a> (Survol du transport sans cruaut\u00e9 des animaux)</li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/transport-des-animaux-de-compagnie/fra/1383445120844/1383445222475\">Transport des animaux de compagnie</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/conditions-meteorologiques-froides/fra/1394551047242/1394551048336\">Transport des animaux par temps froid</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/transport-des-animaux/fra/1374601368429/1374601895769\">Transport des animaux par temps chaud et humide</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}