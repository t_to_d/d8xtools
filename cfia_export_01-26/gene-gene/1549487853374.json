{
    "dcr_id": "1549487853374",
    "title": {
        "en": "The Charlottetown Laboratory",
        "fr": "Le laboratoire de Charlottetown"
    },
    "html_modified": "2024-01-26 2:24:32 PM",
    "modified": "2023-08-23",
    "issued": "2019-02-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_charlottetown_1549487853374_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_charlottetown_1549487853374_fra"
    },
    "ia_id": "1549488448815",
    "parent_ia_id": "1494878085588",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1494878085588",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The Charlottetown Laboratory",
        "fr": "Le laboratoire de Charlottetown"
    },
    "label": {
        "en": "The Charlottetown Laboratory",
        "fr": "Le laboratoire de Charlottetown"
    },
    "templatetype": "content page 1 column",
    "node_id": "1549488448815",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1494878032804",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-laboratories/charlottetown/",
        "fr": "/les-sciences-et-les-recherches/nos-laboratoires/charlottetown/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The Charlottetown Laboratory",
            "fr": "Le laboratoire de Charlottetown"
        },
        "description": {
            "en": "The Charlottetown Laboratory specializes in diagnostic testing and research activities related to potato, grains, oilseeds and soil pathogens. It is a certified biocontainment facility that meets plant pest containment level 2 requirements and houses a multipurpose, segregated greenhouse and state-of-the-art bio-waste system.",
            "fr": "Le laboratoire de Charlottetown est sp\u00e9cialis\u00e9 dans les \u00e9preuves diagnostiques et les activit\u00e9s de recherche associ\u00e9es aux pathog\u00e8nes de la pomme de terre, des c\u00e9r\u00e9ales, des ol\u00e9agineux et du sol."
        },
        "keywords": {
            "en": "Charlottetown Laboratory, Laboratory",
            "fr": "laboratoire de Charlottetown, laboratoire"
        },
        "dcterms.subject": {
            "en": "education,information,educational guidance,educational resources",
            "fr": "\u00e9ducation,information,orientation scolaire,ressources p\u00e9dagogiques"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Strategic Communications Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des communications strat\u00e9giques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2023-08-23",
            "fr": "2023-08-23"
        },
        "type": {
            "en": "guide,educational material",
            "fr": "guide,mat\u00e9riel didactique"
        },
        "audience": {
            "en": "educators,business,government,general public,media,scientists",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,grand public,m\u00e9dia,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Charlottetown Laboratory",
        "fr": "Le laboratoire de Charlottetown"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_chrlttwn_text_1556738336226_eng.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">CFIA science laboratory brochure: The Charlottetown Laboratory <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2,400&nbsp;kb)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n-->\n\n<p>The Charlottetown Laboratory is in Mi'kma'ki Territory, the ancestral and unceded territory of the Mi'kmaq People, including the Abegweit First Nation.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">About the Canadian Food Inspection Agency</h2></summary>\n\n<p>The Canadian Food Inspection Agency (CFIA) is a science-based regulator with a mandate to safeguard the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">food</a> supply, protect the health of <a href=\"/plant-health/eng/1299162629094/1299162708850\">plants</a> and <a href=\"/animal-health/eng/1299155513713/1299155693492\">animals</a>, and support market access. The Agency relies on high-quality, timely and relevant science as the basis of its program design and regulatory decision-making. Scientific activities inform the Agency's understanding of risks, provide evidence for developing mitigation measures, and confirm the effectiveness of these measures.</p>\n\n<p>CFIA scientific activities include laboratory testing, research, surveillance, test method development, risk assessments and expert scientific advice. Agency scientists maintain strong partnerships with universities, industry, and federal, provincial and international counterparts to effectively carry out the CFIA's mandate.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_charlottetown_1498061447016_eng.jpg\" class=\"img-responsive\" alt=\"Photograph - Charlottetown Laboratory building entrance\">\n</div>\n<p>The Charlottetown Laboratory specializes in diagnostic testing and research activities related to potato, grains, oilseeds and soil pathogens. It is a certified biocontainment facility that meets plant pest containment level 2 requirements and houses a multipurpose, segregated greenhouse and state-of-the-art bio-waste system.</p>\n\n<p>The Charlottetown Laboratory is responsible for detecting and diagnosing plant diseases. It plays a key role in supporting the trade of Canadian plant goods through diagnostic testing and the development of technology and techniques that satisfy import, export and domestic requirements. The Charlottetown Laboratory helps ensure plants that enter Canada, as well as Canadian plants that are exported to other countries, are free of disease and pests.</p>\n\n<h2>What we do</h2>\n\n<h3>Services</h3>\n\n<ul>\n<li>Ensure new potato varieties imported into Canada are free from pests and diseases by facilitating the CFIA's Potato Post-Entry Quarantine Program.</li>\n<li>Approve private laboratories for the conduct of regulatory diagnostic testing activities under the CFIA's Seed Potato Approval Program.</li>\n<li>Provide scientific advice to support plant health programs, inspection staff, regulated parties and other government departments.</li>\n<li>Maintain accreditation for developing and validating test methods, evaluating test kits, and conducting non-routine testing.</li>\n</ul>\n\n<h3>Diagnostic testing</h3>\n\n<ul>\n<li>Conduct diagnostic testing in support of the Canadian Seed Potato Certification Program and international trade of Canadian seed potatoes, pulses and other plant commodities.</li>\n<li>Provide proficiency test samples, confirmatory diagnostic testing, test protocols and reference materials to private labs under the Seed Potato Approval Program.</li>\n</ul>\n\n\n<h3>Research and development</h3>\n\n<ul>\n<li>Conduct regulatory research to develop and validate new detection technologies and to increase knowledge of pathogen diversity and their ecological characteristics.</li>\n<li>Participate in collaborative studies with industry partners, commodity groups, academia, other federal and provincial departments, and national and international stakeholders.</li>\n</ul>\n\n<h2>Quality management</h2>\n\n<p>All CFIA laboratories are accredited in accordance with the International Standard ISO/IEC 17025, <i>General requirements for the competence of testing and calibration laboratories</i>. The Standards Council of Canada (SCC) provides accreditation for routine testing, test method development and non-routine testing, as identified on the laboratory's Scope of Accreditation on the <a href=\"https://www.scc.ca/en/accreditation/laboratories/canadian-food-inspection-agency-2\" title=\"Standards Council of Canada\">SCC</a> website. Accreditation formally verifies the CFIA's competence to produce accurate and reliable results. The results are supported by the development, validation and implementation of scientific methods, conducted by highly qualified personnel, using reliable products, services, and equipment, in a quality controlled environment. Participation in international proficiency testing programs further demonstrates that our testing is comparable to laboratories across Canada and around the world.</p>\n\n<h2>Physical address</h2>\n\n<p>Charlottetown Laboratory<br>\n<br>\n<br>\n</p>\n\n<h2>More information</h2>\n<ul>\n<li><a href=\"https://science.gc.ca/site/science/en/blogs/cultivating-science/new-technology-detect-and-identify-plant-diseases\">New technology to detect and identify plant diseases</a></li>\n<li>Article: <a href=\"/science-and-research/our-research-and-publications/this-spud-s-for-you/eng/1549645313475/1549645313725\">This spud's for YOU: CFIA's role in protecting Canadian potatoes</a></li>\n</ul>\n\n<div class=\"well\">\n<div class=\"row mrgn-bttm-lg mrgn-tp-md\">\n<div class=\"col-sm-4\"> <a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/tour-of-the-charlottetown-laboratory/eng/1686676836037/1686677431273\" class=\"ic-overlay\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/thumbnail_charlottetown_lab_tour_1686679998166_eng.jpg\" alt=\"\" class=\"img-responsive\"></a></div>\n<div class=\"col-sm-8\">\n<p><span class=\"glyphicon glyphicon-film\"></span><span class=\"wb-inv\">video</span>\u00a0<strong><a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/tour-of-the-charlottetown-laboratory/eng/1686676836037/1686677431273\">Testing for potato wart: Tour of the Charlottetown Laboratory</a></strong></p>\n<p>Duration: 3:10</p>\n<p>Take a tour of the Canadian Food Inspection Agency's Charlottetown Laboratory and get a closer look at how they help control, contain and prevent the spread of potato wart.</p>\n</div>\n</div>\n</div>\n\n<p>Learn about other <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_chrlttwn_text_1556738336226_fra.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">Brochure du laboratoire scientifique de l'ACIA&nbsp;: Le laboratoire de Charlottetown <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2 400&nbsp;ko)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n\n<div class=\"clearfix\"></div>\n-->\n<p>Le laboratoire de Charlottetown se trouve sur le territoire traditionnel non c\u00e9d\u00e9 du peuple Mi'kmaq, y compris la Premi\u00e8re Nation Abegweit.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">\u00c0 propos de l'Agence canadienne d'inspection des aliments</h2></summary>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est un organisme de r\u00e9glementation \u00e0 vocation scientifique dont le mandat est de pr\u00e9server l'<a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">approvisionnement alimentaire</a>, de prot\u00e9ger la sant\u00e9 des <a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">v\u00e9g\u00e9taux</a> et des <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">animaux</a> et de favoriser l'acc\u00e8s aux march\u00e9s. L'Agence s'appuie sur des donn\u00e9es scientifiques de grande qualit\u00e9, opportunes et pertinentes pour concevoir ses programmes et prendre ses d\u00e9cisions r\u00e9glementaires. Les activit\u00e9s scientifiques permettent \u00e0 l'Agence de mieux comprendre les risques, de fournir des preuves pour \u00e9laborer des mesures d'att\u00e9nuation et de confirmer l'efficacit\u00e9 de ces mesures.</p>\n\n<p>Les activit\u00e9s scientifiques de l'ACIA comprennent les essais en laboratoire, la recherche, la surveillance, l'\u00e9laboration de m\u00e9thodes d'essai, l'\u00e9valuation des risques et les conseils d'experts scientifiques. Les scientifiques de l'Agence entretiennent des partenariats solides avec les universit\u00e9s, l'industrie et leurs homologues f\u00e9d\u00e9raux, provinciaux et internationaux afin de remplir efficacement le mandat de l'ACIA.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_charlottetown_1498061447016_fra.jpg\" class=\"img-responsive\" alt=\"Photographe - Entr\u00e9e du b\u00e2timent du laboratoire de Charlottetown\">\n</div>\n<p>Le laboratoire de Charlottetown est sp\u00e9cialis\u00e9 dans les tests de diagnostic et les activit\u00e9s de recherche sur les pathog\u00e8nes de la pomme de terre, des c\u00e9r\u00e9ales, des ol\u00e9agineux et du sol. Il s'agit d'une installation de bioconfinement certifi\u00e9e qui satisfait aux exigences du niveau 2 de confinement pour les phytoravageurs et qui abrite une serre polyvalente et s\u00e9par\u00e9e, ainsi qu'un syst\u00e8me de biod\u00e9chets \u00e0 la fine pointe de la technologie.</p>\n\n<p>Le laboratoire de Charlottetown est responsable de la d\u00e9tection et du diagnostic des maladies des plantes. Il joue un r\u00f4le cl\u00e9 dans le soutien du commerce des produits v\u00e9g\u00e9taux canadiens gr\u00e2ce \u00e0 ses tests de diagnostic et au d\u00e9veloppement de technologies et de techniques qui r\u00e9pondent aux exigences en mati\u00e8re d'importation, d'exportation et de commerce int\u00e9rieur. Le laboratoire de Charlottetown contribue \u00e0 garantir que les plantes qui entrent au Canada, ainsi que les plantes canadiennes qui sont export\u00e9es vers d'autres pays, sont exemptes de maladies et de parasites.</p>\n\n<h2>Nos activit\u00e9s</h2>\n\n<h3>Services</h3>\n\n<ul>\n<li>S'assurer que les nouvelles vari\u00e9t\u00e9s de pommes de terre import\u00e9es au Canada sont exemptes de ravageurs et de maladies en facilitant le Programme de quarantaine post-entr\u00e9e des pommes de terre de l'ACIA.</li>\n<li>Approuver des laboratoires priv\u00e9s pour la tenue d'activit\u00e9s relatives aux tests de diagnostic r\u00e9glementaires dans le cadre du Programme d'approbation des pommes de terre de semence de l'ACIA.</li>\n<li>Fournir des conseils scientifiques pour appuyer les programmes de protection des v\u00e9g\u00e9taux, le personnel d'inspection, les parties r\u00e9glement\u00e9es et les autres minist\u00e8res.</li>\n<li>Maintenir l'accr\u00e9ditation pour l'\u00e9laboration et la validation de m\u00e9thodes d'essai, l'\u00e9valuation de trousses d'essai et la r\u00e9alisation d'essais non routiniers.</li>\n</ul>\n\n<h3>Tests de diagnostic</h3>\n\n<ul>\n<li>Effectuer des tests de diagnostic \u00e0 l'appui du Programme canadien de certification des pommes de terre de semence et du commerce international des pommes de terre de semence, des l\u00e9gumineuses et d'autres produits v\u00e9g\u00e9taux canadiens.</li>\n<li>Fournir des \u00e9chantillons pour les essais d'aptitude, des tests de diagnostic de confirmation, des protocoles d'essai et des mat\u00e9riaux de r\u00e9f\u00e9rence aux laboratoires priv\u00e9s dans le cadre du Programme d'homologation des pommes de terre de semence.</li>\n</ul>\n\n<h3>Recherche et d\u00e9veloppement</h3>\n\n<ul>\n<li>Mener des recherches r\u00e9glementaires pour d\u00e9velopper et valider de nouvelles technologies de d\u00e9tection, ainsi que pour accro\u00eetre les connaissances sur la diversit\u00e9 des agents pathog\u00e8nes et leurs caract\u00e9ristiques \u00e9cologiques.</li>\n<li>Participer \u00e0 des \u00e9tudes en collaboration avec des partenaires de l'industrie, des groupes de producteurs sp\u00e9cialis\u00e9s, le milieu universitaire, d'autres minist\u00e8res f\u00e9d\u00e9raux et provinciaux, et des intervenants nationaux et internationaux.</li>\n</ul>\n\n<h2>Gestion de la qualit\u00e9</h2>\n\n<p>Tous les laboratoires de l'ACIA sont accr\u00e9dit\u00e9s conform\u00e9ment \u00e0 la norme ISO/IEC 17025, <i>Exigences g\u00e9n\u00e9rales concernant la comp\u00e9tence des laboratoires d'\u00e9talonnage et d'essais</i>. Le Conseil canadien des normes (CCN) accorde l'accr\u00e9ditation pour les essais de routine, l'\u00e9laboration de m\u00e9thodes d'essai et les essais non routiniers, comme l'indique la port\u00e9e d'accr\u00e9ditation du laboratoire sur le site Web du <a href=\"https://www.scc.ca/fr/accreditation/laboratories/agence-canadienne-dinspection-des-aliments-acia-laboratoire-de-charlottetown\" title=\"Conseil canadien des normes\">CCN</a>. L'accr\u00e9ditation v\u00e9rifie officiellement la comp\u00e9tence de l'ACIA \u00e0 produire des r\u00e9sultats pr\u00e9cis et fiables. Ces r\u00e9sultats sont \u00e9tay\u00e9s par l'\u00e9laboration, la validation et la mise en \u0153uvre de m\u00e9thodes scientifiques, men\u00e9es par un personnel hautement qualifi\u00e9, \u00e0 l'aide de produits, de services et d'\u00e9quipement fiables, dans un environnement de qualit\u00e9 contr\u00f4l\u00e9e. La participation \u00e0 des programmes internationaux d'essais d'aptitude d\u00e9montre en outre que nos analyses sont comparables \u00e0 celles de laboratoires du Canada et du monde entier.</p>\n\n<h2>Adresse physique</h2>\n\n<p>Laboratoire de Charlottetown<br>\n<br>\n<br>\n</p>\n\n<h2>Plus d'informations</h2>\n<ul>\n<li><a href=\"https://science.gc.ca/site/science/fr/blogues/cultiver-science/nouvelle-technologie-pour-detecter-determiner-maladies-plantes\">Nouvelle technologie pour d\u00e9tecter et d\u00e9terminer les maladies des plantes</a></li>\n<li>Article: <a href=\"/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/nos-cheres-pommes-de-terre-canadienne/fra/1549645313475/1549645313725\">Nos ch\u00e8res pommes de terre canadiennes\u00a0: l'ACIA assure leur protection</a></li>\n</ul>\n\n<div class=\"well\">\n<div class=\"row mrgn-bttm-lg mrgn-tp-md\">\n<div class=\"col-sm-4\"> <a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/tournee-du-laboratoire-de-charlottetown/fra/1686676836037/1686677431273\" class=\"ic-overlay\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/thumbnail_charlottetown_lab_tour_1686679998166_fra.jpg\" alt=\"\" class=\"img-responsive\"></a></div>\n<div class=\"col-sm-8\">\n<p><span class=\"glyphicon glyphicon-film\"></span><span class=\"wb-inv\">video</span>\u00a0<strong><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/tournee-du-laboratoire-de-charlottetown/fra/1686676836037/1686677431273\">De\u0301tecter la galle verruqueuse de la pomme de terre\u00a0: Tourne\u0301e du laboratoire de Charlottetown</a></strong></p>\n<p>Dur\u00e9e\u00a0: 3:46</p>\n<p>Faites la tourn\u00e9e du laboratoire de Charlottetown de l'Agence canadienne d'inspection des aliments et regardez de plus pr\u00e8s comment ils aident \u00e0 contr\u00f4ler, contenir et pr\u00e9venir la propagation de la galle verruqueuse de la pomme de terre.</p>\n</div>\n</div>\n</div>\n\n<p>Renseignez-vous sur les autres <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}