{
    "dcr_id": "1365794112591",
    "title": {
        "en": "National Bee Farm-Level Biosecurity Standard",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie apicole"
    },
    "html_modified": "2024-02-05 2:51:46 PM",
    "modified": "2013-05-07",
    "issued": "2013-04-12 15:15:15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/manuals-manuels/data/terr_biosec_standards_bee_1365794112591_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/manuals-manuels/data/terr_biosec_standards_bee_1365794112591_fra"
    },
    "ia_id": "1365794221593",
    "parent_ia_id": "1344707981478",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Biosecurity",
        "fr": "Bios\u00e9curit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1344707981478",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "National Bee Farm-Level Biosecurity Standard",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie apicole"
    },
    "label": {
        "en": "National Bee Farm-Level Biosecurity Standard",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie apicole"
    },
    "templatetype": "content page 1 column",
    "node_id": "1365794221593",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "dcr_type": "manuals-manuels",
    "parent_dcr_id": "1344707905203",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "National Bee Farm-Level Biosecurity Standard",
            "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie apicole"
        },
        "description": {
            "en": "The National Bee Farm-level Biosecurity Standard forms the basis of a comprehensive voluntary program designed to provide practical guidance for owners or managers involved in the three main Canadian bee sectors: honey bees, alfalfa leafcutting bees, and bumblebees.",
            "fr": "La pr\u00e9sente Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie apicole constitue le fondement d'un programme exhaustif volontaire qui vise \u00e0 fournir des directives aux propri\u00e9taires ou aux gestionnaires \u0153uvrant dans les trois principaux secteurs apicoles au Canada : abeilles mellif\u00e8res, abeilles d\u00e9coupeuses de la luzerne et bourdons."
        },
        "keywords": {
            "en": "bee, biosecurity, standard",
            "fr": "abeille, bios\u00e9curit\u00e9, norme"
        },
        "dcterms.subject": {
            "en": "insects,inspection,best practices,honey,standards,animal health",
            "fr": "insecte,inspection,meilleures pratiques,miel,norme,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Office of Animal Biosecurity",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-04-12 15:15:15",
            "fr": "2013-04-12 15:15:15"
        },
        "modified": {
            "en": "2013-05-07",
            "fr": "2013-05-07"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public",
            "fr": "entreprises,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "National Bee Farm-Level Biosecurity Standard",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie apicole"
    },
    "body": {
        "en": "-&gt;\r\n\r\n\r\n\r\n\r\n\r\n\r\n        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=0\">Complete Text</a></p>\r\n\r\n\r\n<h2>Table of Contents</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1\">About this Document</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s1c1\">Why a National Standard?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s2c1\">Value of the Canadian Bee Industry</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s3c1\">Who is this Document For?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s4c1\">What is Biosecurity and Why is it Important?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s5c1\">What are the Benefits?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s6c1\">Document Development</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=1#s7c1\">How Should this Document be Used?</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=2\">Glossary</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=3\">Summary of Target Outcomes</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=3#s8c3\">1.0 Bee Health Management</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=3#s9c3\">2.0 Operations Management</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4\">Section 1: Bee Health Management</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4#s10c4\">1.1 Bee Sources</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4#s11c4\">1.2 Prevention: Minimizing Susceptibility to Pests</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4#s12c4\">1.3 Prevention: Minimizing Exposure</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4#s13c4\">1.4 Diagnosis and Monitoring</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4#s14c4\">1.5 Standard Response Plan</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=4#s15c4\">1.6 Elevated Response Plan</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5\">Section 2: Operations Management</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s16c5\">2.1 Obtaining Production Inputs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s17c5\">2.2 Handling and Disposal of Production Inputs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s18c5\">2.3 Obtaining Bee Equipment</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s19c5\">2.4 Management and Maintenance of Bee Equipment, Dead Bees, and Bee Products</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s20c5\">2.5 Personal Sanitation</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s21c5\">2.6 Design of Facilities</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s22c5\">2.7 Maintenance of premises, buildings, vehicles, and other equipment</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s23c5\">2.8 Control of Weeds and Nuisance Pests</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=5#s24c5\">2.9 Training and Education</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=6\">Appendix A: Examples of Management Topics</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=6#s25c6\">Section 1: Bee Health Management</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=6#s26c6\">Section 2: Operations Management</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=7\">Appendix B: <abbr title=\"Bee Biosecurity Advisory Committee\">BeeBAC</abbr> Members and Project Advisors</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/bee-industry/eng/1365794112591/1365794221593?chap=8\">Acknowledgments</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n<p>Prepared for: Office of Animal Biosecurity, Canadian Food Inspection Agency, Ottawa, Ontario</p>\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "-&gt;\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=0\">Texte Complet</a></p>\r\n\r\n\r\n<h2>Table des mati\u00e8res</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1\">\u00c0 propos du pr\u00e9sent document</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s1c1\">Pourquoi une Norme nationale?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s2c1\">Valeur de l'industrie apicole canadienne</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s3c1\">\u00c0 qui ce document s'adresse-t-il?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s4c1\">Importance de la bios\u00e9curit\u00e9</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s5c1\">Avantages li\u00e9s \u00e0 l'application de bons principes de bios\u00e9curit\u00e9</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s6c1\">\u00c9laboration du pr\u00e9sent document</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=1#s7c1\">Utilisation du pr\u00e9sent document</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=2\">Glossaire</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=3\">Sommaire des r\u00e9sultats vis\u00e9s</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=3#s8c3\">1.0 Gestion de la sant\u00e9 des abeilles</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=3#s9c3\">2.0 Gestion des op\u00e9rations</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4\">Section\u00a01\u00a0: Gestion de la sant\u00e9 des abeilles</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4#s10c4\">1.1 Sources d'abeilles</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4#s11c4\">1.2 Pr\u00e9vention\u00a0: r\u00e9duire la sensibilit\u00e9 des abeilles aux organismes nuisibles</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4#s12c4\">1.3 Pr\u00e9vention\u00a0: r\u00e9duire l'exposition</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4#s13c4\">1.4 Diagnostic et surveillance</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4#s14c4\">1.5 Plan d'intervention standard</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=4#s15c4\">1.6 Plan d'intervention d'urgence</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5\">Section 2\u00a0: Gestion des op\u00e9rations</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s16c5\">2.1 Acquisition des intrants de production</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s17c5\">2.2 Manipulation et \u00e9limination des intrants de production</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s18c5\">2.3 Acquisition de l'\u00e9quipement apicole</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s19c5\">2.4 Gestion et entretien de l'\u00e9quipement apicole, des abeilles mortes et des produits apicoles</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s20c5\">2.5 Hygi\u00e8ne du personnel</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s21c5\">2.6 Conception des installations</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s22c5\">2.7 Entretien des installations, des b\u00e2timents, des v\u00e9hicules et de l'\u00e9quipement</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s23c5\">2.8 Lutte contre les mauvaises herbes et les ravageurs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=5#s24c5\">2.9 Formation et \u00e9ducation</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=6\">Annexe\u00a0A : Exemples de points \u00e0 consid\u00e9rer dans la gestion des op\u00e9rations apicoles</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=6#s25c6\">Section 1 \u2013 Gestion de la sant\u00e9 des abeilles</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=6#s26c6\">Section 2 \u2013 Gestion des op\u00e9rations</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=7\">Annexe\u00a0B Membres de <abbr title=\"comit\u00e9 consultatif sur la bios\u00e9curit\u00e9 des abeilles\">CCBA</abbr> et experts-conseils participant au projet</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-apicole/fra/1365794112591/1365794221593?chap=8\">Remerciements</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n<p>Pr\u00e9par\u00e9e pour\u00a0: Bureau de la bios\u00e9curit\u00e9 animale,  Agence canadienne d'inspection des aliments, Ottawa (Ontario)</p>\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}