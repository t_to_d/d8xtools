{
    "dcr_id": "1330200847526",
    "title": {
        "en": "Newcastle Disease",
        "fr": "Maladie de Newcastle"
    },
    "html_modified": "2024-03-12 3:55:37 PM",
    "modified": "2014-02-17",
    "issued": "2012-02-25 15:14:11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_newcastle_index_1330200847526_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_newcastle_index_1330200847526_fra"
    },
    "ia_id": "1330201028686",
    "parent_ia_id": "1303768544412",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1303768544412",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Newcastle Disease",
        "fr": "Maladie de Newcastle"
    },
    "label": {
        "en": "Newcastle Disease",
        "fr": "Maladie de Newcastle"
    },
    "templatetype": "content page 1 column",
    "node_id": "1330201028686",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1303768471142",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/nd/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mn/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Newcastle Disease",
            "fr": "Maladie de Newcastle"
        },
        "description": {
            "en": "Newcastle Disease (ND) is a viral disease that can affect a wide variety of avian species, both wild birds and domestic fowl.",
            "fr": "La maladie de Newcastle (MN) est une maladie virale qui peut s'attaquer \u00e0 une grande vari\u00e9t\u00e9 d'esp\u00e8ces aviaires tant sauvages que domestiques."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, Newcastle Disease, ND, Velogenic Newcastle Disease, VND, poultry, wild birds",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, maladie de Newcastle, MN, virus v\u00e9log\u00e8ne de Newcastle, volaille, oiseaux sauvages"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,livestock,regulation,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,b\u00e9tail,r\u00e9glementation,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-25 15:14:11",
            "fr": "2012-02-25 15:14:11"
        },
        "modified": {
            "en": "2014-02-17",
            "fr": "2014-02-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Newcastle Disease",
        "fr": "Maladie de Newcastle"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Newcastle disease (ND) is a viral disease that can affect a wide variety of avian species, both wild birds and domestic fowl. The most severe forms of the disease, known as velogenic Newcastle disease (VND), are caused by highly pathogenic strains of the virus. <abbr title=\"velogenic Newcastle disease\">VND</abbr> can cause severe mortality in chickens. <abbr title=\"Velogenic Newcastle Disease\">VND</abbr> is <strong>not</strong> present in Canadian poultry.</p>\n<p>In Canada, <abbr title=\"Newcastle Disease\">ND</abbr> is a <a href=\"/animal-health/terrestrial-animals/diseases/reportable/eng/1303768471142/1303768544412\">reportable disease</a> under the <em>Health of Animals Act</em>, and all cases must be reported to the Canadian Food Inspection Agency (CFIA).</p>\n<h2>What information is available?</h2>\n<ul><li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/nd/countries-recognized-as-being-free-of-the-disease/eng/1330203218297/1330203453108\" title=\"Newcastle Disease\u00a0\u2013\u00a0Countries officially recognized by Canada as being free of the disease\">Countries recognized as being free of the disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/nd/fact-sheet/eng/1330202454619/1330202602677\" title=\"Newcastle Disease - Fact Sheet \">Fact Sheet </a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/nd/hazard-specific-plan/eng/1392670129624/1392670192661\" title=\"Newcastle Disease Hazard Specific Plan\">Hazard Specific Plan</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La maladie de Newcastle (MN) est une maladie virale qui peut s'attaquer \u00e0 une grande vari\u00e9t\u00e9 d'esp\u00e8ces aviaires tant sauvages que domestiques. Les formes les plus graves de la maladie sont caus\u00e9es par les souches \u00ab hautement pathog\u00e8nes \u00bb du virus. La maladie \u00e0 virus v\u00e9log\u00e8ne de Newcastle est l'une des maladies les plus infectieuses de la volaille \u00e0 l'\u00e9chelle internationale et cause chez les poulets des infections graves et mortelles. La maladie \u00e0 virus v\u00e9log\u00e8ne de Newcastle <strong>n'est pas</strong> pr\u00e9sente chez la volaille au Canada.</p>\n<p>Au Canada, la <abbr title=\"maladie de Newcastle\">MN</abbr> est une maladie \u00e0 <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fra/1303768471142/1303768544412\">d\u00e9claration obligatoire</a> en vertu de la <em>Loi sur la sant\u00e9 des animaux</em>, et tous les cas doivent \u00eatre d\u00e9clar\u00e9s \u00e0 l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition?</h2>\n<ul><li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mn/fiche-de-renseignements/fra/1330202454619/1330202602677\" title=\"Maladie de Newcastle - Fiche de renseignements\">Fiche de renseignements</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mn/pays-reconnus-indemnes-de-la-maladie/fra/1330203218297/1330203453108\" title=\"Maladie de Newcastle\u00a0\u2013\u00a0Pays officiellement reconnus par le Canada comme \u00e9tant indemnes de la maladie\">Pays reconnus indemnes de la maladie</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mn/plan-specifiquement-lie-aux-risques/fra/1392670129624/1392670192661\" title=\"Plan sp\u00e9cifiquement li\u00e9 aux risques concernant la maladie de Newcastle\">Plan sp\u00e9cifiquement li\u00e9 aux risques</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}