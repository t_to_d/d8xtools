{
    "dcr_id": "1426694335122",
    "title": {
        "en": "Frequently Asked Questions: <i>Health of Animals Regulations</i>",
        "fr": "Foire aux questions : <i>R\u00e8glement sur la sant\u00e9 des animaux</i>"
    },
    "html_modified": "2024-03-12 3:57:02 PM",
    "modified": "2015-03-18",
    "issued": "2015-06-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_animals_regs_1426694335122_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_animals_regs_1426694335122_fra"
    },
    "ia_id": "1426694360491",
    "parent_ia_id": "1419029097256",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1419029097256",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Frequently Asked Questions: <i>Health of Animals Regulations</i>",
        "fr": "Foire aux questions : <i>R\u00e8glement sur la sant\u00e9 des animaux</i>"
    },
    "label": {
        "en": "Frequently Asked Questions: <i>Health of Animals Regulations</i>",
        "fr": "Foire aux questions : <i>R\u00e8glement sur la sant\u00e9 des animaux</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1426694360491",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1419029096537",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/faq-health-of-animals-regulations/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/faq-reglement-sur-la-sante-des-animaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Frequently Asked Questions: Health of Animals Regulations",
            "fr": "Foire aux questions : R\u00e8glement sur la sant\u00e9 des animaux"
        },
        "description": {
            "en": "The Frequently Asked Questions below are meant to provide Canadians and businesses with general information about the Canadian Food Inspection Agency's regulations.",
            "fr": "La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les r\u00e8glements qu'applique l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "frequently asked questions, regulations, Health of Animals Regulations, animal health, control, disease",
            "fr": "foire aux questions, r\u00e8glements, R\u00e8glement sur la sant\u00e9 des animaux, sant\u00e9 des animaux, contr\u00f4le, maladie"
        },
        "dcterms.subject": {
            "en": "infectious diseases,consumer protection,animal health,safety",
            "fr": "maladie infectieuse,protection du consommateur,sant\u00e9 animale,s\u00e9curit\u00e9"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-06-16",
            "fr": "2015-06-16"
        },
        "modified": {
            "en": "2015-03-18",
            "fr": "2015-03-18"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Frequently Asked Questions: Health of Animals Regulations",
        "fr": "Foire aux questions : R\u00e8glement sur la sant\u00e9 des animaux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The <abbr title=\"Frequently Asked Questions\">FAQs</abbr> below are meant to provide Canadians and businesses with general information about the Canadian Food Inspection Agency's regulations.</p>\n<h2 class=\"h5\">1. What is the purpose of these regulations?</h2>\n<p>The <i>Health of Animals Regulations</i>, under the authority of the <i>Health of Animals Act</i>, are intended to protect animals and animal health. They provide for the control of diseases and toxic substances that may affect terrestrial and aquatic animals or that may be transmitted by animals to persons.</p>\n<h2 class=\"h5\">2. What are the key elements of these regulations?</h2>\n<p><strong>Part <abbr title=\"1\">I</abbr> \u2013 Segregation and Inspection of Animals:</strong> provides for the segregation, confinement, and inspection of animals for disease control purposes. It allows for the destruction of an animal that is affected, or suspected of being affected, with a communicable disease, and the disposal of its carcass.</p>\n<p><strong>Part <abbr title=\"1\">I</abbr>.1 \u2013 Specified Risk Material:</strong> sets out requirements for the removal and handling of specified risk material (parts of cattle that could carry a risk of transmitting Bovine Spongiform Encephalopathy) from cattle carcasses.</p>\n<p><strong>Part <abbr title=\"2\">II</abbr> \u2013 Importation:</strong> sets out conditions for importation of animals or things in order to prevent the introduction of a disease into Canada. Requirements for the importation of germplasm and regulated animals are also prescribed.</p>\n<p><strong>Part <abbr title=\"3\">III</abbr> \u2013 Importation of Animal Products:</strong> outlines requirements for the importation of dairy products, eggs and egg products.</p>\n<p><strong>Part <abbr title=\"4\">IV</abbr> \u2013 Importation of Animal By-Products, Animal Pathogens and Other Things:</strong> provides import conditions to reduce the risk that foreign animal diseases are introduced into Canada. Requirements are prescribed for the importation of specific animal by-products, animal pathogens and other things.</p>\n<p><strong>Part <abbr title=\"5\">V</abbr> \u2013 Importation of Fodder:</strong> prohibits limitations to the importation of fodder from another country other than the United States.</p>\n<p><strong>Part <abbr title=\"6\">VI</abbr> \u2013 Importation of Packing Material, Beehives and Beeswax:</strong> prohibits the importation of packing materials, beehives and beeswax unless the shipment is accompanied by a veterinary certificate or has been disinfected under the supervision of an inspector.</p>\n<p><strong>Part <abbr title=\"7\">VII</abbr> \u2013 Quarantine of Imported Animals:</strong> sets out requirements for the quarantining of imported animals at quarantine ports, inspection ports and other places approved by the Minister.</p>\n<p><strong>Part <abbr title=\"8\">VIII</abbr> \u2013 Exportation of Animals, Animal Products and Products of Rendering Plants:</strong> prohibits the exporting out of Canada of livestock, poultry, animal embryos and animal semen unless the exporter has obtained a veterinary certificate and has complied with the requirements of the importing country. It also regulates products of a rendering plant, a fertilizer, a fertilizer supplement and animal food that contains a product of a rendering plant. In addition, it imposes requirements for rest periods for animals being exported out of Canada.</p>\n<p><strong>Part <abbr title=\"9\">IX</abbr> \u2013 Eradication of Diseases:</strong> authorizes the Minister to declare eradication areas for the purposes of animal disease control and to require movement permits, especially with respect to Bovine Tuberculosis and Brucellosis. Conditions with respect to the control of Pullorum Disease, and Fowl Typhoid are included. Requirements for the control of outbreaks of communicable diseases and eradication of specified diseases are provided.</p>\n<p><strong>Part <abbr title=\"10\">X</abbr> \u2013 General Provisions:</strong> sets out requirements for disease notification, record keeping, and quarantining. Requirements for animal markets, the marking of animals, inspection seals, and disinfection are provided. Specific conditions for samples of milk and cream from dairies are outlined. Also included are restrictions on feeding meat or meat by-products to livestock and poultry, and requirements for disposal of diseased carcasses and animal semen production centres.</p>\n<p><strong>Part <abbr title=\"11\">XI</abbr> \u2013 Veterinary Biologics:</strong> regulates the use of veterinary biologics. It also provides for conditions to be attached to permits for the release of veterinary biologics and their information requirements, as well as requirements for permits to import a veterinary biologic into Canada. Outlined are the requirements for the establishment and conditions of product licences. Requirements for the operation of licensed establishments are also set out. Labelling and marketing requirements for veterinary biologics are provided.</p>\n<p><strong>Part <abbr title=\"12\">XII</abbr> \u2013 Transportation of Animals:</strong> sets out requirements for the transportation of animals entering, leaving, or within Canada and their inspection. Specific requirements for sick, pregnant, and unfit animals are included. Conditions for loading and unloading equipment being used to transport animals, as well as prohibitions against overcrowding and requirements for segregation are provided. Requirements for the protection of animals from injury and sickness are outlined. Conditions for the containers, the protective facilities, and the ventilation of aircraft and vessel used in the transportation of animals are provided. Requirements for food and water for animals, special food for calves, and reporting on injured animals in transit are regulated in this part. Also outlined are record-keeping requirements with respect to shipments of livestock and animals. Conditions are provided for attendants and inspectors, for the protection of animals on board a vessel, for securing animals, for reserve pens, lighting, insulation, the disposal of injured animals, and the veterinary drugs to be carried on a vessel transporting animals.</p>\n<p><strong>Part <abbr title=\"13\">XIII</abbr> \u2013 Permits and Licences:</strong> provides requirements for permits and licences for activities relating to the marketing, disposing, exposing for sale, and transporting of animals or things affected with or suffering from, or suspected of being affected with or suffering from infectious or contagious diseases.</p>\n<p><strong>Part <abbr title=\"14\">XIV</abbr> \u2013 Food for Ruminants, Livestock and Poultry, Rendering Plants, Fertilizers and Fertilizer Supplements:</strong> defines \"prohibited material\", prohibits feeding it to ruminants, and sets out record-keeping requirements related to it. Conditions for the operation, importation and sale of products, and recall procedures for a rendering plant are outlined. Animal food and food ingredients, recall procedures, and records for animal food, fertilizers and fertilizer supplements are regulated in this part, especially with respect to prohibited material.</p>\n<p><strong>Part <abbr title=\"15\">XV</abbr> \u2013 Animal Identification:</strong> provides requirements for the identification of animals, including: registration of linked sites (two farms between which pigs are moved); the approval, issuance, and revocation of identification indicators (tags, chips, <abbr title=\"et cetera\">etc.</abbr>) for animals; reporting of sales of tags; the use of tags on animals; and record-keeping and information reporting for pigs. Prohibitions relating to the moving of certain animals from a site are provided. Requirements with respect to tagging sites and the loss or application for an approved tag are outlined. Details for identification tags relating to an animal death or slaughter are provided. Export and import requirements relating to the tagging of animals are prescribed. Details respecting the maintenance of a database and other information obtained by a responsible administrator are outlined in this Part.</p>\n<p><strong>Part <abbr title=\"16\">XVI</abbr> \u2013 Aquatic Animals:</strong> sets out the requirements for the importation of aquatic animals. Establishment of, and requirements respecting, eradication areas with respect to the prevention of the spread of diseases of aquatic animals are outlined. This part also regulates the movement of aquatic animals and their marking, and sets out other permit, certificate and documentation requirements.</p>\n<p><strong>Schedule <abbr title=\"1\">I</abbr> and <abbr title=\"2\">II</abbr>:</strong> sets out quarantine and inspection ports.</p>\n<p><strong>Schedule <abbr title=\"3\">III</abbr>: </strong>identifies susceptible species of aquatic animals.</p>\n<h2 class=\"h5\">3. How do these regulations affect Canadian businesses?</h2>\n<p>These regulations contain provisions for the control of animal diseases and toxic substances, including diseases that may spread to humans. They impose requirements respecting the handling, transportation, and identification of livestock and other animals in Canada. They also impose requirements on businesses to maintain records and provide information for the purpose of protecting the health of Canada's livestock and other regulated animals.</p>\n<h2 class=\"h5\">4. When did these regulations come into force?</h2>\n<p>The <i>Health of Animals Regulations</i> came into force on January 13, 1977, under the title of the <i>Animal Disease and Protection Regulations</i>. On <span class=\"nowrap\">September 5, 1991</span>, the title of the regulations was amended to <i>Health of Animals Regulations</i>.</p>\n<h2 class=\"h5\">5. Where can I get more information?</h2>\n<p>Please refer to the <a href=\"/animal-health/eng/1299155513713/1299155693492\">Animals</a> section of the Canadian Food Inspection Agency's website for more information.</p>\n\n<p>Questions relating to the <i>Health of Animals Regulations</i> may be directed to an Animal Health Specialist at a <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area office</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les r\u00e8glements qu'applique l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<h2 class=\"h5\">1.\tQuel est l'objectif de ce r\u00e8glement?</h2>\n<p>Le <i>R\u00e8glement sur la sant\u00e9 des animaux</i>, en application de la <i>Loi sur la sant\u00e9 des animaux</i>, a pour but de prot\u00e9ger les animaux et la sant\u00e9 de ceux-ci. Il pr\u00e9voit le contr\u00f4le des maladies et des substances toxiques qui peuvent avoir une incidence sur les animaux terrestres et aquatiques ou qui peuvent \u00eatre transmises par ceux-ci aux personnes.</p>\n<h2 class=\"h5\">2.\tQuels sont les principaux \u00e9l\u00e9ments de ce r\u00e8glement?</h2>\n<p>La <strong>partie <abbr title=\"1\">I</abbr> (Isolement et inspection des animaux)</strong> pr\u00e9voit l'isolement, le confinement et l'inspection des animaux \u00e0 des fins de lutte contre les maladies. Elle permet la destruction d'un animal qui est atteint ou soup\u00e7onn\u00e9 d'\u00eatre atteint d'une maladie transmissible ainsi que l'\u00e9limination de sa carcasse.</p>\n<p>La <strong>partie <abbr title=\"1\">I</abbr>.1 (Mat\u00e9riel \u00e0 risque sp\u00e9cifi\u00e9)</strong> comprend des exigences relatives au retrait et \u00e0 la manipulation des mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es (parties des bovins qui risquent de transmettre l'enc\u00e9phalopathie spongiforme bovine) provenant des carcasses de bovins.</p>\n<p>La <strong>partie <abbr title=\"2\">II</abbr> (Importation)</strong> comprend des conditions relatives \u00e0 l'importation d'animaux ou de choses visant \u00e0 pr\u00e9venir l'introduction d'une maladie au Canada ainsi que des exigences concernant l'importation de mat\u00e9riel g\u00e9n\u00e9tique et d'animaux r\u00e9glement\u00e9s.</p>\n<p>La <strong>partie <abbr title=\"3\">III</abbr> (Importation de produits animaux)</strong> donne un aper\u00e7u des exigences relatives \u00e0 l'importation de produits laitiers, d'\u0153ufs et de produits des \u0153ufs.</p>\n<p>La <strong>partie <abbr title=\"4\">IV</abbr> (Importation de sous produits animaux, d'agents zoopathog\u00e8nes et autres)</strong> comprend des conditions d'importation \u00e0 suivre afin d'att\u00e9nuer le risque que des maladies animales exotiques soient introduites au Canada ainsi que des exigences visant l'importation de sous produits animaux et d'agents zoopathog\u00e8nes pr\u00e9cis, entre autres.</p>\n<p>La <strong>partie <abbr title=\"5\">V</abbr> (Importation de fourrage)</strong> interdit l'importation de fourrage d'un pays autre que les \u00c9tats-Unis.</p>\n<p>La <strong>partie <abbr title=\"6\">VI</abbr> (Importation de mat\u00e9riel d'emballage, de ruches et de cire d'abeille)</strong> interdit l'importation de mat\u00e9riaux d'emballage, de ruches et de cire d'abeille sauf si l'envoi est accompagn\u00e9 d'un certificat v\u00e9t\u00e9rinaire ou a \u00e9t\u00e9 d\u00e9sinfect\u00e9 sous la supervision d'un inspecteur.</p>\n<p>La <strong>partie <abbr title=\"7\">VII</abbr> (Quarantaine d'animaux import\u00e9s)</strong> \u00e9nonce les exigences concernant la mise en quarantaine d'animaux import\u00e9s \u00e0 un poste de quarantaine ou d'inspection ou \u00e0 un autre endroit approuv\u00e9 par le ministre.</p>\n<p>La <strong>partie <abbr title=\"8\">VIII</abbr> (Exportation d'animaux, de produits animaux et de produits d'usine de traitement)</strong> interdit l'exportation \u00e0 partir du Canada d'animaux d'\u00e9levage, de volaille, d'embryons animaux et de sperme animal, sauf si l'exportateur a obtenu un certificat v\u00e9t\u00e9rinaire et a respect\u00e9 les exigences du pays importateur. Elle r\u00e9git \u00e9galement les produits des usines d'\u00e9quarrissage, les engrais, les suppl\u00e9ments d'engrais et les aliments pour animaux qui contiennent des produits provenant d'une usine d'\u00e9quarrissage. De plus, elle impose des exigences relatives aux p\u00e9riodes de repos des animaux qui sont export\u00e9s \u00e0 partir du Canada.</p>\n<p>La <strong>partie <abbr title=\"9\">IX</abbr> (\u00c9radication des maladies)</strong> autorise le ministre \u00e0 d\u00e9clarer des zones d'\u00e9radication \u00e0 des fins de lutte contre les maladies animales ainsi qu'\u00e0 exiger des permis de d\u00e9placement, surtout en ce qui concerne la tuberculose bovine et la brucellose bovine. Elle comprend des conditions relatives \u00e0 la lutte contre la pullorose et la typhose aviaire ainsi que des exigences relatives au contr\u00f4le des \u00e9closions de maladies transmissibles et \u00e0 l'\u00e9radication de maladies particuli\u00e8res.</p>\n<p>La <strong>partie <abbr title=\"10\">X</abbr> (Dispositions g\u00e9n\u00e9rales)</strong> comprend ce qui suit\u00a0: exigences concernant la d\u00e9claration des maladies, la tenue de registres, la mise en quarantaine, les march\u00e9s d'animaux, le marquage des animaux, les sceaux d'inspection et la d\u00e9sinfection; conditions relatives aux \u00e9chantillons de lait ou de cr\u00e8me provenant de laiteries; restrictions quant \u00e0 l'alimentation du b\u00e9tail et de la volaille avec de la viande ou des sous produits de viande; exigences relatives \u00e0 l'\u00e9limination des carcasses contamin\u00e9es et aux centres de production de sperme animal.</p>\n<p>La <strong>partie <abbr title=\"11\">XI</abbr> (Produits v\u00e9t\u00e9rinaires biologiques)</strong> r\u00e9git l'utilisation de produits v\u00e9t\u00e9rinaires biologiques, en plus de comprendre ce qui suit\u00a0: conditions \u00e0 joindre aux permis pour la diss\u00e9mination de produits v\u00e9t\u00e9rinaires biologiques et renseignements connexes requis; exigences relatives aux permis pour l'importation de produits v\u00e9t\u00e9rinaires biologiques au Canada et aux permis d'\u00e9tablissement; conditions touchant les permis de fabrication; exigences relatives \u00e0 l'exploitation d'\u00e9tablissements agr\u00e9\u00e9s; exigences concernant l'\u00e9tiquetage et la commercialisation de produits v\u00e9t\u00e9rinaires biologiques.</p>\n<p>La <strong>partie <abbr title=\"12\">XII</abbr> (Transport des animaux)</strong> comprend ce qui suit\u00a0: exigences relatives au transport des animaux qui entrent au Canada ou en sortent, \u00e0 leur transport au pays et \u00e0 leur inspection; exigences particuli\u00e8res pour les animaux malades, en gestation et inaptes; conditions pour l'\u00e9quipement d'embarquement et de d\u00e9barquement qui est utilis\u00e9 pour le transport des animaux; interdictions relatives \u00e0 l'entassement; exigences relatives \u00e0 l'isolement et \u00e0 la protection des animaux contre les blessures ou la maladie; conditions concernant les conteneurs, les installations de protection et la ventilation de l'a\u00e9ronef et du navire utilis\u00e9s pour transporter les animaux; exigences qui r\u00e9gissent l'alimentation et l'abreuvement des animaux, les aliments sp\u00e9ciaux pour les veaux et la d\u00e9claration des animaux bless\u00e9s en transit; exigences en mati\u00e8re de tenue de registres concernant les envois de b\u00e9tail et d'animaux; conditions relatives aux pr\u00e9pos\u00e9s et aux inspecteurs, \u00e0 la protection des animaux \u00e0 bord d'un navire, \u00e0 l'entravement des animaux, aux enclos r\u00e9serv\u00e9s, \u00e0 l'\u00e9clairage, \u00e0 l'isolation, \u00e0 l'\u00e9limination des animaux bless\u00e9s et au transport des m\u00e9dicaments v\u00e9t\u00e9rinaires \u00e0 bord d'un navire transportant des animaux.</p>\n<p>La <strong>partie <abbr title=\"13\">XIII</abbr> (Permis et licences)</strong> comprend des exigences relatives aux permis et aux licences pour des activit\u00e9s li\u00e9es \u00e0 la mise en march\u00e9, \u00e0 l'\u00e9limination, \u00e0 l'\u00e9talage aux fins de vente et au transport d'animaux ou de choses contamin\u00e9s ou souffrants ou soup\u00e7onn\u00e9s d'\u00eatre contamin\u00e9s ou de souffrir d'une maladie infectieuse ou contagieuse.</p>\n<p>La <strong>partie <abbr title=\"14\">XIV</abbr> (Aliments pour ruminants, animaux de ferme et volaille, usines de traitement, engrais et suppl\u00e9ments d'engrais)</strong> d\u00e9finit ce qu'est une \u00ab\u00a0substance interdite\u00a0\u00bb, interdit son utilisation pour l'alimentation des ruminants et \u00e9tablit des exigences connexes en mati\u00e8re de tenue de registres. Elle comprend des conditions relatives \u00e0 l'exploitation d'une usine d'\u00e9quarrissage, \u00e0 l'importation et \u00e0 la vente de produits provenant de cette usine et aux proc\u00e9dures de rappel qui doivent \u00eatre \u00e9tablies, en plus de r\u00e9gir les aliments et ingr\u00e9dients pour animaux, les proc\u00e9dures de rappel ainsi que les registres concernant les aliments du b\u00e9tail, les engrais et les suppl\u00e9ments d'engrais, surtout en ce qui concerne les substances interdites.</p>\n<p>La <strong>partie <abbr title=\"15\">XV</abbr> (Identification des animaux)</strong> comprend ce qui suit\u00a0: exigences concernant l'identification des animaux, notamment l'enregistrement des installations li\u00e9es (deux exploitations entre lesquelles les porcs sont d\u00e9plac\u00e9s), l'approbation, la d\u00e9livrance et la r\u00e9vocation des indicateurs d'identification (\u00e9tiquettes, puces, <abbr title=\"et cetera\">etc.</abbr>) pour les animaux, la d\u00e9claration des ventes d'\u00e9tiquettes, l'utilisation d'\u00e9tiquettes sur les animaux ainsi que la tenue de registres et la communication de renseignements concernant les porcs; interdictions relatives au d\u00e9placement de certains animaux d'un site; exigences concernant les installations d'\u00e9tiquetage et la perte ou l'apposition d'une \u00e9tiquette approuv\u00e9e; renseignements sur les \u00e9tiquettes d'identification li\u00e9es \u00e0 la mort ou \u00e0 l'abattage d'un animal; exigences en mati\u00e8re d'exportation et d'importation li\u00e9es \u00e0 l'apposition d'\u00e9tiquettes sur des animaux; pr\u00e9cisions sur la tenue \u00e0 jour d'une base de donn\u00e9es et les autres renseignements obtenus par un administrateur responsable.</p>\n<p>La <strong>partie <abbr title=\"16\">XVI</abbr> (Animaux aquatiques)</strong> comprend des exigences relatives \u00e0 l'importation d'animaux aquatiques, aux zones d'\u00e9radication et \u00e0 l'\u00e9tablissement de celles-ci afin de pr\u00e9venir la propagation des maladies des animaux aquatiques ainsi qu'aux permis, certificats et documents. De plus, elle r\u00e9git le d\u00e9placement d'animaux aquatiques et leur marquage.</p>\n<p>Les <strong>annexes <abbr title=\"1\">I</abbr> et <abbr title=\"2\">II</abbr></strong> pr\u00e9sentent les postes de mise en quarantaine et d'inspection.</p>\n<p>L'<strong>annexe <abbr title=\"3\">III</abbr></strong> pr\u00e9sente les esp\u00e8ces d'animaux aquatiques vuln\u00e9rables.</p>\n<h2 class=\"h5\">3.\tQuelle incidence ce r\u00e8glement a-t-il sur les entreprises canadiennes?</h2>\n<p>Ce r\u00e8glement contient des dispositions portant sur le contr\u00f4le des maladies animales et des substances toxiques, y compris les maladies pouvant \u00eatre transmises aux humains. Il impose des exigences relatives \u00e0 la manipulation, au transport et \u00e0 l'identification du b\u00e9tail et d'autres animaux au Canada. En outre, il exige des entreprises qu'elles tiennent des registres \u00e0 jour et qu'elles fournissent des renseignements afin de prot\u00e9ger la sant\u00e9 des animaux d'\u00e9levage et d'autres animaux r\u00e9glement\u00e9s du Canada.</p>\n<h2 class=\"h5\">4.\t\u00c0 quel moment ce r\u00e8glement est-il entr\u00e9 en vigueur?</h2>\n<p>Le <i>R\u00e8glement sur la sant\u00e9 des animaux</i> est entr\u00e9 en vigueur le 13 janvier 1977 et portait alors le nom de <i>R\u00e8glement sur les maladies et la protection des animaux</i>. Le <span class=\"nowrap\">5 septembre 1991</span>, on a chang\u00e9 son titre pour <i>R\u00e8glement sur la sant\u00e9 des animaux</i>.</p>\n<h2 class=\"h5\">5.\tO\u00f9 puis-je obtenir de plus amples renseignements?</h2>\n<p>Pour en savoir plus, veuillez consulter la page <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">Animaux</a> du site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n\n<p>Les questions concernant le <i>R\u00e8glement sur la sant\u00e9 des animaux</i> peuvent \u00eatre transmises \u00e0 un sp\u00e9cialiste de la sant\u00e9 des animaux dans un <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau des centres op\u00e9rationnels de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}