{
    "dcr_id": "1549573322559",
    "title": {
        "en": "The Ottawa Plant Laboratory",
        "fr": "Le laboratoire des v\u00e9g\u00e9taux d'Ottawa"
    },
    "html_modified": "2024-03-12 3:58:20 PM",
    "modified": "2023-08-23",
    "issued": "2019-02-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_ottawa_plant_1549573322559_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_ottawa_plant_1549573322559_fra"
    },
    "ia_id": "1549573449538",
    "parent_ia_id": "1494878085588",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1494878085588",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The Ottawa Plant Laboratory",
        "fr": "Le laboratoire des v\u00e9g\u00e9taux d'Ottawa"
    },
    "label": {
        "en": "The Ottawa Plant Laboratory",
        "fr": "Le laboratoire des v\u00e9g\u00e9taux d'Ottawa"
    },
    "templatetype": "content page 1 column",
    "node_id": "1549573449538",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1494878032804",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-laboratories/ottawa-plant/",
        "fr": "/les-sciences-et-les-recherches/nos-laboratoires/vegetaux-d-ottawa/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The Ottawa Plant Laboratory",
            "fr": "Le laboratoire des v\u00e9g\u00e9taux d'Ottawa"
        },
        "description": {
            "en": "The Ottawa Plant Laboratory, located at Fallowfield campus, is part of the Ontario Laboratories Network of the CFIA and it is the agency\u2019s largest plant laboratory. The lab provides diagnostic, research, and scientific advice services in support of the Plant Health and Seed programs of the CFIA to protect Canada\u2019s agricultural and forestry resources.",
            "fr": "Le laboratoire des v\u00e9g\u00e9taux d'Ottawa fait partie du r\u00e9seau des laboratoires de l'Ontario de l'ACIA et il s'agit du plus gros laboratoire des v\u00e9g\u00e9taux de l'Agence."
        },
        "keywords": {
            "en": "Ottawa Plant Laboratory, laboratory",
            "fr": "laboratoire des v\u00e9g\u00e9taux d'Ottawa, laboratoire"
        },
        "dcterms.subject": {
            "en": "educational guidance",
            "fr": "orientation scolaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2023-08-23",
            "fr": "2023-08-23"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "audience": {
            "en": "scientists",
            "fr": "Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Ottawa Plant Laboratory",
        "fr": "Le laboratoire des v\u00e9g\u00e9taux d'Ottawa"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_ottawaplant_text_1556827357493_eng.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">CFIA science laboratory brochure: The Ottawa Plant Laboratory <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2,400&nbsp;kb)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n-->\n\n<p>The Ottawa Plant Laboratory is located on the traditional unceded territory of the Algonquin Anishinaabeg People.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">About the Canadian Food Inspection Agency</h2></summary>\n\n<p>The Canadian Food Inspection Agency (CFIA) is a science-based regulator with a mandate to safeguard the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">food</a> supply, protect the health of <a href=\"/plant-health/eng/1299162629094/1299162708850\">plants</a> and <a href=\"/animal-health/eng/1299155513713/1299155693492\">animals</a>, and support market access. The Agency relies on high-quality, timely and relevant science as the basis of its program design and regulatory decision-making. Scientific activities inform the Agency's understanding of risks, provide evidence for developing mitigation measures, and confirm the effectiveness of these measures.</p>\n\n<p>CFIA scientific activities include laboratory testing, research, surveillance, test method development, risk assessments and expert scientific advice. Agency scientists maintain strong partnerships with universities, industry, and federal, provincial and international counterparts to effectively carry out the CFIA's mandate.</p>\n</details>\n\n<p>The Ottawa Plant Laboratory, located at <a href=\"/science-and-research/our-laboratories/ottawa-fallowfield-/eng/1549570125940/1549570207759\">Fallowfield campus</a>, is part of the Ontario Laboratories Network of the CFIA and it is the agency's largest plant laboratory. The lab provides diagnostic, research, and scientific advice services in support of the Plant Health and Seed programs of the CFIA to protect Canada's agricultural and forestry resources. The laboratory also provides diagnostic services to certify Canadian plants, seeds, and plant-derived products for export.</p>\n\n<p>OPL hosts scientific and technical professionals that provide state-of-the-art laboratory analysis, scientific advice and research activities. Research scientists and professionals at this laboratory collaborate with national and international organizations in developing new methods for detecting and identifying plant pests, plant species, and plant varieties. They also serve as subject matter experts on national and international panels to develop international standards, procedures and regulations.</p>\n\n<h2>What we do</h2>\n\n<h3>Diagnostics</h3>\n\n<h4>Plant pathology</h4>\n\n<ul>\n<li>Provide regulatory diagnostic services for the identification of plant pathogenic fungi, bacteria, and seed borne viruses in various agricultural commodities intended for import, export, and domestic use in support of the CFIA plant health program</li>\n<li>Provide regulatory seed health testing and accreditation in support of the CFIA seed program</li>\n</ul>\n\n<h4>Nematology</h4>\n\n<ul>\n<li>Provide diagnostic services for the detection and identification of plant-parasitic nematodes in soil, seeds, growing media, plants and plant products</li>\n<li>Identify the presence of soil contaminating imported and exported commodities and characterization of growing media</li>\n</ul>\n\n<h4>Entomology</h4>\n\n<ul>\n<li>Provide detection and identifications of insects, mites and terrestrial molluscs on imported, exported, and domestically moved plants and plant products</li>\n<li>Provide diagnostic support for surveys to detect changes in the distribution of quarantine pests within Canada</li>\n<li>Provide diagnostic support to detect new introductions of invasive alien pests that are not already known to be present in Canada or have limited distribution</li>\n</ul>\n\n<h4>Genotyping \u2013 botany</h4>\n\n<ul>\n<li>Provide diagnostic services related to identification and verification of plant species and varieties through qualitative detection, quantification, and molecular genotyping of crop species, invasive alien plants, plants with novel traits, and seed</li>\n</ul>\n\n<h4>Seeds</h4>\n\n<ul>\n<li>Perform a variety of verification testing methods to monitor the varietal purity and varietal identity of seed in the marketplace to provide confidence in the integrity of the Canadian seed certification system</li>\n</ul>\n\n<h3>Research</h3>\n\n<h4>Molecular Identification Research Laboratory</h4>\n\n<ul>\n<li>Develop advanced molecular diagnostic methods for cultivar or plant species detection, verification, identification and differentiation</li>\n<li>Provide validated methods, datasets in support of the diagnostic services</li>\n</ul>\n\n<h4>Pathogen Identification Research Laboratory</h4>\n\n<ul>\n<li>Develop advanced molecular diagnostic methods (such as using genomics) for plant pathogen detection and identification/genotyping for use by diagnostic laboratories and to support other CFIA activities including surveys</li>\n</ul>\n\n<h4>Entomology Research Laboratory</h4>\n\n<ul>\n<li>Develop molecular tools for the identification of insects and development of insect pheromone trapping</li>\n</ul>\n\n<h4>Nematology Research Laboratory</h4>\n\n<ul>\n<li>Develop molecular diagnostic tools for the detection and identification of plant parasitic nematodes</li>\n<li>Study the biology and distribution of regulated nematodes in Canada</li>\n</ul>\n\n<h2>Quality management</h2>\n\n<p>All CFIA laboratories are accredited in accordance with the International Standard ISO/IEC 17025, <i>General requirements for the competence of testing and calibration laboratories</i>. The Standards Council of Canada (SCC) provides accreditation for routine testing, test method development and non-routine testing, as identified on the laboratory's Scope of Accreditation on the <a href=\"https://www.scc.ca/en/accreditation/laboratories/canadian-food-inspection-agency-4\">SCC website</a>. Accreditation formally verifies the CFIA's competence to produce accurate and reliable results. The results are supported by the development, validation and implementation of scientific methods, conducted by highly qualified personnel, using reliable products, services, and equipment, in a quality controlled environment. Participation in international proficiency testing programs further demonstrates that our testing is comparable to laboratories across Canada and around the world.</p>\n\n<h2>Physical address</h2>\n\n<p>The Ottawa Plant Laboratory<br>\n<br>\n<br>\n</p>\n\n<h2>More information</h2>\n\n<p>Learn about other <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_ottawaplant_text_1556827357493_fra.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">Brochure du laboratoire scientifique de l'ACIA&nbsp;: Le laboratoire des v&#233;g&#233;taux d'Ottawa <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2 400&nbsp;ko)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n-->\n<p>Le laboratoire des v\u00e9g\u00e9taux d'Ottawa est situ\u00e9 sur le territoire traditionnel non c\u00e9d\u00e9 du peuple algonquin Anishinaabeg.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">\u00c0 propos de l'Agence canadienne d'inspection des aliments</h2></summary>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est un organisme de r\u00e9glementation \u00e0 vocation scientifique dont le mandat est de pr\u00e9server l'<a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">approvisionnement alimentaire</a>, de prot\u00e9ger la sant\u00e9 des <a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">v\u00e9g\u00e9taux</a> et des <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">animaux</a> et de favoriser l'acc\u00e8s aux march\u00e9s. L'Agence s'appuie sur des donn\u00e9es scientifiques de grande qualit\u00e9, opportunes et pertinentes pour concevoir ses programmes et prendre ses d\u00e9cisions r\u00e9glementaires. Les activit\u00e9s scientifiques permettent \u00e0 l'Agence de mieux comprendre les risques, de fournir des preuves pour \u00e9laborer des mesures d'att\u00e9nuation et de confirmer l'efficacit\u00e9 de ces mesures.</p>\n\n<p>Les activit\u00e9s scientifiques de l'ACIA comprennent les essais en laboratoire, la recherche, la surveillance, l'\u00e9laboration de m\u00e9thodes d'essai, l'\u00e9valuation des risques et les conseils d'experts scientifiques. Les scientifiques de l'Agence entretiennent des partenariats solides avec les universit\u00e9s, l'industrie et leurs homologues f\u00e9d\u00e9raux, provinciaux et internationaux afin de remplir efficacement le mandat de l'ACIA.</p>\n</details>\n\n<p>Le laboratoire des v\u00e9g\u00e9taux d'Ottawa, situ\u00e9 sur le <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/ottawa-fallowfield-/fra/1549570125940/1549570207759\">campus <span lang=\"en\">Fallowfield</span></a>, fait partie du r\u00e9seau des laboratoires de l'Ontario de l'ACIA et est le plus gros laboratoire des v\u00e9g\u00e9taux de l'Agence. Le laboratoire fournit des services de diagnostic, de recherche et de conseils scientifiques \u00e0 l'appui des programmes de la protection des v\u00e9g\u00e9taux et des semences de l'ACIA afin de prot\u00e9ger les ressources agricoles et foresti\u00e8res du Canada. Le laboratoire fournit \u00e9galement des services de diagnostic pour certifier les plantes, les semences et les produits d\u00e9riv\u00e9s des plantes canadiennes pour l'exportation.</p>\n\n<p>Le laboratoire des v\u00e9g\u00e9taux d'Ottawa accueille professionnels scientifiques et techniques qui fournissent des analyses de laboratoire de pointe, des conseils scientifiques et des activit\u00e9s de recherche. Les chercheurs scientifiques et les professionnels de ce laboratoire collaborent avec des organisations nationales et internationales \u00e0 l'\u00e9laboration de nouvelles m\u00e9thodes de d\u00e9tection et d'identification des phytoravageurs, des esp\u00e8ces v\u00e9g\u00e9tales et des vari\u00e9t\u00e9s v\u00e9g\u00e9tales. Ils font \u00e9galement office d'experts en la mati\u00e8re au sein de groupes nationaux et internationaux charg\u00e9s d'\u00e9laborer des normes, des proc\u00e9dures et des r\u00e8glements internationaux.</p>\n\n<h2>Nos activit\u00e9s</h2>\n\n<h3>Diagnostic</h3>\n\n<h4>Phytopathologie</h4>\n\n<ul>\n<li>Fournir des services de diagnostic r\u00e9glementaire pour l'identification des champignons phytopathog\u00e8nes, des bact\u00e9ries et des virus transmis par les semences dans divers produits agricoles destin\u00e9s \u00e0 l'importation, \u00e0 l'exportation et \u00e0 la consommation int\u00e9rieur, \u00e0 l'appui du programme de protection des v\u00e9g\u00e9taux de l'ACIA.</li>\n<li>Fournir des tests r\u00e9glementaires de sant\u00e9 des semences et l'accr\u00e9ditation \u00e0 l'appui du programme des semences de l'ACIA</li>\n</ul>\n\n<h4>N\u00e9matologie</h4>\n\n<ul>\n<li>Fournir des services de diagnostic pour la d\u00e9tection et l'identification des n\u00e9matodes phytoparasites dans le sol, les semences, les milieux de culture, les plantes et les produits v\u00e9g\u00e9taux.</li>\n<li>Identifier la pr\u00e9sence de sol contaminant les produits  import\u00e9s et export\u00e9s et caract\u00e9riser les milieux de culture.</li>\n</ul>\n\n<h4>Entomologie</h4>\n\n<ul>\n<li>Fournir la d\u00e9tection et l'identification d'insectes, acariens et mollusques terrestres sur des plantes et des produits v\u00e9g\u00e9taux import\u00e9s, export\u00e9s et d\u00e9plac\u00e9s dans le pays.</li>\n<li>Fournir un soutien diagnostique pour les enqu\u00eates visant \u00e0 d\u00e9tecter les changements dans la distribution des organismes de quarantaine au Canada.</li>\n<li>Fournir un soutien diagnostique pour la d\u00e9tection des nouvelles introductions de ravageurs exotiques envahissants dont la pr\u00e9sence au Canada n'est pas encore connue ou dont la r\u00e9partition est limit\u00e9e.</li>\n</ul>\n\n<h4>G\u00e9notypage \u2013 botanique</h4>\n\n<ul>\n<li>Fournir des services de diagnostic li\u00e9s \u00e0 l'identification et \u00e0 la v\u00e9rification des esp\u00e8ces et des vari\u00e9t\u00e9s v\u00e9g\u00e9tales par la d\u00e9tection qualitative, la quantification et le g\u00e9notypage mol\u00e9culaire des esp\u00e8ces cultiv\u00e9es, des plantes exotiques envahissantes, des plantes \u00e0 caract\u00e8res nouveaux et des semences.</li>\n</ul>\n\n<h4>Semences</h4>\n\n<ul>\n<li>Effectuer une vari\u00e9t\u00e9 de m\u00e9thodes d'essais pour contr\u00f4ler la puret\u00e9 vari\u00e9tale et l'identit\u00e9 vari\u00e9tale des semences sur le march\u00e9 afin de donner confiance dans l'int\u00e9grit\u00e9 du syst\u00e8me canadien de certification des semences.</li>\n</ul>\n\n<h3>Recherche</h3>\n\n<h4>Laboratoire de recherche sur l'identification mol\u00e9culaire</h4>\n\n<ul>\n<li>D\u00e9velopper des m\u00e9thodes de diagnostic mol\u00e9culaire avanc\u00e9es pour la d\u00e9tection, la v\u00e9rification, l'identification et la diff\u00e9renciation des cultivars ou des esp\u00e8ces v\u00e9g\u00e9tales.</li>\n<li>Fournir des m\u00e9thodes valid\u00e9es et des ensembles de donn\u00e9es \u00e0 l'appui des services de diagnostic.</li>\n</ul>\n\n<h4>Laboratoire de recherche sur l'identification des pathog\u00e8nes</h4>\n\n<ul>\n<li>\u00c9laborer des m\u00e9thodes de diagnostic mol\u00e9culaire avanc\u00e9es (comme l'utilisation de la g\u00e9nomique) pour la d\u00e9tection et l'identification ou le g\u00e9notypage des agents pathog\u00e8nes \u00e0 l'intention des laboratoires de diagnostic et pour appuyer d'autres activit\u00e9s de l'ACIA, y compris les enqu\u00eates.</li>\n</ul>\n\n<h4>Laboratoire de recherche en entomologie</h4>\n\n<ul>\n<li>D\u00e9velopper des outils de diagnostic mol\u00e9culaire pour la d\u00e9tection et l'identification des n\u00e9matodes phytoparasites.</li>\n<li>\u00c9tudier la biologie et la distribution des n\u00e9matodes r\u00e9glement\u00e9s au Canada.</li>\n</ul>\n\n<h4>Laboratoire de recherche en n\u00e9matologie</h4>\n\n<ul>\n<li>Mettre au point des outils de diagnostic mol\u00e9culaire pour la d\u00e9tection et l'identification des n\u00e9matodes phytoparasites.</li>\n<li>\u00c9tudier la biologie et la r\u00e9partition des n\u00e9matodes r\u00e9glement\u00e9s au Canada.</li>\n</ul>\n\n<h2>Gestion de la qualit\u00e9</h2>\n\n<p>Tous les laboratoires de l'ACIA sont accr\u00e9dites conform\u00e9ment \u00e0 la norme ISO/IEC 17025, <i>Exigences g\u00e9n\u00e9rales concernant la comp\u00e9tence des laboratoires d'\u00e9talonnage et d'essais</i>. Le Conseil canadien des normes (CCN) accorde l'accr\u00e9ditation pour les essais de routine, l'\u00e9laboration de m\u00e9thodes d'essai et les essais non routiniers, comme l'indique la port\u00e9e d'accr\u00e9ditation du laboratoire sur le <a href=\"https://www.scc.ca/fr/accreditation/laboratoires/agence-canadienne-dinspection-des-aliments-laboratoire-dottawa-fallowfield\">site Web du CCN</a>. L'accr\u00e9ditation v\u00e9rifie officiellement la comp\u00e9tence de l'ACIA \u00e0 produire des r\u00e9sultats pr\u00e9cis et fiables. Ces r\u00e9sultats sont \u00e9tay\u00e9s par l'\u00e9laboration, la validation et la mise en \u0153uvre de m\u00e9thodes scientifiques, men\u00e9es par un personnel hautement qualifi\u00e9, \u00e0 l'aide de produits, de services et d'\u00e9quipement fiables, dans un environnement de qualit\u00e9 contr\u00f4l\u00e9e. La participation \u00e0 des programmes internationaux d'essais d'aptitude d\u00e9montre en outre que nos analyses sont comparables \u00e0 celles de laboratoires du Canada et du monde entier.</p>\n<h2>Renseignements</h2>\n\n<p>Le laboratoire des v\u00e9g\u00e9taux d'Ottawa<br>\n<span lang=\"en\">Fallowfield</span><br>\n<br>\n</p>\n\n<p>Renseignez-vous sur les autres <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a>.</p>\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}