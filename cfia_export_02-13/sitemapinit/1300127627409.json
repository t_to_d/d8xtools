{
    "dcr_id": "1300127512994",
    "title": {
        "en": "Automated Import Reference System (AIRS)",
        "fr": "Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)"
    },
    "html_modified": "2024-02-13 9:46:38 AM",
    "modified": "2023-12-18",
    "issued": "2015-04-08 14:27:33",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_impo_airs_1300127512994_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_impo_airs_1300127512994_fra"
    },
    "ia_id": "1300127627409",
    "parent_ia_id": "1299168593866",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food",
        "fr": "Importation d\u2019aliments"
    },
    "commodity": {
        "en": "Relevant to All",
        "fr": "Pertinents pour tous"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299168593866",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Automated Import Reference System (AIRS)",
        "fr": "Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)"
    },
    "label": {
        "en": "Automated Import Reference System (AIRS)",
        "fr": "Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1300127627409",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299168480001",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Automated Import Reference System (AIRS)",
            "fr": "Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)"
        },
        "description": {
            "en": "The Automated Import Reference System (AIRS) shows the import requirements for Canadian Food Inspection Agency (CFIA) regulated commodities.",
            "fr": "Le Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI) montre les exigences d'importation pour les produits r\u00e9glement\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "Automated Import Reference System, series of questions, purpose of the AIRS, import requirements, applicable regulations",
            "fr": "r\u00e8glements pertinents, bloqueur de fen\u00eatres publicitaires int, consultation des politiques, Lois adopt\u00e9es, liste des documents"
        },
        "dcterms.subject": {
            "en": "registration,imports,systems",
            "fr": "enregistrement,importation,syst\u00e8me"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-08 14:27:33",
            "fr": "2015-04-08 14:27:33"
        },
        "modified": {
            "en": "2023-12-18",
            "fr": "2023-12-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "educators,business,general public",
            "fr": "\u00e9ducateurs,entreprises,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Automated Import Reference System (AIRS)",
        "fr": "Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)"
    },
    "body": {
        "en": "        \r\n        \n<!-- **Hide under here when not in use alert-warning ending**\n\n\n-->\n\n<section class=\"alert alert-warning\" id=\"add-imports-maintenance\">\n<h2>Maintenance notice</h2>\n<p>The following services may not be available on February\u00a015,\u00a02024 from 6:00\u00a0am to 7:00\u00a0am\u00a0(ET):</p>\n<ul>\n<li>Electronic Data Interchange\u00a0\u2013 Integrated Import Declaration (EDI\u00a0\u2013 IID)</li>\n</ul>\n<p class=\"mrgn-tp-md\">The following services  may not be available on February\u00a018,\u00a02024 from 3:00\u00a0am to 7:00\u00a0am\u00a0(ET):</p>\n<ul>\n<li>Automated Import Reference System (AIRS)</li>\n<li>Automated Import Reference System Verification Service  (AVS)</li>\n<li>CFIA Shipment Tracker for Food, Plant and Animal  products</li>\n<li>Electronic Data Interchange\u00a0\u2013 Integrated Import  Declaration (EDI\u00a0\u2013 IID)</li>\n</ul>\n<p class=\"mrgn-tp-md\">The following services  may not be available on February\u00a019,\u00a02024 from 6:00\u00a0am to 7:00\u00a0am\u00a0(ET):</p>\n<ul>\n<li>Electronic Data Interchange\u00a0\u2013 Integrated Import  Declaration (EDI\u00a0\u2013 IID)</li>\n</ul>\n<p class=\"mrgn-tp-md\">If you require assistance with an admissibility release process for the above noted services please contact the National Import Service Centre (NISC) <a href=\"mailto:cfia.nisc-csni.acia@inspection.gc.ca\" class=\"nowrap\">cfia.nisc-csni.acia@inspection.gc.ca</a> for guidance.</p>\n</section>\n\n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">What is AIRS</a></li>\n<li><a href=\"#a21\">Launch AIRS</a></li>\n<li><a href=\"#a2\">How to get started</a></li>\n<li><a href=\"#a3\">User guide</a></li>\n<li><a href=\"#a4\">Codes and registration types</a></li>\n<li><a href=\"#a5\">Communications and updates</a></li>\n<li><a href=\"#a6\">AIRS Verification Service</a></li>\n</ul>\n\n<h2 id=\"a1\">What is AIRS</h2>\n\n<p>The Automated Import Reference System (AIRS) is a reference tool that shows the import requirements for Canadian Food Inspection Agency (CFIA) regulated commodities.</p>\n\n<h2 id=\"a21\">Launch AIRS</h2>\n\n<div class=\"row mrgn-tp-lg\">\n\n<div class=\"col-sm-6\">\n<button onclick=\"window.open('http://airs-sari.inspection.gc.ca/airs_external/english/decisions-eng.aspx', '_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=0,top=0,width=' + (window.screen.availWidth-10) + ',height=' + (window.screen.availHeight-50));\" class=\"btn btn-call-to-action btn-success btn-lg btn-block\">\n<span class=\"text-uppercase\">Launch</span> AIRS (opens in new window)\n<hr>\n<small>Automated Import Reference System</small></button>\n</div>\n\n<div class=\"col-sm-6\">\n<a href=\"/food-safety-for-industry/video/airs-tutorial/eng/1528316420730/1528316421089\" class=\"btn btn-call-to-action btn-primary btn-lg btn-block\">\n<span class=\"text-uppercase\">Tutorial</span>\n<hr>\n<small>Automated Import Reference System</small></a>\n</div>\n\n</div>\n\n<div class=\"clearfix\"></div>\n\n\n<h2 id=\"a2\">How to get started</h2>\n\n\n<p>Before launching AIRS, please note the following:</p>\n\n<ul>\n<li>AIRS is a tool for reference only</li>\n\n<li>users should consult Canadian acts and regulations to interpret and apply the law</li>\n\n<li>importers should consult acts as passed by Parliament to interpret and apply the law</li>\n<li>AIRS information is updated frequently; please check back regularly</li>\n<li>importers are responsible for ensuring their information is accurate at the time of importation</li>\n</ul>\n\n<p>When accessing the AIRS tool, you will be able to conduct your search in one of two ways:</p>\n\n<ul>\n<li>find by commodity</li>\n<li>select by drill-down</li>\n</ul>\n\n<p>Your selection will include the following options:</p>\n\n<ul>\n<li>commodity to be imported</li>\n<li>origin of the commodity</li>\n<li>destination of the commodity</li>\n<li>end use pertaining to the commodity</li>\n</ul>\n\n<p>Once the information above has been provided, you will be able to view the Harmonized System (HS) classification code for the selected commodity.</p>\n\n<h2 id=\"a3\">User guide</h2>\n\n\n\n<p>Visit the <a href=\"https://airs-sari.inspection.gc.ca/airs_external/english/help-eng.aspx\">AIRS User Guide</a> for:</p>\n\n<ul>\n<li>instructions on how to make a selection using the \"HS Code\" drill-down feature</li>\n<li>instructions on how to use the \"Find by Commodity\" feature</li>\n<li>further information for Electronic Data Interchange (EDI) users</li>\n</ul>\n\n\n\n<h2 id=\"a4\">AIRS codes and registration types</h2>\n\n<ul>\n<li><a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/end-use-codes/eng/1494615798788/1494615880361\">AIRS end use codes</a></li>\n<li><a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/miscellaneous-codes/eng/1494618949803/1494618982962\">AIRS miscellaneous codes</a></li>\n<li><a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/registration-types/eng/1494610401418/1494610650139\">CFIA AIRS registration types</a></li>\n</ul>\n\n<h2 id=\"a5\">Communications and updates</h2>\n\n<h3>Automatic licence verification for imports of manufactured foods</h3>\n\n<p>On <strong>February\u00a012, 2024</strong>, the CFIA will activate automatic licence verification for imports of <a href=\"/eng/1543359915240/1543360663242#anna6\">manufactured foods</a>. If you need a licence to import, you must enter a valid licence number on your import declaration or <strong>your shipment will be denied entry into Canada</strong>.</p>\n\n<p>Visit <a href=\"/food-licences/importing-food-with-a-valid-sfc-licence/eng/1608588974837/1608588975118\">Importing food with a valid Safe Food for Canadians licence</a>.</p>\n\n<h3>Fresh fruit and vegetable imports\u00a0\u2013 Rejection notice for missing Ministerial Exemption</h3>\n\n<p><strong>Important:</strong> please be advised that AIRS has not been updated to reflect recent changes that repealed standard container sizes for some fresh fruit and vegetables. For affected importers receiving a rejection notice from the Integrated Import Declaration system for a missing Ministerial Exemption, the following interim measure has been implemented:</p>\n\n<ul class=\"list-unstyled mrgn-lft-md lst-spcd\">\n<li>step 1: cancel your submission in the Integrated Import Declaration system</li>\n\n<li>step 2: prepare a paper declaration using Form <a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5272/eng/1467026113151/1467026113683\">CFIA/ACIA 5272\u00a0\u2013\u00a0Request for Documentation Review</a>\u00a0\u2013\u00a0Canadian Food Inspection Agency (canada.ca) and include all required documentation</li>\n\n<li>step 3: submit all documents to the National Import Service Centre (NISC) via email at <a href=\"mailto:cfia.nisc-csni.acia@inspection.gc.ca\" class=\"nowrap\">cfia.nisc-csni.acia@inspection.gc.ca</a> and include 'Ministerial Exemption' in the subject line for prompt processing</li>\n</ul>\n\n<h3>Stay informed</h3>\n\n<p>Subscribe to the <a href=\"https://notification.inspection.canada.ca/\">CFIA's email notification service</a> to stay up to date on important information, including additions and modifications of commodities, changes to import requirements and product admissibility. You can <a href=\"https://notification.inspection.canada.ca/\">modify your preferences</a> for this service at any time.</p>\n\n<h2 id=\"a6\">AIRS Verification Service</h2>\n\n<ul>\n<li><a href=\"/importing-food-plants-or-animals/food-imports/airs/avs/eng/1553708673052/1553708673301\">AIRS Verification Service (AVS)</a></li>\n<li><a href=\"/importing-food-plants-or-animals/food-imports/airs/avs/technical-manual/eng/1553710775247/1553710775500\">AVS technical manual</a></li>\n<li><a href=\"/importing-food-plants-or-animals/food-imports/airs/avs/airs-verification-service/eng/1553782504366/1553782504620\">AVS: Frequently asked questions</a></li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!-- **Hide under here when not in use alert-warning ending**\n\n\n-->\n\n<section class=\"alert alert-warning\" id=\"add-imports-maintenance\">\n<h2>Avis d'entretien</h2>\n<p>Les services suivants ne seront peut-\u00eatre pas disponibles le 15\u00a0f\u00e9vrier\u00a02024 de 06\u00a0h\u00a000 \u00e0 07\u00a0h\u00a000\u00a0(HE)\u00a0:</p>\n<ul>\n<li>\u00c9change de donn\u00e9es informatis\u00e9\u00a0\u2013 D\u00e9claration d'importation int\u00e9gr\u00e9e (EDI\u00a0\u2013 DII)</li>\n</ul>\n<p class=\"mrgn-tp-md\">Les services suivants ne seront peut-\u00eatre pas disponibles le 18\u00a0f\u00e9vrier\u00a02024 de 03\u00a0h\u00a000 \u00e0 07\u00a0h\u00a000\u00a0(HE):</p>\n<ul>\n<li>Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</li>\n<li>Service de v\u00e9rification du Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SVS)</li>\n<li>Suivi des envois d'aliments, de v\u00e9g\u00e9taux et d'animaux de l'ACIA</li>\n<li>\u00c9change de donn\u00e9es informatis\u00e9\u00a0\u2013 D\u00e9claration d'importation int\u00e9gr\u00e9e (EDI\u00a0\u2013 DII)</li>\n</ul>\n<p class=\"mrgn-tp-md\">Les services suivants ne seront peut-\u00eatre pas disponibles le 19\u00a0f\u00e9vrier\u00a02024 de 06\u00a0h\u00a000 \u00e0 07\u00a0h\u00a000\u00a0(HE):</p>\n<ul>\n<li>\u00c9change de donn\u00e9es informatis\u00e9\u00a0\u2013 D\u00e9claration d'importation int\u00e9gr\u00e9e (EDI\u00a0\u2013 DII)</li>\n</ul>\n<p class=\"mrgn-tp-md\">Si vous avez besoin d'aide pour le processus de rel\u00e2che concernant les services mentionn\u00e9s ci-haut, veuillez communiquer avec le Centre national de service \u00e0 l'importation <a href=\"mailto:cfia.nisc-csni.acia@inspection.gc.ca\" class=\"nowrap\">cfia.nisc-csni.acia@inspection.gc.ca</a>.</p>\n</section>\n\n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Qu'est-ce que c'est SARI</a></li>\n<li><a href=\"#a21\">Lancer SARI</a></li>\n<li><a href=\"#a2\">\u00c0 retenir</a></li>\n<li><a href=\"#a3\">Guide de r\u00e9f\u00e9rence</a></li>\n<li><a href=\"#a4\">Les codes et types d'enregistrement</a></li>\n<li><a href=\"#a5\">Communications et mises \u00e0 jour</a></li>\n<li><a href=\"#a6\">Service de v\u00e9rification du SARI</a></li>\n</ul>\n\n<h2 id=\"a1\">Qu'est-ce que c'est SARI</h2>\n\n<p>Le Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI) est un outil de recherche qui montre les exigences d'importation pour les produits r\u00e9glement\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA).</p>\n\n\n<h2 id=\"a21\">Lancer SARI</h2>\n\n<div class=\"row mrgn-tp-lg\">\n\n<div class=\"col-sm-6\">\n<button onclick=\"window.open('http://airs-sari.inspection.gc.ca/AIRS_External/francais/decisions-fra.aspx', '_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=0,top=0,width=' + (window.screen.availWidth-10) + ',height=' + (window.screen.availHeight-50));\" class=\"btn btn-call-to-action btn-success btn-lg btn-block\">\n<span class=\"text-uppercase\">Lancer</span> SARI (s'ouvrira dans une nouvelle fen\u00eatre)\n<hr>\n<small>Le syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation</small></button>\n</div>\n\n<div class=\"col-sm-6\">\n<a href=\"/salubrite-alimentaire-pour-l-industrie/video/tutoriel-sur-le-sari-video-/fra/1528316420730/1528316421089\" class=\"btn btn-call-to-action btn-primary btn-lg btn-block\">\n<span class=\"text-uppercase\">Tutoriel</span> SARI\n<hr>\n<small>Le syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation</small></a>\n</div>\n\n</div>\n\n<div class=\"clearfix\"></div>\n\n<h2 id=\"a2\">\u00c0 retenir</h2>\n\n<p>Avant de d\u00e9buter dans le SARI, veuillez noter ceci\u00a0:</p>\n\n<ul>\n<li>le SARI est uniquement un outil de r\u00e9f\u00e9rence</li>\n\n<li>aux fins d'interpr\u00e9tation et d'application de la loi, les utilisateurs sont invit\u00e9s \u00e0 consulter les lois et r\u00e8glements du Canada</li>\n\n<li>l'information du SARI est mise \u00e0 jour fr\u00e9quemment, veuillez v\u00e9rifier r\u00e9guli\u00e8rement pour tout changement</li>\n<li>les importateurs doivent s'assurer que les renseignements sont exacts au moment de l'importation</li>\n</ul>\n\n<p>Lors de l'acc\u00e8s au SARI, vous pourrez effectuer votre recherche de deux\u00a0fa\u00e7ons\u00a0:</p>\n\n<ul>\n<li>trouver par produits</li>\n<li>s\u00e9lectionner au moyen du menu d\u00e9filant</li>\n</ul>\n\n<p>Votre s\u00e9lection comprendra les options suivantes\u00a0:</p>\n\n<ul>\n<li>produit \u00e0 importer</li>\n\n\n<li>origine du produit</li>\n<li>destination du produit</li>\n<li>utilisation finale du produit</li>\n</ul>\n\n<p>D\u00e8s que les informations auront \u00e9t\u00e9 fournies, vous pourrez visualiser les codes de classification du syst\u00e8me harmonis\u00e9 (SH) du produit s\u00e9lectionn\u00e9.</p>\n\n<h2 id=\"a3\">Guide de r\u00e9f\u00e9rence</h2>\n\n<p>Consulter le <a href=\"https://airs-sari.inspection.gc.ca/airs_external/francais/aide-fra.aspx\">Guide de r\u00e9f\u00e9rence du SARI</a> pour plus de renseignement sur\u00a0:</p>\n\n<ul>\n<li>comment s\u00e9lectionner les codes SH du menu d\u00e9filant</li>\n<li>comment utiliser la fonction \u00ab\u00a0Trouver par produits\u00a0\u00bb</li>\n<li>les instructions du syst\u00e8me d'\u00c9change de donn\u00e9es informatis\u00e9 (\u00c9DI)</li>\n</ul>\n\n<h2 id=\"a4\">Les codes et types d'enregistrement du SARI</h2>\n\n<ul>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/codes-d-usages-finaux/fra/1494615798788/1494615880361\">Codes d'usages finaux du SARI</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/codes-divers/fra/1494618949803/1494618982962\">Codes divers du SARI</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/types-d-enregistrement/fra/1494610401418/1494610650139\">Types d'enregistrement du SARI</a></li>\n</ul>\n\n<h2 id=\"a5\">Communications et mises \u00e0 jour</h2>\n\n<h3>V\u00e9rification automatique des licences pour les importations d'aliments manufactur\u00e9s</h3>\n\n<p>Le\u00a0<strong>12 f\u00e9vrier\u00a02024</strong>, l'ACIA va activer la v\u00e9rification automatique des licences pour les importations d'<a href=\"/fra/1543359915240/1543360663242#anna6\">aliments manufactur\u00e9s</a>. Si vous avez besoin d'une licence pour l'importation, vous devez entrer un num\u00e9ro de licence valide sur votre d\u00e9claration d'importation, sinon <strong>votre envoi sera interdit d'entrer au Canada</strong>.</p>\n\n<p>Visitez <a href=\"/licences-pour-aliments/importer-des-aliments-en-utilisant-une-licence-pou/fra/1608588974837/1608588975118\">Importer des aliments en utilisant une licence pour la salubrit\u00e9 des aliments au Canada valide</a></p>\n\n<h3>Importations de fruits et l\u00e9gumes frais \u2013 avis de rejet pour une exemption minist\u00e9rielle manquante</h3>\n\n<p><strong>Important\u00a0:</strong> veuillez noter que le SARI n'a pas \u00e9t\u00e9 mis \u00e0 jour pour tenir compte des changements r\u00e9cents qui ont abrog\u00e9 les tailles de contenant standard pour certains fruits et l\u00e9gumes frais. Pour les importateurs touch\u00e9s qui re\u00e7oivent un avis de rejet du syst\u00e8me de d\u00e9claration int\u00e9gr\u00e9e des importations pour une exemption minist\u00e9rielle manquante, la mesure provisoire suivante a \u00e9t\u00e9 mise en \u0153uvre\u00a0:</p>\n\n<ul class=\"list-unstyled mrgn-lft-md lst-spcd\">\n<li>\u00e9tape 1: annuler votre soumission dans le syst\u00e8me int\u00e9gr\u00e9 de d\u00e9claration des importations</li>\n\n<li>\u00e9tape 2: pr\u00e9parer une d\u00e9claration papier \u00e0 l'aide du formulaire <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5272/fra/1467026113151/1467026113683\">CFIA/ACIA 5272\u00a0\u2013\u00a0Demande de Revision de Documents</a>\u00a0\u2013\u00a0Agence canadienne d'inspection des aliments (canada.ca) et inclure tous les documents requis</li>\n\n<li>\u00e9tape 3: soumettre tous les documents au Centre national des services d'importation (CSNI) par courriel \u00e0 <a href=\"mailto:cfia.nisc-csni.acia@inspection.gc.ca\" class=\"nowrap\">cfia.nisc-csni.acia@inspection.gc.ca</a> et inclure \u00ab\u00a0Exemption minist\u00e9rielle\u00a0\u00bb dans la ligne d'objet pour un traitement rapide</li>\n</ul>\n\n<h3>Restez au courant</h3>\n\n<p>Pour vous tenir \u00e0 jour sur les informations importantes du SARI, y compris les ajouts et les modifications de produits, les changements aux exigences d'importation et \u00e0 l'admissibilit\u00e9 des produits, inscrivez-vous au <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">service de notification par courriel de l'ACIA</a>. Vous pouvez <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">modifier vos pr\u00e9f\u00e9rences</a> pour ce service \u00e0 tout moment.</p>\n\n<h2 id=\"a6\">Service de v\u00e9rification du SARI</h2>\n\n<ul>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/sari/svs/fra/1553708673052/1553708673301\">Service de v\u00e9rification du Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SVS)</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/sari/svs/manuel-technique/fra/1553710775247/1553710775500\">Service de validation de SARI (SVS) Manuel technique</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/sari/svs/service-de-verification-du-sari/fra/1553782504366/1553782504620\">Document de la Foire aux questions du SVS</a></li>\n</ul>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}