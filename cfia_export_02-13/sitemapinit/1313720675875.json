{
    "dcr_id": "1313720601375",
    "title": {
        "en": "Terrestrial Animal Disease Surveillance",
        "fr": "Surveillance des maladies d'animaux terrestres"
    },
    "html_modified": "2024-02-13 9:47:01 AM",
    "modified": "2016-10-06",
    "issued": "2011-08-18 22:23:24",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_surveillance_index_1313720601375_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_surveillance_index_1313720601375_fra"
    },
    "ia_id": "1313720675875",
    "parent_ia_id": "1300388449143",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1300388449143",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Terrestrial Animal Disease Surveillance",
        "fr": "Surveillance des maladies d'animaux terrestres"
    },
    "label": {
        "en": "Terrestrial Animal Disease Surveillance",
        "fr": "Surveillance des maladies d'animaux terrestres"
    },
    "templatetype": "content page 1 column",
    "node_id": "1313720675875",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300388388234",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/surveillance/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/surveillance-des-maladies/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Terrestrial Animal Disease Surveillance",
            "fr": "Surveillance des maladies d'animaux terrestres"
        },
        "description": {
            "en": "Animal disease surveillance supports Canada's ability to recognize and deal with emerging animal disease problems. Surveillance also plays an import role in providing Canadian livestock and poultry products access to more markets.",
            "fr": "La surveillance des maladies animales rehausse la capacit\u00e9 du Canada de reconna\u00eetre et de traiter les probl\u00e8mes associ\u00e9s \u00e0 l'\u00e9mergence de nouvelles maladies animales. La surveillance aide \u00e0 l'acc\u00e8s des produits canadiens du b\u00e9tail et de la volaille \u00e0 un plus grand nombre de march\u00e9s."
        },
        "keywords": {
            "en": "cattle identification program, surveillance, sheep identification program. animal health, disease, detection, Canadian Animal Health Surveillance Network",
            "fr": "programme d&#39;identification du b&#233;tail, surveillance, programme d'identification des moutons animale, sant&#233; animal, maladie, d&#233;tection, R\u00e9seau canadien de surveillance zoosanitaire"
        },
        "dcterms.subject": {
            "en": "animal health,exports,imports,inspection,veterinary medicine",
            "fr": "sant\u00e9 animale,exportation,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-08-18 22:23:24",
            "fr": "2011-08-18 22:23:24"
        },
        "modified": {
            "en": "2016-10-06",
            "fr": "2016-10-06"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Terrestrial Animal Disease Surveillance",
        "fr": "Surveillance des maladies d'animaux terrestres"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Animal disease surveillance supports Canada's ability to recognize  and deal with emerging animal disease problems. Surveillance also plays  an important role in providing Canadian livestock and poultry products access  to more markets.</p>\n<p>The Canadian Food Inspection Agency's surveillance activities are supported by:</p>\n<ul>\n<li>a nationwide network known as Canadian Animal Health Surveillance Network (CAHSN),  which draws on the disease detection capabilities of practising veterinarians, provincial and  university diagnostic laboratories and the federal government;</li>\n<li>animal identification programs which help trace disease outbreaks to their source.</li>\n</ul>\n<h2>What information is available?</h2>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/traceability/eng/1300461751002/1300461804752\">Livestock traceability</a> </li>\n<li>Updates on surveillance activities related to:            \n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/surveillance/avian-influenza-surveillance/eng/1329693810008/1329694298513\">Avian Influenza</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/bovine-spongiform-encephalopathy/enhanced-surveillance/eng/1323992647051/1323992718670\">Bovine Spongiform Encephalopathy</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/surveillance/bovine-surveillance-system-bss-/eng/1399042076293/1399042275724\">Bovine Surveillance System</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/rabies/in-canada/eng/1356156989919/1356157139999\">Rabies</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/surveillance/swine-disease-surveillance/eng/1329691307451/1329691665112\">Swine diseases</a></li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La surveillance des maladies animales rehausse la capacit\u00e9 du Canada de reconna\u00eetre et de traiter les probl\u00e8mes associ\u00e9s \u00e0 l'\u00e9mergence de nouvelles maladies animales. La surveillance aide \u00e0 l'acc\u00e8s des produits canadiens du b\u00e9tail et de la volaille \u00e0 un plus grand nombre de march\u00e9s.</p>\n<p>Les activit\u00e9s de surveillance de l'Agence canadienne d'inspection des aliments sont soutenues par\u00a0:</p>\n<ul>\n<li>un r\u00e9seau \u00e0 l'\u00e9chelle du Canada, d\u00e9nomm\u00e9 R\u00e9seau canadien de surveillance zoosanitaire (RCSZ), a regroup\u00e9 les capacit\u00e9s de d\u00e9pistage des maladies des v\u00e9t\u00e9rinaires praticiens, des laboratoires de diagnostic provinciaux et universitaires et du gouvernement f\u00e9d\u00e9ral;</li>\n<li>des programmes du b\u00e9tail canadien afin de permettre aux enqu\u00eateurs de d\u00e9terminer d'o\u00f9 provient un animal infect\u00e9 quel que soit l'endroit o\u00f9 un diagnostic de maladie est \u00e9tabli.</li>\n</ul>\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition?</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/tracabilite/fra/1300461751002/1300461804752\">Tra\u00e7abilit\u00e9 d'animaux terrestres</a></li>\n<li>Mise \u00e0 jour des activit\u00e9s de surveillance reli\u00e9s \u00e0\u00a0:          \n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/encephalopathie-spongiforme-bovine/surveillance-intensifiee/fra/1323992647051/1323992718670\">Enc\u00e9phalopathie spongiforme bovine</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/surveillance-des-maladies/surveillance-de-l-influenza-aviaire/fra/1329693810008/1329694298513\">Influenza aviaire</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/surveillance-des-maladies/surveillance-des-maladies-porcines/fra/1329691307451/1329691665112\">Maladies porcines</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/rage/au-canada/fra/1356156989919/1356157139999\">La rage</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/surveillance-des-maladies/systeme-de-surveillance-des-bovins-ssb-/fra/1399042076293/1399042275724\">Syst\u00e8me de surveillance des bovins</a></li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}