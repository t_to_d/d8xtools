{
    "dcr_id": "1344821232793",
    "title": {
        "en": "Basic Principles of Biosecurity",
        "fr": "Principes fondamentaux de la bios\u00e9curit\u00e9"
    },
    "html_modified": "2024-03-12 3:55:47 PM",
    "modified": "2012-08-12",
    "issued": "2012-08-12 21:27:16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_tools_principles_1344821232793_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_tools_principles_1344821232793_fra"
    },
    "ia_id": "1344821360235",
    "parent_ia_id": "1344790183249",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Biosecurity",
        "fr": "Bios\u00e9curit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1344790183249",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Basic Principles of Biosecurity",
        "fr": "Principes fondamentaux de la bios\u00e9curit\u00e9"
    },
    "label": {
        "en": "Basic Principles of Biosecurity",
        "fr": "Principes fondamentaux de la bios\u00e9curit\u00e9"
    },
    "templatetype": "content page 1 column",
    "node_id": "1344821360235",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1344790074044",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/tools/basic-principles/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/outils/principes-fondamentaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Basic Principles of Biosecurity",
            "fr": "Principes fondamentaux de la bios\u00e9curit\u00e9"
        },
        "description": {
            "en": "A biosecurity plan should address how you manage animal, vehicle and human access on the farm; animal health; and operations.",
            "fr": "Le plan de bios\u00e9curit\u00e9 devrait stipuler la fa\u00e7on dont vous g\u00e9rez l'acc\u00e8s qu'ont les animaux, les v\u00e9hicules et les personnes \u00e0 votre exploitation, ainsi que la sant\u00e9 des animaux et les op\u00e9rations."
        },
        "keywords": {
            "en": "biosecurity, animal health, diseases, farms, access management, animal health management, operational management",
            "fr": "bios\u00e9curit\u00e9, sant\u00e9 des animaux, maladies, fermes, gestion de l'acc\u00e8s, gestion de la sant\u00e9 des animaux, gestion des op\u00e9rations"
        },
        "dcterms.subject": {
            "en": "livestock,imports,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-08-12 21:27:16",
            "fr": "2012-08-12 21:27:16"
        },
        "modified": {
            "en": "2012-08-12",
            "fr": "2012-08-12"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Basic Principles of Biosecurity",
        "fr": "Principes fondamentaux de la bios\u00e9curit\u00e9"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Putting preventive measures in place to keep animals healthy has been a long-standing and successful practice on Canadian farms. These measures form a biosecurity plan. A biosecurity plan should address how you manage animal, vehicle and human access on the farm; animal health; and operations.</p>\n<p>By following the principles below and working with a veterinarian you can play a significant role in keeping your animals and your industry as healthy as possible.</p>\n<h2>Access Management</h2>\n<h3>Designate distinct zones</h3>\n<p>Establish distinct zones where varying levels of protection are needed. Define these zones with fences (or other features) and identify them with signs.</p>\n<h3>Control movements in and between designated zones</h3>\n<p>Control movements of people, animals, equipment and vehicles</p>\n<ul>\n<li>into a designated zone,</li>\n<li>out of a designated zone, and</li>\n<li>between the designated zones.</li>\n</ul>\n<p>This can be done through the use of controlled access points.</p>\n<h2>Animal Health Management</h2>\n<h3>Manage animal movements</h3>\n<p>Plan animal introductions, their movement within the premises and their removal from the premises. This includes using management strategies such as:</p>\n<ul>\n<li>permanently identifying all animals and keeping records for traceability,</li>\n<li>testing animals before introduction,</li>\n<li>following post arrival isolation procedures,</li>\n<li>scheduling animal movements ahead of time, and</li>\n<li>maximizing downtime in production areas between animal groups.</li>\n</ul>\n<p>Practice animal identification and good record keeping. It is important to participate in traceability systems where available.</p>\n<h3>Observe animals for signs of disease</h3>\n<p>Ensure workers are knowledgeable and experienced in recognizing signs of disease. They should be able to do this by observing animals' production levels, behaviour, clinical signs, and feed and water consumption.</p>\n<h3>Establish response plans for potential disease situations</h3>\n<p>Contact a veterinarian if you see unusual rates of disease or death.</p>\n<p>Work with your veterinarian to have a &amp;quot;disease response plan\" in place for suspected cases of contagious or reportable diseases. A disease response plan should include:</p>\n<ul>\n<li>triggers for the response plan (for example, numerous animals showing signs of disease, a significant decrease in production, a lack of response to routine treatments, unanticipated mortality rates),</li>\n<li>details of whom to contact,</li>\n<li>plans for limiting movements of animals, people or vehicles on or off the premises, and</li>\n<li>other measures determined by you and your veterinarian.</li>\n</ul>\n<h2>Operational Management</h2>\n<h3>Properly dispose of deadstock</h3>\n<p>Plan and control the disposal of carcasses according to municipal and provincial regulations. Carcasses should be disposed of in a timely manner.</p>\n<h3>Manage manure according to regulations</h3>\n<p>Plan and control manure management according to municipal and provincial regulations. Planning should include measures for collecting, storing, moving, and disposing of manure in ways that minimize the chance of spreading any disease organisms.</p>\n<h3>Keep the premises, buildings, equipment and vehicles clean</h3>\n<p>Buildings, equipment and vehicles should be cleaned regularly to prevent the introduction of disease and pests. Consider applying disinfectants when practical.</p>\n<h3>Maintain the facilities in a state of good repair</h3>\n<p>Maintain all facilities in a state of good repair so that your biosecurity plan can be effectively implemented.</p>\n<p>This may include:</p>\n<ul>\n<li>buildings and fences to prevent wildlife and people from entering the premises,</li>\n<li>feed storage areas to prevent access by wildlife and vermin, and</li>\n<li>laneways to allow for cleaning and disinfecting vehicles.</li>\n</ul>\n<h3>Obtain production inputs from a reliable source</h3>\n<p>Purchase production inputs such as feed and bedding from reliable sources. Ensure the water supply is free of contamination.</p>\n<h3>Control pests</h3>\n<p>Ensure a pest management program is in place to prevent the spread of disease.</p>\n<h3>Plan and train</h3>\n<p>Have a written biosecurity plan that is updated regularly. Ensure that employees receive proper training and training materials so they can continue to follow the plan.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'application de mesures pr\u00e9ventives pour garder les animaux en sant\u00e9 est une pratique efficace employ\u00e9e depuis longtemps dans les exploitations agricoles canadiennes. Ces mesures forment un plan de bios\u00e9curit\u00e9. Le plan de bios\u00e9curit\u00e9 devrait stipuler la fa\u00e7on dont vous g\u00e9rez l'acc\u00e8s qu'ont les animaux, les v\u00e9hicules et les personnes \u00e0 votre exploitation, ainsi que la sant\u00e9 des animaux et les op\u00e9rations.</p>\n<p>En suivant les lignes directrices ci-dessous et en travaillant en collaboration avec un v\u00e9t\u00e9rinaire, vous pouvez jouer un r\u00f4le important dans la protection de la sant\u00e9 de vos animaux et de la prosp\u00e9rit\u00e9 de votre industrie.</p>\n<h2>Gestion de l'acc\u00e8s</h2>\n<h3>D\u00e9signez des zones distinctes</h3>\n<p>\u00c9tablissez des zones distinctes o\u00f9 diff\u00e9rents niveaux de protection sont requis. D\u00e9limitez ces zones en installant des cl\u00f4tures (ou autres structures) et affichez des panneaux.</p>\n<h3>Contr\u00f4lez les entr\u00e9es dans les zones d\u00e9sign\u00e9es et les d\u00e9placements entre celles-ci</h3>\n<p>Contr\u00f4lez les d\u00e9placements de personnes, d'animaux, de mat\u00e9riel et de v\u00e9hicules :</p>\n<ul>\n<li>vers une zone d\u00e9sign\u00e9e,</li>\n<li>hors d'une zone d\u00e9sign\u00e9e,</li>\n<li>entre les zones d\u00e9sign\u00e9es.</li>\n</ul>\n<p>Cela peut \u00eatre accompli en installant des points d'acc\u00e8s contr\u00f4l\u00e9s.</p>\n<h2>Gestion de la sant\u00e9 des animaux</h2>\n<h3>G\u00e9rez les d\u00e9placements d'animaux</h3>\n<p>Planifiez les arriv\u00e9es, les d\u00e9placements et les sorties d'animaux sur les lieux et hors des lieux. Cela comprend l'utilisation de strat\u00e9gies de gestion telles que les suivantes :</p>\n<ul>\n<li>Identifier tous les animaux et effectuer une tenue de dossiers \u00e0 des fins de tra\u00e7abilit\u00e9.</li>\n<li>Faire subir des \u00e9preuves de d\u00e9pistage aux animaux avant leur introduction.</li>\n<li>Isoler les animaux apr\u00e8s leur arriv\u00e9e.</li>\n<li>Planifier les d\u00e9placements des animaux \u00e0 l'avance.</li>\n<li>Planifiez des p\u00e9riodes o\u00f9 le b\u00e2timent est vide entre les groupes d'animaux.</li>\n</ul>\n<p>Identifiez vos animaux et tenez des dossiers complets. Il est important de participer aux syst\u00e8mes de tra\u00e7abilit\u00e9 dans la mesure du possible.</p>\n<h3>Surveillez vos animaux et rep\u00e9rez les signes de maladie</h3>\n<p>Assurez-vous que les travailleurs sont bien inform\u00e9s et reconnaissent les signes de maladie. Ils devraient pouvoir d\u00e9tecter les signes cliniques chez les animaux en observant leur niveau de production, leur comportement et leur consommation d'aliments et d'eau.</p>\n<h3>\u00c9tablissez un plan d'intervention pour les situations de maladie possibles</h3>\n<p>Consultez un v\u00e9t\u00e9rinaire si vous observez un taux inhabituel de cas de maladie ou de d\u00e9c\u00e8s.</p>\n<p>\u00c9tablissez, avec votre v\u00e9t\u00e9rinaire, un \u00ab plan d'intervention en cas de maladie \u00bb que vous pouvez mettre en oeuvre lorsque vous soup\u00e7onnez la pr\u00e9sence d'une maladie contagieuse ou \u00e0 d\u00e9claration obligatoire. Le plan d'intervention en cas de maladie devrait comprendre les \u00e9l\u00e9ments suivants :</p>\n<ul>\n<li>Les d\u00e9clencheurs de la mise en oeuvre du plan d'intervention (par exemple, le fait que plusieurs animaux pr\u00e9sentent des signes de maladie, une importante baisse de la production, l'absence de r\u00e9action aux traitements de routine, des taux de mortalit\u00e9 impr\u00e9vus).</li>\n<li>Les coordonn\u00e9es des personnes avec qui il faut communiquer.</li>\n<li>Des dispositions pour limiter les d\u00e9placements d'animaux, de personnes et de v\u00e9hicules sur les lieux et hors des lieux.</li>\n<li>D'autres mesures \u00e9tablies par vous ou votre v\u00e9t\u00e9rinaire.</li>\n</ul>\n<h2>Gestion des op\u00e9rations</h2>\n<h3>\u00c9liminez les carcasses comme il se doit</h3>\n<p>Planifiez et contr\u00f4lez l'\u00e9limination des carcasses conform\u00e9ment aux r\u00e8glements municipaux et provinciaux. Les carcasses devraient \u00eatre \u00e9limin\u00e9es rapidement.</p>\n<h3>G\u00e9rez votre fumier conform\u00e9ment aux r\u00e8glements</h3>\n<p>Planifiez et contr\u00f4lez la gestion de votre fumier conform\u00e9ment aux r\u00e8glements municipaux et provinciaux. La planification devrait inclure des mesures pour la collecte, l'entreposage, le d\u00e9placement et l'\u00e9limination du fumier de mani\u00e8re \u00e0 minimiser le risque de propagation d'organismes pathog\u00e8nes.</p>\n<h3>Nettoyez r\u00e9guli\u00e8rement les lieux, les b\u00e2timents, l'\u00e9quipement et les v\u00e9hicules</h3>\n<p>Les b\u00e2timents, l'\u00e9quipement et les v\u00e9hicules devraient \u00eatre nettoy\u00e9s r\u00e9guli\u00e8rement afin d'emp\u00eacher l'introduction de maladies et de ravageurs. Envisagez d'utiliser des disinfectants lorsqu'il est pratique de le faire.</p>\n<h3>Gardez les installations en bon \u00e9tat</h3>\n<p>Veillez \u00e0 ce que toutes les installations soient en bon \u00e9tat pour que votre plan de bios\u00e9curit\u00e9 puisse \u00eatre mis en oeuvre de mani\u00e8re efficace.</p>\n<p>Les installations comprennent :</p>\n<ul>\n<li>Les b\u00e2timents et les cl\u00f4tures qui emp\u00eachent les animaux sauvages et les personnes d'acc\u00e9der aux lieux</li>\n<li>Les aires d'entreposage des aliments pour animaux pour emp\u00eacher les animaux sauvages et ind\u00e9sirables d'y accede</li>\n<li>Des all\u00e9es pour permettre le nettoyage et la d\u00e9sinfection des v\u00e9hicules</li>\n</ul>\n<h3>Obtenez vos intrants de production d'une source fiable</h3>\n<p>Achetez vos intrants de production comme les aliments pour animaux et la liti\u00e8re de sources fiables. Assurez-vous que les sources d'approvisionnement en eau ne sont pas contamin\u00e9es.</p>\n<h3>Contr\u00f4lez les ravageurs</h3>\n<p>Assurez-vous de mettre en oeuvre un programme de lutte contre les ravageurs afin d'emp\u00eacher la propagation de maladies.</p>\n<h3>Planifiez et \u00e9duquez</h3>\n<p>Assurez-vous d'avoir un plan de bios\u00e9curit\u00e9 par \u00e9crit et mettez-le \u00e0 jour r\u00e9guli\u00e8rement. Assurez-vous que les employ\u00e9s re\u00e7oivent la formation et les outils necessaries pour suivre le plan</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}