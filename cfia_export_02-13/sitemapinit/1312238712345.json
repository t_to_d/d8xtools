{
    "dcr_id": "1312236315046",
    "title": {
        "en": "Plant protection policy directives: imports",
        "fr": "Directives sur la protection des <span class=\"nowrap\">v\u00e9g\u00e9taux :</span> importation"
    },
    "html_modified": "2024-02-13 9:46:51 AM",
    "modified": "2016-06-10",
    "issued": "2011-08-01 18:05:16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/protect_directives_import_1312236315046_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/protect_directives_import_1312236315046_fra"
    },
    "ia_id": "1312238712345",
    "parent_ia_id": "1304570628492",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1304570628492",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Plant protection policy directives: imports",
        "fr": "Directives sur la protection des <span class=\"nowrap\">v\u00e9g\u00e9taux :</span> importation"
    },
    "label": {
        "en": "Plant protection policy directives: imports",
        "fr": "Directives sur la protection des <span class=\"nowrap\">v\u00e9g\u00e9taux :</span> importation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1312238712345",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1304570539802",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/directives/imports/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/directives/importation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Plant protection policy directives: imports",
            "fr": "Directives sur la protection des v\u00e9g\u00e9taux : importation"
        },
        "description": {
            "en": "This page lists the plant protection policy directives that establish export requirements that apply to all plant commodities or do not fall under a commodity list.",
            "fr": "Cette page contient une liste des directives sur la protection des v\u00e9g\u00e9taux qui \u00e9tablissent les exigences relatives \u00e0 l?exportation s?appliquant \u00e0 tous les produits v\u00e9g\u00e9taux ou ne figurant pas dans une liste de produits."
        },
        "keywords": {
            "en": "Plant Protection Act, Plant Protection Regulations, exports, policy, directives",
            "fr": "Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, exportation, politique, directives"
        },
        "dcterms.subject": {
            "en": "trees,crops,exports,fruits,grains,horticulture,insects,plant diseases,plants,policy",
            "fr": "arbre,cultures,exportation,fruit,grain,horticulture,insecte,maladie des plantes,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-08-01 18:05:16",
            "fr": "2011-08-01 18:05:16"
        },
        "modified": {
            "en": "2016-06-10",
            "fr": "2016-06-10"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Plant protection policy directives: imports",
        "fr": "Directives sur la protection des v\u00e9g\u00e9taux : importation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>This page lists the plant protection policy directives that establish <strong>import requirements</strong> that apply to all plant commodities or do not fall under a commodity list.</p>\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/date/eng/1312227346910\">All directives (listed by the date originally issued)</a></li>\n</ul>\n<p>Directives are also listed in the following subject index pages:</p>\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/exports/eng/1312235834399\">Export requirements that are applicable to all commodities</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/eng/1312234775640\">Forestry</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/grains-and-field-crops/eng/1312226480445\">Grains and field crops</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/eng/1312230684400\">Horticulture</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/invasive-alien-species-and-domestic-plant-health-p/eng/1465392555227/1465392680410\">Invasive alien species and domestic plant health programs</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/potatoes/eng/1312235432509\">Potatoes</a></li>\n</ul>\n<div class=\"brdr-tp mrgn-bttm-md\"></div>\n<ul><li class=\"doubleLineBullet\"><a href=\"/plant-health/invasive-species/directives/imports/d-12-02/eng/1432586422006/1432586423037\">D-12-02: Import Requirements for Potentially Injurious Organisms (Other than Plants) to Prevent the Importation of Plant Pests in Canada</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/plant-health/invasive-species/directives/imports/d-08-04/eng/1323752901318/1323753560467\">D-08-04: Plant Protection Import Requirements for Plants and Plant Parts for Planting</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/plant-health/invasive-species/directives/imports/d-97-04/eng/1323791055523/1323803716515\">D-<span class=\"wb-inv\"> </span>97-04: Application, procedures, issuance and use of a permit to import under the Plant Protection Act</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/plant-health/invasive-species/directives/imports/d-96-20/eng/1323854223506/1323854308201\">D-<span class=\"wb-inv\"> </span>96-20: Canadian Growing Media Program, Prior Approval Process and Import Requirements for Plants Rooted in Approved Media</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/plant-health/invasive-species/directives/imports/d-96-13/eng/1323855470406/1343344985958\">D-<span class=\"wb-inv\"> </span>96-13: Import Requirements for Plants with Novel Traits, including Transgenic Plants and their Viable Plant Parts</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/plant-health/invasive-species/directives/imports/d-95-26/eng/1322520617862/1322525397975\">D-<span class=\"wb-inv\"> </span>95-26: Phytosanitary Requirements for Soil and Soil-Related Matter, and for Items Contaminated with Soil and Soil-Related Matter</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Cette page contient une liste des directives sur la protection des v\u00e9g\u00e9taux qui \u00e9tablissent les exigences relatives \u00e0 l'importation s'appliquant \u00e0 tous les produits v\u00e9g\u00e9taux ou ne figurant pas dans une liste de produits.</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/fra/1312227346910\">Toutes les directives (selon leur date de premi\u00e8re publication)</a></li>\n</ul>\n<p>Ces directives sont aussi r\u00e9pertori\u00e9es dans les pages d'index par sujet suivantes\u00a0:</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/especes-exotiques-envahissantes-et-programmes-phyt/fra/1465392555227/1465392680410\">Esp\u00e8ces exotiques envahissantes et programmes phytosanitaires nationaux</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/exportation/fra/1312235834399\">Exigences relatives \u00e0 l'exportation qui s'appliquent \u00e0 tous les produits</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/fra/1312234775640\">For\u00eats</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/grains-et-grandes-cultures/fra/1312226480445\">Grains et des grandes cultures</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/fra/1312230684400\">Horticulture</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/pommes-de-terre/fra/1312235432509\">Pommes de terre</a></li>\n</ul>\n<div class=\"brdr-tp mrgn-bttm-md\"></div>\n<ul><li class=\"doubleLineBullet\"><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-12-02/fra/1432586422006/1432586423037\">D-12-02 : Exigences r\u00e9gissant l'importation d'organismes potentiellement nuisibles (autres que les v\u00e9g\u00e9taux) afin d'emp\u00eacher l'importation de phytoravageurs au Canada</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-08-04/fra/1323752901318/1323753560467\">D-08-04 : Exigences phytosanitaire r\u00e9gissant l'importation de v\u00e9g\u00e9taux et de parties de v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-97-04/fra/1323791055523/1323803716515\">D-<span class=\"wb-inv\"> </span>97-04 : Demande, d\u00e9livrance et utilisation du permis d'importation et proc\u00e9dures connexes, en vertu de la Loi sur la protection des v\u00e9g\u00e9taux</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-96-20/fra/1323854223506/1323854308201\">D-<span class=\"wb-inv\"> </span>96-20 : Programme canadien des milieux de culture, processus d'approbation pr\u00e9alable et exigences en mati\u00e8re d'importation de v\u00e9g\u00e9taux enracin\u00e9s dans des milieux de culture approuv\u00e9s</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-96-13/fra/1323855470406/1343344985958\">D-<span class=\"wb-inv\"> </span>96-13 : Exigences phytosanitaires : Permis d'importation de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux, y compris les v\u00e9g\u00e9taux transg\u00e9niques, et de leurs parties viables</a></li>\n<li class=\"doubleLineBullet\"><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-95-26/fra/1322520617862/1322525397975\">D-<span class=\"wb-inv\"> </span>95-26 : Exigences phytosanitaires s'appliquant \u00e0 la terre et aux mati\u00e8res connexes \u00e0 la terre, ainsi qu'aux articles contamin\u00e9s par de la terre et des mati\u00e8res connexes \u00e0 la terre</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}