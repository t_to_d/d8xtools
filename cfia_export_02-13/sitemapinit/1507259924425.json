{
    "dcr_id": "1507259923738",
    "title": {
        "en": "Food export certification",
        "fr": "Obtenir une certification d'exportation d'aliments"
    },
    "html_modified": "2024-02-13 9:52:28 AM",
    "modified": "2023-10-24",
    "issued": "2017-11-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/export_certification_1507259923738_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/export_certification_1507259923738_fra"
    },
    "ia_id": "1507259924425",
    "parent_ia_id": "1323723662195",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food",
        "fr": "Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Relevant to All",
        "fr": "Pertinents pour tous"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1323723662195",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food export certification",
        "fr": "Obtenir une certification d'exportation d'aliments"
    },
    "label": {
        "en": "Food export certification",
        "fr": "Obtenir une certification d'exportation d'aliments"
    },
    "templatetype": "content page 1 column",
    "node_id": "1507259924425",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323723342834",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/certification/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/certification/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food export certification",
            "fr": "Obtenir une certification d'exportation d'aliments"
        },
        "description": {
            "en": "Getting a food export certificate for Canadian products",
            "fr": "Obtenir un certificat d'exportation alimentaire pour les produits canadiens"
        },
        "keywords": {
            "en": "food, products, exports, Canada, Types of Export Certificates, Certification Requirements, Applying for Certificates Online (e-Cert), Paper-based Certification, Applying for Replacement Certificates",
            "fr": "aliments, produits, exportations, Canada, Types de certificats d'exportation, Exigences de certification, Application en ligne aux certificats (e-Cert), Certification en format papier, Demande pour un certificat de remplacement"
        },
        "dcterms.subject": {
            "en": "certification,food,exports,agri-food industry,information,agri-food products",
            "fr": "accr\u00e9ditation,aliment,exportation,industrie agro-alimentaire,information,produit agro-alimentaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-16",
            "fr": "2017-11-16"
        },
        "modified": {
            "en": "2023-10-24",
            "fr": "2023-10-24"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food export certification",
        "fr": "Obtenir une certification d'exportation d'aliments"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\n<!--<div class=\"alert alert-info\">\n-->\n\n<h2 id=\"a\">On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">Introduction</a></li>\n<li><a href=\"#a2\">Types of export certificates</a></li>\n<li><a href=\"#a3\">Certification requirements</a></li>\n<li><a href=\"#a4\">Applying for certificates online (e-Cert)</a></li> \n<li><a href=\"#a5\">Paper-based certification</a></li>\n<li><a href=\"#a6\">Applying for replacement certificates</a></li>\n</ul>\n\n<h2 id=\"a1\">Introduction</h2>\n\n<p>An export certificate is a type of official assurance, providing an importing country with confirmation from the Canadian government\u00a0\u2013 in this case, the Canadian Food Inspection Agency (CFIA)\u00a0\u2013\u00a0that your product or commodity meets certain standards and requirements.</p>\n\n<p>Not all countries or all products require them.</p>\n\n<p>The CFIA issues export certificates only for products still in Canada. Products no longer in Canada are <strong>not</strong> eligible for certification because the CFIA is unable to attest to the compliance of the food to any requirements.</p>\n\n<p>If required, the export certificate accompanies a consignment to its destination country, confirming the consignment's compliance with specific standards and requirements. An approved export certificate is provided to the appropriate border agency of the destination country\u00a0\u2013\u00a0in either electronic or paper form\u00a0\u2013\u00a0to help clear your product into that country.</p>\n\n<p>Certification is almost exclusively done for products exported for commercial purposes. However, certain countries require an export certificate for product destined for personal consumption, for example, fish caught while sport fishing in Canada. For more information on this please see the <a href=\"/exporting-food-plants-or-animals/food-exports/food-specific-export-requirements/eng/1503941030653/1503941059640\">Food-specific requirements</a>.</p>\n\n<h2 id=\"a2\">Types of export certificates</h2>\n\n<h3>Health or sanitary certificates (negotiated)</h3>\n\n<p>Health or sanitary certificates are government to government certificates that include information about the product, its health status and the consignor.</p>\n\n<p>The information on an export certificate varies, depending on the product or commodity and the destination country, but it may include:</p>\n\n<ul class=\"list-unstyled mrgn-lft-md lst-spcd\">\n\n<li>the country of origin of the product and its ingredients;</li>\n\n<li>treatment or other processes the product has undergone, prior to export;</li>\n\n<li>the microbiological status of the product; and/or,</li>\n\n<li>the product's health status\u00a0\u2013\u00a0for example, whether or not a certain animal or plant disease is present in Canada.</li>\n</ul>\n\n<h3>Inspection certificates (Canadian)</h3>\n\n<p>Inspection certificates are developed by Canada to attest that the product complies with Canadian standards, for example: certificates of origin and hygiene, product grading certificates, standard meat product certificates, etc. Consignments for countries or products with no known requirements may be issued inspection certificates, but these consignments are at commercial risk. The importer in the destination country can tell you if an inspection certificate is required.</p>\n\n\n<h4 id=\"cr1\">Note</h4>\n\n\n<p>Commercial risk means acceptance by the exporter that the CFIA certificate is given in good faith based on the exporter's written assurances that all due enquiries have been made and that there is no known impediment to entry of the product into the country concerned. These products must meet all Canadian requirements.</p> \n\n\n<h3>Certificates of free sale</h3>\n\n<p>A <a href=\"/exporting-food-plants-or-animals/food-exports/certification/certificate-of-free-sale/eng/1507732648572/1507732649258\">certificate of free sale</a> attests that, in accordance with the <a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i> (SFCA)</a> and <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i> (SFCR)</a>, the products are required to:</p>\n\n<ul>\n<li>originate from a manufacturer licensed to produce food for sale in Canada and/or for export</li>\n<li>have been produced by a manufacturer in good regulatory standing with a food safety control plan and traceability system in place</li>\n<li>and, be safe for human consumption</li>\n</ul>\n\n<p>The importer in the destination country is able to tell you if a certificate of free sale is required.</p>\n\n<h3>Radiation certificates</h3>\n\n<p>The radiation certificate (Certificate of Inspection Radiation Content) is a government to government certificate attesting to the safe level of radiation in the exported food.</p>\n\n<p>As of <span class=\"nowrap\">May 1, 2019</span> the CFIA will be the only authority in Canada to issue radiation certificates for food exports. The CFIA will issue radiation certificates when required by the importing country.</p>\n\n<p>The Certificate of Inspection Radiation Content will also attest that food commodities produced in Canada:</p>\n\n<ul>\n<li>show levels of radiation at or near normal background levels which are far below those considered harmful by national and international standards</li>\n<li>may be sold freely without restriction in Canada and are considered to meet standards for levels of radiation of any country importing Canadian products</li>\n</ul>\n\n<p>It is the responsibility of the Canadian exporter to find out from the destination country if a radiation certificate is required.</p>\n\n<h2 id=\"a3\">Certification requirements</h2>\n\n<p>You are responsible to identify and implement the Canadian regulatory requirements and the foreign country import requirements that apply to your product. The CFIA supports industry by publishing known and negotiated requirements in the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/eng/1507329098491/1507329098850\">Export requirements library</a>.</p>\n\n<h3>Reporting export requirements to CFIA</h3>\n\n<p>If you become aware of new or changed export requirements for any countries, you are encouraged to inform your local inspection office (see <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Area and Regional Offices</a>) so that the CFIA can verify the requirements and determine if the Canadian industry is able to meet those requirements and enter into negotiations if required.</p>\n\n<h3>Eligibility</h3>\n\n<p>Export certifications will be issued to you if you meet the <a href=\"/exporting-food-plants-or-animals/food-exports/general-requirements/eng/1503668386853/1503668443414\">general requirements for export</a>, and you are on any necessary export eligibility lists (see <a href=\"/exporting-food-plants-or-animals/food-exports/registers-and-lists/eng/1507261903791/1507261904290\">Export registers and lists</a>). Exporters must be able to demonstrate that they have control measures in place to ensure the foods they export meet all applicable Canadian and importing country requirements, and that their procedures result in accurate export documents.</p>\n\n<p>When requesting an export  certificate or other export permission, exporters must provide the inspector with accurate and complete information and supporting documents, if required. Exporters must maintain documentation on file to support the information that is entered on the export certificate request form and the export certificate. At any time, CFIA inspectors may request the exporter's documents in order to verify that the information submitted is factual, accurate, and complete. </p>\n\n<p>Examples of supporting documents:</p>\n\n<ul>\n<li>pictures or copies of the consignment product labels showing the establishment numbers, lot codes, product name</li>\n<li>pictures of the container or seal</li>\n<li>laboratory test results, if applicable</li>\n</ul>\n\n<h2 id=\"a4\">Applying for certificates online (e-Cert)</h2>\n\n<p>Many export certificates are processed through the CFIA's electronic certification; you can access this system through <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\">My CFIA</a>. Exporters, verifiers, CFIA staff and others in the export chain use this online system to ensure that products are eligible for certification and to issue certificates. For a list of export certificates available  through My CFIA can be found on the <a href=\"/about-the-cfia/my-cfia-account/online-services/eng/1482206358930/1482206359330\">My  CFIA Online Services</a> page.</p>\n\n<p>When you apply for certificates electronically, the system will identify if you are eligible for certificates for certain products and countries, and what notification of shipments, inspections, fees and additional supporting documents are required.</p>\n\n<p>Fees and charges can apply for requesting export certificates and any related inspection activities.</p>\n\n<h2 id=\"a5\">Paper-based certification</h2>\n\n<p>If you do not wish to apply online, please contact your local inspection office (see <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Area and Regional Offices</a>) for assistance.</p>\n\n<p>In addition, some certificates are still issued through paper-based systems. If you cannot find the certificate for a product and country requirement in e-Cert, please contact your local inspection office.</p>\n\n<h2 id=\"a6\">Applying for replacement certificates</h2>\n\n<p>The CFIA may issue a <a href=\"/exporting-food-plants-or-animals/food-exports/certification/replacement-certificates/eng/1507734013223/1507734013691\">replacement certificate</a> for food exports, depending on the circumstances. Most commonly, administrative errors, a change of consignee, or a lost or damaged certificate are the reasons replacement certificates are requested.</p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<!--<div class=\"alert alert-info\">\n-->\n\n\n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Introduction</a></li>\n\n<li><a href=\"#a2\">Types de certificats d'exportation</a></li>\n\n<li><a href=\"#a3\">Exigences de certification</a></li> \n<li><a href=\"#a4\">Application en ligne aux certificats (e-Cert)</a></li>\n\n<li><a href=\"#a5\">Certification en format papier</a></li>\n\n<li><a href=\"#a6\">Demande pour un certificat de remplacement</a></li>\n</ul>\n\n<h2 id=\"a1\">Introduction</h2>\n\n<p>Un certificat d'exportation est un genre d'assurance officielle, fournissant au pays importateur la confirmation du gouvernement canadien, dans ce cas, l'Agence canadienne d'inspection des aliments (ACIA), que votre produit ou marchandise respecte certaines normes et exigences.</p>\n\n<p>Ce ne sont pas tous les pays ni tous les produits qui les exigent.</p>\n\n<p>L'ACIA \u00e9met des certificats d'exportation seulement pour les produits encore au Canada. Les produits qui ne sont plus au Canada ne sont <strong>pas</strong> admissibles parce que l'ACIA est incapable de t\u00e9moigner de la conformit\u00e9 des aliments aux exigences.</p>\n\n<p>Si n\u00e9cessaire, le certificat d'exportation accompagne un envoi jusqu'au pays destinataire, confirmant la conformit\u00e9 de la consignation avec les normes et les exigences particuli\u00e8res. Un certificat d'exportation approuv\u00e9 est fourni \u00e0 l'agent des services frontaliers appropri\u00e9s du pays destinataire, sous forme \u00e9lectronique ou papier, afin d'aider votre produit \u00e0 entrer dans ce pays.</p>\n\n<p>La certification est presque exclusivement effectu\u00e9e pour les produits export\u00e9s \u00e0 des fins commerciales. Toutefois, certains pays demandent un certificat d'exportation pour les produits destin\u00e9s \u00e0 une consommation personnelle, par exemple, un poisson attrap\u00e9 au cours d'une p\u00eache sportive au Canada. Pour plus de enseignements \u00e0 ce sujet, veuillez voir les <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences-d-exportation-particulieres-aux-produits/fra/1503941030653/1503941059640\">Exigences d'exportation particuli\u00e8res aux produits alimentaires</a>.</p>\n\n<h2 id=\"a2\">Types de certificats d'exportation</h2>\n\n<h3>Certificats de sant\u00e9 ou de salubrit\u00e9 (n\u00e9goci\u00e9s)</h3>\n\n<p>Les certificats de sant\u00e9 ou de salubrit\u00e9 sont des certificats de gouvernement \u00e0 gouvernement qui comprennent les renseignements sur le produit, son statut de sant\u00e9 et l'exp\u00e9diteur.</p>\n\n<p>Les renseignements sur un certificat varient, en fonction du produit ou de la marchandise et le pays destinataire, mais il peut comprendre :</p>\n<ul class=\"list-unstyled mrgn-lft-md lst-spcd\">\n<li>le pays d'origine du produit et ses ingr\u00e9dients;</li>\n\n<li>le traitement ou autre processus que le produit a subi, avant l'exportation;</li>\n\n<li>le statut microbiologique du produit;</li>\n\n<li>le statut de sant\u00e9 du produit\u00a0\u2013\u00a0par exemple, si oui ou non une certaine maladie animale ou v\u00e9g\u00e9tale est pr\u00e9sente au Canada.</li>\n</ul>\n\n<h3>Certificats d'inspection (canadiens)</h3>\n\n<p>Les certificats d'inspection sont \u00e9labor\u00e9s par le Canada pour attester que le produit est conforme aux normes canadiennes, par exemple: certificats d'origine et d'hygi\u00e8ne, certificats de classement des produits, certificats de produits de viande standard, etc. Les envois destin\u00e9s \u00e0 des pays ou \u00e0 des produits pour lesquels aucune exigence n'est connue peuvent recevoir un certificat d'inspection, mais ces envois courent un risque commercial. L'importateur dans le pays de destination peut vous indiquer si un certificat d'inspection est requis.</p>\n\n<h4 id=\"cr1\">Remarque</h4>\n\n<p>Le risque commercial signifie l'acceptation par l'exportateur que le certificat de l'ACIA a \u00e9t\u00e9 donn\u00e9 de bonne foi en fonction des assurances \u00e9crites de l'exportateur que toutes les demandes dues ont \u00e9t\u00e9 faites et qu'il n'y a aucun obstacle connu pour l'entr\u00e9e du produit dans le pays concern\u00e9. Ces produits doivent respecter les exigences canadiennes.</p> \n\n<h3>Certificats de vente libre</h3>\n\n<p>Un <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/certification/certificat-de-vente-libre/fra/1507732648572/1507732649258\">certificat de vente libre</a> atteste que, conform\u00e9ment \u00e0 la <a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i> (LSAC)</a> et au <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC)</a>, les produits doivent:</p>\n\n<ul>\n<li>provenir d'un fabricant autoris\u00e9 \u00e0 produire des aliments destin\u00e9s \u00e0 la vente au Canada et/ou \u00e0 l'exportation;</li>\n\n<li>ont \u00e9t\u00e9 produits par un fabricant sous licence ayant un bon statut r\u00e9glementaire ainsi qu 'un plan de contr\u00f4le de la salubrit\u00e9 des aliments et un syst\u00e8me de tra\u00e7abilit\u00e9 en place;</li>\n\n<li>et, \u00eatre propre \u00e0 la consommation humaine.</li>\n</ul>\n\n<p>L'importateur du pays destinataire peut vous dire si un certificat de vente libre est requis.</p>\n\n<h3>Certificat de radiation</h3>\n\n<p>Le certificat de radiation (Certificat d'inspection niveau de radioactivit\u00e9) est un certificat de gouvernement \u00e0 gouvernement attestant du niveau de radiation sans danger dans les aliments export\u00e9s.</p>\n\t\n<p>\u00c0 compter du <span class=\"nowrap\">1\u00a0mai\u00a02019</span>, l'ACIA sera la seule autorit\u00e9 au Canada \u00e0 d\u00e9livrer des certificats de radiation pour les exportations d'aliments. L'ACIA \u00e9mettra des certificats de radiation lorsque requis par le pays importateur.</p>\n\n<p>Le Certificat d'inspection niveau de radioactivit\u00e9 attestera \u00e9galement que les produits alimentaires fabriqu\u00e9s au Canada\u00a0:</p>\n\n<ul>\n<li>montrent des niveaux de rayonnement \u00e9gaux ou proches des niveaux de fond normaux qui sont bien inf\u00e9rieurs \u00e0 ceux consid\u00e9r\u00e9s nocifs par les normes nationales et internationales</li>\n<li>peuvent \u00eatre vendus librement sans restriction au Canada et sont consid\u00e9r\u00e9s comme conformes aux normes relatives aux niveaux de rayonnement de tout pays important des produits canadiens</li>\n</ul>\n\n<p>Il incombe \u00e0 l'exportateur canadien de s'informer aupr\u00e8s du pays de destination si un certificat de radiation est requis.</p>\n\n\n<h2 id=\"a3\">Exigences de certification</h2>\n\n<p>Vous \u00eates responsable d'identifier et de mettre en place les exigences r\u00e9glementaires Canadiennes et les exigences d'importation des pays \u00e9trangers qui s'applique \u00e0 votre produit. L'ACIA appuie l'industrie en publiant les exigences connues et n\u00e9goci\u00e9es dans la <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/fra/1507329098491/1507329098850\">Biblioth\u00e8que des exigences en mati\u00e8re d'exportation</a>.</p>\n\n<h3>Signalement des exigences d'exportation \u00e0 l'ACIA</h3>\n\n<p>Si vous constatez l'existence de nouvelles exigences d'exportation ou des changements aux exigences actuels \u00e0 l'\u00e9gard de tous les pays, vous \u00eates encourager d'aviser votre bureau d'inspection local (voir les <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">Centres op\u00e9rationnels et bureaux r\u00e9gionaux</a>) de sorte que l'ACIA puisse v\u00e9rifier les exigences et voir \u00e0 ce que l'industrie canadienne est apte \u00e0 les respecter et entamer des n\u00e9gociations si n\u00e9cessaire.</p>\n\n<h3>Admissibilit\u00e9</h3>\n\n<p>Un certificat d'exportation vous sera d\u00e9livr\u00e9 si vous r\u00e9pondez aux <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences-generales/fra/1503668386853/1503668443414\">exigences g\u00e9n\u00e9rales d'exportation</a> et que vous \u00eates inscrits sur les listes d'admissibilit\u00e9 \u00e0 l'exportation n\u00e9cessaires (voir les <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/registres-et-listes/fra/1507261903791/1507261904290\">Registres et listes d'exportation</a>). Les exportateurs doivent \u00eatre en mesure de d\u00e9montrer qu'ils ont mis en place des mesures de contr\u00f4le pour garantir que les aliments qu'ils exportent r\u00e9pondent \u00e0 toutes les exigences applicables du Canada et des pays importateurs, et que leurs proc\u00e9dures aboutissent \u00e0 des documents d'exportation pr\u00e9cis.</p>\n\n<p>Lorsque vous demandez un certificat d'exportation ou une autre autorisation d'exportation, les exportateurs doivent fournir \u00e0 l'inspecteur des informations exactes et compl\u00e8tes et des pi\u00e8ces justificatives, si n\u00e9cessaire. Les exportateurs doivent conserver dans leurs dossiers des documents pour supporter les informations saisies sur le formulaire de demande de certificat d'exportation et sur le certificat d'exportation. \u00c0 tout moment, les inspecteurs de l'ACIA peuvent demander les documents de l'exportateur afin de v\u00e9rifier que les informations soumises sur le formulaire de demande de certificat d'exportation sont factuelles, exactes et compl\u00e8tes.</p>\n\n<p>Exemples de documents :</p>\n\n<ul>\n<li>Photos ou copies de l'\u00e9tiquette du produit dans l'envoie qui d\u00e9montre le num\u00e9ro de l'\u00e9tablissement, le code de production, le nom du produit, etc.</li>\n<li>Photos du conteneur ou du sell\u00e9  </li>\n<li>R\u00e9sultats de laboratoire, si applicable </li>\n</ul>\n\n<h2 id=\"a4\">Application en ligne aux certificats (e-Cert)</h2>\n\n<p>Bon nombre de certificats d'exportation sont trait\u00e9s par l'entremise de la certification \u00e9lectronique de l'ACIA, ou le syst\u00e8me e-Cert; vous pouvez acc\u00e9der \u00e0 ce syst\u00e8me via <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\">Mon ACIA</a>. Les exportateurs, les v\u00e9rificateurs, le personnel de l'ACIA et tout autre intervenant dans la cha\u00eene d'exportation utilisent le syst\u00e8me en ligne pour assurer que les produits soient admissibles \u00e0 la certification et \u00e0 la d\u00e9livrance de certificats. Pour une liste de certificats d'exportation  offerts sur Mon ACIA se trouve sur la page des <a href=\"/a-propos-de-l-acia/compte-mon-acia/services-en-ligne/fra/1482206358930/1482206359330\">services en ligne de l'ACIA</a>.</p>\n\n<p>Lorsque vous demandez des certificats par voie \u00e9lectronique, le syst\u00e8me d\u00e9terminera si vous \u00eates admissible aux certificats en ce qui concerne certains produits et pays, et d\u00e9terminera quels avis seront requis relativement aux exp\u00e9ditions, aux inspections, \u00e0 la tarification et aux documents suppl\u00e9mentaires.</p>\n\n<p>Les droits et les frais peuvent s'appliquer \u00e0 la demande de certificats d'exportation et de toutes activit\u00e9s d'inspection connexes.</p>\n\n<h2 id=\"a5\">Certification en format papier</h2>\n\n<p>Si vous ne souhaitez pas faire une demande en ligne, veuillez communiquer avec votre bureau d'inspection local (voir les <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">Centres op\u00e9rationnels et bureaux r\u00e9gionaux</a>) pour obtenir de l'aide.</p>\n\n<p>De plus, certains certificats sont toujours d\u00e9livr\u00e9s au moyen des syst\u00e8mes en format papier. Si vous ne trouvez pas le certificat concernant l'exigence pour un produit ou un pays donn\u00e9 dans le syst\u00e8me e-Cert, veuillez communiquer avec votre bureau d'inspection local.</p>\n\n<h2 id=\"a6\">Demande pour un certificat de remplacement</h2>\n\n<p>L'ACIA peut \u00e9mettre un <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/certification/certificats-de-remplacement/fra/1507734013223/1507734013691\">certificat de remplacement</a> pour les exportations d'aliments, selon les circonstances. Le plus fr\u00e9quemment, les erreurs administratives, un changement d'exp\u00e9diteur, ou un certificat perdu ou endommag\u00e9 constituent les raisons pour lesquelles le remplacement de certificats est demand\u00e9.</p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}