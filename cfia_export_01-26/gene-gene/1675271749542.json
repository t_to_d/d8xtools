{
    "dcr_id": "1675271749542",
    "title": {
        "en": "Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds",
        "fr": "Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs indirects dans les aliments pour animaux dans la production et la fabrication d'aliments pour animaux de ferme"
    },
    "html_modified": "2024-01-26 2:26:06 PM",
    "modified": "2023-11-14",
    "issued": "2023-02-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/safe_use_ofIncidental_feed_additives_1675271749542_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/safe_use_ofIncidental_feed_additives_1675271749542_fra"
    },
    "ia_id": "1675271861098",
    "parent_ia_id": "1387308992607",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1387308992607",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds",
        "fr": "Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs indirects dans les aliments pour animaux dans la production et la fabrication d'aliments pour animaux de ferme"
    },
    "label": {
        "en": "Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds",
        "fr": "Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs indirects dans les aliments pour animaux dans la production et la fabrication d'aliments pour animaux de ferme"
    },
    "templatetype": "content page 1 column",
    "node_id": "1675271861098",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1387308991466",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/inspection-program/safe-use-of-incidental-feed-additives/",
        "fr": "/sante-des-animaux/aliments-du-betail/inspection-des-aliments-du-betail/utilisation-sure-des-additifs-indirects-dans-les-a/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds",
            "fr": "Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs indirects dans les aliments pour animaux dans la production et la fabrication d'aliments pour animaux de ferme"
        },
        "description": {
            "en": "Incidental feed additives (IFAs) are substances used in feed manufacturing processes that have no nutritional or technical function in livestock feeds but, given their use, may become residues in feeds.",
            "fr": "Les additifs indirects dans les aliments pour animaux (AIA) sont des substances utilis\u00e9es dans les processus de fabrication des aliments pour animaux de ferme qui n'ont aucune fonction nutritionnelle ou technique, mais qui, compte tenu de leur utilisation, peuvent se retrouver dans ces aliments sous forme de r\u00e9sidus."
        },
        "keywords": {
            "en": "Industry Guidance, Policy Clarification, Inspector Guidance, Inspection Protocols, Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds",
            "fr": "aliments du b\u00e9tail, Lignes directrices pour l'industrie, Pr\u00e9cision des politiques, Conseils aux inspecteurs, Protocoles d'inspection, Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs indirects dans les aliments pour animaux dans la production et la fabrication d'aliments pour animaux de ferme"
        },
        "dcterms.subject": {
            "en": "animal health,food,government information,inspection,quality control,regulation,standards",
            "fr": "sant\u00e9 animale,aliment,information gouvernementale,inspection,contr\u00f4le de la qualit\u00e9,r\u00e9glementation,norme"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "modified": {
            "en": "2023-11-14",
            "fr": "2023-11-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds",
        "fr": "Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs indirects dans les aliments pour animaux dans la production et la fabrication d'aliments pour animaux de ferme"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Incidental feed additives (IFAs) are substances used in feed manufacturing processes that have no nutritional or technical function in livestock feeds but, given their use, may become residues in feeds. Residues of IFAs may be present in a variety of single ingredient feeds (SIFs) or mixed feeds given their use in livestock feed manufacturing. Some examples include:</p>\n\n<ul>\n<li>boiler chemicals, where the steam comes in contact with livestock feeds, including steam used in pelleting</li>\n\n<li>processing additives used in the production of SIFs<br>\n</li>\n\n<li>water treatment chemicals used in rendering, feed manufacturing, and food processing plants to recover solids, where the recovered solids are then used as raw materials to produce SIFs (for example, where recovered solids are added back to inedible rendering from which meat and bone meal are produced)</li>\n\n<li>denaturants and dyes used for marking inedible and prohibited materials in meat processing and rendering</li>\n</ul>\n\n<p>Feed manufacturers are responsible for ensuring that substances used in their establishments are safe and suitable for their intended use. This is an essential part of good manufacturing practices to prevent the contamination or adulteration of livestock feeds from these materials. Feed manufacturers are also required to meet all applicable regulatory requirements of the <i>Feeds Act</i> and regulations and <i>Health of Animals Act</i> and regulations.</p>\n\n<p>Documentation pertaining to IFAs must be accessible and up to date for Canadian Food Inspection Agency (CFIA) inspectors to verify that the materials being used are compliant with all applicable regulatory requirements. Where there are reasonable doubts about the safety of any IFA products used, CFIA inspectors may request additional supporting documents with which to conduct a more in-depth review to verify that the IFA products meet all applicable regulatory requirements. If the safety assessment indicates potential feed safety issues, the CFIA will take appropriate compliance actions. Compliance actions taken will be on a case-by-case basis and will depend on the composition of the product(s) identified.</p>\n\n<p>This guidance provides feed manufacturers with recommended options to demonstrate the safe use of IFAs below:</p>\n\n<ol>\n<li><strong>Some non-food chemical products previously included in the <a href=\"https://food-nutrition.canada.ca/food-safety/referencelist/index-en.php\">Reference Listing of accepted construction materials, packaging materials and non-food chemical products and those that have a no objection letter (NOL) from Health Canada</a></strong>\n      \n<p>The Reference Listing of Accepted Construction Materials, Packaging Materials and Non-food Chemical Products (otherwise known as the reference listing) is a list of items which were previously found by the CFIA to be acceptable for a specific use in food establishments.</p>\n\n<p>The CFIA no longer pre-approves such materials and products. The reference listing is now available as a reference only. The last update to the listing was made in January 2016. Further, Health Canada's Bureau of Chemical Safety (BCS), on a voluntary basis, assess the safety of incidental additives that may come in direct contact with food in food processing establishments as per their <strong>Guidelines for Incidental Additive Submissions</strong>. Letters expressing acceptable BCS opinions are called \"No Objection Letters\" (NOLs).</p>\n\t  \n<p>The Animal Feed Program (AFP) of the CFIA does not object to the non-food chemical products on the reference listing or, with a Health Canada NOL in the following categories being used for their intended purposes in the production of livestock feeds:</p>\n\t  \n<ul>\n<li>category: boiler water treatment compounds (not for food process water)\n<ul>\n<li>Sub-category: boiler water treatment compounds where the treated water or the steam produced may come in contact with food products (w1)</li>\n</ul></li>\n<li>category: decharacterizing agents\n<ul>\n<li>Sub-category: red meat (i1)</li>\n<li>Sub-category: white meat (i1)</li>\n</ul></li>\n<li>category: denaturing agents\n<ul>\n<li>Sub-category: red meat (i1)</li>\n<li>Sub-category: white meat (i1)</li>\n</ul></li>\n</ul>\n\t  \n<p class=\"mrgn-tp-md\">As the reference listing is no longer maintained, it is recommended that feed manufacturers verify the accuracy of the following information with the manufacturer(s) of the product(s).</p>\n\t  \n<ul>\n<li>the product continues to be used for the intended and previously accepted use(s) (formulation, label directions, etc.)</li>\n<li class=\"mrgn-bttm-lg\">the manufacturer of the product continues to support the product for this use</li>\n</ul>\n</li>\n<li><strong>Products registered by the CFIA Animal Feed Program (AFP)</strong>\n      \n<p>Previously, the CFIA AFP registered some products now classified as IFAs for use in various processes including:</p>\n\t  \n<ul>\n<li>water treatment chemicals like flocculants, coagulants, and anti-foams used in rendering and food processing plants to recover solids, when the recovered solids are then used as raw materials to produce SIFs (for example, recovered solids are added back to inedible rendering from which meat and bone meal are produced)</li>\n</ul>\n\t  \n<p class=\"mrgn-tp-md\">The registrations of these products will lapse over time and will be transitioned by the AFP to a Letter Of No Objection (see item 3 below).\u00a0 Once transitioned, registration numbers should no longer appear on labels but may take some time to fully disappear from the marketplace. It has been program policy to allow product to move through the marketplace carrying the previous label for a period of 6 months. Manufacturers and sellers of livestock feeds will be given a 6 month period to use up old labels containing the registration numbers. After that time period, product offered for sale with labels containing the expired registration number would be considered as non-compliant and require relabeling.</p>\n\t  \n</li>\n<li><strong>Letter Of No-Objection (LONO)</strong>\n\t  \n<p>As per RG-1 Chapter\u00a03 - <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-3/eng/1617909452465/1617909586070?chap=30\">Guidelines for Incidental Feed Additive (IFA) Data Review Applications</a>, the CFIA the AFP evaluates, on a case-by-case basis, the acceptability of various IFA products voluntarily submitted by manufacturers wishing to supply their products to feed manufacturers. Letters expressing favorable opinions are called \"Letter Of No-Objection (LONO)\".\u00a0 The product manufacturers may then present these directly to prospective feed manufacturer customers to be included in a preventive control plan (PCP) or, if needed, provide them in support of a Letter of Guarantee. In the case where new safety information comes available that suggests that the IFA product is no longer safe for its intended use, the CFIA reserves the right to retract a previously issued LONO. It is the responsibility of industry to ensure that all LONOs on file at their establishment are current, and still valid for all applicable IFA products.</p>\n\t  \n</li>\n<li><strong>Supplier or manufacturer Letter of Guarantee (LOG)</strong>\n<p>A Letter of Guarantee (LOG) is a document from the supplier or manufacturer of an IFA indicating that the product is safe, suitable for use and meets the specifications of the customer. The letter should contain the following information:</p>\n\t  \n<ul>\n<li>name of the supplier</li>\n<li>date of issuance</li>\n<li>description of the product</li>\n<li>brand name or code number</li>\n<li>a statement that the IFA product is safe and suitable for the intended use</li>\n<li>a reference to the related LONO (if applicable)</li>\n<li>signature of the supplier</li>\n</ul>\n\n<p class=\"mrgn-tp-md\">It is important to retain a LOG for any product that is not covered by items\u00a01\u00a0to\u00a03\u00a0above.</p>\n\t  \n<p>Since a LONO and LOG can be retracted or changed at any time, it is up to industry to ensure that the status of the letters is verified at an acceptable frequency. Where a feed establishment has preventive control plan in place, it is important to ensure that LOGs and LONOs are included in these plans.</p>\n\t  \n</li>\n<li><strong>SIFs described in Schedule IV or V of the <i>Feeds Regulations</i> included in IFAs</strong>\n\t  \n<p>SIFs listed in part I of Schedule IV or V of the <i>Feeds Regulations</i> and registered sources of part II SIFs may be used as components of an IFA product for an IFA purpose.\u00a0If all the components of such an IFA are listed in part I of Schedule IV or V or are registered part II SIFs, the risk posed by the use of these products for an IFA purpose is considered negligible. IFAs containing unregistered SIFs listed in part II of Schedule IV or V or other formulating ingredients not listed in Schedule IV or V should demonstrate their acceptability by means of items\u00a01\u00a0to\u00a04\u00a0above.</p>\n\t  \n</li>\n</ol>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les additifs indirects dans les aliments pour animaux (AIA) sont des substances utilis\u00e9es dans les processus de fabrication des aliments pour animaux de ferme qui n'ont aucune fonction nutritionnelle ou technique, mais qui, compte tenu de leur utilisation, peuvent se retrouver dans ces aliments sous forme de r\u00e9sidus. Les r\u00e9sidus d'additifs indirects dans les aliments pour animaux peuvent \u00eatre pr\u00e9sents dans une vari\u00e9t\u00e9 d'aliments \u00e0 ingr\u00e9dient unique\u00a0(AIU) ou d'aliments m\u00e9lang\u00e9s \u00e9tant donn\u00e9 leur utilisation dans la fabrication d'aliments pour animaux de ferme. En voici quelques exemples\u00a0:</p>\n\n<ul>\n<li>les produits chimiques pour chaudi\u00e8res o\u00f9 la vapeur entre en contact avec des aliments pour animaux de ferme, y compris la vapeur utilis\u00e9e dans le processus de cubage.</li>\n<li>les additifs de transformation utilis\u00e9s dans la production d'AIU. Par exemple, les agents antimicrobiens, les enzymes, les levures et les produits chimiques employ\u00e9s dans la production industrielle d'\u00e9thanol o\u00f9 les sous-produits du processus de production sont ensuite utilis\u00e9s comme AIU (comme les dr\u00eaches de distillerie).</li>\n<li>les produits chimiques de traitement de l'eau utilis\u00e9s dans les \u00e9tablissements d'\u00e9quarrissage, de fabrication d'aliments du b\u00e9tail et de transformation des produits alimentaires pour r\u00e9cup\u00e9rer les solides, lorsque les solides r\u00e9cup\u00e9r\u00e9s sont ensuite utilis\u00e9s comme mati\u00e8res premi\u00e8res afin de produire des AIU (par exemple, lorsque les solides r\u00e9cup\u00e9r\u00e9s sont r\u00e9introduits dans les produits d'\u00e9quarrissage non comestibles utilis\u00e9s dans la fabrication de la farine de viande et d'os).</li>\n<li>les d\u00e9naturants et colorants utilis\u00e9s pour marquer les mati\u00e8res non comestibles et substances interdites dans le traitement et l'\u00e9quarrissage de la viande.</li>\n</ul>\n\n<p>Les fabricants d'aliments du b\u00e9tail doivent s'assurer que les substances utilis\u00e9es dans leurs installations sont s\u00fbres et adapt\u00e9es \u00e0 l'usage auquel elles sont destin\u00e9es. Il s'agit d'un \u00e9l\u00e9ment essentiel des bonnes pratiques de fabrication pour \u00e9viter la contamination ou la falsification des aliments\u00a0 par ces substances. Les fabricants d'aliments du b\u00e9tail sont \u00e9galement tenus de respecter toutes les exigences r\u00e9glementaires applicables de la <i>Loi sur les aliments du b\u00e9tail</i>, de la <i>Loi sur la sant\u00e9 des animaux</i> et de leurs r\u00e8glements.</p>\n\n<p>La documentation relative aux AIA doit \u00eatre accessible et \u00e0 jour afin que les inspecteurs de l'Agence canadienne d'inspection des aliments (ACIA) puissent v\u00e9rifier que les mati\u00e8res utilis\u00e9es sont conformes \u00e0 toutes les exigences r\u00e9glementaires applicables. S'il existe des doutes raisonnables quant \u00e0 l'innocuit\u00e9 d'un AIA utilis\u00e9, les inspecteurs de l'ACIA peuvent demander des pi\u00e8ces justificatives suppl\u00e9mentaires afin de proc\u00e9der \u00e0 un examen plus approfondi et de s'assurer que les AIA r\u00e9pondent \u00e0 toutes les exigences r\u00e9glementaires applicables. Si l'\u00e9valuation de l'innocuit\u00e9 devait r\u00e9v\u00e9ler des probl\u00e8mes potentiels de salubrit\u00e9 en mati\u00e8re d'aliments pour animaux de ferme, l'ACIA prendra les mesures de conformit\u00e9 appropri\u00e9es. Les mesures de conformit\u00e9 mises en \u0153uvre se feront au cas par cas et d\u00e9pendront de la composition du ou des produits d\u00e9tect\u00e9s.</p>\n<p>Cette directive fournit aux fabricants d'aliments du b\u00e9tail les options recommand\u00e9es pour d\u00e9montrer que les AIA ci-dessous sont utilis\u00e9s en toute s\u00e9curit\u00e9.</p>\n\n<ol>\n<li><strong>Certains produits chimiques non alimentaires pr\u00e9c\u00e9demment inclus dans la <a href=\"https://food-nutrition.canada.ca/food-safety/referencelist/index-fr.php\">Liste de r\u00e9f\u00e9rence pour les mat\u00e9riaux de construction, les mat\u00e9riaux d'emballage, et les produits chimiques non alimentaires accept\u00e9s  et ceux qui ont une lettre de non-objection (LNO) de Sant\u00e9 Canada</a></strong>\n\t \n<p>La Liste de r\u00e9f\u00e9rence pour les mat\u00e9riaux de construction, les mat\u00e9riaux d'emballage, et les produits chimiques non alimentaires accept\u00e9s (\u00e9galement connue sous le nom de \u00ab\u00a0liste de r\u00e9f\u00e9rence\u00a0\u00bb) est une liste d'\u00e9l\u00e9ments dont l'utilisation sp\u00e9cifique dans les \u00e9tablissements alimentaires est consid\u00e9r\u00e9e comme acceptable par l'ACIA.</p>\n\t \n<p>L'ACIA n'approuve plus au pr\u00e9alable ces mat\u00e9riaux et produits. La liste de r\u00e9f\u00e9rence est d\u00e9sormais disponible \u00e0 titre de r\u00e9f\u00e9rence uniquement. La derni\u00e8re mise \u00e0 jour de la liste remonte \u00e0 janvier 2016. De plus, le Bureau d'innocuit\u00e9 des produits chimiques (BIPC) de Sant\u00e9 Canada \u00e9value, sur une base volontaire, l'innocuit\u00e9 des additifs indirects qui peuvent entrer en contact direct avec les aliments dans les \u00e9tablissements de transformation d'aliments conform\u00e9ment aux directives relatives aux demandes d'autorisation d'additifs indirects. Les lettres exprimant une opinion acceptable du BIPC sont appel\u00e9es \"lettres de non-objection\" (LNO).</p>\n\t \n<p>Le Programme des aliments pour animaux (PAA) de l'ACIA ne s'oppose pas \u00e0 ce que les produits chimiques non alimentaires sur la liste de r\u00e9f\u00e9rence ou, avec une LNO de Sant\u00e9 Canada appartenant aux cat\u00e9gories suivantes soient utilis\u00e9s aux fins pr\u00e9vues dans la production d'aliments pour animaux de ferme\u00a0:</p>\n\t \n<ul>\n<li>cat\u00e9gorie\u00a0: compos\u00e9s de traitement de l'eau des bouilloires (pas pour l'eau de transformation alimentaire)\n<ul>\n<li>Sous-cat\u00e9gorie\u00a0: compos\u00e9s de traitement de l'eau des bouilloires o\u00f9 l'eau trait\u00e9e ou la vapeur produite peut entrer en contact avec les produits alimentaires (w1).</li>\n</ul></li>\n<li>cat\u00e9gorie\u00a0: agents de d\u00e9caract\u00e9risation\n<ul>\n<li>Sous-cat\u00e9gorie\u00a0: viande rouge (i1)</li>\n<li>Sous-cat\u00e9gorie\u00a0: viande blanche (i1)</li>\n</ul></li>\n<li>cat\u00e9gorie\u00a0: agents d\u00e9naturants\n<ul>\n<li>Sous-cat\u00e9gorie\u00a0: viande rouge (i1)</li>\n<li>Sous-cat\u00e9gorie\u00a0: viande blanche (i1)</li>\n</ul></li>\n</ul>\n\n<p class=\"mrgn-tp-md\">Comme la liste de r\u00e9f\u00e9rence n'est plus tenue \u00e0 jour, nous recommandons aux fabricants d'aliments pour animaux de ferme de v\u00e9rifier l'exactitude des informations suivantes aupr\u00e8s du ou des fabricants du ou des produits\u00a0:</p>\n\t \n<ul>\n<li>le produit continue \u00e0 \u00eatre utilis\u00e9 pour l'usage ou les usages pr\u00e9vus et pr\u00e9c\u00e9demment accept\u00e9s (formulation, instructions sur l'\u00e9tiquette, etc.);</li>\n<li class=\"mrgn-bttm-lg\">le fabricant du produit continue \u00e0 recommander le produit pour cette utilisation.</li>\n</ul>\n</li>\n<li><strong>Produits homologu\u00e9s par le Programme des aliments pour animaux (PAA) de l'ACIA</strong>\n   \n<p>Certains produits ant\u00e9rieurement homologu\u00e9s par le PAA de l'ACIA en vue d'une utilisation dans divers processus sont maintenant consid\u00e9r\u00e9s comme des AIA, notamment\u00a0:</p>\n\t \n<ul>\n<li>les produits chimiques de traitement de l'eau comme les agents floculants, coagulants et anti-mousse utilis\u00e9s dans les \u00e9tablissements d'\u00e9quarrissage et de transformation des produits alimentaires pour r\u00e9cup\u00e9rer les solides, lorsque les solides r\u00e9cup\u00e9r\u00e9s sont ensuite utilis\u00e9s comme mati\u00e8res premi\u00e8res afin de produire des AIU (par exemple, lorsque les solides r\u00e9cup\u00e9r\u00e9s sont r\u00e9introduits dans les produits d'\u00e9quarrissage non comestibles utilis\u00e9s dans la fabrication de la farine de viande et d'os).</li>\n</ul>\n\t \n<p>Les enregistrements de ces produits deviendront caduques au fil du temps et seront convertis par le PAA en une lettre de non-objection (voir l'\u00e9l\u00e9ment\u00a03 ci-dessous). Une fois la conversion effectu\u00e9e, les num\u00e9ros d'enregistrement ne devraient plus appara\u00eetre sur les \u00e9tiquettes, mais il faudra peut-\u00eatre un certain temps avant qu'ils disparaissent compl\u00e8tement du march\u00e9. La politique du programme consiste \u00e0 permettre aux produits de circuler sur le march\u00e9 avec l'ancienne \u00e9tiquette pendant une p\u00e9riode de six mois. Les fabricants et les fournisseurs d'aliments pour animaux de ferme disposeront d'une p\u00e9riode de six mois pour utiliser les anciennes \u00e9tiquettes sur lesquelles figurent les num\u00e9ros d'enregistrement. Apr\u00e8s cette p\u00e9riode, les produits mis en vente avec des \u00e9tiquettes contenant un num\u00e9ro d'enregistrement expir\u00e9 seront consid\u00e9r\u00e9s comme non conformes et devront faire l'objet d'un r\u00e9-\u00e9tiquetage.</p>\n\t \n</li>\n<li><strong>Lettre de non-objection (LNO)</strong>\n\t \n<p>Conform\u00e9ment au RG-1, Chapitre\u00a03\u00a0\u2013 <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-3/fra/1617909452465/1617909586070?chap=30\">Directives relatives aux demandes d'autorisation concernant les additifs indirects dans les aliments pour animaux</a>, le PAA de l'ACIA \u00e9value au cas par cas l'acceptabilit\u00e9 des AIA pour lesquels des demandes sont soumises volontairement par des fabricants souhaitant fournir leurs produits aux fabricants d'aliments du b\u00e9tail. Les lettres exprimant un avis favorable sont appel\u00e9es \u00ab\u00a0lettres de non-objection\u00a0\u00bb (LNO). Les fabricants de produits peuvent ensuite les pr\u00e9senter directement \u00e0 des clients potentiels de fabricants d'aliments pour animaux afin qu'ils puissent les inclure dans un plan de contr\u00f4le pr\u00e9ventif (PCP) ou, si n\u00e9cessaire, les fournir \u00e0 l'appui d'une lettre de garantie. Dans le cas o\u00f9 de nouvelles informations sur l'innocuit\u00e9 sugg\u00e8rent que l'AIA n'est plus s\u00e9curitaire pour l'usage auquel il est destin\u00e9, l'ACIA se r\u00e9serve le droit de r\u00e9tracter une LNO \u00e9mise pr\u00e9c\u00e9demment. Il incombe \u00e0 l'entreprise de s'assurer que toutes les LNO figurant dans les dossiers de leur \u00e9tablissement sont \u00e0 jour et toujours en vigueur pour tous les AIA applicables.</p>\n</li>\n<li><strong>Lettres de garantie (LG) remises par les fournisseurs ou les fabricants</strong>\n\t \n<p>Une lettre de garantie (LG) est un document remis par le fournisseur ou le fabricant d'un AIA indiquant que le produit est s\u00e9curitaire, qu'il est adapt\u00e9 \u00e0 l'utilisation et qu'il r\u00e9pond aux sp\u00e9cifications du client. La lettre devrait inclure l'information suivante\u00a0:</p>\n\t \n<ul>\n<li>nom du fournisseur</li>\n<li>date de d\u00e9livrance</li>\n<li>description du produit</li>\n<li>nom de la marque ou num\u00e9ro de code</li>\n<li>une d\u00e9claration selon laquelle l'AIA est s\u00e9curitaire et est adapt\u00e9 \u00e0 l'utilisation pr\u00e9vue</li>\n<li>une r\u00e9f\u00e9rence \u00e0 une LNO correspondante (le cas \u00e9ch\u00e9ant)</li>\n<li>signature du fournisseur</li>\n</ul>\n\t \n<p class=\"mrgn-tp-md\">Il est important de conserver une LG pour tout produit qui n'est pas couvert par les points\u00a01\u00a0\u00e0\u00a03\u00a0ci-dessus.</p>\n\t \n<p>\u00c9tant donn\u00e9 qu'une LNO et une LG peuvent \u00eatre r\u00e9tract\u00e9es ou modifi\u00e9es \u00e0 tout moment, il incombe \u00e0 l'entreprise de s'assurer que le statut des lettres est v\u00e9rifi\u00e9 \u00e0 une fr\u00e9quence acceptable. Lorsqu'un \u00e9tablissement d'aliments du b\u00e9tail a mis en place un plan de contr\u00f4le pr\u00e9ventif, il est important de veiller \u00e0 ce que les LG et les LNO soient inclus dans ces plans.</p>\n\t \n</li>\n<li><strong>AIU d\u00e9crits \u00e0 l'annexe IV ou V du <i>R\u00e8glement sur les aliments du b\u00e9tail</i> inclus dans les AIA</strong>\n\n<p>Les AIU \u00e9num\u00e9r\u00e9s \u00e0 la partie I de l'annexe IV ou V du<i> R\u00e8glement sur les aliments du b\u00e9tail</i> et les sources enregistr\u00e9es des AIU dans la partie\u00a0II peuvent \u00eatre utilis\u00e9s comme composants d'un produit \u00e0 des fins d'AIA. Si tous les composants d'un tel AIA sont r\u00e9pertori\u00e9s dans la partie I de l'annexe IV ou V ou sont des AIU enregistr\u00e9s dans la partie\u00a0II, le risque pos\u00e9 par l'utilisation de ces produits \u00e0 des fins d'AIA est consid\u00e9r\u00e9 comme n\u00e9gligeable. L'acceptabilit\u00e9 des AIA contenant des AIU non homologu\u00e9s figurant dans la partie\u00a0II de l'annexe IV ou V ou d'autres ingr\u00e9dients de formulation ne figurant pas dans le tableau IV ou V doit \u00eatre d\u00e9montr\u00e9e au moyen des \u00e9l\u00e9ments\u00a01 \u00e0\u00a04 ci-dessus.</p>\n\n</li>\n</ol>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}