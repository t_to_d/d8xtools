{
    "dcr_id": "1321293402373",
    "title": {
        "en": "Export of Dogs and Cats to the Cayman Islands",
        "fr": "Exportation de chiens et de chats de compagnie aux \u00celes Ca\u00efmans"
    },
    "html_modified": "2024-01-26 2:20:48 PM",
    "modified": "2020-10-28",
    "issued": "2015-04-08 15:48:49",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_certif_dogs_cats_cayman_1321293402373_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_certif_dogs_cats_cayman_1321293402373_fra"
    },
    "ia_id": "1321293532554",
    "parent_ia_id": "1321281361100",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1321281361100",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Export of Dogs and Cats to the Cayman Islands",
        "fr": "Exportation de chiens et de chats de compagnie aux \u00celes Ca\u00efmans"
    },
    "label": {
        "en": "Export of Dogs and Cats to the Cayman Islands",
        "fr": "Exportation de chiens et de chats de compagnie aux \u00celes Ca\u00efmans"
    },
    "templatetype": "content page 1 column",
    "node_id": "1321293532554",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1321265624789",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/pets/cayman-islands/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/animaux-de-compagnie/iles-caimans/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Export of Dogs and Cats to the Cayman Islands",
            "fr": "Exportation de chiens et de chats de compagnie aux \u00celes Ca\u00efmans"
        },
        "description": {
            "en": "Overview of the requirements for export to the Cayman Islands",
            "fr": "Un formulaire de demande dimportation d\u00fbment rempli, une copie du certificat sanitaire officiel, une copie de rapport de laboratoire concernant le titre des anticorps contre la rage et les droits exigibles doivent \u00eatre pr\u00e9sent\u00e9s aux \u00celes Ca\u00efmans en vue des proc\u00e9dures administratives n\u00e9cessaires."
        },
        "keywords": {
            "en": "exports, dogs, cats, microchip, rabies, antibody titration test, health certificate, Cayman Islands",
            "fr": "voyage,sant\u00e9 animale,m\u00e9decine v\u00e9t\u00e9rinaire,exportation"
        },
        "dcterms.subject": {
            "en": "exports,veterinary medicine,animal health,travel",
            "fr": "exportation,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale,voyage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-08 15:48:49",
            "fr": "2015-04-08 15:48:49"
        },
        "modified": {
            "en": "2020-10-28",
            "fr": "2020-10-28"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Export of Dogs and Cats to the Cayman Islands",
        "fr": "Exportation de chiens et de chats de compagnie aux \u00celes Ca\u00efmans"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>All the requirements and information for bringing dogs and cats to the Cayman Islands are available on the <a href=\"http://doa.gov.ky/portal/page/portal/agrhome/publications/importation-of-dogs-and-cats\">Cayman Islands Department of Agriculture</a> website.  Please review this information and the certificate <a href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/cayman_islands_cym_1566316091424_eng.pdf\">Export of Dogs and Cats to the Cayman Islands from Canada - PDF (472\u00a0kb)</a> to ensure that all requirements are met in advance of travel.</p>\n<p>If you have difficulties viewing the information contained on this website, you can contact the Canadian Food Inspection Agency's <a href=\"/eng/1300462382369/1300462438912\">Animal Health Office</a> in your area to receive a copy of the necessary documents.</p>\n<p><strong>Please note that the Cayman Islands may change these requirements without notification to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. As a result, it is strongly advised to review this export certificate against the import permit to ensure that all requirements are addressed.</strong></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Toutes les exigences et les informations pour apporter des chiens et chats aux \u00celes Ca\u00efmans sont disponible sur le site web du <a href=\"http://doa.gov.ky/portal/page/portal/agrhome/publications/importation-of-dogs-and-cats\">Minist\u00e8re de l'agriculture des \u00celes Ca\u00efmans (en anglais seulement)</a>. Veuillez examiner cette information et le certificat intitul\u00e9 <a href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/cayman_islands_cym_1566316091424_fra.pdf\">Exportation de chiens et de chats aux \u00celes Ca\u00efmans du Canada - PDF (472\u00a0ko) (en anglais seulement)</a> et assurez-vous que toutes les exigences sont respect\u00e9es avant le voyage.</p>\n<p>Si vous avez de difficult\u00e9 \u00e0 visualiser toutes les informations contenues sur ce site internet, vous pouvez communiquer avec le <a href=\"/fra/1300462382369/1300462438912\">bureau de la sant\u00e9 des animaux</a> de l'Agence canadienne d'inspection des aliments dans votre r\u00e9gion pour obtenir une copie des documents appropri\u00e9s.</p>\n<p><strong>Veuillez noter que les Iles Ca\u00efman peuvent modifier ces exigences sans notice pr\u00e9alable \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.  Il est donc fortement recommand\u00e9 de v\u00e9rifier que le certificat d'exportation rencontre toutes les exigences du permis d'importation.</strong></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}