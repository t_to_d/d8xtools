{
    "dcr_id": "1525112076928",
    "title": {
        "en": "New Zealand - Export requirements for honey",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le miel"
    },
    "html_modified": "2024-01-26 2:24:08 PM",
    "modified": "2019-11-27",
    "issued": "2018-05-25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/honey_export_overview_zealand_1525112076928_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/honey_export_overview_zealand_1525112076928_fra"
    },
    "ia_id": "1525112077224",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Honey",
        "fr": "Miel"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "New Zealand - Export requirements for honey",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le miel"
    },
    "label": {
        "en": "New Zealand - Export requirements for honey",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le miel"
    },
    "templatetype": "content page 1 column",
    "node_id": "1525112077224",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/new-zealand-honey/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/nouvelle-zelande-miel/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "New Zealand - Export requirements for honey",
            "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le miel"
        },
        "description": {
            "en": "Countries to which exports are currently made \u2013 New Zealand",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Nouvelle-Z\u00e9lande"
        },
        "keywords": {
            "en": "honey, Canadian honey, bee products, export requirements, federally registered honey establishments, New Zealand",
            "fr": "miel, miel canadien, produits d'abeilles, exigences d'exportation,\u00e9tablissements de miel agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral, Nouvelle-Z\u00e9lande"
        },
        "dcterms.subject": {
            "en": "food labelling,exports,imports,food inspection,honey,agri-food products,food safety,food processing",
            "fr": "\u00e9tiquetage des aliments,exportation,importation,inspection des aliments,miel,produit agro-alimentaire,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exportation d'aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-05-25",
            "fr": "2018-05-25"
        },
        "modified": {
            "en": "2019-11-27",
            "fr": "2019-11-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "New Zealand - Export requirements for honey",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le miel"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<ul><li>Products containing honey only from New Zealand</li></ul>\n\n<h3>Ineligible</h3>\n\n<ul><li>Honey from any jurisdictions other than New Zealand</li></ul>\n\n<p><strong>Note:</strong> Bee products for human consumption, such as honeycomb (comb honey), flavoured honey, royal jelly, bee propolis, and bee pollen, are also regulated under the <a href=\"/english/reg/jredirect2.shtml?drga\"><i>Food and Drugs Act</i></a>, the <a href=\"/english/reg/jredirect2.shtml?drgr\"><i>Food and Drugs Regulations</i></a>. Additionally, honey for human consumption is regulated under the <a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i></a> and <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a>.</p>\n\n<h2>Production controls and inspection requirements</h2>\n\n<ul><li class=\"mrgn-bttm-md\">The exporter should contact the <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Canadian Food Inspection Agency (CFIA) regional office</a> closest to where the shipment will be loaded at least seven (7) business days before the scheduled date of shipment, and request the following documents:\n\n<ul><li class=\"mrgn-bttm-md\">Health Certificate Request to Export of Honey Products to New Zealand</li>\n<li class=\"mrgn-bttm-md\">Manufacturer's declaration for the export of honey products to New Zealand</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">The exporter may be required to provide additional information to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to support the submission of the above documents and ultimate certification.</li></ul>\n\n<h3>Imported honey</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Imported honey from New Zealand must be tracked through the entire manufacturing process until the time of export to ensure that no cross-contamination exists with honey from any other sources:\n\n<ol><li class=\"mrgn-bttm-md\">Each imported shipment of New Zealand bulk honey must be legally imported into Canada and accompanied by an export certificate issued by New Zealand Ministry of Primary Industry (NZ\u00a0MPI).</li>\n\n<li class=\"mrgn-bttm-md\">Imported container numbers must be verified to match the container numbers on the export certificate issued by <abbr title=\"New Zealand Ministry of Primary Industry\">NZ\u00a0MPI</abbr>.</li>\n\n<li class=\"mrgn-bttm-md\">The original relevant export documentation from <abbr title=\"New Zealand Ministry of Primary Industry\">NZ\u00a0MPI</abbr> must be copied to file and kept available for reference and future re-export.</li>\n\n<li class=\"mrgn-bttm-md\">Upon receipt of incoming materials, original lot numbers must be recorded and entered into inventory systems.\n\n<ul><li class=\"mrgn-bttm-md\">Manufacturer's standard operating procedure (SOP) must refer to \"Inspection and receiving of incoming goods\", ensuring all honey is received in proper condition with attached lot numbers and paperwork.</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">Each final package must be traceable back to the exact lot of imported New Zealand honey.\n\n<ul><li class=\"mrgn-bttm-md\">Lot number assignments must be applied to all incoming raw material, through to the finished packaged product according to manufacturer's <abbr title=\"standard operating procedure\">SOP</abbr> \"Lot Number Assignment \u2013 Manuka Honey\".</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">The establishment must have procedures in place to segregate incoming goods, through receiving to the finished packaged products.\n\n<ul><li class=\"mrgn-bttm-md\">All honey products must be produced following a full sanitary clean up according to manufacturer's <abbr title=\"standard operating procedure\">SOP</abbr> \"Production Area Premise Inspection\" ensuring there is no product addition, mixing or substitution.</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">The final packaging must be controlled by manufacturer's <abbr title=\"standard operating procedure\">SOP</abbr> \"Procedure for Packaging Dried Product\", which clearly identifies all proper packaging and labelling for all products.</li>\n\n<li class=\"mrgn-bttm-md\">Product labels must be carefully inspected and shipping documents including accompanying export certificate from <abbr title=\"New Zealand Ministry of Primary Industry\">NZ\u00a0MPI</abbr> must be verified against the actual order received.</li>\n\n<li>Products that have lost original labelling must be rejected, will not be used for manufacture, and will not be returned to New Zealand.</li>\n\n<li>Packaging of finished product must be correctly labelled reflecting exact content, including the name of the product, lot and or batch number.</li></ol></li></ul>\n\n<h2>Documentation requirements</h2>\n\n<h3>Certificate</h3>\n\n<ul><li>Health certificate for imports of honey and other apiculture products intended for human consumption</li></ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<ul><li>Produits contenant du miel uniquement de Nouvelle-Z\u00e9lande</li></ul>\n\n<h3>Non admissibles</h3>\n\n<ul><li>Miel provenant de toutes les juridictions autres que la Nouvelle-Z\u00e9lande</li></ul>\n\n<p><strong>Remarque\u00a0:</strong> Les produits apicoles destin\u00e9s \u00e0 la consommation humaine, comme le nid d'abeilles (rayon de miel), le miel aromatis\u00e9, la gel\u00e9e royale, la propolis d'abeilles, et le pollen d'abeille, sont \u00e9galement r\u00e9glement\u00e9s par la <a href=\"/francais/reg/jredirect2.shtml?drga\"><i>Loi sur les aliments et drogues</i></a>, le <a href=\"/francais/reg/jredirect2.shtml?drgr\"><i>R\u00e8glement sur les aliments et drogues</i></a>. De plus, le miel destin\u00e9 \u00e0 la consommation humaine est r\u00e9gi par la <a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i></a> et le <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a>.</p>\n\n<h2>Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<ul><li class=\"mrgn-bttm-md\">L'exportateur doit communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau r\u00e9gional de l'Agence canadienne d'inspection des aliments (ACIA)</a> le plus pr\u00e8s de l'endroit o\u00f9 l'envoi sera charg\u00e9 au moins sept (7) jours ouvrables avant la date d'exp\u00e9dition pr\u00e9vue et demander les documents suivants\u00a0:\n\n<ul><li class=\"mrgn-bttm-md\">Demande de certificat sanitaire pour l'exportation de produits de miel vs la Nouvelle Z\u00e9lande</li>\n<li class=\"mrgn-bttm-md\">D\u00e9claration du fabricant pour l'exportation de produits du miel vers la Nouvelle Z\u00e9lande</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">L'exportateur peut \u00eatre tenu de fournir des renseignements suppl\u00e9mentaires \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pour appuyer la soumission des documents susmentionn\u00e9s et la certification finale (<abbr title=\"c'est-\u00e0-dire\">c.-\u00e0-d.</abbr> des certificats d'analyse).</li></ul>\n\n<h3>Miel import\u00e9</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Le miel import\u00e9 de Nouvelle-Z\u00e9lande doit \u00eatre suivi tout au long du processus de fabrication jusqu'au moment de l'exportation pour s'assurer qu'il n'y a pas de contamination crois\u00e9e avec le miel provenant d'autres sources\u00a0:\n\n<ol><li class=\"mrgn-bttm-md\">Chaque envoi import\u00e9 de miel en vrac n\u00e9o-z\u00e9landais doit \u00eatre import\u00e9 l\u00e9galement au Canada et accompagn\u00e9 d'un certificat d'exportation d\u00e9livr\u00e9 par le minist\u00e8re n\u00e9o-z\u00e9landais de l'industrie primaire (NZ\u00a0MPI).</li>\n\n<li class=\"mrgn-bttm-md\">Les num\u00e9ros de conteneur import\u00e9s doivent \u00eatre v\u00e9rifi\u00e9s pour correspondre aux num\u00e9ros de conteneur sur le certificat d'exportation \u00e9mis par <abbr title=\"minist\u00e8re n\u00e9o-z\u00e9landais de l'industrie primaire\">NZ\u00a0MPI</abbr>.</li>\n\n<li class=\"mrgn-bttm-md\">La documentation d'exportation pertinente originale de <abbr title=\"minist\u00e8re n\u00e9o-z\u00e9landais de l'industrie primaire\">NZ\u00a0MPI</abbr> doit \u00eatre copi\u00e9e dans un fichier et conserv\u00e9e \u00e0 des fins de r\u00e9f\u00e9rence et de r\u00e9exportation future.</li>\n\n<li class=\"mrgn-bttm-md\">\u00c0 la r\u00e9ception des mat\u00e9riaux entrants, les num\u00e9ros de lots originaux doivent \u00eatre enregistr\u00e9s et entr\u00e9s dans les syst\u00e8mes d'inventaire.\n\n<ul><li class=\"mrgn-bttm-md\">La proc\u00e9dure d'op\u00e9rationnelle normalis\u00e9 (PON) du fabricant doit se r\u00e9f\u00e9rer \u00e0 \u00ab\u00a0Inspection et r\u00e9ception des marchandises premi\u00e8res\u00a0\u00bb, en s'assurant que tout le miel est re\u00e7u en bon \u00e9tat avec les num\u00e9ros de lot et les documents joints.</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">Chaque emballage final doit \u00eatre reli\u00e9 jusqu'au lot exact de miel n\u00e9o-z\u00e9landais import\u00e9.\n\n<ul><li class=\"mrgn-bttm-md\">Les attributions de num\u00e9ros de lot doivent \u00eatre appliqu\u00e9es \u00e0 toutes les mati\u00e8res premi\u00e8res entrantes, jusqu'au produit emball\u00e9 fini, conform\u00e9ment \u00e0 la PON du fabricant \u00ab\u00a0Assignation d'un num\u00e9ro de lot \u2013 Miel Manuka\u00a0\u00bb.</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">L'\u00e9tablissement doit avoir des proc\u00e9dures en place pour s\u00e9parer les mati\u00e8res entrantes, en les r\u00e9ceptionnant aux produits finis emball\u00e9s.\n\n<ul><li class=\"mrgn-bttm-md\">Tous les produits du miel doivent \u00eatre produits apr\u00e8s un nettoyage sanitaire complet conform\u00e9ment \u00e0 <abbr title=\"proc\u00e9dure d'op\u00e9rationnelle normalis\u00e9\">PON</abbr> \u00ab\u00a0Inspection de la zone du local de production\u00a0\u00bb du fabricant, pour s'assurer qu'il n'y a pas d'ajout, ni de m\u00e9lange ou substitution.</li></ul></li>\n\n<li class=\"mrgn-bttm-md\">L'emballage final doit \u00eatre contr\u00f4l\u00e9 par la <abbr title=\"proc\u00e9dure d'op\u00e9rationnelle normalis\u00e9\">PON</abbr> du fabricant pour \u00ab\u00a0Proc\u00e9dure pour l'emballage d'un produit sec\u00a0\u00bb, qui identifie clairement tous les emballages et \u00e9tiquetages appropri\u00e9s pour tous les produits.</li>\n\n<li class=\"mrgn-bttm-md\">Les \u00e9tiquettes des produits doivent \u00eatre soigneusement inspect\u00e9es et les documents d'exp\u00e9dition accompagn\u00e9s du certificat d'exportation de <abbr title=\"minist\u00e8re n\u00e9o-z\u00e9landais de l'industrie primaire\">NZ\u00a0MPI</abbr> doivent \u00eatre v\u00e9rifi\u00e9s par rapport \u00e0 l'actuelle commande.</li>\n\n<li>Les produits ayant perdu l'\u00e9tiquetage d'origine doivent \u00eatre rejet\u00e9s, ne seront pas utilis\u00e9s pour la fabrication et ne seront pas retourn\u00e9s en Nouvelle-Z\u00e9lande.</li>\n\n<li>L'emballage du produit fini doit \u00eatre correctement \u00e9tiquet\u00e9 refl\u00e9tant le contenu exact, incluant le nom du produit, et le num\u00e9ro de lot ou batch.</li></ol></li></ul>\n\n<h2>Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<ul><li>Certificat sanitaire pour l'importation de produits de miel et d'autres produits de l'apiculture destin\u00e9s \u00e0 la consommation humaine</li></ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}