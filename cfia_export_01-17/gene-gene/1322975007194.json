{
    "dcr_id": "1322975007194",
    "title": {
        "en": "List of approved ingredients and registered feeds for livestock in Canada",
        "fr": "Listes d'ingr\u00e9dients approuv\u00e9s et des aliments enregistr\u00e9s pour animaux de ferme au Canada"
    },
    "html_modified": "2015-04-08 14:14",
    "modified": "11-8-2023",
    "issued": "8-4-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/anim_feed_approved_ing_1322975007194_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/anim_feed_approved_ing_1322975007194_fra"
    },
    "parent_ia_id": "1320536661238",
    "ia_id": "1322975281243",
    "parent_node_id": "1320536661238",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "List of approved ingredients and registered feeds for livestock in Canada",
        "fr": "Listes d'ingr\u00e9dients approuv\u00e9s et des aliments enregistr\u00e9s pour animaux de ferme au Canada"
    },
    "label": {
        "en": "List of approved ingredients and registered feeds for livestock in Canada",
        "fr": "Listes d'ingr\u00e9dients approuv\u00e9s et des aliments enregistr\u00e9s pour animaux de ferme au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1322975281243",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299157225486",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/approved-ingredients/",
        "fr": "/sante-des-animaux/aliments-du-betail/ingredients-approuves/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "List of approved ingredients and registered feeds for livestock in Canada",
            "fr": "Listes d'ingr\u00e9dients approuv\u00e9s et des aliments enregistr\u00e9s pour animaux de ferme au Canada"
        },
        "description": {
            "en": "Schedules IV and V of the Feeds Regulations are the lists of ingredients that have been evaluated and approved by the CFIA for manufacture, import, and sale for use in livestock feed in Canada.",
            "fr": "Les annexes IV et V du R\u00e8glement sur les aliments du b\u00e9tail constituent les listes d'ingr\u00e9dients \u00e9valu\u00e9s et approuv\u00e9s par l'ACIA pour la fabrication, l'importation et la vente pour utilisation dans les aliments du b\u00e9tail au Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, Feeds Act, Feeds Regulations, livestock, animal feeds, novel feeds, biotechnology, ingredients, toxicology, regulatory guidance",
            "fr": "Loi sur la sant&#233; des animaux, R&#232;glement sur la sant&#233; des animaux, Loi relative aux aliments du b&#233;tail, R&#232;glement sur les aliments du b&#233;tail, aliments du betail, aliment nouveau, biotechnologie, aliments m&#233;dicament&#233;s, ingr&#233;dients, toxicologiques, directives r&#233;glementaires"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,standards,policy,regulation,animal health",
            "fr": "b\u00e9tail,inspection,norme,politique,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-08 14:14:39",
            "fr": "2015-04-08 14:14:39"
        },
        "modified": {
            "en": "2023-08-11",
            "fr": "2023-08-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "List of approved ingredients and registered feeds for livestock in Canada",
        "fr": "Listes d'ingr\u00e9dients approuv\u00e9s et des aliments enregistr\u00e9s pour animaux de ferme au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<ul>\n<li><a href=\"#a\">List of registered livestock feeds in Canada</a></li>\n<li><a href=\"#b\">List of approved ingredients</a></li>\n<li><a href=\"#c\">Part\u00a0I of Schedules\u00a0IV and\u00a0V</a></li>\n<li><a href=\"#d\">Part\u00a0II of Schedules\u00a0IV and\u00a0V</a></li>\n</ul>\n\n\n<h2 id=\"a\">List of registered livestock feeds in Canada</h2>\n\n<p>This is a list of livestock feeds registered by the Canadian Food Inspection Agency (CFIA) under the authority of the <i>Feeds Act</i> and regulations. A feed with an active registration can be manufactured, sold or imported in Canada.</p>\n\n<p><a href=\"/webapps/FeedList/Home/Search?lang=en\">List of Registered Livestock Feeds in Canada</a></p>\n\n<h2 id=\"b\">List of approved ingredients</h2>\n\n<p>Schedules <abbr title=\"4\">IV</abbr> and <abbr title=\"5\">V</abbr> of the <i>Feeds Regulations</i> are the lists of ingredients that have been evaluated and approved by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for manufacture, sale or import in Canada.</p>\n<p>Note: all ingredients or products, in part or in whole, comprised of nano-materials require approval or registration.</p>\n\n<h2 id=\"c\">Part\u00a0I of Schedules\u00a0IV and\u00a0V</h2>\n\n<p>Ingredients listed in part I of Schedules IV and V are exempt from registration and may be used, sold and imported freely in the manufacture of feeds in Canada, provided they:</p>\n\n<ul>\n<li>conform to the ingredient definition as listed </li>\n<li>are labelled as prescribed including required guarantees and </li>\n<li>meet standards identified in the regulations and further defined in regulatory guidance</li>\n</ul>\n\n<p>Part I ingredients may not have extra label guarantees or claims. Ingredients with extra label guarantees or claims that are not prescribed in the <i>Feeds Regulations</i> require pre-market assessment and approval.</p>\n\n<h2 id=\"d\">Part\u00a0II of Schedules\u00a0IV and\u00a0V</h2>\n\n<p>Ingredients in part II Schedule IV and V are not exempt from registration. As there may be inherent safety and/or efficacy variations associated with an individual source of the ingredient, or introduced via the manufacturing process, a pre-market evaluation is required. Each manufacturing site is considered a distinct source for a single ingredient feed (SIF) listed in part II of Schedule IV or V. Each source of a SIF must be evaluated with data to support the safety and/or efficacy of the product. Once the evaluation is completed and approved by the CFIA, a registration number is assigned to the SIF listed in part II. For a SIF part II that is manufactured at multiple sites, the same registration number can be assigned once the CFIA has verified that the manufacturing process, raw materials including grade, purity, levels of contaminants, quality controls and final product specifications are equivalent between these manufacturing sites.</p>\n<p>To search for ingredients that are registered sources, refer to the <a href=\"/animal-health/livestock-feeds/approved-ingredients/eng/1322975007194/1322975281243\">List of registered livestock feeds in Canada.</a></p>\n<h3>Ingredients requiring approval and/or registration</h3>\n\n<ul>\n<li>any ingredient that is not currently listed in Schedules\u00a0IV or\u00a0V, or</li>\n<li>any single ingredient feed that has been modified such that it differs significantly from a conventional ingredient such as composition, manufacturing process, and/or use</li>\n<li>ingredients in part\u00a0II of Schedule\u00a0IV or\u00a0V</li>\n</ul>\n\n<p>For more information about approval or registration, refer to the <a href=\"/animal-health/livestock-feeds/regulatory-guidance/eng/1299871623634/1320602307623\">RG-1 Regulatory Guidance: Feed Registration Procedures and Labelling Standards</a>.</p>\n\n<p>The Schedules\u00a0IV and\u00a0V of the <i>Feeds Regulations</i> are available on request by emailing the Animal Feed Program. Please send an email with the subject line \"Schedules IV and V\" at: <a href=\"mailto:cfia.afp-paa.acia@inspection.gc.ca?subject=Schedules%20IV%20and%20V\">cfia.afp-paa.acia@inspection.gc.ca</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<ul>\n<li><a href=\"#a\">Listes des aliments enregistr\u00e9s pour animaux de ferme au Canada</a></li>\n<li><a href=\"#b\">Liste des ingr\u00e9dients approuv\u00e9s</a></li>\n<li><a href=\"#c\">Partie\u00a0I des annexes\u00a0IV et\u00a0V</a></li>\n<li><a href=\"#d\">Partie\u00a0II des annexes\u00a0IV et\u00a0V</a></li>\n</ul>\n\n<h2 id=\"a\">Listes des aliments pour animaux de ferme enregistr\u00e9s au Canada</h2>\n \n<p>Ceci est une liste des aliments pour animaux de ferme enregistr\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA) en vertu de la <i>Loi sur les aliments du b\u00e9tail</i> et de son r\u00e8glement. Un aliment pour animaux avec un enregistrement actif peut \u00eatre fabriqu\u00e9 vendu ou import\u00e9 au Canada.</p>\n \n<p><a href=\"/webapps/FeedList/Home/Search?lang=fr\">Liste des aliments enregistr\u00e9s pour animaux de ferme au Canada</a></p>\n\n<h2 id=\"b\">Liste des ingr\u00e9dients approuv\u00e9s</h2>\n\n<p>Les annexes <abbr title=\"4\">IV</abbr> et <abbr title=\"5\">V</abbr> du <i>R\u00e8glement sur les aliments du b\u00e9tail</i> constituent les listes d'ingr\u00e9dients \u00e9valu\u00e9s et approuv\u00e9s par l'ACIA pour la fabrication, la vente et l'importation au Canada.</p>\n<p>Remarque\u00a0: tous les ingr\u00e9dients ou produits, en tout ou en partie, constitu\u00e9s de nanomat\u00e9riaux doivent \u00eatre approuv\u00e9s ou enregistr\u00e9s.</p>\n\n<h2 id=\"c\">Partie\u00a0I des annexes\u00a0IV et\u00a0V</h2>\n\n<p>Les ingr\u00e9dients \u00e9num\u00e9r\u00e9s dans la partie I des annexes IV et V sont exempt\u00e9s d'enregistrement et peuvent \u00eatre utilis\u00e9s, vendus et import\u00e9s librement dans la fabrication d'aliments pour animaux au Canada, \u00e0 condition:</p>\n\n<ul>\n<li>qu'ils soient conformes \u00e0 la d\u00e9finition de l'ingr\u00e9dient;</li>\n<li>qu'ils satisfont aux normes d'\u00e9tiquetage prescrit, y compris les garanties requises et;</li>\n<li>qu'ils se conforment aux normes \u00e9nonc\u00e9es dans les r\u00e8glements et dans les directives r\u00e9glementaires.</li>\n</ul>\n\n<p>Les ingr\u00e9dients de la partie I ne peuvent pas avoir de garanties ou d'all\u00e9gations suppl\u00e9mentaires sur l'\u00e9tiquette. Les ingr\u00e9dients avec des garanties ou des all\u00e9gations suppl\u00e9mentaires sur l'\u00e9tiquette qui ne sont pas prescrites dans le <i>R\u00e8glement sur les aliments du b\u00e9tail</i> n\u00e9cessitent une \u00e9valuation et une approbation avant la mise en march\u00e9.</p>\n\n<h2 id=\"d\">Partie\u00a0II des annexes\u00a0IV et\u00a0V</h2>\n\n<p>Les ingr\u00e9dients de la partie II ne sont pas exempt\u00e9s de l'enregistrement. Puisqu'il pourrait y avoir des variations inh\u00e9rentes reli\u00e9es \u00e0 l'innocuit\u00e9 et/ou l'efficacit\u00e9 associ\u00e9es \u00e0 une source individuelle de l'ingr\u00e9dient, ou introduites par l'entremise du processus de fabrication, une \u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 est exig\u00e9e. Chaque site de fabrication est consid\u00e9r\u00e9 comme une source distincte pour un aliment \u00e0 ingr\u00e9dient unique (AIU) inscrit \u00e0 la partie II de l'annexe IV ou V. Chaque source d'un AIU doit \u00eatre \u00e9valu\u00e9e \u00e0 l'aide de donn\u00e9es \u00e0 l'appui de l'innocuit\u00e9 et/ou de l'efficacit\u00e9 du produit. Une fois l'\u00e9valuation termin\u00e9e et approuv\u00e9e par l'ACIA, un num\u00e9ro d'enregistrement est attribu\u00e9 \u00e0 l'AIU inscrit \u00e0 la partie II. Dans le cas d'un AIU de la partie II fabriqu\u00e9 \u00e0 plusieurs sites, le m\u00eame num\u00e9ro d'enregistrement peut \u00eatre attribu\u00e9 une fois que l'ACIA a v\u00e9rifi\u00e9 que le proc\u00e9d\u00e9 de fabrication, les mati\u00e8res premi\u00e8res, y compris la qualit\u00e9, la puret\u00e9, les niveaux de contaminants, les contr\u00f4les de la qualit\u00e9 et les sp\u00e9cifications du produit final sont \u00e9quivalents entre ces sites de fabrication.</p>\n<p>Pour rechercher des ingr\u00e9dients qui sont des sources enregistr\u00e9es, consultez la \u00a0<a href=\"/sante-des-animaux/aliments-du-betail/ingredients-approuves/fra/1322975007194/1322975281243\">Liste des aliments pour animaux de ferme enregistr\u00e9s au Canada</a>.</p>\n<h3>Ingr\u00e9dients n\u00e9cessitant une approbation et/ou un enregistrement</h3>\n\n<ul>\n<li>tout ingr\u00e9dient qui n'est pas actuellement list\u00e9 \u00e0 l'annexe IV ou V</li>\n<li>tout ingr\u00e9dient qui a \u00e9t\u00e9 modifi\u00e9 de telle sorte qu'il diff\u00e8re consid\u00e9rablement d'un ingr\u00e9dient conventionnel tel que la composition, le processus de fabrication et/ou l'utilisation</li>\n<li>les ingr\u00e9dients de la partie\u00a0II de l'annexe\u00a0IV ou\u00a0V.</li>\n</ul>\n\n<p>Pour plus d'informations sur l'approbation ou l'enregistrement, reportez-vous au <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/fra/1329109265932/1329109385432\">RG-1 Directives R\u00e9glementaires\u00a0: Proc\u00e9dures d'enregistrement et normes d'\u00e9tiquetage des aliments pour animaux</a></p>\n\n<p>Les annexes IV et V du <i>R\u00e8glement sur les aliments du b\u00e9tail</i> sont disponibles sur demande en envoyant un courriel au Programme des aliments pour animaux. Veuillez envoyer un courriel avec la ligne d'objet \u00ab\u00a0Annexes IV et V\u00a0\u00bb \u00e0\u00a0: <a href=\"mailto:cfia.afp-paa.acia@inspection.gc.ca?subject=Annexes%20IV%20et%20V\">cfia.afp-paa.acia@inspection.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}