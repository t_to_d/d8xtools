{
    "dcr_id": "1374591597014",
    "title": {
        "en": "Validation of Animal Health Import Permits Prior to Importation",
        "fr": "Validation des permis d'importation de la sant\u00e9 animale avant l'importation "
    },
    "html_modified": "2024-01-26 2:22:01 PM",
    "modified": "2013-07-23",
    "issued": "2013-07-23 10:59:58",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_import_permits_valid_1374591597014_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_import_permits_valid_1374591597014_fra"
    },
    "ia_id": "1374592033347",
    "parent_ia_id": "1374511696513",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1374511696513",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Validation of Animal Health Import Permits Prior to Importation",
        "fr": "Validation des permis d'importation de la sant\u00e9 animale avant l'importation "
    },
    "label": {
        "en": "Validation of Animal Health Import Permits Prior to Importation",
        "fr": "Validation des permis d'importation de la sant\u00e9 animale avant l'importation "
    },
    "templatetype": "content page 1 column",
    "node_id": "1374592033347",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1374511671189",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/imports/permit-application/validation/",
        "fr": "/sante-des-animaux/animaux-terrestres/importation/demande-de-permis/validation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Validation of Animal Health Import Permits Prior to Importation",
            "fr": "Validation des permis d'importation de la sant\u00e9 animale avant l'importation"
        },
        "description": {
            "en": "The CFIA Electronic Data Interchange (EDI) will accept only a valid animal health import permit number that matches the one issued to the importer by the CFIA.",
            "fr": "L'\u00e9change \u00e9lectronique de donn\u00e9es (EED) acceptera uniquement les num\u00e9ros de permis d'importation valides, relatif \u00e0 la sant\u00e9 des animaux, qui correspondent \u00e0 ceux d\u00e9livr\u00e9s \u00e0 l'importateur par l'ACIA"
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, imports, permits, live animals, animal products, animal by-products, validation",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, importation, permis, animaux vivants, produits animaux, sous-produits animaux, validation"
        },
        "dcterms.subject": {
            "en": "imports,inspection,veterinary medicine,regulation,animal health",
            "fr": "importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-07-23 10:59:58",
            "fr": "2013-07-23 10:59:58"
        },
        "modified": {
            "en": "2013-07-23",
            "fr": "2013-07-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Validation of Animal Health Import Permits Prior to Importation",
        "fr": "Validation des permis d'importation de la sant\u00e9 animale avant l'importation"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>According to the <i>Animal Health Regulations</i>, importers of animal products and by-products, and commodities containing these as ingredients, are required to obtain an animal health import permit (dependent on commodity and country of origin), issued by the Canadian Food Inspection Agency (CFIA). The importer is required to obtain a permit for the commodity(ies) being imported, and to include any other documents that may be necessary for compliance with Canadian requirements (<abbr title=\"for example\">e.g.</abbr> registration of livestock feed). Please verify the required documentation by accessing the <a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System</a> (AIRS) or by contacting the <a href=\"/importing-food-plants-or-animals/food-imports/nisc/eng/1364059150360/1364059265637\">National Import Service Centre</a>.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Electronic Data Interchange (EDI) will accept only a valid animal health import permit number that matches the one issued to the importer by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. If the import permit is not valid, the <abbr title=\"Electronic Data Interchange\">EDI</abbr> transaction will be rejected, and the commodity will not be permitted entry into Canada, until such time as a replacement transaction (or version) is submitted.</p>\n<p>The animal health import permit number is referred to in <abbr title=\"Electronic Data Interchange\">EDI</abbr> as a \"Registration Number\". The animal health import permit number (<abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr>) must be entered under the \"Registration Type\" with the appropriate \"Registration Code\".</p>\n<h2>Registration Information</h2>\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<th>Registration Code</th>\n<th>Registration Description</th>\n<th>Registration Type</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>3 =</td>\n<td>Import Permit</td>\n<td><abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr></td>\n</tr>\n<tr>\n<td>11 =</td>\n<td>Permit to Import Animal Products</td>\n<td><abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr></td>\n</tr>\n<tr>\n<td>25 =</td>\n<td>Permit to Import Samples</td>\n<td><abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr></td>\n</tr>\n<tr>\n<td>42 =</td>\n<td>Permit to Import Veterinary Biologics</td>\n<td><abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr></td>\n</tr>\n<tr>\n<td>43 =</td>\n<td>Permit to Import Pathogens</td>\n<td><abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr></td>\n</tr>\n</tbody>\n</table>\n<h2>Rejection Reasons</h2>\n<p>These provides the reasons for rejection of import permit validation.</p>\n<ul>\n<li class=\"mrgn-bttm-md\">Invalid Format = <strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>-Invalid Registration Number.</strong> Import permit <strong>Number</strong> format must be <abbr title=\"Animal\">A</abbr>-<abbr title=\"Year of issuance\">YYYY</abbr>-<abbr title=\"five-digit Unique number\">NNNNN</abbr>-<abbr title=\"area number\">n</abbr></li>\n<li class=\"mrgn-bttm-md\">Invalid Permit Number = <strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>-Invalid Registration Number.</strong> Import permit <strong>Number Not</strong> valid</li>\n<li class=\"mrgn-bttm-md\">Invalid Permit = <strong><abbr title=\"Canadian Food Inspection Agency\">CFI</abbr>A-Invalid Registration Number.</strong> Import permit <strong>Not</strong> valid</li>\n</ul>\n<p>For additional information on rejection reasons and other <abbr title=\"Electronic Data Interchange\">EDI</abbr> issues, please contact the <a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049?EDI-CFIA-ACIA\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> <abbr title=\"Electronic Data Interchange\">EDI</abbr> Coordinator</a> by e-mail, or call the <abbr title=\"Electronic Data Interchange\">EDI</abbr> Help Line at 613-773-5322.</p>\n<p class=\"small text-right\">Originally issued February 14, 2011 (Notice to Industry)</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Conform\u00e9ment au <i>R\u00e8glement sur la sant\u00e9 des animaux</i>, les importateurs de produits et sous-produits animaux et de produits contenant de tels ingr\u00e9dients doivent obtenir un permis d'importation relatif \u00e0 la sant\u00e9 des animaux (en fonction du produit et du pays d'origine) d\u00e9livr\u00e9 par l'Agence canadienne d'inspection des aliments (ACIA). L'importateur doit obtenir un permis pour le ou les produits qu'il souhaite importer et joindre tout autre document n\u00e9cessaire attestant leur conformit\u00e9 aux exigences canadiennes (<abbr title=\"par exemple\">p. ex.</abbr> enregistrement d'aliments du b\u00e9tail). Veuillez v\u00e9rifier la documentation \u00e0 pr\u00e9senter dans le <a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation</a> (SARI) ou aupr\u00e8s du <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/csni/fra/1364059150360/1364059265637\">Centre de service national \u00e0 l'importation</a>.</p>\n<p>L'\u00c9change de donn\u00e9e informatis\u00e9 (\u00c9DI) acceptera uniquement les num\u00e9ros de permis d'importation valides, relatif \u00e0 la sant\u00e9 des animaux, qui correspondent \u00e0 ceux d\u00e9livr\u00e9s \u00e0 l'importateur par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Si le permis d'importation n'est pas valide, la transaction d'<abbr title=\"\u00c9change de donn\u00e9e informatis\u00e9\">\u00c9DI</abbr> sera rejet\u00e9e, et le produit se verra refuser l'entr\u00e9e au Canada jusqu'\u00e0 ce qu'une autre transaction (ou version) soit soumise.</p>\n<p>Le num\u00e9ro du permis d'importation relatif \u00e0 la sant\u00e9 des animaux (<abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr>) figure dans l'<abbr title=\"\u00c9change de donn\u00e9e informatis\u00e9\">\u00c9DI</abbr> sous \u00ab\u00a0Num\u00e9ro d'enregistrement\u00a0\u00bb et doit \u00eatre saisi sous \u00ab\u00a0Type d'enregistrement\u00a0\u00bb accompagn\u00e9 du bon \u00ab\u00a0Code d'enregistrement\u00a0\u00bb.</p>\n<h2>Information relative \u00e0 l'enregistrement</h2>\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<th>Code<br>\n</th>\n<th>Description<br>\n</th>\n<th>Type<br>\n</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>3 =</td>\n<td>Permis d'importation</td>\n<td class=\"nowrap\"><abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr></td>\n</tr>\n<tr>\n<td>11 =</td>\n<td>Permis d'importation de produits animaux</td>\n<td class=\"nowrap\"><abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr></td>\n</tr>\n<tr>\n<td>25 =</td>\n<td>Permis d'importation d'\u00e9chantillons</td>\n<td class=\"nowrap\"><abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr></td>\n</tr>\n<tr>\n<td>42 =</td>\n<td>Permis d'importation des produits biologiques v\u00e9t\u00e9rinaires</td>\n<td class=\"nowrap\"><abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr></td>\n</tr>\n<tr>\n<td>43 =</td>\n<td>Permis d'importation d'agents pathog\u00e8nes</td>\n<td class=\"nowrap\"><abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr></td>\n</tr>\n</tbody>\n</table>\n<h2>Motifs de rejet</h2>\n<p>Ceci pr\u00e9sente les motifs de rejet de la validation d'un permis d'importation.</p>\n<ul>\n<li class=\"mrgn-bttm-md\">Format invalide = <strong>Num\u00e9ro d'enregistrement <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> Invalide</strong>. Le <strong>Num\u00e9ro</strong> du permis d'importation doit \u00eatre inscrit suivant le format <abbr title=\"Animal\">A</abbr>-<abbr title=\"Ann\u00e9e de d\u00e9livrance\">YYYY</abbr>-<abbr title=\"num\u00e9ro unique de cinq chiffres\">NNNNN</abbr>-<abbr title=\"num\u00e9ro de r\u00e9gion\">n</abbr></li>\n<li class=\"mrgn-bttm-md\">Num\u00e9ro de permis invalide = <strong>Num\u00e9ro d'enregistrement <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> Invalide</strong>. Le <strong>Num\u00e9ro</strong> du permis d'importation <strong>n'est pas</strong> valide</li>\n<li class=\"mrgn-bttm-md\">Permis invalide = <strong>Num\u00e9ro d'enregistrement <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> Invalide</strong>. Le permis d'importation <strong>n'est pas</strong> valide</li>\n</ul>\n<p>Pour obtenir des renseignements suppl\u00e9mentaires sur les motifs de rejet ou sur d'autres questions concernant l'<abbr title=\"\u00c9change de donn\u00e9e informatis\u00e9\">\u00c9DI</abbr>, veuillez communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049?EDI-CFIA-ACIA\">coordonnateur de l'<abbr title=\"\u00c9change de donn\u00e9e informatis\u00e9\">\u00c9DI</abbr> \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> par courriel, ou le service d'assistance t\u00e9l\u00e9phonique de l'<abbr title=\"\u00c9change de donn\u00e9e informatis\u00e9\">\u00c9DI</abbr> au 613-773-5322.</p>\n<p class=\"small text-right\">Publi\u00e9 initialement le 14 f\u00e9vrier 2011 (Avis \u00e0 l'industrie)</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}