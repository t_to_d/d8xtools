{
    "dcr_id": "1698411656270",
    "title": {
        "en": "Canadian African Swine Fever Compartment Program",
        "fr": "Programme canadien de compartimentation de la peste porcine africaine"
    },
    "html_modified": "30-10-2023",
    "modified": "30-10-2023",
    "issued": "27-10-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/canadian_asf_compartment_program_1698411656270_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/canadian_asf_compartment_program_1698411656270_fra"
    },
    "parent_ia_id": "1556041718185",
    "ia_id": "1698411656879",
    "parent_node_id": "1556041718185",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Canadian African Swine Fever Compartment Program",
        "fr": "Programme canadien de compartimentation de la peste porcine africaine"
    },
    "label": {
        "en": "Canadian African Swine Fever Compartment Program",
        "fr": "Programme canadien de compartimentation de la peste porcine africaine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1698411656879",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1556041717935",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/producers/compartment-program/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/producteurs/programme-de-compartimentation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Canadian African Swine Fever Compartment Program",
            "fr": "Programme canadien de compartimentation de la peste porcine africaine"
        },
        "description": {
            "en": "Get informed on Canada\u2019s African swine fever compartment program. Learn about roles, responsibilities, requirements and how to apply.",
            "fr": "formez-vous sur le programme canadien du compartiment de la peste porcine africaine. D\u00e9couvrez les r\u00f4les, les responsabilit\u00e9s, les exigences et comment postuler."
        },
        "keywords": {
            "en": "National Compartmentalization Program, ASF Compartment, ASF, African swine fever, animal disease management, zoning, international pork trade, Reportable Diseases Regulations, pig disease, viral disease swine, reportable disease, Government of Canada, producer, on-farm biosecurity",
            "fr": "R\u00e9glementation relative aux maladies; d\u00e9claration obligatoire, Peste porcine africaine, Porc africain, maladie virale contagieuse du porc, maladie; d\u00e9claration obligatoire, gouvernement du Canada, producteur, bios\u00e9curit\u00e9 sur la ferme, aliments du porc"
        },
        "dcterms.subject": {
            "en": "animal,inspection,animal inspection,animal diseases,animal health",
            "fr": "animal,inspection,inspection des animaux,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-10-30",
            "fr": "2023-10-30"
        },
        "modified": {
            "en": "2023-10-30",
            "fr": "2023-10-30"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Canadian African Swine Fever Compartment Program",
        "fr": "Programme canadien de compartimentation de la peste porcine africaine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian African Swine Fever (ASF) Compartment Program is a disease management approach to support trade of live pigs, pork and pork products when the disease is present in Canada. The Canadian ASF Compartment Program is a shared responsibility between the Canadian Food Inspection Agency (CFIA), provincial and territorial governments, the Canadian Pork Council (CPC) and hog producers.</p>\n<p>The CFIA sets the National Standards and Framework for ASF compartments in Canada. The CPC is responsible for the ASF Compartment Operator Program (COP), strictly based on the Standards and Framework, and ensuring pork producers meet the COP requirements. The CFIA approves and oversees the COP.</p>\n<p>The requirements for the Canadian ASF Compartment Program are detailed in the National Standards, the National Framework and the COP.</p>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/producers/national-standards/eng/1697822805041/1697822901053\">Canada's National Standards for African Swine Fever Compartments</a> Version 1 updated <span class=\"nowrap\">2023-10-25</span></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/producers/national-framework/eng/1697821665312/1697821848916\">Canada's National Framework for African Swine Fever Compartments</a> Version 1 updated <span class=\"nowrap\">2023-10-25</span></li>\n<li>Compartment Operator Program (in development)</li>\n</ul>\n<div class=\"alert alert-info\">\n<p>Organizations using the National Standards and National Framework for the Canadian ASF Compartment Program are responsible for ensuring that they have the most current version available. They are reviewed annually, at a minimum</p>\n</div>\n<p>More resources on the Canadian ASF Compartment Program, including information on how to apply, are available from the <a href=\"https://www.cpc-ccp.com/compartments\">Canadian Pork Council</a>.</p>\n<h2>What is compartmentalization</h2>\n<p>Compartmentalization is a disease management approach to create subpopulations (compartments) of pigs within the larger pig population. In accordance with international guidelines for the control of animal health by the World Organisation for Animal Health (WOAH), an ASF compartment is a subpopulation\u00a0of pigs contained in 1 or more\u00a0establishments, separated from other susceptible\u00a0pig populations\u00a0by a common\u00a0biosecurity\u00a0management system, whose health status for ASF is distinct from the rest of the pig population in the country or a zone. The compartment demonstrates that facilities and premises adhere to stringent biosecurity measures, regular surveillance, and traceability protocols to create a secure and isolated production system for the purpose of international trade as well as disease prevention and control in a country or zone.</p>\n<p>More information on compartmentalization can be found on WOAH website under the <a href=\"https://www.woah.org/en/what-we-do/standards/codes-and-manuals/terrestrial-code-online-access/?id=169&amp;L=1&amp;htmfile=chapitre_application_compartment.htm\">Application of Compartmentalisation</a>.</p>\n<h2>How compartmentalization works</h2>\n<p>Compartments are developed and managed by industry with approval and oversight by the CFIA. They are an effective tool in preventing ASF from entering production systems. Approved compartments have more flexibility in moving live pigs and pork products even within an infected zone and may be subject to less restrictions. This is because the compartment is designed to stop ASF from entering since they are applying risk reduction measures at all times.</p>\n<p>In order for a compartment to support trade, it must be established and fully operational before an outbreak of ASF. It must be formally recognized by the Canadian Food Inspection Agency (CFIA) and the country importing the pigs or pork products.</p>\n<h2>Compartmentalization versus zoning</h2>\n<p>Compartmentalization creates a separate subpopulation of animals by common management and biosecurity measurements while zones are based on geographical boundaries. A country may have more than 1 zone or compartment within its territory.</p>\n<p>A compartment or part of a compartment can be present within an infected zone. It is the biosecurity and surveillance practices of the compartment that maintain the ASF-free status of the pigs within the compartment, not the location of the premises. This makes continued trade of pigs and pork products from a compartment free of ASF theoretically possible even when a premises is located within an infected zone.</p>\n<p>Both approaches aim to prevent and manage disease but use different criteria and methods.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le Programme canadien de compartimentation de la peste porcine africaine (PPA) est une strat\u00e9gie de gestion de la maladie visant \u00e0 faciliter le commerce de porcs vivants, de porc et de produits du porc en cas de pr\u00e9sence de la maladie au Canada. Le Programme canadien de compartimentation de la PPA est une responsabilit\u00e9 partag\u00e9e entre l'Agence canadienne d'inspection des aliments (ACIA), les gouvernements provinciaux et territoriaux, le Conseil canadien du porc (CCP) et les producteurs de porcs.</p>\n<p>L'ACIA \u00e9tablit les normes nationales et le cadre aux fins de la compartimentation de la PPA au Canada. Le CCP est responsable du Programme des op\u00e9rateur de compartiments (POC) de la PPA, qui est rigoureusement fond\u00e9 sur les normes et le cadre; il doit aussi veiller \u00e0 ce que les producteurs de porcs respectent les exigences du POC. L'ACIA approuve et supervise le POC.</p>\n<p>Les exigences relatives au Programme canadien de compartimentation de la PPA sont \u00e9nonc\u00e9es en d\u00e9tail dans les normes nationales, le cadre national et le POC. </p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/producteurs/normes-nationales/fra/1697822805041/1697822901053\">Normes nationales du Canada concernant la compartimentation de la peste porcine africaine</a> Version 1 mise \u00e0 jour <span class=\"nowrap\">le 25-10-2023</span></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/producteurs/cadre-national/fra/1697821665312/1697821848916\">Cadre national du Canada concernant la compartimentation de la peste porcine africaine</a> Version 1 mise \u00e0 jour <span class=\"nowrap\">le 25-10-2023</span></li>\n<li>Programme op\u00e9rationnel de compartimentation (en cours d'\u00e9laboration)</li>\n</ul>\n<div class=\"alert alert-info\">\n<p>Les organisations qui suivent les normes nationales et le cadre national se rattachant au Programme canadien de compartimentation de la PPA sont tenues de veiller \u00e0 ce qu'elles utilisent la version la plus r\u00e9cente en vigueur. Ces documents sont revus au moins une fois par ann\u00e9e.</p>\n</div>\n \n<p>D'autres ressources sur le Programme canadien de compartimentation de la PPA, y compris la marche \u00e0 suivre pour pr\u00e9senter une demande, se trouvent sur <a href=\"https://www.cpc-ccp.com/francais/compartments\">le site Web du Conseil canadien du porc</a>.</p>\n\n<h2>En quoi consiste la compartimentation</h2>\n<p>La compartimentation est une strat\u00e9gie de gestion des maladies permettant de cr\u00e9er des sous-populations (<em>compartiments</em>) de porcs \u00e0 l'int\u00e9rieur d'une plus grande population de porcs. Conform\u00e9ment aux lignes directrices internationales sur le contr\u00f4le de la sant\u00e9 animale par l'Organisation mondiale de la sant\u00e9 animale (OMSA), un compartiment de la PPA est une sous-population de porcs se trouvant dans 1 ou plusieurs \u00e9tablissements, s\u00e9par\u00e9s d'autres populations de porcs vuln\u00e9rables par un syst\u00e8me de gestion de la bios\u00e9curit\u00e9 commun, dont le statut sanitaire \u00e0 l'\u00e9gard de la PPA est distinct du reste de la population de porcs d'un pays ou d'une zone. Le compartiment garantit que les installations et les \u00e9tablissements respectent des mesures de bios\u00e9curit\u00e9 rigoureuses, font r\u00e9guli\u00e8rement l'objet d'une surveillance et suivent des protocoles de tra\u00e7abilit\u00e9 pour cr\u00e9er un syst\u00e8me de production s\u00e9curitaire et isol\u00e9 aux fins du commerce international ainsi que pour emp\u00eacher et contr\u00f4ler la propagation d'une maladie dans un pays ou une zone.</p>\n<p>Pour en savoir plus sur la compartimentation, consulter le site Web de l'OMSA sous la rubrique <a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/codes-et-manuels/acces-en-ligne-au-code-terrestre/?id=169&amp;L=1&amp;htmfile=chapitre_application_compartment.htm\">Application de la compartimentation</a>.</p>\n\n<h2>Comment fonctionne la compartimentation</h2>\n<p>Les compartiments sont cr\u00e9\u00e9s et g\u00e9r\u00e9s par l'industrie, avec l'approbation et sous la supervision de l'ACIA. Il s'agit d'un outil efficace pour emp\u00eacher la PPA de s'introduire dans les syst\u00e8mes de production. Des compartiments approuv\u00e9s donnent une plus grande latitude pour le d\u00e9placement de porcs vivants et de produits du porc m\u00eame \u00e0 l'int\u00e9rieur d'une zone infect\u00e9e, et les restrictions peuvent \u00eatre moindres. En effet, le compartiment est con\u00e7u pour emp\u00eacher l'introduction de la PPA \u00e9tant donn\u00e9 que des mesures r\u00e9duisant les risques sont appliqu\u00e9es en tout temps.</p>\n<p>Pour qu'un compartiment puisse soutenir le commerce, il doit \u00eatre \u00e9tabli et enti\u00e8rement op\u00e9rationnel avant une \u00e9closion de PPA. Il doit \u00eatre reconnu officiellement par l'Agence canadienne d'inspection des aliments (ACIA) et par le pays qui importe les porcs ou les produits du porc.</p>\n\n<h2>Diff\u00e9rence entre \u00ab\u00a0compartimentation\u00a0\u00bb et \u00ab\u00a0zonage\u00a0\u00bb</h2>\n<p>La compartimentation cr\u00e9e une sous-population d'animaux distincte assujettie \u00e0 des mesures de gestion et de bios\u00e9curit\u00e9 communes, alors que les zones sont fond\u00e9es sur des fronti\u00e8res g\u00e9ographiques. Un pays peut compter sur son territoire plus de 1 zone ou plus d'un compartiment.</p>\n<p>Un compartiment ou une partie d'un compartiment peut se trouver \u00e0 l'int\u00e9rieur d'une zone infect\u00e9e. Ce sont les pratiques de bios\u00e9curit\u00e9 et de surveillance li\u00e9es \u00e0 un compartiment qui garantissent que les porcs demeurent exempts de la PPA au sein du compartiment, et non l'emplacement de l'\u00e9tablissement. Le commerce de porcs et de produits du porc provenant d'un compartiment exempt de la PPA peut ainsi se poursuivre th\u00e9oriquement m\u00eame si l'\u00e9tablissement se trouve \u00e0 l'int\u00e9rieur d'une zone infect\u00e9e.</p>\n<p>Les 2 approches visent \u00e0 emp\u00eacher et \u00e0 contr\u00f4ler la propagation d'une maladie, mais selon des crit\u00e8res et m\u00e9thodes qui diff\u00e8rent.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}