{
    "dcr_id": "1321316984321",
    "title": {
        "en": "Export of dogs and cats to Mexico",
        "fr": "Exportation de chiens et de chats vers le Mexique"
    },
    "html_modified": "2011-11-14 19:29",
    "modified": "12-3-2021",
    "issued": "14-11-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/terr_anima_export_certif_dogs_cats_mexico_1321316984321_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/terr_anima_export_certif_dogs_cats_mexico_1321316984321_fra"
    },
    "parent_ia_id": "1321281361100",
    "ia_id": "1321317230018",
    "parent_node_id": "1321281361100",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Export of dogs and cats to Mexico",
        "fr": "Exportation de chiens et de chats vers le Mexique"
    },
    "label": {
        "en": "Export of dogs and cats to Mexico",
        "fr": "Exportation de chiens et de chats vers le Mexique"
    },
    "templatetype": "content page 1 column",
    "node_id": "1321317230018",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1321265624789",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/pets/mexico/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/animaux-de-compagnie/mexique/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Export of dogs and cats to Mexico",
            "fr": "Exportation de chiens et de chats vers le Mexique"
        },
        "description": {
            "en": "The import requirements and procedures for pet dog(s) and cat(s) to Mexico from Canada have changed.",
            "fr": "Les exigences et proc\u00e9dures d'importation des chiens et des chats au Mexique en provenance du Canada ont chang\u00e9."
        },
        "keywords": {
            "en": "veterinary health certificate, health certificate, General Health Certificate, CFIA District Office, CFIA veterinarian",
            "fr": "certificat sanitaire v\u00e9t\u00e9rinaire, certificat sanitaire v\u00e9t\u00e9rinaire, certificat sanitaire canadien international, bureau de district de l'ACIA, v\u00e9t\u00e9rinaire officiel de l'ACIA"
        },
        "dcterms.subject": {
            "en": "animal health,exports,inspection,travel",
            "fr": "sant\u00e9 animale,exportation,inspection,voyage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-11-14 19:29:47",
            "fr": "2011-11-14 19:29:47"
        },
        "modified": {
            "en": "2021-03-12",
            "fr": "2021-03-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Export of dogs and cats to Mexico",
        "fr": "Exportation de chiens et de chats vers le Mexique"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The import requirements and procedures for pet dog(s) and cat(s) to Mexico from Canada have changed. If you would like to export a pet dog or cat to Mexico, you must comply with all of the requirements outlined on the <a href=\"https://sistemasssl.senasica.gob.mx/mcrz/moduloConsulta.jsf\">National Service of Agrifood Health, Safety and Quality (SENASICA) (Spanish only)</a> website. Although the site is in Spanish, all of the necessary requirements are available and it is the responsibility of the importer/owner to confirm individual country requirements prior to travel. The import requirements and procedures are outlined below for reference.</p>\n\n<p><strong>A health certificate for dogs and cats from Canada is no longer needed to enter Mexico.</strong> Note: If transiting another country, such as the United States, it is the responsibility of the importer/owner to confirm import requirements for the other countries.</p>\n\n<p>Upon entering Mexico, the pet dog(s) or cat(s) must be presented to the Agricultural Health Inspection Office, <i lang=\"es\">la Oficina de Inspecci\u00f3n de Sanidad Agropecuaria (OISA)</i>, where a physical inspection of the dog(s)/cat(s) will be conducted by an employee of SENASICA. The objective of the physical examination is to determine that the dog(s) and/or cat(s) is/are:</p>\n\n<ol class=\"mrgn-lft-xl\">\n<li>free of signs of an infectious and contagious disease(s),</li>\n<li>free of ectoparasites (that is, external parasites), and</li>\n<li>free of rashes, open wounds or wounds in a healing stage.</li>\n</ol>\n\n<ul class=\"lst-spcd\">\n<li>The dog(s)/cat(s) should enter Mexico in a cage/carrier which is clean and without bedding or accessories (toys, treats, or other objects). If on inspection, the cage/carrier is found to be dirty and/or contains bedding (for example, newspapers, rags, diaper(s), wood, or other materials), toys, and/or edible products, the cage will require disinfection and items in the cage/carrier will be removed and destroyed.</li>\n<li>Only enough pet food for the day of arrival is permitted.</li>\n<li>If a tick(s) is/are detected during the physical examination, the SENASICA inspector will take a sample of the ectoparasite(s). The sample will be submitted to the official laboratory to confirm identity. Any analysis, tests, and/or treatments will be performed at the importer/owner's expense. The animal(s) will remain at OISA until confirmation is received that the ectoparasite(s) are not foreign to Mexico and until SENASICA verifies that all parasites have been removed from the pet(s). If the parasite is identified as foreign or a new species to Mexico SENASICA, through the Animal Health Directorate General, will determine the measures to be applied.</li>\n<li>If your pet is being treated for lesions and/or infections of the skin (for example, sarcoptic mange, dermatomycosis, dermatophylosis, alopecia or similar conditions), the importer/owner should present SENASICA with a letter from a licensed veterinarian with the diagnosis and treatment. The letter should be on official letterhead with the license number, or equivalent, of the veterinarian.</li>\n<li>If any open or healing wounds, rashes, external parasites or infectious diseases are detected, the importer/owner will be required to contact a veterinarian of their choice to apply/administer an effective treatment. Any analysis, tests or treatments will be performed at the importer/owner's expense.</li>\n\n</ul>\n<p>The import requirements to enter Mexico directly from Canada or if they transit the USA prior to entry into Mexico are the same. Compliance with the conditions outlined above does not exempt the importer/owner from submitting documents, and/or complying with procedures required by other authorities (for example, if transiting another country, such as the United States, the owner/importer is required to meet any conditions of import for dog(s) and cat(s) to USA from Canada).</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les exigences et proc\u00e9dures en mati\u00e8re d'importation de chiens et de chats domestiques en provenance du Canada et \u00e0 destination du Mexique ont \u00e9t\u00e9 modifi\u00e9es. Si vous souhaitez exporter un chien ou un chat domestique au Mexique, vous devez satisfaire \u00e0 toutes les exigences d\u00e9crites sur le <a href=\"https://sistemasssl.senasica.gob.mx/mcrz/moduloConsulta.jsf\">site Web du Service national de sant\u00e9, de s\u00e9curit\u00e9 et de qualit\u00e9 agroalimentaire (SENASICA) (en espagnol seulement)</a>. Bien que le site soit en espagnol, toutes les exigences n\u00e9cessaires y sont d\u00e9crites, et il incombe \u00e0 l'importateur ou au propri\u00e9taire de s'enqu\u00e9rir des exigences en vigueur dans le pays consid\u00e9r\u00e9 avant le voyage. Les exigences et proc\u00e9dures en mati\u00e8re d'importation sont d\u00e9crites ci-dessous aux fins de r\u00e9f\u00e9rence.</p>\n\n<p><strong>Un certificat sanitaire pour les chiens et les chats en provenance du Canada n'est plus n\u00e9cessaire pour une entr\u00e9e au Mexique.</strong> Remarque\u00a0: en cas de transit par un autre pays, comme les \u00c9tats-Unis, il incombe \u00e0 l'importateur ou au propri\u00e9taire de s'enqu\u00e9rir des exigences d'importation du pays concern\u00e9.</p>\n\n<p>\u00c0 l'entr\u00e9e au Mexique, tout chien ou tout chat domestique doit \u00eatre pr\u00e9sent\u00e9 au Bureau d'inspection sanitaire agricole, <i lang=\"es\">la Oficina de Inspecci\u00f3n de Sanidad Agropecuaria (OISA)</i>, o\u00f9 une inspection physique du chien ou du chat sera r\u00e9alis\u00e9e par un employ\u00e9 du SENASICA. L'objectif de l'examen physique est de d\u00e9terminer si le chien et/ou le chat sont\u00a0:</p>\n\n<ol class=\"mrgn-lft-xl\">\n<li>exempts de signes de toute maladie infectieuse et contagieuse,</li>\n<li>exempts de tout ectoparasite (c'est \u00e0 dire parasites externes), et</li>\n<li>exempts de toute \u00e9ruption cutan\u00e9e, de blessure ouverte ou de blessure en cours de cicatrisation.</li>\n</ol>\n\n<ul class=\"lst-spcd\">\n<li>Tout chien ou tout chat doit arriver au Mexique dans une cage ou un panier de transport propre et sans liti\u00e8re ou accessoires (jouets, g\u00e2teries ou autres objets). Si la cage ou le panier de transport s'av\u00e8re sale et/ou contient de la liti\u00e8re (par exemple, journaux, chiffons, couches, morceaux de bois ou autres mat\u00e9riaux), des jouets et/ou des produits comestibles lors de l'inspection, la cage devra \u00eatre d\u00e9sinfect\u00e9e et les objets pr\u00e9sents dans la cage ou le panier de transport seront confisqu\u00e9s et d\u00e9truits.</li>\n<li>Seule une quantit\u00e9 suffisante d'aliments pour animaux de compagnie pour le jour de leur arriv\u00e9e est autoris\u00e9e.</li>\n<li>Si au moins une tique est d\u00e9tect\u00e9e lors de l'examen physique, l'inspecteur du SENASICA pr\u00e9l\u00e8vera un \u00e9chantillon de l'ectoparasite. L'\u00e9chantillon sera soumis au laboratoire officiel pour en confirmer la nature. Toute analyse, tout test et/ou tout traitement seront effectu\u00e9s aux frais de l'importateur ou du propri\u00e9taire. L'animal sera gard\u00e9 dans les locaux de l'OISA jusqu'\u00e0 ce qu'il soit confirm\u00e9 que l'ectoparasite n'est pas \u00e9tranger au Mexique et jusqu'\u00e0 ce que le SENASICA v\u00e9rifie que tous les parasites ont \u00e9t\u00e9 \u00e9limin\u00e9s de l'animal domestique. Si le parasite est identifi\u00e9 comme appartenant \u00e0 une esp\u00e8ce \u00e9trang\u00e8re ou nouvelle au Mexique, le SENASICA, d\u00e9terminera les mesures \u00e0 appliquer par l'interm\u00e9diaire de la Direction g\u00e9n\u00e9rale de la sant\u00e9 des animaux.</li>\n<li>Si votre animal domestique est trait\u00e9 pour des l\u00e9sions et/ou des infections de la peau (par exemple, gale sarcoptique, dermatomycose, dermatophilose, alop\u00e9cie ou pathologies similaires), l'importateur ou le propri\u00e9taire doit pr\u00e9senter au SENASICA un document d\u00e9livr\u00e9 par un v\u00e9t\u00e9rinaire agr\u00e9\u00e9 indiquant le diagnostic et le traitement. Le document doit \u00eatre r\u00e9dig\u00e9 sur papier \u00e0 en-t\u00eate officiel faisant appara\u00eetre le num\u00e9ro de licence, ou \u00e9quivalent, du v\u00e9t\u00e9rinaire.</li>\n<li>Si des blessures ouvertes ou en cours de cicatrisation, des \u00e9ruptions cutan\u00e9es, des parasites externes ou des maladies infectieuses sont d\u00e9tect\u00e9s, l'importateur ou le propri\u00e9taire devra contacter un v\u00e9t\u00e9rinaire de son choix aux fins d'application ou d'administration d'un traitement efficace. Toute analyse, tout test ou tout traitement sera effectu\u00e9 aux frais de l'importateur ou du propri\u00e9taire.</li>\n</ul>\n<p>Les exigences en mati\u00e8re d'importation aux fins d'entr\u00e9e au Mexique en provenance directe du Canada ou en cas de transit par les \u00c9tats-Unis avant l'entr\u00e9e au Mexique sont les m\u00eames. Le respect des conditions d\u00e9crites ci-dessus ne dispense pas l'importateur ou le propri\u00e9taire de soumettre des documents et/ou de se conformer aux proc\u00e9dures requises par d'autres autorit\u00e9s (par exemple, en cas de transit par un autre pays, comme les \u00c9tats-Unis, le propri\u00e9taire ou l'importateur est tenu de respecter les conditions \u00e0 l'importation concernant tout chien et tout chat en provenance du Canada et \u00e0 destination des \u00c9tats-Unis).</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}