{
    "dcr_id": "1405360036042",
    "title": {
        "en": "Weed Seed: Blessed milkthistle/Milk thistle (<i lang=\"la\">Silybum marianum</i>)",
        "fr": "Semence de mauvaises herbe : Chardon Marie (<i lang=\"la\">Silybum marianum</i>)"
    },
    "html_modified": "2024-03-12 3:56:47 PM",
    "modified": "2014-11-06",
    "issued": "2014-11-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_silybum_marianum_1405360036042_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_silybum_marianum_1405360036042_fra"
    },
    "ia_id": "1405360037120",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: Blessed milkthistle/Milk thistle (<i lang=\"la\">Silybum marianum</i>)",
        "fr": "Semence de mauvaises herbe : Chardon Marie (<i lang=\"la\">Silybum marianum</i>)"
    },
    "label": {
        "en": "Weed Seed: Blessed milkthistle/Milk thistle (<i lang=\"la\">Silybum marianum</i>)",
        "fr": "Semence de mauvaises herbe : Chardon Marie (<i lang=\"la\">Silybum marianum</i>)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1405360037120",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/silybum-marianum/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/silybum-marianum/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Blessed milkthistle/Milk thistle (Silybum marianum)",
            "fr": "Semence de mauvaises herbe : Chardon Marie (Silybum marianum)"
        },
        "description": {
            "en": "Fact sheet for Blessed milkthistle/Milk thistle (Silybum marianum)",
            "fr": "Fiche de renseignements pour Chardon Marie (Silybum marianum)"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Blessed milkthistle, Milk thistle, Silybum marianum",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Chardon Marie, Silybum marianum"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,weeds,plants",
            "fr": "cultures,grain,inspection,plante nuisible,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-11-06",
            "fr": "2014-11-06"
        },
        "modified": {
            "en": "2014-11-06",
            "fr": "2014-11-06"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Blessed milkthistle/Milk thistle (Silybum marianum)",
        "fr": "Semence de mauvaises herbe : Chardon Marie (Silybum marianum)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Distribution in Canada</h2>\n<p>Found in <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Alberta\">AB</abbr>, <abbr title=\"Manitoba\">MB</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Nova Scotia\">NS</abbr></p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual or biennial</p>\n\n<h2>Seed/Fruit type</h2>\n<p>Achene - a one-seeded, dry fruit</p>\n\n<h2>Seed identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>length: 6.5\u20137.5 <abbr title=\"millimetres\">mm</abbr></li> \n<li>width: 2.0\u20132.5 <abbr title=\"millimetres\">mm</abbr></li> \n<li>thickness: 1.7 <abbr title=\"millimetre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>in outline: oblong (oval with straight sides)</li>\n<li>in cross-section: slightly curved, oval</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>smooth with a sheen</li>\n<li>slight cross-wise wrinkles under magnification</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>overall dark brown, with occasional golden streaks</li>\n<li>golden-coloured ring at top of seed</li>\n<li>middle of seed can be greenish-brown</li>\n<li>occasionally, seeds have large, light-brown patches</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>achene has two ends \n<ul>\n<li>top is a lobed peg, ringed by thin rim (a remnant of the floral parts)</li> \n<li>base is a small slit (seen only under magnification)</li>\n</ul></li> \n<li>a long white pappus may be present on freshly harvested seeds</li>\n</ul>\n\n<h2>Similar species</h2>\n<p>Nodding thistle (<i lang=\"la\">Carduus nutans</i>)</p>\n<ul>\n<li>found across Canada (except in <abbr title=\"Nunavut\">NU</abbr>, <abbr title=\"Yukon\">YT</abbr>, <abbr title=\"Northwest Territories\">NT</abbr>) and the United States (except in Florida)</li> \n<li>one-half the length and width of blessed milkthistle</li>\n<li>similar to blessed milkthistle in shape, satiny surface texture, and the presence of prominent peg at top of achene</li> \n<li>top peg is not lobed, and is a lighter colour than blessed milkthistle</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img1_1405428267961_eng.jpg\" alt=\"Photo - Blessed milkthistle (Silybum marianum) achene (side view)\" class=\"img-responsive\">\n<figcaption>Blessed milkthistle (<i lang=\"la\">Silybum marianum</i>) achene (side view)</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img2_1405428301461_eng.jpg\" alt=\"Photo - Blessed milkthistle (Silybum marianum) achene (edge view)\" class=\"img-responsive\">\n<figcaption>Blessed milkthistle (<i lang=\"la\">Silybum marianum</i>) achene (edge view)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img3_1405428352102_eng.jpg\" alt=\"Photo - Blessed milkthistle (Silybum marianum) achene (close-up of peg at top)\" class=\"img-responsive\">\n<figcaption>Blessed milkthistle (<i lang=\"la\">Silybum marianum</i>) achene (close-up of peg at top)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img4_1405428377446_eng.jpg\" alt=\"Photo - Blessed milkthistle (Silybum marianum) achene (top-down view)\" class=\"img-responsive\">\n<figcaption>Blessed milkthistle (<i lang=\"la\">Silybum marianum</i>) achene (top-down view)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img5_1405428400664_eng.jpg\" alt=\"Photo - Blessed milkthistle (Silybum marianum) group of achenes\" class=\"img-responsive\">\n<figcaption>Blessed milkthistle (<i lang=\"la\">Silybum marianum</i>) group of achenes\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_carduus_nutans_img1_1405428448946_eng.jpg\" alt=\"Photo - Similar species: Nodding thistle (Carduus nutans) achene (side view)\" class=\"img-responsive\">\n<figcaption>Similar species: Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achene (side view) </figcaption>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p>Ast\u00e9rac\u00e9es</p>\n\n<h2>R\u00e9partition au Canada</h2>\n<p><abbr title=\"Colombie-Britannique\">BC</abbr>, <abbr title=\"Alberta\">AB</abbr>, <abbr title=\"Manitoba\">MB</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Qu\u00e9bec\">QC</abbr>, <abbr title=\"Nouveau-Brunswick\">NB</abbr> et <abbr title=\"Nouvelle-\u00c9cosse\">NS</abbr></p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle ou bisannuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne - fruit sec renfermant une seule graine</p>\n\n<h2>Caract\u00e8res servant \u00e0 l'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>longueur\u00a0: 6,5\u20137,5 <abbr title=\"millim\u00e8tres\">mm</abbr></li> \n<li>largeur\u00a0: 2,0\u20132,5 <abbr title=\"millim\u00e8tres\">mm</abbr></li> \n<li>\u00e9paisseur\u00a0: 1,7 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>contour\u00a0: oblong (elliptique, mais avec les c\u00f4t\u00e9s droits)</li>\n<li>coupe transversale\u00a0: l\u00e9g\u00e8rement courb\u00e9e, elliptique</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>lisse, lustr\u00e9e</li>\n<li>avec l\u00e9g\u00e8res rides transversales visibles sous grossissement</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>globalement brun fonc\u00e9, avec quelques stries dor\u00e9es</li>\n<li>anneau dor\u00e9 au sommet de l'ak\u00e8ne</li>\n<li>portion m\u00e9diane de l'ak\u00e8ne parfois brun verd\u00e2tre</li>\n<li>grandes taches brun clair parfois pr\u00e9sentes</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>ak\u00e8ne avec extr\u00e9mit\u00e9s bien diff\u00e9rentes\u00a0: \n<ul>\n<li>sommet avec base stylaire lob\u00e9e, entour\u00e9e d'un rebord mince (restes de parties de la fleur)</li> \n<li>base avec petite fente (visible seulement sous grossissement)</li>\n</ul></li> \n<li>long pappus blanc parfois pr\u00e9sent sur l'ak\u00e8ne fra\u00eechement r\u00e9colt\u00e9</li>\n</ul>\n\n<h2>Esp\u00e8ce semblable</h2>\n<p>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>)</p>\n<ul>\n<li>signal\u00e9 partout au Canada (sauf <abbr title=\"Nunavut\">NU</abbr>, <abbr title=\"Yukon\">YT</abbr> et <abbr title=\"Territoires du Nord-Ouest\">NT</abbr>) et aux \u00c9tats-Unis (sauf Floride)</li> \n<li>ak\u00e8ne une demi-fois aussi long et aussi large que celui du chardon Marie</li>\n<li>ak\u00e8ne ressemblant \u00e0 celui du chardon Marie par sa forme, sa surface satin\u00e9e et la pr\u00e9sence d'une base stylaire pro\u00e9minente au sommet de l'ak\u00e8ne</li> \n<li>base stylaire non lob\u00e9e et de couleur l\u00e9g\u00e8rement plus claire que chez le chardon Marie</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img1_1405428267961_fra.jpg\" alt=\"Photo - Chardon Marie (Silybum marianum), ak\u00e8ne (vue de c\u00f4t\u00e9)\" class=\"img-responsive\">\n<figcaption>Chardon Marie (<i lang=\"la\">Silybum marianum</i>), ak\u00e8ne (vue de c\u00f4t\u00e9)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img2_1405428301461_fra.jpg\" alt=\"Photo - Chardon Marie (Silybum marianum), ak\u00e8ne (bord mince)\" class=\"img-responsive\">\n<figcaption>Chardon Marie (<i lang=\"la\">Silybum marianum</i>), ak\u00e8ne (bord mince)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img3_1405428352102_fra.jpg\" alt=\"Photo - Chardon Marie (Silybum marianum), ak\u00e8ne (gros plan du sommet, avec base stylaire)\" class=\"img-responsive\">\n<figcaption>Chardon Marie (<i lang=\"la\">Silybum marianum</i>), ak\u00e8ne (gros plan du sommet, avec base stylaire)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img4_1405428377446_fra.jpg\" alt=\"Photo - Chardon Marie (Silybum marianum), ak\u00e8ne (vue du dessus)\" class=\"img-responsive\">\n<figcaption>Chardon Marie (<i lang=\"la\">Silybum marianum</i>), ak\u00e8ne (vue du dessus)\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_silybum_marianum_img5_1405428400664_fra.jpg\" alt=\"Photo - Chardon Marie (Silybum marianum), ak\u00e8nes\" class=\"img-responsive\">\n<figcaption>Chardon Marie (<i lang=\"la\">Silybum marianum</i>), ak\u00e8nes\n</figcaption>\n</figure>\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_carduus_nutans_img1_1405428448946_fra.jpg\" alt=\"Photo - Esp\u00e8ce semblable\u00a0: Chardon pench\u00e9 (Carduus nutans), ak\u00e8ne (vue de c\u00f4t\u00e9)\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>), ak\u00e8ne (vue de c\u00f4t\u00e9)</figcaption>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}