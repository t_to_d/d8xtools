{
    "dcr_id": "1332441513911",
    "title": {
        "en": "Aquatic animal export: certification requirements for Malaysia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour la Malaisie"
    },
    "html_modified": "2024-03-12 3:55:41 PM",
    "modified": "2020-01-07",
    "issued": "2012-03-22 14:38:35",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_malay_1332441513911_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_malay_1332441513911_fra"
    },
    "ia_id": "1332441573051",
    "parent_ia_id": "1331743129372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1331743129372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal export: certification requirements for Malaysia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour la Malaisie"
    },
    "label": {
        "en": "Aquatic animal export: certification requirements for Malaysia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour la Malaisie"
    },
    "templatetype": "content page 1 column",
    "node_id": "1332441573051",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331740343115",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/malaysia/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/malaisie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal export: certification requirements for Malaysia",
            "fr": "Exportation d'animaux aquatiques : exigences de certification pour la Malaisie"
        },
        "description": {
            "en": "This page describes aquatic animal health export certification requirements for Malaysia",
            "fr": "Cette page d\u00e9crit les exigences de certification d'exportation de sant\u00e9 des animaux aquatiques pour la Malaisie"
        },
        "keywords": {
            "en": "aquatic animals, finfish, molluscs, crustacean, exports, human consumption, certification requirements, aquatic animal health, seafood products, NAAHP, Malaysia",
            "fr": "animaux aquatiques, poisson \u00e0 nageoires, mollusques, crustac\u00e9s, exportation,  consommation humaine,  exigences de certification, sant\u00e9 des animaux aquatiques, produits du poisson et des produits de la mer, PNSAA, Malaisie"
        },
        "dcterms.subject": {
            "en": "crustaceans,exports,inspection,molluscs,fish,animal health",
            "fr": "crustac\u00e9,exportation,inspection,mollusque,poisson,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux aquatiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-22 14:38:35",
            "fr": "2012-03-22 14:38:35"
        },
        "modified": {
            "en": "2020-01-07",
            "fr": "2020-01-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal export: certification requirements for Malaysia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour la Malaisie"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p><strong>The following certificates are available:</strong></p>\n\n<ul>\n\n<li>Aquatic Animal Health Certificate for the Export of Live Crustaceans for Human Consumption from Canada to Malaysia (AQAH-1050)</li>\n\n<li>Aquatic Animal Health Certificate for the Export of Live Molluscs for Human Consumption from Canada to Malaysia (AQAH-1051)</li>\n\n</ul>\n\n<p><strong>Important Notes:</strong></p>\n\n<ul>\n\n<li>Malaysia has aquatic animal health requirements for imports of both live finfish for human consumption and live ornamental aquatic animals. For these specific commodities, until such time as a negotiated certificate is available, shipments exported without an aquatic animal health certificate may be detained in Malaysia.</li>\n\n<li>There may be existing certificates or additional information  from the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/malaysia-fish-and-seafood/eng/1304261244846/1304261379015\">Fish and Seafood</a> program. However, for the specific commodities above, an aquatic animal  health certificate is required. </li>\n\n<li>To request an export certificate outlined above or if you require any other information on aquatic animal health export certification, please contact your <a href=\"/eng/1300462382369/1365216058692\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a>.</li>\n\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p><strong>Les certificats suivant sont disponible\u00a0:</strong></p>\n<ul>\n<li>Certificat d'hygi\u00e8ne des animaux aquatiques pour l'exportation de crustac\u00e9s vivants destin\u00e9s \u00e0 la consommation humaine du Canada vers la Malaisie (AQAH-1050)</li>\n<li>Certificat d'hygi\u00e8ne des animaux aquatiques pour l'exportation de mollusques vivants destin\u00e9s \u00e0 la consommation humaine du Canada vers la Malaisie (AQAH-1051)</li>\n</ul>\n<p><strong>Remarques importantes\u00a0:</strong></p>\n<ul>\n\n<li>La Malaisie a des normes zoosanitaires r\u00e9gissant tant l'importation de poissons \u00e0 nageoires vivants destin\u00e9s \u00e0 la consommation humaine que l'importation d'animaux vivants destin\u00e9s \u00e0 l'\u00e9levage en aquarium. Par cons\u00e9quent, jusqu'\u00e0 ce qu'un certificat pour ces produits ait \u00e9t\u00e9 n\u00e9goci\u00e9 et soit disponible, les envois de ces animaux aquatiques sans un certificat zoosanitaire pourraient \u00eatre retenus en Malaisie.</li>\n\n\n<li>Il est possible que d'autres certificats ou des renseignements suppl\u00e9mentaires soient disponibles par le biais du programme de <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/malaisie-le-poisson-et-les-produits-de-la-mer/fra/1304261244846/1304261379015\">Poisson et produits de la mer</a>. Cependant, en ce qui a trait aux produits susmentionn\u00e9s, un certificat d'hygi\u00e8ne des animaux aquatiques est requis.</li>\n<li>Pour faire une demande d'un certificat d'exportation indiqu\u00e9 ci-dessus ou pour toute autre demande de renseignements concernant la certification sanitaire des animaux aquatiques destin\u00e9s \u00e0 l'exportation, veuillez communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> de votre Centre op\u00e9rationnel.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}