<?php

global $import_override;
$import_override = TRUE;
include "updaters/UpdateNode.php";

global $argv;
global $export_root;
//global $nid_old_to_new;
//global $nid_new_to_old;
global $site_dir;
global $db;
global $ts_nids;

$site_dir = 'default';
//$nid_old_to_new = array();
//$nid_new_to_old = array();
$ts_nids = array();

$db = NULL;//\Drupal::database();

$myargs = array_slice($argv, 3);

//$updateTypes = array('news'); //Removed 'page' it is now d8 update instead
$updateTypes = array('page');

// Override when testing
// When testing and you just want to test one content type, use the option for the type and it will update only this..

if (empty($myargs)) {
  echo "Syntax: drush scr update_exported_json.php full-path-containing-exports\n";
  echo "OR      drush scr update_exported_json.php \$d8xtools/cfia_export_current/gene-gene page\n";
  echo "When testing and you just want to test one content type, use the option for the type and it will update only this..\n";
  exit;
}

$update_only_option_type = false;
$export_root = array_shift($myargs);
$option = array_shift($myargs);
echo "export_root: $export_root \n";

if (!empty($option)) {
  echo "update option: $option \n";
  if (strlen($option) > 0) {
    $update_only_option_type = true;
    $updateTypes = array($option);
  }
}
chdir($export_root);

// Now update each content type
foreach ($updateTypes as $type) {
  if ($type == $option && $update_only_option_type) {
    update_ctype($type);
  }
  else if (!$update_only_option_type) {
    // Do all types.
    update_ctype($type);
  }
}

function update_ctype($type) {
//  global $nid_old_to_new;
//  global $nid_new_to_old;
  global $db;
  global $ts_nids;

  // retrieve json records in the current working directory
  echo "getcwd()= " . getcwd() . ";\n";
  //echo "chdir('" . $type . "');\n";
  //chdir($type);
  $jsonfiles = array();

  if ($dirh = opendir(".")) {
    while (($entry = readdir($dirh)) !== false) {
      if (!preg_match('/json$/', $entry)) continue;
      $jsonfiles[] = $entry;
      if (strtolower($type) == 'page') {
        echo "json file $entry\n";
      }
    }
    closedir($dirh);
  }

  $cclass = ucfirst($type);
  include "updaters/$cclass.php";
  //include "updaters/News.php";
  //include "updaters/Empl.php";
  //include "updaters/Page.php";

  echo "Importing ".count($jsonfiles)." $type nodes...\n";
  $cnt = 0;
  $cnt_success = 0;
  $cnt_could_not_load = 0;
  foreach ($jsonfiles as $jfile) {
    $dcr_id = null;
    if (preg_match('/(\d+)\.json$/', $jfile, $matches)) {
      $dcr_id = $matches[1];
      echo $matches[1] . '.json' . "\n";
      $dcr_id = ltrim($dcr_id, '0');
    }
    /*if ($dcr_id && in_array($dcr_id, $ts_nids)) {
      echo "Found dcr_id: $dcr_id mapping in D8 \n";
      //continue;
    }*/
    $cnt++;
    //echo "$cnt\n";
    if (($cnt % 100) == 0) {
      echo "$cnt\n";
    }
    $update = new $cclass();
    $update->update($jfile);
    if ($update->success) {
      $cnt_success++;
    }
    if ($update->could_not_load) {
      $cnt_could_not_load++;
    }
  }
  echo "\n";
  echo "$cnt_success $cclass nodes were successfully updated with previously missing fields\n";
  echo "$cnt_could_not_load dcr_id did not resolve to d8nid, unable to load $cnt_could_not_load times.\n";
  echo "$cnt json files were processed\n";
  chdir('..');
}
