{
    "dcr_id": "1614991356713",
    "title": {
        "en": "Meet Olga Pena, scientist and advocate",
        "fr": "Voici Olga Pena, scientifique et d\u00e9fenseure"
    },
    "html_modified": "2024-01-26 2:25:23 PM",
    "modified": "2022-02-03",
    "issued": "2021-03-08",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/c360_meet_olga_pena_1614991356713_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/c360_meet_olga_pena_1614991356713_fra"
    },
    "ia_id": "1614991357698",
    "parent_ia_id": "1565296964476",
    "chronicletopic": {
        "en": "Animal Health|Science and Innovation",
        "fr": "Sant\u00e9 des Animaux|Science et Innovation"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Canadians|Youth",
        "fr": "Canadiens|Jeune"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565296964476",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Meet Olga Pena, scientist and advocate",
        "fr": "Voici Olga Pena, scientifique et d\u00e9fenseure"
    },
    "label": {
        "en": "Meet Olga Pena, scientist and advocate",
        "fr": "Voici Olga Pena, scientifique et d\u00e9fenseure"
    },
    "templatetype": "content page 1 column",
    "node_id": "1614991357698",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565296629434",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/animal-health/meet-olga-pena/",
        "fr": "/inspecter-et-proteger/sante-animale/voici-olga-pena/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Meet Olga Pena, scientist and advocate",
            "fr": "Voici Olga Pena, scientifique et d\u00e9fenseure"
        },
        "description": {
            "en": "My name is Olga Pena. I'm a woman, a scientist, a mom, and a Canadian immigrant. I was born and raised in a small town in the middle of the Andes Cordillera in Colombia.",
            "fr": "Je m\u2019appelle Olga Pena. Je suis une femme, une scientifique, une m\u00e8re et une immigrante canadienne. Je suis n\u00e9e et j\u2019ai grandi dans une petite ville au milieu de la cordill\u00e8re des Andes en Colombie."
        },
        "keywords": {
            "en": "Olga Pena, Canadian Food Inspection Agency, CFIA, science specialist, Canadian immigrant, International Women",
            "fr": "Olga Pena, l\u2019Agence canadienne d\u2019inspection des aliments, ACIA, sp\u00e9cialiste scientifique, Canadian immigrant, International Women"
        },
        "dcterms.subject": {
            "en": "animal health,biology,food,food inspection,sciences,scientific research",
            "fr": "sant\u00e9 animale,biologie,aliment,inspection des aliments,sciences,recherche scientifique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-03-08",
            "fr": "2021-03-08"
        },
        "modified": {
            "en": "2022-02-03",
            "fr": "2022-02-03"
        },
        "type": {
            "en": "news publication",
            "fr": "publication d'information"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Meet Olga Pena, scientist and advocate",
        "fr": "Voici Olga Pena, scientifique et d\u00e9fenseure"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/c360_meet_olga_pena_600X600_1614983988400_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"Meet Olga Pena\">\n</div>\n<p>My name is Olga Pena. I'm a woman, a scientist, a mom, and a Canadian immigrant. I was born and raised in a small town in the middle of the Andes Cordillera in Colombia.</p>\n<p>Despite my family's financial challenges, my parents prioritized my education over many other commodities and their efforts meant that I was able to attend university. My dad used to say that, unlike them (mom and dad), who only got elementary education, supporting us in obtaining a university degree was the best and only inheritance they could give us. </p>\n<h2>Becoming a first-generation graduate</h2>\n<p>Moving from a small town to the capital city, Bogota, at the age of 16 wasn't easy. Adaptation and competition were tough, but despite the challenges, I discovered my passion for science research. I graduated at the top of my class, and became a first-generation graduate with\u00a01 goal: to further pursue a Ph.D. in Microbiology and Immunology. However, achieving this in Colombia was very difficult due to the limited programs and scholarships available. In addition, acceptance into a graduate program overseas without knowing English or having any research experience was also challenging, but I was determined to overcome these obstacles.</p>\n<p>After a few years of learning English and working in research laboratories in Colombia and the United States, my efforts paid off. I was accepted into an excellent Ph.D. program at the University of British Columbia in Vancouver, to do research on sepsis, a life-threating illness caused by a dysregulated immune response to an infection. I was fortunate to complete my doctorate having obtained excellent scholarships, several awards, and a great publication record, while also becoming a mom. I also became the first and only Ph.D. graduate in my family, developing an early diagnostic tool for sepsis. This was a real tribute to my dad, who strongly believed in the impact of education, and who passed away years ago due to this same illness and the absence of an early diagnosis.</p>\n<p>After several years of research and management experience at institutions in Colombia, the United States, Australia and Canada, I became a science specialist at the CFIA. My job included leading international files such as the <a href=\"/science-and-research/science-collaborations/biosafety-level-4-zoonotic-laboratory-network/eng/1597148065020/1597148065380\">Biosafety Level 4 Zoonotic Laboratory Network (BSL4ZNet)</a>, an international network of animal and public health organizations from 5 countries aiming to respond to current and emerging zoonotic pathogens (disease-causing viruses and bacteria from animals) through trusted international partnerships. I joined the CFIA in 2019, through the highly competitive <a href=\"https://www.mitacs.ca/en/program/canadian-science-policy-fellowship\">MITACS Canadian Science Policy Fellowship program</a>. My excitement was evident when I was offered the opportunity to mainly handle a file that combined\u00a02 aspects I deeply enjoy: research in infectious diseases and science diplomacy. Science diplomacy means facilitating international cooperation to achieve scientific goals. As a Canadian citizen, I'm proud to have been a part of this work at the CFIA, contributing my knowledge and skills to enhance Canada's continuing disease preparedness efforts.</p>\n\t\n<h2>Immigrant and International Women in Science Network</h2>\n<p>Although getting here hasn't been an easy or short path, this is usually the path that many immigrant women in science (IWS) experience. Based on these challenges and experiences, together with a fantastic group of IWS like myself, we co-founded the <a href=\"https://iwsnetwork.ca/\">IWS Network</a>, now with\u00a09 chapters across Canada. The aim of the IWS Network is to increase awareness about the challenges experienced by IWS and their role in advancing science and innovation, while promoting equity, diversity and inclusion in Canada. This path has also been\u00a01 of many trials and errors and this is why I'm also conscious of the power of mentorship, as I wish I would have had a bit more guidance in the early stages of my career. As a result, I participate as a mentor through different organizations, such as the <a href=\"https://las.arts.ubc.ca/files/2011/10/3.-ASI-brochure_2011.pdf\">Accessible Science Initiative \u2013 PDF (720\u00a0kb)</a>, to provide support to women and girls in science undergoing similar challenges.</p>\n\t\n<p>As we celebrate International Women's Day, a day to reflect on the progress made on gender equality, let's not forget the many inequalities that still exist for women and girls in science, especially those from under-represented groups. From pay gaps and lack of universal daycare, to limited representation in leadership positions in all science ecosystems, including academia, industry and government. Personally, I hope to combine my science expertise and advocacy efforts to contribute to the development of evidence-informed policy in Canada, and the strengthening of science diplomacy in the world, a world that can offer equal opportunities and a level-playing field for everyone to advance, leaving no one behind. </p>\n\t\n<h2>Learn more</h2>\n<ul>\n<li><a href=\"/science-and-research/eng/1494875229872/1494875261582\">Science and research at CFIA</a></li>\n<li><a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">Our laboratories</a></li>\n<li><a href=\"http://www.science.gc.ca/eic/site/063.nsf/eng/98215.html\">The CFIA leads a new approach to One Health risk assessment for SARS-CoV-2</a> (science.gc.ca)</li>\n<li><a href=\"/eng/1637953775477/1637953776243\">Working with the world's deadliest diseases</a></li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/international-conference-drives-science-forward/eng/1632406330682/1632406562302\">International conference drives science forward in the post-pandemic era</a></li>\n</ul>\n\t\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/c360_meet_olga_pena_600X600_1614983988400_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"Voici Olga Pena\">\n</div>\n<p>Je m'appelle Olga Pena. Je suis une femme, une scientifique, une m\u00e8re et une immigrante canadienne. Je suis n\u00e9e et j'ai grandi dans une petite ville au milieu de la cordill\u00e8re des Andes en Colombie. </p>\n<p>Malgr\u00e9 les difficult\u00e9s financi\u00e8res de ma famille, plut\u00f4t que de se procurer bon nombre d'autres produits, mes parents ont accord\u00e9 la priorit\u00e9 \u00e0 mes \u00e9tudes. Ce sont leurs efforts qui m'ont permis d'aller \u00e0 l'universit\u00e9. Mon p\u00e8re avait l'habitude de dire que, contrairement \u00e0 eux (mes parents), qui n'avaient suivi que des \u00e9tudes au primaire, nous aider \u00e0 obtenir un dipl\u00f4me universitaire \u00e9tait le meilleur et le seul h\u00e9ritage qu'ils pouvaient nous l\u00e9guer.</p>\n<h2>Devenir une dipl\u00f4m\u00e9e de premi\u00e8re g\u00e9n\u00e9ration</h2>\n<p>D\u00e9m\u00e9nager d'une petite ville \u00e0 la capitale, Bogota, \u00e0 l'\u00e2ge de 16 ans n'a pas \u00e9t\u00e9 facile. L'adaptation et la comp\u00e9tition \u00e9taient rudes, mais malgr\u00e9 les d\u00e9fis, j'ai d\u00e9couvert ma passion pour la recherche scientifique. J'ai obtenu mon dipl\u00f4me en tant que premi\u00e8re de ma classe et suis devenue une dipl\u00f4m\u00e9e de premi\u00e8re g\u00e9n\u00e9ration avec 1 seul objectif : obtenir un doctorat en microbiologie et immunologie. Cependant, \u00e9tant donn\u00e9 le nombre limit\u00e9 de programmes et de bourses en Colombie, atteindre cet objectif allait aussi s'av\u00e9rer une t\u00e2che difficile. Qui plus est, \u00eatre admise dans un programme d'\u00e9tudes sup\u00e9rieures \u00e0 l'\u00e9tranger sans conna\u00eetre l'anglais ou poss\u00e9der une exp\u00e9rience de recherche \u00e9tait \u00e9galement difficile, mais j'\u00e9tais d\u00e9termin\u00e9e \u00e0 surmonter ces obstacles.</p>\n<p>Apr\u00e8s avoir pass\u00e9 quelques ann\u00e9es \u00e0 apprendre l'anglais et \u00e0 travailler dans des laboratoires de recherche en Colombie et aux \u00c9tats-Unis, mes efforts ont port\u00e9 fruit. J'ai \u00e9t\u00e9 admise dans un excellent programme de doctorat \u00e0 l'University of British Columbia, \u00e0 Vancouver, pour effectuer des recherches sur la septic\u00e9mie, une maladie mortelle caus\u00e9e par une r\u00e9ponse immunitaire d\u00e9r\u00e9gl\u00e9e \u00e0 l'\u00e9gard d'une infection. J'ai eu la chance de terminer mon doctorat en obtenant d'excellentes bourses, plusieurs prix et un excellent dossier de publication, tout en devenant m\u00e8re. Je suis aussi devenue la premi\u00e8re et la seule personne de ma famille \u00e0 obtenir un doctorat, en mettant au point un outil de diagnostic pr\u00e9coce pour la septic\u00e9mie. C'\u00e9tait un v\u00e9ritable hommage \u00e0 mon p\u00e8re \u2013 qui croyait fermement \u00e0 l'importance des \u00e9tudes \u2013, d\u00e9c\u00e9d\u00e9 il y a des ann\u00e9es \u00e0 cause de cette m\u00eame maladie et parce qu'un diagnostic pr\u00e9coce n'\u00e9tait pas disponible.</p>\n<p>Apr\u00e8s plusieurs ann\u00e9es d'exp\u00e9rience en recherche et en gestion dans des \u00e9tablissements en Colombie, aux \u00c9tats-Unis, en Australie et au Canada, je suis devenue sp\u00e9cialiste des sciences \u00e0 l'Agence canadienne d'inspection des aliments (ACIA). Mon travail touchait, entre autres, \u00e0 des dossiers internationaux de premier plan, comme le <a href=\"/les-sciences-et-les-recherches/collaborations-scientifiques/reseau-de-laboratoires-de-niveau-de-biosecurite-4-/fra/1597148065020/1597148065380\">R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9\u00a04 pour les zoonoses</a>, un r\u00e9seau international d'organismes de sant\u00e9 publique et animale de cinq pays visant \u00e0 r\u00e9pondre aux agents pathog\u00e8nes zoonotiques actuels et \u00e9mergents (virus pathog\u00e8nes et bact\u00e9ries provenant d'animaux) au moyen de partenariats internationaux de confiance. J'ai joint l'ACIA en 2019, par l'entremise du tr\u00e8s comp\u00e9titif <a href=\"https://www.mitacs.ca/fr/program/canadian-science-policy-fellowship\">programme de bourse pour l'\u00e9laboration de politiques scientifiques canadiennes MITACS</a>. Mon enthousiasme \u00e9tait \u00e9vident quand on m'a offert l'occasion de traiter principalement un dossier qui combinait deux aspects qui me plaisaient profond\u00e9ment\u00a0: effectuer des recherches sur les maladies infectieuses et la diplomatie scientifique. La diplomatie scientifique, c'est de faciliter la coop\u00e9ration internationale pour atteindre des objectifs scientifiques. En tant que citoyenne canadienne, je suis fi\u00e8re d'avoir particip\u00e9 \u00e0 ce travail au sein de l'ACIA, ainsi que de mettre \u00e0 profit mes connaissances et mes comp\u00e9tences afin de contribuer \u00e0 am\u00e9liorer les efforts continus du Canada en mati\u00e8re de pr\u00e9paration aux maladies.</p>\n\t\n<h2>R\u00e9seau \u00ab\u00a0<span lang=\"en\">Immigrant and International Women in Science</span>\u00a0\u00bb</h2>\n<p>Mon cheminement n'a \u00e9t\u00e9 ni facile ni court, mais c'est g\u00e9n\u00e9ralement le chemin que beaucoup de femmes immigrantes en sciences ont d\u00fb emprunter. \u00c0 la lumi\u00e8re de ces d\u00e9fis et de ces exp\u00e9riences, ainsi que d'un groupe fantastique de femmes immigrantes en sciences comme moi, nous avons cofond\u00e9 l'<a href=\"https://iwsnetwork.ca/\"><span lang=\"en\">IWS Network</span> (en anglais seulement)</a> (r\u00e9seau des femmes immigrantes en sciences (ce lien est disponible en anglais seulement)), qui compte maintenant\u00a09 chapitres \u00e0 travers le Canada. L'objectif de l'<span lang=\"en\">IWS Network</span> est de sensibiliser davantage les gens aux d\u00e9fis auxquels sont confront\u00e9es les femmes immigrantes en sciences et \u00e0 leur r\u00f4le dans la promotion de la science et de l'innovation, tout en favorisant l'\u00e9quit\u00e9, la diversit\u00e9 et l'inclusion au Canada. Cette voie a \u00e9galement comport\u00e9 de nombreux essais et erreurs, et c'est la raison pour laquelle je suis \u00e9galement consciente du pouvoir du mentorat, car j'aurais aim\u00e9 avoir un peu plus d'encadrement aux premiers stades de ma carri\u00e8re. Par cons\u00e9quent, je participe \u00e0 titre de mentor par l'entremise de diff\u00e9rentes organisations, comme l'<a href=\"https://las.arts.ubc.ca/files/2011/10/3.-ASI-brochure_2011.pdf\"><span lang=\"en\">Accessible Science Initiative</span> \u2013 PDF (720\u00a0ko) (en anglais seulement)</a> (Initiative pour l'acc\u00e8s aux sciences), afin d'apporter un soutien aux femmes et aux filles qui suivent des \u00e9tudes scientifiques et qui font face \u00e0 des d\u00e9fis semblables.</p>\n\t\n<p>Alors que nous c\u00e9l\u00e9brons la Journ\u00e9e internationale des femmes, une journ\u00e9e de r\u00e9flexion sur les progr\u00e8s r\u00e9alis\u00e9s en mati\u00e8re d'\u00e9galit\u00e9 entre les sexes, il est important de ne pas oublier les nombreuses in\u00e9galit\u00e9s qui existent encore pour les femmes et les filles dans le domaine scientifique, en particulier celles des groupes sous-repr\u00e9sent\u00e9s. Il est question \u00e0 la fois d'\u00e9carts de r\u00e9mun\u00e9ration, du manque de services de garderie universels et de la repr\u00e9sentation limit\u00e9e aux postes de direction dans tous les \u00e9cosyst\u00e8mes scientifiques, y compris dans le milieu universitaire, l'industrie et le gouvernement. Personnellement, j'esp\u00e8re combiner mon expertise scientifique et mes efforts de promotion pour contribuer \u00e0 l'\u00e9laboration de politiques fond\u00e9es sur des donn\u00e9es probantes au Canada, et au renforcement de la diplomatie scientifique dans le monde, un monde qui peut offrir des chances \u00e9gales et des conditions de concurrence \u00e9quitables pour tous, en ne laissant personne pour compte.</p>\n\t\n<h2>Pour en savoir plus</h2>\n<ul>\n<li><a href=\"/les-sciences-et-les-recherches/fra/1494875229872/1494875261582\">Les sciences et les recherches \u00e0 l'ACIA</a></li>\n<li><a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">Nos laboratoires</a></li>\n<li><a href=\"http://www.science.gc.ca/eic/site/063.nsf/fra/98215.html\">L'ACIA dirige une nouvelle approche de l'\u00e9valuation des risques pour la sant\u00e9 li\u00e9e au SRAS-CoV-2</a> (science.gc.ca)</li>\n<li><a href=\"/fra/1637953775477/1637953776243\">Ce que c'est que de travailler avec les maladies les plus mortelles au monde</a></li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/une-conference-internationale-fait-avancer-la-scie/fra/1632406330682/1632406562302\">Une conf\u00e9rence internationale fait avancer la science \u00e0 l'\u00e8re post-pand\u00e9mique</a></li>\n</ul>\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}