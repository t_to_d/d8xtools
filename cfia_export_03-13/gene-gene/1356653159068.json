{
    "dcr_id": "1356653159068",
    "title": {
        "en": "Canada's Protocols for <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> Surveillance",
        "fr": "Protocoles canadiens de surveillance de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr>"
    },
    "html_modified": "2024-03-12 3:56:01 PM",
    "modified": "2018-05-07",
    "issued": "2012-12-27 19:12:01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_bse_surv_protocols_1356653159068_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_bse_surv_protocols_1356653159068_fra"
    },
    "ia_id": "1356653329490",
    "parent_ia_id": "1323992718670",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1323992718670",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Canada's Protocols for <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> Surveillance",
        "fr": "Protocoles canadiens de surveillance de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr>"
    },
    "label": {
        "en": "Canada's Protocols for <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> Surveillance",
        "fr": "Protocoles canadiens de surveillance de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1356653329490",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323992647051",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/bovine-spongiform-encephalopathy/enhanced-surveillance/protocols/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/encephalopathie-spongiforme-bovine/surveillance-intensifiee/protocoles/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Canada's Protocols for BSE Surveillance",
            "fr": "Protocoles canadiens de surveillance de l'ESB"
        },
        "description": {
            "en": "A robust surveillance program, with strong producer participation, gives domestic and international consumers confidence that Canada is taking responsible actions to monitor the health of the national herd.",
            "fr": "Un programme de surveillance solide et une participation \u00e9lev\u00e9e des producteurs donnent aux consommateurs canadiens et \u00e9trangers l'assurance que le Canada prend les mesures qui s'imposent pour surveiller la sant\u00e9 du cheptel national."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, Bovine Spongiform Encephalopathy, BSE, transmissible spongiform encephalopathy, TSE, mad cow disease,surveillance , protocols",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, enc\u00e9phalopathie spongiforme bovine. ESB, enc\u00e9phalopathie spongiforme transmissible, EST, maladie de la vache folle,  surveillance. Protocols"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-12-27 19:12:01",
            "fr": "2012-12-27 19:12:01"
        },
        "modified": {
            "en": "2018-05-07",
            "fr": "2018-05-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Canada's Protocols for BSE Surveillance",
        "fr": "Protocoles canadiens de surveillance de l'ESB"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> testing in Canada is in full accordance with the guidelines recommended by the World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE)).</p>\n<p>The samples collected target the highest risk cattle. This includes animals over 30 months of age that are dead, down, dying or diseased. In addition, any cattle that are exhibiting symptoms consistent with <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> must be reported to the Canadian Food Inspection Agency (CFIA). This targeted surveillance program is crucial to defining the level of <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> in Canada and to confirming the effectiveness of the measures in place to protect human and animal health from the disease. A robust surveillance program, with strong producer participation, gives domestic and international consumers confidence that Canada is taking responsible actions to monitor the health of the national herd.</p>\n<p>The Government of Canada   continues to work with provincial governments to support and encourage the participation   of Canadian cattle producers in the surveillance program.</p>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/bovine-spongiform-encephalopathy/enhanced-surveillance/eng/1323992647051/1323992718670#num\">Sampling     numbers and test results</a></li>\n</ul>\n<h2>Preliminary Testing Procedures</h2>\n<p>In Canada, two rapid <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> screening tests are used for routine surveillance: the Prionics\u00ae-Check PrioStrip and the Bio-Rad TeSeE\u00ae ELISA.</p>\n<p>Validations conducted by the European Food Safety Authority (EFSA) and the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and agencies in other governments have found the tests to be 100% accurate for detecting <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> in cattle in later stages of the incubation period. Due to the high analytical sensitivity of the tests, there are rare instances in which samples not infected with <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> may produce an initial reaction, thus necessitating re-testing.</p>\n<h2>Second Round of Testing</h2>\n<p>Initially reactive samples are referred to as \"non-negatives\"and require that the same test be repeated in duplicate by the screening laboratory.</p>\n<p>If the repeat tests are reactive, the sample is considered \u201cinconclusive\u201d for <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr>, and the sample is forwarded to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> laboratory in Lethbridge, Alberta, for confirmatory testing.</p>\n<p>The rapid test on an inconclusive sample is repeated at the National <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> Reference Laboratory and if the test is again reactive, the sample is considered to be a <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> \"suspect\" and is subject to confirmatory testing. Even if this third rapid test is negative, a confirmatory test (immunohistochemistry or WOAH Western Blot) is run for quality assurance purposes.</p>\n<h2>Confirmatory Testing</h2>\n<p>The immunohistochemistry (IHC) and the WOAH Western Blot, also called the SAF Immunoblot, are internationally recognized confirmatory tests for <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr>. The <abbr title=\"immunohistochemistry\">IHC</abbr> is the principle confirmatory test used (gold standard) at the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> laboratory in Lethbridge, Alberta.</p>\n<p>The WOAH Western Blot test may be performed on the sample in addition to, or as an alternative to, the <abbr title=\"immunohistochemistry\">IHC</abbr> test. It is always used on poor quality tissue samples when it may not be possible to conduct the <abbr title=\"immunohistochemistry\">IHC</abbr> test, or on suspect samples where the <abbr title=\"immunohistochemistry\">IHC</abbr> is negative.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le d\u00e9pistage de l'enc\u00e9phalopathie spongiforme bovine (ESB) au Canada est enti\u00e8rement conforme aux lignes directrices de l'Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)).</p>\n<p>Les \u00e9chantillons sont pr\u00e9lev\u00e9s sur les animaux \u00e0 risque le plus \u00e9lev\u00e9, c'est-\u00e0-dire tous les animaux \u00e2g\u00e9s de plus de 30 mois qui sont morts, couch\u00e9s, mourants ou malades. De plus, tous les cas de bovins pr\u00e9sentant des sympt\u00f4mes de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> doivent \u00eatre signal\u00e9s \u00e0 l'Agence canadienne d'inspection des aliments (ACIA). Ce programme de surveillance cibl\u00e9e est absolument n\u00e9cessaire pour d\u00e9terminer le niveau d'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> au Canada et confirmer l'efficacit\u00e9 des mesures d\u00e9ploy\u00e9es pour prot\u00e9ger la sant\u00e9 des humains et des animaux contre cette maladie. Un programme de surveillance solide et une participation \u00e9lev\u00e9e des producteurs donnent aux consommateurs canadiens et \u00e9trangers l'assurance que le Canada prend les mesures qui s'imposent pour surveiller la sant\u00e9 du cheptel national.</p>\n<p>Le gouvernement du Canada continuera de collaborer avec les provinces en vue d'appuyer et d'encourager la participation des producteurs de bovins canadiens au programme de surveillance.</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/encephalopathie-spongiforme-bovine/surveillance-intensifiee/fra/1323992647051/1323992718670#num\">Les chiffres relatifs \u00e0 l'\u00e9chantillonnage   et les r\u00e9sultats des \u00e9preuves</a></li>\n</ul>\n<h2>Proc\u00e9dures de diagnostic pr\u00e9liminaire</h2>\n<p>Au Canada, deux \u00e9preuves de d\u00e9pistage rapide de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> sont utilis\u00e9es pour effectuer la surveillance r\u00e9guli\u00e8re, soit le Prionics\u00ae-Check PrioStrip et le TeSeE \u00ae ELISA de Bio-Rad.</p>\n<p>Les validations effectu\u00e9es par l'Autorit\u00e9 europ\u00e9enne de s\u00e9curit\u00e9 des aliments (EFSA) et l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et d'autres agences gouvernementales ont permis de d\u00e9terminer que ces tests pr\u00e9sentent un taux d'exactitude de 100 <abbr title=\"percent\">p.</abbr> 100 pour d\u00e9pister l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> chez les bovins et ce, au stade avanc\u00e9 de la p\u00e9riode d'incubation de la maladie. Compte tenu de la haute sensibilit\u00e9 analytique de ces \u00e9preuves, il est possible, en de rares occasions, que des \u00e9chantillons non infect\u00e9s par l'ESB soient r\u00e9actifs; ces derniers doivent alors \u00eatre soumis \u00e0 une seconde \u00e9preuve de d\u00e9pistage.</p>\n<h2>Deuxi\u00e8me s\u00e9rie d'\u00e9preuves</h2>\n<p>Les \u00e9chantillons initialement r\u00e9actifs sont consid\u00e9r\u00e9s comme \u00e9tant \u00ab non n\u00e9gatifs \u00bb et doivent \u00eatre soumis \u00e0 nouveau, par le laboratoire responsable, \u00e0 la m\u00eame \u00e9preuve de d\u00e9pistage.</p>\n<p>Si l'\u00e9chantillon est r\u00e9actif \u00e0 la suite de la nouvelle \u00e9preuve, le r\u00e9sultat de d\u00e9pistage de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> est consid\u00e9r\u00e9 \u00ab inconcluant \u00bb et l'\u00e9chantillon est envoy\u00e9 au laboratoire de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e0 Lethbridge (Alberta) pour subir une \u00e9preuve de confirmation.</p>\n<p>Le Laboratoire national de r\u00e9f\u00e9rence pour l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> effectue donc une \u00e9preuve de d\u00e9pistage rapide sur les \u00e9chantillons dont les r\u00e9sultats sont \u00ab inconcluants \u00bb. Si ces \u00e9chantillons s'av\u00e8rent une fois de plus r\u00e9actifs, ils seront consid\u00e9r\u00e9s \u00ab suspects \u00bb et soumis \u00e0 une \u00e9preuve de confirmation. M\u00eame si la troisi\u00e8me \u00e9preuve de d\u00e9pistage rapide produit des r\u00e9sultats n\u00e9gatifs, une \u00e9preuve de confirmation (immunohistochimie ou transfert de type Western de l'OMSA) est tout de m\u00eame effectu\u00e9e \u00e0 des fins d'assurance de la qualit\u00e9.</p>\n<h2>\u00c9preuve de confirmation</h2>\n<p>L'immunohistochimie (IHC) et le transfert de type Western de l'OMSA, qu'on appelle aussi l'\u00e9preuve Immunoblot SAF (fibrilles associ\u00e9es \u00e0 la tremblante), sont des \u00e9preuves de confirmation de l'<abbr title=\"Enc\u00e9phalopathie spongiforme bovine\">ESB</abbr> reconnues \u00e0 l'\u00e9chelle internationale. L'<abbr title=\"immunohistochimie\">IHC</abbr> est la principale \u00e9preuve de confirmation utilis\u00e9e (\u00e9preuve id\u00e9ale) au laboratoire de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e0 Lethbridge (Alberta).</p>\n<p>Le transfert de type Western de l'OMSA peut \u00eatre effectu\u00e9 sur un \u00e9chantillon en plus, ou \u00e0 la place, de l'<abbr title=\"immunohistochimie\">IHC</abbr>. Il est toujours utilis\u00e9 sur les \u00e9chantillons de tissus de pi\u00e8tre qualit\u00e9 lorsqu'il est impossible d'effectuer une <abbr title=\"\u00e9preuve immunocytochimique\">EI</abbr>, ou sur les \u00e9chantillons suspects lorsque l'<abbr title=\"immunohistochimie\">IHC</abbr> produit des r\u00e9sultats n\u00e9gatifs.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}