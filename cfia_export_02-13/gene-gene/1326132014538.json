{
    "dcr_id": "1326132014538",
    "title": {
        "en": "<i lang=\"la\">Anoplophora chinensis</i> (Citrus long-horned beetle) - Fact Sheet",
        "fr": "<i lang=\"la\">Anoplophora chinensis</i> (Longicorne des agrumes) - Fiche de renseignements"
    },
    "html_modified": "2024-02-13 9:47:11 AM",
    "modified": "2012-01-09",
    "issued": "2012-01-09 13:00:17",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_anochi_factsheet_1326132014538_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_anochi_factsheet_1326132014538_fra"
    },
    "ia_id": "1326132291047",
    "parent_ia_id": "1326124817392",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1326124817392",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "<i lang=\"la\">Anoplophora chinensis</i> (Citrus long-horned beetle) - Fact Sheet",
        "fr": "<i lang=\"la\">Anoplophora chinensis</i> (Longicorne des agrumes) - Fiche de renseignements"
    },
    "label": {
        "en": "<i lang=\"la\">Anoplophora chinensis</i> (Citrus long-horned beetle) - Fact Sheet",
        "fr": "<i lang=\"la\">Anoplophora chinensis</i> (Longicorne des agrumes) - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1326132291047",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1326124573039",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/citrus-long-horned-beetle/fact-sheet/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-des-agrumes/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Anoplophora chinensis (Citrus long-horned beetle) - Fact Sheet",
            "fr": "Anoplophora chinensis (Longicorne des agrumes) - Fiche de renseignements"
        },
        "description": {
            "en": "The citrus long-horned beetle is a wood-boring insect that is native to Asia. The larvae of this beetle can kill trees directly, by damaging the tree, or indirectly, by enabling other pests and diseases to affect the tree.",
            "fr": "Le longicorne des agrumes est un col\u00e9opt\u00e8re xylophage (qui se nourrit de bois) originaire d\u2019Asie, dont les larves peuvent tuer les arbres"
        },
        "keywords": {
            "en": "Anoplophora chinensis, Citrus long-horned beetle, wood-boring beetle, plant pest",
            "fr": "Anoplophora chinensis, Longicorne des agrumes, buprestid&#233;s, phytoravageur"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-09 13:00:17",
            "fr": "2012-01-09 13:00:17"
        },
        "modified": {
            "en": "2012-01-09",
            "fr": "2012-01-09"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Anoplophora chinensis (Citrus long-horned beetle) - Fact Sheet",
        "fr": "Anoplophora chinensis (Longicorne des agrumes) - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Background</h2>\n<p>The citrus long-horned beetle is a wood-boring insect that is native to Asia. The larvae of this beetle can kill trees directly, by damaging the tree, or indirectly, by enabling other pests and diseases to affect the tree.</p>\n<p>The beetle gets its name from the damage caused to citrus groves in its native China. However, it feeds on and damages a wide variety of hardwood trees, including many important species found in <a href=\"#a1\">Canada</a>.</p>\n<p>The citrus long-horned beetle is just one species in a genus of damaging pests; the entire genus <i lang=\"la\">Anoplophora</i> <abbr title=\"species\">spp.</abbr> is included in the <i><a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">Pests Regulated by Canada</a></i>.</p>\n<p>This beetle has been intercepted once entering Canada. However, it has never been found in this country.</p>\n<p>The citrus long-horned beetle has been introduced on several occasions into Europe, and has been detected in and subsequently eradicated from the United States.</p>\n<p>If citrus long-horned beetle is detected, immediate regulatory measures are taken to contain potential sources of spread. Citrus long-horned beetle is a regulated quarantine pest in Canada and any suspected infestation must be immediately reported to a <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office</a>.</p>\n<p>All plants and plant products must meet <a href=\"/plant-health/invasive-species/plant-import/eng/1324569244509/1324569331710\">import procedures</a> in accordance with national policy guidelines under provision of the <i><a href=\"/english/reg/jredirect2.shtml?plavega\">Plant Protection Act</a></i> and <i><a href=\"/english/reg/jredirect2.shtml?plavegr\">Regulations</a></i> when entering Canada.</p>\n<h2>How does the beetle spread?</h2>\n<p>Long-distance spread of the citrus long-horned beetle happens mostly through infested nursery plants. This was seen in the European infestation, where it arrived via maple trees. It can also be transported in wood packaging material, firewood, logs and any other wood products made from infested trees. As well, this beetle is capable of flying up to two kilometres.</p>\n<h2>Biology</h2>\n<p>In Japan, the adult female lives an average of 77 days and lays up to 300 eggs. These eggs are about the shape and size of a small grain of rice. The adult female beetle chews a T-shaped slit into the plant's trunk, branches or exposed roots, and lays her eggs inside the slit. The eggs hatch inside the plant after one or two weeks.</p>\n<p>The hatched young larvae begin feeding on the plant tissue just under the bark. As the larvae mature they bore deeper into the wood, creating tunnels while feeding. Larvae mature in the tunnels and then exit the tree as adults through large round holes. The holes are 10 to 20 millimetres in diameter and are typically near the root collar and on exposed roots.</p>\n<p>Depending on the climate, the beetle may take approximately three years to complete its cycle from egg to adult beetle. Because of this, the beetle can remain hidden inside a plant for several years before it emerges.</p>\n<h2>Detection and identification</h2>\n<p>The citrus long-horned beetle is very similar in appearance to the <a href=\"/plant-health/invasive-species/insects/asian-longhorned-beetle/eng/1337792721926/1337792820836\">Asian long-horned beetle</a> (<i lang=\"la\"><abbr title=\"Anoplophora\">A.</abbr> glabripennis</i>). It is large (about 25 to 35 millimetres long) with a shiny black back that is dotted with 10 to 12 white spots.</p>\n<p>Because of their relatively large sizes, mature larvae or pupae will most likely be found inside stems and root collars that are several centimetres in diameter. However, in Europe, eggs and young larvae have been detected in branches as small as 10 millimetres (0.4 inches) in diameter.</p>\n<h2><a id=\"a1\">Hosts</a></h2>\n<p>There are over 85 genera of plants, in more than 40 plant families, that can host this beetle. This includes several economically important fruit, forest and ornamental plant species in Canada.</p>\n<p>Some of the known host plants common in Canada are</p>\n<ul>\n<li>alder (<i lang=\"la\">Alnus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>apple (<i lang=\"la\">Malus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>ash (<i lang=\"la\">Fraxinus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>birch (<i lang=\"la\">Betula</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>hawthorn (<i lang=\"la\">Crataegus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>horse chestnut (<i lang=\"la\">Aesculus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>poplar (<i lang=\"la\">Populus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>beech (<i lang=\"la\">Fagus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>maple (<i lang=\"la\">Acer</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>pine (<i lang=\"la\">Pinus</i> <abbr title=\"species\">spp.</abbr>)</li>\n<li>rose (<i lang=\"la\">Rosa</i> <abbr title=\"species\">spp.</abbr>)</li>\n</ul>\n<h2>Signs and symptoms</h2>\n<p>Signs of this pest on nursery plants could include</p>\n<ul>\n<li>exit holes near the root collar and on exposed roots</li>\n<li>sawdust-like excrement on the surface of the soil</li>\n<li>T-shaped slits on the bark of the lower part of the bole/trunk and on exposed roots</li>\n<li>chewed leaves and scraped bark</li>\n</ul>\n<p>On plants where the citrus long-horned beetle is present, but has not completed its life cycle, there may be no visible external signs that the plant is infested.</p>\n\n<div class=\"brdr-tp mrgn-bttm-md\"></div> \n\n<div class=\"row mrgn-bttm-xl\">\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image1_1326123209934_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image1small_1326123599634_eng.jpg\" alt=\"Citrus Long-horned Beetle on a leaf\" class=\"img-responsive\"></a><br> Figure 1: Adult of <i lang=\"la\">Anoplophora chinensis</i> (<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>)</div>\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image2_1326123289824_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image2small_1326123654243_eng.jpg\" alt=\"Citrus Long-horned Beetle larva in the root\" class=\"img-responsive\"></a><br> Figure 2: Larva in root cellar (Photo: A. Vukadin, Croatia)</div>\n</div>\n \n<div class=\"clearfix\"> </div> \n \n<div class=\"row mrgn-bttm-xl\">\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image3_1326123355714_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image3small_1326123734399_eng.jpg\" alt=\"Citrus Long-horned Beetle damage on a potted plant\" class=\"img-responsive\"></a><br> Figure 3: Damage on potted plant (Photo: A. Vukadin, Croatia)</div>\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image4_1326123401323_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image4small_1326123804227_eng.jpg\" alt=\"Citrus Long-horned Beetle damage on a potted maple\" class=\"img-responsive\"></a><br> Figure 4: Damage on potted maple (Photo: A. Vukadin, Croatia)</div>\n</div>\n \n<div class=\"clearfix\"> </div> \n \n<div class=\"row mrgn-bttm-xl\">\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image5_1326123459151_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image5small_1326123860148_eng.jpg\" alt=\"Citrus Long-horned Beetle signs of dying and dead growth\" class=\"img-responsive\"></a><br> Figure 5: Signs of wilting, dead/dying growth on Japanese maples, frass on the soil (Photo: A. Vukadin, Croatia)</div>\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image6_1326123519900_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image6small_1326123934554_eng.jpg\" alt=\"Citrus Long-horned Beetle exit hole above the root\" class=\"img-responsive\"></a><br> Figure 6: Exit hole above root collar (Photo: Plant Protection Service, the Netherlands)</div>\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Contexte</h2>\n<p>Le longicorne des agrumes est un col\u00e9opt\u00e8re xylophage (qui se nourrit de bois) originaire d'Asie, dont les larves peuvent tuer les arbres :</p>\n<ul>\n<li>de fa\u00e7on directe, en leur causant des dommages;</li>\n<li>de fa\u00e7on indirecte, en permettant \u00e0 d'autres organismes nuisibles ou maladies de les attaquer.</li>\n</ul>\n<p>Le col\u00e9opt\u00e8re tire son nom du fait qu'il cause des dommages aux vergers d'agrumes dans son pays d'origine, la Chine. Toutefois, il peut consommer et endommager une grande vari\u00e9t\u00e9 d'arbres feuillus, y compris de nombreuses essences importantes pr\u00e9sentes au <a href=\"#a1\">Canada</a>.</p>\n<p>Le genre <i lang=\"la\">Anoplophora</i>, dont le longicorne des agrumes fait partie, se compose d'insectes nuisibles, et l'ensemble du genre figure sur la <i><a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Liste des parasites r\u00e9glement\u00e9s par le Canada</a></i>.</p>\n<p>Le longicorne des agrumes a \u00e9t\u00e9 intercept\u00e9 une seule fois au Canada, au moment de son arriv\u00e9e. Il n'a toutefois jamais \u00e9t\u00e9 signal\u00e9 au pays.</p>\n<p>Le col\u00e9opt\u00e8re a \u00e9t\u00e9 introduit \u00e0 plusieurs reprises en Europe. Il a \u00e9t\u00e9 d\u00e9tect\u00e9 aux \u00c9tats-Unis et par la suite \u00e9radiqu\u00e9.</p>\n<p>Si le longicorne des agrumes \u00e9tait d\u00e9tect\u00e9 au Canada, des mesures r\u00e9glementaires seraient imm\u00e9diatement prises pour contenir les sources possibles de contamination. Ce col\u00e9opt\u00e8re est un organisme de quarantaine r\u00e9glement\u00e9 au Canada, et toute infestation soup\u00e7onn\u00e9e doit imm\u00e9diatement \u00eatre signal\u00e9e \u00e0 <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">un bureau r\u00e9gional de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.</p>\n<p>Au moment de leur arriv\u00e9e au Canada, tous les v\u00e9g\u00e9taux et produits v\u00e9g\u00e9taux doivent satisfaire aux <a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/fra/1324569244509/1324569331710\">proc\u00e9dures pour l'importation</a>, conform\u00e9ment aux lignes directrices nationales \u00e9tablies en vertu de <i><a href=\"/francais/reg/jredirect2.shtml?plavega\">la Loi sur la protection des v\u00e9g\u00e9taux</a></i> et le <i><a href=\"/francais/reg/jredirect2.shtml?plavegr\">R\u00e8glement sur la protection des v\u00e9g\u00e9taux</a></i>.</p>\n<h2>Comment le col\u00e9opt\u00e8re se propage-t-il?</h2>\n<p>Le longicorne des agrumes se propage sur de longues distances principalement par l'entremise de plants de p\u00e9pini\u00e8re infest\u00e9s. Ainsi, l'insecte s'est introduit en Europe par l'entremise d'\u00e9rables. Il peut \u00e9galement \u00eatre transport\u00e9 dans les mat\u00e9riaux d'emballage en bois, le bois de chauffage, les billes ou tout autre produit du bois tir\u00e9 d'arbres infest\u00e9s. De plus, l'insecte est capable de parcourir jusqu'\u00e0 2 <abbr title=\"kilom\u00e8tre\">km</abbr> au vol.</p>\n<h2>Biologie</h2>\n<p>Au Japon, la femelle adulte vit en moyenne 77 jours, p\u00e9riode au cours de laquelle elle peut pondre jusqu'\u00e0 300 oeufs. Ces oeufs ont environ la forme et la taille d'un petit grain de riz. La femelle adulte gruge une encoche en forme de T dans le tronc, les branches ou les racines expos\u00e9es des plantes avant d'y pondre ses oeufs. Les oeufs \u00e9closent \u00e0 l'int\u00e9rieur de la plante apr\u00e8s une ou deux semaines.</p>\n<p>Apr\u00e8s l'\u00e9closion, les jeunes larves commencent \u00e0 se nourrir des tissus de la plante, imm\u00e9diatement sous l'\u00e9corce. En grandissant, les larves creusent des tunnels pour se nourrir et s'enfoncent plus profond\u00e9ment dans le bois. Elles atteignent la maturit\u00e9 dans les tunnels, puis sortent de l'arbre par de gros trous ronds, sous leur forme adulte. Ces trous mesurent 10 \u00e0 20 <abbr title=\"millim\u00e8tre\">mm</abbr> de diam\u00e8tre et se trouvent g\u00e9n\u00e9ralement pr\u00e8s du collet ou sur les racines expos\u00e9es.</p>\n<p>Selon le climat, le cycle de vie du longicorne des agrumes, de l'oeuf \u00e0 l'adulte, s'\u00e9chelonne sur environ trois ans. L'insecte peut donc demeurer cach\u00e9 \u00e0 l'int\u00e9rieur de la plante pendant des ann\u00e9es avant d'en \u00e9merger.</p>\n<h2>D\u00e9tection et identification</h2>\n<p>Le longicorne des agrumes ressemble beaucoup au longicorne \u00e9toil\u00e9, \u00e9galement appel\u00e9 <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-asiatique/fra/1337792721926/1337792820836\">\u00ab longicorne \u00bb asiatique</a> (<i lang=\"la\"><abbr title=\"Anoplophora\">A.</abbr> glabripennis</i>). Ce dernier est un gros col\u00e9opt\u00e8re (environ 25 \u00e0 35 <abbr title=\"millim\u00e8tre\">mm</abbr> de long) dont le dos est noir, luisant et parsem\u00e9 de 10 \u00e0 12 taches blanches.</p>\n<p>En raison de leur taille relativement grande, les larves matures et les nymphes se rencontrent le plus souvent \u00e0 l'int\u00e9rieur de tiges ou de collets qui font plusieurs centim\u00e8tres de diam\u00e8tre. Toutefois, en Europe, des oeufs et de jeunes larves ont \u00e9t\u00e9 d\u00e9tect\u00e9s dans des branches de seulement 10 <abbr title=\"millim\u00e8tre\">mm</abbr> (0,4 <abbr title=\"pouce\">po</abbr>) de diam\u00e8tre.</p>\n<h2><a id=\"a1\">H\u00f4tes</a></h2>\n<p>Les h\u00f4tes du longicorne des agrumes se r\u00e9partissent entre plus de 85 genres et au-del\u00e0 de 40 familles. Plusieurs esp\u00e8ces fruiti\u00e8res, foresti\u00e8res et ornementales d'importance \u00e9conomique au Canada comptent parmi ces h\u00f4tes.</p>\n<p>Voici quelques-unes des plantes h\u00f4tes qui sont communes au Canada :</p>\n<ul>\n<li>Aulnes (<i lang=\"la\">Alnus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Pommiers (<i lang=\"la\">Malus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Fr\u00eanes (<i lang=\"la\">Fraxinus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Bouleaux (<i lang=\"la\">Betula</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Aub\u00e9pines (<i lang=\"la\">Crataegus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Marronniers (<i lang=\"la\">Aesculus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Peupliers (<i lang=\"la\">Populus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>H\u00eatres (<i lang=\"la\">Fagus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>\u00c9rables (<i lang=\"la\">Acer</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Pins (<i lang=\"la\">Pinus</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n<li>Rosiers (<i lang=\"la\">Rosa</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</li>\n</ul>\n<h2>Signes et sympt\u00f4mes</h2>\n<p>La pr\u00e9sence des signes suivants chez un plant de p\u00e9pini\u00e8re peut \u00eatre indicatrice d'une infestation par le longicorne des agrumes :</p>\n<ul>\n<li>trous pr\u00e8s du collet ou sur les racines expos\u00e9es;</li>\n<li>excr\u00e9ments ressemblant \u00e0 de la sciure de bois \u00e0 la surface du sol;</li>\n<li>encoches en forme de T dans l'\u00e9corce du bas du tronc ou des racines expos\u00e9es;</li>\n<li>feuilles m\u00e2ch\u00e9es et \u00e9corce arrach\u00e9e.</li>\n</ul>\n<p>Les plantes infest\u00e9es par le longicorne des agrumes peuvent demeurer exemptes de signes externes tant que l'insecte n'est pas arriv\u00e9 au dernier stade de son cycle de vie.</p>\n\n<div class=\"brdr-tp mrgn-bttm-md\"></div>\n\n<div class=\"row mrgn-bttm-xl\">\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image1_1326123209934_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image1small_1326123599634_fra.jpg\" alt=\"Le longicorne des agrumes sur une feuille.\" class=\"img-responsive\"></a><br> Figure 1 : <i lang=\"la\">Anoplophora chinensis</i> adulte (<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>)</div>\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image2_1326123289824_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image2small_1326123654243_fra.jpg\" alt=\"Larve du longicorne des agrumes dans un collet.\" class=\"img-responsive\"></a><br> Figure 2 : Larve dans le collet d'une plante (Photo : A. Vukadin, Croatie).</div>\n</div>\n \n<div class=\"clearfix\"> </div> \n \n<div class=\"row mrgn-bttm-xl\">\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image3_1326123355714_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image3small_1326123734399_fra.jpg\" alt=\"Dommages caus\u00e9s par le longicorne des agrumes \u00e0 une plante en pot.\" class=\"img-responsive\"></a><br> Figure 3 : Plante en pot endommag\u00e9e (Photo : A. Vukadin, Croatie).</div>\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image4_1326123401323_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image4small_1326123804227_fra.jpg\" alt=\"Dommages caus\u00e9s par le longicorne des agrumes \u00e0 un \u00e9rable en pot.\" class=\"img-responsive\"></a><br> Figure 4 : \u00c9rable en pot endommag\u00e9 (Photo : A. Vukadin, Croatie).</div>\n</div>\n \n<div class=\"clearfix\"> </div> \n \n<div class=\"row mrgn-bttm-xl\">\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image5_1326123459151_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image5small_1326123860148_fra.jpg\" alt=\"Feuilles mortes ou en train de mourir en raison des dommages caus\u00e9s par le longicorne des agrumes.\" class=\"img-responsive\"></a><br> Figure 5 : \u00c9rables du Japon avec parties mortes, en train de mourir ou fl\u00e9tries, avec d\u00e9jections sur le sol (Photo : A. Vukadin, Croatie).</div>\n<div class=\"col-sm-6\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image6_1326123519900_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_factsheet_image6small_1326123934554_fra.jpg\" alt=\"Trou d'\u00e9mergence du longicorne des agrumes au-dessus du collet.\" class=\"img-responsive\"></a><br> Figure 6 : Trou d'\u00e9mergence au-dessus du collet (Photo : Plant Protection Service, Pays-Bas).</div>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}