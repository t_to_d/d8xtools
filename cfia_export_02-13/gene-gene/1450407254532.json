{
    "dcr_id": "1450407254532",
    "title": {
        "en": "Notification of Reportable Diseases by Aquaculturists",
        "fr": "D\u00e9claration obligatoire des maladies d\u00e9clarables d\u2019animaux aquatiques par les aquaculteurs"
    },
    "html_modified": "2024-02-13 9:51:09 AM",
    "modified": "2015-12-17",
    "issued": "2015-12-31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_report_aquaculturists_1450407254532_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_report_aquaculturists_1450407254532_fra"
    },
    "ia_id": "1450407255317",
    "parent_ia_id": "1322941111904",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Diseases",
        "fr": "Maladies"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1322941111904",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notification of Reportable Diseases by Aquaculturists",
        "fr": "D\u00e9claration obligatoire des maladies d\u00e9clarables d\u2019animaux aquatiques par les aquaculteurs"
    },
    "label": {
        "en": "Notification of Reportable Diseases by Aquaculturists",
        "fr": "D\u00e9claration obligatoire des maladies d\u00e9clarables d\u2019animaux aquatiques par les aquaculteurs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1450407255317",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1322940971192",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/aquaculturists/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/aquaculteurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notification of Reportable Diseases by Aquaculturists",
            "fr": "D\u00e9claration obligatoire des maladies d\u00e9clarables d\u2019animaux aquatiques par les aquaculteurs"
        },
        "description": {
            "en": "Aquaculturists who own or work with finfish, molluscs or crustaceans are required to notify the Canadian Food Inspection Agency (CFIA) when they suspect a reportable aquatic animal disease or when a reportable aquatic animal disease is detected in the animals under their care, possession or control.",
            "fr": "Les aquaculteurs qui sont propri\u00e9taires ou qui travaillent avec des poissons \u00e0 nageoires, des mollusques ou des crustac\u00e9s doivent aviser l'Agence canadienne d'inspection des aliments (ACIA) lorsqu'ils croient qu'une maladie d'animaux aquatiques est d\u00e9tect\u00e9e dans les animaux dont ils prennent soin, poss\u00e8dent ou contr\u00f4lent."
        },
        "keywords": {
            "en": "federally reportable disease, Aquaculturists working with aquatic animals",
            "fr": "maladie \u00e0 d\u00e9claration obligatoire, d\u00e9claration obligatoire des maladies d\u00e9clarables,  notification imm\u00e9diate d\u2019animaux aquatiques par les aquaculteurs"
        },
        "dcterms.subject": {
            "en": "crustaceans,seafood,government information,inspection,infectious diseases,molluscs,fish,government publication,scientific research,food safety,animal health,viruses",
            "fr": "crustac\u00e9,fruits de mer,information gouvernementale,inspection,maladie infectieuse,mollusque,poisson,publication gouvernementale,recherche scientifique,salubrit\u00e9 des aliments,sant\u00e9 animale,virus"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-12-31",
            "fr": "2015-12-31"
        },
        "modified": {
            "en": "2015-12-17",
            "fr": "2015-12-17"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notification of Reportable Diseases by Aquaculturists",
        "fr": "D\u00e9claration obligatoire des maladies d\u00e9clarables d\u2019animaux aquatiques par les aquaculteurs"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Aquaculturists who own or work with finfish, molluscs or crustaceans are required to notify the Canadian Food Inspection Agency (CFIA) when they suspect a reportable aquatic animal disease or when a reportable aquatic animal disease is detected in the animals under their care, possession or control.</p>\n<p>Amendments to the <i>Health of Animals Regulations</i> and <i>Reportable Diseases Regulations</i> were published in <i><a href=\"http://www.gazette.gc.ca/rp-pr/p2/2011/2011-01-05/html/sor-dors310-eng.html\">Canada Gazette</a></i> Part <abbr title=\"2\">II</abbr>, on <span class=\"nowrap\">December 22, 2010</span>, and <span class=\"nowrap\">January 5, 2011</span>, respectively.</p>\n<p>The requirement is as follows: </p>\n<p class=\"mrgn-lft-lg\">5.\u00a0(1) A person who owns or has the possession, care or control of an animal shall notify the nearest veterinary inspector of the presence of a reportable disease or toxic substance, or any fact indicating its presence, in or around the animal, immediately after the person becomes aware of the presence or fact (<i>Health of Animals Act</i>).</p>\n<p>If you suspect or detect (including cases of non-negative test results) of a reportable aquatic animal disease, notify a veterinary inspector located at your nearest <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Animal Health Office</a>, as soon as is reasonably possible. </p>\n<p>The veterinary inspector may require additional information from you in order to help him/her determine whether an inspection is required to examine the affected animals and collect samples for testing in a Fisheries and Oceans Canada <a href=\"http://www.dfo-mpo.gc.ca/science/aah-saa/National-Aquatic-Animal-Health-Laboratory-System-eng.html\">National Aquatic Animal Health Laboratory System</a>. When notifying the veterinary inspector, please make sure that live animals or fresh, refrigerated carcasses are available for inspection. Requested information may include:</p>\n<ul>\n<li>scientific name of the animal, </li>\n<li>life stage affected, </li>\n<li>number of animals that may be affected, </li>\n<li>disease history of the animals, </li>\n<li>presence of signs of disease, </li>\n<li>mortality numbers, </li>\n<li>whether the animals have been examined by a veterinarian, and </li>\n<li>laboratory reports.</li>\n</ul>\n<p>The reportable aquatic animal diseases are:</p>\n<table class=\"table table-bordered\">\n<caption><strong>Reportable Diseases</strong></caption>\n<thead>\n<tr>\n<th class=\"active\">Disease</th>\n<th class=\"active\">Aquatic Animal</th>\n</tr></thead><tbody>\n<tr>\n<td><i lang=\"la\">Bonamia ostreae</i></td>\n<td>Mollusc</td>\n</tr>\n<tr>\n<td>Ceratomyxosis (<i lang=\"la\">Ceratomyxa shasta</i>)</td>\n<td>Finfish</td>\n</tr>\n<tr>\n<td>Epizootic haematopoietic necrosis </td>\n<td>Finfish</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Haplosporidium nelsoni</i> (MSX) </td>\n<td>Mollusc</td>\n</tr>\n<tr>\n<td>Infectious haematopoietic necrosis </td>\n<td>Finfish</td>\n</tr>\n<tr>\n<td>Infectious pancreatic necrosis </td>\n<td>Finfish </td>\n</tr>\n<tr>\n<td>Infectious salmon anaemia </td>\n<td>Finfish </td>\n</tr>\n<tr>\n<td>Koi herpesvirus disease </td>\n<td>Finfish </td>\n</tr>\n<tr>\n<td><i lang=\"la\">Marteilia refringens</i>  </td>\n<td>Mollusc</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Marteiliodes chungmuensis</i></td>\n<td>Mollusc</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Mikrocytos mackini</i> (Denman Island Disease) </td>\n<td>Mollusc</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Perkinsus marinus</i></td>\n<td>Mollusc</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Perkinsus olseni</i></td>\n<td>Mollusc </td>\n</tr>\n<tr>\n<td>Spring viraemia of carp </td>\n<td>Finfish </td>\n</tr>\n<tr>\n<td>Taura syndrome</td>\n<td>Crustacean</td>\n</tr>\n<tr>\n<td>Viral haemorrhagic septicaemia</td>\n<td>Finfish</td>\n</tr>\n<tr>\n<td>Whirling disease (<i lang=\"la\">Myxobolus cerebralis</i>) </td>\n<td>Finfish </td>\n</tr>\n<tr>\n<td>White spot disease </td>\n<td>Crustacean</td>\n</tr>\n<tr>\n<td>White sturgeon iridoviral disease </td>\n<td>Finfish</td>\n</tr>\n<tr>\n<td>Yellow head disease </td>\n<td>Crustacean </td>\n</tr></tbody>\n</table>\n<p>More information is available on these <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/eng/1322940971192/1322941111904\">Reportable Diseases</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les aquaculteurs qui sont propri\u00e9taires ou qui travaillent avec des poissons \u00e0 nageoires, des mollusques ou des crustac\u00e9s doivent aviser l'Agence canadienne d'inspection des aliments (ACIA) lorsqu'ils croient qu'une maladie d'animaux aquatiques est d\u00e9tect\u00e9e dans les animaux dont ils prennent soin, poss\u00e8dent ou contr\u00f4lent.</p>\n\n<p>Les modifications au <i>R\u00e8glement sur la sant\u00e9 des animaux</i> et au <i>R\u00e8glement sur les maladies d\u00e9clarables</i> ont \u00e9t\u00e9 respectivement publi\u00e9es le 22\u00a0d\u00e9cembre\u00a02010 et le 5\u00a0janvier\u00a02011 dans la <i>Partie II</i> de la <a href=\"http://www.gazette.gc.ca/rp-pr/p2/2011/2011-01-05/html/sor-dors310-fra.html\"><i>Gazette du Canada</i></a>.</p>\n\n<p>L'exigence est la suivante\u00a0:</p>\n\n<p class=\"mrgn-lft-lg\">5.\u00a0(1) Le propri\u00e9taire d'un animal ou toute personne en ayant la possession, la responsabilit\u00e9 ou la charge des soins sont tenus de d\u00e9clarer sans d\u00e9lai au plus proche v\u00e9t\u00e9rinaire-inspecteur la pr\u00e9sence d'une maladie d\u00e9clarable ou d'une substance toxique chez l'animal ou dans son milieu de vie, de m\u00eame que tout fait indicatif \u00e0 cet \u00e9gard (<i>Loi sur la sant\u00e9 des animaux)</i>.</p>\n\n<p>Si vous croyez ou si vous d\u00e9tectez une maladie d'animaux aquatiques d\u00e9clarable (y compris des cas de r\u00e9sultats d'analyse non n\u00e9gatifs), veuillez en aviser un v\u00e9t\u00e9rinaire-inspecteur de <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">Bureau de sant\u00e9 des animaux</a> de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> le plus proche le plus pr\u00e8s aussit\u00f4t que possible.</p>\n\n<p>Le v\u00e9t\u00e9rinaire-inspecteur peut n\u00e9cessiter des renseignements additionnels de votre part pour l'aider \u00e0 d\u00e9terminer si une inspection est n\u00e9cessaire afin d'examiner les animaux affect\u00e9s et recueillir des \u00e9chantillons \u00e0 des fins d'essai dans un laboratoire du <a href=\"http://www.dfo-mpo.gc.ca/science/aah-saa/National-Aquatic-Animal-Health-Laboratory-System-fra.html\">Syst\u00e8me national de laboratoires pour la sant\u00e9 des animaux aquatiques</a> du minist\u00e8re des P\u00e8ches et des Oc\u00e9ans. Lorsque vous avisez le v\u00e9t\u00e9rinaire-inspecteur, veuillez assurer que les animaux vivants ou les carcasses fra\u00eeches r\u00e9frig\u00e9r\u00e9es sont disponibles \u00e0 des fins d'inspection. Les renseignements demand\u00e9s peuvent comprendre ce qui suit\u00a0:</p>\n\n<ul>\n<li>le nom scientifique de l'animal;</li>\n<li>le cycle de vie affect\u00e9;</li>\n<li>le nombre d'animaux possiblement affect\u00e9s;</li>\n<li>l'historique de maladies des animaux;</li>\n<li>la pr\u00e9sence de signes de maladie;</li>\n<li>le nombre de mortalit\u00e9s;</li>\n<li>si les animaux ont \u00e9t\u00e9 examin\u00e9s par un v\u00e9t\u00e9rinaire;</li>\n<li>rapports de laboratoires.</li>\n</ul>\n\n<p>Les maladies d\u00e9clarables des animaux aquatiques sont les suivantes\u00a0:</p>\n\n<table class=\"table table-bordered\">\n<caption><strong>Maladies d\u00e9clarables</strong></caption>\n<thead>\n<tr>\n<th class=\"active\">Maladie</th>\n<th class=\"active\">Animal aquatique</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>An\u00e9mie infectieuse du saumon</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td><i>Bonamia ostreae</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td>C\u00e9ratomyxose (<i>Ceratomyxa shasta</i>)</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Haplosporidium nelsoni</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td>Herp\u00e8s virose de la carpe ko\u00ef</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td>Iridovirose de l'esturgeon blanc</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td>Maladie de la t\u00eate jaune</td>\n<td>Crustac\u00e9</td>\n</tr>\n<tr>\n<td>Maladie des points blancs</td>\n<td>Crustac\u00e9</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Marteilia refringens</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Marteiliodes chungmuensis</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Mikrocytos mackini</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td>N\u00e9crose h\u00e9matopo\u00ef\u00e9tique \u00e9pizootique</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td>N\u00e9crose h\u00e9matopo\u00ef\u00e9tique infectieuse</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td>N\u00e9crose pancr\u00e9atique infectieuse</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Perkinsus marinus</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td><i lang=\"la\">Perkinsus olseni</i></td>\n<td>Mollusque</td>\n</tr>\n<tr>\n<td>Septic\u00e9mie h\u00e9morragique virale</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td>Syndrome de Taura</td>\n<td>Crustac\u00e9</td>\n</tr>\n<tr>\n<td>Tournis des truites (<i lang=\"la\">Myxobolus cerebralis</i>)</td>\n<td>Poisson</td>\n</tr>\n<tr>\n<td>Vir\u00e9mie printani\u00e8re de la carpe</td>\n<td>Poisson</td>\n</tr>\n</tbody>\n</table>\n\n<p>Plus de renseignements sont disponibles sur ces <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/fra/1322940971192/1322941111904\">Maladies d\u00e9clarables</a>.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}