<?php

use Drupal\agri_admin\AgriAdminHelper;

class Empl extends DeleteNode
{
  public function isTooOld() {
    if (isset($this->data->meta->issued->en)) {
      $january2018 = 1514764800;
      $issued = strtotime($this->data->meta->issued->en);
      if ($issued > 1 && $issued < $january2018) {
        $this->too_old = TRUE;
        return TRUE;
      }
    }
    return FALSE;
  }

  private function preImport() {
  }

  function delete($jfile)
  {
    global $export_root;
    $this->loadData($jfile);

    $this->deleteNode();
    return TRUE; // Success.
  }
}
