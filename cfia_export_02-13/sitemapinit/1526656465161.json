{
    "dcr_id": "1526656464895",
    "title": {
        "en": "Importing food to Canada: general requirements",
        "fr": "Importation d'aliments au Canada : exigences g\u00e9n\u00e9rales"
    },
    "html_modified": "2024-02-13 9:49:14 AM",
    "modified": "2019-12-02",
    "issued": "2018-06-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_imports_general_1526656464895_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_imports_general_1526656464895_fra"
    },
    "ia_id": "1526656465161",
    "parent_ia_id": "1526656151476",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1526656151476",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importing food to Canada: general requirements",
        "fr": "Importation d'aliments au Canada : exigences g\u00e9n\u00e9rales"
    },
    "label": {
        "en": "Importing food to Canada: general requirements",
        "fr": "Importation d'aliments au Canada : exigences g\u00e9n\u00e9rales"
    },
    "templatetype": "content page 1 column",
    "node_id": "1526656465161",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526656151226",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/general-requirements/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-generales/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importing food to Canada: general requirements",
            "fr": "Importation d'aliments au Canada : exigences g\u00e9n\u00e9rales"
        },
        "description": {
            "en": "Importing food to Canada: general requirements",
            "fr": "Importer des aliments au Canada : exigences g\u00e9n\u00e9rales"
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, food, Safe Food for Canadians Regulations, SFCR, import, food products, General requirements, licence, preventive control plan, PCP, traceability",
            "fr": "Agence canadienne d'inspection des aliments, aliments, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, Importations, produits alimentaires, plan de contr\u00f4le pr\u00e9ventif, PCP, exigences"
        },
        "dcterms.subject": {
            "en": "food,food labeling,information,food inspection,agri-food products,regulations,food safety,food processing",
            "fr": "aliment,\u00e9tiquetage des aliments,information,inspection des aliments,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exporation d'aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2019-12-02",
            "fr": "2019-12-02"
        },
        "type": {
            "en": "legislation and regulations,reference material",
            "fr": "l\u00e9gislation et r\u00e8glements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importing food to Canada: general requirements",
        "fr": "Importation d'aliments au Canada : exigences g\u00e9n\u00e9rales"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n\n<p>Requirements to import food into Canada apply to all importers, regardless of the food imported. These requirements include the need for a licence, preventive control plan, traceability documents and recall procedures.</p>\n\n<h2>The basics</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/importing-food/eng/1467924359708/1467924360253\">Fact sheet: Importing food</a> <br>\n</li>\n\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/importing-food-to-canada/eng/1434074882721/1434075033259\">Infographic: 3 Key principles for importers</a> <br>\n</li>\n\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/sfcr-handbook-for-food-businesses/eng/1481560206153/1481560532540\">Understanding the Safe Food for Canadians Regulations: A handbook for food businesses</a><br>\n</li>\n\n<li><a href=\"/eng/1492029195746/1492029286734#a4\">Questions and answers: Importing food</a><br>\n</li>\n</ul>\n\n<h2>Know the requirements</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"/food-licences/trade/eng/1539883860127/1539883860720\">Regulatory requirements: Trade</a><br>\n</li>\n\n<li><a href=\"/food-licences/trade/eng/1539883860127/1539883860720\">Licensing</a><br>\n</li>\n\n<li><a href=\"/preventive-controls/preventive-control-plans/eng/1512152894577/1512152952810\">Preventive control plan</a><br>\n</li>\n\n<li><a href=\"/food-safety-for-industry/traceability/eng/1526651817880/1526651951357\">Traceability for food</a><br>\n</li>\n\n<li><a href=\"/eng/1526657042251/1526657042470\">Food safety and emergency response</a><br>\n</li>\n</ul>\n\n<h2>Related information</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System (AIRS)</a><br>\n</li>\n\n<li><a href=\"/importing-food-plants-or-animals/food-imports/step-by-step-guide/eng/1523979839705/1523979840095\">Importing food: A step-by-step guide</a><br>\n</li>\n\n<li><a href=\"/preventive-controls/preventive-control-plans/for-importers/eng/1480084425374/1480084519065\">Guide for preparing a preventive control plan (PCP)\u00a0\u2013\u00a0For importers</a><br>\n</li>\n\n<li><a href=\"/preventive-controls/preventive-control-plans/pcp-templates/eng/1537244466236/1537244504627\">Preventive control plan (PCP) templates\u00a0\u2013 for importers</a><br>\nTemplates that further illustrate what a PCP includes.\n</li><li><a href=\"/food-safety-for-consumers/bringing-food-into-canada-for-personal-use/eng/1389630031549/1389630282362\">Importing food for personal use</a><br>\n</li>\n\n<li><a href=\"/importing-food-plants-or-animals/food-imports/general-requirements/trade-shows-and-exhibitions/eng/1376454237591/1376454238341\">Importation of food and plant products for trade shows and exhibitions in Canada</a><br>\n</li>\n\n</ul>\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<!--<div class=\"alert alert-info\">\n-->\n\n<p>Les exigences relatives \u00e0 l'importation d'aliments au Canada s'appliquent \u00e0 tous les importateurs, quel que soit l'aliment import\u00e9. Au nombre de ces exigences, mentionnons notamment le besoin d'une licence, d'un plan de contr\u00f4le pr\u00e9ventif, de documents sur la tra\u00e7abilit\u00e9 et de proc\u00e9dures de rappel.</p>\n\n<h2>Notions \u00e9l\u00e9mentaires</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/importation-d-aliments/fra/1467924359708/1467924360253\">Fiche d'information\u00a0: Importation d'aliments</a> <br>\n</li>\n\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/importer-des-aliments-au-canada/fra/1434074882721/1434075033259\">Infographie\u00a0: Trois principes cl\u00e9s \u00e0 l'intention des importateurs</a> <br>\n</li>\n\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/rsac-guide-a-l-intention-des-entreprises-alimentai/fra/1481560206153/1481560532540\">Comprendre le projet de R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\u00a0: Guide \u00e0 l'intention des entreprises alimentaires</a><br>\n</li>\n\n<li><a href=\"/fra/1492029195746/1492029286734#a4\">Questions et r\u00e9ponses\u00a0: Importation d'aliments</a><br>\n</li>\n</ul>\n\n<h2>Conna\u00eetre les exigences</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"/licences-pour-aliments/commerce/fra/1539883860127/1539883860720\">Exigences r\u00e9glementaires\u00a0: Commerce</a><br>\n</li>\n\n<li><a href=\"/licences-pour-aliments/fra/1523876882572/1523876882884\">D\u00e9livrance de licences</a><br>\n</li>\n\n<li><a href=\"/controles-preventifs/plans-de-controle-preventif/fra/1512152894577/1512152952810\">Plan de contr\u00f4le pr\u00e9ventif</a><br>\n</li>\n\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/tracabilite/fra/1526651817880/1526651951357\">Tra\u00e7abilit\u00e9 des aliments</a><br>\n</li>\n\n<li><a href=\"/fra/1526657042251/1526657042470\">Salubrit\u00e9 des aliments et intervention en cas d'urgence</a><br>\n</li>\n</ul>\n\n<h2>Renseignements connexes</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a><br>\n</li>\n\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/guide-par-etape/fra/1523979839705/1523979840095\">Importation d'aliments\u00a0: Un guide par \u00e9tapes</a><br>\n</li>\n\n<li><a href=\"/controles-preventifs/plans-de-controle-preventif/guide-pour-les-importateurs/fra/1480084425374/1480084519065\">Pr\u00e9paration d'un plan de contr\u00f4le pr\u00e9ventif\u00a0\u2013 Guide pour les importateurs</a><br>\n</li>\n\n<li><a href=\"/controles-preventifs/plans-de-controle-preventif/modeles-de-pcp/fra/1537244466236/1537244504627\">Mod\u00e8les de plan de contr\u00f4le pr\u00e9ventif (PCP)\u00a0\u2013 pour les importateurs</a><br>\n</li>\n\n<li><a href=\"/salubrite-alimentaire-pour-les-consommateurs/introduction-au-canada-d-aliments-a-des-fins-perso/fra/1389630031549/1389630282362\">Importation d'aliments pour usage personnel</a><br>\n</li>\n\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-generales/foires-commerciales-ou-d-expositions/fra/1376454237591/1376454238341\">Importation  de produits alimentaires et horticoles pour utilisation lors de foires  commerciales ou d'expositions au Canada</a><br>Guide sur l'importation des produits qui seront distribu\u00e9s uniquement comme \u00e9chantillon ou autre distribution gratuite dans le cadre d'une foire commerciale, d'une exposition ou d'un \u00e9v\u00e9nement semblable. </li>\n</ul>\n\n\n\n\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}