{
    "dcr_id": "1461799560842",
    "title": {
        "en": "Protecting Your Flock From Influenza \u2013 Have You Got It Right?",
        "fr": "Prot\u00e9gez votre troupeau contre l'influenza \u2013 avez-vous fait ce qu'il faut?"
    },
    "html_modified": "2024-03-12 3:57:20 PM",
    "modified": "2022-01-20",
    "issued": "2016-04-29",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_poultry_haveugotitright_1461799560842_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_poultry_haveugotitright_1461799560842_fra"
    },
    "ia_id": "1461801401264",
    "parent_ia_id": "1344748451521",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1344748451521",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Protecting Your Flock From Influenza \u2013 Have You Got It Right?",
        "fr": "Prot\u00e9gez votre troupeau contre l'influenza \u2013 avez-vous fait ce qu'il faut?"
    },
    "label": {
        "en": "Protecting Your Flock From Influenza \u2013 Have You Got It Right?",
        "fr": "Prot\u00e9gez votre troupeau contre l'influenza \u2013 avez-vous fait ce qu'il faut?"
    },
    "templatetype": "content page 1 column",
    "node_id": "1461801401264",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1344748344710",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/protecting-your-flock-from-influenza/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/biosecurite-aviaire/avez-vous-fait-ce-qu-il-faut-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Protecting Your Flock From Influenza \u2013 Have You Got It Right?",
            "fr": "Prot\u00e9gez votre troupeau contre l'influenza \u2013 avez-vous fait ce qu'il faut?"
        },
        "description": {
            "en": "Good biosecurity can help keep disease such as Highly Pathogenic Avian Influenza (HPAI) out of Canadian poultry flocks. Do you know if your biosecurity plan is used correctly?",
            "fr": "Les bonnes pratiques de bios\u00e9curit\u00e9 peuvent aider \u00e0 garder les maladies comme l\u2019influenza aviaire hautement pathog\u00e8ne (IAHP) \u00e0 l\u2019\u00e9cart des troupeaux de volaille canadiens."
        },
        "keywords": {
            "en": "animals, animal health, biosecurity, disease control, Highly Pathogenic Avian Influenza, HPAI, poultry, flocks",
            "fr": "animaux, sant&#233; des animaux, oiseaux, bios&#233;curit&#233;, aviaire, volaille, maladies, avez-vous fait ce qu\u2019il faut?"
        },
        "dcterms.subject": {
            "en": "infectious diseases,animal health",
            "fr": "maladie infectieuse,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Government of Canada,Canadian Food Inspection Agency"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-04-29",
            "fr": "2016-04-29"
        },
        "modified": {
            "en": "2022-01-20",
            "fr": "2022-01-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Protecting Your Flock From Influenza \u2013 Have You Got It Right?",
        "fr": "Prot\u00e9gez votre troupeau contre l'influenza \u2013 avez-vous fait ce qu'il faut?"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Good biosecurity can help keep disease such as <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/eng/1323990856863/1323991018946\">Highly Pathogenic Avian Influenza (HPAI)</a> out of Canadian poultry flocks. Do you know if your biosecurity plan is used correctly? Here are some things to consider:</p> \n\n<h2 class=\"h3\">1.  Is your biosecurity plan the best it can be?</h2>\n\n<p>Review your plan once a year with staff or as needed (<abbr title=\"that is to say\">i.e.</abbr> when new employees start, there is a higher risk of disease, change in protocols <abbr title=\"et cetera\">etc.</abbr>).</p>  \n\n<p>Your review should:</p>\n\n<ul><li>assess the risks;</li>\n<li>find gaps in biosecurity;</li>\n<li>review current risk reduction measures;</li>\n<li>identify how new risks can be reduced; and</li>\n<li>train staff on existing (where needed) and new measures.</li>\n</ul>\n\n<h2 class=\"h3\">2. Are you following your plan?</h2>\n\n<p>Monitor your practices. Are biosecurity measures done right? Are they being done consistently?</p> \n\n<p>Consider the following to strengthen biosecurity:</p>\n\n<ul>\n<li>Ensure you and your staff practice biosecurity consistently and correctly.</li>\n<li>Assess how biosecurity is being practiced on your property and adjust/correct when needed. An assessment can be done all at once, or in manageable pieces. Ensure that management and staff are involved with the assessment. Improve the likelihood that biosecurity issues are not overlooked by assigning different staff to complete the assessment every time.</li> \n<li>Promote constructive and open communication about biosecurity. Encourage staff to talk to you about biosecurity concerns and report issues. It's better to resolve biosecurity issues than to lose birds to disease.</li>\n<li>Encourage staff to develop solutions to address biosecurity issues.</li> \n<li>Ensure staff have the time, resources and supplies needed to put biosecurity practices into place.</li>\n</ul>\n\n<h2 class=\"h3\">3. Are you a leader in biosecurity? </h2>\n\n<p>Providing leadership and support to your staff is important. It is also important that you set an example by practicing biosecurity on your property and adjusting your practices as needed to keep your flock healthy. If you suspect your birds may be sick, contact your veterinarian.</p>\n\n<p>If you find a dead wild bird on or around your property, you are encouraged to contact the <a href=\"http://www.cwhc-rcsf.ca/\">Canadian Wildlife Health Cooperative (CWHC)</a>. The <abbr title=\"Canadian Wildlife Health Cooperative\">CWHC</abbr>, in partnership with the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and other government organizations, tracks the presence and spread of avian influenza.</p>\n\n<p>Your veterinarian, producer association and provincial board are a good source of biosecurity information. Additional information can be accessed at:</p>\n\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/backyard-flocks-and-pet-birds/eng/1323643634523/1323644740109\">How to Prevent and Detect Disease in Backyard Flocks and Pet Birds</a></li> \n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/2014-04-17/eng/1429223102524/1429223103227\">Strengthen On-Farm Biosecurity During Wild Bird Migration</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/eng/1344748344710/1344748451521\">Avian Biosecurity \u2013 Protect Poultry, Prevent Disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/poultry-service-industry/eng/1405696997866/1405697055478\">Poultry Service Industry Biosecurity Guide</a></li>\n</ul>\n\n<p>You can use the following biosecurity assessment tools and checklists when reviewing your program:</p>\n\n<ul>\n<li><a href=\"/eng/1375193894256/1375193980266\">National Avian On-Farm Biosecurity Standard - Animals - Canadian Food Inspection Agency</a></li>\n<li><a href=\"http://www.uspoultry.org/animal_husbandry/assessment.cfm\">Animal Husbandry: Checklist for Self-Assessment of Enhanced Poultry Biosecurity: uspoultry.org</a></li>\n<li><a href=\"http://www.protectmyflock.ca/\">Protect My Flock</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les bonnes pratiques de bios\u00e9curit\u00e9 peuvent aider \u00e0  garder les maladies comme l'<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/fra/1323990856863/1323991018946\">influenza  aviaire hautement pathog\u00e8ne (IAHP)</a> \u00e0 l'\u00e9cart des troupeaux de  volaille canadiens. Savez-vous si votre plan de bios\u00e9curit\u00e9 est appliqu\u00e9 correctement?  Voici quelques \u00e9l\u00e9ments \u00e0 prendre en consid\u00e9ration\u00a0:</p>\n<h2 class=\"h3\">1. Votre plan de bios\u00e9curit\u00e9 est-il perfectible?</h2>\n<p>Passez en revue votre plan avec vos employ\u00e9s une fois par ann\u00e9e ou au besoin (<abbr title=\"par exemple\">p. ex.</abbr>, lorsque de nouveaux employ\u00e9s sont embauch\u00e9s, lorsque le risque de maladie est accru, lorsque les protocoles sont modifi\u00e9s, <abbr title=\"et cetera\">etc.</abbr>).</p>\n\n<p>Vous devriez alors :</p>\n\n<ul>\n<li>\u00e9valuer les risques;</li>\n<li>d\u00e9terminer quelles sont les lacunes en mati\u00e8re de  bios\u00e9curit\u00e9;</li>\n<li>examiner les mesures de r\u00e9duction des risques  actuelles;</li>\n<li>d\u00e9terminer comment r\u00e9duire les nouveaux risques;</li>\n<li>former les employ\u00e9s sur les mesures existantes (au  besoin) et sur les nouvelles mesures.</li>\n</ul>\n<h2 class=\"h3\">2. Appliquez-vous votre plan?</h2>\n<p>  Surveillez  vos pratiques. Les mesures de bios\u00e9curit\u00e9 sont-elles bien appliqu\u00e9es? Sont-elles  appliqu\u00e9es de fa\u00e7on constante?</p>\n<p>Examinez  les points suivants pour renforcer votre plan de bios\u00e9curit\u00e9\u00a0:</p>\n<ul>\n<li>Assurez-vous que vos employ\u00e9s et vous-m\u00eame appliquez  les mesures de bios\u00e9curit\u00e9 de fa\u00e7on constante et correcte.</li>\n<li>D\u00e9terminez comment les mesures de bios\u00e9curit\u00e9 sont  appliqu\u00e9es dans votre exploitation et apportez des ajustements/correctifs au  besoin. Cette \u00e9valuation peut se faire d'un seul coup, ou par \u00e9tapes plus  faciles \u00e0 g\u00e9rer. Assurez-vous que les membres de la direction et du personnel  participent \u00e0 l'\u00e9valuation. R\u00e9duisez le risque que des probl\u00e8mes de bios\u00e9curit\u00e9  ne soient pas remarqu\u00e9s en confiant la t\u00e2che \u00e0 des personnes diff\u00e9rentes d'une  fois \u00e0 l'autre.</li>\n<li>Favorisez les discussions constructives et ouvertes  sur les questions de bios\u00e9curit\u00e9. Encouragez les employ\u00e9s \u00e0 vous parler de  leurs pr\u00e9occupations relativement \u00e0 la bios\u00e9curit\u00e9 et \u00e0 signaler les probl\u00e8mes.  Il est pr\u00e9f\u00e9rable de r\u00e9gler des probl\u00e8mes de bios\u00e9curit\u00e9 que de perdre des  oiseaux \u00e0 cause d'une maladie.</li>\n<li>Encouragez les employ\u00e9s \u00e0 trouver des solutions aux  probl\u00e8mes de bios\u00e9curit\u00e9.</li>\n<li>Veillez \u00e0 ce que les employ\u00e9s aient le temps, les  ressources et les fournitures n\u00e9cessaires \u00e0 l'application des mesures de  bios\u00e9curit\u00e9.</li>\n</ul>\n<h2 class=\"h3\">3. \u00cates-vous un leader en mati\u00e8re de bios\u00e9curit\u00e9?</h2>\n<p>Il est important que vous fassiez preuve de leadership  et que vous appuyiez vos employ\u00e9s. Il est \u00e9galement important que vous donniez l'exemple  en appliquant les mesures de bios\u00e9curit\u00e9 dans votre exploitation et que vous  ajustiez vos pratiques au besoin pour garder votre troupeau en bonne sant\u00e9. Si  vous croyez que vos oiseaux sont malades, communiquez avec votre v\u00e9t\u00e9rinaire.</p>\n<p>Si vous trouvez un oiseau sauvage mort sur vos terres  ou pr\u00e8s de celles-ci, nous vous encourageons \u00e0 communiquer avec le <a href=\"http://fr.cwhc-rcsf.ca/\">R\u00e9seau canadien de la sant\u00e9 de la faune (RCSF)</a>. Le <abbr title=\"R\u00e9seau  canadien de la sant\u00e9 de la faune\">RCSF</abbr>, en partenariat avec l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>  et d'autres organismes gouvernementaux, surveille la pr\u00e9sence et la propagation  de l'influenza aviaire.</p>\n<p>  Votre v\u00e9t\u00e9rinaire, votre association de producteurs et  votre conseil provincial sont de bonnes sources de renseignements sur la  bios\u00e9curit\u00e9. Pour en savoir davantage, consultez les ressources  suivantes\u00a0:</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/petits-elevages-et-oiseaux-de-compagnie/fra/1323643634523/1323644740109\">Comment  pr\u00e9venir et d\u00e9tecter la maladie dans les petits \u00e9levages et chez les oiseaux de  compagnie</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/2015-04-17/fra/1429223102524/1429223103227\">Renforcer  la bios\u00e9curit\u00e9 \u00e0 la ferme pendant la p\u00e9riode de migration des oiseaux sauvages</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/biosecurite-aviaire/fra/1344748344710/1344748451521\">Bios\u00e9curit\u00e9  aviaire\u00a0\u2013 Protection de la volaille et pr\u00e9vention des maladies</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/fournisseurs-de-services-de-l-industrie-de-la-vola/fra/1405696997866/1405697055478\">Guide  de bios\u00e9curit\u00e9 pour les fournisseurs de services \u00e0 l'industrie de la volaille.</a></li>\n</ul>\n<p>Vous pouvez utiliser les listes de contr\u00f4le et outils  suivants sur l'\u00e9valuation de la bios\u00e9curit\u00e9 au moment d'examiner votre  programme\u00a0:</p>\n<ul>\n<li><a href=\"/fra/1375193894256/1375193980266\">Norme  nationale de bios\u00e9curit\u00e9 pour les fermes avicoles\u00a0\u2013 Animaux \u2013\u00a0Agence  canadienne d'inspection des aliments</a></li>\n<li><a href=\"http://www.uspoultry.org/animal_husbandry/assessment.cfm\">Liste de contr\u00f4le pour l'auto-\u00e9valuation des mesures  de bios\u00e9curit\u00e9 accrue relatives aux volailles, en anglais seulement, sur le  site uspoultry.org (<span lang=\"en\">Animal  Husbandry: Checklist for Self-Assessment of Enhanced Poultry Biosecurity</span>) </a></li>\n<li><a href=\"http://www.protectmyflock.ca/\" lang=\"en\">Protect  My Flock</a> (en anglais seulement)</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}