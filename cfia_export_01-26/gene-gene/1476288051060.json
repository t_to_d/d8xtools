{
    "dcr_id": "1476288051060",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Jacobaea vulgaris</i> (Tansy ragwort)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Jacobaea vulgaris</i> (S\u00e9ne\u00e7on jacob\u00e9e)"
    },
    "html_modified": "2024-01-26 2:23:26 PM",
    "modified": "2017-11-01",
    "issued": "2017-11-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_jacobaea_vulgaris_1476288051060_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_jacobaea_vulgaris_1476288051060_fra"
    },
    "ia_id": "1476288051528",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Jacobaea vulgaris</i> (Tansy ragwort)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Jacobaea vulgaris</i> (S\u00e9ne\u00e7on jacob\u00e9e)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Jacobaea vulgaris</i> (Tansy ragwort)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Jacobaea vulgaris</i> (S\u00e9ne\u00e7on jacob\u00e9e)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476288051528",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/jacobaea-vulgaris/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/jacobaea-vulgaris/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Jacobaea vulgaris (Tansy ragwort)",
            "fr": "Semence de mauvaises herbe : Jacobaea vulgaris (S\u00e9ne\u00e7on jacob\u00e9e)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Jacobaea vulgaris, Asteraceae, Tansy ragwort",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Jacobaea vulgaris, Asteraceae, S\u00e9ne\u00e7on jacob\u00e9e"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Jacobaea vulgaris (Tansy ragwort)",
        "fr": "Semence de mauvaises herbe : Jacobaea vulgaris (S\u00e9ne\u00e7on jacob\u00e9e)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Common Name</h2>\n<p>Tansy ragwort</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Manitoba\">MB</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Newfoundland and Labrador\">NL</abbr>, <abbr title=\"Nova Scotia\">NS</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Prince Edward Island\">PE</abbr>, <abbr title=\"Quebec\">QC</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, Europe and temperate Asia and introduced in North America, China, Australia, New Zealand, and beyond its native range in Europe (Finland, Norway, Russia)  (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). In the United States, it occurs mostly in the west and northeastern regions (Kartesz 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Short-lived perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Achene length: 1.3 - 2.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width: 0.4 - 1.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene elongate; tapered at base and top is truncate</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Achene surface has a satin sheen with a roughened surface</li>\n<li>Achene is ribbed, with short hairs in the grooves</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene is light yellow to golden brown with white-haired furrows</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>A long, white pappus sometimes present on immature achenes</li>\n<li>Achenes are dimorphic; the uncommon outer fruits are hairless</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Pastures, hay fields, old fields, roadsides, railway lines and disturbed areas (Bain 1991<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). Not a known weed of crops, however a problem in pastures due to toxicity (Royer and Dickinson 1999<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Tansy ragwort is thought to have been introduced into Canada in the 1850s in ships' ballast (Bain 1991<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). Tansy ragwort contains alkaloids which are toxic to cattle, deer, horses and goats. A single plant can produce as many as 150,000 seeds, which can remain viable for over 20 years  (Ministry of Agriculture, Food, and Fisheries 2002<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote\u00a0</span>7</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Common groundsel (<i lang=\"la\">Senecio vulgaris</i>)</h3>\n<ul>\n<li>Common groundsel has a similar size, cylindrical shape, longitudinal ribs and short surface hairs as tansy ragwort.</li>\n\n<li>Common groundsel (length: 2.8 - 3.0 <abbr title=\"millimetres\">mm</abbr>) is generally longer, a darker colour and the surface hairs are more dense than tansy ragwort.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_05cnsh_1475606323517_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Tansy ragwort (<i lang=\"la\">Jacobaea vulgaris</i>) achenes \n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_01cnsh_1475606091675_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Tansy ragwort (<i lang=\"la\">Jacobaea vulgaris</i>) achene \n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_02cnsh_1475606222447_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Tansy ragwort (<i lang=\"la\">Jacobaea vulgaris</i>) achene \n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_03cnsh_1475606255898_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Tansy ragwort (<i lang=\"la\">Jacobaea vulgaris</i>) achene, collar (top-down view)\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_04cnsh_1475606298834_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Tansy ragwort (<i lang=\"la\">Jacobaea vulgaris</i>) achene, collar (side view)\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_vulgaris_01cnsh_1475606459169_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Common groundsel (<i lang=\"la\">Senecio vulgaris</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_vulgaris_02cnsh_1475606476195_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Common groundsel (<i lang=\"la\">Senecio vulgaris</i>) achenes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_vulgaris_05cnsh_1475606493062_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Common groundsel (<i lang=\"la\">Senecio vulgaris</i>) achene, close-up of surface\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, N.C., www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Bain, J. F. 1991</strong>. The biology of Canadian weeds. 96. <i lang=\"la\">Senecio jacobaea</i> L. Canadian Journal of Plant Science 71: 127-140.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Royer, F. and Dickinson, R. 1999</strong>. Weeds of Canada and the Northern United States. The University of Alberta Press/Lone Pine Publishing, Edmonton, Alberta.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 7</dt>\n<dd id=\"fn7\">\n<p><strong>Ministry of Agriculture, Food, and Fisheries. 2002</strong>. Guide to the Weeds in British Columbia,https://www.for.gov.bc.ca/hra/Plants/weedsbc/GuidetoWeeds.pdf [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>7<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Nom commun</h2>\n<p>S\u00e9ne\u00e7on jacob\u00e9e</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> L'esp\u00e8ce est pr\u00e9sente en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, au <abbr title=\"Manitoba\">Man.</abbr>, au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr>, \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr>, en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr>, \u00e0 l'<abbr title=\"\u00cele-du-Prince-\u00c9douard\">\u00ce.-P.-\u00c9.</abbr> et au <abbr title=\"Qu\u00e9bec\">Qc</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Indig\u00e8ne de l'Afrique septentrionale, d'Europe et de l'Asie temp\u00e9r\u00e9e et introduite en Am\u00e9rique du Nord, en Chine, en Australie, en Nouvelle-Z\u00e9lande et \u00e0 l'ext\u00e9rieur de son aire d'indig\u00e9nat en Europe (Finlande, Norv\u00e8ge, Russie) (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Aux \u00c9tats-Unis, elle est surtout pr\u00e9sente dans les r\u00e9gions de l'Ouest et du Nord-Est (Kartesz, 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace de courte p\u00e9rennit\u00e9</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'ak\u00e8ne\u00a0:1,3 \u00e0 2,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de l'ak\u00e8ne\u00a0: 0,4 \u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne allong\u00e9 \u00e0 la base r\u00e9tr\u00e9cie et au sommet tronqu\u00e9</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>La surface de l'ak\u00e8ne est rugueuse et a un lustre satin\u00e9</li>\n<li>Ak\u00e8ne c\u00f4tel\u00e9, avec de courts poils dans les sillons</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne de couleur jaune clair \u00e0 brun dor\u00e9 avec des poils blancs dans les sillons</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Long pappus blanc parfois pr\u00e9sent sur les ak\u00e8nes immatures</li>\n<li>Les ak\u00e8nes sont dimorphes; les rares fruits externes sont glabres</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>P\u00e2turages, pr\u00e9s de fauche, champs abandonn\u00e9s, bords de chemin, voies ferr\u00e9es et terrains perturb\u00e9s (Bain, 1991<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>; Darbyshire, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>). Pas  une mauvaise herbe qu'on retrouve dans les cultures, mais peut causer des probl\u00e8mes dans les p\u00e2turages en raison de sa toxicit\u00e9 (Royer et Dickinson 1999<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>On croit que le s\u00e9ne\u00e7on jacob\u00e9e aurait \u00e9t\u00e9 introduit au Canada par des eaux de ballast de navire dans les ann\u00e9es 1850 (Bain, 1991<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>). La plante contient des alcalo\u00efdes toxiques pour les bovins, les cerfs, les chevaux et les ch\u00e8vres. Une seule plante peut produire jusqu'\u00e0 150\u00a0000 graines qui peuvent demeurer viables plus de 20 ans (Ministry of Agriculture, Food, and Fisheries, 2002<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>7</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>S\u00e9ne\u00e7on vulgaire (<i lang=\"la\">Senecio vulgaris</i>)</h3>\n<ul>\n<li>L'ak\u00e8ne du s\u00e9ne\u00e7on vulgaire ressemble \u00e0 celui du s\u00e9ne\u00e7on jacob\u00e9e par ses dimensions, sa forme cylindrique, ses c\u00f4tes longitudinales et sa surface courtement pubescente.</li>\n\n<li>L'ak\u00e8ne du s\u00e9ne\u00e7on vulgaire (longueur : 2,8 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr>) est g\u00e9n\u00e9ralement plus long, plus fonc\u00e9 et plus dens\u00e9ment pubescent \u00e0 la surface que le s\u00e9ne\u00e7on jacob\u00e9e.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_05cnsh_1475606323517_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>S\u00e9ne\u00e7on jacob\u00e9e (<i lang=\"la\">Jacobaea vulgaris</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_01cnsh_1475606091675_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>S\u00e9ne\u00e7on jacob\u00e9e (<i lang=\"la\">Jacobaea vulgaris</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_02cnsh_1475606222447_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>S\u00e9ne\u00e7on jacob\u00e9e (<i lang=\"la\">Jacobaea vulgaris</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_03cnsh_1475606255898_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>S\u00e9ne\u00e7on jacob\u00e9e (<i lang=\"la\">Jacobaea vulgaris</i>) ak\u00e8ne, le collet (vue du dessus)\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_jacobaea_04cnsh_1475606298834_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>S\u00e9ne\u00e7on jacob\u00e9e (<i lang=\"la\">Jacobaea vulgaris</i>) ak\u00e8ne, le collet (vue de c\u00f4t\u00e9)\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_vulgaris_01cnsh_1475606459169_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : S\u00e9ne\u00e7on vulgaire (<i lang=\"la\">Senecio vulgaris</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_vulgaris_02cnsh_1475606476195_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : S\u00e9ne\u00e7on vulgaire (<i lang=\"la\">Senecio vulgaris</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_senecio_vulgaris_05cnsh_1475606493062_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : S\u00e9ne\u00e7on vulgaire (<i lang=\"la\">Senecio vulgaris</i>) ak\u00e8ne, vue agrandie de la surface\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, N.C., www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Bain, J. F. 1991</strong>. The biology of Canadian weeds. 96. <i lang=\"la\">Senecio jacobaea</i> L. Canadian Journal of Plant Science 71: 127-140.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Royer, F. and Dickinson, R. 1999</strong>. Weeds of Canada and the Northern United States. The University of Alberta Press/Lone Pine Publishing, Edmonton, Alberta.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n\n<dt>Note de bas de page 7</dt>\n<dd id=\"fn7\">\n<p lang=\"en\"><strong>Ministry of Agriculture, Food, and Fisheries. 2002</strong>. Guide to the Weeds in British Columbia, https://www.for.gov.bc.ca/hra/Plants/weedsbc/GuidetoWeeds.pdf [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>7</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}