{
    "dcr_id": "1519653560052",
    "title": {
        "en": "Costa Rica - Export requirements for milk and dairy products",
        "fr": "Costa Rica - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "html_modified": "2024-03-12 3:57:57 PM",
    "modified": "2018-03-09",
    "issued": "2018-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_costa_rica_1519653560052_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_costa_rica_1519653560052_fra"
    },
    "ia_id": "1519653560551",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Costa Rica - Export requirements for milk and dairy products",
        "fr": "Costa Rica - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "label": {
        "en": "Costa Rica - Export requirements for milk and dairy products",
        "fr": "Costa Rica - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1519653560551",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/costa-rica-milk-and-dairy-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/costa-rica-lait-et-produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Costa Rica - Export requirements for milk and dairy products",
            "fr": "Costa Rica - Exigences d'exportation pour le lait et les produits laitiers"
        },
        "description": {
            "en": "Countries to which exports are currently made \u2013 Costa Rica",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013  Costa Rica"
        },
        "keywords": {
            "en": "Costa Rica, Export requirements, milk and dairy products",
            "fr": "Costa Rica, Exigences d'exportation, lait et produits laitiers"
        },
        "dcterms.subject": {
            "en": "exports,agri-food industry,dairy products",
            "fr": "exportation,industrie agro-alimentaire,produit laitier"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exportation d'aliments et de la protect. des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "modified": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Costa Rica - Export requirements for milk and dairy products",
        "fr": "Costa Rica - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Eligible/ineligible product</h2>\n<h3>Eligible</h3>\n<ul>\n<li>All dairy products</li>\n</ul>\n<h2>Pre-export approvals by the competent authority of the importing country</h2>\n<h3>Establishments</h3>\n<ul>\n<li>The Costa Rican authorities have been provided a list of Canadian registered establishments</li>\n<li>The exporter must verify with the importer that the product being shipped comes from an establishment on this list.</li>\n<li>To amend this list provided to the Costa Rican authorities the manufacturer must contact the <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Canadian Food Inspection Agency (CFIA) area office</a> and complete the document following document:\n<ul>\n<li class=\"mrgn-tp-md\">Request for addition to the Canadian dairy establishments list sent to third countries</li>\n</ul>\n</li>\n</ul>\n<h2>Documentation requirements</h2>\n<h3>Certificate</h3>\n<ul>\n<li>Health certificate for the export of dairy products and dairy based products for human consumption</li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Produits admissibles/non admissibles</h2>\n<h3>Admissibles</h3>\n<ul>\n<li>Tous les produits laitiers</li>\n</ul>\n<h2>Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n<h3>\u00c9tablissements</h3>\n<ul>\n<li>Les autorit\u00e9s du Costa Rica ont la liste des \u00e9tablissements enregistr\u00e9s f\u00e9d\u00e9ral au Canada</li>\n<li>L'exportateur doit v\u00e9rifier avec l'importateur que le produit qui sera export\u00e9 provient d'un \u00e9tablissement qui est sur cette liste.</li>\n<li>Pour modifier cette liste fournie aux autorit\u00e9s du Costa Rica, le fabricant doit communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'Agence canadienne d'inspection des aliments (ACIA)</a> et compl\u00e9ter le document \u00e0 la suite du document\u00a0:\n<ul>\n<li class=\"mrgn-tp-md\">Demande d'ajout \u00e0 la liste des \u00e9tablissements laitiers canadiens envoy\u00e9e \u00e0 des pays tiers</li>\n</ul>\n</li>\n</ul>\n<h2>Documents requis</h2>\n<h3>Certificat</h3>\n<ul>\n<li>Certificat sanitaire pour l'exportation de produits laitiers et d'aliments lact\u00e9s pour consommation humaine</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}