{
    "dcr_id": "1495143860550",
    "title": {
        "en": "Ginseng anthracnose - <i lang=\"la\">Colletotrichum panacicola</i>",
        "fr": "Anthracnose du ginseng - <i lang=\"la\">Colletotrichum panacicola</i>"
    },
    "html_modified": "2024-01-26 2:23:40 PM",
    "modified": "2017-05-18",
    "issued": "2017-05-24",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/colletotrichum_panacicola_1495143860550_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/colletotrichum_panacicola_1495143860550_fra"
    },
    "ia_id": "1495143913483",
    "parent_ia_id": "1322807340310",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1322807340310",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Ginseng anthracnose - <i lang=\"la\">Colletotrichum panacicola</i>",
        "fr": "Anthracnose du ginseng - <i lang=\"la\">Colletotrichum panacicola</i>"
    },
    "label": {
        "en": "Ginseng anthracnose - <i lang=\"la\">Colletotrichum panacicola</i>",
        "fr": "Anthracnose du ginseng - <i lang=\"la\">Colletotrichum panacicola</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1495143913483",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1322807235798",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/ginseng-anthracnose/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/anthracnose-du-ginseng/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Ginseng anthracnose - Colletotrichum panacicola",
            "fr": "Anthracnose du ginseng - Colletotrichum panacicola"
        },
        "keywords": {
            "en": "Diseases, Ginseng anthracnose, Colletotrichum panacicola, Panax species",
            "fr": "maladie, Anthracnose du ginseng, Colletotrichum panacicola, Panax esp\u00e8ce"
        },
        "dcterms.subject": {
            "en": "pests,plant diseases,plants,inspection",
            "fr": "organisme nuisible,maladie des plantes,plante,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-05-24",
            "fr": "2017-05-24"
        },
        "modified": {
            "en": "2017-05-18",
            "fr": "2017-05-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Ginseng anthracnose - Colletotrichum panacicola",
        "fr": "Anthracnose du ginseng - Colletotrichum panacicola"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The importation of ginseng (<i lang=\"la\">Panax</i> <abbr title=\"species\">spp.</abbr>) is regulated under the <i>Plant Protection Act</i> to prevent the introduction of ginseng anthracnose (<i lang=\"la\">Colletotrichum panacicola</i>), a disease which causes serious damage in Asia. The disease is not known to occur in Canada or the United States. A pest risk assessment indicates <i lang=\"la\"><abbr title=\"Colletotrichum\">C.</abbr> panacicola</i> is transmitted by propagative material, including seed.</p>\n<h2>What information is available?</h2>\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-94-25/eng/1320027229785/1320027323543\">D-94-25: Plant protection import requirements for ginseng (<i lang=\"la\">Panax</i> <abbr title=\"species\">spp.</abbr>)</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/pest-risk-management/rmd-11-04/eng/1323849186596/1323849425983\"><abbr title=\"Risk Management Document\">RMD</abbr>-11-04: Record of Decision to Permit the Importation of <i lang=\"la\">Panax</i> <abbr title=\"species\">spp.</abbr> (ginseng) seed from Korea</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La <i>Loi sur la protection des v\u00e9g\u00e9taux</i> r\u00e9git l'importation de ginseng (<i lang=\"la\">Panax</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>) afin de pr\u00e9venir l'introduction de l'anthracnose du ginseng (<i lang=\"la\">Colletotrichum panacicola</i>), qui cause de graves dommages en Asie, mais qui n'est pas r\u00e9put\u00e9e pr\u00e9sente au Canada ni aux \u00c9tats-Unis. Selon une \u00e9valuation des risques, <i lang=\"la\"><abbr title=\"Colletotrichum\">C.</abbr> panacicola</i> se transmet par le mat\u00e9riel de propagation, y compris les semences.</p>\n<h2>Quelles informations sont disponibles?</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-94-25/fra/1320027229785/1320027323543\">D-94-25\u00a0: Exigences phytosanitaires d'importation de ginseng (<i lang=\"la\">Panax</i> <abbr title=\"esp\u00e8ce\">spp.</abbr>)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/gestion-des-risques-phytosanitaire/dgr-11-04/fra/1323849186596/1323849425983\"><abbr title=\"Document de gestion du risque\">DGR</abbr>-11-04\u00a0: Compte rendu de d\u00e9cision concernant la permission d'importation de semences de <i lang=\"la\">Panax</i> <abbr title=\"esp\u00e8ce\">spp.</abbr> (ginseng) de la Cor\u00e9e</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}