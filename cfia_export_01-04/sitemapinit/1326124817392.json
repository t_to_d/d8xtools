{
    "dcr_id": "1326124573039",
    "title": {
        "en": "Citrus long-horned beetle \u2013 Anoplophora chinensis",
        "fr": "Longicorne des agrumes \u2013 Anoplophora chinensis"
    },
    "html_modified": "2012-01-09 11:38",
    "modified": "9-7-2019",
    "issued": "9-1-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_anochi_index_1326124573039_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_anochi_index_1326124573039_fra"
    },
    "parent_ia_id": "1307078272806",
    "ia_id": "1326124817392",
    "parent_node_id": "1307078272806",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Citrus long-horned beetle \u2013 Anoplophora chinensis",
        "fr": "Longicorne des agrumes \u2013 Anoplophora chinensis"
    },
    "label": {
        "en": "Citrus long-horned beetle \u2013 Anoplophora chinensis",
        "fr": "Longicorne des agrumes \u2013 Anoplophora chinensis"
    },
    "templatetype": "content page 1 column",
    "node_id": "1326124817392",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1307077188885",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/citrus-long-horned-beetle/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-des-agrumes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Citrus long-horned beetle \u2013 Anoplophora chinensis",
            "fr": "Longicorne des agrumes \u2013 Anoplophora chinensis"
        },
        "description": {
            "en": "The citrus long-horned beetle is a wood-boring insect that is native to Asia. The larvae of this beetle can kill trees directly, by damaging the tree, or indirectly, by enabling other pests and diseases to affect the tree.",
            "fr": "Le longicorne des agrumes est un col\u00e9opt\u00e8re xylophage (qui se nourrit de bois) originaire d'Asie, dont les larves peuvent tuer les arbres"
        },
        "keywords": {
            "en": "Anoplophora chinensis, Citrus long-horned beetle, wood-boring beetle, plant pest",
            "fr": "Anoplophora chinensis, longicorne des agrumes, buprestid\u00e9s, phytoravageur"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-09 11:38:03",
            "fr": "2012-01-09 11:38:03"
        },
        "modified": {
            "en": "2019-07-09",
            "fr": "2019-07-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Citrus long-horned beetle \u2013 Anoplophora chinensis",
        "fr": "Longicorne des agrumes \u2013 Anoplophora chinensis"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<div class=\"col-sm-2\">\n<p><img alt=\"Citrus long-horned beetle\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_index_image1_1326122717624_eng.jpg\"></p></div>\n<div class=\"col-sm-10\">\n<p>The citrus long-horned beetle is a wood-boring insect that is native to Asia. The larvae of this beetle can kill trees:</p>\n\n<ul>\n<li>directly, by damaging the tree</li>\n<li>indirectly, by enabling other pests and diseases to affect the tree</li>\n</ul></div>\n</div>\n\n<p>The beetle gets its name from the damage caused to citrus groves in its native China. However, it feeds on and damages a wide variety of hardwood trees, including many important species found in Canada.</p>\n<p>This beetle has been intercepted once entering Canada. However, it has never been found in this country. The citrus long-horned beetle has been introduced on several occasions into Europe, and has been detected in and subsequently eradicated from the United States.</p>\n\n<h2>What information is available</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/citrus-long-horned-beetle/fact-sheet/eng/1326132014538/1326132291047\">Pest fact sheet</a></li>\n</ul>\n\n<h3>Policy Directives</h3>\n                                \n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/d-11-01/eng/1322607792840/1322607917651\">D-11-01: Phytosanitary Requirements for Plants for Planting and Fresh Branches to Prevent the Entry and Spread of <i lang=\"la\">Anoplophora</i> <abbr title=\"species\">spp.</abbr></a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-08-04/eng/1323752901318/1323753612811\">D-08-04: Plant protection import requirements for plants and plant parts for planting</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-sm-2\">\n<p><img alt=\"Longicorne des agrumes\" width=\"160\" class=\"img-responsive\" height=\"120\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anochi_index_image1_1326122717624_fra.jpg\"></p></div>\n<div class=\"col-sm-10\">\n<p>Le longicorne des agrumes est un col\u00e9opt\u00e8re xylophage (qui se nourrit de bois) originaire d'Asie, dont les larves peuvent tuer les arbres\u00a0:</p>\n<ul>\n<li>de fa\u00e7on directe, en leur causant des dommages;</li>\n<li>de fa\u00e7on indirecte, en permettant \u00e0 d'autres organismes nuisibles ou maladies de les attaquer.</li>\n</ul></div>\n</div>\n\n<p>Le col\u00e9opt\u00e8re tire son nom du fait qu'il cause des dommages aux vergers d'agrumes dans son pays d'origine, la Chine. Toutefois, il peut consommer et endommager une grande vari\u00e9t\u00e9 d'arbres feuillus, y compris de nombreuses essences importantes pr\u00e9sentes au Canada.</p>\n<p>Le longicorne des agrumes a \u00e9t\u00e9 intercept\u00e9 une seule fois au Canada, au moment de son arriv\u00e9e. Il n'a toutefois jamais \u00e9t\u00e9 signal\u00e9 au pays. Le col\u00e9opt\u00e8re a \u00e9t\u00e9 introduit \u00e0 plusieurs reprises en Europe. Il a \u00e9t\u00e9 d\u00e9tect\u00e9 aux \u00c9tats-Unis et par la suite \u00e9radiqu\u00e9.</p>\n\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-des-agrumes/fiche-de-renseignements/fra/1326132014538/1326132291047\">Fiche des renseignements</a></li>\n</ul>\n\n<h3>Directives sur la protection des v\u00e9g\u00e9taux</h3>\n              \n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-11-01/fra/1322607792840/1322607917651\">D-11-01\u00a0: Exigences phytosanitaires visant les v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation et les rameaux d\u00e9coratifs frais afin de pr\u00e9venir l'introduction et la diss\u00e9mination des insectes appartenant au genre <i lang=\"la\">Anoplophora</i> <abbr title=\"esp\u00e8ce\">spp.</abbr></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-08-04/fra/1323752901318/1323753612811\">D-08-04\u00a0: Exigences phytosanitaire r\u00e9gissant l'importation de v\u00e9g\u00e9taux et de parties de v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}