{
    "dcr_id": "1466643835510",
    "title": {
        "en": "Trial importation periods for fresh fruits and vegetables",
        "fr": "P\u00e9riodes d'importation \u00e0 titre d'essai pour les fruits et l\u00e9gumes frais"
    },
    "html_modified": "2024-01-26 2:23:17 PM",
    "modified": "2023-11-22",
    "issued": "2016-07-04",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/protect_directives_d-95-08_app5_1466643835510_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/protect_directives_d-95-08_app5_1466643835510_fra"
    },
    "ia_id": "1466643911736",
    "parent_ia_id": "1299167874884",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299167874884",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Trial importation periods for fresh fruits and vegetables",
        "fr": "P\u00e9riodes d'importation \u00e0 titre d'essai pour les fruits et l\u00e9gumes frais"
    },
    "label": {
        "en": "Trial importation periods for fresh fruits and vegetables",
        "fr": "P\u00e9riodes d'importation \u00e0 titre d'essai pour les fruits et l\u00e9gumes frais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1466643911736",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299167784070",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/horticulture/importing-and-horticulture/",
        "fr": "/protection-des-vegetaux/horticulture/importation-et-horticulture/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Trial importation periods for fresh fruits and vegetables",
            "fr": "P\u00e9riodes d'importation \u00e0 titre d'essai pour les fruits et l\u00e9gumes frais"
        },
        "description": {
            "en": "This document is Appendix 5 of directive D-95-08: Phytosanitary import requirements for fresh temperate fruits and tree nuts.",
            "fr": "Ce document est l'annexe 5 de la directive D-95-08 : Exigences phytosanitaires d'importation pour les fruits temp\u00e9r\u00e9s frais et les noix."
        },
        "keywords": {
            "en": "packing facility, production site, fruit material, quarantine pests, import requirements, phytosanitary import requirements, fresh temperate fruits, tree nuts, trial importation periods",
            "fr": "organismes nuisibles, lieu de production, mat\u00e9riel fruitier, \u00e9tablissement d'emballage, ONPV du pays exportateur, p\u00e9riodes d'importation \u00e0 titre d'essai, fruits frais"
        },
        "dcterms.subject": {
            "en": "fruits,imports,inspection",
            "fr": "fruit,importation,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-07-04",
            "fr": "2016-07-04"
        },
        "modified": {
            "en": "2023-11-22",
            "fr": "2023-11-22"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Trial importation periods for fresh fruits and vegetables",
        "fr": "P\u00e9riodes d'importation \u00e0 titre d'essai pour les fruits et l\u00e9gumes frais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Last updated: <span class=\"nowrap\">November 20, 2023</span></p>\n\n<p>This document replaces Appendix\u00a05 of directive <a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08: Phytosanitary import requirements for fresh temperate fruits and tree nuts</a>, and incorporates trial importation periods for fresh vegetables.</p> \n\n<p>The following trial importation periods for fresh fruits and vegetables are currently in effect and will remain in place until further notice.</p>\n\n<p>All articles that have been authorized entry into Canada under a trial importation period require a <a href=\"/plant-health/invasive-species/plant-import/eng/1324569244509/1324569331710\">Permit to Import</a> issued by the CFIA. The Canadian importer must obtain the Permit to Import before the consignment leaves the country of origin.</p>\n\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<th class=\"active\">Scientific name</th> \n<th class=\"active\">Common name</th>\n<th class=\"active\">Country of origin</th>\n<th class=\"active\">Applicable CFIA program directive</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td><i lang=\"la\">Malus</i> spp.</td>\n<td>apples</td>\n<td>Germany, Japan, People's Republic of China: provinces of Hebei, Henan, Shanxi</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Prunus domestica</i></td>\n<td>plums</td>\n<td>Republic of Moldova</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n\n<tr>\n<td><i lang=\"la\">Pyrus</i> spp.</td>\n<td>pears</td>\n<td>Belgium, People's Republic of China: province of Shanxi</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Rubus</i> spp.</td>\n<td>raspberries, blackberries, boysenberries, loganberries</td>\n<td>Peru</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Solanum lycopersicum</i></td>\n<td>tomatoes</td>\n<td>Republic of Korea</td>\n<td><a href=\"/plant-health/invasive-species/directives/horticulture/d-10-01/eng/1304622464578/1312239593183\">D-10-01</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Vaccinium angustifolium</i>, <i lang=\"la\">V.\u00a0corymbosum</i> and <i lang=\"la\">V.\u00a0myrtilloides</i></td>\n<td>blueberries</td>\n<td>Colombia, Guatemala, Morocco, South Africa, Spain</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Vaccinium</i>\u00a0spp. other than\n<i lang=\"la\">V.\u00a0angustifolium</i>,\n<i lang=\"la\">V.\u00a0corymbosum</i>,\n<i lang=\"la\">V.\u00a0macrocarpon</i>,\n<i lang=\"la\">V.\u00a0myrtilloides</i> and\n<i lang=\"la\">V.\u00a0oxycoccos</i></td>\n<td>bilberries, lingonberries, etc.</td>\n<td>South Africa</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Vitis</i> spp.</td>\n<td>grapes</td>\n<td>Egypt</td>\n<td><a href=\"/plant-health/invasive-species/directives/date/d-95-08/eng/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Derni\u00e8re mise \u00e0 jour\u00a0: <span class=\"nowrap\">20 novembre 2023</span></p>\n\n<p>Ce document remplace l'annexe 5 de la directive <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08\u00a0: Exigences phytosanitaires d'importation pour les fruits temp\u00e9r\u00e9s frais et les noix</a>, et int\u00e8gre des p\u00e9riodes d'importation \u00e0 titre d'essai pour les l\u00e9gumes frais.</p> \n\n<p>Les p\u00e9riodes d'importation \u00e0 titre d'essai suivantes pour les fruits et l\u00e9gumes frais sont pr\u00e9sentement en vigueur et demeureront en place jusqu'\u00e0 nouvel ordre.</p>\n\t\n<p>Tous les articles qui ont \u00e9t\u00e9 autoris\u00e9s \u00e0 entrer au Canada dans le cadre d'une p\u00e9riode d'importation \u00e0 titre d'essai n\u00e9cessitent un permis d'importation d\u00e9livr\u00e9 par l'ACIA. L'importateur canadien doit obtenir le <a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/fra/1324569244509/1324569331710\">permis d'importation</a> avant que l'envoi ne quitte le pays d'origine.</p>\n\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<th class=\"active\">Nom scientifique</th> \n<th class=\"active\">Nom commun</th>\n<th class=\"active\">Pays d'origine</th>\n<th class=\"active\">Directive du programme de l'ACIA applicable</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td><i lang=\"la\">Malus</i> spp.</td>\n<td>pommes</td>\n<td>Allemagne, Japon, R\u00e9publique populaire de Chine\u00a0: provinces de Hebei, Henan, Shanxi</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Prunus domestica</i></td>\n<td>Prunes</td>\n<td>R\u00e9publique de la Moldavie</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Pyrus</i> spp.</td>\n<td>poires</td>\n<td>Belgique, R\u00e9publique populaire de Chine\u00a0: province de Shanxi</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Rubus</i> spp.</td>\n<td>framboises, m\u00fbres, m\u00fbres de Logan, m\u00fbres de Boysen</td>\n<td>P\u00e9rou</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Solanum lycopersicum</i></td>\n<td>tomates</td>\n<td>R\u00e9publique de Cor\u00e9e</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-10-01/fra/1304622464578/1312239593183\">D-10-01</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Vaccinium angustifolium</i>, <i lang=\"la\">V. ashei V.\u00a0corymbosum</i> et <i lang=\"la\">V.\u00a0myrtilloides</i></td>\n<td>bleuets</td>\n<td>Afrique du Sud, Colombie, Espagne, Guatemala, Maroc</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Vaccinium</i>\u00a0spp. autre que\n<i lang=\"la\">V.\u00a0angustifolium</i>,\n<i lang=\"la\">V.\u00a0corymbosum</i>,\n<i lang=\"la\">V.\u00a0macrocarpon</i>,\n<i lang=\"la\">V.\u00a0myrtilloides</i> et\n<i lang=\"la\">V.\u00a0oxycoccos</i></td>\n<td>par exemple, myrtilles, airelles vigne d'Ida</td>\n<td>Afrique du Sud</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n<tr>\n<td><i lang=\"la\">Vitis</i> spp.</td>\n<td>raisins</td>\n<td>\u00c9gypte</td>\n<td><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-08/fra/1322413085880/1322413348292\">D-95-08</a></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}