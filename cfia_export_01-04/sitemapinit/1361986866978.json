{
    "dcr_id": "1361986810905",
    "title": {
        "en": "Forward Regulatory Plan",
        "fr": "Plan prospectif de la r\u00e9glementation"
    },
    "html_modified": "2015-03-31 11:01",
    "modified": "16-5-2023",
    "issued": "31-3-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_forward_regs_1361986810905_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_forward_regs_1361986810905_fra"
    },
    "parent_ia_id": "1299847442232",
    "ia_id": "1361986866978",
    "parent_node_id": "1299847442232",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Forward Regulatory Plan",
        "fr": "Plan prospectif de la r\u00e9glementation"
    },
    "label": {
        "en": "Forward Regulatory Plan",
        "fr": "Plan prospectif de la r\u00e9glementation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1361986866978",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1299846777345",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/acts-and-regulations/forward-regulatory-plan/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/plan-prospectif-de-la-reglementation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Forward Regulatory Plan",
            "fr": "Plan prospectif de la r\u00e9glementation"
        },
        "description": {
            "en": "The CFIA's forward regulatory plan for 2019-2021",
            "fr": "Le plan prospectif de la r\u00e8glementation se pr\u00e9sente sous la forme d'une liste ou d'une description, accessible au public, qui r\u00e9pertorie les modifications r\u00e9glementaires qu'un minist\u00e8re entend, effectivement ou potentiellement, apporter dans un d\u00e9lai \u00e9tabli."
        },
        "keywords": {
            "en": "forward regulatory plan, Business Impacts, Consultation Opportunities, consumers, business, other stakeholders, The CFIA's forward regulatory plan for 2019-2021",
            "fr": "Plan prospectif de la r\u00e9glementation, R\u00e9percussions sur les entreprises, Occasions de consultations, consommateurs, entreprises, intervenants, partenaires commerciaux"
        },
        "dcterms.subject": {
            "en": "government information,inspection,legislation,regulation",
            "fr": "information gouvernementale,inspection,l\u00e9gislation,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-31 11:01:43",
            "fr": "2015-03-31 11:01:43"
        },
        "modified": {
            "en": "2023-05-16",
            "fr": "2023-05-16"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Forward Regulatory Plan",
        "fr": "Plan prospectif de la r\u00e9glementation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Forward Regulatory Plans are intended to help Canadians, including businesses, Indigenous peoples and trading partners, plan for:</p>\n\n<ul>\n<li>opportunities to provide their feedback during regulatory development</li>\n<li>future regulatory changes</li>\n</ul>\n\n<p><a href=\"/about-cfia/acts-and-regulations/forward-regulatory-plan/2023-to-2025/eng/1505738163122/1505738211563\">CFIA's Forward Regulatory Plan: 2023 to 2025</a> <span class=\"label label-default\">Updated 2023-05-16</span></p>\n\n<ul>\n<li>is a public list or description of anticipated regulatory changes (regulatory initiatives) that CFIA intends to propose or finalize within a 2-year period</li>\n<li>may include regulatory initiatives that are planned to come forward over a longer time frame and that are indicated as long-term</li>\n</ul>\n\n<p>This Forward Regulatory Plan briefly describes each regulatory initiative and includes information such as:</p>\n\n<ul>\n<li>who may be affected by a regulatory initiatives</li>\n<li>regulatory cooperation efforts undertaken or planned</li>\n<li>opportunities for public consultation</li>\n<li>links to related information or analysis</li>\n<li>the CFIA's contact information</li>\n</ul>\n\n<p>The initiatives in this plan are associated with the CFIA's multi\u2011year plan to review its existing regulations (<a href=\"/about-cfia/acts-and-regulations/forward-regulatory-plan/regulatory-stock-review-plan/eng/1557930960453/1557930960744\">Regulatory Stock Review Plan</a>).</p>\n\n<p>This Forward Regulatory Plan also includes initiatives that were identified in various Regulatory Review Roadmaps:</p>\n\n<ul>\n<li><a href=\"/about-cfia/acts-and-regulations/forward-regulatory-plan/regulatory-roadmap/eng/1612197905956/1612197906166\">Agri-food and Aquaculture Regulatory Review Roadmap</a></li>\n<li><a href=\"https://tc.canada.ca/en/corporate-services/acts-regulations/international-standards-targeted-regulatory-review-regulatory-roadmap\">International Standards Regulatory Roadmap</a></li>\n<li><a href=\"https://www.ic.gc.ca/eic/site/020.nsf/eng/h_00644.html\">Digitalization and Technology-Neutral Regulations Roadmap</a></li>\n</ul>\n\n<p>The CFIA's Forward Regulatory Plan will be updated over time to reflect, for example:</p>\n\n<ul>\n<li>progress in regulatory development</li>\n<li>changes to the CFIA's regulatory priorities or its operating environment</li>\n</ul>\n\n<h2>For more information</h2>\n<p>Consult the CFIA's <a href=\"/about-cfia/acts-and-regulations/eng/1299846777345/1299847442232\">acts and regulations web page</a> for:</p>\n<ul>\n<li>a list of acts and regulations administered by the CFIA</li>\n<li>further information on the CFIA's implementation of government-wide regulatory management initiatives</li>\n</ul>\n<p>Consult the following for links to the <a href=\"https://www.canada.ca/en/government/system/laws/developing-improving-federal-regulations/requirements-developing-managing-reviewing-regulations/guidelines-tools/cabinet-directive-regulation.html\"><i>Cabinet Directive on Regulation</i></a> and supporting policies and guidance, and for information on government-wide regulatory initiatives implemented by departments and agencies across the Government of Canada:</p>\n<ul>\n<li><a href=\"https://www.canada.ca/en/government/system/laws/developing-improving-federal-regulations.html\">Developing  and improving federal regulations</a></li>\n<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/regulatory-cooperation/learn-about-regulatory-cooperation.html\">Learn more about regulatory cooperation</a></li>\n</ul>\n<p>To learn about upcoming or ongoing consultations on proposed federal regulations, visit:</p>\n<ul>\n<li><a href=\"https://www.canada.ca/en/government/system/consultations/consultingcanadians.html\">Consulting with Canadians</a></li>\n<li><a href=\"http://gazette.gc.ca/accueil-home-eng.html\"><i>Canada Gazette</i></a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les plans prospectifs de la r\u00e9glementation visent \u00e0 aider les Canadiens, y compris les entreprises, les Autochtones et les partenaires commerciaux, \u00e0 planifier ce qui suit\u00a0:</p>\n\n<ul>\n<li>la possibilit\u00e9 de fournir une r\u00e9troaction au cours de l'\u00e9laboration de la r\u00e9glementation;</li>\n<li>les modifications r\u00e9glementaires \u00e0 venir.</li>\n</ul>\n\n<p>Le <a href=\"/a-propos-de-l-acia/lois-et-reglements/plan-prospectif-de-la-reglementation/2023-a-2025/fra/1505738163122/1505738211563\">Plan prospectif de la r\u00e9glementation\u00a0: 2023 \u00e0 2025 de l'ACIA</a> <span class=\"label label-default\">Mise \u00e0 jour 2023-05-16</span></p>\n\n<ul>\n<li>est une liste publique qui contient des descriptions des modifications r\u00e9glementaires (initiatives r\u00e9glementaires) pr\u00e9vues ou anticip\u00e9es que l'ACIA a l'intention de proposer ou de mettre au point au cours d'une p\u00e9riode de 2\u00a0ans;</li>\n<li>peut inclure les initiatives r\u00e9glementaires qui devraient \u00eatre avanc\u00e9es \u00e0 plus long terme et qui sont d\u00e9sign\u00e9es comme \u00e9tant \u00e0 long terme.</li>\n</ul>\n\n<p>Le plan prospectif de la r\u00e9glementation d\u00e9crit bri\u00e8vement chaque initiative r\u00e9glementaire, et il comprend des renseignements tels que\u00a0:</p>\n\n<ul>\n<li>les personnes susceptibles d'\u00eatre touch\u00e9es par une initiative r\u00e9glementaire;</li>\n<li>les efforts de coop\u00e9ration d\u00e9ploy\u00e9s ou pr\u00e9vus en mati\u00e8re de r\u00e9glementation;</li>\n<li>les possibilit\u00e9s de consultation publique;</li>\n<li>les hyperliens vers des renseignements ou des analyses connexes;</li>\n<li>les coordonn\u00e9es de l'ACIA.</li>\n</ul>\n\n<p>Les initiatives contenues dans ce plan sont associ\u00e9es au plan pluriannuel de l'ACIA pr\u00e9voyant l'examen de ses r\u00e8glements existants (<a href=\"/a-propos-de-l-acia/lois-et-reglements/plan-prospectif-de-la-reglementation/plan-d-examen-de-l-inventaire-des-reglements/fra/1557930960453/1557930960744\">Plan d'examen de l'inventaire des r\u00e8glements</a>).</p>\n\n<p>Le pr\u00e9sent plan prospectif de la r\u00e9glementation comprend \u00e9galement des initiatives qui ont \u00e9t\u00e9 identifi\u00e9es dans diverses feuilles de route r\u00e9glementaire\u00a0:</p>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/plan-prospectif-de-la-reglementation/feuille-de-route-reglementaire/fra/1612197905956/1612197906166\">Feuille de route r\u00e9glementaire agroalimentaire et aquaculture</a></li>\n<li><a href=\"https://tc.canada.ca/fr/services-generaux/lois-reglements/normes-internationales-examen-reglementaire-cible-feuille-route-reglementaire\">Feuille de route r\u00e9glementaire des normes internationales</a></li>\n<li><a href=\"https://www.ic.gc.ca/eic/site/020.nsf/fra/h_00644.html\">Feuille de route sur la num\u00e9risation et les r\u00e8glements neutres sur le plan technologique</a></li>\n</ul>\n\n<p>Le plan prospectif de la r\u00e9glementation sera mis \u00e0 jour au fil du temps pour tenir compte, par exemple, des \u00e9l\u00e9ments suivants\u00a0:</p>\n\n<ul>\n<li>les progr\u00e8s dans l'\u00e9laboration des r\u00e8glements;</li>\n<li>les modifications apport\u00e9es aux priorit\u00e9s r\u00e9glementaires de l'ACIA \u00e0 son environnement op\u00e9rationnel.</li>\n</ul>\n\n<h2>Pour de plus amples renseignements</h2>\n<p> Consultez <a href=\"/a-propos-de-l-acia/lois-et-reglements/fra/1299846777345/1299847442232\">la page <span lang=\"en\">Web</span> consacr\u00e9e aux lois et aux r\u00e8glements</a> de l'ACIA pour obtenir\u00a0:</p>\n<ul>\n<li>la liste des lois et des r\u00e8glements administr\u00e9s par l'ACIA;</li>\n<li>des renseignements additionnels sur la mise en \u0153uvre, par l'ACIA, d'initiatives de gestion de la r\u00e9glementation \u00e0 l'\u00e9chelle gouvernementale.</li>\n</ul>\n<p>Cliquez sur les hyperliens ci-dessous pour acc\u00e9der \u00e0 la <a href=\"https://www.canada.ca/fr/gouvernement/systeme/lois/developpement-amelioration-reglementation-federale/exigences-matiere-elaboration-gestion-examen-reglements/lignes-directrices-outils/directive-cabinet-reglementation.html\"><i>Directive du Cabinet sur la r\u00e9glementation</i></a> ainsi qu'aux politiques et lignes directrices \u00e0 l'appui, et pour obtenir des renseignements sur les initiatives r\u00e9glementaires pangouvernementales mises en \u0153uvre par les minist\u00e8res et organismes dans l'ensemble du gouvernement du Canada\u00a0:</p>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/gouvernement/systeme/lois/developpement-amelioration-reglementation-federale.html\">D\u00e9veloppement et am\u00e9lioration de la r\u00e9glementation f\u00e9d\u00e9rale</a></li>\n<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/cooperation-matiere-reglementation/a-propos-cooperation-matiere-reglementation.html\">Apprenez-en davantage sur la coop\u00e9ration en mati\u00e8re de r\u00e9glementation</a></li>\n</ul>\n<p>Pour en apprendre davantage sur les consultations \u00e0 venir ou en cours concernant les projets de r\u00e9glementation f\u00e9d\u00e9raux, visitez les pages suivantes\u00a0:</p>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/gouvernement/systeme/consultations/consultationdescanadiens.html\">Consultations aupr\u00e8s des Canadiens</a></li>\n<li><a href=\"http://www.gazette.gc.ca/accueil-home-fra.html\"><i>Gazette du Canada</i></a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}