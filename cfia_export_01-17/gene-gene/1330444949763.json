{
    "dcr_id": "1330444949763",
    "title": {
        "en": "<span class=\"nowrap\">AGM \u2013</span> fact sheet",
        "fr": "Spongieuse <span class=\"nowrap\">asiatique -</span> fiche de renseignements"
    },
    "html_modified": "2012-02-28 11:02",
    "modified": "8-12-2021",
    "issued": "28-2-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_lymdis_asian_prevention_1330444949763_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_lymdis_asian_prevention_1330444949763_fra"
    },
    "parent_ia_id": "1329833208596",
    "ia_id": "1330445065144",
    "parent_node_id": "1329833208596",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "<span class=\"nowrap\">AGM \u2013</span> fact sheet",
        "fr": "Spongieuse <span class=\"nowrap\">asiatique -</span> fiche de renseignements"
    },
    "label": {
        "en": "<span class=\"nowrap\">AGM \u2013</span> fact sheet",
        "fr": "Spongieuse <span class=\"nowrap\">asiatique -</span> fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1330445065144",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329833100787",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/spongy-moth/agm-fact-sheet/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/spongieuse-asiatique/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "AGM \u2013 fact sheet",
            "fr": "Spongieuse asiatique - fiche de renseignements"
        },
        "description": {
            "en": "Know what the moth and egg mass look like. The female is large in size. They range from white to light grey in colour, with black markings on the wings.",
            "fr": "Apprenez \u00e0 reconna\u00eetre la spongieuse et ses masses d'oeufs. Les femelles sont de grande taille. Leur couleur varie du blanc au gris p\u00e2le. Les ailes portent des marques noires."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Lymantria dispar",
            "fr": "phytoravageurs, enqu\u00eates phytosanitaires, phytoparasites justiciables de quarantaine, pi\u00e9geage, Lymantria dispar, La spongieuse, La spongieuse asiatique"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-28 11:02:31",
            "fr": "2012-02-28 11:02:31"
        },
        "modified": {
            "en": "2021-12-08",
            "fr": "2021-12-08"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "AGM \u2013 fact sheet",
        "fr": "Spongieuse asiatique - fiche de renseignements"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=10&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"navi/eng/1627657762236\"></div>\n\n<p class=\"text-right\"><a title=\"What you can do to prevent the spread of AGM\" href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/pestrava_lymdis_asian_prevention_pdf_1335547080213_eng.pdf\">PDF (82 <abbr title=\"kilobyte\">kb</abbr>)</a></p>\n\n<p>The moths <i lang=\"la\">Lymantria dispar asiatica</i>, <i lang=\"la\">Lymantria dispar  japonica</i>, <i lang=\"la\">Lymantria albescens</i>, <i lang=\"la\">Lymantria umbrosa</i> and <i lang=\"la\">Lymantria postalba</i> are all commonly referred to as AGM.</p>\n\n<h2>The problem</h2>\n\n<p>This pest is not known to occur in North America, although there have been incursions and populations have been eradicated.</p>\n\n<p>AGM is an invasive pest. It poses a significant threat to Canada's forests, biodiversity and economy. These moths can feed on a wide range of economically important hosts of agriculture, forestry and horticulture and also hosts of environmental importance.</p>\n\n<p>Ships and cargo \u2013 including containers and used vehicles \u2013 can carry the egg masses of these moths to Canada from China, Japan, North Korea, South Korea and Russia (Far East region). In the right conditions, caterpillars hatch from these egg masses and they can go great distances with the wind to find food.</p>\n\n<h2>What you can do to prevent the spread of AGM</h2>\n\n<h3>Know what the moth and egg mass look like</h3>\n\n<p>The female is large in size and capable of sustained flight. They range from white to light grey in colour, with black markings on the wings. They are attracted to light and can lay their egg masses on any surface including cargo containers, marine vessels, vehicles and any other items stored outdoors in areas where AGM is known to occur.</p>\n\n<p>AGM egg masses are 2 to 4 centimetres long. They are gold to dark brown in colour, with fine hairs covering the eggs. They are often found in sheltered areas (for example, out of the rain and direct sun).</p>\n\n<h3>Inspect and report</h3>\n\n<p>Marine vessels are considered the highest risk pathway for AGM to be introduced to Canada. All marine vessels entering Canada can be inspected, at any time of year, to verify that they are free from AGM. If an inspection finds this moth, at any of its life stages, vessels are not allowed to stay in Canadian waters.</p>\n\n<p>Egg masses should be removed by scraping them off and securing them in a sealed container.  Do not paint over egg masses, painting over the egg masses will not kill them.</p>\n\n<p>If you find egg masses resembling the images on this web page, please contact the <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">nearest <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office</a>.</p>\n\n<h2>Stop the spread of invasive species</h2>\n\n<p>An invasive species can be any plant, animal, aquatic life or micro-organism that spreads when introduced outside of its natural distribution. Invasive species can cause serious and often irreversible damage to Canada's ecosystems, economy and society.</p>\n<p>The CFIA is the Government of Canada's science-based regulator for animal health, plant protection and food safety. The CFIA plays an important role in protecting Canada's plant resource base from pests and diseases.</p>\n\n<h2>Referenced images</h2>\n\n<figure class=\"mrgn-bttm-lg\"> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image1_1329760834375_eng.jpg\" alt=\"AGM - Natural Resources Canada\" class=\"img-responsive\">\n<figcaption><abbr title=\"Lymantria albescens, Lymantria umbrosa, Lymantria postalba, Lymantria dispar japonica and Lymantria dispar asiatica\">AGM</abbr>\n<br>Photo Courtesy: Klaus Bolte, <abbr title=\"Natural Resources Canada\">NRCAN</abbr></figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image2_1329760943294_eng.jpg\" alt=\"AGM egg mass - Canadian Food Inspection Agency\" class=\"img-responsive\"> \n<figcaption><abbr title=\"Lymantria albescens, Lymantria umbrosa, Lymantria postalba, Lymantria dispar japonica and Lymantria dispar asiatica\">AGM</abbr> egg mass\n<br>Photo Courtesy: <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></figcaption>\n</figure>\n\n<div class=\"row\">\n<div class=\"col-sm-6\">\n<figure class=\"mrgn-bttm-lg\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image3_1329750999408_eng.jpg\" alt=\"Painting over egg masses does not kill them - Canadian Food Inspection Agency\" class=\"img-responsive\">\n<figcaption>Painting over egg masses does not kill them<br>\n<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></figcaption>\n</figure>\n</div>\n<div class=\"col-sm-6\">\n<figure class=\"mrgn-bttm-lg\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image4_1329751179466_eng.jpg\" alt=\"Painting over egg masses does not kill them - Canadian Food Inspection Agency\" class=\"img-responsive\">\n<figcaption>Painting over egg masses does not kill them<br>\n<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></figcaption>\n</figure>\n</div>\n</div>\n\n<figure class=\"mrgn-bttm-lg\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image5_1329751839512_eng.jpg\" alt=\"Look for moths while calling on ports - Jevic Co. Ltd\" class=\"img-responsive\">\n<figcaption>Look for moths while calling on ports in China, Japan, North Korea, South Korea and Russia (Far East region).\n<br>Photo Courtesy: Jevic <abbr title=\"Company\">Co.</abbr> <abbr title=\"Limited\">Ltd</abbr> </figcaption>\n</figure>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=10&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p class=\"text-right\"><a title=\"Ce que vous pouvez faire pour enrayer la spongieuse asiatique\" href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/pestrava_lymdis_asian_prevention_pdf_1335547080213_fra.pdf\">PDF (83 <abbr title=\"kilo-octet\">ko</abbr>)</a></p>\n\n<p>Les papillons de nuit <i lang=\"la\">Lymantria dispar asiatica</i>, <i lang=\"la\">Lymantria dispar japonica</i>, <i lang=\"la\">Lymantria albescens</i>, <i lang=\"la\">Lymantria umbrosa</i> and <i lang=\"la\">Lymantria postalba</i> sont tous commun\u00e9ment appel\u00e9s la spongieuse asiatique.</p>\n\n<h2>Probl\u00e8mes caus\u00e9s par ce ravageur</h2>\n\n<p>Ce ravageur est consid\u00e9r\u00e9 absent de l'Am\u00e9rique du Nord, m\u00eame s'il  a d\u00e9j\u00e0 fait des intrusions et que des populations ont d\u00e9j\u00e0 \u00e9t\u00e9 \u00e9radiqu\u00e9es.</p>\n\n<p>La spongieuse asiatique est un insecte envahissant nuisible. Il fait peser une grave menace sur les for\u00eats, la biodiversit\u00e9 et l'\u00e9conomie du Canada. Ces spongieuses peuvent se nourrir d'une grande vari\u00e9t\u00e9 h\u00f4tes qui ont une importance \u00e9conomique en agriculture, en foresterie,  en horticulture et d'h\u00f4tes qui sont importants pour l'environnement.</p>\n\n<p>Les navires et leur cargaison\u00a0\u2013 y compris les conteneurs et les v\u00e9hicules d'occasion\u00a0\u2013 provenant de la Chine, du Japon, de la Cor\u00e9e du Nord, de la Cor\u00e9e du Sud et de l'extr\u00eame est de la Russie peuvent transporter des masses d'oeufs de ces ravageurs au Canada. Dans des conditions propices, les oeufs \u00e9closent, donnant ainsi naissance \u00e0 des chenilles qui peuvent, port\u00e9es par le vent, franchir des distances importantes \u00e0 la recherche de nourriture.</p>\n\n<h2>Ce que vous pouvez faire pour enrayer la spongieuse asiatique</h2>\n\n<h3>Apprenez \u00e0 reconna\u00eetre la spongieuse et ses masses d'oeufs</h3>\n\n<p>Les femelles sont de grande taille et capables de voler sur de grandes distances. Leur couleur varie du blanc au gris p\u00e2le. Les ailes portent des marques noires. Ces papillons sont attir\u00e9s par la lumi\u00e8re et peuvent pondre leurs masses d'\u0153ufs sur n'importe quelle surface, dont les conteneurs, les navires maritimes, les v\u00e9hicules et n'importe quel article entrepos\u00e9 \u00e0 l'ext\u00e9rieur dans des aires o\u00f9 la spongieuse asiatique est connue.</p>\n\n<p>Les masses d'oeufs de la spongieuse asiatique mesurent de 2 \u00e0 4 centim\u00e8tres de longueur. Leur couleur varie du dor\u00e9 au brun fonc\u00e9. De fins poils couvrent les oeufs qu'on retrouve souvent dans des endroits abrit\u00e9s (par exemple, \u00e0 l'abri de la pluie et des rayons directs du soleil).</p>\n\n<h3>Inspectez et d\u00e9clarez</h3>\n\n<p>Les navires sont consid\u00e9r\u00e9s comme la porte d'entr\u00e9e au plus haut risque d'introduction de la spongieuse asiatique au Canada. Tous les navires de mer qui arrivent au Canada peuvent faire l'objet d'une inspection \u00e0 n'importe quel moment de l'ann\u00e9e pour s'assurer qu'ils sont exempts de la spongieuse asiatique. Si, lors de l'inspection du navire, on d\u00e9couvre ce type de spongieuse \u00e0 n'importe quelle \u00e9tape de son cycle de vie, le navire en question se voit dans l'obligation de quitter les eaux canadiennes.</p>\n\n<p>Les masses d'\u0153ufs doivent \u00eatre enlev\u00e9es au moyen d'un racloir, puis d\u00e9pos\u00e9es dans un contenant ferm\u00e9 herm\u00e9tiquement. Ne peignez pas sur les masses d'\u0153ufs, peindre sur les masses d'oeufs ne les tuera pas.</p>\n\n<p>Si vous trouvez des masses d'oeufs qui ressemblent aux images du d\u00e9pliant, veuillez communiquer avec <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">le bureau de l'Agence canadienne d'inspection des aliments (ACIA)</a> le plus pr\u00e8s de chez vous.</p>\n\n<h2>Freinez la propagation des esp\u00e8ces envahissantes</h2>\n\n<p>Les esp\u00e8ces envahissantes sont des v\u00e9g\u00e9taux, des animaux, des organismes aquatiques ou des microorganismes qui se propagent lorsqu'ils sont introduits \u00e0 l'ext\u00e9rieur de leur aire de distribution naturelle. Ces esp\u00e8ces peuvent causer des dommages graves et souvent irr\u00e9versibles aux \u00e9cosyst\u00e8mes, \u00e0 l'\u00e9conomie et \u00e0 la soci\u00e9t\u00e9 du Canada.</p>\n\n<p>L'ACIA est l'organisme de r\u00e9glementation \u00e0 vocation scientifique du gouvernement du Canada charg\u00e9 de la r\u00e9glementation en mati\u00e8re de salubrit\u00e9 des aliments, de sant\u00e9 des animaux et de protection des v\u00e9g\u00e9taux. Elle joue un r\u00f4le important dans la protection des ressources v\u00e9g\u00e9tales du Canada contre les ravageurs et les maladies.</p>\n\n<h2>Images r\u00e9f\u00e9rentiels</h2>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image1_1329760834375_fra.jpg\" alt=\"La spongieuse asiatique - Ressources naturelles Canada\" class=\"img-responsive\">\n<figcaption>La spongieuse asiatique\n<br>Photo\u00a0: Klaus Bolte, <abbr title=\"Ressources naturelles Canada\">RNC</abbr>\n</figcaption>\n</figure>\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image2_1329760943294_fra.jpg\" alt=\"Masse d'oeufs de la spongieuse asiatique - Agence canadienne d'inspection des aliments\" class=\"img-responsive\">\n<figcaption>Masse d'oeufs de la spongieuse asiatique\n<br>Photo\u00a0: <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\n</figcaption>\n</figure>\n<div class=\"row\">\n<div class=\"col-sm-6\">\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image3_1329750999408_fra.jpg\" alt=\"Peindre sur les masses d'oeufs ne les tuera pas - Agence canadienne d'inspection des aliments\" class=\"img-responsive\">\n<figcaption>Peindre sur les masses d'oeufs ne les tuera pas\n<br>Photo\u00a0: <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\n</figcaption>\n</figure>\n</div>\n<div class=\"col-sm-6\">\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image4_1329751179466_fra.jpg\" alt=\"Peindre sur les masses d'oeufs ne les tuera pas - Agence canadienne d'inspection des aliments\" class=\"img-responsive\">\n<figcaption>Peindre sur les masses d'oeufs ne les tuera pas\n<br>Photo\u00a0: <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\n</figcaption>\n</figure>\n</div>\n</div>\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lymdis_asian_prevention_image5_1329751839512_fra.jpg\" alt=\"Examinez le navire pour rep\u00e9rer la spongieuse lors des escales dans des ports - Jevic Company, Limited\" class=\"img-responsive\">\n<figcaption>Examinez le navire pour rep\u00e9rer la spongieuse lors des escales dans des ports de la Chine, du Japon, de la Cor\u00e9e du Nord, de la Cor\u00e9e du Sud et de l'extr\u00eame est de la Russie\n<br>Photo\u00a0: <span lang=\"en\">Jevic <abbr title=\"Company\">Co.</abbr> <abbr title=\"Limited\">Ltd</abbr></span>\n</figcaption>\n</figure>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}