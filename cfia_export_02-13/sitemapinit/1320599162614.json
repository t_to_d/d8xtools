{
    "dcr_id": "1299156521180",
    "title": {
        "en": "Exporting aquatic animals",
        "fr": "Exporter des animaux aquatiques"
    },
    "html_modified": "2024-02-13 9:46:35 AM",
    "modified": "2021-08-11",
    "issued": "2011-03-03 07:48:42",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anim_aqua_exp_1299156521180_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anim_aqua_exp_1299156521180_fra"
    },
    "ia_id": "1320599162614",
    "parent_ia_id": "1320536294234",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1320536294234",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Exporting aquatic animals",
        "fr": "Exporter des animaux aquatiques"
    },
    "label": {
        "en": "Exporting aquatic animals",
        "fr": "Exporter des animaux aquatiques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1320599162614",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299155892122",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Exporting aquatic animals",
            "fr": "Exporter des animaux aquatiques"
        },
        "description": {
            "en": "The CFIA certifies exports for animal health and food safety purposes, which can include fish and seafood export program requirements.",
            "fr": "L'ACIA certifie les exportations afin d'assurer la sant\u00e9 des animaux et la salubrit\u00e9 des aliments, notamment en vue de respecter les exigences des programmes d'exportation de poisson et de produits de la mer."
        },
        "keywords": {
            "en": "aquatic animal health, aquatic animals, animal health, Canadian exporters, trading partners",
            "fr": "mati\u00e8re de sant\u00e9 des animaux aquatique, sant\u00e9 des animaux aquatiques, produits de la mer, mati\u00e8re d'importation, certification des exportations"
        },
        "dcterms.subject": {
            "en": "animal health,animal inspection,animal nutrition,animal rights,inspection",
            "fr": "sant\u00e9 animale,inspection des animaux,alimentation animale,droits des animaux,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-03-03 07:48:42",
            "fr": "2011-03-03 07:48:42"
        },
        "modified": {
            "en": "2021-08-11",
            "fr": "2021-08-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Exporting aquatic animals",
        "fr": "Exporter des animaux aquatiques"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>Effective January\u00a015,\u00a02022, exporters for food and animal commodities destined for the European Union (EU) where certification is required under the new EU <i>Animal Health Law</i> will be required to use updated export certificates accessed through the Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>). Existing export certificates to the EU will be accepted until March\u00a015,\u00a02022 as long as they were <strong>signed before January\u00a015,\u00a02022</strong>.</p>\n\n<p>The only exception to this is for the Canadian Food Inspection Agency (CFIA) form Certificate of Free Sale (CFIA/ACIA\u00a05786) which is available for food products manufactured by licensed parties under the <i>Safe Food for Canadians Act</i> (SFCA) and <i>Safe Food for Canadians Regulations</i> (SFCR). The Certificate of Free Sale does not replace product or commodity specific certificates that have been negotiated with foreign countries. It also does not replace or supersede additional import requirements that may be established by the importing country.</p>\n\n<p>The Canadian Food Inspection Agency is responsible for certifying exports of aquatic animals in order to:</p>\n\n<ul class=\"lst-spcd\">\n<li>maintain access for existing aquatic animals and seafood markets where there are requirements for aquatic animal health and/or food safety</li>\n<li>secure entry to new markets by negotiating practical, cost-effective and science-based export conditions</li>\n</ul>\n\n<p>The CFIA certifies exports for animal health and food safety purposes, which can include fish and seafood export program requirements.</p>\n\n<p>The National Animal Health Export Program facilitates Canadian export activities related to aquatic animal health. It will do this through its inspection and certification programs that are designed to meet the requirements of other countries, using science-based principles and the <a href=\"https://www.woah.org/en/what-we-do/standards/codes-and-manuals/aquatic-code-online-access/\">World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE))</a> standards.</p>\n\n<h2>More information</h2>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/eng/1331740343115/1331743129372\">Aquatic animal export: certification requirements by country</a></li>\n\n<li><a href=\"/animal-health/aquatic-animals/exporting-aquatic-animals/framework/eng/1327037916104/1327038033467\">Aquatic Animal Health Export Policy Framework</a></li>\n\n<li><a href=\"/animal-health/aquatic-animals/diseases/compartmentalization/eng/1345164530104/1345164735083\">Compartmentalization for international trade of aquatic animals</a></li>\n\n<li><a href=\"/animal-health/aquatic-animals/exporting-aquatic-animals/overview/eng/1327040178560/1327040616509\">Exporting aquatic animals from Canada: overview</a></li>\n\n<li><a href=\"/animal-health/aquatic-animals/exporting-aquatic-animals/external-laboratories/eng/1473978994666/1473979063533\">Policy on the Approval of External Laboratories for the National Aquatic Animal Health Program</a></li>\n\n<li><a href=\"/animal-health/aquatic-animals/exporting-aquatic-animals/reportable-diseases/eng/1327613207253/1327614044216\">Status of regulated aquatic animal reportable diseases</a></li>\n\n<li><a href=\"/animal-health/aquatic-animals/exporting-aquatic-animals/immediately-notifiable-diseases/eng/1327616198060/1327616292274\">Status of regulated aquatic animal immediately notifiable diseases</a></li>\n</ul>\n\n<p>Exporters intending to export an aquatic animal or fish and seafood product should contact the nearest <a href=\"/eng/1300462382369/1365216058692\">CFIA animal health office</a> for information or certification requirements.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>\u00c0 compter du\u00a015 janvier\u00a02022, les exportateurs de produits alimentaires et d'origine animale destin\u00e9s \u00e0 l'Union europ\u00e9enne (UE) o\u00f9 la certification est requise dans le cadre de la nouvelle <i>loi sur la sant\u00e9 animale</i> (LSA) devront utiliser des certificats d'exportation mis \u00e0 jour. Les certificats d'exportation sont accessibles par l'interm\u00e9diaire du syst\u00e8me <a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a> (<span lang=\"en\">Trade Control and Expert System New Technology</span>). Les certificats d'exportation actuels seront accept\u00e9s par l'UE jusqu'au\u00a015 mars\u00a02022, \u00e0 condition qu'ils soient <strong>sign\u00e9s avant le\u00a015 janvier\u00a02022</strong>.</p>\n\n<p>La seule exception est le formulaire Certificat de vente libre (CFIA/ACIA\u00a05786) de l'Agence canadienne d'inspection des aliments (ACIA) qui est offert aux produits alimentaires fabriqu\u00e9s par des parties titulaires d'une licence au titre de la <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> (LSAC) et du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC). Le Certificat de vente libre ne remplace pas les certificats propres aux produits ou aux marchandises qui ont fait l'objet de n\u00e9gociations avec des pays \u00e9trangers. De plus, il ne remplace pas les exigences d'importation suppl\u00e9mentaires qui pourraient \u00eatre \u00e9tablies par le pays importateur et n'a pas pr\u00e9s\u00e9ance sur celles-ci.</p>\n\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est responsable de la certification des exportations d'animaux aquatiques. \u00c0 ce titre, elle\u00a0:</p>\n\n<ul class=\"lst-spcd\">\n<li>maintient l'acc\u00e8s aux march\u00e9s des animaux aquatiques et des produits de la mer d\u00e9j\u00e0 \u00e9tablis pour lesquels il existe des exigences en mati\u00e8re de sant\u00e9 des animaux aquatiques et/ou de salubrit\u00e9 des aliments;</li>\n<li>garantit l'acc\u00e8s \u00e0 de nouveaux march\u00e9s en n\u00e9gociant des conditions d'exportation r\u00e9alisables, rentables et fond\u00e9es sur des principes scientifiques.</li>\n</ul>\n\n<p>L'ACIA certifie les exportations afin d'assurer la sant\u00e9 des animaux et la salubrit\u00e9 des aliments, notamment en vue de respecter les exigences des programmes d'exportation de poisson et de produits de la mer.</p>\n\n<p>Le Programme national sur la sant\u00e9 des animaux aquatiques facilite les activit\u00e9s d'exportation du Canada sur le plan de la sant\u00e9 des animaux aquatiques, et ce, par l'interm\u00e9diaire de ses programmes d'inspection et de certification con\u00e7us en fonction des exigences des pays \u00e9trangers, puisqu'ils se fondent sur des principes scientifiques et sur les normes de l'<a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/codes-et-manuels/acces-en-ligne-au-code-aquatique/\">Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE))</a>.</p>\n\n<h2>Renseignements suppl\u00e9mentaires</h2>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/fra/1331740343115/1331743129372\">Exportation d'animaux aquatiques\u00a0: exigences de certification par pays</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/cadre-strategique/fra/1327037916104/1327038033467\">Cadre strat\u00e9gique sur la sant\u00e9 des animaux aquatiques destin\u00e9s \u00e0 l'exportation</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/compartimentation/fra/1345164530104/1345164735083\">Compartimentation pour le commerce international des animaux aquatiques</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/apercu/fra/1327040178560/1327040616509\">Exportation d'animaux aquatiques du Canada\u00a0: aper\u00e7u</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/laboratoires-externes/fra/1473978994666/1473979063533\">Politique sur l'approbation des laboratoires externes pour le Programme national sur la sant\u00e9 des animaux aquatiques</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/maladies-a-declaration-obligatoire/fra/1327613207253/1327614044216\">Statut des maladies r\u00e9glement\u00e9es d\u00e9clarables touchant les animaux aquatiques</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/maladies-reglementees-a-notification-immediate/fra/1327616198060/1327616292274\">Statut des maladies r\u00e9glement\u00e9es \u00e0 notification imm\u00e9diate touchant les animaux aquatiques</a></li>\n</ul>\n\n<p>Les exportateurs pr\u00e9voyant exporter un animal aquatique ou encore du poisson ou un produit de la mer devraient communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de sant\u00e9 animale de l'ACIA</a> le plus pr\u00e8s pour obtenir des renseignements \u00e0 ce sujet ou pour conna\u00eetre les exigences en mati\u00e8re de certification qui s'appliquent.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}