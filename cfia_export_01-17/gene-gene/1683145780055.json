{
    "dcr_id": "1683145780055",
    "title": {
        "en": "Notice to industry: Consultations on new or amended single ingredient feed descriptions",
        "fr": "Avis \u00e0 <span class=\"nowrap\">l'industrie :</span> Consultations sur les descriptions nouvelles ou modifi\u00e9es d'aliments \u00e0 ingr\u00e9dient unique"
    },
    "html_modified": "17-5-2023",
    "modified": "17-5-2023",
    "issued": "4-5-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/not_to_ind_new_amend_sif_desc_1683145780055_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/not_to_ind_new_amend_sif_desc_1683145780055_fra"
    },
    "parent_ia_id": "1320602705720",
    "ia_id": "1683145780665",
    "parent_node_id": "1320602705720",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice to industry: Consultations on new or amended single ingredient feed descriptions",
        "fr": "Avis \u00e0 <span class=\"nowrap\">l'industrie :</span> Consultations sur les descriptions nouvelles ou modifi\u00e9es d'aliments \u00e0 ingr\u00e9dient unique"
    },
    "label": {
        "en": "Notice to industry: Consultations on new or amended single ingredient feed descriptions",
        "fr": "Avis \u00e0 <span class=\"nowrap\">l'industrie :</span> Consultations sur les descriptions nouvelles ou modifi\u00e9es d'aliments \u00e0 ingr\u00e9dient unique"
    },
    "templatetype": "content page 1 column",
    "node_id": "1683145780665",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300212191074",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/feed-industry-notices/notice-to-industry/",
        "fr": "/sante-des-animaux/aliments-du-betail/avis-a-l-industrie-aliments-du-betail/descriptions-nouvelles-ou-modifiees-d-aliments-a-i/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice to industry: Consultations on new or amended single ingredient feed descriptions",
            "fr": "Avis \u00e0 l'industrie : Consultations sur les descriptions nouvelles ou modifi\u00e9es d'aliments \u00e0 ingr\u00e9dient unique"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) is introducing a new consultation approach for livestock feed ingredients.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) introduit une nouvelle approche de consultation pour les ingr\u00e9dients destin\u00e9s \u00e0 l'alimentation du b\u00e9tail."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, Feeds Act, Feeds Regulations, livestock, animal feeds",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, Loi sur les aliments du b\u00e9tail, R\u00e8glement sur les aliments du b\u00e9tail, b\u00e9tail"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,standards,policy,regulation,animal health",
            "fr": "b\u00e9tail,inspection,norme,politique,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-05-17",
            "fr": "2023-05-17"
        },
        "modified": {
            "en": "2023-05-17",
            "fr": "2023-05-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice to industry: Consultations on new or amended single ingredient feed descriptions",
        "fr": "Avis \u00e0 l'industrie : Consultations sur les descriptions nouvelles ou modifi\u00e9es d'aliments \u00e0 ingr\u00e9dient unique"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-bttm-lg\">2023-05-17</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) is introducing a new consultation approach for livestock feed ingredients. Before any new or amended single ingredient feed (SIF) descriptions are finalized, the CFIA will conduct a consultation to provide an opportunity for stakeholders to share their comments. This approach aligns with the process that will be used when the modernized <i>Feeds Regulations</i> are published in <i>Canada Gazette</i>, Part II (CGII). It will also ensure that any SIFs that are approved or amended between now and that publication date will be able to be incorporated into the new Canadian Feed Ingredients Table (CFIT).</p>\n\n<h2>Background</h2>\n\n<p>Currently, <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-9.html\" title=\"Schedule IV of the Feeds Regulations\">Schedules IV</a> and <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-10.html\" title=\"Schedule V of the Feeds Regulations\">V</a> of the <a href=\"/english/reg/jredirect2.shtml?feebetr\"><i>Feed Regulations,</i> 1983</a> are the lists of SIFs that have been evaluated and approved by the\u00a0CFIA\u00a0for manufacture, sale, or import in Canada.</p>\n\n<p>With the new <a href=\"https://gazette.gc.ca/rp-pr/p1/2021/2021-06-12/html/reg1-eng.html\"><i>Feeds Regulations,</i> 2023</a>, the CFIT will replace <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-9.html\" title=\"Schedule IV of the Feeds Regulations\">Schedules IV</a> and <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-10.html\" title=\"Schedule V of the Feeds Regulations\">V</a> of the current regulations. All SIFs already approved in Schedules IV and V have been transferred to the CFIT. They have been placed into new classes/subclasses and the SIF descriptions have been standardized. The CFIT is 1 of 9 documents incorporated by reference (IBR) under the proposed <a href=\"https://gazette.gc.ca/rp-pr/p1/2021/2021-06-12/html/reg1-eng.html\"><i>Feeds Regulations,</i> 2023</a>. The CFIA's <a href=\"/about-cfia/acts-and-regulations/incorporation-by-reference/cfia-incorporation-by-reference-policy/eng/1450356693608/1450356805085\">Incorporation by Reference (IBR) Policy</a> requires that a consultation be conducted before changes to IBR documents are made, and therefore new and amended SIF descriptions will be consulted on prior to adding them to the CFIT.</p>\n\n<h2>Consultation format</h2>\n\n<p>The CFIA will continue to assess new SIFs to verify they are safe and effective for their intended purpose. In preparation for the <a href=\"https://gazette.gc.ca/rp-pr/p1/2021/2021-06-12/html/reg1-eng.html\"><i>Feeds Regulations,</i> 2023</a>, if a SIF is to be approved, part of the approval process will include consultation on the SIF description. This consultation will ensure transparency and allow for stakeholder feedback on changes to the CFIT. Consultations will be conducted for new SIF descriptions and amendments to existing SIF descriptions.</p>\n\n<p>The consultation will have a two-step process:</p>\n\n<ul>\n<li>the CFIA will post a document for public consultation on proposed changes to the CFIT for new SIF descriptions or changes to an existing SIF description</li>\n<li>the CFIA will post a summary of comments received and the CFIA's responses to the comments from the consultation</li>\n<li>The summary document will include any changes to be made to the SIF description and the finalized SIF description that would be added to the CFIT along with the date the update comes into effect.</li>\n</ul>\n\n<p>The consultation document will only be posted after an assessment of the SIF has been completed by the CFIA, and the CFIA recommends that the SIF be approved or amended. The consultation document will indicate if there are any conditions or restrictions on use as an outcome of the assessment, and that the CFIA intends to add this SIF to CFIT.</p>\n\n<p>Stakeholders will have 30 days to submit any comments on the SIF description. No confidential business information will be included in the consultation. Stakeholders are encouraged to share comments if they have concerns over the accuracy of the SIF description or if there is any additional scientific data that should be considered before the SIF is approved.</p>\n\n<p>Scientific questions or information will be reviewed by CFIA evaluators for consideration in the assessment. Non-scientific input will be evaluated and appropriate ways of addressing the comments will be explored. Subject to the commenters' consent, the CFIA may forward comments to the applicant whose SIF is being assessed, for their consideration. Commenters will remain anonymous however, a summary of the comments and feedback will be included in the What We Heard Report that will be published following the consultation. Requests for changes or additions to SIF descriptions, such as including additional livestock species, will not be considered as part of the consultation. Requests of this type require an additional assessment and should be submitted as an amendment to the SIF, once it is approved.</p>\n\n<p>Following the comment period, the SIF approval will be finalized, provided there were no significant concerns raised, and the applicant will be notified that their SIF has been approved.</p>\n\n<p>The CFIA recognizes that adding a consultation period or step into the SIF approval process may affect how quickly SIFs are approved. In cases where there are no significant concerns raised during consultation, the CFIA will issue the SIF approval and certificate to the applicant after the consultation closes. At that time, applicants will be able to market and sell their SIF. If there are major issues raised after the consultation, these would have to be addressed with the applicant prior to approval and addition to the CFIT.</p>\n\n<h2>Anticipated outcomes</h2>\n\n<p>This approach to the consultation on new or amended SIF descriptions will:</p>\n\n<ul>\n<li>meet the CFIA's obligations to consult on modifications to documents that are incorporated by reference</li>\n<li>provide greater transparency on new and amended SIF descriptions</li>\n<li>give regulated parties and other stakeholders an opportunity to provide feedback on proposed new SIFs or amendments to existing SIF descriptions, before the approval is finalized</li>\n<li>provide a consistent and predictable process for updating the list of approved SIFs</li>\n</ul>\n\n<p>This new approach to consultations will take effect immediately.</p>\n\n<h2>Related information</h2>\n\n<ul>\n<li><a href=\"/animal-health/livestock-feeds/approval-and-registration/eng/1627997831971/1627997833424\">Approval and registration of livestock feeds</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/eng/1329109265932/1329109385432\">RG-1 Regulatory Guidance: Feed Registration Procedures and Labelling Standards</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-modernization/cfit/eng/1613423298011/1613423298323\">Proposed document to be incorporated by reference \u2013 Canadian Feed Ingredients Table (CFIT)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-bttm-lg\">2023-05-17</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) introduit une nouvelle approche de consultation pour les ingr\u00e9dients destin\u00e9s \u00e0 l'alimentation du b\u00e9tail. Avant de terminer toute description nouvelle ou modifi\u00e9e d'un aliment \u00e0 ingr\u00e9dient unique, l'ACIA m\u00e8nera une consultation pour donner l'occasion aux intervenants de partager leurs commentaires. Cette approche s'harmonise au processus qui sera utilis\u00e9 lorsque le r\u00e8glement modernis\u00e9 sur les aliments pour animaux sera publi\u00e9 dans la <i>Gazette du Canada</i>, partie\u00a0II (GCII). Elle garantira \u00e9galement que tous les aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique approuv\u00e9s ou modifi\u00e9s d'ici \u00e0 cette date de publication pourront \u00eatre incorpor\u00e9s dans le nouveau Tableau canadien des ingr\u00e9dients des aliments du b\u00e9tail (TCIAB).</p>\n\n<h2>Contexte</h2>\n\n<p>Actuellement, les <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-9.html\" title=\"annexe IV du R\u00e8glement sur les aliments du b\u00e9tail\">annexes IV</a> et <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-10.html\" title=\"annexe V du R\u00e8glement sur les aliments du b\u00e9tail\">V</a> du <a href=\"/francais/reg/jredirect2.shtml?feebetr\"><i>R\u00e8glement de 1983 sur les aliments du b\u00e9tail</i></a> pr\u00e9sentent les listes des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique qui ont \u00e9t\u00e9 \u00e9valu\u00e9s et approuv\u00e9s par l'ACIA pour la fabrication, la vente ou l'importation au Canada.</p>\n\n<p>Avec le nouveau <a href=\"https://gazette.gc.ca/rp-pr/p1/2021/2021-06-12/html/reg1-fra.html\"><i>R\u00e8glement de 2023 sur les aliments du b\u00e9tail</i></a>, le TCIAB remplacera les <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-9.html\">annexes IV</a> et <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-10.html\">V</a> de la r\u00e9glementation actuelle. Tous les aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique d\u00e9j\u00e0 approuv\u00e9s dans les annexes IV et V ont \u00e9t\u00e9 transf\u00e9r\u00e9s dans le TCIAB. Ils ont \u00e9t\u00e9 plac\u00e9s dans de nouvelles classes ou sous-classes et les descriptions des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique ont \u00e9t\u00e9 normalis\u00e9es. Le TCIAB est 1 des 9 documents adopt\u00e9s par renvoi dans le cadre de la proposition de <a href=\"https://gazette.gc.ca/rp-pr/p1/2021/2021-06-12/html/reg1-fra.html\"><i>R\u00e8glement de 2023 sur les aliments du b\u00e9tail</i></a>. La politique d'adoption par renvoi de l'ACIA exige qu'une consultation soit men\u00e9e avant de modifier les documents d'adoption par renvoi. Par cons\u00e9quent, les descriptions nouvelles et modifi\u00e9es des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique feront l'objet d'une consultation avant d'\u00eatre ajout\u00e9es au TCIAB.</p>\n\n<h2>Structure de la consultation</h2>\n\n<p>L'ACIA continuera d'\u00e9valuer les nouveaux aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique afin de v\u00e9rifier qu'ils sont s\u00fbrs et efficaces pour l'usage auquel ils sont destin\u00e9s. En pr\u00e9vision du <a href=\"https://gazette.gc.ca/rp-pr/p1/2021/2021-06-12/html/reg1-fra.html\"><i>R\u00e8glement de 2023 sur les aliments du b\u00e9tail</i></a>, si un aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique doit \u00eatre approuv\u00e9, le processus d'approbation comprendra une consultation sur la description de l'aliment en question. Cette consultation garantira la transparence et permettra aux parties prenantes de donner leur avis sur les modifications \u00e0 apporter au TCIAB. Des consultations seront men\u00e9es pour les nouvelles descriptions des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique ainsi que pour les modifications des descriptions existantes.</p>\n\n<p>La consultation aura un processus en 2 \u00e9tapes\u00a0:</p>\n\n<ul>\n<li>l'ACIA publiera un document en vue d'une consultation publique sur les propositions de modifications du TCIAB pour les descriptions d'aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique nouvelles ou modifi\u00e9es.</li>\n<li>l'ACIA publiera un r\u00e9sum\u00e9 des commentaires re\u00e7us et les r\u00e9ponses de l'ACIA aux commentaires formul\u00e9s lors de la consultation.</li>\n<li>Le document de synth\u00e8se comprendra toutes les modifications \u00e0 apporter \u00e0 la description actuelle et \u00e0 celle termin\u00e9e de l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique qui sera ajout\u00e9e au TCIAB, ainsi que la date d'entr\u00e9e en vigueur de la mise \u00e0 jour.</li>\n</ul>\n\n<p>Le document de consultation ne sera publi\u00e9 qu'une fois que l'ACIA aura proc\u00e9d\u00e9 \u00e0 l'\u00e9valuation de l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique et qu'elle aura recommand\u00e9 l'approbation ou la modification de celui-ci. Le document de consultation indiquera si l'\u00e9valuation a donn\u00e9 lieu \u00e0 des conditions ou \u00e0 des restrictions d'utilisation et si l'ACIA a l'intention d'ajouter cet aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique au TCIAB.</p>\n\n<p>Les parties prenantes auront 30\u00a0jours pour soumettre leurs commentaires sur la description de l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique. Aucune information commerciale confidentielle ne sera incluse dans la consultation. Les parties prenantes sont invit\u00e9es \u00e0 faire part de leurs commentaires si elles ont des doutes quant \u00e0 l'exactitude de la description de l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique ou si des donn\u00e9es scientifiques suppl\u00e9mentaires doivent \u00eatre prises en compte avant son approbation.</p>\n\n<p>Les questions ou informations scientifiques seront examin\u00e9es par les \u00e9valuateurs de l'ACIA en vue de leur prise en compte dans l'\u00e9valuation. Les contributions non scientifiques seront \u00e9valu\u00e9es et les moyens appropri\u00e9s pour les traiter seront explor\u00e9s. Sous r\u00e9serve de l'accord des commentateurs, l'ACIA peut transmettre les commentaires au demandeur dont l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique est \u00e9valu\u00e9 pour qu'il les examine. Les commentateurs resteront anonymes, toutefois, une synth\u00e8se des commentaires et des r\u00e9actions sera incluse dans le rapport \u00ab\u00a0Ce que nous avons entendu\u00a0\u00bb qui sera publi\u00e9 \u00e0 l'issue de la consultation. Les demandes de modifications ou d'ajouts aux descriptions des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique, telles que l'inclusion d'esp\u00e8ces animales suppl\u00e9mentaires, ne seront pas prises en compte dans le cadre de la consultation. Les demandes de ce type n\u00e9cessitent une \u00e9valuation suppl\u00e9mentaire et doivent \u00eatre soumises sous la forme d'un amendement \u00e0 l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique une fois celui-ci approuv\u00e9.</p>\n\n<p>\u00c0 l'issue de cette p\u00e9riode de commentaires, l'approbation de l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique sera termin\u00e9e, \u00e0 condition qu'aucun probl\u00e8me important n'ait \u00e9t\u00e9 soulev\u00e9, et le demandeur sera inform\u00e9 de l'approbation de son aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique.</p>\n\n<p>L'ACIA reconna\u00eet que l'ajout d'une p\u00e9riode ou d'une \u00e9tape de consultation dans le processus d'approbation des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique peut avoir une incidence sur la rapidit\u00e9 de leur approbation. Dans les cas o\u00f9 aucune pr\u00e9occupation importante n'est soulev\u00e9e au cours de la consultation, l'ACIA d\u00e9livre l'agr\u00e9ment et le certificat de l'aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique au demandeur apr\u00e8s la cl\u00f4ture de la consultation. \u00c0 ce moment-l\u00e0, les demandeurs pourront commercialiser et vendre leur aliment du b\u00e9tail \u00e0 ingr\u00e9dient unique. Si des questions importantes sont soulev\u00e9es apr\u00e8s la consultation, elles devront \u00eatre abord\u00e9es avec le demandeur avant d'\u00eatre approuv\u00e9es et ajout\u00e9es au TCIAB.</p>\n\n<h2>R\u00e9sultats attendus</h2>\n\n<p>Cette approche de la consultation sur les descriptions nouvelles ou modifi\u00e9es des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique permettra de\u00a0:</p>\n\n<ul>\n<li>satisfaire aux obligations de consultation de l'ACIA sur les modifications apport\u00e9es aux documents incorpor\u00e9s par r\u00e9f\u00e9rence;</li>\n<li>fournir une plus grande transparence sur les descriptions nouvelles et modifi\u00e9es des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique;</li>\n<li>donner aux parties r\u00e9glement\u00e9es et aux autres intervenants la possibilit\u00e9 de donner leur avis sur les descriptions des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique existantes ou nouvelles, avant que l'accord ne soit termin\u00e9;</li>\n<li>mettre en place un processus coh\u00e9rent et pr\u00e9visible de mise \u00e0 jour de la liste des aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique agr\u00e9\u00e9s.</li>\n</ul>\n\n<p>Cette nouvelle approche de la consultation prend effet imm\u00e9diatement.</p>\n\n<h2>Renseignements connexes</h2>\n\n<ul>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/fra/1627997831971/1627997833424\">Approbation et enregistrement des aliments pour animaux de ferme</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/fra/1329109265932/1329109385432\">RG-1 Directives r\u00e9glementaires\u00a0: Proc\u00e9dures d'enregistrement et normes d'\u00e9tiquetage des aliments pour animaux</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/tciab/fra/1613423298011/1613423298323\">Documents propos\u00e9s \u00e0 l'incorporation par renvoi \u2013 Tableau canadien des ingr\u00e9dients des aliments du b\u00e9tail (TCIAB)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}