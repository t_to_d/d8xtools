{
    "dcr_id": "1616421533834",
    "title": {
        "en": "Fact sheet - Hazard identification and analysis",
        "fr": "Le feuillet d'information\u00a0- La d\u00e9termination et l'analyse des dangers"
    },
    "html_modified": "2024-01-26 2:25:25 PM",
    "modified": "2021-06-11",
    "issued": "2021-06-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/feed_reg_mod_hazard_id_factsheet_1616421533834_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/feed_reg_mod_hazard_id_factsheet_1616421533834_fra"
    },
    "ia_id": "1616421534162",
    "parent_ia_id": "1612971995765",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1612971995765",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fact sheet - Hazard identification and analysis",
        "fr": "Le feuillet d'information\u00a0- La d\u00e9termination et l'analyse des dangers"
    },
    "label": {
        "en": "Fact sheet - Hazard identification and analysis",
        "fr": "Le feuillet d'information\u00a0- La d\u00e9termination et l'analyse des dangers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1616421534162",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1612969567098",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/regulatory-modernization/hazard-identification-and-analysis/",
        "fr": "/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/la-determination-et-l-analyse-des-dangers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fact sheet - Hazard identification and analysis",
            "fr": "Le feuillet d'information\u00a0- La d\u00e9termination et l'analyse des dangers"
        },
        "description": {
            "en": "Under the proposed Feeds Regulations, 2022, many feed establishments will need a licence based on the feed-related activities they conduct. This is a new regulatory requirement.",
            "fr": "En vertu du R\u00e8glement sur les aliments du b\u00e9tail, 2022 propos\u00e9 la plupart des \u00e9tablissements d'aliments du b\u00e9tail devront cerner les dangers biologiques, chimiques et physiques li\u00e9s \u00e0 leurs activit\u00e9s li\u00e9es aux aliments du b\u00e9tail et analyser ces dangers afin de d\u00e9terminer s'ils posent un risque de contamination. Il s'agit d'une nouvelle exigence r\u00e9glementaire."
        },
        "keywords": {
            "en": "animals, disease, freedom, licencing",
            "fr": "animaux, sant\u00e9 des animaux, d\u00e9termination et l'analyse des dangers"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-06-11",
            "fr": "2021-06-11"
        },
        "modified": {
            "en": "2021-06-11",
            "fr": "2021-06-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fact sheet - Hazard identification and analysis",
        "fr": "Le feuillet d'information\u00a0- La d\u00e9termination et l'analyse des dangers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The information in this document is based on requirements set out in the proposed <i>Feeds Regulations,\u00a02022</i> (the \"regulations\"). The information is intended to help regulated parties understand the requirements within the regulations once they come into force. The proposed requirements are subject to change as the regulatory process advances through its various stages. In the interim, current laws applicable to livestock feed in Canada continue to apply.</p>\n</div>\n\n<p>The proposed <i>Feeds Regulations, 2022</i> will impact a variety of stakeholders, including:</p>\n\n<ul>\n<li>single ingredient feed manufacturers and suppliers</li>\n<li>mixed feed manufacturers and suppliers (for example, commercial feed mills, specialty feed manufacturers, etc.)</li>\n<li>rendering facilities manufacturing livestock feed ingredients</li>\n<li>feed retail outlets</li>\n<li>livestock producers (on-farm feed mills)</li>\n<li>feed importers</li>\n<li>feed exporters</li>\n</ul>\n\n<p>Under the proposed <i>Feeds Regulations,\u00a02022</i>, most feed establishments will be required to identify the biological, chemical and physical hazards associated with their feed related activities, and analyze these hazards to determine if they present a risk of contamination. This is a new regulatory requirement.</p>\n\n<p>This fact sheet applies to you if you conduct any of the following feed-related activities:</p>\n\n<ul>\n<li>manufacturing</li>\n<li>storing</li>\n<li>packaging</li>\n<li>labelling</li>\n<li>selling</li>\n<li>exporting</li>\n</ul>\n\n<p>The regulations do not apply for feeds made on-farm by livestock producers as long as the feed is not sold off the farm and is not medicated.</p>\n\n<h2>Hazards</h2>\n\n<p>A hazard is a biological, chemical or physical agent in feed, that, when not controlled, has the potential to cause an adverse effect on animal health, human health or the environment.</p>\n\n<h3 id=\"a2\">Types of hazards</h3>\n\n<ul>\n<li><a href=\"#a2_1\">Biological hazards</a></li>\n<li><a href=\"#a2_2\">Chemical hazards</a></li>\n<li><a href=\"#a2_3\">Physical hazards</a></li>\n</ul>\n\n<h3 id=\"a2_1\">Biological hazards</h3>\n\n<p>Biological hazards include microorganisms such as bacteria, viruses, parasites, fungi, and prions. Some microorganisms can cause illnesses and other microorganisms can produce harmful toxins.</p>\n\n<p>Examples of sources of biological hazards include:</p>\n\n<ul>\n<li>incoming ingredients, including raw materials</li>\n<li>cross-contamination during processing, storage, and transportation</li>\n<li>employees</li>\n<li>feed contact surfaces</li>\n<li>insects and rodents</li>\n</ul>\n\n<h3 id=\"a2_2\">Chemical hazards</h3>\n\n<p>Chemical hazards can include contaminants (metals and elements, radionuclides, processing aids, dioxins) as well as veterinary drug and pesticide residues, and natural toxins. Some chemical hazards occur naturally while others are intentionally or unintentionally added during manufacturing and processing.</p>\n\n<p>Examples of chemical hazards include:</p>\n\n<ul>\n<li>chemicals intentionally used in feed manufacturing such as processing aids, feed additives, medicating ingredients</li>\n<li>chemicals that are by-products of processing</li>\n<li>chemical contamination from equipment</li>\n<li>industrial chemicals such as cleaning agents and disinfection agents</li>\n<li>naturally occurring toxins such as mycotoxins, histamines, marine biotoxins</li>\n<li>agricultural chemicals such as pesticides and fertilizers</li>\n<li>nutrients such as over-addition of vitamins or minerals</li>\n</ul>\n\n<h3 id=\"a2_3\">Physical hazards</h3>\n\n<p>Physical hazards in feed include many types of extraneous material that may be introduced anywhere along the production chain, from primary production up to and including the farm (livestock producers). Extraneous materials can be introduced by anything or anyone coming in contact with feed, including during processing, transportation or storage. Extraneous materials are considered to be hazards if they result in risk of harm to the animal who consumes the feed or there is a risk of contamination of the food of animal origin (eggs, dairy products and by-products, and meat) consumed by humans.</p>\n\n<p>Examples of physical hazards include:</p>\n\n<ul>\n<li>stones, rocks and dirt</li>\n<li>metal (commonly associated with processing activities such as grinding or cutting operations, as well as packaging materials or containers)</li>\n<li>glass, plastic or other contaminants from packaging materials or containers, or from the processing environment</li>\n<li>wood splinters from broken pallets or packaging material</li>\n<li>flaking paint from overhead structures or equipment</li>\n</ul>\n\n<h2>Hazard identification and analysis</h2>\n\n<p>Feed establishments conduct hazard identification and analysis to identify the specific biological, chemical and physical hazards associated with both the materials used, and the processes employed at the feed establishment.</p>\n\n<p>Hazard identification and analysis takes into account the effect of any factor relating to the safety of the feed, such as:</p>\n\n<ul>\n<li>formulation of the feed</li>\n<li>ingredients of the feed, including raw materials</li>\n<li>concentration of any inherent contaminant in the feed</li>\n<li>manufacturing, processing, packaging and labelling procedures of the feed</li>\n<li>storage and distribution of the feed</li>\n<li>transportation practices</li>\n<li>intended purpose of the feed</li>\n<li>condition, function, design and sanitation of the facility and equipment</li>\n<li>employee hygiene</li>\n<li>weather conditions</li>\n</ul>\n\n<p>In the hazard identification step, hazards from all inputs and processing steps are listed, described, and classified. Then, in the hazard analysis step, hazards are evaluated according to their significance.</p>\n\n<p>Once the hazard identification and analysis is completed, control measures are applied to prevent, eliminate or reduce each hazard identified to an acceptable level. Feed establishments must demonstrate that their control measures are effective.</p>\n\n<p>In addition, critical control points (CCPs), critical limits, monitoring procedures, corrective actions procedures, and verification procedures must be established in relation to each significant hazard.</p>\n\n<h2>Benefits of hazard analysis</h2>\n\n<p>Identifying and analyzing hazards is the first step in a preventive control approach. This allows feed establishments to be proactive in managing hazards. Proactive control of hazards helps to protect feed and food safety and can be more cost effective than a feed safety investigation.</p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n\n<p>Les renseignements contenus dans ce document s'appuient sur les dispositions \u00e9tablies dans le projet de <i>R\u00e8glement de\u00a02022 sur les aliments du b\u00e9tail</i> (le \u00ab\u00a0r\u00e8glement\u00a0\u00bb). Les renseignements visent \u00e0 aider les parties r\u00e9glement\u00e9es \u00e0 comprendre les dispositions contenues dans le r\u00e8glement lorsqu'elles entreront en vigueur. Les dispositions propos\u00e9es peuvent \u00eatre modifi\u00e9es au fur et \u00e0 mesure de l'\u00e9tat d'avancement du processus de r\u00e9glementation au cours de ses diverses \u00e9tapes. Dans l'intervalle, les lois actuelles applicables aux aliments du b\u00e9tail au Canada continueront de s'appliquer.</p>\n\n</div>\n\n<p>Le <i>R\u00e8glement de 2022 sur les aliments du b\u00e9tail</i> propos\u00e9 touchera une vari\u00e9t\u00e9 d'intervenants, y compris</p>\n\n<ul>\n<li>les fabricants et les fournisseurs d'aliments \u00e0 ingr\u00e9dient unique</li>\n<li>les fabricants et les fournisseurs d'aliments m\u00e9lang\u00e9s (par exemple, les meuneries commerciales, fabricant d'aliments sp\u00e9ciaux etc.)</li>\n<li>les usines d'\u00e9quarrissage fabriquant des aliments du b\u00e9tail</li>\n<li>les d\u00e9taillants d'aliments du b\u00e9tail</li>\n<li>les \u00e9leveurs de b\u00e9tail (meuneries \u00e0 la ferme)</li>\n<li>les importateurs d'aliments du b\u00e9tail</li>\n<li>les exportateurs d'aliments du b\u00e9tail</li>\n</ul>\n\n<p>En vertu du <i>R\u00e8glement sur les aliments du b\u00e9tail,\u00a02022</i> propos\u00e9 la plupart des \u00e9tablissements d'aliments du b\u00e9tail devront cerner les dangers biologiques, chimiques et physiques li\u00e9s \u00e0 leurs activit\u00e9s li\u00e9es aux aliments du b\u00e9tail et analyser ces dangers afin de d\u00e9terminer s'ils posent un risque de contamination. Il s'agit d'une nouvelle exigence r\u00e9glementaire.</p>\n\n<p>Ce feuillet d'information s'applique \u00e0 vous si vous menez l'une des activit\u00e9s suivantes li\u00e9es aux aliments du b\u00e9tail doit d\u00e9terminer et analyser les dangers\u00a0:</p>\n\n<ul>\n<li>fabrication</li>\n<li>entreposage</li>\n<li>emballage</li>\n<li>\u00e9tiquetage</li>\n<li>vente</li>\n<li>exportation</li>\n</ul>\n\n<p>Ce r\u00e8glement ne s'applique pas aux aliments du b\u00e9tail fabriqu\u00e9s \u00e0 la ferme par les  \u00e9leveurs de b\u00e9tail si l'aliment n'est pas vendu \u00e0 la ferme et n'est pas m\u00e9dicament\u00e9.</p>\n \n<h2 id=\"a1\">Dangers</h2>\n\n<p>Un danger est d\u00e9fini comme un agent biologique, chimique ou physique pr\u00e9sent dans un aliment ou l'\u00e9tat d'un aliment susceptible d'entra\u00eener des effets ind\u00e9sirables sur la sant\u00e9 animale ou humaine ou sur l'environnement.</p>\n\n<h3 id=\"a1_1\">Types de dangers</h3>\n\n<ul>\n<li><a href=\"#a1_1_1\">Dangers biologiques</a></li>\n<li><a href=\"#a1_1_2\">Dangers chimiques</a></li>\n<li><a href=\"#a1_1_3\">Dangers physiques</a></li>\n</ul>\n\n<h4 id=\"a1_1_1\">Dangers biologiques</h4>\n\n<p>Les dangers biologiques comprennent les microorganismes tels que les bact\u00e9ries, les virus, les parasites, les champignons, les prions et les moisissures. Certains microorganismes peuvent entra\u00eener des maladies tandis que d'autres peuvent produire des toxines nocives.</p>\n\n<p>Voici quelques exemples de sources de dangers biologiques\u00a0:</p>\n\n<ul>\n<li>les ingr\u00e9dients entrants, y compris les mati\u00e8res premi\u00e8res;</li>\n<li>la contamination crois\u00e9e pendant la transformation, l'entreposage et le transport;</li>\n<li>les employ\u00e9s;</li>\n<li>les surfaces en contact avec les aliments;</li>\n<li>les insectes et les rongeurs.</li>\n</ul>\n\n<h4 id=\"a1_1_2\">Dangers chimiques</h4>\n\n<p>Les dangers chimiques peuvent comprendre des contaminants (des m\u00e9taux et des  \u00e9l\u00e9ments, des radionucl\u00e9ides, des agents de transformation des aliments et des dioxines) ainsi que des r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires et de pesticides et des toxines naturelles. Certains dangers chimiques surviennent naturellement tandis que d'autres sont ajout\u00e9s intentionnellement ou non intentionnellement pendant la fabrication et la transformation.</p>\n\n<p>Voici des exemples de dangers chimiques\u00a0:</p>\n\n<ul>\n<li>des produits chimiques utilis\u00e9s intentionnellement dans la fabrication d'aliments du b\u00e9tail, comme les agents de transformation des aliments, les additifs alimentaires et les substances m\u00e9dicatrices;</li>\n<li>des produits chimiques qui sont des sous-produits de la transformation;</li>\n<li>la contamination chimique par de l'\u00e9quipement;</li>\n<li>les produits chimiques industriels tels que les agents de nettoyage et de d\u00e9sinfection;</li>\n<li>les toxines d'origine naturelle comme les mycotoxines, les histamines et les biotoxines marines;</li>\n<li>les produits chimiques agricoles tels que les pesticides et les engrais;</li>\n<li>les \u00e9l\u00e9ments nutritifs tels que l'ajout excessif de vitamines ou de min\u00e9raux.</li>\n</ul>\n\n<h4 id=\"a1_1_3\">Dangers physiques</h4>\n\n<p>Les dangers physiques li\u00e9s aux aliments du b\u00e9tail comprennent de nombreux types de mati\u00e8res ind\u00e9sirables qui peuvent \u00eatre introduites \u00e0 une \u00e9tape de la cha\u00eene de production, de la production primaire \u00e0 la ferme (\u00e9leveurs de b\u00e9tail). Des mati\u00e8res ind\u00e9sirables peuvent \u00eatre introduites par toute personne ou tout objet entrant en contact avec les aliments du b\u00e9tail pendant la transformation, le transport ou l'entreposage. Les mati\u00e8res ind\u00e9sirables sont consid\u00e9r\u00e9es comme des dangers si elles risquent de porter pr\u00e9judice \u00e0 l'animal qui consomme les aliments ou si elles posent un risque de contamination de nourriture d'origine animale (\u0153ufs, produits et sous-produits laitiers et viande) consomm\u00e9e par les humains.</p>\n\n<p>Voici des exemples de dangers physiques\u00a0:</p>\n\n<ul>\n<li>les pierres, les roches et la terre;</li>\n<li>le m\u00e9tal (g\u00e9n\u00e9ralement associ\u00e9 aux activit\u00e9s de transformation comme le broyage ou le coupage, ainsi que les mat\u00e9riaux d'emballage ou les contenants);</li>\n<li>le verre, le plastique et d'autres contaminants provenant des mat\u00e9riaux d'emballage ou des contenants, ou de l'environnement de transformation;</li>\n<li>les \u00e9clats de bois provenant de palettes bris\u00e9es ou de mat\u00e9riaux d'emballage;</li>\n<li>la peinture \u00e9caill\u00e9e provenant de structures ou d'\u00e9quipements situ\u00e9s au plafond.</li>\n</ul>\n\n<h2>La d\u00e9termination et l'analyse des dangers</h2>\n\n<p>Les  \u00e9tablissements d'aliments du b\u00e9tail d\u00e9terminent et analysent les dangers en vue de cerner les dangers biologiques, chimiques et physiques li\u00e9s au mat\u00e9riel utilis\u00e9 et aux processus utilis\u00e9s \u00e0 l'\u00e9tablissement d'aliments du b\u00e9tail.</p>\n\n<p>La d\u00e9termination et l'analyse des dangers prennent en consid\u00e9ration l'incidence de tous les facteurs li\u00e9s \u00e0 la s\u00e9curit\u00e9 des aliments du b\u00e9tail, notamment\u00a0:</p>\n\n<ul>\n<li>la formulation des aliments du b\u00e9tail;</li>\n<li>les ingr\u00e9dients qui entrent dans la composition des aliments du b\u00e9tail, y compris les mati\u00e8res premi\u00e8res;</li>\n<li>la concentration de tout contaminant inh\u00e9rent aux aliments du b\u00e9tail;</li>\n<li>les proc\u00e9dures de fabrication, de transformation, d'emballage et l'\u00e9tiquetage des aliments du b\u00e9tail;</li>\n<li>l'entreposage et la distribution des aliments du b\u00e9tail;</li>\n<li>les pratiques de transport;</li>\n<li>la fin escompt\u00e9e des aliments du b\u00e9tail;</li>\n<li>l'\u00e9tat, le fonctionnement, la conception et l'assainissement de l'\u00e9tablissement et de l'\u00e9quipement;</li>\n<li>l'hygi\u00e8ne des employ\u00e9s;</li>\n<li>les conditions m\u00e9t\u00e9orologiques.</li>\n</ul>\n\n<p>Dans l'\u00e9tape d'identification des dangers, les dangers de toutes les entr\u00e9es et les  \u00e9tapes de traitement sont \u00e9num\u00e9r\u00e9s, d\u00e9crits et classifi\u00e9s. Ensuite, dans l'\u00e9tape de l'analyse des dangers, les dangers sont \u00e9valu\u00e9s en fonction de leur importance.</p>\n\n<p>Une fois l'identification et l'analyse des dangers termin\u00e9es, des mesures de contr\u00f4le sont appliqu\u00e9es pour pr\u00e9venir, \u00e9liminer ou r\u00e9duire chaque danger identifi\u00e9 \u00e0 un niveau acceptable. Les \u00e9tablissements d'aliments du b\u00e9tail doivent montrer que leurs mesures de contr\u00f4le sont efficaces.</p>\n\n<p>En outre, des points de contr\u00f4le critiques (CCP), des limites critiques, des proc\u00e9dures de surveillance, des proc\u00e9dures correctives et des proc\u00e9dures de v\u00e9rification doivent \u00eatre \u00e9tablis en fonction de chaque danger important.</p>\n\n<h2>Avantages de l'analyse des dangers</h2>\n\n<p>La d\u00e9termination et l'analyse des dangers sont la premi\u00e8re \u00e9tape d'une approche de contr\u00f4le pr\u00e9ventif. Ces activit\u00e9s permettent aux \u00e9tablissements d'aliments du b\u00e9tail de g\u00e9rer de fa\u00e7on proactive les dangers. Le contr\u00f4le proactif des dangers contribue \u00e0 prot\u00e9ger les aliments du b\u00e9tail et \u00e0 assurer la salubrit\u00e9 des aliments; il peut \u00e9galement s'av\u00e9rer plus rentable qu'une enqu\u00eate sur la s\u00e9curit\u00e9 des aliments du b\u00e9tail.</p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}