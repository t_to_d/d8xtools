{
    "dcr_id": "1426693888652",
    "title": {
        "en": "Frequently Asked Questions: <i>Food and Drug Regulations</i> (as they relate to food)",
        "fr": "Foire aux questions\u00a0: <i>R\u00e8glement sur les aliments et drogues</i> (dispositions relatives aux aliments)"
    },
    "html_modified": "16-6-2015",
    "modified": "18-3-2015",
    "issued": "18-3-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_fooddrug_regs_1426693888652_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_fooddrug_regs_1426693888652_fra"
    },
    "parent_ia_id": "1419029097256",
    "ia_id": "1426693913140",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Frequently Asked Questions: <i>Food and Drug Regulations</i> (as they relate to food)",
        "fr": "Foire aux questions\u00a0: <i>R\u00e8glement sur les aliments et drogues</i> (dispositions relatives aux aliments)"
    },
    "label": {
        "en": "Frequently Asked Questions: <i>Food and Drug Regulations</i> (as they relate to food)",
        "fr": "Foire aux questions\u00a0: <i>R\u00e8glement sur les aliments et drogues</i> (dispositions relatives aux aliments)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1426693913140",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1419029096537",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/acts-and-regulations/list-of-acts-and-regulations/faqs-food-and-drug-regulations-as-they-relate-to-f/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/faq-reglement-sur-les-aliments-et-drogues-disposit/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Frequently Asked Questions: Food and Drug Regulations (as they relate to food)",
            "fr": "Foire aux questions\u00a0: R\u00e8glement sur les aliments et drogues (dispositions relatives aux aliments)"
        },
        "description": {
            "en": "The Frequently Asked Questions below are meant to provide Canadians and businesses with general information about the Canadian Food Inspection Agency's regulations.",
            "fr": "La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les r\u00e8glements qu'applique l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "frequently asked questions, regulations, Food and Drug Regulations, cosmetic products, residues",
            "fr": "foire aux questions, r\u00e8glements, R\u00e8glement sur les aliments et drogues, produits cosm\u00e9tiques, r\u00e9sidus"
        },
        "dcterms.subject": {
            "en": "food,quality control,agri-food products,consumer protection,regulation",
            "fr": "aliment,contr\u00f4le de la qualit\u00e9,produit agro-alimentaire,protection du consommateur,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-06-16",
            "fr": "2015-06-16"
        },
        "modified": {
            "en": "2015-03-18",
            "fr": "2015-03-18"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Frequently Asked Questions: Food and Drug Regulations (as they relate to food)",
        "fr": "Foire aux questions\u00a0: R\u00e8glement sur les aliments et drogues (dispositions relatives aux aliments)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The <abbr title=\"Frequently Asked Questions\">FAQs</abbr> below are meant to provide Canadians and businesses with general information about the Canadian Food Inspection Agency's regulations.</p>\n<h2 class=\"h5\">1. What is the purpose of these regulations?</h2>\n<p>The <i>Food and Drug Regulations</i>, under the authority of the <i>Food and Drugs Act</i>, regulate food, drugs, and cosmetic products to help protect the health and safety of Canadians. The Canadian Food Inspection Agency is responsible for the enforcement of the provisions related to food.</p>\n<h2 class=\"h5\">2. What are the key elements of these regulations as they relate to the mandate of the Canadian Food Inspection Agency?</h2>\n<p><strong>Part A - Administration:</strong> sets out powers, definitions and obligations that generally apply throughout the regulations.</p>\n<p><strong>Part B - Food:</strong></p>\n<ul class=\"list-unstyled\">\n<li class=\"mrgn-bttm-md\"><strong>Division 1 - General:</strong> sets out additional definitions specific to foods as well as requirements for general labelling, allergen labelling and nutrition labelling; permissible nutrient content claims; and permissible health claims for foods.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Divisions 2 to 14 and 17 to 22 - Standards of Composition and Identity:</strong> set out 334 standards of composition and identity for a wide range of foods. These divisions are categorized on a commodity basis.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 15 - Adulteration of Food:</strong> sets out the tolerances and maximum residue limits for pest control products, veterinary drugs and other chemical substances that may be present in specified foods not to be considered adulterated, and therefore, prohibited from sale in Canada.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 16 - Food Additives:</strong> regulates the food additives that have been assessed and approved for use in Canada. This division also sets out the submission requirements for stakeholders seeking approval for the use of a new food additive, to extend the use of an existing food additive, or for any other change pertaining to food additives.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 23 - Food Packaging Materials:</strong> sets out a general prohibition against the sale of food in a package composed of material that may be harmful to health. The division also prescribes conditions for the use of specified substances in food packaging.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 24 - Foods for Special Use:</strong> prescribes the compositional requirements and labelling requirements specific to foods represented and sold for special dietary use (such as formulated liquid diets, meal replacements, nutritional supplements, prepackaged meals, foods sold by weight reduction clinics, and foods represented for use in low energy diets).</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 25 - Infant Foods and Human Milk Substitutes:</strong> prescribes the compositional requirements, acceptable food additives and labelling requirements specific to infant foods, human milk substitutes and foods containing human milk substitutes.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 26 - Food Irradiation:</strong> prescribes foods that may be irradiated, the permitted source and purpose of the irradiation as well as the acceptable dose absorbed by the food. This division also sets out the submission requirements to seek approval to irradiate a new food, to use a new source of irradiation, and to change a purpose of treatment and/or an absorption dose.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 27 - Low-Acid Foods Packaged In Hermetically Sealed Containers:</strong> sets out the requirements for the sale of low-acid foods in hermetically sealed containers.</li>\n\n<li class=\"mrgn-bttm-md\"><strong>Division 28 - Novel Foods:</strong> sets out the definition of a novel food as well as the pre-market notification requirements for the sale of such foods in Canada. Foods derived from Genetically Modified Organisms (GMOs) are considered novel foods.</li>\n</ul>\n\n<h2 class=\"h5\">3. How do these regulations affect Canadian businesses?</h2>\n<p>Parts A and B, and certain divisions of Part C of these regulations are most relevant to businesses when interacting with the Canadian Food Inspection Agency. These parts of the regulations provide businesses with requirements for the manufacture, packaging, labelling, importation, advertisement and/or distribution for sale of food and veterinary drug products in Canada.</p>\n\n<h2 class=\"h5\">4. When did these regulations come into force?</h2>\n<p>The <i>Food and Drug Regulations</i> came into force on May 25, 1949. The <i>Food and Drugs Act</i> was introduced in 1920 and by the late 1920s, regulations were developed under the Act.</p>\n\n<h2 class=\"h5\">5. Where can I get more information?</h2>\n<p>Please refer to the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">Food</a> section of the Canadian Food Inspection Agency's website for more information. Further information on the regulation of food in Canada is also available under the <a href=\"http://hc-sc.gc.ca/fn-an/index-eng.php\">Food &amp; Nutrition</a> section of Health Canada's website.</p>\n\n<p>Questions relating to the <i>Food and Drug Regulations</i> may be directed to the Food Safety Specialists at the <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> regional office</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les r\u00e8glements qu'applique l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<h2 class=\"h5\">1. Quel est l'objectif de ce r\u00e8glement?</h2>\n<p>Le <i>R\u00e8glement sur les aliments et drogues</i>, en application de la <i>Loi sur les aliments et drogues</i>, r\u00e9git les aliments, les m\u00e9dicaments et les produits cosm\u00e9tiques afin d'aider \u00e0 prot\u00e9ger la sant\u00e9 et la s\u00e9curit\u00e9 des Canadiens. Il incombe \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d'appliquer les dispositions relatives aux aliments.</p>\n<h2 class=\"h5\">2. Quels sont les principaux \u00e9l\u00e9ments de ce r\u00e8glement en lien avec le mandat de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>?</h2>\n<p>La <strong>partie A (Administration)</strong> pr\u00e9sente les pouvoirs, les d\u00e9finitions et les obligations qui s'appliquent de fa\u00e7on g\u00e9n\u00e9rale dans l'ensemble du r\u00e8glement.</p>\n<p><strong>Partie B \u2013 Aliments</strong></p>\n<ul class=\"list-unstyled\">\n<li class=\"mrgn-bttm-md\">Le <strong>titre 1 (Dispositions g\u00e9n\u00e9rales)</strong> \u00e9tablit des d\u00e9finitions suppl\u00e9mentaires qui concernent des aliments particuliers. On y trouve \u00e9galement les exigences relatives \u00e0 l'\u00e9tiquetage en g\u00e9n\u00e9ral, \u00e0 l'\u00e9tiquetage des allerg\u00e8nes, \u00e0 l'\u00e9tiquetage nutritionnel ainsi qu'aux all\u00e9gations acceptables concernant la valeur nutritive et les effets des aliments sur la sant\u00e9.</li>\n<li class=\"mrgn-bttm-md\">Les <strong>titres 2 \u00e0 14 et 17 \u00e0 22 (Normes de composition et d'identification)</strong> \u00e9noncent 334 normes de composition et d'identification qui visent une vaste gamme d'aliments. Ces titres sont d\u00e9finis par type de produit.</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 15 (Falsification des produits alimentaires)</strong> \u00e9tablit les seuils de tol\u00e9rance et les limites maximales de r\u00e9sidus pour les produits antiparasitaires, les m\u00e9dicaments v\u00e9t\u00e9rinaires et d'autres substances chimiques qui peuvent se retrouver dans des aliments en particulier sans que ceux-ci soient consid\u00e9r\u00e9s comme falsifi\u00e9s et, par cons\u00e9quent, sans que leur vente soit interdite au Canada.</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 16 (Additifs alimentaires)</strong> r\u00e9git les additifs alimentaires qui ont \u00e9t\u00e9 \u00e9valu\u00e9s et dont l'utilisation au Canada a \u00e9t\u00e9 approuv\u00e9e. Ce titre \u00e9nonce \u00e9galement les exigences que doivent satisfaire les intervenants qui souhaitent faire approuver l'utilisation d'un nouvel additif alimentaire ou une nouvelle utilisation d'un additif alimentaire autoris\u00e9 ou demander tout autre changement concernant les additifs alimentaires.</li>\n\n<li class=\"mrgn-bttm-md\">Aux termes du <strong>titre 23 (Mat\u00e9riaux \u00e0 emballer les denr\u00e9es alimentaires)</strong>, il est interdit de vendre un aliment dans un emballage compos\u00e9 de mat\u00e9riaux pouvant nuire \u00e0 la sant\u00e9. Ce titre prescrit \u00e9galement les modalit\u00e9s d'utilisation de certaines substances dans les emballages alimentaires.</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 24 (Aliments \u00e0 usage di\u00e9t\u00e9tique sp\u00e9cial)</strong> prescrit les exigences relatives \u00e0 la composition et les exigences relatives \u00e0 l'\u00e9tiquetage des aliments pr\u00e9sent\u00e9s et vendus \u00e0 des fins di\u00e9t\u00e9tiques sp\u00e9ciales (notamment les pr\u00e9parations pour r\u00e9gime liquide, les substituts de repas, les suppl\u00e9ments nutritifs, les repas pr\u00e9emball\u00e9s, les aliments vendus par des cliniques d'amaigrissement et les aliments pr\u00e9sent\u00e9s comme \u00e9tant con\u00e7us pour un r\u00e9gime \u00e0 tr\u00e8s faible teneur en \u00e9nergie).</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 25 (Aliments pour b\u00e9b\u00e9s \u2013 Succ\u00e9dan\u00e9s de lait humain)</strong> prescrit les exigences relatives \u00e0 la composition, les additifs alimentaires acceptables et les exigences en mati\u00e8re d'\u00e9tiquetage propres aux aliments pour b\u00e9b\u00e9s ainsi qu'aux succ\u00e9dan\u00e9s de lait humain et aux aliments qui en contiennent.</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 26 (Irradiation des aliments)</strong> \u00e9nonce les aliments qui peuvent \u00eatre irradi\u00e9s ainsi que la source de rayonnement et la dose de rayonnement absorb\u00e9e par l'aliment qui sont autoris\u00e9es. De plus, ce titre \u00e9nonce les exigences qu'il faut satisfaire pour faire approuver l'irradiation d'un nouvel aliment ou l'utilisation d'une nouvelle source d'irradiation ou pour modifier le but du traitement ou la dose absorb\u00e9e.</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 27 (Aliments peu acides emball\u00e9s dans des r\u00e9cipients herm\u00e9tiquement ferm\u00e9s)</strong> pr\u00e9sente les exigences qui r\u00e9gissent la vente d'aliments peu acides dans des contenants herm\u00e9tiquement ferm\u00e9s.</li>\n\n<li class=\"mrgn-bttm-md\">Le <strong>titre 28 (Aliments nouveaux)</strong> \u00e9tablit la d\u00e9finition d'un aliment nouveau ainsi que les exigences relatives aux avis pr\u00e9alables \u00e0 la mise en march\u00e9 de tels aliments au Canada. Les aliments d\u00e9riv\u00e9s d'organismes g\u00e9n\u00e9tiquement modifi\u00e9s sont consid\u00e9r\u00e9s comme des aliments nouveaux.</li>\n</ul>\n\n<h2 class=\"h5\">3. Quelle incidence ce r\u00e8glement a-t-il sur les entreprises canadiennes?</h2>\n<p>Les parties A et B et certains titres de la partie C du R\u00e8glement sont les plus pertinentes pour les entreprises lorsqu'elles interagissent avec l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Ces parties du R\u00e8glement prescrivent, \u00e0 l'intention des entreprises, des exigences concernant la fabrication, l'emballage, l'\u00e9tiquetage, l'importation et la distribution aux fins de vente d'aliments et de m\u00e9dicaments v\u00e9t\u00e9rinaires au Canada ainsi que la publicit\u00e9 relative \u00e0 ces derniers.</p>\n\n<h2 class=\"h5\">4. \u00c0 quel moment ce r\u00e8glement est-il entr\u00e9 en vigueur?</h2>\n<p>Le <i>R\u00e8glement sur les aliments et drogues</i> est entr\u00e9 en vigueur le 25 mai 1949. La <i>Loi sur les aliments et drogues</i> a \u00e9t\u00e9 adopt\u00e9e en 1920, et le R\u00e8glement a \u00e9t\u00e9 \u00e9labor\u00e9 en vertu de celle-ci vers la fin des ann\u00e9es 1920.</p>\n<h2 class=\"h5\">5. O\u00f9 puis-je obtenir plus d'information?</h2>\n<p>Pour en savoir plus, veuillez consulter la page <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">Aliments</a> du site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. En outre, de plus amples renseignements sur la r\u00e9glementation visant les aliments au Canada se trouvent dans la section <a href=\"http://hc-sc.gc.ca/fn-an/index-fra.php\">Aliments et nutrition</a> du site Web de Sant\u00e9 Canada.</p>\n\n<p>Les questions concernant le <i>R\u00e8glement sur les aliments et drogues</i> peuvent \u00eatre transmises aux sp\u00e9cialistes de la salubrit\u00e9 des aliments aux <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureaux r\u00e9gionaux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}