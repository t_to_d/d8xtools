{
    "dcr_id": "1483638747905",
    "title": {
        "en": "Aquatic animal export: certification requirements for Pakistan",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Pakistan"
    },
    "html_modified": "2024-03-12 3:57:35 PM",
    "modified": "2020-01-07",
    "issued": "2017-01-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_pakistan_1483638747905_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_pakistan_1483638747905_fra"
    },
    "ia_id": "1483638784343",
    "parent_ia_id": "1331743129372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1331743129372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal export: certification requirements for Pakistan",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Pakistan"
    },
    "label": {
        "en": "Aquatic animal export: certification requirements for Pakistan",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Pakistan"
    },
    "templatetype": "content page 1 column",
    "node_id": "1483638784343",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331740343115",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/pakistan/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/pakistan/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal export: certification requirements for Pakistan",
            "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Pakistan"
        },
        "description": {
            "en": "This page describes aquatic animal health export certification requirements for Pakistan.",
            "fr": "Cette page d\u00e9crit les exigences de certification d'exportation de sant\u00e9 des animaux aquatiques pour Pakistan."
        },
        "keywords": {
            "en": "aquatic animals, finfish, molluscs, crustacean, exports, human consumption, certification requirements, aquatic animal health, seafood products, NAAHP, Pakistan",
            "fr": "animaux aquatiques, poisson \u00e0 nageoires, mollusques, crustac\u00e9s, exportation,  consommation humaine, exigences de certification, sant\u00e9 des animaux aquatiques, produits du poisson et des produits de la mer, PNSAA, Pakistan"
        },
        "dcterms.subject": {
            "en": "crustaceans,exports,inspection,molluscs,fish,animal health",
            "fr": "crustac\u00e9,exportation,inspection,mollusque,poisson,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Government of Canada,Canadian Food Inspection Agency"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-01-09",
            "fr": "2017-01-09"
        },
        "modified": {
            "en": "2020-01-07",
            "fr": "2020-01-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal export: certification requirements for Pakistan",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Pakistan"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Pakistan has aquatic animal health requirements for imports of live salmonids, their gametes or fertilized eggs for purposes of aquaculture.</p>\n\n<p>Canada is currently in negotiation with Pakistan for export certification of live salmonids, their gametes or fertilized eggs for purposes of aquaculture. For this specific commodity, until such time as a negotiated certificate is available, shipments exported without an aquatic animal health certificate may be detained in Pakistan.</p>\n\n<h2 class=\"h5\">Important Notes:</h2>\n\n<ul>\n\n<li class=\"mrgn-bttm-md\">If you require any other information on aquatic animal health export certification, please contact your <a href=\"/eng/1300462382369/1365216058692\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a>.</li></ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Le Pakistan a des exigences relatives \u00e0 la sant\u00e9 des animaux aquatiques r\u00e9gissant l'importation de salmonid\u00e9s vivants ainsi que de leurs gam\u00e8tes ou de leur mat\u00e9riel g\u00e9n\u00e9tique aux fins d'aquaculture.</p>\n\n<p>Le Canada n\u00e9gocie actuellement avec le Pakistan concernant la certification des exportations de salmonid\u00e9s vivants ainsi que de leurs gam\u00e8tes ou de leur mat\u00e9riel g\u00e9n\u00e9tique aux fins d'aquaculture. Par cons\u00e9quent, jusqu'\u00e0 ce qu'un certificat n\u00e9goci\u00e9 soit disponible, les envois de ces produits sans certificat d'hygi\u00e8ne des animaux aquatiques pourraient \u00eatre retenus au Pakistan.</p>\n\n<h2 class=\"h5\">Remarques importantes\u00a0:</h2>\n\n<ul>\n\n<li class=\"mrgn-bttm-md\">Pour toute autre demande de renseignements concernant la certification sanitaire des animaux aquatiques destin\u00e9s \u00e0 l'exportation, veuillez communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> de votre Centre op\u00e9rationnel.</li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}