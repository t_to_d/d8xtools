{
    "dcr_id": "1431014338863",
    "title": {
        "en": "Categories of hazards: keeping food safe",
        "fr": "Cat\u00e9gories de risques : comment pr\u00e9server la salubrit\u00e9 des aliments"
    },
    "html_modified": "2024-02-13 9:50:47 AM",
    "modified": "2022-08-12",
    "issued": "2019-05-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/regs_safe_food_inforgraphic_hazards_1431014338863_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/regs_safe_food_inforgraphic_hazards_1431014338863_fra"
    },
    "ia_id": "1431014339457",
    "parent_ia_id": "1427299800380",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Quick reference",
        "fr": "Aide-m\u00e9moire"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Relevant to All|Applying preventive controls",
        "fr": "Pertinents pour tous|Application de mesures de contr\u00f4le pr\u00e9ventif"
    },
    "commodity": {
        "en": "Relevant to All",
        "fr": "Pertinents pour tous"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1427299800380",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Categories of hazards: keeping food safe",
        "fr": "Cat\u00e9gories de risques : comment pr\u00e9server la salubrit\u00e9 des aliments"
    },
    "label": {
        "en": "Categories of hazards: keeping food safe",
        "fr": "Cat\u00e9gories de risques : comment pr\u00e9server la salubrit\u00e9 des aliments"
    },
    "templatetype": "content page 1 column",
    "node_id": "1431014339457",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1427299500843",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-industry/toolkit-for-food-businesses/categories-of-hazards/",
        "fr": "/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/categories-de-risques/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Categories of hazards: keeping food safe",
            "fr": "Cat\u00e9gories de risques : comment pr\u00e9server la salubrit\u00e9 des aliments"
        },
        "description": {
            "en": "This infographic shows international best practices to identify food hazards, in order to keep Canadians safe from harmful illness or injury.",
            "fr": "Cette infographie illustre les pratiques exemplaires adopt\u00e9es \u00e0 l'\u00e9chelle internationale pour cerner les risques li\u00e9s aux aliments et ainsi att\u00e9nuer les risques de maladie ou de blessure pour les Canadiens."
        },
        "keywords": {
            "en": "acts, regulations, legislation, Safe Food for Canadian Act, Safe Food for Canadian Regulations, food safety, consumers, hazard, biological hazard, chemical hazard, physical hazard",
            "fr": "lois, r\u00e8glements, l\u00e9gislation, Loi sur la salubrit\u00e9 des aliments au Canada, R\u00e8glement sur la Salubrit\u00e9 des aliments au Canada, salubrit\u00e9 des aliments, consommateurs, dangers, dangers biologiques, dangers chimiques, dangers physiques"
        },
        "dcterms.subject": {
            "en": "agri-food products,food,food inspection,food labeling,food processing,legislation,regulation",
            "fr": "produit agro-alimentaire,aliment,inspection des aliments,\u00e9tiquetage des aliments, transformation des aliments,l\u00e9gislation,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-05-14",
            "fr": "2019-05-14"
        },
        "modified": {
            "en": "2022-08-12",
            "fr": "2022-08-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Categories of hazards: keeping food safe",
        "fr": "Cat\u00e9gories de risques : comment pr\u00e9server la salubrit\u00e9 des aliments"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1526992762174\"></div>\n\n\n\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/regs_safe_food_inforgraphic_hazards_1431107723281_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/WORKAREA/DAM-comn-comn/images-images/download_pdf_1558729521562_eng.png\" alt=\"\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Infographic: Categories of Hazards</span> <span class=\"gc-dwnld-info nowrap\">(PDF - 67 kb)</span></p>\n</div>\n</div>\n</div>\n</a>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n\n<figure>\n\n<p><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/regs_safe_food_inforgraphic_hazards_1431107650954_eng.jpg\" class=\"img-responsive\" alt=\"Infographic: How to Keep Food Safe: Categories of Hazards. Description follows.\"></p>\n\n<details>\n\n<summary>Description of infographic: How to Keep Food Safe: Categories of Hazards</summary>\n\n<p><strong>Keeping food safe</strong></p>\n<p>The following are considered  to be international <strong>best practices</strong> to identify food hazards. A hazard is  anything present in food with the potential to harm someone, either by causing  illness or injury.<strong> </strong></p>\n<h2>Biological Hazards:</h2>\n<p>Bacteria, viruses, or parasites that could cause foodborne illness</p>\n\n<p>Watch out for:</p>\n<ul>\n<li>Staff with poor hygiene or food handling techniques</li>\n<li>Bacteria commonly found in food</li>\n<li>Storing or preparing food at a temperature that allows bacteria to grow</li>\n<li>Ingredients that have spoiled</li>\n</ul>\n\n<p>Protect your food by:</p>\n<ul>\n<li>Rotating stock</li>\n<li>Storing and preparing food at proper temperatures</li>\n<li>Practicing good hygiene in your facilities</li>\n</ul>\n\n<h2>Chemical Hazards:</h2>\n<p>Anything that could introduce an unwanted chemical into your food</p>\n\n<p>Watch out for:</p>\n<ul>\n<li>Food in contact with cleaning chemicals</li>\n<li>Unintentional contact with common food allergens, such as peanuts or seafood</li>\n<li>Improper use of food additives during preparation</li>\n</ul>\n\n<p>Protect your food by:</p>\n<ul>\n<li>Labelling and storing chemicals separately from food</li>\n<li>Using correct cleaning and preparation procedures</li>\n</ul>\n\n<h2>Physical Hazards:</h2>\n<p>Unintentional or dangerous materials that could end up in your food</p>\n\n<p>Watch out for:</p>\n<ul>\n<li>Personal objects, such as jewellery, that may fall into the food</li>\n<li>Materials that do not belong in some food, such as bone chips, leaves, shells and pits</li>\n</ul>\n\n<p>Protect your food by:</p>\n<ul>\n<li>Conducting regular visual inspections</li>\n<li>Following appropriate procedures in your facility</li>\n</ul>\n\n<p>By preventing food safety hazards, you:</p>\n<ul>\n<li>Reduce the likelihood of foodborne illness and recalls</li>\n<li>Protect your business and reputation</li>\n</ul>\n\n<h2>Did you know?</h2>\n<p>Food can become contaminated during growing, harvesting, processing, shipping, storing or handling.</p>\n\n</details>\n\n</figure>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1526992762174\"></div>\n\n\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/regs_safe_food_inforgraphic_hazards_1431107723281_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/WORKAREA/DAM-comn-comn/images-images/download_pdf_1558729521562_fra.png\" alt=\"\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Infographies\u00a0: Cat\u00e9gories de risques</span> <span class=\"gc-dwnld-info nowrap\">(PDF - 67 ko)</span></p>\n</div>\n</div>\n</div>\n</a>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n\n<figure>\n\n<p><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/regs_safe_food_inforgraphic_hazards_1431107650954_fra.jpg\" class=\"img-responsive\" alt=\"Infographie : Comment pr\u00e9server la salubrit\u00e9 des aliments : Cat\u00e9gories de dangers. Description ci-dessous.\"></p>\n\n<details>\n\n<summary>Description de l'infographie\u00a0: Comment pr\u00e9server la salubrit\u00e9 des aliments\u00a0: Cat\u00e9gories de dangers</summary>\n\n<p><strong>Comment pr\u00e9server la salubrit\u00e9 des aliments</strong></p>\n<p>Les principes suivants sont consid\u00e9r\u00e9s comme des <strong>pratiques exemplaires</strong> internationales pour cerner les risques associ\u00e9s aux aliments. Un danger d\u00e9signe tout ce qui est pr\u00e9sent dans les aliments et qui pourrait nuire \u00e0 quelqu'un en provoquant une maladie ou une blessure.</p>\n\n<h2>Risques biologiques</h2>\n<p>Bact\u00e9ries, virus ou parasites qui pourraient causer une maladie d'origine alimentaire</p>\n\n<p>Portez attention\u00a0:</p>\n<ul>\n<li>aux employ\u00e9s ayant une mauvaise hygi\u00e8ne ou de mauvaises techniques de manipulation des aliments</li>\n<li>aux bact\u00e9ries souvent d\u00e9tect\u00e9es dans les aliments</li>\n<li>\u00e0 l'entreposage ou \u00e0 la pr\u00e9paration d'aliments \u00e0 une temp\u00e9rature propice \u00e0 la prolif\u00e9ration des bact\u00e9ries</li>\n<li>aux ingr\u00e9dients avari\u00e9s</li>\n</ul>\n\n<p>Prot\u00e9gez vos aliments\u00a0:</p>\n<ul>\n<li>en faisant une rotation des stocks</li>\n<li>en entreposant et en conditionnant les aliments aux temp\u00e9ratures ad\u00e9quates</li>\n<li>en appliquant de bonnes pratiques d'hygi\u00e8ne dans votre \u00e9tablissement</li>\n</ul>\n\n<h2>Risques chimiques</h2>\n<p>Tout ce qui pourrait introduire un produit chimique ind\u00e9sirable dans vos aliments</p>\n\n<p>Portez attention\u00a0:</p>\n<ul>\n<li>aux aliments en contact avec des produits chimiques de nettoyage</li>\n<li>au contact accidentel avec des allerg\u00e8nes alimentaires communs (par exemple, arachides ou fruits de mer)</li>\n<li>\u00e0 l'utilisation inappropri\u00e9e d'additifs alimentaires durant le conditionnement</li>\n</ul>\n\n<p>Prot\u00e9gez vos aliments\u00a0:</p>\n<ul>\n<li>en \u00e9tiquetant les produits chimiques et en les entreposant \u00e0 part des aliments</li>\n<li>en employant de bonnes proc\u00e9dures de nettoyage et de conditionnement</li>\n</ul>\n\n<h2>Risques physiques</h2>\n<p>Mati\u00e8res dangereuses ou pouvant se retrouver accidentellement dans vos aliments</p>\n\n<p>Portez attention\u00a0:</p>\n<ul>\n<li>aux objets personnels, comme les bijoux, qui peuvent tomber dans les aliments</li>\n<li>aux mati\u00e8res qui ne devraient pas se retrouver dans les aliments (par exemple, fragments d'os, feuilles, \u00e9cailles, noyaux)</li>\n</ul>\n\n<p>Prot\u00e9gez vos aliments\u00a0:</p>\n<ul>\n<li>en effectuant r\u00e9guli\u00e8rement des inspections visuelles</li>\n<li>en respectant les proc\u00e9dures appropri\u00e9es dans votre \u00e9tablissement</li>\n</ul>\n\n<p>En pr\u00e9venant les risques associ\u00e9s \u00e0 la salubrit\u00e9 des aliments, vous\u00a0:</p>\n<ul>\n<li>r\u00e9duisez la probabilit\u00e9 de maladies d'origine alimentaire et de rappels d'aliments</li>\n<li>prot\u00e9gez votre entreprise et votre r\u00e9putation</li>\n</ul>\n\n<h2>Le saviez-vous?</h2>\n<p>Les aliments peuvent \u00eatre contamin\u00e9s durant la culture, la r\u00e9colte, la transformation, l'exp\u00e9dition, l'entreposage ou la manipulation.</p>\n\n</details>\n\n</figure>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "success": true,
    "chat_wizard": true
}