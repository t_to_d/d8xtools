{
    "dcr_id": "1580436959720",
    "title": {
        "en": "Regulatory transparency and openness",
        "fr": "Transparence et ouverture"
    },
    "html_modified": "2024-02-13 9:46:37 AM",
    "modified": "2021-03-18",
    "issued": "2020-03-27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/reg_trnprcy_opnss_1580436959720_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/reg_trnprcy_opnss_1580436959720_fra"
    },
    "ia_id": "1580436960095",
    "parent_ia_id": "1300126652547",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1300126652547",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Regulatory transparency and openness",
        "fr": "Transparence et ouverture"
    },
    "label": {
        "en": "Regulatory transparency and openness",
        "fr": "Transparence et ouverture"
    },
    "templatetype": "content page 1 column",
    "node_id": "1580436960095",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300126592747",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/regulatory-transparency-and-openness/",
        "fr": "/a-propos-de-l-acia/transparence/transparence-et-ouverture/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Regulatory transparency and openness",
            "fr": "Transparence et ouverture"
        },
        "description": {
            "en": "The Government of Canada is making more data and information available to Canadians than ever before.",
            "fr": "Le gouvernement du Canada met plus de donn\u00e9es et de renseignements \u00e0 la disposition des Canadiens et des Canadiennes que jamais auparavant."
        },
        "keywords": {
            "en": "regulatory transparency and openness, accountability, public opinion research, travel, hospitality, conferences, wrongdoing",
            "fr": "Transparence et ouverture, responsabilisation, march\u00e9s, voyages, subventions et contributions, recherche sur l'opinion publique, voayges, d'accueil, conf\u00e9rences, actes fautifs"
        },
        "dcterms.subject": {
            "en": "government information,inspection",
            "fr": "information gouvernementale,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-03-27",
            "fr": "2020-03-27"
        },
        "modified": {
            "en": "2021-03-18",
            "fr": "2021-03-18"
        },
        "type": {
            "en": "financial report,reference material",
            "fr": "rapport financier\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government,media",
            "fr": "grand public,gouvernement,m\u00e9dia"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Regulatory transparency and openness",
        "fr": "Transparence et ouverture"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-tp-lg\">The Government of Canada is making more data and information available to Canadians than ever before. Canadians are also being offered more opportunities to participate in discussions on government policies and priorities.</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) plays a key role in protecting the health and safety of Canadians and is committed to greater transparency and openness.</p>\n\n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">Openness and transparency at the CFIA</a>\n<ul>\n<li><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/2020-progress-report/eng/1614188040900/1614188231148\">Open and Transparent Agency Progress report</a> <span class=\"label label-info\">New</span><br>\n</li>\n</ul></li>\n<li><a href=\"#a6\">Compliance and enforcement</a></li>\n<li><a href=\"#a2\">Reports on food safety incidents in Canada</a></li>\n<li><a href=\"#a3\">Reports on inspection activities</a></li>\n<li><a href=\"#a4\">Outline of CFIA's effective service delivery</a></li>\n<li><a href=\"#a5\">Governing principles of CFIA's interactions with producers, consumers and other stakeholders</a></li>\n</ul>\n\n<h2 id=\"a1\">Openness and transparency at the CFIA</h2>\n\n<p>Together, the <a href=\"/about-the-cfia/transparency/framework-2019-2022/eng/1554860822548/1554860822782\">Open and Transparent Agency Framework 2019-2022</a> | <a href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/acc_open_transparency_framwrk_2019-2022_1554861382026_eng.pdf\" class=\"nowrap\">PDF (1,153 kb)</a> and <a href=\"/about-the-cfia/transparency/policy/eng/1554871292993/1554871293227\">Open and Transparent Agency Policy</a> provide Agency direction to chart the path for the next phase of CFIA's Transparency Agenda:</p>\n\n<ul>\n<li class=\"mrgn-bttm-sm\">The framework provides the blueprint to guide the Agency's efforts in making more information available about its decisions and activities.</li>\n<li class=\"mrgn-bttm-sm\">The policy sets out the high-level direction CFIA is taking to meet its openness and transparency objectives and bring the framework to life.</li>\n</ul>\n\n<p>The Open and Transparent Agency Framework and Policy were developed with input from CFIA employees and external stakeholders. The Agency shared initial drafts and gathered comments during <a href=\"/about-the-cfia/transparency/consultations-and-engagement/completed/enhancing-openness-and-transparency/eng/1529328240473/1529328499279\">consultations</a> held in 2018, which were analyzed and used to improve these documents.</p>\n\n<p>A summary of feedback received from external stakeholders is available in the <a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/sound-agency-management/external-consultation-on-openness-and-transparency/eng/1550118277727/1550118505770\">What We Learned Report</a>.</p>\n<p>The <a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/2020-progress-report/eng/1614188040900/1614188231148\">2020 Open and Transparent Agency Progress Report</a> demonstrates CFIA's continued commitment to openness, transparency, and to keeping stakeholders informed about its progress.</p>\n\n<h3>For more information</h3>\n\n<p><a href=\"/about-the-cfia/transparency/questions-and-answers/eng/1554758982396/1554758982708\">Questions and answers: Openness and transparency at CFIA</a></p>\n\n<h2 id=\"a6\">Compliance and Enforcement</h2>\n<ul><li><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/compliance-and-enforcement/eng/1299846323019/1299846384123\">Data on its enforcement and compliance activities</a></li></ul>\n\n\n<h2 id=\"a2\">Reports on food safety incidents in Canada</h2>\n\n<ul>\n<li><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/food-safety-investigations/eng/1332299626115/1332299812611\">Food Safety Investigation Reports</a></li>\n</ul>\n\n<h2 id=\"a3\">Reports on inspection activities</h2>\n\n<ul>\n<li><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/inspection-capacity/eng/1521596857393/1521596949071\">Inspection capacity</a></li>\n</ul>\n\n<h2 id=\"a4\">Outline of CFIA's effective service delivery</h2>\n\n<ul>\n<li><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/sound-agency-management/eng/1299845842522/1299845904097\">Sound Agency Management</a></li>\n</ul>\n\n<h2 id=\"a5\">Governing principles of CFIA's interactions with producers, consumers and other stakeholders</h2>\n\n<ul class=\"mrgn-bttm-lg\">\n<li><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/rights-and-services/eng/1326306477874/1326306582012\">Statement of Rights and Service</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-tp-lg\">Le gouvernement du Canada met plus de donn\u00e9es et de renseignements \u00e0 la disposition des Canadiens et des Canadiennes que jamais auparavant. Par ailleurs, les Canadiens et les Canadiennes se voient offrir plus de possibilit\u00e9s de participer aux discussions ax\u00e9es sur les politiques et les priorit\u00e9s du gouvernement.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) joue un r\u00f4le important dans la protection de la sant\u00e9 et de la s\u00e9curit\u00e9 des Canadiens et des Canadiennes et s'engage \u00e0 offrir un niveau accru de transparence et d'ouverture.</p>\n\n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Transparence et ouverture \u00e0 l'ACIA</a>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/rapport-de-situation-2020/fra/1614188040900/1614188231148\">Rapport de situation en mati\u00e8re d'ouverture et de transparence 2020</a> <span class=\"label label-info\">Nouveau</span><br>\n</li>\n</ul></li>\n<li><a href=\"#a6\">Conformit\u00e9 et application de la loi</a></li>\n<li><a href=\"#a2\">Rapports sur les incidents li\u00e9s \u00e0 la salubrit\u00e9 des aliments au Canada</a></li>\n<li><a href=\"#a3\">Rapports sur les activit\u00e9s d'inspection</a></li>\n<li><a href=\"#a4\">Un aper\u00e7u de la prestation de services efficaces de l'ACIA</a></li>\n<li><a href=\"#a5\">Les principes gouvernant des interactions de l'ACIA avec les producteurs, les consommateurs et les autres intervenants</a></li>\n</ul>\n\n<h2 id=\"a1\">Transparence et ouverture \u00e0 l'ACIA</h2>\n\n<p>Ensemble, le <a href=\"/a-propos-de-l-acia/transparence/cadre-de-2019-2022/fra/1554860822548/1554860822782\">Cadre pour une Agence ouverte et transparente 2019-2022</a>\u00a0|\u00a0<a href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/acc_open_transparency_framwrk_2019-2022_1554861382026_fra.pdf\" class=\"nowrap\">PDF (1 070 ko)</a> et la <a href=\"/a-propos-de-l-acia/transparence/politique/fra/1554871292993/1554871293227\">Politique pour une Agence ouverte et transparente</a> permettront \u00e0 l'Agence de tracer la voie de la prochaine \u00e9tape de son programme de transparence\u00a0:</p>\n\n<ul>\n<li class=\"mrgn-bttm-sm\">Le cadre d\u00e9crit les actions que prendra l'Agence pour rendre accessibles davantage de renseignements au sujet de ses d\u00e9cisions et activit\u00e9s</li>\n<li class=\"mrgn-bttm-sm\">La politique d\u00e9crit l'orientation que prendra l'ACIA pour atteindre ses objectifs en mati\u00e8re d'ouverture et de transparence et donner vie au cadre</li>\n</ul>\n\n<p>Le cadre et la politique pour une Agence ouverte et transparente ont \u00e9t\u00e9 \u00e9labor\u00e9s avec la participation des employ\u00e9s de l'ACIA et des intervenants externes. L'Agence a partag\u00e9 la version provisoire de ces documents et recueilli des commentaires lors de <a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/terminees/ameliorer-l-ouverture-et-la-transparence/fra/1529328240473/1529328499279\">consultations</a> en 2018, lesquels ont \u00e9t\u00e9 analys\u00e9s et utilis\u00e9s pour am\u00e9liorer les documents.</p>\n\n<p>Pour obtenir de plus amples renseignements sur ce que le personnel de l'ACIA et les intervenants externes ont dit au sujet de l'ouverture et de la transparence accrues \u00e0 l'Agence, veuillez consulter le rapport <a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/une-saine-gestion-de-l-agence/rapport-de-consultation-externe-sur-l-ouverture-et/fra/1550118277727/1550118505770\">Ce que nous avons appris</a>.</p>\n<p>Le <a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/rapport-de-situation-2020/fra/1614188040900/1614188231148\">Rapport de situation\u00a0en mati\u00e8re d'ouverture et de transparence 2020</a> montre l'engagement continu de l'ACIA de faire preuve d'ouverture et de transparence et de tenir les intervenants au courant de ses progr\u00e8s.</p>\n\n<h3>Pour obtenir de plus amples renseignements</h3>\n\n<p><a href=\"/a-propos-de-l-acia/transparence/questions-et-reponses/fra/1554758982396/1554758982708\">Questions et r\u00e9ponses\u00a0: Ouverture et transparence \u00e0 l'ACIA</a></p>\n\n<h2 id=\"a6\">Conformit\u00e9 et application de la loi</h2>\n<ul><li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/conformite-et-application-de-la-loi/fra/1299846323019/1299846384123\">Donn\u00e9es sur ses activit\u00e9s en mati\u00e8re de conformit\u00e9 et d'application de la loi</a></li></ul>\n\n\n<h2 id=\"a2\">Rapports sur les incidents li\u00e9s \u00e0 la salubrit\u00e9 des aliments au Canada</h2>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/enquete-sur-la-salubrite-des-aliments/fra/1332299626115/1332299812611\">Rapports d'enqu\u00eate sur la salubrit\u00e9 des aliments</a></li>\n</ul>\n\n<h2 id=\"a3\">Rapports sur les activit\u00e9s d'inspection</h2>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/capacite-d-inspection/fra/1521596857393/1521596949071\">Capacit\u00e9 d'inspection</a></li>\n</ul>\n\n<h2 id=\"a4\">Un aper\u00e7u de la prestation de services efficaces de l'ACIA</h2>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/une-saine-gestion-de-l-agence/fra/1299845842522/1299845904097\">Une saine gestion de l'Agence</a></li>\n</ul>\n\n<h2 id=\"a5\">Les principes gouvernant des interactions de l'ACIA avec les producteurs, les consommateurs et les autres intervenants</h2>\n\n<ul class=\"mrgn-bttm-lg\">\n<li><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/droits-et-services/fra/1326306477874/1326306582012\">\u00c9nonc\u00e9 des droits et des services</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}