{
    "dcr_id": "1521739667281",
    "title": {
        "en": "Regulatory requirements: Dairy products",
        "fr": "Exigences r\u00e9glementaires : Produits laitiers"
    },
    "html_modified": "13-6-2018",
    "modified": "13-6-2018",
    "issued": "22-3-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/food_sfcr_specific_dairy_1521739667281_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/food_sfcr_specific_dairy_1521739667281_fra"
    },
    "parent_ia_id": "1526653268554",
    "ia_id": "1521739699003",
    "parent_node_id": "1526653268554",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Regulatory requirements: Dairy products",
        "fr": "Exigences r\u00e9glementaires : Produits laitiers"
    },
    "label": {
        "en": "Regulatory requirements: Dairy products",
        "fr": "Exigences r\u00e9glementaires : Produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1521739699003",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526653268334",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-guidance-by-commodity/dairy/regulatory-requirements/",
        "fr": "/exigences-et-documents-d-orientation-relatives-a-c/produits-laitiers/exigences-reglementaires/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Regulatory requirements: Dairy products",
            "fr": "Exigences r\u00e9glementaires : Produits laitiers"
        },
        "description": {
            "en": "This document provides an overview of the regulatory requirement specific to dairy products found in Part 6, Division 2 of the SFCR related to raw milk and/or raw cream used to prepare dairy products.",
            "fr": "Ce document donne un aper\u00e7u des exigences r\u00e9glementaires propres aux produits laitiers qui figurent \u00e0 la partie 6, section 2 du RSAC associ\u00e9s au lait et \u00e0 la cr\u00e8me utilis\u00e9s dans la pr\u00e9paration de produits laitiers."
        },
        "keywords": {
            "en": "Safe Food for Canadians Regulations, SFCR, dairy products, requirements, food, milk, cream, hazards",
            "fr": "R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, produits laitiers, exigences, aliment, lait, cr\u00e8me, dangers"
        },
        "dcterms.subject": {
            "en": "cream,agri-food industry,milk,dairy products,regulations,food safety",
            "fr": "cr\u00e8me,industrie agro-alimentaire,lait,produit laitier,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Domestic Food Safety Systems and Meat Hygiene Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des sys. de la salub. alim. nation. et de l'hygi\u00e8ne des viandes"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Regulatory requirements: Dairy products",
        "fr": "Exigences r\u00e9glementaires : Produits laitiers"
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>Introduction</h2>\n<p>While the <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\">Safe Food for Canadians Regulations</a> (SFCR) include a set of <a href=\"/eng/1512149634601/1512149659119\">general requirements</a> that apply to a broad range of <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a80\">foods</a>, there are some requirements that apply only to certain foods. This document provides an overview of the regulatory requirement specific to <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a68\">dairy products</a> found in Part\u00a06, Division\u00a02 of the <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr>.</p>\n<h2>Rationale</h2> \n<p>Raw milk and raw cream are used as ingredients to produce a variety of <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a68\">dairy products</a>. Like other incoming ingredients, raw milk and raw cream have the potential to contain <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a10\">hazards</a> that could pose a risk to human health. Some of the requirements regarding the safety and quality of incoming raw milk and raw cream used to prepare dairy products fall under provincial jurisdiction. Using raw milk and raw cream that meet provincial requirements helps to ensure the safety and quality of the dairy products you prepare.</p>\n<h2>What this means for your food business</h2>\n<p>To help you understand this requirement, specific criteria and examples are outlined below. The examples are not exhaustive but help illustrate the intent of the requirement and offer examples of what you could do to comply. In addition, key terms throughout the text have been hyperlinked to the <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405\"><abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr> glossary</a>.</p> \n<h2>Section 94:  Requirement for raw milk and/or raw cream used to prepare dairy products</h2>\n<ul>\n<li>The raw milk and/or raw cream that you use to prepare a <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a68\">dairy product</a> that will be <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a48\">sent or conveyed from one province or territory to another</a> or <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405#a34\">exported</a> meet the applicable requirements of the province in which your dairy product is prepared.</li>\n</ul>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section>\n<h3 class=\"mrgn-tp-0\">Examples:</h3> \n<ul>\n<li>You contact your province's food safety authority for more information on the provincial requirements for the raw milk and/or raw cream you import or source domestically and use to prepare dairy products.</li>\n<li>If there are no applicable provincial requirements, like with any other ingredient, you make sure that the raw milk and/or raw cream you use to prepare dairy products meets the requirements of your process.</li>\n<li class=\"mrgn-bttm-0\">You refer to the <a href=\"https://www.dairyinfo.gc.ca/eng/acts-regulations-codes-and-standards/national-dairy-code-part-i/?id=1503084167796\">National Dairy Code</a><sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup> for guidelines regarding the criteria for raw milk. The National Dairy Code contains details on various topics such as the requirements for the handling and transport of bulk milk and the criteria for raw milk.</li>\n</ul>\n</section>\n</div>\n<aside class=\"wb-fnote\" role=\"note\"> <h2 id=\"fn\">Footnote</h2> <dl> <dt>Footnote 1</dt> <dd id=\"fn1\"> <p>The National Dairy Code was created in collaboration between the federal, provincial, and territorial governments and provides guidance to governing bodies, owners and employees on the production of safe and suitable dairy products.</p> <p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> </dd> </dl> </aside> \r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>Introduction</h2>\n<p>Bien que le <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\">R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</a> (RSAC) comporte un ensemble d'<a href=\"/fra/1512149634601/1512149659119\">exigences g\u00e9n\u00e9rales</a> qui s'appliquent \u00e0 une vaste gamme d'<a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a80\">aliments</a>, certaines exigences ne s'appliquent qu'\u00e0 certains aliments. Ce document donne un aper\u00e7u des exigences r\u00e9glementaires propres aux <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a68\">produits laitiers</a> qui figurent \u00e0 la partie\u00a06, section\u00a02 du <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr>.</p>\n<h2>Justification</h2> \n<p>Le lait cru et la cr\u00e8me crue servent d'ingr\u00e9dients dans la production d'une vari\u00e9t\u00e9 de <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a68\">produits laitiers</a>. Comme c'est le cas pour d'autres ingr\u00e9dients provenant de l'ext\u00e9rieur, le lait cru et la cr\u00e8me crue peuvent pr\u00e9senter des <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a10\">dangers</a> pouvant poser un risque pour la sant\u00e9 humaine. Certaines exigences relatives \u00e0 la salubrit\u00e9 et \u00e0 la qualit\u00e9 du lait cru et de la cr\u00e8me crue utilis\u00e9s dans la pr\u00e9paration de produits laitiers rel\u00e8vent de la province. L'utilisation de lait cru et de cr\u00e8me crue qui satisfont aux exigences provinciales aide \u00e0 assurer la salubrit\u00e9 et la qualit\u00e9 des produits laitiers que vous pr\u00e9parez.</p>\n<h2>Ce que cela signifie pour votre entreprise alimentaire</h2>\n<p>Pour vous aider \u00e0 comprendre cette exigence, des crit\u00e8res pr\u00e9cis et des exemples sont d\u00e9crits ci-dessous. Les exemples ne sont pas exhaustifs, mais ils illustrent le but de l'exigence et donnent des exemples de mesures que vous pourriez prendre pour vous conformer \u00e0 l'exigence. De plus, certains termes cl\u00e9s avec hyperliens renvoient au <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405\">Glossaire du <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr></a>.</p>\n<h2>Article 94\u00a0: Exigences relatives au lait et \u00e0 la cr\u00e8me utilis\u00e9s dans la pr\u00e9paration de produits laitiers</h2>\n<ul>\n<li>Le lait cru et la cr\u00e8me crue que vous utilisez dans la pr\u00e9paration de <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a68\">produits laitiers</a> qui seront <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a48\">exp\u00e9di\u00e9s ou transport\u00e9s d'une province ou d'un territoire \u00e0 un autre</a> ou <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405#a34\">export\u00e9s</a> satisfont aux exigences applicables de la province dans laquelle les produits laitiers ont \u00e9t\u00e9 pr\u00e9par\u00e9s.</li>\n</ul>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section>\n<h3 class=\"mrgn-tp-0\">Exemples\u00a0:</h3> \n<ul>\n<li>Vous communiquez avec l'autorit\u00e9 en mati\u00e8re de salubrit\u00e9 des aliments de votre province pour obtenir de plus amples renseignements sur les exigences provinciales qui s'appliquent au lait cru et \u00e0 la cr\u00e8me crue que vous importez ou achetez au Canada et que vous utilisez dans la pr\u00e9paration de produits laitiers.</li>\n<li>Si aucune exigence provinciale ne s'applique, comme pour tout autre ingr\u00e9dient, vous vous assurez que le lait cru et la cr\u00e8me crue utilis\u00e9s dans la pr\u00e9paration de produits laitiers satisfont aux exigences propres \u00e0 vos activit\u00e9s.</li>\n<li class=\"mrgn-bttm-0\">Vous consultez le <a href=\"https://www.dairyinfo.gc.ca/fra/lois-reglements-codes-et-normes/code-national-sur-les-produits-laitiers-partie-i/?id=1503084167796\">Code national sur les produits laitiers</a><sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>  pour des lignes directrices sur les crit\u00e8res applicables au lait cru. Le Code national sur les produits laitiers contient des d\u00e9tails sur divers sujets tels que les exigences relatives \u00e0 la manipulation et au transport du lait en vrac et les crit\u00e8res relatifs au lait cru.</li>\n</ul>\n</section>\n</div>\n<aside class=\"wb-fnote\" role=\"note\"> <h2 id=\"fn\">Note de bas de page</h2> <dl> <dt>Note de bas de page 1</dt> <dd id=\"fn1\"> <p>Le Code national sur les produits laitiers a \u00e9t\u00e9 cr\u00e9\u00e9 conjointement par les gouvernements f\u00e9d\u00e9ral, provinciaux et territoriaux et offre une orientation aux organismes de gouvernance, aux propri\u00e9taires et aux employ\u00e9s sur la production de produits laitiers salubres et convenables.</p> <p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> </dd> </dl> </aside>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}