{
    "dcr_id": "1389908558632",
    "title": {
        "en": "Advertising requirements for nutrient content claims",
        "fr": "Exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces"
    },
    "html_modified": "2014-01-16 16:42",
    "modified": "4-8-2022",
    "issued": "16-1-2014",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/manuals-manuels/data/lble_industry_claims_nutrient_advertising_1389908558632_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/manuals-manuels/data/lble_industry_claims_nutrient_advertising_1389908558632_fra"
    },
    "parent_ia_id": "1389905991605",
    "ia_id": "1389908615452",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Advertising requirements for nutrient content claims",
        "fr": "Exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces"
    },
    "label": {
        "en": "Advertising requirements for nutrient content claims",
        "fr": "Exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces"
    },
    "templatetype": "content page 1 column",
    "node_id": "1389908615452",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "parent_dcr_id": "1389905941652",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/nutrient-content/advertising-requirements/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Advertising requirements for nutrient content claims",
            "fr": "Exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces"
        },
        "description": {
            "en": "Nutrient content claims that are presented in any form of advertising must meet all applicable conditions listed in the tables in Specific Nutrient Content Claim Requirements.",
            "fr": "Les all\u00e9gations relatives \u00e0 la teneur nutritive qui figurent dans toute forme d'annonces doivent r\u00e9pondre \u00e0 tous les crit\u00e8res applicables \u00e9nonc\u00e9s aux tableaux de la section Exigences particuli\u00e8res concernant les all\u00e9gations relatives \u00e0 la teneur nutritive."
        },
        "keywords": {
            "en": "food, food health, nutrient content claims, advertisements, advertising, conditions, radio, television",
            "fr": "aliments, la sant\u00e9 des aliments, all&eacute;gations relatives &agrave; la teneur nutritive, annonces, annoncer, crit&egrave;res applicables, radio, t&eacute;l&eacute;vision"
        },
        "dcterms.subject": {
            "en": "consumer protection,consumers,food labeling,inspection,labelling",
            "fr": "protection du consommateur,consommateur,\u00e9tiquetage des aliments,inspection,\u00e9tiquetage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-01-16 16:42:40",
            "fr": "2014-01-16 16:42:40"
        },
        "modified": {
            "en": "2022-08-04",
            "fr": "2022-08-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Advertising requirements for nutrient content claims",
        "fr": "Exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/food-labels/labelling/industry/nutrient-content/advertising-requirements/eng/1389908558632/1389908615452?chap=0\">Complete text</a></p>\r\n\r\n<p>Nutrient content claims that are presented in any form of advertising must meet all applicable conditions listed in the tables in <a href=\"/eng/1389907770176/1389907817577\">Specific nutrient content claim requirements</a>.</p>\n<p>Media-specific requirements vary, depending on whether:</p>\n<ul>\n<li>the advertisement is for radio or television</li>\n<li>the advertisement is for other types of media</li>\n<li>the advertisement is placed by the manufacturer; or</li>\n<li>the advertisement is placed by someone other than the manufacturer (that is a third party).</li>\n</ul>\n<p>The decision trees for nutrient content claim advertising requirements below provide a tool to aid in identifying advertising requirements of nutrient content claims.</p>\r\n<h2>On this page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/advertising-requirements/eng/1389908558632/1389908615452?chap=1\">Advertisements placed by the manufacturer versus third party</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/advertising-requirements/eng/1389908558632/1389908615452?chap=2\">General advertising requirements (advertisements other than those for radio or television)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/advertising-requirements/eng/1389908558632/1389908615452?chap=3\">Advertisements for radio or television</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/advertising-requirements/eng/1389908558632/1389908615452?chap=4\">Advertisements making vitamin and mineral nutrient content claims</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/advertising-requirements/eng/1389908558632/1389908615452?chap=5\">Decision trees for nutrient content claim advertising requirements</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=0\">Texte complet</a></p>\r\n\r\n<p>Les all\u00e9gations relatives \u00e0 la teneur nutritive qui figurent dans toute forme d'annonces doivent r\u00e9pondre \u00e0 tous les crit\u00e8res applicables \u00e9nonc\u00e9s aux tableaux de la section <a href=\"/fra/1389907770176/1389907817577\">Exigences particuli\u00e8res concernant les all\u00e9gations relatives \u00e0 la teneur nutritive</a>.</p>\n<p>Les exigences visant des m\u00e9dias particuliers varient en fonction de ce qui suit\u00a0:</p>\n<ul>\n<li>l'annonce est destin\u00e9e \u00e0 la radio ou \u00e0 la t\u00e9l\u00e9vision</li>\n<li>l'annonce est destin\u00e9e \u00e0 d'autres types de m\u00e9dia</li>\n<li>l'annonce est plac\u00e9e par le fabricant</li>\n<li>l'annonce est plac\u00e9e par quelqu'un d'autre que le fabricant (c'est- \u00e0-dire une tierce partie).</li>\n</ul>\n<p>Les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=5#dec-b\">Arbres de d\u00e9cision des exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces</a> aident \u00e0 d\u00e9terminer les exigences s'appliquant aux all\u00e9gations relatives \u00e0 la teneur nutritive dans la publicit\u00e9.</p>\r\n<h2>Sur cette page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=1\">Annonces faites par le fabricant ou une tierce partie</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=2\">Exigences g\u00e9n\u00e9rales concernant les annonces (annonces autres qu'\u00e0 la radio ou \u00e0 la t\u00e9l\u00e9vision)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=3\">Annonces radiophoniques ou t\u00e9l\u00e9vis\u00e9es</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=4\">Annonces contenant des all\u00e9gations relatives \u00e0 la teneur en vitamines et en min\u00e9raux nutritifs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-dans-les-annonces/fra/1389908558632/1389908615452?chap=5\">Arbres de d\u00e9cision des exigences visant les all\u00e9gations relatives \u00e0 la teneur nutritive dans les annonces</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}