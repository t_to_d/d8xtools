{
    "dcr_id": "1378188902717",
    "title": {
        "en": "Guidelines for the acceptable use of \"100% Canadian milk\" claims on dairy products",
        "fr": "Lignes directrice relative \u00e0 l'usage de l'all\u00e9gation \u00ab lait 100\u00a0% canadien \u00bb sur les produits laitiers"
    },
    "html_modified": "2024-02-13 9:49:17 AM",
    "modified": "2022-07-06",
    "issued": "2013-09-03 02:15:06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/dairy_labelling_cdn_milk_1378188902717_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/dairy_labelling_cdn_milk_1378188902717_fra"
    },
    "ia_id": "1378188978345",
    "parent_ia_id": "1624983674393",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling",
        "fr": "\u00c9tiquetage"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1624983674393",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Guidelines for the acceptable use of \"100% Canadian milk\" claims on dairy products",
        "fr": "Lignes directrice relative \u00e0 l'usage de l'all\u00e9gation \u00ab lait 100\u00a0% canadien \u00bb sur les produits laitiers"
    },
    "label": {
        "en": "Guidelines for the acceptable use of \"100% Canadian milk\" claims on dairy products",
        "fr": "Lignes directrice relative \u00e0 l'usage de l'all\u00e9gation \u00ab lait 100\u00a0% canadien \u00bb sur les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1378188978345",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1624983427586",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/dairy/100-canadian-milk/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/produits-laitiers/lait-100-canadien/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Guidelines for the acceptable use of 100% Canadian milk claims on dairy products",
            "fr": "Lignes directrice relative \u00e0 l'usage de l'all\u00e9gation \u00ab lait 100\u00a0% canadien \u00bb sur les produits laitiers"
        },
        "description": {
            "en": "These guidelines help food manufacturers determine if their use of a 100% Canadian Milk claim and acceptable alternatives on labels of dairy product is compliant with Canadian legislative requirements.",
            "fr": "Ces lignes directrices aident les fabricants \u00e0 d\u00e9terminer si l'usage de l'all\u00e9gation lait 100 % canadien et d'autres all\u00e9gations \u00e9quivalentes sur les \u00e9tiquettes de  produits laitiers est conforme aux exigences l\u00e9gislatives."
        },
        "keywords": {
            "en": "dairy, dairy products, establishment, manual, food safety, inspection, design",
            "fr": "laitiers, produits laitiers, \u00e9tablissement, manuel, salubrit\u00e9 des aliments, inspection, concept"
        },
        "dcterms.subject": {
            "en": "dairy products,food,food inspection,food safety,handbooks,inspection",
            "fr": "produit laitier,aliment,inspection des aliments,salubrit\u00e9 des aliments,manuel,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-09-03 02:15:06",
            "fr": "2013-09-03 02:15:06"
        },
        "modified": {
            "en": "2022-07-06",
            "fr": "2022-07-06"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Guidelines for the acceptable use of \"100% Canadian milk\" claims on dairy products",
        "fr": "Lignes directrice relative \u00e0 l'usage de l'all\u00e9gation \u00ab lait 100\u00a0% canadien \u00bb sur les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The purpose of this document is to help food manufacturers to determine if their voluntary use of a \"100% Canadian milk\" claim (or similar) on dairy products:</p>\n<ul>\n<li>is truthful for consumers, and</li>\n<li>meets legislative requirements.</li>\n</ul>\n\n<p>This guideline document sets out criteria for the acceptable use of \"100% Canadian milk\" claims and acceptable alternatives on dairy products. It is the responsibility of all food manufacturers to ensure that their products are labelled and advertised in a manner that does not create a false and misleading impression of the product [199(1)(b), SFCR; 6(1), SFCA; 5(1), FDA].</p>\n\n<h2 id=\"a2\">Background</h2>\n<p>In October 2010, the Canadian Food Inspection Agency (CFIA) did public opinion research to determine consumers' perceptions of the claim \"100% Canadian milk\" when used on dairy products.</p>\n\n<p>The research findings include the following:</p>\n<ul>\n<li>consumers generally perceive the claim \"100% Canadian milk\" to be related to the origin of the ingredient</li>\n<li>consumers' understanding of the word \"milk\" is broader than the strict regulatory definition in the <i>Food and Drug Regulations</i>\n<ul>\n<li>consumer understanding of \"milk\" includes milk, partly skimmed milk, skim milk, cream, buttermilk, skim milk powder, etc.</li>\n<li>the definition of \"milk\" from the <i>Food and Drug Regulations</i>: <br>\n<strong>Milk</strong> or <strong>whole milk</strong> shall be the normal lacteal secretion obtained from the mammary gland of the cow, genus <i>Bos</i>\"</li>\n</ul>\n</li>\n<li>when this claim is used on dairy products, consumers expect the product to contain what they perceive to be \"milk\" (see above)</li>\n<li>when this claim is used, consumers expect that all of the dairy ingredients are Canadian, with no imported dairy ingredients</li>\n</ul>\n\n<p>The results of this research were used, along with stakeholder consultation, to establish criteria for acceptable use of the claim \"100% Canadian milk\" on dairy products.</p>\n\n<h2 id=\"a3\">Scope</h2>\n<p>This guideline applies only to the use of a \"100% Canadian milk\" claim or acceptable alternatives, with or without logos, vignettes etc., when used on dairy products.</p>\n\n<p>Definition of \"dairy product\" from the <i>Safe Food for Canadians Regulations</i> [1, SFCR]:</p>\n\n<p>\"means milk or a food that is derived from milk, alone or combined with another food, and that contains no oil and no fat other than that of milk.\"</p>\n\n<h2>Regulatory authority</h2>\n<p>The following pieces of legislation apply to a \"100% Canadian milk\" claim. The list is not exhaustive and, depending on the situation, there could be other applicable provisions or legislation.</p>\n\n<p>The <i>Food and Drugs Act</i>, subsection 5(1), states that:</p>\n\n<p>\"No person shall label, package, treat, process, sell or advertise any food in a manner that is false, misleading, deceptive or is likely to create an erroneous impression regarding its character, value, quantity, composition, merit, or safety.\"</p>\n\n<p>The <i>Safe Food for Canadians Act</i>, subsection 6(1), states that:</p>\n\n<p>\"It is prohibited for a person to manufacture, prepare, package, label, sell, import or advertise a food commodity in a manner that is false, misleading or deceptive or is likely to create an erroneous impression regarding its character, quality, value, quantity, composition, merit, safety or origin or the method of its manufacture or preparation.\"</p>\n\n<h2 id=\"a4\">Criteria for making an acceptable \"100% Canadian milk\" claim</h2>\n\n<p>The following criteria set out the conditions to make an acceptable \"100% Canadian milk\" claim on a dairy product.</p>\n<h3>The criteria</h3>\n<ol class=\"lst-num\">\n<li>All of the dairy ingredients in the product are derived from Canadian sources.</li>\n<li>At least 1 of the following ingredients is present in the product in liquid, concentrated, dry, frozen or reconstituted form: \n<ul>\n<li>(whole) milk</li>\n<li>partly (partially) skimmed milk</li>\n<li>skim milk</li>\n<li>cream</li>\n<li>condensed milk</li>\n<li>buttermilk</li>\n</ul>\n</li>\n<li>The ingredients listed above are to be listed separately in the ingredient list. \n<ul>\n<li>Under the <i>Food and Drug Regulations</i> section B.01.010(3)(b), these ingredients fall under the collective common name \"milk ingredients\" in item\u00a07 of Table\u00a02 of the <a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/common-names-for-ingredients-and-components/eng/1629832804662/1629832805224\">Common Names for Ingredients and Components</a> document\n<ul>\n<li>\"milk ingredients: any of the  following in liquid, concentrated, dry, frozen or reconstituted form,  namely, butter, buttermilk, butter oil, milk fat, cream, milk, partly  skimmed milk, skim milk and any other component of milk the chemical  composition of which has not been altered and that exists in the food in  the same chemical state in which it is found in milk.\"</li>\n</ul>\n</li>\n<li>As per B.01.010(3)(b), \n<ul>\n<li>since 1 or more of these  ingredients will be listed separately, the common name \"milk  ingredients\" cannot be used, and</li>\n<li>all ingredients that fall under  that common name must be listed separately.</li>\n</ul>\n</li>\n<li>If the product also contains 1 or more ingredients listed under the common name \"modified milk ingredients\" in item 7.1 of Table\u00a02 of the <a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/common-names-for-ingredients-and-components/eng/1629832804662/1629832805224\">Common Names for Ingredients and Components</a> document, these ingredients can be collectively referred to as \"modified milk ingredients\". \n<ul>\n<li>\"modified milk ingredients:  any of the following in liquid, concentrated, dry, frozen or  reconstituted form, namely, calcium-reduced skim milk (obtained by the  ion-exchange process), casein, caseinates, cultured milk products, milk  serum proteins, ultrafiltered milk, whey, whey butter, whey cream and  any other component of milk the chemical state of which has been altered  from that in which it is found in milk.\"</li>\n</ul>\n</li> \n</ul>\n</li> \n</ol>\n\n<h2 id=\"a5\">Alternate acceptable claims</h2>\n<p>The CFIA's public opinion research also indicated 2 other claims that respondents found appropriate to describe a dairy product made with Canadian milk and/or dairy ingredients made from Canadian milk.</p>\n\n<p>Therefore, food manufacturers may also use the following claims on dairy products:</p>\n<ul>\n<li>\"Made with 100% Canadian milk\"</li>\n<li>\"100% Canadian dairy\"</li>\n</ul>\n\n<p>If the \"Made with 100% Canadian milk\" claim is used, all 3 criteria listed above also apply.</p>\n\n<p>If the \"100% Canadian dairy\" claim is used, only criterion 1 listed above needs to be met.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le pr\u00e9sent document destin\u00e9 aux manufacturiers de produits alimentaires vise \u00e0 aider ces derniers \u00e0 d\u00e9terminer si l'utilisation volontaire de l'all\u00e9gation\u00a0 \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb, (ou de toute autre mention semblable) sur des produits laitiers\u00a0:</p>\n<ul>\n<li>est juste et pr\u00e9cise pour les consommateurs, et</li>\n<li>conforme aux exigences l\u00e9gislatives.</li>\n</ul>\n\n<p>La pr\u00e9sente ligne directrice d\u00e9finit les conditions permettant d'apposer l'all\u00e9gation \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb ou d'autres all\u00e9gations \u00e9quivalentes sur les produits laitiers. Il incombe \u00e0 chaque fabricant de produits alimentaires de s'assurer que ses produits ne sont pas \u00e9tiquet\u00e9s ou annonc\u00e9s de mani\u00e8re fausse ou trompeuse [199(1)b), RSAC; 6(1), LSAC; 5(1), RAD].</p>\n\n<h2 id=\"a2\">Contexte</h2>\n\n<p>En octobre 2010, l'Agence canadienne d'inspection des aliments (ACIA) a fait une recherche d'opinion publique afin de d\u00e9terminer la perception des consommateurs concernant l'utilisation de l'all\u00e9gation\u00a0 \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb sur les produits laitiers.</p>\n\n<p>Les renseignements suite \u00e0 ce sondage indiquent\u00a0que :</p>\n<ul>\n<li>la perception des consommateurs par rapport \u00e0 cette all\u00e9gation est li\u00e9e principalement \u00e0 l'origine des ingr\u00e9dients;</li>\n<li>la perception des consommateurs par rapport au mot \u00ab\u00a0lait\u00a0\u00bb est plus universel comparativement \u00e0 la stricte d\u00e9finition du <i>R\u00e8glement sur les aliments et drogues</i>;\n<ul>\n<li>la perception des consommateurs par rapport au mot \u00ab\u00a0lait\u00a0\u00bb comprend le lait entier, le lait partiellement \u00e9cr\u00e9m\u00e9, le lait \u00e9cr\u00e9m\u00e9, la cr\u00e8me, le babeurre, la poudre de lait \u00e9cr\u00e9m\u00e9, etc.;</li>\n<li>la d\u00e9finition dans le <i>R\u00e8glement sur les aliments et drogues</i>: <br> \n<strong>lait</strong> ou <strong>lait entier</strong> doit \u00eatre la s\u00e9cr\u00e9tion lact\u00e9e normale des glandes mammaire de la vache, genre <i>Bos</i>\u00a0\u00bb;</li>\n</ul>\n</li>\n<li>lorsque l'all\u00e9gation \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb figure sur un produit laitier, les consommateurs s'attendent \u00e0 ce que le produit contienne ce que leur perception du \u00ab\u00a0lait\u00a0\u00bb inclue (voir ci-dessus);</li>\n<li>lorsque l'all\u00e9gation \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb figure sur un produit laitier, les consommateurs s'attendent \u00e0 ce que tous les ingr\u00e9dients laitiers soient d'origine canadienne et sans aucun ingr\u00e9dient laitier import\u00e9.</li>\n</ul>\n\n<p>Les r\u00e9sultats de cette recherche, ainsi que les r\u00e9sultats des consultations faites aupr\u00e8s des diff\u00e9rents intervenants, ont servi \u00e0 d\u00e9finir les crit\u00e8res d'acceptabilit\u00e9 de l'all\u00e9gation \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb sur les produits laitiers.</p>\n\n<h2 id=\"a3\">Port\u00e9e</h2>\n<p>La pr\u00e9sente ligne directrice s'applique uniquement \u00e0 l'usage de la mention \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb ou de mentions \u00e9quivalentes sur les produits laitiers, avec ou sans logo, vignette, etc.</p>\n\n<p>La d\u00e9finition d'un produit laitier du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> [1, RSAC]:</p>\n\n<p>d\u00e9signe le \u00ab\u00a0lait ou aliment qui provient du lait, seul ou combin\u00e9 avec un autre aliment, et qui ne contient ni huile ni mati\u00e8re grasse autre que celle du lait.\u00a0\u00bb</p>\n\n<h2>Dispositions juridiques applicables</h2>\n\n<p>Les dispositions juridiques ci-dessous s'appliquent \u00e0 la mention \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb. D'autres dispositions ou l\u00e9gislations pourraient s'appliquer \u00e9galement, selon les circonstances.</p>\n\n<p>Le paragraphe 5(1) de la <i>Loi sur les aliments et drogues</i> pr\u00e9cise ce qui suit :</p>\n\n<p>\u00ab\u00a0Il est interdit d'\u00e9tiqueter, d'emballer, de traiter, de pr\u00e9parer ou de vendre un aliment ou d'en faire la publicit\u00e9 de mani\u00e8re fausse, trompeuse ou mensong\u00e8re ou susceptible de cr\u00e9er une fausse impression quant \u00e0 sa nature, sa valeur, sa quantit\u00e9, sa composition, ses avantages ou sa s\u00fbret\u00e9.\u00a0\u00bb</p>\n\n<p>Le paragraphe 6(1) de la <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> pr\u00e9cise le suivant\u00a0:</p>\n\n<p>\u00ab\u00a0Il est interdit \u00e0 toute personne de fabriquer, de conditionner, d'emballer, d'\u00e9tiqueter, de vendre ou d'importer un produit alimentaire, ou d'en faire la publicit\u00e9, d'une mani\u00e8re fausse, trompeuse ou mensong\u00e8re ou susceptible de cr\u00e9er une fausse impression quant \u00e0 sa nature, sa qualit\u00e9, sa value, sa quantit\u00e9, sa composition, ses avantages, sa salubrit\u00e9, son origine ou son mode de fabrication ou de conditionnement.\u00a0\u00bb</p>\n\n<h2 id=\"a4\">Crit\u00e8res pour l'utilisation acceptable d'une all\u00e9gation \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb</h2>\n<p>Les conditions autorisant l'usage de la mention \u00ab\u00a0lait 100\u00a0% canadien\u00a0\u00bb sur les produits laitiers sont les suivantes.</p>\n\n<p><strong>Les crit\u00e8res</strong></p>\n<ol class=\"lst-num\">\n<li>Tous les ingr\u00e9dients laitiers entrant dans la fabrication du produit sont d'origine canadienne.\u00a0</li>\n<li>Le produit contient au moins 1 des ingr\u00e9dients suivants sous forme liquide, concentr\u00e9e, s\u00e8che, congel\u00e9e ou reconstitu\u00e9e\u00a0: \n<ul>\n<li>lait (entier)</li>\n<li>lait partiellement \u00e9cr\u00e9m\u00e9</li>\n<li>lait \u00e9cr\u00e9m\u00e9</li>\n<li>cr\u00e8me</li>\n<li>lait condens\u00e9</li>\n<li>babeurre</li>\n</ul>\n</li>\n<li>Les ingr\u00e9dients \u00e9num\u00e9r\u00e9s ci-dessus doivent \u00eatre mentionn\u00e9s s\u00e9par\u00e9ment dans la liste des ingr\u00e9dients. \n<ul>\n<li>Selon l'alin\u00e9a B.01.010(3)b) du <i>R\u00e8glement sur les aliments et drogues</i>, ces ingr\u00e9dients sont d\u00e9sign\u00e9s collectivement par le nom usuel \u00ab\u00a0substances laiti\u00e8res\u00a0\u00bb \u00e0 l'article\u00a07 du tableau\u00a02 du document <a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/noms-usuels-d-ingredients-et-de-constituants/fra/1629832804662/1629832805224\">Noms usuels d'ingr\u00e9dients et de constituants</a>\n<ul>\n<li>\u00ab\u00a0substances laiti\u00e8res\u00a0\u00bb d\u00e9signe  toute forme liquide, concentr\u00e9e, s\u00e9ch\u00e9e, congel\u00e9e ou reconstitu\u00e9e des  produits suivants\u00a0: beurre, babeurre, huile de beurre, mati\u00e8re  grasse de lait, cr\u00e8me, lait, lait partiellement \u00e9cr\u00e9m\u00e9, lait \u00e9cr\u00e9m\u00e9 et  tout autre constituant du lait dont la composition chimique n'a pas \u00e9t\u00e9  modifi\u00e9e et dont l'\u00e9tat chimique est celui dans lequel il se trouve dans  le lait.\u00a0\u00bb;</li>\n</ul>\n</li>\n<li>Conform\u00e9ment \u00e0 l'alin\u00e9a B.01.010(3)b), \n<ul>\n<li>\u00e9tant donn\u00e9 qu'un ou plusieurs de ces  ingr\u00e9dients doivent \u00eatre indiqu\u00e9s s\u00e9par\u00e9ment dans la liste des  ingr\u00e9dients, le nom usuel \u00ab\u00a0substances laiti\u00e8res\u00a0\u00bb ne peut pas \u00eatre utilis\u00e9 et,</li>\n<li>tous les ingr\u00e9dients d\u00e9sign\u00e9s par ce nom  usuel doivent \u00eatre mentionn\u00e9s s\u00e9par\u00e9ment.</li>\n</ul>\n</li>\n<li>Si le produit contient \u00e9galement 1 ou plusieurs ingr\u00e9dients d\u00e9sign\u00e9s par le nom usuel \u00ab\u00a0substances laiti\u00e8res modifi\u00e9es\u00a0\u00bb \u00e0 l'article \u00a07.1 du tableau\u00a02 du document <a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/noms-usuels-d-ingredients-et-de-constituants/fra/1629832804662/1629832805224\">Noms usuels d'ingr\u00e9dients et de constituants</a>, ces ingr\u00e9dients peuvent \u00eatre mentionn\u00e9s collectivement par ce nom usuel dans la liste des ingr\u00e9dients. \n<ul>\n<li>\u00ab\u00a0substances laiti\u00e8res modifi\u00e9es\u00a0\u00bb  d\u00e9signe toute forme liquide, concentr\u00e9e, s\u00e9ch\u00e9e, congel\u00e9e ou  reconstitu\u00e9e des produits suivants\u00a0: lait \u00e9cr\u00e9m\u00e9 \u00e0 teneur r\u00e9duite  en calcium (obtenu par proc\u00e9d\u00e9 d'\u00e9change d'ions), cas\u00e9ine, cas\u00e9inates,  produits du lait de culture, prot\u00e9ines lactos\u00e9riques, lait ultrafiltr\u00e9,  lactos\u00e9rum (petit-lait), beurre de lactos\u00e9rum (petit-lait), cr\u00e8me de  lactos\u00e9rum (petit-lait) et tout autre constituant du lait dont l'\u00e9tat  chimique a \u00e9t\u00e9 modifi\u00e9 de fa\u00e7on \u00e0 diff\u00e9rer de celui dans lequel il se  trouve dans le lait.\u00a0\u00bb</li>\n</ul>\n</li> \n</ul>\n</li> \n</ol>\n\n<h2 id=\"a5\">Autres mentions acceptables</h2>\n<p>Le sondage d'opinion publique men\u00e9 par l'ACIA a indiqu\u00e9 2 autres mentions que les personnes sond\u00e9es trouvaient acceptable pour d\u00e9crire un produit laitier fabriqu\u00e9 avec du lait canadien ou des ingr\u00e9dients laitiers fabriqu\u00e9s \u00e0 partir de lait canadien.\u00a0</p>\n\n<p>Par cons\u00e9quent, les manufacturiers pourraient employer les mentions suivantes sur les produits laitiers:</p>\n<ul>\n<li>\u00ab\u00a0fabriqu\u00e9 \u00e0 partir de lait 100\u00a0% canadien\u00a0\u00bb;</li>\n<li>\u00ab\u00a0produit laitier 100\u00a0% canadien\u00a0\u00bb.</li>\n</ul>\n<p>Si la mention \u00ab\u00a0fabriqu\u00e9 \u00e0 partir de lait 100\u00a0% canadien\u00a0\u00bb est utilis\u00e9e, les 3 crit\u00e8res cit\u00e9s ci-dessus s'appliqueraient.</p>\n\n<p>Dans le cas de la mention \u00ab\u00a0produit laitier 100\u00a0% canadien\u00a0\u00bb, seul le crit\u00e8re\u00a01 s'appliquerait.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}