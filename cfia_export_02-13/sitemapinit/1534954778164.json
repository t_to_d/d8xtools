{
    "dcr_id": "1534954777758",
    "title": {
        "en": "Preventive controls for food: Dairy products",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les <span class=\"nowrap\">aliments :</span> Produits laitiers"
    },
    "html_modified": "2024-02-13 9:53:14 AM",
    "modified": "2023-04-28",
    "issued": "2019-01-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_preventive_control_business_dairy_1534954777758_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_preventive_control_business_dairy_1534954777758_fra"
    },
    "ia_id": "1534954778164",
    "parent_ia_id": "1526472290070",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1526472290070",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Preventive controls for food: Dairy products",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les <span class=\"nowrap\">aliments :</span> Produits laitiers"
    },
    "label": {
        "en": "Preventive controls for food: Dairy products",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les <span class=\"nowrap\">aliments :</span> Produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1534954778164",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526472289805",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/dairy-products/",
        "fr": "/controles-preventifs/produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Preventive controls for food: Dairy products",
            "fr": "Contr\u00f4les pr\u00e9ventifs pour les aliments : Produits laitiers"
        },
        "description": {
            "en": "These documents provide information on select preventive control practices for operators to mitigate food safety risks associated with the processing of dairy products.",
            "fr": "Ces documents fournissent des renseignements sur certaines pratiques de contr\u00f4le pr\u00e9ventif \u00e0 l'intention des exploitants pour att\u00e9nuer les risques en mati\u00e8re de salubrit\u00e9 des aliments associ\u00e9s \u00e0 la transformation des produits laitiers."
        },
        "keywords": {
            "en": "food, food health, preventive control, business, SFCR, Safe Food for Canadians Regulations, food businesses, import, export, interprovincial, dairy products, dairy",
            "fr": "aliments, salubrit\u00e9 des aliments, contr\u00f4le pr\u00e9ventif, entreprises, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, entreprises alimentaires, importation, exportation, interprovinciale, produits laitiers, laitiers"
        },
        "dcterms.subject": {
            "en": "access to information,food,food safety,government publication",
            "fr": "acc\u00e8s \u00e0 l'information,aliment,salubrit\u00e9 des aliments,publication gouvernementale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-15",
            "fr": "2019-01-15"
        },
        "modified": {
            "en": "2023-04-28",
            "fr": "2023-04-28"
        },
        "type": {
            "en": "administrative page,reference material",
            "fr": "page administrative,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Preventive controls for food: Dairy products",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les aliments : Produits laitiers"
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n<section class=\"alert alert-info\">\n\n<h2>Information</h2>\n\n<p>Before you access information related to preventive controls for dairy, make sure you review the information on\u00a0<a href=\"/eng/1512149634601/1512149659119\">General food requirements and guidance</a>. The <i>Safe Food for Canadians Regulations</i> (SFCR) set out certain requirements, such as licensing, preventive controls and traceability that apply to most food commodities.</p>\n\n<p>Additional information can also be found at\u00a0<a href=\"/food-guidance-by-commodity/dairy/eng/1526653268334/1526653268554\">Food-specific requirements and guidance: Dairy products</a>.</p>\n\n</section>\n\n<p>These documents provide information on select preventive control practices for operators to mitigate food safety risks associated with the processing of dairy products. This information is intended to inform and support the development and implementation of a preventive control plan.</p>\n\n<h2>Good manufacturing practices</h2>\n\n<ul>\n<li><a href=\"/preventive-controls/dairy-products/dairy-processors/eng/1538762915543/1538763315637\">Dairy processors</a></li>\n<li><a href=\"/preventive-controls/dairy-products/on-farm-dairy-processors/eng/1539279633098/1539279767214\">On-farm dairy processors</a></li>\n</ul>\n\n<h2>Dairy processing</h2>\n\n<ul>\n<li><a href=\"/preventive-controls/dairy-products/aseptic-processing-and-packaging/eng/1538534768909/1538534836452\">Aseptic processing and packaging systems</a></li>\n<li><a href=\"/preventive-controls/dairy-products/batch-pasteurization/eng/1539361429523/1539361508211\">Batch pasteurization systems</a></li>\n<li><a href=\"/preventive-controls/dairy-products/critical-process-test-procedures/eng/1544201289884/1544201479005\">Critical process test procedures</a></li>\n<li><a href=\"/preventive-controls/dairy-products/evaluation-of-dairy-processing-equipment/eng/1536772401983/1536772793220\">Evaluation of dairy processing equipment</a></li>\n<li><a href=\"/preventive-controls/dairy-products/dairy-processors/formulation-and-processing-controls/eng/1579544564963/1579545138658\">Formulation and processing controls for processed cheeses, sweetened condensed milk and aerosol-packaged products</a></li>\n<li><a href=\"/preventive-controls/dairy-products/dairy-processors/heat-exchangers/eng/1579546813867/1579546894609\">Heat exchangers</a></li>\n<li><a href=\"/preventive-controls/dairy-products/htst/eng/1539614754038/1539614826437\">High temperature short time (HTST) pasteurization systems</a></li>\n<li><a href=\"/preventive-controls/dairy-products/hhst-esl/eng/1539632249860/1539711207517\">Higher heat shorter time (HHST) pasteurization systems and Extended shelf life (ESL)</a></li>\n<li><a href=\"/preventive-controls/dairy-products/non-thermal-processing/eng/1536184376601/1536184376944\">Non-thermal processing</a></li>\n<li><a href=\"/preventive-controls/controls-for-food/hermetically-sealed-containers/eng/1576690074213/1576690678546\">Retort systems</a></li>\n<li><a href=\"/preventive-controls/dairy-products/vitamin-addition/eng/1536347666747/1536350060256\">Vitamin addition </a></li>\n</ul>\n\n<h2>Appendices</h2>\n\n<ul>\n<li><a href=\"/preventive-controls/dairy-products/appendix-a/eng/1543943599647/1543943788660\">Appendix A: Compressed air\u00a0- milk and milk product contact surfaces</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-b/eng/1538708170723/1538708171075\">Appendix B: Constant level tank design</a></li>\n<li><a href=\"/preventive-controls/dairy-products/mbts/eng/1543261712125/1543261789019\">Appendix C: Meter based timing system</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-d/eng/1539868653530/1539869407494\">Appendix D: Criteria for the evaluation of computerized food safety controls</a></li>\n<li><a href=\"/preventive-controls/dairy-products/diagram-of-valves/eng/1541433344954/1541433390805\">Appendix E: Diagram of valves (close coupled, leak protector, plug, air space heating)</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-f/eng/1543384250985/1543384251251\">Appendix F: Processed cheese microbiological stability chart for non-refrigerated products (applies only to full fat cheese)</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-g/eng/1539203456346/1539203456559\">Appendix G: Preventing cross connections</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-h/eng/1541783575315/1541784144769\">Appendix H: Use of wood in dairy facilities</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-i/eng/1542906459851/1542906577287\">Appendix I: Water reclaimed from condensing of milk and milk products</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-j/eng/1539196679185/1539196679465\">Appendix J: Design criteria for digital thermometers for use in critical processes</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-k/eng/1537271958735/1537271958969\">Appendix K: Fo value</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-l/eng/1536693121640/1536693121889\">Appendix L: Examples of steam injectors</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-m/eng/1536581864469/1536581864719\">Appendix M: Pressure switch settings</a></li>\n<li><a href=\"/preventive-controls/dairy-products/appendix-n/eng/1537279328222/1537279328471\">Appendix N: Vacuum breaker</a></li>\n</ul>\n\n<h2>Related information</h2>\n\n<p><a href=\"/preventive-controls/eng/1526472289805/1526472290070\">Preventive controls for food businesses</a><br>\n</p>\n\n<p><a href=\"/preventive-controls/preventive-control-plans/eng/1512152894577/1512152952810\">Preventive control plan</a><br>\n</p>\n\n\n<p><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/legislation-guidelines/guidance-documents/voluntary-guidance-improving-safety-soft-semi-soft-cheese-made-unpasteurized-milk-2015.html\">Voluntary guidance on improving the safety of soft and semi-soft cheese made from unpasteurized milk</a><br>\n</p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n<section class=\"alert alert-info\">\n\n<h2>Information</h2>\n\n<p>Avant de consulter les renseignements li\u00e9s aux contr\u00f4les pr\u00e9ventifs pour les produits laitiers, assurez-vous de passer en revue le document <a href=\"/fra/1512149634601/1512149659119\">Exigences et directives g\u00e9n\u00e9rales sur les aliments</a>. Le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC) \u00e9nonce certaines exigences, dont celles sur la d\u00e9livrance de licences, les contr\u00f4les pr\u00e9ventifs et la tra\u00e7abilit\u00e9, qui s'appliquent \u00e0 la plupart des produits alimentaires. </p>\n\n<p>Pour obtenir des renseignements suppl\u00e9mentaires, consultez le document <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-laitiers/fra/1526653268334/1526653268554\">Exigences et documents d'orientation relative \u00e0 certains aliments\u00a0: Produits laitiers</a>.</p>\n\n</section>\n\n<p>Ces documents fournissent des renseignements sur certaines pratiques de contr\u00f4le pr\u00e9ventif \u00e0 l'intention des exploitants pour att\u00e9nuer les risques en mati\u00e8re de salubrit\u00e9 des aliments associ\u00e9s \u00e0 la transformation des produits laitiers. Ces renseignements ont pour but d'orienter et de faciliter l'\u00e9laboration et la mise en \u0153uvre d'un plan de contr\u00f4le pr\u00e9ventif. </p>\n\n<h2>Bonnes pratiques de fabrication</h2>\n\n<ul>\n<li><a href=\"/controles-preventifs/produits-laitiers/transformateurs-laitiers/fra/1538762915543/1538763315637\">Transformateurs laitiers</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/transformateurs-laitiers-a-la-ferme/fra/1539279633098/1539279767214\">Transformateurs laitiers \u00e0 la ferme</a></li>\n</ul>\n\n<h2>Transformation des produits laitiers</h2>\n\n<ul>\n<li><a href=\"/controles-preventifs/produits-laitiers/conditionnement-et-emballage-aseptiques/fra/1538534768909/1538534836452\">Syst\u00e8mes de conditionnement et d'emballage aseptiques</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/pasteurisation-en-discontinu/fra/1539361429523/1539361508211\">Syst\u00e8mes de pasteurisation en discontinu</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/methodes-d-essai-des-procedes-critiques/fra/1544201289884/1544201479005\">M\u00e9thodes d'essai des proc\u00e9d\u00e9s critiques</a></li>\n\n<li><a href=\"/controles-preventifs/produits-laitiers/evaluation-de-l-equipement-de-transformation-des-p/fra/1536772401983/1536772793220\">\u00c9valuation de l'\u00e9quipement de transformation des produits laitiers</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/transformateurs-laitiers/controles-de-formulation-et-de-transformation/fra/1579544564963/1579545138658\">Contr\u00f4les de formulation et de transformation des fromages fondus, du lait concentr\u00e9 sucr\u00e9 et des produits pr\u00e9sent\u00e9s en a\u00e9rosol</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/transformateurs-laitiers/echangeur-de-chaleur/fra/1579546813867/1579546894609\">\u00c9changeur de chaleur</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/htst/fra/1539614754038/1539614826437\">Syst\u00e8mes de pasteurisation \u00e0 haute temp\u00e9rature et \u00e0 action rapide (HTST)</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/hhst-esl/fra/1539632249860/1539711207517\">Syst\u00e8mes de pasteurisation \u00e0 tr\u00e8s haute temp\u00e9rature tr\u00e8s courte dur\u00e9e (HHST) et produits laitiers \u00e0 dur\u00e9e de conservation prolong\u00e9e (ESL)</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/transformation-non-thermique/fra/1536184376601/1536184376944\">Transformation non-thermique</a></li>\n<li><a href=\"/controles-preventifs/controles-pour-les-aliments/recipients-hermetiquement-scelles/fra/1576690074213/1576690678546\">Syst\u00e8mes de st\u00e9rilisation par autoclave</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/ajout-de-vitamines/fra/1536347666747/1536350060256\">Ajout de vitamines</a></li>\n</ul>\n\n<h2>Annexes</h2>\n\n<ul>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-a/fra/1543943599647/1543943788660\">Annexe A\u00a0: Air comprim\u00e9\u00a0- surfaces de contact avec les produits laitiers</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-b/fra/1538708170723/1538708171075\">Annexe B\u00a0: Conception de r\u00e9servoirs \u00e0 niveau constant</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/smad/fra/1543261712125/1543261789019\">Annexe C\u00a0: Syst\u00e8me de minuterie asservi \u00e0 un d\u00e9bitm\u00e8tre</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-d/fra/1539868653530/1539869407494\">Annexe D\u00a0: Crit\u00e8res d'\u00e9valuation des contr\u00f4les informatis\u00e9s de salubrit\u00e9 des aliments</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/diagrammes-des-vannes/fra/1541433344954/1541433390805\">Annexe E\u00a0: Diagrammes des vannes (vannes de sortie raccord\u00e9es, antifuite, robinet, chauffage de l'espace d'air)</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-f/fra/1543384250985/1543384251251\">Annexe F\u00a0: Diagramme de la stabilit\u00e9 microbiologique des fromages fondu \u00e0 tartiner pour les produits non r\u00e9frig\u00e9r\u00e9s</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-g/fra/1539203456346/1539203456559\">Annexe G\u00a0: Pr\u00e9vention des raccordements crois\u00e9s</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-h/fra/1541783575315/1541784144769\">Annexe H\u00a0: Utilisation du bois dans les \u00e9tablissements laitiers</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-i/fra/1542906459851/1542906577287\">Annexe I\u00a0: Eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d'autres produits du lait</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-j/fra/1539196679185/1539196679465\">Annexe J\u00a0: Crit\u00e8res de calcul visant les thermom\u00e8tres num\u00e9riques utilis\u00e9s dans les proc\u00e9d\u00e9s critiques</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-k/fra/1537271958735/1537271958969\">Annexe K\u00a0: Valeur Fo</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-l/fra/1536693121640/1536693121889\">Annexe L\u00a0: Exemples d'injecteurs de vapeur</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-m/fra/1536581864469/1536581864719\">Annexe M\u00a0: R\u00e9glages de pressostat</a></li>\n<li><a href=\"/controles-preventifs/produits-laitiers/annexe-n/fra/1537279328222/1537279328471\">Annexe N\u00a0: Casse-vide de type nettoyage en place (NEP)</a></li>\n</ul>\n\n<h2>Renseignements connexes</h2>\n\n<p><a href=\"/controles-preventifs/fra/1526472289805/1526472290070\">Contr\u00f4les pr\u00e9ventifs pour les entreprises alimentaires</a><br>\n</p>\n\n<p><a href=\"/controles-preventifs/plans-de-controle-preventif/fra/1512152894577/1512152952810\">Plan de contr\u00f4le pr\u00e9ventif</a><br>\n</p>\n\n\n<p><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/legislation-lignes-directrices/document-reference/document-orientation-amelioration-salubrite-fromages-pate-molle-pate-demi-ferme-faits-lait-non-pasteurise-2015.html\">Document d'orientation sur l'am\u00e9lioration de la salubrit\u00e9 des fromages \u00e0 p\u00e2te molle et \u00e0 p\u00e2te demi-ferme faits de lait non pasteuris\u00e9</a><br>\n</p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}