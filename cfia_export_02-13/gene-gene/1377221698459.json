{
    "dcr_id": "1377221698459",
    "title": {
        "en": "Official Seed Tags",
        "fr": "\u00c9tiquettes officielles de semence"
    },
    "html_modified": "2024-02-13 9:49:15 AM",
    "modified": "2013-08-22",
    "issued": "2013-08-22 21:34:59",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/seed_proc_tags_1377221698459_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/seed_proc_tags_1377221698459_fra"
    },
    "ia_id": "1377221699474",
    "parent_ia_id": "1299175155902",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling and Composition",
        "fr": "\u00c9tiquetage et composition"
    },
    "commodity": {
        "en": "Seeds",
        "fr": "Semences"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299175155902",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Official Seed Tags",
        "fr": "\u00c9tiquettes officielles de semence"
    },
    "label": {
        "en": "Official Seed Tags",
        "fr": "\u00c9tiquettes officielles de semence"
    },
    "templatetype": "content page 1 column",
    "node_id": "1377221699474",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299175084055",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-inspection-procedures/seed-tags/",
        "fr": "/protection-des-vegetaux/semences/methodes-d-inspection/etiquettes-de-semence/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Official Seed Tags",
            "fr": "\u00c9tiquettes officielles de semence"
        },
        "description": {
            "en": "An Official Tag is a label that is either issued or approved by the CFIA. An Official Tag is approved when the CFIA receives, reviews and approves an application submitted by a Registered Seed Establishment (RSE) for the printing of Official Tags.",
            "fr": "Les \u00e9tiquettes officielles sont soit \u00e9mises, soit approuv\u00e9es par l'ACIA. On dit d'une \u00e9tiquette officielle qu'elle est approuv\u00e9e quand l'ACIA re\u00e7oit, \u00e9tudie et approuve la demande soumise par un \u00e9tablissement semencier agr\u00e9\u00e9 (\u00c9SA) pour faire imprimer des \u00e9tiquettes officielles."
        },
        "keywords": {
            "en": "Seeds Act, Seeds Regulations, seeds, crops, imports, Official Seed Tags, seed tags, interagency certified tag",
            "fr": "Loi sur les semences, R\u00e8glement sur les semences, semences, importation, \u00e9tiquettes officielles de semence, \u00e9tiquettes de semence, \u00e9tiquette de certification interagences"
        },
        "dcterms.subject": {
            "en": "quality control,crops,imports,inspection,handbooks,plants",
            "fr": "contr\u00f4le de la qualit\u00e9,cultures,importation,inspection,manuel,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-08-22 21:34:59",
            "fr": "2013-08-22 21:34:59"
        },
        "modified": {
            "en": "2013-08-22",
            "fr": "2013-08-22"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Official Seed Tags",
        "fr": "\u00c9tiquettes officielles de semence"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=43#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>An Official Tag is a label that is either \"issued\" or \"approved\" by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. An Official Tag is \"approved\" when the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> receives, reviews and approves an application submitted by a Registered Seed Establishment (RSE) for the printing of Official Tags. The <abbr title=\"Registered Seed Establishment\">RSE</abbr> must complete and submit the application entitled \"<i>Request Form for the Grant of a Licence to Print Official Seed Tags and Resulting Licence Agreement Terms and Conditions</i>\" to seek approval from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for printing Official Tags. The <abbr title=\"Registered Seed Establishment\">RSE</abbr> must receive a written approval from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> prior to printing Official Tags. If <abbr title=\"Registered Seed Establishment\">RSE</abbr> does not meet the application requirement, <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> may reject an application made by the <abbr title=\"Registered Seed Establishment\">RSE</abbr> for the printing of Official Tags.</p>\n<h2><i>Request Form for the Grant of a License to Print Official Seed Tag and Resulting Licence Agreement Terms and Conditions</i></h2>\n<p>Registered Seed Establishments (RSEs) registered under Part <abbr title=\"4\">IV</abbr> (Registration of Establishments that Prepare Seed and the Licensing of Operators) of the <i>Seeds Regulations</i> can apply for and receive approval from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to print \"Official Seed Tags\".</p>\n<p>Approval to print \"Official Seed Tags\" is only granted to Registered Seed Establishments. Establishments not registered under Part <abbr title=\"4\">IV</abbr> of the <i>Seeds Regulations</i> will not receive approval from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to print \"Official Seed Tags\".</p>\n<p>The tags or facsimile of the tags can be printed on bags. The material on which the \"Official Seed Tags\" are printed must be durable and tear resistant.</p>\n<p>The <i>Request Form for the Grant of a Licence to Print Official Seed Tag and Resulting Licence Agreement Terms and Conditions</i> can be obtained by contacting <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">your nearest <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office</a>. Official Seed Tags\" can also be obtained (if available) from <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> District/Regional offices.</p>\n<p>Businesses operating as printers will not receive approval from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to print \"Official Seed Tags\". Advertising for the printing of \"Official Seed Tags\" in any form or media is not permitted.</p>\n<h2>Canadian Interagency Certified Tag</h2>\n<p>\"Interagency certification\" means two or more official certifying agencies participating in performing the services required to certify a seed lot. An \"interagency certification tag\" according to the <i>Seeds Regulations</i> is an official tag that indicates seed that is certified by an official certifying agency and is graded with a Canada pedigreed grade name.</p>\n<ul>\n<li><a href=\"http://www.aosca.org/\">Association of Official Seed Certifying Agencies (AOSCA)</a></li>\n</ul>\n<p>Please note that:</p>\n<ul>\n<li>All imported seed labelled with a Canada Certified grade name requires the blue interagency certified tag (CFIA/ACIA 5627). The blue interagency certified tag is to be used only for a single crop kind.</li>\n<li>Foundation and Registered class of seed will continue to use the white interagency tag (CFIA/ACIA 0034)</li>\n<li>The green coloured certified mixture tag (CFIA/ACIA 0038) should be used for Canada Certified seed mixture (a mixture consists of two or more crop kinds or species).</li>\n<li>The orange unregistered variety tag (CFIA/ACIA 0039) should be applied to imported seed of an unregistered variety that is graded with a Canada pedigreed grade name.</li>\n</ul>\n<p>Imported Certified seed will continue to be exempt from the requirement to use an interagency tag if the seed package:</p>\n<ul>\n<li>bears a label issued or approved by an official certifying agency,</li>\n<li>is of Certified class, and</li>\n<li>the Canada Certified grade name and accredited grader number appear on the package.</li>\n</ul>\n<p>Registred seed establishments can print the blue tag once the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> receives a written request for printing additional tags and an amendment to the licence agreement has been issued.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=43#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les \u00e9tiquettes officielles sont soit \u00e9mises, soit approuv\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. On dit d'une \u00e9tiquette officielle qu'elle est approuv\u00e9e quand l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> re\u00e7oit, \u00e9tudie et approuve la demande soumise par un \u00e9tablissement semencier agr\u00e9\u00e9 (\u00c9SA) pour faire imprimer des \u00e9tiquettes officielles. L'<abbr title=\"\u00e9tablissement semencier agr\u00e9\u00e9\">\u00c9SA</abbr> doit remplir et remettre le formulaire intitul\u00e9 <i>Demande de permis d'impression d'\u00e9tiquettes officielles de semence et modalit\u00e9s d'utilisation du permis</i> pour obtenir l'approbation de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> afin de faire imprimer des \u00e9tiquettes officielles. L'<abbr title=\"\u00e9tablissement semencier agr\u00e9\u00e9\">\u00c9SA</abbr> doit attendre de recevoir l'approbation \u00e9crite de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> avant de faire imprimer des \u00e9tiquettes officielles. Si l'<abbr title=\"\u00e9tablissement semencier agr\u00e9\u00e9\">\u00c9SA</abbr> ne satisfait pas aux exigences, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut rejeter sa demande.</p>\n<h2>Le <i>Formulaire de demande de permis d'impression d'\u00e9tiquettes officielles de semence et Modalit\u00e9s d'utilisation du permis</i></h2>\n<p>Les \u00e9tablissements semenciers agr\u00e9\u00e9s (ESA) inscrits conform\u00e9ment \u00e0 la Partie <abbr title=\"4\">IV</abbr> (Agr\u00e9ment des \u00e9tablissements qui conditionnent les semences et agr\u00e9ment des exploitants) du <i>R\u00e8glement sur les semences</i> peuvent demander \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> l'autorisation d'imprimer des \u00ab\u00a0\u00e9tiquettes officielles de semence\u00a0\u00bb.</p>\n<p>L'autorisation d'imprimer des \u00ab\u00a0\u00e9tiquettes officielles de semence\u00a0\u00bb est uniquement accord\u00e9e \u00e0 des \u00e9tablissements semenciers agr\u00e9\u00e9s. Les demandes \u00e0 cette fin provenant d'\u00e9tablissements non inscrits conform\u00e9ment \u00e0 la Partie <abbr title=\"4\">IV</abbr> du <i>R\u00e8glement sur les semences</i> ne seront pas approuv\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Les \u00e9tiquettes ou fac-simil\u00e9s des \u00e9tiquettes peuvent \u00eatre imprim\u00e9s sur les sacs. Le mat\u00e9riau sur lequel les \u00ab\u00a0\u00e9tiquettes officielles de semence\u00a0\u00bb sont imprim\u00e9es doit \u00eatre solide et r\u00e9sistant aux d\u00e9chirures.</p>\n<p>Le <i>Formulaire de demande de permis d'impression d'\u00e9tiquettes officielles de semence et Modalit\u00e9s d'utilisation du permis</i> peuvent \u00eatre obtenues en communiquant avec <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> le plus proche.</a> Il est aussi possible d'obtenir des \u00ab\u00a0\u00e9tiquettes officielles de semence\u00a0\u00bb aux bureaux r\u00e9gionaux ou de district de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, lorsqu'elles sont disponibles.</p>\n<p>Les commerces d'imprimeurs ne recevront pas non plus l'approbation de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pour imprimer ces \u00e9tiquettes. Il n'est pas permis d'annoncer qu'on imprime des \u00ab\u00a0\u00e9tiquettes officielles de semence\u00a0\u00bb, peu importe la forme de l'annonce ou le m\u00e9dia.</p>\n<h2>\u00c9tiquette de certification interagences pour les semences</h2>\n<p>Une \u00ab\u00a0certification interagences\u00a0\u00bb signifie qu\u2019au moins deux agences de certification participent aux activit\u00e9s visant \u00e0 certifier un lot de semences. En vertu du <i>R\u00e8glement sur les semences</i>, une \u00ab\u00a0\u00e9tiquette de certification interagences\u00a0\u00bb s\u2019entend d\u2019une \u00e9tiquette officielle qui indique qu\u2019une semence est certifi\u00e9e par une agence officielle de certification et est class\u00e9e sous une d\u00e9nomination de la cat\u00e9gorie Canada g\u00e9n\u00e9alogique.</p>\n<ul>\n<li><a href=\"http://www.aosca.org/\">Association of Official Seed Certifying Agencies (AOSCA) (en anglais seulement)</a></li>\n</ul>\n<p>Veuillez noter :</p>\n<ul>\n<li>Toutes les semences import\u00e9es qui sont class\u00e9es sous une d\u00e9nomination de cat\u00e9gorie Canada certifi\u00e9e devront porter l'\u00e9tiquette interagences bleue (CFIA/ACIA 5627). L\u2019\u00e9tiquette interagences bleue ne doit \u00eatre utilis\u00e9e que pour les semences provenant d\u2019une seule sorte de culture.</li>\n<li>On pourra continuer d\u2019utiliser les \u00e9tiquettes interagences blanches pour les semences des cat\u00e9gories fondation et enregistr\u00e9e (CFIA/ACIA 0034).</li>\n<li>L\u2019\u00e9tiquette de certification verte servant \u00e0 identifier les m\u00e9langes (CFIA/ACIA 0038) doit \u00eatre utilis\u00e9e pour les m\u00e9langes de d\u00e9nomination Canada certifi\u00e9e (un m\u00e9lange comporte au moins deux sortes de cultures ou esp\u00e8ces).</li>\n<li>Les semences import\u00e9es d\u2019une vari\u00e9t\u00e9 non enregistr\u00e9e qui sont class\u00e9es sous une d\u00e9nomination de la cat\u00e9gorie Canada g\u00e9n\u00e9alogique doit \u00eatre muni de l\u2019\u00e9tiquette orange des vari\u00e9t\u00e9s non enregistr\u00e9es (CFIA/ACIA 0039).</li>\n</ul>\n<p>Les semences certifi\u00e9es import\u00e9es continueront d\u2019\u00eatre soustraites \u00e0 l\u2019obligation d\u2019utiliser une \u00e9tiquette interagences si :</p>\n<ul>\n<li>leur emballage est muni d\u2019une \u00e9tiquette d\u00e9livr\u00e9e ou approuv\u00e9e par une agence officielle de certification;</li>\n<li>les semences sont de qualit\u00e9 certifi\u00e9e;</li>\n<li>la d\u00e9nomination de cat\u00e9gorie Canada certifi\u00e9e et le num\u00e9ro du classificateur agr\u00e9\u00e9 apparaissent sur l\u2019emballage.</li>\n</ul>\n<p>Les \u00e9tablissements semenciers agr\u00e9\u00e9s peuvent imprimer la nouvelle \u00e9tiquette bleue une fois que l\u2019<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> aura re\u00e7u une demande \u00e9crite pour imprimer des \u00e9tiquettes suppl\u00e9mentaires et qu\u2019une modification de l\u2019entente de permis aura \u00e9t\u00e9 \u00e9mise</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}