{
    "dcr_id": "1528485005815",
    "title": {
        "en": "What the <i>Safe Food for Canadians Regulations</i> mean for consumers",
        "fr": "Ce que signifie le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> pour les consommateurs"
    },
    "html_modified": "13-6-2018",
    "modified": "31-1-2019",
    "issued": "8-6-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/sfcr_chricle_article3_1528485005815_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/sfcr_chricle_article3_1528485005815_fra"
    },
    "parent_ia_id": "1528746084227",
    "ia_id": "1528824875029",
    "parent_node_id": "1528746084227",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "What the <i>Safe Food for Canadians Regulations</i> mean for consumers",
        "fr": "Ce que signifie le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> pour les consommateurs"
    },
    "label": {
        "en": "What the <i>Safe Food for Canadians Regulations</i> mean for consumers",
        "fr": "Ce que signifie le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> pour les consommateurs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1528824875029",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1528746083978",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/food-safety/consumers/",
        "fr": "/inspecter-et-proteger/salubrite-des-aliments/consommateurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "What the Safe Food for Canadians Regulations mean for consumers",
            "fr": "Ce que signifie le R\u00e8glement sur la salubrit\u00e9 des aliments au Canada pour les consommateurs"
        },
        "description": {
            "en": "Canada is widely regarded as having one of the strongest food safety systems in the world. But the speed, volume and complexity of food production have brought significant new risks and challenges, including new threats to food safety, changing consumer preferences and the development of prevention-focused international standards.",
            "fr": "Le Canada est reconnu comme ayant un syst\u00e8me de salubrit\u00e9 des aliments qui compte parmi les plus solides au monde. Toutefois, la rapidit\u00e9, le volume et la complexit\u00e9 de la production alimentaire entra\u00eenent de nouveaux risques et d\u00e9fis consid\u00e9rables, notamment de nouvelles menaces pour la salubrit\u00e9 des aliments, les changements de pr\u00e9f\u00e9rences des consommateurs et l'\u00e9laboration de normes internationales ax\u00e9es sur la pr\u00e9vention."
        },
        "keywords": {
            "en": "Safe Food for Canadians Regulations, SFCR, slaughter, transportation, controls, animal welfare, humane, consumers",
            "fr": "R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RACF, transport, contr\u00f4les, bien-\u00eatre des animaux, cruaut\u00e9, abattage, consommateurs"
        },
        "dcterms.subject": {
            "en": "food,agri-food industry,food inspection,agri-food products,regulations,food safety",
            "fr": "aliment,industrie agro-alimentaire,inspection des aliments,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2019-01-31",
            "fr": "2019-01-31"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "What the Safe Food for Canadians Regulations mean for consumers",
        "fr": "Ce que signifie le R\u00e8glement sur la salubrit\u00e9 des aliments au Canada pour les consommateurs"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\"> \n<img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_chricle_article_3_1528746029596_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"A family of three is eating dinner at their kitchen table.\">\n</div>\n\n<p>Canada is widely regarded as having one of the strongest food safety systems in the world. But the speed, volume and complexity of food production have brought significant new risks and challenges, including new threats to food safety, changing consumer preferences and the development of prevention-focused international standards.</p> \n\n<p>The Canadian Food Inspection Agency (CFIA) is responding to these challenges to maintain Canada's reputation as a world leader in food safety and to help food businesses keep consumers' trust.</p>\n\n<p>The publication and coming into force of the <i>Safe Food for Canadians Regulations</i> (SFCR) is a major step forward in further safeguarding Canada's food supply and enhancing the health and well-being of consumers. The <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr>, which are now in force, simplifies and strengthens rules for food in Canada, whether produced here or imported into the country.</p>\n\n<h2>What the regulations mean for Canadian families</h2>\n\n<p>The <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr> protects Canadian families by putting a greater emphasis on preventing risks to food safety. The regulations provide clear and consistent rules for food commodities so consumers can be confident that food on grocery shelves is safer to eat, whether it is produced in Canada or abroad. Consumers benefit from:</p>\n\n<ul class=\"lst-spcd\">\n<li>safer food</li>\n<li>regulations that focus on prevention and that target unsafe food practices</li>\n<li>tougher penalties for activities that put health and safety at risk</li>\n<li>greater control over imports; and</li>\n<li>faster recalls and removal of unsafe foods from store shelves</li>\n</ul>\n\n<p>The new rules are consistent with international food safety standards, and will strengthen Canada's food safety system, enable industry to innovate, and create greater market access opportunities for Canadian food products exported abroad.</p> \n\n<h2>Consumers with allergies</h2>\n\n<p>The regulations also help get unsafe food (such as undeclared allergens) off the shelves faster by requiring businesses that import, or prepare food for export or to be sent across provincial or territorial boundaries to trace their food back to their supplier and forward to whom they sold their products. Retailers are not required to trace their food forward to consumers to whom they sold their products.</p>\n\n<p>The <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr> requires businesses to develop and implement preventive controls to anticipate and address potential food safety risks, including the possibility of unintentionally introducing allergens.</p>\n\n<p>To learn more about the <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr>, visit our <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/eng/1427299500843/1427299800380\">tools, information and resources</a>.</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n    \n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\"> \n<img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_chricle_article_3_1528746029596_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"Famille de trois en train de souper \u00e0 la table de cuisine.\">\n</div>\n\n<p>Le Canada est reconnu comme ayant un syst\u00e8me de salubrit\u00e9 des aliments qui compte parmi les plus solides au monde. Toutefois, la rapidit\u00e9, le volume et la complexit\u00e9 de la production alimentaire entra\u00eenent de nouveaux risques et d\u00e9fis consid\u00e9rables, notamment de nouvelles menaces pour la salubrit\u00e9 des aliments, les changements de pr\u00e9f\u00e9rences des consommateurs et l'\u00e9laboration de normes internationales ax\u00e9es sur la pr\u00e9vention.</p> \n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) r\u00e9pond \u00e0 ces d\u00e9fis pour maintenir la r\u00e9putation de chef de file mondial du Canada au chapitre de la salubrit\u00e9 alimentaire et pour aider les entreprises alimentaires \u00e0 maintenir la confiance des consommateurs.</p>\n\n<p>La publication et l'entr\u00e9e en vigueur du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC) constituent une \u00e9tape importante pour mieux prot\u00e9ger l'approvisionnement alimentaire du Canada et am\u00e9liorer la sant\u00e9 et le bien-\u00eatre des consommateurs. Le <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr>, qui est maintenant en vigueur, simplifie et renforce les r\u00e8gles de salubrit\u00e9 au Canada pour les aliments produits ici ou import\u00e9s.</p>  \n\n<h2>Ce que le R\u00e8glement signifie pour les familles canadiennes</h2>\n\n<p>Le <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr> prot\u00e8ge les familles canadiennes en mettant l'accent sur la pr\u00e9vention des risques pour la salubrit\u00e9 des aliments. Le r\u00e8glement \u00e9tablit des r\u00e8gles claires et uniformes visant les produits alimentaires de sorte que les consommateurs puissent davantage compter sur la salubrit\u00e9 des aliments offerts \u00e0 l'\u00e9picerie, peu importe s'ils sont produits au Canada ou \u00e0 l'\u00e9tranger. Les avantages pour les consommateurs sont les suivants\u00a0:</p>\n\n<ul class=\"lst-spcd\">\n<li>la salubrit\u00e9 accrue des aliments;</li>\n<li>une r\u00e9glementation qui met l'accent sur la pr\u00e9vention et qui cible les pratiques alimentaires non s\u00e9curitaires;</li>\n<li>l'imposition de sanctions plus s\u00e9v\u00e8res pour les activit\u00e9s qui compromettent la sant\u00e9 et la s\u00e9curit\u00e9 des Canadiens;</li>\n<li>un meilleur contr\u00f4le des produits import\u00e9s;</li>\n<li>le rappel et le retrait plus rapides des aliments non salubres ou dangereux du march\u00e9.</li>\n</ul>\n\n<p>Les nouvelles r\u00e8gles s'harmonisent avec les normes internationales de salubrit\u00e9 des aliments. Elles renforceront le syst\u00e8me canadien de salubrit\u00e9 des aliments, permettront \u00e0 l'industrie d'innover, et ouvriront des d\u00e9bouch\u00e9s pour les produits alimentaires canadiens export\u00e9s \u00e0 l'\u00e9tranger.</p> \n\n<h2>Consommateurs ayant des allergies</h2>\n\n<p>Le R\u00e8glement aide aussi \u00e0 retirer du march\u00e9 plus rapidement les aliments non salubres ou dangereux (notamment ceux qui contiennent des allerg\u00e8nes non d\u00e9clar\u00e9s) en exigeant que les entreprises qui importent des aliments ou qui conditionnent des aliments aux fins d'exportation ou d'exp\u00e9dition au-del\u00e0 des fronti\u00e8res provinciales ou territoriales retracent leurs aliments en amont jusqu'\u00e0 leur fournisseur et en aval jusqu'\u00e0 la personne \u00e0 qui ils vendent leurs produits. Les d\u00e9taillants ne sont pas tenus de retracer leurs aliments en aval jusqu'aux consommateurs \u00e0 qui ils vendent leurs produits.</p>\n\n<p>En application du <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr>, les entreprises doivent \u00e9tablir et mettre en \u0153uvre des contr\u00f4les pr\u00e9ventifs pour pr\u00e9venir et g\u00e9rer les risques potentiels pour la salubrit\u00e9 des aliments, dont la possibilit\u00e9 d'introduire des allerg\u00e8nes involontairement.</p>\n\n<p>Pour apprendre davantage sur le <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr>, visitez nos <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/fra/1427299500843/1427299800380\">outils, renseignements et ressources</a>.</p>\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}