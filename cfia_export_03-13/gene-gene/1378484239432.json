{
    "dcr_id": "1378484239432",
    "title": {
        "en": "Export of Captive Birds to the European Union",
        "fr": "Exportation d'oiseaux \u00e9lev\u00e9s en captivit\u00e9 vers l'Union europ\u00e9enne"
    },
    "html_modified": "2024-03-12 3:56:27 PM",
    "modified": "2013-09-06",
    "issued": "2013-09-06 12:17:21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_pol_captive_birds_eu_1378484239432_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_pol_captive_birds_eu_1378484239432_fra"
    },
    "ia_id": "1378484479214",
    "parent_ia_id": "1377704102343",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports|Guidance Documents",
        "fr": "Exportations|Documents d'orientation"
    },
    "commodity": {
        "en": "Exports|Terrestrial Animal Health|Meat and Poultry Products",
        "fr": "Exportations|Sant\u00e9 des animaux terrestres|Produits de viande et de volaille"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1377704102343",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Export of Captive Birds to the European Union",
        "fr": "Exportation d'oiseaux \u00e9lev\u00e9s en captivit\u00e9 vers l'Union europ\u00e9enne"
    },
    "label": {
        "en": "Export of Captive Birds to the European Union",
        "fr": "Exportation d'oiseaux \u00e9lev\u00e9s en captivit\u00e9 vers l'Union europ\u00e9enne"
    },
    "templatetype": "content page 1 column",
    "node_id": "1378484479214",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1377704017956",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/export-policies/export-of-captive-birds-to-the-european-union/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/politiques-d-exportation/exportation-d-oiseaux-eleves-en-captivite-vers-l-u/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Export of Captive Birds to the European Union",
            "fr": "Exportation d'oiseaux \u00e9lev\u00e9s en captivit\u00e9 vers l'Union europ\u00e9enne"
        },
        "description": {
            "en": "Directive on the export of captive birds to the European Union",
            "fr": "Directive concernant l'exportation d'oiseaux \u00e9lev\u00e9s en captivit\u00e9 vers l'Union europ\u00e9enne"
        },
        "keywords": {
            "en": "Animals, Animal Health, captive birds, export, European Union, breeding establishments",
            "fr": "oiseaux \u00e9lev\u00e9s en captivit\u00e9, exportation, Union europ\u00e9enne, \u00e9tablissements d'\u00e9levage"
        },
        "dcterms.subject": {
            "en": "exports,inspection,regulation",
            "fr": "exportation,inspection,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-09-06 12:17:21",
            "fr": "2013-09-06 12:17:21"
        },
        "modified": {
            "en": "2013-09-06",
            "fr": "2013-09-06"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Export of Captive Birds to the European Union",
        "fr": "Exportation d'oiseaux \u00e9lev\u00e9s en captivit\u00e9 vers l'Union europ\u00e9enne"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=5&amp;ga=27#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p class=\"text-right\">AHPD-DSAE-2007-8-1</p>\n<p>As of July 1, 2007, importation of captive birds into the European Union from approved breeding establishments may recommence. The relevant legislation is Commission Regulation (<abbr title=\"European Community\">EC</abbr>) 318/2007.</p>\n<p>The term <strong>captive bird</strong> shall apply to all avian species with the exception of:</p>\n<ul class=\"lst-lwr-alph\">\n<li>fowls, turkeys, guinea fowl, ducks, geese, quails, pigeons, pheasants, partridge, and ratites reared or kept in captivity for breeding, the production of meat or eggs for consumption, or for re-stocking supplies of game</li>\n<li>birds imported for conservation programmes approved by the competent authority in the Member State of destination</li>\n<li>pet animals accompanying their owner as defined in 92/65/<abbr title=\"European Economic Community\">EEC</abbr></li>\n<li>birds intended for zoos, circuses, amusement parks or experiments</li>\n<li>birds destined for bodies approved according to Article 13 of 92/65/<abbr title=\"European Economic Community\">EEC</abbr></li>\n<li>racing pigeons which are introduced to the territory of the European Union from a neighbouring third country where they are normally resident and then immediately released with the expectation that they will fly back to that country</li>\n<li>birds imported from Andorra, Liechtenstein, Monaco, Norway, San Marino, Switzerland and the Vatican City State.</li>\n</ul>\n<p>Captive birds may only be exported from breeding establishments approved by the competent authority of the exporting country and published in relevant <abbr title=\"European Union\">EU</abbr> documentation.</p>\n<p>Where an existing breeding establishment is interested in export to the <abbr title=\"European Union\">EU</abbr>, Canadian Food Inspection Agency (CFIA) district office staff must inspect the establishment to ensure that it complies with the standards set out in Annex <abbr title=\"2\">II</abbr> of the directive. This approval must be forwarded to the Area Office Export Specialist for review and recommendation. Once signed off by the area office, the inspection should be forwarded to <abbr title=\"Doctor\">Dr.</abbr> M. Homewood for final approval.</p>\n<p>Once approved, an approval number (unique) will be assigned by headquarters. As with other approval numbers, a listing of the approved establishments will be maintained on the O drive (intended for internal use).</p>\n<p>Headquarters will advise Brussels of this information who will in turn publish it officially in order that all border inspection points are aware of the approval.</p>\n<p>No exports may take place prior to this publication.</p>\n<p>From previous experience with other commodities, we know that there may be a significant delay between <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> approval and publication in the <abbr title=\"European Union\">EU</abbr>. Please advise clients of this policy.</p>\n<p>The conditions for premises approval are set out in Annex <abbr title=\"2\">II</abbr> of 318/2007 as is the model health certificate.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=5&amp;ga=27#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p class=\"text-right\">AHPD-DSAE-2007-8-1</p>\n<p>\u00c0 compter du 1er juillet 2007, l'Union europ\u00e9enne pourra de nouveau importer des oiseaux \u00e9lev\u00e9s en captivit\u00e9 en provenance d'\u00e9tablissements d'\u00e9levage agr\u00e9\u00e9s, conform\u00e9ment au R\u00e8glement <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr> 318/2007 de l'Union.</p>\n<p>Le terme <strong>oiseau \u00e9lev\u00e9 en captivit\u00e9</strong> s'applique \u00e0 toutes les esp\u00e8ces aviaires, sauf\u00a0:</p>\n<ul class=\"lst-lwr-alph\">\n<li>aux poules, aux dindes, aux pintades, aux canards, aux oies, aux cailles, aux pigeons, aux faisans, aux perdrix et aux ratites \u00e9lev\u00e9s ou d\u00e9tenus en captivit\u00e9 \u00e0 des fins de reproduction, de production de viandes ou d'oeufs destin\u00e9s \u00e0 la consommation, ou de fourniture de gibier de repeuplement;</li>\n<li>aux oiseaux import\u00e9s dans le cadre de programmes de conservation approuv\u00e9s par l'autorit\u00e9 comp\u00e9tente de l'\u00c9tat membre de destination;</li>\n<li>aux animaux de compagnie vis\u00e9s \u00e0 la directive 92/65/<abbr title=\"Communaut\u00e9 \u00e9conomique europ\u00e9enne\">CEE</abbr> et accompagnant leur propri\u00e9taire;</li>\n<li>aux oiseaux destin\u00e9s \u00e0 des zoos, \u00e0 des cirques, \u00e0 des parcs d'attraction ou \u00e0 des laboratoires d'exp\u00e9rimentation;</li>\n<li>aux oiseaux destin\u00e9s \u00e0 des organismes agr\u00e9\u00e9s conform\u00e9ment \u00e0 l'article 13 de la directive 92/65/<abbr title=\"Communaut\u00e9 \u00e9conomique europ\u00e9enne\">CEE</abbr>;</li>\n<li>aux pigeons voyageurs qui sont introduits sur le territoire de l'Union au d\u00e9part d'un pays tiers voisin o\u00f9 ils sont normalement d\u00e9tenus et qui sont imm\u00e9diatement rel\u00e2ch\u00e9s dans le but qu'ils retournent dans ce pays;</li>\n<li>aux oiseaux import\u00e9s d'Andorre, du Liechtenstein, de Monaco, de Norv\u00e8ge, de Saint-Marin, de Suisse et de l'\u00c9tat de la Cit\u00e9 du Vatican.</li>\n</ul>\n<p>Seuls les \u00e9tablissements d'\u00e9levage agr\u00e9\u00e9s par l'autorit\u00e9 comp\u00e9tente du pays exportateur et figurant dans les documents pertinents de l'Union europ\u00e9enne peuvent exporter des oiseaux \u00e9lev\u00e9s en captivit\u00e9.</p>\n<p>Le personnel du bureau de district de l'Agence canadienne d'inspection des aliments (ACIA) inspecte chaque \u00e9tablissement qui souhaite exporter ses oiseaux vers des pays de l'Union europ\u00e9enne pour s'assurer qu'il respecte les conditions fix\u00e9es \u00e0 l'annexe <abbr title=\"2\">II</abbr> de la directive. Le rapport d'inspection est ensuite envoy\u00e9 au sp\u00e9cialiste des exportations du bureau du centre op\u00e9rationnel pour examen et recommandation. Une fois sign\u00e9 par le bureau du centre op\u00e9rationnel, le rapport d'inspection est envoy\u00e9 \u00e0 la <abbr title=\"Docteure\">D<sup>re</sup></abbr> M. Homewood pour approbation finale.</p>\n<p>Une fois l'approbation obtenue, le si\u00e8ge social attribue un num\u00e9ro d'agr\u00e9ment (unique) \u00e0 l'\u00e9tablissement inspect\u00e9. Tout comme les autres num\u00e9ros d'agr\u00e9ment, la liste des \u00e9tablissements agr\u00e9\u00e9s se trouve sur le lecteur O (r\u00e9serv\u00e9 \u00e0 l\u2019usage interne).</p>\n<p>Le personnel du si\u00e8ge social communique ensuite l'information au bureau de Bruxelles qui la publie officiellement de mani\u00e8re \u00e0 ce que tous les postes d'inspection frontaliers soient au courant de l'agr\u00e9ment.</p>\n<p>Aucune exportation n'est autoris\u00e9e avant la publication de cette information.</p>\n<p>Nous savons par exp\u00e9rience (dans le cas d'autres produits) qu'il peut y avoir un d\u00e9lai important entre le moment o\u00f9 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> agr\u00e9e un \u00e9tablissement et celui o\u00f9 l'information est publi\u00e9e dans les pays de l'Union europ\u00e9enne. Veuillez informer vos clients de cette politique.</p>\n<p>Les conditions relatives \u00e0 l'agr\u00e9ment des \u00e9tablissements se trouvent \u00e0 l'annexe <abbr title=\"2\">II</abbr> du R\u00e8glement <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr> 318/2007, tout comme le mod\u00e8le de certificat de police sanitaire.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}