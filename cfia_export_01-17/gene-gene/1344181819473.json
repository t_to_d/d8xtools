{
    "dcr_id": "1344181819473",
    "title": {
        "en": "Fact Sheet - Pullorum Disease and Fowl Typhoid",
        "fr": "Fiche de renseignements - La pullorose et la typhose aviaire"
    },
    "html_modified": "2012-08-05 11:50",
    "modified": "5-8-2012",
    "issued": "5-8-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_pullorum_typhoid_fact_1344181819473_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_pullorum_typhoid_fact_1344181819473_fra"
    },
    "parent_ia_id": "1344187181281",
    "ia_id": "1344194747058",
    "parent_node_id": "1344187181281",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fact Sheet - Pullorum Disease and Fowl Typhoid",
        "fr": "Fiche de renseignements - La pullorose et la typhose aviaire"
    },
    "label": {
        "en": "Fact Sheet - Pullorum Disease and Fowl Typhoid",
        "fr": "Fiche de renseignements - La pullorose et la typhose aviaire"
    },
    "templatetype": "content page 1 column",
    "node_id": "1344194747058",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1344187098901",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/pullorum-disease/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/pullorose/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fact Sheet - Pullorum Disease and Fowl Typhoid",
            "fr": "Fiche de renseignements - La pullorose et la typhose aviaire"
        },
        "description": {
            "en": "Foot-and-Mouth Disease is a severe, highly communicable viral disease of cattle and swine. It also affects sheep, goats, deer and other cloven-hoofed ruminants.",
            "fr": "La fi\u00e8vre aphteuse est une virose grave hautement transmissible, qui affecte les bovins et les porcs. Le virus peut aussi s'attaquer aux ovins, aux caprins, aux cerfs et \u00e0 d'autres ruminants artiodactyles."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, Reportable Diseases Regulations, reportable diseases, Foot-and-Mouth Disease, FMD, cattle, pigs, sheep goats, deer, ruminants",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, R\u00e8glement sur les maladies d\u00e9clarables, maladies \u00e0 d\u00e9claration obligatoire, fi\u00e8vre aphteuse, bovins, porcs, ovins, caprins, cerfs, ruminants"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,livestock,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,b\u00e9tail,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-08-05 11:50:23",
            "fr": "2012-08-05 11:50:23"
        },
        "modified": {
            "en": "2012-08-05",
            "fr": "2012-08-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fact Sheet - Pullorum Disease and Fowl Typhoid",
        "fr": "Fiche de renseignements - La pullorose et la typhose aviaire"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>What are pullorum disease and fowl typhoid?</h2>\n<p>Pullorum disease is an infectious poultry disease caused by the bacterium <i><span lang=\"la\">Salmonella pullorum</span></i>. The disease affects mainly young chicks and poults, but can also affect older chickens, game birds, guinea fowl, ostriches, parrots, peafowl, ring doves, sparrows and turkeys.</p>\n<p>Fowl typhoid is an infectious poultry disease caused by the bacterium <i><span lang=\"la\">Salmonella gallinarum</span></i>. The disease affects mainly mature or growing chickens, but has the ability to affect all chickens, ducks, grouse, guinea-fowl, peafowl, pheasants, quail and turkeys.</p>\n<h2>Are pullorum disease and fowl typhoid a risk to human health?</h2>\n<p>Both of these diseases are specific to birds and pose a low risk to human health.</p>\n<p>However, Health Canada recommends proper handling and cooking of eggs and poultry meat to prevent all potential risks associated with viral or bacterial contaminants.</p>\n<h2>What are the clinical signs of pullorum disease and fowl typhoid?</h2>\n<p>The clinical signs of pullorum disease and fowl typhoid are very similar. Pullorum disease is generally a disease of young chicks and poults, while fowl typhoid is more predominant in growing and adult birds.</p>\n<p>In young birds:</p>\n<ul>\n<li>anorexia;</li>\n<li>depression;</li>\n<li>diarrhea;</li>\n<li>dying or death (highest mortality rate in the first 2 weeks of life and in incubators); and</li>\n<li>laboured breathing.</li>\n</ul>\n<p>In growing and mature birds:</p>\n<ul>\n<li>anorexia;</li>\n<li>decreased egg production;</li>\n<li>depression;</li>\n<li>diarrhea;</li>\n<li>high fever;</li>\n<li>increased mortality (usually higher in chickens than turkeys); and</li>\n<li>poor hatchability.</li>\n</ul>\n<h2>Where are pullorum disease and fowl typhoid found?</h2>\n<p>Canada has been considered free of pullorum disease and fowl typhoid since 1982. There have been isolated occurrences limited to small backyard flocks in Vancouver, <abbr title=\"British Columbia\">B.C.</abbr></p>\n<h2>How are pullorum disease and fowl typhoid transmitted?</h2>\n<p>The most common ways for these diseases to spread is through contact with infected birds, and the transmission from hens to chicks through the egg. Game birds and backyard flocks may act as reservoirs for the infection.</p>\n<p>These diseases can also spread via contaminated feed, water and litter, as well as through contaminated clothing, footwear, vehicles and equipment.</p>\n<h2>How are pullorum disease and fowl typhoid diagnosed?</h2>\n<p>Pullorum disease and fowl typhoid may be suspected based on the above clinical signs. Laboratory testing of blood and tissue samples from affected animals is necessary to confirm the diagnosis.</p>\n<h2>How are pullorum and fowl typhoid treated?</h2>\n<p>There are no treatments available for these diseases, but the best ways to protect flocks from these diseases are through the following:</p>\n<ul>\n<li>keep poultry away from areas frequented by wild fowl;</li>\n<li>keep strict control over access to poultry houses;</li>\n<li>keep equipment cleaned and disinfected before taking it into poultry houses;</li>\n<li>do not keep bird feeders or create duck ponds close to poultry barns as they attract wild birds; and</li>\n<li>maintain high sanitation standards.</li>\n</ul>\n<p>For <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/eng/1344748344710/1344748451521\">avian biosecurity information</a> please visit the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>\u2019s website at: www.inspection.gc.ca/english/anima/biosec/aviafse.shtml</p>\n<h2>What is done to protect domestic poultry in Canada from pullorum disease and fowl typhoid?</h2>\n<p>Pullorum disease and fowl typhoid are \u201creportable diseases\u201d under the <i>Health of Animals Act</i>. This means that all suspected cases must be reported to the Canadian Food Inspection Agency (CFIA) for immediate investigation by inspectors.</p>\n<h2>How would the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> respond to an outbreak of pullorum disease and fowl typhoid?</h2>\n<p>Canada\u2019s emergency response strategy to an outbreak of pullorum disease and/or fowl typhoid would be to:</p>\n<ul>\n<li>eradicate the disease; and</li>\n<li>re-establish Canada\u2019s disease-free status as quickly as possible.</li>\n</ul>\n<p>In an effort to eradicate pullorum disease and/or fowl typhoid, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would use its \u201cstamping out\u201d policy, which includes:</p>\n<ul>\n<li>the humane destruction of all infected and exposed animals;</li>\n<li>surveillance and tracing of potentially infected or exposed animals;</li>\n<li>strict quarantine and animal movement controls to prevent spread;</li>\n<li>strict decontamination of infected premises; and</li>\n<li>zoning to define infected and disease-free areas.</li>\n</ul>\n<p>Owners whose animals are ordered destroyed may be eligible for compensation.</p>\r\n<h2 class=\"font-medium black\">Additional information</h2>\n<ul>\n<li><a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a></li>\n</ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Qu'est-ce que la pullorose et la typhose aviaire?</h2>\n<p>La pullorose est une maladie infectieuse de la volaille caus\u00e9e par la bact\u00e9rie <i><span lang=\"la\">Salmonella pullorum</span></i>. La maladie touche principalement les poussins et la jeune volaille, mais peut \u00e9galement toucher les poulets plus \u00e2g\u00e9s, le gibier \u00e0 plumes, les pintades, les autruches, les perroquets, les paons, les tourterelles, les moineaux et les dindes.</p>\n<p>La typhose aviaire est une maladie infectieuse de la volaille caus\u00e9e par la bact\u00e9rie <i><span lang=\"la\">Salmonella gallinarum</span></i>. La maladie touche principalement les poulets matures ou en croissance, mais peut aussi toucher tous les poulets, les canards, les t\u00e9tras, les pintades, les paons, les faisans, les cailles et les dindes.</p>\n<h2>La pullorose et la typhose aviaire posent-elles un risque pour la sant\u00e9 humaine?</h2>\n<p>Les deux maladies s'attaquent surtout aux oiseaux et posent peu de risques pour la sant\u00e9 humaine.</p>\n<p>Cependant, Sant\u00e9 Canada recommande de manipuler les \u0153ufs et la volaille avec pr\u00e9caution et de bien les faire cuire afin de pr\u00e9venir tout risque potentiel d'infection associ\u00e9 aux contaminants viraux ou bact\u00e9riens.</p>\n<h2>Quels sont les signes cliniques de la pullorose et la typhose aviaire?</h2>\n<p>Les sympt\u00f4mes de la pullorose et de la typhose aviaire se ressemblent beaucoup. La pullorose frappe g\u00e9n\u00e9ralement les poussins et la jeune volaille alors que la typhose aviaire touche plus fr\u00e9quemment les oiseaux en croissance et adultes.</p>\n<p>Voici les signes qu'on peut observer chez les jeunes oiseaux :</p>\n<ul>\n<li>perte d'app\u00e9tit</li>\n<li>apathie</li>\n<li>diarrh\u00e9e</li>\n<li>agonie ou mort (taux de mortalit\u00e9 le plus \u00e9lev\u00e9 dans les deux premi\u00e8res semaines de vie et dans les incubateurs)</li>\n<li>respiration laborieuse</li>\n</ul>\n<p>Voici les signes qu'on peut observer chez les oiseaux en croissance ou matures :</p>\n<ul>\n<li>perte d'app\u00e9tit</li>\n<li>baisse de la production d'\u0153ufs</li>\n<li>apathie</li>\n<li>diarrh\u00e9e</li>\n<li>fi\u00e8vre \u00e9lev\u00e9e</li>\n<li>augmentation du nombre de d\u00e9c\u00e8s (mortalit\u00e9 habituellement plus \u00e9lev\u00e9e chez les poules que chez les dindes)</li>\n<li>faible taux d'\u00e9closion</li>\n</ul>\n<h2>O\u00f9 trouve-t-on la pullorose et la typhose aviaire?</h2>\n<p>Le Canada est d\u00e9clar\u00e9 exempt de pullorose et de typhose aviaire depuis 1982. Des cas isol\u00e9s ont \u00e9t\u00e9 signal\u00e9s dans de petits \u00e9levages \u00e0 Vancouver, en Colombie-Britannique.</p>\n<h2>Comment la pullorose et la typhose aviaire se transmettent-elles?</h2>\n<p>Les modes de transmission les plus fr\u00e9quents sont le contact avec des oiseaux infect\u00e9s et la transmission de la poule aux poussins par l'entremise des \u0153ufs. Le gibier \u00e0 plumes et les oiseaux de basse-cour peuvent \u00eatre des r\u00e9servoirs de l'infection.</p>\n<p>Ces maladies peuvent \u00e9galement \u00eatre transmises par les aliments pour animaux, l'eau et la liti\u00e8re contamin\u00e9s, ainsi que par des objets contamin\u00e9s comme les v\u00eatements, les chaussures, les v\u00e9hicules et l'\u00e9quipement.</p>\n<h2>Comment diagnostique-t-on la pullorose et la typhose aviaire?</h2>\n<p>On peut soup\u00e7onner la pr\u00e9sence de la pullorose et la typhose aviaire selon les sympt\u00f4mes d\u00e9crits pr\u00e9c\u00e9demment. Il faut cependant soumettre des \u00e9chantillons de sang et de tissus de l'animal infect\u00e9 \u00e0 des analyses en laboratoire pour confirmer le diagnostic.</p>\n<h2>Comment traite-t-on la pullorose et la typhose aviaire?</h2>\n<p>Il n'existe aucun traitement pour l'une ou l'autre des maladies, mais les meilleurs moyens de prot\u00e9ger les troupeaux sont les suivants :</p>\n<ul>\n<li>tenir la volaille loin des aires fr\u00e9quent\u00e9es par les oiseaux sauvages;</li>\n<li>contr\u00f4ler rigoureusement l'acc\u00e8s aux poulaillers;</li>\n<li>nettoyer et d\u00e9sinfecter l'\u00e9quipement avant de l'introduire dans le poulailler;</li>\n<li>\u00e9viter d'installer des mangeoires d'oiseaux ou d'am\u00e9nager des \u00e9tangs pour les canards \u00e0 proximit\u00e9 des poulaillers, car ces installations attirent les oiseaux sauvages;</li>\n<li>maintenir des normes d'hygi\u00e8ne rigoureuses.</li>\n</ul>\n<p>Pour obtenir des <a href=\"http://10.141.237.58:81/francais/anima/biosec/aviafsf.shtml\">renseignements sur la bios\u00e9curit\u00e9 aviaire</a>, visitez le site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e0 l'adresse suivante : www.inspection.gc.ca/francais/anima/biosec/aviafsf.shtml.</p>\n<h2>Que fait-on pour prot\u00e9ger la volaille du Canada contre la pullorose et la typhose aviaire?</h2>\n<p>La pullorose et la typhose aviaire sont des maladies \u00e0 d\u00e9claration obligatoire en vertu de la <i>Loi sur la sant\u00e9 des animaux</i>. Cela signifie que tous les cas suspects doivent \u00eatre signal\u00e9s \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) pour que ses inspecteurs puissent lancer une enqu\u00eate imm\u00e9diate.</p>\n<h2>Comment l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> interviendrait-elle en cas d'\u00e9closion de pullorose et de typhose aviaire?</h2>\n<p>La strat\u00e9gie d'intervention du Canada en cas d'\u00e9closion de pullorose ou de typhose aviaire aurait pour but :</p>\n<ul>\n<li>d'\u00e9radiquer la maladie;</li>\n<li>de r\u00e9tablir le plus t\u00f4t possible le statut du Canada en tant que pays exempt de la maladie.</li>\n</ul>\n<p>Pour tenter d'\u00e9radiquer la pullorose ou la typhose aviaire, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> aurait recours \u00e0 sa politique d'abattage sanitaire qui comprend ce qui suit :</p>\n<ul>\n<li>La destruction sans cruaut\u00e9 de tous les animaux infect\u00e9s et expos\u00e9s \u00e0 la maladie.</li>\n<li>La surveillance et le retra\u00e7age des animaux pouvant \u00eatre infect\u00e9s ou avoir \u00e9t\u00e9 expos\u00e9s \u00e0 la maladie.</li>\n<li>L'imposition de mesures de quarantaine rigoureuses et le contr\u00f4le des d\u00e9placements des animaux pour emp\u00eacher la propagation de la maladie.</li>\n<li>La d\u00e9contamination compl\u00e8te des lieux infect\u00e9s.</li>\n<li>Le zonage pour d\u00e9limiter les zones infect\u00e9es et les zones exemptes de la maladie.</li>\n</ul>\n<p>Les propri\u00e9taires dont on a ordonn\u00e9 la destruction des animaux peuvent \u00eatre admissibles \u00e0 une indemnisation.</p>\r\n<h2 class=\"font-medium black\">Renseignements additionnels</h2>\n\n<ul>\n<li><a href=\"/fra/1300462382369/1300462438912\">Bureaux de sant\u00e9 animale</a></li>\n</ul> \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}