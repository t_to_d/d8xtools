<?php

use Drush\Drush;

use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database; // Rather than db_query use this instead.

class UpdateNode
{
  public $could_not_load = FALSE;
  public $update_success = FALSE;
  protected $node;
  protected $trnode;
  protected $data;
  protected $preserveNid = false;
  protected $connection = NULL;

  public function loadData($jsonfile)
  {
    $this->data = json_decode(file_get_contents($jsonfile));
  }

  public function nid()
  {
    return $this->node->id();
  }

  public function id()
  {
    return $this->data->node_id;
  }

  public function legacy_id()
  {
    //Drush::output()->writeln('debug dcrid ' . (int) $this->data->nid);
    return $this->data->dcr_id;
  }

  public function loadNodeByDcrId() {
    //dcr_id
    if (is_null($this->connection)) {
      $this->connection = Database::getConnection();
    }
    $nid = $this->connection->query('SELECT nid FROM {node} where dcr_id = :dcrid',
       [':dcrid' => $this->legacy_id()])->fetchField();
     
    if (is_numeric($nid)) {
      $node = Node::load($nid);
      $oct11_2020 = strtotime('2020-10-10');
      // @TODO: REMOVE NEXT 4 LINES AFTER HARD LAUNCH OR DISABLE.
      if ($node->getChangedTime() < $oct11_2020) {
        $this->could_not_load = TRUE;
        return FALSE;
      }
    } else {
      $this->could_not_load = TRUE;
      Drush::output()->writeln('Unable to load d8 node, d8nid was not found by dcrid=' . $this->legacy_id());
    }
    if ($node) {
      $this->node = $node;
      $this->trnode = $this->node->getTranslation('fr'); //Get the translation
    }
  }

  public function updateField($field, $d8_field=null)
  {
    global $nid_old_to_new;
    global $nid_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }

    //Implement the mapping relation between legacy news_category_type_id and field_newstype
    $categorytypeid = $this->data->news_category_type_id;
    $newstypeid = 0;
    if ($categorytypeid) {
      switch ($categorytypeid) {
        case '1308325693175':
              $newstypeid = 21;
              break;
        case '1508942999196':
              $newstypeid = 22;
              break;
        case '1508942999197':
              $newstypeid = 23;
              break;
        case '1308334182981':
              $newstypeid = 24;
              break;
        case '1308334182982':
              $newstypeid = 25;
              break;
        case '1308334182983':
              $newstypeid = 26;
              break;
        case '1508942999198':
              $newstypeid = 27;
              break;
        case '1379951494338':
              $newstypeid = 28;
              break;
        case '1591971211315':
              $newstypeid = 111;
              break;
      }  //End of category type id mapping
    }

    switch ($field) {
      case 'field_name':
        // Some field.
        // Do stuff
        // $this->node->set($field, $this->data->field_name);
        // $this->trnode->set($field, $this->data->field_name);
        break;
      case 'field_newstype':
        // Some field.
        // Do stuff
        $this->node->set($field,$newstypeid);
        $this->trnode->set($field,$newstypeid);
        break;
      case 'field_newsexpirydate':
        $timestamp = strtotime($this->data->expirydate);
        $expirydate =  \Drupal::service('date.formatter')->format($timestamp, 'custom', 'Y-m-d\T06:00:00','UTC');
        $this->node->set($field,$expirydate );
        $this->trnode->set($field,$expirydate );
        break;
      case 'field_newsreviewedby':
        $this->node->set($field,$this->data->approvalsadvisor);
        $this->trnode->set($field,$this->data->approvalsadvisor);
        break;
      case 'field_newsapprovedby':
        $this->node->set($field,$this->data->approvalsdirector);
        $this->trnode->set($field,$this->data->approvalsdirector);
        break;
      case 'field_newssubmittername':
        $this->node->set($field,$this->data->submitername);
        $this->trnode->set($field,$this->data->submitername);
        break;
      case 'field_newssubmitteremail':
        $this->node->set($field,$this->data->submiteremail);
        $this->trnode->set($field,$this->data->submiteremail);
        break;
      case 'field_newssubmitterphone':
        $this->node->set($field,$this->data->submiterphoneNumber);
        $this->trnode->set($field,$this->data->submiterphoneNumber);
        break;
      case 'field_newstranslationnum':
        $this->node->set($field,$this->data->translateReqNumber);
        $this->trnode->set($field,$this->data->translateReqNumber);
        break;
      // for updating the missing informaiton of Employment
      case 'field_opportunityapproval':
        $this->node->set($field,$this->data->staffadvisor);
        $this->trnode->set($field,$this->data->staffadvisor);
        break;
      case 'field_name':
        $this->node->set($field,$this->data->submitername);
        $this->trnode->set($field,$this->data->submitername);
        break;
      case 'field_email':
        $this->node->set($field,$this->data->submiteremail);
        $this->trnode->set($field,$this->data->submiteremail);
        break;
      case 'field_employmentrequestorname':
        $this->node->set($field,$this->data->contactname);
        $this->trnode->set($field,$this->data->contactname);
        break;
      case 'field_employmentrequestoremail':
        $this->node->set($field,$this->data->contactemail);
        $this->trnode->set($field,$this->data->contactemail);
        break;
      case 'field_employmentrequestorphone':
        $this->node->set($field,$this->data->contactphonenumber);
        $this->trnode->set($field,$this->data->contactphonenumber);
        break;
      default:
	Drush::output()->writeln('Switch default, no action taken.');
        break;
    } //End switch
  } // End updateField function.

  public function save()
  {
    $this->node->set('moderation_state', 'published');
    $this->trnode->set('moderation_state', 'published');
    $this->node->save();
    $this->trnode->save();
  }

  public function updateBodyJson($lang = 'en') {
    $body_markup = '';
    if (property_exists($this->data, 'body')) {
      if (property_exists($this->data->body, $lang)) {
        $body_markup = $this->data->body->{$lang};
      }
    }
    if (is_array($this->data)) {
      if (is_array($this->data->body) && isset($this->data['body'][$lang])) {
        $body_markup = $this->data['body'][$lang];
      }
    }
    $regex_dwt = '/data-wb-boew=(\'.*?\')/m';
    preg_match_all($regex_dwt, $body_markup, $matches, PREG_SET_ORDER, 0);
    $number_of_groups = count($matches);
    if ($number_of_groups > 0) {
      foreach($matches as $match) {
        $json_to_clean = $match[1];
        $cleaned_json = str_replace('"', '&quot;', $json_to_clean);
        $body_markup = str_replace($json_to_clean, $cleaned_json, $body_markup);
      }
    }
    if (property_exists($this->data, 'body')) {
      if (property_exists($this->data->body, $lang)) {
        if ($this->data->body->{$lang} != $body_markup) {
          $this->update_success = TRUE;
          $this->data->body->{$lang} = $body_markup;
        }
      }
    }
    if (is_array($this->data)) {
      if (is_array($this->data->body) && isset($this->data['body'][$lang])) {
        if ($this->data['body'][$lang] != $body_markup) {
          $this->update_success = TRUE;
          $this->data['body'][$lang] = $body_markup;
        }
      }
    }
  }

  public function write_to_json() {
    if (file_exists($this->id().'.json')) {
      if ($fp = fopen($this->id().'.json', 'w')) {
        echo "Write " . $this->id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        fclose($fp);
      }
    }
    else if (file_exists($this->dcr_id().'.json')) {
      if ($fp = fopen($this->dcr_id().'.json', 'w')) {
        echo "Write " . $this->dcr_id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        fclose($fp);
      }
    }
    else {
      echo "file does not exist? where is it writing to? what folder is this?.\n";
    }
  }

}
