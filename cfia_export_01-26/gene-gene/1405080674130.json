{
    "dcr_id": "1405080674130",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Heracleum mantegazzianum</i> (Giant hogweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Heracleum mantegazzianum</i> (Berce du Caucase)"
    },
    "html_modified": "2024-01-26 2:22:30 PM",
    "modified": "2017-11-01",
    "issued": "2014-11-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_heracleum_mantegazzianum_1405080674130_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_heracleum_mantegazzianum_1405080674130_fra"
    },
    "ia_id": "1405080674943",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Heracleum mantegazzianum</i> (Giant hogweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Heracleum mantegazzianum</i> (Berce du Caucase)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Heracleum mantegazzianum</i> (Giant hogweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Heracleum mantegazzianum</i> (Berce du Caucase)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1405080674943",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/heracleum-mantegazzianum/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/heracleum-mantegazzianum/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Heracleum mantegazzianum (Giant hogweed)",
            "fr": "Semence de mauvaises herbe : Heracleum mantegazzianum (Berce du Caucase)"
        },
        "description": {
            "en": "Fact sheet for Giant hogweed (Heracleum mantegazzianum)",
            "fr": "Fiche de renseignements pour Berce du Caucase (Heracleum mantegazzianum)"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Giant hogweed, Heracleum mantegazzianum",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Berce du Caucase, Heracleum mantegazzianum"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants,weeds",
            "fr": "cultures,grain,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-11-06",
            "fr": "2014-11-06"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Heracleum mantegazzianum (Giant hogweed)",
        "fr": "Semence de mauvaises herbe : Heracleum mantegazzianum (Berce du Caucase)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Giant hogweed</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Newfoundland and Labrador\">NL</abbr>, <abbr title=\"Nova Scotia\">NS</abbr> (Page et al. 2006<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n\n<p><strong>Worldwide:</strong> Native to the Caucasus region of Asia (Georgia, Russia) and introduced in Europe, North America, Australia and New Zealand (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of Life Cycle</h2>\n<p>Monocarpic perennial (dies after flowering)</p>\n\n<h2>Seed or fruit type</h2>\n<p>Schizocarp, divided into 2 mericarps</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Mericarp length: 6.0 - 18.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Mericarp width: 4.0 - 10.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Long oval-shaped mericarp, flattened</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Mericarp smooth, dull</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Mericarp straw yellow with reddish oil ducts</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Teeth along the edge, especially near the top of the mericarp</li>\n<li>The oil ducts extend 3/4 down the mericarp and have enlarged ends</li>\n<li>The ventral side of the style remnant at the top of the mericarp is M-shaped</li>\n</ul>\n\n<h2>Habitat and Crop Association</h2>\n<p>Fields, gardens, pastures, grasslands, open forests, forest edges, forested floodplains, shorelines, ditches, landfills, gravel bars, roadsides and urban parks (Darbyshire 2003<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, Page et al. 2006<sup id=\"fn1b-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). Not a common agricultural weed (Page et al. 2006<sup id=\"fn1c-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Giant hogweed was planted in European botanical gardens of the early 19th century as an ornamental and was first recorded in Canada in 1949 in Ontario (Page et al. 2006<sup id=\"fn1d-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). It favours regions with cool, moist climates and soils with high organic matter (Page et al. 2006<sup id=\"fn1e-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>Similar Species</h2>\n\n<h3>Cow parsnip (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>)</h3>\n<ul>\n<li>Cow parsnip mericarps are similar size, oval shape, reddish oil ducts and straw yellow colour as giant hogweed.</li>\n\n<li>Cow parsnip mericarps have an arrow shape on the ventral side of the style remnant, no teeth on the margin, and have thinner oil ducts compared to giant hogweed.</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img3_1405078107504_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>) mericarps\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img1_1405078049942_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>) mericarp, outer side\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img2_1405078082020_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>) mericarp, inner side\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img4_1508341478488_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>) teeth along the edge of mericarp, especially near the top of the fruit\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_sphondylium_img3_1405078248880_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Cow parsnip (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>) mericarps\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_sphondylium_img1_1405078162349_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Cow parsnip (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>) mericarp, outer side\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_sphondylium_img2_1405078195036_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Cow parsnip (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>) mericarp, inner side\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Page, N. A., Wall, R. E., Darbyshire, S. J. and Mulligan, G. A. 2006.</strong> The Biology of Invasive Alien Plants in Canada. 4. <i lang=\"la\">Heracleum mantegazzianum</i> Sommier &amp; Levier. Canadian Journal of Plant Science 86: 569\u2013589.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Berce du Caucase</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente en <abbr title=\"Colombie-Britannique\">C.-B</abbr>., en <abbr title=\"Ontario\">Ont.</abbr>, au <abbr title=\"Qu\u00e9bec\">Qc</abbr>, au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr>, \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr> et en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr> (Page et al., 2006<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de la r\u00e9gion du Caucase en Asie (G\u00e9orgie, Russie) et introduite en Europe, en Am\u00e9rique du Nord, en Australie et en Nouvelle-Z\u00e9lande (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace monocarpique (elle meurt apr\u00e8s la floraison)</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Schizocarpe, divis\u00e9 en deux m\u00e9ricarpes</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur du m\u00e9ricarpe : 6,0 \u00e0 18,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li> \n<li>Largeur du m\u00e9ricarpe : 4,0 \u00e0 10,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe elliptique allong\u00e9 et aplati</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe lisse et mat</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe jaune paille avec des canaux ol\u00e9if\u00e8res rouge\u00e2tres</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Marge dent\u00e9e, en particulier pr\u00e8s du sommet du m\u00e9ricarpe</li>\n<li>Les canaux ol\u00e9if\u00e8res s\u2019\u00e9tendent sur les trois quarts sup\u00e9rieurs du m\u00e9ricarpe et ont une base dilat\u00e9e</li>\n<li>La face ventrale du vestige de style au sommet du m\u00e9ricarpe est en forme de M</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n\n<p>Champs, jardins, p\u00e2turages, prairies, for\u00eats clairsem\u00e9es, bordures de for\u00eats, plaines inondables bois\u00e9es, rivages, foss\u00e9s, sites d'enfouissement, bancs de gravier, bords de chemin et parcs urbains (Darbyshire, 2003<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; Page et al., 2006<sup id=\"fn1b-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>). Ce n\u2019est pas une mauvaise herbe commune en agriculture (Page et al., 2006<sup id=\"fn1c-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n\n<p>La berce du Caucase a \u00e9t\u00e9 cultiv\u00e9e comme plante ornementale dans les jardins botaniques europ\u00e9ens au d\u00e9but du 19e si\u00e8cle et a \u00e9t\u00e9 signal\u00e9e pour la premi\u00e8re fois au Canada en 1949 en Ontario (Page et al., 2006<sup id=\"fn1d-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>). Elle pr\u00e9f\u00e8re les r\u00e9gions \u00e0 climat frais et humide et les sols riches en mati\u00e8re organique (Page et al., 2006<sup id=\"fn1e-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n\n<h3>Berce laineuse (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>)</h3>\n<ul>\n<li>Les m\u00e9ricarpes de la berce laineuse ressemblent \u00e0 ceux de la berce du Caucase par leurs dimensions, leur forme elliptique, leur couleur jaune paille et leurs canaux ol\u00e9if\u00e8res rouge\u00e2tres.</li>\n\n<li>Les m\u00e9ricarpes de la berce laineuse sont sagitt\u00e9s sur la face ventrale du vestige stylaire, leur marge n\u2019est pas dent\u00e9e et ils pr\u00e9sentent des canaux ol\u00e9if\u00e8res plus minces que ceux de la berce du Caucase.</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img3_1405078107504_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>) m\u00e9ricarpes\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img1_1405078049942_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>)  m\u00e9ricarpe, la face ext\u00e9rieure\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img2_1405078082020_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>) m\u00e9ricarpe, la face int\u00e9rieure\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_img4_1508341478488_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>) dents visibles sur la marge du m\u00e9ricarpe, particuli\u00e8rement pr\u00e8s du sommet \n</figcaption>\n</figure>\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_sphondylium_img3_1405078248880_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Berce laineuse (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>) m\u00e9ricarpes\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_sphondylium_img1_1405078162349_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Berce laineuse (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>) m\u00e9ricarpe, la face ext\u00e9rieure\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_heracleum_sphondylium_img2_1405078195036_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Berce laineuse (<i lang=\"la\">Heracleum sphondylium</i> subsp. <i lang=\"la\">montanum</i>) m\u00e9ricarpe, la face int\u00e9rieure\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Page, N. A., Wall, R. E., Darbyshire, S. J. and Mulligan, G. A. 2006.</strong> The Biology of Invasive Alien Plants in Canada. 4. <i lang=\"la\">Heracleum mantegazzianum</i> Sommier &amp; Levier. Canadian Journal of Plant Science 86: 569\u2013589.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}