<?php

//[0-90-90-90-9]

  // Fonction pour valider et corriger les dates
  function validateAndCorrectDate($dateString) {
    // Convertir la date en tableau avec explode
    $dateParts = explode('-', $dateString);
    // Vérifier s'il y a exactement 3 parties dans le tableau
    if (count($dateParts) !== 3) {
        return false; // Si ce n'est pas le cas, la date est invalide
    }
    // Extraire les parties de la date
    $year = intval($dateParts[0]);
    $month = intval($dateParts[1]);
    $day = intval($dateParts[2]);
    // Vérifier si la date est valide
    if (!checkdate($month, $day, $year)) {
      $mois = 1;
      $jour = 1;
      if (($month > 12 && $month < 31) && $day <= 12) {
        $mois = $day;
        $jour = $month; 
      }
      if ($year < 1970) {
        $year = 1970;
      }
      if ($year > 2038) {
        $year = 2038;
      }
      return sprintf('%04d-%02d-%02d', $year, $mois, $jour);
    }
    // Si la date est valide, la reformater au format 'YYYY-MM-DD'
    return sprintf('%04d-%02d-%02d', $year, $month, $day);
  }
  // Exemple de date invalide
  $dateString = "2018-14-12";
  // Valider et corriger la date
  $correctedDate = validateAndCorrectDate($dateString);
  // Vérifier si la date a été corrigée avec succès
  if ($correctedDate !== false) {
    echo "Date corrigée : $correctedDate";
  } else {
    echo "La date est invalide.";
  }

