{
    "dcr_id": "1405080697802",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Linaria vulgaris</i> (Common toadflax/Butter-and-eggs)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria vulgaris</i> (Linaire vulgaire)"
    },
    "html_modified": "2024-03-12 3:56:47 PM",
    "modified": "2017-11-01",
    "issued": "2014-11-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_linaria_vulgaris_1405080697802_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_linaria_vulgaris_1405080697802_fra"
    },
    "ia_id": "1405080698646",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Linaria vulgaris</i> (Common toadflax/Butter-and-eggs)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria vulgaris</i> (Linaire vulgaire)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Linaria vulgaris</i> (Common toadflax/Butter-and-eggs)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria vulgaris</i> (Linaire vulgaire)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1405080698646",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/linaria-vulgaris/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/linaria-vulgaris/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Linaria vulgaris (Common toadflax/Butter-and-eggs)",
            "fr": "Semence de mauvaises herbe : Linaria vulgaris (Linaire vulgaire)"
        },
        "description": {
            "en": "Fact sheet for Common toadflax/Butter-and-eggs (Linaria vulgaris)",
            "fr": "Fiche de renseignements pour Linaire vulgaire / linaire commune (Linaria vulgaris)"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Common toadflax, Butter-and-eggs, Linaria vulgaris",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Linaire vulgaire, linaire commune, Linaria vulgaris"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants,weeds",
            "fr": "cultures,grain,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-11-06",
            "fr": "2014-11-06"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Linaria vulgaris (Common toadflax/Butter-and-eggs)",
        "fr": "Semence de mauvaises herbe : Linaria vulgaris (Linaire vulgaire)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Plantaginaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Yellow toadflax</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs across Canada except in <abbr title=\"Nunavut\">NU</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to Europe and temperate Asia (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>) and introduced in North America, Chile, South Africa, Australia and New Zealand (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 1.7 - 2.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 1.5 - 2.3 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Seed round to oval with a wing around the outside; flattened</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>The centre of the seed is covered with tubercles</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed black with bronze highlights</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>A thin wing surrounds the seed, 0.3 - 1.0 <abbr title=\"millimetres\">mm</abbr> wide, minutely reticulated</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, old fields, pastures, rangelands, shores, roadsides and disturbed areas (Saner et al. 1995<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). In Canada, it is a problem in pastures and crops such as wheat, barley, oats, canola and peas (Saner et al. 1995<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Yellow toadflax was once grown as an ornamental, dye and medicinal plant (Saner et al. 1995<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). This species first appeared in Ontario and Quebec at the end of the 19th century and moved into the Prairie Provinces after 1920, through contaminated alfalfa seed and as a garden plant (Saner et al. 1995<sup id=\"fn4d-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). Yellow toadflax is a prolific seed producer and reproduces from both seed and clonal propagation (Saner et al. 1995<sup id=\"fn4e-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Melancholy toadflax (<i lang=\"la\">Linaria tristis</i>)</h3>\n<ul>\n<li>This species has rounded, flattened seeds that are a similar size and colour to yellow toadflax.</li>\n\n<li>Melancholy toadflax may be smooth or have a few tubercles like yellow toadflax, but only on one side of the seed.</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_vulgaris_img2_1405078397709_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common toadflax (<i lang=\"la\">Linaria vulgaris</i>) seeds\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_vulgaris_img1_1405078359397_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common toadflax (<i lang=\"la\">Linaria vulgaris</i>) seed\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_vulgaris_img3_1509564765061_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common toadflax (<i lang=\"la\">Linaria vulgaris</i>) seed\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_tristis_img1_1405078501226_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Melancholy toadflax (<i lang=\"la\">Linaria tristis</i>) seeds</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_tristis_img2_1509564785433_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Melancholy toadflax (<i lang=\"la\">Linaria tristis</i>) seed</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Saner, M. A., Clements, D. A., Hall, M. R., Dohan, D. J. and Crompton, C. W. 1995.</strong> The biology of Canadian weeds. 105. Linaria vulgaris Mill. Canadian Journal of Plant Science 75: 525-537.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Plantaginaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Linaire vulgaire</p> \n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> Pr\u00e9sente partout au Canada, sauf au <abbr title=\"Nunavut\">Nt</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>).\n</p>\n\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Indig\u00e8ne d'Europe et de l'Asie temp\u00e9r\u00e9e (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>) et introduite en Am\u00e9rique du Nord, au Chili, en Afrique du Sud, en Australie et en Nouvelle-Z\u00e9lande (CABI, 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 1,7 \u00e0 2,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 1,5 \u00e0 2,3 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine de forme ronde \u00e0 elliptique, entour\u00e9e d'une aile; aplatie</li> \n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Le centre de la graine est recouvert de tubercules</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine noire avec des reflets bronz\u00e9s</li>\n</ul>\n<h3>Autres structures</h3>\n<ul>\n<li>Une mince aile de 0,3 \u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr> de largeur et finement r\u00e9ticul\u00e9e entoure la graine</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n\n<p>Champs cultiv\u00e9s, champs abandonn\u00e9s, p\u00e2turages, parcours, rivages, bords de chemin et terrains perturb\u00e9s (Saner et al., 1995<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>; Darbyshire, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>). Au Canada, elle est probl\u00e9matique dans les p\u00e2turages et des cultures telles que le bl\u00e9, l'orge, l'avoine, le canola et les pois (Saner et al., 1995<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>; CABI, 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n\n<p>La linaire vulgaire fut jadis cultiv\u00e9e comme plante ornementale, tinctoriale et m\u00e9dicinale (Saner et al., 1995<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>). Cette esp\u00e8ce a fait son apparition en Ontario et au Qu\u00e9bec \u00e0 la fin du 19e si\u00e8cle et elle a \u00e9t\u00e9 propag\u00e9e dans les Prairies apr\u00e8s 1920 par de la semence de luzerne contamin\u00e9e et par son utilisation comme plante de jardin (Saner et al., 1995<sup id=\"fn4d-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>). La linaire vulgaire est prolifique en graines et se multiplie \u00e0 la fois par graines et par propagation v\u00e9g\u00e9tative (Saner et al., 1995<sup id=\"fn4e-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n\n<h3>Linaire triste (<i lang=\"la\">Linaria trisitis</i>)</h3>\n<ul>\n<li>La linaire triste a des graines arrondies et aplaties qui sont de dimensions et de couleur semblables \u00e0 celles de la linaire vulgaire.</li>\n\n<li>La graine de la linaire triste peut \u00eatre lisse ou \u00eatre orn\u00e9e de quelques tubercules comme la linaire vulgaire, mais d\u2019un seul c\u00f4t\u00e9 de la graine.</li>\n</ul>\n\n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_vulgaris_img2_1405078397709_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire vulgaire (<i lang=\"la\">Linaria vulgaris</i>) graines\n</figcaption>\n</figure>\n\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_vulgaris_img1_1405078359397_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire vulgaire (<i lang=\"la\">Linaria vulgaris</i>) graine\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_vulgaris_img3_1509564765061_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire vulgaire (<i lang=\"la\">Linaria vulgaris</i>) graine\n</figcaption>\n</figure>\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_tristis_img1_1405078501226_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: <i lang=\"la\">Linaria tristis</i> graines</figcaption>\n</figure>\n\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/WORKAREA/DAM-plants-vegetaux/images-images/invasive_plants_seed_factsheet_linaria_tristis_img2_1509564785433_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: <i lang=\"la\">Linaria tristis</i> graine</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Saner, M. A., Clements, D. A., Hall, M. R., Dohan, D. J. and Crompton, C. W. 1995.</strong> The biology of Canadian weeds. 105. Linaria vulgaris Mill. Canadian Journal of Plant Science 75: 525-537.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}