{
    "dcr_id": "1372183019382",
    "title": {
        "en": "Pet food: animal health certificates for export",
        "fr": "Aliments pour animaux de compagnie : certificats de sant\u00e9 animale pour l'exportation"
    },
    "html_modified": "2024-01-26 2:21:59 PM",
    "modified": "2023-12-20",
    "issued": "2015-03-26 08:23:10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_export_certs_petfood_1372183019382_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_export_certs_petfood_1372183019382_fra"
    },
    "ia_id": "1372183063848",
    "parent_ia_id": "1300388985791",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1300388985791",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pet food: animal health certificates for export",
        "fr": "Aliments pour animaux de compagnie : certificats de sant\u00e9 animale pour l'exportation"
    },
    "label": {
        "en": "Pet food: animal health certificates for export",
        "fr": "Aliments pour animaux de compagnie : certificats de sant\u00e9 animale pour l'exportation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1372183063848",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300388920375",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/pet-food/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pet food: animal health certificates for export",
            "fr": "Aliments pour animaux de compagnie : certificats de sant\u00e9 animale pour l'exportation"
        },
        "description": {
            "en": "Valid export certificates for pet food",
            "fr": "Certificats d'exportation valides pour les aliments pour animaux de compagnie"
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, animal health, exports, animal products, animal by-products, pet food, certificates, sanitary requirements",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, sant\u00e9 des animaux, exportation, produits d'animaux, sous-produits d'animaux, aliments pour animaux de compagnie, certificats, exigences sanitaires"
        },
        "dcterms.subject": {
            "en": "exports,inspection,veterinary medicine,regulation,animal health",
            "fr": "exportation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-26 08:23:10",
            "fr": "2015-03-26 08:23:10"
        },
        "modified": {
            "en": "2023-12-20",
            "fr": "2023-12-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pet food: animal health certificates for export",
        "fr": "Aliments pour animaux de compagnie : certificats de sant\u00e9 animale pour l'exportation"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Certificates may be in English and/or another language without a corresponding French version. The chosen language(s) is determined by the requirements of importing countries for their understanding.</p>\n<p>These certificates are for information purposes only. If you are planning an export, please contact the <a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a> in your area to verify that the certificate is valid, that it applies to your export, and that there are no disease outbreaks or other events that may cause exports to be suspended.</p>\n<p>If you have difficulty obtaining any of these forms please contact the <a href=\"/eng/1300462382369/1300462438912\">Animal Health Office</a> in your area.</p> \n\n<div class=\"btn-group btn-group-justified\">\n<a class=\"btn btn-default\" role=\"button\" href=\"#lista\">A</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listb\">B</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listc\">C</a>\n<a role=\"button\" class=\"btn btn-default disabled\">D</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#liste\">E</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listf\">F</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listg\">G</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listh\">H</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listi\">I</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listj\">J</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listk\">K</a>\n<a class=\"btn btn-default disabled\" role=\"button\">L</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listm\">M</a>\n</div>\n\n\n<div class=\"btn-group btn-group-justified\">\n<a class=\"btn btn-default\" role=\"button\" href=\"#listn\">N</a>\n<a class=\"btn btn-default disabled\" role=\"button\">O</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listp\">P</a>\n<a class=\"btn btn-default disabled\" role=\"button\">Q</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listr\">R</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#lists\">S</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listt\">T</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listu\">U</a>\n<a class=\"btn btn-default disabled\" role=\"button\">V</a>\n<a class=\"btn btn-default disabled\" role=\"button\">W</a>\n<a class=\"btn btn-default disabled\" role=\"button\">X</a>\n<a class=\"btn btn-default disabled\" role=\"button\">Y</a>\n<a class=\"btn btn-default disabled\" role=\"button\">Z</a>\n</div>\n\n<h2 id=\"lista\">A</h2>\n<h3>Algeria</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Argentina</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Azerbaijan</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food/treats/chews<br>\n</li>\n</ul>\n\n<h2 id=\"listb\">B</h2>\n<h3>Barbados</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Brazil</h3>\n<ul class=\"lst-spcd\">\n<li>Animal feed not containing animal by-products<br>\n</li>\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"listc\">C</h2>\n<h3>Chile</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>China</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food other than canned<br>\n</li>\n</ul>\n<h3>Colombia</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food (non-ruminant)<br>\n</li>\n<li>Pet food (ruminant)<br>\n</li>\n</ul>\n<h3>Costa Rica</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"liste\">E</h2>\n<h3>Egypt</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>El Salvador</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>European union</h3>\n<p>Certification for direct export to the European Union (EU) must be done using the Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>).</p>\n<p>However, in case a commodity is first exported to a third country before being exported to the EU (for example, pet food exported to the USA for further export to EU; animal products exported to the USA for further processing into pet food to be exported to EU), additional certification attesting for the EU requirements is required, so please contact the\u00a0<a href=\"https://inspection.canada.ca/eng/1300462382369/1300462438912\">Animal Health Office Office</a>\u00a0in your area.</p>\n\n<h2 id=\"listf\">F</h2>\n<h3>French Polynesia</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n\n<h2 id=\"listg\">G</h2>\n\n<h3>Georgia</h3>\n<ul class=\"lst-spcd\">\n<li>Canned pet food<br>\n</li>\n\n<li>Pet food other than canned<br>\nHA3166 (2021-10-01) \n</li>\n</ul>\n\n<h3>Great Britain, Channel Islands and Isle of Man</h3>\n<ul class=\"lst-spcd\">\n<li>Animal by-products for the manufacture of pet food<br>HA3099 (Amended 2023-04-13)</li>\n<li>Canned pet food<br>HA3101 (Amended 2023-04-13)</li>\n<li>Flavouring innards for use in the manufacture of pet food<br>HA3098 (Amended 2023-04-13)</li>\n<li>Pet chews<br>HA3102 (Amended 2023-04-13)</li>\n<li>Pet food other than canned<br>HA3100 (Amended 2023-04-13)</li>\n<li>Raw pet food<br>HA3103 (Amended 2023-04-13)</li>\n</ul>\n\n<h3>Guatemala</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food (non-ruminant)<br>\n</li>\n<li>Pet food (ruminant)<br>\n</li>\n</ul>\n<h2 id=\"listh\">H</h2>\n\n<h3>Honduras</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n\n<h2 id=\"listi\">I</h2>\n\n<h3>Iceland</h3>\n<p>Iceland has aligned import conditions with those of the European Union (EU) and accepts certification through the EU electronic certification system, Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>). Certificates can be accessed through TRACES-NT.</p>\n<p>Please refer to the <a href=\"https://merlin.cfia-acia.inspection.gc.ca/eng/1490909809836/1490909809837#european\">European Union (accessible only on the Government of Canada network)</a> section for more detailed information.</p> \n \n<h3>India</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Indonesia</h3>\n<ul class=\"lst-spcd\">\n<li>Bird feed<br>\n</li>\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Israel</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"listj\">J</h2>\n<h3>Japan</h3>\n<ul class=\"lst-spcd\">\n<li><a href=\"/animal-health/terrestrial-animals/exports/pet-food/japan-pet-foods-treats-chews/eng/1474898485646/1474898486208\" title=\"Sanitary Requirements for Export of Pet food to Jordan HA2752 (September 5, 2013)\">Extruded or hermetically sealed pet foods, treats and chews containing or not animal products or animal by-products</a></li>\n<li>Pet food derived from poultry\n<br>HA3068 (2022-02-07)</li>\n</ul>\n<h3>Jordan</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"listk\">K</h2>\n<h3>Korea</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"listm\">M</h2>\n<h3>Malaysia</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Mexico</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food and treats<br>\n</li>\n<li>Pet food not containing animal ingredients<br>\n</li>\n</ul>\n<h2 id=\"listn\">N</h2>\n<h3>New Zealand</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food (non-ruminant)<br>\n</li>\n<li class=\"mrgn-tp-md\">Pet food containing bovine material<br>\n</li>\n</ul>\n<h3>Nicaragua</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n  \n<h3>Norway</h3>\n<p>Norway has aligned import conditions with those of the European Union (EU) and accepts certification through the EU electronic certification system, Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>). Certificates can be accessed through TRACES-NT.</p>\n<p>Please refer to the <a href=\"https://merlin.cfia-acia.inspection.gc.ca/eng/1490909809836/1490909809837#european\">European Union (accessible only on the Government of Canada network)</a> section for more detailed information.</p>   \n  \n<h2 id=\"listp\">P</h2>\n<h3>Peru</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food <br>\n</li>\n</ul>\n<h3>Phillipines</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"listr\">R</h2>\n<h3>Russia</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h2 id=\"lists\">S</h2>\n<h3>Saudi Arabia</h3>\n<ul class=\"lst-spcd\">\n\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Singapore</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>South Africa</h3>\n<ul class=\"lst-spcd\">\n<li>Pet chews<br>\n</li>\n<li>Pet food and pet treats<br>\n</li>\n</ul>\n<h3>Switzerland</h3>\n\n<p>Switzerland has aligned import conditions with those of the European Union (EU) and accepts certification through the EU electronic certification system, Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>). Certificates can be accessed through TRACES-NT.</p>\n<p>Please refer to the <a href=\"https://merlin.cfia-acia.inspection.gc.ca/eng/1490909809836/1490909809837#european\">European Union (accessible only on the Government of Canada network)</a> section for more detailed information.</p>\n  \n<h2 id=\"listt\">T</h2>\n<h3>Taiwan</h3>\n<ul class=\"lst-spcd\">\n<li>Pet chews<br>\n</li>\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Thailand</h3>\n\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>Trinidad and Tobago</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>T\u00fcrkiye</h3>\n<ul class=\"lst-spcd\">\n<li>Animal by products intended for manufacture of pet food<br>\n</li>\n<li>Canned pet food<br>\n</li>\n<li>Flavouring innards for use in the manufacture of pet food<br>\n</li>\n<li>Pet chews<br>\n</li>\n<li>Pet food other than canned<br>\n</li>\n</ul>\n<h2 id=\"listu\">U</h2>\n<h3>Ukraine</h3>\n<ul class=\"lst-spcd\">\n<li>Canned pet food <br>\n</li>\n<li>Dry pet food<br>\n</li>\n</ul>\n<h3>United Arab Emirates</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food<br>\n</li>\n</ul>\n<h3>United States</h3>\n<ul class=\"lst-spcd\">\n<li>Pet food, chews and treats<br>\n</li>\n<li>Raw pet food and treats<br>\n</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les certificats peuvent \u00eatre en anglais et (ou) dans une autre langue, sans version fran\u00e7aise correspondante. La ou les langues choisies sont d\u00e9termin\u00e9es par les exigences des pays importateurs pour leur compr\u00e9hension.</p>\n<p>Ces certificats ne servent qu'\u00e0 des fins d'information. Si vous pr\u00e9voyez une exportation, veuillez vous adresser au <a href=\"/fra/1300462382369/1300462438912\">bureau de sant\u00e9 des animaux</a> de votre r\u00e9gion afin de confirmer que le certificat est valide, qu'il s'applique \u00e0 votre exportation, et qu'il n'y a pas d'\u00e9pid\u00e9mie de maladie ou autre ph\u00e9nom\u00e8ne qui pourrait entra\u00eener la suspension des exportations.</p>\n<p>Si vous avez de la difficult\u00e9 \u00e0 obtenir un de ces formulaires, veuillez vous adresser au <a href=\"/fra/1300462382369/1300462438912\">bureau de la sant\u00e9 des animaux</a> de votre r\u00e9gion.</p> \n\n<div class=\"btn-group btn-group-justified\">\n<a class=\"btn btn-default\" role=\"button\" href=\"#lista\">A</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listb\">B</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listc\">C</a>\n<a role=\"button\" class=\"btn btn-default disabled\">D</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#liste\">E</a>\n<a class=\"btn btn-default disabled\" role=\"button\">F</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listg\">G</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listh\">H</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listi\">I</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listj\">J</a>\n<a class=\"btn btn-default disabled\" role=\"button\">K</a>\n<a class=\"btn btn-default disabled\" role=\"button\">L</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listm\">M</a>\n</div>\n\n<div class=\"btn-group btn-group-justified\">\n<a class=\"btn btn-default\" role=\"button\" href=\"#listn\">N</a>\n<a class=\"btn btn-default disabled\" role=\"button\">O</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listp\">P</a>\n<a class=\"btn btn-default disabled\" role=\"button\">Q</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listr\">R</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#lists\">S</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listt\">T</a>\n<a class=\"btn btn-default\" role=\"button\" href=\"#listu\">U</a>\n<a class=\"btn btn-default disabled\" role=\"button\">V</a>\n<a class=\"btn btn-default disabled\" role=\"button\">W</a>\n<a class=\"btn btn-default disabled\" role=\"button\">X</a>\n<a class=\"btn btn-default disabled\" role=\"button\">Y</a>\n<a class=\"btn btn-default disabled\" role=\"button\">Z</a>\n</div>\n\n<h2 id=\"lista\">A</h2>\n<h3>Afrique du Sud</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments et g\u00e2teries pour animaux de compagnie<br>\n</li>\n<li>Articles \u00e0 mastiquer pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Alg\u00e9rie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Arabie Saoudite</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Argentine</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Azerba\u00efdjan</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments/g\u00e2teries/articles \u00e0 mastiquer pour animaux de compagnie<br>\n</li>\n</ul>\n\n<h2 id=\"listb\">B</h2>\n<h3>Barbade</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Br\u00e9sil</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n<li>Aliments pour animaux ne contenant pas de sous-produits animaux<br>\n</li>\n</ul>\n<h2 id=\"listc\">C</h2>\n<h3>Chili</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Chine</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie autres qu'en conserve<br>\n</li>\n</ul>\n<h3>Colombie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie (non-ruminant)<br>\n\n</li>\n<li>Aliments pour animaux de compagnie (ruminant)<br>\n</li>\n</ul>\n<h3>Cor\u00e9e</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Costa Rica</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h2 id=\"liste\">E</h2>\n<h3>\u00c9gypte</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>El Salvador</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>\u00c9mirats Arabes Unis</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>\u00c9tats-Unis</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments, articles \u00e0 mastiquer et g\u00e2teries pour animaux de compagnie<br>\n</li>\n<li>Aliments et g\u00e2teries crus pour animaux de compagnie<br>\nHA3025 (modifi\u00e9 2022-01-03)\n</li>\n</ul>\n\n<h2 id=\"listg\">G</h2>\n\n<h3>G\u00e9orgie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments en conserve pour animaux de compagnie<br>\nHA3001 (modifi\u00e9 2021-10-01)\n</li>\n\n<li>Aliments pour animaux de compagnie autres qu'en conserve<br>\n</li>\n</ul>\n\n<h3>Grande Bretagne, les \u00eeles Anglo-Normandes et l'\u00eele de Man</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments crus pour animaux de compagnie<br>HA3103 (modifi\u00e9 2023-04-13)</li>\n<li>Aliments en conserve pour animaux de compagnie<br>HA3101 (modifi\u00e9 2023-04-13)</li>\n<li>Aliments pour animaux de compagnie autres qu'en conserve<br>HA3100 (modifi\u00e9 2023-04-13)</li>\n<li>Articles \u00e0 mastiquer pour animaux de compagnie<br>HA3102 (modifi\u00e9 2023-04-13)</li>\n<li>Sous-produits animaux pour la fabrication d'aliments pour animaux de compagnie<br>HA3099 (modifi\u00e9 2023-04-13)</li>\n<li>Visc\u00e8res aromatiques pour la fabrication d'aliments pour animaux de compagnie<br>HA3098 (modifi\u00e9 2023-04-13)</li>\n</ul>\n\n<h3>Guatemala</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie (non-ruminant)<br>\n</li>\n<li>Aliments pour animaux de compagnie (ruminant)<br>\n</li>\n</ul>\n<h2 id=\"listh\">H</h2>\n\n<h3>Honduras</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n\n<h2 id=\"listi\">I</h2>\n<h3>Inde</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Indon\u00e9sie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n<li>Aliments pour oiseaux<br>\n</li>\n</ul>\n \n<h3>Islande</h3>\n<p>L'Islande a align\u00e9 ses conditions d'importation sur celles de l'Union europ\u00e9enne (UE) et accepte la certification par le biais du syst\u00e8me de certification \u00e9lectronique de l'UE,\u00a0<span lang=\"en\">Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>)</span>. Les certificats d'exportation sont accessibles par l'interm\u00e9diaire du syst\u00e8me TRACES-NT.</p>\n<p>Veuillez-vous r\u00e9f\u00e9rer \u00e0 la section <a href=\"https://merlin.cfia-acia.inspection.gc.ca/fra/1490909809836/1490909809837#europ%C3%A9enne\">Union europ\u00e9enne (accessible uniquement sur le r\u00e9seau du gouvernement du Canada)</a> pour des informations plus d\u00e9taill\u00e9es.</p>\n<h3>Isra\u00ebl</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h2 id=\"listj\">J</h2>\n<h3>Japon</h3>\n<ul class=\"lst-spcd\">\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/japon-aliments-articles-a-mastiquer-gateries/fra/1474898485646/1474898486208\">Aliments, articles \u00e0 mastiquer et G\u00e2teries pour animaux de compagnie obtenus par extrusion ou en conserve, contenant ou non des produits ou sous-produits d'origine animale</a></li>\n<li>Aliments pour animaux de compagnie fabriqu\u00e9s \u00e0 partir de volaille\n<br>HA3068 (2022-02-07)</li>\n</ul>\n<h3>Jordanie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h2 id=\"listm\">M</h2>\n<h3>Malaisie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Mexique</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments et g\u00e2teries pour animaux de compagnie<br>\n</li>\n<li>Aliments pour animaux de compagnie ne contenant pas d'ingr\u00e9dients d'origine animale<br>\n</li>\n</ul>\n<h2 id=\"listn\">N</h2>\n<h3>Nicaragua</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n\n<h3>Norv\u00e8ge</h3>\n<p>La Norv\u00e8ge a align\u00e9 ses conditions d'importation sur celles de l'Union europ\u00e9enne (UE) et accepte la certification par le biais du syst\u00e8me de certification \u00e9lectronique de l'UE,\u00a0<span lang=\"en\">Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>)</span>. Les certificats d'exportation sont accessibles par l'interm\u00e9diaire du syst\u00e8me TRACES-NT.</p>\n<p>Veuillez-vous r\u00e9f\u00e9rer \u00e0 la section <a href=\"https://merlin.cfia-acia.inspection.gc.ca/fra/1490909809836/1490909809837#europ%C3%A9enne\">Union europ\u00e9enne (accessible uniquement sur le r\u00e9seau du gouvernement du Canada)</a> pour des informations plus d\u00e9taill\u00e9es.</p> \n \n<h3>Nouvelle Z\u00e9lande</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie (non-ruminant)<br>\n</li>\n<li class=\"mrgn-tp-md\">Aliments pour animaux de compagnie contenant de la mati\u00e8re bovine<br>\n</li>\n</ul>\n<h2 id=\"listp\">P</h2>\n<h3>P\u00e9rou</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie <br>\n</li>\n</ul>\n<h3>Philippines</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Polyn\u00e9sie Fran\u00e7aise</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h2 id=\"listr\">R</h2>\n<h3>Russie</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h2 id=\"lists\">S</h2>\n<h3>Singapour</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie <br>\n</li>\n</ul>\n<h3>Suisse</h3>\n<p>La Suisse a align\u00e9 ses conditions d'importation sur celles de l'Union europ\u00e9enne (UE) et accepte la certification par le biais du syst\u00e8me de certification \u00e9lectronique de l'UE,\u00a0<span lang=\"en\">Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>)</span>. Les certificats d'exportation sont accessibles par l'interm\u00e9diaire du syst\u00e8me TRACES-NT.</p>\n<p>Veuillez-vous r\u00e9f\u00e9rer \u00e0 la section <a href=\"https://merlin.cfia-acia.inspection.gc.ca/fra/1490909809836/1490909809837#europ%C3%A9enne\">Union europ\u00e9enne (accessible uniquement sur le r\u00e9seau du gouvernement du Canada)</a> pour des informations plus d\u00e9taill\u00e9es.</p>\n \n<h2 id=\"listt\">T</h2>\n<h3>Ta\u00efwan</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie <br>\n</li>\n\n<li>Articles \u00e0 mastiquer pour animaux de compagnie<br>\n</li>\n \n</ul>\n<h3>Tha\u00eflande</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>Trinit\u00e9-et-Tobago</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h3>T\u00fcrkiye</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments en conserve pour animaux de compagnie<br>\n</li>\n<li>Aliments pour animaux de compagnie autres qu'en conserve<br>\n</li>\n<li>Articles \u00e0 mastiquer<br>\n</li>\n<li>Sous-produits animaux destin\u00e9s \u00e0 la production d'aliments pour animaux de compagnie<br>\n</li>\n<li>Visc\u00e8res aromatiques devant servir \u00e0 la fabrication d'aliments pour animaux de compagnie<br>\n</li>\n</ul>\n<h2 id=\"listu\">U</h2>\n<h3>Ukraine</h3>\n<ul class=\"lst-spcd\">\n<li>Aliments en conserve pour animaux de compagnie<br>\n</li>\n<li>Nourriture s\u00e8che pour animaux de compagnie<br>\n</li>\n</ul>\n\n<h3>Union europ\u00e9enne</h3>\n<p>La certification pour exportation directe en Union europ\u00e9enne (UE) doit \u00eatre faite en utilisant le syst\u00e8me <span lang=\"en\">Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>)</span>.</p>\n<p>Cependant, au cas o\u00f9 ces commodit\u00e9s sont d'abord export\u00e9es vers un pays tiers avant d'\u00eatre export\u00e9es en UE (ex\u00a0: aliments pour animaux de compagnie export\u00e9s aux \u00c9tats-Unis pour exportation subs\u00e9quente en UE; produits animaux export\u00e9s aux \u00c9tats-Unis pour y \u00eatre transform\u00e9s en aliments pour animaux de compagnie) et qu'une certification additionnelle pour les conditions de l'UE est requise, veuillez-vous adresser au\u00a0<a href=\"/fra/1300462382369/1300462438912\">bureau de la sant\u00e9 des animaux</a>\u00a0de votre r\u00e9gion.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}