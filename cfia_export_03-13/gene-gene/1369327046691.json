{
    "dcr_id": "1369327046691",
    "title": {
        "en": "Specified risk material - Blood collection during slaughter",
        "fr": "Mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es - Cueillette du sang durant l'abattage"
    },
    "html_modified": "2024-03-12 3:56:18 PM",
    "modified": "2018-12-27",
    "issued": "2013-05-23 12:37:27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_bse_srm_blood_1369327046691_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_bse_srm_blood_1369327046691_fra"
    },
    "ia_id": "1369327047551",
    "parent_ia_id": "1369288439642",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Diseases",
        "fr": "Maladies"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1369288439642",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Specified risk material - Blood collection during slaughter",
        "fr": "Mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es - Cueillette du sang durant l'abattage"
    },
    "label": {
        "en": "Specified risk material - Blood collection during slaughter",
        "fr": "Mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es - Cueillette du sang durant l'abattage"
    },
    "templatetype": "content page 1 column",
    "node_id": "1369327047551",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1369288439064",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/bovine-spongiform-encephalopathy/srm/abattoirs-meat-processors/blood-collection/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/encephalopathie-spongiforme-bovine/mrs/abattoirs-etablissements-de-transformation-de-vian/cueillette-du-sang/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Specified risk material - Blood collection during slaughter",
            "fr": "Mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es - Cueillette du sang durant l'abattage"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency will continue to allow the exemption of cattle blood from the requirements of the enhanced feed ban, so long as gross contamination of blood with specified risk material (SRM) is prevented.",
            "fr": "L'Agence canadienne d'inspection des aliments continuera de permettre l'exemption du sang de bovins des exigences de l'interdiction am\u00e9lior\u00e9e frappant les aliments du b\u00e9tail en autant qu'on emp\u00eache la forte contamination du sang avec des mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es (MRS)."
        },
        "keywords": {
            "en": "over thirty months, OTM, under thirty months, UTM, Hazard Analysis Critical Control Point, HACCP",
            "fr": "plus de trente mois, PTM, moins de trente mois, MTM, Analyse des dangers et ma&icirc;trise des points critiques, HACCP"
        },
        "dcterms.subject": {
            "en": "inspection",
            "fr": "inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-05-23 12:37:27",
            "fr": "2013-05-23 12:37:27"
        },
        "modified": {
            "en": "2018-12-27",
            "fr": "2018-12-27"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Specified risk material - Blood collection during slaughter",
        "fr": "Mati\u00e8res \u00e0 risque sp\u00e9cifi\u00e9es - Cueillette du sang durant l'abattage"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The CFIA will continue to allow the exemption of cattle blood from being classified as Specified Risk Material (SRM), so long as cross contamination of blood with SRM is prevented.</p>\n<p>The key concern is the prevention of neurological tissue externalized during the stunning process, from falling into the blood collection pit following sticking. The potential for such contamination is associated with the stunning method employed on over thirty month (OTM) cattle. It is the responsibility of the abattoir operator to develop and implement effective controls to prevent occurrence of this contamination.</p>\n<p>Currently, there are four (4) CFIA approved methods to effectively prevent SRM cross contamination of bovine blood intended for use in feeds  and food for animals within Canada:</p>\n<ol>\n<li>Blood collected by open method from age verified under thirty month (UTM) will be considered exempted material if it does not contain blood from <abbr title=\"over thirty months\">OTMs</abbr> (zero tolerance);</li>\n<li>The application of edible grease, tampons or other equivalent devices to seal the stun hole, this method must only be considered after the grossly visible brain material is removed from the face plate by trimming, washing, scraping and/or vacuuming. \n<p>The nasal cavity is considered likely compromised in double stunned or misplaced stunned animals. In such situations the collection of blood is acceptable only if a specific mitigation measure is included in the Preventive Control Plan (PCP). Where the nasal cavity has not been compromised, the CFIA will not require any additional measures to prevent the blood from being contaminated with nasal drip.</p>\n</li>\n<li>Humane stunning using a non-penetrative method (for example, electrical kill stunning, ritual slaughtering without stunning, etc.); </li>\n<li>Closed blood collection method (for example, hollow knife or cannula).</li>\n</ol>\n<p>Any cattle abattoir operator  wishing to utilize an alternate procedure to prevent the risk of SRM  cross-contamination of blood must submit a detailed written protocol to the  CFIA or the competent authority prior to  implementation.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'ACIA continuera de permettre que le sang de bovins soit exempt\u00e9 de la classification mati\u00e8res  \u00e0 risque sp\u00e9cifi\u00e9es (MRS) en  autant que l'on pr\u00e9vienne sa contamination crois\u00e9e.</p>\n<p>  La pr\u00e9occupation principale  est d'\u00e9viter que lors de l'insensibilisation, le tissu neurologique ext\u00e9rioris\u00e9  par le trou d'assommage ne tombe dans la fosse de r\u00e9colte de sang.\u00a0 Cette contamination crois\u00e9e potentielle est  associ\u00e9e \u00e0 la m\u00e9thode d'assommage utilis\u00e9e chez les bovins  de plus de trente mois (PTM).  L'op\u00e9rateur de l'abattoir est responsable de d\u00e9velopper et d'implanter des mesures de  contr\u00f4le efficaces pour  pr\u00e9venir cette contamination.</p>\n<p>  Pr\u00e9sentement au Canada, il y a  quatre (4) m\u00e9thodes efficaces approuv\u00e9es par l'ACIA pour pr\u00e9venir la  contamination crois\u00e9e du sang avec les MRS, lorsque celui-ci est destin\u00e9 \u00e0 la  fabrication des aliments et la nourriture pour les animaux: </p>\n<ol>\n<li>L'utilisation d'un       syst\u00e8me ouvert de r\u00e9colte de sang de bovins de MTM ayant \u00e9t\u00e9 identifi\u00e9s et       regroup\u00e9s selon un plan de d\u00e9termination de l'\u00e2ge, permettra de consid\u00e9rer ce sang comme       mat\u00e9riel exempt\u00e9 en autant qu'il ne contienne aucun sang provenant de bovins de PTM       (tol\u00e9rance z\u00e9ro); </li>\n<li>L'application d'une       graisse comestible, de tampons ou de tout autre dispositif permettant de       sceller le trou d'assommage. Cette       m\u00e9thode ne doit \u00eatre consid\u00e9r\u00e9e, qu'apr\u00e8s l'enl\u00e8vement par parage, grattage et/ou aspiration, du mat\u00e9riel c\u00e9r\u00e9bral expuls\u00e9 et       pr\u00e9sent sur la plaque faciale;\n<p>On consid\u00e8re que la cavit\u00e9 nasale est probablement compromise lors d'un  \u00e9tourdissement double ou mal ex\u00e9cut\u00e9.\u00a0Dans de telles situations, la  r\u00e9colte de sang n'est acceptable que si des mesures sp\u00e9cifiques d'att\u00e9nuation  des risques sont incluses dans le plan de contr\u00f4le pr\u00e9ventif (PCP). Lorsque la  cavit\u00e9 nasale n'a pas \u00e9t\u00e9 compromise, l'ACIA n'exigera aucune mesure  suppl\u00e9mentaire afin de pr\u00e9venir la contamination crois\u00e9e du sang avec  l'\u00e9coulement nasal. </p>\n</li>\n\n\n<li>L'utilisation       d'une m\u00e9thode d'\u00e9tourdissement humanitaire non p\u00e9n\u00e9trante (p. ex.       insensibilisation par m\u00e9thode \u00e9lectrique ou l'abattage rituel sans       \u00e9tourdissement etc) </li>\n<li>L'utilisation       d'une m\u00e9thode ferm\u00e9e de r\u00e9colte du sang (p.ex. \u00e0 l'aide d'un couteau creux       ou d'une canule).</li>\n</ol>\n<p>Tout exploitant d'abattoir de  bovins souhaitant utiliser une autre proc\u00e9dure afin d'\u00e9liminer les risques de  contamination crois\u00e9e du sang par des MRS doit soumettre un protocole de  proc\u00e9dure \u00e9crit, d\u00e9taill\u00e9 aux inspecteurs de l'ACIA ou de l'autorit\u00e9 comp\u00e9tente  avant sa mise en \u0153uvre.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}