{
    "dcr_id": "1686072706592",
    "title": {
        "en": "Environmental assessment",
        "fr": "\u00c9valuation environnementale"
    },
    "html_modified": "2024-02-13 9:56:13 AM",
    "modified": "2023-06-08",
    "issued": "2023-06-08",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/vetbio_guide_environmental_assessment_1686072706592_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/vetbio_guide_environmental_assessment_1686072706592_fra"
    },
    "ia_id": "1686072707406",
    "parent_ia_id": "1320704254070",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1320704254070",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Environmental assessment",
        "fr": "\u00c9valuation environnementale"
    },
    "label": {
        "en": "Environmental assessment",
        "fr": "\u00c9valuation environnementale"
    },
    "templatetype": "content page 1 column",
    "node_id": "1686072707406",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299160285341",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/veterinary-biologics/guidelines-forms/environmental-assessment/",
        "fr": "/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/evaluation-environnementale/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Environmental assessment",
            "fr": "\u00c9valuation environnementale"
        },
        "description": {
            "en": "Identify the veterinary biologic, its indication for use, and intended species.",
            "fr": "Identifier le produit biologique v\u00e9t\u00e9rinaire, son indication d'utilisation et l'esp\u00e8ce vis\u00e9e."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, veterinary biologics, Environmental, assessment",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glements sur la sant\u00e9 des animaux, produits biologiques v\u00e9t\u00e9rinaires, \u00c9valuation, environnementale"
        },
        "dcterms.subject": {
            "en": "animal health,environmental impact assessment,inspection,regulation,standards,veterinary medicine",
            "fr": "sant\u00e9 animale,\u00e9valuation environnementale,inspection,r\u00e9glementation,norme,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-06-08",
            "fr": "2023-06-08"
        },
        "modified": {
            "en": "2023-06-08",
            "fr": "2023-06-08"
        },
        "type": {
            "en": "policy,reference material,standard",
            "fr": "politique,mat\u00e9riel de r\u00e9f\u00e9rence,norme"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Environmental assessment",
        "fr": "\u00c9valuation environnementale"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<h2>Summary</h2>\n\n<p>Identify the veterinary biologic, its indication for use, and intended species.</p>\n\n<h3>1. Introduction</h3>\n\n<ul class=\"list-unstyled\">\n\n<li><p>1.1 Proposed action</p>\n\n<p>Introduce the reason and objectives behind the environmental assessment for a biotechnology-derived veterinary biologic (VB).</p></li>\n\n<li><p>1.2 Background</p>\n\n<p>Provide background information on the infectious disease, affected population, and the proposed action. For use of an unlicensed VB in a confined field trial, describe the study, identify the study location, and summarize the protocol. Append the study protocol.</p>\n</li>\n</ul>\n\n<h3>2. Purpose and need for proposed action</h3>\n\n<ul class=\"list-unstyled\">\n\n<li><p>2.1 Significance</p>\n\n<p>What is the agricultural, animal health, and/or scientific significance of the biotechnology-derived VB?</p></li>\n\n<li><p>2.2 Rationale</p>\n\n<p>Summarize the rationale and criteria followed by the Canadian Centre for Veterinary Biologics to arrive at the final decision.</p>\n</li>\n</ul>\n\n<h3>3. Alternatives</h3>\n\n<p>Discuss the available, alternative VB options and their relative scientific merits. What are the selection criteria and weight given to each alternative option? For proposed confined field trials, what are the implications if permission is not given? Justify the proposed action.</p>\n\n<h3>4. Molecular and biological characteristics of parental and recombinant organisms</h3>\n\n<p>Instructions to the evaluator: For sections\u00a04 through\u00a09 below provide information under each subsection heading for each topic. Use submitted information from the manufacturer's or researcher's licensing dossier and studies, peer-reviewed scientific publications, and expert opinions as the basis for this section. If applicable, reference past environmental assessments for similar biotechnology-derived VB.</p>\n\n<ul class=\"list-unstyled\">\n<li>4.1 Identification, sources, and strains of parental organisms</li>\n<li>4.2 Source, description, and function of foreign genetic material</li>\n<li>4.3 Method of accomplishing genetic modification</li>\n<li>4.4 Genetic and phenotypic stability of vaccine organism</li>\n<li>4.5 Potential for recombination and horizontal gene transfer</li>\n<li>4.6 Host range/specificity, tissue tropism and shed/spread capabilities</li>\n<li>4.7 Comparison of the modified organism to parental properties</li>\n<li>4.8 Route of administration/transmission</li>\n</ul>\n\n<h3>5. Human safety</h3>\n\n<ul class=\"list-unstyled\">\n<li>5.1 Previous safe use</li>\n<li>5.2 Probability of human exposure</li>\n<li>5.3 Possible outcomes of human exposures</li>\n<li>5.4 Pathogenicity of parent microorganisms in humans</li>\n<li>5.5 Effect of gene manipulation on pathogenicity in humans</li>\n<li>5.6 Risk associated with widespread use of the vaccine</li>\n</ul>\n\n<h3>6. Animal safety</h3>\n\n<ul class=\"list-unstyled\">\n<li>6.1 Previous safe use</li>\n<li>6.2 Fate of the vaccine in target and non-target species</li>\n<li>6.3 Potential for shed and/or spread from vaccinate to contact target and non-target animals</li>\n<li>6.4 Reversion to virulence resulting from back passage in animals</li>\n<li>6.5 Effect of overdose in target and potential non-target species</li>\n<li>6.6 The extent of the host range and the degree of mobility of the vector</li>\n<li>6.7 Relative safety when compared to conventional vaccines</li>\n<li>6.8 Safety in pregnant animals and to offspring nursing vaccinated animals</li>\n</ul>\n\n<h3>7. Affected environment</h3>\n\n<ul class=\"list-unstyled\">\n<li>7.1 Extent of release into the environment and identification of study site</li>\n<li>7.2 Persistence of the vector in the environment/cumulative impacts</li>\n<li>7.3 Extent of exposure to non-target species</li>\n<li>7.4 Behaviour of parent microorganisms and vector in non-target species</li>\n<li>7.5 Physical and chemical factors which can affect survival, reproduction and dispersal of the vector</li>\n</ul>\n\n<h3>8. Environmental consequences</h3>\n\n<ul class=\"list-unstyled\">\n\n<li><p>8.1 Risks and benefits</p>\n\n<p>Compare and analyze the potential risks compared to benefits of proposed action, study or licensing of the biotechnology-derived VB.</p></li>\n\n<li>8.2 Relative safety compared to other vaccines</li>\n</ul>\n\n<h3>9. Mitigative measures</h3>\n\n<ul class=\"list-unstyled\">\n<li>9.1 Worker safety</li>\n<li>9.2 Non-worker human safety (hazards to public health and safety)</li>\n<li>9.3 Handling of vaccine</li>\n<li>9.4 Handling vaccinated or exposed animals (shed, spread of vaccine)</li>\n<li>9.5 Environment</li>\n</ul>\n\n<h3>10. Monitoring and record keeping</h3>\n\n<p>Indicate the permission to be issued and summarize specific conditions of VB use and record keeping for manufacturing, production, quality control (QC) testing, storage, distribution, use, and disposal of the VB, as applicable.</p>\n\n<h3>11. Consultation and contacts</h3>\n\n<p>Provide names and contact information for the VB manufacturer, researcher and/or importer, government departments, and topic experts consulted during the preparation of this environmental assessment.</p>\n\n<h3>12. Conclusion and actions</h3>\n\n<p>Summarize the decision and recommendations for use of the biotechnology-derived VB.</p>\n\n<h3>13. References</h3>\n\n<p>List of all references cited or relied upon, including peer-reviewed scientific publications, expert websites, and personal communications.</p>\n\n<h3>14. Appendices</h3>\n\n<ul class=\"list-unstyled\">\n<li>Append tables, figures and maps of study sites, if applicable.</li>\n<li>Append study protocols, final reports, and study deviations, if applicable.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<h2>R\u00e9sum\u00e9</h2>\n\n<p>Identifier le produit biologique v\u00e9t\u00e9rinaire, son indication d'utilisation et l'esp\u00e8ce vis\u00e9e.</p>\n\n<h3>1. Introduction</h3>\n\n<ul class=\"list-unstyled\">\n\n<li><p>1.1 Mesure propos\u00e9e</p>\n\n<p>Pr\u00e9senter la raison et les objectifs de l'\u00e9valuation environnementale pour un produit biologique v\u00e9t\u00e9rinaire (PBV) issu de la biotechnologie.</p></li>\n\n<li><p>1.2 Contexte</p>\n\n<p>Fournir des informations g\u00e9n\u00e9rales sur la maladie infectieuse, la population affect\u00e9e et la mesure propos\u00e9e. Pour la conduite d'un essai sur le terrain dans des conditions de confinement pour un PBV non homologu\u00e9, d\u00e9crire l'\u00e9tude, identifier le lieu de l'\u00e9tude et r\u00e9sumer le protocole. Joindre le protocole d'\u00e9tude.</p>\n</li>\n</ul>\n\n<h3>2. Objet et n\u00e9cessit\u00e9 de la mesure propos\u00e9e</h3>\n\n<ul class=\"list-unstyled\">\n\n<li><p>2.1 Importance</p>\n\n<p>Quelle est l'importance agricole, scientifique et/ou pour la sant\u00e9 animale du PBV issu de la biotechnologie?</p>\n</li>\n\n<li><p>2.2 Justification</p>\n\n<p>R\u00e9sumer la justification et les crit\u00e8res suivis par le Centre canadien des produits biologiques v\u00e9t\u00e9rinaires pour arriver \u00e0 la d\u00e9cision finale.</p></li>\n\n</ul>\n\n<h3>3. Mesures possibles</h3>\n\n<p>Discuter des options alternatives de PBV disponibles ainsi que leurs m\u00e9rites scientifiques relatifs. Quels sont les crit\u00e8res de s\u00e9lection et le poids accord\u00e9 \u00e0 chaque option alternative? Pour les essais sur le terrain dans des conditions de confinement propos\u00e9s, quelles sont les implications si l'autorisation n'est pas accord\u00e9e? Justifier la mesure propos\u00e9e.</p>\n\n<h3>4. Caract\u00e9ristiques mol\u00e9culaires et biologiques des microorganismes parentaux et des microorganismes recombin\u00e9s</h3>\n\n<p>Instructions pour l'\u00e9valuateur\u00a0: Pour les sections\u00a04 \u00e0\u00a09 ci-dessous, fournir des informations sous le titre de la sous-section de chaque sujet. En tant que base pour ces sections, utiliser les informations fournies dans le dossier d'homologation et les \u00e9tudes par le fabricant ou le chercheur, ainsi que les publications scientifiques \u00e9valu\u00e9es par des pairs et les opinions d'experts. Le cas \u00e9ch\u00e9ant, faire r\u00e9f\u00e9rence \u00e0 des \u00e9valuations environnementales ant\u00e9rieures pour des PBV similaires d\u00e9riv\u00e9s de la biotechnologie.</p>\n\n<ul class=\"list-unstyled\">\n<li>4.1 Identification, provenance et souches des organismes parentaux</li>\n<li>4.2 Provenance, description et fonction du mat\u00e9riel g\u00e9n\u00e9tique \u00e9tranger</li>\n<li>4.3 M\u00e9thode utilis\u00e9e pour la modification g\u00e9n\u00e9tique</li>\n<li>4.4 Stabilit\u00e9 g\u00e9n\u00e9tique et ph\u00e9notypique de l'organisme vaccinal</li>\n<li>4.5 Possibilit\u00e9s de recombinaison et transfert horizontal de g\u00e8nes</li>\n<li>4.6 Gamme d'h\u00f4tes et sp\u00e9cificit\u00e9, tropisme tissulaire et capacit\u00e9 de propagation et d'excr\u00e9tion</li>\n<li>4.7 Comparaison des propri\u00e9t\u00e9s de l'organisme modifi\u00e9 et des organismes parentaux</li>\n<li>4.8 Voie d'administration/de transmission</li>\n</ul>\n\n<h3>5. Innocuit\u00e9 pour l'humain</h3>\n\n<ul class=\"list-unstyled\">\n<li>5.1 Donn\u00e9es ant\u00e9rieures sur l'innocuit\u00e9</li>\n<li>5.2 Risque d'exposition pour l'humain</li>\n<li>5.3 Cons\u00e9quences possibles de l'exposition humaine</li>\n<li>5.4 Pathog\u00e9nicit\u00e9 pour l'humain des microorganismes parentaux</li>\n<li>5.5 Effet des manipulations g\u00e9n\u00e9tiques sur la pathog\u00e9nicit\u00e9 pour l'humain</li>\n<li>5.6 Risques associ\u00e9s \u00e0 une utilisation r\u00e9pandue du vaccine</li>\n</ul>\n\n<h3>6. Innocuit\u00e9 pour l'animal</h3>\n\n<ul class=\"list-unstyled\">\n<li>6.1 Donn\u00e9es ant\u00e9rieures sur l'innocuit\u00e9</li>\n<li>6.2 Devenir du vaccin chez les esp\u00e8ces vis\u00e9es et non vis\u00e9es</li>\n<li>6.3 Risques d'excr\u00e9tion et/ou de propagation par suite de contacts entre des animaux vis\u00e9s et non vis\u00e9s</li>\n<li>6.4 R\u00e9version de virulence r\u00e9sultant de la r\u00e9inoculation chez les animaux</li>\n<li>6.5 Effet d'un surdosage chez les esp\u00e8ces vis\u00e9es et non vis\u00e9es potentielles</li>\n<li>6.6 Gamme d'h\u00f4tes et potentiel de diss\u00e9mination du vecteur</li>\n<li>6.7 Innocuit\u00e9 relative par rapport aux vaccins classiques</li>\n<li>6.8 Innocuit\u00e9 pour les femelles gravides et pour les petits nourris \u00e0 la mamelle par une m\u00e8re vaccine</li>\n</ul>\n\n<h3>7. Environnement touch\u00e9</h3>\n\n<ul class=\"list-unstyled\">\n<li>7.1 \u00c9tendue de la diss\u00e9mination dans l'environnement\u00a0\u2013 identification du lieu</li>\n<li>7.2 Persistance du vecteur dans l'environnement et r\u00e9percussions cumulatives</li>\n<li>7.3 Degr\u00e9 d'exposition des esp\u00e8ces non vis\u00e9es</li>\n<li>7.4 Comportement des microorganismes parentaux et du vecteur chez les esp\u00e8ces non vis\u00e9es</li>\n<li>7.5 Facteurs d'ordre physique et chimique qui peuvent modifier la dur\u00e9e de survie, la reproduction et la dispersion du vecteur</li>\n</ul>\n\n<h3>8. Incidences sur l'environnement</h3>\n\n<ul class=\"list-unstyled\">\n\n<li><p>8.1 Risques et avantages</p>\n\n<p>Comparer et analyser les risques potentiels par rapport aux avantages de la mesure propos\u00e9e, de l'\u00e9tude propos\u00e9e ou de l'homologation du PBV issu de la biotechnologie.</p></li>\n\n<li>8.2 Innocuit\u00e9 relative par rapport \u00e0 d'autres vaccins</li>\n\n</ul>\n\n<h3>9. Mesures d'att\u00e9nuation</h3>\n\n<ul class=\"list-unstyled\">\n<li>9.1 S\u00e9curit\u00e9 du personnel</li>\n<li>9.2 S\u00e9curit\u00e9 des personnes autre que le personnel (risques pour la sant\u00e9 et la s\u00e9curit\u00e9 publiques)</li>\n<li>9.3 Manipulation du vaccin</li>\n<li>9.4 Manipulation des animaux vaccin\u00e9s ou expos\u00e9s (excr\u00e9tion et propagation du vaccin)</li>\n<li>9.5 Environnement</li>\n</ul>\n\n<h3>10. Surveillance et tenue des registres</h3>\n\n<p>Indiquer la permission \u00e0 d\u00e9livrer et r\u00e9sumer les conditions sp\u00e9cifiques d'utilisation du PBV et de tenue des registres pour la fabrication, la production, les essais de contr\u00f4le qualit\u00e9, l'entreposage, la distribution, l'utilisation et l'\u00e9limination du VB, le cas \u00e9ch\u00e9ant.</p>\n\n<h3>11. Consultations et personnes-ressources</h3>\n\n<p>Indiquer le nom et les coordonn\u00e9es du fabricant, du chercheur et/ou de l'importateur de PBV, des d\u00e9partements gouvernementaux et des experts consult\u00e9s au cours de la pr\u00e9paration de la pr\u00e9sente \u00e9valuation environnementale.</p>\n\n<h3>12. Conclusions et mesures mises en \u0153uvre</h3>\n\n<p>R\u00e9sumer la d\u00e9cision et les recommandations relatives \u00e0 l'utilisation du PBV issu de la biotechnologie.</p>\n\n<h3>13. R\u00e9f\u00e9rences bibliographiques</h3>\n\n<p>Fournir une liste de toutes les r\u00e9f\u00e9rences cit\u00e9es ou utilis\u00e9es, y compris les publications scientifiques \u00e9valu\u00e9es par des pairs, les sites web d'experts et les communications personnelles.</p>\n\n<h3>14. Annexes</h3>\n\n<ul class=\"list-unstyled\">\n<li>Annexer les tableaux, les figures et les cartes des sites d'\u00e9tude, le cas \u00e9ch\u00e9ant.</li>\n<li>Annexer les protocoles d'\u00e9tude, les rapports finaux et les d\u00e9viations de l'\u00e9tude, le cas \u00e9ch\u00e9ant.</li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}