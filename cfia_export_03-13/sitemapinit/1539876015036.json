{
    "dcr_id": "1539876014831",
    "title": {
        "en": "Food inspection guidance: permission issuance",
        "fr": "Orientation d'inspection des aliments : d\u00e9livrance d'autorisations "
    },
    "html_modified": "2024-03-12 3:58:10 PM",
    "modified": "2024-01-31",
    "issued": "2018-11-20",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_standard_permission_procedures_1539876014831_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_standard_permission_procedures_1539876014831_fra"
    },
    "ia_id": "1539876015036",
    "parent_ia_id": "1542313762789",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1542313762789",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food inspection guidance: permission issuance",
        "fr": "Orientation d'inspection des aliments : d\u00e9livrance d'autorisations "
    },
    "label": {
        "en": "Food inspection guidance: permission issuance",
        "fr": "Orientation d'inspection des aliments : d\u00e9livrance d'autorisations "
    },
    "templatetype": "content page 1 column",
    "node_id": "1539876015036",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1542313686757",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/",
        "fr": "/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food inspection guidance: permission issuance",
            "fr": "Orientation d'inspection des aliments : d\u00e9livrance d'autorisations"
        },
        "description": {
            "en": "This page provides operational guidance to conduct activities related to permissions. It contains food and commodity-specific guidance for export certification, point of entry violations, import clearance procedures and other inspection activities.",
            "fr": "Cette page contient des documents d'orientation op\u00e9rationnelle n\u00e9cessaires \u00e0 la conduite d'activit\u00e9s reli\u00e9es aux permissions. Elle contient des documents d'orientation op\u00e9rationnelle sp\u00e9cifiques aux aliments et \u00e0 certains produits pour la certification \u00e0 l'exportation, les infractions aux point d'entr\u00e9s, les proc\u00e9dures de rel\u00e2che \u00e0 l'importation et d'autres activit\u00e9s d'inspection."
        },
        "keywords": {
            "en": "Standard permissions procedures, SPP",
            "fr": "Proc\u00e9dures de permissions standardis\u00e9es, PPS"
        },
        "dcterms.subject": {
            "en": "inspection,food inspection,processing",
            "fr": "inspection,inspection des aliments,traitement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-11-20",
            "fr": "2018-11-20"
        },
        "modified": {
            "en": "2024-01-31",
            "fr": "2024-01-31"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food inspection guidance: permission issuance",
        "fr": "Orientation d'inspection des aliments : d\u00e9livrance d'autorisations"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Find operational guidance and supporting documents to be used when conducting inspection activities to support the issuance of a domestic, import or export permission. This includes food and commodity-specific guidance for licensing, export certification, and import admissibility. </p>\n\n<h2>Common permission guidance</h2>\n\n<p>The following guidance is common to all permission-related inspection tasks</p>\n\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/standard-permissions-procedure/eng/1542297387999/1542297388274\">Standard permissions procedure</a></li>\n</ul>\n\n<h2>Export permission</h2>\n\n<h3>Food export certification</h3>\n\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/food-export-eligibility-lists/eng/1540923903657/1540923903887\">Procedure for maintaining food export eligibility lists</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/radioactivity-for-shipments-of-exported-food/eng/1546016654916/1546016703254\">Issuing a radiation certificate for shipments of exported food</a></li>\n</ul>\n\n<h3>Commodity specific export certification</h3>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certification-inspection-tasks-tables/eng/1545083390050/1545083481081\">Introduction to Export certification inspection task tables</a></li>\n<li>Dairy and milk products\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certificate-dairy-products/eng/1544328749086/1544328796966\">Issuing an export certificate for dairy products</a></li>\n</ul>\n</li>\n<li>Honey and other apicultural products\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certificate-for-honey/eng/1544124741274/1544125289164\">Issuing an export certificate for honey and other apicultural products</a></li>\n</ul>\n</li>\n<li>Maple and maple products\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/processed-fruit-or-vegetable-products-or-maple-pro/eng/1544035856243/1544035952553\">Issuing an export certificate for processed fruit or vegetable products, or maple products</a></li>\n</ul>\n</li>\n<li>Egg and processed egg\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certificate-for-processed-egg-products/eng/1545441556007/1545441657892\">Issuing an export certificate for processed egg products</a></li>\n</ul>\n</li>\n<li>Processed fruit or vegetable products\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/processed-fruit-or-vegetable-products-or-maple-pro/eng/1544035856243/1544035952553\">Issuing an export certificate for processed fruit or vegetable products, or maple products</a></li>\n</ul>\n</li>\n<li>Fresh fruits and vegetables\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/exemption-of-red-skinned-round-type-potatoes/eng/1540905389649/1540905389883\">Exemption of red-skinned round type potatoes exported to the United States and Puerto Rico</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/onion-pilot-project/eng/1645036598086/1645036598726\">Operational Procedure: Onion Pilot Project for Canadian Partners in Quality Preventive Control Inspection</a></li>\n</ul>\n</li>\n<li>Gelatin and collagen products\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certificate-highly-processed-animal-product/eng/1633618043988/1633618044847\">Issuing an export certificate for highly processed animal products intended for human consumption</a></li>\n</ul>\n</li>\n<li>Highly refined products\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certificate-highly-processed-animal-product/eng/1633618043988/1633618044847\">Issuing an export certificate for highly processed animal products intended for human consumption</a></li>\n</ul>\n</li>\n<li>Insects\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/export-certificate-insects-intended-for-human-cons/eng/1639607736623/1639607737295\">Issuing an export certificate for insects intended for human consumption</a></li>\n</ul>\n</li>\n</ul>\n\n<h2>Import permission</h2>\n\n<h3>Import admissibility (at border)</h3>\n\n<ul>\n\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/return-of-meat-products/eng/1544138898146/1544138898407\">Processing applications for the import (return) of edible meat products that were exported from Canada</a></li>\n</ul>\n\n<h3>Import admissibility (post border)</h3>\n\n<h4>Food </h4>\n\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/postal-and-courier-import-inspections/eng/1545202917053/1545203138242\">Postal and courier import inspections</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/cfia-area-contacts-for-non-resident-importers-and-/eng/1556555772285/1556556103115\">CFIA areas designated as the contact for non-resident parties</a></li>\n</ul>\n\n<h4>Commodity specific</h4>\n\n<h5>Meat products</h5>\n\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/organoleptic-evaluation-meat-products/eng/1546892808848/1546892874615\">Organoleptic evaluation of imported meat products</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/return-meat-products-from-us/eng/1544657691292/1544657691530\">Return (import) of exported meat products from the United States, pending the results of US laboratory analysis</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/retention-of-omic/eng/1545507617988/1545507771758\">Retention of official meat inspection certificate accompanying meat shipments</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/verification-of-seal-number/eng/1544567396717/1544567396970\">Verification of seal number when foreign seal on shipment broken while in transit</a></li>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/visual-inspection-of-imported-meat-products/eng/1546436244337/1546436304407\">Visual inspection of imported meat products</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Trouvez les orientations op\u00e9rationnelles et les documents de support \u00e0 utiliser lors de la r\u00e9alisation des activit\u00e9s d'inspection en lien avec l'\u00e9mission d'une autorisation domestique, d'importation ou d'exportation. Ceci comprend les orientations pour les aliments et les produits sp\u00e9cifiques pour les licences, la certification des exportations, et l'admissibilit\u00e9 \u00e0 l'importation.</p>\n\n<h2>Orientation d'autorisation commune</h2>\n\n<p>Les documents d'orientation suivants sont communs \u00e0 toutes les t\u00e2ches d'inspection li\u00e9es aux autorisations</p>\n\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/procedure-normalisee-des-autorisations/fra/1542297387999/1542297388274\">Proc\u00e9dure normalis\u00e9e des autorisations</a></li>\n</ul>\n\n<h2>Autorisation d'exportation</h2>\n\n<h3>Certification des exportations alimentaires</h3>\n\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/les-listes-d-admissibilite-a-l-exportation-d-alime/fra/1540923903657/1540923903887\">Proc\u00e9dure pour tenir \u00e0 jour les listes d'admissibilit\u00e9 \u00e0 l'exportation d'aliments</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/radioactivite-pour-les-aliments-exportes/fra/1546016654916/1546016703254\">D\u00e9livrance d'un certificat de radiation pour les aliments export\u00e9s</a></li>\n</ul>\n\n<h3>Certification des exportation sp\u00e9cifique \u00e0 certains produits </h3>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/tableaux-des-taches-d-inspection-reliees-a-la-cert/fra/1545083390050/1545083481081\">Introduction aux tableaux des t\u00e2ches d'inspections reli\u00e9es \u00e0 la certification des exportations</a></li>\n<li>Lait et les produits laitiers\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/certificat-d-exportation-produits-laitiers/fra/1544328749086/1544328796966\">D\u00e9livrer un certificat d'exportation de produits laitiers</a></li>\n</ul>\n</li>\n<li>Produits de miel et d'autres produits d'apicoles\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/certificat-d-exportation-de-miel/fra/1544124741274/1544125289164\">D\u00e9livrance d'un certificat d'exportation de miel et d'autres produits d'apicoles</a></li>\n</ul>\n</li>\n<li>Produits d'\u00e9rable\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/produits-de-fruits-et-legumes-transformes-ou-de-pr/fra/1544035856243/1544035952553\">D\u00e9livrer un certificat d'exportation de produits de fruits et l\u00e9gumes transform\u00e9s ou de produits de l'\u00e9rable</a></li>\n</ul>\n</li>\n<li>\u0152ufs et \u0153ufs transform\u00e9s\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/delivrer-un-certificat-d-exportation-de-produits-d/fra/1545441556007/1545441657892\">D\u00e9livrer un certificat d'exportation de produits d'\u0153ufs transform\u00e9s</a></li>\n</ul>\n</li>\n<li>Produits de fruits ou de l\u00e9gumes transform\u00e9s\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/produits-de-fruits-et-legumes-transformes-ou-de-pr/fra/1544035856243/1544035952553\">D\u00e9livrer un certificat d'exportation de produits de fruits et l\u00e9gumes transform\u00e9s ou de produits de l'\u00e9rable</a></li>\n</ul>\n</li>\n<li>Fruits et l\u00e9gumes frais\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/exemption-des-pommes-de-terre-de-type-rond-a-peau-/fra/1540905389649/1540905389883\">Exemption des pommes de terre de type rond \u00e0 peau rouge export\u00e9es aux \u00c9tats-Unis et Porto Rico</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/projet-pilote-sur-l-oignon/fra/1645036598086/1645036598726\">Proc\u00e9dure op\u00e9rationnelle\u00a0: Projet pilote sur l'oignon pour l'inspection de contr\u00f4le pr\u00e9ventif dans le cadre du Programme des partenaires pour la qualit\u00e9 au Canada </a></li>\n</ul>\n</li>\n<li>Produits de g\u00e9latine et collag\u00e8ne\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/certificat-d-exportation-produits-animaux-hautemen/fra/1633618043988/1633618044847\">D\u00e9livrer un certificat d'exportation pour les produits animaux hautement transform\u00e9s destin\u00e9s \u00e0 la consommation humaine</a></li>\n</ul>\n</li>\n<li>Produits hautement raffin\u00e9s\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/certificat-d-exportation-produits-animaux-hautemen/fra/1633618043988/1633618044847\">D\u00e9livrer un certificat d'exportation pour les produits animaux hautement transform\u00e9s destin\u00e9s \u00e0 la consommation humaine</a></li>\n</ul>\n</li>\n<li>Insectes\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/certificat-d-exportation-insectes-destines-a-la-co/fra/1639607736623/1639607737295\">D\u00e9livrer un certificat d'exportation pour les insectes destin\u00e9s \u00e0 la consommation humaine</a></li>\n</ul>\n</li>\n</ul>\n\n<h2>Autorisation d'importation</h2>\n\n<h3>Admissibilit\u00e9 \u00e0 l'importation (\u00e0 la fronti\u00e8re)</h3>\n\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/retour-des-produits-de-viande/fra/1544138898146/1544138898407\">Proc\u00e9dure de traitement des demandes d'importation (retour) de produits de viande comestibles qui ont \u00e9t\u00e9 export\u00e9s du Canada</a></li>\n</ul>\n\n<h3>Admissibilit\u00e9 \u00e0 l'importation (apr\u00e8s la fronti\u00e8re)</h3>\n\n<h4>Aliments </h4>\n\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/inspection-d-importations-expediees-par-la-poste-o/fra/1545202917053/1545203138242\">Inspection d'importations exp\u00e9di\u00e9es par la poste ou par messagerie</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/regions-de-l-acia-designees-comme-points-de-contac/fra/1556555772285/1556556103115\">R\u00e9gions de l'ACIA d\u00e9sign\u00e9es comme points de contact pour les parties</a></li>\n</ul>\n\n<h4>Sp\u00e9cifiques \u00e0 certains produits</h4>\n\n<h5>Produits de viandes</h5>\n\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/evaluation-organoleptique-produits-de-viande/fra/1546892808848/1546892874615\">\u00c9valuation organoleptique des produits de viande import\u00e9s</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/retour-de-produits-de-viande-des-etats-unis/fra/1544657691292/1544657691530\">Retour (importation) des produits de viande export\u00e9s aux \u00c9tats-Unis en attendant les r\u00e9sultats des analyses de laboratoire des \u00c9tats-Unis</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/conservation-du-coiv/fra/1545507617988/1545507771758\">Conservation du certificat officiel d'inspection de viande accompagnant les cargaisons de viande</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/verification-des-numeros-de-sceau/fra/1544567396717/1544567396970\">V\u00e9rification des num\u00e9ros de sceau lorsque les sceaux \u00e9trangers sur les envois de viande sont bris\u00e9s pendant le transit</a></li>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/inspection-visuelle-des-produits-de-viande-importe/fra/1546436244337/1546436304407\">Inspection visuelle des produits de viande import\u00e9s</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}