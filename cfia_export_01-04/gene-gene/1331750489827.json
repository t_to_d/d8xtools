{
    "dcr_id": "1331750489827",
    "title": {
        "en": "Kudzu - <i lang=\"la\">Pueraria montana</i>",
        "fr": "Kudzu - <i lang=\"la\">Pueraria montana</i>"
    },
    "html_modified": "2012-03-14 14:41",
    "modified": "14-5-2019",
    "issued": "14-3-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_pueraria_1331750489827_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_pueraria_1331750489827_fra"
    },
    "parent_ia_id": "1331614823132",
    "ia_id": "1331750551292",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Kudzu - <i lang=\"la\">Pueraria montana</i>",
        "fr": "Kudzu - <i lang=\"la\">Pueraria montana</i>"
    },
    "label": {
        "en": "Kudzu - <i lang=\"la\">Pueraria montana</i>",
        "fr": "Kudzu - <i lang=\"la\">Pueraria montana</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331750551292",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1331614724083",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/kudzu/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/kudzu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Kudzu - Pueraria montana",
            "fr": "Kudzu - Pueraria montana"
        },
        "description": {
            "en": "Kudzu is known as one of the world?s most invasive plants. It grows rapidly and forms dense, ropey mats over other vegetation and structures.",
            "fr": "Le kudzu est reconnu comme l?une des plantes les plus envahissantes au monde. Il cro\u00eet rapidement et recouvre d?autres v\u00e9g\u00e9taux et des structures de tapis denses et noueux."
        },
        "keywords": {
            "en": "invasive plants, plant protection, Plant Protection Act, weed control, kudzu, pueraria montana",
            "fr": "plantes envahissantes, protection des v\u00e9g\u00e9taux, Loi sur la protection des v\u00e9g\u00e9taux, contre les mauvaises herbes, pueraria montana, kudzu"
        },
        "dcterms.subject": {
            "en": "crops,environment,inspection,plants,weeds,environmental protection",
            "fr": "cultures,environnement,inspection,plante,plante nuisible,protection de l'environnement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-14 14:41:31",
            "fr": "2012-03-14 14:41:31"
        },
        "modified": {
            "en": "2019-05-14",
            "fr": "2019-05-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Kudzu - Pueraria montana",
        "fr": "Kudzu - Pueraria montana"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/plant-health/seeds/seed-testing-and-grading/seeds-identification/pueraria-montana/eng/1397755917428/1397755957679\">Weed Seed - Kudzu (<i lang=\"la\">Pueraria montana</i>)</a></p>\n<p><a href=\"/eng/1548085757491/1548085933224#a12\">Plant pest card - Kudzu</a></p>\n<p>Kudzu is known as one  of the world's most invasive plants. It grows rapidly and forms dense, ropey mats over other vegetation and structures. The plants produce massive tuberous  roots, making them difficult to control or eradicate. Kudzu reduces biodiversity and causes significant productivity losses to the forestry industry.</p>\n<h2>Where it's found</h2>\n<p>Native to eastern Asia, the only sustained population of kudzu known to be in Canada was  discovered in southern Ontario in 2009. It has been intentionally introduced to  many countries as an ornamental or forage plant, and as a means of erosion control.</p>\n<p>There are severe infestations of kudzu in the southeastern United States. Kudzu grows in numerous habitats, including abandoned fields, grasslands, natural forests, pastures, plantations, roadsides, riverbanks and urban areas. It prefers full sunlight and tolerates a wide range of soil types.</p>\n<h2>What it looks like</h2>\n<p>Kudzu is a perennial, climbing vine with stems that can grow 10\u201330\u00a0<abbr title=\"metres\">m</abbr> in length. Its hairy leaves are composed of three leaflets. Kudzu flowers are clustered, fragrant, reddish-purple, and pea-like in appearance.</p>\n<h2>How it spreads</h2>\n<p>Intentional planting of kudzu has been the most significant factor in its spread. Once established in an area, its rapid, vegetative growth allows it to spread locally. Movement  of contaminated soil or equipment can result in new introductions.</p>\n<h2>Legislation</h2>\n<p>Kudzu is regulated as a pest in Canada under the <i>Plant Protection Act</i>. It is also listed as a prohibited noxious weed on the Weed Seeds Order, 2016 under the <i>Seeds Act</i>. Importation and domestic movement of regulated plants and their propagative parts is prohibited.</p>\n<h2>What you can do about it</h2>\n<ul>\n<li>Leave natural items in their natural habitats.</li>\n<li>Avoid planting invasive plants in your garden.</li>\n<li>Use clean, high-quality seed that is certified if possible.</li>\n<li>Contact your local Canadian Food Inspection Agency (CFIA) office if you suspect you have found this invasive plant. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will follow up and determine if further action is needed.</li>\n</ul>\n<p>Learn more about <a href=\"/eng/1328325263410/1328325333845\">invasive species</a>.</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img alt=\"Kudzu leaf\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariad_1331669714131_eng.JPG\"><figcaption>Kudzu leaf</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-lg\">\n<img alt=\"Kudzu infestation\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariac_1331669679476_eng.JPG\"><figcaption>Kudzu infestation</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-lg\">\n<img alt=\"Kudzu inflorescence\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariaa_1331669588227_eng.jpg\"><figcaption>Kudzu inflorescence<br>Attribution, Sam Brinker, Ontario Ministry of Natural Resources</figcaption>\n</figure>\n\n<figure>\n<img alt=\"Kudzu seedling\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariab_1331669646211_eng.jpg\"><figcaption>Kudzu seedling<br>Attribution: Stephen Darbyshire, Agriculture and Agri-Food Canada</figcaption>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/pueraria-montana/fra/1397755917428/1397755957679\">Semence de mauvaises herbe - Kudzu (<i lang=\"la\">Pueraria montana</i>)</a></p>\n<p><a href=\"/fra/1548085757491/1548085933224#a12\">Carte de phytoravageur - Kudzu</a></p>\n<p>Le kudzu est reconnu comme l'une des plantes les plus envahissantes au monde. Il cro\u00eet rapidement et recouvre d'autres v\u00e9g\u00e9taux et des structures de tapis denses et noueux. Ses  plants produisent d'imposantes racines tuberculeuses qui les rendent difficiles \u00e0 contr\u00f4ler ou \u00e0 \u00e9liminer. Il r\u00e9duit la biodiversit\u00e9 et fait subir \u00e0 l'industrie foresti\u00e8re d'importantes pertes de productivit\u00e9.</p>\n<h2>O\u00f9 trouve-t-on cette esp\u00e8ce?</h2>\n<p>Originaire de l'est de l'Asie, le hudzu a form\u00e9 une seule population viable au Canada, qui a \u00e9t\u00e9 d\u00e9couverte dans le sud de l'Ontario en 2009. L'esp\u00e8ce a \u00e9t\u00e9 introduite  volontairement dans de nombreux pays comme plante ornementale ou fourrag\u00e8re, ou pour freiner l'\u00e9rosion.</p>\n<p>Dans le sud-est des \u00c9tats-Unis, le kudzu est source de graves infestations. Il pousse dans de nombreux habitats, dont les champs abandonn\u00e9s, les p\u00e2turages, les for\u00eats naturelles, les plantations, le bord des routes, les rives et les r\u00e9gions urbaines. Il pr\u00e9f\u00e8re se trouver en plein soleil et tol\u00e8re une grande vari\u00e9t\u00e9 de types de sol.</p>\n<h2>\u00c0 quoi ressemble-t-elle?</h2>\n<p>Le kudzu est une  vigne grimpante vivace dont la hampe peut faire de 10 \u00e0 30\u00a0<abbr title=\"m\u00e8tres\">m</abbr> de longueur. Ses feuilles velues comportent trois folioles, et ses fleurs group\u00e9es, odorantes et d'un pourpre rouge\u00e2tre ressemblent \u00e0 des pois.</p>\n<h2>Comment se propage-t-elle?</h2>\n<p>La plantation volontaire du kudzu en est le plus important facteur de propagation. Une fois la plante \u00e9tablie dans une zone, sa croissance v\u00e9g\u00e9tative rapide lui permet de  s'\u00e9tendre tout autour d'elle. Le d\u00e9placement de sols contamin\u00e9s ou d'\u00e9quipement peut entra\u00eener de nouvelles introductions.</p>\n<h2>Lois</h2>\n<p>Le kudzu est r\u00e8glement\u00e9 comme \u00e9tant des organismes nuisibles en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>. Elle figure aussi \u00e0 la liste des mauvaises herbes nuisibles interdites de l'Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes, qui s'inscrit dans la <i>Loi sur les semences</i>. L'importation et la circulation sur le territoire canadien de plantes r\u00e9glement\u00e9s et de leurs parties servant \u00e0 la multiplication sont interdites.</p>\n<h2>Que pouvez-vous faire?</h2>\n<ul>\n<li>Laissez les \u00e9l\u00e9ments naturels dans leur habitat naturel.</li>\n<li>\u00c9vitez de planter des plantes envahissantes dans votre jardin.</li>\n<li>Utilisez des semences propres, de haute qualit\u00e9 et si possible certifi\u00e9es.</li>\n<li>Communiquez avec votre bureau local de l'Agence canadienne d'inspection des aliments (ACIA) si vous croyez avoir aper\u00e7u cette plante envahissante. L'<abbr title=\"Agence canadienne d\u2019inspection des aliments\">ACIA</abbr> fera un suivi et d\u00e9terminera si d'autres mesures s'imposent.</li>\n</ul>\n<p>En savoir plus sur les <a href=\"/fra/1328325263410/1328325333845\">esp\u00e8ces envahissantes</a>.</p>\n<figure class=\"mrgn-bttm-lg\">\n<img alt=\"Feuille du kudzu\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariad_1331669714131_fra.jpg\"><figcaption>Feuille du kudzu</figcaption>\n</figure>\n<figure class=\"mrgn-bttm-lg\">\n<img alt=\"Infestation de kudzu\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariac_1331669679476_fra.jpg\"><figcaption>Infestation de kudzu</figcaption>\n</figure>\n<figure class=\"mrgn-bttm-lg\">\n<img alt=\"Inflorescence du kudzu\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariaa_1331669588227_fra.jpg\"><figcaption>Inflorescence du kudzu<br>Source\u00a0: Sam Brinker, minist\u00e8re des Richesses naturelles de l'Ontario</figcaption>\n</figure>\n<figure>\n<img alt=\"Semis de kudzu\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_puerariab_1331669646211_fra.jpg\"><figcaption>Semis de kudzu<br>Source\u00a0: Stephen Darbyshire, Agriculture et Agroalimentaire Canada</figcaption>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}