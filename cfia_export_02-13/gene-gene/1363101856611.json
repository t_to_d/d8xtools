{
    "dcr_id": "1363101856611",
    "title": {
        "en": "Conditions for importing meat products from China",
        "fr": "Conditions pour l'importation des produits de viande en provenance de la Chine"
    },
    "html_modified": "2024-02-13 9:48:44 AM",
    "modified": "2019-12-04",
    "issued": "2015-04-09 07:46:19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexa_china_1363101856611_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexa_china_1363101856611_fra"
    },
    "ia_id": "1363101911891",
    "parent_ia_id": "1336319720090",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food",
        "fr": "Importation d\u2019aliments"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1336319720090",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Conditions for importing meat products from China",
        "fr": "Conditions pour l'importation des produits de viande en provenance de la Chine"
    },
    "label": {
        "en": "Conditions for importing meat products from China",
        "fr": "Conditions pour l'importation des produits de viande en provenance de la Chine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1363101911891",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1336318487908",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/approved-countries/china/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/pays-approuves/chine/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Conditions for importing meat products from China",
            "fr": "Conditions pour l'importation des produits de viande en provenance de la Chine"
        },
        "description": {
            "en": "Conditions for importing meat products from China",
            "fr": "Conditions pour l'importation des produits de viande en provenance de la Chine"
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, testing, chemical, residues, results, compounds",
            "fr": "Loi sur l&#39;inspection des viandes, R&#232;glement de 1990 sur l&#39;inspection des viandes, analyse, chimique, r&#233;sidus, r&#233;sultats, compos&#233;s"
        },
        "dcterms.subject": {
            "en": "livestock,imports,food inspection,products,agri-food products,regulation,food safety,testing,meat,poultry",
            "fr": "b\u00e9tail,importation,inspection des aliments,produit,produit agro-alimentaire,r\u00e9glementation,salubrit\u00e9 des aliments,test,viande,volaille"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Agrifood, Meat and Seafood Safety Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des produits agroalimentaires, de la viande et des produits de la mer"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-09 07:46:19",
            "fr": "2015-04-09 07:46:19"
        },
        "modified": {
            "en": "2019-12-04",
            "fr": "2019-12-04"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Conditions for importing meat products from China",
        "fr": "Conditions pour l'importation des produits de viande en provenance de la Chine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul class=\"list-unstyled mrgn-bttm-md\">\n<li><a href=\"#a1\">1. Meat Inspection systems approved</a></li>\n<li><a href=\"#a2\">2. Types of meat products accepted for import (based on animal health restrictions)</a></li>\n<li><a href=\"#a3\">3. Additional animal health certification attestations required on the OMIC</a></li>\n<li><a href=\"#a4\">4. Establishments eligible for export to Canada</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Meat Inspection systems approved</h2>\n\n<p><strong>1.1</strong> Swine and farmed wild boar: slaughter, cutting, deboning, natural salted casings and edible meat by-products.</p>\n\n<p><strong>1.2</strong> Poultry and farmed feathered game: slaughter, cutting, deboning and edible meat by-products.</p> \n\n<p><strong>1.3</strong> Processing (for poultry of Chinese origin and meat derived from bovine and swine imported into China from Canadian Food Inspection Agency (CFIA) approved sources): comminuting, formulating, curing, cooking and canning.</p>\n\n<p><strong>1.4</strong> Grading of natural salted intestinal casings - pork, beef, sheep, goat</p> \n\n<h2 id=\"a2\">2. Types of meat products accepted for import (based on animal health restrictions)</h2>\n\n<h3>2.1. Fresh meat and raw meat products (chilled or frozen)</h3>\n\n<p><strong>2.1.1.</strong> Meat and meat products derived from swine of Chinese origin - <strong>importation not allowed</strong></p>\n\n<p><strong>2.1.2.</strong> Meat and meat products derived from poultry of Chinese origin \u2013 <strong>Only cooked poultry meat products are allowed.</strong></p>\n\n<p><strong>2.1.3.</strong> Natural salted intestinal casings derived from animals of the family <span lang=\"la\">Bovidae</span> (includes bison, African buffalo, water buffalo, antelope, sheep, goats, muskoxen and domestic cattle), born, raised and slaughtered in Australia, New Zealand and Canada only - see section 3.2 for additional certification attestations required.</p>\n\n<p><strong>2.1.4.</strong> Natural salted intestinal casings derived from swine, born, raised and slaughtered in Canada or <abbr title=\"United States of America\">USA</abbr> only - see section 3.3 for additional certification attestations required.</p>\n\n<h3>2.2 All processed meat products (heat treated and raw), other than shelf stable commercially sterile meat products packaged in hermetically sealed containers (cans and/or retortable pouches), and shelf stable dried soup-mix products, bouillon cubes and meat extracts</h3>\n\n<p><strong>2.2.1</strong> Meat products derived from swine - <strong>importation of processed meat products of Chinese origin is not allowed</strong>.</p>\n\n<p><strong>2.2.2</strong> Meat products derived from poultry - see section 3.4 for additional certification attestations required.</p>\n<p><strong>2.2.3.</strong> Meat products prepared from bovine meat imported into China from <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> approved sources- see section 3.5 and 3.6 for additional attestations</p>\n<p><strong>2.2.4.</strong> Meat products prepared from swine meat imported into China from <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> approved sources - see section 3.5 and 3.6 for additional attestations</p>\n\n<p><strong>2.2.5</strong> Dried tubed hog collagen casings of Chinese origin - see section 3.1 for additional certification attestations required.</p>\n\n<h3>2.3. Shelf stable commercially sterile meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes, meat extracts</h3>\n\n<p><strong>2.3.1.</strong> For all pork meat products manufactured from approved <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> sources \u2013 no additional certification attestations required.</p>\n\n<p><strong>2.3.2.</strong> For all poultry meat derived meat products - no additional certification attestations required.</p> \n\n<h2 id=\"a3\">3. Additional animal health certification attestations required on the Official Meat inspection Certificate (OMIC)</h2>\n\n<h3>3.1. For dried, tubed collagen hog casings of Chinese origin</h3>\n\n<p>I hereby certify that:</p>\n\n<p><strong>3.1.1.</strong> The above mentioned casings were processed from Chinese hog casing material at a temperature of 100\u00b0<abbr title=\"celsius\">C</abbr> for a period of not less than 20 minutes.</p>\n\n<h3>3.2. For natural salted intestinal casings derived from animals of the family <span lang=\"la\">Bovidae</span></h3>\n\n<p>I hereby certify that:</p>\n\n<p><strong>3.2.1.</strong> The natural salted intestinal casings covered by this certificate are strictly derived from animals slaughtered in Australia or New Zealand and Canada have not been exposed at any time during their handling, processing, packaging or storage to any product or by product derived from animals from any other country.</p>\n\n<h3>3.3. For natural salted intestinal casings derived from pigs</h3>\n\n<p>I hereby certify that:</p>\n\n<p><strong>3.3.1</strong> The natural salted intestinal casings covered by this certificate are strictly derived from animals slaughtered in Canada or <abbr title=\"United States of America\">USA</abbr> and have not been exposed at any time during their handling, processing, packaging or storage to any product or by product derived from animals from any other country.</p>\n\n<h3>3.4. For all poultry and all other bird derived meat and meat products other than shelf stable, commercially sterile poultry meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes and meat extract</h3>\n\n<p>I hereby certify that:</p>\n<p><strong>3.4.1.</strong> The poultry meat products are derived from birds which were raised and fed together, received the same treatments, and were sent to slaughter together.</p>\n\n<p><strong>3.4.2.</strong> The poultry meat products covered by the present certificate are derived from birds which were subject to humane slaughter and were stunned before slaughter.</p>\n\n<p><strong>3.4.3.</strong> The poultry meat has been cooked:</p>\n\n<p><strong>Either</strong></p>\n\n<p><strong>3.4.3.1.</strong> Reaching an internal temperature of 65 degrees Celsius for 840 seconds;</p> \n\n<p><strong>Or</strong></p> \n\n<p><strong>3.4.3.2.</strong> Reaching an internal temperature of 70 degrees Celsius for 574 seconds;</p> \n\n<p><strong>Or</strong></p>\n\n<p><strong>3.4.3.3.</strong> Reaching an internal temperature of 74 degrees Celsius for 280 seconds;</p>\n\n<p><strong>Or</strong></p>\n\n<p><strong>3.4.3.4.</strong> Reaching an internal temperature of 80 degrees Celsius for 203 seconds:</p>\n\n<p><strong>3.4.4.</strong> There is a complete separation, in both space and personnel, between the uncooked and cooked poultry meat product areas of the processing establishment and the poultry meat products were handled in a way that any possibility of recontamination of the cooked poultry meat products by raw poultry meat products, either directly or indirectly, was prevented.</p> \n\n<p><strong>3.4.5.</strong> Every precaution was taken to prevent any direct or indirect contact during the slaughter, handling, processing, storage and packaging of the poultry meat products with any animal product or by-product that does not fulfill the requirements of this certificate.</p>\n<h3>3.5. For all bovine and swine meat products other than shelf stable, commercially sterile swine meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes and meat extract</h3>\n<p>\"I hereby certify that:</p>\n<p><strong>3.5.1.</strong> The grain products stuffed with meat ingredients<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup> for export to Canada were produced at the beginning of the day of production after the sanitation period and after a compliant pre-operational sanitation inspection was verified by a <abbr title=\"China Inspection and Quarantine\">CIQ</abbr> official.</p>\n<p><strong>3.5.1.2.</strong> The production of the grain products stuffed with meat ingredients for export to Canada was carried out under <abbr title=\"China Inspection and Quarantine\">CIQ</abbr> supervision.</p>\n<p><strong>3.5.1.3.</strong> Every precaution was taken during the during the handling, processing, packaging and storage of the raw meat ingredients and the finished grain products stuffed with meat ingredients to prevent direct or indirect contact with any animal product or by-product that does not fulfill the requirements of this certificate.\"</p>\n<h3>3.6. For meat and meat products prepared from bovine and swine meat and meat products imported into China</h3>\n<p>\"I hereby certify that:</p>\n<p><strong>3.6.1.</strong> The meat and/or meat products:</p>\n<p><strong>3.6.1.1.</strong> Were legally imported into China;</p>\n<p><strong>3.6.1.2.</strong> Meet all Canadian requirements for commercial importation of meat products as listed in <a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/approved-countries/eng/1336318487908/1336319720090\">Countries from which commercial importation of meat products is permitted</a> as if they were exported from the country of origin directly to Canada.</p>\n<p><strong>3.6.2.</strong> Every precaution was taken to prevent direct or indirect contact during the processing and packaging of the meat product with any animal product or by-product that does not meet the requirements of this certificate.</p>\n<p><strong>3.6.3.</strong> The meat inspection certificate with which the meat was imported into China is kept on file at the Chinese processing establishment for verification purposes for a minimum of 2 years following the importation.</p>\n<h2 id=\"a4\">4. Establishments eligible for export to Canada</h2>\n<p><strong>4.1.</strong>\u00a0Refer to the\u00a0<a href=\"/active/netapp/meatforeign-viandeetranger/forliste.aspx\">List of foreign countries establishments eligible to export meat products to Canada</a>.</p>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Footnotes</h2>\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\"> <p>For example dumplings, spring rolls or buns.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> </dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul class=\"list-unstyled mrgn-bttm-md\">\n<li><a href=\"#a1\">1. Syst\u00e8mes d'inspection accept\u00e9s</a></li>\n<li><a href=\"#a2\">2. Types de produits de viande accept\u00e9s pour l'importation (fond\u00e9 sur les restrictions pour raisons en sant\u00e9 animale)</a></li>\n<li><a href=\"#a3\">3. Attestations de certification suppl\u00e9mentaires de la sant\u00e9 des animaux exig\u00e9es sur le COIV</a></li>\n<li><a href=\"#a4\">4. Les \u00e9tablissements \u00e9ligibles pour exportation au Canada</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Syst\u00e8mes d'inspection accept\u00e9s</h2>\n\n<p><strong>1.1</strong> Porc et sanglier d'\u00e9levage\u00a0: abattage, d\u00e9coupage, d\u00e9sossage boyaux naturels sal\u00e9s et sous-produits de viande comestibles.</p>\n\n<p><strong>1.2</strong> Volaille et gibier \u00e0 plume\u00a0: abattage, d\u00e9coupage, d\u00e9sossage et sous-produits de viande comestibles.</p> \n\n<p><strong>1.3</strong> Transformation (volaille provenant de la Chine et viande issue de bovins et de porcs import\u00e9s par la Chine \u00e0 partir de sources approuv\u00e9es par l'Agence canadienne d'inspection des aliments)\u00a0: hachage, formulation, salaison, caisson, mise en conserve.</p> \n\n<p><strong>1.4</strong> Classification de boyaux naturels sal\u00e9s d'animaux \u2013 porc, boeuf, mouton et ch\u00e8vre </p> \n\n<h2 id=\"a2\">2. Les types de produits de viande accept\u00e9s pour importation (fond\u00e9 sur les restrictions en sant\u00e9 animale)</h2>\n\n<h3>2.1. Viande fra\u00eeche et produits de viande crus (congel\u00e9s ou r\u00e9frig\u00e9r\u00e9s)</h3>\n\n<p><strong>2.1.1</strong> Viande et produits de viande issus de porcs provenant de la Chine - <strong>l'importation n'est pas permise</strong></p>\n\n<p><strong>2.1.2</strong>. Viande et produits de viande issus de volaille provenant de la Chine \u2013 <strong>seuls les produits de viande de volaille cuits sont permis.</strong></p>\n\n<p><strong>2.1.3.</strong> Boyaux naturels sal\u00e9s provenant d'animaux de la famille des <span lang=\"la\">Bovidae</span> (inclut bison, buffle d'Afrique, buffle d'Asie, antilope, mouton, ch\u00e8vre, b\u0153uf musqu\u00e9 et bovin domestique) n\u00e9s, \u00e9lev\u00e9s et abattus en Australie, Nouvelle-Z\u00e9lande et au Canada seulement - consulter la section 3.2 pour les attestations de certification suppl\u00e9mentaires requises.</p>\n\n<p><strong>2.1.4.</strong> Boyaux naturels sal\u00e9s provenant de porcs n\u00e9s, \u00e9lev\u00e9s et abattus au Canada ou aux \u00c9tats-Unis seulement - consulter la section 3.3 pour les attestations de certification suppl\u00e9mentaires requises.</p>\n\n<h3>2.2. Produits de viande transform\u00e9s (trait\u00e9s \u00e0 la chaleur et crus), autres que les produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves et/ou sachets st\u00e9rilis\u00e9s), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation</h3>\n\n<p><strong>2.2.1</strong> Viande et produits de viande issus de porcs - <strong>l'importation de produits transform\u00e9s provenant de la Chine n'est pas permise</strong></p> \n\n<p><strong>2.2.2</strong> Viande et produits de viande issus de volaille - consulter la section 3.4 pour les attestations de certification suppl\u00e9mentaires requises.</p>\n<p><strong>2.2.3</strong> Produits de viande fabriqu\u00e9s \u00e0 partir de viande de bovins import\u00e9s par la Chine \u00e0 partir de sources approuv\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u2013 consulter les sections 3.5 et 3.6 pour conna\u00eetre les attestations de certification suppl\u00e9mentaires.</p>\n<p><strong>2.2.4</strong> Produits de viande fabriqu\u00e9s \u00e0 partir de viande de porcs import\u00e9s par la Chine \u00e0 partir de sources approuv\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u2013 consulter les sections 3.5 et 3.6 pour conna\u00eetre les attestations de certification suppl\u00e9mentaires.</p>\n\n<p><strong>2.2.5</strong> Boyaux de collag\u00e8ne de porc, s\u00e9ch\u00e9s et en tube \u2013 consulter la section 3.1 pour les attestations de certification suppl\u00e9mentaires requises.</p>\n\n<h3>2.3. Produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilis\u00e9s), et m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, cubes de bouillon et extraits de viande de longue conservation</h3>\n\n<p><strong>2.3.1</strong> Pour tous les produits de viande de porc transform\u00e9s et provenant de sources approuv\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> - aucune attestation de certification suppl\u00e9mentaire requise.</p>\n\n<p><strong>2.3.2.</strong> Pour tous les produits de viande de volaille - aucune attestation de certification suppl\u00e9mentaire requise.</p> \n\n<h2 id=\"a3\">3. Attestations de certification suppl\u00e9mentaires de la sant\u00e9 des animaux exig\u00e9es sur le Certificat officiel d'inspection des viandes (COIV)</h2>\n\n<h3>3.1. Pour les boyaux de collag\u00e8ne de porc, s\u00e9ch\u00e9s et en tube provenant de la Chine</h3>\n\n<p>Je certifie par la pr\u00e9sente que\u00a0:</p>\n\n<p><strong>3.1.1</strong> Les boyaux mentionn\u00e9s ci-dessus ont \u00e9t\u00e9 produits \u00e0 partir de mat\u00e9riel de boyaux de porc chinois \u00e0 une temp\u00e9rature de 100\u00b0<abbr title=\"celsius\">C</abbr> pour une p\u00e9riode d'au moins 20 minutes.</p> \n\n<h3>3.2. Pour les boyaux naturels sal\u00e9s provenant d'animaux de la famille <span lang=\"la\">Bovidae</span></h3>\n\n<p>Je certifie par la pr\u00e9sente que\u00a0:</p>\n\n<p><strong>3.2.1</strong> Les boyaux intestinaux naturels et sal\u00e9s couverts par ce certificat sont strictement d\u00e9riv\u00e9s d'animaux abattus en Australie ou en Nouvelle-Z\u00e9lande ou au Canada, et n'ont \u00e9t\u00e9 expos\u00e9s, en tout temps, pendant leur manipulation, transformation, emballage ou entreposage, \u00e0 tout produit ou sous-produit issu d'animaux de tout autre pays.</p>\n\n<h3>3.3 Pour les boyaux naturels sal\u00e9s provenant de porcs</h3>\n\n<p>Je certifie par la pr\u00e9sente que\u00a0:</p>\n\n<p><strong>3.3.1</strong> Les boyaux intestinaux naturels et sal\u00e9s couverts par ce certificat sont strictement d\u00e9riv\u00e9s d'animaux abattus au Canada ou aux \u00c9tats-Unis et n'ont \u00e9t\u00e9 expos\u00e9s, en tout temps, pendant leur manipulation, transformation, emballage ou entreposage, \u00e0 tout produit ou sous-produit issu d'animaux de tout autre pays.</p>\n\n<h3>3.4. Pour tous les viandes et produits de viande de volaille et viandes et produits de viande provenant d'autres oiseaux transform\u00e9s autres que les produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves et/ou sachets st\u00e9rilis\u00e9s), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation</h3>\n\n<p>Je certifie par la pr\u00e9sente que\u00a0:</p>\n<p><strong>3.4.1</strong> Les produits de viande de volaille sont issus d'oiseaux qui ont \u00e9t\u00e9 \u00e9lev\u00e9s et nourris ensemble, ont re\u00e7u les m\u00eames traitements et ont \u00e9t\u00e9 envoy\u00e9s ensemble \u00e0 l'abattoir.</p>\n\n<p><strong>3.4.2</strong> Les produits de viande de volailles couvertes par le pr\u00e9sent certificat sont d\u00e9riv\u00e9s de volailles qui ont \u00e9t\u00e9 \u00e9tourdies pr\u00e9alablement \u00e0 leur abattage et abattus par des moyens respectueux du bien-\u00eatre animal.</p>\n \n<p><strong>3.4.3</strong> La viande de volaille a \u00e9t\u00e9 cuite\u00a0:</p>\n\n<p><strong>Soit</strong></p>\n\n<p><strong>3.4.3.1</strong> jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne d'au moins 65 degr\u00e9s Celsius durant 840 secondes;</p> \n\n<p><strong>Ou</strong></p> \n\n<p><strong>3.4.3.2</strong> jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne d'au moins 70 degr\u00e9s Celsius durant 574 secondes;</p> \n\n<p><strong>Ou</strong></p> \n\n<p><strong>3.4.2.3</strong> jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne d'au moins 74 degr\u00e9s Celsius durant 280 secondes;</p> \n\n<p><strong>Ou</strong></p>\n\n<p><strong>3.4.3.4</strong> jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne d'au moins 80 degr\u00e9s Celsius durant 203 secondes;</p> \n\n<p><strong>3.4.4</strong> Dans l'\u00e9tablissement de transformation, il y a une s\u00e9paration compl\u00e8te, physique et du personnel, entre les aires de produits de volaille crus et les aires de produits cuits et les produits de viande de volaille ont \u00e9t\u00e9 manipul\u00e9s de fa\u00e7on \u00e0 pr\u00e9venir une re-contamination (directe ou indirecte) des produits de viande de volaille cuits par des produits de viande de volaille crus.</p>\n\n<p><strong>3.4.5</strong> Toutes les pr\u00e9cautions ont \u00e9t\u00e9 prises afin de pr\u00e9venir tout contact direct ou indirect durant l'abattage, la manipulation, la transformation, le stockage et l'emballage des produits de viande de volaille avec tout produit ou sous-produit animal qui ne respecte pas les exigences du pr\u00e9sent certificat.</p>\n<h3>3.5. Pour tous les produits de viande issus de bovins ou de porcs autres que les produits de viande de longue conservation, les produits de viande de porc commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves et/ou sachets st\u00e9rilis\u00e9s), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation</h3>\n<p>\u00ab\u00a0Je certifie par la pr\u00e9sente que\u00a0:</p>\n<p><strong>3.5.1.</strong> Les produits c\u00e9r\u00e9aliers farcis d'ingr\u00e9dients<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup> \u00e0 base de viande destin\u00e9s \u00e0 l'exportation au Canada ont \u00e9t\u00e9 produits au d\u00e9but de la journ\u00e9e de production, apr\u00e8s la p\u00e9riode d'assainissement et \u00e0 la suite d'une inspection sanitaire pr\u00e9-op\u00e9rationnelle jug\u00e9e conforme par un repr\u00e9sentant officiel du Service d'inspection et de quarantaine de la Chine (SIQ).</p>\n<p><strong>3.5.1.2.</strong> La production des produits c\u00e9r\u00e9aliers farcis d'ingr\u00e9dients \u00e0 base de viande destin\u00e9s \u00e0 l'exportation au Canada s'est effectu\u00e9e sous la supervision du <abbr title=\"Service d'inspection et de quarantaine de la Chine\">SIQ</abbr>.</p>\n<p><strong>3.5.1.3.</strong> Toutes les pr\u00e9cautions ont \u00e9t\u00e9 prises durant la manipulation, la transformation, l'emballage et l'entreposage des ingr\u00e9dients \u00e0 base de viande crue ainsi que des produits c\u00e9r\u00e9aliers finis farcis d'ingr\u00e9dients \u00e0 base de viande afin de pr\u00e9venir tout contact direct ou indirect avec tout produit ou sous-produit animal qui ne respecte pas les exigences du pr\u00e9sent certificat.\u00a0\u00bb</p>\n<h3>3.6. Pour la viande et les produits de viande issus de viande de bovins et de porcs et de produits de viande import\u00e9s par la Chine</h3>\n<p>\u00ab\u00a0Je certifie par la pr\u00e9sente que\u00a0:</p>\n<p><strong>3.6.1.</strong> La viande et/ou les produits de viande\u00a0:</p>\n<p><strong>3.6.1.1.</strong> Ont \u00e9t\u00e9 l\u00e9galement import\u00e9s par la Chine;</p>\n<p><strong>3.6.1.2.</strong> Respectent toutes les exigences canadiennes visant l'importation commerciale de produits de viande conform\u00e9ment \u00e0 <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/pays-approuves/fra/1336318487908/1336319720090\">Pays pour lesquels l'importation commerciale de produits de viande est autoris\u00e9e</a>, comme s'ils \u00e9taient export\u00e9s directement au Canada \u00e0 partir du pays d'origine.</p>\n<p><strong>3.6.2.</strong> Toutes les pr\u00e9cautions ont \u00e9t\u00e9 prises afin de pr\u00e9venir tout contact direct ou indirect durant la transformation et l'emballage du produit de viande avec tout produit ou sous-produit animal qui ne respecte pas les exigences du pr\u00e9sent certificat.</p>\n<p><strong>3.6.3.</strong> Le certificat d'inspection des viandes accompagnant la viande import\u00e9e par la Chine est conserv\u00e9 au dossier \u00e0 l'\u00e9tablissement de transformation chinois \u00e0 des fins de v\u00e9rification pendant au moins deux ans suivant la date de l'importation.\u00a0\u00bb</p>\n<h2 id=\"a4\">4. Les \u00e9tablissements \u00e9ligibles pour exportation au Canada</h2>\n<p><strong>4.1.</strong>\u00a0Consultez la <a href=\"/active/netapp/meatforeign-viandeetranger/forlistf.aspx\">Liste des \u00e9tablissements des pays \u00e9trangers autoris\u00e9s \u00e0 exporter les produits de viande au Canada.</a></p>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Notes de bas de page</h2>\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p>Par exemple dumplings, rouleaux de printemps ou petits pains farcis.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}