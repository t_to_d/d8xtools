{
    "dcr_id": "1583266232843",
    "title": {
        "en": "Working at the \"plant hospital\"",
        "fr": "Travailler \u00e0 \u00ab\u00a0l'h\u00f4pital des plantes\u00a0\u00bb"
    },
    "html_modified": "2024-03-12 3:58:41 PM",
    "modified": "2019-04-27",
    "issued": "2020-03-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_chronicle_mar_2020_plant_hospital_1583266232843_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_chronicle_mar_2020_plant_hospital_1583266232843_fra"
    },
    "ia_id": "1583266233316",
    "parent_ia_id": "1565298312402",
    "chronicletopic": {
        "en": "Science and Innovation|Plant Health",
        "fr": "Science et Innovation|Sant\u00e9 des plantes"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Industry|Academia|Canadians",
        "fr": "Industrie|Acad\u00e9mie|Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565298312402",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Working at the \"plant hospital\"",
        "fr": "Travailler \u00e0 \u00ab\u00a0l'h\u00f4pital des plantes\u00a0\u00bb"
    },
    "label": {
        "en": "Working at the \"plant hospital\"",
        "fr": "Travailler \u00e0 \u00ab\u00a0l'h\u00f4pital des plantes\u00a0\u00bb"
    },
    "templatetype": "content page 1 column",
    "node_id": "1583266233316",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298312168",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/science-and-innovation/working-at-the-plant-hospital/",
        "fr": "/inspecter-et-proteger/science-et-innovation/travailler-a-l-hopital-des-plantes-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Working at the plant hospital",
            "fr": "Travailler \u00e0 \u00ab\u00a0l'h\u00f4pital des plantes\u00a0\u00bb"
        },
        "description": {
            "en": "One misty Island morning, as I climbed into a cab on my way to work, the driver asked where I was going.",
            "fr": "Par un matin brumeux sur l'\u00eele, alors que je grimpais dans un taxi pour me rendre au travail, le chauffeur me demande ma destination."
        },
        "keywords": {
            "en": "The CFIA Chronicle, C360, plant hospital, April 2019",
            "fr": "Les chroniques de l'ACIA, C360, l'h\u00f4pital des plantes, Avril 2019"
        },
        "dcterms.subject": {
            "en": "food inspection,food safety,government information,government publications,regulation,sciences,scientific research",
            "fr": "inspection des aliments,salubrit\u00e9 des aliments,information gouvernementale,publication gouvernementale,r\u00e9glementation,sciences,recherche scientifique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernment du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-03-06",
            "fr": "2020-03-06"
        },
        "modified": {
            "en": "2019-04-27",
            "fr": "2019-04-27"
        },
        "type": {
            "en": "news publication,reference material",
            "fr": "publication d'information\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Working at the \"plant hospital\"",
        "fr": "Travailler \u00e0 \u00ab\u00a0l'h\u00f4pital des plantes\u00a0\u00bb"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<p>By Jennifer Platts-Fanning and Shara Cody</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img alt=\"\u201cPlant hospital\u201d.\" class=\"img-thumbnail img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_chronicle_mar_2020_plant_hospital_600x600_1583259986748_eng.jpg\">\n</div>\n\n\n<p>This blog post was originally published to <a href=\"http://science.gc.ca/eic/site/063.nsf/eng/h_97679.html\">Cultivating Science</a> on science.gc.ca.</p>\n\n\n<blockquote>\n<p>One misty Island morning, as I climbed into a cab on my way to work, the driver asked where I was going. I responded in the Island way, simply, \"the big white building with the greenhouses.\" Immediately he exclaimed, \"Oh, the plant hospital!\" to which I replied smiling, \"It does look like a hospital, but we only diagnose and we don't treat the plants.\" The work performed in the incredible building has always been a topic of rumour around our little town.</p>\n</blockquote>\n\n\n<p><a href=\"http://science.gc.ca/eic/site/063.nsf/eng/97841.html\">Read the full blog post</a>.</p>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<p>Par Jennifer Platts-Fanning et Shara Cody</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img alt=\"\u00ab L'h\u00f4pital des plantes \u00bb.\" class=\"img-thumbnail img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_chronicle_mar_2020_plant_hospital_600x600_1583259986748_fra.jpg\">\n</div>\n\n\n<p>Ce billet de blogue est d'abord paru dans <a href=\"http://science.gc.ca/eic/site/063.nsf/fra/h_97679.html\">Cultiver la science</a> sur science.gc.ca.</p>\n\n\n<blockquote>\n<p>Par un matin brumeux sur l'\u00eele, alors que je grimpais dans un taxi pour me rendre au travail, le chauffeur me demande ma destination. Je lui r\u00e9ponds simplement, \u00e0 la fa\u00e7on des insulaires, \u00ab\u00a0au grand b\u00e2timent blanc avec les serres\u00a0\u00bb. Imm\u00e9diatement, il s'exclame \u00ab\u00a0Oh, \u00e0 l'h\u00f4pital des plantes!\u00a0\u00bb. Je lui r\u00e9ponds en souriant \u00ab\u00a0C'est vrai que \u00e7a ressemble \u00e0 un h\u00f4pital. En fait on y diagnostique les maladies des plantes, mais on ne les traite pas\u00a0\u00bb. Le travail r\u00e9alis\u00e9 dans ce b\u00e2timent incroyable a toujours \u00e9t\u00e9 l'objet de rumeurs dans notre petite ville.</p>\n</blockquote>\n\n\n<p><a href=\"http://science.gc.ca/eic/site/063.nsf/fra/97841.html\">Lisez le billet de blogue complet</a>!</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}