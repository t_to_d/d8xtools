{
    "dcr_id": "1537279328222",
    "title": {
        "en": "Appendix N: Clean in place (CIP)-type vacuum breaker",
        "fr": "Annexe N : Casse-vide de type nettoyage en place (NEP)"
    },
    "html_modified": "2024-02-13 9:53:17 AM",
    "modified": "2019-01-11",
    "issued": "2019-01-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_preventive_control_business_dairy_injectors_appN_1537279328222_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_preventive_control_business_dairy_injectors_appN_1537279328222_fra"
    },
    "ia_id": "1537279328471",
    "parent_ia_id": "1534954778164",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Making food - Manufacturing|processing|preparing|preserving|treating|Applying preventive controls|Maintaining equipment",
        "fr": "Production d'aliments - Fabrication|transformation|conditionnement|pr\u00e9servation et traitement|Application de mesures de contr\u00f4le pr\u00e9ventif|Entretien de l'\u00e9quipement"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1534954778164",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Appendix N: Clean in place (CIP)-type vacuum breaker",
        "fr": "Annexe N : Casse-vide de type nettoyage en place (NEP)"
    },
    "label": {
        "en": "Appendix N: Clean in place (CIP)-type vacuum breaker",
        "fr": "Annexe N : Casse-vide de type nettoyage en place (NEP)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1537279328471",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1534954777758",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/dairy-products/appendix-n/",
        "fr": "/controles-preventifs/produits-laitiers/annexe-n/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Appendix N: Clean in place (CIP)-type vacuum breaker",
            "fr": "Annexe N : Casse-vide de type nettoyage en place (NEP)"
        },
        "description": {
            "en": "This diagram illustrates the proper installation of a clean in place (CIP)-type vacuum breaker so that the air intake is not a source of contamination.",
            "fr": "Installation appropri\u00e9e d'un casse-vide de type nettoyage en place (NEP) de fa\u00e7on \u00e0 ce que l'entr\u00e9e d'air ne soit pas une source de contamination."
        },
        "keywords": {
            "en": "food, guidance, Making Food, SFCR, Applying Preventive Controls, Manufacturing, Processing, Preparing, Preserving, Treating, Dairy Products, Appendix N, proper installation of a clean in place (CIP)-type vacuum breaker",
            "fr": "aliments, salubrit\u00e9 des aliments, contr\u00f4le pr\u00e9ventif, entreprises, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, entreprises alimentaires, application de mesures de contr\u00f4le pr\u00e9ventif, installation d'un casse-vide de type nettoyage en place, NEP"
        },
        "dcterms.subject": {
            "en": "access to information,food,agri-food industry,agri-food products,dairy products,government publication,food safety",
            "fr": "acc\u00e8s \u00e0 l'information,aliment,industrie agro-alimentaire,produit agro-alimentaire,produit laitier,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-12",
            "fr": "2019-01-12"
        },
        "modified": {
            "en": "2019-01-11",
            "fr": "2019-01-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Appendix N: Clean in place (CIP)-type vacuum breaker",
        "fr": "Annexe N : Casse-vide de type nettoyage en place (NEP)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<figure> <img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_preventive_control_business_dairy_injectors_appN_img1_1537279659394_eng.jpg\" alt=\"image of a vacuum breaker. Description follows.\" class=\"img-responsive\"> <details> <summary>Description of image: Vacuum breaker</summary> <p>This diagram illustrates the proper installation of a clean in place (CIP)-type vacuum breaker so that the air intake is not a source of contamination. The components in the diagram are as follows:</p>\n<ul>\n<li>Constant level tank</li>\n<li>Plate heat exchanger</li>\n<li><abbr title=\"Clean in place\">CIP</abbr>-type Vacuum breaker Vacuum breaker's air intake at 30 cm (12 inches) above highest potential raw line</li>\n<li>Umbrella deflector</li>\n<li>Funnel</li>\n<li>Air gap that is 2 times the diameter of the vacuum breaker's air intake line</li>\n<li>CIP return line to constant level tank</li>\n<li>Pasteurized product tank</li>\n</ul></details> </figure>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<figure> <img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_preventive_control_business_dairy_injectors_appN_img1_1537279659394_fra.jpg\" alt=\"image d'un casse-vide de type nettoyage en place. Description ci-dessous.\" class=\"img-responsive\"> <details> <summary>Description de l'image - Casse-vide de type <abbr title=\"nettoyage en place\">NEP</abbr></summary> <p>Ce sch\u00e9ma montre une installation appropri\u00e9e d'un casse-vide de type nettoyage en place (NEP) de fa\u00e7on \u00e0 ce que l'entr\u00e9e d'air ne soit pas une source de contamination. Ci-dessous les composants de cette installation:</p>\n<ul>\n<li>R\u00e9servoir \u00e0 niveau constant</li>\n<li>\u00c9changeur \u00e0 plaques</li>\n<li>Casse-vide de type <abbr title=\"nettoyage en place\">NEP</abbr>; la prise d'air du casse-vide est situ\u00e9e \u00e0 30 centim\u00e8tres  (12 pouces) au-dessus du point le plus \u00e9lev\u00e9 susceptible d'\u00eatre atteint par le produit cru</li>\n<li>D\u00e9flecteur conique</li>\n<li>Entonnoir</li>\n<li>La coupure antiretour doit \u00eatre 2 fois le diam\u00e8tre de la ligne de la prise d'air du casse-vide</li>\n<li>Ligne de retour NEP au r\u00e9servoir \u00e0 niveau constant</li>\n<li>R\u00e9servoir du produit pasteuris\u00e9</li>\n</ul>\n</details> </figure>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}