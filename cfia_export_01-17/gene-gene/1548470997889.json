{
    "dcr_id": "1548470997889",
    "title": {
        "en": "What is in your bag of frozen berries? The science sleuths at CFIA take a closer look",
        "fr": "Qu'est-ce qui se trouve dans votre sac de petits fruits congel\u00e9s? Les d\u00e9tectives scientifiques de l'ACIA y regardent de plus pr\u00e8s"
    },
    "html_modified": "11-2-2019",
    "modified": "11-2-2019",
    "issued": "25-1-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/what_is_in_your_bag_of_frozen_berries_1548470997889_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/what_is_in_your_bag_of_frozen_berries_1548470997889_fra"
    },
    "parent_ia_id": "1495210111055",
    "ia_id": "1548470998123",
    "parent_node_id": "1495210111055",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "What is in your bag of frozen berries? The science sleuths at CFIA take a closer look",
        "fr": "Qu'est-ce qui se trouve dans votre sac de petits fruits congel\u00e9s? Les d\u00e9tectives scientifiques de l'ACIA y regardent de plus pr\u00e8s"
    },
    "label": {
        "en": "What is in your bag of frozen berries? The science sleuths at CFIA take a closer look",
        "fr": "Qu'est-ce qui se trouve dans votre sac de petits fruits congel\u00e9s? Les d\u00e9tectives scientifiques de l'ACIA y regardent de plus pr\u00e8s"
    },
    "templatetype": "content page 1 column",
    "node_id": "1548470998123",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1495210110540",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-research-and-publications/what-is-in-your-bag-of-frozen-berries-/",
        "fr": "/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/sac-de-petits-fruits-congeles/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "What is in your bag of frozen berries? The science sleuths at CFIA take a closer look",
            "fr": "Qu'est-ce qui se trouve dans votre sac de petits fruits congel\u00e9s? Les d\u00e9tectives scientifiques de l'ACIA y regardent de plus pr\u00e8s"
        },
        "description": {
            "en": "'s easy to ignore the details behind the food we eat: where it was made, by whom and how safe it is.",
            "fr": "Il est facile de ne pas pr\u00eater attention aux d\u00e9tails qui se cachent derri\u00e8re les aliments que nous consommons\u00a0: o\u00f9 ont-ils \u00e9t\u00e9 produits, par qui, et \u00e0 quel point sont-ils salubres."
        },
        "keywords": {
            "en": "science, food, animal, plant, laboratories, veterinarians, scientists, research, What is in your bag of frozen berries, sleuths, CFIA",
            "fr": "science, aliments, animaux, v\u00e9g\u00e9taux, laboratoires, v\u00e9t\u00e9rinaires, scientistes, recherche, Qu'est-ce qui se trouve dans votre sac de petits fruits congel\u00e9s, d\u00e9tectives, scientifiques, ACIA"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Science",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Science"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "What is in your bag of frozen berries? The science sleuths at CFIA take a closer look",
        "fr": "Qu'est-ce qui se trouve dans votre sac de petits fruits congel\u00e9s? Les d\u00e9tectives scientifiques de l'ACIA y regardent de plus pr\u00e8s"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>It's easy to ignore the details behind the food we eat: where it was made, by whom and how safe it is. As long as it looks good on Instagram, we assume it's safe to chow down. It's only when food recalls happen that the safety of our food is propelled into the spotlight, making those with even the most adventurous palette second guess the foods they love. A common reason for a food-related illness is as simple as not following cooking instructions or not cleaning food before it is consumed. Other times, even the best personal food handling practices can't prevent an illness. When people start getting sick, the scientific sleuths at the Canadian Food Inspection Agency's (CFIA) laboratories jump into action.</p>\n\n<p>You may be aware of CFIA announcing outbreaks and recalls to the public. Among them was a norovirus outbreak in 2017 that was linked to frozen berries. It was the team of scientific detectives at the CFIA's Saint-Hyacinthe laboratory, or St-Hy, as it is known, that got to the bottom of the bag.</p>\n\n<p>The food mystery started in March\u00a02017, when small clusters of a norovirus outbreak were found in seniors residences in Quebec. With suspicions that the outbreak was food related, Quebec's food safety department, the Minist\u00e8re de l'agriculture, des p\u00eacheries et de l'alimentation du Qu\u00e9bec (MAPAQ), contacted the CFIA, home to St-Hy, one of the\u00a0 few laboratories in Canada able to effectively test for foodborne viruses . Since viruses do not easily multiply outside of a host, without food or a human for a virus to live off of, a virus will simply fail to multiply, making molecular techniques for detection of viral parts the only option. For illnesses like these, provincial and territorial partners will sometimes request the CFIA's support to test large quantities of food during illness investigations. This huge quantity makes the analysis much more complicated than a detective show would imply. Nevertheless, the science sleuths at St-Hy got down to business and took action to investigate the source of the food poisoning with MAPAQ.</p>\n\n<h2>Teamwork</h2>\n\n<p>Several bags of frozen berries arrived at the CFIA's laboratory in Saint-Hyacinthe in early June. Scientists immediately began testing them, using methods developed at CFIA's Food Virology National Reference Centre (FVNRC) located at the St-Hy's laboratory. These methods included looking for tell-tale viral molecules. Within a few days of arriving at the Reference Centre, scientists confirmed that the berries carried norovirus, a common stomach bug. This led CFIA to issue a total of 14\u00a0recalls between June\u00a020 and <span class=\"nowrap\">August 21, 2017</span>, for the berries, as well as other products, such as yogurt, mousse, cakes and other pastries.</p>\n\n<p>From March to August that year, 724\u00a0cases of norovirus were reported to health authorities in Quebec. The actual number of people who became sick is likely much higher, since not all cases of food poisoning are reported. But teamwork and collaboration helps to contain the spread of illness.</p>\n\n<h2>What happened next?</h2>\n\n<p>Learning from this outbreak and others is an important step for CFIA to uphold food safety standards. After the successful provincial-federal collaboration, an agreement was signed giving four MAPAQ analysts the opportunity to visit St-Hy for a three-day training session on CFIA foodborne virus investigations, equipment, laboratory layout and best practices. The CFIA's St-Hy continues to support foodborne virus-related illness investigations to strengthen response readiness in Canada.</p>\n\n<h2>Is it safe to eat?</h2>\n\n<p>Taking the plunge (or the bite) requires trust that the food on our shelves is safe to eat. While sometimes looks can be deceiving, especially on social media, scientists at CFIA use more than just pretty pictures to ensure the safety of Canadian food. The experts use the latest tools, and often develop new ones, based on molecular and genetic detection methods to track down disease-causing agents. The work done by scientists helps keep all of us safe. The public also has a key role to play in being safe; through safe food handling and preparation as well as <a href=\"/about-cfia/media-relations/stay-connected/eng/1299856061207/1299856119191\">staying connected</a> with the CFIA for up-to-date information and recalls so they can ensure that the food on their plate is both Instagram-worthy AND stomach-worthy.</p>\n\n<h2>Want to know more?</h2>\n\n<ul class=\"lst-spcd\">\n\n<li>Listen to our podcast with <a href=\"/inspect-and-protect/science-and-innovation/dr-emilie-larocque/eng/1572621040261/1573240128498\">Dr.\u00a0<span lang=\"fr\">\u00c9milie Larocque</span>, Virologist, Saint-Hyacinthe Laboratory</a></li>\n\n<li>Learn about <a href=\"/science-and-research/our-research-and-publications/using-dna-science-to-fight-food-borne-illness/eng/1511282633357/1511282633638\">Using DNA science to fight food-borne illness</a> (fact sheet)</li>\n\n<li><strong>Find out what is happening at other</strong> <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a></li>\n\n<li><a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">Learn more about CFIA food safety programs</a></li>\n\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Il est facile de ne pas pr\u00eater attention aux d\u00e9tails qui se cachent derri\u00e8re les aliments que nous consommons\u00a0: o\u00f9 ont-ils \u00e9t\u00e9 produits, par qui, et \u00e0 quel point sont-ils salubres. Tant que notre plat para\u00eet bien sur <span lang=\"en\">Instagram</span>, nous pr\u00e9sumons que nous pouvons le consommer sans crainte. C'est seulement lorsque des rappels surviennent que la salubrit\u00e9 de nos aliments se retrouve sous les projecteurs, et ces rappels font en sorte que m\u00eame les fourchettes les plus aventureuses regardent d'un autre \u0153il les aliments qu'ils aiment. Souvent, les maladies d'origine alimentaire sont simplement caus\u00e9es par des aliments qui ne sont pas pr\u00e9par\u00e9s selon les instructions de cuisson ou qui ne sont pas lav\u00e9s avant d'\u00eatre consomm\u00e9s. Mais parfois, m\u00eame les meilleures pratiques de manipulation des aliments ne permettent pas de pr\u00e9venir une maladie. Lorsque des cas de maladie commencent \u00e0 se d\u00e9clarer, les d\u00e9tectives scientifiques des laboratoires de l'Agence canadienne d'inspection des aliments (ACIA) passent \u00e0 l'action.</p>\n\n<p>Vous avez peut-\u00eatre d\u00e9j\u00e0 vu les avis de l'ACIA destin\u00e9s au public concernant les \u00e9closions et les rappels. Il y a notamment eu une \u00e9closion de norovirus en 2017 li\u00e9e \u00e0 des petits fruits congel\u00e9s. C'est l'\u00e9quipe de d\u00e9tectives scientifiques du laboratoire de Saint-Hyacinthe de l'ACIA, couramment appel\u00e9 le labo de St-Hy, qui est all\u00e9 au fond de cette affaire.</p>\n\n<p>L'\u00e9nigme alimentaire a d\u00e9but\u00e9 en mars\u00a02017, lorsque de petites grappes de cas de norovirus ont \u00e9t\u00e9 observ\u00e9es dans des r\u00e9sidences pour personnes \u00e2g\u00e9es au Qu\u00e9bec. Soup\u00e7onnant que l'\u00e9closion \u00e9tait d'origine alimentaire, l'entit\u00e9 responsable de la salubrit\u00e9 alimentaire de la province, le minist\u00e8re de l'Agriculture, des P\u00eacheries et de l'Alimentation du Qu\u00e9bec (MAPAQ), a communiqu\u00e9 avec le labo de St-Hy de l'ACIA, l'un des rares laboratoires au Canada capable d'effectuer les essais de d\u00e9pistage des virus d'origine alimentaire. Les virus ne se multiplient pas facilement \u00e0 l'ext\u00e9rieur d'un h\u00f4te; sans un aliment ou un humain qu'il peut utiliser, un virus ne peut tout simplement pas se multiplier, de sorte que les techniques mol\u00e9culaires sont le seul moyen de d\u00e9tection pouvant \u00eatre utilis\u00e9. Dans des cas d'\u00e9closion comme celui-l\u00e0, les partenaires provinciaux et territoriaux demandent parfois l'aide de l'ACIA pour faire l'analyse de grandes quantit\u00e9s d'aliments dans le cadre des enqu\u00eates sur les maladies. Ces grandes quantit\u00e9s rendent l'analyse beaucoup plus complexe que ce qui serait d\u00e9peint dans une \u00e9mission de d\u00e9tectives. N\u00e9anmoins, les d\u00e9tectives scientifiques de St-Hy se sont mis au travail et ont enqu\u00eat\u00e9 sur la source de l'intoxication alimentaire conjointement avec le MAPAQ.</p>\n\n<h2>Travail d'\u00e9quipe</h2>\n\n<p>Plusieurs sacs de petits fruits congel\u00e9s ont \u00e9t\u00e9 envoy\u00e9s au laboratoire de l'ACIA de Saint-Hyacinthe au d\u00e9but juin. Les scientifiques ont imm\u00e9diatement commenc\u00e9 \u00e0 les analyser, au moyen de m\u00e9thodes mises au point par le Centre de r\u00e9f\u00e9rence national sur la virologie alimentaire, situ\u00e9 au labo de St-Hy. Ces m\u00e9thodes consistaient notamment \u00e0 chercher des mol\u00e9cules indicatrices de la pr\u00e9sence de virus. Quelques jours apr\u00e8s l'arriv\u00e9e des petits fruits au Centre de r\u00e9f\u00e9rence, les scientifiques ont pu confirmer que ceux-ci renfermaient un norovirus, un pathog\u00e8ne intestinal courant. Cette d\u00e9couverte a men\u00e9 l'ACIA \u00e0 effectuer, du 20\u00a0juin au <span class=\"nowrap\">21 ao\u00fbt 2017</span>, 14\u00a0rappels visant des petits fruits ainsi que d'autres produits, notamment des yogourts, des mousses, des g\u00e2teaux et d'autres p\u00e2tisseries.</p>\n\n<p>De mars \u00e0 ao\u00fbt\u00a02017, 724\u00a0cas de norovirus ont \u00e9t\u00e9 signal\u00e9s aux autorit\u00e9s sanitaires du Qu\u00e9bec. Le nombre r\u00e9el de personnes ayant \u00e9t\u00e9 malades est probablement beaucoup plus \u00e9lev\u00e9, puisque ce ne sont pas tous les cas d'intoxication alimentaire qui sont signal\u00e9s. Toutefois, le travail d'\u00e9quipe et la collaboration ont permis de limiter la propagation de la maladie.</p>\n\n<h2>Que s'est-il pass\u00e9 ensuite?</h2>\n\n<p>Il est important pour l'ACIA de tirer des le\u00e7ons de cette \u00e9closion et des autres cas pour veiller au respect des normes en mati\u00e8re de salubrit\u00e9 alimentaire. Apr\u00e8s cette fructueuse collaboration provinciale-f\u00e9d\u00e9rale, une entente a \u00e9t\u00e9 sign\u00e9e pour que quatre analystes du MAPAQ puissent se rendre au labo de St-Hy pour une s\u00e9ance de formation de trois jours concernant les m\u00e9thodes d'enqu\u00eate de l'ACIA en cas de maladies d'origine alimentaire caus\u00e9es par des virus, l'\u00e9quipement, l'am\u00e9nagement du laboratoire et les pratiques exemplaires. Le labo de St-Hy de l'ACIA continue d'appuyer les enqu\u00eates relatives aux maladies d'origine alimentaire caus\u00e9es par des virus pour am\u00e9liorer l'\u00e9tat de pr\u00e9paration au Canada.</p>\n\n<h2>Peut-on se nourrir sans danger?</h2>\n\n<p>Il faut avoir confiance en la salubrit\u00e9 des aliments qui se trouvent sur les \u00e9talages pour pouvoir se nourrir. L'apparence peut parfois \u00eatre d\u00e9cevante, particuli\u00e8rement sur les m\u00e9dias sociaux, mais les scientifiques de l'ACIA utilisent plus que de jolies photos pour s'assurer de la salubrit\u00e9 des aliments consomm\u00e9s par les Canadiens. Les experts utilisent les outils les plus r\u00e9cents et cr\u00e9ent m\u00eame souvent de nouveaux outils, en se servant de m\u00e9thodes de d\u00e9tection mol\u00e9culaires et g\u00e9n\u00e9tiques pour d\u00e9pister les agents pathog\u00e8nes. Le travail des scientifiques contribue \u00e0 assurer notre s\u00e9curit\u00e9 \u00e0 tous. De plus, les citoyens ont un important r\u00f4le \u00e0 jouer pour leur propre s\u00e9curit\u00e9, en manipulant et en pr\u00e9parant leurs aliments de fa\u00e7on s\u00e9curitaire ainsi qu'en <a href=\"/a-propos-de-l-acia/relations-avec-les-medias/restez-branches/fra/1299856061207/1299856119191\">restant branch\u00e9s</a> aux nouvelles de l'ACIA, pour recevoir les derniers renseignements et les avis de rappel qui leur permettront de s'assurer que les aliments qui se retrouvent dans leur assiette sont tout aussi dignes de leur compte Instagram que de se retrouver dans leur estomac.</p>\n\n<h2>Souhaitez-vous en savoir plus?</h2>\n\n<ul class=\"lst-spcd\">\n\n<li>\u00c9coutez notre balado avec <a href=\"/inspecter-et-proteger/science-et-innovation/dre-emilie-larocque/fra/1572621040261/1573240128498\">\u00c9milie Larocque, Ph.\u00a0D., virologue au laboratoire de Saint-Hyacinthe</a></li>\n\n<li>Apprenez-en plus sur l'<a href=\"/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/recourir-a-la-science-de-l-adn-pour-lutter-contre-/fra/1511282633357/1511282633638\">utilisation de la science de l'ADN pour pr\u00e9venir les maladies d'origine alimentaire</a> (fiche de renseignements)</li>\n\n<li><strong>D\u00e9couvrez ce qui se passe dans d'autres</strong> <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a></li>\n\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">D\u00e9couvrez plus sur les programmes de salubrit\u00e9 alimentaire de l'ACIA</a></li>\n\n</ul>\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}