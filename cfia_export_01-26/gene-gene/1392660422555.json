{
    "dcr_id": "1392660422555",
    "title": {
        "en": "1. About This Document",
        "fr": "1. \u00c0 propos de ce document"
    },
    "html_modified": "2024-01-26 2:22:17 PM",
    "modified": "2014-02-17",
    "issued": "2014-04-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_nd_hazard_plan_mod1_1392660422555_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_nd_hazard_plan_mod1_1392660422555_fra"
    },
    "ia_id": "1392660499045",
    "parent_ia_id": "1392670192661",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1392670192661",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "1. About This Document",
        "fr": "1. \u00c0 propos de ce document"
    },
    "label": {
        "en": "1. About This Document",
        "fr": "1. \u00c0 propos de ce document"
    },
    "templatetype": "content page 1 column",
    "node_id": "1392660499045",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1392670129624",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/nd/hazard-specific-plan/about-this-document/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mn/plan-specifiquement-lie-aux-risques/a-propos-de-ce-document/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "1. About This Document",
            "fr": "1. \u00c0 propos de ce document"
        },
        "description": {
            "en": "The Newcastle Disease Hazard Specific Plan (NDHSP) replaces the previous Part D of the Foreign Animal Disease Manual of Procedures (FAD MOP), Newcastle Disease Strategy.",
            "fr": "LePlan sp\u00e9cifiquement li\u00e9 au risque de la maladie de Newcastle (PSLRMN) remplace la partie D du Manuel des proc\u00e9dures concernant les maladies animales exotiques (MDP-MAE), Strat\u00e9gie de lutte contre la maladie de Newcastle."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, Reportable Diseases Regulations, reportable diseases, Newcastle Disease, avian species, birds, management plan",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, R\u00e8glement sur les maladies d\u00e9clarables, maladies \u00e0 d\u00e9claration obligatoire, Maladie de Newcastle, esp\u00e8ces aviaires, oiseaux, plan de gestion"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-04-15",
            "fr": "2014-04-15"
        },
        "modified": {
            "en": "2014-02-17",
            "fr": "2014-02-17"
        },
        "audience": {
            "en": "business,general public",
            "fr": "entreprises,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "1. About This Document",
        "fr": "1. \u00c0 propos de ce document"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 id=\"a11\">1.1 Context</h2>\n<p>The <i>Newcastle Disease Hazard Specific Plan</i> (NDHSP) replaces the previous Part D of the <i>Foreign Animal Disease Manual of Procedures </i>(FAD\u2011MOP), Newcastle Disease Strategy.</p>\n<p>This plan is not a stand-alone document; rather, it is part of an overall management plan used by the Canadian Food Inspection Agency (CFIA) to respond to an incursion of any animal disease requiring an emergency response in Canada.</p>\n<p>For the purpose of this document, Newcastle disease (ND) refers to the velogenic form of the disease.</p>\n<h2 id=\"a12\">1.2 Purpose</h2>\n<p>To ensure a consistent response by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> when there is suspicion of a developing outbreak of <abbr title=\"Newcastle disease\">ND</abbr> or when an outbreak occurs.</p>\n<h2 id=\"a13\">1.3 Audience</h2>\n<p>This document is intended to provide <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> animal health emergency responders with <abbr title=\"Newcastle disease\">ND</abbr>-specific information that is necessary for the control and eradication of the disease when an outbreak occurs.</p>\n<h2 id=\"a14\">1.4 Authority</h2>\n<p>This policy is under the authority of the <i>Health of Animals Act (1990)</i>.</p>\n<h2 id=\"a15\">1.5 Related Documents</h2>\n<p>The following documents provide information on emergency response organization and procedures related to the <abbr title=\"Newcastle Disease Hazard Specific Plan\">NDHSP</abbr>:</p>\n<ul>\n<li>\n<i>\n<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Emergency Response Plan;</i>\n</li>\n<li>\n<i>\n<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Animal Health Functional Plan </i>(AHFP);</li>\n<li>\n<i>Common Procedures Manual;</i>\n</li>\n<li>\n<i>National Newcastle Disease Operating Policy and Procedures for Canadian Animal Health Surveillance Network Laboratoriess; and</i>\n</li>\n<li>\n<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Response Plans and agreements with provincial and industry stakeholders that provide emergency support for foreign animal disease (FAD) eradication (<abbr title=\"that is to say\">i.e.</abbr>\n<i>Foreign Animal Disease Emergency Support</i> [FADES] <i>Plans</i>).</li>\n</ul>\n<h2 id=\"a16\">1.6 Amendments and Revisions</h2>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will review the <abbr title=\"Newcastle Disease Hazard Specific Plan\">NDHSP</abbr> every second year.</p>\n<h2 id=\"a17\">1.7 Contact Information</h2>\n<p>\n<abbr title=\"Doctor\">Dr</abbr> Abed Harchaoui, Foreign Animal Disease<br>\n<br>\n<br>\n<br>\n<abbr title=\"Ontario\">ON</abbr> K1A OY9</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 id=\"a11\">1.1 Contexte</h2>\n<p>Le Plan sp\u00e9cifiquement li\u00e9 au risque de la maladie de Newcastle (PSLRMN) remplace la partie D du <i>Manuel des proc\u00e9dures concernant les maladies animales exotiques</i> (MDP-MAE), Strat\u00e9gie de lutte contre la maladie de Newcastle.</p>\n<p>Ce plan n'est pas un document ind\u00e9pendant; il fait partie d'un plan de gestion global mis en \u0153uvre par l'Agence canadienne de l'inspection des aliments (ACIA) pour lutter contre l'incursion d'une maladie animale n\u00e9cessitant une intervention d'urgence au Canada.</p>\n<p>Dans le pr\u00e9sent document, maladie de Newcastle (MN) d\u00e9signe la forme v\u00e9log\u00e8ne de la maladie.</p>\n<h2 id=\"a12\">1.2 Objet</h2>\n<p>Le pr\u00e9sent document a pour objet d'uniformiser l'approche adopt\u00e9e par l'<abbr title=\"Agence canadienne de l'inspection des aliments\">ACIA</abbr> en cas d'\u00e9closion de la maladie de Newcastle soup\u00e7onn\u00e9e ou av\u00e9r\u00e9e.</p>\n<h2 id=\"a13\">1.3 Port\u00e9e</h2>\n<p>Le pr\u00e9sent document vise \u00e0 fournir au personnel de l'<abbr title=\"Agence canadienne de l'inspection des aliments\">ACIA</abbr> charg\u00e9 d'intervenir en cas d'urgence en mati\u00e8re de sant\u00e9 animale, les renseignements sp\u00e9cifiques \u00e0 la maladie de Newcastle (MN) n\u00e9cessaire au contr\u00f4le et \u00e0 l'\u00e9radication de cette maladie en cas d'\u00e9closion.</p>\n<h2 id=\"a14\">1.4 Fondement l\u00e9gislatif</h2>\n<p>La politique \u00e9nonc\u00e9e dans ce document rel\u00e8ve de la <i>Loi sur la sant\u00e9 des animaux (1990)</i>.</p>\n<h2 id=\"a15\">1.5 Documents connexes</h2>\n<p>Les documents \u00e9num\u00e9r\u00e9s ci-apr\u00e8s contiennent des renseignements sur l'organisation des interventions d'urgence et sur les proc\u00e9dures relatives \u00e0 la strat\u00e9gie d\u00e9crite ici :</p>\n<ul>\n<li>\n<i>Plan d'intervention en cas d'urgence de l'<abbr title=\"Agence canadienne de l'inspection des aliments\">ACIA</abbr>\u00a0;</i>\n</li>\n<li>\n<i>Plan fonctionnel pour la sant\u00e9 animale (PFSA)\u00a0;</i>\n</li>\n<li>\n<i>Manuel des proc\u00e9dures communes\u00a0;</i>\n</li>\n<li>\n<i>Politiques et proc\u00e9dures en mati\u00e8re de d\u00e9pistage de la maladie de Newcastle \u00e0 l'intention des laboratoires du R\u00e9seau canadien de surveillance zoosanitaire; et</i>\n</li>\n<li>Plans d'intervention pour les Centres op\u00e9rationnels de l'<abbr title=\"Agence canadienne de l'inspection des aliments\">ACIA</abbr> et ententes conclues avec les intervenants provinciaux et industriels qui procurent un soutien lors d'urgence pour l'\u00e9radication des maladies animales exotiques (MAE) (<abbr title=\"c'est-\u00e0-dire\">c.-\u00e0-d.</abbr> Plan de soutien d'urgence contre les maladies animales exotiques [PSUMAE]).</li>\n</ul>\n<h2 id=\"a16\">1.6 R\u00e9visions et modifications</h2>\n<p>L'<abbr title=\"Agence canadienne de l'inspection des aliments\">ACIA</abbr> r\u00e9vise le <abbr title=\"Plan sp\u00e9cifiquement li\u00e9 au risque de la maladie de Newcastle\">PSLRMN</abbr> tous les deux ans.</p>\n<h2 id=\"a17\">1.7 Personnes-ressources</h2>\n<p>\n<abbr title=\"Docteur\">Dr</abbr> Abed Harchaoui<br> V\u00e9t\u00e9rinaire principale, Maladies animales exotiques<br> Division de la sant\u00e9, du bien-\u00eatre et de la bios\u00e9curit\u00e9 des animaux<br> Agence canadienne d'inspection des aliments<br> 59, promenade Camelot<br> Ottawa (Ontario) K1A OY9</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}