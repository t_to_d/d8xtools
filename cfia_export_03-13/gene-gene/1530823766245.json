{
    "dcr_id": "1530823766245",
    "title": {
        "en": "Biosecurity recommendations for rabbits",
        "fr": "Recommandations sur la bios\u00e9curit\u00e9 pour les lapins"
    },
    "html_modified": "2024-03-12 3:58:07 PM",
    "modified": "2023-04-21",
    "issued": "2023-03-23",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_gnl_biosecurity_rabbits_1530823766245_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_gnl_biosecurity_rabbits_1530823766245_fra"
    },
    "ia_id": "1530823766513",
    "parent_ia_id": "1530824799683",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1530824799683",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biosecurity recommendations for rabbits",
        "fr": "Recommandations sur la bios\u00e9curit\u00e9 pour les lapins"
    },
    "label": {
        "en": "Biosecurity recommendations for rabbits",
        "fr": "Recommandations sur la bios\u00e9curit\u00e9 pour les lapins"
    },
    "templatetype": "content page 1 column",
    "node_id": "1530823766513",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1530824799434",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/immediately-notifiable/rhd/biosecurity-recommendations/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/mhl/recommandations-sur-la-biosecurite/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biosecurity recommendations for rabbits",
            "fr": "Recommandations sur la bios\u00e9curit\u00e9 pour les lapins"
        },
        "description": {
            "en": "Biosecurity measures are practices intended to reduce the spread of infectious diseases and are essential in protecting animal health.",
            "fr": "Les mesures de bios\u00e9curit\u00e9 sont des pratiques destin\u00e9es \u00e0 r\u00e9duire la propagation des maladies infectieuses et sont un \u00e9l\u00e9ment essentiel \u00e0 la protection de la sant\u00e9 animale."
        },
        "keywords": {
            "en": "Health of Animals Act, Statement, Rabbit haemorrhagic disease, RHD, Biosecurity measures, General biosecurity recommendations for rabbits, infectious diseases, protecting animal health",
            "fr": "R\u00e8glement sur la sant\u00e9 des animaux, \u00c9nonc\u00e9, maladie h\u00e9morragique virale du lapin, MHL, recommandations g\u00e9n\u00e9rales sur la bios\u00e9curit\u00e9 pour les lapins, mesures de bios\u00e9curit\u00e9, propagation des maladies infectieuses, protection de la sant\u00e9 animale"
        },
        "dcterms.subject": {
            "en": "animal health,animal inspection,inspection,livestock,pests,regulation,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection des animaux,inspection,b\u00e9tail,organisme nuisible,r\u00e9glementation,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-03-23",
            "fr": "2023-03-23"
        },
        "modified": {
            "en": "2023-04-21",
            "fr": "2023-04-21"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biosecurity recommendations for rabbits",
        "fr": "Recommandations sur la bios\u00e9curit\u00e9 pour les lapins"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Biosecurity measures are practices intended to reduce the spread of infectious diseases and are essential in protecting animal health. Rabbit breeders and owners are encouraged to adopt the following biosecurity measures to reduce the risk of the spread of many infectious diseases in rabbits, including rabbit haemorrhagic disease (RHD).</p>\n\n<p>Additional advice is available in the <a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/proactive-management/eng/1374175296768/1374176128059?chap=0\">National Farm-Level Biosecurity Planning Guide Proactive Management of Animal Resources</a>.</p>\n\n<p>RHD is a sudden onset, highly contagious and fatal viral disease of lagomorphs (rabbits and hares). There are two main genotypes of the virus, both of which have been reported in Canada. RHDV1 was found in Manitoba in 2011 and has not been identified since. Since then, only RHDV2 has been found in Canada.</p>\n\n<p>RHDV2 affects several species of rabbits and hares, including both captive and feral European rabbits, from which our own domestic rabbits are descended. It also affects several species of wild rabbits and hares that are indigenous to Canada. High rates of illness and death can occur in exposed rabbits.</p>\n\n<p>The virus spreads among rabbits and hares through secretions and excretions including those from runny eyes/noses, saliva, urine, feces as well as contaminated bedding, fur, food and water. It can also be spread by humans, wildlife and insects on contaminated clothing, fur, and other surfaces. The virus can survive for long periods of time in the environment and remain infectious to animals.</p>\n\n<p>The disease does not affect humans and is not known to affect other animals. In Canada RHD is a federally immediately notifiable disease; laboratories are required to notify the Canadian Food Inspection Agency (CFIA) of suspected or diagnosed of the disease.</p>\n\n<p>Some key biosecurity measures include the following.</p>\n\n<h2>People and equipment</h2>\n\n<ul>\n<li>Minimize access to the premises and restrict contact with rabbits to only those people necessary for their care</li>\n<li>Post biosecurity signs to advise visitors that access to the property and animals is restricted</li>\n<li>Lock gates and doors to secure access to animal housing areas</li>\n<li>Require that all essential visitors (for example, veterinarians or service personnel):\n<ul>\n<li>obtain approval before visiting</li>\n<li>understand and implement established biosecurity protocols</li>\n<li>fill out a visitor log</li>\n<li>be accompanied by owner or person responsible for animal</li>\n</ul></li>\n</ul>\n\n<p>Avoid non-essential visitor contact with rabbits; if this is unavoidable, employ these practices:</p>\n\n<ul>\n<li>Wash or sanitize hands, clean and disinfect boots and wear clothing dedicated to the farm or premises before caring for rabbits</li>\n<li>Do not share equipment with other rabbit breeders or owners</li>\n<li>Clean and disinfect equipment, waterers, feeders and other items coming into contact with rabbits</li>\n<li>Follow the directions supplied by the disinfectant manufacturer and rinse waterers and feeders thoroughly before refilling</li>\n<li>Avoid travel to areas experiencing disease outbreaks</li>\n</ul>\n\n<h2>Animals</h2>\n\n<ul>\n<li>Monitor rabbits at least once a day for signs of illness, including:\n<ul>\n<li>difficulty breathing, loss of coordination, reduced appetite, and reduced activity</li>\n<li>bleeding from the nose, blood in the feces, hemorrhages in the eye</li>\n<li>sudden death with few clinical signs</li>\n</ul></li>\n<li>Consider vaccinating animals against the disease and discuss with a veterinarian</li>\n<li>Prevent rabbits from having contact with other domestic or wild rabbits/hares, and other animals</li>\n<li>Manage and minimize exposure to insects</li>\n<li>Manage and minimize the use of outdoor exercise areas for rabbits</li>\n<li>Consider disease risks when attending rabbit shows or fairs due to exposure to potentially sick animals</li>\n<li>Limit the introduction of new rabbits: rabbits that appear healthy can be infected and pose a risk to resident animals</li>\n<li>Isolate all returning show and new rabbits from contact with resident animals for at least 14 days to ensure they are healthy: to protect specifically against RHD, isolate for 60 days </li>\n<li>During this isolation period, manage rabbits separately:\n<ul>\n<li>provide care for the isolated rabbits only after handling the resident animals</li>\n<li>prevent physical and indirect contact between show and resident animals (e.g. do not use the same equipment)</li>\n<li>closely monitor the health status of isolated and resident animals during the isolation period</li>\n<li>seek veterinary guidance if there are signs of disease</li>\n</ul></li>\n</ul>\n\n<h2>Feed, water, bedding</h2>\n\n<ul>\n<li>Obtain feed from suppliers with quality control programs</li>\n<li>Do not collect and use wild plants as a food source</li>\n<li>Obtain all feed and bedding from RHDV2-free jurisdictions or store hay for at least 8 months prior to use</li>\n<li>Use municipal water sources as surface water sources and shallow wells are not recommended due to the increased risk of contamination</li>\n<li>Protect feed and bedding from contamination by storing them indoors or in tightly sealed containers</li>\n<li>Clean up feed spills</li>\n</ul>\n\n<h2 class=\"font-medium black\">Additional information</h2>\n<ul>\n<li><a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a></li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les mesures de bios\u00e9curit\u00e9 sont des pratiques destin\u00e9es \u00e0 r\u00e9duire la propagation des maladies infectieuses et sont un \u00e9l\u00e9ment essentiel \u00e0 la protection de la sant\u00e9 animale. Les \u00e9leveurs et les propri\u00e9taires de lapins sont encourag\u00e9s \u00e0 adopter les mesures de bios\u00e9curit\u00e9 ci-dessous afin de r\u00e9duire le risque de propagation de nombreuses maladies infectieuses chez les lapins, y compris la maladie h\u00e9morragique du lapin (MHL).</p>\n\n<p>Vous trouverez des conseils additionnels \u00e0 l'adresse suivante\u00a0: <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/gestion-proactive/fra/1374175296768/1374176128059?chap=0\">Guide de planification nationale pour la bios\u00e9curit\u00e9 \u00e0 la ferme\u00a0\u2013\u00a0Gestion proactive des ressources animales</a>.</p>\n\n<p>La MHL est une maladie virale tr\u00e8s contagieuse et mortelle, qui appara\u00eet soudainement chez les lagomorphes (lapins et li\u00e8vres). Il existe deux principaux g\u00e9notypes du virus, qui ont tous deux \u00e9t\u00e9 d\u00e9clar\u00e9s au Canada. Le virus de la maladie h\u00e9morragique du lapin (VMHL) de type 1 a \u00e9t\u00e9 trouv\u00e9 au Manitoba en 2011 et n'a pas \u00e9t\u00e9 d\u00e9tect\u00e9 depuis. Depuis lors, seul le VMHL de type 2 a \u00e9t\u00e9 d\u00e9tect\u00e9 au Canada.</p>\n\n<p>Le VMHL de type 2 touche plusieurs esp\u00e8ces de lapins et de li\u00e8vres, y compris les lapins europ\u00e9ens captifs et sauvages, dont sont issus nos propres lapins domestiques. Il peut \u00e9galement affecter plusieurs esp\u00e8ces de lapins et de li\u00e8vres sauvages indig\u00e8nes au Canada. Des taux \u00e9lev\u00e9s de maladies et de d\u00e9c\u00e8s peuvent survenir chez les lapins expos\u00e9s.</p>\n\n<p>Le virus se propage chez les lapins et les li\u00e8vres par le biais de s\u00e9cr\u00e9tions et d'excr\u00e9tions, dont les \u00e9coulements des yeux et du nez, la salive, l'urine, les mati\u00e8res f\u00e9cales ainsi que la liti\u00e8re, la fourrure, la nourriture et l'eau contamin\u00e9es. Il peut aussi \u00eatre propag\u00e9 par les humains, les animaux sauvages et les insectes par l'interm\u00e9diaire de v\u00eatements, de fourrures et d'autres surfaces contamin\u00e9s. Le virus peut survivre pendant de longues p\u00e9riodes dans l'environnement et demeurer infectieux pour les animaux.</p>\n\n<p>La maladie n'affecte pas les humains et il n'y a aucun cas connu de cette maladie chez d'autres animaux. Au Canada, la MHL est une maladie \u00e0 d\u00e9claration imm\u00e9diate au niveau f\u00e9d\u00e9ral; les laboratoires sont tenus d'aviser l'Agence canadienne d'inspection des aliments (ACIA) de tout cas soup\u00e7onn\u00e9 ou diagnostiqu\u00e9 de la maladie.</p>\n\n<p>Vous trouverez ci-dessous quelques mesures de bios\u00e9curit\u00e9 \u00e0 prendre.</p>\n\n<h2>Personnes et \u00e9quipement</h2>\n\n<ul>\n<li>Limiter l'acc\u00e8s aux lieux et restreindre le contact avec les lapins aux personnes charg\u00e9es de leurs soins</li>\n<li>Installer des affiches sur la bios\u00e9curit\u00e9 pour aviser les visiteurs que l'acc\u00e8s \u00e0 la propri\u00e9t\u00e9 et aux animaux est restreint</li>\n<li>Verrouiller les portes et les barri\u00e8res pour s\u00e9curiser l'acc\u00e8s aux b\u00e2timents abritant des animaux</li>\n<li>Exiger que tous les visiteurs essentiels (par exemple, les v\u00e9t\u00e9rinaires ou le personnel des services)\u00a0:</li>\n<ul>\n<li>obtiennent une autorisation pr\u00e9alablement \u00e0 leur visite</li>\n<li>comprennent et appliquent les protocoles de bios\u00e9curit\u00e9 \u00e9tablis</li>\n<li>remplissent le registre des visiteurs</li>\n<li>soient accompagn\u00e9s du propri\u00e9taire ou de la personne responsable de l'animal</li>\n</ul>\n</ul>\n\n<p>\u00c9vitez les contacts non essentiels des visiteurs avec les lapins; si cela est in\u00e9vitable, appliquez ces pratiques\u00a0:</p>\n\n<ul>\n<li>Se laver ou se d\u00e9sinfecter les mains, nettoyer et d\u00e9sinfecter les bottes et porter des v\u00eatements r\u00e9serv\u00e9s \u00e0 la ferme ou au lieu en question avant de donner des soins aux lapins</li>\n<li>Ne pas \u00e9changer d'\u00e9quipement avec d'autres \u00e9leveurs ou propri\u00e9taires de lapins</li>\n<li>Nettoyer et d\u00e9sinfecter l'\u00e9quipement, les abreuvoirs, les mangeoires et les autres articles qui sont en contact avec les lapins</li>\n<li>Suivre le mode d'emploi du fabricant du d\u00e9sinfectant et rincer les abreuvoirs et les mangeoires \u00e0 fond avant de les remplir</li>\n<li>\u00c9viter de se rendre dans les r\u00e9gions touch\u00e9es par des \u00e9pid\u00e9mies</li>\n</ul>\n\n<h2>Animaux</h2>\n<ul>\n<li>V\u00e9rifier au moins une fois par jour si les lapins pr\u00e9sentent des signes de la maladie, y compris\u00a0:\n<ul>\n<li>difficult\u00e9 \u00e0 respirer, perte de coordination, baisse d'app\u00e9tit et niveau d'activit\u00e9 r\u00e9duit</li>\n<li>saignements du nez, sang dans les mati\u00e8res f\u00e9cales, h\u00e9morragies dans les yeux</li>\n<li>mort subite avec peu de signes cliniques</li>\n</ul></li>\n<li>Envisager de vacciner les animaux contre la maladie et discuter avec un v\u00e9t\u00e9rinaire</li>\n<li>Emp\u00eacher les lapins d'avoir des contacts avec des lapins domestiques ou des lapins sauvages et d'autres animaux</li>\n<li>G\u00e9rer et limiter l'exposition aux insectes</li>\n<li>G\u00e9rer et limiter l'utilisation des aires ext\u00e9rieures d'exercice pour les lapins</li>\n<li>Prendre en consid\u00e9ration des risques de maladie lors de la participation \u00e0 une exposition de lapins ou \u00e0 des foires en raison de l'exposition \u00e0 des animaux qui pourraient \u00eatre malades</li>\n<li>Limiter l'introduction de nouveaux lapins\u00a0: les lapins qui semblent \u00eatre en bonne sant\u00e9 peuvent \u00eatre infect\u00e9s et pr\u00e9senter un risque pour les animaux r\u00e9sidents</li>\n<li>Isoler tous les lapins revenant d'une exposition et tous les nouveaux lapins pendant au moins 14 jours pour \u00e9viter le contact avec les animaux r\u00e9sidents et pour s'assurer que ces lapins soient en bonne sant\u00e9\u00a0: pour une protection contre la MHL, isoler les lapins pendant 60 jours</li>\n<li>Durant cette p\u00e9riode, prendre en charge les lapins isol\u00e9s s\u00e9par\u00e9ment\u00a0:\n<ul>\n<li>Donnez les soins aux lapins isol\u00e9s uniquement apr\u00e8s avoir manipul\u00e9 les animaux r\u00e9sidents</li>\n<li>Emp\u00eachez tout contact physique et indirect entre les animaux revenant d'une exposition et les animaux r\u00e9sidents (par exemple, ne pas utiliser le m\u00eame \u00e9quipement)</li>\n<li>Surveillez de pr\u00e8s l'\u00e9tat de sant\u00e9 des animaux isol\u00e9s et des animaux r\u00e9sidents durant la p\u00e9riode d'isolement</li>\n<li>Demandez conseil au v\u00e9t\u00e9rinaire en cas de signes de la maladie</li>\n</ul></li>\n</ul>\n\n<h2>Nourriture, eau, liti\u00e8re</h2>\n\n<ul>\n<li>Se procurer la nourriture aupr\u00e8s de fournisseurs qui appliquent des programmes de contr\u00f4le de la qualit\u00e9</li>\n<li>Ni cueillir, ni utiliser des plantes sauvages comme source de nourriture</li>\n<li>Obtenir toute la nourriture pour animaux et toute la liti\u00e8re de r\u00e9gions exemptes du VMHL de type 2 ou entreposer le foin pendant au moins 8 mois avant l'utilisation</li>\n<li>Utiliser des sources d'eau municipales. Les sources d'eau de surface et les puits peu profonds ne sont pas recommand\u00e9s en raison du risque accru de contamination</li>\n<li>Prot\u00e9ger la nourriture et la liti\u00e8re de la contamination en les entreposant \u00e0 l'int\u00e9rieur ou dans des contenants bien ferm\u00e9s.</li>\n<li>Nettoyer la nourriture renvers\u00e9e, le cas \u00e9ch\u00e9ant</li>\n</ul>\n\n<h2 class=\"font-medium black\">Renseignements additionnels</h2>\n\n<ul>\n<li><a href=\"/fra/1300462382369/1300462438912\">Bureaux de sant\u00e9 animale</a></li>\n</ul> \r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}