{
    "dcr_id": "1395272842057",
    "title": {
        "en": "Asian Longhorned Beetle - Signs of Infestation",
        "fr": "Longicorne asiatique - Signes d'infestation"
    },
    "html_modified": "2024-03-12 3:56:39 PM",
    "modified": "2014-03-19",
    "issued": "2014-04-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_anogla_signs_infest_1395272842057_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_anogla_signs_infest_1395272842057_fra"
    },
    "ia_id": "1395273052449",
    "parent_ia_id": "1337792820836",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1337792820836",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Asian Longhorned Beetle - Signs of Infestation",
        "fr": "Longicorne asiatique - Signes d'infestation"
    },
    "label": {
        "en": "Asian Longhorned Beetle - Signs of Infestation",
        "fr": "Longicorne asiatique - Signes d'infestation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1395273052449",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1337792721926",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/asian-longhorned-beetle/signs-of-infestation/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-asiatique/signes-d-infestation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Asian Longhorned Beetle - Signs of Infestation",
            "fr": "Longicorne asiatique - Signes d'infestation"
        },
        "description": {
            "en": "Signs of Asian Longhorned Beetle infestation",
            "fr": "Les signes d\u2019infestation de Longicorne asiatique."
        },
        "keywords": {
            "en": "Plant Protection Act, Plant Protection Regulations, plants, plant products, insects, regulated pests, imports, movement, regulated area, permits, ALHB, Asian Long-horned Beetle",
            "fr": "Loi sur la protection des v\u00e9g&\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, v\u00e9g\u00e9taux, produits v\u00e9g\u00e9taux, insectes, organismes nuisibles, importation, organismes nuisibles, d\u00e9placement, zone r\u00e9glement\u00e9e, permis, longicorne asiatique"
        },
        "dcterms.subject": {
            "en": "imports,insects,inspection,plants",
            "fr": "importation,insecte,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-04-09",
            "fr": "2014-04-09"
        },
        "modified": {
            "en": "2014-03-19",
            "fr": "2014-03-19"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Asian Longhorned Beetle - Signs of Infestation",
        "fr": "Longicorne asiatique - Signes d'infestation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"text-right\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/pestrava_anogla_signs_infest_1395275115053_eng.pdf\" title=\"Asian Longhorned Beetle - Signs of Infestation in portable document format\">PDF (2,043 <abbr title=\"kilobyte\">kb</abbr>)</a></p>\n\n<figure> \n<figcaption><strong>Asian Longhorned Beetle</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img1_1395269705921_eng.jpg\" alt=\"Asian Longhorned Beetle\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Emergence hole</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img3_1395269854376_eng.jpg\" alt=\"Emergence hole\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Egg-laying site</strong></figcaption> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img6_1395270278757_eng.jpg\" alt=\"Egg-laying site\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Dripping sap from egg laying site</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img7_1395270333695_eng.jpg\" alt=\"Dripping sap from egg laying site\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Sloughed bark from first instar larva; egg laying scar no longer visible.</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img2_1395269785579_eng.jpg\" alt=\"Sloughed bark from first instar larva; egg laying scar no longer visible.\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Piles of coarse sawdust at joint of tree and branches</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img4_1395269909268_eng.jpg\" alt=\"Piles of coarse sawdust at joint of tree and branches.\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Egg-laying site</strong></figcaption> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img5_1395269995691_eng.jpg\" alt=\"Egg-laying site\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<p>If you believe that you have found an Asian Longhorned Beetle, call the Canadian Food Inspection Agency (CFIA) at 1-647-790-1012.</p>\n\n<p>For more information on how to identify signs of the insect and infested trees, please visit the <a href=\"/eng/1297964599443/1297965645317\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> website</a>.</p>\n\n<p><strong>Photos - B. D. Gill, <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></strong></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"text-right\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/pestrava_anogla_signs_infest_1395275115053_fra.pdf\" title=\"Longicorne asiatique - Signes d'infestation  en format de document portable\">PDF (2,043 <abbr title=\"kilo-octet\">ko</abbr>)</a></p>\n\n<figure> \n<figcaption><strong>Longicorne asiatique</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img1_1395269705921_fra.jpg\" alt=\"Longicorne asiatique\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Trou d'\u00e9mergence</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img3_1395269854376_fra.jpg\" alt=\"Trou d'\u00e9mergence\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Site d'oviposition</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img6_1395270278757_fra.jpg\" alt=\"Site d'oviposition\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Une coul\u00e9e de s\u00e8ve du site d'oviposition</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img7_1395270333695_fra.jpg\" alt=\"Une coul\u00e9e de s\u00e8ve du site d'oviposition\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>\u00c9corce endommag\u00e9e par une larve du premier stade; la cicatrice de ponte n'est plus visible</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img2_1395269785579_fra.jpg\" alt=\"\u00c9corce endommag\u00e9e par une larve du premier stade la cicatrice de ponte n'est plus visible\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Piles de sciure de bois grossi\u00e8re aux aisselles des branches</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img4_1395269909268_fra.jpg\" alt=\"Piles de sciure de bois grossi\u00e8re aux aisselles des branches\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<figure> \n<figcaption><strong>Sites d'oviposition</strong></figcaption> \n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_anogla_signs_infest_img5_1395269995691_fra.jpg\" alt=\"Sites d'oviposition\" class=\"img-responsive\">\n</figure>\n\n<div class=\"clearfix\"> </div>\n\n<p>Si vous croyez avoir d\u00e9couvert un Longicorne asiatique, contactez l'Agence canadienne d'inspection des aliments (ACIA) au 1-647-790-1012.</p>\n\n<p>Des informations et des images de l'insecte et des ravages qu'il cause sont disponibles sur le site <a href=\"/fra/1297964599443/1297965645317\">Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.</p>\n\n<p><strong>Photos - B. D. Gill, <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></strong></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}