{
    "dcr_id": "1318465620825",
    "title": {
        "en": "Importer-distributors of licensed veterinary biologics",
        "fr": "Importateurs\u00a0- distributeurs de produits biologiques v\u00e9t\u00e9rinaires homologu\u00e9s"
    },
    "html_modified": "2024-03-12 3:55:26 PM",
    "modified": "2023-12-22",
    "issued": "2020-01-20",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/vetbio_importers_1318465620825_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/vetbio_importers_1318465620825_fra"
    },
    "ia_id": "1320717321544",
    "parent_ia_id": "1320545281259",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Licenced Products",
        "fr": "Produits licenci\u00e9s"
    },
    "commodity": {
        "en": "Veterinary Biologics",
        "fr": "Produits biologiques v\u00e9t\u00e9rinaires"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1320545281259",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importer-distributors of licensed veterinary biologics",
        "fr": "Importateurs\u00a0- distributeurs de produits biologiques v\u00e9t\u00e9rinaires homologu\u00e9s"
    },
    "label": {
        "en": "Importer-distributors of licensed veterinary biologics",
        "fr": "Importateurs\u00a0- distributeurs de produits biologiques v\u00e9t\u00e9rinaires homologu\u00e9s"
    },
    "templatetype": "content page 1 column",
    "node_id": "1320717321544",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299159403979",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/veterinary-biologics/importer-distributors/",
        "fr": "/sante-des-animaux/produits-biologiques-veterinaires/importateurs-distributeurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importer-distributors of licensed veterinary biologics",
            "fr": "Importateurs\u00a0- distributeurs de produits biologiques v\u00e9t\u00e9rinaires homologu\u00e9s"
        },
        "description": {
            "en": "In Canada, veterinary biologics may be distributed and sold only after they have been licensed by the Canadian Centre for Veterinary Biologics (CCVB) of the Canadian Food Inspection Agency.",
            "fr": "Au Canada, les produits biologiques v\u00e9t\u00e9rinaires peuvent \u00eatre distribu\u00e9s et vendus seulement une fois homologu\u00e9s par le Centre canadien des produits biologiques v\u00e9t\u00e9rinaires (CCPBV) de l'Agence canadienne d'inspection des aliments."
        },
        "keywords": {
            "en": "veterinary biologics, imports, products",
            "fr": "biologiques v\u00e9t\u00e9rinaires, importation, produits"
        },
        "dcterms.subject": {
            "en": "biotechnology,imports,products",
            "fr": "biotechnologie,importation,produit"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-01-20",
            "fr": "2020-01-20"
        },
        "modified": {
            "en": "2023-12-22",
            "fr": "2023-12-22"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importer-distributors of licensed veterinary biologics",
        "fr": "Importateurs\u00a0- distributeurs de produits biologiques v\u00e9t\u00e9rinaires homologu\u00e9s"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=46#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>In Canada, veterinary biologics may be distributed and sold only after they have been licensed by the Canadian Centre for Veterinary Biologics (CCVB) of the Canadian Food Inspection Agency. A significant number of veterinary biologics used in Canada are imported from the USA, Europe, Australia and New Zealand.</p>\n\n<p>When a foreign manufacturer applies to license a veterinary biologic in Canada, it must designate a Canadian company as its commercial importer-distributor. The prospective importer-distributor must demonstrate that it utilizes appropriate facilities and expertise to import and distribute veterinary biologics in conformance with Canadian regulatory requirements. Once the veterinary biologic meets Canadian licensing requirements, the CCVB issues an annual commercial import permit to the Canadian importer-distributor.</p>\n\n<p>The list below is published in order to help foreign manufacturers locate importer-distributors who meet Canadian regulatory requirements. Companies are listed in alphabetical order; those marked Footnote\u00a01 are international companies or their subsidiaries; footnotes\u00a02 to\u00a07 indicate the types of veterinary biologics they distribute. This list is updated periodically. Recently approved importer-distributors may not be listed.</p>\n\n<p><strong>ASEA Animal Health Inc.</strong><sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:asea@bellnet.ca\">asea@bellnet.ca</a><br>\n</p>\n\n<p><strong>Bio-Rad Laboratories (Canada) Ltd.</strong><sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote</span>3</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:customercare_canada@bio-rad.com\">customercare_canada@bio-rad.com</a></p>\n\n<p><strong>Boehringer-Ingelheim Animal Health Canada Inc.</strong><sup id=\"fn1c-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>CDMV Inc.</strong><sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<span lang=\"fr\">Saint-Hyacinthe</span>, Quebec J2S\u00a07C2<br>\n<br>\n<br>\n</p>\n\n<p><strong>CEVA Animal Health Inc.</strong><sup id=\"fn1d-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:pedro.patricio@ceva.com\">pedro.patricio@ceva.com</a></p>\n\n<p><strong>Eastgen</strong> <sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote</span>6</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:mike.weber@eastgen.ca\">mike.weber@eastgen.ca</a></p>\n\n<p><strong>Elanco Canada Ltd. (Farm and Companion Animal Business)</strong><sup id=\"fn1e-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2e-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup> <sup id=\"fn5a-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote</span>5</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Elanco Canada Limited (Aqua Health Business)</strong><sup id=\"fn1f-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2f-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Hipra Animal Health Canada</strong> <sup id=\"fn1l-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2g-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Huvepharma Canada Corporation Inc.</strong> <sup id=\"fn1o-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2n-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<a href=\"mailto:Customerservice@huvepharma.ca\" class=\"nowrap\">Customerservice@huvepharma.ca</a>\n</p>\n\n<p><strong>Idexx Laboratories Canada Corporation Ltd.</strong><sup id=\"fn1g-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn3f-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote</span>3</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Intervet Canada Corp.</strong><sup id=\"fn1h-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2h-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup> (doing business in Canada as Merck Animal Health)<br>\n<br>\n<br>\n</p>\n\n<p><strong>Kane Veterinary Supplies Ltd.</strong><sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote</span>6</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Merck Animal Health</strong> - see Intervet Canada Corp.</p>\n\n<p><strong>NovaVive Inc.</strong><sup id=\"fn1j-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn7a-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote</span>7</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@novavive.ca\">info@novavive.ca</a><br>\n</p>\n\n<p><strong>Pharmgate Animal Health Canada Inc.</strong><sup id=\"fn1m-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2l-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup><br> \n<br>\n<br>\n<br> \n<br>\n<a href=\"mailto:bill.maxwell@pharmgate.com\">bill.maxwell@pharmgate.com</a></p>\n\n<p><strong>Salutaris Health Ltd.</strong><sup id=\"fn5b-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote</span>5</a></sup><br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Stallergenes Canada Inc.</strong><sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote</span>4</a></sup><br>\n<br>\n<br>\n<br>\n<br> \n<br>\n<a href=\"mailto:christina.porter@stallergenesgreer.com\">christina.porter@stallergenesgreer.com</a></p>\n\n<p><strong>Vetoquinol N.-A. Inc.</strong><sup id=\"fn1n-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2m-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup> <sup id=\"fn7c-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote</span>7</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:edith.lepine@vetoquinol.com\">edith.lepine@vetoquinol.com</a></p>\n\n<p><strong>Western Drug Distribution Center Limited</strong><sup id=\"fn3g-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote</span>3</a></sup> <sup id=\"fn5c-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote</span>5</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:ghall@wddc.com\">ghall@wddc.com</a></p>\n\n<p><strong>Zoetis Canada Inc.</strong><sup id=\"fn1k-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote</span>1</a></sup> <sup id=\"fn2j-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote</span>2</a></sup> <sup id=\"fn3h-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote</span>3</a></sup> <sup id=\"fn5d-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote</span>5</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:Toni.A.Bothwell@Zoetis.com\">Toni.A.Bothwell@Zoetis.com</a></p>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Footnotes</h2>\n<dl>\n<dt>Footnote\u00a01</dt>\n<dd id=\"fn1\">\n<p>International company or its subsidiary</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote\u00a02</dt>\n<dd id=\"fn2\">\n<p>Importer-distributor of veterinary vaccines</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote\u00a03</dt>\n<dd id=\"fn3\">\n<p>Importer-distributor of veterinary diagnostic kits</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote\u00a04</dt>\n<dd id=\"fn4\">\n<p>Importer-distributor of allergenic extracts for veterinary use</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote\u00a05</dt>\n<dd id=\"fn5\">\n<p>Importer-distributor of injectable antibody products for veterinary use</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn5a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote\u00a06</dt>\n<dd id=\"fn6\">\n<p>Importer-distributor of spray-dried antibody products (colostrum; egg) for use in animals</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote\u00a07</dt>\n<dd id=\"fn7\">\n<p>Importer-distributor of other veterinary biologics</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn7a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>7<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=46#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Au Canada, les produits biologiques v\u00e9t\u00e9rinaires peuvent \u00eatre distribu\u00e9s et vendus seulement une fois homologu\u00e9s par le Centre canadien des produits biologiques v\u00e9t\u00e9rinaires (CCPBV) de l'Agence canadienne d'inspection des aliments. Une importante proportion des produits biologiques v\u00e9t\u00e9rinaires utilis\u00e9s au Canada sont import\u00e9s des \u00c9tats-Unis, de l'Europe, de l'Australie et de la Nouvelle-Z\u00e9lande.</p>\n\n<p>Quand un fabricant d'un pays \u00e9tranger pr\u00e9sente une demande d'homologation de produit biologique v\u00e9t\u00e9rinaire au Canada, il doit d\u00e9signer une compagnie canadienne qui sera son importateur-distributeur commercial. L'importateur-distributeur prospectif doit d\u00e9montrer qu'il utilise les installations et l'expertise qui se conforment aux exigences r\u00e9glementaires canadiennes. Apr\u00e8s l'homologation du produit biologique v\u00e9t\u00e9rinaire, le CCPBV d\u00e9livre un permis d'importation annuel commercial \u00e0 l'importateur-distributeur canadien.</p>\n\n<p>La liste ci-dessous est publi\u00e9e afin d'aider les fabricants de pays \u00e9trangers \u00e0 trouver des importateurs-distributeurs qui r\u00e9pondent \u00e0 toutes les exigences r\u00e9glementaires canadiennes. Les compagnies sont \u00e9num\u00e9r\u00e9es par ordre alphab\u00e9tique; celles marqu\u00e9es de la note de bas de page\u00a01 sont des compagnies internationales ou leurs filiales; les notes de bas de page\u00a02 \u00e0\u00a07 indiquent les types de produits biologiques v\u00e9t\u00e9rinaires qu'ils distribuent. Cette liste est mise \u00e0 jour p\u00e9riodiquement. Il se peut que les importateurs-distributeurs r\u00e9cemment approuv\u00e9s ne soient pas encore inscrits.</p>\n\n<p><strong lang=\"en\">ASEA Animal Health Inc.</strong><sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<span lang=\"en\">Hanlon Creek</span>, Unit\u00e9\u00a011<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:asea@bellnet.ca\">asea@bellnet.ca</a><br>\n</p>\n\n<p><strong lang=\"en\">Bio-Rad Laboratories (Canada) Ltd.</strong><sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page</span>3</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:customercare_canada@bio-rad.com\">customercare_canada@bio-rad.com</a></p>\n\n<p><strong lang=\"en\">Boehringer-Ingelheim Sant\u00e9 Animale Canada Inc.</strong><sup id=\"fn1c-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong lang=\"en\">CDMV Inc.</strong><sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong lang=\"en\">CEVA Animal Health Inc.</strong><sup id=\"fn1d-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<span lang=\"en\">Research Lane, Suite</span>\u00a0225<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:pedro.patricio@ceva.com\">pedro.patricio@ceva.com</a></p>\n\n<p><strong lang=\"en\">Eastgen</strong><sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page</span>6</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:mike.weber@eastgen.ca\">mike.weber@eastgen.ca</a></p>\n\n<p><strong><span lang=\"en\">Elanco Canada Limited</span> (En affaires pour animaux de compagnie et de ferme)</strong><sup id=\"fn1e-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2e-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup> <sup id=\"fn5a-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page</span>5</a></sup><br>\n<span class=\"nowrap\">Suite 401, 1919 cour</span> du Minnesota<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong><span lang=\"en\">Elanco Canada Limited</span> (En affaires pour la sant\u00e9 aquatique)</strong><sup id=\"fn1f-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2f-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Hipra Animal Health Canada</strong><sup id=\"fn1l-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2g-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Huvepharma Canada Corporation Inc.</strong><sup id=\"fn1o-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2o-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<br>\n<br>\n<br>\n<a href=\"mailto:Customerservice@huvepharma.ca\" class=\"nowrap\">Customerservice@huvepharma.ca</a></p>\n\n\n<p><strong lang=\"en\">Idexx Laboratories Canada Corporation</strong><sup id=\"fn1g-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn3f-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page</span>3</a></sup><br>\n<span lang=\"en\">Denison</span><br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Intervet Canada Corp.</strong> (faisant affaires au Canada sous le nom de Merck Sant\u00e9 animale)<sup id=\"fn1h-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2h-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br>\n<br>\n<br>\n</p>\n\n<p><strong lang=\"en\">Kane Veterinary Supplies Ltd.</strong><sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page</span>6</a></sup><br>\n<sup>e</sup> rue<br>\n<br>\n<br>\n<br>\n<span lang=\"en\">Terry Duchscher</span></p>\n\n<p><strong>Laboratoires Omega Lt\u00e9.</strong><sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page</span>4</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<p><strong>Merck Sant\u00e9 animale</strong> - voir <span lang=\"en\">Intervet</span> Canada Corp.</p>\n\n<p><strong lang=\"en\">NovaVive Inc.</strong><sup id=\"fn1j-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn7a-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Note de bas de page</span>7</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@novavive.ca\">info@novavive.ca</a><br>\n<sup>r</sup> Stan Alkemade</p>\n\n<p><strong>Pharmgate Animal Health Canada Inc.</strong><sup id=\"fn1m-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2l-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup><br> \n<br>\n<br>\n<br> \n<br>\n<a href=\"mailto:bill.maxwell@pharmgate.com\">bill.maxwell@pharmgate.com</a></p>\n\n<p><strong lang=\"en\">Salutaris Health Ltd.</strong><sup id=\"fn5b-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page</span>5</a></sup><br>\n<span lang=\"en\">PO Box</span>\u00a0550,\u00a069 rue Perreault<br>\n<br>\n<br>\n</p>\n\n<p><strong lang=\"en\">Stallergenes Canada Inc.</strong><sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page</span>4</a></sup><br>\n<br>\n<br>\n<br>\n<br> \n<br>\n<a href=\"mailto:christina.porter@stallergenesgreer.com\">christina.porter@stallergenesgreer.com</a></p>\n\n<p><strong>Vetoquinol N.-A. Inc.</strong><sup id=\"fn1n-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2n-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup> <sup id=\"fn7c-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Note de bas de page</span>7</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:edith.lepine@vetoquinol.com\">edith.lepine@vetoquinol.com</a></p>\n\n\n<p><strong lang=\"en\">Western Drug Distribution Center Limited</strong> <sup id=\"fn3g-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page</span>3</a></sup> <sup id=\"fn5c-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page</span>5</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<span lang=\"en\">Greg Hall</span><br>\n<a href=\"mailto:ghall@wddc.com\">ghall@wddc.com</a></p>\n\n<p><strong lang=\"en\">Zoetis Canada Inc.</strong><sup id=\"fn1k-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page</span>1</a></sup> <sup id=\"fn2j-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page</span>2</a></sup> <sup id=\"fn3h-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page</span>3</a></sup> <sup id=\"fn5d-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page</span>5</a></sup><br>\n<br>\n<br>\n<br>\n<br>\n<span lang=\"en\">Toni Bothwell</span><br>\n<a href=\"mailto:T.A.Bothwell@Zoetis.com\">T.A.Bothwell@Zoetis.com</a></p>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Notes de bas de page</h2>\n<dl>\n<dt>Note de bas de page\u00a01<br>\n</dt>\n<dd id=\"fn1\">\n<p>Compagnie internationale ou sa filiale</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>1</a></p>\n</dd>\n\n<dt>Note de bas de page\u00a02<br>\n</dt>\n<dd id=\"fn2\">\n<p>Importateurs-distributeurs de vaccins v\u00e9t\u00e9rinaires</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>2</a></p>\n</dd>\n\n<dt>Note de bas de page\u00a03<br>\n</dt>\n<dd id=\"fn3\">\n<p>Importateurs-distributeurs de trousses diagnostiques v\u00e9t\u00e9rinaires</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>3</a></p>\n</dd>\n\n<dt>Note de bas de page\u00a04<br>\n</dt>\n<dd id=\"fn4\">\n<p>Importateurs-distributeurs d'extraits allerg\u00e9niques, pour usage v\u00e9t\u00e9rinaire</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>4</a></p>\n</dd>\n\n<dt>Note de bas de page\u00a05<br>\n</dt>\n<dd id=\"fn5\">\n<p>Importateurs-distributeurs de produits anticorps injectables, pour usage v\u00e9t\u00e9rinaire</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn5a-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>5</a></p>\n</dd>\n\n<dt>Note de bas de page\u00a06<br>\n</dt>\n<dd id=\"fn6\">\n<p>Importateurs-distributeurs de produits anticorps s\u00e9ch\u00e9s par pulv\u00e9risation (colostrum; \u0153ufs), pour usage chez les animaux</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>6</a></p>\n</dd>\n\n<dt>Note de bas de page\u00a07<br>\n</dt>\n<dd id=\"fn7\">\n<p>Importateurs-distributeurs d'autres produits biologiques v\u00e9t\u00e9rinaires</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn7a-rf\"><span class=\"wb-inv\">Retour \u00e0 la <span>premi\u00e8re</span> r\u00e9f\u00e9rence de la note de bas de page </span>7</a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}