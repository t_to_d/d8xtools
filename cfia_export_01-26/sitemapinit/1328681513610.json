{
    "dcr_id": "1328681410934",
    "title": {
        "en": "World Organisation for Animal Health (WOAH)",
        "fr": "Organisation mondiale de la sant\u00e9 animale (OMSA)"
    },
    "html_modified": "2024-01-26 2:20:57 PM",
    "modified": "2018-11-05",
    "issued": "2015-04-09 11:12:18",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anim_vet_oie_1328681410934_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anim_vet_oie_1328681410934_fra"
    },
    "ia_id": "1328681513610",
    "parent_ia_id": "1323802773521",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1323802773521",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "World Organisation for Animal Health (WOAH)",
        "fr": "Organisation mondiale de la sant\u00e9 animale (OMSA)"
    },
    "label": {
        "en": "World Organisation for Animal Health (WOAH)",
        "fr": "Organisation mondiale de la sant\u00e9 animale (OMSA)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1328681513610",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323802461727",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/chief-veterinary-officer/woah/",
        "fr": "/sante-des-animaux/veterinaire-en-chef/omsa/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "World Organisation for Animal Health (WOAH)",
            "fr": "Organisation mondiale de la sant\u00e9 animale (OMSA)"
        },
        "description": {
            "en": "The WOAH is the science based standard setting organization at the international level for animal and veterinary public health.",
            "fr": "The OMSA est l'organisation de normalisation scientifique internationale en ce qui concerne la sant\u00e9 animale et la sant\u00e9 publique v\u00e9t\u00e9rinaire."
        },
        "keywords": {
            "en": "World Organisation for Animal Health, WOAH, animal health, disease control, zoonotic diseases, Manual of Diagnostic Test and Vaccines for Terrestrial Animals, Manual of Diagnostic Tests for Aquatic Animals,Terrestrial Animal Health Code, Aquatic Animal Health Code",
            "fr": "Organisation mondiale de la sant&#233; animale, OMSA, sant&#233; animale, contr&#244;le des maladies, zoonoses, Manuel des tests de diagnostic et des vaccins pour les animaux terrestres, Manuel des tests de diagnostic pour les animaux aquatiques, Code sanitaire pour les animaux terrestres, Code sanitaire pour les animaux aquatiques"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Office of the President",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Bureau du pr\u00e9sident"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-09 11:12:18",
            "fr": "2015-04-09 11:12:18"
        },
        "modified": {
            "en": "2018-11-05",
            "fr": "2018-11-05"
        },
        "type": {
            "en": "contact information,reference material",
            "fr": "information sur la personne-ressource,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "World Organisation for Animal Health (WOAH)",
        "fr": "Organisation mondiale de la sant\u00e9 animale (OMSA)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The <a href=\"https://www.woah.org/en/home/\">World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE))</a> is the science based standard setting organization at the international level for animal and veterinary public health. It also serves as the scientific  reference body for international trade of animals and animal derived  products under the Sanitary and Phyto-sanitary (SPS) Agreement of the World Trade  Organization.</p>\n<p>The <abbr title=\"World Organization for Animal Health\">WOAH</abbr>'s objectives are:</p>\n<ul>\n<li>To ensure transparency in the global animal disease and zoonosis situation</li>\n<li>To collect, analyse and disseminate scientific veterinary information</li>\n<li>To provide expertise and encourage international solidarity in the control of animal diseases</li>\n<li>Within its mandate under the <abbr title=\"World Trade Organization\">WTO</abbr> <abbr title=\"Sanitary and Phyto-sanitary\">SPS</abbr> Agreement, to safeguard world trade by publishing health standards for international trade in animals and animal products</li>\n<li>To improve the legal framework and resources of National Veterinary Services</li>\n<li>To provide a better guarantee of the safety of food of animal origin  and to promote animal welfare through a science-based approach</li>\n</ul>\n\n<p>The duties of the <abbr title=\"World Organisation for Animal Health\">WOAH</abbr> Delegate for Canada include, but are not limited to:</p>\n\n<ul>\n<li>Representing Canada at the World Assembly of Delegates and voting on international standards, recommendations, and resolutions</li>\n\n<li>Notifying the <abbr title=\"World Organisation for Animal Health\">WOAH</abbr> of animal diseases present in Canada</li>\n\n<li>Bringing the resolutions of the World Assembly to the attention of the Canadian government, and ensuring that, as far as possible, the resolution of the World Assembly are applied in Canada</li>\n\n<li>Providing scientific input into the development of international standards, and </li>\n\n<li>Designating national focal points for support in the fields of animal health information, wildlife diseases, veterinary medicinal products, animal production food safety, animal welfare, communications and laboratories</li>\n</ul>\n\n<h2>International Standards</h2>\n\n<p>International Sanitary Standards are drafted by the <abbr title=\"World Organization for Animal Health\">WOAH</abbr> Specialist Commissions. Standards are created to protect countries from the introduction of  diseases and pathogens, while ensuring they are fair and scientifically justified. These sanitary standards are continually revised and updated.</p>\n<p>At the Specialist Commission level comments that are supported by  sound scientific information will be taken into account and draft  standards may be revised accordingly.\u00a0 All revised draft standards are submitted to  the <abbr title=\"World Organization for Animal Health\">WOAH</abbr> for ratification by the International Committee at the General Session. Ratified standards are then incorporated into the relevant <abbr title=\"World Organization for Animal Health\">WOAH</abbr> publications.\u00a0</p>\n<ul>\n<li><a href=\"https://www.woah.org/en/what-we-do/standards/codes-and-manuals/terrestrial-manual-online-access/\">Manual of Diagnostic Test and Vaccines for Terrestrial Animals</a></li>\n<li><a href=\"https://www.woah.org/en/what-we-do/standards/codes-and-manuals/aquatic-manual-online-access/\">Manual of Diagnostic Tests for Aquatic Animals</a></li>\n<li><a href=\"https://www.woah.org/en/what-we-do/standards/codes-and-manuals/terrestrial-code-online-access/\">Terrestrial Animal Health Code</a></li>\n<li><a href=\"https://www.woah.org/en/what-we-do/standards/codes-and-manuals/aquatic-code-online-access/\">Aquatic Animal Health Code</a></li>\n</ul>\n<p>The <abbr title=\"World Organization for Animal Health\">WOAH</abbr> oversees <a href=\"https://www.woah.org/en/what-we-do/standards/standards-setting-process/\">four specialist commissions that develop and revise the WOAH's international sanitary standards</a>, by addressing scientific and technical issues raised by Member Countries.</p>\n<ul>\n<li>Terrestrial Animal Health Standards Commission (\"Code Commission\") -  establishes standards governing the trade of terrestrial animals and animal products.</li>\n<li>Scientific Commission for Animal Diseases (\"Scientific Commission\")-  assists in identifying the most appropriate strategies and measures for disease prevention and control. The Commission also  reviews submissions regarding animal health status for Member Countries  that wish to be included on the WOAH's list of countries 'free' of certain  diseases.</li>\n<li>Biological Standards Commission (\"Laboratories Commission\")-  establishes methods for diagnosing diseases of mammals, birds and bees. Furthermore, the Commission tests biological products, such as vaccines.  It oversees the production of the Manual of Diagnostic Tests and  Vaccines for Terrestrial Animals.</li>\n<li>Aquatic Animal Health Standards Commission (\"Aquatic Commission\")-  compiles information on diseases of fish, molluscs and crustaceans, and on methods used to control these diseases.</li>\n</ul>\n<p>In addition to the four Specialist Commissions, there are <a href=\"https://www.woah.org/en/what-we-do/standards/standards-setting-process/working-groups/%0A\">three working groups</a> focusing on Wildlife Diseases, Animal Welfare and Food Safety. The purpose of the working groups is to collect, analyse and disseminate information relevant to their respective fields.</p>\n\n<h2>WOAH's Performance of Veterinary Services (PVS) Evaluation Report of Canada</h2> \n\n<p>The WOAH has evaluated Canada's veterinary services and has found Canada to be a top performing country and a leading example for meeting international veterinary service standards. The report is now available on the WOAH's website.</p>\n<ul><li><a href=\"https://www.woah.org/en/what-we-offer/improving-veterinary-services/pvs-pathway/evaluation/pvs-evaluation-reports/\">WOAH's PVS evaluation report of Canada's veterinary services</a></li>\n<li><a href=\"/animal-health/chief-veterinary-officer/woah/2018-08-02/eng/1533220176097/1533220176611\">Notice to industry: WOAH releases PVS report on its evaluation of Canada's veterinary services</a></li>\n<li><a href=\"https://www.canada.ca/en/food-inspection-agency/news/2018/08/oies-performance-of-veterinary-services-pvs-evaluation-report-of-canada.html\">CVO Statement: WOAH's PVS evaluation report of Canada (2018-08-02)</a></li>\n<li><a href=\"/animal-health/chief-veterinary-officer/woah/how-canada-s-veterinary-services-measure-up-in-the/eng/1541445035262/1541445035649\">Infographic: How Canada's veterinary services measure up in the world</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'<a href=\"https://www.woah.org/fr/accueil/\">Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE))</a> est l'organisation de normalisation scientifique internationale en ce qui concerne la sant\u00e9 animale et la sant\u00e9 publique v\u00e9t\u00e9rinaire. L'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> sert \u00e9galement d'organisme scientifique de r\u00e9f\u00e9rence pour le commerce international des animaux et des produits d'origine animale aux termes de l'Accord sur les mesures sanitaires et phytosanitaires (SPS) de l'Organisation mondiale du commerce.</p>\n\n<p>Les objectifs de l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> sont les suivants\u00a0:</p>\n\n<ul>\n<li>garantir la transparence de la situation des maladies animales et des zoonoses dans le monde;</li>\n<li>recueillir, analyser et diffuser l'information scientifique v\u00e9t\u00e9rinaire;</li>\n<li>apporter son expertise et stimuler la solidarit\u00e9 internationale pour contr\u00f4ler les maladies animales;</li>\n<li>garantir la s\u00e9curit\u00e9 du commerce mondial en publiant des normes sanitaires pour les \u00e9changes internationaux d'animaux et de produits connexes dans le cadre du mandat confi\u00e9 \u00e0 l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> par l'Accord SPS de l'OMC;</li>\n<li>am\u00e9liorer le cadre juridique et les ressources des Services v\u00e9t\u00e9rinaires nationaux;</li>\n<li>mieux garantir la salubrit\u00e9 des aliments d'origine animale et promouvoir le bien-\u00eatre animal par une approche fond\u00e9e sur les principes scientifiques.</li>\n</ul>\n\n<p>Les t\u00e2ches du d\u00e9l\u00e9gu\u00e9 du Canada par int\u00e9rim aupr\u00e8s de l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> incluent notamment les suivantes\u00a0:</p>\n\n<ul>\n<li>repr\u00e9senter le Canada \u00e0 l'Assembl\u00e9e mondiale des d\u00e9l\u00e9gu\u00e9s et voter afin d'adopter des normes internationales, des recommandations et des r\u00e9solutions;</li>\n\n<li>aviser l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> de toute maladie animale pr\u00e9sente au Canada;</li>\n\n<li>pr\u00e9senter les r\u00e9solutions de l'Assembl\u00e9e mondiale au gouvernement du Canada, et, dans la mesure du possible, faire en sorte que ces r\u00e9solutions soient adopt\u00e9es au Canada;</li>\n\n<li>formuler des observations scientifiques dans le cadre de l'\u00e9laboration de normes internationales;</li>\n\n<li>d\u00e9signer des centres nationaux de coordination pour offrir de l'information sur la sant\u00e9 animale, les maladies de la faune, les produits m\u00e9dicaux \u00e0 usage v\u00e9t\u00e9rinaire, la production animale, la salubrit\u00e9 des aliments, le bien-\u00eatre animal, la communication et les laboratoires.</li>\n</ul>\n\n<h2>Normes internationales</h2>\n\n<p>Les normes sanitaires internationales sont \u00e9labor\u00e9es par les commissions sp\u00e9cialis\u00e9es de l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr>. Elles visent \u00e0 prot\u00e9ger les pays contre l'introduction de maladies et d'agents pathog\u00e8nes, tout en s'assurant qu'elles sont \u00e9quitables et justifiables du point de vue scientifique. Ces normes sanitaires sont constamment examin\u00e9es et mises \u00e0 jour.</p>\n\n<p>Les commissions sp\u00e9cialis\u00e9es prennent en compte les commentaires qui s'appuient sur des donn\u00e9es scientifiques solides et elles peuvent modifier les projets de normes en cons\u00e9quence. Tous les projets de normes ainsi modifi\u00e9s sont pr\u00e9sent\u00e9s \u00e0 l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> pour que le Comit\u00e9 international les ratifie \u00e0 la session g\u00e9n\u00e9rale. Une fois ratifi\u00e9es, les normes sont incorpor\u00e9es aux publications pertinentes de l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr>.</p>\n\n<ul>\n<li><a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/codes-et-manuels/acces-en-ligne-au-manuel-terrestre/\">Manuel des tests de diagnostic et des vaccins pour les animaux terrestres</a></li>\n<li><a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/codes-et-manuels/acces-en-ligne-au-manuel-aquatique/\">Manuel des tests de diagnostic pour les animaux aquatiques</a></li>\n<li><a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/codes-et-manuels/acces-en-ligne-au-code-terrestre/\">Code sanitaire pour les animaux terrestres</a></li>\n<li><a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/codes-et-manuels/acces-en-ligne-au-code-aquatique/\">Code sanitaire pour les animaux aquatiques</a></li>\n</ul>\n\n<p>L'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr> supervise <a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/processus-detablissement-des-normes/\">quatre commissions sp\u00e9cialis\u00e9es qui \u00e9laborent et r\u00e9visent ses normes sanitaires internationales</a> en traitant les questions scientifiques et techniques soulev\u00e9es par les pays membres.</p>\n\n<ul>\n<li>La Commission des normes sanitaires pour les animaux terrestres (\u00ab\u00a0Commission du Code\u00a0\u00bb) \u00e9tablit les normes r\u00e9gissant le commerce des animaux terrestres et des produits connexes.</li>\n<li>La Commission scientifique pour les maladies animales (\u00ab\u00a0Commission scientifique\u00a0\u00bb) contribue \u00e0 cerner les strat\u00e9gies et les mesures les plus pertinentes pour la pr\u00e9vention et le contr\u00f4le des maladies. La Commission examine \u00e9galement les pr\u00e9sentations concernant le statut sanitaire des pays membres qui d\u00e9sirent figurer sur la liste des pays \u00ab\u00a0indemnes\u00a0\u00bb de certaines maladies que dresse l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr>.</li>\n<li>La Commission des normes biologiques (\u00ab\u00a0Commission des laboratoires\u00a0\u00bb) \u00e9tablit des m\u00e9thodes pour le diagnostic des maladies des mammif\u00e8res, des oiseaux et des abeilles. De plus, la Commission met \u00e0 l'essai des produits biologiques comme des vaccins. Elle supervise la production du Manuel des tests de diagnostic et des vaccins pour les animaux terrestres.</li>\n<li>La Commission des normes sanitaires pour les animaux aquatiques (\u00ab\u00a0Commission des animaux aquatiques\u00a0\u00bb) rassemble les renseignements relatifs aux maladies des poissons, des mollusques et des crustac\u00e9s ainsi qu'aux m\u00e9thodes utilis\u00e9es pour lutter contre ces maladies.</li>\n</ul>\n\n<p>En plus des quatre commissions sp\u00e9cialis\u00e9es, mentionnons <a href=\"https://www.woah.org/fr/ce-que-nous-faisons/normes/processus-detablissement-des-normes/groupes-de-travail/\">trois groupes de travail</a> dont les activit\u00e9s sont ax\u00e9es sur les maladies des animaux sauvages, le bien-\u00eatre animal et la salubrit\u00e9 des aliments. L'objectif des groupes de travail consiste \u00e0 recueillir, \u00e0 analyser et \u00e0 diffuser des renseignements pertinents dans leur domaine de travail respectif.</p>\n\n<h2>Le rapport d'\u00e9valuation des performances des services v\u00e9t\u00e9rinaires (PVS) au Canada</h2>\n\n<p>L'OMSA a \u00e9valu\u00e9 les services v\u00e9t\u00e9rinaires du Canada et a d\u00e9termin\u00e9 qu'il \u00e9tait un exemple phare et un des pays avec le meilleur rendement \u00e0 l'\u00e9gard du respect des normes internationales en mati\u00e8re de services v\u00e9t\u00e9rinaires. Ce rapport est maintenant disponible sur le site Web de l'OMSA.</p>\n<ul>\n<li><a href=\"https://www.woah.org/fr/ce-que-nous-proposons/ameliorer-les-services-veterinaires/processus-pvs/evaluation/rapports-devaluation-pvs-de-loie/\">Le rapport d'\u00e9valuation PVS des services v\u00e9t\u00e9rinaires au Canada</a></li>\n<li><a href=\"/sante-des-animaux/veterinaire-en-chef/omsa/2018-08-02/fra/1533220176097/1533220176611\">Avis \u00e0 l'industrie\u00a0: l'OMSA publie le rapport PVS de son \u00e9valuation des services v\u00e9t\u00e9rinaires au Canada</a></li>\n<li><a href=\"https://www.canada.ca/fr/agence-inspection-aliments/nouvelles/2018/08/rapport-devaluation-des-performances-des-services-veterinaires-pvs-au-canada-de-loie.html\">D\u00e9claration du VCC\u00a0: Rapport d'\u00e9valuation PVS au Canada de l'OMSA (2018-08-02)</a></li>\n<li><a href=\"/sante-des-animaux/veterinaire-en-chef/omsa/comment-les-services-veterinaires-du-canada-mesure/fra/1541445035262/1541445035649\">Infographie\u00a0: Comment les services v\u00e9t\u00e9rinaires du Canada mesurent-ils dans le monde</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}