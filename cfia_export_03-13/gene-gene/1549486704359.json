{
    "dcr_id": "1549486704359",
    "title": {
        "en": "The Dartmouth Laboratory",
        "fr": "Le laboratoire de Dartmouth"
    },
    "html_modified": "2024-03-12 3:58:19 PM",
    "modified": "2023-08-23",
    "issued": "2019-02-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_dartmouth_1549486704359_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_dartmouth_1549486704359_fra"
    },
    "ia_id": "1549486756922",
    "parent_ia_id": "1494878085588",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1494878085588",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The Dartmouth Laboratory",
        "fr": "Le laboratoire de Dartmouth"
    },
    "label": {
        "en": "The Dartmouth Laboratory",
        "fr": "Le laboratoire de Dartmouth"
    },
    "templatetype": "content page 1 column",
    "node_id": "1549486756922",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1494878032804",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-laboratories/dartmouth/",
        "fr": "/les-sciences-et-les-recherches/nos-laboratoires/dartmouth/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The Dartmouth Laboratory",
            "fr": "Le laboratoire de Dartmouth"
        },
        "description": {
            "en": "The Dartmouth Laboratory is the CFIA's expert laboratory for the testing of fish and seafood for a wide variety of substances, including marine toxins, veterinary drug residues and toxic metals.",
            "fr": "Le laboratoire de Dartmouth est le laboratoire d'expertise de l'ACIA dans la d\u00e9tection de diverses substances dans le poisson et les fruits de mer, dont les toxines marines, les r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires et les m\u00e9taux toxiques."
        },
        "keywords": {
            "en": "Dartmouth Laboratory, laboratory",
            "fr": "laboratoire de Dartmouth, laboratoire"
        },
        "dcterms.subject": {
            "en": "education,educational guidance,scientific research,educational resources",
            "fr": "\u00e9ducation,orientation scolaire,recherche scientifique,ressources p\u00e9dagogiques"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Strategic Communications Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des communications strat\u00e9giques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2023-08-23",
            "fr": "2023-08-23"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "audience": {
            "en": "educators,business,government,general public,media,scientists",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,grand public,m\u00e9dia,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Dartmouth Laboratory",
        "fr": "Le laboratoire de Dartmouth"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_dartmouth_text_1556735928522_eng.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">CFIA science laboratory brochure: The Dartmouth Laboratory <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2,400&nbsp;kb)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n\n<div class=\"clearfix\"></div>\n-->\n<p>The Dartmouth Laboratory is on the traditional unceded territory of the Mi'kmaq People.</p>\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">About the Canadian Food Inspection Agency</h2></summary>\n\n<p>The Canadian Food Inspection Agency (CFIA) is a science-based regulator with a mandate to safeguard the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">food</a> supply, protect the health of <a href=\"/plant-health/eng/1299162629094/1299162708850\">plants</a> and <a href=\"/animal-health/eng/1299155513713/1299155693492\">animals</a>, and support market access. The Agency relies on high-quality, timely and relevant science as the basis of its program design and regulatory decision-making. Scientific activities inform the Agency's understanding of risks, provide evidence for developing mitigation measures, and confirm the effectiveness of these measures.</p>\n\n<p>CFIA scientific activities include laboratory testing, research, surveillance, test method development, risk assessments and expert scientific advice. Agency scientists maintain strong partnerships with universities, industry, and federal, provincial and international counterparts to effectively carry out the CFIA's mandate.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_dartmouth_1498061462319_eng.jpg\" class=\"img-responsive\" alt=\"Photograph - Dartmouth Laboratory building entrance\">\n</div>\n<p>The Dartmouth Laboratory is the CFIA's expert laboratory for the testing of fish and seafood for a wide variety of substances, including marine toxins, veterinary drug residues and toxic metals, and for detecting fish fraud through DNA barcoding. Research activities focus on the development and validation of regulatory food testing methods, and providing scientific data to support national programs, policies and priorities.</p>\n\n<h2>What we do</h2>\n\n<h3>Services</h3>\n\n<ul>\n<li>Testing to support:\n<ul>\n<li>monitoring programs</li>\n<li>surveys</li>\n<li>food investigations and consumer complaints</li>\n</ul>\n</li>\n<li>Validation of methods for different substances in a variety of food commodities.</li>\n<li>Provide scientific advice to support food safety programs, inspection staff, regulated parties and other government departments.</li>\n<li>Accredited for developing and validating test methods and conducting non-routine testing.</li>\n</ul>\n\n<h3>Chemistry</h3>\n\n<ul>\n<li>Develop, optimize and validate methods needed for use in the regulatory testing.</li>\n<li>Test for toxic elements, heavy metals in various foods, marine toxins in shellfish, and veterinary drug residues in fish and seafood products.</li>\n<li>Participate in collaborative research with other government departments and academia in support of the CFIA's mandate\u00a0\u2013 to safeguard Canada's food, animals and plants</li>\n</ul>\n\n<h3>Microbiology</h3>\n\n<ul>\n<li>Regulatory testing and provide expertise in the validation of methods for the bacteriological analyses of foods and environmental samples, including <i lang=\"la\">E.coli O157:H7,</i> verotoxigenic <i lang=\"la\">E.\u00a0coli</i>, <i lang=\"la\">Listeria monocytogenes</i>, <i lang=\"la\">Salmonella</i> species and <i lang=\"la\">Vibrio parahaemolyticus</i>.</li>\n<li>Provides whole genome sequencing analysis of food bacterial isolates</li>\n<li>Provides DNA barcode testing for the identification of fin fish to support the CFIA's efforts to combat mislabelling and food fraud.</li>\n<li>Research focused on the development of more efficient, sensitive and/or faster regulatory testing methods.</li>\n<li>An established research program using microbiology to focus on improving the detection and isolation of foodborne bacterial pathogens through:\n<ul>\n<li>genomics and bioinformatics</li>\n<li>physiological characterization</li>\n<li>metabolism and molecular detection </li>\n</ul>\n</li>\n</ul>\n\n<h2>Quality management</h2>\n\n<p>All CFIA laboratories are accredited in accordance with the International Standard ISO/IEC 17025, <i>General requirements for the competence of testing and calibration laboratories</i>. The Standards Council of Canada (SCC) provides accreditation for routine testing, test method development and non-routine testing, as identified on the laboratory's Scope of Accreditation on the <a href=\"https://www.scc.ca/en/accreditation/laboratories/canadian-food-inspection-agency-5\">SCC website</a>. Accreditation formally verifies the CFIA's competence to produce accurate and reliable results. The results are supported by the development, validation and implementation of scientific methods, conducted by highly qualified personnel, using reliable products, services, and equipment, in a quality controlled environment. Participation in international proficiency testing programs further demonstrates that our testing is comparable to laboratories across Canada and around the world.</p>\n\n<h2>Physical address</h2>\n\n<p>Dartmouth Laboratory<br>\n<br>\n<br>\n</p>\n\n<h2>More information</h2>\n\n<ul>\n<li><a href=\"https://science.gc.ca/site/science/en/blogs/cultivating-science/streamlining-food-testing-dartmouth-laboratory\">Streamlining food testing at the Dartmouth Laboratory</a></li>\n<li><a href=\"/inspect-and-protect/food-safety/bree-ann-lightfoot-and-dr-lisa-hodges/eng/1572287688898/1572287689258\">Women in Science\u00a0\u2013 podcast with Bree Ann Lightfoot and Dr. Lisa Hodges</a></li>\n</ul>\n\n<p>Learn about other <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_dartmouth_text_1556735928522_fra.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">Brochure du laboratoire scientifique de l'ACIA&nbsp;: Le laboratoire de Dartmouth <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2 400&nbsp;ko)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n-->\n\n<div class=\"clearfix\"></div>\n\n<p>Le laboratoire de Dartmouth se trouve sur le territoire traditionnel non c\u00e9d\u00e9 du peuple Mi'kmaq.</p>\n\n<details class=\"mrgn-bttm-lg\">\n    \n<summary><h2 class=\"h4\">\u00c0 propos de l'Agence canadienne d'inspection des aliments</h2></summary>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est un organisme de r\u00e9glementation \u00e0 vocation scientifique dont le mandat est de pr\u00e9server l'<a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">approvisionnement alimentaire</a>, de prot\u00e9ger la sant\u00e9 des <a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">v\u00e9g\u00e9taux</a> et des <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">animaux</a> et de favoriser l'acc\u00e8s aux march\u00e9s. L'Agence s'appuie sur des donn\u00e9es scientifiques de grande qualit\u00e9, opportunes et pertinentes pour concevoir ses programmes et prendre ses d\u00e9cisions r\u00e9glementaires. Les activit\u00e9s scientifiques permettent \u00e0 l'Agence de mieux comprendre les risques, de fournir des preuves pour \u00e9laborer des mesures d'att\u00e9nuation et de confirmer l'efficacit\u00e9 de ces mesures.</p>\n\n<p>Les activit\u00e9s scientifiques de l'ACIA comprennent les essais en laboratoire, la recherche, la surveillance, l'\u00e9laboration de m\u00e9thodes d'essai, l'\u00e9valuation des risques et les conseils d'experts scientifiques. Les scientifiques de l'Agence entretiennent des partenariats solides avec les universit\u00e9s, l'industrie et leurs homologues f\u00e9d\u00e9raux, provinciaux et internationaux afin de remplir efficacement le mandat de l'ACIA.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_dartmouth_1498061462319_fra.jpg\" class=\"img-responsive\" alt=\"Photographe - Entr\u00e9e du b\u00e2timent du laboratoire de Dartmouth\">\n</div>\n<p>Le laboratoire de Dartmouth est le laboratoire expert de l'ACIA pour l'analyse du poisson et des fruits de mer en ce qui concerne la d\u00e9tection d'une grande vari\u00e9t\u00e9 de substances, notamment les toxines marines, les r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires et les m\u00e9taux toxiques, ainsi que la d\u00e9tection de la fraude des poissons gr\u00e2ce aux codes \u00e0 barres de l'ADN. Les activit\u00e9s de recherche sont ax\u00e9es sur l'\u00e9laboration et la validation de m\u00e9thodes r\u00e9glementaires d'analyse des aliments et sur la fourniture de donn\u00e9es scientifiques \u00e0 l'appui des programmes, politiques et priorit\u00e9s nationaux.</p>\n\n<h2>Nos activit\u00e9s</h2>\n\n<h3>Services</h3>\n\n<ul>\n<li>Essais pour soutenir\u00a0:\n<ul>\n<li>les programmes de surveillance;</li>\n<li>les enqu\u00eates;</li>\n<li>les enqu\u00eates alimentaires et les plaintes de consommateurs.</li>\n</ul>\n</li>\n<li>Validation de m\u00e9thodes pour la d\u00e9tection de diff\u00e9rentes substances dans une vari\u00e9t\u00e9 de produits alimentaires.</li>\n<li>Prestation de conseils scientifiques pour soutenir les programmes de salubrit\u00e9 alimentaire, le personnel d'inspection, les parties r\u00e9glement\u00e9es et les autres minist\u00e8res du gouvernement.</li>\n<li>Accr\u00e9dit\u00e9 pour le d\u00e9veloppement et la validation de m\u00e9thodes d'essai ainsi que la r\u00e9alisation d'essais non routiniers.</li>\n</ul>\n\n<h3>Chimie</h3>\n\n<ul>\n<li>D\u00e9velopper, optimiser et valider les m\u00e9thodes n\u00e9cessaires \u00e0 l'utilisation des essais r\u00e9glementaires.</li>\n<li>Tester les \u00e9l\u00e9ments toxiques, les m\u00e9taux lourds dans divers aliments, les toxines marines dans les crustac\u00e9s et les r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires dans les poissons et les fruits de mer.</li>\n<li>Participer \u00e0 des recherches en collaboration avec d'autres minist\u00e8res et le milieu universitaire  \u00e0 l'appui au mandat de l'ACIA \u2013 prot\u00e9ger les aliments, les animaux et les v\u00e9g\u00e9taux du Canada.</li>\n</ul>\n\n<h3>Microbiologie</h3>\n\n<ul>\n<li>Effectuer des essais r\u00e9glementaires et fournir une expertise dans la validation des m\u00e9thodes d'analyse bact\u00e9riologique des aliments et des \u00e9chantillons environnementaux, y compris  E. coli O157:H7, E. Coli producteur de v\u00e9rotoxine, Listeria monocytogenes, des esp\u00e8ces de Salmonella et Vibrio parahaemolyticus.</li>\n<li>Fournir des analyses de s\u00e9quen\u00e7age du g\u00e9nome entier des isolats bact\u00e9riens alimentaires.</li>\n<li>Fournir des analyses de codes \u00e0 barres ADN pour l'identification des poissons \u00e0 nageoires afin de soutenir les efforts de l'ACIA pour lutter contre les erreurs d'\u00e9tiquetage et la fraude alimentaire.</li> \n<li>Recherche ax\u00e9e sur le d\u00e9veloppement de m\u00e9thodes d'essais r\u00e9glementaires plus efficaces, plus sensibles ou plus rapides.</li>\n<li>Un programme de recherche \u00e9tabli utilisant la microbiologie pour se concentrer sur l'am\u00e9lioration de la d\u00e9tection et de l'isolement des pathog\u00e8nes bact\u00e9riens d'origine alimentaire par le biais de\u00a0:\n<ul>\n<li>la g\u00e9nomique et la bioinformatique;</li>\n<li>la caract\u00e9risation physiologique; et</li>\n<li>le m\u00e9tabolisme et la d\u00e9tection mol\u00e9culaire.</li>\n</ul>    \n</li>\n</ul>\n\n<h3>Programme de recherche</h3>\n\n<ul>\n<li>Recherche ax\u00e9e sur la mise au point de m\u00e9thodes d'essai r\u00e9glementaires plus efficaces, plus sensibles ou plus rapides.</li>\n<li>Programme de recherche \u00e9tabli fond\u00e9 sur la microbiologie pour am\u00e9liorer la d\u00e9tection et l'isolement de pathog\u00e8nes alimentaires bact\u00e9riens par\u00a0:\n<ul>\n<li>la g\u00e9nomique et la bioinformatique</li>\n<li>la caract\u00e9risation physiologique</li>\n<li>le m\u00e9tabolisme et la d\u00e9tection mol\u00e9culaire</li>\n</ul>\n</li>\n</ul>\n<h2>Gestion de la qualit\u00e9</h2>\n\n<p>Tous les laboratoires de l'ACIA sont accr\u00e9dit\u00e9s conform\u00e9ment \u00e0 la norme ISO/IEC 17025, <i>Exigences g\u00e9n\u00e9rales concernant la comp\u00e9tence des laboratoires d'\u00e9talonnage et d'essais</i>. Le Conseil canadien des normes (CCN) accorde l'accr\u00e9ditation pour les essais de routine, l'\u00e9laboration de m\u00e9thodes d'essai et les essais non routiniers, comme l'indique la port\u00e9e d'accr\u00e9ditation du laboratoire sur le <a href=\"https://www.scc.ca/fr/accreditation/laboratoires/agence-canadienne-dinspection-des-aliments-laboratoire-de-dartmouth\">site Web du CCN</a>. L'accr\u00e9ditation v\u00e9rifie officiellement la comp\u00e9tence de l'ACIA \u00e0 produire des r\u00e9sultats pr\u00e9cis et fiables. Ces r\u00e9sultats sont \u00e9tay\u00e9s par l'\u00e9laboration, la validation et la mise en \u0153uvre de m\u00e9thodes scientifiques, men\u00e9es par un personnel hautement qualifi\u00e9, \u00e0 l'aide de produits, de services et d'\u00e9quipement fiables, dans un environnement de qualit\u00e9 contr\u00f4l\u00e9e. La participation \u00e0 des programmes internationaux d'essais d'aptitude d\u00e9montre en outre que nos analyses sont comparables \u00e0 celles de laboratoires du Canada et du monde entier.</p>\n\n<h2>Adresse physique</h2>\n\n<p>Laboratoire de Dartmouth<br>\n<br>\n<br>\n</p>\n\n<h2>Plus d'informations</h2>\n<ul>\n<li><a href=\"https://science.gc.ca/site/science/fr/blogues/cultiver-science/rationalisation-lanalyse-aliments-laboratoire-dartmouth\">Rationalisation de l'analyse des aliments au laboratoire de Dartmouth</a></li>\n<li><a href=\"/inspecter-et-proteger/salubrite-des-aliments/bree-ann-lightfoot-et-dre-lisa-hodges/fra/1572287688898/1572287689258\">Les femmes en sciences\u00a0\u2013 balado avec Bree Ann Lightfoot et le Dr\u00a0Lisa Hodges</a></li>\n</ul>\n<p>Renseignez-vous sur les autres <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}