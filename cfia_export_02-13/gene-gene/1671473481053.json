{
    "dcr_id": "1671473481053",
    "title": {
        "en": "The 12 days of plant protection",
        "fr": "Les 12 jours de protection des v\u00e9g\u00e9taux"
    },
    "html_modified": "2024-02-13 9:56:04 AM",
    "modified": "2022-12-19",
    "issued": "2022-12-20",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_pro_the_12_days_of_plant_protection_1671473481053_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_pro_the_12_days_of_plant_protection_1671473481053_fra"
    },
    "ia_id": "1671473771803",
    "parent_ia_id": "1565298134171",
    "chronicletopic": {
        "en": "Plant Health",
        "fr": "Sant\u00e9 des plantes"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Canadians",
        "fr": "Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565298134171",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The 12 days of plant protection",
        "fr": "Les 12 jours de protection des v\u00e9g\u00e9taux"
    },
    "label": {
        "en": "The 12 days of plant protection",
        "fr": "Les 12 jours de protection des v\u00e9g\u00e9taux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1671473771803",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298133875",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/plant-health/plant-protection/",
        "fr": "/inspecter-et-proteger/sante-des-vegetaux/protection-des-vegetaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The 12 days of plant protection",
            "fr": "Les 12 jours de protection des v\u00e9g\u00e9taux"
        },
        "description": {
            "en": "Whether you\u2019re shopping online, headed on a road trip or enjoying the frosty outdoors this holiday season, take note of these plant health tips from CFIA expert David Bailey.",
            "fr": "Que vous soyez en train de faire des achats en ligne, de faire un voyage sur la route ou de profiter de l'air glac\u00e9 pendant les vacances, prenez note de ces conseils en sant\u00e9 des v\u00e9g\u00e9taux de David Bailey, expert de l'ACIA."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, CFIA, 12 days of plant protection",
            "fr": "Agence canadienne d'inspection des aliments, ACIA, 12 jours de protection des v\u00e9g\u00e9taux"
        },
        "dcterms.subject": {
            "en": "animal health,food,food inspection,plants,sciences,scientific research",
            "fr": "sant\u00e9 animale,aliment,inspection des aliments,plante,sciences,recherche scientifique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-12-20",
            "fr": "2022-12-20"
        },
        "modified": {
            "en": "2022-12-19",
            "fr": "2022-12-19"
        },
        "type": {
            "en": "news publication",
            "fr": "publication d'information"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The 12 days of plant protection",
        "fr": "Les 12 jours de protection des v\u00e9g\u00e9taux"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_the_12_days_of_plant_protection_600x600_1671469175581_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>By David Bailey, Executive Director, Plant Health and Biosecurity Directorate, Canadian Food Inspection Agency (CFIA)</p> \n\n<p>The holiday season is one of my favourite times of the year. I enjoy seeing the colourful seasonal lights that brighten a rather dull period of the year, which in turn, I believe brings happiness to many and the opportunity to pause and spend time with friends, family and colleagues. It also signals the beginning of a new year and all the hopes and dreams that we may have for the upcoming year.</p>\n<p>Like me, many look forward to the holiday season. Unfortunately, this includes opportunistic invasive species.</p>\n<p>Because of some of our habits, travels and activities during this period, <a href=\"/plant-health/invasive-species/eng/1299168913252/1299168989280\">invasive species and other plant pests</a> may find ways to enter and move around Canada to the detriment of our natural resources and environment.</p>\n<p>Plant protection is an important aspect of the Canadian Food Inspection Agency's (CFIA) mandate. Here are my <strong>12 days of plant protection,</strong> which include things to watch out for and be mindful of during the holiday season.</p>\n<h2>Purchase wisely</h2>\n<ul class=\"lst-none lst-spcd\">\n<li>1. <a href=\"/importing-food-plants-or-animals/e-commerce/plants/eng/1611780200013/1611780200247\">When purchasing gifts online</a>, as I often do, make sure the products do not present a risk to plant health and are allowed in Canada. Do not assume that they are!</li>\n<li>2. Christmas trees present a way for certain plant pests, including the <a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/eng/1329836269430/1329836504450\">spongy moth</a>, from spreading in Canada. Try to obtain yours from a local source. </li>\n<li>3. Firewood is also a potential culprit for spreading plant pests. While many of us enjoy a nice fire during the holidays, remember to <a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">avoid moving firewood</a>. Instead, buy local, burn local.</li>\n<li>4. <a href=\"/plant-health/invasive-species/invasive-plants/eng/1306601411551/1306601522570\">Invasive plants</a> come in all sorts of shapes and sizes, including as seeds. If you plan on offering seed packages as a stocking stuffer, choose wisely and avoid giving a gift that spreads undesirably!</li>\n<li>5. If purchasing a <a href=\"/plant-health/invasive-species/importing-live-snails-for-commercial-purposes/eng/1628689652744/1628689923502\">pet snail</a> over the holidays, be mindful that some are plant pests and are subject to plant protection requirements.</li>\n</ul>\n<h2>Travel smart</h2>\n<p>If you plan to travel during the holiday season, here are some things to think about:</p>\n<ul class=\"lst-none lst-spcd\">\n<li>6. Your vehicle could be carrying unwanted passengers unbeknownst to you. Check for the spongy moth before <a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/moving-across-canada/eng/1629692231648/1629692233085\">moving between certain areas in Canada</a>. </li>\n<li>7. <a href=\"/plant-health/soil/n-l-soil-restrictions/eng/1528323701311/1528323784529\">If leaving Newfoundland and Labrador</a>, avoid spreading soil-borne pests by not carrying certain items such as potatoes, soil or root vegetables.</li>\n<li>8. If traveling to the United States, make sure to check the outside of your vehicle before making the return trip because it could be carrying the unwanted <a href=\"/plant-health/invasive-species/insects/spotted-lanternfly/eng/1612895045716/1612895046419\">spotted lanternfly</a> into Canada.</li>\n<li>9. When coming back to Canada, be sure to <a href=\"https://cbsa-asfc.gc.ca/services/fpa-apa/bringing-apporter-fpa-eng.html\">declare regulated plant commodities to the Canadian Border Services Agency</a>. </li>\n</ul>\n<h2>Learn more about plant pests</h2>\n<p>And here are more tips to consider year-round:</p>\n<ul class=\"lst-none lst-spcd\">\n<li>10. Be familiar with the plant pests of concern where you live. Many resources are available on the CFIA website, including a series of <a href=\"/plant-health/invasive-species/insects/plant-pest-cards/eng/1548085757491/1548085933224\">plant pest cards</a> to help you recognize common pests.</li>\n<li>11. Know the areas regulated for plant pests where you live and travel and what <a href=\"/plant-health/invasive-species/domestic-plant-protection-measures/eng/1629843699782/1629901542997\">movement prohibition and restrictions are in place in Canada</a>. </li>\n<li>12. And finally, if you should find a plant pest where it is not supposed to be, <a href=\"https://science.gc.ca/site/science/en/blogs/cultivating-science/report-plant-pests-help-protect-canadas-plant-resources\">report it</a>!</li>\n</ul>\n<p>Each of us can take action to help prevent plant pests from dampening our collective cheer over the season.</p>\n<p>Happy holidays!</p>\n  \n \n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_the_12_days_of_plant_protection_600x600_1671469175581_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>Par David Bailey, directeur ex\u00e9cutif, Direction de la protection des v\u00e9g\u00e9taux et de la bios\u00e9curit\u00e9, Agence canadienne d'inspection des aliments (ACIA)</p> \n\n<p>Le temps des F\u00eates est l'un de mes moments pr\u00e9f\u00e9r\u00e9s de l'ann\u00e9e. J'aime voir les lumi\u00e8res saisonni\u00e8res color\u00e9es qui illuminent une p\u00e9riode plut\u00f4t ennuyeuse de l'ann\u00e9e, qui \u00e0 son tour, je crois, apporte le bonheur \u00e0 beaucoup et l'occasion de s'arr\u00eater et de passer du temps avec des amis, la famille et des coll\u00e8gues. Il marque aussi le d\u00e9but d'une nouvelle ann\u00e9e et tous les espoirs et r\u00eaves que nous pouvons avoir pour l'ann\u00e9e \u00e0 venir.</p>\n<p>Comme moi, beaucoup se r\u00e9jouissent de la saison des f\u00eates. Malheureusement, cela inclut les esp\u00e8ces envahissantes opportunistes.</p>\n<p>En raison de certaines de nos habitudes, de nos d\u00e9placements et de nos activit\u00e9s durant cette p\u00e9riode, <a href=\"/protection-des-vegetaux/especes-envahissantes/fra/1299168913252/1299168989280\">les esp\u00e8ces envahissantes et les autres phytoravageurs</a> peuvent trouver des moyens d'entrer et de se d\u00e9placer au Canada au d\u00e9triment de nos ressources naturelles et de notre environnement.</p>\n<strong>12\u00a0jours de protection des v\u00e9g\u00e9taux</strong>, qui comprennent des choses \u00e0 surveiller et \u00e0 garder \u00e0 l'esprit pendant la saison des f\u00eates.\n<h2>Achetez sagement</h2>\n  \n<ul class=\"lst-none lst-spcd\">\n<li>1. <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/cybercommerce/vegetaux/fra/1611780200013/1611780200247\">Lorsque vous achetez des cadeaux en ligne</a>, comme je le fais souvent, assurez-vous que les produits ne pr\u00e9sentent aucun risque \u00e0 la protection des v\u00e9g\u00e9taux et sont autoris\u00e9s au Canada. Ne supposez pas qu'ils le sont!</li>\n<li>2. Les arbres de No\u00ebl sont un moyen pour certains organismes nuisibles, y compris la <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/fra/1329836269430/1329836504450\">spongieuse</a>, de se propager au Canada. Essayez d'obtenir le v\u00f4tre aupr\u00e8s d'une source locale. </li>\n<li>3. Le bois de chauffage est aussi un coupable potentiel de propagation des phytoravageurs. Alors que beaucoup d'entre nous profitent d'un joli feu pendant les vacances, n'oubliez pas <a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">d'\u00e9viter de d\u00e9placer du bois de chauffage</a>. Au lieu de cela, achetez local, br\u00fblez local.</li>\n<li>4. Les <a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/fra/1306601411551/1306601522570\">plantes envahissantes</a> sont pr\u00e9sentes sous toutes sortes de formes et de tailles, y compris sous forme de graines. Si vous envisagez d'offrir des paquets de semences pour remplir un bas de No\u00ebl, choisissez sagement et \u00e9vitez de donner un cadeau qui se propage de mani\u00e8re ind\u00e9sirable!</li>\n<li>5. Si vous achetez un <a href=\"/protection-des-vegetaux/especes-envahissantes/importation-des-escargots-vivants/fra/1628689652744/1628689923502\">escargot comme animal de compagnie</a> pendant les vacances, n'oubliez pas que certains sont des phytoravageurs et qu'ils sont soumis \u00e0 des exigences de protection des v\u00e9g\u00e9taux.</li>\n</ul>\n\n<h2>Voyagez intelligemment</h2>\n<p>Si vous pr\u00e9voyez voyager pendant la p\u00e9riode des f\u00eates, voici quelques points \u00e0 consid\u00e9rer\u00a0:</p>\n  \n<ul class=\"lst-none lst-spcd\">\n<li>6. Votre v\u00e9hicule pourrait transporter des passagers ind\u00e9sirables \u00e0 votre insu. V\u00e9rifiez la pr\u00e9sence de la spongieuse avant de <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/demenagement-au-canada/fra/1629692231648/1629692233085\">passer d'une r\u00e9gion \u00e0 l'autre du Canada</a>.</li>\n<li>7.\u00a0<a href=\"/protection-des-vegetaux/terre/restrictions-de-sol-frappant-t-n-l-/fra/1528323701311/1528323784529\">Si vous quittez Terre-Neuve-et-Labrador</a>, \u00e9vitez de r\u00e9pandre des ravageurs transmis par le sol en ne transportant pas certains articles comme des pommes de terre, de la terre ou des racines. </li>\n<li>8. Si vous voyagez aux \u00c9tats-Unis, assurez-vous de v\u00e9rifier l'ext\u00e9rieur de votre v\u00e9hicule avant de faire le voyage de retour parce qu'il pourrait transporter le <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/le-fulgore-tachete/fra/1612895045716/1612895046419\">fulgore tachet\u00e9</a> non d\u00e9sir\u00e9 au Canada.</li>\n<li>9. Lorsque vous revenez au Canada, assurez-vous de <a href=\"https://cbsa-asfc.gc.ca/services/fpa-apa/bringing-apporter-fpa-fra.html\">d\u00e9clarer les produits v\u00e9g\u00e9taux r\u00e9glement\u00e9s \u00e0 l'Agence des services frontaliers du Canada</a>.</li>\n</ul>\n\n<h2>En savoir plus sur les phytoravageurs</h2>\n<p>Et voici d'autres conseils \u00e0 consid\u00e9rer toute l'ann\u00e9e\u00a0:</p>\n  \n<ul class=\"lst-none lst-spcd\">\n<li>10. Familiarisez-vous avec les phytoravageurs qui posent probl\u00e8me dans votre r\u00e9gion. De nombreuses ressources sont disponibles dans le site Web de l'ACIA, y compris une s\u00e9rie de <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/cartes-de-phytoravageurs/fra/1548085757491/1548085933224\">cartes des phytoravageurs</a> pour vous aider \u00e0 reconna\u00eetre les organismes nuisibles communs. </li>\n<li>11. Connaissez les zones r\u00e9glement\u00e9es pour les phytoravageurs o\u00f9 vous vivez et voyagez, ainsi que <a href=\"/protection-des-vegetaux/especes-envahissantes/mesures-de-protection-des-vegetaux-en-territoire-c/fra/1629843699782/1629901542997\">les interdictions et les restrictions de d\u00e9placement en vigueur au Canada</a>.</li>\n<li>12. Et enfin, si vous trouvez un phytoravageur l\u00e0 o\u00f9 il n'est pas cens\u00e9 \u00eatre, <a href=\"https://science.gc.ca/site/science/fr/blogues/cultiver-science/signaler-phytoravageurs-pour-aider-proteger-ressources-vegetales-canada\">signalez\u2011le</a>!</li>\n</ul>\n\n<p>Chacun d'entre nous peut prendre des mesures pour aider \u00e0 emp\u00eacher les phytoravageurs d'att\u00e9nuer notre enthousiasme collectif pendant la saison.</p>\n<p>Joyeuses f\u00eates!</p>\n \n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}