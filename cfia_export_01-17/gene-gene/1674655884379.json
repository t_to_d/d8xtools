{
    "dcr_id": "1674655884379",
    "title": {
        "en": "New technology to detect and identify plant diseases",
        "fr": "Nouvelle technologie pour d\u00e9tecter et d\u00e9terminer les maladies des plantes"
    },
    "html_modified": "26-1-2023",
    "modified": "25-1-2023",
    "issued": "25-1-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/ins_pro_tech_id_disease_1674655884379_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/ins_pro_tech_id_disease_1674655884379_fra"
    },
    "parent_ia_id": "1565298312402",
    "ia_id": "1674655884895",
    "parent_node_id": "1565298312402",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "New technology to detect and identify plant diseases",
        "fr": "Nouvelle technologie pour d\u00e9tecter et d\u00e9terminer les maladies des plantes"
    },
    "label": {
        "en": "New technology to detect and identify plant diseases",
        "fr": "Nouvelle technologie pour d\u00e9tecter et d\u00e9terminer les maladies des plantes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1674655884895",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298312168",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/science-and-innovation/new-technology/",
        "fr": "/inspecter-et-proteger/science-et-innovation/nouvelle-technologie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "New technology to detect and identify plant diseases",
            "fr": "Nouvelle technologie pour d\u00e9tecter et d\u00e9terminer les maladies des plantes"
        },
        "description": {
            "en": "With the help an online tool, scientists at the CFIA\u2019s Charlottetown Laboratory can quickly pinpoint plant pathogens based on their DNA. That\u2019s great news for our crops and ecosystems!",
            "fr": "Gr\u00e2ce \u00e0 un outil en ligne, les scientifiques du laboratoire de Charlottetown de l'ACIA peuvent rapidement identifier les agents pathog\u00e8nes des plantes en fonction de leur ADN. C'est une excellente nouvelle pour nos cultures et nos \u00e9cosyst\u00e8mes!"
        },
        "keywords": {
            "en": "Plant pests, protect plant resources, cultivating science, CFIA, scientists, potential food safety investigations.",
            "fr": "Signaler les phytoravageurs, prot\u00e9ger les ressources v\u00e9g\u00e9tales, ACIA, scientifiques, \u00e9ventuelles enqu\u00eates sur la salubrit\u00e9 alimentaire"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-01-26",
            "fr": "2023-01-26"
        },
        "modified": {
            "en": "2023-01-25",
            "fr": "2023-01-25"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "New technology to detect and identify plant diseases",
        "fr": "Nouvelle technologie pour d\u00e9tecter et d\u00e9terminer les maladies des plantes"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263148420\"></div>\n\n<p>By <a href=\"https://profils-profiles.science.gc.ca/en/profile/li-xiang-sean\">Dr. Sean Li</a>, Khanh Duong and Nathan Buchanan</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_tech_id_disease_600x600_1674655996005_eng.jpg\" alt=\"\" class=\"img-thumbnail img-responsive\">\n</figure>\n</div>\n\n<p>This blog post was originally published to <a href=\"https://science.gc.ca/eic/site/063.nsf/eng/h_97679.html\">Cultivating Science</a> on science.gc.ca.</p>\n\n<blockquote>\n<p>As international trade increases, a growing number of plant diseases are threatening global agriculture and food security. The Canadian Food Inspection Agency (CFIA) is constantly seeking ways to improve plant health while supporting Canada's economy.</p>\n<p>Early detection and identification of these diseases help the CFIA protect Canada's crops and the environment while facilitating trade. At the CFIA's Charlottetown Laboratory, a new online tool called Clasnip combines genetic data and computer science to quickly and accurately identify diseases.</p>\n</blockquote>\n\n<p><a href=\"https://science.gc.ca/site/science/en/blogs/cultivating-science/new-technology-detect-and-identify-plant-diseases\">Continue reading to learn more</a>.</p>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263148420\"></div>\n\n<p>Par <a href=\"https://profils-profiles.science.gc.ca/fr/profil/li-xiang-sean\">Dr Sean Li</a>, Khanh Duong et Nathan Buchanan</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_tech_id_disease_600x600_1674655996005_fra.jpg\" alt=\"\" class=\"img-thumbnail img-responsive\">\n</figure>\n</div>\n\n<p>Ce billet de blogue est d'abord paru dans <a href=\"https://science.gc.ca/eic/site/063.nsf/fra/h_97679.html\">Cultiver la science</a> sur science.gc.ca.</p>\n\n<blockquote>\n<p>Avec l'augmentation du commerce international, un nombre croissant de maladies des plantes menacent l'agriculture et la s\u00e9curit\u00e9 alimentaire mondiales. L'Agence canadienne d'inspection des aliments (ACIA) cherche constamment des moyens d'am\u00e9liorer la sant\u00e9 des v\u00e9g\u00e9taux tout en soutenant l'\u00e9conomie canadienne.</p>\n<p>La d\u00e9tection et la d\u00e9termination pr\u00e9coces de ces maladies aident l'ACIA \u00e0 prot\u00e9ger les cultures et l'environnement du Canada tout en appuyant le commerce. Au laboratoire de Charlottetown de l'ACIA, un nouvel outil en ligne, appel\u00e9 Clasnip, combine les donn\u00e9es g\u00e9n\u00e9tiques et l'informatique pour d\u00e9terminer rapidement et pr\u00e9cis\u00e9ment les maladies.</p>\n</blockquote>\n\n<p><a href=\"https://science.gc.ca/site/science/fr/blogues/cultiver-science/nouvelle-technologie-pour-detecter-determiner-maladies-plantes\">Lisez le billet de blogue complet.</a></p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}