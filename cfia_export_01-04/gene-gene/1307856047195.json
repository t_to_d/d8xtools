{
    "dcr_id": "1307856047195",
    "title": {
        "en": "T-4-<span class='wb-invisible'> </span>105 \u2013 Requirements for seeds treated with fertilizers or supplements",
        "fr": "T-4-<span class='wb-invisible'> </span>105 \u2013 Exigences relatives aux semences trait\u00e9es avec des engrais ou suppl\u00e9ments"
    },
    "html_modified": "2011-06-12 01:20",
    "modified": "26-10-2023",
    "issued": "12-6-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/fert_tmemo_4-105_1307856047195_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/fert_tmemo_4-105_1307856047195_fra"
    },
    "parent_ia_id": "1299873786929",
    "ia_id": "1307856175577",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "T-4-<span class='wb-invisible'> </span>105 \u2013 Requirements for seeds treated with fertilizers or supplements",
        "fr": "T-4-<span class='wb-invisible'> </span>105 \u2013 Exigences relatives aux semences trait\u00e9es avec des engrais ou suppl\u00e9ments"
    },
    "label": {
        "en": "T-4-<span class='wb-invisible'> </span>105 \u2013 Requirements for seeds treated with fertilizers or supplements",
        "fr": "T-4-<span class='wb-invisible'> </span>105 \u2013 Exigences relatives aux semences trait\u00e9es avec des engrais ou suppl\u00e9ments"
    },
    "templatetype": "content page 1 column",
    "node_id": "1307856175577",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1299873703612",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/trade-memoranda/t-4-105/",
        "fr": "/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-105/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "T-4- 105 \u2013 Requirements for seeds treated with fertilizers or supplements",
            "fr": "T-4- 105 \u2013 Exigences relatives aux semences trait\u00e9es avec des engrais ou suppl\u00e9ments"
        },
        "description": {
            "en": "The purpose of this document is to outline registration and labelling requirements for microbial supplements regulated under the Fertilizers Act.",
            "fr": "L'objectif du pr\u00e9sent document est de d\u00e9finir les exigences d'enregistrement et d'\u00e9tiquetage des suppl\u00e9ments microbiens r\u00e9glement\u00e9s par la Loi sur les engrais."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizers Regulations, fertilizers, supplements, trade memoranda, t-memo, labelling, import, registration, requirements, growing, media, containing fertilizers or supplements, T-4-105, Requirements for Microbial Supplements",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, engrais, suppl\u00e9ments, circulaire \u00e0 la profession, circulaire, s\u00e9curit\u00e9, importation, enregistrement, exigences, relatives, milieux, culture, contenant des engrais ou des suppl\u00e9ments, T-4-105, Exigences relatives aux suppl\u00e9ments microbiens"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-06-12 01:20:49",
            "fr": "2011-06-12 01:20:49"
        },
        "modified": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "type": {
            "en": "policy,reference material",
            "fr": "politique,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "T-4- 105 \u2013 Requirements for seeds treated with fertilizers or supplements",
        "fr": "T-4- 105 \u2013 Exigences relatives aux semences trait\u00e9es avec des engrais ou suppl\u00e9ments"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The 3 year regulatory transition period (October 26, 2020 to October 26, 2023) has now ended. As a result, regulated parties, including all manufacturers, importers, distributors and sellers of fertilizers and supplements must adhere to the amended <a href=\"/english/reg/jredirect2.shtml?ferengr\"><i>Fertilizers Regulations</i></a>. There are few notable exceptions for some product categories. Learn more about the <a href=\"/plant-health/fertilizers/notices-to-industry/2023-05-03/eng/1682633127754/1682633128598\">implementation of the amended <i>Fertilizers Regulations</i></a>.</p>\n</div>\n\n<h2>1. Purpose</h2>\n\n<p>The purpose of the document is to outline registration and labelling requirements for seed treated with fertilizers or supplements under the <i>Fertilizers Act</i> and Regulations.</p>\n\n<h2>2. Exemptions from registration for treated seeds</h2>\n\n<p>The <i>Fertilizers Regulations</i> exempt seeds treated with fertilizers, supplements or both, only if each of those fertilizers or supplements is either:</p>\n\n<ol class=\"lst-lwr-alph\">\n<li>exempt from registration; or</li>\n<li>is registered for use with those seeds</li>\n</ol>\n\n<p>Treated seeds that meet the exemption from registration above are still subject to safety standards and labelling requirements as prescribed in the <i>Fertilizers Regulations</i>. If the above conditions (a-b) for each of the fertilizers or supplements added to the seed are not met, the final treated seed product requires registration under the <i>Fertilizers Act</i>. <strong>Note</strong> that treating seeds with fertilizers or supplements is not considered manufacturing. Therefore, the exemption for fertilizers and supplements imported or manufactured in Canada for manufacturing purposes only does not apply to seed treatments.</p>\n\n<h2>3. Labelling requirements\u00a0\u2013 <i>Fertilizers Act</i></h2>\n\n<p>Seeds treated with fertilizers or supplements meet the definition(s) of a fertilizer or supplement and as such are subject to the same requirements prescribed by the Regulations as fertilizers or supplements themselves.</p>\n\n<h3>3.1 Mandatory labelling of registered or exempt ingredients</h3>\n\n<p>Seeds treated with registered fertilizers or supplements or mixtures thereof <strong>must</strong> be labelled with the registration number of any registered component/ingredient it contains. If the seed is treated with one or more fertilizer or supplement material(s) present on the <a href=\"/plant-health/fertilizers/regulatory-modernization/list-of-primary-fertilizer-and-supplement-material/eng/1533213691120/1533213691867\">List of Materials</a>  the term for each component must be included on the label together with any other information that is sufficient to demonstrate that the final product is exempt from registration. The record keeping alternative in lieu of mandatory labelling as described in <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-130/eng/1604424185581/1604424268008\">Trade Memorandum T-4-130</a> does not apply to seeds treated with fertilizers or supplements.</p>\n\n<h3>3.2 Guaranteed analysis</h3>\n\n<p>In addition to the information required for all fertilizers and supplements; the guaranteed analysis section on a label of seeds treated with a fertilizer or supplement must show the minimum percentage of active ingredient(s), expressed on a weight by weight basis of the <strong>final treated product</strong>. For instance, if seeds have a bulk density of 720 kg/m3 and are treated with a fertilizer or supplement that contains 20% of the active ingredient (XXXX) added at a rate of 0.15 L/kg1seed, then the percentage of the active ingredient in the final, treated product is:<br>\n<br>\n<br>\n<strong>Guaranteed analysis</strong>:<br>\n</p>\n\n<h3>3.3 Precautionary statements</h3>\n\n<p>The labels/seed tags of treated seed products (whether sold in bags or bulk) must contain all applicable precautionary statements that appear on the registered fertilizer or supplement label. For safety standards for fertilizers and supplements please refer to <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-93/eng/1305611387327/1305611547479\">Trade memorandum T-4-93</a>.</p>\n\n<h3>3.4 Directions for use</h3>\n\n<p>Where a seed is treated with fertilizers, supplements or both, the product label for the treated seed is not required to bear directions for use associated with the fertilizers or supplements with which the seeds were treated.</p>\n\n<h2>4. Requirements other than those under the <i>Fertilizers Act</i> and Regulations</h2>\n\n<h3>4.1 <i>Seeds Act</i> and Regulations</h3>\n\n<p>While the <i>Fertilizers Act</i> and Regulations contain requirements for seeds treated with fertilizers or supplements, or both, this is not the sole piece of federal legislation that contains requirements for seeds. The CFIA's Seed Section is responsible for the administration of the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a> and <a href=\"/english/reg/jredirect2.shtml?seesemr\">Regulations</a> to help ensure that seeds sold in, imported into and exported from Canada meet established standards for quality and are labelled so that they are properly represented in the marketplace, and the variety is registered prior to sale in Canada (most agricultural crop varieties). Please refer to the <a href=\"/plant-health/seeds/eng/1299173228771/1299173306579\">CFIA Seed Section</a>'s website for additional information regarding import and export requirements, industry guidance, inspection procedures, testing and grading of seeds.</p>\n\n<h2>5. Contact information</h2>\n\n<p>Fertilizer Safety Section<br>\n<br>\n<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\" class=\"nowrap\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n<p>La p\u00e9riode de transition r\u00e9glementaire de 3 ans (26 octobre 2020 au 26 octobre 2023) est termin\u00e9e. Par cons\u00e9quent, les parties r\u00e9glement\u00e9es, y compris tous les fabricants, importateurs, distributeurs et vendeurs d'engrais et de suppl\u00e9ments, doivent adh\u00e9rer au <a href=\"/francais/reg/jredirect2.shtml?ferengr\"><i>R\u00e8glement sur les engrais</i></a> modifi\u00e9. Il existe quelques exceptions notables pour certaines cat\u00e9gories de produits. Apprenez-en davantage sur la <a href=\"/protection-des-vegetaux/engrais/avis-a-l-industrie/2023-05-03/fra/1682633127754/1682633128598\">mise en \u0153uvre de la modification du <i>R\u00e8glement sur les engrais</i></a>.</p>\n</div>\n\n<h2>1. Objectif</h2>\n\n<p>L'objectif du pr\u00e9sent document est de d\u00e9finir les exigences d'enregistrement et d'\u00e9tiquetage des semences trait\u00e9es avec des engrais ou des suppl\u00e9ments, en vertu de la <i>Loi sur les engrais</i> et son r\u00e8glement d'application.</p>\n\n<h2>2. Exemptions de l'enregistrement des semences trait\u00e9es</h2>\n\n<p>Le <i>R\u00e8glement sur les engrais</i> pr\u00e9voit que les semences trait\u00e9es avec des engrais, des suppl\u00e9ments, ou les deux, sont exempt\u00e9es de l'enregistrement uniquement si chaque engrais ou suppl\u00e9ment utilis\u00e9 r\u00e9pond \u00e0 l'un des crit\u00e8res suivants\u00a0:</p>\n\n<ol class=\"lst-lwr-alph\">\n<li>il est exempt\u00e9 de l'enregistrement;</li>\n<li>il est enregistr\u00e9 pour usage avec ces semences.</li>\n</ol>\n\n<p>Les semences trait\u00e9es qui satisfont aux crit\u00e8res d'exemption de l'enregistrement mentionn\u00e9s ci-dessus demeurent assujetties aux normes d'innocuit\u00e9 et aux exigences d'\u00e9tiquetage prescrites dans le <i>R\u00e8glement sur les engrais</i>. Si les conditions ci-dessus (a et b) pour chacun des engrais et suppl\u00e9ments ajout\u00e9s \u00e0 la semence ne sont pas satisfaites, le produit de semence trait\u00e9 final doit \u00eatre enregistr\u00e9 en vertu de la <i>Loi sur les engrais</i>. <strong>Veuillez prendre note</strong> que le fait de traiter des semences avec des engrais ou des suppl\u00e9ments n'est pas consid\u00e9r\u00e9 comme un traitement suppl\u00e9mentaire. Par cons\u00e9quent, l'exemption des engrais et des suppl\u00e9ments import\u00e9s ou fabriqu\u00e9s au Canada \u00e0 des fins de fabrication seulement ne s'applique pas au traitement des semences.</p>\n\n<h2>3. Exigences d'\u00e9tiquetage\u00a0\u2013 <i>Loi sur les engrais</i></h2>\n\n<p>Les semences trait\u00e9es avec des engrais ou des suppl\u00e9ments, ou avec les deux, r\u00e9pondent \u00e0 la d\u00e9finition d'un engrais ou d'un suppl\u00e9ment et sont donc assujetties aux m\u00eames exigences du R\u00e8glement que les engrais ou les suppl\u00e9ments.</p>\n\n<h3>3.1 \u00c9tiquetage obligatoire des ingr\u00e9dients enregistr\u00e9s ou exempt\u00e9s</h3>\n\n<p>Les semences trait\u00e9es avec des engrais, des suppl\u00e9ments ou des m\u00e9langes d'engrais et de suppl\u00e9ments enregistr\u00e9s <strong>doivent</strong> porter sur leur \u00e9tiquette le num\u00e9ro d'enregistrement de tout composant ou ingr\u00e9dient enregistr\u00e9 qu'ils contiennent. Si la semence est trait\u00e9e avec un ou plusieurs engrais ou suppl\u00e9ments pr\u00e9sents sur la <a href=\"/protection-des-vegetaux/engrais/modernisation-de-la-reglementation/liste-des-composants-de-base-des-engrais-et-supple/fra/1533213691120/1533213691867\">Liste des composants</a>, le terme de chacun des composants doit \u00eatre inclus sur l'\u00e9tiquette, de m\u00eame que tout autre renseignement qui suffit \u00e0 d\u00e9montrer que le produit final est exempt\u00e9 d'enregistrement. L'option permettant de conserver des documents plut\u00f4t que d'\u00e9tiqueter le produit, d\u00e9crite dans la <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-130/fra/1604424185581/1604424268008\">circulaire \u00e0 la profession T-4-130</a>, ne s'applique pas aux semences trait\u00e9es avec des engrais ou des suppl\u00e9ments.</p>\n\n<h3>3.2 Analyse garantie</h3>\n\n<p>Outre les renseignements obligatoires pour tous les engrais et suppl\u00e9ments, la section d'analyse garantie de l'\u00e9tiquette d'une semence trait\u00e9e avec un engrais ou un suppl\u00e9ment doit montrer le pourcentage minimal de l'ingr\u00e9dient actif ou des ingr\u00e9dients actifs, exprim\u00e9 en pourcentage poids par poids du <strong>produit final trait\u00e9</strong>. Par exemple, si des semences ont une masse volumique de 720 kg/m3 et sont trait\u00e9es avec un engrais ou un suppl\u00e9ment qui contient 20\u00a0% de l'ingr\u00e9dient actif (XXXX) ajout\u00e9 \u00e0 un taux de 0,15 L/kg de semence, alors le pourcentage de l'ingr\u00e9dient actif dans le produit trait\u00e9 final est de\u00a0:<br>\n<br>\n<br>\n<strong>Analyse garantie</strong>\u00a0:<br>\n</p>\n\n<h3>3.3 Mises en garde</h3>\n\n<p>Les \u00e9tiquettes des semences trait\u00e9es (qu'elles soient vendues dans des sacs ou en vrac) doivent comprendre toutes les mises en garde applicables qui se trouvent sur l'\u00e9tiquette de l'engrais ou du suppl\u00e9ment enregistr\u00e9. Pour conna\u00eetre les normes d'innocuit\u00e9 qui s'appliquent aux engrais et aux suppl\u00e9ments, consultez la <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-93/fra/1305611387327/1305611547479\">circulaire \u00e0 la profession T-4-93</a>.</p>\n\n<h3>3.4 Mode d'emploi</h3>\n\n<p>Lorsqu'une semence est trait\u00e9e avec des engrais, des suppl\u00e9ments ou les deux, l'\u00e9tiquette du produit pour la semence trait\u00e9e n'est pas tenue d'indiquer le mode d'emploi associ\u00e9 aux engrais ou aux suppl\u00e9ments avec lequel les semences ont \u00e9t\u00e9 trait\u00e9es.</p>\n\n<h2>4. Exigences autres que celles relevant de la <i>Loi sur les engrais</i> et son r\u00e8glement d'application</h2>\n\n<h3>4.1 <i>Loi sur les semences</i> et son r\u00e8glement d'application</h3>\n\n<p>Section des semences de l'ACIA est charg\u00e9e d'administrer la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a> et le <a href=\"/francais/reg/jredirect2.shtml?seesemr\">R\u00e8glement</a> sur les semences afin de garantir que les semences vendues ou import\u00e9es au Canada, ou export\u00e9es du pays, satisfont aux crit\u00e8res de qualit\u00e9 \u00e9tablis et sont \u00e9tiquet\u00e9es de fa\u00e7on \u00e0 ce qu'elles soient correctement repr\u00e9sent\u00e9es sur le march\u00e9 et, dans le cas de la plupart des vari\u00e9t\u00e9s agricoles, qu'elles soient enregistr\u00e9es avant la vente au Canada. Veuillez consulter le site Web de la <a href=\"/protection-des-vegetaux/semences/fra/1299173228771/1299173306579\">Section des semences de l'ACIA</a> pour plus d'informations sur les exigences d'importation et d'exportation, les directives \u00e0 l'industrie, les proc\u00e9dures d'inspection, les essais et le classement des semences.</p>\n\n<h2>5. Coordonn\u00e9es</h2>\n\n<p>Section de l'innocuit\u00e9 des engrais<br>\n<br>\n<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\" class=\"nowrap\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}