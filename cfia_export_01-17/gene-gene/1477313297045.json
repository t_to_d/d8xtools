{
    "dcr_id": "1477313297045",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Prunella vulgaris</i> (Heal-all)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Prunella vulgaris</i> (Prunelle vulgaire)"
    },
    "html_modified": "7-11-2017",
    "modified": "7-11-2017",
    "issued": "24-10-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_prunella_vulgaris_1477313297045_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_prunella_vulgaris_1477313297045_fra"
    },
    "parent_ia_id": "1333136685768",
    "ia_id": "1477313297535",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Prunella vulgaris</i> (Heal-all)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Prunella vulgaris</i> (Prunelle vulgaire)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Prunella vulgaris</i> (Heal-all)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Prunella vulgaris</i> (Prunelle vulgaire)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1477313297535",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/prunella-vulgaris/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/prunella-vulgaris/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Prunella vulgaris (Heal-all)",
            "fr": "Semence de mauvaises herbe : Prunella vulgaris (Prunelle vulgaire)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Prunella vulgaris, Lamiaceae, Heal-all",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, esp\u00e8ces, Prunella vulgaris, Lamiaceae, Prunelle vulgaire"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,general public,government,scientists",
            "fr": "entreprises,grand public,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Prunella vulgaris (Heal-all)",
        "fr": "Semence de mauvaises herbe : Prunella vulgaris (Prunelle vulgaire)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Lamiaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Heal-all</p>\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 4 and Noxious, Class 5 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs throughout Canada except in <abbr title=\"Nunavut\">NU</abbr> and <abbr title=\"Northwest Territories\">NT</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). There are native and introduced populations (Darbyshire 2003<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n<p><strong>Worldwide:</strong> Exact native range obscure, present throughout most of North America, Europe, northern Africa and Asia and introduced in other temperate areas such as Australia, New Zealand and southern Africa (eFloras 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, <abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Nutlet</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Nutlet length: 1.8 - 2.3 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Nutlet width: 1.3 - 1.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Nutlet thickness: 1.0 - 1.3 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Obovate nutlet, compressed</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Nutlet smooth, shiny</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Nutlet reddish-brown</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hilum at narrow end of nutlet, associated with white tissue</li>\n<li>Distinctive dark lines run down centre of each nutlet face and around the outside</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Old fields, gardens, pastures, grasslands, lawns, turf, thickets, open   forests, riparian and wetland sites, roadsides, railway lines and   disturbed areas (Darbyshire 2003<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>, DiTomaso and Healy 2007<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Many cultures have used heal-all to help heal wounds and deal with inflammation (Oh et al. 2011<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). This species is reported to have both antiviral and antibacterial properties (Oh et al. 2011<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Large-flowered selfheal (<i lang=\"la\">Prunella grandiflora</i>)</h3>\n<ul>\n<li>Large-flowered selfheal nutlets are a similar size, reddish-brown colour, similar white tissue at the hilum and dark lines as heal-all.</li>\n\n<li>Large-flowered selfheal nutlets are an oval shape with up to six dark lines on the dorsal side of the nutlet compared to heal-all nutlets with 2 lines on the dorsal side.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_vulgaris_04cnsh_1476891120122_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Heal-all (<i lang=\"la\">Prunella vulgaris</i>) nutlets</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_vulgaris_02cnsh_1476891043741_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Heal-all (<i lang=\"la\">Prunella vulgaris</i>) nutlet</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_vulgaris_03cnsh_1476891081771_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Heal-all (<i lang=\"la\">Prunella vulgaris</i>) nutlet, side view</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_grandiflora_02cnsh_1476890995347_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Large-flowered selfheal (<i lang=\"la\">Prunella grandiflora</i>) nutlets</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_grandiflora_01cnsh_1476890955479_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Large-flowered selfheal (<i lang=\"la\">Prunella grandiflora</i>) nutlet</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>eFloras. 2016</strong>. Electronic Floras. Missouri Botanical Garden, St. Louise, MO &amp; Harvard University Herbaria, Cambridge, MA., http://www.efloras.org [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>DiTomaso, J. M. and Healy, E. A. 2007</strong>. Weeds of California and Other Western States. Vol. 1. 834 pp. University of California, CA.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Oh, C., Price, J., Brindley, M. A., Widrlechner, M. P., Luping, Q., McCoy, J-A., Murphy, P., Hauck, C. and Maury, W. 2011</strong>. Inhibition of HIV-1 Infection by Aqueous Extracts of <i lang=\"la\">Prunella vulgaris</i> L. Virology Journal 8: 188.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Lamiaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Prunelle vulgaire</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie 4, et mauvaise herbe nuisible, cat\u00e9gorie 5 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> L'esp\u00e8ce est pr\u00e9sente dans tout le Canada, sauf au <abbr title=\"Nunavut\">Nt</abbr> et dans les <abbr title=\"Territoires du Nord-Ouest\">T.N-O.</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>). Il existe des populations indig\u00e8nes et des populations introduites (<span lang=\"en\">Darbyshire</span>, 2003<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Aire d'indig\u00e9nat exacte incertaine. Pr\u00e9sente dans la majeure partie de   l'Am\u00e9rique du Nord, en Europe, en Afrique septentrionale et en Asie. A   \u00e9t\u00e9 introduite dans d'autres r\u00e9gions temp\u00e9r\u00e9es, notamment en Australie,   en Nouvelle-Z\u00e9lande et en Afrique australe (<span lang=\"en\">eFloras</span>, 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>; <abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Nucule</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la nucule\u00a0: 1,8 \u00e0 2,3 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la nucule\u00a0: 1,3 \u00e0 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>\u00c9paisseur  de la nucule\u00a0: 1,0 \u00e0 1,3 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Nucule obov\u00e9e, comprim\u00e9e</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Nucule lisse et luisante</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> <ul class=\"mrgn-lft-lg\">\n<li>Nucule brun rouge\u00e2tre</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hile situ\u00e9 \u00e0 l'extr\u00e9mit\u00e9 \u00e9troite de la nucule, associ\u00e9 avec des tissus blancs</li>\n<li>Lignes fonc\u00e9es distinctives traversant le centre de chaque face de la nucule et entourant l'ext\u00e9rieur</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs abandonn\u00e9s, jardins, p\u00e2turages, prairies, pelouses, gazons,   fourr\u00e9s, for\u00eats clairsem\u00e9es, zones riveraines, milieux humides, bords de   chemin, voies ferr\u00e9es et terrains perturb\u00e9s  (<span lang=\"en\">Darbyshire</span>, 2003<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>; <span lang=\"en\">DiTomaso</span> et <span lang=\"en\">Healy</span>, 2007<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>De nombreuses cultures utilisaient la prunelle vulgaire pour soigner des plaies et r\u00e9duire l'inflammation (Oh et al., 2011<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>). L'esp\u00e8ce aurait des propri\u00e9t\u00e9s antivirales et antibact\u00e9riennes (Oh et al., 2011<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Prunelle  \u00e0 grandes fleurs (<i lang=\"la\">Prunella grandiflora</i>)</h3>\n<ul>\n<li>Les nucules de la prunelle \u00e0 grandes fleurs ressemblent \u00e0 celles de la prunelle commune par leurs dimensions, leur couleur brun rouge\u00e2tre, leurs tissus hilaires blancs et leurs lignes fonc\u00e9es caract\u00e9ristiques.</li>\n\n<li>Par contre, les nucules de la prunelle \u00e0 grandes fleurs sont elliptiques et pr\u00e9sentent jusqu'\u00e0 six lignes fonc\u00e9es sur leur face dorsale alors que les nucules de la prunelle commune n'en ont que deux.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_vulgaris_04cnsh_1476891120122_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Prunelle vulgaire (<i lang=\"la\">Prunella vulgaris</i>) nucules</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_vulgaris_02cnsh_1476891043741_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Prunelle vulgaire (<i lang=\"la\">Prunella vulgaris</i>) nucule</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_vulgaris_03cnsh_1476891081771_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Prunelle vulgaire (<i lang=\"la\">Prunella vulgaris</i>) nucule, la vue de c\u00f4t\u00e9</figcaption>\n</figure>\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_grandiflora_02cnsh_1476890995347_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Prunelle \u00e0 grandes fleurs (<i lang=\"la\">Prunella grandiflora</i>) nucules</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_prunella_grandiflora_01cnsh_1476890955479_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Prunelle \u00e0 grandes fleurs (<i lang=\"la\">Prunella grandiflora</i>) nucule</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>eFloras. 2016</strong>. Electronic Floras. Missouri Botanical Garden, St. Louise, MO &amp; Harvard University Herbaria, Cambridge, MA., http://www.efloras.org [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>DiTomaso, J. M. and Healy, E. A. 2007</strong>. Weeds of California and Other Western States. Vol. 1. 834 pp. University of California, CA.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Oh, C., Price, J., Brindley, M. A., Widrlechner, M. P., Luping, Q., McCoy, J-A., Murphy, P., Hauck, C. and Maury, W. 2011</strong>. Inhibition of HIV-1 Infection by Aqueous Extracts of <i lang=\"la\">Prunella vulgaris</i> L. Virology Journal 8: 188.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}