{
    "dcr_id": "1576772414149",
    "title": {
        "en": "Cyanide in apricot kernels",
        "fr": "Cyanure dans les noyaux d'abricot"
    },
    "html_modified": "19-12-2019",
    "modified": "1-12-2020",
    "issued": "19-12-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/cyanide_apricot_1576772414149_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/cyanide_apricot_1576772414149_fra"
    },
    "parent_ia_id": "1544505212400",
    "ia_id": "1576772414633",
    "parent_node_id": "1544505212400",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Cyanide in apricot kernels",
        "fr": "Cyanure dans les noyaux d'abricot"
    },
    "label": {
        "en": "Cyanide in apricot kernels",
        "fr": "Cyanure dans les noyaux d'abricot"
    },
    "templatetype": "content page 1 column",
    "node_id": "1576772414633",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1544505197742",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-import-notices-for-industry/notice-2019-12-19/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/avis-d-importation-d-aliments-pour-l-industrie/avis-2019-12-19/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Cyanide in apricot kernels",
            "fr": "Cyanure dans les noyaux d'abricot"
        },
        "description": {
            "en": "New maximum level of cyanide in apricot kernels",
            "fr": "Nouvelle concentration maximale de cyanure dans les noyaux dabricot"
        },
        "keywords": {
            "en": "Safe Food for Canadians Regulations, SFCR, apricot, apricot kernels, cyanide, maximum level",
            "fr": "R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, abricot, noyaux dabricot, cyanure, concentration maximale"
        },
        "dcterms.subject": {
            "en": "food inspection,policy,agri-food products,food safety",
            "fr": "inspection des aliments,politique,produit agro-alimentaire,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-12-19",
            "fr": "2019-12-19"
        },
        "modified": {
            "en": "2020-12-01",
            "fr": "2020-12-01"
        },
        "type": {
            "en": "guide,reference material",
            "fr": "guide,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Cyanide in apricot kernels",
        "fr": "Cyanure dans les noyaux d'abricot"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>On July 25, 2019, Health Canada published a <a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/public-involvement-partnerships/modification-permitted-maximum-level-cyanide-apricot-kernels.html\">Notice of Modification to the List of Contaminants and Other Adulterating Substances in Foods to Add a Maximum Level for Cyanide in Apricot Kernels</a> of <strong>20 parts per million</strong> (ppm) total extractable cyanide in apricot kernels sold for human consumption or used as an ingredient in other foods. The new maximum level aligns with the precautionary approaches adopted internationally to manage the potential health risks associated with consuming apricot kernels. This maximum level will be effective <strong>January 25, 2020</strong>.</p>\n\n<p>You are responsible for the safety of the food you produce or import. After January 25, 2020, you must be able to demonstrate that the concentration of total extractable cyanide in apricot kernels for human consumption is not more than the 20 ppm maximum level.</p> \n\n<p>One way to demonstrate compliance to the maximum level is to test your apricot kernels. The Canadian Food Inspection Agency (CFIA) recommends that analyses should be performed by an accredited laboratory. There are two national accreditation bodies in Canada for testing laboratories (ISO 17025), the <a href=\"https://www.scc.ca/\">Standards Council of Canada</a> and the <a href=\"https://cala.ca/\">Canadian Association for Laboratory Accreditation</a>. These organisations' websites could be searched to determine whether any Canadian laboratories are accredited to test for total extractable cyanide. If there is no accredited lab in Canada that has a method in their scope for total extractable cyanide, the CFIA would also consider results from an accredited lab in another country, provided the lab is accredited by an accreditation body that is a signatory to the <a href=\"https://ilac.org/ilac-mra-and-signatories/\">International Laboratory Accreditation Cooperation (ILAC) Mutual Recognition Arrangement</a>.  ILAC provides a <a href=\"https://ilac.org/signatory-search/?q=s\">search tool</a> that can be used to identify accredited laboratories.</p>\n\n<p>All apricot kernels that do not meet the maximum level of 20\u00a0ppm for total extractable cyanide cannot be sold for human consumption or used as an ingredient in other foods after January\u00a025,\u00a02020.</p>\n\n<p>You are also responsible for making sure your foods comply with the labelling and consumer protection requirements of the <i>Safe Food for Canadians Regulations</i> and <i>Food and Drug Regulations</i> (FDR). This includes the use of any claims, stated or implied, that food can treat or prevent diseases and health conditions, such as cancer or diabetes. Such claims on foods are subject to mandatory pre-market assessment under the <i>Food and Drugs Act</i>. Health Canada has not reviewed any such claims for apricot kernels sold as a food.</p> \n\n<p>Also, \"Vitamin B17\" is not a recognized vitamin under the FDR. Therefore, any food, including apricot kernels, making a statement or claim relating to its \"Vitamin B17\" content would be considered to be in violation of the FDR.</p> \n\n<p>If you have further questions contact your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a> for more information.</p>\n\n<h2>Supporting information</h2>\n\n<ul><li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/public-involvement-partnerships/proposal-update-maximum-levels-cyanide-apricot-kernals.html\">Notice of Health Canada's Proposal to Add Cyanide in Apricot Kernels to the List of Contaminants and Other Adulterating Substances in Foods \u2013 Reference Number: NOP/ADP C-2018-1</a></li>\n<li><a href=\"/food-labels/labelling/industry/eng/1383607266489/1383607344939\">Food Labelling for Industry</a></li></ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le 25 juillet 2019, Sant\u00e9 Canada a publi\u00e9 un <a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/participation-public-partenariats/modification-enzymes-alimentaires-concentration-maximale-cyanure-noyaux-abricot.html\">Avis de modification \u00e0 la Liste des contaminants et des autres substances adult\u00e9rantes dans les aliments afin d'ajouter une concentration maximale en cyanure dans les noyaux d'abricot</a>, soit <strong>20 parties par million</strong> (ppm) de cyanure total extractible dans les noyaux d'abricot vendus pour la consommation humaine ou utilis\u00e9s comme ingr\u00e9dients dans d'autres aliments. La nouvelle concentration maximale \u00e9tablie cadre avec les mesures de pr\u00e9vention adopt\u00e9es \u00e0 l'\u00e9chelle internationale pour g\u00e9rer les risques pour la sant\u00e9 que pourrait poser la consommation de noyaux d'abricot. Cette concentration maximale sera en vigueur \u00e0 compter du <strong>25 janvier 2020</strong>.</p>\n\n<p>Vous \u00eates responsable de la salubrit\u00e9 de l'aliment que vous produisez ou importez. \u00c0 compter du 25 janvier 2020, vous devrez pouvoir prouver que la concentration de cyanure total extractible dans les noyaux d'abricot vendus pour la consommation humaine ne d\u00e9passe pas la limite maximale de 20 ppm.</p> \n\n<p>Une fa\u00e7on de prouver que votre produit respecte la concentration maximale \u00e9tablie est de faire analyser vos noyaux d'abricot. L'Agence canadienne d'inspection des aliments (ACIA) recommande que les analyses soient effectu\u00e9es par un laboratoire accr\u00e9dit\u00e9. Au Canada, il existe deux organismes d'accr\u00e9ditation nationaux pour les laboratoires d'analyse (ISO 17025), soit le <a href=\"https://www.scc.ca/fr\">Conseil canadien des normes</a> et la <a href=\"https://cala.ca/\"><span lang=\"en\">Canadian Association for Laboratory Accreditation</span> (site Web en anglais seulement)</a>. On peut faire des recherches sur les sites Web de ces organismes pour trouver les laboratoires canadiens accr\u00e9dit\u00e9s pour l'analyse de cyanure total extractible. S'il n'y a, au Canada, aucun laboratoire dont la port\u00e9e de l'accr\u00e9ditation renferme une m\u00e9thode de d\u00e9tection de cyanure total extractible, l'ACIA pourrait prendre en consid\u00e9ration les r\u00e9sultats d'un laboratoire accr\u00e9dit\u00e9 d'un autre pays, \u00e0 condition que l'organisme d'accr\u00e9ditation soit signataire de l'<a href=\"https://ilac.org/ilac-mra-and-signatories/\">Arrangement de reconnaissance mutuelle de l'<span lang=\"en\">International Laboratory Accreditation Cooperation</span> (ILAC) (site Web en anglais seulement)</a>. ILAC donne acc\u00e8s \u00e0 un <a href=\"https://ilac.org/signatory-search/?q=s\">outil de recherche</a> permettant de trouver des laboratoires accr\u00e9dit\u00e9s.</p>\n\n<p>Les amandes d'abricots qui ne respectent pas la teneur maximale de \u2264\u00a020\u00a0ppm pour le cyanure extractible total ne peuvent pas \u00eatre vendues aux fins de consommation humaine ou utilis\u00e9es comme ingr\u00e9dient dans d'autres aliments apr\u00e8s le 25\u00a0janvier\u00a02020.</p>\n\n<p>Vous avez aussi la responsabilit\u00e9 de veiller \u00e0 ce que vos aliments soient conformes aux exigences en mati\u00e8re d'\u00e9tiquetage et de protection des consommateurs du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> et du <i>R\u00e8glement sur les aliments et drogues</i> (RAD). Ces exigences visent notamment l'utilisation de toute all\u00e9gation, implicite ou explicite, mentionnant qu'un aliment peut traiter ou pr\u00e9venir une maladie et un \u00e9tat de sant\u00e9, comme le cancer ou le diab\u00e8te. De telles all\u00e9gations doivent obligatoirement faire l'objet d'une \u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 au titre de la <i>Loi sur les aliments et drogues</i>. Sant\u00e9 Canada n'a pas examin\u00e9 de telles all\u00e9gations concernant les noyaux d'abricot vendus comme aliment.</p> \n\n<p>Aussi, la \u00ab\u00a0vitamine B17\u00a0\u00bb n'est pas une vitamine reconnue aux termes du RAD. Par cons\u00e9quent, tout aliment, y compris les noyaux d'abricot, dont l'\u00e9tiquette renferme une d\u00e9claration ou une all\u00e9gation concernant sa teneur en \u00ab\u00a0vitamine B17\u00a0\u00bb serait consid\u00e9r\u00e9 comme non conforme au RAD.</p> \n\n<p>Si vous avez d'autres questions, n'h\u00e9sitez pas \u00e0 communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau de l'ACIA</a> le plus pr\u00e8s pour obtenir de plus amples renseignements.</p>\n\n<h2>Information connexe</h2>\n\n<ul><li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/participation-public-partenariats/proposition-visant-mettre-jour-concentrations-maximales-cyanure-amandes-apricots.html\">Avis de proposition de Sant\u00e9 Canada visant \u00e0 ajouter le cyanure contenu dans les amandes d'abricots \u00e0 la Liste des contaminants et autres substances adult\u00e9rantes dans les aliments \u2013 Num\u00e9ro de r\u00e9f\u00e9rence\u00a0: NOP / ADP C-2018-1</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/fra/1383607266489/1383607344939\">L'\u00e9tiquetage des aliments pour l'industrie</a></li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}