{
    "dcr_id": "1531413778277",
    "title": {
        "en": "Overview of CFIA's role in cannabis regulation",
        "fr": "Aper\u00e7u du r\u00f4le de l'ACIA dans la r\u00e9glementation du cannabis"
    },
    "html_modified": "2024-01-26 2:24:15 PM",
    "modified": "2019-10-02",
    "issued": "2018-07-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plant_cannabis_overview_1531413778277_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plant_cannabis_overview_1531413778277_fra"
    },
    "ia_id": "1531413811413",
    "parent_ia_id": "1531251514243",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1531251514243",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Overview of CFIA's role in cannabis regulation",
        "fr": "Aper\u00e7u du r\u00f4le de l'ACIA dans la r\u00e9glementation du cannabis"
    },
    "label": {
        "en": "Overview of CFIA's role in cannabis regulation",
        "fr": "Aper\u00e7u du r\u00f4le de l'ACIA dans la r\u00e9glementation du cannabis"
    },
    "templatetype": "content page 1 column",
    "node_id": "1531413811413",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1531250234189",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/cannabis/overview/",
        "fr": "/protection-des-vegetaux/le-cannabis/apercu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Overview of CFIA's role in cannabis regulation",
            "fr": "Aper\u00e7u du r\u00f4le de l'ACIA dans la r\u00e9glementation du cannabis"
        },
        "description": {
            "en": "On October 17, 2018, the Cannabis Act came into force, which put in place a new, strict framework for controlling the production, distribution, sale and possession of cannabis in Canada.",
            "fr": "La Loi sur le cannabis est entr\u00e9e en vigueur le 17\u00a0octobre\u00a02018 pour encadrer rigoureusement la production, la distribution, la vente et la possession de cannabis au Canada."
        },
        "keywords": {
            "en": "cannabis, industrial hemp, by-products, The Cannabis Act, Bill C-45, overview",
            "fr": "cannabis, chanvre industriel, sous produits, Loi sur le cannabis, projet de loi C-45, aper\u00e7u"
        },
        "dcterms.subject": {
            "en": "inspection,legislation,regulation",
            "fr": "inspection,l\u00e9gislation,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-07-16",
            "fr": "2018-07-16"
        },
        "modified": {
            "en": "2019-10-02",
            "fr": "2019-10-02"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Overview of CFIA's role in cannabis regulation",
        "fr": "Aper\u00e7u du r\u00f4le de l'ACIA dans la r\u00e9glementation du cannabis"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>On October 17, 2018, the <i>Cannabis Act</i> came into force, which put in place a new, strict framework for controlling the production, distribution, sale and possession of cannabis in Canada. In keeping with the federal government's public health and public safety approach to the legalization and regulation of cannabis, effective October 17, 2019, the <i>Cannabis Regulations</i> will be amended to permit the legal sale of edible cannabis, cannabis extracts and cannabis topicals.</p>\n<h2>Cannabis plants, seeds and feed</h2>\n<p>The Canadian Food Inspection Agency (CFIA) administers a number of acts and regulations that relate broadly to the import, production and use of plants. These requirements also apply to cannabis, industrial hemp, cannabis products and by-products, such as regulations for livestock feed and the disposal of unused plant parts. The legalization of cannabis and the passing of the <i>Cannabis Act</i> does not affect these acts and regulations; they remain in effect.</p>\n<h2>Edibles</h2>\n<p>The production, distribution and sale of edible cannabis are controlled primarily at the federal level under the <i>Cannabis Act</i> and the <i>Cannabis Regulations</i>. The <i>Safe Food for Canadians Act</i> and its regulations do not apply to edible cannabis.</p>\n<p>For more information on the regulatory control of edible cannabis please see <a href=\"https://www.canada.ca/en/health-canada/services/drugs-medication/cannabis/laws-regulations/regulations-support-cannabis-act/notice-control-framework-edible.html\">Health  Canada's Notice</a>.</p>\n<p><a href=\"https://www.canada.ca/en/services/health/campaigns/cannabis.html\">Get the facts about cannabis in Canada</a> and learn more about the <i>Cannabis Act</i> and <i>Regulations</i>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La <i>Loi sur le cannabis</i> est entr\u00e9e en vigueur le 17\u00a0octobre\u00a02018 pour encadrer rigoureusement la production, la distribution, la vente et la possession de cannabis au Canada. Conform\u00e9ment \u00e0 l'approche adopt\u00e9e par le gouvernement f\u00e9d\u00e9ral en mati\u00e8re de sant\u00e9 publique et de s\u00e9curit\u00e9 publique concernant la l\u00e9galisation et la r\u00e8glementation du cannabis, \u00e0 compter du 17\u00a0octobre\u00a02019, le <i>R\u00e8glement sur le cannabis</i> sera modifi\u00e9 afin d'autoriser la vente l\u00e9gale de cannabis comestible, d'extraits de cannabis et de cannabis pour usage topique. </p>\n<h2>Plants et semences de cannabis et aliments pour animaux \u00e0 base de cannabis</h2>\n<p>L'Agence canadienne d'inspection des aliments (ACIA) applique d\u00e9j\u00e0 un certain nombre de lois et de r\u00e8glementations qui se concentrent largement sur les exigences d'importation, sur la production et sur l'utilisation des plantes. Ces politiques s'appliquent \u00e9galement au cannabis, au chanvre industriel ainsi que les divers produits et sous-produits de cannabis, notamment des r\u00e8glements sur l'alimentation du b\u00e9tail et l'\u00e9limination des parties non utilis\u00e9es des plants. La l\u00e9galisation du cannabis et l'adoption de la <i>Loi sur le cannabis</i> n'ont aucune incidence sur ces lois et r\u00e8glements, qui demeurent en vigueur.</p>\n<h2>Produits comestibles</h2>\n<p>La production, la distribution et la vente de cannabis comestible sont contr\u00f4l\u00e9es principalement \u00e0 l'\u00e9chelon f\u00e9d\u00e9ral en vertu de la <i>Loi sur le cannabis</i> et du <i>R\u00e8glement sur le cannabis</i>. La <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> et son <i>r\u00e8glement</i> ne s'appliquent pas au cannabis comestible.</p>\n<p>Pour obtenir plus d'information concernant le contr\u00f4le r\u00e8glementaire du cannabis comestible, veuillez consulter <a href=\"https://www.canada.ca/fr/sante-canada/services/drogues-medicaments/cannabis/lois-reglementation/reglements-appuyant-loi-cannabis/avis-cadre-controle-comestible.html\">l'avis de Sant\u00e9 Canada</a>.</p>\n<p><a href=\"https://www.canada.ca/fr/services/sante/campagnes/cannabis.html?utm_campaign=cannabis-18&amp;utm_medium=vurl-fr&amp;utm_source=canada-ca_lecannabis\">Renseignez-vous sur les faits du cannabis au Canada</a>\u00a0et apprenez-en plus sur la <i>Loi sur le cannabis</i> et son r\u00e8glement d'application.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}