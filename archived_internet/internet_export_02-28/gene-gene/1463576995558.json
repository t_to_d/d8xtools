{
    "dcr_id": "1463576995558",
    "title": {
        "en": "About the Canadian Drought Monitor",
        "fr": "\u00c0 propos de l'Outil de surveillance des s\u00e9cheresses au Canada"
    },
    "modified": "2019-03-27",
    "issued": "2016-06-17",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1463687205957",
    "dc_date_created": "2016-06-17",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/05/about_canadian_drought_monitor_1463576995558_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/05/about_canadian_drought_monitor_1463576995558_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "About the Canadian Drought Monitor",
        "fr": "\u00c0 propos de l'Outil de surveillance des s\u00e9cheresses au Canada"
    },
    "breadcrumb": {
        "en": "About the Canadian Drought Monitor",
        "fr": "\u00c0 propos de l'Outil de surveillance des s\u00e9cheresses au Canada"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2016-06-17",
            "fr": "2016-06-17"
        },
        "modified": {
            "en": "2019-03-27",
            "fr": "2019-03-27"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "About the Canadian Drought Monitor",
            "fr": "\u00c0 propos de l'Outil de surveillance des s\u00e9cheresses au Canada"
        },
        "subject": {
            "en": "manure;greenhouse gases;climate change",
            "fr": "pois chiches;produits avicoles;volailles-transformation"
        },
        "dcterms:subject": {
            "en": "scientific information;taxes;climate change",
            "fr": "engrais;ressources p\u00e9dagogiques;terre agricole"
        },
        "description": {
            "en": "DW-GS Drought Watch About the Canadian Drought Monitor. The ongoing development of the CDM involved regular communication with partners in the United States and Mexico including the National Oceanic and Atmospheric Administration\u2019s National Climatic Data Center (NCDC), the National Drought Mitigation Centre and the National Meteorological Service of Mexico (. Servicio Meteorol\u00f3gico Nacional. Drought classes in the CDM range from D0 to D4, with D1 to D4 indicating moderate to exceptional drought and D0 indicating abnormally dry conditions.",
            "fr": "DW-GS Guetter la s\u00e9cheresse \u00c0 propos de l\u2019Outil de surveillance des s\u00e9cheresses au Canada. Par ailleurs, pour assurer l\u2019\u00e9laboration continue de l\u2019OSSC, il faut communiquer r\u00e9guli\u00e8rement avec des partenaires aux \u00c9tats Unis et au Mexique, notamment le National Climatic Data Center (NCDC) de la National Oceanic and Atmospheric Administration, le National Drought Mitigation Centre et le Bureau m\u00e9t\u00e9orologique du Mexique (Servicio Meteorol\u00f3gico Nacional \u2013 SMN).Syst\u00e8me de classification. Une s\u00e9cheresse exceptionnelle (D4), par exemple, s\u2019entend de conditions qui se manifestent moins de deux fois en cent ans."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "blend of art;CDM and NADM;Servicio Meteorol\u00f3gico Nacional",
            "fr": "Climatic;Servicio Meteorol\u00f3gico"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "policy",
            "fr": "d\u00e9cision"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "About the Canadian Drought Monitor",
        "fr": "\u00c0 propos de l'Outil de surveillance des s\u00e9cheresses au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p>The Canadian Drought Monitor (CDM) uses a variety of federal, provincial, and regional data sources to establish a single drought rating based on a five category system. These ratings are shared through monthly maps that show the extent and intensity of drought across Canada. Tracking drought across the country is challenging, as there are varying definitions and indicators used to measure and define its extent and severity. The CDM overcomes these challenges by combining multiple indicators and impacts, and through consultations with federal, provincial, regional, and academic scientists.</p>\n\n<h2>Background</h2>\n\n<p>Since 2002 Agriculture and Agri-Food Canada has been the lead agency responsible for providing monthly assessments of drought for Canada that feed directly into the <a href=\"http://www.ncdc.noaa.gov/temp-and-precip/drought/nadm/overview\">North American Drought Monitor</a>, a cooperative effort between drought experts in Canada, Mexico and the United States. The North American Drought Monitor (NADM) is based on the methodology of the highly successful <a href=\"http://droughtmonitor.unl.edu/CurrentMap.aspx\">United States Drought Monitor</a>, and as such, has been developed to provide an ongoing comprehensive and integrated assessment of drought throughout all three countries in North America. </p>\n\n<p>A number of provincial and federal organizations are consulted to produce the CDM, in addition to ongoing communication with international partners. In the United States, partners include the <a href=\"http://www.noaa.gov/\">National Oceanic and Atmospheric Administration's</a> National Climatic Data Center (NCDC) and the <a href=\"http://drought.unl.edu/\">National Drought Mitigation Centre</a>. In Mexico, partners include the <a href=\"https://smn.conagua.gob.mx/es/\">National Meteorological Service of Mexico (in Spanish only)</a> (<i>Servicio Meteorol\u00f3gico Nacional</i> \u2013 SMN) which operates the <a href=\"https://smn.conagua.gob.mx/es/climatologia/monitor-de-sequia/monitor-de-sequia-en-mexico\">Mexico Drought Monitor (in Spanish only)</a>.</p>\n\n<h2>Data sources</h2>\n\n<p>Drought is a \"creeping phenomenon\" \u2013 difficult to define and measure, slow to develop, continuous, cumulative, and long lasting. There is no universally applicable tool for measuring drought; as impacts are non-structural, spread over large areas, and best described by multiple indices.</p>\n\n<p>To address the challenges of monitoring drought in a comprehensive way, the CDM is developed from an assortment of sources, such as: various precipitation and temperature indicators, Normalized Difference Vegetation Index satellite imagery, streamflow values, Palmer Drought Index, Standardized Precipitation Index; as well as drought indicators used by the agriculture, forestry, and water management sectors. Drought-prone regions are analyzed based on precipitation, temperature, drought model index maps, and climate data; and are interpreted by federal, provincial, and academic scientists. Once a consensus is reached, a monthly map showing drought designations for Canada is produced and used by Agriculture and Agri-Food Canada (AAFC) to assess the current drought risk to agriculture. </p>\n\n<h2>Classification scheme</h2>\n\n<p>The CDM uses a five category system to define the severity, spatial extent, and impacts of drought. Drought classes in the CDM range from D0 to D4, with D0 indicating abnormally dry conditions, and D1 to D4 indicating moderate to exceptional drought. Each category is based on the percentile chance of those conditions occurring. An exceptional drought (D4) for example, represents conditions that historically only appear less than two years in one hundred. Currently the spatial extent of the CDM maps encompasses these five categories across all of Canada except Nunavut and the Arctic Archipelago.</p>\n\n\r\n    ",
        "fr": "\r\n    <p>L'Outil de surveillance des s\u00e9cheresses au Canada (OSSC) utilise un grand nombre de sources de donn\u00e9es f\u00e9d\u00e9rales, provinciales et r\u00e9gionales pour \u00e9tablir une cote unique de s\u00e9cheresse fond\u00e9e sur un syst\u00e8me de classification \u00e0 cinq cat\u00e9gories. Ces cotes sont communiqu\u00e9es par des cartes mensuelles indiquant l'\u00e9tendue et l'intensit\u00e9 des s\u00e9cheresses au Canada. Il est difficile de faire le suivi des s\u00e9cheresses \u00e0 l'\u00e9chelle nationale, car les d\u00e9finitions et les indicateurs servant \u00e0 mesurer et \u00e0 \u00e9tablir l'\u00e9tendue et la gravit\u00e9 des s\u00e9cheresses varient beaucoup. L'OSSC rel\u00e8ve ce d\u00e9fi en combinant de nombreux indicateurs et r\u00e9percussions et par des consultations aupr\u00e8s des scientifiques du gouvernement f\u00e9d\u00e9ral, des provinces, des r\u00e9gions et des universit\u00e9s.</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n\n<p>Depuis 2002, Agriculture et Agroalimentaire Canada est le principal organisme responsable des \u00e9valuations mensuelles de la s\u00e9cheresse au Canada. Ces \u00e9valuations sont fournies directement au <a href=\"http://www.ncdc.noaa.gov/temp-and-precip/drought/nadm/overview\">Programme de surveillance de la s\u00e9cheresse en Am\u00e9rique du Nord (en anglais seulement)</a>, lequel est le fruit d\u2019une collaboration entre des sp\u00e9cialistes de la s\u00e9cheresse du Canada, du Mexique et des \u00c9tats\u2011Unis. Le programme de surveillance de la s\u00e9cheresse en Am\u00e9rique du Nord (NADM), qui est fond\u00e9 sur la m\u00e9thodologie de l'excellent <a href=\"http://droughtmonitor.unl.edu/CurrentMap.aspx\"><i lang=\"en\">United States Drought Monitor</i> (en anglais seulement)</a>, a \u00e9t\u00e9 cr\u00e9\u00e9 pour fournir une \u00e9valuation continue, globale et int\u00e9gr\u00e9e des s\u00e9cheresses dans les trois pays de l'Am\u00e9rique du Nord.</p>\n\n<p>Un certain nombre d'organisations provinciales et f\u00e9d\u00e9rales participent \u00e0 la production de l'OSSC, en plus de communiquer r\u00e9guli\u00e8rement avec les partenaires internationaux. Aux \u00c9tats-Unis, ces partenaires comprennent le <i lang=\"en\">National Climatic Data Center</i> (NCDC) de la <a href=\"http://www.noaa.gov/\"><i lang=\"en\">National Oceanic and Atmospheric Administration's</i> (en anglais seulement)</a> et le <a href=\"http://drought.unl.edu/\"><i lang=\"en\">National Drought Mitigation Centre</i> (en anglais seulement)</a>. Au Mexique, ces partenaires comprennent le <a href=\"http://smn.cna.gob.mx/es/\">Bureau m\u00e9t\u00e9orologique du Mexique (en espagnol seulement)</a> (<i>Servicio Meteorol\u00f3gico Nacional</i> \u2013 SMN) qui exploite <a href=\"https://smn.conagua.gob.mx/es/climatologia/monitor-de-sequia/monitor-de-sequia-en-mexico\">l'Outil de surveillance des s\u00e9cheresses du Mexique (en espagnol seulement)</a>.</p>\n\n<h2>Sources de donn\u00e9es</h2>\n\n<p>La s\u00e9cheresse est un \u00ab\u00a0ph\u00e9nom\u00e8ne progressif\u00a0\u00bb qui est difficile \u00e0 d\u00e9finir et \u00e0 mesurer, lent \u00e0 se d\u00e9velopper, continu, cumulatif et de longue dur\u00e9e. Il n'existe aucun outil universel pour mesurer les s\u00e9cheresses, car leurs r\u00e9percussions ne sont pas structurelles, elles s'\u00e9tendent sur de vastes secteurs et elles exigent plusieurs indicateurs pour bien les d\u00e9crire.</p>\n\n<p>Pour arriver \u00e0 effectuer une surveillance compl\u00e8te des s\u00e9cheresses, l'OSSC a \u00e9t\u00e9 \u00e9labor\u00e9 \u00e0 partir de nombreuses sources comme les divers indicateurs de temp\u00e9rature et de pr\u00e9cipitations, l'indice de v\u00e9g\u00e9tation par diff\u00e9rence normalis\u00e9e, les images satellites, les valeurs de d\u00e9bit d'eau, l'indice Palmer de gravit\u00e9 de la s\u00e9cheresse, l'indice normalis\u00e9 des pr\u00e9cipitations et les indices de s\u00e9cheresse utilis\u00e9s par les secteurs de l'agriculture, des for\u00eats et de la gestion de l'eau. Les r\u00e9gions susceptibles d'\u00eatre touch\u00e9es par la s\u00e9cheresse font l'objet d'analyses fond\u00e9es sur les pr\u00e9cipitations, la temp\u00e9rature, l'indice de mod\u00e9lisation des s\u00e9cheresses ainsi que les donn\u00e9es climatiques; les r\u00e9sultats sont interpr\u00e9t\u00e9s par des scientifiques du gouvernement f\u00e9d\u00e9ral, des provinces et des universit\u00e9s. Lorsque ces scientifiques parviennent \u00e0 un consensus, Agriculture et Agroalimentaire Canada (AAC) produit une carte mensuelle illustrant les d\u00e9signations de s\u00e9cheresse pour le Canada qu'il utilise pour \u00e9valuer le risque de s\u00e9cheresse pour l'agriculture.</p>\n\n<h2>Syst\u00e8me de classification</h2>\n\n<p>L'OSSC utilise un syst\u00e8me \u00e0 cinq cat\u00e9gories pour indiquer la gravit\u00e9, l'\u00e9tendue et les r\u00e9percussions des s\u00e9cheresses. Les cat\u00e9gories de s\u00e9cheresses vont de D0 \u00e0 D4; la cat\u00e9gorie D0 indique des conditions de s\u00e9cheresse anormale, et les cat\u00e9gories D1 \u00e0 D4 indiquent une s\u00e9cheresse allant de mod\u00e9r\u00e9e \u00e0 exceptionnelle. Chaque cat\u00e9gorie est associ\u00e9e au risque (en percentile) que de telles conditions se produisent. Une s\u00e9cheresse exceptionnelle (D4), par exemple, s'entend de conditions qui se manifestent \u00e0 moins de deux reprises en cent ans. Ces cinq cat\u00e9gories sont repr\u00e9sent\u00e9es sur les cartes de l'OSSC partout au Canada, sauf au Nunavut et dans l'archipel Arctique.</p>\n\r\n    "
    }
}