{
    "dcr_id": "1383794340026",
    "title": {
        "en": "Wood packaging material requirements to the United States",
        "fr": "Exigences relatives aux mat\u00e9riaux d'emballage en bois pour l'exportation aux \u00c9tats-Unis"
    },
    "html_modified": "2024-02-13 9:49:27 AM",
    "modified": "2023-11-21",
    "issued": "2013-11-06 22:19:01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/for_exp_wood_packaging_entry_1383794340026_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/for_exp_wood_packaging_entry_1383794340026_fra"
    },
    "ia_id": "1383794447179",
    "parent_ia_id": "1300380612246",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1300380612246",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Wood packaging material requirements to the United States",
        "fr": "Exigences relatives aux mat\u00e9riaux d'emballage en bois pour l'exportation aux \u00c9tats-Unis"
    },
    "label": {
        "en": "Wood packaging material requirements to the United States",
        "fr": "Exigences relatives aux mat\u00e9riaux d'emballage en bois pour l'exportation aux \u00c9tats-Unis"
    },
    "templatetype": "content page 1 column",
    "node_id": "1383794447179",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300380523318",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/forestry/exports/wood-packaging-u-s-/",
        "fr": "/protection-des-vegetaux/forets/exportation/emballage-en-bois-e-u-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Wood packaging material requirements to the United States",
            "fr": "Exigences relatives aux mat\u00e9riaux d'emballage en bois pour l'exportation aux \u00c9tats-Unis"
        },
        "description": {
            "en": "The purpose of this document is to notify all affected Canadian exporters of the existing plant quarantine import requirements.",
            "fr": "Ce document a pour but d?aviser tous les exportateurs canadiens affect\u00e9s des exigences actuelles relatives \u00e0 l?importation des v\u00e9g\u00e9taux en quarantaine."
        },
        "keywords": {
            "en": "import, wood packaging, United States",
            "fr": "importation, mat\u00e9riaux d?emballage en bois, \u00c9tats-Unis"
        },
        "dcterms.subject": {
            "en": "imports",
            "fr": "importation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-11-06 22:19:01",
            "fr": "2013-11-06 22:19:01"
        },
        "modified": {
            "en": "2023-11-21",
            "fr": "2023-11-21"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Wood packaging material requirements to the United States",
        "fr": "Exigences relatives aux mat\u00e9riaux d'emballage en bois pour l'exportation aux \u00c9tats-Unis"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Understand the import requirements for wood packaging, including dunnage, for all species to the United States (U.S.).</p>\n\t\n<h2>Requirements for all wood species </h2>\n\t\n<p>Wood packaging exported to the United States from Canada may be inspected by U.S. Customs and Border Protection to verify that it either complies with the requirements of International Standard for Phytosanitary Measures (ISPM) 15 or is eligible under the Canada-U.S. exemption from ISPM\u00a015.</p>\n\n<p>All shipments of goods with wood packaging to be exported to the U.S., as well as wood packaging exported as a commodity to the U.S., must be free of live pests, weed seeds and soil, and:</p>\n<ul>\n<li>identified with legible ISPM\u00a015 marks (re-used repaired wood packaging should be marked in accordance with the\u00a0<a href=\"/plant-health/invasive-species/directives/forest-products/d-13-01/eng/1438703782830/1438711494768\">D-13-01: Canadian Heat treated Wood Products Certification Program\u00a0(HT program)</a> or ISPM 15)\n\n<p><strong>or</strong></p> </li>\n\n<li>eligible under the Canada-U.S. exemption from ISPM\u00a015.</li>\n</ul>\n\t\n<h2>Canada-U.S. exemption from ISPM\u00a015</h2>\n\t\n<p>In 2005, an exemption was established for the movement of wood packaging between the U.S. and Canada.</p>\n<p>In order to be exempt, the wood packaging must be accompanied by export/shipping documents clearly stating that any unmarked wood packaging in the shipment was:</p>\n<ul>\n<li>produced in either the U.S. and/or Canada (as the case may be), and</li>\n<li>manufactured with wood originating from either Canada or the U.S.</li>\n</ul>\n<p>This exemption applies only to the \"Continental United States\". Wood packaging imported from or exported to Hawaii and U.S. territories is not included in the exemption and thus need to bear the ISPM 15 mark.</p>\n<div class=\"well well-sm\">\n<p>The obliteration of any markings on the wood may be considered as removing marks which identify the wood as having an origin other than Canada and the U.S. and may result in U.S. authorities considering the wood as non-compliant.</p>\n</div>\n\t\n<h2>Wood packaging of mixed origins</h2>\n<p>In the case where shipments may consist of units of wood packaging of differing origins (for example, mixed loads), the exporter must ensure that all the wood packaging clearly meets the standards above (for example, within the mixed shipment, the unmarked wood packaging is identified as being produced in Canada or the U.S. on shipping documentation and ISPM\u00a015 marks appear on any re-used offshore wood packaging).</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Comprenez les exigences d'importation des \u00c9tats-Unis (\u00c9.-U.) pour le bois d'emballage, y compris le bois de calage, pour toutes les esp\u00e8ces.</p>\n\t\n<h2>Exigences pour toutes esp\u00e8ces de bois</h2>\n\t\n<p>Les mat\u00e9riaux d'emballage en bois export\u00e9s du Canada vers les \u00c9tats-Unis peuvent \u00eatre inspect\u00e9s par l'<span lang=\"en\">United States Customs and Border Protection</span> afin de v\u00e9rifier qu'ils sont conformes aux exigences de les Normes internationales pour les mesures phytosanitaires (NIMP)\u00a015 ou qu'ils sont \u00e9ligibles \u00e0 une exemption de la NIMP\u00a015 entre le Canada et les \u00c9tats-Unis.</p>\n<p>Tous les envois de marchandises avec des mat\u00e9riaux d'emballage en bois \u00e0 exporter aux \u00c9tats-Unis, de m\u00eame que les mat\u00e9riaux d'emballage en bois export\u00e9s en tant que marchandise, doivent \u00eatre libres d'organismes nuisibles aux v\u00e9g\u00e9taux, de graines de mauvaises herbes, ou de semences et de sol, et\u00a0:</p>\n<ul>\n<li>\u00eatre identifi\u00e9s avec des marques de certification lisibles selon la NIMP\u00a015 (les mat\u00e9riaux d'emballage en bois r\u00e9utilis\u00e9s ou r\u00e9par\u00e9s doivent \u00eatre marqu\u00e9s selon\u00a0<a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-13-01/fra/1438703782830/1438711494768\">D-13-01\u00a0: Programme canadien de certification des produits de bois et trait\u00e9s \u00e0 la chaleur (Programme HT)</a> ou la NIMP 15)\n\n<p><strong>ou</strong></p></li>\n\n<li>\u00eatre admissible \u00e0 l'exemption canado-am\u00e9ricaine de la NIMP\u00a015.</li>\n</ul>\n\t\n<h2>Exemption canado-am\u00e9ricaine de la NIMP\u00a015</h2>\n\t\n<p>En 2005, une exemption a \u00e9t\u00e9 \u00e9tablie pour le mouvement des emballages en bois entre les \u00c9tats-Unis et le Canada.</p>\n<p>Pour \u00eatre exempt\u00e9, l'emballage en bois doit\u00a0\u00eatre accompagn\u00e9 de documents d'exp\u00e9dition/d'exportation attestant clairement que tous les mat\u00e9riaux d'emballage sans marque d'identification dans l'envoi ont \u00e9t\u00e9\u00a0:</p>\n<ul>\n<li>produits aux \u00c9tats-Unis ou au Canada (selon le cas), et</li>\n<li>produits \u00e0 partir de bois qui provient soit du Canada ou des \u00c9tats-Unis</li>\n</ul>\n<p>Cette exemption s'applique uniquement \u00ab\u00a0\u00e0 la partie continentale des \u00c9tats-Unis\u00a0\u00bb. Les emballages en bois import\u00e9s ou export\u00e9s vers Hawa\u00ef et les territoires am\u00e9ricains ne sont pas inclus dans l'exemption et doivent donc porter la marque de la NIMP 15.</p>\n<p>L'oblit\u00e9ration de toute marque d'identification sur le bois peut \u00eatre consid\u00e9r\u00e9e comme \u00e9tant un retrait de marques qui identifient le bois comme provenant d'un autre pays et non du Canada ou des \u00c9tats-Unis. Comme r\u00e9sultat, les autorit\u00e9s am\u00e9ricaines pourraient consid\u00e9rer le bois comme \u00e9tant non-conforme.</p>\n\t\n<h2>Mat\u00e9riaux d'emballage d'origines mixtes</h2>\n\t\n<p>Dans le cas o\u00f9 les envois peuvent \u00eatre compos\u00e9s d'unit\u00e9s de mat\u00e9riaux d'emballage en bois provenant de diff\u00e9rentes origines (par exemple, cargaisons mixtes), l'exportateur doit s'assurer que tous les mat\u00e9riaux d'emballage en bois rencontrent clairement les normes indiqu\u00e9es ci-haut (par exemple, dans l'envoi mixte, les mat\u00e9riaux d'emballage en bois non marqu\u00e9s sont identifi\u00e9s comme \u00e9tant produits au Canada ou aux \u00c9tats-Unis sur les documents d'exp\u00e9dition et des marques de certification selon la NIMP\u00a015 apparaissent sur tous les mat\u00e9riaux d'emballage en bois r\u00e9utilis\u00e9s hauturiers).</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}