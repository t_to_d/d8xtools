{
    "dcr_id": "1551969281601",
    "title": {
        "en": "Bugging you to do your part",
        "fr": "Votre r\u00f4le est important en b\u00e9bite!"
    },
    "html_modified": "8-3-2019",
    "modified": "7-3-2019",
    "issued": "7-3-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/bugging_you_to_do_your_part_1551969281601_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/bugging_you_to_do_your_part_1551969281601_fra"
    },
    "parent_ia_id": "1495210111055",
    "ia_id": "1551969281881",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Bugging you to do your part",
        "fr": "Votre r\u00f4le est important en b\u00e9bite!"
    },
    "label": {
        "en": "Bugging you to do your part",
        "fr": "Votre r\u00f4le est important en b\u00e9bite!"
    },
    "templatetype": "content page 1 column",
    "node_id": "1551969281881",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1495210110540",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-research-and-publications/bugging-you-to-do-your-part/",
        "fr": "/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/votre-role-est-important-en-bebite-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Bugging you to do your part",
            "fr": "Votre r\u00f4le est important en b\u00e9bite!"
        },
        "description": {
            "en": "There are approximately 1.5 million species of organisms on Earth at this very moment. Yes, this is an extremely large number.",
            "fr": "Il y a environ 1,5\u00a0million d'esp\u00e8ces d'organismes sur Terre en ce moment m\u00eame. Oui, c'est un nombre extr\u00eamement \u00e9lev\u00e9."
        },
        "keywords": {
            "en": "science, food, animal, plant, laboratories, veterinarians, scientists, research, Bugging you to do your part, invasive insect or plant, Asian longhorned beetle, Dr. Mireille Marcotte",
            "fr": "science, aliments, animaux, v\u00e9g\u00e9taux, laboratoires, v\u00e9t\u00e9rinaires, scientistes, recherche, Votre r\u00f4le est important en b\u00e9bite!, longicorne asiatique, une plante ou un insecte envahissant, bois de chauffage local, Dre Mireille Marcotte"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Science",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Science"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-03-08",
            "fr": "2019-03-08"
        },
        "modified": {
            "en": "2019-03-07",
            "fr": "2019-03-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Bugging you to do your part",
        "fr": "Votre r\u00f4le est important en b\u00e9bite!"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>There are approximately 1.5 million species of organisms on Earth at this very moment. Yes, this is an extremely large number. So large, you are likely thinking, where could all these species possibly be hiding? Well, it happens that most of them aren't actually hiding. The surprising truth is that two thirds of all species can hardly be seen by the human eye.</p>\n<p>Growing up, Dr. <span lang=\"fr\">Mireille Marcotte</span> was always fascinated by this fact, and it was one of the reasons why she was inspired to study entomology, the study of insects. Having followed her passion, <span lang=\"fr\">Mireille</span> has worked hard to become the national manager for the Plant Health Surveillance Unit at the Canadian Food Inspection Agency (CFIA). </p>\n<p>One of the important responsibilities Mireille's unit holds is the surveillance of invasive species in Canada. There are many invasive species to stay aware of, but according to Mireille, \"the biggest threat to Canadian forests today is the <a href=\"/plant-health/invasive-species/insects/asian-longhorned-beetle/eng/1337792721926/1337792820836\">Asian longhorned beetle</a>.\"</p>\n<p>Native to Asia, this nickel-sized insect has the firepower to cause some serious damage to maple trees and other hardwood species in Canada. In other words, the Asian longhorned beetle could be the reason your grandchildren no longer have maple syrup for their pancakes.</p>\n<blockquote>\n<p>\"If that pest was to get established in Canada, the consequences would be enormous for our economy and our environment.\"</p>\n</blockquote>\n<p>This was almost the case in 2003 when these beetles were found in an area of Toronto. Now you might think getting rid of a few beetles would be a simple task, but it took a whole 10 years before the last beetles were cleared out. One could say this infestation really bugged the CFIA, and was a real pest to deal with.</p>\n<p>You don't need to be an entomologist to take action to prevent the spread of invasive species. The most important step you can take right now is using only <a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">local firewood.</a></p>\n<p>Firewood? Yes, firewood. \"The movement of untreated firewood to or from the campground or cottage can spread invasive species and diseases that are hidden under the bark and that we do not see\", says Mireille.</p>\n<p>Whether camping or heating your home or cottage, know what invasive species are in your area, and don't move firewood. When buying firewood, ask where it's from and whether it has been heat treated to kill invasive species.</p>\n<p>Check out our <a href=\"/inspect-and-protect/food-safety/dr-emilie-larocque/eng/1572621040261/1572621040573\">podcast</a>! Learn more about <a href=\"https://profils-profiles.science.gc.ca/en/profile/mireille-marcotte-phd\">Mireille</a>, her passion for science, and how her work at the CFIA is helping protect Canadian forests, crops and gardens.</p>\n\n<p>If you think you've found an <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">invasive insect or plant</a> where you don't expect to see it, contact the <a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">Canadian Food Inspection Agency</a>.\n</p>\n\n\n<h2>Want to know more?</h2>\n<ul>\n<li><a href=\"/plant-health/eng/1299162629094/1299162708850\">Learn about CFIA plant health programs</a></li>\n<li>Listen to our science <a href=\"/inspect-and-protect/food-safety/dr-emilie-larocque/eng/1572621040261/1572621040573\">podcasts</a></li>\n<li><strong>Find out what is happening in</strong> <a href=\"/science-and-research/eng/1494875229872/1494875261582\">science at the CFIA</a>.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Il y a environ 1,5\u00a0million d'esp\u00e8ces d'organismes sur Terre en ce moment m\u00eame. Oui, c'est un nombre extr\u00eamement \u00e9lev\u00e9. Si grand que vous vous demandez probablement, o\u00f9 se cachent toutes ces esp\u00e8ces? Eh bien, il arrive que la plupart d'entre eux ne se cachent pas vraiment. La v\u00e9rit\u00e9 est en fait que les deux tiers de toutes les esp\u00e8ces peuvent difficilement \u00eatre vus par l'\u0153il humain.</p>\n<p>D<sup>re</sup> Mireille Marcotte a toujours \u00e9t\u00e9 fascin\u00e9e par ce fait, et c'est l'une des raisons qui l'ont incit\u00e9e \u00e0 \u00e9tudier l'entomologie, soit l'\u00e9tude des insectes. Ayant suivi sa passion, Mireille a travaill\u00e9 fort pour devenir gestionnaire nationale de l'Unit\u00e9 de surveillance phytosanitaire \u00e0 l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<p>L'une des grandes responsabilit\u00e9s de l'unit\u00e9 de Mireille est la surveillance des esp\u00e8ces envahissantes au Canada. Il y a beaucoup d'esp\u00e8ces envahissantes \u00e0 surveiller, mais selon Mireille, \u00ab\u00a0la plus grande menace pour les for\u00eats canadiennes aujourd'hui est le <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-asiatique/fra/1337792721926/1337792820836\">longicorne asiatique</a>.\u00a0\u00bb</p>\n<p>Originaire d'Asie, cet insecte de la taille d'une pi\u00e8ce de cinq cents a la capacit\u00e9 de causer de graves dommages aux \u00e9rables et \u00e0 d'autres esp\u00e8ces de feuillus au Canada. Autrement dit, le longicorne asiatique pourrait \u00eatre la raison pour laquelle vos petits-enfants ne pourraient plus mettre de sirop d'\u00e9rable sur leurs cr\u00eapes. </p>\n<blockquote>\n<p>\u00ab\u00a0Si ce ravageur devait s'\u00e9tablir au Canada, les cons\u00e9quences seraient \u00e9normes pour notre \u00e9conomie et notre environnement.\u00a0\u00bb</p>\n</blockquote>\n<p>Ce fut presque le cas en 2003 lorsque ces col\u00e9opt\u00e8res ont \u00e9t\u00e9 trouv\u00e9s dans une r\u00e9gion de Toronto. Vous pourriez penser que se d\u00e9barrasser de quelques col\u00e9opt\u00e8res serait une t\u00e2che simple, mais il a fallu 10\u00a0ans avant que les derniers col\u00e9opt\u00e8res ne soient \u00e9limin\u00e9s. On pourrait dire que cette infestation a vraiment emb\u00eat\u00e9 l'ACIA et a fait des ravages.</p>\n<p>Il n'est pas n\u00e9cessaire d'\u00eatre entomologiste pour prendre des mesures visant \u00e0 pr\u00e9venir la propagation des esp\u00e8ces envahissantes. La mesure la plus importante que vous pouvez prendre actuellement est d'utiliser uniquement du <a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">bois de chauffage local</a>.</p>\n<p>Du bois de chauffage? Oui, du bois de chauffage. \u00ab\u00a0Le d\u00e9placement de bois de chauffage non trait\u00e9 en provenance ou \u00e0 destination du terrain de camping ou du chalet peut propager des esp\u00e8ces envahissantes et des maladies qui sont cach\u00e9es sous l'\u00e9corce et que nous ne voyons pas\u00a0\u00bb, explique Mireille.</p>\n<p>Que ce soit pour camper ou pour chauffer votre maison ou votre chalet, sachez quelles esp\u00e8ces envahissantes se trouvent dans votre r\u00e9gion et ne d\u00e9placez pas de bois de chauffage. Lorsque vous achetez du bois de chauffage, demandez d'o\u00f9 il vient et s'il a \u00e9t\u00e9 trait\u00e9 \u00e0 la chaleur pour tuer les esp\u00e8ces envahissantes.</p>\n<p>\u00c9coutez notre <a href=\"/inspecter-et-proteger/salubrite-des-aliments/dre-emilie-larocque/fra/1572621040261/1572621040573\">balado</a> pour en apprendre davantage sur <a href=\"https://profils-profiles.science.gc.ca/fr/profil/mireille-marcotte-phd\">Mireille</a>, sa passion pour les sciences et la fa\u00e7on dont son travail \u00e0 l'ACIA aide \u00e0 prot\u00e9ger les for\u00eats, les cultures et les jardins du Canada.</p>\n<p>Si vous pensez avoir trouv\u00e9 <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">une plante ou un insecte envahissant</a> l\u00e0 o\u00f9 vous ne vous en attendiez pas, communiquez avec l'<a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">Agence canadienne d'inspection des aliments</a>. </p>\n<h2>Souhaitez-vous en savoir plus?</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">D\u00e9couvrez plus sur les programmes de la sant\u00e9 des v\u00e9g\u00e9taux de l'ACIA</a></li>\n<li>\u00c9coutez nos <a href=\"/inspecter-et-proteger/salubrite-des-aliments/dre-emilie-larocque/fra/1572621040261/1572621040573\">balados</a> scientifique</li>\n<li><strong>D\u00e9couvrez ce qui se passe en</strong> <a href=\"/les-sciences-et-les-recherches/fra/1494875229872/1494875261582\">les sciences \u00e0 l'ACIA</a></li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}