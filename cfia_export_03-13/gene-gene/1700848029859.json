{
    "dcr_id": "1700848029859",
    "title": {
        "en": "Questions and answers: Risk management documents",
        "fr": "Questions et r\u00e9ponses\u00a0: Documents de gestion du risque"
    },
    "html_modified": "2024-03-12 3:59:36 PM",
    "modified": "2023-11-28",
    "issued": "2023-11-28",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pw_qa_rsk_mngmnt_docs_1700848029859_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pw_qa_rsk_mngmnt_docs_1700848029859_fra"
    },
    "ia_id": "1700848030453",
    "parent_ia_id": "1698950840743",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1698950840743",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and answers: Risk management documents",
        "fr": "Questions et r\u00e9ponses\u00a0: Documents de gestion du risque"
    },
    "label": {
        "en": "Questions and answers: Risk management documents",
        "fr": "Questions et r\u00e9ponses\u00a0: Documents de gestion du risque"
    },
    "templatetype": "content page 1 column",
    "node_id": "1700848030453",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1698950840056",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/consultations-and-engagement/national-potato-wart-response-plan/risk-management-documents/",
        "fr": "/a-propos-de-l-acia/transparence/consultations-et-participation/plan-d-intervention-contre-la-galle-verruqueuse/documents-de-gestion-du-risque/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and answers: Risk management documents",
            "fr": "Questions et r\u00e9ponses\u00a0: Documents de gestion du risque"
        },
        "description": {
            "en": "Pest risk management is a science-based process of identifying and evaluating measures that could help lower the risk of a particular pest, such as potato wart, to an acceptable level.",
            "fr": "La gestion du risque phytosanitaire est un processus scientifique d'identification et d'\u00e9valuation des mesures susceptibles de r\u00e9duire le risque li\u00e9 \u00e0 un organisme nuisible particulier, comme la galle verruqueuse de la pomme de terre, \u00e0 un niveau acceptable."
        },
        "keywords": {
            "en": "potato wart, risk, risk management, pest",
            "fr": "galle verruqueuse, risque, gestion du risque, peste"
        },
        "dcterms.subject": {
            "en": "plant diseases,pests,potatoes,plants",
            "fr": "maladie des plantes,organisme nuisible,patates,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-11-28",
            "fr": "2023-11-28"
        },
        "modified": {
            "en": "2023-11-28",
            "fr": "2023-11-28"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and answers: Risk management documents",
        "fr": "Questions et r\u00e9ponses\u00a0: Documents de gestion du risque"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h3\">What is pest risk?</h2>\n\n<p>According to the International Plant Protection Convention (IPPC), pest risk means how likely it is for a pest, such as potato wart, to be introduced and spread, and how much it could cost our economy/how bad it could be for the economy.</p>\n\n<h2 class=\"h3\">How is the risk assessed?</h2>\n\n<p>The Canadian Food Inspection Agency (CFIA) is responsible for the management of <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">quarantine plant pests</a>, such as potato wart. The Agency evaluates these pests using guidelines, such as pest risk analysis, created by the IPPC to determine if they could cause harm. This analysis, based on scientific and economic information, helps the Agency decide if the organism is a pest and if it should be regulated.</p>\n\n<h2 class=\"h3\">What is pest risk management?</h2>\n\n<p>Pest risk management is a science-based process of identifying and evaluating measures that could help lower the risk of a particular pest, such as potato wart, to an acceptable level. In the case of potato wart, it is already regulated as a quarantine pest in Canada and many other countries. After the pest risk analysis process, the CFIA evaluates tools and options for managing pests.</p>\n\n<h2 class=\"h3\">What is a pest risk management document?</h2>\n\n<p>A pest risk management document outlines the different pest risk management options, including their advantages and disadvantages. It gives an overview of the results of a pest risk assessment and documents how the pest risk can be managed for the specific problem. The CFIA provides recommendations, which serves as a place for feedback which are considered when the document is finalized.</p>\n\n<h2 class=\"h3\">When creating a pest risk management document, what factors are taken into consideration?</h2>\n\n<p>Several factors are taken into consideration in developing a pest risk management document (RMD). The pest risk analysis helps figure out the total risk and how it affects the economy. If the risk is too high, the first step in risk management is to find possible actions to lower the risk to an acceptable level.</p>\n\n<p>The following are considered when creating an RMD:</p>\n\n<ul>\n<li>How well the proposed measures can reduce the pest risk </li>\n<li>The characteristics of the pest and the risk it brings</li>\n<li>The economic consequences of the measures</li>\n<li>Whether the measures are cost-effective and feasible to put in place</li>\n<li>The impact on trade</li>\n<li>Whether other measures are already in place</li>\n</ul>\n\n<h2 class=\"h3\">Why are the pest risk management documents being shared for public feedback?</h2>\n\n<p>An important part of creating and completing a pest risk management document is talking with the people involved/who have a stake in it. This consultation lets the CFIA share information with stakeholders and vice versa. The CFIA outlines the risks and potential measures to help contain and control a pest, while providing the public with an opportunity to give their input.</p>\n\n<p>The thoughts and concerns from stakeholders gathered during this process are carefully looked at when deciding how to manage the risk and finalizing the plan for dealing with pest risks.</p>\n\n<h2 class=\"h3\">How are risk management options selected and how does the CFIA decide the final approach?</h2>\n\n<p>When developing a pest risk management document, various ways to manage the risk are identified and evaluated to see if they can help reduce the risk to an acceptable level and if they are practical and affordable. The RMD looks at all of the pros and cons of each potential measure. The CFIA chooses the phytosanitary measure that not only lowers the pest risk to an acceptable level but is also practical, cost-effective, and causes the least harm to trade and the industry.</p>\n\n<h2 class=\"h3\">How is a pest risk management document implemented? What does it lead to?</h2>\n\n<p>For the case of potato wart, there are 3 RMDs related to potato wart that are open for public comments for 60 days. The CFIA will review all the comments received and then complete and publish the pest risk management documents. The final RMD will be used to help finalize a new national potato wart response plan, which will replace the 2009 Potato Wart Domestic Long-term Management Plan.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h3\">Qu'est-ce qu'un risque phytosanitaire?</h2>\n\n<p>Selon la Convention internationale pour la protection des v\u00e9g\u00e9taux (CIPV), le risque phytosanitaire correspond \u00e0 la probabilit\u00e9 d'introduction et de propagation d'un organisme nuisible, comme la galle verruqueuse de la pomme de terre, ainsi qu'au co\u00fbt ou \u00e0 l'impact \u00e9conomique qu'il pourrait avoir.</p>\n\n<h2 class=\"h3\">Comment le risque est-il \u00e9valu\u00e9?</h2>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est responsable de la gestion des <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">organismes nuisibles de quarantaine</a>, comme la galle verruqueuse de la pomme de terre. L'Agence \u00e9value ces organismes nuisibles \u00e0 l'aide de lignes directrices, comme l'analyse du risque phytosanitaire, qui sont \u00e9labor\u00e9es par la CIPV, afin de d\u00e9terminer s'ils sont susceptibles de causer des dommages. Cette analyse, bas\u00e9e sur des informations scientifiques et \u00e9conomiques, aide l'ACIA \u00e0 d\u00e9cider si l'organisme est un organisme nuisible et s'il doit \u00eatre r\u00e9glement\u00e9.</p>\n\n<h2 class=\"h3\">Qu'est-ce que la gestion du risque phytosanitaire?</h2>\n\n<p>La gestion du risque phytosanitaire est un processus scientifique d'identification et d'\u00e9valuation des mesures susceptibles de r\u00e9duire le risque li\u00e9 \u00e0 un organisme nuisible particulier, comme la galle verruqueuse de la pomme de terre, \u00e0 un niveau acceptable. La galle verruqueuse de la pomme de terre est d\u00e9j\u00e0 r\u00e9glement\u00e9e \u00e0 titre d'organisme de quarantaine au Canada et dans de nombreux autres pays. Apr\u00e8s le processus d'analyse du risque phytosanitaire, l'ACIA \u00e9value les outils et les options de gestion des organismes nuisibles.</p>\n\n<h2 class=\"h3\">Qu'est-ce qu'un document de gestion du risque phytosanitaire? </h2>\n\n<p>Un document sur la gestion du risque phytosanitaire d\u00e9crit les diff\u00e9rentes options de gestion du risque phytosanitaire, y compris leurs avantages et leurs inconv\u00e9nients. Il fournit une vue d'ensemble des r\u00e9sultats de l'\u00e9valuation du risque phytosanitaire et indique comment le risque phytosanitaire peut \u00eatre g\u00e9r\u00e9 pour le probl\u00e8me pr\u00e9cis. L'ACIA formule des recommandations, ce qui permet de recueillir des commentaires qui sont pris en compte lors de la finalisation du document. </p>\n\n<h2 class=\"h3\">Quels sont les facteurs \u00e0 prendre en consid\u00e9ration lors de l'\u00e9laboration d'un document de gestion du risque phytosanitaire?</h2>\n\n<p>Plusieurs facteurs sont pris en consid\u00e9ration lors de l'\u00e9laboration d'un document de gestion du risque phytosanitaire. L'analyse du risque phytosanitaire aide \u00e0 d\u00e9terminer le risque global et ses incidences \u00e9conomiques potentielles. Si le risque est trop \u00e9lev\u00e9, la premi\u00e8re \u00e9tape de la gestion du risque phytosanitaire consiste \u00e0 trouver des mesures possibles pour ramener le risque \u00e0 un niveau acceptable.</p>\n\n<p>Les \u00e9l\u00e9ments suivants sont pris en compte lors de l'\u00e9laboration d'un document de gestion du risque\u00a0:</p>\n\n<ul>\n<li>L'efficacit\u00e9 \u00e9ventuelle des mesures propos\u00e9es \u00e0 r\u00e9duire le risque phytosanitaire; </li>\n<li>Les caract\u00e9ristiques de l'organisme nuisible et le risque associ\u00e9;</li>\n<li>Les cons\u00e9quences \u00e9conomiques des mesures;</li>\n<li>L'aspect \u00e9conomique et r\u00e9aliste des mesures \u00e0 mettre en place;</li>\n<li>Les incidences sur le commerce;</li>\n<li>L'existence d'autres mesures d\u00e9j\u00e0 en place.</li>\n</ul>\n\n<h2 class=\"h3\">Pourquoi les documents de gestion du risque phytosanitaire sont-ils soumis aux commentaires du public? </h2>\n\n<p>Une partie importante de la cr\u00e9ation et de l'ach\u00e8vement d'un document de gestion du risque phytosanitaire consiste \u00e0 discuter avec les personnes concern\u00e9es ou qui ont un int\u00e9r\u00eat dans le projet. Cette consultation permet \u00e0 l'ACIA de partager des informations avec les parties int\u00e9ress\u00e9es et vice versa. L'ACIA d\u00e9crit les risques et les mesures potentiels pour aider \u00e0 contenir et \u00e0 contr\u00f4ler un organisme nuisible, tout en donnant au public la possibilit\u00e9 de donner son avis.</p>\n\n<p>Les r\u00e9flexions et les pr\u00e9occupations des parties int\u00e9ress\u00e9es recueillies au cours de ce processus sont soigneusement examin\u00e9es lorsqu'il s'agit de d\u00e9cider de la mani\u00e8re de g\u00e9rer le risque et de finaliser le plan de gestion du risque li\u00e9 \u00e0 l'organisme nuisible.</p>\n\n<h2 class=\"h3\">Comment les options de gestion du risque sont-elles s\u00e9lectionn\u00e9es et comment l'ACIA d\u00e9cide-t-elle de l'approche finale? </h2>\n\n<p>Lors de l'\u00e9laboration d'un document de gestion du risque phytosanitaire, plusieurs mesures de gestion du risque sont recens\u00e9es et \u00e9valu\u00e9es afin de d\u00e9terminer si elles peuvent aider \u00e0 r\u00e9duire le risque \u00e0 un niveau acceptable et si ces mesures sont pratiques et abordables. Le document de gestion du risque permet d'\u00e9valuer tous les avantages et les inconv\u00e9nients de chaque mesure \u00e9ventuelle. L'ACIA choisit la mesure phytosanitaire qui, en plus de ramener le risque phytosanitaire \u00e0 un niveau acceptable, est \u00e9galement pratique et \u00e9conomique et cause le moins de pr\u00e9judices au commerce et \u00e0 l'industrie.</p>\n\n<h2 class=\"h3\">Comment un document de gestion du risque phytosanitaire est-il mis en \u0153uvre? \u00c0 quoi cela m\u00e8ne-t-il?</h2>\n\n<p>Dans le cas de la galle verruqueuse, il existe trois documents de gestion du risque de la galle verruqueuse qui sont ouverts aux commentaires du public pendant 60 jours. L'ACIA \u00e9tudiera tous les commentaires re\u00e7us, puis compl\u00e9tera et publiera les documents de gestion du risque phytosanitaire. Le document de gestion du risque d\u00e9finitif sera utilis\u00e9 pour finaliser un nouveau plan national de gestion de la galle verruqueuse, lequel remplacera le plan national de gestion \u00e0 long terme de la galle verruqueuse de 2009.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}