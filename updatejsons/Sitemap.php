<?php
//require '/var/www/clients/client1/web38/web/d8tools/vendor/autoload.php';
//require '/home/mo/d8xtools/vendor/autoload.php';
require '../../vendor/autoload.php';
//require 'C:\Users\OlstadJ\d8tools\vendor\autoload.php';
//use \Goutte\Client;
//use \Symfony\Component\HttpClient\HttpClient;

#[\AllowDynamicProperties]
class Sitemap extends UpdateExport
{
  public $success = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile, $refaire = FALSE)
  {
    $this->loadData($jfile);
    $this->data->refaire = $refaire;
    if ((property_exists($this->data, 'success') && $this->data->success == true) || $this->validationOfExport()) {
      // This content has already been processed, skip it, we do not want to reprocess.
      $this->success = TRUE;
      $this->data->success = TRUE;
      return $this->success;
    }
    if ($this->data->has_dcr_id || is_numeric($this->data->dcr_id)) {
      $this->add_or_update_body_field();
      $this->add_or_update_body_field_no_section();
    }

    $this->save();
    $this->success = TRUE;
    return $this->success;
  }


}

