{
    "dcr_id": "1644352289898",
    "title": {
        "en": "Transfer of care requirements for transporting animals",
        "fr": "Exigences en mati\u00e8re de transfert de garde pour le transport des animaux"
    },
    "html_modified": "16-2-2022",
    "modified": "11-3-2022",
    "issued": "8-2-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/transfer_of_care_req_transport_animals_1644352289898_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/transfer_of_care_req_transport_animals_1644352289898_fra"
    },
    "parent_ia_id": "1300460096845",
    "ia_id": "1644352653424",
    "parent_node_id": "1300460096845",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Transfer of care requirements for transporting animals",
        "fr": "Exigences en mati\u00e8re de transfert de garde pour le transport des animaux"
    },
    "label": {
        "en": "Transfer of care requirements for transporting animals",
        "fr": "Exigences en mati\u00e8re de transfert de garde pour le transport des animaux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1644352653424",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300460032193",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/humane-transport/transfer-of-care/",
        "fr": "/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/exigences-en-matiere-de-transfert/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Transfer of care requirements for transporting animals",
            "fr": "Exigences en mati\u00e8re de transfert de garde pour le transport des animaux"
        },
        "description": {
            "en": "The Transfer of Care (TOC) document helps ensure the humane transport of animals within Canada.",
            "fr": "Le document sur le transfert de garde (TDG) aide \u00e0 assurer le transport sans cruaut\u00e9 des animaux au Canada."
        },
        "keywords": {
            "en": "Animals, Animal Health, Livestock, transport, Canada",
            "fr": "Animaux, Sant\u00e9 des animaux, Transport, b\u00e9tail, Canada"
        },
        "dcterms.subject": {
            "en": "animal,livestock,inspection,animal inspection,animal diseases,regulations,animal health,transport",
            "fr": "animal,b\u00e9tail,inspection,inspection des animaux,maladie animale,r\u00e9glementations,sant\u00e9 animale,transport"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-02-16",
            "fr": "2022-02-16"
        },
        "modified": {
            "en": "2022-03-11",
            "fr": "2022-03-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Transfer of care requirements for transporting animals",
        "fr": "Exigences en mati\u00e8re de transfert de garde pour le transport des animaux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/transfer_of_care_req_transport_animals_poster_1644331649552_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Transfer of care requirements for transporting animals in Portable document format\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Transfer of care requirements for transporting animals</span> <span class=\"gc-dwnld-info nowrap\">(PDF \u2013 112 kb)</span></p>\n</div>\n</div>\n</div></a>\n</div>\n</div>\n\n<figure class=\"mrgn-bttm-lg\"> \n<img alt=\"Transfer of care requirements for transporting animals - description follows\" class=\"mrgn-bttm-sm img-responsive\" src=\"/DAM/DAM-animals-animaux/STAGING/images-images/transfer_of_care_req_transport_animals_poster_1644331526117_eng.jpg\"> \n<details> \n<summary>Transfer of care requirements for transporting animals  \u2013 Text version</summary>\n \n<p>Transporting animals?</p>\n<p>Important</p>\n<p><i>Health of Animals Regulations</i> Section 153 Transfer Of Care:</p>\n<p>The Transfer of Care (TOC) document helps ensure the humane transport of animals within Canada. Federal regulations require that transporters give the TOC document to the recipient of the animals before leaving the slaughter establishment or assembly centre. This ensures that there is no interruption in responsibility for animal care.</p>\n<p>The TOC document requires the following information:</p>\n<ol class=\"lst-lwr-alph\">\n<li>Condition of the animals upon arrival </li>\n<li>Date, time and place when the animals were last fed, watered and rested </li>\n<li>Date and time the animals arrived at the slaughter establishment or assembly centre</li>\n</ol>\n\n<p>Once the TOC document has been acknowledged by the recipient, responsibility for care of the animals shifts from the transporter to the establishment.</p>\n<p>For <a href=\"http://inspection.canada.ca/humane\">additional information</a> on the <i>Health of Animals Regulations</i>, visit inspection.canada.ca/humane</p>\n<p>CFIA P1038E-21 Catalogue No.: A104-233/2021E ISBN: 978-0-660-41114-9</p>\n</details>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/transfer_of_care_req_transport_animals_poster_1644331649552_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Exigences en mati\u00e8re de transfert de soins pour le transport des animaux en format PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Exigences en mati\u00e8re de transfert de garde pour le transport des animaux</span> <span class=\"gc-dwnld-info nowrap\">(PDF \u2013 112 ko)</span></p>\n</div>\n</div>\n</div></a>\n</div>\n</div>\n\n\n<figure class=\"mrgn-bttm-lg\"> \n<img alt=\"Exigences en mati\u00e8re de transfert de soins pour le transport des animaux - description ci-dessous\" class=\"mrgn-bttm-sm img-responsive\" src=\"/DAM/DAM-animals-animaux/STAGING/images-images/transfer_of_care_req_transport_animals_poster_1644331526117_fra.jpg\"> \n<details> \n<summary>Exigences en mati\u00e8re de transfert de garde pour le transport des animaux \u2013 Version textuelle</summary>\n\n<p>Transport d'animaux</p>\n<p>Important</p>\n<p>Article 153 sur le transfert de garde du <i>R\u00e8glement sur la sant\u00e9 des animaux</i>\u00a0:</p>\n<p>Le document sur le transfert de garde (TDG) aide \u00e0 assurer le transport sans cruaut\u00e9 des animaux au Canada. La r\u00e9glementation f\u00e9d\u00e9rale exige que les transporteurs transmettent le document sur le TDG \u00e0 la personne recevant les animaux avant de quitter l'\u00e9tablissement d'abattage ou le centre de rassemblement. Cela permet de s'assurer qu'il n'y a pas d'interruption dans la responsabilit\u00e9 de la garde des animaux.</p>\n<p>Le document de TDG exige les renseignements suivants\u00a0:</p>\n<ol class=\"lst-lwr-alph\">\n<li>l'\u00e9tat des animaux \u00e0 l'arriv\u00e9e;</li>\n<li>la date, l'heure et l'endroit o\u00f9 les animaux ont \u00e9t\u00e9 aliment\u00e9s, abreuv\u00e9s et se sont repos\u00e9s pour la derni\u00e8re fois;</li>\n<li>la date et l'heure de l'arriv\u00e9e des animaux \u00e0 l'\u00e9tablissement d'abattage ou au centre de rassemblement.</li>\n</ol>\n\n<p>Lorsque le destinataire accuse r\u00e9ception du document, la responsabilit\u00e9 de la garde des animaux lui est transf\u00e9r\u00e9e.</p>\n\n<p>Pour <a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/fra/1300460032193/1300460096845\">obtenir de plus amples</a> renseignements sur le R\u00e8glement sur la sant\u00e9 des animaux, consultez le site inspection.canada.ca/sanscruaute</p>\n\n<p>ACIA P1038F-21 No de catalogue\u00a0: A104-233/2021F ISBN\u00a0: 978-0-660-41115-6</p>\n</details>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}