{
    "dcr_id": "1396451527777",
    "title": {
        "en": "Import requirements for soil on imported plants (other than potatoes)",
        "fr": "Exigences d'importation relatives \u00e0 la terre sur les v\u00e9g\u00e9taux import\u00e9s (autres que les pommes de terre)"
    },
    "html_modified": "2024-02-13 9:49:47 AM",
    "modified": "2014-04-10",
    "issued": "2014-04-10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/plan_hort_imp_soil_1396451527777_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/plan_hort_imp_soil_1396451527777_fra"
    },
    "ia_id": "1396451570777",
    "parent_ia_id": "1542127490190",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Imports",
        "fr": "Importations"
    },
    "commodity": {
        "en": "Horticulture",
        "fr": "Horticulture"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1542127490190",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Import requirements for soil on imported plants (other than potatoes)",
        "fr": "Exigences d'importation relatives \u00e0 la terre sur les v\u00e9g\u00e9taux import\u00e9s (autres que les pommes de terre)"
    },
    "label": {
        "en": "Import requirements for soil on imported plants (other than potatoes)",
        "fr": "Exigences d'importation relatives \u00e0 la terre sur les v\u00e9g\u00e9taux import\u00e9s (autres que les pommes de terre)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1396451570777",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1542127489815",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/soil/requirements-for-soil/",
        "fr": "/protection-des-vegetaux/terre/exigences-relatives-a-la-terre/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Import requirements for soil on imported plants (other than potatoes)",
            "fr": "Exigences d'importation relatives \u00e0 la terre sur les v\u00e9g\u00e9taux import\u00e9s (autres que les pommes de terre)"
        },
        "description": {
            "en": "This document is intended to clearly explain the Canadian import requirements for soil that may be associated with imported plants intended for planting, as a resource for anyone involved in importing plants to Canada.",
            "fr": "Le pr\u00e9sent document vise \u00e0 expliquer clairement les exigences canadiennes d?importation concernant la terre qui peut se trouver sur les v\u00e9g\u00e9taux import\u00e9s destin\u00e9s \u00e0 la plantation, afin de servir de ressource pour toute personne participant \u00e0 l?importation de v\u00e9g\u00e9taux au Canada."
        },
        "keywords": {
            "en": "imports, requirements, soil, plants, shipments, pests, prohibited, quarantine",
            "fr": "importation, exigences, terre, plantes, envois, phytoravageurs, interdit, quarantaine"
        },
        "dcterms.subject": {
            "en": "imports,inspection,plants",
            "fr": "importation,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-04-10",
            "fr": "2014-04-10"
        },
        "modified": {
            "en": "2014-04-10",
            "fr": "2014-04-10"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Import requirements for soil on imported plants (other than potatoes)",
        "fr": "Exigences d'importation relatives \u00e0 la terre sur les v\u00e9g\u00e9taux import\u00e9s (autres que les pommes de terre)"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=15&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>This document is intended to clearly explain the Canadian import requirements for soil that may be associated with imported plants intended for planting, as a resource for anyone involved in importing plants to Canada. Fully understanding these requirements helps everyone protect Canada and minimizes problems with non-compliant shipments (that is, shipments that do not meet regulatory requirements).</p>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p><strong>Note:</strong></p>\n<p>This document is only about the requirements for soil on imported plants. There are also requirements for the plants themselves.</p>\n<p>Please see also:</p>\n<p><a href=\"/plant-health/horticulture/questions-and-answers/eng/1396453190750/1396453225939\">Questions and answers - <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s import requirements for soil on plants intended for planting</a></p>\n<p><a href=\"/plant-health/invasive-species/plant-import/primer/eng/1324568450671/1324569734910\">Importing plants and plant products: what you need to know</a></p>\n<p><a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/rights-and-services/importer-s-guide/eng/1326314465411/1326314609820\">An Importer's Guide to Canadian Food Inspection Agency (CFIA) Inspections</a></p>\n<p><a href=\"/plant-health/invasive-species/directives/date/d-98-01/eng/1312247584545/1312247721536\">Directive D-98-01</a>, for requirements for potato (<i lang=\"la\">Solanum tuberosum</i>) material intended for planting</p>\n<p><a href=\"/plant-health/invasive-species/directives/date/d-08-04/eng/1323752901318/1323753612811\">Directive D-08-04</a>, for general import requirements for plants for planting</p>\n</div>\n\n<p>A wide range and large volume of plants (and plant parts) intended for planting are imported into Canada from all over the world. These plants could have soil on them. Soil can contain serious plant pests, including several that are regulated by Canada as prohibited quarantine pests. Even very small amounts of soil can contain microscopic pests. Pests that can be found in soil include molluscs, insects, nematodes, fungi, bacteria and viruses.</p>\n<p>Soil on imported plants can be introduced into the environment, where pests can multiply and spread, endangering the Canadian economy and environment. Because of this, the Canadian Food Inspection Agency (CFIA) strictly regulates the importation of plants intended for planting to ensure that they are free of soil-borne quarantine plant pests.</p>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p><strong>Note:</strong></p>\n<p>Throughout this document, the term \"soil\" also includes soil-related matter, such as compost, muck and earthworm castings, as well as soil-free growing media other than that imported under the <a href=\"/plant-health/invasive-species/directives/date/d-96-20/eng/1323854223506/1323854343186\">Canadian Growing Media Program</a></p>\n<p>See the <a href=\"/plant-health/invasive-species/directives/glossary/eng/1304730588212/1304730789969\">Plant Health Glossary of Terms</a> for definitions of the terms used in this document.</p>\n</div>\n\n<h2>Plants from areas other than the continental United States</h2>\n<p>You cannot import soil from areas other than the continental United States. <strong>All plants and plant parts intended for planting that are imported into Canada must be entirely free from soil.</strong> At most, the plants can have a fine film of dust, such as what might be left by dirty wash water. If any thicker films, patches or clumps of soil are found during the import inspection in Canada, the plants are considered non-compliant.</p>\n<p>You can help avoid incidents of non-compliance by ensuring that your foreign suppliers are fully informed of Canadian import requirements. When preparing a lot for shipment to Canada, suppliers should ensure that plants are thoroughly cleaned of all soil before they are shipped. They should pay particular attention to:</p>\n<ul>\n<li>cleaning roots that can more easily trap soil, such as mats of fine rootlets, closely packed roots, crevices, branching points, <abbr title=\"et cetera\">etc.</abbr>; and</li>\n<li>types of soil that may be more difficult to clean, such as clays.</li>\n</ul>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p>For information on how non-compliant material is managed, please see <a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/rights-and-services/importer-s-guide/eng/1326314465411/1326314609820\">An Importer's Guide to Canadian Food Inspection Agency (CFIA) Inspections</a>.</p>\n</div>\n\n<h2>Plants from the continental United States</h2>\n<p>You can import plants with soil from most parts of the continental United States, as long as they are accompanied by a Phytosanitary Certificate stating that they are free of certain quarantine soil-borne pests regulated by Canada.</p>\n<p>For some plant species, you must obtain a Permit to Import from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. On the permit application form, you must indicate whether the plants are being imported with or without soil. Under a permit for plants without soil, the plants must be entirely free from soil. If any films, patches or clumps of soil are found during the import inspection in Canada, the plants are considered non-compliant.</p>\n<p>If the plants are treated to remove most soil \u2013 for example, shaken to facilitate shipping \u2013 but are not cleaned expressly to remove all soil, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will not consider the plants free from soil. These plants should not be imported under a permit for plants without soil. If there is a chance that even small amounts of soil could be left on the plants, it may be safer to include both plants with and without soil on your permit application. There is no additional fee for including both types of material on the permit.</p>\n<p>Please contact the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> at any time if you have questions about the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s import requirements.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=15&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Le pr\u00e9sent document vise \u00e0 expliquer clairement les exigences canadiennes d'importation concernant la terre qui peut se trouver sur les v\u00e9g\u00e9taux import\u00e9s destin\u00e9s \u00e0 la plantation, afin de servir de ressource pour toute personne participant \u00e0 l'importation de v\u00e9g\u00e9taux au Canada. Une bonne compr\u00e9hension de ces exigences aide tous les participants \u00e0 prot\u00e9ger le Canada et minimise les probl\u00e8mes li\u00e9s aux envois non conformes (envois qui ne satisfont pas aux exigences r\u00e9glementaires).</p>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p><strong>Remarque :</strong></p>\n<p>Le pr\u00e9sent document ne concerne que les exigences relatives \u00e0 la terre sur les v\u00e9g\u00e9taux import\u00e9s. Il existe \u00e9galement des exigences propres aux v\u00e9g\u00e9taux.</p>\n<p>Veuillez aussi consulter :</p>\n<p><a href=\"/protection-des-vegetaux/horticulture/questions-et-reponses/fra/1396453190750/1396453225939\">Questions et r\u00e9ponses - Les exigences d'importation de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> concernant la terre sur les v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation</a></p>\n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/guide-d-introduction/fra/1324568450671/1324569734910\">Importation de v\u00e9g\u00e9taux et de produits v\u00e9g\u00e9taux\u00a0: Ce que vous devez savoir</a></p>\n<p><a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/droits-et-services/guide-destine-aux-importateurs/fra/1326314465411/1326314609820\">Guide sur l'inspection destin\u00e9 aux importateurs</a></p>\n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-98-01/fra/1312247584545/1312247721536\">Directive D-98-01</a>, pour les exigences relatives au mat\u00e9riel de pomme de terre (<i lang=\"la\">Solanum tuberosum</i>) destin\u00e9 \u00e0 la plantation</p>\n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-08-04/fra/1323752901318/1323753612811\">Directive D-08-04</a>, pour les exigences g\u00e9n\u00e9rales d'importation visant les v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation</p>\n</div>\n\n<p>Une grande quantit\u00e9 et vari\u00e9t\u00e9 de v\u00e9g\u00e9taux (et de parties de v\u00e9g\u00e9taux) destin\u00e9s \u00e0 la plantation sont import\u00e9s au Canada en provenance de partout dans le monde. De la terre pourrait se trouver sur ces v\u00e9g\u00e9taux. La terre peut contenir des phytoravageurs tr\u00e8s nuisibles, y compris plusieurs qui sont r\u00e9glement\u00e9s par le Canada comme organismes nuisibles interdits justiciables de quarantaine. M\u00eame de tr\u00e8s petites quantit\u00e9s de terre peuvent contenir des organismes nuisibles microscopiques. Mollusques, insectes, n\u00e9matodes, champignons, bact\u00e9ries et virus sont des organismes nuisibles qui peuvent se trouver dans la terre.</p>\n<p>La terre se trouvant sur les v\u00e9g\u00e9taux import\u00e9s peut \u00eatre introduite dans l'environnement, o\u00f9 les organismes nuisibles peuvent se multiplier et se propager, mettant en danger l'environnement et l'\u00e9conomie du Canada. Pour cette raison, l'Agence canadienne d'inspection des aliments (ACIA) r\u00e9glemente rigoureusement l'importation de v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation afin de s'assurer qu'ils sont exempts de phytoravageurs terricoles justiciables de quarantaine.</p>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p><strong>Remarque :</strong></p>\n<p>Tout au long du pr\u00e9sent document, le terme \u00ab terre \u00bb englobe \u00e9galement les mati\u00e8res connexes \u00e0 la terre, comme le compost, la terre noire et les turricules, ainsi que les milieux de culture exempts de terre autres que ceux qui sont import\u00e9s au titre du <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-96-20/fra/1323854223506/1323854343186\">Programme canadien des milieux de culture</a>.</p>\n<p>Veuillez vous r\u00e9f\u00e9rer au <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/glossaire/fra/1304730588212/1304730789969\">Glossaire de la protection des v\u00e9g\u00e9taux</a> pour conna\u00eetre la d\u00e9finition des termes utilis\u00e9s dans le pr\u00e9sent document.</p>\n</div>\n\n<h2>V\u00e9g\u00e9taux provenant de r\u00e9gions autres que la zone continentale des \u00c9tats Unis</h2>\n<p>Vous ne pouvez pas importer de la terre en provenance de r\u00e9gions autres que la zone continentale des \u00c9tats-Unis. <strong>Tous les v\u00e9g\u00e9taux et parties de v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation qui sont import\u00e9s au Canada doivent \u00eatre enti\u00e8rement exempts de terre.</strong> Au plus, les v\u00e9g\u00e9taux peuvent \u00eatre couverts d'une fine pellicule de poussi\u00e8re, comme ce qu'aurait pu laisser de l'eau de lavage sale. Les v\u00e9g\u00e9taux sont jug\u00e9s non conformes si des pellicules plus \u00e9paisses, des plaques ou des mottes de terre sont trouv\u00e9es lors de l'inspection \u00e0 l'importation au Canada.</p>\n<p>Vous pouvez aider \u00e0 \u00e9viter les incidents de non-conformit\u00e9 en vous assurant que vos fournisseurs \u00e0 l'\u00e9tranger disposent de tous les renseignements concernant les exigences canadiennes d'importation. Lorsqu'ils pr\u00e9parent un lot qui sera exp\u00e9di\u00e9 au Canada, les fournisseurs devraient s'assurer que les v\u00e9g\u00e9taux sont m\u00e9ticuleusement d\u00e9barrass\u00e9s de toute trace de terre avant d'\u00eatre exp\u00e9di\u00e9s. Ils devraient porter une attention particuli\u00e8re \u00e0 ce qui suit\u00a0:</p>\n<ul>\n<li>nettoyage des racines qui peuvent plus facilement emprisonner la terre, comme les masses de fines racines, les racines \u00e9troitement group\u00e9es, les fissures, les points de ramification, <abbr title=\"et cetera\">etc.</abbr>; et</li>\n<li>types de terre qui peuvent \u00eatre plus difficiles \u00e0 nettoyer, comme la glaise.</li>\n</ul>\n\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p>Pour en savoir plus sur la gestion du mat\u00e9riel non conforme, veuillez consulter le <a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/droits-et-services/guide-destine-aux-importateurs/fra/1326314465411/1326314609820\">Guide sur l'inspection destin\u00e9 aux importateurs</a>.</p>\n</div>\n\n<h2>V\u00e9g\u00e9taux provenant de la zone continentale des \u00c9tats Unis</h2>\n<p>Vous pouvez importer des v\u00e9g\u00e9taux avec de la terre \u00e0 partir de la majeure partie de la zone continentale des \u00c9tats-Unis, pourvu qu'ils soient accompagn\u00e9s d'un certificat phytosanitaire attestant qu'ils sont exempts de certains organismes nuisibles terricoles qui sont justiciables de quarantaine et r\u00e9glement\u00e9s par le Canada.</p>\n<p>Pour certaines esp\u00e8ces de v\u00e9g\u00e9taux, vous devez obtenir un permis d'importation aupr\u00e8s de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Dans le formulaire de demande de permis, vous devez pr\u00e9ciser si les v\u00e9g\u00e9taux sont import\u00e9s avec ou sans terre. En vertu d'un permis d'importation de v\u00e9g\u00e9taux sans terre, les v\u00e9g\u00e9taux doivent \u00eatre totalement exempts de terre. Les v\u00e9g\u00e9taux sont jug\u00e9s non conformes si des pellicules, des plaques ou des mottes de terre sont trouv\u00e9es lors de l'inspection \u00e0 l'importation au Canada.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> ne consid\u00e9rera pas comme \u00e9tant exempts de terre des v\u00e9g\u00e9taux qu'on a trait\u00e9s pour les d\u00e9barrasser de la plus grande partie de la terre (par exemple secou\u00e9s de mani\u00e8re \u00e0 en faciliter le transport), mais qu'on n'a pas nettoy\u00e9s express\u00e9ment pour enlever toute trace de terre. Ces v\u00e9g\u00e9taux ne devraient pas \u00eatre import\u00e9s en vertu d'un permis pour v\u00e9g\u00e9taux exempts de terre. S'il existe un risque que m\u00eame une petite quantit\u00e9 de terre pourrait demeurer sur ces v\u00e9g\u00e9taux, il peut \u00eatre plus prudent d'inclure dans votre demande de permis les v\u00e9g\u00e9taux avec terre et exempts de terre. On peut inclure les deux types de mat\u00e9riel dans la demande de permis sans subir de frais suppl\u00e9mentaires.</p>\n<p>Veuillez communiquer avec l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en tout temps si vous avez des questions concernant ses exigences en mati\u00e8re d'importation.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}