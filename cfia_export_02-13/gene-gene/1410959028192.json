{
    "dcr_id": "1410959028192",
    "title": {
        "en": "Service Standard for Labelling Review and Approval for Minor Changes",
        "fr": "Norme de service pour l'\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs"
    },
    "html_modified": "2024-02-13 9:50:16 AM",
    "modified": "2019-06-03",
    "issued": "2015-04-30",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_reg_service_standards_vet_bio_labelling_1410959028192_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_reg_service_standards_vet_bio_labelling_1410959028192_fra"
    },
    "ia_id": "1410959029832",
    "parent_ia_id": "1396057840621",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1396057840621",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Service Standard for Labelling Review and Approval for Minor Changes",
        "fr": "Norme de service pour l'\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs"
    },
    "label": {
        "en": "Service Standard for Labelling Review and Approval for Minor Changes",
        "fr": "Norme de service pour l'\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1410959029832",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1396057840090",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/acts-and-regulations/service-standards/veterinary-biologics/labelling/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/normes-de-service/programme-relatif-aux-produits-biologiques-veterin/etiquetage/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Service Standard for Labelling Review and Approval for Minor Changes",
            "fr": "Norme de service pour l'\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs"
        },
        "description": {
            "en": "Service Standard for Labelling Review and Approval for Minor Changes",
            "fr": "Norme de service pour l?\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs."
        },
        "keywords": {
            "en": "acts, regulations, administration, enforcement, service standards, veterinary biologics program, labelling",
            "fr": "lois, r\u00e8glementaires, assurer, cont\u00f4ler l'application, normes de service, frais d'utilisation, Programme relatif aux produits biologiques v\u00e9t\u00e9rinaires, \u00e9tiquetage"
        },
        "dcterms.subject": {
            "en": "legislation,standards,regulation,food safety,animal health",
            "fr": "l\u00e9gislation,norme,r\u00e9glementation,salubrit\u00e9 des aliments,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-30",
            "fr": "2015-04-30"
        },
        "modified": {
            "en": "2019-06-03",
            "fr": "2019-06-03"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Service Standard for Labelling Review and Approval for Minor Changes",
        "fr": "Norme de service pour l'\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Labelling review and approval for minor changes will be completed within 28 days.</p>\n\n<p>Labelling review and approval for major changes will be completed within 70 days.</p>\n\n<h2>Performance Target</h2>\n\n<p>The target for achieving this standard is set at 90%.</p>\n\n<h2>Performance Results</h2>\n\n<p>Annual performance:</p>\n\n<ul>\n<li>2018-19: the Agency processed 99% of 135 submissions within the service standard.</li>\n</ul>\n\n<h2>Applying for Labelling Review and Approval for Minor Changes</h2>\n<p>Information pertaining to the labelling for veterinary biologics can be found in the <a href=\"/animal-health/veterinary-biologics/guidelines-forms/3-3/eng/1328284048248/1328284150222\">Veterinary biologics guideline 3.3 - Labelling of veterinary biologics</a>.</p>\n\n<h2>Service feedback</h2>\n<p>If you have a service complaint, please complete a <a href=\"/eng/1299860523723/1299860643049#form-formulaire\">Feedback Form</a> or communicate with one of our <a href=\"/animal-health/veterinary-biologics/contacts/eng/1299162041191/1320704114965\">Contacts</a>.</p>\n\n<h2>For more information</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/federal-regulatory-management.html\">Federal regulatory management and modernization</a></li>\n<li><a href=\"https://www.canada.ca/en/treasury-board-secretariat/services/federal-regulatory-management/government-wide-forward-regulatory-plans.html\">Government-Wide Forward Regulatory Plans</a></li>\n<li><a href=\"/about-the-cfia/cfia-2025/rcc/eng/1427420901391/1427420973247\">The Canada-United States Regulatory Cooperation Council</a></li>\n</ul>\n<p>To learn about upcoming or ongoing consultations on proposed federal regulations, visit the <a href=\"http://gazette.gc.ca/accueil-home-eng.html\"><i>Canada Gazette</i></a> and <a href=\"https://www.canada.ca/en/government/system/consultations/consultingcanadians.html\">Consulting with Canadians</a> websites.</p>\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'examen de l'\u00e9tiquetage et l'approbation des modifications mineures seront termin\u00e9s dans un d\u00e9lai de 28 jours.</p>\n\n<p>L'examen de l'\u00e9tiquetage et l'approbation des modifications majeures seront termin\u00e9s dans un d\u00e9lai de 70 jours.</p>\n\n<h2>Cible de rendement</h2>\n\n<p>La cible pour satisfaire \u00e0 cette norme est de <span class=\"nowrap\">90 %</span>.</p>\n\n<h2>Information sur le rendement</h2>\n\n<p>Rendement annuel\u00a0:</p>\n<ul>\n<li>2018-2019\u00a0: l'Agence a trait\u00e9 99\u00a0% des 135 demandes dans le cadre de la norme de service.</li>\n</ul>\n\n<h2>Demande de l'\u00e9valuation de l'\u00e9tiquetage et approbation de changements mineurs</h2>\n\n<p>Les renseignements relatifs \u00e0 l'\u00e9tiquetage des produits biologiques v\u00e9t\u00e9rinaires se trouvent dans les <a href=\"/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-3/fra/1328284048248/1328284150222\">Lignes directrices des produits biologiques v\u00e9t\u00e9rinaires 3.3 \u2013 Lignes directrices pour l'\u00e9tiquetage des produits biologiques v\u00e9t\u00e9rinaires</a>.</p>\n\n<h2>M\u00e9canisme de r\u00e9troaction</h2>\n\n<p>Si vous souhaitez formuler une plainte relative au service, veuillez remplir une <a href=\"/fra/1299860523723/1299860643049#form-formulaire\">Feuille de r\u00e9ponse</a> ou communiquez avec un de nos <a href=\"/sante-des-animaux/produits-biologiques-veterinaires/personnes-ressources/fra/1299162041191/1320704114965\">Personnes-ressources</a>.</p>\n\n<h2>Pour de plus amples renseignements</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/gestion-reglementation-federale.html\">Gestion et modernisation de la r\u00e9glementation f\u00e9d\u00e9rale</a></li>\n<li><a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/services/gestion-reglementation-federale/plans-prospectif-reglementation-echelle-gouvernement.html\">Plans prospectifs de la r\u00e9glementation \u00e0 l'\u00e9chelle du gouvernement</a></li>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/ccr/fra/1427420901391/1427420973247\">Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation Canada\u2013\u00c9tats-Unis</a></li>\n</ul>\n<p>Pour de plus amples renseignements concernant les consultations actuelles ou \u00e0 venir sur les projets de r\u00e8glement f\u00e9d\u00e9raux, veuillez consulter la <a href=\"http://gazette.gc.ca/accueil-home-fra.html\"><i>Gazette du Canada</i></a> ou le site <a href=\"https://www.canada.ca/fr/gouvernement/systeme/consultations/consultationdescanadiens.html\">Web Consultations aupr\u00e8s des Canadiens</a>.</p>\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}