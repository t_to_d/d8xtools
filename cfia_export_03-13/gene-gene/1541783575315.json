{
    "dcr_id": "1541783575315",
    "title": {
        "en": "Appendix H: Use of wood in dairy facilities",
        "fr": "Annexe H : Utilisation du bois dans les \u00e9tablissements laitiers"
    },
    "html_modified": "2024-03-12 3:58:13 PM",
    "modified": "2019-01-11",
    "issued": "2019-01-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/apph_uow_df_1541783575315_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/apph_uow_df_1541783575315_fra"
    },
    "ia_id": "1541784144769",
    "parent_ia_id": "1534954778164",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Applying preventive controls|Maintaining physical structures and surroundings|Making food - Manufacturing, processing, preparing, preserving, treating",
        "fr": "Application de mesures de contr\u00f4le pr\u00e9ventif|Maintien des structures physiques et de l'environnement|Production d'aliments - Fabrication, transformation, conditionnement, pr\u00e9servation et traitement"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1534954778164",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Appendix H: Use of wood in dairy facilities",
        "fr": "Annexe H : Utilisation du bois dans les \u00e9tablissements laitiers"
    },
    "label": {
        "en": "Appendix H: Use of wood in dairy facilities",
        "fr": "Annexe H : Utilisation du bois dans les \u00e9tablissements laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1541784144769",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1534954777758",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/dairy-products/appendix-h/",
        "fr": "/controles-preventifs/produits-laitiers/annexe-h/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Appendix H: Use of wood in dairy facilities",
            "fr": "Annexe H : Utilisation du bois dans les \u00e9tablissements laitiers"
        },
        "description": {
            "en": "This document provides guidance on the conditions for the use of wood in dairy facilities.",
            "fr": "Le pr\u00e9sent document fournit des directives sur les conditions pour l'utilisation du bois dans les \u00e9tablissements laitiers."
        },
        "keywords": {
            "en": "wood, dairy facilities",
            "fr": "bois, \u00e9tablissements laitie"
        },
        "dcterms.subject": {
            "en": "food,agri-food industry,food inspection,milk,agri-food products,dairy products,food processing",
            "fr": "aliment,industrie agro-alimentaire,inspection des aliments,lait,produit agro-alimentaire,produit laitier, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-12",
            "fr": "2019-01-12"
        },
        "modified": {
            "en": "2019-01-11",
            "fr": "2019-01-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Appendix H: Use of wood in dairy facilities",
        "fr": "Annexe H : Utilisation du bois dans les \u00e9tablissements laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\n<h2>Introduction</h2>\n\n<p>Wood is an absorbent surface that is difficult to clean to keep free of contaminating bacteria. In dairy facilities, wood is not a suitable material for food contact surfaces, supplementary utensils or physical structures where product is exposed or placed at risk of contamination, with a few exceptions:</p>\n\n<ul>\n<li>Wood used for \u201c640\u201d cheese boxes (with appropriate liners and pallets)</li>\n<li>Wood used for shelving in cheese curing rooms for bacterial surface ripened cheeses</li>\n</ul>\n\n<h2>Definitions</h2>\n\n<p>For the purpose of this document, the following definition applies.</p>\n\n<p><strong>Bacterial surface ripened cheese</strong>: a cheese whose rind is formed by bacterial action (for example, Saint Paulin, Bel Pase and Tilsit).</p>\n\n<h2>Wood shelving used for cheese curing</h2>\n\n<p>For reasons of tradition and suitability, wood is used to aid the surface ripening of cheeses.\u00a0 The bacteria (usually Brevibacterium linens) on the shelf surface break down part of the cheese protein and form the rind.\u00a0 The cheeses are washed and turned regularly during ripening.\u00a0 The cheese shelves are seeded with the bacteria which become ingrained in the wood.\u00a0 Under controlled conditions, the harmless bacteria significantly outnumber the undesirable microorganisms and therefore the risk of contamination is minimized.</p>\n\n<h3>Conditions</h3>\n\n<ul>\n<li class=\"mrgn-bttm-md\">Ensure the wood that is used for cheese shelves to cure bacterial surface ripened cheeses is smooth and either unsealed or sealed with a suitable sealant.\n<ul>\n<li>Supports for the shelves should be stainless steel or a non-corrosive, non-absorbent material.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Have a written, effective program to clean and maintain the shelves.\u00a0\n<ul>\n<li>The program specifies:\n<ul>\n<li>the frequency and methods of washing;</li>\n<li>checking and replacing for physical condition (splinters, cracks, mite infestation);</li>\n<li>and if required, environmental sample monitoring of the product contact surfaces and the room environment.</li>\n</ul>\n</li>\n<li>Keep applicable records.</li>\n</ul>\n</li>\n<li>Verify that there is no scientific evidence indicating that the use of wood shelves used to cure bacterial surface ripened cheeses poses a biological risk to the cheese.</li>\n</ul>\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<h2>Introduction</h2>\n\n<p>Le bois est une surface absorbante difficile \u00e0 nettoyer et, par cons\u00e9quent, \u00e0 tenir exempt de bact\u00e9ries contaminants. Dans les \u00e9tablissements laitiers, le bois ne doit pas \u00eatre utilis\u00e9 pour les surfaces entrant en contact avec les aliments, les ustensiles suppl\u00e9mentaires ou les structures physiques o\u00f9 le produit est expos\u00e9 ou risque d\u2019\u00eatre contamin\u00e9, \u00e0 quelques exceptions pr\u00e8s\u00a0:</p>\n\n<ul>\n<li>Le bois utilis\u00e9 pour les contenants \u00e0 fromage\u00a0\u00ab\u00a0640\u00a0\u00bb (avec palettes et rev\u00eatements appropri\u00e9s);</li>\n<li>Le bois utilis\u00e9 pour les \u00e9tag\u00e8res dans les salles de maturation de fromages affin\u00e9s en surface par action bact\u00e9rienne.</li>\n</ul>\n\n<h2>D\u00e9finition</h2>\n\n<p>Les d\u00e9finitions qui suivent s\u2019appliquent aux fins du pr\u00e9sent document.</p>\n\n<p><strong>Fromages affin\u00e9s en surface par action\u00a0bact\u00e9rienne\u00a0: </strong>les fromages affin\u00e9s en surface par action bact\u00e9rienne sont ceux dont la cro\u00fbte est form\u00e9e par action bact\u00e9rienne (par exemple, Saint-Paulin, Bel\u00a0Pase, Tilsit, etc.).</p>\n\n<h2>\u00c9tag\u00e8res en bois utilis\u00e9es pour l\u2019affinage du fromage</h2>\n\n<p>En raison de son utilisation traditionnelle et de son adaptation \u00e0 cet usage, le bois est utilis\u00e9 pour l\u2019affinage en surface de certains fromages. Les bact\u00e9ries (habituellement Brevibacterium\u00a0linens) pr\u00e9sentes sur la surface des \u00e9tag\u00e8res d\u00e9composent une partie des prot\u00e9ines du fromage pour former la cro\u00fbte. Les fromages sont lav\u00e9s et retourn\u00e9s r\u00e9guli\u00e8rement pendant l\u2019affinage. Les \u00e9tag\u00e8res \u00e0 fromage sont ensemenc\u00e9es avec des bact\u00e9ries qui s\u2019impr\u00e8gnent dans le bois. Dans des conditions contr\u00f4l\u00e9es, les bact\u00e9ries non nuisibles sont sensiblement plus abondantes que les microorganismes ind\u00e9sirables, ce qui r\u00e9duit le risque de contamination.</p>\n\n<h3>Conditions</h3>\n\n<ul>\n<li class=\"mrgn-bttm-md\">Veiller \u00e0 ce que le bois utilis\u00e9 dans la fabrication des \u00e9tag\u00e8res servant \u00e0 l\u2019affinage des fromages par action bact\u00e9rienne soit lisse et impr\u00e9gn\u00e9 ou non avec un produit d\u2019\u00e9tanch\u00e9it\u00e9 approuv\u00e9.\n<ul>\n<li>Les supports des \u00e9tag\u00e8res doivent \u00eatre faits d\u2019acier inoxydable ou d\u2019un mat\u00e9riel r\u00e9sistant \u00e0 la corrosion et non absorbant.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Mettre en place un programme efficace de nettoyage et d\u2019entretien des \u00e9tag\u00e8res; celui-ci doit \u00eatre consign\u00e9 par \u00e9crit.\n<ul>\n<li>Veiller \u00e0 ce que le programme pr\u00e9cise\u00a0:\n<ul>\n<li>la fr\u00e9quence et les m\u00e9thodes de lavage;</li>\n<li>la v\u00e9rification et le remplacement en raison du mauvais \u00e9tat (\u00e9cornures, fissures, infestation par des acariens);</li>\n<li>s\u2019il y a lieu, la surveillance par pr\u00e9l\u00e8vement d\u2019\u00e9chantillons environnementaux sur les surfaces de contact avec le produit et dans le milieu.</li>\n</ul>\n</li>\n<li>Ternir des registres appropri\u00e9s.</li>\n</ul>\n</li>\n<li>V\u00e9rifier que les donn\u00e9es scientifiques d\u00e9montrent que l\u2019utilisation d\u2019\u00e9tag\u00e8res en bois pour l\u2019affinage des fromages en surface par action bact\u00e9rienne ne pr\u00e9sente aucun risque biologique pour le fromage.</li>\n</ul>\n\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}