{
    "dcr_id": "1502402540642",
    "title": {
        "en": "Federal requirements for information to accompany animals",
        "fr": "Exigences f\u00e9d\u00e9rales quant aux informations accompagnant les animaux"
    },
    "html_modified": "15-8-2017",
    "modified": "18-11-2022",
    "issued": "11-8-2017",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/terr_anima_trace_requirements_1502402540642_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/terr_anima_trace_requirements_1502402540642_fra"
    },
    "parent_ia_id": "1300461804752",
    "ia_id": "1502402540985",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Federal requirements for information to accompany animals",
        "fr": "Exigences f\u00e9d\u00e9rales quant aux informations accompagnant les animaux"
    },
    "label": {
        "en": "Federal requirements for information to accompany animals",
        "fr": "Exigences f\u00e9d\u00e9rales quant aux informations accompagnant les animaux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1502402540985",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1300461751002",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/traceability/federal-requirements/",
        "fr": "/sante-des-animaux/animaux-terrestres/tracabilite/exigences-federales/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Federal requirements for information to accompany animals",
            "fr": "Exigences f\u00e9d\u00e9rales quant aux informations accompagnant les animaux"
        },
        "description": {
            "en": "In Part XV of the Health of Animals Regulations, pig carcasses transported to any site and non-bred pigs transported between non-contiguous parts of a farm and between farms must be accompanied with consignment information in a form that can be immediately read by an inspector.",
            "fr": "Dans la partie XV du R\u00e8glement sur la sant\u00e9 des animaux, les carcasses de porcs transport\u00e9s vers une autre installation et les porcs non saillis transport\u00e9s entre deux endroits non contigus dans une m\u00eame ferme ou entre deux fermes doivent \u00eatre accompagn\u00e9s des renseignements sur l\u2019exp\u00e9dition, pr\u00e9sent\u00e9s sous une forme pouvant \u00eatre lue imm\u00e9diatement par un inspecteur."
        },
        "keywords": {
            "en": "Health of Animals Regulations, identification numbers, revoked tags, traceability program, national livestock identification, pig carcasses, non-bred pigs, farms",
            "fr": "R\u00e8glement sur la sant\u00e9 des animaux, \u00e9tiquettes, programmes identification, \u00e9tiquettes approuv\u00e9es, tra\u00e7abilit\u00e9 des animaux d'\u00e9levage, r\u00e9vocation des \u00e9tiquettes, Crit\u00e8res de s\u00e9lection, gamme de num\u00e9ros, carcasses de porcs, porcs non saillis, exigences"
        },
        "dcterms.subject": {
            "en": "inspection,agri-food products,animal health",
            "fr": "inspection,produit agro-alimentaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-08-15",
            "fr": "2017-08-15"
        },
        "modified": {
            "en": "2022-11-18",
            "fr": "2022-11-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Federal requirements for information to accompany animals",
        "fr": "Exigences f\u00e9d\u00e9rales quant aux informations accompagnant les animaux"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/eng/1672964761819\"></div>\n\n<p>In Part XV of the <a href=\"/english/reg/jredirect2.shtml?heasanr\"><i>Health of Animals Regulations</i></a>, pig carcasses transported to any site and non-bred pigs transported between non-contiguous parts of a farm and between farms must be accompanied with consignment information in a form that can be immediately read by an inspector.</p> \n\n<p>In Part\u00a0XII\u00a0of the <i><a href=\"/english/reg/jredirect2.shtml?heasanr\">Health of Animals Regulations</a></i>, there are 2\u00a0forms of documentation used with the movement of all animals:</p>\n\n<ul class=\"lst-spcd\">\n<li>The Transfer of Care (<a href=\"https://laws-lois.justice.gc.ca/eng/regulations/C.R.C.%2C_c._296/page-13.html#h-548225\">HAR s153</a>) ensures the continuity of care, that no animal is to be left at any slaughter facility, or assembly centre without written notice that care has been transferred between the transporter and the receiver. This is done to ensure that the individual responsible for caring for the animals can be clearly identified at all times.\n<ul class=\"lst-spcd\">\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/transfer-of-care/eng/1644352289898/1644352653424\">Transfer of care requirements for transporting animals</a> (PDF)</li>\n</ul></li>\n<li>The second is an animal transport record (<a href=\"https://laws-lois.justice.gc.ca/eng/regulations/C.R.C.,_c._296/page-13.html#h-548240\">HAR s154</a>) where commercial carriers, and those who transport animals in the course of business or for financial benefit, must make a record at the time of loading the animals, for each shipment, related to the movement of those animals</li>\n</ul>\n\n<p>For further guidance, review the <a href=\"/animal-health/terrestrial-animals/humane-transport/health-of-animals-regulations-part-xii/eng/1582126008181/1582126616914\"><i>Health of Animals Regulations</i>: Part\u00a0XII: Transport of Animals-Regulatory Amendment Interpretive Guidance for Regulated Parties</a>.</p>\n\n<p><a href=\"/animal-health/terrestrial-animals/traceability/provincial-requirements/eng/1505764055253/1505764055737\">Provincial requirements for information to accompany animals apply</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/fra/1672964761819\"></div>\n\n<p>Dans la partie XV du <a href=\"/francais/reg/jredirect2.shtml?heasanr\"><i>R\u00e8glement sur la sant\u00e9 des animaux</i></a>, les carcasses de porcs transport\u00e9s vers une autre installation et les porcs non saillis transport\u00e9s entre deux endroits non contigus dans une m\u00eame ferme ou entre deux fermes doivent \u00eatre accompagn\u00e9s des renseignements sur l'exp\u00e9dition, pr\u00e9sent\u00e9s sous une forme pouvant \u00eatre lue imm\u00e9diatement par un inspecteur.</p> \n\n<p>Dans la partie\u00a0XII\u00a0du <i><a href=\"/francais/reg/jredirect2.shtml?heasanr\">R\u00e8glement sur la sant\u00e9 des animaux</a></i>, il existe 2\u00a0types de documentation utilis\u00e9s lorsque des animaux\u00a0sont transport\u00e9s:</p>\n\n<ul class=\"lst-spcd\">\n<li>Le Transfert de garde (<a href=\"https://laws-lois.justice.gc.ca/fra/reglements/C.R.C.,_ch._296/page-13.html#h-536882\">RSA 153</a>) assure une continuit\u00e9 de la responsabilit\u00e9 de la garde des animaux en exigeant qu'aucun animal ne soit laiss\u00e9 \u00e0 un \u00e9tablissement d'abattage ou un centre de rassemblement sans un avis \u00e9crit indiquant que la responsabilit\u00e9 pour leur garde a \u00e9t\u00e9 transf\u00e9r\u00e9e du transporteur au destinataire. Cette exigence vise \u00e0 permettre l'identification de la personne responsable de la garde des animaux \u00e0 tout moment.</li>\n<ul class=\"lst-spcd\">\n<li><a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/exigences-en-matiere-de-transfert/fra/1644352289898/1644352653424\">Exigences en mati\u00e8re de transfert de garde pour le transport des animaux</a> (PDF)</li>\n</ul>\n<li>Le deuxi\u00e8me type est le registre de transport d'animaux (<a href=\"https://laws-lois.justice.gc.ca/fra/reglements/C.R.C.,_ch._296/page-13.html#h-536897\">RSA 154</a>) exigeant que les transporteurs commerciaux et toute personne qui transporte des animaux dans le cadre d'une entreprise ou \u00e0 des fins lucratives consigne dans un registre l'information requise quant au transport de ces animaux et ce, au moment de leur embarquement et pour chaque envoi.</li>\n</ul>\n\n<p>Pour obtenir de plus amples renseignements, consulter le <a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/reglement-sur-la-sante-des-animaux-partie-xii/fra/1582126008181/1582126616914\"><i>R\u00e8glement sur la sant\u00e9 des animaux</i>\u00a0partie XII\u00a0: modification au r\u00e8glement sur le transport des animaux Document d'orientation \u00e0 l'intention des parties r\u00e9glement\u00e9es</a>.</p>\n\n<p><a href=\"/sante-des-animaux/animaux-terrestres/tracabilite/exigences-provinciales/fra/1505764055253/1505764055737\">Des exigences provinciales quand \u00e0 des informations devant accompagner les animaux s'appliquent</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": false,
    "chat_wizard": false
}