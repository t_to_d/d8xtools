{
    "dcr_id": "1332424982450",
    "title": {
        "en": "Aquatic animal export: certification requirements for Vietnam",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Vietnam"
    },
    "html_modified": "2024-02-13 9:47:30 AM",
    "modified": "2022-07-29",
    "issued": "2012-03-22 10:03:03",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_vie_1332424982450_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_vie_1332424982450_fra"
    },
    "ia_id": "1332425107758",
    "parent_ia_id": "1331743129372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1331743129372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal export: certification requirements for Vietnam",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Vietnam"
    },
    "label": {
        "en": "Aquatic animal export: certification requirements for Vietnam",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Vietnam"
    },
    "templatetype": "content page 1 column",
    "node_id": "1332425107758",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331740343115",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/vietnam/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/vietnam/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal export: certification requirements for Vietnam",
            "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Vietnam"
        },
        "description": {
            "en": "This page describes aquatic animal health export certification requirements for Vietnam.",
            "fr": "Cette page d\u00e9crit les exigences de certification d'exportation de sant\u00e9 des animaux aquatiques pour le Vietnam."
        },
        "keywords": {
            "en": "aquatic animals, finfish, molluscs, crustacean, exports, human consumption, certification requirements, aquatic animal health, seafood products, NAAHP, Vietnam",
            "fr": "animaux aquatiques, poisson \u00e0 nageoires, mollusques, crustac\u00e9s, exportation,  consommation humaine,  exigences de certification, sant\u00e9 des animaux aquatiques, produits du poisson et des produits de la mer, PNSAA, Vietnam"
        },
        "dcterms.subject": {
            "en": "certification,aquatic animals,seafood,fish",
            "fr": "accr\u00e9ditation,animal aquatique,fruits de mer,poisson"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-22 10:03:03",
            "fr": "2012-03-22 10:03:03"
        },
        "modified": {
            "en": "2022-07-29",
            "fr": "2022-07-29"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,media",
            "fr": "entreprises,grand public,m\u00e9dia"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal export: certification requirements for Vietnam",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour le Vietnam"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p><strong>The following certificates are available:</strong></p>\n\n<ul>\n\n<li>Aquatic Animal Health Certificate for the Export of Live Aquatic Animals Intended for  Human Consumption from Canada to Vietnam (<abbr title=\"Aquatic Animal Health\">AQAH</abbr>-1025)</li>\n\n<li>Certificate of Origin and Hygiene CFIA/ACIA\u00a05003, available through <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/vietnam-fish-and-seafood/eng/1304219514425/1304219682673\">Vietnam \u2013 Export requirements for fish and seafood</a></li>\n\n</ul>\n\n<p><strong>Important Notes:</strong></p>\n\n<ul>\n\n<li>If wishing to export non-live fish, please contact Fish and Seafood.</li>\n\n<li>For consignments containing any <strong>live species of aquatic animals</strong>, except Pacific Oyster (<span lang=\"la\">Crassostrea gigas</span>), it is the importer's and exporter's responsibility to obtain a valid import licence or permit issued by the Vietnamese Authorities prior to import.</li>\n\n<li>Except for Pacific Oyster, which can be imported into Vietnam without the need for a risk assessment or import permit, all <strong>other live aquatic animal species</strong> from Canada are required to undergo a risk assessment approval process through the General Department of Fisheries of Vietnam's Ministry of Agriculture and Rural Development (MARD) before an import permit can be issued.</li>\n\n<li>The exporter shall provide the Inspector with a copy or copies of any and all import licences and/or permits required to import live aquatic animal species, except Pacific Oyster (<span lang=\"la\">Crassostrea gigas</span>), into Vietnam prior to <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s issuance of the certificate, and provide supporting documentation such as an affidavit indicating that the exporter believes the provided import license and/or permit is valid.</li>\n\n<li>In addition to the above mentioned certificate, each shipment must also be accompanied by a certificate of Origin and Hygiene CFIA/ACIA\u00a05003.</li>\n\n<li>Except in the case of establishments that only ship live product, <strong>processing establishments and the products</strong> they wish to export must appear on the <a href=\"http://cucthuy.gov.vn/Pages/danh-sach-thuy-san.aspx\">list of Canadian establishments and their products approved for export to Vietnam (Vietnamese only)</a>. As of <span class=\"nowrap\">July 18, 2017</span>, responsibility for approving foreign establishments to export foodstuffs of animal origin and for maintaining the above-noted list rests with the Department of Animal Health (DAH) of Vietnam's <abbr title=\"Ministry of Agriculture and Rural Development\">MARD</abbr>. Please refer to the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/vietnam-fish-and-seafood/eng/1304219514425/1304219682673\">Procedure to update the list of Canadian establishments and their products approved for export to Vietnam</a>.</li>\n\n<li>There may be existing certificates or additional information from the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/vietnam-fish-and-seafood/eng/1304219514425/1304219682673\">Fish and Seafood</a> program. However, for the specific commodities above, an aquatic animal health certificate is required.</li>\n\n<li>To request an export certificate outlined above or if you require any other information on aquatic animal health export certification, please contact your <a href=\"/eng/1300462382369/1365216058692\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a>.</li>\n\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p><strong>Les certificats suivants sont disponibles\u00a0:</strong></p>\n\n<ul>\n\n<li>Certificat d'hygi\u00e8ne des animaux aquatiques pour l'exportation des animaux aquatiques vivants pour consommation humaine du Canada au Vietnam (<abbr title=\"hygi\u00e8ne des animaux aquatiques\">AQAH</abbr>-1025)</li>\n\n<li>Certificat d'origine et d'hygi\u00e8ne (CFIA/ACIA\u00a05003) lequel s'obtient sur la page <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/vietnam-le-poisson-et-les-produits-de-la-mer/fra/1304219514425/1304219682673\">Vietnam\u00a0\u2013 Exigences d'exportation pour le poisson et les produits de la mer</a></li>\n\n</ul>\n\n<p><strong>Remarques importantes\u00a0:</strong></p>\n\n<ul>\n\n<li>Pour exporter des poissons non vivants, veuillez communiquer avec la Division du poisson, des produits de la mer et de la production.</li>\n\n<li>Pour les envois contenant des <strong>esp\u00e8ces vivantes d'animaux aquatiques</strong>, sauf l'Hu\u00eetre du Pacifique (<span lang=\"la\">Crassostrea gigas</span>), il incombe \u00e0 l'importateur et \u00e0 l'exportateur d'obtenir une licence ou un permis d'importation valide d\u00e9livr\u00e9 par les autorit\u00e9s vietnamiennes avant l'importation.</li>\n\n<li>\u00c0 l'exception de l'Hu\u00eetre du Pacifique, qui peut \u00eatre import\u00e9 au Vietnam sans avoir besoin d'une \u00e9valuation des risques ou d'un permis d'importation, toutes les <strong>autres esp\u00e8ces d'animaux aquatiques vivants</strong> du Canada doivent subir un processus d'\u00e9valuation des risques par le minist\u00e8re vietnamien de l'Agriculture et du D\u00e9veloppement rural (MARD) avant qu'un permis d'importation puisse \u00eatre d\u00e9livr\u00e9.</li>\n\n<li>L'exportateur doit fournir \u00e0 l'inspecteur une ou plusieurs copies de tous les permis et\u00a0/ ou licences d'importation requis pour importer des esp\u00e8ces animales aquatiques vivantes, \u00e0 l'exception de l'Hu\u00eetre du Pacifique (<span lang=\"la\">Crassostrea gigas</span>), au Vietnam avant la d\u00e9livrance du certificat par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, et des documents tels qu'un affidavit indiquant que l'exportateur est d'avis que le permis d'importation et\u00a0/ ou le permis sont valides.</li>\n\n<li>Outre le certificat susmentionn\u00e9, chaque envoi doit \u00eatre accompagn\u00e9 d'un certificat d'origine et d'hygi\u00e8ne (CFIA/ACIA 5003).</li>\n\n<li>Sauf dans le cas des \u00e9tablissements qui exp\u00e9dient uniquement des produits vivants, <strong>les \u00e9tablissements de transformation et les produits</strong> qu'ils souhaitent exporter doivent figurer sur <a href=\"http://cucthuy.gov.vn/Pages/danh-sach-thuy-san.aspx\">la liste des \u00e9tablissements canadiens et de leurs produits approuv\u00e9s pour l'exportation vers le Vietnam (vietnamiennes seulement)</a>. Au <span class=\"nowrap\">18 juillet 2017</span>, la responsabilit\u00e9 de l'approbation des \u00e9tablissements \u00e9trangers pour l'exportation de produits alimentaires d'origine animale et du maintien de la liste susmentionn\u00e9e incombe au D\u00e9partement de la sant\u00e9 animale (DAH) du <abbr title=\"minist\u00e8re vietnamien de l'Agriculture et du D\u00e9veloppement rural\">MARD</abbr> du Vietnam.\u00a0 Veuillez vous reporter \u00e0 la <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/vietnam-le-poisson-et-les-produits-de-la-mer/fra/1304219514425/1304219682673\">Proc\u00e9dure pour mettre \u00e0 jour la liste des \u00e9tablissements canadiens et de leurs produits approuv\u00e9s pour l'exportation vers le Vietnam</a>.</li>\n\n<li>Il est possible que d'autres certificats ou des renseignements suppl\u00e9mentaires soient disponibles par le biais du programme de <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/vietnam-le-poisson-et-les-produits-de-la-mer/fra/1304219514425/1304219682673\">Poisson et produits de la mer</a>. Cependant, en ce qui a trait aux produits susmentionn\u00e9s, un certificat d'hygi\u00e8ne des animaux aquatiques est requis.</li>\n\n<li>Pour faire une demande d'un certificat d'exportation indiqu\u00e9 ci-dessus ou pour toute autre demande de renseignements concernant la certification sanitaire des animaux aquatiques destin\u00e9s \u00e0 l'exportation, veuillez communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> de votre Centre op\u00e9rationnel.</li>\n\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}