{
    "dcr_id": "1556664558232",
    "title": {
        "en": "Notice to industry: Simplified plant import permit application process",
        "fr": "Avis \u00e0 l'industrie : Processus simplifi\u00e9 de demande de permis d'importation de v\u00e9g\u00e9taux"
    },
    "html_modified": "2024-03-12 3:58:25 PM",
    "modified": "2019-05-01",
    "issued": "2019-05-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/notice_ind_simpl_permit_app_1556664558232_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/notice_ind_simpl_permit_app_1556664558232_fra"
    },
    "ia_id": "1556664558557",
    "parent_ia_id": "1324569331710",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1324569331710",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice to industry: Simplified plant import permit application process",
        "fr": "Avis \u00e0 l'industrie : Processus simplifi\u00e9 de demande de permis d'importation de v\u00e9g\u00e9taux"
    },
    "label": {
        "en": "Notice to industry: Simplified plant import permit application process",
        "fr": "Avis \u00e0 l'industrie : Processus simplifi\u00e9 de demande de permis d'importation de v\u00e9g\u00e9taux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1556664558557",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1324569244509",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-import/notice-to-industry/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/avis-a-l-industrie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice to industry: Simplified plant import permit application process",
            "fr": "Avis \u00e0 l'industrie : Processus simplifi\u00e9 de demande de permis d'importation de v\u00e9g\u00e9taux"
        },
        "description": {
            "en": "Notice to industry: Simplified plant import permit application process",
            "fr": "Avis \u00e0 l'industrie : Processus simplifi\u00e9 de demande de permis d'importation de v\u00e9g\u00e9taux"
        },
        "keywords": {
            "en": "Plant Protection Act, Plant Protection Act, plants, insects, imports, permits, application, electronically, notice to industry, simplified process",
            "fr": "Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, v\u00e9g\u00e9taux, insectes, importation, permis, \u00e9lectronique, processus simplifi\u00e9"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-05-01",
            "fr": "2019-05-01"
        },
        "modified": {
            "en": "2019-05-01",
            "fr": "2019-05-01"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice to industry: Simplified plant import permit application process",
        "fr": "Avis \u00e0 l'industrie : Processus simplifi\u00e9 de demande de permis d'importation de v\u00e9g\u00e9taux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>May 1, 2019</p>\n\n<p>Beginning June 1, 2019, importers applying for an import permit under the <i>Plant Protection Act</i> can submit their application electronically to the Canadian Food Inspection Agency, without having to submit a hard copy.</p>\n\n<p>Removing the hard-copy requirement makes the electronic application process easier, faster and cheaper, and reduces the administrative burden. This updated process promotes electronic access to CFIA services in line with the Agency's priority to offer digital-first tools and services.</p> \n\n<p>You can now submit the <a href=\"/eng/1328823628115/1328823702784#c5256\">Application for Permit to Import Plants and Other Things (CFIA/ACIA 5256)</a> by using <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\">My CFIA</a> online services or by <a href=\"mailto:PermitOffice@inspection.gc.ca\">e-mail</a>, fax or mail.</p> \n\n<p>For more information on how to apply for this permit, see CFIA's <a href=\"/plant-health/invasive-species/directives/imports/d-97-04/eng/1323791055523/1323803716515\">Directive D-97-04</a>.</p>\n\n<p>If you have questions about this notice, please email <a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le 1<sup>er</sup> mai 2019</p>\n\n<p>\u00c0 compter du 1<sup>er</sup> juin 2019, les importateurs souhaitant obtenir un permis d'importation en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> peuvent pr\u00e9senter leur demande par voie \u00e9lectronique \u00e0 l'Agence canadienne d'inspection des aliments, et ce, sans avoir \u00e0 fournir de copie en format papier.</p>\n\n<p>Le retrait de l'exigence relative aux copies en format papier permet de rendre le processus de demande \u00e9lectronique plus facile, plus rapide et moins co\u00fbteux, et de r\u00e9duire le fardeau administratif. La mise \u00e0 niveau de ce processus fait la promotion de l'acc\u00e8s \u00e9lectronique aux services de l'ACIA, conform\u00e9ment \u00e0 la priorit\u00e9 de l'Agence de favoriser les outils et les services num\u00e9riques.</p> \n  \n<p>Vous pouvez maintenant pr\u00e9senter le formulaire <a href=\"/fra/1328823628115/1328823702784#c5256\">Demande de permis pour importer des v\u00e9g\u00e9taux et d'autres choses en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> (ACIA/CFIA 5256)</a> \u00e0 l'aide des services en ligne de <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\">Mon ACIA</a> ou par <a href=\"mailto:PermitOffice@inspection.gc.ca\">courriel</a>, par t\u00e9l\u00e9copieur ou par courrier.</p> \n\n<p>Pour obtenir de plus amples renseignements sur la fa\u00e7on de pr\u00e9senter une demande pour ce permis, veuillez consulter la <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-97-04/fra/1323791055523/1323803716515\">directive D-97-04</a> de l'ACIA.</p>\n\n<p>Pour toute question au sujet de cet avis, veuillez envoyer un courriel \u00e0 l'adresse <a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}