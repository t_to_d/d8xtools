{
    "dcr_id": "1524844134086",
    "title": {
        "en": "Algeria - Export requirements for honey",
        "fr": "Alg\u00e9rie - Exigences d'exportation pour le miel"
    },
    "html_modified": "2024-02-13 9:53:00 AM",
    "modified": "2019-11-27",
    "issued": "2018-05-25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/honey_export_overview_algeria_1524844134086_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/honey_export_overview_algeria_1524844134086_fra"
    },
    "ia_id": "1524844161696",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "|Agency personnel|Industry",
        "fr": "|Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Inspecting and investigating - Performing export certification activities|Exporting food",
        "fr": "Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation|Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Honey",
        "fr": "Miel"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Algeria - Export requirements for honey",
        "fr": "Alg\u00e9rie - Exigences d'exportation pour le miel"
    },
    "label": {
        "en": "Algeria - Export requirements for honey",
        "fr": "Alg\u00e9rie - Exigences d'exportation pour le miel"
    },
    "templatetype": "content page 1 column",
    "node_id": "1524844161696",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/algeria-honey/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/algerie-miel/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Algeria - Export requirements for honey",
            "fr": "Alg\u00e9rie - Exigences d'exportation pour le miel"
        },
        "description": {
            "en": "Countries to which exports are currently made - Algeria",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Alg\u00e9rie"
        },
        "keywords": {
            "en": "honey, Canadian honey, bee products, export requirements, federally registered honey establishments, Algeria",
            "fr": "miel, miel canadien, produits d'abeilles, exigences d'exportation,\u00e9tablissements de miel agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral, Alg\u00e9rie"
        },
        "dcterms.subject": {
            "en": "food labelling,exports,imports,food inspection,honey,agri-food products,food safety,food processing",
            "fr": "\u00e9tiquetage des aliments,exportation,importation,inspection des aliments,miel,produit agro-alimentaire,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exportation d'aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-05-25",
            "fr": "2018-05-25"
        },
        "modified": {
            "en": "2019-11-27",
            "fr": "2019-11-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Algeria - Export requirements for honey",
        "fr": "Alg\u00e9rie - Exigences d'exportation pour le miel"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Canadian honey prepared in federally registered or licensed honey establishments</li></ul>\n\n<p><strong>Note:</strong> Bee products for human consumption, such as honeycomb (comb honey), flavoured honey, royal jelly, bee propolis, and bee pollen, are also regulated under the <a href=\"/english/reg/jredirect2.shtml?drga\"><i>Food and Drugs Act</i></a>, the\u00a0<a href=\"/english/reg/jredirect2.shtml?drgr\"><i>Food and Drugs Regulations</i></a>. Additionally, honey for human consumption is regulated under the\u00a0<a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i></a> and <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a>.</p>\n\n<h2>Production controls and inspection requirements</h2>\n\n<ul><li class=\"mrgn-bttm-md\">The exporter should contact the <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Canadian Food Inspection Agency (CFIA) regional office</a> closest to where the shipment will be loaded at least seven (7) business days before the scheduled date of shipment, and request the following documents:\n<ul><li class=\"mrgn-bttm-md\">Health certificate request for export of honey to Algeria</li>\n<li class=\"mrgn-bttm-md\">Manufacturer's declaration for export of honey to Algeria</li></ul></li></ul>\n\n<h3>Provincial sanitary certificate</h3>\n\n<ul><li class=\"mrgn-bttm-md\">The provincial sanitary certificate is provided to the exporter by the provincial apiarist and is for <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> internal use only, as a supporting document for final certification where applicable.</li>\n<li class=\"mrgn-bttm-md\">If the option of provincial certification is not selected, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will require certificates of analysis for American foulbrood (<i lang=\"la\">Paenibacillus larvae</i>) and European foulbrood (<i lang=\"la\">Melissococcus plutonius</i>) to grant the health certificate. See <a href=\"#a1\">documentation requirements</a> for more details.</li></ul>\n\n<h2 id=\"a1\">Documentation requirements</h2>\n\n<h3>Certificate</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Health certificate for the export of honey from Canada to Algeria\n<ul><li class=\"mrgn-bttm-md\"><p>Statements on the small hive beetle (<span lang=\"la\">Aethina tumida</span>):</p>\n\n<h4>Condition 1:</h4>\n<p>Honey must come from apiaries that are not subject to restrictions for animal health reasons related to infestation by the small hive beetle <i lang=\"la\">Aethina tumida</i>.</p>\n<p>If condition 1 is selected, certification is based on the sanitary certificate issued by <strong>provincial authorities</strong>.</p>\n\n<h4>Condition 2:</h4>\n<p>If provincial certification is not available, the honey must have been strained through a filter of pore size no greater than 0.42 <abbr title=\"millimetre\">mm</abbr>, <strong>and</strong> all precautions must have been taken to prevent contamination with <i lang=\"la\">Aethina tumida</i>.</p>\n<p>If the option of provincial certification is not selected, condition 2 will be certified by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> inspector on the basis of the <strong>Manufacturer's declaration</strong> for export of honey to Algeria.</p>\n</li>\n<li class=\"mrgn-bttm-md\"><p>Statements on American foulbrood (<i lang=\"la\">Paenibacillus larvae</i>):</p>\n<h4>Condition 1:</h4>\n<p>Honey must come from apiaries situated in a country or zone free from American foulbrood.</p>\n<p>If condition 1 is selected, this certification is based on the sanitary certificate issued by <strong>provincial authorities</strong>.</p>\n\n<h4>Condition 2:</h4>\n<p>The honey has been found free from spore forms of <i lang=\"la\">Paenibacillus larvae</i> by a test method described in the relevant chapter of the Terrestrial Manual of the World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE))</p>\n<p>Selection of condition 2 is based on <strong>laboratory results</strong> (accredited) provided to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> by the exporter.</p>\n</li>\n<li class=\"mrgn-bttm-md\"><p>Statements on European foulbrood (<i lang=\"la\">Melissococcus plutonius</i>):</p>\n<h4>Condition 1:</h4>\n<p>Honey must come from apiaries situated in a country or zone free from European foulbrood.</p>\n<p>If condition 1 is selected, this certification is based on the sanitary certificate issued by <strong>provincial authorities</strong>.</p>\n\n<h4>Condition 2:</h4>\n<p>The honey has been found free from <span lang=\"la\">Melissococcus plutonius</span> by a test method described in the relevant chapter of the <abbr title=\"World Organisation for Animal Health\">WOAH</abbr>'s Terrestrial Manual.</p>\n<p>Selection of condition 2 is based on <strong>laboratory results</strong> (accredited) provided to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> by the exporter.</p>\n</li>\n<li class=\"mrgn-bttm-md\">It is the responsibility of the exporter to ensure that the health certificate is accompanied by the applicable certificates of analyses as per the note at the bottom of the health certificate.</li></ul></li></ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Miel canadien pr\u00e9par\u00e9 dans des \u00e9tablissements de miel agr\u00e9\u00e9s ou sont titulaires d'une licence par le gouvernement f\u00e9d\u00e9ral</li></ul>\n\n<p><strong>Remarque\u00a0:</strong> Les produits apicoles destin\u00e9s \u00e0 la consommation humaine, comme le nid d'abeilles (rayon de miel), le miel aromatis\u00e9, la gel\u00e9e royale, la propolis d'abeilles, et le pollen d'abeille, sont \u00e9galement r\u00e9glement\u00e9s par la\u00a0<a href=\"/francais/reg/jredirect2.shtml?drga\"><i>Loi sur les aliments et drogues</i></a>, le\u00a0<a href=\"/francais/reg/jredirect2.shtml?drgr\"><i>R\u00e8glement sur les aliments et drogues</i></a>. De plus, le miel destin\u00e9 \u00e0 la consommation humaine est r\u00e9gi par la\u00a0<a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i></a> et le\u00a0<a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a>.</p>\n\n<h2>Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<ul><li class=\"mrgn-bttm-md\">L'exportateur doit communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau r\u00e9gional de l'Agence canadienne d'inspection des aliments (ACIA)</a> le plus pr\u00e8s de l'endroit o\u00f9 l'envoi sera charg\u00e9 au moins sept (7) jours ouvrables avant la date d'exp\u00e9dition pr\u00e9vue et demander les documents suivants\u00a0:\n<ul><li class=\"mrgn-bttm-md\">Demande de certificat sanitaire pour l'exportation de miel vers l'Alg\u00e9rie</li>\n<li class=\"mrgn-bttm-md\">D\u00e9claration du fabricant pour l'exportation de miel vers l'Alg\u00e9rie</li></ul></li></ul>\n\n<h3>Certificat sanitaire provincial</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Le certificat sanitaire provincial est fourni \u00e0 l'exportateur par l'apiculteur provincial et est destin\u00e9 \u00e0 l'usage interne de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> seulement, \u00e0 titre de document \u00e0 l'appui de la certification finale, le cas \u00e9ch\u00e9ant.</li>\n<li class=\"mrgn-bttm-md\">Si l'option de certification provinciale n'est pas s\u00e9lectionn\u00e9e, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> exigera des certificats d'analyse pour la loque am\u00e9ricaine (<i lang=\"la\">Paenibacillus larvae</i>) et la loque europ\u00e9enne (<i lang=\"la\">Melissococcus plutonius</i>) pour d\u00e9livrer le certificat sanitaire. Voir <a href=\"#a1\">les exigences de documentation</a> pour plus de d\u00e9tails.</li></ul>\n\n<h2 id=\"a1\">Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<ul><li class=\"mrgn-bttm-md\">Certificat sanitaire pour l'exportation de miel du Canada vers l'Alg\u00e9rie\n<ul><li class=\"mrgn-bttm-md\"><p>D\u00e9clarations sur le petit col\u00e9opt\u00e8re des ruches (<i lang=\"la\">Aethina tumida</i>)\u00a0:</p>\n<h4g>Condition 1\u00a0:\n<p>Le miel doit provenir de ruchers qui ne sont pas soumis \u00e0 des restrictions pour des raisons de sant\u00e9 animale li\u00e9es \u00e0 l'infestation par le petit col\u00e9opt\u00e8re des ruches <i lang=\"la\">Aethina tumida</i>.</p>\n<p>Si la condition 1 est s\u00e9lectionn\u00e9e, la certification est bas\u00e9e sur le certificat sanitaire d\u00e9livr\u00e9 par <strong>les autorit\u00e9s provinciales</strong>.</p>\n\n<h4>Condition 2\u00a0:</h4>\n<p>Si la certification provinciale n'est pas disponible, le miel doit avoir \u00e9t\u00e9 filtr\u00e9 \u00e0 travers un filtre dont la taille des pores ne d\u00e9passe pas 0.42 <abbr title=\"millim\u00e8tre\">mm</abbr> <strong>et</strong> toutes les pr\u00e9cautions doivent avoir \u00e9t\u00e9 prises pour \u00e9viter la contamination par <i lang=\"la\">Aethina tumida</i>.</p>\n<p>Si l'option de certification provinciale n'est pas s\u00e9lectionn\u00e9e, la condition 2 sera certifi\u00e9e par l'inspecteur de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> sur la base de la <strong>D\u00e9claration du fabricant</strong> pour l'exportation de miel vers l'Alg\u00e9rie.</p>\n</h4g></li>\n<li class=\"mrgn-bttm-md\"><p>D\u00e9clarations sur la loque am\u00e9ricaine (<i lang=\"la\">Paenibacillus larvae</i>)\u00a0:</p>\n<h4>Condition 1\u00a0:</h4>\n<p>Le miel doit provenir de ruchers situ\u00e9s dans un pays ou une zone indemne de loque am\u00e9ricaine.</p>\n<p>Si la condition 1 est s\u00e9lectionn\u00e9e, cette certification est bas\u00e9e sur le certificat sanitaire d\u00e9livr\u00e9 par <strong>les autorit\u00e9s provinciales</strong>.</p>\n<h4>Condition 2\u00a0:</h4>\n<p>Le miel a \u00e9t\u00e9 trouv\u00e9 exempt de formes de spores de <i lang=\"la\">Paenibacillus larvae</i> par une m\u00e9thode d'essai d\u00e9crite dans le chapitre pertinent du Manuel terrestre de l'Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)).</p>\n<p>La s\u00e9lection de la condition 2 est bas\u00e9e sur <strong>les r\u00e9sultats de laboratoire</strong> (accr\u00e9dit\u00e9s) fournis \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> par l'exportateur.</p>\n</li>\n<li class=\"mrgn-bttm-md\"><p>D\u00e9clarations sur la loque europ\u00e9enne (<i lang=\"la\">Melissococcus plutonius</i>)\u00a0:</p>\n<h4>Condition 1\u00a0:</h4>\n<p>Le miel doit provenir de ruchers situ\u00e9s dans un pays ou une zone indemne de loque europ\u00e9enne.</p>\n<p>Si la condition 1 est s\u00e9lectionn\u00e9e, cette certification est bas\u00e9e sur le certificat sanitaire d\u00e9livr\u00e9 par <strong>les autorit\u00e9s provinciales</strong>.</p>\n<h4>Condition 2\u00a0:</h4>\n<p>Le miel a \u00e9t\u00e9 trouv\u00e9 indemne de <i lang=\"la\">Melissococcus plutonius</i> par une m\u00e9thode d'essai d\u00e9crite dans le chapitre pertinent du Manuel terrestre de l'<abbr title=\"Organisation mondiale de la sant\u00e9 animale\">OMSA</abbr>.</p>\n<p>La s\u00e9lection de la condition 2 est bas\u00e9e sur <strong>les r\u00e9sultats de laboratoire</strong> (accr\u00e9dit\u00e9s) fournis \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> par l'exportateur.</p>\n\n</li>\n<li class=\"mrgn-bttm-md\">Il est de la responsabilit\u00e9 de l'exportateur de s'assurer que le certificat est accompagn\u00e9 des analyses bact\u00e9riologiques conform\u00e9ment \u00e0 la note figurant au bas du certificat</li></ul></li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}