{
    "dcr_id": "1646095692928",
    "title": {
        "en": "Fertilizer or supplement registration: Overview",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Aper\u00e7u"
    },
    "html_modified": "2024-01-26 2:20:46 PM",
    "modified": "2023-10-25",
    "issued": "2023-10-26",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-overview_new_regs_1646095692928_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-overview_new_regs_1646095692928_fra"
    },
    "ia_id": "1646095693725",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fertilizer or supplement registration: Overview",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Aper\u00e7u"
    },
    "label": {
        "en": "Fertilizer or supplement registration: Overview",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Aper\u00e7u"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646095693725",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/overview/",
        "fr": "/protection-des-vegetaux/engrais/apercu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fertilizer or supplement registration: Overview",
            "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Aper\u00e7u"
        },
        "description": {
            "en": "The amended Fertilizers Regulations are in force. Until October 25, 2023 regulated parties can comply with either the: new regulations, old regulations.",
            "fr": "Le r\u00e8glement modifi\u00e9 sur les engrais est en vigueur. Jusqu'au 25 octobre 2023, les parties r\u00e9glement\u00e9es peuvent se conformer soit\u00a0: \u00ab\u00a0nouveau\u00a0\u00bb r\u00e8glement, ancien\u00a0\u00bb r\u00e8glement."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizer regulations, Fertilizer, supplement, registration, Overview, new, regulations",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, Enregistrement, engrais, suppl\u00e9ment,\u00a0Aper\u00e7u, nouveau, r\u00e8glement"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy",
            "fr": "cultures,engrais,inspection,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "modified": {
            "en": "2023-10-25",
            "fr": "2023-10-25"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fertilizer or supplement registration: Overview",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Aper\u00e7u"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646102406709/1646102407100\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/before-you-apply/eng/1646274247575/1646274247919\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/how-to-apply/eng/1646282540949/1646282541291\">4. How to apply</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/after-you-apply/eng/1646347538968/1646347539296\">5. After you apply</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646359107928/1646359108272\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<div class=\"row\">\n<section class=\"col-md-12\">\n<div class=\"col-sm-8\">\n\n<h2 class=\"mrgn-tp-0\">What is fertilizer or supplement registration</h2>\n\n<section id=\"centred-popup\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h3 class=\"modal-title\">Definition</h3>\n</header>\n<div class=\"modal-body\">\n\n<p>We are committed to achieving the stated processing times a minimum of\u00a0<strong>90% of the time</strong> under normal operational circumstances. The processing time impacting the remaining\u00a010% will vary based on:</p>\n\n<ul>\n<li>the type of application submitted (level of risk or complexity)</li>\n<li>how long you take to respond to any requests or concerns</li>\n<li>the volume of applications received</li>\n<li>technical reviews or inspections</li>\n<li>changes in policy and how we operate</li>\n<li>factors beyond our control (for example, natural disasters, conflicts in certain regions, animal diseases outbreaks, plant health pest restrictions)</li>\n</ul>\n\n<p>Your processing time starts the day we receive your <strong>complete application</strong> and ends when we have issued a decision.</p>\n</div>\n</section>\n\n\n<p>Some <a href=\"#centred-popup2\" aria-controls=\"centred-popup2\" class=\"wb-lbx\" role=\"button\">fertilizers</a> and most <a href=\"#centred-popup3\" aria-controls=\"centred-popup3\" class=\"wb-lbx\" role=\"button\">supplements</a> need mandatory pre-market assessment and registration before importation or sale in Canada.</p>\n\n<section id=\"centred-popup2\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h3 class=\"modal-title\">Definition</h3>\n</header>\n<div class=\"modal-body\">\n<p><strong>Fertilizers:</strong> essential plant nutrients.</p>\n</div>\n</section>\n\n<section id=\"centred-popup3\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h3 class=\"modal-title\">Definition</h3>\n</header>\n<div class=\"modal-body\">\n<p><strong>Supplements:</strong> products other than fertilizers that improve the physical condition of the soil or aid plant growth or crop yield.</p>\n</div>\n</section>\n\n<p>The Canadian Food Inspection Agency (CFIA) conducts product assessments to verify product compliance with the <i>Fertilizers Act</i>, regulations and prescribed standards. As part of pre-market product assessments, CFIA officials evaluate the safety information/data and review the product label.</p>\n\n<p>CFIA officials will consider the following when reviewing product safety:</p>\n\n<ul>\n<li>all ingredients in the product (both active and inert)</li>\n<li>potential contaminants</li>\n<li>degradation products</li>\n</ul>\n<h2>Checklists for completing your fertilizer or supplement registration application</h2>\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/major-amendment/eng/1489416362659/1489417253330\">Major amendment</a> (AM)</li>\n<li><a href=\"/plant-health/fertilizers/overview/minor-amendment/eng/1489416667745/1489417220381\">Minor amendment</a> (MA)</li>\n<li><a href=\"/plant-health/fertilizers/overview/new-registration-and-re-registration/eng/1489416814515/1489417163675\">New registrations (NR) or re-registrations (RR)</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/me-too/eng/1489416897097/1489417080954\">Me-too registrations</a> (reserved for specific cases where an applicant wishes to register their own name/brand)</li>\n<li><a href=\"/plant-health/fertilizers/overview/product-specific-inquiry/eng/1320385315947/1320385808224\">Product specific inquiries</a> (IQ) (information requests)</li>\n</ul>\n\n<h2>Related links</h2>\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/fertilizers-and-supplements/eng/1330932243713/1330933201778\">Registration triggers for fertilizers and supplements</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498\">New regulations: Guide to submitting applications for registration under the\u00a0<i>Fertilizers Act</i></a></li>\n</ul>\n\n\n</div>\n\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Fees</h2>\n</header>\n<div class=\"panel-body\">\n\n<p>Refer to <a href=\"/eng/1582641645528/1582641871296#c6\">Part 5 of the CFIA Fees Notice</a> for Fertilizers Fees.</p>\n\n</div>\n</section>\n\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\"><a href=\"#centred-popup\" aria-controls=\"centred-popup\" class=\"wb-lbx\" role=\"button\">Processing time</a></h2>\n</header>\n<div class=\"panel-body\">\n<p>Processing times vary depending on submission type. Review the appendices of <a href=\"/eng/1305609994431/1307910971122#appa\" title=\"Appendix A and B\">service delivery standards for fertilizer and supplement registration</a> for more information.</p>\n</div>\n</section>\n\n\n</div>\n\n<div class=\"clearfix\"></div>\n\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n\n<li class=\"next\"><a href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646102406709/1646102407100\" rel=\"next\">Next</a></li>\n\n</ul>\n</nav>\n\n</section>\n\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646102406709/1646102407100\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646274247575/1646274247919\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646282540949/1646282541291\">4. Comment pr\u00e9senter une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apres-avoir-presente-une-demande/fra/1646347538968/1646347539296\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646359107928/1646359108272\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\n<div class=\"row\">\n<section class=\"col-md-12\">\n<div class=\"col-sm-8\">\n\n<h2 class=\"mrgn-tp-0\">Qu'est-ce que l'enregistrement d'engrais ou de suppl\u00e9ment</h2>\n\n<section id=\"cs-centre\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h3 class=\"modal-title\">D\u00e9finition</h3>\n</header>\n<div class=\"modal-body\">\n\n<p>Nous nous sommes engag\u00e9s \u00e0 la r\u00e9alisation des d\u00e9lais de traitement indiqu\u00e9s au moins\u00a0<strong>90\u00a0% du temps</strong> dans des conditions op\u00e9rationnelles normales. Le temps de traitement impactant les 10% restants varie en fonction de\u00a0:</p>\n\n<ul>\n<li>le type de la demande pr\u00e9sent\u00e9e (niveau de risque ou de complexit\u00e9)</li>\n<li>le temps qu'il vous faut pour r\u00e9pondre \u00e0 des demandes ou \u00e0 questions suppl\u00e9mentaires</li>\n<li>le volume des demandes re\u00e7ues</li>\n<li>examen techniques ou inspections</li>\n<li>des changements dans la politique et la fa\u00e7on dont nous fonctionnons</li>\n<li>facteurs ind\u00e9pendants de notre volont\u00e9 (par exemple\u00a0: catastrophes naturelles, conflits dans certaines r\u00e9gions, \u00e9pid\u00e9mies de maladies animales, restrictions phytosanitaires)</li>\n</ul>\n\n<p>Votre d\u00e9lai de traitement commence le jour o\u00f9 nous recevons votre <strong>demande compl\u00e8te</strong> et se termine lorsque nous prenons une decision.</p>\n</div>\n</section>\n\n<p>Certains <a href=\"#cs-centre2\" aria-controls=\"cs-centre\" class=\"wb-lbx\" role=\"button\">engrais</a> et la plupart des <a href=\"#cs-centre3\" aria-controls=\"cs-centre\" class=\"wb-lbx\" role=\"button\">suppl\u00e9ments</a> doivent faire l'objet d'une \u00e9valuation et d'un enregistrement obligatoires avant leur importation ou leur vente au Canada.</p>\n\n<section id=\"cs-centre2\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h3 class=\"modal-title\">D\u00e9finition</h3>\n</header>\n<div class=\"modal-body\">\n<p><strong>Engrais\u00a0:</strong> nutriments essentiels pour les plantes.</p>\n</div>\n</section>\n\n<section id=\"cs-centre3\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h3 class=\"modal-title\">D\u00e9finition</h3>\n</header>\n<div class=\"modal-body\">\n<p><strong>Suppl\u00e9ments:</strong> les produits autres que les engrais qui am\u00e9liorent l'\u00e9tat physique du sol ou favorisent la croissance des plantes ou le rendement des cultures.</p>\n</div>\n</section>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) effectue des \u00e9valuations de produits pour v\u00e9rifier leur conformit\u00e9 \u00e0 la <i>Loi et le r\u00e8glement sur les engrais</i> et aux normes prescrites. Dans le cadre des \u00e9valuations de produits avant leur mise en march\u00e9, les agents de l'ACIA \u00e9valuent les renseignements et les donn\u00e9es sur la s\u00e9curit\u00e9 et examinent l'\u00e9tiquette du produit.</p>\n\n<p>Le personnel de l'ACIA tiendront compte des \u00e9l\u00e9ments suivants lorsqu'ils examineront la s\u00e9curit\u00e9 des produits\u00a0:</p>\n<ul>\n<li>toutes les ingr\u00e9dients du produit (actifs et inertes)</li>\n<li>les contaminants potentiels</li>\n<li>les produits de d\u00e9gradation</li>\n</ul>\n<h2>Liste de contr\u00f4le  pour remplir votre demande d'enregistrement d'engrais ou de suppl\u00e9ments</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/modification-majeure/fra/1489416362659/1489417253330\">Modification majeur</a> (AM)</li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/modification-mineure/fra/1489416667745/1489417220381\">Modification mineure</a> (MA)</li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/nouvel-enregistrement-et-reenregistrement/fra/1489416814515/1489417163675\">Enregistrement (NR) et r\u00e9enregistrement (RR)</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/-me-too-/fra/1489416897097/1489417080954\">Produits semblables \u00ab\u00a0<span lang=\"en\">Me-Too</span>\u00a0\u00bb</a> (r\u00e9serv\u00e9s \u00e0 des cas particuliers, lorsqu'un demandeur souhaite enregistrer son propre nom / marque)</li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/demande-d-information-sur-un-produit-specifique/fra/1320385315947/1320385808224\">Demande d'information sur un produit sp\u00e9cifique</a> (IQ) (demande d'information compl\u00e8te)</li>\n</ul>\n\n<h2>Liens pertinents</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/engrais-et-les-supplements/fra/1330932243713/1330933201778\">D\u00e9clencheurs de l'enregistrement pour les engrais et les suppl\u00e9ments en vertu de la\u00a0<i>Loi sur les engrais</i></a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498\">Nouveau r\u00e8glement\u00a0: Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la\u00a0<i>Loi sur les engrais</i></a></li>\n</ul>\n\n\n</div>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Frais</h2>\n</header>\n<div class=\"panel-body\">\n\n<p>Veuillez vous r\u00e9f\u00e9rer \u00e0 la <a href=\"/fra/1582641645528/1582641871296#c6\">Partie 5 de l'Avis sur les prix de l'ACIA</a> pour les prix applicables aux engrais.</p>\n\n</div>\n</section>\n\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\"><a href=\"#cs-centre\" aria-controls=\"cs-centre\" class=\"wb-lbx\" role=\"button\">D\u00e9lais de traitement</a></h2>\n</header>\n<div class=\"panel-body\">\n<p>Le d\u00e9lais de traitement varie selon le type de demande. Consultez les annexes des <a href=\"/fra/1305609994431/1307910971122#appa\" title=\"Annexe A et B\">normes de prestation de services s'appliquant aux demandes d'enregistrement des engrais et suppl\u00e9ments</a> pour plus d'informations.</p>\n\n</div>\n</section>\n\n\n</div>\n\n<div class=\"clearfix\"></div>\n\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n\n<li class=\"next\"><a href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646102406709/1646102407100\" rel=\"next\">Suivant</a></li>\n\n</ul>\n</nav>\n\n</section>\n\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}