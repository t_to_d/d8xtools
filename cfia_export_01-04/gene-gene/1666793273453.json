{
    "dcr_id": "1666793273453",
    "title": {
        "en": "Mexico \u2013 Export requirements for composite products",
        "fr": "Mexique\u00a0\u2013\u00a0Exigences d'exportation pour les produits compos\u00e9s"
    },
    "html_modified": "27-10-2022",
    "modified": "26-10-2022",
    "issued": "26-10-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/meavia_man_produits_composes_export_mexico_20221026_1666793273453_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/meavia_man_produits_composes_export_mexico_20221026_1666793273453_fra"
    },
    "parent_ia_id": "1507329098850",
    "ia_id": "1666793274078",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Mexico \u2013 Export requirements for composite products",
        "fr": "Mexique\u00a0\u2013\u00a0Exigences d'exportation pour les produits compos\u00e9s"
    },
    "label": {
        "en": "Mexico \u2013 Export requirements for composite products",
        "fr": "Mexique\u00a0\u2013\u00a0Exigences d'exportation pour les produits compos\u00e9s"
    },
    "templatetype": "content page 1 column",
    "node_id": "1666793274078",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/mexico-composite-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/mexique-produits-composes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Mexico \u2013 Export requirements for composite products",
            "fr": "Mexique\u00a0\u2013\u00a0Exigences d'exportation pour les produits compos\u00e9s"
        },
        "description": {
            "en": "Countries to which exports are currently made - Mexico",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Mexique"
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, meat inspection, products, certificate, Mexico - composite products",
            "fr": "Loi sur l&#39;inspection des viandes, R&#232;glement de 1990 sur l&#39;inspection des viandes, inspection des viandes, produits, certificat, Mexique - produits compos\u00e9s"
        },
        "dcterms.subject": {
            "en": "agri-food products,food safety,animal health,meat,poultry",
            "fr": "produit agro-alimentaire,salubrit\u00e9 des aliments,sant\u00e9 animale,viande,volaille"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-10-27",
            "fr": "2022-10-27"
        },
        "modified": {
            "en": "2022-10-26",
            "fr": "2022-10-26"
        },
        "type": {
            "en": "guide,reference material",
            "fr": "guide,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Mexico \u2013 Export requirements for composite products",
        "fr": "Mexique\u00a0\u2013\u00a0Exigences d'exportation pour les produits compos\u00e9s"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ol>\n<li><a href=\"#a1\">Eligible/ineligible product</a></li>\n<li><a href=\"#a2\">Pre-export approvals by competent authority of importing country</a></li>\n<li><a href=\"#a3\">Production controls and inspection requirements</a></li>\n<li><a href=\"#a4\">Labelling, marking and packaging requirements</a></li>\n<li><a href=\"#a5\">Required documents</a></li>\n<li><a href=\"#a6\">Other information</a></li>\n</ol>\n\n<h2 id=\"a1\">1. Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<p>Food preparations, even canned, that contain in their composition 1\u00a0or more ingredients of animal origin (meat, poultry, dairy ingredient, honey product, egg product and hydrolyzed proteins). It is recommended that you work with your importer in order to obtain more specificities regarding eligible products.</p>\n\n<h3>Ineligible</h3>\n\n<ul>\n<li>Products that do not contain ingredients of animal origin</li>\n</ul>\n\n<h2 id=\"a2\">2. Pre-export approvals by competent authority of importing country</h2>\n\n<h3>Establishments</h3>\n\n<p>Mexico does not have a list of approved establishments.</p>\n\n<h2 id=\"a3\">3. Production controls and inspection requirements</h2>\n\n<p>The inspector must verify during a preventive control inspection that the manufacturer is aware of the standards and requirements of Mexico and has a specific export procedure in place.</p>\n\n<p>The composite products (CP) must come from an establishment licensed under the Safe Food for Canadian Regulations (SFCR).</p>\n\n<p>A recent version of the Requisitos Zoosanitarios Para Importaci\u00f3n (RZPI) must be presented to the Canadian Food Inspection Agency (CFIA) prior to certification. This document can be found on the Servicio Nacional de Sanidad, Inocuidad y Calidad Agroalimentaria (SENASICA) import requirements query module: M\u00f3dulo de consulta de requisitos para la importaci\u00f3n de mercanc\u00edas zoosanitarias (Spanish only).</p>\n\n<p>The RZPI currently available for CPs is: 098-13-241-CAN-CAN (Preparaciones alimenticias)</p>\n\n<h3>Manufacturer\u2019s declaration</h3>\n\n<p>To obtain an export certificate, some information related to public health and animal health will have to be provided by the applicant. This information should be provided in the form of a manufacturer's declaration when applying for the export certificate. For more details, please communicate with your local CFIA office.</p>\n\n<ul>\n<li>For CPs containing dairy ingredients:\n<ul>\n<li>the dairy ingredients must originate from Canada or it has been legally imported from a country free from foot and mouth disease (FMD)</li>\n</ul></li>\n<li>For CPs containing processed egg:\n<ul>\n<li>provide CFIA with supporting documentation regarding heat treatment of product</li>\n<li>provide necessary supporting documentation from competent authority for imported processed eggs</li>\n</ul></li>\n<li>For CPs containing honey:\n<ul>\n<li>honey ingredient processed in establishment under official control</li>\n<li>provide necessary supporting documentation from competent authority for imported honey</li>\n</ul></li>\n<li>For CPs containing hydrolyzed protein derived from bovine species:\n<ul>\n<li>provide supporting documentation stating that hydrolyzed protein was derived exclusively from bovine hides, and that bovine originated from location free from FMD or that hydrolyzed protein was subjected to\u00a01 of the heat/chemical treatment processes as defined on the certificate/RZPI</li>\n</ul></li>\n</ul>\n\n<p><strong>Note:</strong> the inspector reserves the right to request any other information that he thinks is necessary for the final certification of the product.</p>\n\n<h2 id=\"a4\">4. Labelling, marking and packaging requirements</h2>\n\n<p>It is the exporter's responsibility to meet all the requirements for labelling, packaging and marking as per the importing country.</p>\n\n<h2 id=\"a5\">5. Documentation requirements</h2>\n\n<p>The certificate below covers only composite products containing dairy products, egg products, honey or hydrolyzed proteins (gelatin and/or collagen). If your CP contains a meat product, an additional certification could be required.</p>\n\n<p>As a manufacturer/exporter and in order to facilitate the certification process, it is important that you are familiar with the conditions stipulated in the RZPI.</p>\n\n<h3>Certificate</h3>\n\n<h4>Notes</h4>\n\n<ul>\n<li>Health certificate for export of composite products for human consumption from Canada to Mexico</li>\n</ul>\n\n<p><strong>Note:</strong> export certificates cannot be issued after the products have left Canada.</p>\n\n<h2 id=\"a6\">6. Other information</h2>\n\n<p>The operator/exporter is responsible for requesting the appropriate certificate from the CFIA inspector. Any discrepancy between a RZPI certification requirements and the approved CFIA certificate for the product intended for export should be brought to the attention of the inspector. The operator/exporter bears full responsibility to ensure that the certification provided by the CFIA is in compliance with the certification requirements appearing on the applicable RZPIs.</p>\n\n<p>Exported products transiting through a country may require transit documentation. It is the responsibility of the exporter to ensure that the shipment will be accompanied by all necessary certificates. Please work closely with your importer.</p>\n \n<p>Samples (personal or commercial) may be subject to the same requirements as a regular shipment.</p>\n\n<p>More information on export requirements to Mexico can be obtained from the\u00a0<a href=\"https://sistemasssl.senasica.gob.mx/mcrz/moduloConsulta.jsf\">M\u00f3dulo de consulta de requisitos para la importaci\u00f3n de mercanc\u00edas zoosanitarias\u00a0(Spanish only)</a>\u00a0administered by\u00a0SENASICA.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ol>\n<li><a href=\"#a1\">Produits admissibles/non admissibles</a></li>\n<li><a href=\"#a2\">Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</a></li>\n<li><a href=\"#a3\">Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</a></li>\n<li><a href=\"#a4\">Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</a></li>\n<li><a href=\"#a5\">Documents requis</a></li>\n<li><a href=\"#a6\">Autres renseignements</a></li>\n</ol>\n\n<h2 id=\"a1\">1. Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n \n<p>Les pr\u00e9parations alimentaires, m\u00eame en conserves pouvant contenir dans leur composition un ou plusieurs ingr\u00e9dients d'origine animale (viande et volaille, ingr\u00e9dient laitier, produit de miel, ovoproduits et prot\u00e9ines hydrolys\u00e9es. Il est recommand\u00e9 de travailler avec son importateur afin d'obtenir plus de sp\u00e9cificit\u00e9s concernant les produits admissibles.</p>\n\n<h3>Non admissibles</h3>\n\n<ul>\n<li>Produits qui ne contiennent pas d'ingr\u00e9dients d'origine animale.</li>\n</ul>\n\n<h2 id=\"a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<h3>\u00c9tablissements</h3>\n\n<p>Le Mexique n'a pas de liste d'\u00e9tablissements approuv\u00e9s.</p>\n\n<h2 id=\"a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<p>Les produits compos\u00e9s (PC) doivent provenir d'un \u00e9tablissement d\u00e9tenteur d'une licence en vertu <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>\u00a0(RSAC).</p>\n\n<p>L'inspecteur doit v\u00e9rifier au cours d'une inspection de contr\u00f4le pr\u00e9ventif que le fabricant est au courant des normes et exigences du Mexique et qu'une proc\u00e9dure d'exportation sp\u00e9cifique est en place.</p>\n\n<p>Une version r\u00e9cente de la\u00a0Requisitos Zoosanitarios Para Importaci\u00f3n\u00a0(RZPI) doit \u00eatre pr\u00e9sent\u00e9e \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) avant la certification. Ce document peut \u00eatre consult\u00e9 sur le module de requ\u00eate sur les exigences d'importation du\u00a0Servicio Nacional de Sanidad, Inocuidad y Calidad Agroalimentaria\u00a0(SENASICA)\u00a0:\u00a0<a href=\"https://sistemasssl.senasica.gob.mx/mcrz/moduloConsulta.jsf\">M\u00f3dulo de consulta de requisitos para la importaci\u00f3n de mercanc\u00edas zoosanitarias\u00a0(espagnol seulement)</a>.</p>\n\n<p>La RZPI actuellement disponible pour les PC est\u00a0: 098-13-241-CAN-CAN (Preparaciones alimenticias)</p>\n\n<h3>D\u00e9claration du fabricant</h3>\n\n<p>Pour obtenir \u00a0un certificat d'exportation, certaines informations en rapport avec la sant\u00e9 publique et la sant\u00e9 animale devront \u00eatre fournies par le demandeur. Ces informations devront \u00eatre fournies sous la forme d'une d\u00e9claration du fabricant lors de la demande du certificat d'exportation. Pour plus de d\u00e9tails, veuillez communiquer avec votre bureau local de l'ACIA.</p>\n\n<ul>\n<li>Pour les PC contenants des ingr\u00e9dients laitiers\u00a0:\n<ul>\n<li>l'ingr\u00e9dient laitier doit provenir du Canada ou a \u00e9t\u00e9 l\u00e9galement import\u00e9 d'un pays indemne de Fi\u00e8vre aphteuse.</li>\n</ul></li>\n<li>Pour les PC contenant des \u0153ufs transform\u00e9s\u00a0:\n<ul>\n<li>fournir \u00e0 l'ACIA des documents \u00e0 l'appui concernant le traitement thermique du produit.</li>\n<li>fournir les documents justificatifs n\u00e9cessaires de l'autorit\u00e9 comp\u00e9tente pour les \u0153ufs transform\u00e9s import\u00e9s.</li>\n</ul></li>\n<li>Pour les PC contenant des produits de miel\u00a0:\n<ul>\n<li>ingr\u00e9dient du miel transform\u00e9 en \u00e9tablissement sous contr\u00f4le officiel.</li>\n<li>fournir les documents justificatifs n\u00e9cessaires de l'autorit\u00e9 comp\u00e9tente pour le miel import\u00e9.</li>\n</ul></li>\n<li>Pour les PC contenant les prot\u00e9ines hydrolys\u00e9es\u00a0d\u00e9riv\u00e9s de l'esp\u00e8ce bovine:\n<ul>\n<li>fournir des documents \u00e0 l'appui attestant que les prot\u00e9ines hydrolys\u00e9es proviennent exclusivement de peaux de bovins et que les bovins proviennent d'un endroit indemne de fi\u00e8vre aphteuse ou que les prot\u00e9ines hydrolys\u00e9es ont \u00e9t\u00e9 soumises \u00e0 l'un des proc\u00e9d\u00e9s de traitement thermique / chimique d\u00e9finis dans le certificat/RZPI. </li>\n</ul></li>\n</ul>\n\n<p><strong>Remarque\u00a0:</strong> l'inspecteur se r\u00e9serve le droit de demander toute autre formation qu'il pense \u00eatre n\u00e9cessaire \u00e0 la certification finale.</p>\n\n<h2 id=\"a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</h2>\n\n<p>Il incombe \u00e0 l'exportateur de satisfaire \u00e0 toutes les exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage du pays importateur.</p>\n\n<h2 id=\"a5\">5. Documents requis</h2>\n\n<p>Le certificat ci-dessous couvre que les produits compos\u00e9s contenant un produit laitier, des ovoproduits, un produits de miel ou des prot\u00e9ines hydrolys\u00e9e (g\u00e9latine et/ou collag\u00e8ne). Si votre PC contient de la viande, une certification additionnelle sera n\u00e9cessaire.</p>\n\n<p>En tant que fabricant/exportateur et afin de faciliter le processus de certification, il important que vous soyez familier avec les conditions stipul\u00e9es dans la RZPI.</p>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Certificat sanitaire pour l'exportation de produits compos\u00e9s pour consommation humaine du Canada vers le Mexique. </li>\n</ul>\n\n<p><strong>Remarque\u00a0:</strong> les certificats d'exportation ne peuvent pas \u00eatre \u00e9mis pour des produits qui ont quitt\u00e9 le Canada.</p>\n\n\n<h2 id=\"a6\">6. Autres renseignements</h2>\n\n<p>Il revient \u00e0 l'exploitant/exportateur de demander le certificat appropri\u00e9 \u00e0 l'inspecteur de l'ACIA. Toute divergence entre les exigences d'une RZPI et le certificat concernant le produit destin\u00e9 \u00e0 l'exportation doit \u00eatre port\u00e9e \u00e0 l'attention de l'inspecteur. Il incombe \u00e0 l'exploitant/exportateur et \u00e0 lui seul de s'assurer que la certification fournie par l'ACIA est conforme aux exigences de certification apparaissant sur la RZPI.</p>\n\n<p>Des produits export\u00e9s transitant par un pays pourraient n\u00e9cessiter aussi des documents de transit. C'est la responsabilit\u00e9 de l'exportateur de s'assurer que son exp\u00e9dition sera accompagn\u00e9e de tous les certificats n\u00e9cessaires. Veuillez travailler en \u00e9troite collaboration avec votre importateur.</p>\n \n<p>Les \u00e9chantillons (personnels ou commerciaux) pourraient \u00eatre soumis aux m\u00eame exigences qu'une exp\u00e9dition r\u00e9guli\u00e8re.</p>\n \n<p>Pour plus d'informations sur les conditions d'exportation vers le Mexique, veuillez consulter le\u00a0<a href=\"https://sistemasssl.senasica.gob.mx/mcrz/moduloConsulta.jsf\">M\u00f3dulo de consulta de requisitos para la importaci\u00f3n de mercanc\u00edas zoosanitarias\u00a0(espagnol seulement)</a>\u00a0administr\u00e9 par le\u00a0SENASICA.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}