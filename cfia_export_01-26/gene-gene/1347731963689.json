{
    "dcr_id": "1347731963689",
    "title": {
        "en": "Pre-clearance procedure for seed importers",
        "fr": "Proc\u00e9dure de pr\u00e9d\u00e9douanement visant les importateurs de semences"
    },
    "html_modified": "2024-01-26 2:21:14 PM",
    "modified": "2019-04-01",
    "issued": "2012-09-15 13:59:31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/seed_imports_preclearance_1347731963689_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/seed_imports_preclearance_1347731963689_fra"
    },
    "ia_id": "1347732129072",
    "parent_ia_id": "1299174773794",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Imports",
        "fr": "Importations"
    },
    "commodity": {
        "en": "Seeds",
        "fr": "Semences"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299174773794",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pre-clearance procedure for seed importers",
        "fr": "Proc\u00e9dure de pr\u00e9d\u00e9douanement visant les importateurs de semences"
    },
    "label": {
        "en": "Pre-clearance procedure for seed importers",
        "fr": "Proc\u00e9dure de pr\u00e9d\u00e9douanement visant les importateurs de semences"
    },
    "templatetype": "content page 1 column",
    "node_id": "1347732129072",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299174165725",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-imports/pre-clearance/",
        "fr": "/protection-des-vegetaux/semences/importation-de-semences/prededouanement/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pre-clearance procedure for seed importers",
            "fr": "Proc\u00e9dure de pr\u00e9d\u00e9douanement visant les importateurs de semences"
        },
        "description": {
            "en": "Seed importers: pre-clearance procedure",
            "fr": "Importateurs de semences : proc\u00e9dure de pr\u00e9d\u00e9douanement"
        },
        "keywords": {
            "en": "Seeds Act, Seeds Regulations, seed, imports, authorized importers, pre-clearance, import conformity assessment",
            "fr": "Loi sur les semences, R\u00e8glement sur les semences, semences, importation, importateurs autoris\u00e9s, pr\u00e9d\u00e9douanement, conformit\u00e9 des importations"
        },
        "dcterms.subject": {
            "en": "crops,imports,inspection,plants",
            "fr": "cultures,importation,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-09-15 13:59:31",
            "fr": "2012-09-15 13:59:31"
        },
        "modified": {
            "en": "2019-04-01",
            "fr": "2019-04-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pre-clearance procedure for seed importers",
        "fr": "Proc\u00e9dure de pr\u00e9d\u00e9douanement visant les importateurs de semences"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The Canadian Food Inspection Agency (CFIA) has implemented a pre-clearance procedure for seed importers.</p>\n<p>All imported seed that requires an import conformity assessment (ICA) can be pre-cleared. The CFIA can complete the ICA process and issue the Notice of Import Conformity in advance. When the shipment arrives at  the border, the associated documentation will be verified and the seed can move directly to destination without further delay.</p>\n<p>Prior to this procedure, only accredited industry personnel under the CFIA's authorized  importer program could get seed pre-cleared prior to import. Seed from  other importers had to be held separate and intact in the original packages after it was imported while the CFIA completed ICA process.</p>\n<h2>What is an import conformity assessment (ICA)?</h2>\n<p>The CFIA's ICA process includes a review of all mandatory documents (specifically, the import declaration and the seed analysis certificate) to confirm that a seed lot meets the minimum requirements of the <i>Seeds Act</i> and the <i>Seeds Regulations</i>. A Notice of Import Conformity is then issued if the seed meets Canadian import requirements. Imported seed must remain separate and intact in the original packages during the ICA process. After the Notice of Import Conformity is issued, the seed can be planted, repackaged, or sold in Canada.</p>\n<h2>How do I obtain a pre-clearance?</h2>\n<p>Email the information required under subsections 40(1), 40(2), and 40(3) of the <a href=\"/english/reg/jredirect2.shtml?seesemr\"><i>Seeds Regulations</i></a> to the CFIA's ICA office (<a href=\"mailto:ICA-VCI@inspection.gc.ca\">ICA-VCI@inspection.gc.ca</a>) in Ottawa.</p>\n<p>Clearly identify that the information is for pre-clearance of seed and provide the customs transaction number from the customs broker, if available.</p>\n<p>A Notice of Import Conformity will be issued to you by the ICA office when the ICA is complete. Retain the original Notice of Import Conformity and provide a copy to accompany the importation.</p>\n<p>The import declaration should state the following:</p>\n<ul>\n<li>the name of the species,</li>\n<li>the seed lot designations,</li>\n<li>the quantities of each type of seed,</li>\n<li>variety names (where applicable),</li>\n<li>the name and address of the importer,</li>\n<li>the name and address of the exporter,</li>\n<li>the country in which the seed was grown,</li>\n<li>the intended purpose of the seed import, and</li>\n<li>a seed analysis certificate indicating that the seed has been tested pursuant to paragraph 11(1)(b) of the <i>Seeds Regulations</i>.</li>\n</ul>\n<h2>Will applying for pre-clearance mean that a shipment has priority processing?</h2>\n<p>The pre-clearance option provides the importer with an opportunity to complete paperwork before seed is imported. Although it does not change priority of processing, it provides greater flexibility to the importer to complete his paperwork ahead of time to avoid delays. As the seed will be pre-cleared, it will not need to be kept separate and intact once it enters the country and can be released at the border to be planted or sold faster.</p>\n<h2>How long will it take to issue a Notice of Import Conformity?</h2>\n<p>All ICAs are completed on a first-come, first-served basis and are only started once all required documentation is received. A Notice of Import Conformity will be issued if the seed meets Canadian import requirements.</p>\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">The following table outlines the estimated turnaround time for submissions received throughout the year.</caption>\n<thead>\n<tr class=\"active\">\n<th>Time period</th> <th>Estimated turnaround time</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>July 16 to December 31</td>\n<td>2 weeks</td>\n</tr>\n<tr>\n<td>January 1 to February 28</td>\n<td>3 weeks</td>\n</tr>\n<tr>\n<td>March 1 to March 31</td>\n<td>6 weeks</td>\n</tr>\n<tr>\n<td>April to July 15</td>\n<td>8 weeks</td>\n</tr>\n</tbody>\n</table>\n<h2>What is the authorized importer program?</h2>\n<p>Under the CFIA's authorized importer program, accredited industry personnel are delegated the authority to perform ICAs and issue Notices of Import Conformity before seed is imported into Canada. Companies that frequently import seed are encouraged to consider becoming <a href=\"/plant-health/seeds/seed-imports/eng/1299174165725/1299174773794\">authorized importers</a>.</p>\n<h2>Will the CFIA continue to monitor imported seed lots?</h2>\n<p>Yes, the Government of Canada is committed to protecting Canada's  plant resource base from diseases and pests, including prohibited seeds  from noxious weeds and invasive plants. The CFIA will continue to monitor imported seed lots through regular sampling and testing at the destination and in the market place.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'Agence canadienne d'inspection des aliments (ACIA) a adopt\u00e9 une proc\u00e9dure de pr\u00e9d\u00e9douanement \u00e0 l'intention des importateurs de semences.</p>\n<p>Toutes les semences import\u00e9es qui font l'objet d'une \u00e9valuation de la conformit\u00e9 des importations (ECI) pourront \u00eatre pr\u00e9d\u00e9douan\u00e9es. L'ACIA peut effectuer l'ECI et d\u00e9livrer un avis de conformit\u00e9 d'importation au pr\u00e9alable. Lorsque la cargaison arrive \u00e0 la fronti\u00e8re, la documentation connexe est examin\u00e9e, apr\u00e8s quoi les semences peuvent \u00eatre livr\u00e9es \u00e0 destination.</p>\n<p>Avant l'adoption de la proc\u00e9dure, seul le personnel de l'industrie agr\u00e9\u00e9 en vertu du programme des importateurs autoris\u00e9s de l'ACIA pouvait autoriser le pr\u00e9d\u00e9douanement des semences avant l'importation. Les semences des autres importateurs devaient \u00eatre gard\u00e9es \u00e0  l'\u00e9cart et intactes dans les emballages d'origine apr\u00e8s leur importation, pendant que l'ACIA proc\u00e9dait \u00e0 l'ECI.</p>\n<h2>Qu'est ce q'une \u00e9valuation de la conformit\u00e9 des importations (ECI)?</h2>\n<p>Le processus d'ECI de l'ACIA comprend un examen de tous les documents exig\u00e9s (la d\u00e9claration d'importation et le certificat d'analyse des semences) pour confirmer qu'un lot de semences satisfait les exigences minimales de la <i>Loi sur les semences</i> et du <i>R\u00e8glement sur les semences</i>. Par la suite, un avis de conformit\u00e9 d'importation est d\u00e9livr\u00e9 si les semences satisfont les exigences canadiennes en mati\u00e8re d'importation. Les semences import\u00e9es doivent  \u00eatre gard\u00e9es \u00e0 part et intactes dans leur emballage d'origine durant le processus d'ECI. Une fois l'avis de conformit\u00e9 d'importation d\u00e9livr\u00e9, les semences peuvent \u00eatre plant\u00e9es, remball\u00e9es ou vendues au Canada.</p>\n<h2>Comment puis-je obtenir un pr\u00e9d\u00e9douanement?</h2>\n<p>Il faut envoyer les renseignements exig\u00e9s en vertu des paragraphes 40(1), 40(2) et 40(3) du <a href=\"/francais/reg/jredirect2.shtml?seesemr\"><i>R\u00e8glement sur les semences</i></a> \u00a0par courrier \u00e9lectronique au bureau de l'ACIA responsable des ECI (<a href=\"mailto:ICA-VCI@inspection.gc.ca\">ICA-VCI@inspection.gc.ca</a>) \u00e0 Ottawa.</p>\n<p>Il faut indiquer clairement que l'information concerne le pr\u00e9d\u00e9douanement des semences et fournir le num\u00e9ro de transaction des douanes obtenu du courtier en douanes, le cas \u00e9ch\u00e9ant.</p>\n<p>Le bureau responsable des ECI vous enverra un avis de conformit\u00e9 d'importation une fois l'ECI termin\u00e9e. Vous devez conserver l'original de l'avis de conformit\u00e9 d'importation et en joindre une copie aux produits import\u00e9s.</p>\n<p>La d\u00e9claration d'importation doit fournir l'information suivante\u00a0:</p>\n<ul>\n<li>le nom de l'esp\u00e8ce;</li>\n<li>la d\u00e9signation du lot de la semence;</li>\n<li>les quantit\u00e9s de chaque type de semence;</li>\n<li>le nom des vari\u00e9t\u00e9s (le cas \u00e9ch\u00e9ant);</li>\n<li>le nom et l'adresse de l'importateur;</li>\n<li>le nom et l'adresse de l'exportateur;</li>\n<li>le pays o\u00f9 les semences ont \u00e9t\u00e9 cultiv\u00e9es;</li>\n<li>la raison de l'importation des semences;</li>\n<li>un certificat d'analyse des semences indiquant qu'elles ont \u00e9t\u00e9 analys\u00e9es conform\u00e9ment \u00e0 l'alin\u00e9a 11(1)b) du <i>R\u00e8glement sur les semences</i>.</li>\n</ul>\n<h2>Est-ce que le fait de pr\u00e9senter une demande de pr\u00e9d\u00e9douanement signifie qu'une cargaison sera trait\u00e9e en priorit\u00e9?</h2>\n<p>La possibilit\u00e9 d'obtenir un pr\u00e9d\u00e9douanement permet \u00e0 l'importateur de remplir les documents avant que les semences ne soient import\u00e9es. Bien que le pr\u00e9d\u00e9douanement ne garantisse pas que l'importation sera trait\u00e9e en priorit\u00e9, il offre  \u00e0 l'importateur une plus grande souplesse, ce qui lui permet de remplir les documents requis \u00e0 l'avance afin d'\u00e9viter les retards. \u00c9tant donn\u00e9 que les semences feront l'objet d'un pr\u00e9d\u00e9douanement, il ne sera pas n\u00e9cessaire de les conserver s\u00e9par\u00e9ment et intactes apr\u00e8s leur entr\u00e9e au pays. On pourra accorder la mainlev\u00e9e des semences \u00e0 la fronti\u00e8re afin qu'elles soient plant\u00e9es ou vendues plus rapidement.</p>\n<h2>Dans le cadre de cette nouvelle proc\u00e9dure, combien de temps faudra-t-il avant qu'un avis de conformit\u00e9 d'importation ne soit d\u00e9livr\u00e9?</h2>\n<p>Toutes les ECI sont effectu\u00e9es selon le principe du premier arriv\u00e9, premier servi, et le processus ne s'amorce qu'une fois tous les documents exig\u00e9s re\u00e7us. Un avis de conformit\u00e9 d'importation n'est d\u00e9livr\u00e9 que si les semences satisfont les exigences canadiennes en mati\u00e8re d'importation.</p>\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Le tableau ci-apr\u00e8s pr\u00e9cise les d\u00e9lais approximatifs de traitement des demandes re\u00e7ues \u00e0 diff\u00e9rentes p\u00e9riodes de l'ann\u00e9e.</caption>\n<thead>\n<tr class=\"active\">\n<th>P\u00e9riode</th> <th>D\u00e9lai approximatif</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>16 juillet \u00e0 31 d\u00e9cembre</td>\n<td>2 semaines</td>\n</tr>\n<tr>\n<td>1er janvier \u00e0 28 f\u00e9vrier</td>\n<td>3 semaines</td>\n</tr>\n<tr>\n<td>1er mars \u00e0 31 mars</td>\n<td>6 semaines</td>\n</tr>\n<tr>\n<td>1er avril1 \u00e0 15 juillet</td>\n<td>8 semaines</td>\n</tr>\n</tbody>\n</table>\n<h2>Qu'est-ce que le programme des importateurs autoris\u00e9s?</h2>\n<p>En vertu du programme des importateurs autoris\u00e9s de l'ACIA, on  d\u00e9l\u00e8gue au personnel agr\u00e9\u00e9 de l'industrie le pouvoir de proc\u00e9der \u00e0 une ECI et de d\u00e9livrer des avis de conformit\u00e9 d'importation avant l'importation des semences au Canada. On encourage les entreprises qui importent souvent des semences \u00e0 envisager de devenir des <a href=\"/protection-des-vegetaux/semences/importation-de-semences/fra/1299174165725/1299174773794\">importateurs autoris\u00e9s</a>.</p>\n<h2>Est-ce que l'ACIA continuera de surveiller les lots de semences import\u00e9es?</h2>\n<p>Oui, le gouvernement du Canada est d\u00e9termin\u00e9 \u00e0 prot\u00e9ger les ressources v\u00e9g\u00e9tales du Canada contre les maladies et les ravageurs, notamment les semences interdites provenant de mauvaises herbes nuisibles et les plantes envahissantes. L'ACIA continuera de surveiller les lots de semences import\u00e9es en proc\u00e9dant \u00e0 des \u00e9chantillonnages et \u00e0 des analyses \u00e0 destination et sur le march\u00e9.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}