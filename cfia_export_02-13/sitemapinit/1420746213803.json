{
    "dcr_id": "1420746212959",
    "title": {
        "en": "Verticillium stripe \u2013 <span lang=\"la\">Verticillium longisporum</span>",
        "fr": "Rayure verticillienne du canola ou du colza \u2013 <span lang=\"la\">Verticillium longisporum</span>"
    },
    "html_modified": "2024-02-13 9:50:34 AM",
    "modified": "2018-09-04",
    "issued": "2015-01-08",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_vertlong_index_1420746212959_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_vertlong_index_1420746212959_fra"
    },
    "ia_id": "1420746213803",
    "parent_ia_id": "1322807340310",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1322807340310",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Verticillium stripe \u2013 <span lang=\"la\">Verticillium longisporum</span>",
        "fr": "Rayure verticillienne du canola ou du colza \u2013 <span lang=\"la\">Verticillium longisporum</span>"
    },
    "label": {
        "en": "Verticillium stripe \u2013 <span lang=\"la\">Verticillium longisporum</span>",
        "fr": "Rayure verticillienne du canola ou du colza \u2013 <span lang=\"la\">Verticillium longisporum</span>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1420746213803",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1322807235798",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/verticillium-stripe/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/rayure-verticillienne-du-canola-ou-du-colza/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Verticillium stripe \u2013 Verticillium longisporum",
            "fr": "Rayure verticillienne du canola ou du colza \u2013 Verticillium longisporum"
        },
        "description": {
            "en": "Verticillium longisporum is a plant pathogen that can cause advanced ageing, early plant death and decreased yield. It infects a broad range of crops, but the most severe impact is on canola.",
            "fr": "Verticillium longisporum est un phytopathog\u00e8ne qui peut causer le fl\u00e9trissement de la plante, ainsi que son vieillissement pr\u00e9coce ou sa mort pr\u00e9matur\u00e9e, et entra\u00eener une diminution du rendement des r\u00e9coltes."
        },
        "keywords": {
            "en": "plant, plant health, plant pathogen, Verticillium longisporum, Manitoba, pests",
            "fr": "plantes, protection des v\u00e9g\u00e9taux, phytopathog\u00e8ne, Verticillium longisporum, Manitoba, organisme nuisible"
        },
        "dcterms.subject": {
            "en": "insects,inspection,plant diseases,plants",
            "fr": "insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-01-08",
            "fr": "2015-01-08"
        },
        "modified": {
            "en": "2018-09-04",
            "fr": "2018-09-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Verticillium stripe \u2013 Verticillium longisporum",
        "fr": "Rayure verticillienne du canola ou du colza \u2013 Verticillium longisporum"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-left mrgn-rght-md\">\n<p><img class=\"img-responsive\" alt=\"Verticillium longisporum\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_vertlong_image1_1420748540975_eng.jpg\"></p>\n</div>\n\n<p>The Canadian Food Inspection Agency (CFIA) confirmed the presence of <span lang=\"la\">Verticillium longisporum</span> (Verticillium stripe) in a canola field at a single location in Manitoba and issued a NAPPO Pest Alert on 01/15/2015.  Prior to this detection, <span lang=\"la\">V. longisporum</span> had not been reported in Canada.</p>\n\n<p>Consistent with the International Plant Protection Convention (IPPC) and its international standards for phytosanitary measures (ISPMs), the CFIA implemented provisional measures to contain the spread of the pest and completed a pest risk assessment while the regulatory status of <span lang=\"la\">V. longisporum</span> was being determined.</p> \n \n<p>To make an informed regulatory decision, the CFIA conducted a national survey of canola fields between August and November of 2015.  This survey confirmed the presence of <span lang=\"la\">V. longisporum</span> in six provinces: British Columbia, Alberta, Saskatchewan, Manitoba, Ontario, and Quebec.</p> \n\n<p>Based on the results of the survey and the pest risk assessment, a risk management decision (RMD) document was prepared recommending that <span lang=\"la\">V. longisporum</span> not be regulated as a quarantine pest in Canada.  The RMD was circulated for review and comments by stakeholders between March 15 and May 15, 2017 with stakeholders broadly supporting the CFIA's recommendation.</p> \n \n<p>The CFIA is formally notifying that <span lang=\"la\">V. longisporum</span> is widely distributed in Canada.  The regulation of <span lang=\"la\">V. longisporum</span> is not warranted, nor is it cost-justifiable to implement official measures to control further introductions or domestic spread of this pest.</p> \n\n<h2>What information is available</h2>\n<ul>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/verticillium-stripe/questions-and-answers/eng/1420821459459/1420821460224\">Questions and Answers - <span lang=\"la\">Verticillium longisporum</span></a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/verticillium-stripe/notice-to-industry/eng/1458134997626/1458135081349\">Notice to industry: Results of national survey for <span lang=\"la\">Verticillium longisporum</span></a></li>\n<li><a href=\"/plant-health/invasive-species/national-voluntary-farm-level-biosecurity-standard/eng/1354649087792/1355168633095\">Crop biosecurity information</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/pest-risk-management/rmd-17-01/eng/1487004855251/1487004951480\">RMD-17-01: Pest Risk Management Document \u2013 <span lang=\"la\">Verticillium longisporum</span> (Verticillium stripe)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-left mrgn-rght-md\">\n<p><img class=\"img-responsive\" alt=\"Verticillium longisporum\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_vertlong_image1_1420748540975_fra.jpg\"></p>\n</div>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) a confirm\u00e9 la pr\u00e9sence de <span lang=\"la\">Verticillium longisporum</span> (rayure verticillienne) dans un champ de canola \u00e0 un seul endroit au Manitoba et a \u00e9mis une alerte phytosanitaire de l'Organisation nord-am\u00e9ricaine pour la protection des plantes (NAPPO) le 15 janvier 2015. Avant cette d\u00e9tection, la pr\u00e9sence de <span lang=\"la\">V. longisporum</span> n'avait jamais \u00e9t\u00e9 signal\u00e9e auparavant au Canada.</p>\n\n<p>Conform\u00e9ment \u00e0 la Convention internationale pour la protection des v\u00e9g\u00e9taux et \u00e0 ses Normes internationales pour les mesures phytosanitaires, l'ACIA a mis en \u0153uvre des mesures provisoires afin de limiter la propagation du phytoravageur et a r\u00e9alis\u00e9 une \u00e9valuation des risques li\u00e9s \u00e0 l'organisme pendant qu'on d\u00e9terminait le statut r\u00e9glementaire de <span lang=\"la\">V. longisporum</span>.</p>\n\n<p>Dans le but de prendre des d\u00e9cisions \u00e9clair\u00e9es en mati\u00e8re de r\u00e9glementation, l'ACIA a men\u00e9 une enqu\u00eate nationale sur les champs de canola entre ao\u00fbt et novembre 2015. Cette enqu\u00eate a permis de confirmer la pr\u00e9sence de <span lang=\"la\">V. longisporum</span> dans six provinces\u00a0: Colombie-Britannique, Alberta, Saskatchewan, Manitoba, Ontario et Qu\u00e9bec.</p> \n\n<p>En fonction des r\u00e9sultats de l'enqu\u00eate et de l'\u00e9valuation des risques phytosanitaires, on a pr\u00e9par\u00e9 un document de d\u00e9cision en mati\u00e8re de gestion des risques dans lequel on recommandait que <span lang=\"la\">V. longisporum</span> ne soit pas r\u00e9glement\u00e9 en tant qu'organisme de quarantaine au Canada. Le document de d\u00e9cision en mati\u00e8re de gestion des risques a \u00e9t\u00e9 distribu\u00e9 aux intervenants aux fins d'examen et de r\u00e9troaction entre le 15 mars et le 15 mai 2017; un grand nombre d'intervenants appuyaient la recommandation de l'ACIA.</p>\n\n<p>L'ACIA avise officiellement que <span lang=\"la\">V. longisporum</span> est largement pr\u00e9sent au Canada. Sa r\u00e9glementation n'est pas justifi\u00e9e pas plus qu'il n'est justifi\u00e9 sur le plan \u00e9conomique de mettre en \u0153uvre des mesures de lutte contre les nouvelles introductions ou la propagation au pays de ce phytoravageur.</p>\n\n<h2>Quels sont les renseignements disponibles</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/rayure-verticillienne-du-canola-ou-du-colza/questions-et-reponses/fra/1420821459459/1420821460224\">Questions et r\u00e9ponses - <span lang=\"la\">Verticillium longisporum</span></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/rayure-verticillienne-du-canola-ou-du-colza/avis-a-l-industrie/fra/1458134997626/1458135081349\">Avis \u00e0 l'industrie - R\u00e9sultats de l'enqu\u00eate nationale sur <span lang=\"la\">Verticillium longisporum</span></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/norme-nationale-volontaire-de-biosecurite-a-la-fer/fra/1354649087792/1355168633095\">Renseignements sur la bios\u00e9curit\u00e9 des cultures</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/gestion-des-risques-phytosanitaire/dgr-abbr-17-01/fra/1487004855251/1487004951480\">DGR-17-01\u00a0: Document de gestion des risques phytosanitaires \u2013 <span lang=\"la\">Verticillium longisporum</span> (Rayure verticillienne du canola ou du colza)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}