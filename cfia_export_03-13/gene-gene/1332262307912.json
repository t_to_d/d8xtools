{
    "dcr_id": "1332262307912",
    "title": {
        "en": "Aquatic animal export: certification requirements for Israel",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour Isra\u00ebl"
    },
    "html_modified": "2024-03-12 3:55:41 PM",
    "modified": "2020-01-07",
    "issued": "2012-04-02 14:38:27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_isr_1332262307912_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_isr_1332262307912_fra"
    },
    "ia_id": "1332262375470",
    "parent_ia_id": "1331743129372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1331743129372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal export: certification requirements for Israel",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour Isra\u00ebl"
    },
    "label": {
        "en": "Aquatic animal export: certification requirements for Israel",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour Isra\u00ebl"
    },
    "templatetype": "content page 1 column",
    "node_id": "1332262375470",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331740343115",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/israel/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/israel/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal export: certification requirements for Israel",
            "fr": "Exportation d'animaux aquatiques : exigences de certification pour Isra\u00ebl"
        },
        "description": {
            "en": "This page describes aquatic animal health export certification requirements for Israel.",
            "fr": "Cette page d\u00e9crit les exigences de certification d'exportation de sant\u00e9 des animaux aquatiques pour l'Isra\u00ebl"
        },
        "keywords": {
            "en": "aquatic animals, finfish, molluscs, crustacean, exports, human consumption, certification requirements, aquatic animal health, seafood products, NAAHP, Israel",
            "fr": "animaux aquatiques, poisson \u00e0 nageoires, mollusques, crustac\u00e9s, exportation,  consommation humaine,  exigences de certification, sant\u00e9 des animaux aquatiques, produits du poisson et des produits de la mer, PNSAA, Isra\u00ebl"
        },
        "dcterms.subject": {
            "en": "animal,aquatic animals,exports,seafood,animal diseases,animal health",
            "fr": "animal,animal aquatique,exportation,fruits de mer,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-04-02 14:38:27",
            "fr": "2012-04-02 14:38:27"
        },
        "modified": {
            "en": "2020-01-07",
            "fr": "2020-01-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal export: certification requirements for Israel",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour Isra\u00ebl"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2>The following certificates are available:</h2>\n<ul>\n<li>Aquatic Animal Health Certificate for the Export of Processed Fish Protein for Agricultural Fertilizer from Canada to Israel (AQAH-1023)</li>\n<li>Aquatic Animal Health Certificate for Export of Fish and Fishery Products to Israel (AQAH-1070)</li>\n</ul>\n<h2>Important Notes:</h2>\n<ul>\n<li>Certificate (AQAH-1023) only applies to the export of processed fish protein. </li>\n<li>This commodity requires sampling. </li>\n<li>This certificate (AQAH-1070) applies to Fish and Seafood for human consumption.</li>\n\n<li>All fish and fishery products to be exported from Canada to Israel must come from registered establishments or registered processing vessels approved for export to the European Union (EU).</li>\n\n<li>There may be existing certificates or additional information from the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/israel-fish-and-seafood/eng/1304267843365/1304268025602\" title=\"Israel - Certification Requirements\">Fish and Seafood</a> program. However, for the specific commodities above, an aquatic animal health certificate is required. </li>\n<li>To request an export certificate outlined above or if you require any other information on aquatic animal health export certification, please contact your <a href=\"/eng/1300462382369/1365216058692\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a>. </li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2>Les certificats suivants sont disponibles\u00a0:</h2>\n\n<ul>\n\n<li>Certificat d'hygi\u00e8ne des animaux aquatiques pour l'exportation de prot\u00e9ine de poisson trait\u00e9e pour l'engrais d'agriculture de Canada \u00e0 l'Isra\u00ebl (AQAH-1023)</li>\n\n<li>Certificat sanitaire pour l'exportation de poissons et produits de la p\u00eache a l'Isra\u00ebl (AQAH-1070)</li>\n\n</ul>\n\n<h2>Remarques importantes\u00a0:</h2>\n\n<ul>\n\n<li>Le certificat AQAH-1023 vise seulement les exportations de prot\u00e9ine de poisson trait\u00e9e.</li>\n\n<li>Ce produit doit \u00eatre soumis \u00e0 un \u00e9chantillonnage.</li>\n\n<li>Ce certificat (AQAH-1070) s'applique aux poissons et fruits de mer destin\u00e9s \u00e0 la consommation humaine.</li>\n\n<li>Les poissons et produits de poisson destin\u00e9s \u00e0 l'exportation du Canada vers Isra\u00ebl doivent provenir d'\u00e9tablissements agr\u00e9\u00e9s ou de navires-usine agr\u00e9\u00e9s autoris\u00e9s \u00e0 exporter vers l'Union Europ\u00e9enne.</li>\n\n<li>Il est possible que d'autres certificats ou des renseignements suppl\u00e9mentaires soient disponibles par le biais du programme de <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/israel-poisson-et-produits-de-la-mer/fra/1304267843365/1304268025602\" title=\"Isra\u00ebl - Exigences de certification\">Poisson et produits de la mer</a>. Cependant, en ce qui a trait aux produits susmentionn\u00e9s, un certificat d'hygi\u00e8ne des animaux aquatiques est requis.</li>\n\n<li>Pour faire une demande d'un certificat d'exportation indiqu\u00e9 ci-dessus ou pour toute autre demande de renseignements concernant la certification sanitaire des animaux aquatiques destin\u00e9s \u00e0 l'exportation, veuillez communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> de votre Centre op\u00e9rationnel.</li>\n\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}