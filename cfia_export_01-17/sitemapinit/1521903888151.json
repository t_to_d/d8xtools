{
    "dcr_id": "1521903887306",
    "title": {
        "en": "Managing cases of non-compliance of unauthorized plant products derived through biotechnology",
        "fr": "Gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie"
    },
    "html_modified": "26-3-2018",
    "modified": "14-6-2018",
    "issued": "24-3-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pnts_noncompliance_1521903887306_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pnts_noncompliance_1521903887306_fra"
    },
    "parent_ia_id": "1300208874046",
    "ia_id": "1521903888151",
    "parent_node_id": "1300208874046",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Managing cases of non-compliance of unauthorized plant products derived through biotechnology",
        "fr": "Gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie"
    },
    "label": {
        "en": "Managing cases of non-compliance of unauthorized plant products derived through biotechnology",
        "fr": "Gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie"
    },
    "templatetype": "content page 1 column",
    "node_id": "1521903888151",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300208718953",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plants-with-novel-traits/applicants/managing-cases-of-non-compliance/",
        "fr": "/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/gestion-des-situations-de-non-conformite/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Managing cases of non-compliance of unauthorized plant products derived through biotechnology",
            "fr": "Gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie"
        },
        "description": {
            "en": "Under Canadian legislation, the presence of an unauthorized product in the marketplace or environment constitutes regulatory non-compliance. In such a case, the CFIA and/or Health Canada evaluate the risk associated with the non-compliance.",
            "fr": "En vertu des lois canadiennes, la pr\u00e9sence d'un produit non autoris\u00e9 sur le march\u00e9 ou dans l'environnement constitue une situation de non-conformit\u00e9 r\u00e9glementaire. Dans ce cas, l'ACIA et/ou Sant\u00e9 Canada \u00e9value le risque associ\u00e9 \u00e0 la situation de non-conformit\u00e9."
        },
        "keywords": {
            "en": "biotechnology, plants with novel traits, non-compliance, unauthorized",
            "fr": "biotechnologie, v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux, non-conformit\u00e9, non autoris\u00e9"
        },
        "dcterms.subject": {
            "en": "biotechnology,environment,inspection,plants,scientific research",
            "fr": "biotechnologie,environnement,inspection,plante,recherche scientifique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-26",
            "fr": "2018-03-26"
        },
        "modified": {
            "en": "2018-06-14",
            "fr": "2018-06-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,media",
            "fr": "entreprises,gouvernement,grand public,m\u00e9dia"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Managing cases of non-compliance of unauthorized plant products derived through biotechnology",
        "fr": "Gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie"
    },
    "body": {
        "en": "        \r\n        \n\n<div class=\"row\">\n<div class=\"col-md-8\">\n<p>Under Canadian legislation, the presence of an unauthorized product in the marketplace or environment constitutes regulatory non-compliance. In such a case, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and/or Health Canada evaluate the risk associated with the non-compliance. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would then determine which risk management options and compliance actions are required.</p>\n\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/managing-cases-of-non-compliance/overview/eng/1338275008823/1338275093927\">Overview - Managing cases of non-compliance of unauthorized plant products derived through biotechnology</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/managing-cases-of-non-compliance/fact-sheet/eng/1338518900639/1338519150701\">How does the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> manage cases of non-compliance of unauthorized plant products derived through biotechnology?</a></li>\n</ul>\n</div>\n<div class=\"col-md-4\">\n<section class=\"alert alert-info\">\n<h2 class=\"h4\">Detection of genetically modified herbicide-tolerant wheat in Alberta</h2>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> has examined an isolated finding of a few genetically modified wheat plants in Alberta.</p>\n<p><a href=\"/plant-varieties/plants-with-novel-traits/general-public/wheat-detection-2018/eng/1522107927141/1522107927640\">Get the facts</a></p>\n</section>\n</div>\n</div>\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-md-8\">\n<p>En vertu des lois canadiennes, la pr\u00e9sence d'un produit non autoris\u00e9 sur le march\u00e9 ou dans l'environnement constitue une situation de non-conformit\u00e9 r\u00e9glementaire. Dans ce cas, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et/ou Sant\u00e9 Canada \u00e9value le risque associ\u00e9 \u00e0 la situation de non-conformit\u00e9. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d\u00e9termine ensuite les options de gestion du risque et les mesures \u00e0 prendre pour revenir \u00e0 une situation conforme.</p>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/gestion-des-situations-de-non-conformite/apercu/fra/1338275008823/1338275093927\">Aper\u00e7u - La gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/gestion-des-situations-de-non-conformite/fiche-de-renseignements/fra/1338518900639/1338519150701\">Comment l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> g\u00e8re-t-elle les situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie?</a></li>\n</ul>\n</div>\n<div class=\"col-md-4\">\n<section class=\"alert alert-info\">\n<h2 class=\"h4\">D\u00e9couverte de bl\u00e9 g\u00e9n\u00e9tiquement modifi\u00e9 tol\u00e9rant \u00e0 un herbicide en Alberta</h2>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a examin\u00e9 la d\u00e9couverte isol\u00e9e de quelques plants de bl\u00e9s g\u00e9n\u00e9tiquement modifi\u00e9s en Alberta.</p>\n<p><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/decouverte-de-ble-2018/fra/1522107927141/1522107927640\">Les faits</a></p>\n</section>\n</div>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}