{
    "dcr_id": "1646274247575",
    "title": {
        "en": "Fertilizer or supplement registration: Before you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Avant de pr\u00e9senter une demande"
    },
    "html_modified": "2024-02-13 9:55:40 AM",
    "modified": "2023-11-16",
    "issued": "2022-08-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-before_you_apply_1646274247575_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-before_you_apply_1646274247575_fra"
    },
    "ia_id": "1646274247919",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fertilizer or supplement registration: Before you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Avant de pr\u00e9senter une demande"
    },
    "label": {
        "en": "Fertilizer or supplement registration: Before you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Avant de pr\u00e9senter une demande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646274247919",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/before-you-apply/",
        "fr": "/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fertilizer or supplement registration: Before you apply",
            "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Avant de pr\u00e9senter une demande"
        },
        "description": {
            "en": "The extent of the safety data requirements varies depending on the: application type,  nature of the product, and product's risk profile.",
            "fr": "L'ampleur des exigences relatives aux donn\u00e9es d'innocuit\u00e9 varie selon : le type de demande, la nature du produit, et le profil de risque du produit."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizer regulations, Fertilizer, supplement, registration, Before you apply, new",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, Enregistrement, engrais, suppl\u00e9ment,\u00a0Avant de pr\u00e9senter une demande, nouveau"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy",
            "fr": "cultures,engrais,inspection,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "modified": {
            "en": "2023-11-16",
            "fr": "2023-11-16"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fertilizer or supplement registration: Before you apply",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Avant de pr\u00e9senter une demande"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/overview/eng/1646095692928/1646095693725\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646102406709/1646102407100\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item active\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/how-to-apply/eng/1646282540949/1646282541291\">4. How to apply</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/after-you-apply/eng/1646347538968/1646347539296\">5. After you apply</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646359107928/1646359108272\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<h2 class=\"mrgn-tp-0\" id=\"a1\">Get your information ready</h2>\n\n<h3>Product details</h3>\n\n<p>You must provide the following product details when applying for a fertilizer or supplement registration:</p>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h4 class=\"panel-title\">Additional information</h4>\n</header>\n<div class=\"panel-body\">\n<p>The following resources can help you prepare your application for registration:</p>\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/eng/1646095692928/1646095693725\">Checklists for completing your fertilizer or supplement registration application</a></li>\n<li><a href=\"/plant-health/fertilizers/fertilizer-or-supplement-registration/guidelines/eng/1346531775900/1346537405987\">Guidelines to completing the new fertilizer or supplement registration application</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0\">Guide to submitting applications for registration under the <i>Fertilizers Act</i></a></li>\n</ul>\n</div>\n</section>\n</div>\n\n<ul>\n<li>Product type</li>\n<li>Brand name in English and French (if applicable)</li>\n<li>Product name in English and French</li>\n<li>Grade if the product contains a major nutrient (Nitrogen, Phosphorus or Potassium)</li>\n<li>Constituent materials including:\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#a2313\">Composition of the final product</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#a231\">List of ingredients and their sources</a>\n<ul>\n<li>You can enter the this information in the text field or you can upload a file to describe the product composition</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>Other qualities and characteristics including:\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#a234\">Physical characteristics of the final product</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#a232\">Method of manufacture</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#a233\">Quality assurance (QA) and quality control (QC) procedures</a>\n<ul>\n<li>You can enter this information in the text field or you can upload a file to describe the other qualities and characteristics, including method of manufacture and QA/QC</li>\n</ul>\n</li>\n</ul>\n</li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#a222\">Guaranteed analysis</a></li>\n</ul>\n\n<h3>Proposed safety risk assessment level for the final product (level I, II or III)</h3>\n\n<p>To complete the application for registration you must:</p>\n\n<ul>\n<li>select a proposed safety risk assessment level for your product</li>\n<li>upload a rationale that supports your choice of safety-level</li>\n</ul>\n\n<p>For help determining your product's proposed risk assessment level refer to <a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s13c6\">product ingredients and associated safety data requirements</a>.</p>\n\n<p>For safety-level II and III applications, you must upload the following required information (if applicable) along with your rationale:</p>\n\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#s10c4\">Results of Analysis</a></li>\n<li><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0#s11c4\">Safety rationale and supplemental data</a></li>\n</ul>\n\n<p>You can view the supporting document requirements for the proposed safety assessment level (I, II, or III) in <a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s6c3\">safety data requirements</a>.</p>\n\n<p><strong>Note:</strong> The proposed safety risk assessment level may change following the initial screening of the application.</p>\n\n<h3>Manufacturer details</h3>\n\n<p>You must provide the following information in the application to confirm the manufacturer details:</p>\n\n<ul>\n<li>Legal Name of the individual or company that manufactures the final product or supplement</li>\n<li>Address where the final product or supplement is manufactured</li>\n</ul>\n\n<h3>Proposed label</h3>\n\n<p>You must upload a copy of the proposed text for the marketplace label that will appear on your product or supplement package before you review and submit your application.</p>\n\n<p>For guidance about proposed labels, refer to <a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4#s8c4\">proposed marketplace label</a>.</p>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Processing time</h3>\n</header>\n<div class=\"panel-body\">\n<p>You'll receive a response from an evaluator within\u00a030 working days after your product specific inquiry information is received and an Inquiry file is opened.</p>\n</div>\n</section>\n</div>\n\n<h2 id=\"b1\">Inquiry (IQ) process and pre-submission consultations</h2>\n\n<p>Prospective registrants can use the IQ and pre-submission consultation processes to get guidance, advice, and clarification on the regulatory requirements <strong>before</strong> applying for registration.</p>\n\n<p>For more information about the IQ and pre-submission consultation processes refer to:\n</p>\n\n<ul>\n<li><a href=\"/eng/1330910327069/1330910496655#a3\">Initiate contact with an inquiry</a></li>\n<li><a href=\"/eng/1330910327069/1330910496655#a4\">Procedure for requesting a pre-submission consultation meeting</a></li>\n</ul>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646102406709/1646102407100\" rel=\"prev\">Previous</a></li>\n\n\n<li class=\"next\"><a href=\"/plant-health/fertilizers/how-to-apply/eng/1646282540949/1646282541291\" rel=\"next\">Next</a></li>\n\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apercu/fra/1646095692928/1646095693725\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646102406709/1646102407100\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item active\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646282540949/1646282541291\">4. Comment pr\u00e9senter une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apres-avoir-presente-une-demande/fra/1646347538968/1646347539296\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646359107928/1646359108272\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\n<h2 class=\"mrgn-tp-0\" id=\"a1\">Pr\u00e9parez vos informations</h2>\n\n<h3>D\u00e9tails du produit</h3>\n\n<p>Vous devez fournir les d\u00e9tails suivants sur le produit lorsque vous faites une demande d'enregistrement d'un engrais ou d'un suppl\u00e9ment\u00a0:</p>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h4 class=\"panel-title\">Informations suppl\u00e9mentaires</h4>\n</header>\n<div class=\"panel-body\">\n<p>Les ressources suivantes peuvent vous aider \u00e0 pr\u00e9parer votre demande d'enregistrement\u00a0:</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/fra/1646095692928/1646095693725\">Liste de contr\u00f4le pour remplir votre demande d'enregistrement d'engrais ou de suppl\u00e9ments</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/enregistrement-d-engrais-ou-de-supplement/directives/fra/1346531775900/1346537405987\">Directives pour remplir le nouveau formulaire de demande d'enregistrement d'engrais ou de suppl\u00e9ment</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0\">Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la <i>Loi sur les engrais</i></a></li>\n</ul>\n</div>\n</section>\n</div>\n\n<ul>\n<li>Le type du produit</li>\n<li>Nom de la marque en anglais et en fran\u00e7ais (le cas \u00e9ch\u00e9ant)</li>\n<li>Le nom du produit en anglais et en fran\u00e7ais</li>\n<li>Indiquez si le produit contient un \u00e9l\u00e9ment nutritif majeur (Azote, Phosphore ou Potassium)</li>\n<li>Les mat\u00e9riaux constitutifs, y compris\u00a0:\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#a2313\">Composition du produit final</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#a231\">La liste des ingr\u00e9dients et leurs sources</a>\n<ul>\n<li>Vous pouvez saisir ces informations dans le champ de texte ou t\u00e9l\u00e9charger un fichier pour d\u00e9crire la composition du produit.</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>Autres qualit\u00e9s et caract\u00e9ristiques, notamment\u00a0:\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#a234\">Caract\u00e9ristiques physiques du produit final</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#a232\">M\u00e9thode de fabrication</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#a233\">Proc\u00e9dures de contr\u00f4le de la qualit\u00e9 et d'assurance de la qualit\u00e9</a>\n<ul>\n<li>Vous pouvez saisir ces informations dans le champ de texte ou t\u00e9l\u00e9charger un fichier pour d\u00e9crire les autres qualit\u00e9s et caract\u00e9ristiques, y compris la m\u00e9thode de fabrication et le syst\u00e8me AQ/CQ.</li>\n</ul>\n</li>\n</ul>\n</li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#a222\">Analyse garantie</a></li>\n</ul>\n\n<h3>Niveau d'\u00e9valuation du risque de s\u00e9curit\u00e9 propos\u00e9 pour le produit final (niveau I, II ou III)</h3>\n\n<p>Pour remplir la demande d'enregistrement, vous devez\u00a0:</p>\n\n<ul>\n<li>s\u00e9lectionner un niveau d'\u00e9valuation du risque de s\u00e9curit\u00e9 propos\u00e9 pour votre produit</li>\n<li>t\u00e9l\u00e9charger un raisonnement qui justifie votre choix du niveau de s\u00e9curit\u00e9</li>\n</ul>\n\n<p>Pour vous aider \u00e0 d\u00e9terminer le niveau d'\u00e9valuation des risques propos\u00e9 pour votre produit, consultez les <a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s13c6\">exigences propres aux ingr\u00e9dients pour d\u00e9montrer leur innocuit\u00e9</a>.</p>\n\n<p>Pour les demandes de niveau de s\u00e9curit\u00e9 II et III, vous devez t\u00e9l\u00e9charger les informations requises suivantes (le cas \u00e9ch\u00e9ant) ainsi que votre raisonnement\u00a0:</p>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#s10c4\">R\u00e9sultats d'analyses</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0#s11c4\">Justificatifs de l'innocuit\u00e9 et donn\u00e9es compl\u00e9mentaires</a></li>\n</ul>\n\n\n<p>Vous pouvez consulter les exigences relatives aux documents justificatifs pour le niveau d'\u00e9valuation de la s\u00e9curit\u00e9 propos\u00e9 (I, II ou III) dans les <a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s6c3\">exigences concernant les donn\u00e9es d'innocuit\u00e9</a>.</p>\n\n\n<p><strong>Remarque\u00a0:</strong> le niveau d'\u00e9valuation du risque de s\u00e9curit\u00e9 propos\u00e9 peut changer apr\u00e8s l'examen initial de la demande.</p>\n\n<h3>D\u00e9tails du fabricant</h3>\n\n<p>Vous devez fournir les informations suivantes dans la demande pour confirmer les d\u00e9tails du fabricant\u00a0:</p>\n\n<ul>\n<li>Le nom l\u00e9gal de l'individu ou de la compagnie qui fabrique le produit final ou le suppl\u00e9ment</li>\n<li>L'adresse o\u00f9 le produit final ou le suppl\u00e9ment est fabriqu\u00e9</li>\n</ul>\n\n<h3>\u00c9tiquette propos\u00e9e</h3>\n\n<p>Vous devez t\u00e9l\u00e9charger une copie du texte propos\u00e9 pour l'\u00e9tiquette de march\u00e9 qui appara\u00eetra sur l'emballage de votre produit ou suppl\u00e9ment avant d'examiner et de soumettre votre demande.</p>\n\n<p>Pour obtenir des conseils sur les \u00e9tiquettes propos\u00e9es, consultez l'onglet <a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4#s8c4\">\u00e9tiquette propos\u00e9e pour le produit</a>.</p>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">D\u00e9lais de traitement</h3>\n</header>\n<div class=\"panel-body\">\n<p>Vous recevrez une r\u00e9ponse d'un \u00e9valuateur dans les\u00a030\u00a0jours ouvrables suivant la r\u00e9ception des informations relatives \u00e0 votre demande sp\u00e9cifique de produit et l'ouverture d'un dossier de demande.</p>\n</div>\n</section>\n</div>\n\n<h2 id=\"b1\">Processus de demande d'informations et consultations pr\u00e9paratoires</h2>\n\n<p>Les candidats \u00e0 l'enregistrement peuvent utiliser les processus de demande d'informations et de consultation pr\u00e9paratoires pour obtenir des directives, des conseils et des \u00e9claircissements sur les exigences r\u00e9glementaires avant de pr\u00e9senter leur demande d'enregistrement.</p>\n\n<p>Pour plus d'informations sur les demandes d'informations et consultations pr\u00e9paratoires, consultez\u00a0:</p>\n\n<ul>\n<li><a href=\"/fra/1330910327069/1330910496655#a3\">Pr\u00e9sentation d'une demande de renseignements</a></li>\n<li><a href=\"/fra/1330910327069/1330910496655#a4\">Proc\u00e9dure de demande d'une consultation pr\u00e9paratoire \u00e0 une r\u00e9union</a></li>\n</ul>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646102406709/1646102407100\" rel=\"prev\">Pr\u00e9c\u00e9dent</a></li>\n\n<li class=\"next\"><a href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646282540949/1646282541291\" rel=\"next\">Suivant</a></li>\n\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}