{
    "dcr_id": "1467981423932",
    "title": {
        "en": "European cherry fruit fly \u2013 Rhagoletis cerasi",
        "fr": "Mouche europ\u00e9enne des cerises - Rhagoletis cerasi"
    },
    "html_modified": "8-7-2016",
    "modified": "1-6-2023",
    "issued": "8-7-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_rhagoletis_index_1467981423932_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_rhagoletis_index_1467981423932_fra"
    },
    "parent_ia_id": "1307078272806",
    "ia_id": "1467981769931",
    "parent_node_id": "1307078272806",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "European cherry fruit fly \u2013 Rhagoletis cerasi",
        "fr": "Mouche europ\u00e9enne des cerises - Rhagoletis cerasi"
    },
    "label": {
        "en": "European cherry fruit fly \u2013 Rhagoletis cerasi",
        "fr": "Mouche europ\u00e9enne des cerises - Rhagoletis cerasi"
    },
    "templatetype": "content page 1 column",
    "node_id": "1467981769931",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1307077188885",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/european-cherry-fruit-fly/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/mouche-europeenne-des-cerises/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "European cherry fruit fly \u2013 Rhagoletis cerasi",
            "fr": "Mouche europ\u00e9enne des cerises - Rhagoletis cerasi"
        },
        "description": {
            "en": "The European cherry fruit fly (Rhagoletis cerasi L.) is the most serious pest of cherries in Europe.",
            "fr": "La mouche europ\u00e9enne des cerises (Rhagoletis cerasi L.) est le ravageur le plus important des cerisiers en Europe."
        },
        "keywords": {
            "en": "European cherry fruit fly, Rhagoletis cerasi L, damage, cherries, fruit, plants",
            "fr": "mouche europ\u00e9enne des cerises, Rhagoletis cerasi L,  endommager, cerises, fruit, plantes"
        },
        "dcterms.subject": {
            "en": "fruits,insects,plants",
            "fr": "fruit,insecte,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-07-08",
            "fr": "2016-07-08"
        },
        "modified": {
            "en": "2023-06-01",
            "fr": "2023-06-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "European cherry fruit fly \u2013 Rhagoletis cerasi",
        "fr": "Mouche europ\u00e9enne des cerises - Rhagoletis cerasi"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-left mrgn-rght-md\">\n<p><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_rhagoletis_factsheet_image3_1467991970248_eng.jpg\" class=\"img-responsive thumbnail\" alt=\"Female adult on cherry\"></p>\n</div>\n<p class=\"mrgn-tp-xl\">The European cherry fruit fly (<i lang=\"la\">Rhagoletis cerasi</i> <abbr title=\"Lonicera\">L.</abbr>) is the most serious pest of cherries in Europe. Damage associated with this pest is caused by larval feeding in the fruit pulp, which can result in losses of up to 100% if left uncontrolled. This pest may be introduced to new areas with fresh cherries or with soil or fruit from host plants grown in areas where this pest occurs.</p> \n\n<h2>What information is available?</h2>\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/european-cherry-fruit-fly/fact-sheet/eng/1467913088353/1467914654510\"><i lang=\"la\">Rhagoletis cerasi</i> (European Cherry Fruit Fly) \u2013 Fact Sheet</a></li>\n<li><a href=\"/eng/1494252564516/1494358703222%20\">D-17-03: Phytosanitary requirements to prevent the spread of <i lang=\"la\">Rhagoletis cerasi</i> <abbr title=\"Lonicera\">L.</abbr> (European cherry  fruit fly) within Canada</a></li>\n<li><a href=\"https://www.pestalerts.org/official-pest-report/rhagoletis-cerasi-european-cherry-fruit-fly-detected-ontario\">Report of European cherry fruit fly in Ontario (2016)</a></li>\n<li><a href=\"https://www.pestalerts.org/official-pest-report/report-european-cherry-fruit-fly-rhagoletis-cerasi-quebec-canada-2022\">Report of European cherry fruit fly in Quebec (2022)</a></li> \n<li><a href=\"/plant-health/invasive-species/insects/european-cherry-fruit-fly/ontario-cherries-faq/eng/1495544859895/1495544860490\">Frequently asked questions: Movement requirements for Ontario cherries</a></li>\n</ul>\n\n<h2>Notices to industry</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/european-cherry-fruit-fly/notice-to-industry-2017-05-23/eng/1495544997843/1495544998246\">Notice to industry (2017): New movement requirements for Ontario cherries to prevent spread of plant pests</a></li>\n</ul>\n\n<h2>Plant pest credit card: European cherry fruit fly</h2>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/european_cherry_fruit_fly_1548355311606_eng.pdf\" title=\"Plant Pest Credit Card: European cherry fruit fly in portable document format\">\n<p>PDF\u00a0(926\u00a0kb)</p>\n<div class=\"row\">\n<div class=\"col-sm-4\"><img alt=\"Side 1: Thumbnail image for plant pest credit card: European cherry fruit fly. Description follows.\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/plant_pest_cards_cherry_fruit_fly_front_1685121870080_eng.png\" class=\"img_rounded img-responsive\"></div>\n<div class=\"col-sm-4\"><img alt=\"Side 2: Thumbnail image for plant pest credit card: European cherry fruit fly. Description follows.\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/plant_pest_cards_cherry_fruit_fly_back_1685121926389_eng.png\" class=\"img_rounded img-responsive\">\n</div></div></a>\n\n<details class=\"mrgn-tp-md\"><summary>Text version</summary>\n<p>The adult is 3.5\u00a0to 4\u00a0mm long and predominantly black in colour. The wings are transparent, with characteristic dark crossbands. The scutellum of the thorax is yellow. Hosts include cherry and honeysuckle.</p>\n<p>Signs of European cherry fruit fly include maggots in fruit, brown, rotting fruit, and maggot exit holes in cherries.</p>\n<p>\u00a9\u00a02017 Her Majesty the Queen in Right of Canada. Aussi disponible en fran\u00e7ais. Use without permission is prohibited.</p>\n<p>Photo credits: R.\u00a0Coutin (OPIE), C.\u00a0Daniel (FiBL), www.entomart.be, J.\u00a0Grunder (ZHAW). Please report suspect specimens.</p>\n</details>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-left mrgn-rght-md\">\n<p><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_rhagoletis_factsheet_image3_1467991970248_fra.jpg\" class=\"img-responsive thumbnail\" alt=\"Femelle adulte sur une cerise\"></p>\n</div>\n<p>La mouche europ\u00e9enne des cerises (<i lang=\"la\">Rhagoletis cerasi</i> <abbr title=\"Lonicera\">L.</abbr>) est l'organisme nuisible le plus important des cerisiers en Europe. Les larves endommagent les fruits en s'alimentant dans la pulpe et peuvent provoquer une perte totale de r\u00e9colte en l'absence d'intervention. L'introduction de cet organisme nuisible dans de nouvelles r\u00e9gions peut se produire par le transport de sol ou de cerises fra\u00eeches infest\u00e9es ou encore de fruits de plantes h\u00f4tes cultiv\u00e9es dans les r\u00e9gions infest\u00e9es.</p>\n\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition?</h2>\n<ul><li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-europeenne-des-cerises/fiche-d-information/fra/1467913088353/1467914654510\">Fiche d'information sur la mouche europ\u00e9enne des cerises (<i lang=\"la\">Rhagoletis cerasi</i>)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-17-03/fra/1494252564516/1494358703222\">D-17-03\u00a0: Exigences phytosanitaires visant \u00e0 pr\u00e9venir la propagation de <i lang=\"la\">Rhagoletis cerasi</i> <abbr title=\"Lonicera\">L.</abbr> (mouche europ\u00e9enne des cerises) au Canada</a></li>\n<li><a href=\"https://www.pestalerts.org/official-pest-report/rhagoletis-cerasi-european-cherry-fruit-fly-detected-ontario\">Signalement de la mouche europ\u00e9enne des cerises en Ontario (2016) (anglais seulement)</a></li>\n<li><a href=\"https://www.pestalerts.org/official-pest-report/report-european-cherry-fruit-fly-rhagoletis-cerasi-quebec-canada-2022\">Signalement de la mouche europ\u00e9enne des cerises au Qu\u00e9bec (2022)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-europeenne-des-cerises/cerises-de-l-ontario-faq/fra/1495544859895/1495544860490\">Foire aux questions\u00a0: exigences relatives au d\u00e9placement des cerises de l'Ontario</a></li>\n</ul>\n\n<h2>Avis \u00e0 l'industrie</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-europeenne-des-cerises/avis-a-l-industrie-2017-05-23/fra/1495544997843/1495544998246\">Avis \u00e0 l'industrie (2017)\u00a0: Nouvelles exigences relatives au d\u00e9placement des cerises de l'Ontario, dans le but d'emp\u00eacher la propagation de phytoravageurs</a></li> \n</ul>\n\n<h2>Carte de phytoravageur\u00a0: Mouche europ\u00e9enne des cerises</h2>\n\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/european_cherry_fruit_fly_1548355311606_fra.pdf\" title=\"Plant Pest Credit Card: Mouche europ\u00e9enne des cerises in portable document format\">\n<p>PDF\u00a0(970\u00a0kb)</p>\n<div class=\"row\">\n<div class=\"col-sm-4\"><img alt=\"C\u00f4t\u00e9 1 : L'image vignette pour la cartes de cr\u00e9dit de phytoravageurs : Mouche europ\u00e9enne des cerises. Description ci-dessous.\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/plant_pest_cards_cherry_fruit_fly_front_1685121870080_fra.png\" class=\"img-rounded img-responsive\"></div>\n<div class=\"col-sm-4\"><img alt=\"C\u00f4t\u00e9 2 : L'image vignette pour la cartes de cr\u00e9dit de phytoravageurs : Mouche europ\u00e9enne des cerises. Description ci-dessous.\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/plant_pest_cards_cherry_fruit_fly_back_1685121926389_fra.png\" class=\"img-rounded img-responsive\">\n</div></div></a>\n\n<details class=\"mrgn-tp-md\"><summary>Version textuelle</summary>\n<p>L'adulte est de\u00a03.5 \u00e0 4 mm\u00a0de long et  a une couleur pr\u00e9dominante noire. Les ailes sont transparentes et ont des  bandes transversales fonc\u00e9es caract\u00e9ristiques. Le scutellum du thorax est  enti\u00e8rement jaune. Les h\u00f4tes incluent le cerisier et le ch\u00e8vrefeuille.</p>\n\n<p>Les signes de la mouche europ\u00e9enne des cerises  incluent les larves dans les cerises, les fruits pourrissants, et les trous de  sortie de larves dans les cerises.</p>\n\n<p>\u00a9\u00a0Sa\u00a0Majest\u00e9 la Reine du chef du Canada  2017.\u00a0Also available in English. L'utilisation sans autorisation est  interdite. Photos\u00a0: gracieuset\u00e9 de R.\u00a0Coutin (OPIE), C.\u00a0Daniel (FiBL),  www.entomart.be, J.\u00a0Grunder (ZHAW). Signalez un  sp\u00e9cimen suspect.</p>\n</details>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}