{
    "dcr_id": "1303139689562",
    "title": {
        "en": "Canadian Agricultural Loans Act (CALA) Program",
        "fr": "Programme de la Loi canadienne sur les pr\u00eats agricoles (LCPA)"
    },
    "modified": "2020-02-24",
    "issued": "2011-04-19",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1365605560469",
    "dc_date_created": "2011-04-19",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/cala_program_1303139689562_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/cala_program_1303139689562_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Canadian Agricultural Loans Act (CALA) Program",
        "fr": "Programme de la Loi canadienne sur les pr\u00eats agricoles (LCPA)"
    },
    "breadcrumb": {
        "en": "Canadian Agricultural Loans Act (CALA) Program",
        "fr": "Programme de la Loi canadienne sur les pr\u00eats agricoles (LCPA)"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2011-04-19",
            "fr": "2011-04-19"
        },
        "modified": {
            "en": "2020-02-24",
            "fr": "2020-02-24"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Canadian Agricultural Loans Act (CALA) Program",
            "fr": "Programme de la Loi canadienne sur les pr\u00eats agricoles (LCPA)"
        },
        "subject": {
            "en": "programs;finance;farm management;agricultural cooperatives",
            "fr": "finances;gestion de l'exploitation agricole;programmes;coop\u00e9ratives agricoles"
        },
        "dcterms:subject": {
            "en": "finance;agricultural workers;agricultural assistance;cooperatives",
            "fr": "finances;agriculture;travailleur agricole;aide \u00e0 l'agriculture;coop\u00e9rative"
        },
        "description": {
            "en": null,
            "fr": null
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": null
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Canadian Agricultural Loans Act (CALA) Program",
        "fr": "Programme de la Loi canadienne sur les pr\u00eats agricoles (LCPA)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    \n<h2>Introduction</h2>\n\n<p>The <cite>Canadian Agricultural Loans Act</cite> (CALA) program is a financial loan guarantee program that makes credit more available to farmers and agricultural co-operatives. Farmers can use these loans to establish, improve, and develop farms, while agricultural co-operatives may also access loans to process, distribute, or market the products of farming.</p>\n<p>The CALA program builds on and replaces the previous <cite>Farm Improvement and Marketing Cooperatives Loans Act</cite> (FIMCLA) program, which has helped farming operations grow their businesses by guaranteeing loans issued through financial institutions since 1988.</p>\n<p>Through the CALA, the Government of Canada is supporting the renewal of the agricultural sector and enabling co-operatives to better seize market opportunities.</p>\n\n<h2>Objective</h2>\n\n<p>As the CALA Program involves the collection and management of personal information, a Privacy Impact Assessment (PIA) was conducted to determine if there were any privacy risks, and if so, to make recommendations for their resolution or mitigation.</p>\n\n<h2>Description</h2>\n\n<p>The scope of this PIA is restricted to the business process and basic data flows of personal information associated with the CALA. This includes Agriculture and Agri-Food Canada's (AAFC) collection, use, retention, disclosure and disposal of personal information in the context of the CALA.</p>\n<p>Personal Information Bank (PIB) AAFC PPU 165 outlines the details regarding the personal information collected under the program, including the purpose, use, retention and disposal standards.</p>\n\n<h2>Conclusion</h2>\n\n<p>As a result of the PIA, measures have been determined to mitigate risks associated with the collection of personal information. These include: a physical Threat and Risk Assessment (TRA) on the CALA file storage room, mandatory privacy training for all AAFC staff who handle personal information, and revision of the Personal Information Bank.</p>\n\n<p>For further information on this PIA please contact:</p>\n\n<address>Access to Information and Privacy<br>\n1341 Baseline Road, 10th Floor, Tower 7, Room 130<br>\nOttawa ON \u00a0K1A\u00a00C5<br>\nTelephone: 613-773-0970<br>\nFacsimile: 613-773-1380<br>\nEmail: <a href=\"mailto:aafc.atip-aiprp.aac@canada.ca\">aafc.atip-aiprp.aac@canada.ca</a></address>\n\r\n    ",
        "fr": "\r\n      <h2>Introduction</h2>\n  \n  <p>Le Programme de la <cite>Loi canadienne sur les pr\u00eats agricoles</cite> (LCPA) est un programme de garantie de pr\u00eats de financement facilitant l'acc\u00e8s des producteurs agricoles et des coop\u00e9ratives agricoles au cr\u00e9dit. Les producteurs agricoles peuvent utiliser ces pr\u00eats pour construire, r\u00e9nover et agrandir leurs exploitations agricoles, alors que les coop\u00e9ratives agricoles peuvent aussi avoir acc\u00e8s aux pr\u00eats pour transformer, distribuer ou commercialiser des produits agricoles.</p>\n  \n  <p>Le Programme de la LCPA vient remplacer l'ancien Programme de la <cite>Loi sur les pr\u00eats destin\u00e9s aux am\u00e9liorations agricoles et \u00e0 la commercialisation selon la formule coop\u00e9rative</cite> (LPAACFC), qui a contribu\u00e9 \u00e0 l'expansion des exploitations agricoles en garantissant les pr\u00eats \u00e9mis par les institutions financi\u00e8res depuis 1988.</p>\n  \n  <p>Par le truchement de la LCPA, le gouvernement du Canada appuie le renouvellement du secteur agricole et permet aux coop\u00e9ratives de tirer davantage parti des d\u00e9bouch\u00e9s.</p>\n\n  <h2>Objectif</h2>\n  \n  <p>Compte tenu du fait que le Programme de la LCPA comprend la collecte et la gestion des renseignements personnels, une \u00c9valuation des facteurs relatifs \u00e0 la vie priv\u00e9e (EFVP) a \u00e9t\u00e9 effectu\u00e9e dans le but de d\u00e9terminer si le programme comportait des risques d'entrave \u00e0 la vie priv\u00e9e et, le cas \u00e9ch\u00e9ant, d'\u00e9mettre des recommandations permettant d'\u00e9liminer ou d'att\u00e9nuer ces risques.</p>\n\n  <h2>Description</h2>\n  \n  <p>La port\u00e9e de cette EFVP se limite au processus administratif et \u00e0 la circulation de renseignements personnels g\u00e9n\u00e9raux associ\u00e9s \u00e0 l'administration de la LCPA, y compris la collecte, l'utilisation, la conservation, la divulgation et l'\u00e9limination de renseignements personnels par Agriculture et Agroalimentaire Canada (AAC) dans le cadre de la LCPA.</p>\n  \n  <p>Le Fichier de renseignements personnels AAC PPU 165 renferme les d\u00e9tails en ce qui a trait aux renseignements personnels recueillis dans le cadre du programme, y compris les normes r\u00e9gissant l'objet, l'utilisation, la conservation et l'\u00e9limination.</p>\n\n  <h2>Conclusion</h2>\n  \n  <p>\u00c0 la suite de l'EFVP, les mesures suivantes ont \u00e9t\u00e9 mises en place pour att\u00e9nuer les risques associ\u00e9s \u00e0 la collecte de renseignements personnels\u00a0: l'\u00e9valuation de la menace physique et des risques (EMR) associ\u00e9s au local de stockage des fichiers LCPA; une s\u00e9ance obligatoire de formation sur la protection des renseignements personnels pour l'ensemble des employ\u00e9s d'AAC qui sont responsables de la gestion de renseignements personnels; l'examen du Fichier de renseignements personnels.</p>\n  \n  <p>Pour de plus amples renseignements sur cette EFVP, veuillez communiquer avec\u00a0:</p>\n  \n  <address>\n  Acc\u00e8s \u00e0 l'information et protection des renseignements personnels<br>\n  1341, chemin Baseline, 10e \u00e9tage, tour 7, bureau 130<br>\n  Ottawa ON \u00a0K1A\u00a00C5<br>\n  T\u00e9l\u00e9phone\u00a0: 613-773-0970<br>\n  T\u00e9l\u00e9copieur\u00a0: 613-773-1380<br>\n  Courriel\u00a0: <a href=\"mailto:aafc.atip-aiprp.aac@canada.ca\">aafc.atip-aiprp.aac@canada.ca</a>\n  </address>\n\n\n\r\n    "
    }
}