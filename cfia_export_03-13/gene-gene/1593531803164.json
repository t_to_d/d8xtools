{
    "dcr_id": "1593531803164",
    "title": {
        "en": "Mother of vinegar",
        "fr": "M\u00e8re de vinaigre"
    },
    "html_modified": "2024-03-12 3:58:46 PM",
    "modified": "2020-07-16",
    "issued": "2020-07-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/mother_of_vinegar_1593531803164_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/mother_of_vinegar_1593531803164_fra"
    },
    "ia_id": "1593531803773",
    "parent_ia_id": "1592232381996",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1592232381996",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Mother of vinegar",
        "fr": "M\u00e8re de vinaigre"
    },
    "label": {
        "en": "Mother of vinegar",
        "fr": "M\u00e8re de vinaigre"
    },
    "templatetype": "content page 1 column",
    "node_id": "1593531803773",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1592232381574",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/commonly-occurring-issues-in-food/mother-of-vinegar/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/problemes-courants-dans-les-aliments/mere-de-vinaigre/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Mother of vinegar",
            "fr": "M\u00e8re de vinaigre"
        },
        "description": {
            "en": "Occasionally, you may find a gelatinous disc in your bottle of vinegar. This is called mother of vinegar. Although it may look unappealing, it is completely harmless.",
            "fr": "Parfois, vous pouvez trouver un disque g\u00e9latineux dans votre bouteille de vinaigre. C'est ce qu'on appelle la m\u00e8re de vinaigre. M\u00eame si cela peut para\u00eetre peu attrayant, c'est un ph\u00e9nom\u00e8ne totalement inoffensif."
        },
        "keywords": {
            "en": "food safety tips, labels, food recalls, food borne illess, food packaging, storage, food handling, specific products, risks, mother of vinegar",
            "fr": "Conseils sur la salubrit\u00e9 des aliments, \u00e9tiquettes, Rappels d'aliments, toxi-infections alimentaire, emballage, entreposage, Produits, risques sp\u00e9cifiques, m\u00e8re de vinaigre"
        },
        "dcterms.subject": {
            "en": "food,consumers,labelling,food labeling,government information,inspection,food inspection,consumer protection,government publication,food safety",
            "fr": "aliment,consommateur,\u00e9tiquetage,\u00e9tiquetage des aliments,information gouvernementale,inspection,inspection des aliments,protection du consommateur,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-07-16",
            "fr": "2020-07-16"
        },
        "modified": {
            "en": "2020-07-16",
            "fr": "2020-07-16"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Mother of vinegar",
        "fr": "M\u00e8re de vinaigre"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Occasionally, you may find a gelatinous disc in your bottle of vinegar. This is called mother of vinegar. Although it may look unappealing, it is completely harmless.</p>\n\n<div class=\"pull-right mrgn-lft-md col-sm-4\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/mother_of_vinegar_img_1594907111197_eng.jpg\" class=\"img-responsive\" alt=\"\"></p>\n</div>\n\n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">How mother of vinegar occurs</a></li>\n<li><a href=\"#a2\">Product safety</a></li>\n<li><a href=\"#a3\">What to do if you find mother of vinegar</a></li>\n</ul>\n\n\n<h2 id=\"a1\">How mother of vinegar occurs</h2>\n\n<p>Mother of vinegar is a biofilm formed by bacteria that turn alcohol into acetic acid (primary ingredient of vinegar) when exposed to oxygen.</p>\n\n<p>Mother of vinegar can occur in store-bought vinegar if there is non-fermented sugar and/or alcohol contained in the vinegar. It is more common in unpasteurized vinegar.</p>\n\n<h2 id=\"a2\">Product safety</h2>\n\n<p>Although mother of vinegar looks unappealing, it does not pose a health risk. The vinegar can still be used.</p>\n\n<h2 id=\"a3\">What to do if you find mother of vinegar</h2>\n\n<p>If you want to remove the mother of vinegar, you can filter it out by using a coffee filter. The vinegar can still be used.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Parfois, vous pouvez trouver un disque g\u00e9latineux dans votre bouteille de vinaigre. C'est ce qu'on appelle la m\u00e8re de vinaigre. M\u00eame si cela peut para\u00eetre peu attrayant, c'est un ph\u00e9nom\u00e8ne totalement inoffensif.</p>\n\n<div class=\"pull-right mrgn-lft-md col-sm-4\">\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/mother_of_vinegar_img_1594907111197_fra.jpg\" class=\"img-responsive\" alt=\"\"></p>\n</div>\n\n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Comment se forme la m\u00e8re de vinaigre</a></li>\n<li><a href=\"#a2\">S\u00e9curit\u00e9 des produits</a></li>\n<li><a href=\"#a3\">Que faire si vous trouvez de la m\u00e8re de vinaigre</a></li>\n</ul>\n\n<h2 id=\"a1\">Comment se forme la m\u00e8re de vinaigre</h2>\n\n<p>La m\u00e8re de vinaigre est un biofilm form\u00e9 par des bact\u00e9ries qui transforment l'alcool en acide ac\u00e9tique (ingr\u00e9dient principal du vinaigre) lorsqu'il est expos\u00e9 \u00e0 l'oxyg\u00e8ne.</p>\n\n<p>La m\u00e8re de vinaigre peut se produire dans le vinaigre achet\u00e9 en magasin s'il y a du sucre ou de l'alcool non ferment\u00e9 contenu dans le vinaigre. Ce ph\u00e9nom\u00e8ne est plus courant dans le vinaigre non pasteuris\u00e9.</p>\n\n<h2 id=\"a2\">S\u00e9curit\u00e9 des produits</h2>\n\n<p>M\u00eame si la m\u00e8re de vinaigre semble peu attrayante, elle ne pose pas de risque pour la sant\u00e9. Le vinaigre peut encore \u00eatre utilis\u00e9.</p>\n\n<h2 id=\"a3\">Que faire si vous trouvez de la m\u00e8re de vinaigre</h2>\n\n<p>Si vous voulez retirer la m\u00e8re de vinaigre, vous pouvez la filtrer \u00e0 l'aide d'un filtre \u00e0 caf\u00e9. Le vinaigre peut encore \u00eatre utilis\u00e9.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}