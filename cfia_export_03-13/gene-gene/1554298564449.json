{
    "dcr_id": "1554298564449",
    "title": {
        "en": "Herds infected with chronic wasting disease in Canada",
        "fr": "Troupeaux infect\u00e9s par la maladie d\u00e9bilitante chronique au Canada"
    },
    "html_modified": "2024-03-12 3:58:24 PM",
    "modified": "2024-03-11",
    "issued": "2019-04-10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_monthly_reportable_cwd_1554298564449_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_monthly_reportable_cwd_1554298564449_fra"
    },
    "ia_id": "1554298564710",
    "parent_ia_id": "1330143991594",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1330143991594",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Herds infected with chronic wasting disease in Canada",
        "fr": "Troupeaux infect\u00e9s par la maladie d\u00e9bilitante chronique au Canada"
    },
    "label": {
        "en": "Herds infected with chronic wasting disease in Canada",
        "fr": "Troupeaux infect\u00e9s par la maladie d\u00e9bilitante chronique au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1554298564710",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1330143462380",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/cwd/herds-infected/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mdc/troupeaux-infectes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Herds infected with chronic wasting disease in Canada",
            "fr": "Troupeaux infect\u00e9s par la maladie d\u00e9bilitante chronique au Canada"
        },
        "description": {
            "en": "The CFIA works with provincial governments and industry to conduct regular Chronic Wasting Disease (CWD) surveillance. Ongoing provincial surveillance for CWD varies with each particular province's perceived threat and infection status.",
            "fr": "L'ACIA collabore avec les gouvernements provinciaux et l'industrie pour assurer une surveillance continue de la MDC. Les activit\u00e9s de surveillance provinciales de la MDC varient d'une province \u00e0 l'autre selon le niveau de risque pr\u00e9sum\u00e9 et le statut zoosanitaire."
        },
        "keywords": {
            "en": "animals, animal health, Health of Animals Regulations, Chronic Wasting Disease, CWD, reportable disease, disease, cervid herds, 2019, 2020, 2021",
            "fr": "animaux, sant\u00e9 animale, R\u00e8glement sur la sant\u00e9 des animaux, maladie d\u00e9bilitante chronique, MDC, cervid\u00e9s, maladies, maladies \u00e0 d\u00e9claration obligatoire, 2019, 2020, 2021"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-04-10",
            "fr": "2019-04-10"
        },
        "modified": {
            "en": "2024-03-11",
            "fr": "2024-03-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Herds infected with chronic wasting disease in Canada",
        "fr": "Troupeaux infect\u00e9s par la maladie d\u00e9bilitante chronique au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Chronic wasting disease (CWD) is a reportable disease under the <i>Health of Animals Regulations</i>. This means that all suspected cases must be reported to the <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Canadian Food Inspection Agency</a> (CFIA).</p>\n\n<p><strong>Current as of: 2024-02-29</strong></p>\n\n<h2 class=\"h4\">Domestic cervid herds confirmed to be infected with CWD in Canada</h2>\n\n<div class=\"table-responsive\">\n<table class=\"wb-tables table table-striped table-hover\" data-wb-tables='{ \"pageLength\" : 25 , \"order\" : [0,\"desc\"] }'>\n\n<thead>\n<tr>\n<th>Year</th>\n<th>Date confirmed</th>\n<th>Location</th>\n<th>Animal type infected</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>2024</td>\n<td>February\u00a026</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>November\u00a06</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>August\u00a02</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>June\u00a06</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>June\u00a06</td>\n<td>Alberta</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>April\u00a020</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>March\u00a029</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>March\u00a027</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>March\u00a08 </td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2022</td>\n<td>October\u00a011</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>September\u00a014</td>\n<td>Saskatchewan</td>\n<td>Elk (2\u00a0herds)</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>February\u00a025</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>January\u00a024</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>January\u00a05</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>December\u00a024</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>December\u00a021</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>November\u00a019</td>\n<td>Alberta</td>\n<td>White-tailed deer</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>October\u00a025</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>October\u00a013</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>September\u00a03</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>August\u00a024</td>\n<td>Alberta</td>\n<td>Elk (2 herds)</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>June\u00a04</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>April\u00a019</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>March\u00a03</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>February\u00a017</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>February\u00a022</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>February\u00a013</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>March\u00a05</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>March\u00a06</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>March\u00a020</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>May\u00a019</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>July\u00a07</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>July\u00a029</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>September\u00a030</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>October\u00a02</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>October\u00a014</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>October\u00a021</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>October\u00a021</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>October\u00a028</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>December\u00a010</td>\n<td>Alberta</td>\n<td>Elk (2 herds)</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>February\u00a028</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>June\u00a021</td>\n<td>Alberta</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>July\u00a026</td>\n<td>Alberta</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>January\u00a031</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>April\u00a010</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>September\u00a010</td>\n<td>Quebec</td>\n<td>Red deer</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>October\u00a01</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>October\u00a03</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>December\u00a020</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>December\u00a04</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>December\u00a01</td>\n<td>Saskatchewan</td>\n<td>White-tailed deer</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>November\u00a028</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>November\u00a016</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>August\u00a022</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>February\u00a013</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>November\u00a01</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>October\u00a03</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>February\u00a018</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>March\u00a021</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>May\u00a018</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>July\u00a021</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>November\u00a025</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>July\u00a016</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>June\u00a011</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>April\u00a09</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>March\u00a019</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>January\u00a016</td>\n<td>Alberta</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>December\u00a023</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>November\u00a028</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June\u00a05</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>May\u00a021</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>February\u00a03</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>November\u00a013</td>\n<td>Saskatchewan</td>\n<td>Deer and elk</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>April\u00a08</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>February\u00a018</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>January\u00a030</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>June\u00a011</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>April\u00a026</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>November\u00a022</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>May\u00a015</td>\n<td>Saskatchewan</td>\n<td>Elk</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>January\u00a019</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>January\u00a04</td>\n<td>Saskatchewan</td>\n<td>Deer</td>\n</tr>\n</tbody>\n</table>\n</div>\n\n<details>\n<summary><strong>Domestic cervid herds confirmed to be infected with CWD in Canada from 1996 to 2010</strong></summary>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<th>Year</th>\n<th>Number of herds confirmed with Chronic wasting disease</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>2010</td>\n<td>5</td>\n</tr>\n\n<tr>\n<td>2009</td>\n<td>2</td>\n</tr>\n<tr>\n<td>2008</td>\n<td>4</td>\n</tr>\n<tr>\n<td>2007</td>\n<td>6</td>\n</tr>\n<tr>\n<td>2006</td>\n<td>2</td>\n</tr>\n<tr>\n<td>2005</td>\n<td>0</td>\n</tr>\n<tr>\n<td>2004</td>\n<td>1</td>\n</tr>\n<tr>\n<td>2003</td>\n<td>1</td>\n</tr>\n<tr>\n<td>2002</td>\n<td>3</td>\n</tr>\n<tr>\n<td>2001</td>\n<td>21</td>\n</tr>\n<tr>\n<td>2000</td>\n<td>15</td>\n</tr>\n<tr>\n<td>1999</td>\n<td>0</td>\n</tr>\n<tr>\n<td>1998</td>\n<td>1</td>\n</tr>\n<tr>\n<td>1997</td>\n<td>0</td>\n</tr>\n<tr>\n<td>1996</td>\n<td>1</td>\n</tr>\n</tbody>\n</table>\n</div>\n</details>\n\n\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/cwd/eng/1330143462380/1330143991594\">Chronic wasting disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/federally-reportable-diseases-for-terrestrial-anim/eng/1329499145620/1329499272021\">Federally Reportable Diseases in Canada</a></li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La maladie d\u00e9bilitante chronique (MDC) est une maladie \u00e0 d\u00e9claration obligatoire en vertu du <i>R\u00e8glement sur la sant\u00e9 des animaux</i>. Cela signifie que tous les cas suspects doivent \u00eatre signal\u00e9s \u00e0 l'<a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">Agence canadienne d'inspection des aliments</a> (ACIA).</p>\n\n<p><strong>\u00c0 jour en date du\u00a0: 2024-02-29</strong></p>\n\n<h2 class=\"h4\">Les troupeaux de cervid\u00e9s domestiques dans lesquels la pr\u00e9sence de la MDC a \u00e9t\u00e9 confirm\u00e9e au Canada</h2>\n\n<div class=\"table-responsive\">\n<table class=\"wb-tables table table-striped table-hover\" data-wb-tables='{ \"pageLength\" : 25 , \"order\" : [0,\"desc\"] }'>\n\n<thead>\n<tr>\n<th>Ann\u00e9e</th>\n<th>Date de confirmation</th>\n<th>Lieu</th>\n<th>Type d'animal infect\u00e9</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>2024</td>\n<td>26\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>6\u00a0novembre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>2\u00a0ao\u00fbt</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>6\u00a0juin</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>6\u00a0juin</td>\n<td>Alberta</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>20\u00a0avril</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>29\u00a0mars</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>27\u00a0mars</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2023</td>\n<td>8\u00a0mars</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2022</td>\n<td>11\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>14\u00a0septembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti (2 troupeaux)</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>25\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>24\u00a0janvier</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n\n<tr>\n<td>2022</td>\n<td>5\u00a0janvier</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>24\u00a0d\u00e9cembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>21\u00a0d\u00e9cembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n\n\n<tr>\n<td>2021</td>\n<td>19\u00a0novembre</td>\n<td>Alberta</td>\n<td>Cerf de Virginie</td>\n</tr>\n\n\n<tr>\n<td>2021</td>\n<td>25\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>13\u00a0octobre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>3\u00a0septembre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2021</td>\n<td>24\u00a0ao\u00fbt</td>\n<td>Alberta</td>\n<td>Wapiti (2 troupeaux)</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>4\u00a0juin</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>19\u00a0avril</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>3\u00a0mars</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>17\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2021</td>\n<td>22\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>13\u00a0f\u00e9vrier</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>5\u00a0mars</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>6\u00a0mars</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>20\u00a0mars</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>19\u00a0mai</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>7\u00a0juillet</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2020</td>\n<td>29\u00a0juillet</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>30\u00a0septembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>2\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>14\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>21\u00a0octobre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>21\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>28\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2020</td>\n<td>10\u00a0d\u00e9cembre</td>\n<td>Alberta</td>\n<td>Wapiti (2 troupeaux)</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>28\u00a0f\u00e9vrier</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>21\u00a0juin</td>\n<td>Alberta</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>26\u00a0juillet</td>\n<td>Alberta</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>31\u00a0janvier</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>10\u00a0avril</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>10\u00a0septembre</td>\n<td>Qu\u00e9bec</td>\n<td>Cerf Rouge</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>1\u00a0octobre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>3\u00a0octobre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>20\u00a0d\u00e9cembre</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>4\u00a0d\u00e9cembre</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>1\u00a0d\u00e9cembre</td>\n<td>Saskatchewan</td>\n<td>Cerf de Virginie</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>28\u00a0novembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>16\u00a0novembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>22\u00a0ao\u00fbt</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>13\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>1\u00a0novembre</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>3\u00a0octobre</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n\n<tr>\n<td>2016</td>\n<td>18\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>21\u00a0mars</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>18\u00a0mai</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>21\u00a0juillet</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>25\u00a0novembre</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>16\u00a0juillet</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>11\u00a0juin</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>9\u00a0avril</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>19\u00a0\u00a0mars</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>16\u00a0janvier</td>\n<td>Alberta</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>23\u00a0d\u00e9cembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>28\u00a0novembre</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>5\u00a0juin</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>21\u00a0mai</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>3\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Wapiti</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>13\u00a0novembre</td>\n<td>Saskatchewan</td>\n<td>Cerf et wapiti</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>8\u00a0avril</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>18\u00a0f\u00e9vrier</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>30\u00a0janvier</td>\n<td>Saskatchewan</td>\n<td>Cerf</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>11\u00a0juin </td>\n<td>Saskatchewan </td>\n<td>Wapitis </td>\n</tr>\n<tr>\n<td>2012</td>\n<td>26\u00a0avril </td>\n<td>Saskatchewan </td>\n<td>Wapitis </td>\n</tr>\n<tr>\n<td>2011</td>\n<td>22\u00a0novembre </td>\n<td>Saskatchewan </td>\n<td>Cerfs </td>\n</tr>\n<tr>\n<td>2011</td>\n<td>15\u00a0mai </td>\n<td>Saskatchewan </td>\n<td>Wapitis </td>\n</tr>\n<tr>\n<td>2011</td>\n<td>19\u00a0janvier </td>\n<td>Saskatchewan </td>\n<td>Cerfs </td>\n</tr>\n<tr>\n<td>2011</td>\n<td>4\u00a0janvier </td>\n<td>Saskatchewan </td>\n<td>Cerfs </td>\n</tr>\n</tbody>\n</table>\n</div>\n\n<details>\n<summary><strong>Les troupeaux infect\u00e9s par la maladie d\u00e9bilitante chronique au Canada de 2010 \u00e0 1996</strong></summary>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr>\n<th>Ann\u00e9e</th>\n<th>Nombre de troupeaux infect\u00e9s par la MDC</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>2010</td>\n<td>5</td>\n</tr>\n<tr>\n<td>2009</td>\n<td>2</td>\n</tr>\n<tr>\n<td>2008</td>\n<td>4</td>\n</tr>\n<tr>\n<td>2007</td>\n<td>6</td>\n</tr>\n<tr>\n<td>2006</td>\n<td>2</td>\n</tr>\n<tr>\n<td>2005</td>\n<td>0</td>\n</tr>\n<tr>\n<td>2004</td>\n<td>1</td>\n</tr>\n<tr>\n<td>2003</td>\n<td>1</td>\n</tr>\n<tr>\n<td>2002</td>\n<td>3</td>\n</tr>\n<tr>\n<td>2001</td>\n<td>21</td>\n</tr>\n<tr>\n<td>2000</td>\n<td>15</td>\n</tr>\n<tr>\n<td>1999</td>\n<td>0</td>\n</tr>\n<tr>\n<td>1998</td>\n<td>1</td>\n</tr>\n<tr>\n<td>1997</td>\n<td>0</td>\n</tr>\n<tr>\n<td>1996</td>\n<td>1</td>\n</tr>\n</tbody>\n</table>\n</div>\n</details>\n\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mdc/fra/1330143462380/1330143991594\">Maladie d\u00e9bilitante chronique</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/maladies-a-declaration-obligatoire-pour-les-animau/fra/1329499145620/1329499272021\">Maladies \u00e0 d\u00e9claration obligatoire pour les animaux terrestres au Canada</a></li>\n</ul>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}