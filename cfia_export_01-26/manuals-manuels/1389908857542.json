{
    "dcr_id": "1389908857542",
    "title": {
        "en": "Nutrient content claims: reference information",
        "fr": "All\u00e9gations relatives \u00e0 la teneur <span class=\"nowrap\">nutritive :</span> renseignements de r\u00e9f\u00e9rence"
    },
    "html_modified": "2024-01-26 2:22:16 PM",
    "modified": "2023-06-01",
    "issued": "2014-01-16 16:47:39",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/manuals-manuels/data/lble_industry_claims_nutrient_reference_1389908857542_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/manuals-manuels/data/lble_industry_claims_nutrient_reference_1389908857542_fra"
    },
    "ia_id": "1389908896254",
    "parent_ia_id": "1389905991605",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling",
        "fr": "\u00c9tiquetage"
    },
    "commodity": {
        "en": "Alcoholic beverages|Dairy products|Egg, processed products|Egg, shell|Fish and seafood|Fruits and vegetables, fresh|Fruits and vegetables, processed products|Grain-based foods|Honey|Maple products|Meat products and food animals",
        "fr": "Aliments \u00e0 base de grains|Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande|Boissons alcoolis\u00e9es|Fruits et l\u00e9gumes, frais|Fruits et l\u00e9gumes, produits  transform\u00e9s|Miel|Oeufs, en coquille|Oeufs, produits transform\u00e9s|Poissons et fruits de mer|Produits d'\u00e9rable|Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1389905991605",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Nutrient content claims: reference information",
        "fr": "All\u00e9gations relatives \u00e0 la teneur <span class=\"nowrap\">nutritive :</span> renseignements de r\u00e9f\u00e9rence"
    },
    "label": {
        "en": "Nutrient content claims: reference information",
        "fr": "All\u00e9gations relatives \u00e0 la teneur <span class=\"nowrap\">nutritive :</span> renseignements de r\u00e9f\u00e9rence"
    },
    "templatetype": "content page 1 column",
    "node_id": "1389908896254",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "dcr_type": "manuals-manuels",
    "parent_dcr_id": "1389905941652",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/nutrient-content/reference-information/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/renseignements-de-reference/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Nutrient content claims: reference information",
            "fr": "All\u00e9gations relatives \u00e0 la teneur nutritive : renseignements de r\u00e9f\u00e9rence"
        },
        "description": {
            "en": "A reference table of foods to which vitamins, mineral nutrients and amino acids may or must be added, listing the specific nutrient and the regulatory reference. Examples on how to validate if nutrient content claim requirements are met.",
            "fr": "La table de r\u00e9f\u00e9rence des aliments auxquels les vitamines, min\u00e9raux et acides amin\u00e9s peuvent ou doivent \u00eatre ajout\u00e9es, \u00e9num\u00e9rant nutriments sp\u00e9cifiques et renvoi r\u00e9glementaire. Exemples de v\u00e9rification d'all\u00e9gations relatives \u00e0 la teneur nutritive."
        },
        "keywords": {
            "en": "food, food health, vitamins, mineral nutrients, amino acids, fat, fibre",
            "fr": "aliments, la sant\u00e9 des aliments, vitamines, min&#233;raux nutritifs, acides amin&#233;s, gras, fibre"
        },
        "dcterms.subject": {
            "en": "food,food labeling,inspection,nutrition,products",
            "fr": "aliment,\u00e9tiquetage des aliments,inspection,alimentation,produit"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-01-16 16:47:39",
            "fr": "2014-01-16 16:47:39"
        },
        "modified": {
            "en": "2023-06-01",
            "fr": "2023-06-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Nutrient content claims: reference information",
        "fr": "All\u00e9gations relatives \u00e0 la teneur nutritive : renseignements de r\u00e9f\u00e9rence"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/food-labels/labelling/industry/nutrient-content/reference-information/eng/1389908857542/1389908896254?chap=0\">Complete text </a></p>\r\n\r\n\r\n<h2>On this page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/reference-information/eng/1389908857542/1389908896254?chap=1\">Foods to which vitamins, mineral nutrients and amino acids may or must be added [D.03.002, <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/reference-information/eng/1389908857542/1389908896254?chap=2\">Nutrient content claim examples</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/reference-information/eng/1389908857542/1389908896254?chap=2#s1c2\">Nutrient content claim examples \u2013 fat claims</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrient-content/reference-information/eng/1389908857542/1389908896254?chap=2#s2c2\">Nutrient content claim example \u2013 fibre claims</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/renseignements-de-reference/fra/1389908857542/1389908896254?chap=0\">Texte complet</a></p>\r\n\r\n\r\n<h2>Sur cette page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/renseignements-de-reference/fra/1389908857542/1389908896254?chap=1\">Aliments auxquels des vitamines, des min\u00e9raux nutritifs et des acides amin\u00e9s peuvent ou doivent \u00eatre ajout\u00e9s [D.03.002, <abbr title=\"R\u00e8glement sur les aliments et les drogues\">RAD</abbr>]</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/renseignements-de-reference/fra/1389908857542/1389908896254?chap=2\">Exemples d'all\u00e9gations relatives \u00e0 la teneur nutritive</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/renseignements-de-reference/fra/1389908857542/1389908896254?chap=2#s1c2\">Exemples d'all\u00e9gations relatives \u00e0 la teneur nutritive \u2013 teneur en lipides</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/renseignements-de-reference/fra/1389908857542/1389908896254?chap=2#s2c2\">Exemple d'all\u00e9gation relative \u00e0 la teneur nutritive \u2013 teneur en fibres</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}