{
    "dcr_id": "1331151916451",
    "title": {
        "en": "Causes of food poisoning",
        "fr": "Causes des empoisonnements alimentaires"
    },
    "html_modified": "2024-01-26 2:21:02 PM",
    "modified": "2019-12-02",
    "issued": "2012-03-07 15:31:18",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_causes_foodborne_1331151916451_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_causes_foodborne_1331151916451_fra"
    },
    "ia_id": "1331152055552",
    "parent_ia_id": "1304966421147",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1304966421147",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Causes of food poisoning",
        "fr": "Causes des empoisonnements alimentaires"
    },
    "label": {
        "en": "Causes of food poisoning",
        "fr": "Causes des empoisonnements alimentaires"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331152055552",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1304966258994",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/food-poisoning/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/empoisonnements-alimentaires/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Causes of food poisoning",
            "fr": "Causes des empoisonnements alimentaires"
        },
        "description": {
            "en": "Food contaminated by bacteria, viruses and parasites (foodborne pathogens) can make you sick.",
            "fr": "Les aliments contamin\u00e9s par des bact\u00e9ries, des virus et des parasites (les pathog\u00e8nes alimentaires) peuvent provoquer une maladie d'origine alimentaire."
        },
        "keywords": {
            "en": "food, consumers, food safety, foodborne illness, illness, health, cooking, food poisoning",
            "fr": "aliments, consommateurs, salubrit&#233; des aliments, toxi-infection alimentaire, sant&#233;, cuisson, empoisonnements alimentaire"
        },
        "dcterms.subject": {
            "en": "bacteria,inspection,best practices,agri-food products,food safety,viruses",
            "fr": "bact\u00e9rie,inspection,meilleures pratiques,produit agro-alimentaire,salubrit\u00e9 des aliments,virus"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des sciences de la salubrit\u00e9 des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-07 15:31:18",
            "fr": "2012-03-07 15:31:18"
        },
        "modified": {
            "en": "2019-12-02",
            "fr": "2019-12-02"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Causes of food poisoning",
        "fr": "Causes des empoisonnements alimentaires"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Food contaminated by bacteria, viruses and parasites (foodborne pathogens) can make you sick. Some people can have foodborne illness, also known as \"food poisoning\", and not even know they have it. Many people experience nausea, vomiting and diarrhea. You should see a doctor as soon as possible if you think you have a foodborne illness.</p>\n<p>The Government of Canada estimates that there are about 4\u00a0million cases of foodborne illness in Canada every year. Many foodborne illnesses can be prevented by following these safe food-handling practices: clean, separate, cook and chill.</p>\n<p>You can report any concerns about restaurant food to your <a href=\"/food-safety-for-consumers/where-to-report-a-complaint/restaurants-and-food-services/eng/1323139279504/1323140830752\">local inspection authority</a>.</p>\n<h2>Common causes of food poisoning</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/campylobacter-jejuni.html\"><i lang=\"la\">Campylobacter jejuni</i></a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/botulism-clostridium-botulinum.html\"><i lang=\"la\">Clostridium botulinum</i></a></li>\n<li><a href=\"/food-safety-for-consumers/fact-sheets/food-poisoning/clostridium-perfringens/eng/1332280009004/1332280082990\"><i lang=\"la\">Clostridium perfringens</i></a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/cronobacter.html\"><i lang=\"la\">Cronobacter</i></a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/diseases/cyclosporiasis-cyclospora.html\"><i lang=\"la\">Cyclospora</i></a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/diseases/e-coli.html\"><i lang=\"la\"><abbr title=\"Escherichia\">E.</abbr>\u00a0coli</i> O157:H7</a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/hepatitis-a.html\">Hepatitis A and E </a></li>\n<li><i lang=\"la\"><a href=\"https://www.canada.ca/en/public-health/services/diseases/listeriosis.html\">Listeria monocytogenes</a></i></li>\n<li><a href=\"/food-safety-for-consumers/fact-sheets/specific-products-and-risks/fish-and-seafood/toxins-in-shellfish/eng/1332275144981/1332275222849\">Marine Toxins in Bivalve Shellfish</a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/norovirus.html\">Norovirus</a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/diseases/salmonellosis-salmonella.html\"><i lang=\"la\">Salmonella</i></a></li>\n<li><a href=\"/food-safety-for-consumers/fact-sheets/food-poisoning/scombroid/eng/1332280657698/1332280735024\">Scombroid Poisoning</a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/shigella.html\"><i lang=\"la\">Shigella</i></a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/food-poisoning/vibrio.html\"><i lang=\"la\">Vibrio</i></a></li>\n</ul>\n<p>If you believe a product presents a health and safety risk, you can <a href=\"/food-safety-for-consumers/where-to-report-a-complaint/eng/1364500149016/1364500195684\">report a potential food safety incident to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les aliments contamin\u00e9s par des bact\u00e9ries, des virus et des parasites (les pathog\u00e8nes alimentaires) peuvent provoquer une maladie d'origine alimentaire, aussi connue sous le nom d'\u00ab\u00a0intoxication ou empoisonnement alimentaire\u00a0\u00bb. Ces maladies peuvent se manifester chez certaines personnes qui ne savent m\u00eame pas qu'elles en sont atteintes. Bon nombre de personnes souffrent de naus\u00e9es, de vomissements et de diarrh\u00e9e. Consultez un m\u00e9decin le plus t\u00f4t possible si vous pensez avoir contract\u00e9 une maladie d'origine alimentaire.</p>\n<p>Le gouvernement du Canada estime \u00e0 environ 4\u00a0millions le nombre des cas de maladie d'origine alimentaire au Canada chaque ann\u00e9e. Bon nombre de ces cas peuvent \u00eatre \u00e9vit\u00e9s en utilisant des pratiques s\u00fbres de manipulation des aliments\u00a0: nettoyer, s\u00e9parer, cuire et r\u00e9frig\u00e9rer.</p>\n<p>Vous pouvez \u00e9galement signaler toute pr\u00e9occupation au sujet d'aliments consomm\u00e9s dans un restaurant aux <a href=\"/salubrite-alimentaire-pour-les-consommateurs/ou-signaler-une-plainte/restaurants-et-services-alimentaires/fra/1323139279504/1323140830752\">autorit\u00e9s d'inspection locales</a>.</p>\n<h2>Causes communes des empoisonnements alimentaires</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/campylobacter-jejuni.html\"><i lang=\"la\">Campylobacter jejuni</i></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/botulisme-clostridium-botulinum.html\"><i lang=\"la\">Clostridium botulinum</i></a></li>\n<li><a href=\"/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/empoisonnements-alimentaires/clostridium-perfringens/fra/1332280009004/1332280082990\"><i lang=\"la\">Clostridium perfringens</i></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/cronobacter.html\"><i lang=\"la\">Cronobacter</i></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/cyclosporose-cyclospora.html\"><i lang=\"la\">Cyclospora</i></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/e-coli.html\"><i lang=\"la\"><abbr title=\"Escherichia\">E.</abbr> coli</i> O157:H7</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/hepatite-a.html\">H\u00e9patite A et E</a></li>\n<li><a href=\"/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/empoisonnements-alimentaires/scombroides/fra/1332280657698/1332280735024\">Intoxication par des scombro\u00efdes</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/listeriose.html\"><i lang=\"la\">Listeria monocytogenes</i></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/norovirus.html\">Norovirus</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/salmonellose-salmonella.html\"><i lang=\"la\">Salmonella</i></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/shigella.html\"><i lang=\"la\">Shigella</i></a></li>\n<li><a href=\"/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/poisson-et-produits-de-mer/biotoxines-dans-les-mollusques/fra/1332275144981/1332275222849\">Toxines marines dans les mollusques bivalves</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/intoxication-alimentaire/bacterie-vibrio.html\"><i lang=\"la\">Vibrio</i></a></li>\n</ul>\n<p>Si vous croyez qu'un produit pr\u00e9sente un risque en mati\u00e8re de sant\u00e9 ou de salubrit\u00e9, <a href=\"/salubrite-alimentaire-pour-les-consommateurs/ou-signaler-une-plainte/fra/1364500149016/1364500195684\">vous pouvez signaler l'incident \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}