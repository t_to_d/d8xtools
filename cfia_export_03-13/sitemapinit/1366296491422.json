{
    "dcr_id": "1366296427764",
    "title": {
        "en": "Complaints process",
        "fr": "Processus de traitement des plaintes"
    },
    "html_modified": "2024-03-12 3:56:14 PM",
    "modified": "2022-10-11",
    "issued": "2013-04-18 10:47:10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_complaints_process_1366296427764_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_complaints_process_1366296427764_fra"
    },
    "ia_id": "1366296491422",
    "parent_ia_id": "1547179421595",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1547179421595",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Complaints process",
        "fr": "Processus de traitement des plaintes"
    },
    "label": {
        "en": "Complaints process",
        "fr": "Processus de traitement des plaintes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1366296491422",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1547179421299",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/complaints-and-appeals/process/",
        "fr": "/a-propos-de-l-acia/bureau-des-plaintes-et-des-appels/processus/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Complaints process",
            "fr": "Processus de traitement des plaintes"
        },
        "description": {
            "en": "The Complaints and Appeals Office (CAO) accepts comments, compliments and complaints from anyone who has dealings with the Canadian Food Inspection Agency, on matters related to the quality of the CFIA?s service delivery or decisions made by CFIA staff which are typically regulatory in nature. This includes regulated parties, stakeholders, and members of the public.",
            "fr": "Toute personne qui a des relations d'affaires avec l'Agence canadienne d'inspection des aliments (ACIA) peut pr\u00e9senter au Bureau de traitement des plaintes et des appels (BPA) des commentaires, des compliments ou des plaintes concernant la qualit\u00e9 du service de l'ACIA ou les d\u00e9cisions prises par le personnel de l'ACIA, qui sont typiquement de nature r\u00e9glementaire."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, complains process, Complaints and Appeals Office",
            "fr": "Agence canadienne d&#39;inspection des aliments, processus de traitement des plaintes, Bureau de traitement des plaintes et des appels"
        },
        "dcterms.subject": {
            "en": "government information,inspection,government publication",
            "fr": "information gouvernementale,inspection,publication gouvernementale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Secr\u00e9tariat de l'int\u00e9grit\u00e9 et des recours"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-04-18 10:47:10",
            "fr": "2013-04-18 10:47:10"
        },
        "modified": {
            "en": "2022-10-11",
            "fr": "2022-10-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Complaints process",
        "fr": "Processus de traitement des plaintes"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<h2 class=\"h3\">The Complaints and Appeals Office (CAO) does not have the legislative authority to reconsider, confirm, vary or cancel a regulatory decision</h2>\n<p>The CAO continues to review service-related complaints and may assist in clarifying applicable regulatory requirements and CFIA policies. Regulated parties that disagree with a regulatory decision should continue to discuss their concerns with a CFIA inspector.</p>\n</div>\n\n<h2 class=\"h5\">Are there complaints that the Complaints and Appeals Office (CAO) will not review?</h2>\n\n<p>The CAO will triage your complaint to determine whether it falls within the CAO mandate and can therefore be reviewed. Your complaint will fall into one of the following categories:</p>\n\n<ol class=\"lst-lwr-alph lst-spcd\">\n<li><strong>Existing Process</strong>\u00a0\u2013\u00a0when the CAO determines there is already an existing mechanism to deal with the complaint. Examples include:\n<ul>\n<li>an appeal of a compensation decision under the <i>Health of Animals Act</i> or <i>Plant Protection Act</i>,</li>\n<li>an appeal of decisions made under the <i>Administrative Monetary Penalties Act</i>,</li>\n<li>a food complaint from a consumer (food safety or non-food safety),</li>\n<li>a trade complaint made by industry or a competitor.</li>\n</ul></li>\n\n<li><strong>No review</strong>\u00a0\u2013\u00a0when the CAO determines that they can not investigate the matter. These are generally issues that are already before the courts or under CFIA investigation for potential prosecution. In addition, they could be events that occurred more than 12 months in the past.</li>\n<li><strong>Review</strong>\u00a0\u2013\u00a0when the CAO determines the complaint relates to CFIA quality of service or regulatory program or other decision made by CFIA officials. These generally include matters of staff behaviour, undue delays, poor or misleading information, mistakes, administrative errors, decisions related to licensing, registration, permits, orders, inspection results, seizure, labelling and other items.</li>\n</ol>\n\n<h2 class=\"h5\">Can I remain anonymous?</h2>\n\n<p>The CAO will make every attempt to treat your complaint anonymously if it is your preference to do so. This decision is taken on a case by case basis as it may not always be possible to carry out a comprehensive review without full disclosure of information such as your name, your company's name, and your paperwork (for example, the relevant export certificate).</p>\n\n<p>The CAO will only share information about your file with the CFIA on a \"need to know\" basis. Sharing information may be required to allow us to carry out our duties and perform a thorough review. For example, if we are asking the CFIA for information about an incident that you have reported to us we may need to disclose some information about your complaint in order to obtain the CFIA perspective on the matter.</p>\n\n<p>All of the information that you provide to us is subject to the <i>Access to Information Act</i> and the <i>Privacy Act</i>. The <a href=\"/english/reg/jredirect2.shtml?aia\"><i>Access to Information Act</i></a> gives Canadian citizens the right to access information in federal government records. The <a href=\"/english/reg/jredirect2.shtml?pa\"><i>Privacy Act</i></a> gives Canadian citizens, as well as individuals present in Canada, the right to access their personal information which is held by the Government and protection of that information against unauthorized use and disclosure.</p>\n\n<h2>Reviewing your complaint</h2>\n\n<p>After receiving your complaint we will assign a file number and a Complaints Analyst to your file. A Complaints Analyst will request a call with you to gather more information about your complaint and to ensure that they have a good understanding of all of the details.</p>\n\n<p>Once your complaint is thoroughly understood and has been reviewed, the Complaints Analyst will conduct a preliminary inquiry within the CFIA which may include:</p>\n\n<ul>\n<li>asking the Agency technical specialists to provide their perspective on the issue</li>\n<li>reviewing relevant technical documents provided (e.g., procedures, policies, regulations, phytosanitary certificates, notices of removal, inspection files etc.)</li>\n<li>meeting with Agency staff to explore any options for addressing the issue</li>\n</ul>\n\n<p>Once all of the relevant information has been gathered and you and the Agency contact have had an opportunity to provide comments, the CAO will consider both viewpoints before making a determination about the incident. We may find that the CFIA's actions or decisions were reasonable, that your matter has been adequately addressed or we may make recommendations to the CFIA on reconsidering your matter. You and the Agency management will be informed of the decision and the rationale for our findings.</p>\n\n<p>The CAO is an impartial office. After reviewing all information and documents/policies that are relevant to your issue, we will determine if CFIA decisions and actions are being implemented consistently as per the established procedures, in a timely manner and professionally as outlined in the CFIA's <a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/rights-and-services/message/eng/1326320466901/1326320527397\">Statement of Rights and Service</a>.</p>\n<p>The CAO is currently operating as an administrative redress office which means that we do not have the legal authority to amend specific CFIA decisions. If the CAO\u00a0\u2013\u00a0through the review process\u00a0\u2013\u00a0identifies a specific CFIA decision that should be amended or reconsidered, we advise CFIA management. A request for review by the CAO does not change or stay the CFIA decision under review. The decision remains valid and enforceable while the review is taking place.</p>\n\n<p>Occasionally, we may find after speaking separately with the complainant and the Agency that the best next step is to facilitate discussions between the complainant and the CFIA. Once communications are restored and there is a willingness to work together to resolve a matter, we will close the file to allow the complainant and the Agency to continue working toward a successful resolution on their own. All complainants are invited to return to the CAO if they remain dissatisfied after their discussions with the way the CFIA has addressed or failed to address their complaint.</p>\n\n<h2>Closing your complaint</h2>\n\n<p>Following the review of your file the CAO will contact you by phone with information, options, next steps and final decisions. We will send this same information to you in writing following the closing phone call. Once you receive your letter outlining the outcome, you may have additional comments. You can contact our office and we will work to find answers for you.</p>\n\n<h2 class=\"h5\">What happens after my complaint is closed?</h2>\n\n<p>The Chief Redress Officer will sometimes highlight potential opportunities for improvement to CFIA's senior management. These recommendations could range from the identification of policies or procedures that need to be updated or clarified to areas that could benefit from improved service delivery.</p>\n\n<h2>Timeframes</h2>\n\n<p>We will try to resolve your complaint as quickly as possible.</p>\n\n<p>Within two business days we will send you an acknowledgement that we received your complaint and assign you a file number for future reference.</p>\n\n<p>We take all complaints seriously and we will keep you up to date with regular feedback throughout the review of your matter. The length of the review will depend on the level of complexity of the matter and the availability of the complainant and CFIA officials to contribute to the information gathering stage of the review.</p>\n\n<p>You can find further information on our service standards here: <a href=\"/about-the-cfia/complaints-and-appeals/process/service-standards/eng/1432733804664/1432733805883\">Complaints and Appeals Office (CAO) Service Standards</a>.</p>\n\n<h2>Submit online</h2>\n\n<p>Access the form <a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5686/eng/1452187949770/1452187950696\">\"Complaints, Comments and Compliments\" form (CFIA/ACIA 5686)</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n <div class=\"alert alert-info\">\n<h2 class=\"h3\">Le Bureau des plaintes et des appels (BPA) n'a pas l'autorit\u00e9 l\u00e9gislative de reconsid\u00e9rer, de confirmer, de varier ou d'annuler une d\u00e9cision de r\u00e9glementation</h2>\n<p>Le BPA continue d'examiner les plaintes li\u00e9es au service et d'aider \u00e0 clarifier les exigences r\u00e9glementaires applicables et les politiques de l'ACIA. Les parties r\u00e9glementaires qui sont en d\u00e9saccord avec une d\u00e9cision r\u00e9glementaire devraient continuer \u00e0 partager leurs pr\u00e9occupations avec un inspecteur de l'ACIA.</p>\n</div> \n\n<h2 class=\"h5\">Y a-t-il des plaintes que le Bureau des plaintes et des appels (BPA) n'examine pas?</h2>\n\n<p>Le BPA v\u00e9rifiera votre plainte afin de d\u00e9terminer si elle rel\u00e8ve de son mandat et si elle peut alors \u00eatre examin\u00e9e. Votre plainte sera class\u00e9e dans l'une des cat\u00e9gories suivantes\u00a0:</p>\n\n<ol class=\"lst-lwr-alph lst-spcd\">\n<li><strong>Processus existant</strong>\u00a0\u2013\u00a0Le BPA d\u00e9termine qu'il existe d\u00e9j\u00e0 un m\u00e9canisme pour r\u00e9gler la plainte. En voici des exemples\u00a0:\n\n<ul>\n<li>un appel d'une d\u00e9cision relative \u00e0 l'indemnisation en vertu de la <i>Loi sur la sant\u00e9 des animaux</i> ou de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>;</li>\n<li>un appel d'une d\u00e9cision prise en vertu de la <i>Loi sur les sanctions administratives p\u00e9cuniaires</i>;</li>\n<li>une plainte d'un consommateur concernant un produit alimentaire (li\u00e9e ou non \u00e0 la salubrit\u00e9 des aliments);</li>\n<li>une plainte faite par l'industrie ou un comp\u00e9titeur.</li>\n</ul>\n</li>\n<li><strong>Aucun examen</strong>\u00a0\u2013\u00a0Le BPA d\u00e9termine qu'il ne peut pas enqu\u00eater sur la situation. Il s'agit g\u00e9n\u00e9ralement de cas qui sont d\u00e9j\u00e0 devant les tribunaux ou qui font l'objet d'une enqu\u00eate de l'ACIA en vue d'\u00e9ventuelles poursuites judiciaires. Il pourrait aussi s'agir d'\u00e9v\u00e9nements qui ont eu lieu il y a plus de 12 mois.</li>\n<li><strong>Examen</strong>\u00a0\u2013\u00a0Le BPA d\u00e9termine que la plainte porte sur la qualit\u00e9 du service ou le programme de r\u00e9glementation de l'ACIA, ou une d\u00e9cision prise par le personnel de l'ACIA. Cela peut comprendre le comportement du personnel, les retards injustifi\u00e9s, les renseignements inexacts ou trompeurs, les erreurs, les erreurs administratives, ainsi que les d\u00e9cisions relatives aux licences, aux enregistrements, aux permis, aux ordonnances, aux r\u00e9sultats d'inspection, aux saisies, \u00e0 l'\u00e9tiquetage et autres.</li>\n</ol>\n\n<h2 class=\"h5\">Puis-je demeurer anonyme?</h2>\n\n<p>Si vous le demandez, nous ferons notre possible pour traiter votre  plainte de fa\u00e7on anonyme. Dans certaines situations, il peut \u00eatre impossible de  faire un examen exhaustif sans divulguer vos renseignements (p. ex. nom, nom  d'entreprise, etc.) aux fonctionnaires de l'ACIA. Cette d\u00e9cision sera prise au  cas par cas.</p>\n\n<p>  Le BPA ne partagera les renseignements sur votre dossier avec l'ACIA que  s'il existe un \u00ab\u00a0besoin de conna\u00eetre\u00a0\u00bb. Le partage d'information peut  \u00eatre n\u00e9cessaire pour nous permettre de nous acquitter de nos fonctions et de  mener un examen approfondi. Par exemple, si nous demandons \u00e0 l'ACIA de  l'information sur un incident que vous nous avez signal\u00e9, nous devrons  peut-\u00eatre divulguer de l'information sur votre plainte afin d'obtenir le point  de vue de l'ACIA sur le sujet.</p>\n\n<p>Tous les renseignements que vous nous fournissez sont assujettis \u00e0 la <i>Loi sur l'acc\u00e8s \u00e0 l'information</i> et \u00e0 la <i>Loi sur la protection des renseignements personnels</i>. La <a href=\"/francais/reg/jredirect2.shtml?aia\"><i>Loi sur l'acc\u00e8s \u00e0 l'information</i></a> conf\u00e8re aux citoyens canadiens un droit d'acc\u00e8s \u00e0 l'information contenue dans les dossiers du gouvernement f\u00e9d\u00e9ral. La <a href=\"/francais/reg/jredirect2.shtml?pa\"><i>Loi sur la protection des renseignements personnels</i></a> conf\u00e8re aux citoyens canadiens, ainsi qu'aux personnes pr\u00e9sentes au Canada, un droit d'acc\u00e8s \u00e0 l'information personnelle que le gouvernement poss\u00e8de \u00e0 leur sujet et prot\u00e8ge cette information contre toute divulgation non autoris\u00e9e.</p>\n\n<h2>Examen de votre plainte</h2>\n\n<p>Lorsque nous recevrons votre plainte, nous assignerons un num\u00e9ro et un analyste des plaintes \u00e0 votre dossier. L'analyste des plaintes communiquera avec vous pour obtenir de plus amples renseignements concernant votre plainte et assurer une bonne compr\u00e9hension de tous les d\u00e9tails.</p>\n<p>Une fois qu'il aura bien examin\u00e9 votre plainte, l'analyste m\u00e8nera une enqu\u00eate pr\u00e9liminaire au sein de l'ACIA, ce qui pourrait inclure les mesures suivantes\u00a0:</p>\n<ul>\n<li>demander aux sp\u00e9cialistes techniques de l'Agence leur point de vue sur la question;</li>\n<li>examiner les documents techniques fournis (p. ex. proc\u00e9dures, politiques, r\u00e8glements, certificats phytosanitaires, avis de retrait, fichiers d'inspection, etc.);</li>\n<li>rencontrer le personnel de l'Agence pour examiner les options en vue de r\u00e9gler la situation.</li>\n</ul>\n\n<p>Une fois que tous les renseignements pertinents ont \u00e9t\u00e9 recueillis et que vous et la personne ressource avez eu l'occasion de fournir des commentaires, le BPA examinera les deux points de vue avant de se prononcer sur la question. Le BPA pourrait juger que les mesures ou les d\u00e9cisions de l'ACIA \u00e9taient raisonnables, d\u00e9terminer que votre plainte a \u00e9t\u00e9 r\u00e9gl\u00e9e de mani\u00e8re satisfaisante, ou recommander \u00e0 l'ACIA de r\u00e9examiner votre plainte. Vous et la direction de l'Agence serez inform\u00e9s de notre d\u00e9cision, ainsi que de la justification de notre d\u00e9cision.</p>\n<p>Le BPA est un bureau impartial. Apr\u00e8s avoir examin\u00e9 tous les renseignements, politiques et documents pertinents, nous d\u00e9terminerons si les d\u00e9cisions et les mesures de l'ACIA sont mises en \u0153uvre de fa\u00e7on uniforme conform\u00e9ment aux proc\u00e9dures \u00e9tablies, et en temps opportun et de mani\u00e8re professionnelle comme il est indiqu\u00e9 dans l'<a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/droits-et-services/message/fra/1326320466901/1326320527397\">\u00c9nonc\u00e9 des droits et des services</a> de l'ACIA.</p>\n<p>Le BPA sert actuellement de bureau de recours administratif, ce qui veut dire qu'il n'a pas l'autorisation l\u00e9gale de modifier des d\u00e9cisions pr\u00e9cises de l'ACIA. Si, durant le processus d'examen, le BPA d\u00e9termine qu'une d\u00e9cision particuli\u00e8re de l'ACIA doit \u00eatre modifi\u00e9e ou r\u00e9examin\u00e9e, il en informe la direction de l'ACIA. Une demande d'examen par le BPA ne modifie ni suspend la d\u00e9cision de l'ACIA faisant l'objet de l'examen. La d\u00e9cision demeure valide et en vigueur durant l'examen.</p>\n\n<p>Parfois, nous d\u00e9terminerons, apr\u00e8s avoir parl\u00e9 s\u00e9par\u00e9ment au plaignant et \u00e0 l'Agence que la mesure la plus appropri\u00e9e consiste \u00e0 faciliter des discussions entre le plaignant et l'ACIA. Une fois que la communication a \u00e9t\u00e9 r\u00e9tablie et que les parties sont pr\u00eates \u00e0 travailler ensemble pour r\u00e9gler la situation, nous fermerons le dossier pour permettre au plaignant et \u00e0 l'Agence de parvenir eux-m\u00eames \u00e0 une solution. Tous les plaignants sont invit\u00e9s \u00e0 revenir au BPA si, apr\u00e8s les discussions, ils demeurent insatisfaits de la fa\u00e7on dont l'ACIA a abord\u00e9 leur plainte.</p>\n\n<h2>Fermeture d'une plainte</h2>\n\n<p>Apr\u00e8s l'examen de votre dossier, le BPA communiquera avec-vous par t\u00e9l\u00e9phone afin de vous donner de l'information et ensuite expliquer les options, les prochaines \u00e9tapes et la d\u00e9cision d\u00e9finitive. Nous vous enverrons ces m\u00eames renseignements par \u00e9crit apr\u00e8s l'appel de fermeture. Lorsque vous recevrez votre lettre d\u00e9crivant le r\u00e9sultat de la plainte, vous pourriez avoir d'autres commentaires. Vous pouvez communiquer avec notre bureau et nous trouverons les r\u00e9ponses pour vous.</p>\n\n<h2>Qu'arrive-t-il une fois que mon dossier est ferm\u00e9?</h2>\n\n<p>Le chef des recours signalera parfois des possibilit\u00e9s d'am\u00e9lioration \u00e0 la haute direction de l'ACIA. Ces recommandations vont de la mise \u00e0 jour ou de la clarification de politiques et de proc\u00e9dures, \u00e0 l'am\u00e9lioration de la prestation de services dans certains secteurs.</p>\n\n<h2>D\u00e9lais</h2>\n\n<p>Nous essayerons de r\u00e9gler votre plainte le plus rapidement possible.</p>\n\n<p>Dans les deux jours ouvrables suivant la r\u00e9ception de votre plainte, nous vous enverrons un accus\u00e9 r\u00e9ception et vous donnerons un num\u00e9ro de dossier pour consultation future.</p>\n\n<p>Nous prenons toutes les plaintes au s\u00e9rieux et nous vous tiendrons au courant au moyen de r\u00e9troactions r\u00e9guli\u00e8res tout au long de l'examen de votre plainte. La dur\u00e9e de l'examen d\u00e9pendra de la complexit\u00e9 de la situation et de la disponibilit\u00e9 du plaignant et des fonctionnaires de l'ACIA durant l'\u00e9tape de collecte de renseignements.</p>\n\n<p>Vous pouvez trouver plus d'informations sur nos normes de service ici\u00a0: <a href=\"/a-propos-de-l-acia/bureau-des-plaintes-et-des-appels/processus/normes-de-service/fra/1432733804664/1432733805883\">Bureau des plaintes et des appels (BPA) Normes de service</a>.</p>\n\n<h2>Soumettre par courriel</h2>\n\n<p>Acc\u00e9dez au formulaire <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5686/fra/1452187949770/1452187950696\">\u00ab\u00a0Plaintes, commentaires et compliments\u00a0\u00bb (CFIA/ACIA 5686)</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}