{
    "dcr_id": "1362510710821",
    "title": {
        "en": "Disease caused by <i lang=\"la\">Bonamia ostreae</i>",
        "fr": "Maladie caus\u00e9e par <i lang=\"la\">Bonamia ostreae</i>"
    },
    "html_modified": "2024-01-26 2:21:43 PM",
    "modified": "2013-03-06",
    "issued": "2013-03-05 14:11:52",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_bonamia_ostreae_index_1362510710821_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_bonamia_ostreae_index_1362510710821_fra"
    },
    "ia_id": "1362510995727",
    "parent_ia_id": "1322941111904",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1322941111904",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Disease caused by <i lang=\"la\">Bonamia ostreae</i>",
        "fr": "Maladie caus\u00e9e par <i lang=\"la\">Bonamia ostreae</i>"
    },
    "label": {
        "en": "Disease caused by <i lang=\"la\">Bonamia ostreae</i>",
        "fr": "Maladie caus\u00e9e par <i lang=\"la\">Bonamia ostreae</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1362510995727",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1322940971192",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/bonamia-ostreae/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/bonamia-ostreae/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Disease caused by Bonamia ostreae",
            "fr": "Maladie caus\u00e9e par Bonamia ostreae"
        },
        "description": {
            "en": "Bonamia ostreae is a protozoan that infects molluscs. Bonamia ostreae belongs to the phylum Haplosporidia.",
            "fr": "Bonamia ostreae est un protozoaire qui infecte les mollusques. Il appartient au phylum Haplosporidia."
        },
        "keywords": {
            "en": "Bonamia ostreae, protozoan, reportable disease, aquatics",
            "fr": "Bonamia ostreae, protozoaire, maladie d&#233;clarable, aquatique"
        },
        "dcterms.subject": {
            "en": "crustaceans,inspection,infectious diseases,molluscs",
            "fr": "crustac\u00e9,inspection,maladie infectieuse,mollusque"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-03-05 14:11:52",
            "fr": "2013-03-05 14:11:52"
        },
        "modified": {
            "en": "2013-03-06",
            "fr": "2013-03-06"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Disease caused by Bonamia ostreae",
        "fr": "Maladie caus\u00e9e par Bonamia ostreae"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><i lang=\"la\">Bonamia ostreae</i> is a protozoan that infects molluscs. <i lang=\"la\">Bonamia ostreae</i> belongs to the phylum Haplosporidia.</p>\n<p>In Canada, <i lang=\"la\">Bonamia ostreae</i> is a <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/eng/1322940971192/1322941111904\">federally reportable disease</a>. This means that anyone who owns or works with aquatic animals, who knows of or suspects <i lang=\"la\">Bonamia ostreae</i> disease in the animals that they own or work with, is required by law to notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/bonamia-ostreae/fact-sheet/eng/1362582424837/1362582620961\">Fact Sheet \u2013 <i lang=\"la\">Bonamia ostreae</i></a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><i lang=\"la\">Bonamia ostreae</i> est un protozoaire qui infecte les mollusques. Il appartient au phylum Haplosporidia.</p>\n<p>Au Canada, <i lang=\"la\">Bonamia ostreae</i> est une <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/fra/1299156296625/1320599059508\">maladie \u00e0 d\u00e9claration obligatoire</a>. Par cons\u00e9quent, quiconque poss\u00e8de des animaux aquatiques, ou s'en occupe, et soup\u00e7onne, ou d\u00e9c\u00e8le, la pr\u00e9sence de <i lang=\"la\">Bonamia ostreae</i> chez les animaux qu'il poss\u00e8de ou dont il s'occupe est tenu par la loi d'en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/bonamia-ostreae/fiche-de-renseignements/fra/1362582424837/1362582620961\">Fiche de renseignements \u2013 <i lang=\"la\">Bonamia ostreae</i></a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}