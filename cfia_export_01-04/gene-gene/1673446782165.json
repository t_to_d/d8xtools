{
    "dcr_id": "1673446782165",
    "title": {
        "en": "Notice of intent: Amendment to the Safe Food for Canadians Regulations",
        "fr": "Avis d'intention\u00a0: Modification du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
    },
    "html_modified": "16-1-2023",
    "modified": "16-1-2023",
    "issued": "12-1-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/sfcr_amendment_notice_of_intent_1673446782165_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/sfcr_amendment_notice_of_intent_1673446782165_fra"
    },
    "parent_ia_id": "1299849093611",
    "ia_id": "1673446782946",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice of intent: Amendment to the Safe Food for Canadians Regulations",
        "fr": "Avis d'intention\u00a0: Modification du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
    },
    "label": {
        "en": "Notice of intent: Amendment to the Safe Food for Canadians Regulations",
        "fr": "Avis d'intention\u00a0: Modification du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1673446782946",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1299849033508",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/acts-and-regulations/regulatory-initiatives-notices-of-intent/amendment-of-sfcr/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/initiatives-reglementaires-avis-d-intention/modification-du-rsac/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice of intent: Amendment to the Safe Food for Canadians Regulations",
            "fr": "Avis d'intention\u00a0: Modification du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
        },
        "description": {
            "en": "This notice is to inform stakeholders of the Canadian Food Inspection Agency's (CFIA) intent to amend the Safe Food for Canadians Regulations (SFCR) to address the unique interprovincial context of the city of Lloydminster.",
            "fr": "Le pr\u00e9sent avis vise \u00e0 informer les parties int\u00e9ress\u00e9es de l'intention de l'Agence canadienne d'inspection des aliments (ACIA) de modifier le R\u00e8glement sur la salubrit\u00e9 des aliments au Canada (RSAC) afin de tenir compte du contexte interprovincial unique de la ville de Lloydminster."
        },
        "keywords": {
            "en": "information, Consultation, public, transparent government, Notice of intent, Amendments, Safe Food for Canadians Regulations, SFCR, Saskatchewan, Lloydminster",
            "fr": "information, Consultation, publique, transparence du gouvernement, avis d'intention, modifications, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, Saskatchewan, Lloydminster"
        },
        "dcterms.subject": {
            "en": "consumer protection,consumers,food,food safety,government information,government publication,inspection",
            "fr": "protection du consommateur,consommateur,aliment,salubrit\u00e9 des aliments,information gouvernementale,publication gouvernementale,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-01-16",
            "fr": "2023-01-16"
        },
        "modified": {
            "en": "2023-01-16",
            "fr": "2023-01-16"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice of intent: Amendment to the Safe Food for Canadians Regulations",
        "fr": "Avis d'intention\u00a0: Modification du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Purpose</h2>\n<p>This notice is to inform stakeholders of the Canadian Food Inspection Agency's (CFIA) intent to amend the <i>Safe Food for Canadians Regulations</i> (SFCR) to address the unique interprovincial context of the city of Lloydminster. This amendment would make Saskatchewan (SK) and Alberta (AB) businesses not subject to the specific provisions of the SFCR relating to interprovincial trade as long as the trade outside the province is limited to the city of Lloydminster. This would allow such food businesses to prepare food for trade into all of the city of Lloydminster, including the part of the city that is not within their province, without having to hold an Safe Food for Canadians licence and be subject to related requirements. SK and AB food businesses preparing food for export or interprovincial trade outside the city of Lloydminster would continue to be subject to all SFCR requirements including licensing.</p>\n\n<h2>Drivers to amend the <i>Safe Food for Canadians Regulations</i></h2>\n<p>The city of Lloydminster is unique in that it is situated in both SK and AB. SK and AB food businesses that prepare food to be sold to the part of Lloydminster in the other province are subject to the interprovincial trade provisions of the SFCR. The goal of the regulatory amendment is for safe foods in the city of Lloydminster to move similarly as in cities that are not split by provincial boundaries. This amendment would not apply to SK and AB food businesses that prepare food for export or interprovincial trade beyond the city limits of Lloydminster.</p> \n\n<p>The <i>Food and Drugs Act</i> and <i>Food and Drug Regulations</i> continue to apply to all foods sold in Canada. Foods sold in SK and AB are also subject to their respective applicable provincial legislation.</p> \n\n\n<h2>Related information</h2>\n<ul>\n<li><a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a></li>\n<li><a href=\"/food-licences/food-business-activities/eng/1524074697160/1524074697425\">Food business activities that require a licence under the <i>Safe Food for Canadians Regulations</i></a></li>\n</ul>\n\n<h2>Contact us</h2>\n<p>If you wish to provide any comments or have questions related to this notice, please submit them to:</p>\n<p>Natasha Richard<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:natasha.richard@inspection.gc.ca\">natasha.richard@inspection.gc.ca</a></p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Objet</h2>\n<p>Le pr\u00e9sent avis vise \u00e0 informer les parties int\u00e9ress\u00e9es de l'intention de l'Agence canadienne d'inspection des aliments (ACIA) de modifier le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC) afin de tenir compte du contexte interprovincial unique de la ville de <span lang=\"en\">Lloydminster</span>. Cette modification ferait en sorte que les entreprises de la Saskatchewan et de l'Alberta ne seraient pas assujetties aux dispositions sp\u00e9cifiques du RSAC relatives au commerce interprovincial lorsque le commerce \u00e0 l'ext\u00e9rieur de la province se limite \u00e0 la ville de <span lang=\"en\">Lloydminster</span>. Cela permettrait \u00e0 ces entreprises alimentaires de pr\u00e9parer des aliments destin\u00e9s au commerce dans l'ensemble de la ville de <span lang=\"en\">Lloydminster</span>, y compris la partie de la ville qui ne se trouve pas dans leur province, sans avoir \u00e0 d\u00e9tenir une licence pour la salubrit\u00e9 des aliments au Canada et \u00e0 \u00eatre assujetties aux exigences connexes. Les entreprises alimentaires de la Saskatchewan et de l'Alberta qui pr\u00e9parent des aliments destin\u00e9s \u00e0 l'exportation ou au commerce interprovincial \u00e0 l'ext\u00e9rieur de la ville de Lloydminster continueraient d'\u00eatre assujetties \u00e0 toutes les exigences du RSAC, y compris l'obtention d'une licence.</p>\n\n<h2>Facteurs motivant la modification du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></h2>\n<p>La ville de <span lang=\"en\">Lloydminster</span> est unique en ce sens qu'elle est situ\u00e9e \u00e0 la fois en Saskatchewan et en Alberta. Les entreprises alimentaires de la Saskatchewan et de l'Alberta qui pr\u00e9parent des aliments destin\u00e9s \u00e0 \u00eatre vendus \u00e0 la partie de <span lang=\"en\">Lloydminster</span> situ\u00e9e dans l'autre province sont assujetties aux dispositions relatives au commerce interprovincial du RSAC. L'objectif de la modification r\u00e9glementaire est que les aliments salubres circulent dans la ville de <span lang=\"en\">Lloydminster</span> de la m\u00eame fa\u00e7on que dans les villes qui ne sont pas divis\u00e9es par des fronti\u00e8res provinciales. Cette modification ne s'appliquerait pas aux entreprises alimentaires de la Saskatchewan et de l'Alberta qui pr\u00e9parent des aliments destin\u00e9s \u00e0 l'exportation ou au commerce interprovincial au-del\u00e0 des limites de la ville de <span lang=\"en\">Lloydminster</span>.</p> \n\n<p>La <i>Loi sur les aliments et drogues</i> et le <i>R\u00e8glement sur les aliments et drogues</i> continuent de s'appliquer \u00e0 tous les aliments vendus au Canada. Les aliments vendus en Saskatchewan et en Alberta sont \u00e9galement assujettis aux lois provinciales respectives applicables.</p> \n\n\n<h2>Renseignements connexes</h2>\n<ul>\n<li><a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a></li>\n<li><a href=\"/licences-pour-aliments/activites-du-secteur-alimentaire/fra/1524074697160/1524074697425\">Activit\u00e9s des entreprises alimentaires qui requi\u00e8rent une licence au titre du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a></li>\n</ul>\n\n<h2>Communiquez avec nous</h2>\n<p>Si vous souhaitez nous faire part de commentaires ou si vous avez des questions au sujet du pr\u00e9sent avis, veuillez les faire parvenir \u00e0\u00a0:</p>\n\n<p>Natasha Richard<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:natasha.richard@inspection.gc.ca\">natasha.richard@inspection.gc.ca</a></p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}