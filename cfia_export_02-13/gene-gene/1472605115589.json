{
    "dcr_id": "1472605115589",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Ambrosia artemisiifolia</i> (Common ragweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Ambrosia artemisiifolia</i> (Petite herbe \u00e0 poux)"
    },
    "html_modified": "2024-02-13 9:51:41 AM",
    "modified": "2017-11-06",
    "issued": "2017-11-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_1472605115589_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_1472605115589_fra"
    },
    "ia_id": "1472605116050",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Ambrosia artemisiifolia</i> (Common ragweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Ambrosia artemisiifolia</i> (Petite herbe \u00e0 poux)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Ambrosia artemisiifolia</i> (Common ragweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Ambrosia artemisiifolia</i> (Petite herbe \u00e0 poux)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1472605116050",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/ambrosia-artemisiifolia/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/ambrosia-artemisiifolia/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Ambrosia artemisiifolia (Common ragweed)",
            "fr": "Semence de mauvaises herbe : Ambrosia artemisiifolia (Petite herbe \u00e0 poux)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Ambrosia artemisiifolia, Asteraceae, Common ragweed",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Ambrosia artemisiifolia, Asteraceae, Petite herbe \u00e0 poux"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-06",
            "fr": "2017-11-06"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Ambrosia artemisiifolia (Common ragweed)",
        "fr": "Semence de mauvaises herbe : Ambrosia artemisiifolia (Petite herbe \u00e0 poux)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Common Name</h2>\n<p>Common ragweed</p>\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 3 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs across Canada except in <abbr title=\"Nunavut\">NU</abbr> and <abbr title=\"Yukon\">YT</abbr> (Brouillet et al. 2016<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to North and South America, and widely introduced elsewhere   including Europe, Africa, temperate and tropical Asia, Australasia and   the Pacific Islands (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Widely distributed in North America with both native and introduced populations present in the <abbr title=\"United States\">U.S.</abbr> and Canada (Brouillet et al. 2016<sup id=\"fn1b-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>, <abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Natural Resources Conservation Service\">NRCS</abbr> 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene within a bur</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Bur length: 1.9 - 3.7 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Bur width: 1.5 - 2.8 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene length: 1.5 - 3.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width: 1.5 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Bur and achene are obovate\u00a0</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Bur is dull, woody</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Bur is dark grey with purple streaks</li>\n<li>Achene is brown</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Bur has a ring of several spines near top approximately 0.5 <abbr title=\"millimetres\">mm</abbr> long and an apical spine approximately 1.5 <abbr title=\"millimetres\">mm</abbr> long</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, fencerows, pastures, shores, canals, quarries, railway lines, roadsides, wasteland and disturbed areas (<abbr title=\"Flora of North America\">FNA</abbr> 1993+<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). Found in a range of both agronomic and horticultural crops, particularly in cereals and cultivated row crops (Basset and Crompton 1975<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote\u00a0</span>7</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). Often present as a dominant species in early succession of uncultivated areas (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn6c-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Seeds of common ragweed are commonly found in stored and transported grains, and have been dispersed around the world as contaminants in seed and grain crops, including cereals, oilseeds and bird seed.</p>\n<p>A single plant typically produces 3,000 - 4000 seeds, although up to 32,000 seeds per plant have been reported. Seeds remain viable for up to 40 years  (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn6d-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n<h2>Similar species</h2>\n<h3>Perennial ragweed (<i lang=\"la\">Ambrosia psilostachya</i>)</h3>\n<ul>\n<li>Perennial ragweed burs are a similar size, obovate shape and grey colour as common ragweed.</li>\n\n<li>Perennial ragweed burs have smaller spines, or only an apical spine that may be broken off, and may lack the purple streaks of common ragweed burs.</li>\n\n<li>If the burs are removed, it is difficult to distinguish between common and perennial ragweed achenes.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_05cnsh_1476383256451_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common ragweed (<i lang=\"la\">Ambrosia artemisiifolia</i>) burs\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_img1_1509649182884_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common ragweed (<i lang=\"la\">Ambrosia artemisiifolia</i>) burs and one achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_01cnsh_1476383198542_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common ragweed (<i lang=\"la\">Ambrosia artemisiifolia</i>) bur\n</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_copyright_1476383280351_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Common ragweed (<i lang=\"la\">Ambrosia artemisiifolia</i>) burs\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_psilostachya_03cnsh_1476383360616_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Perennial ragweed (<i lang=\"la\">Ambrosia psilostachya</i>) burs</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_psilostachya_01cnsh_1476383317933_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Perennial ragweed (<i lang=\"la\">Ambrosia psilostachya</i>) bur</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_psilostachya_02cnsh_1476383340085_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Perennial ragweed (<i lang=\"la\">Ambrosia psilostachya</i>) bur wall partially removed showing achene</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Natural Resources Conservation Service\">NRCS</abbr>. 2016</strong>. The PLANTS Database. National Plant Data Center, Baton Rouge, LA USA, http://plants.usda.gov [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"Flora of North America\">FNA</abbr>. 1993+</strong>. Flora of North America North of Mexico. 19+ vols. Flora of North America Editorial Committee, eds. New York and Oxford, http://www.fna.org/FNA/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 7</dt>\n<dd id=\"fn7\">\n<p><strong>Bassett, I. J. and Crompton, C. W. 1975</strong>. The biology of Canadian weeds. 11. <i lang=\"la\">Ambrosia artemisiifolia</i> L. and <i lang=\"la\">A</i>.<i lang=\"la\"> psilostachya</i> DC. Canadian Journal of Plant Science 55: 463-476.</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>7<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Petite herbe \u00e0 poux</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie 3 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> L'esp\u00e8ce est pr\u00e9sente partout au Canada, sauf au<abbr title=\"Nunavut\">Nt</abbr> et au <abbr title=\"Yukon\">Yn</abbr> (Brouillet et al., 2016<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l\u2019Am\u00e9rique du Nord et de l\u2019Am\u00e9rique du Sud. Largement   introduite ailleurs dans le monde, notamment en Europe, en Afrique, en   Asie temp\u00e9r\u00e9e et tropicale, en Australasie et dans les \u00eeles du Pacifique (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>). Largement r\u00e9pandue en Am\u00e9rique du Nord. On retrouve des populations indig\u00e8nes et introduites aux \u00c9tats-Unis et au Canada (Brouillet et al., 2016<sup id=\"fn1b-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>; <abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Natural Resources Conservation Service\">NRCS</abbr>, 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne entour\u00e9 de son enveloppe \u00e9pineuse</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'enveloppe \u00e9pineuse\u00a0: 1,9\u00a0\u00e0\u00a03,7 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de l'enveloppe \u00e9pineuse\u00a0: 1,5\u00a0\u00e0\u00a02,8 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Longueur de l'ak\u00e8ne : 1,5 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li> \n<li>Largeur de l'ak\u00e8ne : 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Enveloppe \u00e9pineuse et ak\u00e8ne de forme obov\u00e9e</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Enveloppe \u00e9pineuse mate et ligneuse</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Enveloppe \u00e9pineuse gris fonc\u00e9 stri\u00e9e de violet</li>\n<li>Ak\u00e8ne brun</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Enveloppe \u00e9pineuse orn\u00e9e d\u2019un anneau de plusieurs \u00e9pines pr\u00e8s du sommet   qui mesurent environ 0,5\u00a0<abbr title=\"millim\u00e8tre\">mm</abbr> de long et d\u2019une \u00e9pine apicale d\u2019environ   1,5\u00a0<abbr title=\"millim\u00e8tre\">mm</abbr> de long</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, haies, p\u00e2turages, rivages, canaux, carri\u00e8res, voies   ferr\u00e9es, bords de chemin, terrains vagues et terrains perturb\u00e9s (<abbr lang=\"en\" title=\"Flora of North America\">FNA</abbr>, 1993+<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>; Darbyshire, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>). Observ\u00e9e dans diverses cultures agricoles et horticoles, particuli\u00e8rement dans les c\u00e9r\u00e9ales et les cultures en rangs (Basset et Crompton, 1975<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>7</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>). Souvent pr\u00e9sente comme esp\u00e8ce dominante dans des aires non cultiv\u00e9es qui se trouvent aux premiers stades de succession v\u00e9g\u00e9tale (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn6c-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>). </p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Les graines de la petite herbe \u00e0 poux se trouvent souvent dans les grains entrepos\u00e9s et transport\u00e9s. Elles ont \u00e9t\u00e9 diss\u00e9min\u00e9es partout dans le monde par des semences et des grains contamin\u00e9s, notamment de c\u00e9r\u00e9ales, d'ol\u00e9agineux et de graines pour oiseaux.</p>\n<p>Chaque plante produit g\u00e9n\u00e9ralement de 3\u00a0000 \u00e0 4\u00a0000 graines, et selon certains rapports, jusqu'\u00e0 32\u00a0000 graines. Les graines demeurent viables jusqu'\u00e0 40 ans (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn6d-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Herbe \u00e0 poux vivace (<i lang=\"la\">Ambrosia psilostachya</i>)</h3>\n<ul>\n<li>L'enveloppe \u00e9pineuse de l'herbe \u00e0 poux vivace ressemble \u00e0 celle de la petite herbe \u00e0 poux par ses dimensions, sa forme obov\u00e9e et sa couleur grise.</li>\n\n<li>Par contre, les enveloppes de l'herbe \u00e0 poux vivace sont orn\u00e9es d'\u00e9pines plus petites, ou ne sont parfois surmont\u00e9es que d'une \u00e9pine apicale qui peut avoir \u00e9t\u00e9 bris\u00e9e, et elles peuvent ne pas \u00eatre stri\u00e9es de violet.</li>\n\n<li>Lorsque les enveloppes \u00e9pineuses ont \u00e9t\u00e9 enlev\u00e9es, il est difficile de diff\u00e9rencier les deux esp\u00e8ces.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_05cnsh_1476383256451_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Petite herbe \u00e0 poux (<i lang=\"la\">Ambrosia artemisiifolia</i>) enveloppes \u00e9pineuse\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_img1_1509649182884_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Petite herbe \u00e0 poux (<i lang=\"la\">Ambrosia artemisiifolia</i>) enveloppes \u00e9pineuse\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_01cnsh_1476383198542_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Petite herbe \u00e0 poux (<i lang=\"la\">Ambrosia artemisiifolia</i>) enveloppe \u00e9pineuse</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_artemisiifolia_copyright_1476383280351_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Petite herbe \u00e0 poux (<i lang=\"la\">Ambrosia artemisiifolia</i>) enveloppes \u00e9pineuse</figcaption>\n</figure>\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_psilostachya_03cnsh_1476383360616_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Herbe \u00e0 poux vivace (<i lang=\"la\">Ambrosia psilostachya</i>) enveloppes \u00e9pineuse</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_psilostachya_01cnsh_1476383317933_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Herbe \u00e0 poux vivace (<i lang=\"la\">Ambrosia psilostachya</i>) enveloppe \u00e9pineuse</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_ambrosia_psilostachya_02cnsh_1476383340085_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Herbe \u00e0 poux vivace (<i lang=\"la\">Ambrosia psilostachya</i>) ak\u00e8ne entour\u00e9 de son enveloppe \u00e9pineuse</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Natural Resources Conservation Service\">NRCS</abbr>. 2016</strong>. The PLANTS Database. National Plant Data Center, Baton Rouge, LA USA, http://plants.usda.gov [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"Flora of North America\">FNA</abbr>. 1993+</strong>. Flora of North America North of Mexico. 19+ vols. Flora of North America Editorial Committee, eds. New York and Oxford, http://www.fna.org/FNA/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n\n<dt>Note de bas de page 7</dt>\n<dd id=\"fn7\">\n<p lang=\"en\"><strong>Bassett, I. J. and Crompton, C. W. 1975</strong>. The biology of Canadian weeds. 11. <i lang=\"la\">Ambrosia artemisiifolia</i> L. and <i lang=\"la\">A</i>.<i lang=\"la\"> psilostachya</i> DC. Canadian Journal of Plant Science 55: 463-476.</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>7</a></p> \n</dd>\n\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}