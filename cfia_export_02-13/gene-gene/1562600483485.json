{
    "dcr_id": "1562600483485",
    "title": {
        "en": "Flocks infected with scrapie in Canada",
        "fr": "Troupeaux infect\u00e9s par la tremblante classique au Canada"
    },
    "html_modified": "2024-02-13 9:54:00 AM",
    "modified": "2019-09-13",
    "issued": "2019-07-10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_monthly_reportable_2019_scrapie_1562600483485_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_monthly_reportable_2019_scrapie_1562600483485_fra"
    },
    "ia_id": "1562600483734",
    "parent_ia_id": "1329723572482",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1329723572482",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Flocks infected with scrapie in Canada",
        "fr": "Troupeaux infect\u00e9s par la tremblante classique au Canada"
    },
    "label": {
        "en": "Flocks infected with scrapie in Canada",
        "fr": "Troupeaux infect\u00e9s par la tremblante classique au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1562600483734",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329723409732",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/scrapie/herds-infected/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/troupeaux-infectes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Flocks infected with scrapie in Canada",
            "fr": "Troupeaux infect\u00e9s par la tremblante classique au Canada"
        },
        "description": {
            "en": "The CFIA, in co-operation with provincial governments and industry, launched a national scrapie surveillance program in 2005. Under the program, producers are encouraged to report animals that die on the farm or exhibit symptoms of the disease.",
            "fr": "L'ACIA, en collaboration avec les gouvernements provinciaux et l'industrie, a lanc\u00e9 un programme national de surveillance de la tremblante en 2005. Le programme encourage les \u00e9leveurs \u00e0 signaler les animaux qui meurent \u00e0 l'exploitation ou qui pr\u00e9sentent des sympt\u00f4mes de la maladie."
        },
        "keywords": {
            "en": "animals, animal health, Health of Animals Regulations, 2019, national scrapie surveillance program, scrapie, sheep",
            "fr": "animaux, sant\u00e9 animale, R\u00e8glement sur la sant\u00e9 des animaux, 2019, tremblante, mouton, programme national de surveillance de la tremblante"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux terrestres"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-07-10",
            "fr": "2019-07-10"
        },
        "modified": {
            "en": "2019-09-13",
            "fr": "2019-09-13"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Flocks infected with scrapie in Canada",
        "fr": "Troupeaux infect\u00e9s par la tremblante classique au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>The CFIA, in co-operation with provincial governments and industry, launched a national scrapie surveillance program in 2005. Under the program, producers are encouraged to report animals that die on the farm or exhibit symptoms of the disease.</p>\n<p>In addition, scrapie is a reportable disease under the <i>Health of Animals Regulations</i>. This means that all suspected cases must be reported to the CFIA.</p>\n\n<p><strong>Current as of: 2024-01-31</strong></p>\n\n<h2 class=\"h4\">Sheep flocks and/or goat herds confirmed to be infected with classical scrapie in Canada</h2>\n\n<figure>\n<table class=\"wb-tables table table-striped table-hover\" data-wb-tables='{ \"pageLength\" : 25, \"ordering\" : false }'>\n<thead>\n<tr>\n<th>Year</th>\n<th>Date confirmed</th>\n<th>Location</th>\n<th>Animal type infected</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>2019</td>\n<td>June 21</td>\n<td>Alberta</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>June 21</td>\n<td>Alberta</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>January 16</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>January 16</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>November 1</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>October 4</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>October 4</td>\n<td>Alberta</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>September 15</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>September 7</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>August 28</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>July 11</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>June 29</td>\n<td>Manitoba</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>April 7</td>\n<td>Saskatchewan</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>November 14</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>November 14</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>May 18</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>January 5</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>May 22</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>June 16</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>July 31</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June 30</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June 18</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June 18</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June 16</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June 4</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>June 4</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>December 19</td>\n<td>Ontario</td>\n<td>Goat</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>April 11</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>April 11</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>February 25</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>November 8</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>October 11</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>October 11</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>September 27</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>September 27</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>September 13</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>August 2</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>June 4 <sup id=\"fn1a-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>*</a></sup> </td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>May 31 <sup id=\"fn1b-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>*</a></sup></td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>May 2</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>January 26 <sup id=\"fn1c-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>*</a></sup></td>\n<td>Alberta</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>September 26 <sup id=\"fn1d-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>*</a></sup></td>\n<td>Nova Scotia</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>July 14</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>July 13</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>May 5</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>May 3</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>March 30</td>\n<td>Quebec</td>\n<td>Sheep</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>January 19</td>\n<td>Ontario</td>\n<td>Sheep</td>\n</tr>\n</tbody>\n</table>\n\n<div class=\"wb-fnote\" role=\"note\">\n<p id=\"fn-tnotes\" class=\"wb-inv\">Table Notes</p>\n<dl>\n<dt>Table note\u00a0*</dt> <dd id=\"fn1\"> \n<p>Atypical scrapie</p>\n<p class=\"fn-rtn\"> <a href=\"#fn1a-rf\"><span class=\"wb-inv\"> Return to table note\u00a0</span>* <span class=\"wb-inv\">\u00a0referrer</span></a></p> </dd> \n</dl> \n</div>\n</figure>\n\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/scrapie/eng/1329723409732/1329723572482\">Scrapie</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/federally-reportable-diseases-for-terrestrial-anim/eng/1329499145620/1329499272021\">Federally Reportable Diseases in Canada</a></li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'ACIA, en collaboration avec les gouvernements provinciaux et l'industrie, a lanc\u00e9 un programme national de surveillance de la tremblante en 2005. Le programme encourage les \u00e9leveurs \u00e0 signaler les animaux qui meurent \u00e0 l'exploitation ou qui pr\u00e9sentent des sympt\u00f4mes de la maladie.</p>\n<p>De plus, puisque la tremblante est une maladie \u00e0 d\u00e9claration obligatoire en vertu du <i>R\u00e8glement sur la sant\u00e9 des animaux</i>, tous les cas pr\u00e9sum\u00e9s doivent \u00eatre d\u00e9clar\u00e9s \u00e0 l'ACIA.</p>\n\n<p><strong>\u00c0 jour en date du\u00a0: 2024-01-31</strong></p>\n\n<h2 class=\"h4\">Les troupeaux infect\u00e9s par la tremblante classique au Canada</h2>\n<figure>\n<table class=\"wb-tables table table-striped table-hover\" data-wb-tables='{ \"pageLength\" : 25, \"ordering\" : false }'>\n<thead>\n<tr>\n<th>Ann\u00e9e</th>\n<th>Date de confirmation</th>\n<th>Lieu</th>\n<th>Type d'animal infect\u00e9</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>2019</td>\n<td>21 juin</td>\n<td>Alberta</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2019</td>\n<td>21 huin</td>\n<td>Alberta</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>16 janvier</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2018</td>\n<td>16 janvier</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>1 novembre</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>4 octobre</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>4 octobre</td>\n<td>Alberta</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>15 septembre</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>7 septembre</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>28 ao\u00fbt</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>11 juillet</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>29 juin</td>\n<td>Manitoba</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2017</td>\n<td>7 avril</td>\n<td>Saskatchewan</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>14 novembre</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>14 novembre</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2016</td>\n<td>18 mai</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>5 janvier</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>22 mai</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2015</td>\n<td>16 juin</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>31 juillet</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>30 juin</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>18 juin</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>18 juin</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>16 juin</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>4 juin</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2014</td>\n<td>4 juin</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>19 d\u00e9cembre</td>\n<td>Ontario</td>\n<td>Ch\u00e8vre</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>11 avril</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>11 avril</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2013</td>\n<td>25 f\u00e9vrier</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>8 novembre</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>11 octobre</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>11 octobre</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>27 septembre</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>27 septembre</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>13 septembre</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>2 ao\u00fbt</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>4 juin <sup id=\"fn1a-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Note de tableau\u00a0</span>*</a></sup> </td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>31 mai <sup id=\"fn1b-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Note de tableau\u00a0</span>*</a></sup></td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>2 mai</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2012</td>\n<td>26 janvier<sup id=\"fn1c-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Note de tableau\u00a0</span>*</a></sup></td>\n<td>Alberta</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>26 septembre <sup id=\"fn1d-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Note de tableau\u00a0</span>*</a></sup></td>\n<td>Nouvelle-\u00c9cosse</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>14 juillet</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>13 juillet</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>5 mai</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>3 mai</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>30 mars</td>\n<td>Qu\u00e9bec</td>\n<td>Mouton</td>\n</tr>\n<tr>\n<td>2011</td>\n<td>19 janvier</td>\n<td>Ontario</td>\n<td>Mouton</td>\n</tr>\n</tbody>\n</table>\n\n<div class=\"wb-fnote\" role=\"note\">\n<p id=\"fn-tnotes\" class=\"wb-inv\">Notes de tableau</p>\n<dl>\n<dt>Note de tableau\u00a0*</dt> <dd id=\"fn1\"> \n<p>Forme atypique de la tremblante</p>\n<p class=\"fn-rtn\"> <a href=\"#fn1a-rf\"><span class=\"wb-inv\"> Retour \u00e0 la r\u00e9f\u00e9rence de la note de tableau\u00a0</span> *</a></p> </dd> \n</dl> \n</div>\n</figure>\n\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/fra/1329723409732/1329723572482\">Tremblante</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/maladies-a-declaration-obligatoire-pour-les-animau/fra/1329499145620/1329499272021\">Maladies \u00e0 d\u00e9claration obligatoire pour les animaux terrestres au Canada</a></li>\n</ul>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}