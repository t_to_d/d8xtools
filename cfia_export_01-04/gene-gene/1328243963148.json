{
    "dcr_id": "1328243963148",
    "title": {
        "en": "VB product submission checklist \u2013 Export only",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV \u2013 Exportation seulement"
    },
    "html_modified": "2012-02-02 23:39",
    "modified": "15-2-2023",
    "issued": "2-2-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/vetbio_guide_31_appe_1328243963148_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/vetbio_guide_31_appe_1328243963148_fra"
    },
    "parent_ia_id": "1328225600916",
    "ia_id": "1328244123531",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "VB product submission checklist \u2013 Export only",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV \u2013 Exportation seulement"
    },
    "label": {
        "en": "VB product submission checklist \u2013 Export only",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV \u2013 Exportation seulement"
    },
    "templatetype": "content page 1 column",
    "node_id": "1328244123531",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1328225508353",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/veterinary-biologics/guidelines-forms/3-1/checklist-export/",
        "fr": "/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-1/liste-de-controle-exportation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "VB product submission checklist \u2013 Export only",
            "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV \u2013 Exportation seulement"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency's Canadian Centre for Veterinary Biologics is responsible for regulating the manufacturing, importation, testing, distribution, and use of veterinary biologics in Canada.",
            "fr": "Le Centre canadien des produits biologiques v\u00e9t\u00e9rinaires (CCPBV) de l'Agence canadienne d'inspection des aliments (ACIA) est charg\u00e9e de la r\u00e9glementation pertinente \u00e0 la fabrication, \u00e0 l'importation, \u00e0 l'analyse, \u00e0 la distribution et \u00e0 l'utilisation des produits biologiques v\u00e9t\u00e9rinaires au Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, veterinary biologics, licensing requirements, checklists, guidelines, new product, export only",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glements sur la sant\u00e9 des animaux, produits biologiques v\u00e9t\u00e9rinaires, d\u00e9livrance des permis, lignes directrices, listes de contr\u00f4le, nouveaux produits, exportation seulement"
        },
        "dcterms.subject": {
            "en": "exports,inspection,veterinary medicine,standards,regulation,animal health",
            "fr": "exportation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-02 23:39:25",
            "fr": "2012-02-02 23:39:25"
        },
        "modified": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "type": {
            "en": "reference material,standard,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,norme,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "VB product submission checklist \u2013 Export only",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV \u2013 Exportation seulement"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th>No.</th>\n<th>Documentation requirement</th>\n<th>Canadian applicant</th>\n<th>File or document name, version and date (yyyymmdd), if applicable</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>00.</td>\n<td>Index of submission contents</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>01.</td>\n<td><p>Cover letter introducing the licensing submission and identifying regulatory contact</p>\n<p>Official document from the regulatory authorities of the importing country indicating consideration or acceptance of the product</p></td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>02.</td>\n<td>Veterinary Biologics information - Form CFIA/ACIA\u00a01503 </td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>03.</td>\n<td><p>Outline of Production (OP) for the VB</p>\n<p>(If applicable) Referenced OP, Special Outlines (SO) and SOPs, if a current version is not on file with the CCVB.</p>\n<p>(If applicable) Validation data referenced in\u00a0OP\u00a0including data for reference standards and product inactivation</p></td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>04.</td>\n<td><p>Draft or final labels for each importing country</p>\n<p>(If applicable) Certified translation in English or French of the foreign language labels to be used in the importing country or countries</p></td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>05.</td>\n<td><p>Declaration of Compliance regarding\u00a0TSE</p>\n<p>Material of Animal Origin (MAO) Special Outline </p></td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>06.</td>\n<td><p>Master seed(s): purity, safety and identity</p>\n<p>(If applicable) Genetic characterization data for biotechnology derived product</p></td>\n<td>If applicable</td>\n<td></td>\n</tr>\n<tr>\n<td>07.</td>\n<td>Master cell stock(s): purity, safety and identity</td>\n<td>If applicable</td>\n<td></td>\n</tr>\n<tr>\n<td>08.</td>\n<td>Efficacy: laboratory data and proof of concept or, for a diagnostic kit, data on laboratory performance</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>09.</td>\n<td>Safety: laboratory and containment safety studies</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>10.</td>\n<td><p>Other supporting documentation (identify)</p>\n<p>(Live or biotechnology products) Environmental assessment </p></td>\n<td>If applicable</td>\n<td></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th>No. </th>\n<th>Documents requis</th>\n<th>Canada (CAN)</th>\n<th>Nom du fichier ou du document, version et date (aaaammjj), le cas \u00e9ch\u00e9ant</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>00.</td>\n<td>Index du contenu de la soumission</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>01.</td>\n<td><p>Lettre d'introduction de la demande d'homologation (d'enregistrement) et du responsable des affaires r\u00e9glementaires</p>\n<p>Document officiel des autorit\u00e9s r\u00e9glementaires du pays importateur indiquant la consid\u00e9ration ou l'acceptation du produit</p></td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>02.</td>\n<td>Donn\u00e9es sur les produits biologiques v\u00e9t\u00e9rinaires - formulaire CFIA/ACIA\u00a01503 </td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>03.</td>\n<td><p>Protocole de production (PP) du produit biologique v\u00e9t\u00e9rinaire</p>\n<p>(Le cas \u00e9ch\u00e9ant) PP, protocoles sp\u00e9ciaux (PS) et autres proc\u00e9dures standardis\u00e9es cit\u00e9es, si une version \u00e0 jour n'est pas d\u00e9j\u00e0 au dossier du CCPBV</p>\n<p>(Le cas \u00e9ch\u00e9ant) Donn\u00e9es de validation cit\u00e9es dans le PP incluant les donn\u00e9es pour les r\u00e9actifs-\u00e9talons de r\u00e9f\u00e9rence et l'inactivation du produit </p></td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>04.</td>\n<td><p>\u00c9bauches ou \u00e9tiquettes finales pour chaque pays importateur</p>\n<p>(Le cas \u00e9ch\u00e9ant) Traduction certifi\u00e9e en anglais ou en fran\u00e7ais des \u00e9tiquettes de langue \u00e9trang\u00e8re destin\u00e9es \u00e0 \u00eatre utilis\u00e9es dans le ou les pays importateurs</p></td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>05.</td>\n<td>D\u00e9claration de conformit\u00e9 concernant les enc\u00e9phalopathies spongiformes transmissibles (EST ) Substances d'origine animale (SOA) - PS</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>06.</td>\n<td><p>Souche(s) m\u00e8re(s) : puret\u00e9, innocuit\u00e9 &amp; identit\u00e9</p>\n<p>(Le cas \u00e9ch\u00e9ant) Donn\u00e9es de caract\u00e9risation g\u00e9n\u00e9tique si produit d\u00e9riv\u00e9 de la biotechnologie</p></td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td></td>\n</tr>\n<tr>\n<td>07.</td>\n<td>Souche(s) m\u00e8re(s) de cellules : puret\u00e9, innocuit\u00e9 &amp; identit\u00e9</td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td></td>\n</tr>\n<tr>\n<td>08.</td>\n<td>Efficacit\u00e9: donn\u00e9es de laboratoire et preuve du concept ou, pour les trousses diagnostiques, donn\u00e9es sur la performance en laboratoire</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>09.</td>\n<td>Innocuit\u00e9: \u00e9tudes d'innocuit\u00e9 en laboratoire et en confinement</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>10.</td>\n<td><p>Autre documentation \u00e0 l'appui (identifier)</p>\n<p>(Produits vivants ou issus de la biotechnologie) \u00c9valuation environnementale</p></td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}