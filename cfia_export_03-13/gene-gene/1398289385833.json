{
    "dcr_id": "1398289385833",
    "title": {
        "en": "Weed Seed - Sessile joyweed (<i lang=\"la\">Alternanthera sessilis</i>)",
        "fr": "Semence de mauvaises herbe - Magloire (<i lang=\"la\">Alternanthera sessilis</i>)"
    },
    "html_modified": "2024-03-12 3:56:41 PM",
    "modified": "2014-04-24",
    "issued": "2014-06-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_Alternanthera_sessilis_1398289385833_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_Alternanthera_sessilis_1398289385833_fra"
    },
    "ia_id": "1398289412685",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed - Sessile joyweed (<i lang=\"la\">Alternanthera sessilis</i>)",
        "fr": "Semence de mauvaises herbe - Magloire (<i lang=\"la\">Alternanthera sessilis</i>)"
    },
    "label": {
        "en": "Weed Seed - Sessile joyweed (<i lang=\"la\">Alternanthera sessilis</i>)",
        "fr": "Semence de mauvaises herbe - Magloire (<i lang=\"la\">Alternanthera sessilis</i>)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1398289412685",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/alternanthera-sessilis/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/alternanthera-sessilis/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed - Sessile joyweed (Alternanthera sessilis)",
            "fr": "Semence de mauvaises herbe - Magloire (Alternanthera sessilis)"
        },
        "description": {
            "en": "Fact sheet for Alternanthera sessilis.",
            "fr": "Fiche de renseignements pour Alternanthera sessilis."
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, import, US, grader, Alternanthera sessilis, Sessile joyweed",
            "fr": "fiche de renseignements, envahissantes, semence envahissantes, semence, importation, \u00c9tats-Unis, calibreuse, Alternanthera sessilis, Magloire"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants",
            "fr": "cultures,grain,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-06-12",
            "fr": "2014-06-12"
        },
        "modified": {
            "en": "2014-04-24",
            "fr": "2014-04-24"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed - Sessile joyweed (Alternanthera sessilis)",
        "fr": "Semence de mauvaises herbe - Magloire (Alternanthera sessilis)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Amaranthaceae</i></p> \n \n<h2>Synonyms</h2>\n<p><i lang=\"la\">Achyranthes sessilis</i></p> \n \n<h2>Distribution in Canada</h2>\n<p>Not known to occur in Canada.</p>\n \n<h2>Duration of life cycle</h2>\n<p>Annual or short-lived perennial</p>\n \n<h2>Seed/Fruit type</h2>\n<ul>\n<li>True seed (often shed enclosed within a single-seeded utricle and perianth)</li>\n</ul>\n<h2>Seed identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>fruit length: 1.5\u20132.5 <abbr title=\"millimetres\">mm</abbr></li> \n<li>fruit width: 1.5\u20132.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>seed diameter: 1.0 <abbr title=\"millimetre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>fruit is heart-shaped</li>\n<li>seed is round, with a notch at seed root area</li>\n<li>in cross-section: both fruit and seed are a narrow oval</li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Surface texture</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>fruit: dull and roughened</li>\n<li>seed: shiny and smooth</li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Colour</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>fruit: straw-yellow</li>\n<li>seed: translucent, dark red</li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Other structures</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>perianth is attached to base of fruit, and has 5 sharp-pointed lobes</li>\n<li>top of utricle extends past perianth</li>\n</ul>\n \n<h2>Similar species</h2>\n \n<p>Smooth joyweed (<i lang=\"la\">Alternanthera paronychioides</i>)</p> \n<ul>\n<li>not found in Canada; found in the southeastern United States</li>\n<li>distribution overlaps with that of sessile joyweed</li>\n<li>very similar to sessile joyweed (fruits are difficult to distinguish)</li>\n<li>tends to be larger than sessile joyweed by up to 1.0 <abbr title=\"millimetre\">mm</abbr></li>\n<li>utricle remains within perianth (utricle of sessile joyweed extends past perianth lobes)</li>\n</ul>\n \n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image1_1401975945932_eng.jpg\" alt=\"Photo - Sessile joyweed (Alternanthera sessilis) utricle and perianth\" class=\"img-responsive\">\n<figcaption>Sessile joyweed (<i lang=\"la\">Alternanthera sessilis</i>) utricle and perianth</figcaption>\n</figure>\n \n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image2_1401975988541_eng.jpg\" alt=\"Photo - Sessile joyweed (Alternanthera sessilis) seed\" class=\"img-responsive\">\n<figcaption>Sessile joyweed (<i lang=\"la\">Alternanthera sessilis</i>) seed</figcaption>\n</figure>\n \n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image3_1401976028495_eng.jpg\" alt=\"Photo - Sessile joyweed (Alternanthera sessilis) utricles and seeds\" class=\"img-responsive\">\n<figcaption>Sessile joyweed (<i lang=\"la\">Alternanthera sessilis</i>) utricles and seeds</figcaption>\n</figure>\n \n<h3>Similar species</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image4_1401976049604_eng.jpg\" alt=\"Photo - Smooth joyweed (Alternanthera paronychioides) utricles and seeds\" class=\"img-responsive\">\n<figcaption>Similar species: Smooth joyweed (<i lang=\"la\">Alternanthera paronychioides</i>) utricles and seeds</figcaption>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Amarantac\u00e9es</i></p>\n \n<h2>Synonyme</h2>\n<p><i lang=\"la\">Achyranthes sessilis</i></p>\n \n<h2>R\u00e9partition au Canada</h2>\n<p>L'esp\u00e8ce n'est pas signal\u00e9e au Canada.</p>\n \n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle, ou vivace \u00e0 vie courte</p>\n \n<h2>Type de graine ou de fruit</h2>\n<ul>\n<li>Graine v\u00e9ritable (souvent diss\u00e9min\u00e9e avec l'utricule et le p\u00e9rianthe, qui renferment une seule graine)</li>\n</ul>\n \n<h2>Caract\u00e8res servant \u00e0 l'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>longueur du fruit\u00a0: 1,5\u20132,5 <abbr title=\"millim\u00e8tres\">mm</abbr></li> \n<li>largeur du fruit\u00a0: 1,5\u20132,5 <abbr title=\"millim\u00e8tres\">mm</abbr></li>\n<li>diam\u00e8tre de la graine\u00a0: 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>fruit en forme de c\u0153ur</li>\n<li>graine ronde, avec encoche pr\u00e8s de la radicule</li>\n<li>coupe transversale\u00a0: \u00e9troitement elliptique chez la graine et le fruit</li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>fruit\u00a0: surface mate et rugueuse</li>\n<li>graine\u00a0: surface lisse et luisante</li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>fruit\u00a0: jaune paille</li>\n<li>graine\u00a0: translucide, rouge fonc\u00e9</li>\n</ul>\n \n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>p\u00e9rianthe fix\u00e9 \u00e0 la base du fruit et comportant 5 lobes pointus</li>\n<li>sommet de l'utricule d\u00e9passant du p\u00e9rianthe</li>\n</ul>\n \n<h2>Esp\u00e8ce semblable</h2>\n<p><i lang=\"la\">Alternanthera paronychioides</i></p> \n<ul>\n<li>non signal\u00e9 au Canada; pr\u00e9sent dans le sud-est des \u00c9tats-Unis</li>\n<li>r\u00e9partition chevauchant celle de la magloire</li>\n<li>graine tr\u00e8s semblable \u00e0 celle de la magloire (les fruits des deux esp\u00e8ces sont difficiles \u00e0 distinguer)</li>\n<li>graine g\u00e9n\u00e9ralement plus grosse que celle de la magloire (mesurant jusqu'\u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr> de plus)</li>\n<li>utricule envelopp\u00e9 par le p\u00e9rianthe (chez la magloire, l'utricule d\u00e9passe des lobes du p\u00e9rianthe)</li>\n</ul>\n \n<h2>Photos</h2>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image1_1401975945932_fra.jpg\" alt=\"Photo - Magloire (Alternanthera sessilis), utricule et p\u00e9rianthe\" class=\"img-responsive\">\n<figcaption>Magloire (<i lang=\"la\">Alternanthera sessilis</i>), utricule et p\u00e9rianthe</figcaption>\n</figure>\n \n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image2_1401975988541_eng.jpg\" alt=\"Photo - Magloire (Alternanthera sessilis), graine\" class=\"img-responsive\">\n<figcaption>Magloire (<i lang=\"la\">Alternanthera sessilis</i>), graine</figcaption>\n</figure>\n \n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image3_1401976028495_eng.jpg\" alt=\"Photo - Magloire (Alternanthera sessilis), utricules et graines\" class=\"img-responsive\">\n<figcaption>Magloire (<i lang=\"la\">Alternanthera sessilis</i>), utricules et graines</figcaption>\n</figure>\n \n<h3>Esp\u00e8ce semblable</h3>\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alternanthera_image4_1401976049604_fra.jpg\" alt=\"Photo - Esp\u00e8ce semblable\u00a0: (Alternanthera paronychioides), utricules et graines\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: (<i lang=\"la\">Alternanthera paronychioides</i>), utricules et graines</figcaption>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}