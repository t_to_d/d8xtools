{
    "dcr_id": "1673645728582",
    "title": {
        "en": "Incorporation by reference of plant protection movement prohibitions and restrictions",
        "fr": "Incorporation par renvoi des interdictions et restrictions relatives \u00e0 la circulation des v\u00e9g\u00e9taux"
    },
    "html_modified": "17-1-2023",
    "modified": "18-2-2023",
    "issued": "13-1-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/share_your_thoughts_incorporation_by_reference_plant_protection_movement_1673645728582_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/share_your_thoughts_incorporation_by_reference_plant_protection_movement_1673645728582_fra"
    },
    "parent_ia_id": "1330978369105",
    "ia_id": "1673645862760",
    "parent_node_id": "1330978369105",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Incorporation by reference of plant protection movement prohibitions and restrictions",
        "fr": "Incorporation par renvoi des interdictions et restrictions relatives \u00e0 la circulation des v\u00e9g\u00e9taux"
    },
    "label": {
        "en": "Incorporation by reference of plant protection movement prohibitions and restrictions",
        "fr": "Incorporation par renvoi des interdictions et restrictions relatives \u00e0 la circulation des v\u00e9g\u00e9taux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1673645862760",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1330978174872",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/transparency/consultations-and-engagement/completed/incorporation-by-reference-of-plant-protection-mov/",
        "fr": "/a-propos-de-l-acia/transparence/consultations-et-participation/terminees/incorporation-par-renvoi-des-interdictions-et-rest/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Incorporation by reference of plant protection movement prohibitions and restrictions",
            "fr": "Incorporation par renvoi des interdictions et restrictions relatives \u00e0 la circulation des v\u00e9g\u00e9taux"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) has launched a consultation related to plant-movement. Restrictions and prohibitions help prevent or control the spread of plant pests in Canada.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) a lanc\u00e9 une consultation sur la circulation des v\u00e9g\u00e9taux. Les restrictions et les interdictions aident \u00e0 pr\u00e9venir ou \u00e0 contr\u00f4ler la propagation des phytoravageurs au Canada."
        },
        "keywords": {
            "en": "information, consultation, Incorporation by reference of plant protection movement prohibitions and restrictions, Plant Protection Regulations, PPR, domestic plant protection measures",
            "fr": "information, consultation, Incorporation par renvoi des interdictions et restrictions relatives \u00e0 la circulation des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, mesures de protection des v\u00e9g\u00e9taux en territoire canadien"
        },
        "dcterms.subject": {
            "en": "consumer protection,consumers,food safety,government information,government publication,inspection",
            "fr": "protection du consommateur,consommateur,salubrit\u00e9 des aliments,information gouvernementale,publication gouvernementale,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-01-17",
            "fr": "2023-01-17"
        },
        "modified": {
            "en": "2023-02-18",
            "fr": "2023-02-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Incorporation by reference of plant protection movement prohibitions and restrictions",
        "fr": "Incorporation par renvoi des interdictions et restrictions relatives \u00e0 la circulation des v\u00e9g\u00e9taux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Current status: Closed</h2>\n<p>Opened on January 17, 2023 and closed on February 18, 2023</p>\n    \n<p>The Canadian Food Inspection Agency (CFIA) has launched a consultation related to plant-movement. Restrictions and prohibitions help prevent or control the spread of plant pests in Canada.</p>\n\n<h2>About the consultation</h2>\n\n<p>The CFIA is seeking input on the use of incorporation by reference, which introduces the content of a document into a regulation (for example, via a link) without having to include the document's full text in the regulation. This makes it easier to update information, for example when a list of pests or requirements changes.</p>\n\n<p>Currently, the plant-related movement prohibitions and restrictions are listed in Schedules I and II of the <a href=\"/english/reg/jredirect2.shtml?plavegr\"><i>Plant Protection Regulations</i></a> (PPR). They are one of the CFIA's <a href=\"/plant-health/invasive-species/domestic-plant-protection-measures/eng/1629843699782/1629901542997\">domestic plant protection measures</a> and apply to the movement of certain products in Canada (for example, plants, firewood and soil). Instead, the CFIA is proposing to move the lists to the CFIA's website. The proposal would \"incorporate by reference\" Schedules I and II of the PPR to allow the list of regulated pests to be maintained and updated in a transparent, timely and efficient manner. </p>\n\n<p>This is one way the CFIA is working to update and modernize the PPR, as listed on the CFIA's <a href=\"/eng/1505738163122/1505738211563#a1_7_2\">Forward Regulatory Plan</a> and in accordance with the <a href=\"/about-cfia/acts-and-regulations/incorporation-by-reference/cfia-incorporation-by-reference-policy/eng/1450356693608/1450356805085\">CFIA Incorporation by Reference Policy</a>.</p>\n\n<p>The CFIA will continue to consult stakeholders and provide opportunities to comment on proposed changes, except for minor administrative adjustments and changes to address an immediate risk.</p>\n\n<h2>How to participate</h2>\n\n<p>Comments on CFIA's proposal to incorporate by reference Schedules I and II of the PPR, can be sent by e-mail to the CFIA's Invasive Alien Species and Domestic Programs at <a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a>.</p>\n\n<h2>Who was the focus of this consultation</h2>\n\n<p>The CFIA is seeking comments from anyone (such as businesses, academia, other levels of government, other organizations and the public at large) that moves or may move products within Canada that are regulated by the CFIA under the <i>Plant Protection Act</i>.\u00a0 This includes products such as:</p>\n\n<ul>\n<li>plants for planting, including rooted plants and plant root systems, greenhouse and nursery plants, fruit, vegetables, root crops, cut flowers, branches</li>\n<li>trees and tree parts, including sawn wood, bark, logs, lumber, pulpwood, firewood, Christmas trees, wood chips, wood packaging and dunnage</li>\n<li>grains and field crops, including cereals, oilseeds, pulses, hay, pastures, straw, hay</li>\n<li>potatoes and potato parts for propagation, including seed tubers, true (botanical) seed, in vitro plantlets, micro-tubers, mini-tubers and cuttings</li>\n<li>potatoes not intended for propagation, including packaged and bulk potato tubers for human or animal consumption, processing, packing, and repacking</li>\n<li>soil, related matter and the loose surface of the earth in which plants grow, including humus, compost, earthworm castings, muck, plant litter and debris, peat moss</li>\n</ul>\n\n<h2>Related information</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/domestic-plant-protection-measures/eng/1629843699782/1629901542997\">Domestic plant protection measures</a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?plavegr\"><i>Plant Protection Regulations</i> (justice.gc.ca)</a></li>\n<li><a href=\"/about-cfia/acts-and-regulations/incorporation-by-reference/cfia-incorporation-by-reference-policy/eng/1450356693608/1450356805085\">CFIA Incorporation by Reference Policy</a></li>\n<li><a href=\"/eng/1505738163122/1505738211563#a1_7_2\">CFIA Forward Regulatory Plan: 2022 to 2024</a></li>\n</ul>\n\n<h2>Contact us</h2>\n\n<p>Invasive Alien Species and Domestic Programs<br>\n<br>\n<br>\n<a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>\u00c9tat actuel\u00a0: Ferm\u00e9</h2>\n<p>Ouvert le 17 janvier 2023 et ferm\u00e9 le 18 f\u00e9vrier\u00a02023</p>\n    \n<p>L'Agence canadienne d'inspection des aliments (ACIA) a lanc\u00e9 une consultation sur la circulation des v\u00e9g\u00e9taux. Les restrictions et les interdictions aident \u00e0 pr\u00e9venir ou \u00e0 contr\u00f4ler la propagation des phytoravageurs au Canada.</p>\n<h2>\u00c0 propos de la consultation</h2>\n<p>L'ACIA souhaite recueillir des commentaires sur l'utilisation de l'incorporation par renvoi, qui permet d'introduire le contenu d'un document dans un r\u00e8glement (par exemple, par un lien) sans avoir \u00e0 inclure le texte int\u00e9gral du document dans le r\u00e8glement. Cela facilite la mise \u00e0 jour de l'information, par exemple lorsqu'une modification est apport\u00e9e \u00e0 une liste de phytoravageurs ou d'exigences.</p>\n<p>Les interdictions et restrictions de circulation des v\u00e9g\u00e9taux sont actuellement \u00e9num\u00e9r\u00e9es aux annexes I et II du <a href=\"/francais/reg/jredirect2.shtml?plavegr\"><i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i></a>. Elles s'agissent de l'une des <a href=\"/protection-des-vegetaux/especes-envahissantes/mesures-de-protection-des-vegetaux-en-territoire-c/fra/1629843699782/1629901542997\">mesures de protection des v\u00e9g\u00e9taux en territoire canadien</a> de l'ACIA et s'appliquent aux d\u00e9placement de certains produits au Canada (par exemple, des plantes, du feu de bois et de la terre). L'ACIA sugg\u00e8re plut\u00f4t de d\u00e9placer ces listes au site Web de l'ACIA. Cette proposition \u00ab\u00a0incorporerait par renvoi\u00a0\u00bb les annexes I et II du <i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i> afin de permettre le maintien et la mise \u00e0 jour de la liste des phytoravageurs r\u00e9glement\u00e9s de mani\u00e8re transparente, opportune et efficace. </p>\n<p>Ceci s'agit d'une des fa\u00e7ons dont l'ACIA travaille afin de mettre \u00e0 jour et de moderniser le <i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i>, comme d\u00e9crit dans le <a href=\"/fra/1505738163122/1505738211563#a1_7_2\">Plan prospectif de la r\u00e9glementation de l'ACIA</a> et en vertu de la <a href=\"/a-propos-de-l-acia/lois-et-reglements/incorporation-par-renvoi/politique-de-l-acia-sur-l-incorporation-par-renvoi/fra/1450356693608/1450356805085\">Politique de l'ACIA sur l'incorporation par renvoi</a>. </p>\n<p>L'ACIA continuera de consulter les intervenants et leur offre l'occasion de formuler des commentaires sur les changements propos\u00e9s, \u00e0 moins que ceux-ci ne soient apport\u00e9s en r\u00e9ponse \u00e0 un risque imm\u00e9diat ou repr\u00e9sentent seulement des modifications administratives mineures.</p>\n\n<h2>Qui \u00e9tait vis\u00e9 par cette consultation</h2>\n\n<p>L'ACIA veut obtenir les commentaires de ceux (comme des entreprises, des universit\u00e9s, d'autres ordres de gouvernement, des organisations et du grand public) qui d\u00e9placent ou pourraient d\u00e9placer au Canada de produits r\u00e9glement\u00e9s par l'ACIA en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>. Ces produits incluent notamment les suivants\u00a0:</p>\n<ul>\n<li>Les v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation, y compris les plantes portant des racines et les syst\u00e8mes racinaires de plantes; plantes de serre et de p\u00e9pini\u00e8re; fruits; l\u00e9gumes, y compris les l\u00e9gumes racines; fleurs coup\u00e9es; branches;</li>\n<li>Les arbres ou parties d'arbres, bois sci\u00e9, \u00e9corce, billes, bois d'\u0153uvre et bois \u00e0 p\u00e2te; bois de chauffage; arbres de No\u00ebl; copeaux de bois; mat\u00e9riaux d'emballage en bois et bois de calage;</li>\n<li>Les grains et produits de grandes cultures, y compris les c\u00e9r\u00e9ales, les ol\u00e9agineux, les l\u00e9gumineuses, le foin et les plantes de p\u00e2turage; paille; foin;</li>\n<li>Les pommes de terre et portions de pommes de terre destin\u00e9es \u00e0 la multiplication, dont les pommes de terre de semence, les graines de pommes de terre, les plantules in vitro, les microtubercules, les minitubercules et les boutures; </li>\n<li>Les pommes de terre non destin\u00e9es \u00e0 la multiplication, y compris les tubercules emball\u00e9s et en vrac destin\u00e9s \u00e0 la consommation humaine ou animale, \u00e0 la transformation, \u00e0 l'emballage et au r\u00e9emballage; et</li>\n<li>Du sol et les mati\u00e8res connexes, et la couche meuble de la terre dans laquelle poussent les v\u00e9g\u00e9taux, g\u00e9n\u00e9ralement compos\u00e9e d'humus, de compost, de turricule, de sol organique, de liti\u00e8re et de d\u00e9bris v\u00e9g\u00e9taux; mousse de tourbe.</li>\n</ul>\n\n<h2>Renseignements connexes</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/mesures-de-protection-des-vegetaux-en-territoire-c/fra/1629843699782/1629901542997\">Mesures de protection des v\u00e9g\u00e9taux en territoire canadien </a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?plavegr\"><i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i> (justice.gc.ca)</a></li>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/incorporation-par-renvoi/politique-de-l-acia-sur-l-incorporation-par-renvoi/fra/1450356693608/1450356805085\">Politique de l'ACIA sur l'incorporation par renvoi</a></li>\n<li><a href=\"/fra/1505738163122/1505738211563#a1_7_2\">Plan prospectif de la r\u00e9glementation de l'ACIA\u00a0: 2022 \u00e0 2024</a></li>\n</ul>\n\n<h2>Communiquez avec nous</h2>\n<p>Programmes nationaux des esp\u00e8ces exotiques envahissantes et phytosanitaires<br>\n<br>\n<br>\n<a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}