<?php

global $argv;
global $debug;
$debug = FALSE;
global $checked_ids_array;
$checked_ids_array = [];

$myargs = array_slice($argv, 3);
use Drush\Drush;
use Drupal\node\Entity\Node;
include "db_operations.php";
global $import_override;
$import_override = TRUE;


if (empty($myargs)) {
  echo "Syntax: drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL\n";
  echo "OR      drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH\n";
  echo "The FULL_MEAL_DEAL flag processes all available nodes.\n";
  echo "The HARD_LAUNCH flag currently hard coded for doing nodes newer than Oct-11-2020. Also skips blocks which we have already done by this point.\n";
  exit;
}

$import_phase = array_shift($myargs);
echo "Import phase chosen=$import_phase\n";
//sleep(2);
if ($import_phase != 'FULL_MEAL_DEAL' && $import_phase != 'HARD_LAUNCH') {
  echo "Syntax: drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL\n";
  echo "OR      drush scr ../d8tools/convertTeamsiteIdTODrupal/convert_teamsiteId_to_drupal.php HARD_LAUNCH\n";
  echo "The FULL_MEAL_DEAL flag processes all available nodes.\n";
  echo "The HARD_LAUNCH flag currently hard coded for doing nodes newer than Oct-11-2020. Also skips blocks which we have already done by this point.\n";
  exit;
}

$nodeId;


$db = new DBOperations();
$countNode = 0;

$count = 0;

//process menu_link URLs in Footer
$storage = \Drupal::entityTypeManager()->getStorage('menu_link_content');
$menu_links = $storage->loadByProperties(['menu_name' => "Footer"]);
processMenuLink($menu_links);

$menu_links_fr = $storage->loadByProperties(['menu_name' => "footer-fr"]);

processMenuLink($menu_links_fr);

//finished process footer


//$uuids = ['8c43040c-9e83-4f13-b774-f4771b2a5a8b', '49baecda-63c8-4e16-811b-be2ff38cea33'];

$uuids = ['2ae71642-ca4f-4e8a-9f43-588ae3e110f9',
          '30e4c971-edb5-48f2-9f12-86589e46b849',
          '41c877fc-4f51-422b-821c-1f23452edb93',
          '49baecda-63c8-4e16-811b-be2ff38cea33',
          '78aca85d-3afc-4257-902b-144ae0d874e1',
          '878c1db8-a3da-452d-8479-8a1c1f740d6e',
          '8c43040c-9e83-4f13-b774-f4771b2a5a8b',
          '9cc4d184-ead7-4997-9da1-fc10b12ab2af',
          'a499786a-3b20-4157-89a6-2d3a0a0edf06',
          'b1130f0a-f3ae-4dab-b765-4ffc5467d499',
          'b290c5dc-03ed-4f0b-bcf3-fa489258543c',
          'b382751d-0591-4714-8dc8-506d8d8aa343',
          'be4d3824-c613-4aef-8992-42ba10131caf'];

//$uuids = ['49baecda-63c8-4e16-811b-be2ff38cea33']; // This one doesn't seem to work.
//foreach ($uuids as $uuid) {
$query = \Drupal::entityQuery('block_content');
$ids  = $query->accessCheck(FALSE)->execute();
foreach ($ids as $id) {
  if ($debug) {
    continue;
  }
  // Do all navi-nav blocks for CFIA now.
  $block_content = \Drupal::entityTypeManager()->getStorage('block_content')->load($id);
  if ($block_content) {
    $body = $block_content->get('body')->value;
    $body = updateInternalReference($body);
    // $body = html_entity_decode($body);
    //Drush::output()->writeln($body);
    if ($block_content->body->value != $body) {
      $block_content->body->value = $body;
      $block_content->setSyncing(TRUE);
      if ($import_phase == 'FULL_MEAL_DEAL') {
        $block_content->save();
      }
    }
    if ($block_content->hasTranslation('fr')) {
      $block_content_fr = $block_content->getTranslation('fr');
      $body_fr = $block_content_fr->get('body')->value;
      $body_fr = updateInternalReference($body_fr);
      // $body_fr = html_entity_decode($body_fr);
      if ($block_content_fr->body->value != $body_fr) {
        $block_content_fr->body->value = $body_fr;
        $block_content_fr->setSyncing(TRUE);
        if ($import_phase == 'FULL_MEAL_DEAL') {
          $block_content_fr->save();
        }
      }
    }
  }
  $block_content = NULL;
  Drush::output()->writeln('Process block body field for id=' . $id);
  //Drush::output()->writeln('Process block body field for uuid=' . $uuid);
}
// Done with blocks, now start NODES.


if ($import_phase == 'FULL_MEAL_DEAL') {
  $result = $db->getAllNodeIdFromNodeTable();
}
elseif ($import_phase == 'HARD_LAUNCH') {
  $result = $db->getHardlaunchNewsEmplFromTables();// HARD LAUNCH.
}

// Reporting.
global $referenceSuccessArray;
global $referenceFailureArray;
$referenceSuccessArray = [];
$referenceFailureArray = [];
global $dcrReferenceFailureArray;
$dcrReferenceFailureArray = [];

foreach ($result as $row) {
  global $nodeId;
  $nodeId = (int) $row->nid;
  global $referenceSuccessArray;
  global $referenceFailureArray;
  //$referenceSuccessArray[$nodeId];
  //$referenceFailureArray[$nodeId];
  //echo $nodeId . "\n";

  if (empty($nodeId))
    continue;

  if ($debug && $nodeId != 937) {
    // DEBUG
    continue;
  }
  if ($debug) {
    echo "\nnid:";
    echo $nodeId;
  }
  //echo $nodeId . "\n";
  $node = Node::load($nodeId);

  $nodeValueEn = $node->body->value;
  if ($nodeId < 800) {
    $nodeValueEn = cleanFollowus($nodeValueEn);
  }
  $nodeValueEn = updateInternalReference($nodeValueEn);
  if ($debug) {
    echo "********************************** BEGIN processing node id: $nodeId English **************************************************** \n";
    $pos_end = strpos($nodeValueEn, 'Key priorities');
    if ($pos_end < 10) {
      $pos_end = 1500;
    }
    echo substr($nodeValueEn, 0, $pos_end);
    echo "\n\nsleep 2 s\n";
    sleep(2);
  }
  if ($node->body->value != $nodeValueEn) {
    $nodeValueEn = dealWithEncoding($nodeValueEn);
    $node->body->value = $nodeValueEn;

    $changed = $node->get('field_modified')->getValue();
    $changed = reset($changed);
    if (is_bool($changed)) {
      $changed = time();
    }
    else {
      $changed = current($changed);
      $changed = (isset($changed) && !empty($changed)) ? strtotime($changed) : time();
    }
    $node->changed = $changed;
    //$node->setNewRevision(TRUE);
    //$msg = 'update internal reference for english, reference conversion script';
    //$node->setRevisionLogMessage($msg);
    $node->setSyncing(TRUE);
    $node->save();
  }
  else if ($debug) {
    echo "********************************** END processing node id: $nodeId English **************************************************** \n";
    echo "c = continue:";
    $input = rtrim(fgets(STDIN));
    while (!$input == 'c' && !$input == 'C') {
      echo "\n c = continue:";
      $input = rtrim(fgets(STDIN));
    }
  }

  //update French content
  if ($node->hasTranslation("fr")) {
    $trnode = $node->getTranslation('fr');
    $nodeValueFr = $trnode->body->value;
    if ($nodeId < 800) {
      $nodeValueFr = cleanFollowus($nodeValueFr);
    }
    if ($debug) {
      echo "********************************** BEGIN processing node id: $nodeId Français **************************************************** \n";
    }
    $nodeValueFr = updateInternalReference($nodeValueFr);
    if ($trnode->body->value != $nodeValueFr) {
      $nodeValueFr = dealWithEncoding($nodeValueFr);
      $trnode->body->value = $nodeValueFr;
      $trnode->changed = $changed;
      //$trnode->setNewRevision(TRUE);
      //$msg = 'update internal reference for french, reference conversion script';
      //$trnode->setRevisionLogMessage($msg);
      $trnode->setSyncing(TRUE);
      $trnode->save();
      usleep(5000);
    }
    else if ($debug) {
      echo "********************************** END processing node id: $nodeId Français **************************************************** \n";
      echo "c = continue:";
      $input = rtrim(fgets(STDIN));
      while (!$input == 'c' && !$input == 'C') {
        echo "\n c = continue:";
        $input = rtrim(fgets(STDIN));
      }
    }
  }
  $count++;
  echo "\n";
  echo $nodeId;
  echo "\n";

}

/*$result = $db->getDistinctNodeIdFromNodeBody();


foreach ($result as $row) {

    echo $row->entity_id . "\n" ;
    $nodeBodys = $db->getNodeBodyByEntityId($row->entity_id);
    foreach ($nodeBodys as $nodeBody) {
      $page = $nodeBody->body_value;
      // echo "\n count = " . $count++;
      $page = updateInternalReference($page);
      if ($nodeBody->body_value != $page) {
        $nodeBody->body_value = $page;
        $db->updateNodeBody($nodeBody);
      }
    }

}
*/

// Rapport des échouées.
foreach ($dcrReferenceFailureArray as $matchedId => $failure_details) {

   $surPages = [];
   $surDcrids = [];
   foreach ($failure_details as $faildetail) {
     $nid = $faildetail['nodeId'];
     $surPages[] = $nid;
     if (is_numeric($nid)) {
       $dbops = new DBOperations();
       $surDcrids[] = $dbops->getDcridByNid($nid);
     }
     else {
       $surDcrids[] = $nid;
     }
   }
   $surPages=array_unique($surPages);
   $surDcrids=array_unique($surDcrids);
   if (in_array($count, $surPages)) {
    $nombreEchoue = count($surPages)-1;
   }
   else {
    $nombreEchoue = count($surPages);
   }
   if (in_array($count, $surDcrids)) {
    $nombreEchoueDcr = count($surDcrids)-1;
   }
   else {
    $nombreEchoueDcr = count($surDcrids);
   }

   $surPages = implode(',', $surPages);
   $surDcrids = implode(',', $surDcrids);
   $surPages = str_replace((','.$count) , '' , $surPages);
   $surDcrids = str_replace((','.$count) , '' , $surDcrids);
   echo "$matchedId failed $nombreEchoue times on drupal nodeIds: $surPages \n";
   echo "$matchedId failed $nombreEchoueDcr times on teamsite dcrIds: $surDcrids \n";
}
echo "\n count = " . $count . "\n";

function cleanFollowus($page) {
  global $nodeId;
  if ($nodeId > 800) {
    return $page;
  }
  $re = '/\.followus .*./m';
  $subst = "";

  $result = preg_replace($re, $subst, $page);
  if ($result != $page) {
    echo "Cleaned up followus css styles for nid: $nodeId\n";
  }
  else {
    return $page;
  }
  return $result;
}

// $page = file_get_contents('/app/d8tools/convertTeamsiteIdToDrupal/testdata.txt');
// $page = updateInternalReference($page);
// echo $page . "\n";
//echo page;
function updateInternalReference($page){

    $matches = array();

    // NEXT, figure out attributes processing.
    $re = '/<a (class=".*?"\ |title=".*?"\ |)href=\"(\\\\"|[^"])*(\/?e?f?r?a?n?g?\/?[\d]{13})\/{1}?[\d]{0,13}(#{0,1}.*?)">/mi';
    $page_before = $page;
    for ($i = 0; $i < 1000; $i++) {
      preg_match_all($re, $page, $matches, PREG_SET_ORDER, 0);
      if (!empty($matches)) {
        $page = findAndReplace($matches, $page);
      }
      if (empty($matches) || $page_before == $page) {
        // Processing of this pattern is complete.
        // Exit this structure.
        $i+=1000;
        break;
      }
    }

    $re = '/<a href=\"\\\\\\\\"|[^"]*(\/eng\/|\/fra\/{1})(([\d]{13}){1})(\/[\d]{0,13}){0,1}">/mi';
    $page_before = $page;
    for ($i = 0; $i < 1000; $i++) {
      preg_match_all($re, $page, $matches, PREG_SET_ORDER, 0);
      if (!empty($matches)) {
        $page = findAndReplace($matches, $page);
      }
      if (empty($matches) || $page_before == $page) {
        // Processing of this pattern is complete.
        // Exit this structure.
        $i+=1000;
        break;
      }
    }

    // SKIP ALL THE REST!!!! TEST CFIA TODAY.
    return $page;

    preg_match_all('<a href=\"(\/{0,1}display-afficher.do\?id=\s{0,1}[\d]{13})">', $page, $matches);
    $page = findAndReplace($matches, $page);

    preg_match_all('<a href=\"(\S+\?id=\s{0,1}[\d]{13})">', $page, $matches);
    $page = findAndReplace($matches, $page);

//    preg_match_all('<a href=\"(\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);
    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(\/?display-afficher.do|\/?|https?:\/\/.{0,6}?|)(agr.gc.ca\S+\?id=\s{0,1}[\d]{13}\S?.*?|dairyinfo.gc.ca\S+\?id=\s{0,1}[\d]{13}\S?.*?|infolait.gc.ca\S+\?id=\s{0,1}[\d]{13}\S?.*?|\S+\?id=\s{0,1}[\d]{13}\S?.*?)("? ?class=".*?"?|ref=".*?"|"? ?title=".*?"?|)">', $page, $matches);
    $page = findAndReplace($matches, $page);
//    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(\/?display-afficher.do|\/?|https?:\/\/.{0,6}?|)(agr.gc.ca\S+\?id=\s{0,1}[\d]{13}|dairyinfo.gc.ca\S+\?id=\s{0,1}[\d]{13}|infolait.gc.ca\S+\?id=\s{0,1}[\d]{13}|\S+\?id=\s{0,1}[\d]{13})("? ?class=".*?"?|ref=".*?"|"? ?class=".*?"?|)">', $page, $matches);
//    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(https?:\/\/.{0,6}?|)(agr.gc.ca\S+|dairyinfo.gc.ca\S+|infolait.gc.ca\S+|\S+)(\/eng\/.*?|\/fra.*?|)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d)("? ?class=".*?"?|ref=".*?"|"? ?class=".*?"?|)">', $page, $matches);
//    $page = findAndReplace($matches, $page);


//    preg_match_all('<a href=\"(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

//    preg_match_all('<a href=\"(https?:\/\/.*?)(agr.gc.ca|dairyinfo.gc.ca|infolait.gc.ca)(\S*?)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

//    preg_match_all('<a (class=\".*?\" |title=\".*?\" |)href=\"(https?:\/\/.*?)(agr.gc.ca|dairyinfo.gc.ca|infolait.gc.ca)(\S*?)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

  //preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(https?:\/\/\S{0,300}|w*\..*?[^ ]|)(\/eng\/.*?[^ ]|\/fra.*?[^ ]|)(\?id=\S+.\d?)">', $page, $matches);
//    preg_match_all('<a (class=".*?"\ |title=".*?"\ |)href=\"?(https?:\/\/\S{0,300}|w*\..*?[^ ]|)(\/eng\/.*?|\/fra.*?|)(display-afficher.do\?id=\S+.\d?|\?id=\S+.\d?)">', $page, $matches);
//    $page = findAndReplace($matches, $page);

//    preg_match_all('/<a (class=".*?" |)href=\"?(http:\/\/\S{0,200}|w*\..*?|)(.*?"|\/eng\/.*?|\/fra.*?|)(\?id=\S+.\d?)">/i', $page, $matches);

    return $page;
}


function findAndReplace($matches, $page) {
  global $debug;
  global $nodeId;
  if (!isset($matches[0])) {
    echo "findAndReplace() - already successfully processed these matches for nid: $nodeId\n";
    return $page;
  }
  foreach ($matches as $cle_ex => $match) {
    $tempArray=$match;
    foreach ($match as $cle => $val) {
        // echo "matched: " . $val[0] . "\n";
        // echo "part 1: " . $val[1] . "\n";
        if ($debug) {
          echo 'val=' . $val . "\n" ;
          echo 'cle=' . $cle . "\n" ;
        }
        if (strlen($val) === 13 && is_numeric($val)) {
          if (isset($tempArray[$cle-3])) {
            $val = $tempArray[$cle-3];
          }
        }
        if (strpos($val, '.pdf') > 5
        || strpos($val, '.jpg') > 5
        || strpos($val, '.jpeg') > 5
        || strpos($val, '.gif') > 5
        || strpos($val, '.docx') > 5
        || strpos($val, '.htm') > 5
        || strpos($val, '.png') > 5) {
          continue;
        }
        echo 'val=' . $val . "\n" ;
        echo 'cle=' . $cle . "\n" ;

        $attributes = strtolower($tempArray[$cle]);
        $next_attributes = '';
        if (isset($tempArray[1]) && !empty($tempArray[1])) {
          $next_attributes = strtolower($tempArray[1]);
          $next_attributes = trim($next_attributes);
        }
        if (empty($next_attributes) || strpos($next_attributes, 'class=') < 0) {
          if (isset($tempArray[4])) {
            $next_attributes = strtolower($tempArray[4]);
            $next_attributes = trim($next_attributes);
          }
        }
        $attributes = trim($attributes);
        $next_attributes = str_replace('" class=', 'class=', $next_attributes);
        $next_attributes = str_replace('" alt class=', 'class=', $next_attributes);
        if (strpos($next_attributes, 'class=') > 0) {
          $pos = strpos($next_attributes, 'class=');
          $next_attributes = substr($next_attributes, $pos);
        }
        if ($debug) {
          echo "attributes: $attributes\n";
          echo "next_attributes: $next_attributes\n";
          usleep(3);
        }
        if (strpos($next_attributes, 'lass=') >= 1) {
          // Some class= are not wrapped in a double quote, check for these.
          if (substr_count($next_attributes, '"') == 1) {
            $pos_quote = strpos($next_attributes, '"');
            $length_of_this = strlen($next_attributes);
            if ($length_of_this > ($pos_quote + 3)) {
              // Add missing double quotes.
              $next_attributes .= '"';
            }
          }
        }
        echo "next_attributes: $next_attributes\n";

        //skip if the link to AgriDoc
        if (stripos($val, "AgriDoc.do?") !== false) {
          echo "Agridoc ignor: " . $val . "\n" ;
          continue;
        }

        // $position = strpos($val, "?id=", 0);
        // //
        // $matchedId =  substr($val,$position+4, -1);
        // $position = strpos($matchedId, "&amp;lang", 0);
        // // echo $matchedId . "\n" ;
        // // echo $position . "\n" ;
        // $matchedAnchor = '';
        // $positionAnchor = strpos($matchedId, "#", 0);
        // if($positionAnchor>0)
        //   $matchedAnchor = substr($matchedId, $positionAnchor);
        // if($position>0)
        //   $matchedId = substr($matchedId, 0, $position);

        //if find match of pattern like "id=1580246566002", extract matchedId
        if (preg_match('/([0-9]{13})/', $val, $matchedIDs)) {
          // $matchedString = $matchedIDs[0][0];
          // $position = strpos($val, "?id=", 0);
          $matchedId = $matchedIDs[1];
          if (strlen($matchedId) !== 13) {
            if (preg_match('/[\/]([0-9]{13})[\/]/', $val, $matchedIDs)) {
              echo "BEFORE\n";
              echo "Unexpected matchedId = $matchedId\n";
              echo "AFTER\n";
              $matchedId = $matchedIDs[1];
              echo "\nmatchedId = $matchedId\n";
            }
            else {
              echo "NOTICE:\n";
              echo "nodeId = $nodeId\n";
              echo "Unexpected matchedId = $matchedId\n";
              continue;
            }
          }
          else if ($debug && $val == '1706821902464') {
            echo "trouvé ce qu'on cherchait: val=$val\n";
            echo "next_attributes: $next_attributes\n";
            sleep(2);
          }

          //get anchor value;
          $matchedAnchor = '';
          if (preg_match('/#(.*?)\"/', $val, $match)) {
            $matchedAnchor = "#" . $match[1];
          }

          global $checked_ids_array;
          $checked_ids_array;
          if (!isset($checked_ids_array[$matchedId])) {
            $node = findDrupalNodeIdByTeamsiteId($matchedId);
            $checked_ids_array[$matchedId] = $node;
          }
          else {
            $node = $checked_ids_array[$matchedId];
          }
          if (isset($node[0])) {
            echo 'node id=' . $node[0]->nid . "\n";
          }
          else {
            echo 'node id= no node id found with matchedId: ' . $matchedId . "\n";
            // Less noise.
            continue;
          }

          if ($matchedId=='1512149634601') {
            echo 'ATTRIBUTES $attributes=';
            echo "\n";
            echo $attributes;
            echo "\n";
            echo "cle = " . $cle . "\n";
            echo "\n";
            echo "$val";
            echo "\n";
            // echo print_r($tempArray, TRUE) . "\n";
            //echo "sleep(1)\n";
            //sleep(1);
          }
          if (!empty($attributes) && empty($node) && (stripos($attributes, 'href') < 4 && stripos($attributes, 'href') >= 0)) {
            $matchedId = $attributes;
            $val = $attributes;
            $attributes = '';
            if (strlen($matchedId) !== 13) {
              if (preg_match('/[\/]([0-9]{13})[\/]/', $val, $matchedIDs)) {
                echo "BEFORE\n";
                echo "Unexpected matchedId = $matchedId\n";
                echo "AFTER\n";
                $matchedId = $matchedIDs[1];
                echo "\nmatchedId = $matchedId\n";
                echo "\nval = $val\n";
                echo "\nnodeId = $nodeId\n";
                $node = findDrupalNodeIdByTeamsiteId($matchedId);
                if (isset($node[0])) {
                  echo 'node id=' . $node[0]->nid . "\n";
                }
                else {
                  echo 'node id= no node id found with matchedId: ' . $matchedId . "\n";
                }
              }
              else {
                echo "NOTICE:\n";
                echo "nodeId = $nodeId\n";
                echo "Unexpected matchedId = $matchedId\n";
                continue;
              }
            }
          }
          if ($debug && $val == '1706821902464') {
            echo "Trouvé ce qu'on cherchait: val=$val\n";
            echo "next_attributes: $next_attributes\n";
            echo "attributes: $attributes\n";
            echo "val: $val\n";
            echo "ref uuid: " . $node[0]->uuid . "\n";
          }
          if (stripos($val, 'href') > 0 && !empty($next_attributes) && !empty($node) && stripos($next_attributes, 'class=') >= 0) {
            echo "anchor has a class attribute.\n";
            echo                      "<a " . $next_attributes . " data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\">" . "\n";
            $page = str_replace($val, "<a " . $next_attributes . " data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\">", $page);
            echo "\n";
            global $referenceSuccessArray;
            if (!isset($referenceSuccessArray[$nodeId])) {
              $referenceSuccessArray[$nodeId][] = [
                'attribute'    => $next_attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            else {
              $successCount = count($referenceSuccessArray[$nodeId]);
              $successCount++;
              $referenceSuccessArray[$nodeId][$successCount-1] = [
                'attribute'    => $next_attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            //echo "Test, debug what is happening for nid:" . $node[0]->nid . "\n";
          }
          if (!empty($node) && stripos($val, 'href') > 0 && !stripos($next_attributes, 'class=') >= 0) {
            echo "val = " . $val ."\n";
            echo 'matchedAnchor=' . $matchedAnchor . "\n" ;
            echo 'cle=' . $cle . "\n" ;
            echo 'nid=' . $node[0]->nid . "\n" ;
            echo                      "<a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\">" . "\n";
            $page = str_replace($val, "<a data-entity-substitution=\"canonical\" data-entity-type=\"node\" data-entity-uuid=" . $node[0]->uuid ." href=\"/node/" . $node[0]->nid . $matchedAnchor . "\">", $page);
            echo "\n";
            global $referenceSuccessArray;
            if (!isset($referenceSuccessArray[$nodeId])) {
               $referenceSuccessArray[$nodeId][] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            else {
              $successCount = count($referenceSuccessArray[$nodeId]);
              $successCount++;
              $referenceSuccessArray[$nodeId][$successCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
          } else if ($nodeId > 0 && $matchedId > 0) {
            echo "DEBUG JOSEPH PAY ATTENTION.\n";
            echo "nodeId = " . $nodeId ."\n";
            echo "val = " . $val ."\n";
            echo "matchedId = " . $matchedId ."\n";
            echo 'matchedAnchor=' . $matchedAnchor . "\n" ;
            echo 'cle=' . $cle . "\n" ;
          } else {
            //global $nodeId;
            echo " node_id = " . $nodeId;
            echo " dcr_id not existing in node table = " . $matchedId ."\n";
            global $referenceFailureArray;
            if (!isset($referenceFailureArray[$nodeId])) {
              $referenceFailureArray[$nodeId][0] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            else {
              $failureCount = count($referenceFailureArray[$nodeId]);
              $failureCount++;
              $referenceFailureArray[$nodeId][$failureCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'matchedId'    => $matchedId
              ];
            }
            global $dcrReferenceFailureArray;
            $dcrFailCnt;
            $dcrReferenceFailureArray[$matchedId];
            if (!isset($dcrReferenceFailureArray[$matchedId])) {
              $dcrReferenceFailureArray[$matchedId][0] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'nodeId'    => $nodeId
              ];
            }
            else {
              $failureCount = count($dcrReferenceFailureArray[$matchedId]);
              $failureCount++;
              $dcrReferenceFailureArray[$matchedId][$failureCount-1] = [
                'attribute'    => $attributes,
                'matchedAnchor'=> $matchedAnchor,
                'nodeId'    => $nodeId
              ];
            }
          }
        }
    }
  }
  return $page;
}


/*
 * search for Drupal id by using Teamsite Id after content is imported into Drupal
 */
function findDrupalNodeIdByTeamsiteId($param) {
    $db = new DBOperations();
    $node = $db->getNodeBydcrId($param);
    //TODO
    return $node;
}

function processMenuLink($menu_links){
  if (!empty($menu_links)) {
    foreach ($menu_links as $menu_link) {
      // $link = [];
      // //$link['type'] = 'menu_link';
      // $link['mlid'] = $menu_link->id->value;
      // //$link['plid'] = $menu_link->parent->value ?? '0';
      // $link['menu_name'] = $menu_link->menu_name->value;
      // $link['link_title'] = $menu_link->title->value;
      $link = $menu_link->link->uri;
      //$link['options'] = $menu_link->link->options;
      //$link['weight'] = $menu_link->weight->value;
      // print_r($link);
      //https://www.agr.gc.ca/eng/news-from-agriculture-and-agri-food-canada/?id=1360882468517
      //<a href="/eng/news-from-agriculture-and-agri-food-canada/events/?id=1281614162882">Events</a> //fonctionne pas
      //<a href="?id=1603280592682">Minister Bibeau commemorates the 75<sup>th</sup> anniversary of   the founding of the Food and Agriculture Organization</a> //fonctionne bien
      // OLD https://regex101.com/r/5Fxpss/1/
      // NEW https://regex101.com/r/YVwYDb/1

      if (startsWith($link ,"https://inspection.canada.ca") ||
      startsWith($link ,"https://www.inspection.gc.ca") ||
      startsWith($link ,"https://www.inspection.canada.ca")) {
        $position = strpos($link, "/eng/", 0);
        if ($position < 2) {
          $position = strpos($link, "/fra/", 0);
        }
        //
        $matchedId =  substr($link,$position+4);
        $position = strpos($matchedId, "&amp;lang", 0);
        // echo $matchedId . "\n" ;
        // echo $position . "\n" ;
        if($position>0)
          $matchedId = substr($matchedId, 0, $position);
        $res = preg_replace("/[^0-9]/", "", $matchedId );

        echo $link . "\n" ;
        echo $matchedId . "\n" ;
        echo $res . "\n";

        $node = findDrupalNodeIdByTeamsiteId($matchedId);
        if (!empty($node)) {
          $menu_link->link->uri = 'entity:node/' . $node[0]->nid;
          $menu_link->save();
        }

        //handle translation
        // if (!$menu_link->hasTranslation('fr') && !empty($titleFr)) {
        //   $tr_menu_link = $menu_link->getTranslation('fr');
        //   $trlink = $tr_menu_link->link->uri;
        //   if(startsWith($trlink ,"https://www.agr.gc.ca")) {
        //     $position = strpos($trlink, "?id=", 0);
        //     //
        //     $matchedId =  substr($trlink,$position+4);
        //     $position = strpos($matchedId, "&amp;lang", 0);
        //     // echo $matchedId . "\n" ;
        //     // echo $position . "\n" ;
        //     if($position>0)
        //       $matchedId = substr($matchedId, 0, $position);
        //     $res = preg_replace("/[^0-9]/", "", $matchedId );
        //
        //     echo $link . "\n" ;
        //     echo $matchedId . "\n" ;
        //     echo $res . "\n";
        //
        //     $node = findDrupalNodeIdByTeamsiteId($matchedId);
        //     print_r($node);
        //     if (!empty($node)) {
        //       $tr_menu_link->link->uri = 'entity:node/' . $node[0]->nid;
        //       $tr_menu_link->save();
        //     }
        //   }
        // }

      }
    }
  }
}


function startsWith ($string, $startString) {
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}


/**
 * Make sure our html is not broken.
 */
function dealWithEncoding($content) {
  $dom = new \DOMDocument();
  libxml_use_internal_errors(true);
  $dom->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8'));
  libxml_use_internal_errors(false);
  $encoding = mb_detect_encoding($content);
  // pass it to the DOMDocument constructor
  $doc = new DOMDocument('', $encoding);
  // Must include the content-type/charset meta tag with $encoding.
  // Bad HTML will trigger warnings, suppress those.
  @$doc->loadHTML('<html><head>'
    . '<meta http-equiv="content-type" content="text/html; charset=UTF-8">'
    . '</head><body>' . trim($content) . '</body></html>');
   // extract the components we want
  $nodes = $doc->getElementsByTagName('body')->item(0)->childNodes;
  $html = '';
  $len = $nodes->length;
  for ($i = 0; $i < $len; $i++) {
    $html .= $doc->saveHTML($nodes->item($i));
  }
  return $html;
}

?>
