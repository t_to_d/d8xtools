{
    "dcr_id": "1327198930863",
    "title": {
        "en": "Facts about infectious salmon anaemia (ISA)",
        "fr": "Fiche de renseignements sur l'an\u00e9mie infectieuse du saumon"
    },
    "html_modified": "2024-02-13 9:47:14 AM",
    "modified": "2020-05-28",
    "issued": "2020-05-01 21:22:15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_isa_fs_1327198930863_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_isa_fs_1327198930863_fra"
    },
    "ia_id": "1327199219511",
    "parent_ia_id": "1327197115891",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1327197115891",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Facts about infectious salmon anaemia (ISA)",
        "fr": "Fiche de renseignements sur l'an\u00e9mie infectieuse du saumon"
    },
    "label": {
        "en": "Facts about infectious salmon anaemia (ISA)",
        "fr": "Fiche de renseignements sur l'an\u00e9mie infectieuse du saumon"
    },
    "templatetype": "content page 1 column",
    "node_id": "1327199219511",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1327197013896",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/isa/facts/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/ais/fiches/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Facts about infectious salmon anaemia (ISA)",
            "fr": "Fiche de renseignements sur l'an\u00e9mie infectieuse du saumon"
        },
        "description": {
            "en": "Infectious salmon anaemia (ISA) is a disease affecting finfish and is caused by a virus that belongs to a family of viruses called Orthomyxoviridae.",
            "fr": "L'an\u00e9mie infectieuse du saumon (AIS) est une maladie des poissons caus\u00e9e par un virus de la famille des Orthomyxoviridae."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, diseases, fact sheet, infectious salmon anaemia, Orthomyxoviridae, salmon",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, maladies, fiche de renseignements, an\u00e9mie infectieuse du saumon, orthomyxoviridae, saumon"
        },
        "dcterms.subject": {
            "en": "animal health,fish,imports,inspection,policy",
            "fr": "sant\u00e9 animale,poisson,importation,inspection,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-05-01 21:22:15",
            "fr": "2020-05-01 21:22:15"
        },
        "modified": {
            "en": "2020-05-28",
            "fr": "2020-05-28"
        },
        "type": {
            "en": "policy,reference material",
            "fr": "politique,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Facts about infectious salmon anaemia (ISA)",
        "fr": "Fiche de renseignements sur l'an\u00e9mie infectieuse du saumon"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-right mrgn-lft-md col-sm-5\"> \n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">What we know about the disease</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>outbreaks occur when water temperatures are between 3\u00b0C and 15\u00b0C</li>\n<li>an outbreak of ISA does not affect all the finfish in the facility at the same time</li>\n<li>removal of the affected finfish from the facility stops or substantially slows down the progression of the outbreak</li>\n<li>the ISA virus does not survive for long in natural seawater, only for a few hours</li>\n<li>most Pacific salmon and rainbow trout are resistant to ISA</li>\n</ul>\n</div>\n</section>\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">What we would like to know about the disease</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>which genes of the virus cause disease so that we can predict when a severe outbreak is likely to occur</li>\n<li>which genes of the virus can be reliably used to trace the spread of the infection or outbreak</li>\n<li>which genes of the virus cause infection of finfish so that a vaccine against infection can be developed</li>\n<li>mutation capability of the ISA virus from no or low significance to high significance and what factors increase or decrease the capability</li>\n<li>how long the ISA virus survives in naturally occurring freshwater</li>\n<li>susceptibility of pink salmon to infection with ISA virus and development of disease</li>\n<li>wild finfish reservoirs of the ISA virus for cultured finfish sharing the same habitat </li>\n</ul>\n</div>\n</section>\n</div>\n<p>Infectious salmon anaemia (ISA) is a disease affecting finfish and is caused by a virus that belongs to a family of viruses called <i lang=\"la\">Orthomyxoviridae</i>.</p>\n\n<p>ISA does not infect humans and is not a risk to food safety.</p>\n\n<p>A study led by the European Union in 2000 concluded that ISA cannot be transmitted to humans from live or dead animals.</p>\n\n<p>ISA is a reportable disease in Canada. If you own or work with finfish and suspect or detect ISA, you must report it to the Canadian Food Inspection Agency (CFIA). Here's how:</p>\n\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/aquaculturists/eng/1450407254532/1450407255317\">Notification of reportable diseases by aquaculturists</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/veterinarians/eng/1450406407952/1450406408789\">Notification of reportable diseases by veterinarians</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/researchers/eng/1450405664371/1450405665318\">Notification of reportable and immediately notifiable diseases by researchers</a></li>\n</ul>\n\n<h2>Infectious salmon anaemia in Canada</h2>\n\n<p>ISA was first detected in farmed Atlantic salmon in New Brunswick in 1996. Since then, the virus has also been detected in farmed Atlantic salmon in Nova Scotia, Prince Edward Island and Newfoundland and Labrador.</p>\n\n<p>ISA has been detected in wild salmon in Quebec but has not occurred in the rest of Canada, including the Pacific Ocean watershed of British Columbia.</p>\n\n<p><a href=\"/animal-health/aquatic-animals/diseases/finfish/eng/1450409829304/1450409830112\">Declarations by province and marine areas</a> provides information about where ISA occurs in Canada.</p>\n\n<p><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/federally-reportable-aquatic-animal-diseases/eng/1339174937153/1339175227861\">Federally reportable aquatic animal diseases in Canada</a> identifies cases of ISA we have confirmed in Canada.</p>\n\n<h2>Species of finfish that can be infected</h2>\n\n<p>Species of finfish that can become infected with the ISA virus are:</p>\n\n<ul>\n<li><i lang=\"la\">Salmo salar</i> (Atlantic salmon)</li>\n<li><i lang=\"la\">Salmo trutta</i> (brown trout)</li>\n<li><i lang=\"la\">Oncorhynchus mykiss</i> (rainbow trout)</li>\n</ul>\n\n<p>One genetic strain of rainbow trout has been experimentally infected with ISA. This strain of trout does not occur naturally in Canada and is not farmed. ISA has not been found in wild or farmed rainbow trout in Canada.</p>\n\n<h2>Signs of infectious salmon anaemia</h2>\n\n<p>Only some strains of the virus cause disease and potentially kill finfish. Most of the virus strains identified in the Atlantic region do not lead to disease and death. One strain in particular does not cause disease. This strain is described as non-pathogenic ISA virus. You may also see this strain described as non-deleted highly polymorphic region (HPR) ISA virus or HPR0.</p>\n\n<p>Not all disease-causing strains of ISA virus (also described as pathogenic ISA virus or HPR-deleted ISA virus) cause fatal disease. Some strains cause no obvious disease or a mild increase in disease. Some strains cause a moderate increase in disease and, less commonly, a few strains cause a severe increase in disease.</p>\n\n<p>ISA outbreaks are most often seen in cultured Atlantic salmon reared in seawater. They also occasionally occur in freshwater hatcheries of Atlantic salmon. The disease tends to be slow moving, so the death rate is initially low and increases over weeks or months.</p>\n\n<p>Affected finfish may exhibit any of the following external signs:</p>\n\n<ul>\n<li>loss of appetite</li>\n<li>separation from the main group of fish (typically found at edges or outlets of the pen or tank)</li>\n<li>slow swimming</li>\n<li>gulping air at the surface</li>\n<li>pale pink to grey gills</li>\n<li>swollen abdomen</li>\n<li>areas of bleeding on the skin of the flanks and belly</li>\n</ul>\n\n<p>Affected finfish may exhibit any of the following internal signs:</p>\n\n<ul>\n<li>dark and swollen kidney, liver and spleen</li>\n<li>areas of pinpoint bleeding in the fatty tissue surrounding organs</li>\n<li>areas of bleeding in the pyloric caeca, intestines and liver</li>\n<li>pale heart</li>\n<li>bloody fluid in the abdominal cavity and around the heart</li>\n</ul>\n\n<h2>Managing infectious salmon anaemia</h2>\n\n<p>No treatment is currently available for this disease, although a vaccine is available in Canada. Talk to your veterinarian about whether this vaccine can be an effective tool to manage this disease on your farm.</p>\n\n<p>The ISA virus can be spread between finfish that come into contact with discharged bodily wastes or mucus secretions from infected finfish. It can also be spread through water contaminated with the virus.</p>\n\n<p>People can also spread the virus to other finfish by moving infected live or dead finfish, by using contaminated equipment, vehicles and boats, or by using or moving contaminated water.</p>\n\n<p>You can prevent the introduction of the ISA virus into your farm and prevent the spread of the virus within your farm by using specific safety practices. You can find out more about what practices you need to consider by visiting <a href=\"/animal-health/aquatic-animals/aquatic-animal-biosecurity/eng/1320594187303/1320594268146\">Biosecurity for aquatic animals</a>. Talk to your veterinarian about what practices are most effective to manage this disease for your farm.</p>\n\n<p>In areas where ISA is known to occur, you may also need to follow additional practices that are required by your provincial ISA control program.</p>\n\n<h2>CFIA's initial response to a notification</h2>\n\n<p>When you notify a CFIA veterinary inspector of the possible presence of ISA in your finfish, we will launch an investigation, which could include an inspection of the facility.</p>\n\n<p>We will note all relevant information you can provide, such as species affected, life stages affected, signs of illness in the animals, vaccination history, presence of any other diseases and water temperature.</p>\n\n<p>At the same time, we will collect samples for testing at one of Canada's three National Aquatic Animal Health Laboratories.</p>\n\n<p>We may issue movement controls on the animals and equipment, including vehicles and boats. Movement controls are always issued immediately if the farm is in an area where ISA is not known to occur.</p>\n\n<p>We evaluate all the collected information and test results to confirm the presence of ISA on the farm.</p>\n\n<p>The Agency also confirms the presence of ISA in wild finfish. The Agency receives notifications from provincial or federal staff who have investigated a wild fish death event.</p>\n\n<h2>CFIA's response once the disease is confirmed</h2>\n\n<p>Further disease response measures will either eradicate ISA from the facility or geographically contain the disease. The response measures are also determined by whether the farm is located in an area where the disease occurs or is not known to occur. <a href=\"/animal-health/aquatic-animals/diseases/investigation-and-response/eng/1338313604809/1338313669210\">Aquatic animal disease investigation and response</a> provides information about CFIA's response. Although each disease investigation and response situation is different, the steps involved are often similar.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-right mrgn-lft-md col-sm-5\"> \n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">Ce que nous savons sur la maladie</h2>\n</header>\n<div class=\"panel-body\">\n  \n<ul>\n<li>les \u00e9closions surviennent lorsque la temp\u00e9rature de l'eau se situe entre 3\u00a0\u00b0C et 15\u00a0\u00b0C</li>\n<li>une \u00e9closion d'AIS ne touche pas tous les poissons de l'installation en m\u00eame temps</li>\n<li>le retrait des poissons atteints de l'\u00e9tablissement arr\u00eate ou ralentit consid\u00e9rablement la progression de l'\u00e9closion</li>\n<li>le virus de l'AIS ne survit pas longtemps dans l'eau de mer naturelle, seulement pendant quelques heures</li>\n<li>la plupart des saumons du Pacifique et des truites arc-en-ciel sont r\u00e9sistants \u00e0 l'AIS</li>\n</ul>\n</div>\n</section>\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">Ce que nous aimerions savoir sur la maladie</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>quels sont les g\u00e8nes du virus qui provoquent la maladie afin que nous puissions pr\u00e9dire \u00e0 quel moment une grave \u00e9closion est susceptible de survenir</li>\n<li>quels sont les g\u00e8nes du virus qui peuvent \u00eatre utilis\u00e9s de mani\u00e8re fiable pour suivre la propagation de l'infection ou de l'\u00e9closion</li>\n<li>quels sont les g\u00e8nes du virus qui provoquent l'infection des poissons afin qu'un vaccin contre l'infection puisse \u00eatre mis au point</li>\n<li>quelle est la capacit\u00e9 de mutation du virus de l'AIS, pour passer d'une importance nulle ou faible \u00e0 une importance \u00e9lev\u00e9e, et les facteurs qui augmentent ou diminuent cette capacit\u00e9</li>\n<li>quelle est la dur\u00e9e de survie du virus de l'AIS dans l'eau douce naturelle</li>\n<li>quelle est la vuln\u00e9rabilit\u00e9 du saumon rose \u00e0 l'infection par le virus de l'AIS et au d\u00e9veloppement de la maladie</li>\n<li>quelles sont les populations de poisson sauvages qui servent de r\u00e9servoirs du virus de l'AIS pour les poissons d'\u00e9levage partageant le m\u00eame habitat</li>\n</ul>\n</div>\n</section>\n</div>\n<p>L'an\u00e9mie infectieuse du saumon (AIS) est une maladie des poissons caus\u00e9e par un virus de la famille des <i lang=\"la\">Orthomyxoviridae</i>.</p>\n\n<p>L'AIS n'infecte pas les humains et ne pose aucun risque pour la salubrit\u00e9 des aliments.</p>\n\n<p>Une \u00e9tude men\u00e9e par l'Union europ\u00e9enne en 2000 a conclu que l'AIS ne peut pas \u00eatre transmise aux humains par des animaux vivants ou morts.</p>\n\n<p>L'AIS est une maladie \u00e0 d\u00e9claration obligatoire au Canada. Si vous poss\u00e9dez ou manipulez des poissons dans le cadre de votre travail et que vous soup\u00e7onnez ou d\u00e9tectez la pr\u00e9sence de l'AIS, vous devez aviser l'Agence canadienne d'inspection des aliments (ACIA). Voici comment proc\u00e9der\u00a0:</p>\n\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/aquaculteurs/fra/1450407254532/1450407255317\">D\u00e9claration obligatoire des maladies d\u00e9clarables d'animaux aquatiques par les aquaculteurs</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/veterinaires/fra/1450406407952/1450406408789\">D\u00e9claration obligatoire des maladies d\u00e9clarables d'animaux aquatiques par les v\u00e9t\u00e9rinaires</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/chercheurs/fra/1450405664371/1450405665318\">D\u00e9claration obligatoire des maladies d\u00e9clarables ou \u00e0 notification imm\u00e9diate d'animaux aquatiques par les chercheurs</a></li>\n</ul>\n\n<h2>An\u00e9mie infectieuse du saumon au Canada</h2>\n\n<p>L'AIS a \u00e9t\u00e9 d\u00e9tect\u00e9e pour la premi\u00e8re fois chez le saumon de l'Atlantique d'\u00e9levage au Nouveau-Brunswick en 1996. Depuis lors, le virus a \u00e9galement \u00e9t\u00e9 d\u00e9tect\u00e9 chez le saumon de l'Atlantique d'\u00e9levage en Nouvelle-\u00c9cosse, \u00e0 l'\u00cele-du-Prince-\u00c9douard et \u00e0 Terre-Neuve-et-Labrador.</p>\n\n<p>L'AIS a \u00e9t\u00e9 d\u00e9tect\u00e9e chez le saumon sauvage au Qu\u00e9bec mais la maladie n'est pas apparue dans le reste du Canada, y compris le bassin versant de l'oc\u00e9an Pacifique en Colombie-Britannique.</p>\n\n<p>La page Web <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/poissons/fra/1450409829304/1450409830112\">D\u00e9clarations par province et zone marine</a> fournit des renseignements sur les endroits o\u00f9 l'AIS s'est manifest\u00e9e au Canada.</p>\n\n<p>La page Web <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/maladies-des-animaux-aquatiques-a-declaration-obli/fra/1339174937153/1339175227861\">Maladies des animaux aquatiques \u00e0 d\u00e9claration obligatoire au Canada</a> \u00e9num\u00e8re les cas d'AIS que nous avons confirm\u00e9s au Canada.</p>\n\n<h2>Esp\u00e8ces de poissons qui peuvent \u00eatre infect\u00e9es</h2>\n\n<p>Les esp\u00e8ces de poissons qui peuvent \u00eatre infect\u00e9es par le virus de l'AIS sont les suivantes\u00a0:</p>\n\n<ul>\n<li><i lang=\"la\">Salmo salar</i> (saumon de l'Atlantique)</li>\n<li><i lang=\"la\">Salmo trutta</i> (truite brune)</li>\n<li><i lang=\"la\">Oncorhynchus mykiss</i> (truite arc-en-ciel)</li>\n</ul>\n\n<p>Une souche g\u00e9n\u00e9tique de truite arc-en-ciel a \u00e9t\u00e9 infect\u00e9e exp\u00e9rimentalement par l'AIS. Cette souche de truite n'est pas naturellement pr\u00e9sente au Canada et ne fait pas l'objet d'\u00e9levage. L'AIS n'a pas \u00e9t\u00e9 d\u00e9tect\u00e9e chez la truite arc-en-ciel sauvage ou d'\u00e9levage au Canada.</p>\n\n<h2>Signes de l'an\u00e9mie infectieuse du saumon</h2>\n\n<p>Seules certaines souches du virus provoquent la maladie et peuvent tuer les poissons. La plupart des souches du virus recens\u00e9es dans la r\u00e9gion de l'Atlantique n'entra\u00eenent pas la maladie ni la mort. Une souche en particulier ne provoque pas la maladie. Cette souche est d\u00e9crite comme virus non pathog\u00e8ne de l'AIS. Vous pouvez \u00e9galement voir cette souche d\u00e9crite comme le virus de l'AIS qui ne pr\u00e9sentent pas de d\u00e9l\u00e9tion dans la r\u00e9gion hautement polymorphe (RHP) ou RHP0.</p>\n\n<p>Ce ne sont pas toutes les souches du virus de l'AIS capables de causer la maladie (\u00e9galement qualifi\u00e9 de virus pathog\u00e8ne de l'AIS ou de virus de l'AIS qui pr\u00e9sente des d\u00e9l\u00e9tions dans la RHP) qui provoquent une maladie mortelle. Certaines souches ne provoquent aucune maladie \u00e9vidente ou elles provoquent une l\u00e9g\u00e8re augmentation de la maladie. Certaines souches provoquent une augmentation mod\u00e9r\u00e9e de la maladie et, moins fr\u00e9quemment, quelques souches provoquent une grave augmentation de la maladie.</p>\n\n<p>Les \u00e9closions d'AIS sont le plus souvent observ\u00e9es chez les saumons de l'Atlantique \u00e9lev\u00e9s dans l'eau de mer. Elles se produisent aussi parfois dans les \u00e9closeries de saumons de l'Atlantique en eau douce. La maladie a tendance \u00e0 \u00e9voluer lentement, de sorte que le taux de mortalit\u00e9 est initialement faible et augmente au fil des semaines ou des mois.</p>\n\n<p>Les poissons atteints peuvent pr\u00e9senter les signes ext\u00e9rieurs suivants\u00a0:</p>\n\n<ul>\n<li>perte d'app\u00e9tit</li>\n<li>s\u00e9paration du groupe principal de poissons (on les trouve g\u00e9n\u00e9ralement sur les bords ou aux sorties de l'enclos ou du bassin de pisciculture)</li>\n<li>nage lente</li>\n<li>prise d'air \u00e0 la surface</li>\n<li>branchies rose p\u00e2le \u00e0 gris</li>\n<li>gonflement de l'abdomen</li>\n<li>saignement sur la peau des flancs et du ventre</li>\n</ul>\n\n<p>Les poissons atteints peuvent pr\u00e9senter l'un des signes internes suivants\u00a0:</p>\n<ul>\n<li>gonflement et brunissement des reins, du foie et de la rate</li>\n<li>p\u00e9t\u00e9chies dans les tissus adipeux autour des organes</li>\n<li>saignements dans le c\u00e6cum pylorique, les intestins et le foie</li>\n<li>c\u0153ur de coloration p\u00e2le</li>\n<li>liquide teint\u00e9 de sang dans la cavit\u00e9 abdominale et autour du c\u0153ur</li>\n</ul>\n\n<h2>Gestion de l'an\u00e9mie infectieuse du saumon</h2>\n\n<p>Aucun traitement n'est actuellement disponible pour cette maladie, bien qu'un vaccin soit offert au Canada. Demandez \u00e0 votre v\u00e9t\u00e9rinaire si ce vaccin peut \u00eatre un outil efficace pour g\u00e9rer cette maladie dans votre exploitation.</p>\n\n<p>Le virus de l'AIS peut se propager aux poissons qui entrent en contact avec des excr\u00e9ments ou des s\u00e9cr\u00e9tions de mucus provenant de poissons infect\u00e9s. Il peut \u00e9galement se propager par l'eau contamin\u00e9e par le virus.</p>\n\n<p>Les \u00eatres humains peuvent \u00e9galement transmettre le virus \u00e0 d'autres poissons en d\u00e9pla\u00e7ant des poissons infect\u00e9s, qu'ils soient vivants ou morts, en utilisant de l'\u00e9quipement, des v\u00e9hicules et des navires contamin\u00e9s, ou en utilisant ou d\u00e9pla\u00e7ant de l'eau contamin\u00e9e.</p>\n\n<p>Vous pouvez pr\u00e9venir l'introduction du virus de l'AIS dans votre exploitation et emp\u00eacher la propagation du virus au sein de votre exploitation en adoptant certaines pratiques en mati\u00e8re de s\u00e9curit\u00e9. Pour en savoir plus sur les pratiques que vous devez prendre en consid\u00e9ration, veuillez consulter la page Web intitul\u00e9e <a href=\"/sante-des-animaux/animaux-aquatiques/biosecurite-relative-aux-animaux-aquatiques/fra/1320594187303/1320594268146\">Bios\u00e9curit\u00e9 pour les animaux aquatiques</a>. Demandez \u00e0 votre v\u00e9t\u00e9rinaire quelles sont les pratiques les plus efficaces pour g\u00e9rer cette maladie dans votre exploitation.</p>\n\n<p>Dans les zones o\u00f9 l'AIS est pr\u00e9sente, vous devrez peut-\u00eatre \u00e9galement suivre des pratiques suppl\u00e9mentaires exig\u00e9es par le programme de lutte contre l'AIS de votre province.</p>\n\n<h2>Intervention initiale de l'ACIA suite \u00e0 une notification</h2>\n\n<p>Lorsque vous informez un v\u00e9t\u00e9rinaire-inspecteur de l'ACIA de la pr\u00e9sence possible de l'AIS dans votre population de poissons, nous lan\u00e7ons une enqu\u00eate, qui peut inclure une inspection de l'\u00e9tablissement.</p>\n\n<p>Nous prendrons note de tous les renseignements pertinents que vous pourrez nous fournir, tels que les esp\u00e8ces touch\u00e9es, les \u00e9tapes du cycle de vie touch\u00e9es, les signes de maladie chez les animaux, les ant\u00e9c\u00e9dents de vaccination, la pr\u00e9sence de toute autre maladie et la temp\u00e9rature de l'eau.</p>\n\n<p>Par la m\u00eame occasion, nous pr\u00e9l\u00e8verons des \u00e9chantillons pour analyses de laboratoire dans l'un des trois Laboratoires nationaux pour la sant\u00e9 des animaux aquatiques du Canada.</p>\n\n<p>Il se peut que nous imposions des mesures de contr\u00f4le des d\u00e9placements des animaux et de l'\u00e9quipement, y compris les v\u00e9hicules et les navires. Des mesures de contr\u00f4le des d\u00e9placements sont toujours imm\u00e9diatement mises en place lorsque l'exploitation se situe dans une zone o\u00f9 l'AIS n'est r\u00e9put\u00e9e \u00eatre pas pr\u00e9sente.</p>\n\n<p>Nous \u00e9valuons l'ensemble des renseignements recueillis et des r\u00e9sultats d'analyses pour confirmer la pr\u00e9sence de l'AIS \u00e0 l'exploitation.</p>\n\n<p>L'Agence confirme \u00e9galement la pr\u00e9sence de l'AIS chez les poissons sauvages. L'Agence re\u00e7oit des notifications du personnel provincial ou f\u00e9d\u00e9rale qui a enqu\u00eat\u00e9 sur un cas de d\u00e9c\u00e8s de poissons sauvages.</p>\n\n<h2>Intervention de l'ACIA une fois que la pr\u00e9sence de la maladie est confirm\u00e9e</h2>\n\n<p>D'autres mesures d'intervention pour lutter contre la maladie permettront soit d'\u00e9radiquer l'AIS de l'\u00e9tablissement, soit de limiter la maladie sur le plan g\u00e9ographique. Les mesures d'intervention sont \u00e9galement d\u00e9termin\u00e9es en fonction du fait que l'exploitation est situ\u00e9e dans une zone o\u00f9 la maladie est pr\u00e9sente ou n'est pas r\u00e9put\u00e9e \u00eatre pr\u00e9sente. La page Web <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/enquete-et-intervention/fra/1338313604809/1338313669210\">Enqu\u00eate et intervention en cas de maladie des animaux aquatiques</a> fournit des renseignements sur l'intervention de l'ACIA. Bien que chaque situation soit diff\u00e9rente, les \u00e9tapes \u00e0 suivre lors d'une enqu\u00eate et d'une intervention en cas de maladie sont souvent similaires.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}