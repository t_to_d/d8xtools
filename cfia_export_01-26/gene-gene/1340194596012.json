{
    "dcr_id": "1340194596012",
    "title": {
        "en": "Compliance and enforcement of gluten-free claims",
        "fr": "Mesures d'application de la loi et de conformit\u00e9 concernant les all\u00e9gations sans gluten"
    },
    "html_modified": "2024-01-26 2:21:10 PM",
    "modified": "2017-09-27",
    "issued": "2012-06-20 08:16:40",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/labeti_otherreq_claims_composition_glutenfree_1340194596012_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/labeti_otherreq_claims_composition_glutenfree_1340194596012_fra"
    },
    "ia_id": "1340194681961",
    "parent_ia_id": "1388152326591",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "Allergen considerations",
        "fr": "Consid\u00e9rations d'allerg\u00e8nes"
    },
    "activity": {
        "en": "Labelling",
        "fr": "\u00c9tiquetage"
    },
    "commodity": {
        "en": "Relevant to All",
        "fr": "Pertinents pour tous"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1388152326591",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Compliance and enforcement of gluten-free claims",
        "fr": "Mesures d'application de la loi et de conformit\u00e9 concernant les all\u00e9gations sans gluten"
    },
    "label": {
        "en": "Compliance and enforcement of gluten-free claims",
        "fr": "Mesures d'application de la loi et de conformit\u00e9 concernant les all\u00e9gations sans gluten"
    },
    "templatetype": "content page 1 column",
    "node_id": "1340194681961",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1388152325341",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/allergens-and-gluten/gluten-free-claims/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/allergenes-et-gluten/allegations-sans-gluten/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Compliance and enforcement of gluten-free claims",
            "fr": "Mesures d'application de la loi et de conformit\u00e9 concernant les all\u00e9gations sans gluten"
        },
        "description": {
            "en": "In all instances, if more than 20 ppm of gluten is present in a food labelled as gluten-free, the product may be subject to appropriate enforcement action by the CFIA, which may include the possibility of recall.",
            "fr": "Dans tous les cas, si, une concentration sup\u00e9rieure \u00e0 20 ppm de gluten est pr\u00e9sente dans un aliment \u00e9tiquet\u00e9 sans gluten, le produit peut faire l'objet de mesures d'application de la loi de la part de l'ACIA, notamment la possibilit\u00e9 d'un rappel."
        },
        "keywords": {
            "en": "label, labelling, advertising, allergen, sensitivity, gluten, claims",
            "fr": "&#233;tiquette, &#233;tiquetage, publicit&#233;, allerg&#232;ne, sensibilit&#233;, gluten, all&#233;gations"
        },
        "dcterms.subject": {
            "en": "labelling,food labeling,inspection,food inspection,legislation,policy,consumer protection,regulation",
            "fr": "\u00e9tiquetage,\u00e9tiquetage des aliments,inspection,inspection des aliments,l\u00e9gislation,politique,protection du consommateur,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exporation d'aliments et de la protect. des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-06-20 08:16:40",
            "fr": "2012-06-20 08:16:40"
        },
        "modified": {
            "en": "2017-09-27",
            "fr": "2017-09-27"
        },
        "type": {
            "en": "decision,fact sheet,legislation and regulations,reference material,policy",
            "fr": "d\u00e9cision,fiche de renseignements,l\u00e9gislation et r\u00e8glements,mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Compliance and enforcement of gluten-free claims",
        "fr": "Mesures d'application de la loi et de conformit\u00e9 concernant les all\u00e9gations sans gluten"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>A gluten-free claim is any representation in labelling or advertising that states, suggests or implies that a food is gluten-free, as per B.24.018 of the <i>Food and Drug Regulations</i> (FDR).</p>\n<p><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/food-safety/food-allergies-intolerances/celiac-disease/health-canada-position-gluten-free-claims.html\">Health Canada's current position on gluten-free claims</a> will be used to guide the  <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s compliance activities and enforcement actions.</p>\n<p>Any gluten that is present due to cross-contamination in a food labelled gluten-free should be as low as reasonably achievable and must not surpass 20\u00a0<abbr title=\"parts per million\">ppm</abbr> of gluten, a level that is considered protective for the majority of people with Celiac disease.</p>\n<p>Manufacturers and importers of gluten-free foods are expected to make every reasonable effort to minimize gluten present through cross-contamination. In instances where the gluten is present due to cross-contamination at a level of less than 20\u00a0<abbr title=\"parts per million\">ppm</abbr> in the food, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will follow up with the manufacturer or importer regarding the presence of gluten in the product. These manufacturers and importers should have good manufacturing/importing practices (GMP/GIP) in place to achieve the lowest levels of gluten possible to avoid cross-contamination. However, based on Health Canada's position, enforcement action on products containing less than 20\u00a0<abbr title=\"parts per million\">ppm</abbr> gluten as a result of cross-contamination will not include a recall of the product, nor a request to remove the gluten-free claim.</p>\n<p>In all instances, regardless of source, if more than 20\u00a0<abbr title=\"parts per million\">ppm</abbr> of gluten is present in a food labelled as gluten-free, the product may be in violation of the <abbr title=\"Food and Drug Regulations\">FDR</abbr> Section B.24.018 and/or Section 5.1 of the <i>Food and Drugs Act</i> (FDA) and, on the basis of a health risk assessment provided by Health Canada, subject to appropriate enforcement action by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, which may include the possibility of recall.</p>\n<p>All information appearing on the labels or in advertising, including trademarks, brand names, pictures, logos, or slogans that suggest or imply a food is gluten-free will be assessed against the above criteria and may be subject to appropriate enforcement action by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> if determined to contravene the <abbr title=\"Food and Drugs Act\">FDA</abbr> and/or the <abbr title=\"Food and Drug Regulations\">FDR</abbr>.</p>\n<h2>Intentional addition</h2>\n<p>As described in Health Canada's position, foods containing gluten as a result of intentional addition may not be represented as being gluten-free. However, if a manufacturer using a cereal-derived ingredient includes additional processing steps which are demonstrated to be effective in removing gluten, the food may be represented as gluten-free.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Une all\u00e9gation sans gluten est un \u00e9nonc\u00e9 figurant sur l'\u00e9tiquette ou dans la publicit\u00e9 d'un produit qui indique de fa\u00e7on explicite ou implicite qu'un aliment est sans gluten, conform\u00e9ment \u00e0 l'article B.24.018 du <i>R\u00e8glement sur les aliments et drogues</i> (RAD).</p>\n<p>La <a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/salubrite-aliments/allergies-alimentaires-intolerances-alimentaires/maladie-coeliaque/position-sante-canada-sujet-allegations-sans-gluten.html\">position actuelle de Sant\u00e9 Canada au sujet des all\u00e9gations sans gluten</a> servira \u00e0 orienter les mesures d'application de la loi et les activit\u00e9s de conformit\u00e9 de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Toute concentration de gluten qui d\u00e9coule de la contamination crois\u00e9e et qui est pr\u00e9sente dans un aliment \u00e9tiquet\u00e9 comme \u00e9tant sans gluten doit \u00eatre aussi faible que possible et ne doit pas d\u00e9passer 20\u00a0<abbr title=\"parties par million\">ppm</abbr>, concentration qui n'est pas consid\u00e9r\u00e9e comme dangereuse pour la majorit\u00e9 des personnes souffrant de la maladie coeliaque.</p>\n<p>Les fabricants et les importateurs d'aliments sans gluten devraient d\u00e9ployer tous les efforts possibles pour minimiser la pr\u00e9sence de gluten d\u00e9coulant de la contamination crois\u00e9e dans leurs produits. Dans les cas o\u00f9 le gluten d\u00e9coulant de la contamination crois\u00e9e est pr\u00e9sent selon une concentration inf\u00e9rieure \u00e0 20\u00a0<abbr title=\"parties par million\">ppm</abbr>, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> assure un suivi aupr\u00e8s du fabricant ou de l'importateur concernant la pr\u00e9sence de gluten dans le produit. Ces fabricants et importateurs devraient avoir de bonnes pratiques de fabrication ou d'importation (BPF/BPI) en place en vue d'obtenir des concentrations de gluten aussi faibles que possible et d'\u00e9viter la contamination crois\u00e9e. Toutefois, selon la position de Sant\u00e9 Canada, les mesures d'application de la loi \u00e0 l'\u00e9gard des produits contenant moins de 20\u00a0<abbr title=\"parties par million\">ppm</abbr> de gluten d\u00e9coulant de la contamination crois\u00e9e ne comprennent ni le rappel des produits, ni le retrait de l'all\u00e9gation sans gluten.</p>\n<p>Dans tous les cas, peu importe la source, si une concentration sup\u00e9rieure \u00e0 20\u00a0<abbr title=\"parties par million\">ppm</abbr> de gluten est pr\u00e9sente dans un aliment \u00e9tiquet\u00e9 sans gluten, le produit peut enfreindre l'article B.24.018 du <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr> et/ou l'article\u00a05.1 de la <i>Loi sur les aliments et drogues</i> (LAD) et, \u00e0 la lumi\u00e8re d'une \u00e9valuation des risques pour la sant\u00e9 fournie par Sant\u00e9 Canada, faire l'objet de mesures d'application de la loi de la part de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, notamment la possibilit\u00e9 d'un rappel.</p>\n<p>Toute information figurant sur les \u00e9tiquettes ou dans la publicit\u00e9, y compris les marques de commerce, les marques de fabrique, les photos, les logos ou les slogans qui sugg\u00e8rent ou laissent entendre qu'un aliment est sans gluten, sera \u00e9valu\u00e9e en fonction des crit\u00e8res susmentionn\u00e9s et peut \u00eatre assujettie \u00e0 des mesures d'application de la loi de la part de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> si elle est jug\u00e9e enfreindre la <abbr title=\"Loi sur les aliments et drogues\">LAD</abbr> et/ou le <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>.</p>\n<h2>Ajout intentionnel</h2>\n<p>Comme le d\u00e9crit la position de Sant\u00e9 Canada, il est possible que des aliments contenant du gluten ajout\u00e9 intentionnellement ne portent pas l'all\u00e9gation sans gluten. Cependant, si un fabricant qui utilise un ingr\u00e9dient d\u00e9riv\u00e9 de c\u00e9r\u00e9ales ajoute des \u00e9tapes de transformation suppl\u00e9mentaires en vue de retirer efficacement le gluten, l'aliment peut \u00eatre d\u00e9clar\u00e9 comme \u00e9tant sans gluten.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}