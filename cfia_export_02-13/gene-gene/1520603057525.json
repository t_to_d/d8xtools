{
    "dcr_id": "1520603057525",
    "title": {
        "en": "Export requirements for eggs",
        "fr": "Exigences d'exportation pour les \u0153ufs"
    },
    "html_modified": "2024-02-13 9:52:50 AM",
    "modified": "2018-03-09",
    "issued": "2018-03-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/egg_specific_export_1520603057525_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/egg_specific_export_1520603057525_fra"
    },
    "ia_id": "1520603121536",
    "parent_ia_id": "1503941059640",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food",
        "fr": "Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Egg, shell|Egg, processed products",
        "fr": "Oeufs, en coquille|Oeufs, produits transform\u00e9s"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1503941059640",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Export requirements for eggs",
        "fr": "Exigences d'exportation pour les \u0153ufs"
    },
    "label": {
        "en": "Export requirements for eggs",
        "fr": "Exigences d'exportation pour les \u0153ufs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1520603121536",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1503941030653",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/food-specific-export-requirements/export-requirements-for-eggs/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences-d-exportation-particulieres-aux-produits/exigences-d-exportation-pour-les-%c5%93ufs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Export requirements for eggs",
            "fr": "Exigences d'exportation pour les \u0153ufs"
        },
        "description": {
            "en": "Export requirements for eggs",
            "fr": "Exigences d'exportation pour les \u0153ufs"
        },
        "keywords": {
            "en": "food, food health, Dairy and Egg, Food Specific Requirements, export, Processed Egg, Shell Egg",
            "fr": "Produits Laitiers et \u0153ufs, Exigences particuli\u00e8res aux produits alimentaires, Produit Laitiers, exportation, Oeufs, Oeufs transforme, Oeufs en coquille"
        },
        "dcterms.subject": {
            "en": "food,exports,imports,agri-food industry,food inspection,eggs,agri-food products,dairy products,regulation,food safety,processing",
            "fr": "aliment,exportation,importation,industrie agro-alimentaire,inspection des aliments,oeuf,produit agro-alimentaire,produit laitier,r\u00e9glementation,salubrit\u00e9 des aliments,traitement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-14",
            "fr": "2018-03-14"
        },
        "modified": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Export requirements for eggs",
        "fr": "Exigences d'exportation pour les \u0153ufs"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<nav class=\"mrgn-tp-lg\">\n<h2 class=\"wb-inv\">Table of contents</h2>\n<ul class=\"list-unstyled\">\n<li><a href=\"#a1\">Processed egg</a></li>\n<li><a href=\"#a2\">Shell egg</a></li>\n</ul>\n</nav>\n\n<h2 id=\"a1\">Processed egg</h2>\n\n<p>Exports of processed egg to a foreign country must be packed, marked and certified in accordance with the provisions of Canadian regulations, unless that foreign country discloses to Canada that different requirements exist and must be met for purposes of export. It is the shipper's responsibility to determine the import requirements, if any, of the foreign country. If the shipper wishes to export processed egg that does not meet the requirements of Canadian regulations in respect of standards, packing or marking, they must:</p>\n\n<ol class=\"lst-spcd\">\n<li>demonstrate that the processed egg has been prepared in a registered processed egg station, and,</li>\n<li>provide the inspector with a signed statement:\n<ol class=\"lst-lwr-alph lst-spcd\">\n<li>confirming that the container and markings comply with the requirements of the importing country, and</li>\n<li>setting out the quality specifications of the contract under which the processed egg is being exported.</li>\n</ol></li>\n</ol>\n\n<p>The shipper must also ensure that:</p>\n\n<ol class=\"lst-spcd\">\n<li>the lot number or code of the shipment is marked on the label or container of the processed egg;</li>\n<li>the label affixed to the container does not misrepresent the quality, quantity, composition, characteristics, safety or nutritional value of the processed egg;</li>\n<li>if processed egg is being exported to the United States (U.S.), the product has been prepared under continuous supervision of an inspector; and,</li>\n<li>a certificate of inspection that indicates the requirements are met, signed by an inspector, has been issued in respect of the processed egg, together with a statement of certification.</li>\n</ol>\n\n<h3>Sampling of egg product for export</h3>\n\n<p>All lots must be sampled for Salmonella, aerobic colony count (ACC), coliforms, solids (where applicable), moisture (where applicable), and odour. A station employee may be designated by the Canadian Food Inspection Agency (CFIA) to take the samples. These tests must be performed, either by a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> lab or by a lab accredited by the Standards Council of Canada (SCC). The Certificate of Inspection for Processed Egg CFIA/ACIA\u00a02684 is to indicate all the previously stated lab results except for Listeria.</p>\n\n<p>For exports of pasteurized egg products packed in totes or tankers, the Salmonella test results must be available and be identified as <strong>negative</strong> on the Certificate of Inspection for Processed Egg (CFIA/ACIA\u00a02684) that accompanies the load.</p>\n\n<p>All processed egg products must be sampled for salmonella.</p>\n\n<h3>Shipments of processed egg products</h3>\n\n<h4>Export tankers</h4>\n\n<p>In order to prevent contamination of egg product, tankers that are to be used to transport processed egg must have been previously used to transport food products only. These tankers must be thoroughly washed, rinsed and sanitized. If the tanker has been cleaned in a place other than the processed egg station, the wash certificate must be made available to the inspector. Tankers should be inspected prior to loading product, to ensure that the cleaning process has been effective and that the tankers are visually clean. Prior to loading product, the inspector should also verify that the tanker can be sealed in a manner that prevents any tampering of the product during transport. This check should include seal ports in the tanker lid, as well as the valves at the back of the unit.</p>\n\n<p>Although many tankers are now insulated, the product temperature of liquid egg may rise during transport. The processor should be aware of the maximum product temperature upon arrival at the customer.</p>\n\n<h4>Processed egg exports shipped from third parties</h4>\n\n<p>Shipments of processed egg exported from the company that did not manufacturer the product may occur. The chain of custody requires supporting documentation throughout the process. The inspector of the manufacturing establishment can provide supporting documentation to the certifying inspector to enable certification of the final product.</p>\n\n<p>Supporting documentation includes:</p>\n\n<ol class=\"lst-lwr-alph lst-spcd\">\n<li>The production date(s) and time(s) are recorded on the Inspection report of Shell Eggs/Processed Egg (CFIA/ACIA\u00a05109) subtitled \"Inter/Intra Provincial Movement\".</li>\n\n<li>The product description, date of production, container type, lot code and licensed establishment information are recorded on an Inspection Report of Shell Eggs/Processed Egg (CFIA/ACIA\u00a05109) subtitled \"Export Requirement Attestation\".</li>\n</ol>\n\n<p>Product that has not been subjected to heat treatment and final packaging will be sent to the third party station with documentation (a). Product that has gone through heat treatment and final packaging will be sent to the third party station with documentation (b). The product is subsequently presented for export certification at a either a 3<sup>rd</sup> party licenced processed egg station or a non-licenced 3<sup>rd</sup> party premises (<abbr title=\"that is\">i.e.</abbr> storage) provided it has the required documents.</p>\n\n<h2 id=\"a2\">Shell egg</h2>\n\n<p>The exportation of shell eggs that do not meet the requirements of Canadian regulations in respect of grade, packing or marking is permitted, provided certain conditions are met. This situation is normally due to specific labelling requirements of a foreign country. The inspector must indicate, on the certificate, that the shipment does not meet the requirements of Canadian regulations. In order for an inspector to certify that a shipment of eggs meets the requirements of a foreign country, the company exporting the eggs must provide the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> inspector with a written copy of the foreign country's requirements.</p>\n\n<h3>Notification</h3>\n\n<p>The applicant must contact the relevant <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office by phone, e-mail or in writing at least 48\u00a0hours in advance, to request an inspection. The applicant must provide details including the date, location, volume, grade, and size designation of the eggs to be inspected. An Application for Inspection (Egg or Processed Egg) (CFIA/ACIA\u00a05435) must be completed by the applicant and presented to the inspector prior to the inspection.</p>\n\n<p>If a foreign country requires official notification, this must also be completed. </p>\n\n<h3>Shell eggs \u2013 breaking stock</h3>\n\n<p>Inspection and certification must take place at the licenced egg station.  The inspector completes the Certificate of Inspection/Grading (Eggs and Poultry) (CFIA/ACIA\u00a01022). The following information is to be included on the certificate:</p>\n\n<ol class=\"lst-lwr-rmn lst-spcd\">\n<li><p>Packing date. Transfer the date from the Pallet Tag to the Certificate of Inspection/Grading.</p>\n<p><strong>Note</strong>:  The United States Department of Agriculture (USDA) Food Safety and Inspection Service (FSIS) requires that the eggs be 45\u00b0<abbr title=\"Fahrenheit\">F</abbr> upon receipt, if received after 36\u00a0hours after the date of lay. The packing date from the pallet tag indicates whether the 36\u00a0hours has elapsed or not. If the timeframe has elapsed, the <abbr title=\"Food Safety and Inspection Service\">FSIS</abbr> inspector will be required to monitor the temperature of the load to verify compliance, as per <abbr title=\"Code of Federal Regulations\">CFR</abbr>\u00a0590.950(b).</p>\n</li>\n\n<li>Identification/shipping mark for each container/pallet</li>\n\n<li>Net weight of the shipment</li>\n\n<li>Species of the eggs</li>\n\n<li>Product description (eggs / egg products \u2013 shell eggs \u2013 for breaking)</li>\n\n<li>Name of exporter / consignor (this may also be the applicant)</li>\n\n<li>Transport vehicles are to be sealed with <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> seals and the seal numbers are to be recorded on the Export Certificate.</li>\n</ol>\n\n<h3>Transportation</h3>\n\n<h4>Preparation of product</h4>\n\n<ol class=\"lst-spcd\">\n<li>Eggs should be cooled prior to being shrink-wrapped to reduce the risk of mould development.</li>\n\n<li>Pallets should only be shrink-wrapped immediately prior to shipping or wrapped in a manner which allows ventilation of the product (<abbr title=\"for example\">e.g.</abbr> plastic netting).</li>\n\n<li>Eggs should not be stored on the loading dock prior to shipping.</li>\n</ol>\n\n<h4>Transportation vehicles</h4>\n\n<ol class=\"lst-spcd\">\n<li>Transport vehicles used for transporting eggs must be capable of maintaining the eggs in a refrigerated state. For graded eggs moving to the <abbr title=\"United States\">U.S.</abbr> the required refrigeration temperature is 7.2\u00b0<abbr title=\"Celsius\">C</abbr> (45\u00b0<abbr title=\"Fahrenheit\">F</abbr>).</li>\n\n<li>Transport vehicles must be closed to the outside environment and capable of providing adequate protection from contamination.</li>\n\n<li>Vehicles used for transporting eggs must not have been used to transport any product or substance which might adversely affect the eggs (<abbr title=\"for example\">e.g.</abbr> chemicals, livestock, <abbr title=\"et cetera\">etc.</abbr>).</li>\n<li>Transport vehicles are to be sealed with <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> seals and the seal numbers are to be recorded on the Export Certificate.</li>\n</ol>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<nav class=\"mrgn-tp-lg\">\n<h2 class=\"wb-inv\">Table des mati\u00e8res</h2>\n<ul class=\"list-unstyled\">\n<li><a href=\"#a1\">\u0152ufs transform\u00e9s</a></li>\n<li><a href=\"#a2\">\u0152ufs en coquilles</a></li>\n</ul>\n</nav>\n\n<h2 id=\"a1\">\u0152ufs transform\u00e9s</h2>\n\n<p>Les \u0153ufs transform\u00e9s  export\u00e9s vers un pays \u00e9tranger doivent \u00eatre emball\u00e9s, marqu\u00e9s et certifi\u00e9s  conform\u00e9ment aux dispositions du R\u00e8glement sur les \u0153ufs transform\u00e9s, \u00e0 moins  que le pays \u00e9tranger avise le Canada de l'existence d'autres exigences devant  \u00eatre respect\u00e9es aux fins d'exportation. Si l'exp\u00e9diteur souhaite  exporter des \u0153ufs transform\u00e9s qui ne satisfont pas aux exigences du R\u00e8glement  en ce qui concerne les normes, l'emballage ou le marquage, il doit\u00a0:</p>\n\n<ol class=\"lst-spcd\">\n<li>D\u00e9montrer que le l'ovo-produit  a \u00e9t\u00e9 fabriqu\u00e9 dans un poste d'\u0153ufs transform\u00e9s agr\u00e9\u00e9; et,</li>\n<li>Pr\u00e9senter \u00e0 l'inspecteur  une d\u00e9claration sign\u00e9e\u00a0:\n<ol class=\"lst-lwr-alph lst-spcd\">\n<li>qui atteste que le  contenant et les marques sont conformes aux exigences du pays importateur; et,</li>\n<li>qui pr\u00e9cise les exigences  de qualit\u00e9 stipul\u00e9es dans le contrat aux termes duquel les \u0153ufs transform\u00e9s  sont export\u00e9s;</li>\n</ol></li>\n</ol>\n\n<p>L'exp\u00e9diteur doit aussi  s'assurer que\u00a0:</p>\n\n<ol class=\"lst-spcd\">\n<li>le num\u00e9ro de lot ou le code  de l'envoi est marqu\u00e9 sur l'\u00e9tiquette ou le contenant d'\u0153ufs transform\u00e9s;</li>\n<li>l'\u00e9tiquette appos\u00e9e sur le  contenant ne constitue pas une fausse d\u00e9claration quant \u00e0 la qualit\u00e9, la  quantit\u00e9, la composition, la nature, la suret\u00e9 ou la valeur nutritive des \u0153ufs  transform\u00e9s;</li>\n<li>dans le cas d'une  destination aux \u00c9tats-Unis, les \u0153ufs ont \u00e9t\u00e9 conditionn\u00e9s sous la surveillance  continuelle de l'inspecteur;</li>\n<li>un certificat d'inspection  indiquant que les exigences sont respect\u00e9es, sign\u00e9 par un inspecteur, a \u00e9t\u00e9  d\u00e9livr\u00e9 pour l'\u0153uf transform\u00e9, accompagn\u00e9 d'une d\u00e9claration de certification.</li>\n</ol>\n\n<h3>\u00c9chantillonnage des produits d'\u0153ufs</h3>\n\n<p>Tous  les lots doivent \u00eatre \u00e9chantillonn\u00e9s et faire l'objet des analyses  suivantes\u00a0: tests de d\u00e9pistage de Salmonella,  num\u00e9ration des colonies a\u00e9robies (NCA), num\u00e9ration des coliformes, mati\u00e8res  solides (s'il y a lieu), d\u00e9termination de la teneur en eau (s'il y a lieu) et  odeur. Un employ\u00e9 de l'usine peut \u00eatre d\u00e9sign\u00e9 par l'Agence canadienne d'inspection  des aliments (ACIA) pour effectuer le pr\u00e9l\u00e8vement des \u00e9chantillons. Ces  analyses doivent \u00eatre effectu\u00e9es par un laboratoire de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> ou par un  laboratoire accr\u00e9dit\u00e9 par le Conseil canadien des normes. On doit indiquer sur  le Certificat d'inspection pour les ovo-produits CFIA/ACIA\u00a02684 - r\u00e9serv\u00e9  \u00e0 l'usage interne) tous les r\u00e9sultats des analyses de laboratoire indiqu\u00e9s  pr\u00e9c\u00e9demment, \u00e0 l'exception de ceux concernant Listeria.</p>\n\n<p>Les r\u00e9sultats des analyses pour la Salmonella  doivent \u00eatre disponibles et identifi\u00e9s comme \u00e9tant <strong>n\u00e9gatifs</strong> sur le certificat d'inspection pour les ovo-produits  CFIA/ACIA\u00a02684 - r\u00e9serv\u00e9 \u00e0 l'usage interne) pour tous les ovo-produits en  bac ou en citerne destin\u00e9s \u00e0 l'exportation.</p>\n\n<p>Tous les ovo-produits transform\u00e9s doivent \u00eatre \u00e9chantillonn\u00e9s pour la Salmonella.</p>\n\n<h3>Proc\u00e9dures d'exportation</h3>\n\n<h4>Citernes d'exportation</h4>\n\n<p>Pour  pr\u00e9venir la contamination des produits d'\u0153ufs, les citernes qui serviront au  transport des \u0153ufs transform\u00e9s doivent n'avoir \u00e9t\u00e9 utilis\u00e9es que pour  transporter des produits alimentaires. Elles doivent \u00eatre lav\u00e9es \u00e0 fond,  rinc\u00e9es et d\u00e9sinfect\u00e9es. Si la citerne a \u00e9t\u00e9 nettoy\u00e9e dans un endroit autre que  le poste d'\u0153ufs transform\u00e9s, le certificat de lavage doit \u00eatre mis \u00e0 la  disposition de l'inspecteur. On devrait inspecter les citernes avant d'y  charger le produit afin de s'assurer de l'efficacit\u00e9 du processus de nettoyage  et de la propret\u00e9 apparente des citernes. Avant de charger le produit,  l'inspecteur doit aussi v\u00e9rifier que la citerne peut \u00eatre scell\u00e9e de fa\u00e7on \u00e0  emp\u00eacher la falsification du produit pendant le transport. Cette inspection  doit comprendre la v\u00e9rification de l'\u00e9tanch\u00e9it\u00e9 des orifices du couvercle de la  citerne ainsi que les vannes \u00e0 l'arri\u00e8re de la cuve.</p>\n\n<p>Bien  qu'un bon nombre de citernes soient maintenant isol\u00e9es, la temp\u00e9rature des \u0153ufs  liquides peut augmenter pendant le transport. Le transformateur doit conna\u00eetre  la temp\u00e9rature maximale du produit \u00e0 son arriv\u00e9e chez le client.</p>\n\n<h4>Exportations d'\u0153ufs transform\u00e9s par des tierces  parties</h4>\n\n<p>Il peut arriver qu'une entreprise exporte des  produits d'\u0153ufs transform\u00e9s qu'elle n'a pas fabriqu\u00e9s. La cha\u00eene de possession  exige une documentation \u00e0 l'appui tout au long du processus. L'inspecteur de  l'\u00e9tablissement de fabrication peut fournir la documentation \u00e0 l'appui \u00e0  l'inspecteur responsable de la certification du produit final.</p>\n\n<p>Parmi les documents \u00e0 l'appui,  mentionnons\u00a0:</p>\n\n<ol class=\"lst-lwr-alph lst-spcd\">\n<li>la date et l'heure de production sont consign\u00e9es  dans le Rapport d'inspection des \u0153ufs en coquille/d'\u0153ufs transform\u00e9s  (CFIA/ACIA\u00a05109) \u00e0 la section sur les d\u00e9placements interprovinciaux et  intraprovinciaux.</li>\n\n<li>la description du produit, la date de  production, le type de conteneur, le code de lot et les coordonn\u00e9es de  l'\u00e9tablissement agr\u00e9\u00e9 sont consign\u00e9s dans un Rapport d'inspection des \u0153ufs en  coquille/d'\u0153ufs transform\u00e9s (CFIA/ACIA\u00a05109) \u00e0 la section sur  l'attestation des exigences en mati\u00e8re d'exportation.</li>\n</ol>\n\n<p>Le produit qui n'a pas fait l'objet d'un  traitement thermique et d'un emballage final sera envoy\u00e9 au poste tiers avec la  documentation du point a). Le produit qui a fait l'objet d'un traitement  thermique et d'un emballage final sera envoy\u00e9 au poste tiers avec la  documentation du point b). Le produit sera ensuite pr\u00e9sent\u00e9 pour un certificat  d'exportation soit au poste d'\u0153ufs transform\u00e9s d'une tierce partie agr\u00e9\u00e9e, soit  \u00e0 un \u00e9tablissement tiers non agr\u00e9\u00e9 (<abbr title=\"c'est-\u00e0-dire\">c.-\u00e0-d.</abbr> entreposage), \u00e0 condition d'avoir  les documents requis.</p>\n\n<h2 id=\"a2\">\u0152ufs en coquilles</h2>\n\n<p>Les \u0153ufs en coquille qui ne  r\u00e9pondent pas aux exigences du R\u00e8glement sur les \u0153ufs en ce qui concerne la  cat\u00e9gorie, l'emballage ou le marquage peuvent \u00eatre export\u00e9s, si certaines  conditions sont r\u00e9unies. Cette situation est attribuable, en g\u00e9n\u00e9ral, aux  exigences en mati\u00e8re d'\u00e9tiquetage particuli\u00e8res d'un pays \u00e9tranger. Pour de  plus amples renseignements, veuillez consulter le R\u00e8glement sur les \u0153ufs.  L'inspecteur doit indiquer sur le certificat que l'envoi n'est pas conforme aux  exigences du R\u00e8glement sur les \u0153ufs. Pour qu'un inspecteur certifie qu'un envoi  d'\u0153ufs satisfait aux exigences d'un pays \u00e9tranger, l'entreprise qui exporte les  \u0153ufs doit fournir \u00e0 l'inspecteur de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\u00a0une copie \u00e9crite des exigences  du pays \u00e9tranger.</p>\n\n<h3>Notification</h3>\n\n<p>Le demandeur doit communiquer (par t\u00e9l\u00e9phone,  courriel ou par \u00e9crit) avec le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> au moins 48 heures \u00e0 l'avance  pour demander une inspection. Il doit fournir divers renseignements, notamment  la date, l'endroit, la quantit\u00e9, la cat\u00e9gorie et la d\u00e9signation du calibre des \u0153ufs  \u00e0 inspecter. Le demandeur doit remplir une Demande d'inspection (\u0153ufs ou \u0153ufs  transform\u00e9s) (CFIA/ACIA\u00a05435) et la pr\u00e9senter \u00e0 l'inspecteur avant  l'inspection. Le sp\u00e9cialiste des \u0153ufs du Centre op\u00e9rationnel doit \u00eatre avis\u00e9  par le superviseur ou l'agent r\u00e9gional de programme qu'une inspection pour  exportation a \u00e9t\u00e9 demand\u00e9e et fournir les renseignements sur l'envoi.</p>\n\n<p>Si des \u0153ufs doivent \u00eatre export\u00e9s vers un autre  pays, un avis officiel doit \u00eatre envoy\u00e9 au gouvernement du pays importateur  seulement si le pays importateur en a fait la demande.</p>\n\n<h3>\u0152ufs en coquille destin\u00e9s au cassage</h3>\n\n<p>L'inspection et la certification doivent avoir  lieu au poste d'\u0153ufs agr\u00e9\u00e9. L'inspecteur remplit le Certificat  d'inspection/classement (\u0152ufs et Volaille) (CFIA/ACIA\u00a01022). L'information  suivante doit \u00eatre incluse sur le certificat\u00a0:</p>\n\n<ol class=\"lst-lwr-rmn lst-spcd\">\n<li><p>date d'emballage. Transf\u00e9rer la date de  l'\u00e9tiquette de la palette sur le certificat d'inspection/de classement.</p>\n<p><strong>Remarque</strong>\u00a0:  Le <abbr lang=\"en\" title=\"Food Safety and Inspection Service\">FSIS</abbr> exige que les \u0153ufs soient \u00e0 une temp\u00e9rature  de 45\u00a0\u00b0<abbr title=\"Fahrenheit\">F</abbr> \u00e0 leur r\u00e9ception, s'ils sont re\u00e7us dans les 36\u00a0heures  suivant la date de ponte. La date d'emballage sur l'\u00e9tiquette de la palette  indique si la p\u00e9riode de 36\u00a0heures s'est \u00e9coul\u00e9e ou non. Si le d\u00e9lai s'est  \u00e9coul\u00e9, l'inspecteur du <abbr lang=\"en\" title=\"Food Safety and Inspection Service\">FSIS</abbr> devra surveiller la temp\u00e9rature de la charge pour  en v\u00e9rifier la conformit\u00e9, en vertu de la disposition\u00a0590.950(b) du <abbr lang=\"en\" title=\"Code of Federal Regulations\">CFR</abbr>.</p>\n</li>\n\n<li>marque d'exp\u00e9dition/identification pour chaque  conteneur/palette</li>\n\n<li>poids net de l'envoi</li>\n\n<li>esp\u00e8ce des \u0153ufs</li>\n\n<li>description du produit\u00a0: \u0152ufs/Produits  d'\u0153ufs \u2013 \u0152ufs destin\u00e9s au cassage</li>\n\n<li>nom de l'exportateur/exp\u00e9diteur. \u00c0 noter qu'il  peut aussi s'agir du demandeur.</li>\n\n<li>Les v\u00e9hicules de transport doivent porter un  sceau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, et le num\u00e9ro du sceau doit \u00eatre indiqu\u00e9 sur le certificat  d'exportation.</li>\n</ol>\n\n<h3>Transport</h3>\n\n<h4>Pr\u00e9paration du produit</h4>\n\n<ol class=\"lst-spcd\">\n<li>Les \u0153ufs doivent \u00eatre  refroidis avant que les palettes ne soient emball\u00e9es avec une pellicule  plastique moulante et ce afin de r\u00e9duire le risque de d\u00e9veloppement de  moisissures.</li>\n\n<li>Les palettes doivent \u00eatre  envelopp\u00e9es avec une pellicule plastique moulante imm\u00e9diatement avant  l'exp\u00e9dition ou d'une mani\u00e8re qui permet l'a\u00e9ration du produit (<abbr title=\"par exemple\">p.\u00a0ex.</abbr> filet en  plastique).</li>\n\n<li>Les \u0153ufs ne devraient pas  \u00eatre entrepos\u00e9s sur le quai de chargement avant leur exp\u00e9dition.</li>\n</ol>\n\n<h4>V\u00e9hicules de transport</h4>\n\n<ol class=\"lst-spcd\">\n<li>Les v\u00e9hicules utilis\u00e9s pour  le transport des \u0153ufs doivent maintenir les \u0153ufs \u00e0 l'\u00e9tat r\u00e9frig\u00e9r\u00e9. Dans les  cas des \u0153ufs class\u00e9s transport\u00e9s vers les \u00c9tats-Unis, la temp\u00e9rature requise  est de 7.2\u00a0\u00b0<abbr title=\"Celsius\">C</abbr> (45\u00a0\u00b0<abbr title=\"Fahrenheit\">F</abbr>).</li>\n\n<li>Les v\u00e9hicules de transport  doivent \u00eatre \u00e9tanches \u00e0 l'environnement ext\u00e9rieur et \u00eatre capables de fournir  une protection ad\u00e9quate contre la contamination.</li>\n\n<li>Les v\u00e9hicules utilis\u00e9s pour  le transport des \u0153ufs ne doivent pas avoir \u00e9t\u00e9 utilis\u00e9s pour le transport de  tout produit ou de toute substance qui peut avoir des effets n\u00e9fastes sur les \u0153ufs  (<abbr title=\"par exemple\">p.\u00a0ex.</abbr> produits chimiques, animaux d'\u00e9levage, etc.).</li>\n\n<li>Les v\u00e9hicules de transport  doivent \u00eatre scell\u00e9s au moyen d'un sceau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Les num\u00e9ros de sceau  doivent \u00eatre consign\u00e9s sur le Certificat d'exportation.</li>\n</ol>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}