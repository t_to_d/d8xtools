{
    "dcr_id": "1646066202630",
    "title": {
        "en": "Work as a veterinarian at the CFIA",
        "fr": "Travailler en tant que v\u00e9t\u00e9rinaire \u00e0 l'ACIA"
    },
    "html_modified": "2024-01-26 2:25:48 PM",
    "modified": "2023-05-04",
    "issued": "2022-03-02",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/hrrh_vets_at_the_cfia_1646066202630_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/hrrh_vets_at_the_cfia_1646066202630_fra"
    },
    "ia_id": "1646066203505",
    "parent_ia_id": "1299857657230",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299857657230",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Work as a veterinarian at the CFIA",
        "fr": "Travailler en tant que v\u00e9t\u00e9rinaire \u00e0 l'ACIA"
    },
    "label": {
        "en": "Work as a veterinarian at the CFIA",
        "fr": "Travailler en tant que v\u00e9t\u00e9rinaire \u00e0 l'ACIA"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646066203505",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299857348736",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/job-opportunities/cfia-veterinarians/",
        "fr": "/a-propos-de-l-acia/possibilites-d-emploi/les-veterinaires-de-l-acia/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Work as a veterinarian at the CFIA",
            "fr": "Travailler en tant que v\u00e9t\u00e9rinaire \u00e0 l'ACIA"
        },
        "description": {
            "en": "We\u2019re making a difference in the health and safety of all Canadians. CFIA programs and services impact people\u2019s daily lives. We need you to be a part of this important and exciting work.",
            "fr": "Nous faisons une diff\u00e9rence dans la sant\u00e9 et la s\u00e9curit\u00e9 de tous les Canadiens.  Nos programmes et services ont un impact sur la vie quotidienne des gens.  Nous avons besoin de vous pour faire partie de ce travail important et passionnant."
        },
        "keywords": {
            "en": "Job Opportunities, Veterinarians at the CFIA, hiring, Veterinary career profile, Veterinary Recruitment Team, career opportunity",
            "fr": "Possibilit\u00e9s d'emploi, Les v\u00e9t\u00e9rinaires de l'ACIA, embauchons, Profil de carri\u00e8re des v\u00e9t\u00e9rinaires, recrutement de v\u00e9t\u00e9rinaires, carri\u00e8re passionnante"
        },
        "dcterms.subject": {
            "en": "human resources,inspection,job search,personnel management,staffing",
            "fr": "ressources humaines,inspection,recherche d'emploi,gestion du personnel,dotation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-03-02",
            "fr": "2022-03-02"
        },
        "modified": {
            "en": "2023-05-04",
            "fr": "2023-05-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Work as a veterinarian at the CFIA",
        "fr": "Travailler en tant que v\u00e9t\u00e9rinaire \u00e0 l'ACIA"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA)'s veterinarians play an important role in Canadian food safety and animal health. Their work enhances the health and well-being of Canada's people, environment and economy. They deliver science-based regulatory services to safeguard Canada's food, animals and plants and support continued market access.</p>\n\n<div class=\"row well\">\n<div class=\"col-md-8\">\n<h2 class=\"mrgn-tp-sm\">We're hiring!</h2>\n<p>Our veterinarians are part of the first line of defense against the spread of many diseases - ensuring the health and safety of Canadians.</p>\n<p><a href=\"/eng/1299858033819/1299858089960#f\">Veterinarians</a> joining the CFIA belong to the <a href=\"/eng/1611703182661/1611703358644#s68c8\">Veterinary Medicine (VM) occupational group</a>. We're hiring at the VM-01 level for Meat Hygiene and Animal Health positions in various locations across Canada.</p>\n<p>Are you ready to make a change?</p>\n<p><a href=\"https://cfia-acia.hiringplatform.ca/processes/49726-veterinarian-vm-01?locale=en\"><button type=\"button\" class=\"btn btn-primary\">Apply now</button></a></p>\n</div>\n<div class=\"col-md-4 hidden-sm hidden-xs\">\n<p><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_vets_at_the_cfia_main_featuire_1677179541835_eng.jpg\" alt=\"\" class=\"img-responsive thumbnail\"></p>\n</div>\n</div>\n\n<h2>Why you should join the CFIA as a veterinarian</h2>\n\n<p>If you are looking for work life balance, we offer structured work hours for flexible work-life balance.</p>\n\n<p>The CFIA has great benefits, such as:</p>\n\n<ul>\n<li>1 of the best pension plans in Canada</li>\n<li>disability and life insurance</li>\n<li>excellent health, dental and benefit plans</li>\n</ul>\n\n<p>Paid vacation and overtime for our veterinarians include:</p>\n\n<ul>\n<li>3 weeks of vacation per year upon hiring, and 4 weeks after the first year</li>\n<li>3 weeks of cumulative sick leave per year</li>\n<li>other paid leave, such as 1 week for family-related responsibilities and 2 days for personal reasons</li>\n<li>paid overtime</li>\n</ul>\n\n<p>You will also benefit from:</p>\n\n<ul>\n<li>reimbursement of veterinarian membership fees</li>\n<li>continuing education, development and advancement opportunities</li>\n<li>assistance with relocation expenses, as applicable</li>\n</ul>\n\n<h2>Learn more about our current veterinary staff</h2>\n\n<p>Veterinarians working at the CFIA have the opportunity to become involved in the One Health initiative and work with multidisciplinary partners on a unified approach to global health threats. One Health recognizes that the health of people, animals, plants, and the environment are linked, and common threats should be addressed with a collaborative and integrated approach. Apply now and you can join our veterinary community of practice to advance One Health at the CFIA.</p>\n\n<p>Read the stories of veterinarians who are currently working at the CFIA:</p>\n\n<ul>\n<li><a href=\"/inspect-and-protect/animal-health/cfia-veterinarian/eng/1665083953622/1665084328813\">Dr. Mary Jane Ireland, Chief Veterinary Officer</a></li>\n<li><a href=\"/inspect-and-protect/animal-health/dr-yves-robinson/eng/1648485873101/1648485873804\">Dr. Yves Robinson, Veterinary Pathologist</a></li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/dr-carmencita-lake/eng/1675131840369/1675131841026\">Dr. Carmencita Lake, Veterinarian</a></li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/dr-caitlyn-best/eng/1682085363822/1682085364369\">Dr. Caitlyn Best, Veterinary Program Officer</a></li>\n</ul>\n\n<h2>We celebrate diversity</h2>\n\n<p>When you join the CFIA, you become part of our vision for equity, diversity and inclusion. The CFIA is creating an inclusive workplace free from barriers, racism, harassment, and discrimination, supported by all employees.</p>\n\n<p>We believe that a diverse workforce is about more than visible differences; it's about creating a rich and creative pool of talent that can take the agency \u2013 and its employees \u2013 to new levels of success by blending:</p>\n\n<ul>\n<li>work and communication styles</li>\n<li>educational, cultural and linguistic backgrounds</li>\n<li>lifestyle approaches</li>\n<li>stages of life</li>\n<li>belief systems</li>\n</ul>\n\n<p>The CFIA considers applications from individuals who have legal status to work in Canada and does not give preference to Canadian citizens.</p>\n\n<h2>Contact our veterinary recruitment team</h2>\n\n<p>Our veterinary recruitment team is here to answer questions you may have about this exciting career opportunity and can be reached by email at <a href=\"mailto:cfia.nhqhrstaffing-dotationrh.acia@inspection.gc.ca\">cfia.nhqhrstaffing-dotationrh.acia@inspection.gc.ca</a>.</p>\n\n<section class=\"gc-prtts\">\n<h2>Features</h2>\n<div class=\"row\">\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/animal-health/chief-veterinary-officer/eng/1323802461727/1323802773521\">\n<figure>\n<figcaption>Chief Veterinary Officer for Canada</figcaption>\n<span class=\"visible-lg\"></span>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_vets_at_the_cfia_cvo_feature_1677179542773_eng.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Learn more about the role of the Chief Veterinary Officer of Canada.</p>\n</figure>\n</a>\n</div>\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/inspect-and-protect/animal-health/eng/1565296629434/1565296964476\">\n<figure>\n<figcaption>Inspect and Protect: animal health</figcaption>\n<span class=\"visible-lg\"></span>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/chronicle_360_animal_health_1576180051926_eng.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Explore articles, videos, and podcasts featuring our experts.</p>\n</figure>\n</a>\n</div>\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/inspect-and-protect/science-and-innovation/dr-caitlyn-best/eng/1682085363822/1682085364369\">\n<figure>\n<figcaption>Dr. Caitlyn Best</figcaption>\n<span class=\"visible-lg\"></span>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_vets_at_the_cfia_dr_best_feature_1683130242889_eng.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Discover why I chose to work at the Canadian Food Inspection Agency.</p>\n</figure>\n</a>\n</div>\n</div>\n</section>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les v\u00e9t\u00e9rinaires de l'Agence canadienne des aliments (ACIA) jouent un r\u00f4le important dans la salubrit\u00e9 alimentaire et la sant\u00e9 des animaux au Canada. Leur travail am\u00e9liore la sant\u00e9 et le bien-\u00eatre des Canadiens, ainsi que de l'environnement et de l'\u00e9conomie du Canada. Ils fournissent des services de r\u00e9glementation bas\u00e9s sur la science pour prot\u00e9ger les aliments, les animaux et les v\u00e9g\u00e9taux du Canada, ainsi que pour soutenir l'acc\u00e8s continu au march\u00e9.</p>\n\n<div class=\"row well\">\n<div class=\"col-md-8\">\n<h2 class=\"mrgn-tp-sm\">Nous embauchons!</h2>\n<p>Nos v\u00e9t\u00e9rinaires font partie de la premi\u00e8re ligne de d\u00e9fense contre la propagation de nombreuses maladies \u2013 assurant la sant\u00e9 et la s\u00e9curit\u00e9 des Canadiens.</p>\n<p>Les <a href=\"/fra/1299858033819/1299858089960#f\">v\u00e9t\u00e9rinaires</a>\u00a0qui joignent l'ACIA appartiennent au <a href=\"/fra/1611703182661/1611703358644#s68c8\">groupe professionnel de la M\u00e9decine v\u00e9t\u00e9rinaire (VM)</a>. Nous embauchons des personnes au niveau\u00a0VM-01\u00a0pour remplir des postes li\u00e9s \u00e0 l'hygi\u00e8ne des viandes et la sant\u00e9 des animaux dans de divers endroits dans l'ensemble du Canada.</p>\n<p>\u00cates-vous pr\u00eat \u00e0 faire un changement?</p>\n<p><a href=\"https://cfia-acia.hiringplatform.ca/processes/49726-veterinarian-vm-01?locale=fr_CA\"><button type=\"button\" class=\"btn btn-primary\">Postulez d\u00e8s maintenant</button></a></p>\n</div>\n<div class=\"col-md-4 hidden-sm hidden-xs\">\n<p><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_vets_at_the_cfia_main_featuire_1677179541835_fra.jpg\" alt=\"\" class=\"img-responsive thumbnail\"></p>\n</div>\n</div>\n\n<h2>Pourquoi vous devriez vous joindre \u00e0 l'ACIA en tant que v\u00e9t\u00e9rinaire</h2>\n\n<p>Si vous recherchez un \u00e9quilibre entre vie professionnelle et vie priv\u00e9e, nous offrons des horaires de travail structur\u00e9s pour un \u00e9quilibre flexible entre vie professionnelle et vie priv\u00e9e.</p>\n\n<p>L'ACIA offre des avantages sociaux importants, incluant\u00a0:</p>\n\n<ul>\n<li>\u00a01 des meilleurs r\u00e9gimes de retraite au Canada;</li>\n<li>\u00a0une assurance-invalidit\u00e9 et une assurance-vie; et</li>\n<li>\u00a0d'excellents r\u00e9gimes de soins de sant\u00e9, de soins dentaires et d'avantages sociaux.</li>\n</ul>\n\n<p>Les cong\u00e9s pay\u00e9s et les heures suppl\u00e9mentaires pour nos v\u00e9t\u00e9rinaires comprennent\u00a0:</p>\n\n<ul>\n<li>3 semaines de vacances par an \u00e0 l'embauche et de 4 semaines apr\u00e8s la premi\u00e8re ann\u00e9e;</li>\n<li>3 semaines de cong\u00e9s de maladie cumulatifs par ann\u00e9e;</li>\n<li>d'autres cong\u00e9s pay\u00e9s, tels que 1 semaine pour des responsabilit\u00e9s familiales et 2 jours pour des raisons personnelles; et</li>\n<li>des heures suppl\u00e9mentaires pay\u00e9es.</li>\n</ul>\n\n<p>Vous b\u00e9n\u00e9ficierez \u00e9galement de\u00a0:</p>\n\n<ul>\n<li>le remboursement des frais d'adh\u00e9sion des v\u00e9t\u00e9rinaires;</li>\n<li>les possibilit\u00e9s de formation continue, de perfectionnement et d'avancement professionnels; et</li>\n<li>l'assistance en ce qui concerne les frais de d\u00e9m\u00e9nagement, le cas \u00e9ch\u00e9ant.</li>\n</ul>\n\n<h2>Pour en apprendre plus sur les v\u00e9t\u00e9rinaires actuels de l'ACIA</h2>\n\n<p>Les v\u00e9t\u00e9rinaires travaillant \u00e0 l'ACIA ont la chance de participer \u00e0 l'initiative \u00ab Une seule sant\u00e9 \u00bb et de travailler avec des partenaires multidisciplinaires sur une approche unifi\u00e9e des menaces sanitaires mondiales. \u00ab\u00a0Une seule sant\u00e9\u00a0\u00bb reconna\u00eet que la sant\u00e9 des personnes, des animaux, des v\u00e9g\u00e9taux et de l'environnement sont li\u00e9es, et que les menaces communes doivent \u00eatre adress\u00e9es avec une approche collaborative et int\u00e9gr\u00e9e. Posez votre candidature d\u00e8s maintenant et rejoignez notre communaut\u00e9 de pratique v\u00e9t\u00e9rinaire pour faire progresser l'initiative \u00ab\u00a0Une seule sant\u00e9\u00a0\u00bb \u00e0 l'ACIA.</p>\n\n<p>Lisez les histoires de v\u00e9t\u00e9rinaires qui travaillent actuellement \u00e0 l'ACIA\u00a0:</p>\n\n<ul>\n<li><a href=\"/inspecter-et-proteger/sante-animale/veterinaire-de-l-acia/fra/1665083953622/1665084328813\">Dre Mary Jane Ireland, v\u00e9t\u00e9rinaire en chef</a></li>\n<li><a href=\"/inspecter-et-proteger/sante-animale/dr-yves-robinson/fra/1648485873101/1648485873804\">Dr Yves Robinson, pathologiste v\u00e9t\u00e9rinaire de l'ACIA</a></li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/dre-carmencita-lake/fra/1675131840369/1675131841026\">Dre Carmencita Lake, v\u00e9t\u00e9rinaire de l'ACIA</a></li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/dre-caitlyn-best/fra/1682085363822/1682085364369\">Dre Caitlyn Best, agente de programme v\u00e9t\u00e9rinaire</a></li>\n</ul>\n\n<h2>Nous c\u00e9l\u00e9brons la diversit\u00e9</h2>\n\n<p>Lorsque vous joignez les rangs de l'ACIA, vous prenez part \u00e0 notre vision de l'\u00e9quit\u00e9, de la diversit\u00e9 et de l'inclusion. L'ACIA cr\u00e9e un environnement de travail inclusif sans barri\u00e8res, racisme, harc\u00e8lement et discrimination, soutenu par tous les employ\u00e9s.</p>\n\n<p>Nous croyons qu'un effectif diversifi\u00e9 ne se r\u00e9sume pas aux diff\u00e9rences visibles; il s'agit de cr\u00e9er un bassin de talents riche et cr\u00e9atif qui peut mener l'agence\u00a0- et ses employ\u00e9s\u00a0- \u00e0 de nouveaux niveaux de succ\u00e8s en m\u00e9langeant\u00a0:</p>\n\n<ul>\n<li>les styles de travail et de communication;</li>\n<li>les ant\u00e9c\u00e9dents scolaires, culturels et linguistiques;</li>\n<li>les approches bas\u00e9es sur les modes de vie;</li>\n<li>les \u00e9tapes de la vie; et</li>\n<li>les syst\u00e8mes de croyances.</li>\n</ul>\n\n<p>L'ACIA consid\u00e8re la candidature des individus qui ont un statut l\u00e9gal pour travailler au Canada et ne donne pas de pr\u00e9f\u00e9rence aux citoyens canadiens.</p>\n\n<h2>Communiquez avec notre \u00e9quipe de recrutement de v\u00e9t\u00e9rinaires</h2>\n\n<p>Notre \u00e9quipe de recrutement de v\u00e9t\u00e9rinaires est l\u00e0 pour r\u00e9pondre \u00e0 toute question que vous pourriez avoir sur cette passionnante possibilit\u00e9 de carri\u00e8re. Vous pouvez la joindre par courriel \u00e0 <a href=\"mailto:cfia.nhqhrstaffing-dotationrh.acia@inspection.gc.ca\">cfia.nhqhrstaffing-dotationrh.acia@inspection.gc.ca</a>.</p>\n\n<section class=\"gc-prtts\">\n<h2>En vedette</h2>\n<div class=\"row\">\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/sante-des-animaux/veterinaire-en-chef/fra/1323802461727/1323802773521\">\n<figure>\n<figcaption>V\u00e9t\u00e9rinaire en chef du Canada</figcaption>\n<span class=\"visible-lg\"><br></span>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_vets_at_the_cfia_cvo_feature_1677179542773_fra.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Apprenez-en davantage sur le r\u00f4le de la v\u00e9t\u00e9rinaire en chef du Canada.</p>\n</figure>\n</a>\n</div>\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/inspecter-et-proteger/sante-animale/fra/1565296629434/1565296964476\">\n<figure>\n<figcaption>Inspecter et prot\u00e9ger\u00a0: sant\u00e9 animale</figcaption>\n<span class=\"visible-lg\"></span>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/chronicle_360_animal_health_1576180051926_fra.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>Explorez des articles, vid\u00e9os et balados mettant en vedette nos experts.</p>\n</figure>\n</a>\n</div>\n<div class=\"col-lg-4 col-md-6 mrgn-bttm-md\">\n<a href=\"/inspecter-et-proteger/science-et-innovation/dre-caitlyn-best/fra/1682085363822/1682085364369\">\n<figure>\n<figcaption>Dre Caitlyn Best</figcaption>\n<span class=\"visible-lg\"><br></span>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_vets_at_the_cfia_dr_best_feature_1683130242889_fra.jpg\" alt=\"\" class=\"img-responsive thumbnail mrgn-bttm-sm\">\n<p>D\u00e9couvrez pourquoi j'ai choisi de travailler \u00e0 l'Agence canadienne d'inspection des aliments.</p>\n</figure>\n</a>\n</div>\n</div>\n</section>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}