{
    "dcr_id": "1573850654930",
    "title": {
        "en": "Community for Emerging and Zoonotic Diseases",
        "fr": "Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques"
    },
    "html_modified": "21-11-2019",
    "modified": "6-7-2023",
    "issued": "15-11-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/science_cezd_1573850654930_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/science_cezd_1573850654930_fra"
    },
    "parent_ia_id": "1548468418007",
    "ia_id": "1573850655445",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Community for Emerging and Zoonotic Diseases",
        "fr": "Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques"
    },
    "label": {
        "en": "Community for Emerging and Zoonotic Diseases",
        "fr": "Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1573850655445",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1548468417757",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/science-collaborations/cezd/",
        "fr": "/les-sciences-et-les-recherches/collaborations-scientifiques/cmez/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Community for Emerging and Zoonotic Diseases",
            "fr": "Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques"
        },
        "description": {
            "en": "The Community for Emerging and Zoonotic Diseases (CEZD) is a virtual network that includes representatives from federal, provincial and municipal governments, academia and the private sector who have an expertise in public, animal and environmental health.",
            "fr": "La Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques (CMEZ) est un r\u00e9seau virtuel qui regroupe des repr\u00e9sentants des gouvernements f\u00e9d\u00e9ral, provinciaux et municipaux, des universit\u00e9s et du secteur priv\u00e9 poss\u00e9dant une expertise en sant\u00e9 publique, animale et environnementale."
        },
        "keywords": {
            "en": "Science, food, Community for Emerging and Zoonotic Diseases, About CEZD, Benefit to Canadians, Early warning disease detection, Intelligence generation",
            "fr": "Science, Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques, La CMEZ, Avantages pour les Canadiens, Avertissements pr\u00e9coces de d\u00e9tection d'une maladie"
        },
        "dcterms.subject": {
            "en": "laboratories,sciences,scientific research,veterinary medicine",
            "fr": "laboratoire,sciences,recherche scientifique,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-11-21",
            "fr": "2019-11-21"
        },
        "modified": {
            "en": "2023-07-06",
            "fr": "2023-07-06"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government,scientists",
            "fr": "entreprises,grand public,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Community for Emerging and Zoonotic Diseases",
        "fr": "Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>About CEZD</h2>\n<p>The Community for Emerging and Zoonotic Diseases (CEZD) is a virtual network that includes representatives from federal, provincial and municipal governments, academia and the private sector who have an expertise in public, animal and environmental health. Members come from a variety of professions and industries across Canada and some from the United States, such as scientists, veterinarians, producers, public health professionals and public servants, among others.</p>\n<h2>Benefit to Canadians</h2>\n<p>The Community for Emerging and Zoonotic Diseases (CEZD) benefits Canadians by helping governments, industry and others across Canada identify and plan for potential disease threats that can harm people, <a href=\"/animal-health/eng/1299155513713/1299155693492\">animal  health</a>, the environment and the economy. They gather up-to-date information on emerging and zoonotic diseases and share those early detections and warnings to build knowledge and preparedness, the first lines of defense in helping save lives and prevent the spread of disease.</p>\n<h2>Early warning disease detection</h2>\n<p>As a virtual network, technology is essential. CEZD uses an automated information mining tool, called KIWI, which is short for Knowledge Integration using Web Based Intelligence. KIWI collects and filters information from open and online sources related to emerging and zoonotic diseases. For example, a natural disaster or climate change may change the geographic range of a disease-carrying insect or animal, increasing the risk of disease elsewhere. <br>\n<a href=\"https://cnphi.canada.ca/gts/main\">Canadian Network for Public Health Intelligence</a> (CNPHI) within the <a href=\"https://www.canada.ca/en/public-health/programs/national-microbiology-laboratory.html\">National Microbiology Laboratory</a> of the Public Health Agency of Canada.</p>\n\n<h2>Intelligence generation</h2>\n<p>KIWI searches and filters information of interest, which is then rated daily by experts across the country for its relevance to Canada. The CEZD core team, based within the Canadian Food Inspection Agency (CFIA), generates weekly <a href=\"https://cezd.ca/reports\">intelligence reports</a> from the most relevant information gathered throughout the week.</p>\n<p>The core team and community experts also produce event-specific intelligence reports as needed.</p>\n<h2>Collaboration</h2>\n<p>CEZD uses a secure online platform for ongoing collaboration and communication, thanks to the Canadian Network for Public Health Intelligence. CEZD regularly meets to discuss report findings and other important issues. Experts also discuss and give presentations on diseases of concern, such as avian influenza and African swine fever, and other disease-related issues.</p>\n<h2>Our community mission</h2>\n<p> Multidisciplinary perspectives to generate anticipatory intelligence to provide early warning for emerging and zoonotic diseases.</p>\n<h2>Open information sharing</h2>\n<p>Disease intelligence reports and risk products are publicly available on the <a href=\"https://cezd.ca/\">CEZD</a> website. </p>\n<h2>Contact CEZD</h2>\n<p>Get more information on <a href=\"https://cezd.ca/\">CEZD</a>, emerging diseases and membership.</p>\n<p>You can also contact us with any questions about CEZD: <a href=\"mailto:CEZD@inspection.gc.ca\">CEZD@inspection.gc.ca</a></p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>La CMEZ</h2>\n<p>La Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques (CMEZ) est un r\u00e9seau virtuel qui regroupe des repr\u00e9sentants des gouvernements f\u00e9d\u00e9ral, provinciaux et municipaux, des universit\u00e9s et du secteur priv\u00e9 poss\u00e9dant une expertise en sant\u00e9 publique, animale et environnementale. Les membres viennent de divers milieux professionnels et diverses industries au Canada et aux \u00c9tats-Unis, notamment des chercheurs, des v\u00e9t\u00e9rinaires, des producteurs, des professionnels de la sant\u00e9 publique et des fonctionnaires.</p>\n<h2>Avantages pour les Canadiens</h2>\n<p>La Communaut\u00e9 des maladies \u00e9mergentes et zoonotiques (CMEZ) profite aux Canadiens en aidant les gouvernements, l'industrie et d'autres intervenants partout au Canada \u00e0 d\u00e9tecter et \u00e0 pr\u00e9voir les menaces \u00e9ventuelles de maladies qui peuvent nuire aux gens, \u00e0 <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">la sant\u00e9 animale</a>, \u00e0 l'environnement et \u00e0 l'\u00e9conomie. La CMEZ recueille des donn\u00e9es \u00e0 jour sur les maladies \u00e9mergentes et zoonotiques et transmet de l'information sur les d\u00e9tections pr\u00e9coces et les avertissements afin d'acqu\u00e9rir des connaissances et de mieux se pr\u00e9parer \u2013 la premi\u00e8re ligne de d\u00e9fense pour sauver des vies et emp\u00eacher la propagation de la maladie.</p>\n<h2>Avertissements pr\u00e9coces de d\u00e9tection d'une maladie</h2>\n<p>En tant que r\u00e9seau virtuel, la technologie est essentielle \u00e0 la CMEZ. La CMEZ utilise un outil automatis\u00e9 d'exploration de l'information, appel\u00e9 KIWI, l'abr\u00e9viation de Knowledge Integration using Web Based Intelligence (int\u00e9gration des connaissances \u00e0 l'aide de l'information Web). KIWI recueille et filtre l'information provenant de sources ouvertes et en ligne li\u00e9es aux maladies \u00e9mergentes et zoonotiques. \u00c0 titre d'exemple, une catastrophe naturelle ou un changement climatique pourrait modifier l'aire de r\u00e9partition d'un insecte ou d'un animal porteur d'une maladie, ce qui accro\u00eetrait le risque de maladie ailleurs. <br>\n<a href=\"https://cnphi.canada.ca/gts/main\">R\u00e9seau canadien de renseignements sur la sant\u00e9 publique</a> (RCRSP) au sein du <a href=\"https://www.canada.ca/fr/sante-publique/programmes/laboratoire-national-microbiologie.html\">Laboratoire national de microbiologie</a> de l'Agence de la sant\u00e9 publique du Canada.</p>\n<h2>Production de renseignements</h2>\n<p>KIWI recherche et filtre l'information d'int\u00e9r\u00eat, et la pertinence de cette information pour le Canada est ensuite cot\u00e9e quotidiennement par des experts de l'ensemble du pays. L'\u00e9quipe de base de la CMEZ, \u00e9tablie \u00e0 l'Agence canadienne d'inspection des aliments (ACIA), produit des <a href=\"https://cezd.ca/reports\">rapports de renseignement</a> (en anglais seulement) hebdomadaire \u00e0 partir des donn\u00e9es les plus pertinentes recueillies au cours de la semaine. </p>\n<p>L'\u00e9quipe de base et les experts de la communaut\u00e9 produisent \u00e9galement au besoin des rapports de renseignements sur des \u00e9v\u00e9nements pr\u00e9cis.<strong> </strong></p>\n<h2>Collaboration</h2>\n<p>La CMEZ utilise une plateforme en ligne s\u00e9curis\u00e9e pour la collaboration et la communication continues, gr\u00e2ce au R\u00e9seau canadien de renseignements sur la sant\u00e9 publique. Les membres de la CMEZ se r\u00e9unissent r\u00e9guli\u00e8rement pour discuter des conclusions des rapports et d'autres sujets importants. Les experts discutent \u00e9galement des maladies pr\u00e9occupantes, telles que la grippe aviaire et la peste porcine africaine, et d'autres questions li\u00e9es \u00e0 la maladie, et pr\u00e9sentent des expos\u00e9s \u00e0 leur sujet.</p>\n<h2>Mission de notre communaut\u00e9</h2>\n<p>Favoriser les points de vue multidisciplinaires dans le but de produire des renseignements de mani\u00e8re anticip\u00e9e et de lancer des signaux d'alerte pr\u00e9coces concernant les maladies \u00e9mergentes et les zoonoses.</p>\n<h2>\u00c9change d'information ouverte</h2>\n<p>Les rapports de renseignement sur les maladies et les produits d'analyse sont accessibles au public sur le site Web de la <a href=\"https://cezd.ca/?l=fr-CA\">CMEZ</a>.</p>\n<h2>Communiquez avec la CMEZ</h2>\n<p>Obtenez plus d'informations sur la <a href=\"https://cezd.ca/?l=fr-CA\">CMEZ</a>, les maladies \u00e9mergentes et l'adh\u00e9sion \u00e0 la communaut\u00e9.</p>\n<p>Vous pouvez aussi communiquer avec nous pour toute question concernant la CMEZ en \u00e9crivant \u00e0\u00a0: <a href=\"mailto:CEZD@inspection.gc.ca\">CEZD@inspection.gc.ca</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}