{
    "dcr_id": "1663954521127",
    "title": {
        "en": "Designation order",
        "fr": "Ordonnance de d\u00e9signation"
    },
    "html_modified": "27-9-2022",
    "modified": "28-9-2022",
    "issued": "23-9-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/designation_order_prohibited_rabies_entry_1663954521127_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/designation_order_prohibited_rabies_entry_1663954521127_fra"
    },
    "parent_ia_id": "1356152541083",
    "ia_id": "1663954521456",
    "parent_node_id": "1356152541083",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Designation order",
        "fr": "Ordonnance de d\u00e9signation"
    },
    "label": {
        "en": "Designation order",
        "fr": "Ordonnance de d\u00e9signation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1663954521456",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1356138388304",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/rabies/designation-order/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/rage/ordonnance-de-designation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Designation order",
            "fr": "Ordonnance de d\u00e9signation"
        },
        "description": {
            "en": "Secondary control zone for rabies caused by canine-variant viruses in Canada \u2013 Declaration Schedule",
            "fr": "Zone de contr\u00f4le secondaire en lien avec le variant canin du virus de la rage au Canada \u2013 Annexe de d\u00e9claration"
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, rabies, warm-blooded animals, humans, pets, vaccination",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire,maladie, rage, animaux \u00e0 sang chaud, humains, animaux domestiques, vacciner"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-09-27",
            "fr": "2022-09-27"
        },
        "modified": {
            "en": "2022-09-28",
            "fr": "2022-09-28"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Designation order",
        "fr": "Ordonnance de d\u00e9signation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Whereas:</p>\n\n<p>On June 28, 2022, secondary control zones were declared, under subsection 27.1(2) of the <i>Health of Animals Act</i><sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1a\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup> (the \"act\"), by the delegate of the Minister of Agriculture and Agri-Food (the \"Minister\"), Dr. Siddika Mithani, President of the Canadian Food Inspection Agency, in respect of rabies caused by canine-variant viruses;</p>\n\n<p>As an inspector, I am authorized by section 33 of the act to exercise the power of the Minister under subsection 27.1(3) of the act;</p>\n\n<p>Therefore, I, under subsection 27.1(3) of that act, in respect of those secondary control zones, by this designation order, designate the animal listed in the attached Schedule as being capable of being affected or contaminated by rabies caused by canine-variant viruses.</p>\n\n<p>Dated at Ottawa, September 28, 2022.<br>Dr. Mary Jane Ireland, Inspector</p>\n\n<h2>Schedule</h2>\n\n<p>All dogs (<i lang=\"la\">Canis familiaris</i>), including hybrids of <i lang=\"la\">Canis familiaris</i>, with the exception of personal pet dogs and assistance dogs.</p>\n\n<p>For the purpose of this order:</p>\n\n<ul class=\"lst-none\">\n<li>a \"<strong>personal pet dog</strong>\" is defined as a dog that is intended to live with the owner who is bringing the animal into Canada as a personal pet dog. The said dog is not intended to be transferred or given to another person upon its arrival into Canada and/or is not intended for commercial purposes such as reproduction, breeding or sale of offspring, showing or exhibition, sale of germplasm, sale of the dog itself, scientific use or research or special training status (regardless of whether a profit is made or a transfer of funds occurs).</li>\n<li>an \"<strong>assistance dog</strong>\" is defined as a dog that is trained and certified by an organization accredited by Assistance Dogs International or the International Guide Dog Federation to provide a distinct service to the individual to which the dog belongs. It does not include emotional support animals, therapy animals, comfort animals, companionship animals, or service animals in training.</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\" class=\"wb-inv\">Footnote</h2>\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1a\">\n<p>S.C. 1990, c. 21.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Attendu que\u00a0:</p>\n\n<p>Le 28 juin 2022, des zones de contr\u00f4le secondaire ont \u00e9t\u00e9 d\u00e9clar\u00e9es, en vertu du paragraphe\u00a027.1(2) de la <i>Loi sur la sant\u00e9 des animaux</i><sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup> (la \u00ab\u00a0loi\u00a0\u00bb), par la d\u00e9l\u00e9gu\u00e9e de la ministre de l'Agriculture et de l'Agroalimentaire (la \u00ab\u00a0ministre\u00a0\u00bb), Dre Siddika\u00a0Mithani, pr\u00e9sidente de l'Agence canadienne d'inspection des aliments, en ce qui concerne le variant canin du virus de la rage;</p>\n\n<p>En tant qu'inspecteur, je suis autoris\u00e9(e) en vertu de l'article\u00a033 de la loi \u00e0 exercer le pouvoir de la ministre en vertu du paragraphe\u00a027.1(3) de la loi;</p>\n\n<p>Par cons\u00e9quent, en vertu du paragraphe\u00a027.1(3) de cette Loi, en ce qui concerne ces zones de contr\u00f4le secondaire, je d\u00e9signe, par la pr\u00e9sente ordonnance, l'animal \u00e9num\u00e9r\u00e9 dans l'annexe ci-jointe comme \u00e9tant susceptible d'\u00eatre contamin\u00e9 par le variant canin du virus de la rage.</p>\n\n<p>Le 28 septembre 2022, \u00e0 Ottawa.<br>Dre Mary Jane Ireland, Inspectrice</p>\n\n<h2>Annexe</h2>\n\n<p>Tous les chiens (<i lang=\"la\">Canis familiaris</i>), y compris les hybrides de <i lang=\"la\">Canis familiaris</i>, \u00e0 l'exception des chiens de compagnie et des chiens d'assistance.</p>\n\n<p>Aux fins de la pr\u00e9sente ordonnance\u00a0:</p>\n\n<ul class=\"lst-none\">\n<li>Un <strong>\u00ab\u00a0chien de compagnie\u00a0\u00bb</strong> est d\u00e9fini comme un chien destin\u00e9 \u00e0 vivre avec le propri\u00e9taire qui transporte l'animal au Canada en tant que chien de compagnie. Ledit chien n'est pas destin\u00e9 \u00e0 \u00eatre transf\u00e9r\u00e9 ou donn\u00e9 \u00e0 une autre personne \u00e0 son arriv\u00e9e au Canada et/ou n'est pas destin\u00e9 \u00e0 des fins commerciales, telles que la reproduction, l'\u00e9levage ou la vente de sa prog\u00e9niture, l'exposition, la vente de mat\u00e9riel g\u00e9n\u00e9tique, la vente du chien lui-m\u00eame, l'utilisation scientifique/recherche, ou le statut de formation sp\u00e9ciale (qu'il y ait ou non un profit ou un transfert de fonds).</li>\n<li>Un <strong>\u00ab\u00a0chien d'assistance\u00a0\u00bb</strong> s'entend d'un chien qui est dress\u00e9 et qui a re\u00e7u une certification d\u00e9livr\u00e9e par un organisme accr\u00e9dit\u00e9 par l'Assistance Dogs International ou l'International Guide Dog Federation pour fournir un service distinct \u00e0 la personne qui appartient le chien. Cette d\u00e9finition n'inclut pas les animaux de soutien \u00e9motionnel, de zooth\u00e9rapie, de confort, de compagnie ou les animaux d'assistance en formation.</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\" class=\"wb-inv\">Note de bas de page</h2>\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p>L.C. 1990, ch. 21.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}