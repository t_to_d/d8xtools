{
    "dcr_id": "1468423955222",
    "title": {
        "en": "Guideline for preparing export certificates for the Canadian pet food industry",
        "fr": "Lignes directrices pour la pr\u00e9paration de certificats d'exportation par l'industrie Canadienne des aliments pour animaux de compagnie"
    },
    "html_modified": "2-8-2016",
    "modified": "23-10-2023",
    "issued": "13-7-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/manuals-manuels/data/terr_anima_export_pol_pet_food_guidelines_prep_1468423955222_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/manuals-manuels/data/terr_anima_export_pol_pet_food_guidelines_prep_1468423955222_fra"
    },
    "parent_ia_id": "1321153110863",
    "ia_id": "1468424125724",
    "parent_node_id": "1321153110863",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Guideline for preparing export certificates for the Canadian pet food industry",
        "fr": "Lignes directrices pour la pr\u00e9paration de certificats d'exportation par l'industrie Canadienne des aliments pour animaux de compagnie"
    },
    "label": {
        "en": "Guideline for preparing export certificates for the Canadian pet food industry",
        "fr": "Lignes directrices pour la pr\u00e9paration de certificats d'exportation par l'industrie Canadienne des aliments pour animaux de compagnie"
    },
    "templatetype": "content page 1 column",
    "node_id": "1468424125724",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1321152787689",
    "orig_type_name": "manuals-manuels",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Guideline for preparing export certificates for the Canadian pet food industry",
            "fr": "Lignes directrices pour la pr\u00e9paration de certificats d'exportation par l'industrie Canadienne des aliments pour animaux de compagnie"
        },
        "description": {
            "en": "Preparing export certificates for pet food",
            "fr": "Pr\u00e9parer des certificats d'exportation pour les aliments pour animaux de compagnie"
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, animal health, exports, policy, completed certificate, Pet Food Association of Canada, Additional government, Pet, import conditions",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, sant\u00e9 des animaux, exportation, politique, animaux de compagnie, certificat remplis, Pet Food Association of Canada, gouvernement suppl\u00e9mentaire, animal de compagnie, conditions d'importation"
        },
        "dcterms.subject": {
            "en": "animal health,exports,inspection",
            "fr": "sant\u00e9 animale,exportation,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-08-02",
            "fr": "2016-08-02"
        },
        "modified": {
            "en": "2023-10-23",
            "fr": "2023-10-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Guideline for preparing export certificates for the Canadian pet food industry",
        "fr": "Lignes directrices pour la pr\u00e9paration de certificats d'exportation par l'industrie Canadienne des aliments pour animaux de compagnie"
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=37&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=0\">Complete text</a></p>\r\n\r\n\r\n<h2>On this page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=1\">List of updates</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=2\">Roles and responsibilities of exporters</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=2#s1c2\">Request for certification</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=2#s2c2\">Labelling requirements</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=3\">Role and responsibilities of <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=3#s3c3\">Obligations of certifying veterinarians</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=3#s4c3\">Free sale declaration</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=4\">Supporting documents and safekeeping</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=4#s5c4\">Exporter's declaration</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=4#s6c4\">Traceability Certificate (TC)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=4#s7c4\">Other supporting documents</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=5\">Audits and inspection</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=6\">Glossary of terms</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7\">Listing of requirements from importing countries</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s8c7\">Africa</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s9c7\">North America</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s10c7\">Central and South America</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s11c7\">Asia</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s12c7\">Middle East</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s13c7\">Oceania</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/exports/pet-food/preparing-export-certificates-for-pet-food/eng/1468423955222/1468424125724?chap=7#s14c7\">Europe</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n</ul>\r\n<p><strong>June 17, 2016</strong></p>\n<p>This document contains Canadian Food Inspection Agency (CFIA) guidelines for the preparation by the Canadian pet food industry and the documentation requested for certification of pet food prior to export. The exporters must ensure that they have the required certificate endorsed, and have their importer confirm with the specific importing country that the certificate is completed in an acceptable fashion for the specific consignment.</p>\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=37&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=0\">Texte complet</a></p>\r\n\r\n\r\n<h2>Sur cette page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=1\">Liste des mises \u00e0 jour</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=2\">R\u00f4les et responsabilit\u00e9s des exportateurs</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=2#s1c2\">Demande de certification</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=2#s2c2\">Exigences d'\u00e9tiquetage</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=3\">R\u00f4les et responsabilit\u00e9s de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=3#s3c3\">Obligations des v\u00e9t\u00e9rinaires certificateurs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=3#s4c3\">D\u00e9claration sur la vente libre</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=4\">Documents de supports et conservation</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=4#s5c4\">D\u00e9claration de l'exportateur</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=4#s6c4\">Certificat de tra\u00e7abilit\u00e9 (CT)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=4#s7c4\">Autres documents de support</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=5\">Audits et inspection</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=6\">Glossaire</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7\">\u00c9num\u00e9ration des exigences des pays importateurs</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s8c7\">Afrique</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s9c7\">Am\u00e9rique du Nord</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s10c7\">Am\u00e9rique Centrale et Am\u00e9rique du Sud</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s11c7\">Asie</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s12c7\">Moyen-Orient</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s13c7\">Oc\u00e9anie</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/preparation-de-certificats-d-exportation-pour-les-/fra/1468423955222/1468424125724?chap=7#s14c7\">Europe</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n</ul>\r\n<p><strong>Le 17 juin 2016</strong></p>\n<p>Le pr\u00e9sent document contient les lignes directrices de l'Agence canadienne d'inspection des aliments (ACIA) pour la pr\u00e9paration par l'industrie canadienne des aliments pour animaux de compagnie de la documentation demand\u00e9e pour la certification \u00e0 l'exportation des aliments pour animaux de compagnie. Les exportateurs doivent s'assurer qu'ils ont le certificat requis sign\u00e9 et doivent confirmer avec leur importateur que le certificat est compl\u00e9t\u00e9 de fa\u00e7on acceptable pour le produit en question.</p>\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}