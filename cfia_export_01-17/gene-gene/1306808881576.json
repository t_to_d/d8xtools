{
    "dcr_id": "1306808881576",
    "title": {
        "en": "Brazil\u00a0- Disease freedom recognition",
        "fr": "Br\u00e9sil\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "html_modified": "2011-05-30 22:28",
    "modified": "14-11-2023",
    "issued": "30-5-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_recognition_brazil_1306808881576_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_recognition_brazil_1306808881576_fra"
    },
    "parent_ia_id": "1306649135327",
    "ia_id": "1306809007117",
    "parent_node_id": "1306649135327",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Brazil\u00a0- Disease freedom recognition",
        "fr": "Br\u00e9sil\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "label": {
        "en": "Brazil\u00a0- Disease freedom recognition",
        "fr": "Br\u00e9sil\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "templatetype": "content page 1 column",
    "node_id": "1306809007117",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306648587424",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/status-by-country/brazil/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/statut-par-pays/bresil/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Brazil\u00a0- Disease freedom recognition",
            "fr": "Br\u00e9sil\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
        },
        "description": {
            "en": "Brazil is officially recognized by Canada as free of certain diseases listed in the World Organisation for Animal Health (WOAH) Terrestrial Animal Code.",
            "fr": "Br\u00e9sil est officiellement reconnus par le Canada comme \u00e9tant indemnes de certaines des maladies figurant dans le Code sanitaire pour les animaux terrestres de l'Organisation mondiale de la sant\u00e9 animale (OMSA)."
        },
        "keywords": {
            "en": "animals, disease, freedom, World Organisation for Animal Health, WOAH, Terrestrial Animal Code, Brazil",
            "fr": "animaux, maladies d\u00e9clarables, indemnes, Organisation mondiale de la sant\u00e9 animale, OMSA, Code sanitaire pour les animaux terrestres, Br\u00e9sil"
        },
        "dcterms.subject": {
            "en": "imports,imports,inspection,inspection,veterinary medicine,veterinary medicine,animal health",
            "fr": "importation,importation,inspection,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-30 22:28:03",
            "fr": "2011-05-30 22:28:03"
        },
        "modified": {
            "en": "2023-11-14",
            "fr": "2023-11-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,government",
            "fr": "entreprises,gouvernement,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Brazil\u00a0- Disease freedom recognition",
        "fr": "Br\u00e9sil\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Brazil is officially recognized by Canada as free of certain diseases listed in the World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE)) Terrestrial animal code.</p>\n\n<ul>\n<li>African horse sickness</li>\n\n<li>African swine <span class=\"nowrap\">fever<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote </span>1</a></sup></span></li>\n\n<li>Classical swine <span class=\"nowrap\">fever<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote </span>2</a></sup></span></li>\n\n<li>Contagious bovine pleuropneumonia</li>\n\n<li>Foot-and-mouth <span class=\"nowrap\">disease<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote </span>3</a></sup></span></li>\n\n<li>Highly pathogenic avian <span class=\"nowrap\">influenza<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote </span>4</a></sup></span></li>\n\n<li>Lumpy skin disease</li>\n\n<li>Peste des petits ruminants</li>\n\n<li>Rift valley fever</li>\n\n<li>Sheep pox and goat pox</li>\n\n<li>Swine vesicular <span class=\"nowrap\">disease<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote </span>5</a></sup></span></li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\">\n\n<h2 id=\"fn\">Footnotes</h2>\n\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p>Refer to <a href=\"/animal-health/terrestrial-animals/diseases/status-by-disease/countries-recognized-as-free-from-the-disease/eng/1314998276518/1318975727959\">African Swine Fever</a> (ASF) for areas considered free</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n\n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p>Refer to <a href=\"/animal-health/terrestrial-animals/diseases/status-by-disease/countries-recognized-as-free-of-the-disease/eng/1330140222443/1330140295767\">Classical swine fever</a> (CSF) for areas considered free</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p>Refer to <a href=\"/animal-health/terrestrial-animals/diseases/reportable/foot-and-mouth-disease/countries-recognized-as-free-of-the-disease/eng/1330483635966/1330483942804\">Foot and Mouth Disease</a> (FMD) for areas considered free</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p>Refer to <a href=\"/animal-health/terrestrial-animals/diseases/status-by-disease/countries-recognized-as-free-from-the-disease/eng/1343108465347/1343108628931\">Highly Pathogenic Avian Influenza</a> (HPAI) for areas considered free</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p>Refer to <a href=\"/animal-health/terrestrial-animals/diseases/status-by-disease/countries-recognized-as-free-from-the-disease/eng/1315489335531/1315489443835\">Swine Vesicular Disease</a> (SVD) for areas considered free</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n\n</dl>\n\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Le Br\u00e9sil est officiellement reconnu par le Canada comme \u00e9tant indemne de certaines des maladies figurant dans le Code sanitaire pour les animaux terrestres de l'Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)).</p>\n\n<ul>\n<li>Clavel\u00e9e et variole caprine</li>\n\n<li>Dermatose nodulaire contagieuse</li>\n\n<li>Fi\u00e8vre <span class=\"nowrap\">aphteuse<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page </span>1</a></sup></span></li>\n\n<li>Fi\u00e8vre de la vall\u00e9e du Rift</li>\n\n<li>Influenza aviaire hautement <span class=\"nowrap\">pathog\u00e8ne<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page </span>2</a></sup></span></li>\n\n<li>Maladie v\u00e9siculeuse <span class=\"nowrap\">du porc<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page </span>3</a></sup></span></li>\n\n<li>Peste des petits ruminants</li>\n\n<li>Peste \u00e9quine</li>\n\n<li>Peste porcine <span class=\"nowrap\">africaine<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page </span>4</a></sup></span></li>\n\n<li>Peste porcine <span class=\"nowrap\">classique<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page </span>5</a></sup></span></li>\n\n<li>Pleuropneumonie contagieuse bovine</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\">\n\n<h2 id=\"fn\">Notes de bas de page</h2>\n\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p>Consulter Se reporter \u00e0 la page concernant la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-aphteuse/pays-reconnus-etant-indemnes-de-la-maladie/fra/1330483635966/1330483942804\">fi\u00e8vre aphteuse</a> pour les secteurs consid\u00e9r\u00e9s comme exempts</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p>\n\n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p>Se reporter \u00e0 la page concernant l'<a href=\"/sante-des-animaux/animaux-terrestres/maladies/statut-par-maladie/pays-reconnus-etant-indemnes-de-la-maladie/fra/1343108465347/1343108628931\">influenza aviaire hautement pathog\u00e8ne</a> pour les secteurs consid\u00e9r\u00e9s comme exempts</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p>\n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p>Se reporter \u00e0 la page concernant la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/statut-par-maladie/pays-reconnus-etant-indemnes-de-la-maladie/fra/1315489335531/1315489443835\">maladie v\u00e9siculeuse du porc</a> pour les secteurs consid\u00e9r\u00e9s comme exempts</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p>\n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p>Se reporter \u00e0 la page concernant la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/statut-par-maladie/pays-reconnus-indemnes-de-la-maladie/fra/1314998276518/1318975727959\">peste porcine africaine</a> pour les secteurs consid\u00e9r\u00e9s comme exempts</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p>\n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p>Se reporter \u00e0 la page concernant la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/statut-par-maladie/pays-reconnus-etant-indemnes-de-la-maladie/fra/1330140222443/1330140295767\">peste porcine classique</a> pour les secteurs consid\u00e9r\u00e9s comme exempts</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p>\n</dd>\n</dl>\n\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}