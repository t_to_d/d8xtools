<?php

use Drush\Drush;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;

include "db_operations.php";


global $import_override;
$import_override = TRUE;

global $importedFileNameArray;
global $emptyFileArray;
global $dcrId;
$importedFileNameArray = array();
$emptyFileArray = array();

// echo $_SERVER['argv'][3] . "\n";
// $myargs = array_slice($argv, 3);
//$export_root;
// $export_root = array_shift($myargs);
$export_root = $_SERVER['argv'][3];
echo "EXPORT_ROOT=$export_root \n *****************************************************************\n";

$count = 0;
$fileCount = 0;
$successCount = 0;
$failCount = 0;
$db = new DBOperations();

if ($dirh = opendir($export_root)) {
    while (($entry = readdir($dirh)) !== false) {
        if (! preg_match('/json$/', $entry))
            continue;

        $jfile = $entry;
        //echo $export_root . $jfile . "\n";
        $data = json_decode(file_get_contents($export_root . $jfile));
        global $dcrId;
        $dcrId = (int)$data->dcr_id;


        $nodeId = $db->getNodeIdBydcrId($dcrId);
        $debug = FALSE;
        if ($debug && $dcrId != 1695232303831) {
          continue;
        }
        echo "*************************************\n";
        echo "nodeId: $nodeId\n";
        echo "*************************************\n";
        if (empty($nodeId))
          continue;
        $node = Node::load($nodeId);
        $nodeValueEn = $node->body->value;
        // print_r($data->urlEn);
        foreach ($data->urlEn as $oldurl) {
          //$imageoldURl = "http://intranet.agr.gc.ca/resources/prod/pdf/hrb_dgrh/awards_recognition/inverted_pyramid_mini_text-eng.jpg";
          //$imageNewURL = "/sites/default/files/legacy/resources/prod/pdf/hrb_dgrh/awards_recognition/inverted_pyramid_mini_text-eng.jpg";
          $testurl = '';
          $id = 0;
          $search_type = $node->field_search_type->value;
          if ($search_type == 'inspect') {
            $testurl = testUrlProcessing($oldurl);
            $id = getMediaIdFromFileUri('public://legacy' . $testurl);
          }
          if (is_numeric($id) && $id > 0) {
            // Process images that were previously imported.
            echo "testurl: $testurl \n";
            echo "Media found id: $id\n";
            // Already processed this one, prevent mangling.
            $fullpath = DRUPAL_ROOT . '/sites/default/files/legacy' . $testurl;
            reprocessInspectImageRefs($fullpath, $node);
            continue;
          }
          $newurl = oldUrlProcessing($oldurl);

          echo "newurl: $newurl \n";
          $nodeValueEn = str_replace('"' . $oldurl, '"' . $newurl, $nodeValueEn);
          importContentIntoMediaLib($newurl, $importedFileNameArray, $node);
          // echo $node->body->value . "\n";
        }
        if ($nodeValueEn != $node->body->value) {
          $node->body->value = $nodeValueEn;

          $changed = $node->get('field_modified')->getValue();
          $changed = reset($changed);
          $changed = current($changed);
          $changed = (isset($changed) && !empty($changed)) ? strtotime($changed) : time();
          $node->changed = $changed;
          //$node->setNewRevision(TRUE);
          //$msg = 'update the image path to sites/default/files/legacy for english, asset conversion script';
          //$node->setRevisionLogMessage($msg);
          $node->setSyncing(TRUE);
          $node->save();
          // $node = null;
        }

        // print_r($node->body[]);
        //echo $node->body->value . "\n";
        if ($node->hasTranslation("fr")) {
          $trnode = $node->getTranslation('fr');
          $nodeValueFr = $trnode->body->value;
          if (!empty($trnode)){
            foreach ($data->urlFr as $oldurl) {
              $testurl = '';
              $search_type = $node->field_search_type->value;
              if ($search_type == 'inspect') {
                $testurl = testUrlProcessing($oldurl);
                $id = getMediaIdFromFileUri('public://legacy' . $testurl);
              }
              if (is_numeric($id) && $id > 0) {
                echo "Media found id: $id\n";
                echo "testurl: $testurl \n";
                // Process images that were previously imported.
                $fullpath = DRUPAL_ROOT . '/sites/default/files/legacy' . $testurl;
                reprocessInspectImageRefs($fullpath, $trnode);
                continue;
              }
              $newurl = oldUrlProcessing($oldurl);
              // End of fix url get param for filepath.

              echo "newurl: $newurl \n";
              $nodeValueFr = str_replace('"' . $oldurl, '"' . $newurl, $nodeValueFr);
              importContentIntoMediaLib($newurl, $importedFileNameArray, $trnode);
            }
	    if ($nodeValueFr != $trnode->body->value) {
            $trnode->body->value = $nodeValueFr;
              $trnode->changed = $changed;
              //$trnode->setNewRevision(TRUE);
              //$msg = 'update the image path to sites/default/files/legacy for english, asset conversion script';
              //$trnode->setRevisionLogMessage($msg);
              $trnode->setSyncing(TRUE);
              $trnode->save();
            }
            // echo $trnode->body->value . "\n";
          }
        }
    }
}





function startsWith ($string, $startString)
{
  $len = strlen($startString);
  return (substr($string, 0, $len) === $startString);
}


function importContentIntoMediaLib($url, &$importedFileNameArray, &$node){
  //Drush::output()->writeln("url: " . $url);
  //Drush::output()->writeln("fullpath: " . DRUPAL_ROOT  . $url);
  $fullpath = DRUPAL_ROOT  . $url;
  //$fileArray = global $importedFileNameArray;

  if (!file_exists($fullpath) || filesize($fullpath) === 0){
    global $emptyFileArray;
    global $dcrId;
    $emptyFileArray[$dcrId][] = $fullpath . ' on nid:' . $node->id();
    echo "----file not exist or is an empty file: " . $fullpath . "\n";
    return;
  }

  if (in_array($fullpath, $importedFileNameArray)) {
    // Logic for inspect nodes that do not yet have a media image reference.
    $testurl = testUrlProcessing($url);
    $fullpath = DRUPAL_ROOT . '/sites/default/files/legacy' . $testurl;
    reprocessInspectImageRefs($fullpath, $node);
    echo "----skip existing: " . $fullpath . "\n";
    return;
  }

  $file_data = file_get_contents($fullpath);
  //Drush::output()->writeln("getcwd: " . getcwd());
  //Drush::output()->writeln("replace: " . DRUPAL_ROOT . "/sites/default/files/");
  $directory = str_replace(DRUPAL_ROOT . "/sites/default/files/","",$fullpath); // fullpath

  //Drush::output()->writeln("directory: " . $directory);
  echo 'public://' . $directory;
  if ($file = \Drupal\file\Entity\File::create(['uri' => 'public://' . $directory, 'status' => 1, 'uid' => 1])) {
    $existing_files = \Drupal::entityTypeManager()
      ->getStorage('file')
      ->loadByProperties([
      'uri' => 'public://' . $directory,
    ]);
    if (count($existing_files)) {
      $existing = reset($existing_files);
      $file->fid = $existing->id();
      $file->setOriginalId($existing->id());
      $file->setFilename($existing->getFilename());
      $file->save();
    }
    else {
      $file->save();
    }
  }
  //$file = file_save_data($file_data, 'public://' . $directory, FILE_EXISTS_REPLACE);
  // print_r($directory);
  echo "\ntest\n";
  echo "\nfullpath $fullpath\n";
  echo "\ndirectory $directory\n";
  // print_r($file);

  $path_parts = pathinfo($fullpath);
  $filename = $path_parts['basename'];
  $fileExtension = $path_parts['extension'];
  $bundleType = '';
  switch (strtolower($fileExtension)) {
       case "doc":
       case "docx":
       case "json":
       case "pdf":
       case "ppt":
       case "xls":
       case "xlsm":
       case "xlsx":
          $bundleType = 'document';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            'title'       => $filename,
            'field_document' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
          break;
       case "gif":
       case "jfif":
       case "jpg":
       case "jpeg":
       case "png":
       case "tif":
          $bundleType = 'image';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            /*'alt'       => $filename,*/ // WCAG says no , filename should not be same as title attribute. agrcms/d8#179 gitlab.com
            'image' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
          if (strtolower($fileExtension) == 'jpg' || strtolower($fileExtension) == 'jpeg') {
            $search_type = $node->field_search_type->value;
            if ($search_type == 'inspect') {
              if ($node->get('langcode')->value == 'en') {
                if (!$node->field_image_ref->target_id) {
                  // Only set this if it hasn't already been set.
                  $node->field_image_ref->target_id = $media->id();
                }
              }
              else if ($node->get('langcode')->value == 'fr') {
                if (!$node->field_image_ref_fr->target_id) {
                  // Only set this if it hasn't already been set.
                  $node->field_image_ref_fr->target_id = $media->id();
                }
              }
            }
          }
          break;
       case "mp3":
          $bundleType = 'audio_file';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            /*'title'       => $filename,*/ // WCAG says no , filename should not be same as title attribute. agrcms/d8#179 gitlab.com.
            'field_media_audio_file' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
       case "mp4":
       case "wmv":
          $bundleType = 'video_file';
          $media = Media::create([
            'bundle'           => $bundleType,
            'uid'              => 1,
            /*'title'       => $filename,*/ // WCAG says no , filename should not be same as title attribute. agrcms/d8#179 gitlab.com.
            'field_media_video_file' => [
              'target_id' => $file->id()
            ],
          ]);
          $media->setName($filename)->setPublished(TRUE)->save();
          break;

  }
  // $media = Media::create([
  //   'bundle'           => $bundleType,
  //   'uid'              => 1,
  //   'title'       => $filename,
  //   'field_document' => [
  //     'target_id' => $file->id()
  //   ],
  // ]);
  // $media->setName($filename)->setPublished(TRUE)->save();

  $url = $file->createFileUrl();

  // echo file_url_transform_relative($url) . "\n";
  //
  // echo $media->uuid() . "\n";

  $importedFileNameArray[] = $fullpath;


  return $media;
}


/*

$nodeBodys = $db->getNodeBodyByEntityId(118);
foreach ($nodeBodys as $nodeBody) {
    $page = $nodeBody->body_value;
   // echo "\n count = " . $count++;
    $nodeBody->body_value = updateInternalReference($page);
    //echo $nodeBody->body_value;
    $db->updateNodeBody($nodeBody);

    $count++;
}
function updateInternalReference($page){
  $matches = array();
  $imageoldURl = "https://inspection.canada.ca/resources/prod/pdf/hrb_dgrh/awards_recognition/inverted_pyramid_mini_text-eng.jpg";
  $imageNewURL = "/sites/default/files/legacy/resources/prod/pdf/hrb_dgrh/awards_recognition/inverted_pyramid_mini_text-eng.jpg";

  $page = str_replace($imageoldURl, $imageNewURL, $page);

  $oldPDFURl = "https://inspection.canada.ca/resources/prod/pdf/hrb_dgrh/awards_recognition/inverted_pyramid_text-eng.pdf";
  $newPDFURl = "/sites/default/files/legacy/resources/prod/pdf/hrb_dgrh/awards_recognition/inverted_pyramid_text-eng.pdf";
  $page = str_replace($oldPDFURl, $newPDFURl, $page);
  return $page;
}
*/

function getFileExtensionType($fullpath) {
  $path_parts = pathinfo($fullpath);
  $filename = $path_parts['basename'];
  $fileExtension = $path_parts['extension'];
  $fileExtension = strtolower($fileExtension);
  return $fileExtension;
}

function getMediaIdFromFileUri($uri) {
  //$uri = "public://legacy/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/feed_regulatory_renewal_cmrfp_fig_1535464399824_fra.jpg";
  $files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => $uri]);
  $first_media_id = NULL;
  foreach($files as $fid => $file) {
    $media_entities = \Drupal::entityTypeManager()->getStorage('media')->loadByProperties([
      'image' => $fid,
    ]);
    foreach ($media_entities as $mid => $media_entity) {
      $first_media_id = $mid;
      break;
    }
  }
  return $first_media_id;
}

function reprocessInspectImageRefs($fullpath, &$node) {
  if (foundSocialMediaImages($fullpath)) {
    return;
  }
  $search_type = $node->field_search_type->value;
  if ($search_type == 'inspect') {
    echo "search_type = inspect.\n";
    $file_extension = getFileExtensionType($fullpath);
    if ($file_extension == 'jpg' || $file_extension == 'jpeg') {
      echo "file_extension = $file_extension\n";
      $directory = str_replace(DRUPAL_ROOT . "/sites/default/files/","",$fullpath);
      $uri = 'public://' . $directory;
      echo "uri: $uri\n";
      if ($node->get('langcode')->value == 'en') {
        echo "langcode: " . $node->get('langcode')->value . "\n";
	echo "field_image_ref gettype = " . gettype($node->field_image_ref->target_id) . "\n";
	echo "field_image_ref = " . $node->field_image_ref->target_id . "\n";
        if (!$node->field_image_ref->target_id) {
          echo "Inspect image_ref processing\n";
          echo "node id:" . $node->id() . "\n";
          $media_id = getMediaIdFromFileUri($uri);
          echo "media_id:" . $media_id . "\n";
          $node->field_image_ref->target_id = $media_id;
          $changed = $node->get('field_modified')->getValue();
          $changed = reset($changed);
          $changed = current($changed);
          $changed = (isset($changed) && !empty($changed)) ? strtotime($changed) : time();
          $node->changed = $changed;
          //$node->setNewRevision(TRUE);
          //$msg = 'update the image path to sites/default/files/legacy for english, asset conversion script';
          //$node->setRevisionLogMessage($msg);
          $node->setSyncing(TRUE);
          $node->save();
        }
        else {
          echo "done en nid: " . $node->id() . " ";
        }
      }
      if ($node->get('langcode')->value == 'fr') {
        echo "langcode: " . $node->get('langcode')->value . "\n";
	echo "field_image_ref_fr gettype = " . gettype($node->field_image_ref_fr->target_id) . "\n";
	echo "field_image_ref_fr = " . $node->field_image_ref_fr->target_id . "\n";
        if (!$node->field_image_ref_fr->target_id) {
          echo "Inspect image_ref_fr processing\n";
          echo "node id:" . $node->id() . "\n";
          echo "uri:" . $uri . "\n";
          $media_id = getMediaIdFromFileUri($uri);
          echo "media_id:" . $media_id . "\n";
          $node->field_image_ref_fr->target_id = $media_id;
          $changed = $node->get('field_modified')->getValue();
          $changed = reset($changed);
          $changed = current($changed);
          $changed = (isset($changed) && !empty($changed)) ? strtotime($changed) : time();
          $node->changed = $changed;
          //$node->setNewRevision(TRUE);
          //$msg = 'update the image path to sites/default/files/legacy for english, asset conversion script';
          //$node->setRevisionLogMessage($msg);
          $node->setSyncing(TRUE);
          $node->save();
        }
        else {
          echo "done fr nid: " . $node->id() . " ";
        }
      }
    }
  }
}

function testUrlProcessing($oldurl) {
  $testurl = '';
  if (startsWith($oldurl,"/"))
  {
    $testurl = $oldurl;
  } else if (startsWith($oldurl, 'https://inspection.canada.')) {
    $testurl =        str_replace("https://inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'https://www.inspection.')) {
    $testurl =        str_replace("https://www.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://inspection.canada.')) {
    $testurl =        str_replace("http://inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://www.inspection.')) {
    $testurl =        str_replace("http://www.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://multimedia.')) {
    $testurl =        str_replace("http://multimedia.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'https://multimedia.')) {
    $testurl =        str_replace("https://multimedia.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  }
  // urldecode for filename that somehow gets %20 and other url encoding characters (empty space and others).
  $filename_segment = end(explode('/',$oldurl));
  $testurl = str_replace($filename_segment, urldecode($filename_segment), $testurl);
  // end of urldecode workaround
  // Fix url get param , should not have this on a file path, lets remove it. example: ?param=123&lang=eng&session=12345
  $exploded = explode('?',$testurl);
  if (!empty($exploded)) {
    $get_param_junk = end($exploded);
    if (strlen($get_param_junk) > 1) {
      $testurl = str_replace('?' . $get_param_junk, '', $testurl);
    }
  }
  return $testurl;
}

function oldUrlProcessing($oldurl) {
  $newurl = '';
  if (startsWith($oldurl,"/"))
  {
    $newurl = "/sites/default/files/legacy" . $oldurl;
  } else if (startsWith($oldurl, 'https://inspection.canada.')) {
    $newurl =        str_replace("https://inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'https://www.inspection.')) {
    $newurl =        str_replace("https://www.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://inspection.canada.')) {
    $newurl =        str_replace("http://inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://www.inspection.')) {
    $newurl =        str_replace("http://www.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'http://multimedia.')) {
    $newurl =        str_replace("http://multimedia.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  } else if (startsWith($oldurl, 'https://multimedia.')) {
    $newurl =        str_replace("https://multimedia.inspection.canada.ca/", "/sites/default/files/legacy/", $oldurl);
  }
  // urldecode for filename that somehow gets %20 and other url encoding characters (empty space and others).
  $filename_segment = end(explode('/',$oldurl));
  $newurl = str_replace($filename_segment, urldecode($filename_segment), $newurl);
  // end of urldecode workaround
  // Fix url get param , should not have this on a file path, lets remove it. example: ?param=123&lang=eng&session=12345
  $exploded = explode('?',$newurl);
  if (!empty($exploded)) {
    $get_param_junk = end($exploded);
    if (strlen($get_param_junk) > 1) {
      $newurl = str_replace('?' . $get_param_junk, '', $newurl);
    }
  }
  return $newurl;
}


function foundSocialMediaImages($url) {
  $google = 'google_podcasts';
  $apple = 'protect_apple';
  $spotify = 'protect_spotify';
  $found = FALSE;
  if (strpos($url, $apple) > 1) {
    echo "apple\n";
    $found = TRUE;
  }
  if (strpos($url, $google) > 1) {
    echo "google\n";
    $found = TRUE;
  }
  if (strpos($url, $spotify) > 1) {
    echo "spotify\n";
    $found = TRUE;
  }
  if ($found) {
    echo "Found social media image, skip. \n";
  }
  return $found;
}

$cnt_nodes = 0;
$cnt_files = 0;
echo "**************************************************************************************\n";
echo "* Begin report of empty files, files that were 0 bytes after 403 / 400 or 500 errors *\n";
echo "**************************************************************************************\n";
foreach ($emptyFileArray as $legacy_id => $emptyfiles) {
  $cnt_empty_files_for_node = 0;
  $cnt_nodes++;
  echo "*******************************************************************\n";
  echo "* Empty files report for:                                         *\n";
  echo "* https://inspection.canada.ca/eng/$legacy_id/$legacy_id   *\n";
  echo "*******************************************************************\n";
  foreach($emptyfiles as $emptyfile) {
    $cnt_files++;
    $emptyfile = improveFilePath($emptyfile);
    echo " " . $emptyfile . "\n";
  }
  echo "**************************************************************************\n";
  echo "* Summary of file processing errors for legacy id $legacy_id   *\n";
  echo "* $cnt_empty_files_for_node failed/empty downloads for $legacy_id *\n";
  echo "**************************************************************************\n";
  echo "\n";
}
echo "**************************************************************************************\n";
echo "* Summary of empty files for this migration run                                      *\n";
echo "* $cnt_nodes legacy entities have innacessible files                             *\n";
echo "* $cnt_files total number of files that were innacessible for download or broken *\n";
echo "**************************************************************************************\n";
echo "\n";
echo "**************************************************************************************\n";
echo "* End of empty files report                                                          *\n";
echo "**************************************************************************************\n";


function improveFilePath($path) {
  $position = strpos($path, 'sites/');

  if ($position > 1) {
    $replace_this_part = substr($path, 0, $position);
    $path = str_replace($replace_this_part, 'https://cfia1.11pro.ca/', $path);

  }
  return $path;
}

?>
