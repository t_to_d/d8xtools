{
    "dcr_id": "1537472425013",
    "title": {
        "en": "Production of a field guide on pest and beneficial insects of field crops in Western Canada",
        "fr": "Production d\u2019un guide de champ sur les ravageurs et insectes b\u00e9n\u00e9fiques des cultures commerciales dans l\u2019Ouest canadien"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr13-040_1537472425013_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/prr13-040_1537472425013_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Production of a field guide on pest and beneficial insects of field crops in Western Canada",
        "fr": "Production d\u2019un guide de champ sur les ravageurs et insectes b\u00e9n\u00e9fiques des cultures commerciales dans l\u2019Ouest canadien"
    },
    "breadcrumb": {
        "en": "Production of a field guide on pest and beneficial insects of field crops in Western Canada",
        "fr": "Production d\u2019un guide de champ sur les ravageurs et insectes b\u00e9n\u00e9fiques des cultures commerciales dans l\u2019Ouest canadien"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Production of a field guide on pest and beneficial insects of field crops in Western Canada",
            "fr": "Production d\u2019un guide de champ sur les ravageurs et insectes b\u00e9n\u00e9fiques des cultures commerciales dans l\u2019Ouest canadien"
        },
        "subject": {
            "en": "technology transfer;architectural heritage;soil assessement",
            "fr": "eau ressource;foresterie;c\u00e9r\u00e9ales"
        },
        "dcterms:subject": {
            "en": "statistics;architectural heritage;architecture",
            "fr": "sol;d\u00e9veloppement du Nord;m\u00e9dicament"
        },
        "description": {
            "en": "Alternative Formats. International Standard Serial Number: 2292-1524. Sub-Program 2.1.6: Federal-Provincial-Territorial Cost-shared Environment. Agriculture and Agri-Food Canada provides leadership in the growth and development of a competitive, innovative and sustainable Canadian agriculture and agri-food sector.Responsibilities.",
            "fr": "Le Centre de recherche et de d\u00e9veloppement de Harrow (CRD Harrow) appuie les activit\u00e9s innovatrices de recherche, de d\u00e9veloppement et de transfert des technologies et des connaissances recens\u00e9es dans quatre des neuf. 2018-03-31. J-000247. Reynolds, Dan. 2020-03-31. J-001328. Hao, Xiuming."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "nerve-centre",
            "fr": "implantation au Canada.Proc\u00e9dure denvoi;SNI de ne pas alourdir;Owen Lonsdale"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "sound",
            "fr": "\u00e9v\u00e9nement"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Production of a field guide on pest and beneficial insects of field crops in Western Canada",
        "fr": "Production d\u2019un guide de champ sur les ravageurs et insectes b\u00e9n\u00e9fiques des cultures commerciales dans l\u2019Ouest canadien"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <h2>Project code: PRR13-040</h2>\n<h3>Project Lead </h3>\n<p><b>Erl Svendsen and Owen Olfert</b> -  Agriculture and Agri-Food Canada</p>\n<h3>Objective</h3>\n\n<p><b>To develop an in-field decision support tool for identification of economically harmful and beneficial insects and mites of field crops in the Prairie eco-region</b></p>\n\n<h3>Summary of Results</h3>\n\n<h4>Background</h4>\n\n<p>Accurate identification of pests is essential to make timely and effective pest management decisions. Recognizing associated natural enemies and other beneficial insect species in crops is also important because their presence may reduce pest populations to the point where chemical control is minimized or not required. Thus far, the only comprehensive management tool available to Canadian Prairie field crop and forage producers for pest identification, biology and control information has been the <cite>Insect Pests of the Prairies</cite>, published in 1989. New information generated over the last 25 years about emerging pests and novel pest management technologies, including new biocontrol approaches, needed to be captured and made available for use by growers. </p>\n\n<p>The goal of this project was to meet this need by creating a user-friendly field guide to be used as an up-to-date decision support tool for sustainable pest management in Prairie field crops. The development of the field guide was identified as a priority solution for support within the \u201cReduced-Risk Strategy for Foliar Insect Pests of Field Crops\u201d. </p>\n\n<h4>Approaches</h4>\n\n<p>A team of about 15 subject matter experts was assembled to collaborate on putting together the new field guide. The team consisted of Agriculture and Agri-Food Canada (AAFC), provincial (Alberta, Saskatchewan, Manitoba), university and private entomologists, each of whom had significant experience in research or agricultural extension. Their key roles were to contribute content and illustrations for the guide and to review the drafts. In addition, a number of partner commodity associations and agri-business organisations were engaged to ensure efficient collaboration and knowledge transfer opportunities with key players across various stakeholders groups and sectors. </p>\n\n<p>Hugh Philip, co-author of the previous publication <cite>Insect Pests of the Prairie Provinces</cite> (1989), was contracted to write the text and assemble the associated images. With over 35 years of experience in the field of Integrated Pest Management (IPM), Hugh used many sources of information including extensive personal expertise, existing field guides from other regions, academic scientific publications and consultations with technical experts. </p>\n\n<p>The guide builds on the premise that inclusion of biological and other non-chemical approaches to pest management programs can help minimize losses due to pest damage while reducing the risk from pesticides to pollinators, parasitoids, predators and other important beneficial organisms present in the crops. </p>\n\n<h4>Results</h4>\n\n<p>This project, through extensive collaboration among many experts, gave rise to a new illustrated field guide <a href=\"http://publications.gc.ca/site/eng/9.629939/publication.html\">Field Crop and Forage Pests and their Natural Enemies in Western Canada: Identification and Management Field Guide</a>. The guide is now available in hardcopy, electronically (<abbr title=\"Universal Serial Bus\">USB</abbr> card) and on-line through the Government of Canada Publications.</p>\n\n<p>This 152-page full-colour publication, available in English or French, consolidates up-to-date information about the hosts, identification characteristics, life cycle, crop damage, monitoring and scouting techniques, economic thresholds, and management options for about 90 economically important crop and forage insect and mite pests in Western Canada. It also features similar information about 30 species of natural enemies found in these regions that prey on or parasitize these pests. </p>\n\n<p>Recognizing, conserving and fostering populations of natural enemies can enhance their role in reducing pest populations below economic levels, thus reducing the need for pesticide sprays. </p>\n\n<p>Growers, pest management practitioners, field crop scouts, extension specialists, educators, crop advisors and other agriculture service providers can use this guide as a tool to make informed pest management decisions and support implementation of IPM in field crop and forage production. It is anticipated that the widespread use of this field guide will encourage judicious, science-based decisions about when and where a pesticide spray application is warranted, and enhance the efficacy of IPM programs through supporting area-wide pest monitoring in Western Canada.</p>\n<p>For more information, please contact <a href=\"mailto:Erl.Svendsen@AGR.GC.CA\">Erl Svendsen</a>.</p>\n \n\r\n    ",
        "fr": "\r\n    <h2>Code du projet : PRR13-040</h2>\n<h3>Chef de projet </h3>\n<p><b>Erl Svendsen et Owen Olfert</b> -  Agriculture et Agroalimentaire Canada</p>\n<h3>Objectif</h3>\n\n<p><b>\u00c9laborer un outil d\u2019aide \u00e0 la d\u00e9cision sur le terrain permettant d\u2019identifier les insectes et les acariens des grandes cultures nuisibles et b\u00e9n\u00e9fiques sur le plan \u00e9conomique dans l\u2019\u00e9cor\u00e9gion des Prairies</b></p>\n\n<h3>Sommaire de r\u00e9sultats</h3>\n\n<h4>Contexte</h4>\n\n<p>Il est essentiel de pouvoir proc\u00e9der \u00e0 une identification exacte pour prendre des d\u00e9cisions \u00e9clair\u00e9es en temps opportun en mati\u00e8re de lutte antiparasitaire. Il est \u00e9galement important de savoir reconna\u00eetre les ennemis naturels associ\u00e9s et d\u2019autres esp\u00e8ces d\u2019insectes b\u00e9n\u00e9fiques dans les cultures, car leur pr\u00e9sence peut r\u00e9duire les populations de ravageurs au point o\u00f9 peu de moyens chimiques sont utilis\u00e9s ou n\u00e9cessaires. Jusqu\u2019\u00e0 maintenant, le seul outil complet sur la lutte antiparasitaire dont disposaient les producteurs canadiens de grandes cultures et de cultures fourrag\u00e8res des Prairies canadiennes pour trouver de l\u2019information sur l\u2019identification et la biologie des ravageurs et sur la lutte antiparasitaire \u00e9tait l\u2019ouvrage <cite lang=\"en\">Insect Pests of the Prairies</cite>, publi\u00e9 en 1989. Il fallait donc compiler les nouvelles donn\u00e9es recueillies au cours des 25 derni\u00e8res ann\u00e9es sur les ravageurs \u00e9mergents et les nouvelles technologies de lutte antiparasitaire, y compris les nouvelles m\u00e9thodes de lutte biologique, et les mettre \u00e0 la disposition des producteurs. </p>\n\n<p>Le but du ce projet \u00e9tait de combler ce besoin en cr\u00e9ant un guide d\u2019identification convivial afin d\u2019en faire un nouvel outil d\u2019aide \u00e0 la d\u00e9cision pour la lutte antiparasitaire durable dans les grandes cultures des Prairies. L\u2019\u00e9laboration du guide a \u00e9t\u00e9 identifi\u00e9e comme une solution prioritaire dans la strat\u00e9gie de r\u00e9duction des risques li\u00e9s aux ravageurs des feuilles dans les grandes cultures.</p>\n\n<h4>Approches</h4>\n\n<p>Une \u00e9quipe d\u2019environ 15 experts en la mati\u00e8re a \u00e9t\u00e9 r\u00e9unie pour collaborer \u00e0 la production du nouveau guide. Des entomologistes d\u2019Agriculture et Agroalimentaire Canada (AAC), des provinces (Alberta, Saskatchewan, Manitoba), du milieu universitaire et du secteur priv\u00e9 en font partie et chacun d\u2019entre eux poss\u00e9dait une vaste exp\u00e9rience dans la recherche ou les activit\u00e9s de vulgarisation agricole. Ils devaient principalement contribuer au contenu et aux illustrations du guide et examiner les \u00e9bauches. De plus, un certain nombre de partenaires - associations de producteurs et organisations de l\u2019agroentreprise - ont \u00e9t\u00e9 mis \u00e0 contribution pour assurer une collaboration et un transfert des connaissances efficaces avec les principaux acteurs des diff\u00e9rents secteurs et groupes d\u2019intervenants. </p>\n\n<p>Hugh Philip, coauteur de la publication pr\u00e9c\u00e9dente, <cite lang=\"en\">Insect Pests of the Prairie Provinces</cite> (1989), s\u2019est vu confi\u00e9 la r\u00e9daction du texte et l\u2019assemblage des images connexes. Monsieur Philip compte plus de 35 ann\u00e9es d\u2019exp\u00e9rience dans la lutte int\u00e9gr\u00e9e et a utilis\u00e9 de nombreuses sources d\u2019information pour r\u00e9aliser ce projet. Il s\u2019est notamment servi de sa vaste expertise personnelle ainsi que de guides d\u2019identification provenant d\u2019autres r\u00e9gions, de publications scientifiques universitaires et de consultations avec des experts techniques. </p>\n\n<p>Le guide part du principe qu\u2019en incluant des m\u00e9thodes biologiques et d\u2019autres approches non chimiques dans les programmes de lutte antiparasitaire, il est possible de limiter les pertes caus\u00e9es par les ravageurs tout en r\u00e9duisant les risques des pesticides aux pollinisateurs, parasito\u00efdes, pr\u00e9dateurs et autres organismes b\u00e9n\u00e9fiques importants pr\u00e9sents dans les cultures.</p>\n<h4>R\u00e9sultats</h4>\n<p>Gr\u00e2ce \u00e0 la collaboration de nombreux experts, ce projet a donn\u00e9 naissance \u00e0 un nouveau guide d\u2019identification <a href=\"http://publications.gc.ca/site/fra/9.677532/publication.html\">Guide d\u2019identification des ravageurs des grandes cultures et des cultures fourrag\u00e8res et de leurs ennemis naturels, et mesures de lutte applicables \u00e0 l\u2019Ouest canadien</a>. Le guide est maintenant offert en version imprim\u00e9e, en version \u00e9lectronique (carte <abbr title=\"Universal Serial Bus\">USB</abbr>) et en ligne sur le Publications du gouvernement du Canada. </p>\n<p>Ce guide d\u2019identification illustr\u00e9 de 152 pages, offert en anglais et en fran\u00e7ais, regroupe les renseignements les plus r\u00e9cents sur les h\u00f4tes, les caract\u00e9ristiques d\u2019identification, le cycle de vie, les dommages ils causent, les techniques de surveillance et de d\u00e9pistage, les seuils d\u2019intervention et les options de lutte visant plus de 90 ravageurs (insectes et acariens) d\u2019importance \u00e9conomique des grandes cultures et des cultures fourrag\u00e8res dans l\u2019Ouest du Canada. Il fournit \u00e9galement des renseignements similaires sur quelque 30 esp\u00e8ces de pr\u00e9dateurs et parasito\u00efdes naturels de ces ravageurs dans cette r\u00e9gion. </p>\n\n<p>Savoir reconna\u00eetre et pr\u00e9server ces ennemis naturels et favoriser le d\u00e9veloppement de leurs populations permet de mieux les utiliser pour r\u00e9duire les populations de ravageurs qui nuisent \u00e0 la rentabilit\u00e9 tout en minimisant la n\u00e9cessit\u00e9 de recourir aux pesticides. </p>\n<p>Les producteurs, les sp\u00e9cialistes de la lutte antiparasitaire, les observateurs sur le terrain, les sp\u00e9cialistes de la vulgarisation, les enseignants, les conseillers agricoles et d\u2019autres fournisseurs de services agricoles peuvent utiliser ce guide pour prendre des d\u00e9cisions \u00e9clair\u00e9es sur la lutte antiparasitaire et favoriser la lutte int\u00e9gr\u00e9e dans les grandes cultures et les cultures fourrag\u00e8res. L\u2019utilisation de ce guide d\u2019identification \u00e0 grande \u00e9chelle devrait faciliter la prise de d\u00e9cisions judicieuses fond\u00e9es sur des crit\u00e8res scientifiques pour savoir quand et o\u00f9 l\u2019application des pesticides est n\u00e9cessaire, et elle devrait am\u00e9liorer l\u2019efficacit\u00e9 des programmes de lutte int\u00e9gr\u00e9e en favorisant la surveillance des ravageurs dans toute la r\u00e9gion de l\u2019Ouest du Canada.</p>\n\n<p>Pour plus de renseignements, veuillez communiquer avec <a href=\"mailto:Erl.Svendsen@AGR.GC.CA\">Erl Svendsen</a>.</p>\n\r\n    "
    }
}