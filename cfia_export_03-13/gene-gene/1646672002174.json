{
    "dcr_id": "1646672002174",
    "title": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Eligibility requirements",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "html_modified": "2024-03-12 3:59:13 PM",
    "modified": "2023-10-26",
    "issued": "2022-08-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/research_auth_fert_act_new_regs-eligibility_requirements_1646672002174_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/research_auth_fert_act_new_regs-eligibility_requirements_1646672002174_fra"
    },
    "ia_id": "1646672002486",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Eligibility requirements",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "label": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Eligibility requirements",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646672002486",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/eligibility-requirements/",
        "fr": "/protection-des-vegetaux/engrais/conditions-d-eligibilite/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Research authorizations under the Fertilizers Act and regulations: Eligibility requirements",
            "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Conditions d'\u00e9ligibilit\u00e9"
        },
        "description": {
            "en": "A fertilizer that is imported into or manufactured in Canada for experimental purposes must be safe. But, a fertilizer is exempt from registration and labelling requirements as long as: the product does not contain a supplement or a pesticide, and all treated plant material and all residual product are destroyed at the end of the trial so that they don't enter the commercial food or feed chains.",
            "fr": "Un engrais import\u00e9 ou fabriqu\u00e9 au Canada \u00e0 des fins exp\u00e9rimentales doit \u00eatre s\u00fbr (article\u00a02.1 du R\u00e8glement), mais peut \u00eatre exempt\u00e9 des exigences li\u00e9es \u00e0 l'enregistrement et \u00e0 l'\u00e9tiquetage si\u00a0: Le produit ne contient pas un suppl\u00e9ment ou un pesticide; et tout le mat\u00e9riel v\u00e9g\u00e9tal trait\u00e9 et les produits r\u00e9siduels sont d\u00e9truits \u00e0 la fin de l'essai afin d'\u00e9viter qu'ils n'entrent pas dans les cha\u00eenes commerciales d'alimentation humaine ou animale."
        },
        "keywords": {
            "en": "Research, authorizations, New, Fertilizers Act, Regulations, Overview",
            "fr": "recherche, autorisations, nouveau, Loi sur les engrais, r\u00e8glement d'application, Aper\u00e7u"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy",
            "fr": "cultures,engrais,inspection,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "modified": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Eligibility requirements",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/overview/eng/1646671738613/1646671738988\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/before-you-apply/eng/1646672025348/1646672025708\">3. Before you apply</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/how-to-apply/eng/1646672046841/1646672047216\">4. How to apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/after-you-apply/eng/1646672201606/1646672201950\">5. After you apply</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646672246507/1646672246851\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<h2>Conducting research on fertilizers</h2>\n\n<p>A fertilizer that is imported into or manufactured in Canada for experimental purposes must be safe (<a href=\"https://laws.justice.gc.ca/eng/regulations/C.R.C.,_c._666/page-1.html#h-559882\">section\u00a02.1 of the regulations</a>). But, a fertilizer is exempt from registration and labelling requirements as long as:</p>\n\n<ul>\n<li>the product does not contain a supplement or a pesticide</li>\n<li>all treated plant material and all residual product are destroyed at the end of the trial so that they don't enter the commercial food or feed chains</li>\n</ul>\n\n<p>Research trials conducted on fertilizers that are currently registered but for a different use pattern (for example ornamental plants vs. agricultural food crops) still require that all treated plant material be disposed of at the end of the trial to prevent their entry into the commercial food or feed chains.</p>\n\n<p>Requirements for conducting research on fertilizers that <strong>contain supplements</strong> are the same as for <a href=\"#a1\">novel supplements</a>.</p>\n\n<h2>Conducting research on supplements</h2>\n\n<p>Researchers don't need an authorization for research trials on supplements that are either:</p>\n\n<ul>\n<li>exempt from registration</li>\n<li>already registered under the <i>Fertilizers Act</i> when conducted in accordance with the approved directions for use</li>\n</ul>\n\n<p>But, researchers will need a research authorization before the trial starts if the use pattern or directions for use of the product under test don't correspond to those approved. These types of authorizations are equivalent to testing <a href=\"#a1\">novel supplements</a>.</p>\n\n<h2 id=\"a1\">Research authorizations for novel supplements</h2>\n\n<p>Research authorizations are designed to make sure the environmental release of the <a href=\"#centred-popup\" aria-controls=\"centred-popup\" class=\"wb-lbx\" role=\"button\">novel supplement</a> is safe.</p>\n\n<section id=\"centred-popup\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h2 class=\"modal-title\">Definitions</h2>\n\n</header>\n<div class=\"modal-body\">\n<p><strong>Novel supplement</strong> means:</p>\n<ul>\n<li>a supplement that is not registered and not exempt from registration, or</li>\n<li>a supplement that is derived through biotechnology and has a novel trait</li>\n</ul>\n\n<p>A <strong>novel trait</strong>, means a characteristic of the supplement that:</p>\n<ul class=\"mrgn-bttm-sm\">\n<li>has been intentionally selected, created or introduced into a distinct, stable population of the same species through a specific genetic change, and</li>\n<li>based on valid scientific rationale, is not substantially equivalent, in terms of its specific use and safety both for the environment and for human health, to any characteristic of a similar supplement that is in use as a supplement in Canada and is considered safe for use as a supplement in Canada</li>\n</ul>\n\n</div>\n</section>\n\n<p>Research authorizations prescribe:</p>\n\n<ul>\n<li>confinement conditions</li>\n<li>protective equipment</li>\n<li>crop disposal methods</li>\n</ul>\n\n<h2>Importation for research purposes</h2>\n\n<p>The quantity of the product imported must not exceed the total amount required to conduct the testing.</p>\n\n<p>In some cases, an applicant may also need an import permit from\u00a01 or more of the following Canadian Food Inspection Agency (CFIA) offices:</p>\n\n<ul>\n<li>Animal Health Import/Export Office</li>\n<li>Plant Health Import/Export office</li>\n</ul>\n\n<p>Refer to the CFIA's <a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">Automated Import Reference System (AIRS)</a> to find out if any further importation requirements apply to the product.</p>\n\n<p><strong>Note:</strong> A proponent that imports fertilizers and novel supplements under the research exemption provisions can't:</p>\n\n<ul>\n<li>use or distribute the product(s) outside the confines of the research trial</li>\n<li>offer the product(s) for sale</li>\n</ul>\n\n<p>Doing so contravenes the <i>Fertilizers Act</i> and regulations and the proponent may be subject to enforcement action.</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Additional information</h2>\n\n</header>\n<div class=\"panel-body\">\n\n<p>For further guidance on safe crop disposal methods, contact the <a href=\"/about-the-cfia/permits-licences-and-approvals/paso/eng/1403831598660/1403831599519\">Pre-market Application Submissions Office</a> (PASO).</p>\n\n<p>For more information about crop destruct waivers (CDW), see <a href=\"/eng/1307855399697/1320243929540#a6\">crop destruct waiver</a>.</p>\n\n</div>\n</section>\n\n</div>\n\n\n<h2>Criteria for research trials</h2>\n\n<p>All research trials conducted on novel supplements in Canada must meet the following general criteria:</p>\n\n\n<ul>\n<li>a qualified researcher must conduct trails using scientifically valid methods\n<ul>\n<li>the researcher(s) responsible for the field trials must either have: \n<ul>\n<li>successfully completed a Bachelor's or higher degree in agriculture or a related scientific field</li>\n<li>previous experience in carrying out scientifically sound research trials on agricultural products</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>researchers must follow good laboratory practices (GLP) and appropriate quality control procedures\n<ul>\n<li>the company or research institute doesn't necessarily need to be GLP certified</li>\n</ul>\n</li>\n<li>the research institution must ensure the safety of its employees throughout the duration of the trial</li>\n<li>researchers must conduct the research in areas where:\n<ul>\n<li>spread of the product being tested outside the test site isn't likely to occur (for example, via run-off, erosion, drift, leaching, etc.)</li>\n<li>bystander exposure to the supplement or to treated plants or soil is limited</li>\n</ul>\n</li>\n<li><strong>unless</strong> the CFIA grants the proponent a crop destruct waiver:\n<ul>\n<li>product proponents <strong>must not sell or distribute</strong> treated plants, growing media and unused product</li>\n<li>researchers must safely dispose of all supplement, fertilizer and plant material, including harvested crops, from the treated sites once testing is complete</li>\n<li>applicants must propose a method of safe disposal \n<ul>\n<li>methods may vary based on the: \n<ul>\n<li>nature of the supplement (microbial versus chemical)</li>\n<li>probability of its spread and establishment in the environment</li>\n<li>relative risk associated with the release</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n\n<h2>Contained research exemptions and research waivers (RW)</h2>\n\n<p>Research trials that preclude the release of the supplement into the environment, such as testing in contained facilities (laboratories, for example), don't need a research authorization. But, these trials must have appropriate measures in place to prevent release into the environment.</p>\n\n<p>Applicants wishing to get an exemption from the research authorization for contained research trials must apply for a research waiver (RW). For more information about the requirements for a RW, see <a href=\"/eng/1307855399697/1320243929540#a5\">contained research exemptions and research waivers</a> (RW).</p>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/plant-health/fertilizers/overview/eng/1646671738613/1646671738988\" rel=\"prev\">Previous</a></li>\n\n<li class=\"next\"><a href=\"/plant-health/fertilizers/before-you-apply/eng/1646672025348/1646672025708\" rel=\"next\">Next</a></li>\n\n</ul>\n</nav>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apercu/fra/1646671738613/1646671738988\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646672025348/1646672025708\">3. Avant de pr\u00e9senter une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/comment-presenter-une-demande/fra/1646672046841/1646672047216\">4. Comment pr\u00e9senter une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apres-avoir-presente-une-demande/fra/1646672201606/1646672201950\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646672246507/1646672246851\">6. Modifier et renouveler</a></li>\n\n</ul>\n</div>\n</div>\n\n<h2>Effectuer des recherches sur les engrais</h2>\n\n<p>Un engrais import\u00e9 ou fabriqu\u00e9 au Canada \u00e0 des fins exp\u00e9rimentales doit \u00eatre s\u00fbr (<a href=\"https://laws.justice.gc.ca/fra/reglements/C.R.C.%2C_ch._666/page-1.html#h-548535\">article\u00a02.1 du R\u00e8glement</a>), mais peut \u00eatre exempt\u00e9s des exigences li\u00e9es \u00e0 l'enregistrement et \u00e0 l'\u00e9tiquetage si\u00a0:</p>\n\n<ul>\n<li>Le produit ne contient pas un suppl\u00e9ment ou un pesticide;</li>\n<li>Tout le mat\u00e9riel v\u00e9g\u00e9tal trait\u00e9 et les produits r\u00e9siduels sont d\u00e9truits \u00e0 la fin de l'essai afin d'\u00e9viter qu'ils n'entrent pas dans les cha\u00eenes commerciales d'alimentation humaine ou animale.</li>\n</ul>\n\n<p>Dans le cadre des essais \u00e0 des fins de recherche men\u00e9s sur des engrais qui sont actuellement enregistr\u00e9s pour un autre usage (par exemple, plantes ornementales par rapport aux cultures vivri\u00e8res agricoles), tout le mat\u00e9riel v\u00e9g\u00e9tal trait\u00e9 doit \u00e9galement \u00eatre \u00e9limin\u00e9 \u00e0 la fin de l'essai afin d'emp\u00eacher leur entr\u00e9e dans les cha\u00eenes commerciales d'alimentation humaine ou animale.</p>\n\n<p>Les exigences concernant la recherche sur les engrais qui <strong>contiennent des suppl\u00e9ments</strong> sont les m\u00eames que pour les <a href=\"#a1\">suppl\u00e9ments nouveaux</a>.</p>\n\n<h2>Effectuer des recherches sur les suppl\u00e9ments</h2>\n\n<p>Les chercheurs n'ont pas besoin d'une autorisation pour les essais de recherche sur les suppl\u00e9ments qui sont\u00a0:</p>\n\n<ul>\n<li>exempt\u00e9s de l'enregistrement</li>\n<li>d\u00e9j\u00e0 enregistr\u00e9s en vertu de la <i>Loi sur les engrais</i>, lorsqu'ils sont men\u00e9s conform\u00e9ment au mode d'emploi approuv\u00e9</li>\n</ul>\n\n<p>Cependant, les chercheurs auront besoin d'une autorisation de recherche avant le d\u00e9but de l'essai si le mod\u00e8le d'utilisation ou le mode d'emploi du produit test\u00e9 ne correspondent pas \u00e0 ceux approuv\u00e9s. Ces types d'autorisations sont \u00e9quivalents aux essais avec des <a href=\"#a1\">suppl\u00e9ments nouveaux</a>.</p>\n\n<h2 id=\"a1\">Autorisations de recherche pour des suppl\u00e9ments nouveaux</h2>\n\n<p>Les autorisations de recherche visent \u00e0 assurer la diss\u00e9mination s\u00e9curitaire du <a href=\"#cs-centre\" aria-controls=\"cs-centre\" class=\"wb-lbx\" role=\"button\">suppl\u00e9ment nouveau</a>.</p>\n\n<section id=\"cs-centre\" class=\"mfp-hide modal-dialog modal-content overlay-def\">\n<header class=\"modal-header\">\n<h2 class=\"modal-title\">D\u00e9finitions</h2>\n\n</header>\n<div class=\"modal-body\">\n\n<p><strong>Un suppl\u00e9ment nouveau</strong> signifie, selon le cas\u00a0:</p>\n\n<ul>\n<li>d'un suppl\u00e9ment non enregistr\u00e9 et non exempt\u00e9 de l'enregistrement;</li>\n<li>d'un suppl\u00e9ment issu de la biotechnologie et dot\u00e9 d'un caract\u00e8re nouveau.</li>\n</ul>\n\n<p><strong>Un caract\u00e8re nouveau</strong> signifie un caract\u00e8re d'un suppl\u00e9ment issu de la biotechnologie qui</p>\n\n<ul>\n<li>d'une part, a \u00e9t\u00e9 intentionnellement s\u00e9lectionn\u00e9, cr\u00e9\u00e9 ou incorpor\u00e9 dans une population distincte et stable de suppl\u00e9ments de la m\u00eame esp\u00e8ce par une modification g\u00e9n\u00e9tique particuli\u00e8re;</li>\n<li>d'autre part, est bas\u00e9 sur un raisonnement scientifique valide, n'est pas substantiellement \u00e9quivalent, du point de vue de son utilisation sp\u00e9cifique et de sa s\u00e9curit\u00e9 tant pour l'environnement que pour la sant\u00e9 humaine, \u00e0 toute caract\u00e9ristique d'un suppl\u00e9ment similaire qui est utilis\u00e9 comme suppl\u00e9ment au Canada et qui est consid\u00e9r\u00e9 comme s\u00fbr pour l'utilisation.</li>\n</ul>\n\n</div>\n</section>\n\n<p>Les autorisations de recherche prescrivent\u00a0:</p>\n\n<ul>\n<li>des conditions de confinement</li>\n<li>l'\u00e9quipement de protection</li>\n<li>les m\u00e9thodes d'\u00e9limination des r\u00e9coltes</li>\n</ul>\n\n<h2>Importation pour les besoins de la recherche</h2>\n\n<p>La quantit\u00e9 du produit import\u00e9 ne doit pas d\u00e9passer la quantit\u00e9 totale n\u00e9cessaire pour effectuer l'essai.</p>\n\n<p>Dans certains cas, un demandeur peut \u00e9galement avoir besoin d'un permis d'importation d\u00e9livr\u00e9 par un ou plusieurs des bureaux de l'Agence canadienne d'inspection des aliments (ACIA) suivants\u00a0:</p>\n\n<ul>\n<li>la Section des Exportations et des Importations de la Division de la sant\u00e9 des animaux</li>\n<li>la Section des Exportations et des Importations de la Division de la protection des v\u00e9g\u00e9taux</li>\n</ul>\n\n<p>Veuillez consulter le <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a> de l'ACIA pour d\u00e9terminer s'il y a d'autres exigences relatives \u00e0 l'importation.</p>\n\n<p><strong>Remarque\u00a0:</strong> Un chercheur qui importe les engrais et les suppl\u00e9ments nouveaux en vertu des exemptions pr\u00e9vues pour la recherche ne doit pas\u00a0:</p>\n\n<ul>\n<li>utiliser ou distribuer les produits en dehors des limites de l'essai de recherche</li>\n<li>mettre les produits en vente.</li>\n</ul>\n\n<p>Le non-respect de ces dispositions constitue une violation de la <i>Loi sur les engrais</i> et son r\u00e8glement d'application et peut entra\u00eener des mesures d'application de la loi.</p>\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Informations suppl\u00e9mentaires</h2>\n\n</header>\n<div class=\"panel-body\">\n\n<p>Pour plus de conseils sur les m\u00e9thodes s\u00e9curitaires d'\u00e9limination de la r\u00e9colte, veuillez communiquer avec le <a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/bpdpm/fra/1403831598660/1403831599519\">Bureau de pr\u00e9sentation de demandes pr\u00e9alables \u00e0 la mise en march\u00e9</a> (BPDPM).</p>\n\n<p>Pour en savoir plus sur les exemptions de l'obligation de d\u00e9truire la r\u00e9colte veuillez vous r\u00e9f\u00e9rer aux <a href=\"/fra/1307855399697/1320243929540#a6\">exemptions de l'obligation de d\u00e9truire la r\u00e9colte</a>.</p>\n\n</div>\n</section>\n\n</div>\n\n<h2>Crit\u00e8res pour les essais \u00e0 des fins de recherche</h2>\n\n<p>Tous les essais effectu\u00e9s sur des suppl\u00e9ments nouveaux au Canada doivent satisfaire aux crit\u00e8res g\u00e9n\u00e9raux suivants\u00a0:</p>\n\n\n<ul>\n<li>Les essais doivent \u00eatre effectu\u00e9s par un chercheur qualifi\u00e9, \u00e0 l'aide de m\u00e9thodes scientifiques valides\n<ul>\n<li> Le ou les chercheurs responsables des essais sur le terrain doivent au moins avoir\u00a0: \n<ul>\n<li>un baccalaur\u00e9at en agriculture ou dans un domaine scientifique connexe ou</li>\n<li>poss\u00e9der l'exp\u00e9rience de la r\u00e9alisation d'essais scientifiques sur le rendement d'un produit agricole.</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>Les chercheurs doivent suivre les bonnes pratiques de laboratoire (BPL) et les proc\u00e9dures appropri\u00e9es de contr\u00f4le de la qualit\u00e9\n<ul>\n<li>L'entreprise ou l'institut de recherche n'a pas n\u00e9cessairement besoin d'\u00eatre certifi\u00e9 comme BPL</li>\n</ul>\n</li>\n<li>L'\u00e9tablissement de recherche doit assurer la s\u00e9curit\u00e9 de ses employ\u00e9s durant l'essai.</li>\n<li>Les chercheurs doivent effectuer la recherche dans des endroits o\u00f9\u00a0:\n<ul>\n<li>le produit n'est pas susceptible de s'\u00e9chapper (par exemple, par ruissellement, \u00e9rosion, d\u00e9rive, lessivage, etc.);</li>\n<li>l'exposition des passants au suppl\u00e9ment, au sol ou aux v\u00e9g\u00e9taux trait\u00e9s est limit\u00e9e.</li>\n</ul>\n</li>\n<li><strong>Sauf</strong> si une exemption de l'obligation de d\u00e9truire la r\u00e9colte a \u00e9t\u00e9 accord\u00e9e par l'ACIA\u00a0:\n<ul>\n<li>Les chercheurs <strong>ne doivent pas vendre ou distribuer</strong> les plantes trait\u00e9es, les milieux de croissance, et le produit inutilis\u00e9</li>\n<li>Les chercheurs doivent \u00e9liminer en toute s\u00e9curit\u00e9 tous les suppl\u00e9ments, engrais et mat\u00e9riel v\u00e9g\u00e9tal, y compris les cultures r\u00e9colt\u00e9es, provenant des sites trait\u00e9s \u00e0 la fin des essais</li>\n<li>Les requ\u00e9rants doivent proposer une m\u00e9thode d'\u00e9limination s\u00e9curitaire\n<ul>\n<li>Les m\u00e9thodes peuvent varier en fonction de\u00a0: \n<ul>\n<li>la nature du suppl\u00e9ment (microbien par rapport \u00e0 chimique)</li>\n<li>la probabilit\u00e9 de sa propagation et de son \u00e9tablissement dans l'environnement</li>\n<li>le risque relatif rattach\u00e9 \u00e0 la diss\u00e9mination.</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n</li>\n</ul>\n\n<h2>Exemptions s'appliquant aux essais en milieu ferm\u00e9 et d\u00e9rogation de recherche</h2>\n\n<p>Les essais de recherche o\u00f9 la diss\u00e9mination du suppl\u00e9ment dans l'environnement est impossible, tels que les essais en milieu ferm\u00e9 (laboratoires, par exemple), ne n\u00e9cessitent pas d'autorisation de recherche. Mais les essais doivent \u00eatre accompagn\u00e9s des mesures appropri\u00e9es pour pr\u00e9venir les rejets dans l'environnement.</p>\n\n<p>Les requ\u00e9rants souhaitant recevoir une exemption de l'obligation d'obtenir une autorisation de recherche doivent donc pr\u00e9senter une demande afin d'obtenir une d\u00e9rogation de recherche. Pour en savoir plus sur les exigences pour une d\u00e9rogation de recherche, veuillez vous r\u00e9f\u00e9rer aux <a href=\"/fra/1307855399697/1320243929540#a5\">exemptions s'appliquant aux essais en milieu ferm\u00e9 et d\u00e9rogation de recherche</a> (RW).</p>\n\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/protection-des-vegetaux/engrais/apercu/fra/1646671738613/1646671738988\" rel=\"prev\">Pr\u00e9c\u00e9dent</a></li>\n\n<li class=\"next\"><a href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646672025348/1646672025708\" rel=\"next\">Suivant</a></li>\n\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}