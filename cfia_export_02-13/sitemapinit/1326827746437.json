{
    "dcr_id": "1326827653939",
    "title": {
        "en": "European grapevine moth \u2013 Lobesia botrana",
        "fr": "L'eud\u00e9mis de la vigne \u2013 Lobesia botrana"
    },
    "html_modified": "2024-02-13 9:47:12 AM",
    "modified": "2019-07-09",
    "issued": "2012-01-17 14:14:15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_lobbot_index_1326827653939_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_lobbot_index_1326827653939_fra"
    },
    "ia_id": "1326827746437",
    "parent_ia_id": "1307078272806",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1307078272806",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "European grapevine moth \u2013 Lobesia botrana",
        "fr": "L'eud\u00e9mis de la vigne \u2013 Lobesia botrana"
    },
    "label": {
        "en": "European grapevine moth \u2013 Lobesia botrana",
        "fr": "L'eud\u00e9mis de la vigne \u2013 Lobesia botrana"
    },
    "templatetype": "content page 1 column",
    "node_id": "1326827746437",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1307077188885",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/european-grapevine-moth/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/l-eudemis-de-la-vigne/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "European grapevine moth \u2013 Lobesia botrana",
            "fr": "L'eud\u00e9mis de la vigne \u2013 Lobesia botrana"
        },
        "description": {
            "en": "The European grapevine moth is currently regulated as a quarantine pest in Canada, the United States (under eradication in California) and Chile (under official control). It is also regulated by Argentina, Australia, China, Korea, New Zealand and Taiwan.",
            "fr": "L'eud\u00e9mis de la vigne est actuellement r\u00e9glement\u00e9 au Canada comme ravageur de quarantaine. Il est \u00e9galement r\u00e9glement\u00e9 \u00e0 ce titre aux \u00c9tats-Unis (o\u00f9 il est la cible de mesures d'\u00e9radication en Californie) et au Chili (o\u00f9 il fait l'objet de mesures de lutte officielles). Il est aussi r\u00e9glement\u00e9 en Argentine, en Australie, en Chine, en Cor\u00e9e, en Nouvelle-Z\u00e9lande et \u00e0 Ta\u00efwan."
        },
        "keywords": {
            "en": "inspection, plants, plant health, nursery, greenhouse, European grapevine moth, Lobesia botrana",
            "fr": "inspection, plante, protection des v&#233;g&#233;taux, serre, p&#233;pini&#232;res, L&#39;eud&#233;mis de la vigne, Lobesia botrana"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-17 14:14:15",
            "fr": "2012-01-17 14:14:15"
        },
        "modified": {
            "en": "2019-07-09",
            "fr": "2019-07-09"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "European grapevine moth \u2013 Lobesia botrana",
        "fr": "L'eud\u00e9mis de la vigne \u2013 Lobesia botrana"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-left mrgn-rght-md\">\n<p><img alt=\"European grapevine moth - Lobesia botrana\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lobbot_index_image_1326470761124_eng.jpg\" class=\"img-responsive\"></p></div>\n<p>The European grapevine moth is currently regulated as a quarantine pest in Canada and is present in the countries listed in <a href=\"/plant-health/invasive-species/directives/date/d-13-03/eng/1448986060402/1448986062493#app2\">Appendix 2 of directive D-13-03</a>.</p>\n<p>The preferred hosts of the European grapevine moth are grapes. However, it may also feed on several other plants or plant products, including stone fruits, blueberries, blackberries, raspberries, gooseberries, currants and pomegranates.</p>\n\n<h2>What information is available</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/european-grapevine-moth/factsheet/eng/1326832076174/1326832179736\">Pest fact sheet</a></li>\n</ul>\n\n<h3>Policy directive</h3>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-13-03/eng/1448986060402/1448986062493\">D-13-03: Phytosanitary import requirements to prevent the introduction of <i lang=\"la\">Lobesia  botrana</i>, the European grapevine moth</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-95-08/eng/1322413085880/1322413275292\">D-95-08: Phytosanitary import requirements for fresh temperate fruits and tree nuts</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-left mrgn-rght-md\">\n<p><img alt=\"L'eud\u00e9mis de la vigne - Lobesia botrana\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_lobbot_index_image_1326470761124_fra.jpg\" class=\"img-responsive\"></p></div>\n\n<p>L'eud\u00e9mis de la vigne est actuellement r\u00e9glement\u00e9 au Canada comme ravageur de quarantaine. Il est pr\u00e9sent dans les pays indiqu\u00e9s \u00e0 l'<a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-13-03/fra/1448986060402/1448986062493#app2\">annexe 2 de la directive D-13-03</a>.</p>\n<p>Le raisin est le principal fruit h\u00f4te de l'eud\u00e9mis de la vigne. Le ravageur s'attaque cependant \u00e0 plusieurs autres v\u00e9g\u00e9taux ou produits v\u00e9g\u00e9taux, dont les fruits \u00e0 noyaux, les bleuets, les m\u00fbres, les framboises, les groseilles, les gadelles et les grenades.</p>\n\n<h2>Quels renseignements sont disponibles</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/l-eudemis-de-la-vigne/fiche-d-information/fra/1326832076174/1326832179736\">Feuillet d'information</a></li>\n</ul>\n\n<h3>Directives de la protection des v\u00e9g\u00e9taux</h3>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-13-03/fra/1448986060402/1448986062493\">D-13-03\u00a0: Exigences phytosanitaires relatives \u00e0 l'importation visant \u00e0 pr\u00e9venir l'introduction de <i lang=\"la\">Lobesia        botrana</i>, l'eud\u00e9mis de la vigne</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-95-08/fra/1322413085880/1322413275292\">D-95-08\u00a0: Exigences phytosanitaires d'importation pour les fruits temp\u00e9r\u00e9s frais et les noix</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}