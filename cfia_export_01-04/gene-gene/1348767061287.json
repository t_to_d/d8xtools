{
    "dcr_id": "1348767061287",
    "title": {
        "en": "Statistics: Consumer food safety complaints",
        "fr": "Statistiques\u00a0: Plaintes de consommateurs sur la salubrit\u00e9 des aliments"
    },
    "html_modified": "6-5-2021",
    "modified": "2-6-2023",
    "issued": "23-4-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/news_resource_food_safety_complaints_1348767061287_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/news_resource_food_safety_complaints_1348767061287_fra"
    },
    "parent_ia_id": "1374822930369",
    "ia_id": "1348767710199",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Statistics: Consumer food safety complaints",
        "fr": "Statistiques\u00a0: Plaintes de consommateurs sur la salubrit\u00e9 des aliments"
    },
    "label": {
        "en": "Statistics: Consumer food safety complaints",
        "fr": "Statistiques\u00a0: Plaintes de consommateurs sur la salubrit\u00e9 des aliments"
    },
    "templatetype": "content page 1 column",
    "node_id": "1348767710199",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1332207100013",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/canada-s-food-safety-system/consumer-food-safety-complaints/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/systeme-canadien-de-salubrite-des-aliments/plaintes-de-consommateurs-sur-la-salubrite-des-ali/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Statistics: Consumer food safety complaints",
            "fr": "Statistiques\u00a0: Plaintes de consommateurs sur la salubrit\u00e9 des aliments"
        },
        "description": {
            "en": "Consumers play an important role in the food safety system by reporting any food safety concerns to the Canadian Food Inspection Agency (CFIA).",
            "fr": "Les consommateurs jouent un r\u00f4le important dans le syst\u00e8me de salubrit\u00e9 des aliments en signalant tout probl\u00e8me de salubrit\u00e9 alimentaire \u00e0 l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "Statistics, Consumer, food, safety, complaints",
            "fr": "Statistiques, Plaintes, consommateurs, salubrit\u00e9, aliments"
        },
        "dcterms.subject": {
            "en": "house of commons,food labeling,government information,inspection,food inspection,parliament,plants,government publication,food safety,animal health",
            "fr": "chambre des communes,\u00e9tiquetage des aliments,information gouvernementale,inspection,inspection des aliments,parlement,plante,publication gouvernementale,salubrit\u00e9 des aliments,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-05-06",
            "fr": "2021-05-06"
        },
        "modified": {
            "en": "2023-06-02",
            "fr": "2023-06-02"
        },
        "type": {
            "en": "reference material,statistics",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,statistiques"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Statistics: Consumer food safety complaints",
        "fr": "Statistiques\u00a0: Plaintes de consommateurs sur la salubrit\u00e9 des aliments"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Consumers play an important role in the food safety system by reporting any food safety concerns to the Canadian Food Inspection Agency (CFIA). The information you provide may help us ensure that unsafe products are removed from the marketplace and that other consumers are informed.</p>\n\n<p>On average, the CFIA receives\u00a02,035 reports of potential food safety concerns from consumers each year.</p>\n\n<p>To find out more about the consumer's role in the food safety system, and where to report a food complaint, please consult the <a href=\"/food-safety-for-consumers/eng/1299093858143/1303766424564\">Food safety for consumers</a> page.</p>\n\n<h2>Consumer food safety complaints</h2>\n\n<figure>\n\n<img alt=\"Consumer food safety complaints: April 2017 - March 2022. Description follows.\" src=\"/DAM/DAM-food-aliments/STAGING/images-images/news_resource_food_safety_complaints_chart1_1620611666259_eng.jpg\" class=\"img-responsive\">\n<details>\n<summary>Description of graph\u00a0\u2013 April\u00a02018\u00a0\u2013 March\u00a02023 Table</summary>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Consumer food safety complaints from April\u00a01,\u00a02018\u00a0\u2013 March\u00a031,\u00a02023</caption>\n<thead>\n<tr class=\"active\">\n<th scope=\"col\"><span class=\"wb-inv\">-</span></th>\n<th scope=\"col\">Apr\u00a018\u00a0\u2013 Mar\u00a019</th>\n<th scope=\"col\">Apr\u00a019\u00a0\u2013 Mar\u00a020</th>\n<th scope=\"col\">Apr\u00a020\u00a0\u2013 Mar\u00a021</th>\n<th scope=\"col\">Apr\u00a021\u00a0\u2013 Mar\u00a022</th>\n<th scope=\"col\">Apr\u00a022\u00a0\u2013 Mar\u00a023</th>\n</tr>\n</thead>\n\n<tbody>\n<tr>\n<td>Total complaints</td>\n<td class=\"text-right\">2389</td>\n<td class=\"text-right\">2079</td>\n<td class=\"text-right\">1918</td>\n<td class=\"text-right\">1840</td>\n<td class=\"text-right\">1958</td>\n</tr>\n</tbody>\n</table>\n\n</div>\n</details>\n\n</figure>\n\n<h2>Consumer food safety complaints by hazard</h2>\n\n<figure>\n\n<img alt=\"Consumer food safety complaints by hazard: April 2017 - March 2022. Description follows.\" src=\"/DAM/DAM-food-aliments/STAGING/images-images/news_resource_food_safety_complaints_chart2_1620611667102_eng.jpg\" class=\"img-responsive\">\n\n\n<details>\n<summary>Description of graph\u00a0\u2013 April\u00a02018\u00a0\u2013 March\u00a02023 Table</summary>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Consumer food safety complaints by hazard from April\u00a01,\u00a02018 to March\u00a031,\u00a02023</caption>\n<thead>\n<tr class=\"active\">\n<th scope=\"col\"><span class=\"wb-inv\">-</span></th>\n<th scope=\"col\">Apr\u00a018\u00a0\u2013 Mar\u00a019</th>\n<th scope=\"col\">Apr\u00a019\u00a0\u2013 Mar\u00a020</th>\n<th scope=\"col\">Apr\u00a020\u00a0\u2013 Mar\u00a021</th>\n<th scope=\"col\">Apr\u00a021\u00a0\u2013 Mar\u00a022</th>\n<th scope=\"col\">Apr\u00a022\u00a0\u2013 Mar\u00a023</th>\n</tr>\n</thead>\n\n<tbody>\n<tr>\n<td>Allergen complaints</td>\n<td class=\"text-right\">221</td>\n<td class=\"text-right\">160</td>\n<td class=\"text-right\">156</td>\n<td class=\"text-right\">174</td>\n<td class=\"text-right\">157</td>\n</tr>\n<tr>\n<td>Chemical complaints</td>\n<td class=\"text-right\">133</td>\n<td class=\"text-right\">115</td>\n<td class=\"text-right\">100</td>\n<td class=\"text-right\">86</td>\n<td class=\"text-right\">100</td>\n</tr>\n<tr>\n<td>Extraneous material complaints</td>\n\n<td class=\"text-right\">1383</td>\n<td class=\"text-right\">1291</td>\n<td class=\"text-right\">1202</td>\n<td class=\"text-right\">1162</td>\n<td class=\"text-right\">1202</td>\n</tr>\n<tr>\n<td>Microbiological complaints</td>\n\n<td class=\"text-right\">633</td>\n<td class=\"text-right\">484</td>\n<td class=\"text-right\">429</td>\n<td class=\"text-right\">405</td>\n<td class=\"text-right\">430</td>\n</tr>\n<tr>\n<td>Other</td>\n\n<td class=\"text-right\">56</td>\n<td class=\"text-right\">54</td>\n<td class=\"text-right\">69</td>\n<td class=\"text-right\">41</td>\n<td class=\"text-right\">69</td>\n</tr>\n</tbody>\n</table>\n\n</div>\n</details>\n\n</figure>\n\n\n<h2>Glossary</h2>\n\n<dl>\n\n<dt>Allergen</dt>\n\n<dd>A food product may contain ingredients such as peanuts, milk or eggs that are not identified or are incorrectly identified on the label and that can cause adverse reactions in people who are allergic to the item.</dd>\n\n<dt>Chemical</dt>\n\n<dd>A food product may contain chemical residues such as lead, mercury or pesticides that, at certain levels, can affect human health.</dd>\n\n<dt>Extraneous material</dt>\n\n<dd>A food product may contain material from an outside source, such as metal, glass or hair. These are not necessarily a risk to human health.</dd>\n\n<dt>Microbiological</dt>\n\n<dd>A food product may be contaminated by micro-organisms, such as bacteria, viruses or parasites, which have the potential to cause illness.</dd>\n\n<dt>Other</dt>\n\n<dd>A food product may be of concern due to the presence of a hazard that does not fall within one of the above categories. Examples include non-permitted ingredients, nutrition concerns and potential tampering.</dd>\n</dl>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Les consommateurs jouent un r\u00f4le important dans le syst\u00e8me de salubrit\u00e9 des aliments en signalant tout probl\u00e8me de salubrit\u00e9 alimentaire \u00e0 l'Agence canadienne d'inspection des aliments (ACIA). Les renseignements obtenus peuvent permettre \u00e0 l'ACIA de s'assurer que les aliments insalubres soient retir\u00e9s du march\u00e9 et que l'ensemble de la population en soit inform\u00e9.</p>\n\n<p>Chaque ann\u00e9e, l'ACIA re\u00e7oit en moyenne\u00a02\u00a0035 signalements de la part des consommateurs concernant des probl\u00e8mes li\u00e9s \u00e0 la salubrit\u00e9 des aliments.</p>\n\n<p>Pour en apprendre davantage sur le r\u00f4le des consommateurs dans le syst\u00e8me de salubrit\u00e9 des aliments, et conna\u00eetre o\u00f9 signaler une plainte alimentaire, veuillez consulter la page sur la <a href=\"/salubrite-alimentaire-pour-les-consommateurs/fra/1299093858143/1303766424564\">Salubrit\u00e9 des aliments pour les consommateurs</a>.</p>\n\n<h2>Plaintes de consommateurs sur la salubrit\u00e9 des aliments</h2>\n\n<figure>\n\n<img alt=\"Plaintes au sujet de la salubrit\u00e9 des aliments : avril 2017 \u00e0 mars 2022. Description ci-dessous.\" src=\"/DAM/DAM-food-aliments/STAGING/images-images/news_resource_food_safety_complaints_chart1_1620611666259_fra.jpg\" class=\"img-responsive\">\n\n<details>\n<summary>Description du graphique\u00a0\u2013 Tableau d'avril\u00a02018 \u00e0 mars\u00a02023</summary>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Plaintes de consommateurs sur la salubrit\u00e9 des aliments du\u00a01 avril\u00a02018 au\u00a031 mars\u00a02023</caption>\n<thead>\n<tr class=\"active\">\n<th scope=\"col\"><span class=\"wb-inv\">-</span></th>\n\n<th scope=\"col\">Avril\u00a02018\u00a0\u2013 mars\u00a02019</th>\n<th scope=\"col\">Avril\u00a02019\u00a0\u2013 mars\u00a02020</th>\n<th scope=\"col\">Avril\u00a02020\u00a0\u2013 mars\u00a02021</th>\n<th scope=\"col\">Avril\u00a02021\u00a0\u2013 mars\u00a02022</th>\n<th scope=\"col\">Avril\u00a02022\u00a0\u2013 mars\u00a02023</th>\n</tr>\n</thead>\n\n<tbody>\n<tr>\n<td>Total des plaintes</td>\n\n<td class=\"text-right\">2389</td>\n<td class=\"text-right\">2079</td>\n<td class=\"text-right\">1918</td>\n<td class=\"text-right\">1840</td>\n<td class=\"text-right\">1958</td>\n</tr>\n</tbody>\n</table>\n\n</div>\n</details>\n\n</figure>\n\n\n<h2>Plaintes de consommateurs sur la salubrit\u00e9 des aliments par danger</h2>\n\n<figure>\n\n<img alt=\"Plaintes de consommateurs sur la salubrit\u00e9 des aliments par danger : avril 2017 \u00e0 mars 2022. Description ci-dessous.\" src=\"/DAM/DAM-food-aliments/STAGING/images-images/news_resource_food_safety_complaints_chart2_1620611667102_fra.jpg\" class=\"img-responsive\">\n\n<details>\n<summary>Description de graphique\u00a0\u2013 Tableau d'avril\u00a02018 \u00e0 mars\u00a02023</summary>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Plaintes de consommateurs sur la salubrit\u00e9 des aliments par danger de\u00a01 avril\u00a02018 au\u00a031 mars\u00a02023</caption>\n<thead>\n<tr class=\"active\">\n<th scope=\"col\"><span class=\"wb-inv\">-</span></th>\n\n<th scope=\"col\">Avril\u00a02018\u00a0\u2013 mars\u00a02019</th>\n<th scope=\"col\">Avril\u00a02019\u00a0\u2013 mars\u00a02020</th>\n<th scope=\"col\">Avril\u00a02020\u00a0\u2013 mars\u00a02021</th>\n<th scope=\"col\">Avril\u00a02021\u00a0\u2013 mars\u00a02022</th>\n<th scope=\"col\">Avril\u00a02022\u00a0\u2013 mars\u00a02023</th>\n</tr>\n</thead>\n\n<tbody>\n<tr>\n<td>Plaintes concernant un allerg\u00e8ne alimentaire</td>\n\n<td class=\"text-right\">221</td>\n<td class=\"text-right\">160</td>\n<td class=\"text-right\">156</td>\n<td class=\"text-right\">174</td>\n<td class=\"text-right\">157</td>\n</tr>\n<tr>\n<td>Plaintes concernant un produit chimique</td>\n\n<td class=\"text-right\">133</td>\n<td class=\"text-right\">115</td>\n<td class=\"text-right\">100</td>\n<td class=\"text-right\">86</td>\n<td class=\"text-right\">100</td>\n</tr>\n<tr>\n<td>Plaintes concernant une mati\u00e8re \u00e9trang\u00e8re</td>\n\n<td class=\"text-right\">1383</td>\n<td class=\"text-right\">1291</td>\n<td class=\"text-right\">1202</td>\n<td class=\"text-right\">1162</td>\n<td class=\"text-right\">1202</td>\n</tr>\n<tr>\n<td>Plaintes concernant un produit microbiologique</td>\n\n<td class=\"text-right\">633</td>\n<td class=\"text-right\">484</td>\n<td class=\"text-right\">429</td>\n<td class=\"text-right\">405</td>\n<td class=\"text-right\">430</td>\n</tr>\n<tr>\n<td>Autre</td>\n\n<td class=\"text-right\">56</td>\n<td class=\"text-right\">54</td>\n<td class=\"text-right\">69</td>\n<td class=\"text-right\">41</td>\n<td class=\"text-right\">69</td>\n</tr>\n</tbody>\n</table>\n\n</div>\n</details>\n\n</figure>\n\n\n<h2>Glossaire</h2>\n\n<dl>\n\n<dt>Allerg\u00e8ne</dt>\n\n<dd>Un aliment peut contenir des ingr\u00e9dients, comme des arachides, du lait ou des \u0153ufs, qui ne sont pas d\u00e9clar\u00e9s ou sont incorrectement d\u00e9clar\u00e9s sur l'\u00e9tiquette et qui peuvent causer des r\u00e9actions ind\u00e9sirables chez les personnes qui en sont allergiques.</dd>\n\n<dt>Produit chimique</dt>\n\n<dd>Un aliment peut contenir des r\u00e9sidus chimiques, comme le plomb, le mercure ou des pesticides, qui peuvent, \u00e0 certaines concentrations, avoir une incidence sur la sant\u00e9 humaine.</dd>\n\n<dt>Mati\u00e8re \u00e9trang\u00e8re</dt>\n\n<dd>Un aliment peut contenir des mati\u00e8res provenant d'une source externe, comme du m\u00e9tal, du verre ou des cheveux, qui ne pr\u00e9sentent pas forc\u00e9ment de risque pour la sant\u00e9 humaine.</dd>\n\n<dt>Produit microbiologique</dt>\n\n<dd>Un aliment peut \u00eatre contamin\u00e9 par des micro-organismes, comme des bact\u00e9ries, des virus ou des parasites, qui pourraient engendrer des maladies.</dd>\n\n<dt>Autre</dt>\n\n<dd>Un aliment peut constituer une source de pr\u00e9occupation autre que celles d\u00e9crites ci-dessus, comme par exemple la pr\u00e9sence d'ingr\u00e9dients interdits, des probl\u00e8mes li\u00e9s \u00e0 la nutrition ou une alt\u00e9ration potentielle due \u00e0 un acte de sabotage.</dd>\n\n</dl>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}