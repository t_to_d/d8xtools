{
    "dcr_id": "1508180105358",
    "title": {
        "en": "Foreign establishment verifications",
        "fr": "V\u00e9rification d'\u00e9tablissements \u00e9trangers"
    },
    "html_modified": "2024-02-13 9:52:28 AM",
    "modified": "2020-06-18",
    "issued": "2017-10-27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_import_for_verif_office_1508180105358_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_import_for_verif_office_1508180105358_fra"
    },
    "ia_id": "1508180202351",
    "parent_ia_id": "1526656151476",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1526656151476",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Foreign establishment verifications",
        "fr": "V\u00e9rification d'\u00e9tablissements \u00e9trangers"
    },
    "label": {
        "en": "Foreign establishment verifications",
        "fr": "V\u00e9rification d'\u00e9tablissements \u00e9trangers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1508180202351",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526656151226",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/foreign-establishment-verifications/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/verification-d-etablissements-etrangers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Foreign establishment verifications",
            "fr": "V\u00e9rification d'\u00e9tablissements \u00e9trangers"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) conducts on-site verifications in foreign establishments that manufacture food for export to Canada.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) effectue des v\u00e9rifications sur place dans des \u00e9tablissements \u00e9trangers o\u00f9 sont fabriqu\u00e9s des produits alimentaires destin\u00e9s au march\u00e9 canadien."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, Foreign establishment verifications, FEV, food inspection tools, Partnerships",
            "fr": "V\u00e9rification d\u2019\u00e9tablissements \u00e9trangers, VEE, outils d'inspection, partenariats"
        },
        "dcterms.subject": {
            "en": "imports,inspection,food inspection,tools,border crossing,consumer protection,food safety,audit",
            "fr": "importation,inspection,inspection des aliments,outil,passage des fronti\u00e8res,protection du consommateur,salubrit\u00e9 des aliments,v\u00e9rification"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,International Programs Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction internationale des programmes"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-10-27",
            "fr": "2017-10-27"
        },
        "modified": {
            "en": "2020-06-18",
            "fr": "2020-06-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public,business",
            "fr": "gouvernement,grand public,entreprises"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Foreign establishment verifications",
        "fr": "V\u00e9rification d'\u00e9tablissements \u00e9trangers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) conducts on-site verifications in foreign establishments that manufacture food for export to Canada.  The main objectives of these verifications are to:</p>\n<ul class=\"lst-spcd\">\n<li>observe how the manufactured food products meet Canadian import requirements or provide the same level of food safety as compared to what is expected from food manufactured in Canada</li>\n<li>promote compliance to Canadian requirements with trading partners and foreign food safety authorities</li>\n<li>gather information to further address the risks associated with imported foods and help better refine the import surveillance work within Canada</li>\n</ul>\n<p>Foreign establishment verifications are one of many food inspection tools used by the CFIA to improve food safety. Other measures include <a href=\"/importing-food-plants-or-animals/food-imports/foreign-systems/audits/eng/1404502855855/1404502949798\">audits and assessments</a> of the food safety systems of Canada's trading partners, technical assistance activities conducted with developing countries, inspection and compliance verification of food manufacturers and importers in Canada, sampling activities, and border controls on imported food.</p>\n<p>Foreign establishment verifications allow the CFIA to collaboratively and proactively build on existing relationships with other foreign food safety authorities to ensure the safety of products exported to Canada.</p>\n<h2>Reports</h2>\n<a href=\"/importing-food-plants-or-animals/food-imports/foreign-establishment-verifications/annual-summary-report-2016-2017/eng/1508248624887/1508260597990\">Annual Summary Report 2016-17</a>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) effectue des v\u00e9rifications sur place dans des \u00e9tablissements \u00e9trangers o\u00f9 sont fabriqu\u00e9s des produits alimentaires destin\u00e9s au march\u00e9 canadien. Les principaux objectifs de ces v\u00e9rifications sont les suivants\u00a0:</p>\n<ul class=\"lst-spcd\">\n<li>observer comment les produits alimentaires manufactur\u00e9s r\u00e9pondent aux exigences canadiennes en mati\u00e8re d'importation ou offrent le m\u00eame niveau de salubrit\u00e9 que doivent offrir les aliments fabriqu\u00e9s au Canada</li>\n<li>promouvoir la conformit\u00e9 aux exigences canadiennes aupr\u00e8s des partenaires commerciaux et des autorit\u00e9s de salubrit\u00e9 des aliments \u00e9trang\u00e8res</li>\n<li>recueillir des renseignements afin de diminuer davantage les risques associ\u00e9s aux aliments import\u00e9s et d'aider \u00e0 mieux cibler les activit\u00e9s de surveillance des importations au Canada</li>\n</ul>\n<p>Les v\u00e9rifications d'\u00e9tablissements \u00e9trangers sont l'un des nombreux outils d'inspection utilis\u00e9s par l'ACIA pour am\u00e9liorer la salubrit\u00e9 des aliments. D'autres mesures sont aussi mises en \u0153uvre, comme des <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/systemes-etrangers/verifications/fra/1404502855855/1404502949798\">audits et \u00e9valuations</a> des syst\u00e8mes de salubrit\u00e9 des aliments des partenaires commerciaux du Canada, de l'assistance technique, des inspections et des v\u00e9rifications de la conformit\u00e9 des fabricants et importateurs d'aliments au Canada, des activit\u00e9s d'\u00e9chantillonnage et des contr\u00f4les des aliments import\u00e9s \u00e0 la fronti\u00e8re.</p>\n<p>Les v\u00e9rifications d'\u00e9tablissements \u00e9trangers permettent \u00e0 l'ACIA de renforcer de fa\u00e7on proactive et collaborative les relations avec les autorit\u00e9 de salubrit\u00e9 des aliments des pays \u00e9trangers et ainsi assurer la s\u00e9curit\u00e9 des produits export\u00e9s au Canada.</p>\n<h2>Rapports</h2>\n<a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/verification-d-etablissements-etrangers/rapport-sommaire-annuel-2016-2017/fra/1508248624887/1508260597990\">Rapport sommaire annuel 2016-2017</a>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}