{
    "dcr_id": "1519418391827",
    "title": {
        "en": "Egypt\u00a0\u2013\u00a0Export requirements for milk and dairy products",
        "fr": "\u00c9gypte\u00a0\u2013\u00a0Exigences d'exportation pour le lait et les produits laitiers"
    },
    "html_modified": "2024-02-13 9:52:48 AM",
    "modified": "2023-08-18",
    "issued": "2018-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_egypt_1519418391827_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_egypt_1519418391827_fra"
    },
    "ia_id": "1519418392280",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Inspecting and investigating - Performing export certification activities|Exporting food",
        "fr": "Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation|Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Egypt\u00a0\u2013\u00a0Export requirements for milk and dairy products",
        "fr": "\u00c9gypte\u00a0\u2013\u00a0Exigences d'exportation pour le lait et les produits laitiers"
    },
    "label": {
        "en": "Egypt\u00a0\u2013\u00a0Export requirements for milk and dairy products",
        "fr": "\u00c9gypte\u00a0\u2013\u00a0Exigences d'exportation pour le lait et les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1519418392280",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/egypt-milk-and-dairy-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/egypte-lait-et-produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Egypt\u00a0\u2013\u00a0Export requirements for milk and dairy products",
            "fr": "\u00c9gypte\u00a0\u2013\u00a0Exigences d'exportation pour le lait et les produits laitiers"
        },
        "description": {
            "en": "Countries to which exports are currently made - Egypt",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e - \u00c9gypte"
        },
        "keywords": {
            "en": "Egypt, Export requirements, milk and dairy products",
            "fr": "\u00c9gypte, Exigences d'exportation, lait et produits laitiers"
        },
        "dcterms.subject": {
            "en": "agri-food industry,dairy products,exports",
            "fr": "industrie agro-alimentaire,produit laitier,exportation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "modified": {
            "en": "2023-08-18",
            "fr": "2023-08-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Egypt\u00a0\u2013\u00a0Export requirements for milk and dairy products",
        "fr": "\u00c9gypte\u00a0\u2013\u00a0Exigences d'exportation pour le lait et les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">1. Eligible/ineligible product</a></li>\n<li><a href=\"#a2\">2. Pre-export approvals by the competent authority of the importing country</a></li>\n<li><a href=\"#a3\">3. Production Controls and Inspection Requirements</a></li>\n<li><a href=\"#a4\">4. Labelling, packaging and marking requirements</a></li>\n<li><a href=\"#a5\">5. Documentation requirements</a></li>\n<li><a href=\"#a6\">6. Other information</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<ul>\n<li>As per the import permit.</li>\n</ul>\n\n<h3>Ineligible</h3>\n\n<ul>\n<li>Information not available.</li>\n</ul>\n\n<h2 id=\"a2\">2. Pre-export approvals by the competent authority of the importing country</h2>\n\n\n<h3>Establishments</h3>\n<p>No information known regarding an approved establishment list.</p>\n\n<h3>Import Permit</h3>\n\n<ul>\n<li>Import permit required to export dairy products or dairy based foods. The importer and exporter are responsible to make the necessary arrangements with the Egyptian authorities to obtain the import permit.</li>\n</ul>\n\n<h2 id=\"a3\">3. Production controls and inspection requirements</h2>\n\n<p>The inspector must verify during a preventive control inspection that the establishment is aware of the dairy standards and requirements of Egypt and has a specific export procedure in place.</p>\n<p>The exporter and importer are responsible to supply the required Halal certification as well as the required authorization from the Egyptian Embassy in Canada for all the certificates issued for the shipment.</p>\n\n\n<h3>Manufacturer's declaration</h3>\n\n<ul class=\"lst-spcd\">\n<li>All export requests for milk and dairy products must be accompanied by a Manufacturer's declaration completed and signed by an authorized person of the food manufacturing establishment/facility.</li>\n<li>To request a template of the manufacturer's declaration, please contact your local CFIA office.</li>\n<li>The product(s) must be clearly identified on the manufacturer's declaration and match exactly the product(s) that are part of the export shipment.</li>\n</ul>\n\n<p><strong>Note:</strong> Inspectors will verify that the manufacturer's declaration is completed appropriately and reserve the right to request any other information that they think is necessary for the final certification of the product</p>\n\n\n<h2 id=\"a4\">4. Labelling, marking and packaging requirements</h2>\n\n<p>The exporter and importer are responsible to label the product according to applicable Egyptian requirements, for example Halal and Arabic labelling.</p>\n\n<h2 id=\"a5\">5. Documentation requirements</h2>\n\n<p>Canada does not have a negotiated certificate with Egypt. The certificate below is issued on the basis of commercial risk.</p>\n\n<h3>Certificate</h3>\n\n<ul>\n<li>Health certificate for the export of milk and milk products for human consumption from Canada to Egypt. TS.EG\n<p><strong>Note:</strong>\n</p><ul class=\"lst-spcd\">\n<li>This certificate is signed by the CFIA official veterinarian and official inspector.</li>\n<li>It is the responsibility of the exporter to check that the certificate above covers the requirements of Egypt. Specific import conditions could be required. If this is the case, please forward these conditions to your local CFIA office who will forward them to the Food Import and Export Division (FIED) for review and action if necessary.</li>\n<li>Export certificates cannot be issued for products that have left Canada.</li>\n<li>The exporter is responsible for providing, where applicable, the unique Advanced Cargo Information Declaration (ACIA) number that will be added to importer box of the export certificate issued by the CFIA. The certificate can be issued without the ACID number, however, the exporter must add it in the form of a sticker on the issued certificate. Any documentation accompanying the shipment should also contain this number. See NAFZA's question and answer section.</li>\n</ul>\n</li>\n</ul>\n\n\n\n<h2 id=\"a6\">6. Other Information</h2>\n\n<ul class=\"lst-spcd\">\n<li>Samples (personal or commercial) of dairy products may be subject to the same requirements as a regular shipment. It is strongly recommended that the exporter verify these requirements with their importer.</li>\n<li>Exported dairy products transiting through a third country may require transit documentation. It is the responsibility of the exporter to ensure that the shipment will be accompanied by all the necessary certificates. Please work closely with your importer.</li>\n<li><a href=\"https://www.iseghalal.com/\">IS EG Halal Certification</a>. Is the only entity exclusively authorized by the Government of Egypt to certify Halal exports worldwide.</li>\n<li><a href=\"https://www.nafeza.gov.eg/en\">NAFEZA</a> National Single Window For Egyptian Trade Across Borders System</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">1. Produits admissibles/non admissibles</a></li>\n<li><a href=\"#a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</a></li>\n<li><a href=\"#a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</a></li>\n<li><a href=\"#a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</a></li>\n<li><a href=\"#a5\">5. Documents requis</a></li>\n<li><a href=\"#a6\">6. Autres renseignements</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<ul>\n<li>Selon le permis d'importation.</li>\n</ul>\n\n<h3>Non admissibles</h3>\n\n<ul>\n<li>Information non disponible.</li>\n</ul>\n\n<h2 id=\"a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<h3>\u00c9tablissements</h3>\n<p>Aucune information n'est connue concernant une liste d'approbation des \u00e9tablissements.</p>\n\n\n<h3>Permis d'importation</h3>\n\n<ul>\n<li>Permis d'importation requis pour exporter des produits laitiers ou des produits laitiers. L'importateur et l'exportateur sont responsables de faire les d\u00e9marches n\u00e9cessaires aupr\u00e8s des autorit\u00e9s \u00e9gyptiennes pour obtenir les permis d'importation.</li>\n</ul>\n\n<h2 id=\"a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'Inspection</h2>\n<p>L'inspecteur doit v\u00e9rifier au cours d'une inspection de contr\u00f4le pr\u00e9ventif que l'\u00e9tablissement est au courant des normes et exigences des produits laitiers de l'\u00c9gypte et qu'une proc\u00e9dure d'exportation sp\u00e9cifique est en place.</p>\n<p>L'importateur et l'exportateur sont responsables de fournir la certification halal requise de m\u00eame que les autorisations n\u00e9cessaires de la part de l'ambassade de l'\u00c9gypte au Canada relativement \u00e0 tous les certificats \u00e9mis pour la cargaison.</p>\n\n<h3>D\u00e9claration du fabricant</h3>\n<ul class=\"lst-spcd\">\n<li>Toutes les demandes finales d'exportation doivent \u00eatre accompagn\u00e9es d'une D\u00e9claration du fabricant compl\u00e9t\u00e9e et sign\u00e9e par une personne autoris\u00e9e de l'\u00e9tablissement/installation de fabrication d'aliments.</li>\n<li>Veuillez communiquer avec votre bureau local de l'ACIA afin d'obtenir un mod\u00e8le de la d\u00e9claration du fabricant.</li>\n<li>Le(s) produit(s) doivent \u00eatre clairement identifi\u00e9s sur la D\u00e9claration du fabricant et correspondre exactement au(x) produit(s) faisant partie de l'exp\u00e9dition d'exportation.</li>\n</ul>\n<p><strong>Remarque\u00a0:</strong> Les inspecteurs v\u00e9rifieront que la d\u00e9claration du fabricant est remplie de fa\u00e7on appropri\u00e9e et se r\u00e9servent le droit de demander tout autre renseignement qu'ils jugent n\u00e9cessaire \u00e0 la certification finale du produit.</p>\n\n\n<h2 id=\"a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</h2>\n\n<p>L'importateur et l'exportateur sont responsable de s'assurer que l'\u00e9tiquetage est conforme aux exigences de l'\u00c9gypte, par exemple \u00e9tiquetage Halal et en langue arabe.</p>\n\n<h2 id=\"a5\">5. Documents requis</h2>\n\n<p>Le Canada n'a pas de certificat n\u00e9goci\u00e9 avec l'\u00c9gypte. Le certificat ci-dessous est \u00e9mis sur la base du risque commercial.</p>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Certificat sanitaire pour l'exportation de lait et les produits laitiers pour consommation humaine du Canada vers l'\u00c9gypte. TS.EG\n<p><strong>Remarque\u00a0:</strong>\n</p><ul class=\"lst-spcd\">\n<li>Ce certificat est sign\u00e9 par le v\u00e9t\u00e9rinaire officiel et l'inspecteur officiel de l'ACIA.</li>\n<li>Il incombe \u00e0 l'exportateur de v\u00e9rifier que le certificat ci-dessus couvre les exigences de l'\u00c9gypte. Des conditions sp\u00e9cifiques pourraient \u00eatre exig\u00e9es. Si tel est le cas veuillez transmettre ces conditions \u00e0 votre bureau local qui les transmettra \u00e0 la Division d'importation et d'exportation des aliments (FIED) pour r\u00e9vision et action si n\u00e9cessaire.</li>\n<li>Les certificats d'exportation ne peuvent pas \u00eatre \u00e9mis pour des produits qui ont quitt\u00e9 le Canada.</li>\n<li>L'exportateur est responsable de fournir, le cas \u00e9ch\u00e9ant le num\u00e9ro unique Advanced Cargo Information Declaration (ACID) qui sera rajout\u00e9 \u00e0 la case importateur du certificat d'exportation \u00e9mis par l'ACIA. Le certificat  peut \u00eatre \u00e9mis sans le num\u00e9ro ACID, cependant l'exportateur devra l'apposer sous forme d'autocollant apr\u00e8s l'\u00e9mission du certificat. Tout document accompagnant l'exp\u00e9dition devrait aussi contenir ce num\u00e9ro. Voir la section question et r\u00e9ponses de NAFZA.</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a6\">6. Autres renseignements</h2>\n<ul class=\"lst-spcd\">\n<li>Les \u00e9chantillons (personnels ou commerciaux) de produits laitiers pourraient \u00eatre soumis aux m\u00eame exigences qu'une exp\u00e9dition r\u00e9guli\u00e8re. Il est fortement recommand\u00e9 \u00e0 l'exportateur de v\u00e9rifier ces exigences aupr\u00e8s de son importateur.</li>\n<li>Des produits laitiers export\u00e9s transitant par un pays tiers pourraient n\u00e9cessiter des documents de transit. C'est la responsabilit\u00e9 de l'exportateur de s'assurer que son exp\u00e9dition sera accompagn\u00e9e de tous les certificats n\u00e9cessaires. Veuillez travailler en \u00e9troite collaboration avec votre importateur.</li>\n<li><a href=\"https://www.iseghalal.com/\" lang=\"en\">IS EG Halal Certification (en anglais seulement)</a>. Est pr\u00e9sentement le seul organisme exclusivement autoris\u00e9 par le gouvernement \u00e9gyptien \u00e0 certifier les exportations Halal dans le monde entier.</li>\n<li><a href=\"https://www.nafeza.gov.eg/en\">NAFEZA (en anglais seulement)</a> Syst\u00e8me douanier de guichet unique \u00e9gyptien pour le commerce transfrontalier.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}