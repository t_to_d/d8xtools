{
    "dcr_id": "1200079270876",
    "title": {
        "en": "Ash Borer",
        "fr": "S\u00e9sie du fr\u00eane"
    },
    "modified": "2015-08-05",
    "issued": "2009-04-27",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2009-04-27",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/01/pfra_org_ash_borer_1200079270876_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/01/pfra_org_ash_borer_1200079270876_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Ash Borer",
        "fr": "S\u00e9sie du fr\u00eane"
    },
    "breadcrumb": {
        "en": "Ash Borer",
        "fr": "S\u00e9sie du fr\u00eane"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2009-04-27",
            "fr": "2009-04-27"
        },
        "modified": {
            "en": "2015-08-05",
            "fr": "2015-08-05"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Ash Borer",
            "fr": "S\u00e9sie du fr\u00eane"
        },
        "subject": {
            "en": "fruit;insecticides;caterpillars",
            "fr": "lutte antiparasitaire int\u00e9gr\u00e9e;lutte contre les mauvaises herbes;pommiers"
        },
        "dcterms:subject": {
            "en": "fungi;entomology",
            "fr": "biologie;entomologie"
        },
        "description": {
            "en": "Adults are present from late May to late July and deposit clusters of eggs in bark crevices and in wounds caused by pruning or previous borer attacks.",
            "fr": "Les larves adultes sont blanches ont une t\u00eate brun-roux et mesurent de 18 \u00e0 25 mm de long. Juste avant l'\u00e9mergence de l'adulte la nymphe brise la mince couche d'\u00e9corce en se tortillant."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "life cycle;late June;Photo credit;full grown larvae;late May",
            "fr": "int\u00e9rieur du bois;seconde fois;cycle biologique;larves adultes;s\u00e9sie du fr\u00eane"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "frequently asked questions",
            "fr": "transcription"
        },
        "creator": {
            "en": "Prairie Farm Rehabilitation Administration;Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Administration du r\u00e9tablissement agricole des Prairies;Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Ash Borer",
        "fr": "S\u00e9sie du fr\u00eane"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    \n<p><i>Podosesia syringae</i></p>\n\n<h2>Hosts</h2>\n\n<div class=\"row\">\n    <div class=\"col-md-4 pull-right\">\n\t\t\t\t\n<figure><img alt=\"Description of this image follows.\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_ash_borer_1.jpg\"><br>\n<figcaption><i>Ash borer larva and damage. <br>Photo credit: James Solomon, United States Department of Agriculture Forest Service, Bugwood.org </i></figcaption>\n</figure>\t\n\n<br>\n\t\t\t\n<figure><img alt=\"Description of this image follows.\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_ash_borer_2.jpg\"><br><figcaption><i>Infested ash trunk with ash borer pupal skins extruding from exit wounds. <br>Photo credit: Whitney Cranshaw, Colorado State University, Bugwood.org</i></figcaption>\n</figure>\t\t\t\t\n\t</div>\n\t\t\t\t\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n    \n<p>Ash and lilac</p>\n\n<h2>Appearance and Life Cycle</h2>\n\n<p>The ash borer requires two years to complete its life cycle. The adult is a clearwing moth that resembles a small wasp. Adults are present from late May to late July and deposit clusters of eggs in bark crevices and in wounds caused by pruning or previous borer attacks. After hatching, the larvae bore into the tree and feed on the inner wood for almost two years. In the spring of the third year the full grown larvae make a tunnel almost to the exterior, leaving only a thin covering of bark. The full grown larvae are white with reddish-brown heads and are 18 to 25 millimetres (mm) long. Just before the adult emerges the pupa twists and turns until it breaks the thin layer of bark. As the adult emerges, it leaves behind a pupal skin protruding through the opening.</p>\n\n<h2>Damage</h2>\n\n<p>The ash borer attacks trees of all ages, but younger, weakened trees are most susceptible. The insect causes damage by making 8 mm diameter tunnels that can extend 100 mm within the wood. Severely-infested trees may contain 50 or more borers that cause weak limbs to break during high winds. Ash borers seldom kill trees directly, but their damage allows other insects and diseases to enter the tree.</p>\n\n<h2>Control</h2>\n\n<p>Wrap burlap around infested trunks to prevent adult emergence and egg laying. Probe active burrows with flexible wire to kill the larvae. Remove and burn heavily infested trees. The insecticide, chlorpyrifos can be applied to the lower 3 metres (m) of the tree in late May and again in late June for control of the ash borer.</p>\n\t\n    </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\r\n    ",
        "fr": "\r\n    \n<p><i>Podosesia syringae</i></p>\n\n<h2>H\u00f4tes</h2>\n\n<div class=\"row\">\n    <div class=\"col-md-4 pull-right\">\n<figure><img alt=\"La description de cette image suit.\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_ash_borer_1.jpg\">\n<br><figcaption><i>Chenille de s\u00e9sie du fr\u00eane et dommages. <br>Photo : James Solomon, <abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr> <span lang=\"en\" xml:lang=\"en\">Forest Service</span>, Bugwood.org</i></figcaption></figure>\t\t\t\t\n\t\t\t\t\n\t\t\t\t<br>\n<figure><img alt=\"La description de cette image suit.\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/pfra_ash_borer_2.jpg\"><br><figcaption><i>Tronc de fr\u00eane infest\u00e9 avec des exuvies nymphales de s\u00e9sie du fr\u00eane d\u00e9passant de leur trou d\u2019\u00e9mergence. <br>Photo : Whitney Cranshaw,  <span lang=\"en\" xml:lang=\"en\">Colorado State University</span>,  Bugwood.org</i></figcaption></figure>\t\t\t\t\t\t\n\t</div>\t\n\t\t\t\t\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n\n<p>Fr\u00eane et lilas</p>\n\n<h2>Aspect et cycle de vie</h2>\n\n<p>La s\u00e9sie du fr\u00eane met deux ans \u00e0 compl\u00e9ter son cycle biologique. L'adulte est un papillon nocturne aux ailes transparentes ressemblant \u00e0 une petite gu\u00eape. Les adultes, pr\u00e9sents de la fin mai \u00e0 la fin juillet, d\u00e9posent des masses d'oeufs dans les crevasses d'\u00e9corce et dans les blessures occasionn\u00e9es par la taille ou d'anciennes attaques d'insectes foreurs. Apr\u00e8s l'\u00e9closion, les larves creusent des tunnels dans l'arbre et se nourrissent du bois de coeur pendant presque deux ans. Au printemps de la troisi\u00e8me ann\u00e9e, les larves adultes creusent un tunnel presque jusqu'\u00e0 l'ext\u00e9rieur, en laissant seulement une mince couverture d'\u00e9corce. Les larves adultes sont blanches, ont une t\u00eate brun-roux et mesurent de 18 \u00e0 25\u00a0millim\u00e8tres (mm) de long. Juste avant l'\u00e9mergence de l'adulte, la nymphe brise la mince couche d'\u00e9corce en se tortillant. \u00c0 l'\u00e9mergence, l'adulte laisse son enveloppe derri\u00e8re lui, en saillie de l'ouverture.</p>\n\n<h2>Dommages</h2>\n\n<p>La s\u00e9sie du fr\u00eane s'attaque aux arbres de tous \u00e2ges, mais les jeunes arbres affaiblis sont les plus vuln\u00e9rables. L'insecte cause des d\u00e9g\u00e2ts en creusant des galeries de 8\u00a0mm de diam\u00e8tre pouvant s'\u00e9tendre jusqu'\u00e0 100\u00a0mm \u00e0 l'int\u00e9rieur du bois. Une forte infestation, estim\u00e9e \u00e0 50 larves et plus par arbre, peut causer un affaiblissement des branches qui se casseront par forts vents. Les s\u00e9sies du fr\u00eane font rarement mourir les arbres, mais leurs dommages constituent une porte d'entr\u00e9e pour d'autres insectes et les maladies.</p>\n\n<h2>Lutte</h2>\n\n<p>Afin de pr\u00e9venir l'\u00e9mergence des adultes et la ponte, enrouler une toile de jute autour des troncs infest\u00e9s. Introduire une tige m\u00e9tallique flexible dans les galeries de larves en activit\u00e9 pour tuer ces derni\u00e8res. Couper et br\u00fbler les arbres fortement infest\u00e9s. Pour lutter contre la s\u00e9sie du fr\u00eane, l'insecticide chlorpyrifos s'applique sur les 3 premiers m\u00e8tres de la base de l'arbre une premi\u00e8re fois \u00e0 la fin mai et une seconde fois \u00e0 la fin juin.</p>\n\n    </div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n\r\n    "
    }
}