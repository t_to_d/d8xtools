{
    "dcr_id": "1327587864375",
    "title": {
        "en": "Sudden oak death \u2013 <i lang=\"la\">Phytophthora ramorum</i>",
        "fr": "Encre des ch\u00eanes rouges \u2013 <i lang=\"la\">Phytophthora ramorum</i>"
    },
    "html_modified": "2024-01-26 2:20:54 PM",
    "modified": "2022-02-11",
    "issued": "2012-01-26 09:24:25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_phyram_index_1327587864375_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_phyram_index_1327587864375_fra"
    },
    "ia_id": "1327587972647",
    "parent_ia_id": "1322807340310",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1322807340310",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Sudden oak death \u2013 <i lang=\"la\">Phytophthora ramorum</i>",
        "fr": "Encre des ch\u00eanes rouges \u2013 <i lang=\"la\">Phytophthora ramorum</i>"
    },
    "label": {
        "en": "Sudden oak death \u2013 <i lang=\"la\">Phytophthora ramorum</i>",
        "fr": "Encre des ch\u00eanes rouges \u2013 <i lang=\"la\">Phytophthora ramorum</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1327587972647",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1322807235798",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/sudden-oak-death/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/encre-des-chenes-rouges/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Sudden oak death \u2013 Phytophthora ramorum",
            "fr": "Encre des ch\u00eanes rouges \u2013 Phytophthora ramorum"
        },
        "description": {
            "en": "Sudden Oak Death is a disease caused by Phytophthora ramorum, a serious fungal pathogen that has killed a large number of oak trees in California.",
            "fr": "L'encre des ch\u00eanes rouges est une maladie caus\u00e9e par phytophthora ramorum, un dangereux pathog\u00e8ne fongique qui a tu\u00e9 un grande nombre de ch\u00eanes en Californie."
        },
        "keywords": {
            "en": "plants, pests, surveillance, national, survey, quarantine, maps, Sudden Oak Death, Phytophthora ramorum",
            "fr": "plante, organisme nuisible, surveillance, national, enqu&#234;te, quarantaine, cartes, encre des ch&#234;nes rouges, Phytophthora ramorum"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-26 09:24:25",
            "fr": "2012-01-26 09:24:25"
        },
        "modified": {
            "en": "2022-02-11",
            "fr": "2022-02-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Sudden oak death \u2013 Phytophthora ramorum",
        "fr": "Encre des ch\u00eanes rouges \u2013 Phytophthora ramorum"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><i lang=\"la\">Phytophthora ramorum</i>, is a fungus-like plant pathogen that causes a disease known as Ramorum Blight and Leafdrop on a wide variety of nursery plants. It has also been associated with a disease of oak, known as \"Sudden Oak Death\" that was first observed in coastal California in the mid-1990's and now occurs as far north as southern Oregon.</p>\n\n<p>The Canadian Food Inspection Agency conducts annual surveys for <i lang=\"la\"><abbr title=\"Phytophthora\">P.</abbr> ramorum</i> and has detected the pathogen on host plants in a number of nurseries in southern coastal British Columbia since 2003.</p>\n\n<p>When <i lang=\"la\"><abbr title=\"Phytophthora\">P.</abbr> ramorum</i> is detected, the nursery is placed under quarantine and all infected plant material is destroyed. Trace forward and trace back investigations are conducted to eliminate the organism from the plants for planting pathway.</p>\n\n<ul>\n<li><a href=\"https://notification.inspection.canada.ca/\"><strong>E-mail subscription list</strong></a></li>\n</ul>\n\n<h2>What information is available</h2>\n\n<h3>Inspection procedures</h3>\n\n<ul>\n<li><a href=\"/plant-health/horticulture/horticulture-manuals/pi-010/eng/1357672417208/1357672483930\">PI-010: Regulatory Response Protocol for Nurseries Confirmed with <i lang=\"la\">Phytophthora ramorum</i></a></li>\n</ul>\n\n<h3>Policy directives</h3>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/imports/d-08-04/eng/1323752901318/1323753560467\">D-08-04: Plant protection import requirements for plants and plant parts for planting</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-02-12/eng/1488215831209/1488215832067\">D-02-12: Phytosanitary import requirements for non-processed wood and other wooden products, bamboo and bamboo products, originating from all areas other than the continental United States</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/d-01-12/eng/1323828428558/1323828505539\">D-01-12: Phytosanitary requirements for the importation and domestic movement of firewood</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-01-01/eng/1323825108375/1323825264324\">D-01-01: Phytosanitary requirements to prevent the entry and spread of <i lang=\"la\">Phytophthora ramorum</i></a></li>\n<li><a href=\"/plant-health/invasive-species/directives/forest-products/d-98-08/eng/1323963831423/1323964135993\">D-98-08: Entry Requirements for wood packaging material into Canada</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/imports/d-96-20/eng/1323854223506/1323854308201\">D-96-20: Canadian Growing Media Program, prior approval process and import requirements for plants rooted in approved media</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/imports/d-95-26/eng/1322520617862/1322525397975\">D-95-26: Phytosanitary requirements for soil and soil-related matter, and for items contaminated with soil and soil-related matter</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-01-01/appendix-2/eng/1363037929822/1363038002856\">Areas regulated for the control of <i lang=\"la\">Phytophthora ramorum</i></a></li>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-01-01/appendix-1/eng/1363039571899/1363039666772\">List of plants regulated for <i lang=\"la\">Phytophthora ramorum</i> (Sudden Oak Death)</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/sudden-oak-death/summary/eng/1327588394994/1327589076065\">Pest Risk Assessment Summary (Kristjansson and Miller, 2009)</a></li>\n<li><i><span lang=\"la\">Phytophthora Ramorum</span> Compensation Regulations</i> [Repealed, SOR/2017-94, s. 24] - 2017-05-19</li>\n</ul>\n\n<h2>Additional government and industry information</h2>\n\n<ul>\n<li><a href=\"http://www.suddenoakdeath.org/\">California Oak Mortality Task Force</a></li>\n<li><a href=\"http://www.cdfa.ca.gov/plant/PE/InteriorExclusion/SuddenOakDeath/index.html\">State of California</a></li>\n<li><a href=\"https://www.oregon.gov/ODA/programs/PlantHealth/Pages/SODProgram.aspx\">State of Oregon</a></li>\n<li><a href=\"https://www.aphis.usda.gov/aphis/ourfocus/planthealth/plant-pest-and-disease-programs/pests-and-diseases/phytophthora-ramorum\">United States Department of Agriculture</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le <i lang=\"la\">Phytophthora ramorum</i> est un champignon phytopathog\u00e8ne qui s'attaque \u00e0 de nombreuses esp\u00e8ces de plantes de p\u00e9pini\u00e8re. Il est \u00e9galement l'agent d'une maladie des ch\u00eanes appel\u00e9e \u00ab\u00a0encre des ch\u00eanes rouges\u00a0\u00bb, observ\u00e9e pour la premi\u00e8re fois dans les r\u00e9gions c\u00f4ti\u00e8res de Californie au milieu des ann\u00e9es 1990 et maintenant confirm\u00e9e jusque dans le sud de l'Oregon.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) m\u00e8ne chaque ann\u00e9e une campagne de d\u00e9pistage du <i lang=\"la\"><abbr title=\"Phytophthora\">P.</abbr> ramorum</i>. La pr\u00e9sence du pathog\u00e8ne a \u00e9t\u00e9 d\u00e9tect\u00e9e sur des plantes h\u00f4tes dans un certain nombre de p\u00e9pini\u00e8res de la c\u00f4te sud de la Colombie-Britannique depuis 2003.</p>\n\n<p>Lorsque le <i lang=\"la\"><abbr title=\"Phytophthora\">P.</abbr> ramorum</i> est d\u00e9tect\u00e9, la p\u00e9pini\u00e8re est mise en quarantaine et tout le mat\u00e9riel v\u00e9g\u00e9tal infect\u00e9 est d\u00e9truit. Des enqu\u00eates de tra\u00e7age en amont et en aval sont conduites pour \u00e9liminer le pathog\u00e8ne de la voie de cheminement que prennent les v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation.</p>\n\n<ul>\n<li><a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\"><strong>Liste de diffusion \u00e9lectronique</strong></a></li>\n</ul>\n\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition</h2>\n\n<h3>Proc\u00e9dures d'inspection</h3>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/horticulture/manuels-sur-l-horticulture/pi-010/fra/1357672417208/1357672483930\">PI-010\u00a0: Protocole d'intervention r\u00e9glementaire visant les p\u00e9pini\u00e8res infest\u00e9es par le <i lang=\"la\">Phytophthora ramorum</i></a></li>\n</ul>\n\n<h3>Directives sur la protection des v\u00e9g\u00e9taux</h3>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-08-04/fra/1323752901318/1323753612811\">D-08-04\u00a0: Exigences phytosanitaire r\u00e9gissant l'importation de v\u00e9g\u00e9taux et de parties de v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-02-12/fra/1488215831209/1488215832067\">D-02-12\u00a0: Exigences phytosanitaires pour l'importation de produits en bois non transform\u00e9 et autres produits de bois, bambou et produits de bambou, en provenance de toutes les r\u00e9gions du monde autres que la zone continentale des \u00c9tats-Unis</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-01-12/fra/1323828428558/1323828547757\">D-01-12\u00a0: Exigences phytosanitaires r\u00e9gissant l'importation et le transport en territoire canadien de bois de chauffage</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-01-01/fra/1323825108375/1323825264324\">D-01-01\u00a0: Exigences phytosanitaires visant \u00e0 pr\u00e9venir l'introduction et la propagation du <i lang=\"la\">Phytophthora ramorum</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-98-08/fra/1323963831423/1323964732223\">D-98-08\u00a0: Exigences relatives \u00e0 l'entr\u00e9e au Canada des mat\u00e9riaux d'emballage en bois</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-96-20/fra/1323854223506/1323854343186\">D-96-20\u00a0: Programme canadien des milieux de culture, processus d'approbation pr\u00e9alable et exigences en mati\u00e8re d'importation de v\u00e9g\u00e9taux enracin\u00e9s dans des milieux de culture approuv\u00e9s</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-95-26/fra/1322520617862/1322525442569\">D-95-26\u00a0: Exigences phytosanitaires s'appliquant \u00e0 la terre et aux mati\u00e8res connexes \u00e0 la terre, ainsi qu'aux articles contamin\u00e9s par de la terre et des mati\u00e8res connexes \u00e0 la terre</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/encre-des-chenes-rouges/sommaire/fra/1327588394994/1327589076065\">Sommaire de l'\u00e9valuation du risque phytosanitaire (Kristjansson et Miller, 2009)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-01-01/annexe-1/fra/1363039571899/1363039666772\">Liste des genres r\u00e9glement\u00e9s \u00e0 l'\u00e9gard du <i lang=\"la\">Phytophthora ramorum</i> (Encre des ch\u00eanes rouges)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-01-01/annexe-2/fra/1363037929822/1363038002856\">R\u00e9gions vis\u00e9es par la directive aux fins de lutte contre le <i lang=\"la\">Phytophthora ramorum</i></a></li>\n<li><i>R\u00e8glement sur l'indemnisation relative au Phytophthora Ramorum</i> [Abrog\u00e9, DORS/2017-94, art. 24] - 2017-05-19</li>\n</ul>\n\n<h2>Autres renseignements du gouvernement et de l'industrie</h2>\n\n<ul>\n<li><a href=\"http://www.suddenoakdeath.org/\"><span lang=\"en\">California Oak Mortality Task Force</span> (en anglais seulement)</a></li>\n<li><a href=\"http://www.cdfa.ca.gov/plant/PE/InteriorExclusion/SuddenOakDeath/index.html\"><span lang=\"en\">State of California</span> (en anglais seulement)</a></li>\n<li><a href=\"https://www.oregon.gov/ODA/programs/PlantHealth/Pages/SODProgram.aspx\"><span lang=\"en\">State of Oregon</span> (en anglais seulement)</a></li>\n<li><a href=\"https://www.aphis.usda.gov/aphis/ourfocus/planthealth/plant-pest-and-disease-programs/pests-and-diseases/phytophthora-ramorum\"><span lang=\"en\">United States Department of Agriculture</span> (en anglais seulement)</a></li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}