{
    "dcr_id": "1375566453693",
    "title": {
        "en": "Animals and Animal Products Derived Through Modern Biotechnology",
        "fr": "Animaux et produits animaux d\u00e9riv\u00e9s de la biotechnologie moderne"
    },
    "html_modified": "2024-01-26 2:21:07 PM",
    "modified": "2017-09-22",
    "issued": "2015-03-31 10:33:27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anima_biotech_index_1375566453693_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anima_biotech_index_1375566453693_fra"
    },
    "ia_id": "1375566502836",
    "parent_ia_id": "1299155693492",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299155693492",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Animals and Animal Products Derived Through Modern Biotechnology",
        "fr": "Animaux et produits animaux d\u00e9riv\u00e9s de la biotechnologie moderne"
    },
    "label": {
        "en": "Animals and Animal Products Derived Through Modern Biotechnology",
        "fr": "Animaux et produits animaux d\u00e9riv\u00e9s de la biotechnologie moderne"
    },
    "templatetype": "content page 1 column",
    "node_id": "1375566502836",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299155513713",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/animal-biotechnology/",
        "fr": "/sante-des-animaux/biotechnologie-animale/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Animals and Animal Products Derived Through Modern Biotechnology",
            "fr": "Animaux et produits animaux d\u00e9riv\u00e9s de la biotechnologie moderne"
        },
        "description": {
            "en": "The term animal biotechnology is an extension of the definition of biotechnology.",
            "fr": "L'expression \u00ab biotechnologie animale \u00bb d\u00e9coule de la d\u00e9finition de la biotechnologie."
        },
        "keywords": {
            "en": "CFIA, animals, animal products, biotechnology",
            "fr": "ACIA, animaux, produits animaux, biotechnologie"
        },
        "dcterms.subject": {
            "en": "inspection",
            "fr": "inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-31 10:33:27",
            "fr": "2015-03-31 10:33:27"
        },
        "modified": {
            "en": "2017-09-22",
            "fr": "2017-09-22"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Animals and Animal Products Derived Through Modern Biotechnology",
        "fr": "Animaux et produits animaux d\u00e9riv\u00e9s de la biotechnologie moderne"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The term \"animal biotechnology\" is an extension of the definition of  biotechnology. This term may include, but is not limited to, the  following categories of animals:</p>\n<ol>\n<li>Genetically engineered or modified animals in which genetic material  has been added, deleted, silenced or altered to influence expression of  genes and traits.</li>\n<li>Clones of animals derived by nuclear transfer from embryonic and somatic cells.</li>\n<li>Chimeric animals that have received transplanted cells from another animal.</li>\n<li>Interspecies hybrids produced by any methods employing biotechnology.</li>\n<li>Animals derived by <span lang=\"la\"><em>in vitro</em></span> cultivation such as maturation or manipulation of embryos.</li>\n</ol> \n\n<ul>\n\n<li><a href=\"https://www.canada.ca/en/health-canada/news/2016/05/health-canada-and-canadian-food-inspection-agency-approve-aquadvantage-salmon.html?_ga=2.119741064.1617568643.1505365662-2077739461.1502341793\">Health Canada and Canadian Food Inspection Agency approve AquAdvantage Salmon</a></li>\n\n<li><a href=\"/animal-health/animal-biotechnology/roles-and-responsibilities/eng/1334783323017/1375568214394\">Roles and Responsibilities of the Government of Canada</a></li>\n</ul>\n\n\n<h2>Additional information</h2>\n\n<ul>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/genetically-modified-foods-other-novel-foods.html\">Genetically Modified (GM) Foods and Other Novel Foods (Health Canada)</a></li>\n\n<li><a href=\"/food-labels/labelling/consumers/genetically-engineered-foods/eng/1333373177199/1333373638071\">Labelling of Genetically Engineered Foods in Canada</a></li>\n<li><a href=\"/animal-health/livestock-feeds/novel-feeds/eng/1370227088259/1370227136675\">Novel Feeds</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/eng/1300137887237/1300137939635\">Plants with Novel Traits</a></li>\n</ul>\n<h3 id=\"gen\">General Information on Biotechnology</h3>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/general-public/regulating-agricultural-biotechnology/eng/1338187581090/1338188593891\">Modern Biotechnology : A Brief Overview </a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/general-public/international-activities/eng/1338618832522/1338618965377\">International Activities</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/general-public/transparency/eng/1338143383841/1338144227355\">Transparency</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'expression \u00ab biotechnologie animale \u00bb d\u00e9coule de la d\u00e9finition de  la biotechnologie. Cette expression englobe, sans s'y limiter, les  cat\u00e9gories d'animaux suivantes\u00a0:</p>\n<ol>\n<li>Les animaux issus du g\u00e9nie g\u00e9n\u00e9tique ou g\u00e9n\u00e9tiquement modifi\u00e9s dans  lesquels du mat\u00e9riel g\u00e9n\u00e9tique a \u00e9t\u00e9 ajout\u00e9, enlev\u00e9, neutralis\u00e9 ou  modifi\u00e9 pour influer sur l'expression de leurs g\u00e8nes et de leur  caract\u00e8re g\u00e9n\u00e9tique.</li>\n<li>Les clones d'animaux issus du transfert du noyau de cellules embryonnaires et somatiques.</li>\n<li>Les animaux chim\u00e9riques dans lesquels on a transplant\u00e9 des cellules provenant d'un autre animal.</li>\n<li>Les hybrides intersp\u00e9cifiques issus de n'importe quelle technique.</li>\n<li>Les animaux issus de techniques de culture <span lang=\"la\"><em>in vitro</em></span>, comme la maturation ou la manipulation d'embryons.</li>\n</ol> \n<ul>\n\n\n<li><a href=\"https://www.canada.ca/fr/sante-canada/nouvelles/2016/05/sante-canada-et-l-agence-canadienne-d-inspection-des-aliments-approuvent-le-saumon-aquadvantage.html\">Sant\u00e9 Canada et l'Agence canadienne d'inspection des aliments approuvent le saumon AquAdvantage</a></li>\n\n\n\n<li><a href=\"/sante-des-animaux/biotechnologie-animale/roles-et-responsabilites/fra/1334783323017/1375568214394\">R\u00f4les et responsabilit\u00e9s du gouvernement du Canada</a></li>\n</ul>\n<h2>Information suppl\u00e9mentaire</h2>\n\n\n<ul>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/aliments-genetiquement-modifies-autres-aliments-nouveaux.html\">Aliments g\u00e9n\u00e9tiquement modifi\u00e9s et autres aliments nouveaux (Sant\u00e9 Canada)</a></li>\n\n\n\n<li><a href=\"/sante-des-animaux/aliments-du-betail/aliments-nouveaux/fra/1370227088259/1370227136675\">Aliments nouveaux du b\u00e9tail</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/consommateurs/aliments-issus-du-genie-genetique/fra/1333373177199/1333373638071\">\u00c9tiquetage des aliments issus du g\u00e9nie g\u00e9n\u00e9tique au Canada</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/fra/1300137887237/1300137939635\">V\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a></li>\n</ul>\n<h3>Renseignements g\u00e9n\u00e9raux sur la biotechnologie</h3>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/activites-internationales/fra/1338618832522/1338618965377\">Activit\u00e9s internationales </a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/reglementation-de-la-biotechnologie-agricole/fra/1338187581090/1338188593891\">Biotechnologie moderne : Un bref aper\u00e7u</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/transparence/fra/1338143383841/1338144227355\">Transparence </a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}