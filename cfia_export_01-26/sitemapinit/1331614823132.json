{
    "dcr_id": "1331614724083",
    "title": {
        "en": "Invasive plants",
        "fr": "Plantes envahissantes"
    },
    "html_modified": "2024-01-26 2:21:03 PM",
    "modified": "2021-12-14",
    "issued": "2012-03-13 00:58:45",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_index_1331614724083_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_index_1331614724083_fra"
    },
    "ia_id": "1331614823132",
    "parent_ia_id": "1306601522570",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1306601522570",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Invasive plants",
        "fr": "Plantes envahissantes"
    },
    "label": {
        "en": "Invasive plants",
        "fr": "Plantes envahissantes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331614823132",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306601411551",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Invasive plants",
            "fr": "Plantes envahissantes"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency's (CFIA) Invasive Plants Program focuses on preventing the introduction and spread of invasive plants in Canada because these species can invade agricultural, horticultural, forestry and natural areas, causing serious damage to our economy and environment.",
            "fr": "Le Programme des plantes envahissantes de l'Agence canadienne d'inspection des aliments (ACIA) se concentre sur Le Programme de lutte contre les v\u00e9g\u00e9taux envahissants de l'ACIA vise \u00e0 pr\u00e9venir l'introduction et la propagation de v\u00e9g\u00e9taux envahissants au Canada, car ces esp\u00e8ces peuvent envahir les zones agricoles, horticoles, foresti\u00e8res et naturelles, causant ainsi d'importants dommages \u00e0 l'\u00e9conomie et \u00e0 l'environnement du Canada."
        },
        "keywords": {
            "en": "invasive, invasive plants, Plant Protection Act",
            "fr": "envahissantes, plantes envahissantes, Loi sur la protection des v\u00e9g\u00e9taux"
        },
        "dcterms.subject": {
            "en": "crops,imports,inspection,plants,weeds",
            "fr": "cultures,importation,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-13 00:58:45",
            "fr": "2012-03-13 00:58:45"
        },
        "modified": {
            "en": "2021-12-14",
            "fr": "2021-12-14"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Invasive plants",
        "fr": "Plantes envahissantes"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>The Canadian Food Inspection Agency's (CFIA) Invasive Plants Program focuses on preventing the introduction and spread of invasive plants in Canada because these species can invade agricultural, horticultural, forestry and natural areas, causing serious damage to our economy and environment.</p>\n\n<p>The following factsheets have been developed to promote public awareness and encourage reporting of potential sightings of these species that have the potential to be invasive in Canada, some of which are regulated under the <i>Plant Protection Act</i>. The invasive plants regulated under the <i>Plant Protection Act</i> are included in the list of <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">Pests regulated by Canada</a>. The invasive plants regulated under the <i>Seeds Act</i> are listed in the <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\">Weed Seeds Order,\u00a02016</a>.</p>\n\n<h2>Regulated</h2>\n\n<p>This list includes quarantine or regulated non-quarantine pests. Regulated non-quarantine pest may already be established in specific areas of Canada. Contact the CFIA if a pest is found in an area where it is currently not known to occur.</p>\n\n<ul>\n\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/chinese-yam/eng/1331732275924/1331732474608\">Chinese yam\u00a0\u2013 <i lang=\"la\">Dioscorea polystachya</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/common-crupina/eng/1331733358077/1331733427684\">Common crupina\u00a0\u2013 <i lang=\"la\">Crupina vulgaris</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/dallis-grass/eng/1331735501639/1331735722968\">Dallis grass\u00a0\u2013 <i lang=\"la\">Paspalum dilatatum</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/devil-s-tail-tearthumb/eng/1331740962114/1331741252346\">Devil's-tail tearthumb\u00a0\u2013 <i lang=\"la\">Persicaria perfoliata</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/giant-reed/eng/1557939393747/1557939394042\">Giant reed\u00a0\u2013 <i lang=\"la\">Arundo donax L.</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/iberian-starthistle/eng/1331743090018/1331743160387\">Iberian starthistle\u00a0\u2013 <i lang=\"la\">Centaurea iberica</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/japanese-stiltgrass/eng/1331745159167/1331745245097\">Japanese stiltgrass\u00a0\u2013 <i lang=\"la\">Microstegium vimineum</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/jointed-goatgrass/eng/1331749027765/1331749093789\">Jointed goatgrass\u00a0\u2013 <i lang=\"la\">Aegilops cylindrica</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/kudzu/eng/1331750489827/1331750551292\">Kudzu\u00a0\u2013 <i lang=\"la\">Pueraria montana</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/paterson-s-curse/eng/1331752933012/1331753015808\">Paterson's curse\u00a0\u2013 <i lang=\"la\">Echium plantagineum</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/serrated-tussock/eng/1331759382936/1331759461668\">Serrated tussock\u00a0\u2013 <i lang=\"la\">Nassella trichotoma</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/silverleaf-nightshade/eng/1331815973077/1331816036718\">Silverleaf nightshade\u00a0\u2013 <i lang=\"la\">Solanum elaeagnifolium</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/slender-foxtail/eng/1331819196984/1331819270890\">Slender foxtail\u00a0\u2013 <i lang=\"la\">Alopecurus myosuroides</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/south-african-ragwort/eng/1331757285388/1331757407583\">South African ragwort\u00a0\u2013 <i lang=\"la\">Senecio inaequidens</i> and Madagascar Ragwort\u00a0\u2013 <i lang=\"la\">Senecio madagascariensis</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/syrian-bean-caper/eng/1331820817035/1331820887749\">Syrian bean-caper\u00a0\u2013 <i lang=\"la\">Zygophyllum fabago</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/woolly-cup-grass/eng/1331822413731/1331822996178\">Woolly cup grass\u00a0\u2013 <i lang=\"la\">Eriochloa villosa</i></a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/yellow-starthistle/eng/1331825936442/1331826040057\">Yellow starthistle\u00a0\u2013 <i lang=\"la\">Centaurea solstitialis</i></a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Le Programme des plantes envahissantes de l'Agence canadienne d'inspection des aliments (ACIA) se concentre sur Le Programme de lutte contre les v\u00e9g\u00e9taux envahissants de l'ACIA vise \u00e0 pr\u00e9venir l'introduction et la propagation de v\u00e9g\u00e9taux envahissants au Canada, car ces esp\u00e8ces peuvent envahir les zones agricoles, horticoles, foresti\u00e8res et naturelles, causant ainsi d'importants dommages \u00e0 l'\u00e9conomie et \u00e0 l'environnement du Canada.</p>\n\n<p>Les fiches d'information suivantes ont \u00e9t\u00e9 \u00e9labor\u00e9es pour sensibiliser le public et encourager le signalement des observations suspectes de ces esp\u00e8ces potentiellement envahissantes au Canada, dont certaines sont r\u00e9glement\u00e9es en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>. Les plantes envahissantes r\u00e9glement\u00e9es en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> sont incluses dans la <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Liste des organismes nuisibles r\u00e9glement\u00e9s par le Canada</a>. Les plantes envahissantes r\u00e9glement\u00e9es en vertu de la <i>Loi sur les semences</i> sont r\u00e9pertori\u00e9es dans le <a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\">Arr\u00eat\u00e9 sur les graines de mauvaises herbes</a>.</p>\n\n<h2>Plantes envahissantes r\u00e9glement\u00e9</h2>\n\n<p>Cette liste comprend les organismes de quarantaine et les organismes r\u00e9glement\u00e9s non de quarantaine. Des organismes r\u00e9glement\u00e9s non de quarantaine peuvent d\u00e9j\u00e0 \u00eatre \u00e9tablis dans certaines r\u00e9gions du Canada. Communiquez avec l'ACIA si un organisme nuisible est trouv\u00e9 dans une zone o\u00f9 sa pr\u00e9sence n'est pas connue.</p>\n\n<ul>\n\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/canne-de-provence/fra/1557939393747/1557939394042\">Canne de Provence\u00a0\u2013 <i lang=\"la\">Arundo donax</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/centaurea-iberica/fra/1331743090018/1331743160387\">Centaurea iberica\u00a0\u2013 <i lang=\"la\">Centaurea iberica</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/centauree-du-solstice/fra/1331825936442/1331826040057\">Centaur\u00e9e du solstice\u00a0\u2013 <i lang=\"la\">Centaurea solstitialis</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/crupine/fra/1331733358077/1331733427684\">Crupine\u00a0\u2013 <i lang=\"la\">Crupina vulgaris</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/egilope-cylindrique/fra/1331749027765/1331749093789\">\u00c9gilope cylindrique\u00a0\u2013 <i lang=\"la\">Aegilops cylindrica</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/eriochloe-velue/fra/1331822413731/1331822996178\">\u00c9riochlo\u00e9 velue\u00a0\u2013 <i lang=\"la\">Eriochloa villosa</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/fabagelle/fra/1331820817035/1331820887749\">Fabagelle\u00a0\u2013 <i lang=\"la\">Zygophyllum fabago</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/herbe-de-dallis/fra/1331735501639/1331735722968\">Herbe de Dallis\u00a0\u2013 <i lang=\"la\">Paspalum dilatatum</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/igname-de-chine/fra/1331732275924/1331732474608\">Igname de Chine\u00a0\u2013 <i lang=\"la\">Dioscorea polystachya</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/kudzu/fra/1331750489827/1331750551292\">Kudzu\u00a0\u2013 <i lang=\"la\">Pueraria montana</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/microstegium-vimineum/fra/1331745159167/1331745245097\">Microst\u00e9gie en osier\u00a0\u2013 <i lang=\"la\">Microstegium vimineum</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/morelle-jaune/fra/1331815973077/1331816036718\">Morelle jaune\u00a0\u2013 <i lang=\"la\">Solanum elaeagnifolium</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/renouee-perfoliee/fra/1331740962114/1331741252346\">Renou\u00e9e perfoli\u00e9e\u00a0\u2013 <i lang=\"la\">Persicaria perfoliata</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/senecon-du-cap/fra/1331757285388/1331757407583\">S\u00e9ne\u00e7on du Cap\u00a0\u2013 <i lang=\"la\">Senecio inaequidens</i> et S\u00e9ne\u00e7on de Madagascar\u00a0\u2013 <i lang=\"la\">Senecio madagascariensis</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/stipe-a-feuilles-dentees/fra/1331759382936/1331759461668\">Stipe \u00e0 feuilles dent\u00e9es\u00a0\u2013 <i lang=\"la\">Nassella trichotoma</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/viperine-a-feuilles-de-plantain/fra/1331752933012/1331753015808\">Vip\u00e9rine \u00e0 feuilles de plantain\u00a0\u2013 <i lang=\"la\">Echium plantagineum</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/vulpin-des-champs/fra/1331819196984/1331819270890\">Vulpin des champs\u00a0\u2013 <i lang=\"la\">Alopecurus myosuroides</i></a></li>\n</ul>\n\n\n\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}