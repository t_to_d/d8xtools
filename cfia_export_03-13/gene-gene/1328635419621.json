{
    "dcr_id": "1328635419621",
    "title": {
        "en": "VB-GL-3.23: Licensing veterinary nucleic acid vaccines",
        "fr": "LD-PBV-3.23 : Homologation de vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques"
    },
    "html_modified": "2024-03-12 3:55:35 PM",
    "modified": "2023-10-30",
    "issued": "2012-02-07 12:23:44",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/vetbio_guide_323_1328635419621_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/vetbio_guide_323_1328635419621_fra"
    },
    "ia_id": "1328635555931",
    "parent_ia_id": "1320704254070",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Guidelines",
        "fr": "Lignes directrices"
    },
    "commodity": {
        "en": "Veterinary Biologics",
        "fr": "Produits biologiques v\u00e9t\u00e9rinaires"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1320704254070",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "VB-GL-3.23: Licensing veterinary nucleic acid vaccines",
        "fr": "LD-PBV-3.23 : Homologation de vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques"
    },
    "label": {
        "en": "VB-GL-3.23: Licensing veterinary nucleic acid vaccines",
        "fr": "LD-PBV-3.23 : Homologation de vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1328635555931",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299160285341",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/veterinary-biologics/guidelines-forms/vb-gl-3-23/",
        "fr": "/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/ld-pbv-3-23/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "VB-GL-3.23: Licensing veterinary nucleic acid vaccines",
            "fr": "LD-PBV-3.23 : Homologation de vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques"
        },
        "description": {
            "en": "Nucleic acid vaccines, like other biotechnology derived vaccines, will be evaluated on a case-by-case basis. Evaluation of vaccine master seeds is an essential first step for fulfilling these requirements.",
            "fr": "Les vaccins \u00e0 base d'acides nucl\u00e9iques, comme d'autres vaccins issus de la biotechnologie, seront \u00e9valu\u00e9s \u00e0 la pi\u00e8ce. L'\u00e9valuation des souches m\u00e8res de vaccins est une premi\u00e8re \u00e9tape essentielle vers la satisfaction de ces exigences."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, veterinary biologics, licensing requirements, guidelines, nucleic acid vaccines, master seeds, environmental assessment",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glements sur la sant\u00e9 des animaux, produits biologiques v\u00e9t\u00e9rinaires, d\u00e9livrance des permis, lignes directrices, vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques, souches m\u00e8res, \u00e9valuation environnementale"
        },
        "dcterms.subject": {
            "en": "inspection,veterinary medicine,standards,regulation,animal health",
            "fr": "inspection,m\u00e9decine v\u00e9t\u00e9rinaire,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-07 12:23:44",
            "fr": "2012-02-07 12:23:44"
        },
        "modified": {
            "en": "2023-10-30",
            "fr": "2023-10-30"
        },
        "type": {
            "en": "reference material,standard,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,norme,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "VB-GL-3.23: Licensing veterinary nucleic acid vaccines",
        "fr": "LD-PBV-3.23 : Homologation de vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=29#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2>1. Introduction</h2>\n \n<p>The purpose of this document is to inform veterinary biologics (VB) researchers, manufacturers and Canadian importers of the requirements for veterinary nucleic acid vaccines. The Canadian Centre for Veterinary Biologics (CCVB) of the Canadian Food Inspection Agency (CFIA) is responsible for regulating the manufacturing, testing, importation and use of VB in Canada, including veterinary nucleic acid vaccines.</p>\n\n<p>The primary requirements for licensing nucleic acid vaccines are similar to any other veterinary vaccines in that they both require the fulfilment of 4\u00a0criteria\u00a0- purity, potency, safety and efficacy. Nucleic acid vaccines, like other biotechnology derived vaccines, will be evaluated on a case-by-case basis.</p>\n\n<h3>1.1\u00a0Legal authority</h3>\n \n<p><a href=\"/english/reg/jredirect2.shtml?heasana\"><i>Health of Animals Act</i></a></p>\n\n<p><a href=\"https://laws-lois.justice.gc.ca/eng/regulations/C.R.C.%2C_c._296/page-8.html#h-547833\"><i>Health of Animals Regulations</i>, Part\u00a0XI</a></p>\n \n<h2>2. Master seeds</h2>\n\n<p>The master seeds used for the preparation of VB must be demonstrated to be pure, safe and immunogenic. For nucleic acid vaccines, master seeds can be defined as a clone of cells produced from a single prokaryotic cell containing the desired plasmid construct. The manufacturers are required to provide information on the sources and identity, methods of propagation, and molecular and biological characteristics of the master seed. The identity of the master seed can be determined by microbiological, biochemical and immunological methods. The molecular information required should include sequence information on the inserted genes and flanking regions, and restriction maps of regulatory elements including genetic markers. Information on the plasmid copy number per bacterial cell is required. The biological characteristics of the master seed should include information on growth characteristics, environmental distribution, genetic stability, phenotypic stability and data on animal pathogenicity. All master seeds must be shown to be free from extraneous agents and be genetically and phenotypically stable beyond the number of passages used for vaccine production.</p>\n\n<h2>3. Purity, potency, and efficacy</h2>\n\n<p>In the purity assessment of nucleic acid vaccines, both biochemical and microbiological purity should be considered. The final purified plasmid product may contain very low levels of microbial by products such as protein, chromosomal\u00a0DNA\u00a0and endotoxin; and residual material from processing and purification steps. While it is desirable to reduce such contaminants as much as practically possible, the level of biochemical purity for the vaccine is likely to be determined by safety considerations related to the species and the intended use of the product. Microbiological purity is determined by tests for contaminating bacteria and fungi in the finished product.</p>\n\n<p>Both <i>in vivo</i> (measurement of immunological responses or protection following animal inoculation) and <i>in\u00a0vitro</i> methods (for example,\u00a0detection of the expressed gene product following transfection of mammalian cells) can contribute for potency evaluation of nucleic acid vaccines. The potency test should correlate to the immunogenicity and efficacy of the vaccine.</p>\n\n<p>A vaccination-challenge study conducted in target animals is the standard requirement for nucleic acid vaccine efficacy assessment. Efficacy studies measure the specific protective capacity of the vaccine when used in the final formulation. Label recommendations for use of the product are based on the results of the efficacy study. Manufacturers should submit efficacy trial protocols for review to the CCVB prior to initiating efficacy studies to generate data for licensure.</p>\n\n<h2>4. Safety</h2>\n\n<p>Similar to other biotechnology-derived vaccines, a broader definition of safety that includes specific examination of animal, human, and environmental safety is the standard requirement for the evaluation of nucleic acid vaccines. Target animal safety is evaluated in both laboratory and field safety studies. Consideration of non-target animal safety may be required depending on the intended use, target species, and environment. Local and systemic reactions in animals to vaccination are monitored using predetermined criteria during the conduct of these studies.</p>\n\n<p>For nucleic acid vaccines, integration of injected plasmids into host genome, germline transmission and adverse immunological sequelae such as autoimmunity and immune-tolerance have been suggested as potential safety considerations. It is important that the CCVB assess the risk for each of these considerations in the proper context and formulate test requirements for nucleic acid vaccines.</p>\n\n<h2>5. Risk analysis</h2>\n\n<p>The licensing decisions for nucleic acid vaccines, as for other biotechnology derived vaccines, will be based on risk analysis. The essential components of risk analysis are risk assessment, risk management and risk communication. Risk assessment, which is the process of identifying a hazard and characterizing or estimating the risk presented by that hazard, is performed using qualitative and quantitative methods. The CFIA\u00a0has started using 'scenario tree analysis' to predict the likelihood of various outcomes and their respective impacts in the development of licensing decisions.</p>\n\n<h2>6. Environmental assessment</h2>\n\n<p>The environmental assessment (EA) contains information on the molecular and biological characteristics of the biotechnology-derived organism, target animal and non-target animal safety, human safety, environmental considerations and risk mitigation measures. The manufacturer must submit all relevant data to aid the CCVB reviewer in preparing the EA. The CCVB reviewer will also independently research any potential safety issues, and may consult other federal and provincial government departments with expertise in areas of concern. For instance, potential human health and safety risks may be assessed in collaboration with Health Canada and the Public Health Agency of Canada. An EA must be completed before the CCVB will authorize the release of a novel organism into the Canadian environment.</p>\n\n<p>The scheduling of the <i>Health of Animals Act</i> and <i>Health of Animals Regulations</i> under Schedule 4 of the <i>Canadian Environmental Protection Act</i>, 1999 (CEPA 1999) exempts new biotechnology-derived VB regulated by the CCVB from additional notification and assessment under the CEPA 1999. This scheduling is recognition that the EA authorities within the <i>Health of Animals Act</i> and <i>Health of Animals Regulations</i> provide an adequate level of regulatory control. Consequently, the CCVB is fully responsible for the environmental, human health and animal health assessment of a novel VB product.</p>\n\n<p>After the CCVB has completed the draft EA for a biotechnology-derived VB, the manufacturer is provided a copy of the document and given the opportunity to identify any confidential business information. A publicly available bilingual version of the EA, which omits this confidential business information, is subsequently posted on the CFIA website, where it is accessible to the general public.</p>\n\n<ul>\n<li><a href=\"/animal-health/veterinary-biologics/environmental-assessments/eng/1318464704520/1320704752007\">Environmental assessments</a></li>\n<li><a href=\"/animal-health/veterinary-biologics/guidelines-forms/environmental-assessment/eng/1686072706592/1686072707406\">Environmental assessment template</a></li>\n</ul>\n\n<h2>7. Additional information</h2>\n\n<p><a href=\"/animal-health/veterinary-biologics/contacts/eng/1299162041191/1320704114965\">Contact the CCVB</a> if you have additional questions or need clarification on regulations and requirements for veterinary nucleic acid vaccines.</p>\n\n<p><a href=\"/animal-health/veterinary-biologics/guidelines-forms/eng/1299160285341/1320704254070\">Guidelines and forms</a> are available on the\u00a0CCVB\u00a0website.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=29#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2>1. Introduction</h2>\n\n<p>L'objectif de ce document est d'informer les chercheurs, fabricants et importateurs de produits biologiques v\u00e9t\u00e9rinaires (PBV) des exigences relatives aux vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques. Le Centre canadien des produits biologiques v\u00e9t\u00e9rinaires (CCPBV) de l'Agence canadienne d'inspection des aliments (ACIA) est charg\u00e9 de r\u00e9glementer la fabrication, l'analyse, l'importation et l'utilisation des PBV au Canada, y compris les vaccins v\u00e9t\u00e9rinaires \u00e0 base d'acides nucl\u00e9iques.</p>\n\n<p>Les principales exigences relatives \u00e0 l'homologation des vaccins \u00e0 base d'acides nucl\u00e9iques ressemblent \u00e0 celles de tout autre vaccin v\u00e9t\u00e9rinaire du fait qu'ils doivent se conformer \u00e0 4 crit\u00e8res, soit la puret\u00e9, l'activit\u00e9, l'innocuit\u00e9 et l'efficacit\u00e9. Les vaccins \u00e0 base d'acides nucl\u00e9iques, comme d'autres vaccins issus de la biotechnologie, seront \u00e9valu\u00e9s \u00e0 la pi\u00e8ce.</p>\n\n<h3>1.1\u00a0Fondement juridique</h3>\n\n<p><a href=\"/francais/reg/jredirect2.shtml?heasana\"><i>Loi sur la sant\u00e9 des animaux</i></a></p>\n\n<p><a href=\"https://laws-lois.justice.gc.ca/fra/reglements/C.R.C.%2C_ch._296/page-8.html#h-536489\"><i>R\u00e8glement sur la sant\u00e9 des animaux</i>, partie\u00a0XI</a></p>\n\n<h2>2. Souches m\u00e8res</h2>\n\n<p>Les souches m\u00e8res utilis\u00e9es dans la pr\u00e9paration de PBV doivent s'av\u00e9rer pures, s\u00fbres et immunog\u00e8nes. Pour les vaccins \u00e0 base d'acides nucl\u00e9iques, les souches m\u00e8res d\u00e9signent un clone de cellules produites \u00e0 partir d'un seul procaryote contenant le plasmide souhait\u00e9. Les fabricants sont tenus de fournir de l'information sur les sources et l'identit\u00e9, les m\u00e9thodes de multiplication, ainsi que les caract\u00e9ristiques mol\u00e9culaires et biologiques de la souche m\u00e8re. On peut d\u00e9terminer l'identit\u00e9 de la souche m\u00e8re par des m\u00e9thodes microbiologiques, biochimiques et immunologiques. L'information mol\u00e9culaire requise doit comprendre de l'information sur les g\u00e8nes d'insertion et les r\u00e9gions flanquantes, ainsi que des cartes de restriction d'\u00e9l\u00e9ments r\u00e9gulateurs, notamment les marqueurs g\u00e9n\u00e9tiques. Des donn\u00e9es sur le nombre de copies de plasmides par cellule bact\u00e9rienne sont requises. Les caract\u00e9ristiques biologiques de la souche m\u00e8re doivent comprendre des donn\u00e9es sur les caract\u00e9ristiques de croissance, la distribution environnementale, la stabilit\u00e9 g\u00e9n\u00e9tique, la stabilit\u00e9 ph\u00e9notypique et de l'information sur la pathog\u00e9n\u00e9cit\u00e9 animale. Toutes les souches m\u00e8res doivent s'av\u00e9rer exemptes d'agents \u00e9trangers et \u00eatre g\u00e9n\u00e9tiquement et ph\u00e9notypiquement stables au-del\u00e0 du nombre de passages n\u00e9cessaires \u00e0 la production du vaccin.</p>\n\n<h2>3. Puret\u00e9, activit\u00e9, et efficacit\u00e9</h2>\n\n<p>Dans l'\u00e9valuation de la puret\u00e9 des vaccins \u00e0 base d'acides nucl\u00e9iques, il faut tenir compte de la puret\u00e9 biochimique et microbiologique. Le produit plasmidique de derni\u00e8re purification peut contenir de tr\u00e8s faibles concentrations de sous-produits microbiens comme la prot\u00e9ine, l'ADN\u00a0chromosomique et l'endotoxine, ainsi que des mati\u00e8res r\u00e9siduelles provenant des \u00e9tapes de la transformation et de la purification. Comme il faut r\u00e9duire ces contaminants le plus possible, le niveau de puret\u00e9 biochimique du vaccin devrait \u00eatre d\u00e9termin\u00e9 par des facteurs d'innocuit\u00e9 relatifs \u00e0 l'esp\u00e8ce animale et l'utilisation pr\u00e9vue du produit. La puret\u00e9 microbiologique est d\u00e9termin\u00e9e par des \u00e9preuves pour la pr\u00e9sences de bact\u00e9ries et de fongus contaminants dans le produit fini.</p>\n\n<p>Des m\u00e9thodes <i>in vivo</i> (mesure de r\u00e9actions ou de la protection immunologiques apr\u00e8s inoculation de l'animal) et <i>in vivo</i> (par exemple, d\u00e9tection du produit hygi\u00e9nique exprim\u00e9 apr\u00e8s transfection de cellules mammaliennes) peuvent contribuer \u00e0 l'\u00e9valuation des vaccins \u00e0 base d'acides nucl\u00e9iques. L'\u00e9preuve d'activit\u00e9 devra \u00eatre corr\u00e9l\u00e9e \u00e0 l'immunog\u00e9nicit\u00e9 et \u00e0 l'efficacit\u00e9 du vaccin.</p>\n\n<p>Une \u00e9tude de provocation par vaccination, effectu\u00e9e chez certains animaux cibl\u00e9s, est la norme \u00e9talon permettant d'\u00e9valuer l'efficacit\u00e9 des vaccins \u00e0 base d'acides nucl\u00e9iques. Les \u00e9tudes d'efficacit\u00e9 mesurent la capacit\u00e9 protectrice sp\u00e9cifique du vaccin utilis\u00e9 dans sa formulation d\u00e9finitive. Les recommandations sur l'utilisation du produit figurant sur l'\u00e9tiquette s'appuient sur les r\u00e9sultats de l'\u00e9tude d'efficacit\u00e9. Les fabricants doivent pr\u00e9senter des protocoles d'essai d'efficacit\u00e9 pour examen par le CCPBV avant d'entreprendre des \u00e9tudes d'efficacit\u00e9 visant \u00e0 produire des donn\u00e9es pour les permis d'exercice.</p>\n\n<h2>4. Innocuit\u00e9</h2>\n\n<p>Comme pour d'autres vaccins issus de la biotechnologie, une d\u00e9finition plus large du terme \u00ab\u00a0innocuit\u00e9\u00a0\u00bb, qui comprend l'examen sp\u00e9cifique du risque pour les animaux, les humains et l'environnement, est l'exigence standard pour l'\u00e9valuation des vaccins \u00e0 base d'acides nucl\u00e9iques. L'innocuit\u00e9 \u00e0 l'\u00e9gard des animaux vis\u00e9s est \u00e9valu\u00e9e en laboratoire et sur le terrain. La prise en compte de l'innocuit\u00e9 \u00e0 l'\u00e9gard des animaux non-vis\u00e9s peut \u00eatre n\u00e9cessaire en fonction de l'utilisation pr\u00e9vue, de l'esp\u00e8ce vis\u00e9e et de l'environnement. Les r\u00e9actions locales et syst\u00e9miques des animaux \u00e0 la vaccination sont suivies au moyen de crit\u00e8res pr\u00e9d\u00e9termin\u00e9s au cours de ces \u00e9tudes.</p>\n\n<p>Pour les vaccins \u00e0 base d'acides nucl\u00e9iques, l'int\u00e9gration de plasmides inject\u00e9es dans le g\u00e9nome h\u00f4te, la transmission de lign\u00e9es germinales et les s\u00e9quelles immunologiques nuisibles, comme l'auto-immunit\u00e9 et la tol\u00e9rance immunitaire, ont \u00e9t\u00e9 propos\u00e9es comme facteurs d'innocuit\u00e9 \u00e9ventuels. Il importe que le CCPBV\u00a0\u00e9value le risque de chacun de ces facteurs dans le contexte appropri\u00e9 et formule des exigences d'analyse pour les vaccins \u00e0 base d'acides nucl\u00e9iques.</p>\n\n<h2>5. Analyse des risques</h2>\n\n<p>Les d\u00e9cisions d'homologation de vaccins \u00e0 base d'acides nucl\u00e9iques, comme pour d'autres vaccins issus de la biotechnologie, s'appuieront sur une analyse des risques. Les \u00e9l\u00e9ments essentiels de l'analyse des risques sont l'\u00e9valuation, la gestion et la communication des risques. L'\u00e9valuation des risques, qui est le processus de d\u00e9termination du danger et de caract\u00e9risation ou d'estimation du risque que pr\u00e9sente ce danger, est effectu\u00e9e au moyen de m\u00e9thodes qualitatives et quantitatives. L'ACIA\u00a0a commenc\u00e9 \u00e0 utiliser une \u00ab\u00a0analyse arborescente par sc\u00e9nario\u00a0\u00bb pour pr\u00e9dire la probabilit\u00e9 de divers r\u00e9sultats et de leurs r\u00e9percussions respectives sur la prise de d\u00e9cisions en mati\u00e8re d'homologation.</p>\n\n<h2>6. \u00c9valuation environnementale</h2>\n\n<p>L'\u00e9valuation environnementale (\u00c9E) contient des renseignements sur les caract\u00e9ristiques mol\u00e9culaires et biologiques de l'organisme issu de la biotechnologie, son innocuit\u00e9 pour les animaux vis\u00e9s et non vis\u00e9s, son innocuit\u00e9 pour l'humain, diverses consid\u00e9rations d'ordre environnemental et les mesures d'att\u00e9nuation des risques. Le fabricant doit pr\u00e9senter toutes les donn\u00e9es pertinentes pour aider l'\u00e9valuateur du CCPBV \u00e0 pr\u00e9parer l'\u00c9E. Ce dernier m\u00e8nera aussi des recherches de fa\u00e7on ind\u00e9pendante sur des probl\u00e8mes possibles d'innocuit\u00e9, et il peut consulter d'autres minist\u00e8res f\u00e9d\u00e9raux et provinciaux qui poss\u00e8dent une expertise dans des domaines d'int\u00e9r\u00eat. Par exemple, les risques potentiels pour la sant\u00e9 et l'innocuit\u00e9 humaines peuvent \u00eatre \u00e9valu\u00e9s en collaboration avec Sant\u00e9 Canada et l'Agence de la sant\u00e9 publique du Canada. Une \u00c9E doit \u00eatre termin\u00e9e avant que le CCPBV n'autorise la diss\u00e9mination d'un nouvel organisme dans l'environnement canadien.</p>\n\n<p>L'inscription de la <i>Loi sur la sant\u00e9 des animaux</i> et du <i>R\u00e8glement sur la sant\u00e9 des animaux</i> sur la liste de l'annexe 4 de la <i>Loi canadienne sur la protection de l'environnement</i> (1999) (LCPE 1999) exempte les nouveaux PBV issus de la biotechnologie qui sont r\u00e9glement\u00e9s par le CCPBV de toute notification et \u00e9valuation suppl\u00e9mentaires en vertu de la LCPE 1999. Ces inscriptions ont pour but de reconnaitre que les pouvoirs en mati\u00e8re d'\u00c9E conf\u00e9r\u00e9s en vertu de la <i>Loi sur la sant\u00e9 des animaux</i> et du <i>R\u00e8glement sur la sant\u00e9 des animaux</i> fournissent un niveau ad\u00e9quat de mesures de contr\u00f4le. Par cons\u00e9quent, le CCPBV est enti\u00e8rement responsable de l'\u00e9valuation des risques du nouveau PBV pour la sant\u00e9 humaine et animale et pour l'environnement.</p>\n\n<p>Une fois que le CCPBV aura termin\u00e9 l'\u00c9E pr\u00e9liminaire pour un PBV issu de la biotechnologie, le fabricant re\u00e7oit une copie du document et on l'invite \u00e0 identifier tout renseignement commercial confidentiel. Une version publique bilingue de l'\u00c9E, sans ces renseignements confidentiels, est ensuite publi\u00e9e sur le site Web de l'ACIA, o\u00f9 elle est accessible au grand public.</p>\n\n<ul>\n<li><a href=\"/sante-des-animaux/produits-biologiques-veterinaires/evaluations-environnementales/fra/1318464704520/1320704752007\">\u00c9valuations environnementales</a> </li>\n<li><a href=\"/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/evaluation-environnementale/fra/1686072706592/1686072707406\">Gabarit\u00a0- \u00c9valuation environnementale</a></li>\n</ul>\n\n<h2>7.\u00a0Renseignements suppl\u00e9mentaires</h2>\n\n<p>Veuillez adresser toute question ou demande de clarification quant \u00e0 la r\u00e9glementation et aux exigences relatives aux vaccins \u00e0 base d'acides nucl\u00e9iques <a href=\"/sante-des-animaux/produits-biologiques-veterinaires/personnes-ressources/fra/1299162041191/1320704114965\">aupr\u00e8s du CCPBV</a>.</p>\n\n<p><a href=\"/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/fra/1299160285341/1320704254070\">Les lignes directrices et les formulaires</a> sont disponibles sur le site Web du\u00a0CCPBV.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}