{
    "dcr_id": "1528486939821",
    "title": {
        "en": "Consistent and efficient inspections",
        "fr": "Les inspections uniformes et efficaces"
    },
    "html_modified": "2024-03-12 3:58:05 PM",
    "modified": "2019-01-31",
    "issued": "2018-06-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_chricle_article7_1528486939821_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_chricle_article7_1528486939821_fra"
    },
    "ia_id": "1528486940070",
    "parent_ia_id": "1565297581210",
    "chronicletopic": {
        "en": "Food Safety",
        "fr": "Salubrit\u00e9 des Aliments"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565297581210",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Consistent and efficient inspections",
        "fr": "Les inspections uniformes et efficaces"
    },
    "label": {
        "en": "Consistent and efficient inspections",
        "fr": "Les inspections uniformes et efficaces"
    },
    "templatetype": "content page 1 column",
    "node_id": "1528486940070",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565297580961",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/food-safety/consistent-and-efficient-inspections/",
        "fr": "/inspecter-et-proteger/salubrite-des-aliments/les-inspections-uniformes-et-efficaces/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Consistent and efficient inspections",
            "fr": "Les inspections uniformes et efficaces"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) recognizes a good regulatory system is one that is consistent and efficient. This is why it is replacing 14 sets of existing regulations with one which streamlines food safety requirements across sectors.",
            "fr": "RSAC - mythes et r\u00e9alit\u00e9 : Le RSAC entrera en vigueur le 15 janvier 2019. Les entreprises alimentaires auront le temps de se familiariser avec le r\u00e8glement d\u00e9finitif et de se pr\u00e9parer \u00e0 r\u00e9pondre aux nouvelles exigences \u2013 y compris la d\u00e9livrance des licences, la tra\u00e7abilit\u00e9 et les mesures de contr\u00f4le pr\u00e9ventif."
        },
        "keywords": {
            "en": "Safe Food for Canadians Regulations, SFCR, slaughter, transportation, controls, animal welfare, humane, Consistent and Efficient Inspections, backgrounder",
            "fr": "R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RACF, transport, contr\u00f4les, bien-\u00eatre des animaux, cruaut\u00e9, abattage, mythes et r\u00e9alit\u00e9"
        },
        "dcterms.subject": {
            "en": "food,agri-food industry,food inspection,agri-food products,regulations,food safety",
            "fr": "aliment,industrie agro-alimentaire,inspection des aliments,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2019-01-31",
            "fr": "2019-01-31"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Consistent and efficient inspections",
        "fr": "Les inspections uniformes et efficaces"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n    \n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\"> \n<img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_chricle_article_7_1528746359988_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"A Canadian Food Inspection Agency inspector is standing beside a grocery shelf with a tablet in her hand.\">\n</div>\n\n<p>The Canadian Food Inspection Agency (CFIA) recognizes a good regulatory system as one that is consistent and efficient. This is why it replaced 14 sets of existing regulations with one which streamlines food safety requirements across sectors. The <i>Safe Food for Canadians Regulations</i> (SFCR) introduces consistent standards across the food industry in Canada, which helps to ensure food safety along the entire supply chain.</p> \n\n<p>In order to ensure this legislation is applied nationally in a fair, consistent and predictable manner across industry, CFIA has made changes to its food inspection procedures. The implementation of a Standard Inspection Procedure, or SIP, has been implemented across food commodities.</p>\n\n<p><abbr title=\"Standard Inspection Procedure\">SIP</abbr> is a contemporary approach to inspections that represents a fundamental shift in our regulatory approach; with less emphasis on prescriptive-based requirements and more focus on safety outcomes. This means a stronger food safety system which enables industry to innovate and respond to emerging risks and developments. The Agency is making improvements in a number of areas to ensure regulated parties can be confident that inspections are being carried out consistently and efficiently.</p>\n\n<ul class=\"lst-spcd\">\n<li>Firstly, all <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> inspection staff have received the same training in how to implement the components of <abbr title=\"Standard Inspection Procedure\">SIP</abbr>, and how to use the new suite of operational and interpretive guidance. Interpretive guidance enables industry and inspectors to share a common understanding of what meeting the regulatory requirements looks like. To further support consistency, inspection staff across the country has access to peer support and subject matter experts when they have questions.</li>\n<li>Secondly, our inspectors are carrying new digital tools that give them better connectivity and online access to guidance and historical inspection records. A new Digital Service Delivery Platform (DSDP) allows inspectors to record findings, complete inspection reports, record images and share information with stakeholders while fully mobile. This increases the efficiency of inspections.</li>\n<li>Lastly, part of a consistent and efficient approach to inspection is to focus inspection resources on the areas of highest risk. <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is taking a risk-based planning approach which analyzes data collected by inspectors and assigns a level of risk to a regulated establishment. This impacts how often an inspector will conduct their inspections and where they will be focusing their efforts.</li>\n</ul>\n\n<p>Stakeholders and Canadians can be assured that as the Agency introduces new regulations, procedures and program changes, the inspectorate is well prepared to carry out their duties in a consistent and efficient manner.</p> \n\n<p>To learn more about the <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr>, visit our <a href=\"/food-safety-for-industry/toolkit-for-food-businesses/eng/1427299500843/1427299800380\">tools, information and resources</a>.</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n    \n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\"> \n<img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_chricle_article_7_1528746359988_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"Inspectrice de l'\u2019Agence canadienne d'inspection des aliments debout devant une \u00e9tag\u00e8re d'\u00e9picerie, une tablette \u00e0 la main.\">\n</div>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) reconna\u00eet qu'un bon syst\u00e8me de r\u00e9glementation se doit d'\u00eatre uniforme et efficace. Voil\u00e0 pourquoi elle a remplac\u00e9 14 r\u00e8glements existants par un seul simplifiant les exigences en mati\u00e8re de salubrit\u00e9 des aliments dans tous les secteurs. Le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC) vise la mise en place de normes uniformes \u00e0 l'\u00e9chelle de l'industrie alimentaire au Canada, lesquelles aident \u00e0 assurer la salubrit\u00e9 des aliments tout au long de la cha\u00eene d'approvisionnement.</p>\n<p>Afin que cette l\u00e9gislation soit appliqu\u00e9e de mani\u00e8re juste, uniforme et pr\u00e9visible \u00e0 l'\u00e9chelle de l'industrie canadienne, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a modifi\u00e9e ses proc\u00e9dures d'inspection des aliments. Une Proc\u00e9dure d'inspection normalis\u00e9e (PIN) a \u00e9t\u00e9 mise en \u0153uvre pour tous les produits alimentaires.</p>\n<p>Une <abbr title=\"Proc\u00e9dure d'inspection normalis\u00e9e\">PIN</abbr> est une approche contemporaine \u00e0 l'\u00e9gard des inspections qui repr\u00e9sente un changement de cap fondamental dans notre approche de r\u00e9glementation. En effet, l'accent est davantage mis sur les r\u00e9sultats relatifs \u00e0 la salubrit\u00e9 plut\u00f4t que sur les exigences prescriptives. Cela se traduit par un syst\u00e8me de l'assurance de la salubrit\u00e9 des aliments plus fort gr\u00e2ce auquel les entreprises auront davantage de possibilit\u00e9s d'innover et d'intervenir en cas de risques \u00e9mergents et de faits nouveaux.</p>\n<p>L'Agence apporte des am\u00e9liorations dans de nombreux domaines afin de veiller \u00e0 ce que les parties r\u00e9glement\u00e9es puissent \u00eatre assur\u00e9es que les inspections sont r\u00e9alis\u00e9es uniform\u00e9ment et efficacement.</p>\n<ul class=\"lst-spcd\">\n<li>D'abord, tout le personnel d'inspection de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a re\u00e7u la m\u00eame formation sur la fa\u00e7on de mettre en \u0153uvre les composantes de la <abbr title=\"Proc\u00e9dure d'inspection normalis\u00e9e\">PIN</abbr> et d'utiliser le nouvel ensemble de directives op\u00e9rationnelles et d'interpr\u00e9tation. Les directives d'interpr\u00e9tation permettent aux intervenants de l'industrie et aux inspecteurs d'arriver \u00e0 une compr\u00e9hension commune de ce \u00e0 quoi ressemble le respect des exigences r\u00e9glementaires. Pour favoriser davantage l'uniformit\u00e9, les membres du personnel d'inspection de l'ensemble du pays ont acc\u00e8s au soutien par les pairs ainsi qu'\u00e0 des experts en la mati\u00e8re lorsqu'ils ont des questions.</li>\n<li>De plus, nos inspecteurs ont \u00e9quip\u00e9s de nouveaux outils num\u00e9riques qui leur offriront une meilleure connectivit\u00e9 et un acc\u00e8s en ligne aux directives et aux registres d'inspection historiques. Une nouvelle Plateforme de prestation num\u00e9rique des services permet aux inspecteurs de consigner les constatations, de remplir des rapports d'inspection, d'enregistrer des images et de communiquer des renseignements aux intervenants tout en jouissant d'une mobilit\u00e9 compl\u00e8te. Cela augmente l'efficacit\u00e9 des inspections.</li>\n<li>Enfin, une approche d'inspection uniforme et efficace consiste, entre autres, \u00e0 concentrer les ressources d'inspection dans les domaines pr\u00e9sentant les risques les plus \u00e9lev\u00e9s. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> adopte une approche de planification fond\u00e9e sur les risques qui visera \u00e0 analyser les donn\u00e9es recueillies par les inspecteurs puis \u00e0 attribuer un niveau de risque \u00e0 un \u00e9tablissement r\u00e9glement\u00e9. Cela a une incidence sur la fr\u00e9quence \u00e0 laquelle les inspecteurs r\u00e9aliseront leurs inspections et sur les domaines o\u00f9 ils concentreront leurs efforts.</li>\n</ul>\n<p>Les intervenants et les Canadiens peuvent \u00eatre assur\u00e9s que les membres du personnel d'inspection sont pr\u00eats \u00e0 s'acquitter de leurs fonctions de fa\u00e7on uniforme et efficace \u00e0 mesure que l'Agence adopte un nouveau r\u00e8glement, de nouvelles proc\u00e9dures et de nouveaux changements aux programmes.</p>\n\n<p>Pour apprendre davantage sur le <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr>, visitez nos <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/fra/1427299500843/1427299800380\">outils, renseignements et ressources</a>.</p>\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}