{
    "dcr_id": "1377401804805",
    "title": {
        "en": "CVB / CFIA roles and responsibilities with respect to the Seed Program",
        "fr": "R\u00f4les et responsabilit\u00e9s de l'OVC / ACIA relativement au Programme des semences"
    },
    "html_modified": "2024-02-13 9:49:16 AM",
    "modified": "2022-03-08",
    "issued": "2022-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/seed_proc_roles_csi_1377401804805_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/seed_proc_roles_csi_1377401804805_fra"
    },
    "ia_id": "1377401936026",
    "parent_ia_id": "1299173306579",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Conformity Verification Bodies",
        "fr": "Organismes de la v\u00e9rification de la conformit\u00e9"
    },
    "commodity": {
        "en": "Seeds",
        "fr": "Semences"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299173306579",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "CVB / CFIA roles and responsibilities with respect to the Seed Program",
        "fr": "R\u00f4les et responsabilit\u00e9s de l'OVC / ACIA relativement au Programme des semences"
    },
    "label": {
        "en": "CVB / CFIA roles and responsibilities with respect to the Seed Program",
        "fr": "R\u00f4les et responsabilit\u00e9s de l'OVC / ACIA relativement au Programme des semences"
    },
    "templatetype": "content page 1 column",
    "node_id": "1377401936026",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299173228771",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/roles-and-responsibilities/",
        "fr": "/protection-des-vegetaux/semences/roles-et-responsabilites/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "CVB / CFIA roles and responsibilities with respect to the Seed Program",
            "fr": "R\u00f4les et responsabilit\u00e9s de l'OVC / ACIA relativement au Programme des semences"
        },
        "description": {
            "en": "Glossary of Abbreviations and Terms",
            "fr": "Glossaire des abr\u00e9viations et des termes"
        },
        "keywords": {
            "en": "plants, plant health",
            "fr": "v\u00e9g\u00e9taux, la sant\u00e9 des v\u00e9g\u00e9taux"
        },
        "dcterms.subject": {
            "en": "crops,inspection,plants",
            "fr": "cultures,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-03-09",
            "fr": "2022-03-09"
        },
        "modified": {
            "en": "2022-03-08",
            "fr": "2022-03-08"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "CVB / CFIA roles and responsibilities with respect to the Seed Program",
        "fr": "R\u00f4les et responsabilit\u00e9s de l'OVC / ACIA relativement au Programme des semences"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=108#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">Glossary of abbreviations and terms</a></li>\n<li><a href=\"#a2\">Roles and responsibilities</a>\n<ul>\n<li><a href=\"#a2_1\">Canadian Food Inspection Agency</a></li>\n<li><a href=\"#a2_2\">Conformity verification body</a></li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a1\">Glossary of abbreviations and terms</h2>\n<dl>\n<dt>AC (approved conditioner)</dt>\n<dd>An establishment that prepares seed of pedigreed status and in respect of which a registration as an approved conditioner is in force</dd>\n<dt>AI (authorized importer)</dt>\n<dd>An establishment that prepares imported seed and in respect of which a registration as an authorized importer is in force</dd>\n<dt>BSF (bulk storage facility)</dt>\n<dd>An establishment that stores, in bulk, seed graded with a Canada pedigreed grade name and in respect of which a registration as a bulk storage facility is in force</dd>\n<dt>CFIA</dt>\n<dd>Canadian Food Inspection Agency</dd>\n<dt>CVB</dt>\n<dd>Conformity verification body</dd>\n<dt>GB grader</dt>\n<dd>A grader accredited to examine seed and to review analytical information on certificates of analysis to assign a Canada pedigreed grade name to all crop kinds set out in Tables I to VI and XVIII.</dd>\n<dt>GD grader</dt>\n<dd>A grader accredited to assign a Canada pedigreed grade name to seed lots based on a review of the reports of analysis (ROAs) only. GD graders are accredited to assign a grade to crop kinds found in 1 or more groups of the Tables in Schedule I (the Grade Tables) to the regulations.</dd>\n<dt>SICAs (seed import conformity assessors)</dt>\n<dd>(formerly GI graders/SIRAs) are accredited to assess whether imported seed meets the minimum standards for import into Canada based on their review of Canadian and/or foreign ROAs. SICAs are accredited to assess conformity with minimum standards for crop kinds found in any group(s) of the Grade Tables for which they are accredited.</dd>\n<dt>ISTA</dt>\n<dd>International Seed Testing Association</dd>\n<dt>QA</dt>\n<dd>quality assurance</dd>\n<dt>SLAAP</dt>\n<dd>seed laboratory accreditation and audit protocol</dd>\n</dl>\n\n<h2 id=\"a2\">Roles and responsibilities</h2>\n\n<h3 id=\"a2_1\">Canadian Food Inspection Agency</h3>\n\n<p>The Canadian Food Inspection Agency (CFIA) is responsible for the:</p>\n<ul>\n<li>accreditation of laboratories, analysts and graders</li>\n<li>licensing of operators and samplers</li>\n<li>registration of approved conditioners, bulk storage facilities and authorized importers, and</li>\n<li>administering and enforcing the <i>Seeds Act</i> and regulations</li>\n</ul>\n\n<h4>CFIA roles and responsibilities</h4>\n\n<ul>\n<li>audit the CVB to verify that the CVB does and can continue to meet its responsibilities with respect to the Seed Program</li>\n<li>maintain and update, as required, the seed laboratory accreditation and audit protocol (SLAAP)</li>\n<li>receive and review applications for laboratory accreditation for conformity with the SLAAP, including the initial laboratory audit</li>\n<li>administer seed analyst evaluations</li>\n<li>conduct pre-accreditation proficiency sample testing for scope of testing requested by new laboratory</li>\n<li>provide assistance in the development of proficiency testing programs delivered by the CVB</li>\n<li>provide CVB and accredited laboratories with the test results/reports on all proficiency testing programs for the laboratories</li>\n<li>conduct inspections at a registered seed establishment at any time but <strong>must be done</strong> when:\n<ul>\n<li>the CVB issues a corrective action request pertaining to a critical non-conformance</li>\n<li>a review of the CVB annual report triggers an inspection as part of the annual directed inspection plan</li>\n<li>a complaint or other evidence of non-compliance is received, or</li>\n<li>a registered seed establishment is suspended or cancelled</li>\n</ul>\n</li>\n<li>during an inspection, the inspector may take samples, verify that product meets the required standard, verify labels and records and conduct any other activity to verify compliance with the <i>Seeds Act</i> and regulations</li>\n<li>administer practical assessments for all graders and administer theory assessments for SICAs</li>\n<li>The Seed Section \u00a0respond to requests from CVB for interpretation of regulations, policies and procedures</li>\n<li>approve and validate the use of automatic seed samplers for use in the taking of samples drawn for the issuance of an ISTA certificate by CFIA</li>\n<li>maintain records of registered seed establishments and their personnel</li>\n<li>collect fees for renewal of graders and operators not located at registered seed establishments</li>\n</ul>\n\n<h3 id=\"a2_2\">The conformity verification body</h3>\n\n<p>The conformity verification body (CVB) is accredited by the CFIA for the:</p>\n<ul>\n<li>assessment, recommendation for approval and audit of approved conditioners (AC), bulk storage facilities (BSF) and authorized importers (AI)</li>\n<li>recommendation for accreditation/licensing of graders and operators who have successfully completed written evaluations as referred to in the <i>Seeds Regulations</i>, and</li>\n<li>recommendation for annual renewal of laboratories that have successfully completed an audit in the current or preceding calendar year and successfully participated in check sample programs approved by the CFIA</li>\n</ul>\n\n<h4>CVB roles and responsibilities:</h4>\n\n<ul>\n<li>accept and review applications for registration of establishments, licensing of operators, and accreditation of graders</li>\n<li>provide or accredit, in accordance with the CVB standards, technical auditors who will assess/audit an establishment's ability to meet the CVB standards according to an established frequency</li>\n<li>provide or accredit, in accordance with the CVB standards, technical auditors who will audit a laboratory's ability to meet the SLAAP according to an established frequency</li>\n<li>assess the applicants who wish to be licensed as approved conditioner \u00a0for competence in complying with CFIA requirements</li>\n<li>administer theory assessments for GB and GD graders in compliance with the <a href=\"/plant-health/seeds/seed-testing-and-grading/candidate-s-guide/eng/1377363765109/1377363964670\">Guide to the Evaluation of Candidate Seed Graders and Seed Import Conformity Assessors</a></li>\n<li>develops/maintains quality system standards for the accreditation of AC, BSF and AI facilities</li>\n<li>develops/maintains technical manuals to provide industry guidance on the AC, BSF and AI programs</li>\n<li>develops/maintains audit protocols and procedures to assess/audit facilities to verify facility compliance with CVB quality system standards and the <i>Seeds Regulations</i></li>\n<li>recommend to CFIA for registration, licensing, recognition or accreditation and renewal thereof, as the case may be, establishments, facilities, and individuals, as the case may be, that meet the CFIA requirements</li>\n<li>draw monitoring samples and arrange for testing at a monitoring laboratory</li>\n<li>during an establishment assessment, obtain and document objective evidence to determine whether\n<ul>\n<li>the establishment's QA documentation is accessible, current and available to all staff</li>\n<li>the procedures described in the client's QA manual are being adhered to</li>\n<li>the processes are being performed by competent personnel (accredited/licensed as necessary) according to specifications stipulated in the relevant procedures</li>\n<li>the seed meets the required standard</li>\n</ul>\n</li>\n<li>identify and classify non-conformances and issue corrective action requests to establishments</li>\n<li>maintain the database on seed establishments which includes information on assessments/audits and may include information on recognition programs other than those under the authority of the <i>Seeds Regulations</i></li>\n<li>report annually to the CFIA on the results of audits conducted in the prior seed year</li>\n</ul>\n\n<p>Note that CVB collects annual fees from seed establishments for the renewal of their registration and the renewal of accreditation/licensing for their personnel.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=108#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Glossaire des abr\u00e9viations et des termes</a></li>\n<li><a href=\"#a2\">R\u00f4les et responsabilit\u00e9s</a>\n<ul>\n<li><a href=\"#a2_1\">Agence canadienne d'inspection des aliments</a></li>\n<li><a href=\"#a2_2\">Organisme de la v\u00e9rification de la conformit\u00e9</a></li>\n</ul>\n</li>\n</ul>\n\t\t\n<h2 id=\"a1\">Glossaire des abr\u00e9viations et des termes</h2>\n\n<dl>\n<dt>ACIA</dt>\n<dd>Agence canadienne d'inspection des aliments</dd>\n<dt>AQ</dt>\n<dd>assurance de la qualit\u00e9</dd>\n<dt>CA (conditionneur agr\u00e9\u00e9)</dt>\n<dd>\u00c9tablissement qui conditionne de la semence de qualit\u00e9 g\u00e9n\u00e9alogique et qui fait l'objet d'un agr\u00e9ment en vigueur \u00e0 titre de conditionneur agr\u00e9\u00e9.</dd>\n<dt>Classificateur GB</dt>\n<dd>Classificateur agr\u00e9\u00e9 pour examiner la semence et pour examiner l'information analytique sur les Certificats d'analyse pour attribuer une d\u00e9nomination de cat\u00e9gorie Canada g\u00e9n\u00e9alogique pour tous les types de cultures vis\u00e9s dans les Tableaux I \u00e0 VI et XVIII.</dd>\n<dt>Classificateur GD</dt>\n<dd>Classificateur agr\u00e9\u00e9 pour attribuer une d\u00e9nomination de la cat\u00e9gorie \u00ab Canada g\u00e9n\u00e9alogique \u00bb \u00e0 des lots de semences uniquement d'apr\u00e8s l'examen des rapports d'analyse. Les classificateurs GD sont agr\u00e9\u00e9s pour attribuer une cat\u00e9gorie aux semences des esp\u00e8ces qui figurent dans un ou plus des groupes des tableaux de l'annexe I.</dd>\n<dt>ECSI (\u00e9valuateur de la conformit\u00e9 des semences import\u00e9es)</dt>\n<dd>(anciennement nomm\u00e9 classificateurs GI ou APDIS) classificateurs agr\u00e9\u00e9s pour \u00e9valuer si les semences import\u00e9es respectent les normes minimales d'importation du Canada d'apr\u00e8s l'examen des rapports d'analyse produits au Canada ou \u00e0 l'\u00e9tranger. Les ECSI sont agr\u00e9\u00e9s pour v\u00e9rifier la conformit\u00e9 aux normes minimales des semences de toutes les esp\u00e8ces qui figurent dans un ou plus des groupes des tableaux des normes de cat\u00e9gories que lui permet son agr\u00e9ment.</dd>\n<dt>IA (Importateur autoris\u00e9)</dt>\n<dd>\u00c9tablissement qui pr\u00e9pare de la semence import\u00e9e et dont l'enregistrement est en vigueur en tant qu'importateur autoris\u00e9</dd>\n<dt>OVC</dt>\n<dd>Organisme de la v\u00e9rification de la conformit\u00e9</dd>\n<dt>ISTA</dt>\n<dd>Association internationale d'essais de semences</dd>\n<dt>ISV (Installation de stockage en vrac)</dt>\n<dd>\u00c9tablissement qui entrepose en vrac de la semence ayant un nom de cat\u00e9gorie de g\u00e9n\u00e9alogie contr\u00f4l\u00e9e au Canada et dont l'enregistrement est en vigueur en tant qu'installation de stockage en vrac</dd>\n<dt>PAALAS</dt>\n<dd>protocole d'accr\u00e9ditation et d'audit des laboratoires d'analyse des semences</dd>\n</dl>\n\n<h2 id=\"a2\">R\u00f4les et responsabilit\u00e9s</h2>\n\n<h3 id=\"a2_1\">Agence canadienne d'inspection des aliments</h3>\n\n<p>L'Agence Canadienne d'inspection des Aliments (ACIA) est responsable de\u00a0:</p>\n\n<ul>\n<li>l'accr\u00e9ditation des laboratoires, des analystes et des classificateurs,</li>\n<li>l'octroi de permis aux exploitants et l'accr\u00e9ditation des \u00e9chantillonneurs,</li>\n<li>l'enregistrement des conditionneurs approuv\u00e9s, des installations de stockage en vrac et des importateurs autoris\u00e9s, et</li>\n<li>l'administration et l'application de la Loi et de la r\u00e9glementation sur les semences.</li>\n</ul>\n\n<h4>R\u00f4les et responsabilit\u00e9s de l'ACIA</h4>\n\n<ul>\n<li>faire une v\u00e9rification de l'OVC pour v\u00e9rifier que l'OVC fait et peut continuer \u00e0 assumer ses responsabilit\u00e9s relatives au Programme des semences;</li>\n<li>maintenir et mettre \u00e0 jour, lorsque n\u00e9cessaire, le protocole d'accr\u00e9ditation et d'audit des laboratoires d'analyse des semences (PAALAS);</li>\n<li>recevoir et passer en revue les demandes d'accr\u00e9ditation de laboratoire par rapport \u00e0 la conformit\u00e9 au PAALAS, y compris la v\u00e9rification initiale du laboratoire;</li>\n<li>administrer des \u00e9valuations d'Analyste de semences;</li>\n<li>r\u00e9aliser un \u00e9chantillonnage des comp\u00e9tences de pr\u00e9-accr\u00e9ditation en vue de d\u00e9terminer la port\u00e9e de l'essai demand\u00e9e par le nouveau laboratoire;</li>\n<li>contribuer \u00e0 la d\u00e9finition de programmes d'essai d'aptitude fournis par les OVC;</li>\n<li>fournir \u00e0 l'OVC et \u00e0 des laboratoires accr\u00e9dit\u00e9s les r\u00e9sultats d'essai/rapports sur tous les programmes d'essai d'aptitude pour les laboratoires;</li>\n<li>r\u00e9aliser une inspection sur le site d'une compagnie de semence enregistr\u00e9e \u00e0 tout moment, mais de fa\u00e7on obligatoire dans les situations suivantes\u00a0:\n<ul>\n<li>l'OVC \u00e9met une demande de mesure corrective concernant une non-conformit\u00e9 critique,</li>\n<li>un examen du rapport annuel de l'OVC d\u00e9clenche une inspection dans le cadre du plan dirig\u00e9 annuel d'inspection,</li>\n<li>r\u00e9ception d'une plainte ou de tout autre indice de non-conformit\u00e9, ou</li>\n<li>un \u00e9tablissement semencier agr\u00e9\u00e9 est suspendue ou perd son agr\u00e9ment;</li>\n</ul>\n</li>\n<li>pendant une inspection, l'inspecteur peut pr\u00e9lever des \u00e9chantillons, v\u00e9rifier que le produit r\u00e9pond aux normes requises, v\u00e9rifier les \u00e9tiquettes et les dossiers et exerce toute autre activit\u00e9 pour v\u00e9rifier la conformit\u00e9 \u00e0 la Loi et \u00e0 la r\u00e9glementation sur les semences;</li>\n<li>r\u00e9aliser des \u00e9valuations pratiques de tous les classificateurs et administrent des \u00e9valuations th\u00e9oriques des classificateurs ECSI;</li>\n<li>LaSection des semences r\u00e9pond aux demandes de l'OVC en vue de l'application des r\u00e8glements, de la politiques et des proc\u00e9dures;</li>\n<li>approuver et valider l'utilisation des \u00e9chantillonneurs automatiques de semence par rapport \u00e0 leur application \u00e0 la prise d'\u00e9chantillons en vue de l'\u00e9mission d'un certificat de l'ISTA par l'ACIA;</li>\n<li>tenir des dossiers des \u00e9tablissements semenciers agr\u00e9\u00e9s et de leur personnel;</li>\n<li>percevoir des honoraires pour le renouvellement des classificateurs et des op\u00e9rateurs non situ\u00e9s aux \u00e9tablissements semenciers agr\u00e9\u00e9s</li>\n</ul>\n\n<h3 id=\"a2_2\">Organisme de la v\u00e9rification de la conformit\u00e9</h3>\n\n<p>L'organisme de la v\u00e9rification de la conformit\u00e9 (OVC)C est accr\u00e9dit\u00e9 par l'ACIA en tant qu'organisme \u00a0pour\u00a0:</p>\n\n<ul>\n<li>\u00e9valuation, recommandation pour l'approbation et v\u00e9rification de conditionneurs approuv\u00e9s (CA), des installations de stockage en vrac approuv\u00e9es (ISV) et importateurs autoris\u00e9s (IA);</li>\n<li>recommandation d'accr\u00e9ditation/l'autorisation des classificateurs et des op\u00e9rateurs qui ont pass\u00e9 avec succ\u00e8s des \u00e9valuations \u00e9crites comme vis\u00e9 dans la R\u00e9glementation sur les semences,</li>\n<li>recommandation de renouvellement annuel des laboratoires qui ont pass\u00e9 avec succ\u00e8s une v\u00e9rification dans l'ann\u00e9e civile en cours ou pr\u00e9c\u00e9dente et ont particip\u00e9 avec succ\u00e8s aux programmes d'\u00e9chantillonnage-t\u00e9moin approuv\u00e9s par l'ACIA.</li>\n</ul>\n\n<h4>R\u00f4les et responsabilit\u00e9s de l'OVC</h4>\n\n<ul>\n<li>accepter et passer en revue des demandes d'enregistrement des \u00e9tablissements, autorisation des op\u00e9rateurs, et l'accr\u00e9ditation des classificateurs;</li>\n<li>fournir ou accr\u00e9diter, selon les normes de l'OVC, les auditeurs techniques qui \u00e9valuent/v\u00e9rifient la capacit\u00e9 d'un \u00e9tablissement \u00e0 satisfaire aux normes de l'OVC selon une fr\u00e9quence \u00e9tablie;</li>\n<li>fournir ou accr\u00e9diter, selon les normes de l'OVC, les auditeurs techniques qui v\u00e9rifieront la capacit\u00e9 d'un laboratoire \u00e0 satisfaire le PAALAS selon une fr\u00e9quence \u00e9tablie;</li>\n<li>\u00e9valuer les demandeurs qui souhaitent \u00eatre autoris\u00e9s comme conditionneur agr\u00e9\u00e9 , selon leur capacit\u00e9 \u00e0 se conformer aux exigences de l'ACIA;;</li>\n<li>administrer des \u00e9valuations th\u00e9oriques pour les classificateurs GB et GD conform\u00e9ment au <a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/guide-du-candidat/fra/1377363765109/1377363964670\">Guide du candidat \u00e0 l'\u00e9valuation du classificateur de semences et de l'\u00e9valuateur de la conformit\u00e9 des semences import\u00e9es</a>;</li>\n<li>\u00e9laborer/maintenir les normes du syst\u00e8me de qualit\u00e9 pour l'accr\u00e9ditation d'installations de CA, d'ISV et d'IA;</li>\n<li>\u00e9laborer/maintenir les manuels techniques qui servent de guide aux industries sur les programmes de CA, d'ISV et d'IA;</li>\n<li>\u00e9laborer/maintenir des protocoles de v\u00e9rification et des proc\u00e9dures pour \u00e9valuer/v\u00e9rifier les installations pour v\u00e9rifier la conformit\u00e9 du service selon les normes de syst\u00e8me de qualit\u00e9 de l'OVC et \u00e0 la R\u00e9glementation sur les semences;</li>\n<li>recommander \u00e0 l'ACIA l'enregistrement, l'autorisation, la reconnaissance ou l'accr\u00e9ditation et le renouvellement, selon les cas, d'\u00e9tablissements, d'installations et de personnes, selon les cas, satisfaisant aux exigences de l'ACIA;</li>\n<li>choisir des \u00e9chantillons de surveillance et faire le n\u00e9cessaire pour organiser des essais dans un laboratoire de surveillance;</li>\n<li>pendant l'\u00e9valuation d'un \u00e9tablissement, recueillir et noter des preuves objectives permettant de d\u00e9terminer\n<ul>\n<li>si la documentation d'AQ de cet \u00e9tablissement est accessible, \u00e0 jour et disponible \u00e0 tout le personnel</li>\n<li>les proc\u00e9dures d\u00e9crites dans le manuel d'AQ du client sont suivies</li>\n<li>les processus sont r\u00e9alis\u00e9s par un personnel comp\u00e9tent (autoris\u00e9/accr\u00e9dit\u00e9 si n\u00e9cessaire), selon les modalit\u00e9s stipul\u00e9es dans les proc\u00e9dures pertinentes</li>\n<li>la semence r\u00e9pond aux normes requises</li>\n</ul>\n</li>\n<li>d\u00e9terminer et classifier les infractions et communiquer aux \u00e9tablissements des demandes de mesures correctrices;</li>\n<li>maintenir la base de donn\u00e9es sur \u00e9tablissements semenciers avec les informations sur les \u00e9valuations/v\u00e9rifications et \u00e9ventuellement les informations sur les programmes de reconnaissance autres que ceux couverts par la r\u00e9glementation sur les semences;</li>\n<li>faire annuellement un rapport \u00e0 l'ACIA sur les r\u00e9sultats des v\u00e9rifications r\u00e9alis\u00e9es au cours de l'ann\u00e9e semenci\u00e8re ant\u00e9rieure.</li>\n</ul>\n\n<p>N.B.\u00a0: L'OVC per\u00e7oit aupr\u00e8s des \u00e9tablissements semenciers les frais \u00a0annuels pour le renouvellement de leur enregistrement et le renouvellement de l'accr\u00e9ditation/ autorisation de leur personnel.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}