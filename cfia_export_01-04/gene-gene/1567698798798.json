{
    "dcr_id": "1567698798798",
    "title": {
        "en": "Importing fish products containing meat",
        "fr": "Importation de produits de poisson contenant de la viande"
    },
    "html_modified": "20-9-2019",
    "modified": "4-12-2019",
    "issued": "20-9-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/fish_products_meat_1567698798798_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/fish_products_meat_1567698798798_fra"
    },
    "parent_ia_id": "1536170495320",
    "ia_id": "1567698799220",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importing fish products containing meat",
        "fr": "Importation de produits de poisson contenant de la viande"
    },
    "label": {
        "en": "Importing fish products containing meat",
        "fr": "Importation de produits de poisson contenant de la viande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1567698799220",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1536170455757",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/fish-products-containing-meat/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/produits-de-poisson-contenant-de-la-viande/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importing fish products containing meat",
            "fr": "Importation de produits de poisson contenant de la viande"
        },
        "description": {
            "en": "Importing fish products containing meat",
            "fr": "Importer des produits de poisson contenant de la viande"
        },
        "keywords": {
            "en": "Food-specific requirements and guidance, Meat products, food animals, Safe Food for Canadians Regulations, SFCR, fish product, fish",
            "fr": "Exigences et directives sp\u00e9cifiques aux aliments, produits de viande, animaux destin\u00e9s \u00e0 l'alimentation, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, produit de poisson, poisson"
        },
        "dcterms.subject": {
            "en": "food,food labeling,food inspection,legislation,agri-food products,regulation,food safety,food processing",
            "fr": "aliment,\u00e9tiquetage des aliments,inspection des aliments,l\u00e9gislation,produit agro-alimentaire,r\u00e9glementation,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-09-20",
            "fr": "2019-09-20"
        },
        "modified": {
            "en": "2019-12-04",
            "fr": "2019-12-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importing fish products containing meat",
        "fr": "Importation de produits de poisson contenant de la viande"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-tp-lg\">A person who imports a <a href=\"/eng/1430250286859/1430250287405#a79\">fish</a> product containing <a href=\"/eng/1430250286859/1430250287405#a108\">meat</a> must ensure that all the meat ingredients:</p>\n\n<ul>\n<li class=\"mrgn-bttm-md\">originate from a country with a meat inspection system recognized by the Canadian Food Inspection Agency (CFIA) under Part 7 of the <i>Safe Food for Canadians Regulations</i> (SFCR).\n<ul>\n<li>Refer to the <a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/approved-countries/eng/1336318487908/1336319720090\">List of Countries Eligible for Exportation to Canada, Conditions for Importation of Meat Products</a>.</li>\n</ul></li>\n\n<li class=\"mrgn-bttm-md\">were issued an official document by the foreign competent authority that accompanies the fish product such as:\n<ul>\n<li>an <a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/procedures-omics/eng/1336324305944/1336324402422\">Official Meat Inspection Certificate</a> (OMIC) or an official document in a format approved by CFIA.</li>\n</ul></li>\n</ul>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Note</h2>\n</header>\n<div class=\"panel-body\">\n<p>The requirements specific to the import of meat products are outlined in the <a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/importing-meat-products/eng/1545799257612/1545799287057\">Overview\u00a0\u2013\u00a0Importing meat products</a>.</p>\n</div>\n</section>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Keep in mind!</h2>\n</header>\n<div class=\"panel-body\">\n<p class=\"mrgn-bttm-sm\">When applying for a Safe Food for Canadians (SFC licence) to import a fish product containing meat, you should select only the food commodity \"fish and seafood.\"</p>\n</div>\n</section>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-tp-lg\">Une personne qui importe un produit de <a href=\"/fra/1430250286859/1430250287405#a79\">poisson</a> qui contient de la <a href=\"/fra/1430250286859/1430250287405#a108\">viande</a> doit s'assurer que tous les ingr\u00e9dients de viande\u00a0:</p>\n\n<ul>\n<li class=\"mrgn-bttm-md\">proviennent d'un pays dont le syst\u00e8me d'inspection de la viande a \u00e9t\u00e9 reconnu par l'Agence d'inspection des aliments (ACIA) au titre de la partie 7 du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC).\n<ul>\n<li>Consultez la <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/pays-approuves/fra/1336318487908/1336319720090\">liste des pays pouvant exporter au Canada et les conditions pour l'importation de produits de viande</a> pour conna\u00eetre la liste des pays pouvant exporter de la viande au Canada.</li>\n</ul></li>\n\n<li class=\"mrgn-bttm-md\">sont assortis d'un document officiel \u00e9mis par les autorit\u00e9s comp\u00e9tentes \u00e9trang\u00e8res qui accompagne le produit de poisson, tel que\u00a0:\n<ul>\n<li>un <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/procedures-coiv/fra/1336324305944/1336324402422\">certificat officiel d'inspection des viandes</a> ou un document officiel dans un format approuv\u00e9 par l'ACIA.</li>\n</ul></li>\n</ul>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Remarque</h2>\n</header>\n<div class=\"panel-body\">\n<p>La page <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/importer-des-produits-de-viande/fra/1545799257612/1545799287057\">Aper\u00e7u \u2013 Importation de produits de viande</a> pr\u00e9sente les grandes lignes des exigences propres \u00e0 l'importation de produits de viande.</p>\n</div>\n</section>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">\u00c0 retenir!</h2>\n</header>\n<div class=\"panel-body\">\n<p class=\"mrgn-bttm-sm\">Lorsque vous pr\u00e9senter une demande de licence pour la salubrit\u00e9 des aliments au Canada (licence SAC) pour importer un produit de poisson contenant de la viande, vous devez seulement s\u00e9lectionner la cat\u00e9gorie d'aliments \u00ab\u00a0poisson et fruits de mer\u00a0\u00bb.</p>\n</div>\n</section>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}