{
    "dcr_id": "1364105180839",
    "title": {
        "en": "Inadvertent release of unapproved <abbr title=\"plants with novel traits\">PNTs</abbr>",
        "fr": "<abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveau\">VCN</abbr> non approuv\u00e9 diss\u00e9min\u00e9 dans l'environnement de fa\u00e7on involontaire"
    },
    "html_modified": "2024-03-12 3:56:13 PM",
    "modified": "2013-03-24",
    "issued": "2013-03-24 02:06:23",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/seed_advisories_inadvertent_release_1364105180839_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/seed_advisories_inadvertent_release_1364105180839_fra"
    },
    "ia_id": "1364105247510",
    "parent_ia_id": "1364064312276",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Additional Information",
        "fr": "Information suppl\u00e9mentaire"
    },
    "commodity": {
        "en": "Seeds",
        "fr": "Semences"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1364064312276",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Inadvertent release of unapproved <abbr title=\"plants with novel traits\">PNTs</abbr>",
        "fr": "<abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveau\">VCN</abbr> non approuv\u00e9 diss\u00e9min\u00e9 dans l'environnement de fa\u00e7on involontaire"
    },
    "label": {
        "en": "Inadvertent release of unapproved <abbr title=\"plants with novel traits\">PNTs</abbr>",
        "fr": "<abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveau\">VCN</abbr> non approuv\u00e9 diss\u00e9min\u00e9 dans l'environnement de fa\u00e7on involontaire"
    },
    "templatetype": "content page 1 column",
    "node_id": "1364105247510",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1364064277710",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-industry-advisories/unapproved-pnts/",
        "fr": "/protection-des-vegetaux/semences/avis-a-l-industrie-des-semences/vcn-non-approuve/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Inadvertent release of unapproved PNTs",
            "fr": "VCN non approuv\u00e9 diss\u00e9min\u00e9 dans l'environnement de fa\u00e7on involontaire"
        },
        "description": {
            "en": "plants, plant health",
            "fr": "v\u00e9g\u00e9taux, la sant\u00e9 des v\u00e9g\u00e9taux"
        },
        "keywords": {
            "en": "plants, plant health",
            "fr": "v\u00e9g\u00e9taux, la sant\u00e9 des v\u00e9g\u00e9taux"
        },
        "dcterms.subject": {
            "en": "inspection",
            "fr": "inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-03-24 02:06:23",
            "fr": "2013-03-24 02:06:23"
        },
        "modified": {
            "en": "2013-03-24",
            "fr": "2013-03-24"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Inadvertent release of unapproved PNTs",
        "fr": "VCN non approuv\u00e9 diss\u00e9min\u00e9 dans l'environnement de fa\u00e7on involontaire"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Seed companies must inform the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> when routine quality control testing identifies novel traits unapproved in Canada. This allows the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to work with the seed companies to resolve any outstanding regulatory issues  resulting from the detection of unapproved novel traits. For example, it  may be necessary to place the seed production field under post-harvest  restrictions to reduce the risk of persistence of the unapproved novel  trait. Withdrawal of seed from the marketplace, international notifications and  tracebacks may also be necessary to satisfy Canadian domestic  requirements as well as our obligations to trading partners.</p>\n<p>Recognizing that inadvertent release of unapproved plants with novel traits (PNTs) could occur, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is committed to working with the seed and associated industries to ensure timely and effective remedial actions. Failure to notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> of the inadvertent release of unapproved <abbr title=\"plants with novel traits\">PNTs</abbr> is a serious contravention of the Seeds Regulations and will be handled accordingly.</p>\n<p>If, through quality control testing, it appears that an inadvertent release of unapproved <abbr title=\"plants with novel traits\">PNTs</abbr> may have occurred or is likely to occur, you are required to immediately notify your local office of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p class=\"text-right small\">Originally issued May 3, 2001 (Notice to Canadian Seed Companies)</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=29&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les entreprises de semences doit informer l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> si elles d\u00e9tectent des caract\u00e8res nouveaux non approuv\u00e9s au Canada lors d'un test de contr\u00f4le de la qualit\u00e9. Ainsi, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pourra collaborer avec les entreprises pour r\u00e9soudre toute question de r\u00e9glementation faisant suite \u00e0 la d\u00e9couverte de caract\u00e8res nouveaux non  approuv\u00e9s. Par exemple, il y aurait peut-\u00eatre lieu d' imposer des restrictions au champ de production de semences,  apr\u00e8s la r\u00e9colte, pour r\u00e9duire le risque de persistance du caract\u00e8re nouveau non approuv\u00e9. Il faudra peut-\u00eatre aussi retirer la  semence en question des march\u00e9s, envoyer des avis internationaux et retracer les produits vis\u00e9s pour respecter les  exigences nationales du Canada ainsi que nos obligations envers nos  partenaires commerciaux.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> reconna\u00eet qu'il est possible de diss\u00e9miner par m\u00e9garde dans l'environnement des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux (VCN) non approuv\u00e9s. C'est pourquoi elle est r\u00e9solue \u00e0 joindre ses efforts \u00e0 ceux de l'industrie des semences et des autres industries connexes pour r\u00e9agir de fa\u00e7on rapide et efficace. En omettant d'aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de la diss\u00e9mination involontaire de <abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux\">VCN</abbr> non approuv\u00e9s, les entreprises contreviennent de fa\u00e7on s\u00e9rieuse au R\u00e8glement sur les semences, et subiront les mesures qui s'imposent.</p>\n<p>Si, en effectuant des tests de contr\u00f4le de la qualit\u00e9, vous constatez qu'un <abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux\">VCN</abbr> non approuv\u00e9 a \u00e9t\u00e9 diss\u00e9min\u00e9 ou pourrait \u00eatre diss\u00e9min\u00e9 dans l'environnement de fa\u00e7on involontaire, vous \u00eates tenus d'en informer imm\u00e9diatement votre bureau local de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p class=\"text-right small\">Publi\u00e9 initialement le 4 mai 2001 (Avis aux entreprises canadiennes de semences)</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}