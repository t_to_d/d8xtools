{
    "dcr_id": "1304243817860",
    "title": {
        "en": "Philippines - Export requirements for fish and seafood",
        "fr": "Philippines - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "html_modified": "2024-02-13 9:46:42 AM",
    "modified": "2019-03-22",
    "issued": "2011-05-01 05:57:01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/fispoi_export_philippines_1304243817860_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/fispoi_export_philippines_1304243817860_fra"
    },
    "ia_id": "1304243914409",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Inspecting and investigating - Performing export certification activities|Exporting food|Relevant to All",
        "fr": "Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation|Exportation d\u2019aliments|Pertinents pour tous"
    },
    "commodity": {
        "en": "Fish and seafood",
        "fr": "Poissons et fruits de mer"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Philippines - Export requirements for fish and seafood",
        "fr": "Philippines - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "label": {
        "en": "Philippines - Export requirements for fish and seafood",
        "fr": "Philippines - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "templatetype": "content page 1 column",
    "node_id": "1304243914409",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/philippines-fish-and-seafood/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/philippines-le-poisson-et-les-produits-de-la-mer/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Philippines - Export requirements for fish and seafood",
            "fr": "Philippines - Exigences d'exportation pour le poisson et les produits de la mer"
        },
        "description": {
            "en": "Countries to which exports are currently made - Philippines",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Philippines"
        },
        "keywords": {
            "en": "fish, seafood, safety, QMP, HACCP, molluscs, exports, certification, Philippines",
            "fr": "poisson, produits de la mer, PGQ, HACCP, mollusques, exportation, certification, Philippines"
        },
        "dcterms.subject": {
            "en": "crustaceans,exports,seafood,inspection,food inspection,fish,fisheries products",
            "fr": "crustac\u00e9,exportation,fruits de mer,inspection,inspection des aliments,poisson,produit de la peche"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exportation d'aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-01 05:57:01",
            "fr": "2011-05-01 05:57:01"
        },
        "modified": {
            "en": "2019-03-22",
            "fr": "2019-03-22"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Philippines - Export requirements for fish and seafood",
        "fr": "Philippines - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"text-right\"><strong>Last update: April 30, 2018</strong></p>\n\n<h2>Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<ul>\n<li>All fish and seafood products</li>\n</ul>\n\n<h2>Pre-export approvals by the competent authority of the importing country</h2>\n\n<ul>\n<li>Prior to import, approval must be obtained from the Bureau of Fisheries and Aquatic Resources and the Food and Drug Administration Philippines</li>\n</ul>\n\n<h3>Product specifications</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-7\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">\n<strong>Maximum levels for chemical contaminants</strong>\n</caption>\n<thead>\n<tr class=\"text-center\">\n<th>Metal</th>\n<th>Maximum level permitted in <abbr title=\"parts per million\">ppm</abbr></th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Arsenic</td>\n<td class=\"text-center\">3.0</td>\n</tr>\n<tr>\n<td>Lead</td>\n<td class=\"text-center\">0.5</td>\n</tr>\n<tr>\n<td>Mercury</td>\n<td class=\"text-center\">0.5 (methyl mercury)</td>\n</tr>\n<tr>\n<td>Tin</td>\n<td class=\"text-center\">200.0</td>\n</tr>\n</tbody>\n</table>\n</div>\n</div>\n<h2>Labelling, packaging and marking requirements</h2>\n\n<ul>\n<li>Labels must be marked in English, Spanish or Filipino, showing brand name, trade mark or trade name, physical or chemical composition, weight or measurement in metric units, name and address of packer or seller, and country of origin.</li>\n</ul>\n\n<h2>Documentation requirements</h2>\n\n<h3>Certificate</h3>\n\n<ul>\n<li>Certificate of Origin and Hygiene (CFIA/ACIA 5003)</li>\n</ul>\n\n<h2>Other information</h2>\n\n<p><a href=\"http://www.fda.gov.ph/\">Food and Drug Administration Philippines</a><br>\n<br>\n</p>\n\n<p><a href=\"https://www.bfar.da.gov.ph/\">Bureau of Fisheries and Aquatic Resources</a><br>\n<br>\n</p>\n\n<p><a href=\"http://www.bps.dti.gov.ph/\">Bureau of Philippine Standards</a> \u2013 Department of Trade and Industry<br>\n<sup>rd</sup> floor)<br>\n<abbr title=\"Senator\">Sen.</abbr> Gil J. Puyat Avenue, Makati City<br>\n</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"text-right\"><strong>Derni\u00e8re mise \u00e0 jour\u00a0: le 30\u00a0avril 2018</strong></p>\n\n<h2>Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<ul>\n<li>Tous les poissons et produits de la mer</li>\n</ul>\n\n<h2>Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<ul>\n<li>Une autorisation doit \u00eatre obtenue du Bureau of <span lang=\"en\">Fisheries and Aquatic Resources</span> et de la <span lang=\"en\">Food and Drug Administration Philippines</span> avant importation.</li>\n</ul>\n\n<h3>Sp\u00e9cifications du produit</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-7\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">\n<strong>Concentrations maximales des contaminants chimiques</strong>\n</caption>\n<thead>\n<tr class=\"text-center\">\n<th>M\u00e9tal</th>\n<th>Concentration maximale permise (en <abbr title=\"parties par million\">ppm</abbr>)</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Arsenic</td>\n<td class=\"text-center\">3,0</td>\n</tr>\n<tr>\n<td>Plomb</td>\n<td class=\"text-center\">0,5</td>\n</tr>\n<tr>\n<td>Mercure</td>\n<td class=\"text-center\">0,5 (methyl mercury)</td>\n</tr>\n<tr>\n<td>\u00c9tain</td>\n<td class=\"text-center\">200,0</td>\n</tr>\n</tbody>\n</table>\n</div>\n</div>\n\n<h2>Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de  marquage</h2>\n\n<ul>\n<li>Les \u00e9tiquettes doivent \u00eatre indiqu\u00e9es, en anglais, espagnol ou  philippin, le nom de la marque ou le nom commercial, la composition physique ou  chimique, le poids ou le volume en unit\u00e9s m\u00e9triques, le nom et l'adresse de  l'emballeur ou du vendeur et le pays d'origine.</li>\n</ul>\n\n<h2>Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Certificat d'origine et d'hygi\u00e8ne (CFIA/ACIA 5003)</li>\n</ul>\n\n<h2>Autres renseignements</h2>\n\n<p><a href=\"http://www.fda.gov.ph/\"><span lang=\"en\">Food and Drug Administration Philippines</span> (en anglais seulement)</a><br>\n<span lang=\"en\">Civic Drive, Filinvest Corporate City, Alabang<br>\n</span></p>\n\n<p><a href=\"https://www.bfar.da.gov.ph/\"><span lang=\"en\">Bureau of Fisheries and Aquatic Resources</span> (en anglais seulement)</a><br>\n<span lang=\"en\">PCA Building, Commonwealth Avenue, Diliman<br>\n</span></p>\n\n<p><a href=\"http://www.bps.dti.gov.ph/\"><span lang=\"en\">Bureau of Philippine Standards</span> (en anglais seulement)</a> \u2013 <span lang=\"en\">Department of Trade and Industry</span><br>\n<span lang=\"en\">Trade and Industry Building (3<sup>rd</sup> floor)<br>\n<abbr title=\"Senator\">Sen.</abbr> Gil J. Puyat Avenue, Makati City<br>\n</span></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}