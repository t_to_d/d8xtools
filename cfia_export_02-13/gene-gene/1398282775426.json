{
    "dcr_id": "1398282775426",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Alopecurus myosuroides</i> (Slender foxtail)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Alopecurus myosuroides</i> (Vulpin des champs)"
    },
    "html_modified": "2024-02-13 9:49:51 AM",
    "modified": "2017-09-22",
    "issued": "2016-10-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_alopecurus_myosuroides_1398282775426_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_alopecurus_myosuroides_1398282775426_fra"
    },
    "ia_id": "1398282817272",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Alopecurus myosuroides</i> (Slender foxtail)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Alopecurus myosuroides</i> (Vulpin des champs)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Alopecurus myosuroides</i> (Slender foxtail)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Alopecurus myosuroides</i> (Vulpin des champs)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1398282817272",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/alopecurus-myosuroides/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/aegilops-myosuroides/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Alopecurus myosuroides (Slender foxtail)",
            "fr": "Semence de mauvaises herbe : Alopecurus myosuroides (Vulpin des champs)"
        },
        "description": {
            "en": "Fact sheet for Alopecurus myosuroides.",
            "fr": "Fiche de renseignements pour Aegilops myosuroides."
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Alopecurus myosuroides, Poaceae, Slender foxtail",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Alopecurus myosuroides, Poaceae, Vulpin des champs"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants",
            "fr": "cultures,grain,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-10-12",
            "fr": "2016-10-12"
        },
        "modified": {
            "en": "2017-09-22",
            "fr": "2017-09-22"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Alopecurus myosuroides (Slender foxtail)",
        "fr": "Semence de mauvaises herbe : Alopecurus myosuroides (Vulpin des champs)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/slender-foxtail/eng/1331819196984/1331819270890\">Invasive Plant - Slender foxtail (<i lang=\"la\">Alopecurus myosuroides</i>)</a></p>\n\n<h2>Family</h2>\n<p><i lang=\"la\">Poaceae</i></p>\n\n<h2>Common name</h2>\n<p>Slender foxtail</p>\n\n<h2>Regulation</h2>\n<p>Prohibited Noxious, Class 1 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>. All imported and domestic seed must be free of Prohibited Noxious weed seeds.</p>\n<p>Listed on the <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">List of Pests Regulated by Canada</a> established under the <a href=\"/english/reg/jredirect2.shtml?plavega\"><i>Plant Protection Act</i></a>.</p>\n\n\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Previously reported from <abbr title=\"British Columbia\">BC</abbr> and <abbr title=\"Manitoba\">MB</abbr>; however, populations did not persist (<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> 2012<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, Asia and Europe and introduced in Australia, New Zealand, China, North America (<abbr title=\"United States\">U.S.</abbr>, Mexico), and South America (<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> 2014<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Winter annual</p>\n\n<h2>Seed or fruit type</h2>\n<p>Spikelet</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Spikelet length: 5.0 - 8.3 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Spikelet width: 1.5 - 2.8 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Caryopsis length: 2.0 - 3.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Caryopsis width: 1.0 - 2.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Spikelet is long oval; strongly laterally compressed</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Spikelet surface is granular</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Spikelet and caryopsis are brownish-yellow</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Short hairs occur around the callus and on the lower half of the spikelet; the upper half has teeth along the margins</li>\n<li>Round, upturned callus at spikelet base has a \u00a0'sucker mouth' appearance</li>\n<li>Caryopsis is laterally compressed so that the embryo is on the narrow edge</li>\n<li>Spikelet may have a thin awn that is bent near the base</li>\n</ul>\n<h2>Habitat and crop association</h2>\n<p>Cultivated fields, moist meadows, forests and disturbed ground. A significant weed of temperate cereal crops, and also infests canola, grass seed and forage legumes (<abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> 2014<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>General information</h2>\n<p>Slender foxtail is recognized as one of the most damaging weeds in winter cereals in western Europe (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). While the mechanism of introduction into the United States is not clear, specimens of slender foxtail were collected in 1914 (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). It is currently a problem in cereals in western Oregon and eastern Washington, and has also spread into pastures (Lass and Prather 2007<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). It appears to prefer heavy clay and chalk soils, but can tolerate sandy or gravelly soil (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Meadow foxtail (<i lang=\"la\">Alopecurus pratensis</i>)</h3>\n<ul><li>Meadow foxtail spikelets have a similar size, oval shape and brownish colour as slender foxtail spikelets.</li>\n<li>Slender foxtail spikelets have hairs only on lower half and a granular surface while meadow foxtail has a smooth surface and hairs along the length of the spikelet.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_03cnsh_1475586211350_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Slender foxtail (<i lang=\"la\">Alopecurus myosuroides</i>) spikelets and caryopsis</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_05cnsh_1475586395621_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Slender foxtail (<i lang=\"la\">Alopecurus myosuroides</i>) spikelet\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_04cnsh_1475586240427_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Slender foxtail (<i lang=\"la\">Alopecurus myosuroides</i>) spikelet\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_02cnsh_1475586181599_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Slender foxtail (<i lang=\"la\">Alopecurus myosuroides</i>) caryopsis</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_pratensis_02cnsh_1475586456440_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Meadow foxtail (<i lang=\"la\">Alopecurus pratensis</i>) spikelets</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_pratensis_01cnsh_1475586425725_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Meadow foxtail (<i lang=\"la\">Alopecurus pratensis</i>) spikelet</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2012</strong>. Invasive Plant Field Guide. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2014</strong>. Invasive Plants - Fact Sheets. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>., http://www.inspection.gc.ca/eng/1331614724083/1331614823132 [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016.</strong> Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Lass, L. and Prather, T. 2007</strong>. A Scientific Evaluation for Noxious and Invasive Weeds of the Highway 95 Construction Project between the Uniontown Cutoff and Moscow. AquilaVision <abbr title=\"incorporated\">Inc.</abbr>, Missoula, <abbr title=\"Montana\">MT</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/vulpin-des-champs/fra/1331819196984/1331819270890\">Plante envahissante - Vulpin des champs (<i lang=\"la\">Alopecurus myosuroides</i>)</a></p>\n\n<h2>Famille</h2>\n<p><i lang=\"la\">Poaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Vulpin des champs</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible interdite, cat\u00e9gorie 1 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>. Toutes les semences canadiennes et import\u00e9es doivent \u00eatre exemptes de mauvaises herbes nuisibles interdites.</p>\n<p>Figure sur la <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Liste des parasites r\u00e9glement\u00e9s par le Canada</a>, \u00e9tablie en vertu de la <a href=\"/francais/reg/jredirect2.shtml?plavega\"><i>Loi sur la protection des v\u00e9g\u00e9taux</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> L'esp\u00e8ce a d\u00e9j\u00e0 \u00e9t\u00e9 signal\u00e9e en <abbr title=\"Colombie-Britannique\">C.-B.</abbr> et au <abbr title=\"Manitoba\">Man.</abbr>, mais ces populations n'ont pas surv\u00e9cu (<abbr lang=\"en\" title=\"Canadian Food Inspection Agency\">CFIA</abbr> 2012<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Afrique septentrionale, d'Asie et d'Europe et introduite en Australie, en Nouvelle-Z\u00e9lande, en Chine et en Am\u00e9rique du Nord (\u00c9tats-Unis, Mexique) et en Am\u00e9rique du Sud (<abbr lang=\"en\" title=\"Canadian Food Inspection Agency\">CFIA</abbr>, 2014<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle d'hiver</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>\u00c9pillet</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'\u00e9pillet\u00a0: 5,0 \u00e0 8,3  <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de l'\u00e9pillet\u00a0: 1,5 \u00e0 2,8 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Longueur du caryopse\u00a0: 2,0 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur du caryopse\u00a0: 1,0 \u00e0 2,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>\u00c9pillet de forme elliptique allong\u00e9e; fortement comprim\u00e9 lat\u00e9ralement</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Surface de l\u2019\u00e9pillet granuleuse</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>\u00c9pillet et caryopse de couleur jaune brun\u00e2tre</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Pr\u00e9sence de poils courts autour du callus et sur la moiti\u00e9 inf\u00e9rieure de l'\u00e9pillet; les marges de la moiti\u00e9 sup\u00e9rieure de l'\u00e9pillet sont dentel\u00e9es</li>\n<li>Le callus \u00e0 la base de l'\u00e9pillet est arrondi et recourb\u00e9, ressemblant \u00e0 un su\u00e7oir</li>\n<li>Le caryopse \u00e9tant comprim\u00e9 lat\u00e9ralement, l'embryon se trouve sur le bord mince</li>\n<li>L'\u00e9pillet a parfois une mince ar\u00eate qui est coud\u00e9e pr\u00e8s de la base</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, pr\u00e9s humides, for\u00eats et terrains perturb\u00e9s. Importante mauvaise herbe des c\u00e9r\u00e9ales cultiv\u00e9es en zones temp\u00e9r\u00e9es; infeste \u00e9galement le canola, les gramin\u00e9es de semence et les l\u00e9gumineuses fourrag\u00e8res (<abbr lang=\"en\" title=\"Canadian Food Inspection Agency\">CFIA</abbr>, 2014<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le vulpin des champs est consid\u00e9r\u00e9 comme l\u2019une des mauvaises herbes les plus nuisibles des c\u00e9r\u00e9ales d'hiver en Europe occidentale (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>). M\u00eame si on ignore comment le vulpin des champs a \u00e9t\u00e9 introduit aux \u00c9tats-Unis, des sp\u00e9cimens de cette plante ont \u00e9t\u00e9 r\u00e9colt\u00e9s en 1914 (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>). Il est actuellement probl\u00e9matique dans les c\u00e9r\u00e9ales dans l'Ouest de l'Oregon et dans l'Est de l\u2019\u00c9tat de Washington, et il se propage aussi dans les p\u00e2turages (Lass et Prather, 2007<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>). Il semble pr\u00e9f\u00e9rer les sols argileux lourds et les sols crayeux, mais il tol\u00e8re les sols sableux ou graveleux (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Vulpin des pr\u00e9s (<i lang=\"la\">Alopecurus pratensis</i>)</h3>\n<ul><li>Les \u00e9pillets du vulpin des pr\u00e9s ressemblent \u00e0 ceux du vulpin des champs par leurs dimensions, leur forme elliptique et leur couleur brun\u00e2tre.</li>\n<li>Les \u00e9pillets du vulpin des champs sont pubescents seulement sur leur moiti\u00e9 inf\u00e9rieure et ont une surface granuleuse alors que les \u00e9pillets du vulpin des pr\u00e9s ont une surface lisse et sont pubescents sur toute leur longueur.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_03cnsh_1475586211350_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Vulpin des champs (<i lang=\"la\">Alopecurus myosuroides</i>) \u00e9pillets et caryopse\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_05cnsh_1475586395621_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Vulpin des champs (<i lang=\"la\">Alopecurus myosuroides</i>) \u00e9pillet\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_04cnsh_1475586240427_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Vulpin des champs (<i lang=\"la\">Alopecurus myosuroides</i>) \u00e9pillet\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_myosuroides_02cnsh_1475586181599_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Vulpin des champs (<i lang=\"la\">Alopecurus myosuroides</i>) caryopse\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_pratensis_02cnsh_1475586456440_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Vulpin des pr\u00e9s (<i lang=\"la\">Alopecurus pratensis</i>) \u00e9pillets</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_alopecurus_pratensis_01cnsh_1475586425725_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Vulpin des pr\u00e9s (<i lang=\"la\">Alopecurus pratensis</i>) \u00e9pillet</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2012</strong>. Invasive Plant Field Guide. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. 2014</strong>. Invasive Plants - Fact Sheets. Canadian Food Inspection Agency, Ottawa, <abbr title=\"Ontario\">ON</abbr>., http://www.inspection.gc.ca/eng/1331614724083/1331614823132 [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016.</strong> Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Lass, L. and Prather, T. 2007</strong>. A Scientific Evaluation for Noxious and Invasive Weeds of the Highway 95 Construction Project between the Uniontown Cutoff and Moscow. AquilaVision Inc., Missoula, MT.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}