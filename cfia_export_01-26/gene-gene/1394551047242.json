{
    "dcr_id": "1394551047242",
    "title": {
        "en": "Transporting animals during cold weather",
        "fr": "Transport des animaux lorsque les conditions m\u00e9t\u00e9orologiques sont froides"
    },
    "html_modified": "2024-01-26 2:22:19 PM",
    "modified": "2020-08-18",
    "issued": "2014-03-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_transport_20140311ind_1394551047242_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_transport_20140311ind_1394551047242_fra"
    },
    "ia_id": "1394551048336",
    "parent_ia_id": "1300460096845",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Humane Transport",
        "fr": "Transport sans cruaut\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1300460096845",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Transporting animals during cold weather",
        "fr": "Transport des animaux lorsque les conditions m\u00e9t\u00e9orologiques sont froides"
    },
    "label": {
        "en": "Transporting animals during cold weather",
        "fr": "Transport des animaux lorsque les conditions m\u00e9t\u00e9orologiques sont froides"
    },
    "templatetype": "content page 1 column",
    "node_id": "1394551048336",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300460032193",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/humane-transport/cold-weather/",
        "fr": "/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/conditions-meteorologiques-froides/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Transporting animals during cold weather",
            "fr": "Transport des animaux lorsque les conditions m\u00e9t\u00e9orologiques sont froides"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) reminds anyone involved in handling or transporting animals to take appropriate measures to protect animals from extreme temperatures.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) rappelle \u00e0 toute personne qui manipule ou transporte des animaux qu'elle doit prendre les mesures appropri\u00e9es pour prot\u00e9ger les animaux contre les temp\u00e9ratures extr\u00eames."
        },
        "keywords": {
            "en": "animals, welfare, protection, humane transport, livestock, animal transporters, animal producers, Transporting animals during cold weather",
            "fr": "animaux, bien-\u00eatre, protection, transport sans cruaut\u00e9, b\u00e9tail, transformateurs, transporteurs, \u00e9leveurs, Transport des animaux lorsque les conditions m\u00e9t\u00e9orologiques sont froides"
        },
        "dcterms.subject": {
            "en": "press releases,inspection,animal health",
            "fr": "communiqu\u00e9 de presse,inspection,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-03-11",
            "fr": "2014-03-11"
        },
        "modified": {
            "en": "2020-08-18",
            "fr": "2020-08-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Transporting animals during cold weather",
        "fr": "Transport des animaux lorsque les conditions m\u00e9t\u00e9orologiques sont froides"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=34#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The Canadian Food Inspection Agency (CFIA) reminds anyone involved in handling or transporting animals to take appropriate measures to protect animals from extreme temperatures.</p>\n\n<p>Every person transporting animals by any means in Canada is legally obligated to ensure that the entire trip\u00a0\u2013 including loading, confining, transporting, and unloading\u00a0\u2013 does not result in injury, suffering, or death to animals. Regulations apply to all groups involved in the loading, transporting and receiving of animals including owners and producers, processors, assembly centres, feedlots, shippers and drivers.</p>\n\n<p>During the winter months, extra measures should be taken to help protect every animal from the cold temperature and elements. These can include:</p>\n\n<ul>\n<li>adjusting loading density</li>\n<li>providing additional bedding or insulation</li>\n<li>increasing weather protection for animals on vehicles</li>\n<li>delaying transport to wait for warmer temperatures</li>\n</ul>\n\n<p>Transporters should consult the National Farm Animal Care Council's <a href=\"http://www.nfacc.ca/codes-of-practice/transport/code\">Recommended code of practice for the care and handling of farm animals\u00a0\u2013 Transportation</a>.</p>\n\n<p>The majority of Canadian producers and transporters are committed to treating animals humanely. However, in circumstances where animals are not appropriately cared for, the CFIA will not hesitate to take enforcement actions which may include license suspensions, issuance of charges, fines and possible prosecutions under various federal regulations. The CFIA also works closely with provincial authorities in cases where provincial animal welfare regulations are contravened.</p>\n\n<p>Learn more about <a href=\"/animal-health/terrestrial-animals/humane-transport/eng/1300460032193/1300460096845\">humane transport and animal welfare</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=34#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'Agence canadienne d'inspection des aliments (ACIA) rappelle \u00e0 toute personne qui manipule ou transporte des animaux qu'elle doit prendre les mesures appropri\u00e9es pour prot\u00e9ger les animaux contre les temp\u00e9ratures extr\u00eames.</p>\n\n<p>Toute personne assurant le transport d'animaux au Canada, par quelque moyen que ce soit, est tenue par la loi de veiller \u00e0 ce que les animaux soient embaruq\u00e9s, confin\u00e9s, transport\u00e9s et d\u00e9barqu\u00e9s d'une mani\u00e8re qui n'entra\u00eene pas de blessures, de souffrances inutiles ou de d\u00e9c\u00e8s. La r\u00e9glementation s'applique \u00e0 tous les groupes qui sont impliqu\u00e9s dans l'embarquement, le transport et la r\u00e9ception des animaux, notamment les propri\u00e9taires, les producteurs, les transformateurs, les centres de rassemblement et les parcs d'engraissement, les exp\u00e9diteurs et les conducteurs.</p>\n\n<p>Durant l'hiver, des mesures suppl\u00e9mentaires doivent \u00eatre prises afin de prot\u00e9ger tous les animaux contre les \u00e9l\u00e9ments et temp\u00e9ratures froides. Voici quelques exemples\u00a0:</p>\n\n<ul>\n<li>modifier la densit\u00e9 de chargement en cons\u00e9quence;</li>\n<li>pr\u00e9voir une plus grande quantit\u00e9 de liti\u00e8re ou une meilleure isolation;</li>\n<li>accro\u00eetre la protection des animaux contre le froid \u00e0 bord des v\u00e9hicules;</li>\n<li>retarder le transport jusqu'\u00e0 ce que la temp\u00e9rature se r\u00e9chauffe.</li>\n</ul>\n\n<p>Les <a href=\"http://www.nfacc.ca/codes-de-pratiques/transport/code\">transporteurs devraient consulter le Code de pratiques pour le soin et la manipulation des animaux de ferme</a> du Conseil national pour les soins aux animaux d'\u00e9levage.</p>\n\n<p>La majorit\u00e9 des producteurs et des transporteurs canadiens sont d\u00e9termin\u00e9s \u00e0 traiter les animaux sans cruaut\u00e9. Cependant, lorsque des animaux ne sont pas ad\u00e9quatement trait\u00e9s, l'ACIA n'h\u00e9sitera pas \u00e0 prendre des mesures d'application de la loi, dont la suspension de permis, l'imposition d'amendes et l'engagement de poursuites en vertu de divers r\u00e8glements f\u00e9d\u00e9raux. L'ACIA travaille aussi \u00e9troitement avec les autorit\u00e9s provinciales dans les cas o\u00f9 les r\u00e8glements provinciaux sur le bien-\u00eatre des animaux ne sont pas respect\u00e9s.</p>\n<p>Informez-vous sur le <a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/fra/1300460032193/1300460096845\">transport sans cruaut\u00e9 et le bien-\u00eatre des animaux</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}