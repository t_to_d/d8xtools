{
    "dcr_id": "1567706052455",
    "title": {
        "en": "Meat products containing shellfish",
        "fr": "Produits de viande qui contiennent des mollusques"
    },
    "html_modified": "2024-01-26 2:24:49 PM",
    "modified": "2019-09-20",
    "issued": "2019-09-20",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meat_products_shellfish_b_1567706052455_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meat_products_shellfish_b_1567706052455_fra"
    },
    "ia_id": "1567706052799",
    "parent_ia_id": "1536170495320",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food|Making food - Manufacturing|processing|preparing|preserving|treating",
        "fr": "Importation d\u2019aliments|Production d'aliments - Fabrication|transformation|conditionnement|pr\u00e9servation et traitement"
    },
    "commodity": {
        "en": "Fish and seafood|Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande|Poissons et fruits de mer"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1536170495320",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Meat products containing shellfish",
        "fr": "Produits de viande qui contiennent des mollusques"
    },
    "label": {
        "en": "Meat products containing shellfish",
        "fr": "Produits de viande qui contiennent des mollusques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1567706052799",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1536170455757",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/meat-products-containing-shellfish/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/produits-de-viande-qui-contiennent-des-mollusques/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Meat products containing shellfish",
            "fr": "Produits de viande qui contiennent des mollusques"
        },
        "description": {
            "en": "Requirements that apply to a meat product containing shellfish.",
            "fr": "Exigences qui s'appliquent \u00e0 un produit de viande qui contient des mollusques."
        },
        "keywords": {
            "en": "Food-specific requirements and guidance, Meat products, food animals, Safe Food for Canadians Regulations, SFCR, shellfish",
            "fr": "Exigences et directives sp\u00e9cifiques aux aliments, produits de viande, animaux destin\u00e9s \u00e0 l'alimentation, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, mollusques et crustac\u00e9s"
        },
        "dcterms.subject": {
            "en": "food,food labeling,food inspection,legislation,agri-food products,regulation,food safety,food processing",
            "fr": "aliment,\u00e9tiquetage des aliments,inspection des aliments,l\u00e9gislation,produit agro-alimentaire,r\u00e9glementation,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-09-20",
            "fr": "2019-09-20"
        },
        "modified": {
            "en": "2019-09-20",
            "fr": "2019-09-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Meat products containing shellfish",
        "fr": "Produits de viande qui contiennent des mollusques"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-tp-lg\">A person who imports a meat product containing raw <a href=\"/eng/1430250286859/1430250287405#a155\">shellfish</a> (excluding the adductor muscles of scallops or the meat of geoduck) must ensure that the shellfish originate from a country with a shellfish inspection system recognized by the Canadian Food Inspection Agency under Part 7 of the <i>Safe Food for Canadians Regulations</i> (SFCR). </p>\n\n<ul>\n<li class=\"mrgn-bttm-lg\">Refer to <a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/importing-fish-and-shellfish/molluscan-shellfish/eng/1377987441620/1377987693551\">Importing Live and Raw Molluscan Shellfish</a> for a list of the authorized countries and approved species of molluscan shellfish.</li>\n</ul>\n\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Keep in mind!</h2>\n</header>\n<div class=\"panel-body\">\n<p class=\"mrgn-bttm-sm\">When applying for a Safe Food for Canadians (SFC licence) to import meat products containing <a href=\"/eng/1430250286859/1430250287405#a79\">fish</a>, you should only select the commodity \"meat and poultry products.\"</p>\n</div>\n</section>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-tp-lg\">Une personne qui importe un produit de viande qui contient des <a href=\"/fra/1430250286859/1430250287405#a155\">mollusques</a> crus (\u00e0 l'exception du muscle adducteur du p\u00e9toncle et de la chair de panope) doit s'assurer que les mollusques proviennent d'un pays dont le syst\u00e8me d'inspection des mollusques a \u00e9t\u00e9 reconnu par l'Agence canadienne d'inspection des aliments au titre de la partie 7 du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada </i>(RSAC).</p>\n\n<ul>\n<li class=\"mrgn-bttm-lg\">Vous trouverez une liste des pays autoris\u00e9s et des esp\u00e8ces approuv\u00e9es de mollusques \u00e0 la page <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/importation-de-poisson-et-de-mollusques/mollusques/fra/1377987441620/1377987693551\">Importation des mollusques vivants et crus</a>.</li>\n</ul>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">\u00c0 retenir!</h2>\n</header>\n<div class=\"panel-body\">\n<p class=\"mrgn-bttm-sm\">Lorsque vous pr\u00e9senter une demande de licence pour la salubrit\u00e9 des aliments au Canada (licence SAC) pour importer un produit de viande qui contient du <a href=\"/fra/1430250286859/1430250287405#a79\">poisson</a>, vous devez seulement s\u00e9lectionner la cat\u00e9gorie \u00ab\u00a0produits de viande et de volaille\u00a0\u00bb.</p>\n</div>\n</section>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}