{
    "dcr_id": "1476276753781",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Centaurea diffusa</i> (Diffuse knapweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Centaurea diffusa</i> (Centaur\u00e9e diffuse)"
    },
    "html_modified": "2024-01-26 2:23:26 PM",
    "modified": "2017-09-22",
    "issued": "2017-09-22",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_centaurea_diffusa_1476276753781_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_centaurea_diffusa_1476276753781_fra"
    },
    "ia_id": "1476276754280",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Centaurea diffusa</i> (Diffuse knapweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Centaurea diffusa</i> (Centaur\u00e9e diffuse)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Centaurea diffusa</i> (Diffuse knapweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Centaurea diffusa</i> (Centaur\u00e9e diffuse)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476276754280",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/centaurea-diffusa/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/centaurea-diffusa/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Centaurea diffusa (Diffuse knapweed)",
            "fr": "Semence de mauvaises herbe : Centaurea diffusa (Centaur\u00e9e diffuse)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Centaurea diffusa, Asteraceae, Diffuse knapweed",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Centaurea diffusa, Asteraceae, Centaur\u00e9e diffuse"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-09-22",
            "fr": "2017-09-22"
        },
        "modified": {
            "en": "2017-09-22",
            "fr": "2017-09-22"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Centaurea diffusa (Diffuse knapweed)",
        "fr": "Semence de mauvaises herbe : Centaurea diffusa (Centaur\u00e9e diffuse)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Common name</h2>\n<p>Diffuse knapweed</p>\n\n<h2>Regulation</h2>\n<p>Prohibited Noxious, Class 1 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>. All imported and domestic seed must be free of Prohibited Noxious weed seeds.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"Alberta\">AB</abbr>, <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr>, <abbr title=\"Saskatchewan\">SK </abbr>(Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to southeastern Europe and western Asia and it has expanded beyond its native range into central and western Europe and has also been introduced into North America and Argentina (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Biennial or short-lived perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Achene length: 1.4 - 3.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width: 0.7 - 1.5  <abbr title=\"millimetres\">mm</abbr></li>\n<li>Pappus length: 0.1 - 1.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene is oblong with rounded bottom and flat top, laterally compressed; large notch near the base</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Achene has smooth, dull surface texture</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Pale, reddish-brown, occasionally dark greenish-brown achene with pale markings: a strong central stripe within lines to either side and a pale area around the basal notch</li>\n</ul>\n\n<h2>Habitat and crop association</h2>\n<p>Fields, rangelands, grasslands, dunes, open forests, roadsides, railway lines, gravel pits, industrial sites and other disturbed areas (Darbyshire 2003<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, <abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). A serious weed of alfalfa in its native range but mainly a weed of rangelands (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>General information</h2>\n<p>Diffuse knapweed can be found over a wide range of soil and environmental conditions but cannot tolerate cultivation or excessive moisture (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Diffuse knapweed produces large numbers of seeds that are dispersed by wind, water, and animals. Plant stems may break off to become tumbleweeds, dispersing seeds over large distances (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn2e-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Spotted knapweed (<i lang=\"la\">Centurea stoebe</i>)</h3>\n<ul><li>Spotted knapweed achenes have a similar size, oblong shape, pappus and dark colouring with pale stripes as diffuse knapweed.</li>\n<li>Spotted knapweed achenes are wider, with a prominent central stripe and a pale area around the top compared to diffuse knapweed. The brown colour of diffuse knapweed achenes is usually reddish while spotted knapweed is greenish. The pappus is also longer by up to 2.0 <abbr title=\"millimetres\">mm</abbr> in spotted knapweed.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_05cnsh_1475593526841_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Diffuse knapweed (<i lang=\"la\">Centurea diffusa</i>) achenes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_06cnsh_1475593552091_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Diffuse knapweed (<i lang=\"la\">Centurea diffusa</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_03cnsh_1475593491701_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Diffuse knapweed (<i lang=\"la\">Centurea diffusa</i>) achene, basal notch\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_07cnsh_1475593574716_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Diffuse knapweed (<i lang=\"la\">Centurea diffusa</i>) flower head\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_copyright_1475593626514_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Diffuse knapweed (<i lang=\"la\">Centurea diffusa</i>) achenes\n</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_stoebe_05cnsh_1475593869099_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Spotted knapweed (<i lang=\"la\">Centurea stoebe</i>) achenes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_stoebe_01cnsh_1475593792749_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Spotted knapweed (<i lang=\"la\">Centurea stoebe</i>) achene</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Centaur\u00e9e diffuse</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible interdite, cat\u00e9gorie 1 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>. Toutes les semences canadiennes et import\u00e9es doivent \u00eatre exemptes de mauvaises herbes nuisibles interdites.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente en <abbr title=\"Alberta\">Alb.</abbr>, en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr>, au <abbr title=\"Qu\u00e9bec\">Qc</abbr> et en <abbr title=\"Saskatchewan\">Sask.</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Europe du Sud-Est et de l'Asie occidentale et s'est propag\u00e9e \u00e0 l'ext\u00e9rieur de son aire d'indig\u00e9nat en Europe centrale et occidentale et a \u00e9t\u00e9 introduite en Am\u00e9rique du Nord et en Argentine (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Bisannuelle ou vivace de courte p\u00e9rennit\u00e9</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'ak\u00e8ne\u00a0: 1,4 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de l'ak\u00e8ne\u00a0: 0,7 \u00e0 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Longueur du pappus 0,1 \u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne de forme oblongue \u00e0 base arrondie et \u00e0 sommet plat; comprim\u00e9 lat\u00e9ralement; une grande encoche est pr\u00e9sente pr\u00e8s de la base</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>L\u2019ak\u00e8ne a une texture de surface lisse et mate</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>L\u2019ak\u00e8ne est de couleur brun rouge\u00e2tre, parfois brun verd\u00e2tre fonc\u00e9 avec   des marques p\u00e2les\u00a0: il a une bande centrale bien marqu\u00e9e qui est   flanqu\u00e9e de lignes p\u00e2les de chaque c\u00f4t\u00e9 et il pr\u00e9sente une zone p\u00e2le   autour de l'encoche basale</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs, parcours, prairies, dunes, for\u00eats clairsem\u00e9es, bords de chemin, voies ferr\u00e9es, gravi\u00e8res, sites industriels et autres terrains perturb\u00e9s (Darbyshire, 2003<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Importante adventice de la luzerne dans son aire d\u2019indig\u00e9nat, mais cette mauvaise herbe infeste principalement les parcours (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La centaur\u00e9e diffuse se rencontre dans divers sols et conditions environnementales, mais elle ne tol\u00e8re pas le travail du sol ni les conditions d'humidit\u00e9 excessives (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). La centaur\u00e9e diffuse produit un grand nombre de graines qui sont dispers\u00e9es par le vent, l'eau et les animaux. Les tiges peuvent se briser et former des \u00ab\u00a0virevoltants\u00a0\u00bb et diss\u00e9miner ainsi des graines sur de grandes distances (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn2e-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Centaur\u00e9e macul\u00e9e (<i lang=\"la\">Centurea stoebe</i>)</h3>\n<ul><li>Les ak\u00e8nes de la centaur\u00e9e macul\u00e9e ressemblent \u00e0 ceux de la centaur\u00e9e diffuse par leurs dimensions, leur forme oblongue, leurs pappus et leur couleur fonc\u00e9e avec des bandes p\u00e2les.</li>\n<li>Les ak\u00e8nes de la centaur\u00e9e macul\u00e9e sont plus larges que ceux de la centaur\u00e9e diffuse, ils ont une bande centrale marqu\u00e9e et une zone p\u00e2le autour du sommet. Les ak\u00e8nes de la centaur\u00e9e diffuse sont brun rouge\u00e2tre, tandis que ceux de la centaur\u00e9e macul\u00e9e sont brun verd\u00e2tre. Le pappus de la centaur\u00e9e macul\u00e9e est aussi plus long (\u00e9cart jusqu'\u00e0 2,0 <abbr title=\"millim\u00e8tre\">mm</abbr>) que celui de la centaur\u00e9e diffuse.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_05cnsh_1475593526841_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Centaur\u00e9e diffuse (<i lang=\"la\">Centurea diffusa</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_06cnsh_1475593552091_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Centaur\u00e9e diffuse (<i lang=\"la\">Centurea diffusa</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_03cnsh_1475593491701_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Centaur\u00e9e diffuse(<i lang=\"la\">Centurea diffusa</i>) ak\u00e8ne, l'encoche basale\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_07cnsh_1475593574716_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Centaur\u00e9e diffuse (<i lang=\"la\">Centurea diffusa</i>), t\u00eate de fleur\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_diffusa_copyright_1475593626514_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Centaur\u00e9e diffuse (<i lang=\"la\">Centurea diffusa</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_stoebe_05cnsh_1475593869099_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Centaur\u00e9e macul\u00e9e (<i lang=\"la\">Centurea stoebe</i>) ak\u00e8nes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_centaurea_stoebe_01cnsh_1475593792749_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Centaur\u00e9e macul\u00e9e (<i lang=\"la\">Centurea stoebe</i>) ak\u00e8nes</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}