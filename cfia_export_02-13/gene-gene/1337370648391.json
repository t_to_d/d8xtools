{
    "dcr_id": "1337370648391",
    "title": {
        "en": "Pest Alert Emerald Ash Borer (<span lang=\"la\">Agrilus planipennis</span>)",
        "fr": "Avertissement phytosanitaire Agrile du fr\u00eane (<span lang=\"la\">Agrilus planipennis</span>)"
    },
    "html_modified": "2024-02-13 9:47:34 AM",
    "modified": "2018-07-24",
    "issued": "2012-05-18 15:51:04",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_agrpla_alert_1337370648391_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_agrpla_alert_1337370648391_fra"
    },
    "ia_id": "1337370752179",
    "parent_ia_id": "1337273975030",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1337273975030",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pest Alert Emerald Ash Borer (<span lang=\"la\">Agrilus planipennis</span>)",
        "fr": "Avertissement phytosanitaire Agrile du fr\u00eane (<span lang=\"la\">Agrilus planipennis</span>)"
    },
    "label": {
        "en": "Pest Alert Emerald Ash Borer (<span lang=\"la\">Agrilus planipennis</span>)",
        "fr": "Avertissement phytosanitaire Agrile du fr\u00eane (<span lang=\"la\">Agrilus planipennis</span>)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1337370752179",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1337273882117",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/emerald-ash-borer/pest-alert/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/avertissement-phytosanitaire/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pest Alert Emerald Ash Borer (Agrilus planipennis)",
            "fr": "Avertissement phytosanitaire Agrile du fr\u00eane (Agrilus planipennis)"
        },
        "description": {
            "en": "The Emerald Ash Borer (EAB) is a highly destructive insect pest of ash trees. Native to eastern Asia, this pest was first discovered in Canada and the U.S. in 2002.",
            "fr": "L\u2019agrile du fr\u00eane est un insecte ravageur qui cause de graves dommages aux fr\u00eanes. Originaire de l\u2019Asie orientale, on l\u2019a d\u00e9tect\u00e9 pour la premi\u00e8re fois au Canada et aux \u00c9tats-Unis en 2002."
        },
        "keywords": {
            "en": "Pest Alert, Emerald Ash Borer, EAB, Agrilus planipennis, Signs of infestation",
            "fr": "Avertissement phytosanitaire, Agrile du fr&#234;ne, Agrilus planipennis, Signes d&#8217;infestation"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-18 15:51:04",
            "fr": "2012-05-18 15:51:04"
        },
        "modified": {
            "en": "2018-07-24",
            "fr": "2018-07-24"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pest Alert Emerald Ash Borer (Agrilus planipennis)",
        "fr": "Avertissement phytosanitaire Agrile du fr\u00eane (Agrilus planipennis)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"text-right small\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/pestrava_agrpla_alert_pdf_1337369655573_eng.pdf\" title=\"Pest Alert Emerald Ash Borer (Agrilus planipennis) in portable document format\">PDF (417 <abbr title=\"kilobyte\">kb</abbr>)</a></p>\n\n<figure> \n<img alt=\"image - Pest Alert Emerald Ash Borer. Description follows.\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_agrpla_alert_image1_1337369458142_eng.jpg\"> \n<details> \n<summary>Description of image - Pest Alert Emerald Ash Borer (<span lang=\"la\">Agrilus planipennis</span>)</summary>\n<p>The Emerald Ash Borer (EAB) is a highly destructive insect pest of ash trees. Native to eastern Asia, this pest was first discovered in Canada and the <abbr title=\"United States\">U.S.</abbr> in 2002. The <abbr title=\"Emerald Ash Borer\">EAB</abbr> has killed millions of ash trees in Southwestern Ontario, Michigan and surrounding states, and poses a major economic and environmental threat to urban and forested areas in both countries. The <abbr title=\"Emerald Ash Borer\">EAB</abbr> attacks and kills all species of ash (except Mountain ash which is not a true ash).</p>\n<p><strong>How does the <abbr title=\"Emerald Ash Borer\">EAB</abbr> spread?</strong></p>\n<p>While the <abbr title=\"Emerald Ash Borer\">EAB</abbr> can fly up to several kilometres, another significant factor contributing to its spread is the movement of firewood, nursery stock, trees, logs, lumber, wood with bark attached and wood or bark chips.</p>\n<p><strong>Signs of infestation</strong></p>\n<p>Tree decline, including:</p>\n<ul>\n<li>thinning crown</li>\n<li>diminished density of leaves</li>\n<li>evidence of adult beetle feeding on leaves</li>\n<li>long shoots growing from the trunk or branches</li>\n<li>vertical cracks in the trunk</li>\n<li>small D-shaped emergence holes</li>\n<li>S-shaped tunnels under the bark filled with fine sawdust</li>\n</ul>\n<p><strong>Contact the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></strong></p>\n<p>If you see suspected signs of infestation on your ash trees or if you plan on moving firewood (<abbr title=\"for example\">e.g.</abbr>, when camping, relocating or managing a woodlot), contact the Canadian Food Inspection Agency (CFIA) for more information.</p>\n<p><strong>Help Protect Canada's Trees and Forests.</strong></p>\n<p>www.inspection.gc.ca/pests</p>\n</details> \n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"text-right small\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/pestrava_agrpla_alert_pdf_1337369655573_fra.pdf\" title=\"Avertissement phytosanitaire Agrile du fr\u00eane (Agrilus planipennis) en format de document portable\">PDF (542 <abbr title=\"kilo-octet\">ko</abbr>)</a></p>\n<figure> <img alt=\"image - Agrile du fr\u00eane. Description ci-dessous.\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_agrpla_alert_image1_1337369458142_fra.jpg\"> \n<details> \n<summary>Description de l'image - Avertissement phytosanitaire Agrile du fr\u00eane (<span lang=\"la\">Agrilus planipennis</span>)</summary>\n<p>L'agrile du fr\u00eane est un insecte ravageur qui cause de graves dommages aux fr\u00eanes. Originaire de l'Asie orientale, on l'a d\u00e9tect\u00e9 pour la premi\u00e8re fois au Canada et aux \u00c9tats-Unis en 2002. L'agrile a tu\u00e9 des millions de fr\u00eanes dans le Sud-Ouest de l'Ontario, le Michigan et ses \u00c9tats avoisinants. Ce ravageur constitue une menace grave pour l'\u00e9conomie et l'environnement dans les zones urbaines et foresti\u00e8res des deux pays. L'agrile du fr\u00eane attaque et tue toutes les essences de fr\u00eanes (sauf le sorbier d'Am\u00e9rique qui n'est pas apparent\u00e9 au fr\u00eane m\u00eame si son nom commun anglais est mountain ash).</p>\n<p><strong>Comment se propage-t-il?</strong></p>\n<p>L'agrile du fr\u00eane peut effectuer des vols de plusieurs kilom\u00e8tres, mais un autre facteur important qui contribue \u00e0 sa propagation est le transport du bois de chauffage, du mat\u00e9riel de p\u00e9pini\u00e8re, des arbres, des billes, du bois non \u00e9corc\u00e9 et des copeaux de bois ou d'\u00e9corce.</p>\n<p><strong>Signes d'infestation</strong></p>\n<p>D\u00e9p\u00e9rissement de l'arbre, et notamment :</p>\n<ul>\n<li>\u00e9claircissement de la couronne de l'arbre</li>\n<li>diminution de la densit\u00e9 du feuillage</li>\n<li>traces laiss\u00e9es par l'insecte adulte lorsqu'il se nourrit des feuilles</li>\n<li>prolif\u00e9ration de gourmands sur le tronc ou les branches</li>\n<li>fentes verticales sur le tronc</li>\n<li>petits trous de sortie en forme de D</li>\n<li>galeries (en S) sous l'\u00e9corce remplies de sciure fine</li>\n</ul>\n<p><strong>Contactez l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></strong></p>\n<p>Si vous observez des signes suspects d'infestation sur vos fr\u00eanes ou si vous pr\u00e9voyez transporter du bois de chauffage (<abbr title=\"par exemple\">p. ex.</abbr>, lors d'une excursion de camping, d'un d\u00e9m\u00e9nagement ou de l'am\u00e9nagement d'un bois\u00e9), communiquez avec l'Agence canadienne d'inspection des aliments (ACIA) pour obtenir de plus amples renseignements.</p>\n<p><strong>Aidez-nous \u00e0 prot\u00e9ger les arbres et les for\u00eats du Canada.</strong></p>\n<p>www.inspection.gc.ca/phytoravageurs</p>\n</details> \n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}