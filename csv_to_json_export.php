<?php


if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . '  path/to/d8xtools/z_cfia_raw/export_with_ia_en_and_fr.csv \t' . "\n";
  echo "originally assume the header as follows \"dcr_id\"	\"langcode\"	\"title\"	\"html_modified\"	\"modified\"	\"issued\"	\"teamsite_location\"      \"parent_ia_id\"      \"ia_id\"\n";
  echo "Then was assuming the header will be          \"dcr_id\"	\"langcode\"	\"title\"	\"html_modified\"	\"modified\"	\"issued\"	\"teamsite_location\"	\"ia_id\"	\"parent_ia_id\"\n";
  echo "Newest   assuming the header will be          \"dcr_id\"	\"langcode\"	\"title\"	\"html_modified\"	\"modified\"	\"issued\"	\"teamsite_location\"	\"ia_id\"	\"parent_ia_id\"	\"chronicletopic\"	\"chroniclecontent\"	\"chronicleaudience\"	\"sfcrdocumenttype\"	\"sfcraudience\"	\"sfcrmethodofproduction\"	\"activity\"	\"commodity\"	\"program\"	\"search_type\"\n";
  die;
}

$file = file_get_contents($argv[1]);
$csv = csv_to_array($argv[1]);
function csv_to_array($filename='', $delimiter="\t")
{
  if (!file_exists($filename) || !is_readable($filename)) {
    return FALSE;
  }

    $header = NULL;
    $data = array();
    $home_row_en = [];
    $home_row_fr = [];
    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
      {
        if (!$header) {
          $new_row = [];
          foreach ($row as $cle => $colonne) {
            if (is_string($colonne)) {
              $colonne = strtolower($colonne);
            }
            $new_row[$cle] = $colonne;
          }
          $new_row[7] = 'ia_id';
          $new_row[8] = 'parent_ia_id';
          $new_row[9] = 'chronicletopic';
          $new_row[10] = 'chroniclecontent';
          $new_row[11] = 'chronicleaudience';
          $new_row[12] = 'sfcrdocumenttype';
          $new_row[13] = 'sfcraudience';
          $new_row[14] = 'sfcrmethodofproduction';
          $new_row[15] = 'activity';
          $new_row[16] = 'commodity';
          $new_row[17] = 'program';
          $new_row[18] = 'search_type';
          $new_row[19] = 'parent_node_id';
          $new_row[20] = 'layout_name';
          $new_row[21] = 'breadcrumb';
          $new_row[22] = 'label';
          $new_row[23] = 'templatetype';
          $new_row[24] = 'node_id';
          $new_row[25] = 'managing_branch';
          $new_row[26] = 'type_name';
          $new_row[27] = 'dcr_type';
          $new_row[28] = 'parent_dcr_id';
          $row = $new_row;
          $header = $row;
          echo print_r($header, TRUE);
        }
        else {
          $temp_row = $row;
//        $temp_row[8] = NULL;
//        $temp_row[9] = NULL;
//        $temp_row[10] = NULL;
//        $temp_row[11] = NULL;
//        $temp_row[12] = NULL;
//        $temp_row[13] = NULL;
//        $temp_row[14] = NULL;
//        $temp_row[15] = NULL;
//        $temp_row[16] = NULL;
//        $temp_row[17] = NULL;
//        $temp_row[18] = NULL;
          $temp_row[19] = NULL;
          $temp_row[20] = NULL;
          $temp_row[21] = NULL;
          $temp_row[22] = $row[2]; // Title is element 2.
          $temp_row[23] = NULL;
          $temp_row[24] = $row[7];
          $temp_row[25] = NULL;
          $temp_row[26] = $temp_row[2];
          $temp_row[27] = NULL;
          $temp_row[28] = NULL;
          $temp_row = array_combine($header, $temp_row);
          // CFIA fake this out also.
          $temp_row['templatetype'] = 'content page 1 column';
          // CFIA fake out this as well for now, processing for later, default to the home page as parent for everything..
          // For cfia, fake this out for now, crawl the breadcrumb to figure this out.
          // The export file didn't have the hierarchy so figure it out with scripts and breadcrumb.
          //$temp_row['parent_node_id'] = NULL;
          $path_tmp = $temp_row['teamsite_location'];
          $segments = explode("/", $path_tmp);
          $idx_type = 0;
          foreach ($segments as $segment_idx => $segment) {
            if ($segment == 'data') {
              $idx_type = $segment_idx - 1;
            }
          }
          $temp_row['dcr_type'] = $segments[$idx_type];
          $managing_tmp = $segments[$idx_type - 1];
          $managing_arr = explode('-', $managing_tmp);
          $managing_branch = reset($managing_arr);
          $temp_row['managing_branch'] = $managing_branch;
          $temp_row['type_name'] = $temp_row['dcr_type'];
          if ($temp_row['langcode'] == 'eng' && $temp_row['dcr_id'] == 1297964599443) {
            $temp_row['dcr_type'] = 'gene-gene';
            $temp_row['type_name'] = 'sitemapinit';
            $home_row_en = $temp_row;
          }
          if ($temp_row['langcode'] == 'fra' && $temp_row['dcr_id'] == 1297964599443) {
            $temp_row['dcr_type'] = 'gene-gene';
            $temp_row['type_name'] = 'sitemapinit';
            $home_row_fr = $temp_row;
          }
          $data[] = array_combine($header, $temp_row);
          //echo print_r($data, TRUE);
        }
      }
      fclose($handle);
    }
    if (!empty($home_row_en) && !empty($home_row_fr)) {
      // Home page processes first, logic is getting weird here because sitemap is not provided at this stage, we'll see how this works out.
      $data[] = $home_row_en;
      $data[] = $home_row_fr;
    }
    return $data;
}

$array_of_content = [];

$has_dcr_id = TRUE;
$eng_exturl = '';
$no_dcr_id_array = [];
// Sitemap processing.
echo "\n";
echo "This takes a while...";
echo "\n";
global $dcr_id;
$dcrid=0;
$parent_id_id=0;
$ia_id=0;
foreach ($csv as $csvkey => $testrow) {
  $dcr_id=$testrow['dcr_id'];
  if ($testrow['parent_ia_id'] == 0) {
    continue;
  }
  // Fix data corruption , the parent_ia_id should be exactly the same in french as the english row.
  if ($testrow['langcode'] == 'eng' && isset($csv[$csvkey+1]['langcode']) && $csv[$csvkey+1]['langcode'] == 'fra') {
    if (!is_numeric($csv[$csvkey+1]['parent_ia_id']) && is_numeric($testrow['parent_ia_id'])) {
      echo "\nAuto fixing corruption: parent_ia_id for fra (français) with dcr_id=$dcr_id is being assigned the parent_ia_id from eng (english)\n because the fra parent_ia_id is not numeric but the eng parent_ia_id is numeric.\n";
      $csv[$csvkey+1]['parent_ia_id']=$testrow['parent_ia_id'];
    }
    if (is_numeric($csv[$csvkey+1]['parent_ia_id']) && !is_numeric($testrow['parent_ia_id'])) {
      echo "\nAuto fixing corruption: parent_ia_id for eng (english) with dcr_id=$dcr_id is being assigned the parent_ia_id from fra (french)\n because the eng parent_ia_id is not numeric but the fra parent_ia_id is numeric.\n";
      $testrow['parent_ia_id'] = $csv[$csvkey+1]['parent_ia_id'];
      $csv[$csvkey]['parent_ia_id']=$testrow['parent_ia_id'];
    }
    if (!is_numeric($csv[$csvkey+1]['ia_id']) && is_numeric($testrow['ia_id'])) {
      echo "\nAuto fixing corruption: ia_id for fra (français) with dcr_id=$dcr_id is being assigned the ia_id from eng (english)\n because the fra ia_id is not numeric but the eng ia_id is numeric.\n";
      $csv[$csvkey+1]['ia_id']=$testrow['ia_id'];
    }
    if (is_numeric($csv[$csvkey+1]['ia_id']) && !is_numeric($testrow['ia_id'])) {
      echo "\nAuto fixing corruption: ia_id for eng (english) with dcr_id=$dcr_id is being assigned the ia_id from fra (french)\n because the eng ia_id is not numeric but the fra ia_id is numeric.\n";
      $testrow['ia_id'] = $csv[$csvkey+1]['ia_id'];
      $csv[$csvkey]['ia_id']=$testrow['ia_id'];
    }
    if (($csv[$csvkey+1]['ia_id'] == $testrow['ia_id']) && ($csv[$csvkey+1]['parent_ia_id'] == $testrow['parent_ia_id'])) {
      continue;
    }
    // The ia_id or parent_ia_id is not the same between english/french, make some noise, try to fix automatically.
    echo $csvkey . ",";
  //if ($csvkey > 1000) {
  //    die;
  //}
    $parent_ia_id = $testrow['parent_ia_id'];
    $ia_id = $testrow['ia_id'];
    $csv[$csvkey+1]['parent_ia_id']=$parent_ia_id;
    $csv[$csvkey+1]['ia_id']=$ia_id;
   // echo "\n";
   // echo "dcr_id is " . $csv[$csvkey]['dcr_id'];
   // echo "\ntest after eng\n";
   // echo print_r($csv[$csvkey], TRUE);
   // echo "\ntest after fra\n";
   // echo print_r($csv[$csvkey+1], TRUE);
  }
}
$csv_check = $csv;
foreach ($csv as $csvkey => $testrow) {
  if ($testrow['parent_ia_id'] == 0) {
    continue;
  }
  // Eng or Fra have same id reference values, only check once per entity.
  if ($testrow['langcode'] == 'eng') {
    $parent_ia_id = $testrow['parent_ia_id'];
    $ia_id = $testrow['ia_id'];
    $count_ia_id=0;
    foreach ($csv_check as $icsvkey => $iirow) {
      if ($iirow['ia_id'] == $testrow['parent_ia_id']) {
        $csv_check[$icsvkey]['orig_type_name']=$csv_check[$icsvkey]['type_name'];
        $csv_check[$icsvkey]['type_name']='sitemapinit';
        $csv_check[$icsvkey]['parent_node_id']=$csv_check[$icsvkey]['parent_ia_id'];
      }
    }
  }
}

$csvkey=NULL;
$testrow=NULL;
$iirow=NULL;
$csv=$csv_check;
echo "\n";
echo "This also takes a while...";
echo "\n";
foreach ($csv as $csvkey => $testrow) {
  if ($testrow['parent_ia_id'] == 0) {
    continue;
  }
  // Eng or Fra have same id reference values, only check once per entity.
  if ($testrow['langcode'] == 'eng') {
    $parent_ia_id = $testrow['parent_ia_id'];
    $count_ia_id=0;
    foreach ($csv_check as $icsvkey => $iirow) {
      if ($iirow['ia_id'] == $testrow['parent_ia_id']) {
        // English row, set parent_dcr_id value.
        $csv[$csvkey]['parent_dcr_id'] = $iirow['dcr_id'];
        // French row, set parent_dcr_id value.
        $csv[$csvkey+1]['parent_dcr_id'] = $iirow['dcr_id'];
      }
    }
  }
}

foreach ($csv as $row) {
  $row['type_name'] = $row['dcr_type'];
  // source data changed from first run for intranet, but we are lazy, just copy over.
  //unset($row['dcr_type']); // Remove this noise. du bruit.// On second thought, might need this.
  if (isset($row['parent_node_id']) && !empty($row['parent_node_id']) || $row['parent_node_id'] == "0") {
    $row['type_name'] = 'sitemapinit';
  }
  if (0 && !empty($row['exturl']) && !is_numeric($row['exturl']) && strlen($row['exturl']) > 7) {
    if ($row['type_name'] == 'sitemapinit' && $row['langcode'] == 'eng') {
      $row['dcr_id'] = $row['exturl'];
      $no_dcr_id_array[] = '/' . $row['node_id'] . '.json';
      $eng_exturl = $row['exturl'];
      $has_dcr_id = FALSE;
      //$row['node_id'] = $row['node_id'] . '_TEST'; // DEBUGGING.
    }
    else if ($row['type_name'] == 'sitemapinit' && $row['langcode'] == 'fra') {
      $has_dcr_id = FALSE;
    }
  }
  else {
    $has_dcr_id = TRUE;
    $eng_exturl = '';
  }
  $row['has_dcr_id'] = $has_dcr_id;
  if (isset($row['short_title'])){
    $row['label'] = $row['short_title'];
  }
  $row['breadcrumb'] = $row['label'];
  if (empty($row['label'])) {
    $row['breadcrumb'] = $row['title'];
    $row['label'] = $row['title'];
  }
  $row['templatetype'] = $row['templatetype'];
  $row['layout_name'] = $row['templatetype'];
  if ($row['layout_name'] == '<NULL>') {
    $row['layout_name'] = NULL;
  }
  if (!isset($row['lang']) && isset($row['langcode'])) {
    $row['lang'] = $row['langcode'];
    unset($row['langcode']);
  }
  if ($row['lang'] == 'eng') {
    $row['lang'] = 'en';
  }
  if ($row['lang'] == 'fra') {
    $row['lang'] = 'fr';
  }
  if ((!isset($array_of_content[$row['dcr_id']]) && $row['lang'] == 'en' && $row['type_name'] != 'sitemapinit') ||
       (!isset($array_of_content[$row['node_id']]) && $row['lang'] == 'en' && $row['type_name'] == 'sitemapinit')
    ) {
    $array_of_content[$row['dcr_id']] = $row;
  } else {
    if (!$has_dcr_id && !empty($eng_exturl)) {
      $origRow = $array_of_content[$eng_exturl];
    }
    else {
      $origRow = $array_of_content[$row['dcr_id']];
    }
    if ($origRow['lang'] == 'en' && !isset($row['chronicletopic']['en']) && $row['lang'] == 'fr') {
      $chronicletopic = $origRow['chronicletopic'];
      $chronicletopicFr = $row['chronicletopic'];
      $origRow['chronicletopic'] = [
        'en' => $chronicletopic,
        'fr' => $chronicletopicFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['chroniclecontent']['en']) && $row['lang'] == 'fr') {
      $chroniclecontent = $origRow['chroniclecontent'];
      $chroniclecontentFr = $row['chroniclecontent'];
      $origRow['chroniclecontent'] = [
        'en' => $chroniclecontent,
        'fr' => $chroniclecontentFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['chronicleaudience']['en']) && $row['lang'] == 'fr') {
      $chronicleaudience = $origRow['chronicleaudience'];
      $chronicleaudienceFr = $row['chronicleaudience'];
      $origRow['chronicleaudience'] = [
        'en' => $chronicleaudience,
        'fr' => $chronicleaudienceFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['sfcrdocumenttype']['en']) && $row['lang'] == 'fr') {
      $sfcrdocumenttype = $origRow['sfcrdocumenttype'];
      $sfcrdocumenttypeFr = $row['sfcrdocumenttype'];
      $origRow['sfcrdocumenttype'] = [
        'en' => $sfcrdocumenttype,
        'fr' => $sfcrdocumenttypeFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['sfcraudience']['en']) && $row['lang'] == 'fr') {
      $sfcraudience = $origRow['sfcraudience'];
      $sfcraudienceFr = $row['sfcraudience'];
      $origRow['sfcraudience'] = [
        'en' => $sfcraudience,
        'fr' => $sfcraudienceFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['sfcrmethodofproduction']['en']) && $row['lang'] == 'fr') {
      $sfcrmethodofproduction = $origRow['sfcrmethodofproduction'];
      $sfcrmethodofproductionFr = $row['sfcrmethodofproduction'];
      $origRow['sfcrmethodofproduction'] = [
        'en' => $sfcrmethodofproduction,
        'fr' => $sfcrmethodofproductionFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['activity']['en']) && $row['lang'] == 'fr') {
      $activity = $origRow['activity'];
      $activityFr = $row['activity'];
      $origRow['activity'] = [
        'en' => $activity,
        'fr' => $activityFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['commodity']['en']) && $row['lang'] == 'fr') {
      $commodity = $origRow['commodity'];
      $commodityFr = $row['commodity'];
      $origRow['commodity'] = [
        'en' => $commodity,
        'fr' => $commodityFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['program']['en']) && $row['lang'] == 'fr') {
      $program = $origRow['program'];
      $programFr = $row['program'];
      $origRow['program'] = [
        'en' => $program,
        'fr' => $programFr,
      ];
    }
    if ($origRow['lang'] == 'en' && !isset($row['title']['en']) && $row['lang'] == 'fr') {
      //echo print_r($row, TRUE);
      //die;
      $titleEn = $origRow['title'];
      if (isset($origRow['title']['en']) && !empty($titleEn['en'])) {
        $titleEn = $origRow['title']['en'];
      }
      $breadcrumbEn = $origRow['breadcrumb'];
      if (isset($breadcrumbEn['en']) && !empty($breadcrumbEn['en'])) {
        $breadcrumbEn = $breadcrumbEn['en'];
      }
      $labelEn = $origRow['label'];
      if (isset($labelEn['en']) && !empty($labelEn['en'])) {
        $labelEn = $labelEn['en'];
      }
      $origRow['breadcrumb'] = [];
      $origRow['breadcrumb']['en'] = $breadcrumbEn;
      $origRow['breadcrumb']['fr'] = $row['breadcrumb'];
      $origRow['label'] = [];
      $origRow['label']['en'] = $labelEn;
      $origRow['label']['fr'] = $row['label'];
      $origRow['title'] = [];
      $origRow['title']['en'] = $titleEn;
      $origRow['title']['fr'] = $row['title'];
      if ($origRow['lang'] == 'en' && !isset($row['teamsite_location']['en']) && $row['lang'] == 'fr') {
        $tslocationEn = $origRow['teamsite_location'];
        if (isset($tslocationEn['en']) && !empty($tslocationEn['en'])) {
          $tslocationEn = $origRow['teamsite_location']['en'];
        }
        $origRow['teamsite_location'] = array();
        $origRow['teamsite_location']['en'] = $tslocationEn;
        $origRow['teamsite_location']['fr'] = $row['teamsite_location'];
      //  $array_of_content[$origRow['dcr_id']] = $origRow;
      }
      if (!empty($row['parent_node_id']) && $row['type_name'] == 'sitemapinit') {
        $array_of_content[$row['node_id']] = $origRow;
      }
      else {
        if (isset($origRow['parent_ia_id'])) {
          // Ensure that the parent_node_id is set, this is a carry-over.
          $origRow['parent_node_id'] = $origRow['parent_ia_id'];
        }
        $array_of_content[$row['dcr_id']] = $origRow;
      }
    }
  }
}

function missing_en_fr_substitutions($array_of_content) {
  // I don't have time to figure out what went wrong prior to this, easier just to fix the glitch with the crawler later.
  foreach($array_of_content as $key => $value) {
    $dcr_id = $array_of_content['dcr_id'];
    if ($key == 'teamsite_location') {
      if (!isset($value['en']) && !isset($value['fr'])) {
        echo ", teamsite_location ";
        $array_of_content[$key] = [];
        $array_of_content[$key]['en'] = $value;
        $value_fr = str_replace('_eng', '_fra', $value);
        $array_of_content[$key]['fr'] = $value_fr;
      }
    }
    if ($key == 'title') {
      if (!isset($value['en']) && !isset($value['fr'])) {
        echo "$dcr_id substitution/fix for missing en fr title ";
        $array_of_content[$key] = [];
        $array_of_content[$key]['en'] = $value;
        $array_of_content[$key]['fr'] = $value;
      }
    }
    if ($key == 'label') {
      if (!isset($value['en']) && !isset($value['fr'])) {
        echo ", label\n";
        $array_of_content[$key] = [];
        $array_of_content[$key]['en'] = $value;
        $array_of_content[$key]['fr'] = $value;
      }
    }
    if ($key == 'breadcrumb') {
      if (!isset($value['en']) && !isset($value['fr'])) {
        echo ", breadcrumb  ";
        $array_of_content[$key] = [];
        $array_of_content[$key]['en'] = $value;
        $array_of_content[$key]['fr'] = $value;
      }
    }
  }
  return $array_of_content;
}
//$objs = arrayToObject($array_of_content);
function arrayToObject($array) {
  if (!is_array($array)) {
    return $array;
  }

  $object = new stdClass();
  if (is_array($array) && count($array) > 0) {
    foreach ($array as $name=>$value) {
      $name = strtolower(trim($name));
      if (!empty($name)) {
        $object->$name = arrayToObject($value);
      }
    }
    return $object;
  }
  else {
    return FALSE;
  }
}

//echo print_r($objs, TRUE);
//$month_day = '12-11';
$month_day = date('m-d', time());

$dir = getcwd() . '/cfia_export_' . $month_day;
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir . '/..');
unlink('cfia_export_current');
symlink('cfia_export_' . $month_day, 'cfia_export_current');
chdir($dir);
unlink('gene-gene/*json');
unlink('gene-gene');
unlink('page');
symlink('gene-gene', 'page');
$keep_existing = true;
$count_json = 0;
$count_skipped = 0;
$dec31_2017 = strtotime('2017-12-31');
$oct11_2020 = strtotime('2020-10-10');
$skip = FALSE;
$written_by_type = [];
foreach ($array_of_content as $key => $entity) {
  if ($entity['type_name'] == 'sitemapinit') {
    if (!empty($entity['node_id'])) {
      $key = $entity['node_id'];
      echo "Sitemapinit json key=$key";
      echo "\n";
    }
    else {
      echo "@TODO, debug this , see how we can deal with missing ia_id case later, it appears to actually be a valid sitemapid item but this will likely affect processing later.\n";
      echo "Strange sitemapinit item, has no ia_id, using dcrid instead, json key=$key instead of using the ia_id / node_id key which doesn't exist in this case\n";
      echo "sleep(5);\n";
            sleep(5);
    }
  }
  $skip = FALSE;// This needs to be at the top of the forloop inside.
  if ($keep_existing) {
    if (file_exists($key.'.json')) {
      continue;
    }
  }
  $entity['layout_name'] = $entity['templatetype'];
  //unset($entity['templatetype']);
  $type = str_replace('intra-intra/', '', $entity['type_name']);
  $type = str_replace('comm-comm/', '', $entity['type_name']);
  if ($entity['type_name'] == 'exec-exec/summ-somm')
    $type = 'gene-gene';

  //unset($entity['dcr_type']);
  chdir($dir);
  if (!is_dir($type)) {
    mkdir($type);
  }
  chdir($type);
  if ($fp = fopen($key.'.json', 'w')) {
    if (!isset($written_by_type[$type])) {
      $written_by_type[$type] = 1;
    }
    else {
      $written_by_type[$type]++;
    }
    $count_json++;
    $entity = missing_en_fr_substitutions($entity);
    $entity_obj = arrayToObject($entity);
    if ($type == 'empl-empl' || $type == 'news-nouv') {
//    if (strtotime($entity_obj->modified) < $dec31_2017) {
      if (strtotime($entity_obj->modified) < $oct11_2020) {
        $count_skipped++;
        $skip = TRUE;
      }
    }
    echo "Write $key.json\n";
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
    fclose($fp);
    if ($skip) {
      echo "Delete $key.json $type\n";
      unlink($key.'.json');// or die("Couldn't delete $key.json");
      $skip = FALSE;
    }
  }
  chdir($dir);
}

echo "$count_json entities processed, skipped $count_skipped entities.\n";
echo "Summary by type:\n";
foreach ($written_by_type as $key => $value) {
  echo "Wrote $value $key items\n";
}
echo "------------------------------------------------------------------------------\n";
echo count($no_dcr_id_array) . " has no dcr_id (menu link) items:\n";
foreach ($no_dcr_id_array as $value) {
  echo "has no dcr_id (menu link) internet_export_$month_day/sitemapinit$value\n";
}
echo "------------------------------------------------------------------------------\n";
echo "Wrote " . ($count_json - $count_skipped) . " json files to $dir\n";
echo "      " . ($count_json - $count_skipped - count($no_dcr_id_array))*2 . " TOTAL pages (English AND French).\n";
