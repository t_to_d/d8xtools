{
    "dcr_id": "1597148065020",
    "title": {
        "en": "Biosafety Level 4 Zoonotic Laboratory Network",
        "fr": "R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses"
    },
    "html_modified": "8-9-2020",
    "modified": "28-6-2023",
    "issued": "13-8-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/level_4_zoo_lab_ntwork_1597148065020_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/level_4_zoo_lab_ntwork_1597148065020_fra"
    },
    "parent_ia_id": "1548468418007",
    "ia_id": "1597148065380",
    "parent_node_id": "1548468418007",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biosafety Level 4 Zoonotic Laboratory Network",
        "fr": "R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses"
    },
    "label": {
        "en": "Biosafety Level 4 Zoonotic Laboratory Network",
        "fr": "R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses"
    },
    "templatetype": "content page 1 column",
    "node_id": "1597148065380",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1548468417757",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/science-collaborations/biosafety-level-4-zoonotic-laboratory-network/",
        "fr": "/les-sciences-et-les-recherches/collaborations-scientifiques/reseau-de-laboratoires-de-niveau-de-biosecurite-4-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biosafety Level 4 Zoonotic Laboratory Network",
            "fr": "R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses"
        },
        "description": {
            "en": "The Biosafety Level 4 Zoonotic Laboratory Network (BSL4ZNet) is a network of 17 animal and public health organizations from 5 countries.",
            "fr": "Le R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses (r\u00e9seau BSL4Z) est un r\u00e9seau de 17 organisations de sant\u00e9 animale et publique de 5 pays."
        },
        "keywords": {
            "en": "Science, food, animal, plant, laboratories, veterinarians, scientists, research, collaborations, Canadian Animal Health Surveillance Network, CAHSN, Canadian Food Safety Information Network, CFSIN, Level 4 Zoonotic Laboratory Network, BSL4ZNet",
            "fr": "Science, aliments, animaux, v\u00e9g\u00e9taux, laboratoires, v\u00e9t\u00e9rinaires, scientistes, recherche, collaborations, scientifiques, R\u00e9seau canadien de surveillance zoosanitaire, RCSZ, R\u00e9seau canadien d'information sur la salubrite des aliments, RCISA, bios\u00e9curit\u00e9 4, r\u00e9seau de 17 organisations de sant\u00e9 animale et publique, BSL4Z"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-09-08",
            "fr": "2020-09-08"
        },
        "modified": {
            "en": "2023-06-28",
            "fr": "2023-06-28"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biosafety Level 4 Zoonotic Laboratory Network",
        "fr": "R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<div class=\"col-sm-8\"><p class=\"mrgn-tp-lg\">The Biosafety Level 4 Zoonotic Laboratory Network (BSL4ZNet) is a network of animal and public health organizations from 5 countries. It was established to respond to current and emerging high-consequence bio-threats using a One Health approach, and facilitated by strong international partnerships.</p></div>\n<div class=\"col-sm-4\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/2021_bsl4znet_logo_1625492117059_eng.jpg\" class=\"img-responsive mrgn-tp-lg\" alt=\"Banner for the 2021 BSL4ZNet International Conference\">\n</div> \n</div>\n\n<h2>Vision</h2>\n\n<p>A coordinated global alliance of high-containment laboratories that optimizes collaboration and knowledge exchange to protect human health, animal health and agriculture resources.</p>\n\n<h2>Mission</h2>\n\n<ul class=\"lst-spcd\">\n<li>Establish and sustain trusted partnerships</li>\n<li>Strengthen international coordination</li>\n<li>Improve knowledge sharing</li>\n<li>Leverage capacity for diagnostics, research and training</li>\n</ul>\n\n<h2 id=\"a1\">Partners</h2>\n\n<h3>Australia</h3>\n\n<ul>\n<li>Commonwealth Scientific and Industrial Research Organization, Australian Centre for Disease Preparedness</li>\n</ul>\n\n<h3>Canada</h3>\n\n<ul class=\"lst-spcd\">\n<li>Canadian Food Inspection Agency, National Centre for Foreign Animal Disease</li>\n<li>Public Health Agency of Canada, National Microbiology Laboratory</li>\n<li>Department of National Defence, Canadian Safety and Security Program</li>\n<li>Global Affairs Canada, Threat Reduction Program</li>\n</ul>\n\n<h3>Germany</h3>\n\n<ul class=\"lst-spcd\">\n<li>Friedrich-Loeffler-Institut, National Research Institute for Animal Health, Novel and Emerging Infectious Diseases</li>\n<li>Robert Koch Institut, Centre for Biological Threats and Special Pathogens / Biosafety Level 4 Laboratories</li>\n</ul>\n\n<h3>United Kingdom</h3>\n\n<ul class=\"lst-spcd\">\n<li>UK Research &amp; Innovation, The Pirbright Institute</li>\n<li>Animal and Plant Health Agency, Weybridge Veterinary Laboratories</li>\n<li>Public Health England, Porton Down Laboratory</li>\n<li>Ministry of Defence of the United Kingdom, Defence Science and Technology Laboratories</li>\n</ul>\n\n<h3>United States</h3>\n\n<ul class=\"lst-spcd\">\n<li>United States Department of Agriculture, Animal and Plant Health Inspection Services</li>\n<li>United States Department of Agriculture, Agricultural Research Services</li>\n<li>Center of Disease Control and Prevention, Emerging Infectious Disease Laboratory</li>\n<li>United States Department of Agriculture and Department of Homeland Security, National Bio and Agro-Defense Facility</li>\n</ul>\n\n<h2>Structure</h2>\n\n<p>The network communicates regularly, hosts learning events, conducts surveys, and shares best practices, templates, contracts and protocols. Its steering committee and working groups are organized under the following pillars:</p>\n\n<ul class=\"lst-spcd\">\n<li>Training world-class personnel</li>\n<li>Scientific excellence</li>\n<li>Institutional cooperation</li>\n<li>International response</li>\n</ul>\n\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left h3\">Working groups: objectives and outcomes</caption>\n<colgroup>\n<col class=\"col-sm-4\">\n<col class=\"col-sm-4\">\n<col class=\"col-sm-4\">\n</colgroup>\n<thead>\n<tr>\n<th class=\"active\" scope=\"col\">Working groups</th>\n<th class=\"active\" scope=\"col\">Objectives</th>\n<th class=\"active\" scope=\"col\">Outcomes</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th class=\"active\" scope=\"row\">Training world-class personnel</th>\n<td><p>To strengthen training of biosafety level 4 laboratory personnel by identifying and creating training opportunities.</p>\n<p>To build capacity and promote scientific collaboration within the Network through strategic personnel exchanges.</p></td>\n<td><p>Highly trained, collaborative workforce able to respond to emerging pressures.</p></td>\n</tr>\n<tr>\n<th class=\"active\" scope=\"row\">Scientific excellence</th>\n<td><p>To promote scientific collaboration and learning within the network.</p></td>\n<td><p>Enhanced diagnostic capabilities, scientific collaborations and collective advice for decision-makers.</p></td>\n</tr>\n<tr>\n<th class=\"active\" scope=\"row\">Institutional cooperation</th>\n<td><p>To promote institutional cooperation and sharing of knowledge within the network.</p></td>\n<td><p>Efficient mechanisms for harmonized exchange of laboratory-based information and diagnostics.</p></td>\n</tr>\n<tr>\n<th class=\"active\" scope=\"row\">International response</th>\n<td><p>To strengthen laboratory preparedness and response during an outbreak (sample sharing and exercise subgroups).</p>\n<p>To use shared data to better understand hazards, exposure and vulnerability.</p></td>\n<td><p>Network communications plan and provisions for laboratory surge capacity.</p></td>\n</tr>\n</tbody>\n</table>\n</div>\n    \n<h2>BSL4ZNet recent publications and editorials</h2>\n<ul>\n<li><a href=\"https://science.gc.ca/site/science/en/blogs/cultivating-science/global-experts-join-together-learn-about-pandemic-preparedness\">Global experts join together to learn about pandemic preparedness</a> (science.gc.ca)</li>\n<li><a href=\"/inspect-and-protect/episode-5/eng/1637953775477/1637953776571\">Working with the world's deadliest diseases</a></li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/international-conference-drives-science-forward/eng/1632406330682/1632406562302\">International conference drives science forward  in the post-pandemic era</a></li>\n<li><a href=\"https://sciencepolicy.ca/posts/bsl4znet-covid-19-rapid-response-a-true-example-of-science-diplomacy/\">BSL4ZNET COVID-19 Rapid Response: A True Example of Science Diplomacy</a></li>\n<li><a href=\"/eng/1600884323826/1600884324717\">Canada hosts COVID-19 international symposium</a></li>\n<li><a href=\"https://www.dhs.gov/science-and-technology/blog/2020/05/04/how-st-collaborates-international-partners-during-global-pandemic\">How S&amp;T Collaborates with International Partners During a Global Pandemic</a></li>\n<li><a href=\"https://pubmed.ncbi.nlm.nih.gov/31669332/\">The Biosafety Level 4 Zoonotic Laboratory Network (BSL4ZNet): Report of a workshop on live animal handling</a></li>\n<li><a href=\"https://www.ic.gc.ca/eic/site/063.nsf/eng/97704.html\">A glimpse into Canada's highest containment laboratory for animal health: The National Centre for Foreign Animal Diseases</a> (science.gc.ca)</li>\n</ul>\n<h2>Contact BSL4ZNet</h2>\n<p>Questions? Contact <a href=\"mailto:cfia.bsl4znet.acia@inspection.gc.ca\">cfia.bsl4znet.acia@inspection.gc.ca</a> for more information.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-sm-8\"><p class=\"mrgn-tp-lg\">Le R\u00e9seau de laboratoires de niveau de bios\u00e9curit\u00e9 4 pour les zoonoses (r\u00e9seau BSL4Z) est un r\u00e9seau de 17 organisations de sant\u00e9 animale et publique de 5 pays. Il a \u00e9t\u00e9 cr\u00e9\u00e9 pour r\u00e9pondre aux menaces biologiques actuelles et \u00e9mergentes \u00e0 fortes cons\u00e9quences en adoptant l'approche \u00ab\u00a0Une seule sant\u00e9\u00a0\u00bb et en s'appuyant sur de solides partenariats internationaux.</p></div>\n<div class=\"col-sm-4\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/2021_bsl4znet_logo_1625492117059_fra.jpg\" class=\"img-responsive mrgn-tp-lg\" alt=\"Banni\u00e8re de la Conf\u00e9rence internationale 2021 du r\u00e9seau BSL4Z\">\n</div>\n</div>\n\n<h2>Vision</h2>\n\n<p>Une alliance mondiale coordonn\u00e9e de laboratoires \u00e0 haut confinement qui optimise la collaboration et l'\u00e9change de connaissances pour prot\u00e9ger la sant\u00e9 humaine, la sant\u00e9 animale et les ressources agricoles.</p>\n\n<h2>Mission</h2>\n\n<ul class=\"lst-spcd\">\n<li>\u00c9tablir et maintenir des partenariats de confiance </li>\n<li>Renforcer la coordination internationale</li>\n<li>Am\u00e9liorer le partage des connaissances </li>\n<li>Tirer parti de la capacit\u00e9 de diagnostic, de recherche et de formation</li>\n</ul>\n\n<h2 id=\"a1\">Partenaires</h2>\n\n<h3>Australie</h3>\n\n<ul>\n<li><span lang=\"en\">Commonwealth Scientific and Industrial Research Organization, Australian Centre for Disease Preparedness</span></li>\n</ul>\n\n<h3>Canada</h3>\n\n<ul class=\"lst-spcd\">\n<li>Agence canadienne d'inspection des aliments, Centre national des maladies animales exotiques</li>\n<li>Agence de la sant\u00e9 publique du Canada, Laboratoire national de microbiologie</li>\n<li>Minist\u00e8re de la D\u00e9fense nationale, Programme canadien pour la s\u00fbret\u00e9 et la s\u00e9curit\u00e9</li>\n<li>Affaires mondiales Canada, Programme de r\u00e9duction de la menace</li>\n</ul>\n\n<h3>Allemagne</h3>\n\n<ul class=\"lst-spcd\">\n<li>Institut Friedrich-Loeffler, Institut national de recherche sur la sant\u00e9 animale, Maladies infectieuses actuelles et \u00e9mergentes</li>\n<li>Institut Robert Koch, Centre pour les menaces biologiques et les agents pathog\u00e8nes sp\u00e9ciaux/Laboratoires de niveau de bios\u00e9curit\u00e9 4</li>\n</ul>\n\n<h3>Royaume-Uni</h3>\n\n<ul class=\"lst-spcd\">\n<li><span lang=\"en\">UK Research &amp; Innovation, The Pirbright Institute</span></li>\n<li><span lang=\"en\">Animal and Plant Health Agency, Weybridge Veterinary Laboratories</span></li>\n<li><span lang=\"en\">Public Health England, Porton Down Laboratory</span></li>\n<li><span lang=\"en\">Ministry of Defence of the United Kingdom, Defence Science and Technology Laboratories</span></li>\n</ul>\n\n\n<h3>\u00c9tats-Unis</h3>\n\n<ul class=\"lst-spcd\">\n<li>D\u00e9partement de l'Agriculture des \u00c9tats-Unis, <span lang=\"en\">Animal and Plant Health Inspection Services</span></li>\n<li>D\u00e9partement de l'Agriculture des \u00c9tats-Unis, <span lang=\"en\">Agricultural Research Services</span></li>\n<li>Center of Disease Control and Prevention, <span lang=\"en\">Emerging Infectious Disease Laboratory</span></li>\n<li>D\u00e9partement de l'Agriculture des \u00c9tats-Unis et D\u00e9partement de la s\u00e9curit\u00e9 int\u00e9rieure, <span lang=\"en\">National Bio and Agro-Defence Facility</span></li>\n</ul>\n\n<h2>Structure</h2>\n\n<p>Le r\u00e9seau communique r\u00e9guli\u00e8rement, organise des \u00e9v\u00e9nements d'apprentissage, effectue des sondages et partage les meilleures pratiques, les meilleurs mod\u00e8les, contrats et protocoles. Son comit\u00e9 directeur et ses groupes de travail sont organis\u00e9s selon les piliers suivants\u00a0:</p>\n\n<ul class=\"lst-spcd\">\n<li>la formation de personnel de classe mondiale</li>\n<li>l'excellence scientifique</li>\n<li>la coop\u00e9ration institutionnelle</li>\n<li>la r\u00e9ponse internationale</li>\n</ul>\n\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption class=\"text-left h3\">Groupes de travail\u00a0: objectifs et r\u00e9sultats</caption>\n<colgroup>\n<col class=\"col-sm-4\">\n<col class=\"col-sm-4\">\n<col class=\"col-sm-4\">\n</colgroup>\n<thead>\n<tr>\n<th class=\"active\" scope=\"col\">Groupes de travail</th>\n<th class=\"active\" scope=\"col\">Objectifs</th>\n<th class=\"active\" scope=\"col\">R\u00e9sultats</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th class=\"active\" scope=\"row\">Formation de personnel de classe mondiale</th>\n<td><p>Renforcer la formation du personnel de laboratoire de niveau de bios\u00e9curit\u00e9 4 en cernant et en cr\u00e9ant des possibilit\u00e9s de formation.</p>\n<p>Renforcer la capacit\u00e9 et promouvoir la collaboration scientifique au sein du r\u00e9seau gr\u00e2ce \u00e0 des \u00e9changes strat\u00e9giques.</p></td>\n<td><p>Une main-d'\u0153uvre hautement qualifi\u00e9e et collaborative capable de r\u00e9pondre aux pressions \u00e9mergentes.</p></td>\n</tr>\n<tr>\n<th class=\"active\" scope=\"row\">Excellence scientifique</th>\n<td><p>Promouvoir la collaboration scientifique et l'apprentissage au sein du r\u00e9seau.</p></td>\n<td><p>Des capacit\u00e9s de diagnostic am\u00e9lior\u00e9es, des collaborations scientifiques et des conseils collectifs pour les d\u00e9cideurs.</p></td>\n</tr>\n<tr>\n<th class=\"active\" scope=\"row\">Coop\u00e9ration institutionnelle</th>\n<td><p>Promouvoir la coop\u00e9ration institutionnelle et le partage des connaissances au sein du r\u00e9seau.</p></td>\n<td><p>M\u00e9canismes efficaces pour l'\u00e9change harmonis\u00e9 d'information et de diagnostics de laboratoire.</p></td>\n</tr>\n<tr>\n<th class=\"active\" scope=\"row\">R\u00e9ponse internationale</th>\n<td><p>Renforcer l'\u00e9tat de pr\u00e9paration et la r\u00e9ponse du laboratoire pendant une \u00e9pid\u00e9mie (partage d'\u00e9chantillons et sous-groupes d'exercices).</p>\n<p>Utiliser les donn\u00e9es partag\u00e9es pour mieux comprendre les dangers, l'exposition et la vuln\u00e9rabilit\u00e9.</p></td>\n<td><p>Plan de communication r\u00e9seau et dispositions pour la capacit\u00e9 de surtension de laboratoire.</p></td>\n</tr>\n</tbody>\n</table>\n</div>\n<h2>Publications et \u00e9ditoriaux r\u00e9cents du r\u00e9seau BSL4Z</h2>\n<ul>\n<li><a href=\"https://science.gc.ca/site/science/fr/blogues/cultiver-science/experts-monde-entier-rassemblent-pour-savoir-plus-preparation-cas-pandemie\">Des experts du monde entier se rassemblent pour en savoir plus sur la pr\u00e9paration en cas de pand\u00e9mie</a> (science.gc.ca)</li>\n<li><a href=\"/inspecter-et-proteger/episode-5/fra/1637953775477/1637953776571\">Ce que c'est que de travailler avec les maladies les plus mortelles au monde</a></li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/une-conference-internationale-fait-avancer-la-scie/fra/1632406330682/1632406562302\">Une conf\u00e9rence internationale fait avancer la science \u00e0 l'\u00e8re post-pand\u00e9mique</a></li>\n<li><a href=\"https://sciencepolicy.ca/posts/bsl4znet-covid-19-rapid-response-a-true-example-of-science-diplomacy/\">R\u00e9ponse rapide BSL4ZNET COVID-19: un v\u00e9ritable exemple de diplomatie scientifique</a></li>\n<li><a href=\"/fra/1600884323826/1600884324717\">Le Canada accueille le symposium international sur la COVID-19</a></li>\n<li><a href=\"https://www.dhs.gov/science-and-technology/blog/2020/05/04/how-st-collaborates-international-partners-during-global-pandemic\"><span lang=\"en\">How S&amp;T Collaborates with International Partners During a Global Pandemic</span> (en anglais seulement)</a></li>\n<li><a href=\"https://pubmed.ncbi.nlm.nih.gov/31669332/\"><span lang=\"en\">The Biosafety Level 4 Zoonotic Laboratory Network (BSL4ZNet): Report of a workshop on live animal handling</span> (en anglais seulement)</a></li>\n<li><a href=\"https://www.ic.gc.ca/eic/site/063.nsf/fra/97704.html\">Un aper\u00e7u du laboratoire de niveau de confinement le plus \u00e9lev\u00e9 pour la sant\u00e9 animale au Canada : le Centre national des maladies animales exotiques</a> (science.gc.ca)</li>\n</ul>\n<h2>Communiquez avec le r\u00e9seau BSL4Z</h2>\n<p>Vous avez des questions? Envoyez un courriel \u00e0 l'adresse <a href=\"mailto:cfia.bsl4znet.acia@inspection.gc.ca\">cfia.bsl4znet.acia@inspection.gc.ca</a> pour obtenir de plus amples renseignements.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}