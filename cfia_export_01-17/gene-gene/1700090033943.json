{
    "dcr_id": "1700090033943",
    "title": {
        "en": "Antimicrobial resistance: What we are doing",
        "fr": "R\u00e9sistance aux <span class=\"nowrap\">antimicrobiens\u00a0:</span> ce que nous faisons"
    },
    "html_modified": "27-11-2023",
    "modified": "27-11-2023",
    "issued": "20-11-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/microbial_resistance_wht_are_we_doing_1700090033943_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/microbial_resistance_wht_are_we_doing_1700090033943_fra"
    },
    "parent_ia_id": "1700088271056",
    "ia_id": "1700090034323",
    "parent_node_id": "1700088271056",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Antimicrobial resistance: What we are doing",
        "fr": "R\u00e9sistance aux <span class=\"nowrap\">antimicrobiens\u00a0:</span> ce que nous faisons"
    },
    "label": {
        "en": "Antimicrobial resistance: What we are doing",
        "fr": "R\u00e9sistance aux <span class=\"nowrap\">antimicrobiens\u00a0:</span> ce que nous faisons"
    },
    "templatetype": "content page 1 column",
    "node_id": "1700090034323",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1700088270307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/antimicrobial-resistance/what-we-are-doing/",
        "fr": "/sante-des-animaux/resistance-aux-antimicrobiens/ce-que-nous-faisons/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Antimicrobial resistance: What we are doing",
            "fr": "R\u00e9sistance aux antimicrobiens\u00a0: ce que nous faisons"
        },
        "description": {
            "en": "On June 22 2023, the Government of Canada released the Pan-Canadian Action Plan on Antimicrobial Resistance (PCAP), developed in partnership with provinces and territories.",
            "fr": "Le 22 juin 2023, le gouvernement du Canada a publi\u00e9 le Plan d'action pancanadien sur la r\u00e9sistance aux antimicrobiens (PCAP), \u00e9labor\u00e9 en partenariat avec les provinces et les territoires."
        },
        "keywords": {
            "en": "antimicrobial resistance, animals, people, environment, bacteria, viruses, fungi, parasites, infections, what we are doing",
            "fr": "r\u00e9sistance aux antimicrobiens, animaux, personnes, environnement, bact\u00e9ries, virus, champignons, parasites, infections, ce que nous faisons"
        },
        "dcterms.subject": {
            "en": "agri-food industry,agri-food products,labelling",
            "fr": "industrie agro-alimentaire,produit agro-alimentaire,\u00e9tiquetage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-11-27",
            "fr": "2023-11-27"
        },
        "modified": {
            "en": "2023-11-27",
            "fr": "2023-11-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Antimicrobial resistance: What we are doing",
        "fr": "R\u00e9sistance aux antimicrobiens\u00a0: ce que nous faisons"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>On June 22 2023, the Government of Canada released the <a href=\"https://www.canada.ca/en/public-health/services/publications/drugs-health-products/pan-canadian-action-plan-antimicrobial-resistance.html\">Pan-Canadian Action Plan on Antimicrobial Resistance</a> (PCAP), developed in partnership with provinces and territories. The PCAP is a 5-year blueprint (2023 to 2027) for strengthening Canada's collective antimicrobial resistance (AMR) response using the One Health approach. It recognizes the close interplay between humans, animals and their shared environment. The CFIA continues to work with federal, provincial and territorial partners to support the PCAP and actions outlined in<a href=\"https://www.canada.ca/en/public-health/services/publications/drugs-health-products/pan-canadian-action-plan-antimicrobial-resistance/building-momentum-activities-underway-address-antimicrobial-resistance-canada.html\"> Building Momentum: Activities Underway to Address Antimicrobial Resistance.</a> CFIA actions include:</p>\n\n<ul>\n<li>continued participation and contribution to research programs that focus on expanding AMR research such as the <a href=\"https://grdi.canada.ca/en/projects/antimicrobial-resistance-amr2-project\">Genomics Research and Development Initiatives</a></li>\n<li>establishing baselines, goals and measures of progress on prudent antimicrobial use and reduction of AMR in agriculture and agri-food sector</li>\n<li>developing and implementing policies and guidance material on Veterinary Health Products</li>\n<li>modernizing feed regulations and continuing to support innovative feed products</li>\n<li>facilitating access to veterinary biologics (for example, vaccines, antibody products and <i>in vitro</i> diagnostic test kits)</li>\n<li>continued improvement and expansion of existing on-farm food safety programming</li>\n<li>involvement in Canadian inter-departmental and global AMR initiatives</li>\n</ul>\n\n<h2>Additional information</h2>\n\n<h3>Publications</h3>\n\n<ul>\n<li><a href=\"https://www.canada.ca/en/public-health/services/publications/drugs-health-products/pan-canadian-action-plan-antimicrobial-resistance.html\">Pan-Canadian Action Plan on Antimicrobial Resistance</a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/publications/drugs-health-products/pan-canadian-action-plan-antimicrobial-resistance/building-momentum-activities-underway-address-antimicrobial-resistance-canada.html\">Building Momentum: Activities Underway to Address Antimicrobial Resistance</a></li>\n<li><a href=\"https://grdi.canada.ca/en/projects/antimicrobial-resistance-amr2-project\">GRDI Antimicrobial Resistance (the AMR2 Project)</a></li>\n<li><a href=\"https://www.cdc.gov/drugresistance/tatfar/index.html\">Trans-Atlantic Task Force on AMR</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-7/eng/1328858316692/1328858462304\">RG-7 for Medicated Feeds</a></li>\n<li><a href=\"https://active.inspection.gc.ca/netapp/veterinarybio-bioveterinaire/vetbioe.aspx\">Veterinary biologics licensed in Canada</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le 22 juin 2023, le gouvernement du Canada a publi\u00e9 le <a href=\"https://www.canada.ca/fr/sante-publique/services/publications/medicaments-et-produits-sante/plan-action-pancanadien-resistance-antimicrobiens.html\">Plan d'action pancanadien sur la r\u00e9sistance aux antimicrobiens</a> (PCAP), \u00e9labor\u00e9 en partenariat avec les provinces et les territoires. Le PCAP est un projet quinquennal (2023 \u00e0 2027) visant \u00e0 coordonner une r\u00e9ponse collective du Canada \u00e0 la R\u00e9sistance aux antimicrobiens (RAM) en adoptant une seule approche. Il reconna\u00eet l'\u00e9troite interaction entre les humains, les animaux et leur environnement commun. L'ACIA continue de travailler avec ses partenaires f\u00e9d\u00e9raux, provinciaux et territoriaux pour appuyer le PCAP et les mesures d\u00e9crites dans la section <a href=\"https://www.canada.ca/fr/sante-publique/services/publications/medicaments-et-produits-sante/plan-action-pancanadien-resistance-antimicrobiens/donner-elan-activites-cours-pour-lutter-contre-resistance-antimicrobiens-canada.html\">Donner de l'\u00e9lan\u00a0: Activit\u00e9s en cours pour lutter contre la r\u00e9sistance aux antimicrobiens</a>. Les mesures de l'ACIA comprennent\u00a0:</p>\n\n<ul>\n<li>participer et contribuer de fa\u00e7on continue aux programmes de recherche ax\u00e9s sur l'approfondissement de la recherche sur la RAM, tels que les <a href=\"https://grdi.canada.ca/fr/projets/resistance-aux-antimicrobiens-2-projet-amr2\">Initiatives de recherche-d\u00e9veloppement en g\u00e9nomique</a>;</li>\n<li>\u00e9tablir des niveaux de r\u00e9f\u00e9rence, des objectifs et des mesures de progr\u00e8s en mati\u00e8re d'utilisation prudente des antimicrobiens et de r\u00e9duction de la RAM dans le secteur agricole et agroalimentaire;</li>\n<li>\u00e9laborer et mettre en \u0153uvre de politiques et de documents d'orientation sur les produits de sant\u00e9 v\u00e9t\u00e9rinaires;</li>\n<li>moderniser les r\u00e8glements sur les aliments du b\u00e9tail et continuer d'appuyer le d\u00e9veloppement des produits innovants pour les aliments du b\u00e9tail;</li>\n<li>faciliter l'acc\u00e8s aux produits biologiques v\u00e9t\u00e9rinaires (par exemple, vaccins, anticorps et trousses de diagnostic <i>in vitro</i>) ;</li>\n<li>am\u00e9liorer et \u00e9largir de fa\u00e7on continue les programmes actuels de salubrit\u00e9 des aliments \u00e0 la ferme ;</li>\n\n<li>participer aux initiatives interminist\u00e9rielles et internationales canadiennes de lutte contre la RAM.</li>\n</ul>\n\n<h2>Renseignements suppl\u00e9mentaires</h2>\n\n<h3>Publications</h3>\n\n<ul>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/publications/medicaments-et-produits-sante/plan-action-pancanadien-resistance-antimicrobiens.html\">Plan d'action pancanadien sur la r\u00e9sistance aux antimicrobiens</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/publications/medicaments-et-produits-sante/plan-action-pancanadien-resistance-antimicrobiens/donner-elan-activites-cours-pour-lutter-contre-resistance-antimicrobiens-canada.html\">Donner de l'\u00e9lan\u00a0: Activit\u00e9s en cours pour lutter contre la r\u00e9sistance aux antimicrobiens</a></li>\n<li><a href=\"https://grdi.canada.ca/fr/projets/resistance-aux-antimicrobiens-2-projet-amr2\">IRDG-r\u00e9sistance aux antimicrobiens (projet RAM.2)</a></li>\n<li><a href=\"https://www.cdc.gov/drugresistance/tatfar/index.html\">Groupe de travail transatlantique sur la RAM. (en anglais seulement)</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-7/fra/1328858316692/1328858462304\">RG-7 pour les aliments m\u00e9dicamenteux de b\u00e9tail</a></li>\n<li><a href=\"https://active.inspection.gc.ca/netapp/veterinarybio-bioveterinaire/vetbiof.aspx\">Produits biologiques v\u00e9t\u00e9rinaires homologu\u00e9s au Canada</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}