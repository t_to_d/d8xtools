{
    "dcr_id": "1600198042434",
    "title": {
        "en": "Strawberry blossom <span class=\"nowrap\">weevil \u2013</span> Anthonomus rubi Herbst",
        "fr": "Anthonome du <span class=\"nowrap\">fraisier -</span> Anthonomus rubi Herbst"
    },
    "html_modified": "2024-01-26 2:25:13 PM",
    "modified": "2021-10-07",
    "issued": "2020-09-17",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_antrub_index_1600198042434_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_antrub_index_1600198042434_fra"
    },
    "ia_id": "1600198043152",
    "parent_ia_id": "1307078272806",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1307078272806",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Strawberry blossom <span class=\"nowrap\">weevil \u2013</span> Anthonomus rubi Herbst",
        "fr": "Anthonome du <span class=\"nowrap\">fraisier -</span> Anthonomus rubi Herbst"
    },
    "label": {
        "en": "Strawberry blossom <span class=\"nowrap\">weevil \u2013</span> Anthonomus rubi Herbst",
        "fr": "Anthonome du <span class=\"nowrap\">fraisier -</span> Anthonomus rubi Herbst"
    },
    "templatetype": "content page 1 column",
    "node_id": "1600198043152",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1307077188885",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/strawberry-blossom-weevil/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/anthonome-du-fraisier/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Strawberry blossom weevil \u2013 Anthonomus rubi Herbst",
            "fr": "Anthonome du fraisier - Anthonomus rubi Herbst"
        },
        "description": {
            "en": "Strawberry blossom weevil, is native to Europe where it is a serious pest of Fragaria and Rubus and has been reported in association with Rosa.",
            "fr": "Anthonome du fraisier, est originaire d\u2019Europe o\u00f9 il est un organisme nuisible grave de Fragaria et Rubus et a \u00e9t\u00e9 signal\u00e9 en association avec Rosa."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Anthonomus rubi, Strawberry blossom weevil, strawberry",
            "fr": "phytoravageurs, enqu&#234;tes phytosanitaires, phytoparasites justiciables de quarantaine, pi&#233;geage, Anthonomus rubi, Anthonome du fraisier, fraisier"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health Science Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-09-17",
            "fr": "2020-09-17"
        },
        "modified": {
            "en": "2021-10-07",
            "fr": "2021-10-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Strawberry blossom weevil \u2013 Anthonomus rubi Herbst",
        "fr": "Anthonome du fraisier - Anthonomus rubi Herbst"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div>\n<div class=\"pull-left mrgn-rght-md col-sm-3\"><figure>\n<img alt=\"Anthonomus rubi\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_antrub_index_image1_1600199008962_eng.jpg\">\n<figcaption>Photo: Anthonomus rubi 5513196-LGPT Mariusz Sobieski, Bugwood.org</figcaption>\n</figure>\n</div>\n\n<p><i lang=\"la\">Anthonomus rubi</i> Herbst, Strawberry blossom weevil, is native to Europe where it is a serious pest of <i lang=\"la\">Fragaria</i> and <i lang=\"la\">Rubus</i> and has been reported in association with <i lang=\"la\">Rosa</i>. The Canadian Food Inspection Agency (CFIA) has confirmed that the pest is established in many municipalities of the Fraser Valley and metro Vancouver, British Columbia.</p>\n\n<p>If you think you have found a plant pest, <a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">report it</a> to the Canadian Food Inspection Agency.</p>\n</div>\n<div class=\"clearfix\"></div>\n\n<h2>What information is available</h2>\n\n<ul>\n<li><a href=\"https://www.pestalerts.org/official-pest-report/anthonomus-rubi-detection-canada-anthonomus-rubi-d-tection-au-canada\">North American Plant Protection Organization (NAPPO) pest alert</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/strawberry-blossom-weevil/fact-sheet/eng/1632774012031/1632774012719\">Pest fact sheet</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-left mrgn-rght-md col-sm-3\"><figure>\n<img alt=\"Anthonomus rubi\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_antrub_index_image1_1600199008962_fra.jpg\">\n<figcaption>Photo\u00a0: Anthonomus rubi 5513196-LGPTMariusz Sobieski, Bugwood.org</figcaption> </figure>\n</div>\n\n<p><i lang=\"la\">Anthonomus rubi</i> Herbst, Anthonome du fraisier, est originaire d'Europe o\u00f9 il est un organisme nuisible grave de <i lang=\"la\">Fragaria</i> et <i lang=\"la\">Rubus</i> et a \u00e9t\u00e9 signal\u00e9 en association avec <i lang=\"la\">Rosa</i>. L'Agence canadienne d'inspection des aliments (ACIA) a confirm\u00e9 que l'organisme nuisible est \u00e9tabli dans de nombreuses municipalit\u00e9s de la vall\u00e9e du Fraser et de la r\u00e9gion m\u00e9tropolitaine de Vancouver, en Colombie-Britannique.</p>\n\n<p>Si vous pensez d'avoir trouv\u00e9 un phytoravageur, <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">signaler-le</a> \u00e0 l'Agence canadienne d'inspection des aliments.</p>\n<div class=\"clearfix\"></div>\n\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition</h2>\n\n<ul>\n<li>Une alerte de l'organisation Nord-Am\u00e9ricaine, <a href=\"https://www.pestalerts.org/official-pest-report/anthonomus-rubi-detection-canada-anthonomus-rubi-d-tection-au-canada\">North American Plant Protection Organization (NAPPO) (en anglais seulement)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/anthonome-du-fraisier/fiche-d-information/fra/1632774012031/1632774012719\">Feuillet d'information</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}