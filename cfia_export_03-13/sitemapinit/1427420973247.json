{
    "dcr_id": "1427420901391",
    "title": {
        "en": "Regulatory Cooperation Council",
        "fr": "Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation"
    },
    "html_modified": "2024-03-12 3:57:02 PM",
    "modified": "2021-05-31",
    "issued": "2015-05-28",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/regu_coop_1427420901391_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/regu_coop_1427420901391_fra"
    },
    "ia_id": "1427420973247",
    "parent_ia_id": "1374871197211",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1374871197211",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Regulatory Cooperation Council",
        "fr": "Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation"
    },
    "label": {
        "en": "Regulatory Cooperation Council",
        "fr": "Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation"
    },
    "templatetype": "content page 1 column",
    "node_id": "1427420973247",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1374871172385",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/cfia-2025/rcc/",
        "fr": "/a-propos-de-l-acia/acia-2025/ccr/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Regulatory Cooperation Council",
            "fr": "Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation"
        },
        "description": {
            "en": "Canada and the United States are working together to align regulatory approaches between the two countries where possible.",
            "fr": "Le Canada et les \u00c9tats-Unis travaillent ensemble pour harmoniser dans la mesure du possible les approches r\u00e9glementaires des deux pays."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, Regulatory Cooperation Council, United States, Canada, United States Department of Agriculture, United States Food and Drug Administration",
            "fr": "Agence canadienne d'inspection des aliments, Conseil de coop\u00e9ration, \u00c9tats-Unis, Canada, United States Department of Agriculture, United States Food and Drug Administration"
        },
        "dcterms.subject": {
            "en": "food inspection,regulation,food safety",
            "fr": "inspection des aliments,r\u00e9glementation,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-05-28",
            "fr": "2015-05-28"
        },
        "modified": {
            "en": "2021-05-31",
            "fr": "2021-05-31"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Regulatory Cooperation Council",
        "fr": "Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Aligning regulatory approaches between Canada and the United States</h2>\n\n<p>Canada and the United States are working together to align regulatory approaches between the two countries where possible. Under the <a href=\"https://www.canada.ca/en/government/system/laws/developing-improving-federal-regulations/regulatory-cooperation.html\">Regulatory Cooperation Council</a> (RCC), the Canadian Food Inspection Agency (CFIA) works with its counterparts in the United States Department of Agriculture (USDA) and the United States Food &amp; Drug Administration (USFDA) in areas of plant health, animal health, meat inspection and food safety.</p>\n\n<p>The commitments are broadly explained in the <a href=\"https://www.canada.ca/en/treasury-board-secretariat/corporate/transparency/acts-regulations/canada-us-regulatory-cooperation-council/joint-forward-plan-august-2014.html\"><abbr title=\"Regulatory Cooperation Council\">RCC</abbr> Joint Forward Plan</a> and in the partnership statements between the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and its <abbr title=\"United States\">U.S.</abbr> counterparts below. The work plans provide more details on the specific initiatives themselves.</p>\n\n<h2>Regulatory Partnership Statements</h2>\n\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/usfda-partnership/eng/1427426961555/1427427039122\">Canadian Food Inspection Agency and United States Food and Drug Administration</a></p>\n\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/usda-partnership/eng/1427424036379/1427424070185\">Canadian Food Inspection Agency and United States Department of Agriculture</a></p>\n<h2>Consultations</h2>\n<p>The Canada-<abbr title=\"United States\">U.S.</abbr> Regulatory Cooperation Council (RCC) Joint Forward Plan emphasizes stakeholder input and engagement in the development of <abbr title=\"Regulatory Cooperation Council\">RCC</abbr> work plans.</p>\n\n\n<h3>Consultation Reports</h3>\n\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/foreign-animal-disease-zoning-recognition/eng/1463429758955/1463430336063\">Regulatory Cooperation Council \u2013 Foreign Animal Diseases Zoning Recognition: Consultation Report</a></p>\n\n<h2>Work Plans</h2>\n\n<h3>2016</h3>\n\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/animal-health-work-plan-2016/eng/1477315469797/1477315470157\">Animal Health</a></p>\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/food-safety-work-plan-2016/eng/1477315540530/1477315540857\">Food Safety</a></p>\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/meat-inspection-and-certification-work-plan-2016/eng/1477315400804/1477315401475\">Meat Inspection and Certification</a></p>\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/plant-health-work-plan-2016/eng/1480105297161/1480105297552\">Plant Health</a></p>\n\n<h3>2015</h3>\n\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/animal-health-work-plan-2015/eng/1431366802034/1431366802862\">Animal Health</a></p>\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/food-safety-work-plan-2015/eng/1431366847455/1431366848204\">Food Safety</a></p>\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/meat-inspection-and-certification-work-plan-2015/eng/1431366916641/1431366917328\">Meat Inspection and Certification</a></p>\n<p><a href=\"/about-the-cfia/cfia-2025/rcc/plant-health-work-plan-2015/eng/1431346390473/1431349334224\">Plant Health</a></p>\n\n<p>To stay informed by email about <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> initiatives under the <abbr title=\"Regulatory Cooperation Council\">RCC</abbr>, please subscribe to receive updates though the Agency <a href=\"https://notification.inspection.canada.ca/\">email notification service</a>. Please select the subject \"What's New\". If you are already subscribed to the service, you will simply need to add this topic to your list.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Harmonisation des approches r\u00e9glementaires du Canada et des \u00c9tats-Unis</h2>\n\n<p>Le Canada et les \u00c9tats-Unis travaillent ensemble pour harmoniser dans la mesure du possible les approches r\u00e9glementaires des deux pays. Sous la gouverne du <a href=\"https://www.canada.ca/fr/gouvernement/systeme/lois/developpement-amelioration-reglementation-federale/cooperation-matiere-reglementation.html\">Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation</a> (CCR), l'Agence canadienne d'inspection des aliments (ACIA) collabore avec ses homologues du <span lang=\"en\">United States Department of Agriculture</span> (USDA) et du <span lang=\"en\">United States Food and Drug Administration</span> (USFDA) dans les domaines de la protection des v\u00e9g\u00e9taux, de la sant\u00e9 des animaux, de l'inspection des viandes et de la salubrit\u00e9 des aliments.</p>\n\n<p>Les engagements sont expliqu\u00e9s bri\u00e8vement dans le <a href=\"https://www.canada.ca/fr/secretariat-conseil-tresor/organisation/transparence/lois-reglements/conseil-cooperation-canada-eu-reglementation/plan-prospectif-conjoint-aout-2014.html\">plan prospectif conjoint du <abbr title=\"Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation\">CCR</abbr></a> et dans les \u00e9nonc\u00e9s de partenariat entre l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et ses homologues am\u00e9ricains ci-dessous. Les plans de travail fournissent plus de d\u00e9tails sur les initiatives particuli\u00e8res.</p>\n\n<h2>\u00c9nonc\u00e9s de partenariat en mati\u00e8re de r\u00e9glementation</h2>\n\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/usfda-partenariat/fra/1427426961555/1427427039122\">Agence canadienne d'inspection des aliments et <span lang=\"en\">Food and Drug Administration</span> des \u00c9tats-Unis</a></p>\n\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/usda-partenariat/fra/1427424036379/1427424070185\">Agence canadienne d'inspection des aliments et <span lang=\"en\">United States Department of Agriculture</span></a></p>\n<h2>Consultations</h2>\n<p>Le plan prospectif conjoint du Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation (CCR) du Canada et des \u00c9tats-Unis mise sur les commentaires des intervenants et la participation de ces derniers \u00e0 l'\u00e9laboration des plans de travail du <abbr title=\"Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation\">CCR</abbr>.</p>\n\n\n<h3>Rapports de consultation</h3>\n\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/reconnaissance-des-zones-de-controle-des-maladies-/fra/1463429758955/1463430336063\">Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation\u00a0\u2013\u00a0Reconnaissance des zones de contr\u00f4le des maladies animales exotiques (MAE)\u00a0: Rapport de consultation</a></p>\n\n<h2>Plans de travail</h2>\n\n<h3>2016</h3>\n\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2016-sur-l-inspection-et-la-certif/fra/1477315400804/1477315401475\">Inspection et certification des viandes</a></p>\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2016-sur-la-protection-des-vegetau/fra/1480105297161/1480105297552\">Protection des v\u00e9g\u00e9taux</a></p>\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2016-sur-la-salubrite-alimentaire/fra/1477315540530/1477315540857\">Salubrit\u00e9 des aliments</a></p>\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2016-sur-la-sante-des-animaux/fra/1477315469797/1477315470157\">Sant\u00e9 des animaux</a></p>\n\n<h3>2015</h3>\n\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2015-sur-l-inspection-et-la-certif/fra/1431366916641/1431366917328\">Inspection et certification des viandes</a></p>\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2015-sur-la-protection-des-vegetau/fra/1431346390473/1431349334224\">Protection des v\u00e9g\u00e9taux</a></p>\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2015-sur-la-salubrite-des-aliments/fra/1431366847455/1431366848204\">Salubrit\u00e9 des aliments</a></p>\n<p><a href=\"/a-propos-de-l-acia/acia-2025/ccr/plan-de-travail-2015-sur-la-sante-des-animaux/fra/1431366802034/1431366802862\">Sant\u00e9 des animaux</a></p>\n\n<p>Pour rester au courant des initiatives de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> relevant du <abbr title=\"Conseil de coop\u00e9ration en mati\u00e8re de r\u00e9glementation\">CCR</abbr>, veuillez vous inscrire au <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">service d'avis par courriel</a> de l'Agence pour recevoir des mises \u00e0 jour. Veuillez s\u00e9lectionner le sujet \u00ab\u00a0Quoi de neuf\u00a0\u00bb. Si vous \u00eates d\u00e9j\u00e0 abonn\u00e9 au service d'avis par courriel, vous n'avez qu'\u00e0 ajouter ce sujet \u00e0 votre liste.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}