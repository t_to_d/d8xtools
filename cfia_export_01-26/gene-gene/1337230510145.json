{
    "dcr_id": "1337230510145",
    "title": {
        "en": "Facts about spring viraemia of carp",
        "fr": "Fiche de renseignements sur la vir\u00e9mie printani\u00e8re de la carpe"
    },
    "html_modified": "2024-01-26 2:21:09 PM",
    "modified": "2020-09-21",
    "issued": "2012-05-17 00:55:11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_springviraemia_factsheet_1337230510145_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_springviraemia_factsheet_1337230510145_fra"
    },
    "ia_id": "1337230669570",
    "parent_ia_id": "1337228157853",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1337228157853",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Facts about spring viraemia of carp",
        "fr": "Fiche de renseignements sur la vir\u00e9mie printani\u00e8re de la carpe"
    },
    "label": {
        "en": "Facts about spring viraemia of carp",
        "fr": "Fiche de renseignements sur la vir\u00e9mie printani\u00e8re de la carpe"
    },
    "templatetype": "content page 1 column",
    "node_id": "1337230669570",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1337228062625",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/spring-viraemia-of-carp/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/viremie-printaniere-de-la-carpe/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Facts about spring viraemia of carp",
            "fr": "Fiche de renseignements sur la vir\u00e9mie printani\u00e8re de la carpe"
        },
        "description": {
            "en": "Spring viraemia of carp is an infectious disease of finfish.  It is caused by a virus called spring viraemia of carp virus, which belongs to the family Rhabdoviridae.",
            "fr": "La vir\u00e9mie printani\u00e8re de la carpe est une maladie infectieuse qui touche les poissons. Elle est caus\u00e9e par un virus qui appartient \u00e0 la famille des Rhabdovirid\u00e9s."
        },
        "keywords": {
            "en": "infectious disease, finfish, reportable disease, aquatic animals, Spring viraemia, carp, factsheet",
            "fr": "maladie, touche les poissons, animaux aquatiques, Maladies d\u00e9clarables, Vir\u00e9mie printani\u00e8re, carpe, Fiche de renseignements"
        },
        "dcterms.subject": {
            "en": "inspection,infectious diseases,fish,fisheries products",
            "fr": "inspection,maladie infectieuse,poisson,produit de la peche"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Aquatic Animal Health Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux aquatiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-17 00:55:11",
            "fr": "2012-05-17 00:55:11"
        },
        "modified": {
            "en": "2020-09-21",
            "fr": "2020-09-21"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Facts about spring viraemia of carp",
        "fr": "Fiche de renseignements sur la vir\u00e9mie printani\u00e8re de la carpe"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-right mrgn-lft-md col-sm-5\"> \n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">What we know about the disease</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>disease only occurs in the freshwater environment</li>\n<li>outbreaks in ponds with common carp occur when water temperatures begin to rise in the spring</li>\n<li>signs of disease occur when the water temperature is between 5\u00b0C and 10\u00b0C</li>\n<li>mortality is most noticeable when the water temperature is between 11\u00b0C and 17\u00b0C</li>\n<li>younger finfish are more affected than older finfish</li>\n<li><i lang=\"la\">Carp sprivivirus</i> can survive in the mud of an earthen pond for more than a month</li>\n</ul>\n</div>\n</section>\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">What we would like to know about the disease</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>where this disease occurs in wild finfish populations outside of Ontario</li>\n<li>how susceptible are northern pike to this disease</li>\n</ul>\n</div>\n</section>\n</div>\n<p>Spring viraemia of carp (SVC) is an infectious disease of some freshwater finfish. It is caused by a virus called <i lang=\"la\">Carp sprivivirus</i>, formerly known as spring viraemia of carp virus, which belongs to the family <i lang=\"la\">Rhabdoviridae</i>.</p>\n<p>Spring viraemia of carp is not a risk to human health, nor is it a food safety issue.</p>\n<p>In Canada, SVC is a reportable aquatic animal disease. If you own or work with finfish and suspect or detect SVC, you must let the Canadian Food Inspection Agency (CFIA) know. Here's how:</p>\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/aquaculturists/eng/1450407254532/1450407255317\">Notification of reportable diseases by aquaculturists</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/veterinarians/eng/1450406407952/1450406408789\">Notification of reportable diseases by veterinarians</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/researchers/eng/1450405664371/1450405665318\">Notification of reportable and immediately notifiable diseases by researchers</a></li>\n</ul>\n<h2>Spring viraemia of carp in Canada</h2>\n<p>In Canada, the virus has been found in wild common carp in Ontario.</p>\n<p>The agency is currently conducting surveillance activities to determine how widespread the disease is in wild carp populations in Canada.</p>\n<p>For dates and locations (provinces/territories) of CFIA-confirmed cases of the disease, see <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/federally-reportable-aquatic-animal-diseases/eng/1339174937153/1339175227861\">Federally reportable aquatic animal diseases in Canada</a>.</p>\n<h2>Species of finfish that can be infected</h2>\n<p>Freshwater species of finfish that can become infected with <i lang=\"la\">Carp sprivivirus</i> are:</p>\n<ul>\n<li><i lang=\"la\">Abramis brama</i> (common bream) </li>\n<li><i lang=\"la\">Carassius auratus</i>\u00a0(goldfish)</li>\n<li><i lang=\"la\">Ctenopharyngodon idella</i>\u00a0(grass carp)</li>\n<li><i lang=\"la\">Cyprinus carpio</i>\u00a0(common carp)</li>\n<li><i lang=\"la\">Danio rerio</i>\u00a0(zebra danio)</li>\n<li><i lang=\"la\">Esox lucius</i>\u00a0(northern pike)</li>\n<li><i lang=\"la\">Hypophthalmichthys molitrix </i>(silver carp)</li>\n<li><i lang=\"la\">Hypophthalmichthys nobilis</i>\u00a0(bighead carp)</li>\n<li><i lang=\"la\">Notemigonus crysoleucas </i>(golden shiner)</li>\n<li><i lang=\"la\">Pimephales promelas</i>\u00a0(fathead minnow)</li>\n<li><i lang=\"la\">Percocypris pingi </i>(no common name)</li>\n<li><i lang=\"la\">Rutilus kutum </i>(Caspian white fish)</li>\n<li><i lang=\"la\">Rutilus rutilus</i>\u00a0(roach)</li>\n<li><i lang=\"la\">Silurus glanis</i> (wels catfish)</li>\n</ul>\n<h2>Signs of spring viraemia of carp</h2>\n<p>SVC is a cause of death in hatched finfish. Typically\u00a0 less than 30 percent of infected animals will die, but the death rate can range from 5 percent to 100 percent.</p>\n<p>Affected finfish may exhibit any of the following signs:</p>\n<p>Behaviour</p>\n<ul>\n<li>fish lying on their sides, often at the bottom of the tank</li>\n<li>fish congregating where water flow is slow and near the sides of ponds</li>\n</ul>\n<p>External appearance</p>\n<ul>\n<li>dark skin colour</li>\n<li>bulging eyes</li>\n<li>pale gills with or without areas of bleeding</li>\n<li>areas of bleeding in the skin, eyes, anal vent and at the base of fins</li>\n<li>swollen belly</li>\n<li>enlarged anal vent</li>\n<li>feces trailing from the anal vent</li>\n</ul>\n<p>Internal appearance</p>\n<ul>\n<li>bloody fluid in the abdominal cavity</li>\n<li>inflamed, bleeding or swollen spleen, kidney, intestines and air bladder</li>\n<li>areas of pinpoint bleeding in the fatty tissue surrounding organs, stomach wall and muscles</li>\n<li>empty intestine that may contain mucus</li>\n</ul>\n<h2>Managing spring viraemia of carp</h2>\n<p>There are no treatment options currently available for SVC.</p>\n<p>Finfish may contract the virus through contact with the discharged bodily wastes or mucus secretions from infected finfish. It can also spread as a result of water that has been contaminated with the virus.</p>\n<p>People can spread the virus to other finfish by moving infected live or dead finfish, using contaminated equipment, vehicles and vessels, or using or moving contaminated water.</p>\n<p>You can prevent the introduction of <i lang=\"la\">Carp sprivivirus</i> into your farm and prevent the spread of the virus within your farm by using specific safety practices. You can find out more about what practices you need to consider by visiting <a href=\"/animal-health/aquatic-animals/aquatic-animal-biosecurity/eng/1320594187303/1320594268146\">Aquatic animal biosecurity</a>. Talk to your veterinarian about what practices are most effective for the management of this disease for your farm.</p>\n<p>There is no vaccine approved in Canada against SVC. Talk to your veterinarian about whether a vaccine approved in another country can be an effective tool to manage this disease on your farm.</p>\n<h2>CFIA's initial response to a notification</h2>\n<p>When you notify a CFIA veterinary inspector of the possible presence of SVC in your finfish, we will launch an investigation, which could include an inspection of your facility.</p>\n<p>We will note all relevant information you can provide, such as species affected, life stage affected, signs of illness in the animals, presence of any other diseases and water temperature.</p>\n<p>At the same time, we will collect samples for testing at one of Canada's three National Aquatic Animal Health Laboratories.</p>\n<p>We evaluate all the collected information and test results to confirm the presence of SVC on the farm.</p>\n<p>The agency also confirms the presence of SVC in wild finfish. The agency receives notifications from provincial staff who have investigated a wild fish death event.</p>\n<h2>CFIA's response once the disease is confirmed</h2>\n<p>Further disease response measures include a tracing investigation to help us figure out how the disease entered your farm and if other farms could have been affected. An evaluation of the risk your farm poses to wild fisheries will also be carried out. If you are considering eradication of the disease from your finfish, we can provide you with advice on how to safely remove the disease from your farm.</p>\r\n<h2 class=\"font-medium black\">How do I get more information</h2>\n<p>For more information about reportable diseases, visit the <a href=\"/animal-health/aquatic-animals/eng/1299155892122/1320536294234\">Aquatic Animal Health</a> page, contact your <a href=\"/eng/1300462382369/1365216058692\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Animal Health Office</a>,  or your <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area office:</p>\n\n<ul><li>Atlantic: 506-777-3939</li>\n<li>Quebec: 514-283-8888</li>\n<li>Ontario: 226-217-8555</li>\n<li>West: 587-230-2200</li></ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-right mrgn-lft-md col-sm-5\"> \n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">Ce que nous savons sur la maladie</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>la maladie ne se manifeste qu'en milieu d'eau douce</li>\n<li>les \u00e9closions dans les bassins o\u00f9 vivent des carpes communes se produisent lorsque la temp\u00e9rature de l'eau commence \u00e0 monter au printemps</li>\n<li>les signes de maladie apparaissent lorsque la temp\u00e9rature de l'eau se situe entre 5 \u00b0C et 10 \u00b0C</li>\n<li>la mortalit\u00e9 est plus importante lorsque la temp\u00e9rature de l'eau se situe entre 11 \u00b0C et 17 \u00b0C</li>\n<li>les jeunes poissons sont plus touch\u00e9s que les poissons plus \u00e2g\u00e9s</li>\n<li><i lang=\"la\">Carp sprivivirus</i> peut survivre dans la boue d'un bassin de terre pendant plus d'un mois</li>\n</ul>\n</div>\n</section>\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">Ce que nous aimerions savoir sur la maladie</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>o\u00f9 cette maladie est pr\u00e9sente chez les populations de poissons sauvages en dehors de l'Ontario</li>\n<li>quelle est la sensibilit\u00e9 du grand brochet \u00e0 cette maladie</li>\n</ul>\n</div>\n</section>\n</div>\n<p>La vir\u00e9mie printani\u00e8re de la carpe (VPC) est une maladie infectieuse qui touche certains poissons d'eau douce. Elle est caus\u00e9e par un virus appel\u00e9 <i lang=\"la\">Carp sprivivirus</i>, anciennement connu sous le nom de virus de la vir\u00e9mie printani\u00e8re de la carpe, qui appartient \u00e0 la famille des Rhabdoviridae.</p>\n<p>La VPC ne repr\u00e9sente pas un risque pour la sant\u00e9 humaine ni un probl\u00e8me de salubrit\u00e9 des aliments.</p>\n<p>Au Canada, la VPC est une maladie des animaux aquatiques \u00e0 d\u00e9claration obligatoire. Si vous poss\u00e9dez ou manipulez des poissons dans le cadre de votre travail et que vous soup\u00e7onnez ou d\u00e9tectez la pr\u00e9sence de la VPC, vous devez informer l'Agence canadienne d'inspection des aliments (ACIA). Voici comment proc\u00e9der\u00a0:</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/aquaculteurs/fra/1450407254532/1450407255317\">D\u00e9claration obligatoire des maladies d\u00e9clarables d'animaux aquatiques par les aquaculteurs</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/veterinaires/fra/1450406407952/1450406408789\">D\u00e9claration obligatoire des maladies d\u00e9clarables d'animaux aquatiques par les v\u00e9t\u00e9rinaires</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/chercheurs/fra/1450405664371/1450405665318\">D\u00e9claration obligatoire des maladies d\u00e9clarables ou \u00e0 notification imm\u00e9diate d'animaux aquatiques par les chercheurs</a></li>\n</ul>\n<h2>Vir\u00e9mie printani\u00e8re de la carpe au Canada</h2>\n<p>Au Canada, le virus a \u00e9t\u00e9 d\u00e9tect\u00e9 dans la carpe commune sauvage en Ontario.</p>\n<p>L'Agence m\u00e8ne actuellement des activit\u00e9s de surveillance pour d\u00e9terminer l'\u00e9tendue de la maladie chez les populations de carpes sauvages au Canada.</p>\n<p>Pour conna\u00eetre les dates et les lieux (provinces et territoires) des cas de maladie confirm\u00e9s par l'Agence, veuillez consulter les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/maladies-des-animaux-aquatiques-a-declaration-obli/fra/1339174937153/1339175227861\">maladies des animaux aquatiques \u00e0 d\u00e9claration obligatoire au Canada</a>.</p>\n<h2>Esp\u00e8ces de poissons qui peuvent \u00eatre infect\u00e9es</h2>\n<p>Les esp\u00e8ces de poissons qui peuvent \u00eatre infect\u00e9es par <i lang=\"la\">Carp sprivivirus</i> sont les suivantes\u00a0:</p>\n<ul>\n<li><i lang=\"la\">Abramis brama</i> (br\u00e8me commune)</li>\n<li><i lang=\"la\">Carassius auratus</i> (carassin dor\u00e9)</li>\n<li><i lang=\"la\">Ctenopharyngodon idella</i> (carpe de roseau)</li>\n<li><i lang=\"la\">Cyprinus carpio</i> (carpe commune)</li>\n<li><i lang=\"la\">Danio rerio</i> (poisson z\u00e8bre)</li>\n<li><i lang=\"la\">Esox lucius</i> (grand brochet)</li>\n<li><i lang=\"la\">Hypophthalmichthys molitrix</i> (carpe argent\u00e9e)</li>\n<li><i lang=\"la\">Hypophthalmichthys nobilis</i> (carpe \u00e0 grosse t\u00eate)</li>\n<li><i lang=\"la\">Notemigonus crysoleucas</i> (m\u00e9n\u00e9 jaune)</li>\n<li><i lang=\"la\">Percocypris pingi</i> (aucun nom commun)</li>\n<li><i lang=\"la\">Pimephales promelas</i> (t\u00eate-de-boule)</li>\n<li><i lang=\"la\">Rutilus kutum</i> (poisson blanc de la mer Caspienne)</li>\n<li><i lang=\"la\">Rutilus rutilus</i> (gardon)</li>\n<li><i lang=\"la\">Silurus glanis</i> (silure glan)</li>\n</ul>\n<h2>Signes de la vir\u00e9mie printani\u00e8re de la carpe</h2>\n<p>La VPC peut entra\u00eener la mort des poissons apr\u00e8s l'\u00e9closion des \u0153ufs. En g\u00e9n\u00e9ral, moins de 30\u00a0% des animaux infect\u00e9s meurent, mais le taux de mortalit\u00e9 peut varier de 5\u00a0% \u00e0 100\u00a0%.</p>\n<p>Les poissons atteints peuvent pr\u00e9senter l'un des signes internes suivants\u00a0:</p>\n<p>Comportement</p>\n<ul>\n<li>sujets gisant sur le c\u00f4t\u00e9, souvent au fond du bassin</li>\n<li>sujets se rassemblant aux endroits o\u00f9 le d\u00e9bit de l'eau est lent et pr\u00e8s des bords des bassins</li>\n</ul>\n<p>Aspect externe</p>\n<ul>\n<li>coloration fonc\u00e9e de la peau</li>\n<li>yeux exorbit\u00e9s</li>\n<li>p\u00e2leur des branchies (avec pr\u00e9sence possible de zones d'h\u00e9morragie)</li>\n<li>zones d'h\u00e9morragie au niveau de la peau, des yeux et de l'anus et \u00e0 la base des nageoires</li>\n<li>abdomen distendu</li>\n<li>anus tum\u00e9fi\u00e9</li>\n<li>tra\u00een\u00e9es de f\u00e8ces \u00e0 partir de l'anus</li>\n</ul>\n<p>Aspect interne</p>\n<ul>\n<li>liquide teint\u00e9 de sang dans la cavit\u00e9 abdominale</li>\n<li>rate, reins, intestins et vessie gazeuse touch\u00e9s par une inflammation ou une h\u00e9morragie, ou encore tum\u00e9fi\u00e9s</li>\n<li>p\u00e9t\u00e9chies dans les tissus adipeux entourant les organes, sur la paroi de l'estomac et dans les muscles</li>\n<li>intestins vides qui peuvent contenir du mucus</li>\n</ul>\n<h2>Gestion de la vir\u00e9mie printani\u00e8re de la carpe</h2>\n<p>Il n'existe actuellement aucune option de traitement pour la VPC.</p>\n<p>Les poissons peuvent contracter le virus \u00e0 la suite d'un contact avec des excr\u00e9ments ou des s\u00e9cr\u00e9tions de mucus provenant de poissons infect\u00e9s. La VPC peut \u00e9galement se propager par l'eau qui a \u00e9t\u00e9 contamin\u00e9e par le virus.</p>\n<p>Les \u00eatres humains peuvent \u00e9galement transmettre le virus \u00e0 d'autres poissons en d\u00e9pla\u00e7ant des poissons infect\u00e9s, qu'ils soient vivants ou morts, en utilisant de l'\u00e9quipement, des v\u00e9hicules et des navires contamin\u00e9s, ou en utilisant ou d\u00e9pla\u00e7ant de l'eau contamin\u00e9e.</p>\n<p>Vous pouvez pr\u00e9venir l'introduction du virus de la VPC dans votre exploitation et emp\u00eacher la propagation du virus au sein de votre exploitation en adoptant certaines pratiques en mati\u00e8re de s\u00e9curit\u00e9. Pour en savoir plus sur les pratiques que vous devez prendre en consid\u00e9ration, veuillez consulter la page Web intitul\u00e9e <a href=\"/sante-des-animaux/animaux-aquatiques/biosecurite-relative-aux-animaux-aquatiques/fra/1320594187303/1320594268146\">Bios\u00e9curit\u00e9 relative aux animaux aquatiques</a>. Demandez \u00e0 votre v\u00e9t\u00e9rinaire quelles sont les pratiques les plus efficaces pour g\u00e9rer cette maladie dans votre exploitation.</p>\n<p>Il n'y a aucun vaccin approuv\u00e9 au Canada contre la VPC. Demandez \u00e0 votre v\u00e9t\u00e9rinaire si ce vaccin peut \u00eatre un outil efficace pour g\u00e9rer cette maladie dans votre exploitation.</p>\n<h2>Intervention initiale de l'ACIA visant \u00e0 donner suite \u00e0 une notification</h2>\n<p>Lorsque vous informez un v\u00e9t\u00e9rinaire-inspecteur de l'ACIA de la pr\u00e9sence possible de la VPC chez vos poissons, nous lan\u00e7ons une enqu\u00eate, qui peut inclure une inspection de l' \u00e9tablissement.</p>\n<p>Nous prendrons note de tous les renseignements pertinents que vous pourrez nous fournir, tels que les esp\u00e8ces touch\u00e9es, l'\u00e9tape du cycle de vie touch\u00e9e, les signes de maladie chez les animaux, les ant\u00e9c\u00e9dents de vaccination, la pr\u00e9sence de toute autre maladie et la temp\u00e9rature de l'eau.</p>\n<p>Nous pr\u00e9l\u00e8verons \u00e9galement des \u00e9chantillons pour les \u00e9preuves de laboratoire dans l'un des trois Laboratoires nationaux pour la sant\u00e9 des animaux aquatiques du Canada.</p>\n<p>Nous \u00e9valuons l'ensemble des renseignements recueillis et des r\u00e9sultats des \u00e9preuves pour confirmer la pr\u00e9sence de la VPC \u00e0 l'exploitation.</p>\n<p>L'Agence confirme \u00e9galement la pr\u00e9sence de la VPC chez les poissons sauvages. L'Agence re\u00e7oit des notifications du personnel provincial qui a enqu\u00eat\u00e9 sur un cas de d\u00e9c\u00e8s de poissons sauvages.</p>\n<h2>Intervention de l'ACIA une fois que la pr\u00e9sence de la maladie est confirm\u00e9e</h2>\n<p>D'autres mesures d'intervention pour lutter contre la maladie comprennent une enqu\u00eate de retra\u00e7age pour nous aider \u00e0 d\u00e9terminer de quelle fa\u00e7on la maladie s'est introduite dans votre exploitation et si d'autres exploitations ont pu avoir \u00e9t\u00e9 touch\u00e9es.</p>\n<p>Une \u00e9valuation du risque que votre exploitation repr\u00e9sente pour les p\u00eaches sauvages sera \u00e9galement effectu\u00e9e.</p>\n<p>Si vous envisagez d'\u00e9radiquer la maladie chez vos poissons, nous pouvons vous donner des conseils sur la mani\u00e8re d'\u00e9liminer la maladie de votre \u00e9levage en toute s\u00e9curit\u00e9.</p>\r\n<h2 class=\"font-medium black\">Comment puis-je obtenir davantage de renseignements</h2>\n<p>Pour de plus amples renseignements sur les maladies d\u00e9clarables, consultez la page <a href=\"/sante-des-animaux/animaux-aquatiques/fra/1299155892122/1320536294234\">Sant\u00e9 des animaux aquatiques</a>,  communiquez avec votre <a href=\"/fra/1300462382369/1365216058692\">bureau local de sant\u00e9 des animaux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou  le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de votre centre op\u00e9rationnel\u00a0: </p>\n<ul><li>Atlantique : 506-777-3939</li>\n<li>Qu\u00e9bec : 514-283-8888</li>\n<li>Ontario : 226-217-8555</li>\n<li>Ouest : 587-230-2200</li></ul>\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}