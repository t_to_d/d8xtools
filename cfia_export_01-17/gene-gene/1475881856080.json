{
    "dcr_id": "1475881856080",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Tribulus terrestris</i> (Puncture vine)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Tribulus terrestris</i> (Croix-de-Malte)"
    },
    "html_modified": "14-7-2017",
    "modified": "6-7-2017",
    "issued": "11-7-2014",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_tribulus_terrestris_1475881856080_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_tribulus_terrestris_1475881856080_fra"
    },
    "parent_ia_id": "1333136685768",
    "ia_id": "1475881856436",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Tribulus terrestris</i> (Puncture vine)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Tribulus terrestris</i> (Croix-de-Malte)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Tribulus terrestris</i> (Puncture vine)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Tribulus terrestris</i> (Croix-de-Malte)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1475881856436",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/tribulus-terrestris/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/tribulus-terrestris/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Tribulus terrestris (Puncture vine)",
            "fr": "Semence de mauvaises herbe : Tribulus terrestris (Croix-de-Malte)"
        },
        "description": {
            "en": "Fact sheet for Dodder (Cuscuta spp.)",
            "fr": "Fiche de renseignements pour V\u00e9lar d'Orient (Conringia orientalis)"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Tribulus terrestris, Zygophyllaceae, Puncture vine",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Tribulus terrestris, Zygophyllaceae, Croix-de-Malte"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,plants,weeds",
            "fr": "cultures,grain,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-07-14",
            "fr": "2017-07-14"
        },
        "modified": {
            "en": "2017-07-06",
            "fr": "2017-07-06"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Tribulus terrestris (Puncture vine)",
        "fr": "Semence de mauvaises herbe : Tribulus terrestris (Croix-de-Malte)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Family</h2>\n<p><i lang=\"la\">Zygophyllaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Puncture vine</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"British Columbia\">BC</abbr> and <abbr title=\"Ontario\">ON</abbr> (Brouillet <abbr title=\"and other\">et al.</abbr> 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Exact native range obscure, possibly native to Africa, Europe, temperate Asia and Australia (USDA-ARS 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Introduced in North and South America. Found throughout the United States with the exception of a few states in the eastern and northeastern regions (Kartesz 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual or biennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Bur, divided into 5 nutlets , each nutlet contains 2-4 seeds (DiTomaso and Healy 1993<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>)</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Nutlet length: 5.0 - 6.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Nutlet width: 3.5 - 4.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed length: 2.0 - 5.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Nutlet is wedge-shaped, three-angled in cross-section, with several spines.</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Nutlet is dull and woody</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Nutlet is straw yellow to grey-yellow</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed contained in the nutlet is wedge-shaped and pale yellow.</li>\n<li>Largest spine on nutlet is 5.0-6.0 <abbr title=\"millimetres\">mm</abbr> long.</li>\n</ul>\n\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, pastures, gardens, orchards, vineyards, roadsides and disturbed areas (Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>, CABI 2016<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). A weed of cereals, corn, legumes, tree crops, vegetables and ornamentals across the world (CABI 2016<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Puncture vine fruits easily become attached to livestock, people, farm machinery, and tires for dispersal. The spiny fruits are aligned so that at least one spine is pointing upwards when lying on the soil surface. Puncture vine may also be dispersed by water or as a contaminant in hay, straw, manure, sand, gravel, and dried fruit (CABI 2016<sup id=\"fn6c-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n<p>One plant can produce more than 5000 nutlets (CABI 2016<sup id=\"fn6d-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>). Seeds may remain viable in the ground for up to 5 years (University of California  Agriculture &amp; Natural Resources 2006<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote\u00a0</span>7</a></sup>).</p>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_06cnsh_1475608637509_eng.jpg\" alt=\"Puncture vine (Tribulus terrrestris) nutlets\" class=\"img-responsive\">\n<figcaption>Puncture vine (<i lang=\"la\">Tribulus terrrestris</i>) nutlets</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_03cnsh_1475608580504_eng.jpg\" alt=\"Puncture vine (Tribulus terrrestris) nutlet\" class=\"img-responsive\">\n<figcaption>Puncture vine (<i lang=\"la\">Tribulus terrrestris</i>) nutlet</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_04cnsh_1475608599616_eng.jpg\" alt=\"Puncture vine (Tribulus terrrestris) nutlet\" class=\"img-responsive\">\n<figcaption>Puncture vine (<i lang=\"la\">Tribulus terrrestris</i>) nutlet</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_07cnsh_1475608655148_eng.jpg\" alt=\"Puncture vine (Tribulus terrrestris) seed\" class=\"img-responsive\">\n<figcaption>Puncture vine (<i lang=\"la\">Tribulus terrrestris</i>) seed</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_08cnsh_1475608675616_eng.jpg\" alt=\"Puncture vine (Tribulus terrrestris) nutlet, cross-section\" class=\"img-responsive\">\n<figcaption>Puncture vine (<i lang=\"la\">Tribulus terrrestris</i>) nutlet, cross-section</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\"><p><strong>USDA-ARS 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, N.C., www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>DiTomaso, J. M. and Healy, E. A. 2007.</strong> Weeds of California and Other Western States. <abbr title=\"Volume\">Vol.</abbr> 1. 834 <abbr title=\"pages\">pp.</abbr> University of California, <abbr title=\"California\">CA</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>CABI. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 7</dt>\n<dd id=\"fn7\">\n<p><strong>University of California Agriculture &amp; Natural Resources. 2006</strong>. Pests in gardens and landscapes: Puncture vine, http://www.ipm.ucdavis.edu/PMG/PESTNOTES/pn74128.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>7<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Famille</h2>\n<p><i lang=\"la\">Zygophyllaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Croix-de-Malte</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> L'esp\u00e8ce est pr\u00e9sente en <abbr title=\"Colombie-Britannique\">C.-B.</abbr> et en <abbr title=\"Ontario\">Ont.</abbr> (Brouillet <abbr title=\"et autre\">et al.</abbr>, 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Aire d'indig\u00e9nat exacte incertaine; peut-\u00eatre indig\u00e8ne d'Afrique, d'Europe, d'Asie temp\u00e9r\u00e9e et d'Australie (USDA-ARS, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Introduite en Am\u00e9rique du Nord et en Am\u00e9rique du Sud. Elle est pr\u00e9sente dans l'ensemble des \u00c9tats-Unis, sauf dans quelques \u00c9tats de l'Est et du Nord-Est (Kartesz, 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle ou bisannuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Fruit \u00e0 enveloppe \u00e9pineuse divis\u00e9 en 5 nucules, contenant chacune 2 \u00e0 4 graines (DiTomaso et Healy 1993<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>)</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la nucule\u00a0: 5,0 - 6,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la nucule\u00a0: 3,5 - 4,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Longueur de la graine\u00a0: 2,0 - 5,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>\tLa nucule est cun\u00e9iforme, triangulaire en coupe transversale, et est munie de plusieurs \u00e9pines.</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Surface de la nucule mate et ligneuse</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Nucule jaune paille \u00e0 jaune-gris</li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>La graine contenue dans la nucule est cun\u00e9iforme et jaune p\u00e2le.</li>\n<li>L'\u00e9pine la plus grosse de la nucule mesure 5,0 \u00e0 6,0 <abbr title=\"millim\u00e8tre\">mm</abbr> de longueur.</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, p\u00e2turages, jardins, vergers, vignobles, bords de chemin et terrains perturb\u00e9s (Darbyshire, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>; CABI, 2016<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>). Mauvaise herbe des cultures de c\u00e9r\u00e9ales, de ma\u00efs, de l\u00e9gumineuses, d'arbres, de l\u00e9gumes et de plantes ornementales partout dans le monde (CABI, 2016<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le fruit de la croix-de-Malte reste facilement accroch\u00e9 au b\u00e9tail, aux personnes, \u00e0 la machinerie agricole et aux pneus et est ainsi dispers\u00e9. La disposition des \u00e9pines de l'enveloppe fait en sorte qu'au moins une \u00e9pine est orient\u00e9e vers le haut lorsque le fruit se trouve sur le sol. En outre, les graines peuvent \u00eatre dispers\u00e9es par l'eau ou par l'entremise de foin, de paille, de fumier, de sable, de gravier et de fruits s\u00e9ch\u00e9s contamin\u00e9s (CABI, 2016<sup id=\"fn6c-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>).</p>\n<p>Chaque plante peut produire plus de 5 000 nucules (CABI, 2016<sup id=\"fn6d-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>). Les graines peuvent demeurer viables dans le sol durant jusqu'\u00e0 5 ans (<span lang>University of California Agriculture &amp; Natural Resources</span>, 2006<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>7</a></sup>).</p>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_06cnsh_1475608637509_fra.jpg\" alt=\"Croix-de-Malte (Tribulus terrrestris) nucules\" class=\"img-responsive\">\n<figcaption>Croix-de-Malte (<i lang=\"la\">Tribulus terrrestris</i>) nucule</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_03cnsh_1475608580504_fra.jpg\" alt=\"Croix-de-Malte (Tribulus terrrestris) nucule\" class=\"img-responsive\">\n<figcaption>Croix-de-Malte (<i lang=\"la\">Tribulus terrrestris</i>) nucule</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_04cnsh_1475608599616_fra.jpg\" alt=\"Croix-de-Malte (Tribulus terrrestris) nucule\" class=\"img-responsive\">\n<figcaption>Croix-de-Malte (Tribulus terrrestris) nucule</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_07cnsh_1475608655148_fra.jpg\" alt=\"Croix-de-Malte (Tribulus terrrestris) graine\" class=\"img-responsive\">\n<figcaption>Croix-de-Malte (<i lang=\"la\">Tribulus terrrestris</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_tribulus_terrestris_08cnsh_1475608675616_fra.jpg\" alt=\"Croix-de-Malte (Tribulus terrrestris) nucule, section transversale\" class=\"img-responsive\">\n<figcaption>Croix-de-Malte (<i lang=\"la\">Tribulus terrrestris</i>)  nucule, section transversale</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>USDA-ARS 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, N.C., www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>DiTomaso, J. M. and Healy, E. A. 2007.</strong> Weeds of California and Other Western States. <abbr title=\"Volume\">Vol.</abbr> 1. 834 <abbr title=\"pages\">pp.</abbr> University of California, <abbr title=\"California\">CA</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>CABI. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n\n<dt>Note de bas de page 7</dt>\n<dd id=\"fn7\">\n<p lang=\"en\"><strong>University of California Agriculture &amp;amp Natural Resources. 2006.</strong>  Pests in gardens and landscapes: Puncture vine, http://www.ipm.ucdavis.edu/PMG/PESTNOTES/pn74128.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>7</a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}