{
    "dcr_id": "1657306963131",
    "title": {
        "en": "Livestock feed approval or registration: Eligibility requirements",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "html_modified": "2024-02-13 9:55:52 AM",
    "modified": "2022-07-19",
    "issued": "2022-07-19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/2_eligibility_lvstck_fd_apprvl_1657306963131_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/2_eligibility_lvstck_fd_apprvl_1657306963131_fra"
    },
    "ia_id": "1657306963740",
    "parent_ia_id": "1627997833424",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1627997833424",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Livestock feed approval or registration: Eligibility requirements",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "label": {
        "en": "Livestock feed approval or registration: Eligibility requirements",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "templatetype": "content page 1 column",
    "node_id": "1657306963740",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1627997831971",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/approval-and-registration/livestock-feed/",
        "fr": "/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Livestock feed approval or registration: Eligibility requirements",
            "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Conditions d'\u00e9ligibilit\u00e9"
        },
        "description": {
            "en": "Animals, Animal Health",
            "fr": "Animals, Animal Health"
        },
        "keywords": {
            "en": "Animals, Animal Health, livestock, feed, eligibility, application, regulation",
            "fr": "aliments, animaux de ferme, \u00e9ligibilit\u00e9, demande"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "modified": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Livestock feed approval or registration: Eligibility requirements",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657223639320/1657223788955\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\" href=\"\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657309218484/1657309218702\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657557647452/1657557647827\">4. How to apply</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657558817043/1657558817433\">5. After you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657559631753/1657559632175\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<h2>Make sure your product is regulated under the <i>Feeds Act</i> and regulations</h2>\n\t\n<h3>Products fed to livestock</h3>\n\t\n<p>For an ingredient to be assessed and authorized as a livestock feed ingredient by the CFIA, it must first be classified as a livestock feed. There are other substances that can be orally consumed by livestock which are not livestock feed and are regulated differently. These include:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>therapeutic (drug) products (regulated under the <i>Food and Drugs Act</i>)</li>\n<li>pest control products (regulated under the <i>Pest Control Products Act</i>)</li>\n<li>veterinary biologics (regulated under the <i>Health of Animals Act</i>)</li>\n</ul>\n\t\n<p>The intended use of the product and its mode of action are important elements in determining how the product is regulated in Canada.</p>\n\t\n<h3>Veterinary drugs or livestock feeds</h3>\n\t\n<p>In Canada, products consumed by livestock species are largely regulated as either veterinary drugs or livestock feeds. Veterinary drugs are regulated under the <i>Food and Drugs Act</i> and regulations by Health Canada while livestock feeds are regulated under the <i>Feeds Act</i> and regulations, which are administered by the CFIA. The overlap of the definitions for these\u00a02 types of products creates a challenge whereby nutritional and non-nutritional products may be classified as either a drug or a livestock feed, or both.</p>\n\t\n<p>For more information regarding the classification of these products, please refer to the <a href=\"https://www.canada.ca/en/health-canada/services/drugs-health-products/veterinary-drugs/legislation-guidelines/guidance-documents/guidance-document-classification-veterinary-drugs-livestock-feeds.html\">Guidance document on classification of veterinary drugs and livestock feeds</a>.</p>\n\t\n<h2>Find out if you need to approve or register your livestock feed</h2>\n\n<div class=\"row\">\n\t\n<div class=\"col-md-8\">\n\t\n<p>You'll need to approve your livestock feed, other than a mixed feed, if the feed:</p>\n\n<ul class=\"lst-spcd\">\n<li>is new (that is, not already listed in Schedules IV and V of the <i>Feeds Regulations</i>),</li>\n<li>a feed that has a novel trait<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote </span>1</a></sup> , or </li>\n<li>is a single ingredient feed (SIF) listed in the Schedule and whose description differs from that set out for that feed in that Schedule in terms of:\n<ul class=\"lst-spcd\">\n<li>its purpose</li>\n<li>its composition, including any hazard inherent in the feed, its structure, its nutritional quality or its physiological effects</li>\n<li>the process by which it is manufactured, and</li>\n<li>the species or class of livestock for which it is intended and the usage rate</li>\n</ul>\n</li>\n</ul>\n\t\n<p>You'll need to register your livestock feed if:</p>\n<ul>\n<li>your feed meets 1\u00a0or more of the criteria outlined in the list of <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/eng/1329109265932/1329109385432#intro-14\">feeds requiring registration</a></li>\n</ul>\n\t\n</div>\n\t\n<div class=\"col-sm-4 pull-right mrgn-tp-0 mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Additional information</h2>\n</header>\n<div class=\"panel-body\">\n<p>The following regulatory guidance could help you to complete your application</p>\n<ul>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/eng/1329109265932/1329109385432\">RG-1: Feed registration procedures and labelling standards</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-6/eng/1329275341920/1329275491608\">RG-6 Ethanol distillers' grains for livestock feed</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-8/eng/1347383943203/1347384015909\">RG-8 Contaminants in feed</a></li>\n</ul>\n</div>\n</section>\n</div>\n\t\n</div>\n\t\n<h2>Exemptions</h2>\n\t\n<p>You don't need to approve or register your product if it:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>is not regulated under the <i>Feeds Act</i> and regulations</li>\n<li>meets the criteria for exemption under section 4 of the <a href=\"https://laws-lois.justice.gc.ca/eng/acts/f-9/page-1.html#docCont\"><i>Feeds Act</i></a> and section 3 of the <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-2.html#h-878931\"><i>Feeds Regulations</i></a></li>\n<li>meets the exemption criteria for registration as outlined in <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-2.html#h-879019\">section 5 of the <i>Feeds Regulations</i></a></li>\n</ul>\n\t\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Additional information</h2>\n</header>\n<div class=\"panel-body\">\n<p>Ingredients listed in Part\u00a0I of either <span class=\"nowrap\">Schedule IV or V</span> are exempt from registration; as long as they:</p>\n<ul class=\"lst-spcd\">\n<li>meet the standards for composition described in the ingredient definition</li>\n<li>are labelled appropriately</li>\n<li>meet the standards as defined in the Regulations and further elaborated in regulatory guidance (for example, <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-8/eng/1347383943203/1347384015909\">RG-8 Contaminants in feed</a>)</li>\n</ul>\n<p>Furthermore, many complete feeds, supplements and macro premixes are exempt from registration as long as they follow all the labelling standards and requirements set out in the <i>Feeds Regulations</i>.</p>\n<p>For more information about Schedules IV and V and what livestock feeds require registration, refer to the <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/eng/1329109265932/1329109385432#intro\">introduction</a> of the RG-1 document.</p>\n</div>\n</section>\n\t\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Footnotes</h2>\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p>novel trait, in respect of a feed, means a characteristic of the feed that</p>\n<p>(a) has been intentionally selected, created or introduced into the feed through a specific genetic change, and</p>\n<p>(b) based on valid scientific rationale, is not substantially equivalent, in terms of its specific use and safety both for the environment and for human and animal health, to any characteristic of a similar feed that is set out in Schedule IV or V;</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote </span>1<span class=\"wb-inv\"> referrer</span></a></p>\n</dd>\n</dl>\n</aside>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657223639320/1657223788955\" rel=\"prev\">Previous: Overview</a></li>\n<li class=\"next\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657309218484/1657309218702\" rel=\"next\">Next: Before you apply</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657223639320/1657223788955\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\" href=\"\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657309218484/1657309218702\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657557647452/1657557647827\">4. Comment pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657558817043/1657558817433\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657559631753/1657559632175\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\n<h2>Veuillez-vous assurer que votre produit est r\u00e9glement\u00e9 en vertu de la <i>Loi sur les aliments du b\u00e9tail</i> et son r\u00e8glement d'application</h2>\n\t\n<h3>Produits nourris aux animaux de ferme</h3>\n\t\n<p>Pour qu'un ingr\u00e9dient soit \u00e9valu\u00e9 et autoris\u00e9 par l'ACIA en tant qu'ingr\u00e9dient d'aliments pour animaux de ferme, il doit d'abord \u00eatre class\u00e9 en tant qu'aliment pour animaux de ferme. Il existe d'autres substances qui peuvent \u00eatre ing\u00e9r\u00e9es par les animaux de ferme qui ne sont pas des aliments pour animaux de ferme et qui sont r\u00e9glement\u00e9es diff\u00e9remment. Il s'agit notamment des\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>produits th\u00e9rapeutiques (m\u00e9dicaments) (r\u00e9glement\u00e9s en vertu de la <i>Loi sur les aliments et drogues</i>)</li>\n<li>produits antiparasitaires (r\u00e9glement\u00e9s en vertu de la <i>Loi sur les produits antiparasitaires</i>)</li>\n<li>produits biologiques v\u00e9t\u00e9rinaires (r\u00e9glement\u00e9s en vertu de la <i>Loi sur la sant\u00e9 des animaux</i>)</li>\n</ul>\n\n<p>L'utilisation pr\u00e9vue du produit et son mode d'action sont des \u00e9l\u00e9ments importants pour d\u00e9terminer comment le produit est r\u00e9glement\u00e9 au Canada.</p>\n\t\n<h3>M\u00e9dicaments v\u00e9t\u00e9rinaires ou aliments pour animaux de ferme</h3>\n\t\n<p>Au Canada, les produits consomm\u00e9s par les esp\u00e8ces des animaux de ferme sont en grande partie r\u00e9glement\u00e9s comme des m\u00e9dicaments v\u00e9t\u00e9rinaires ou aliments pour animaux de ferme. Les m\u00e9dicaments v\u00e9t\u00e9rinaires sont r\u00e9glement\u00e9s par la <i>Loi sur les aliments et drogues</i> et son r\u00e8glement par Sant\u00e9 Canada, tandis que les aliments pour animaux de ferme sont r\u00e9glement\u00e9s par la <i>Loi sur les aliments du b\u00e9tail</i> et de son r\u00e8glement d'application, qui sont administr\u00e9s par l'ACIA. Le chevauchement des d\u00e9finitions de ces 2\u00a0types de produits cr\u00e9e un d\u00e9fi dans la mesure o\u00f9 les produits nutritionnels et non nutritionnels peuvent \u00eatre class\u00e9s soit comme un m\u00e9dicament, soit comme un aliment pour animaux de ferme, ou les deux.</p>\n\t\n<p>Pour plus d'informations concernant la classification de ces produits, veuillez consulter le <a href=\"https://www.canada.ca/fr/sante-canada/services/medicaments-produits-sante/medicaments-veterinaires/legislation-lignes-directrices/lignes-directrices/document-orientation-classification-medicaments-veterinaires-aliments-betail.html\">Document d'orientation sur la classification des m\u00e9dicaments v\u00e9t\u00e9rinaires et des aliments du b\u00e9tail</a>.</p>\n\t\n<h2>D\u00e9terminez si vous devez faire approuver ou enregistrer vos aliments pour animaux de ferme</h2>\n\t\n<div class=\"row\">\n\t\n<div class=\"col-md-8\">\n\t\n<p>Vous devrez faire approuver votre aliment pour animaux de ferme, autre qu'un aliment m\u00e9lang\u00e9, si l'aliment pour animaux\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>est nouveau (c'est-\u00e0-dire qu'il n'est pas d\u00e9j\u00e0 inscrit aux annexes IV et V du <i>R\u00e8glement sur les aliments du b\u00e9tail</i>),</li>\n<li>un aliment pour animaux qui a un caract\u00e8re nouveau<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote </span>1</a></sup>, ou</li>\n<li>est un aliment \u00e0 ingr\u00e9dient unique (AIU) figurant dans l'annexe et dont la description diff\u00e8re de celle \u00e9tablie pour cet aliment pour animaux dans cette annexe en termes\u00a0:\n<ul>\n<li>de son but</li>\n<li>de sa composition, y compris tout danger inh\u00e9rent \u00e0 l'aliment pour animaux, sa structure, sa qualit\u00e9 nutritionnelle ou ses effets physiologiques</li>\n<li>du processus par lequel il est fabriqu\u00e9, et</li>\n<li>de l'esp\u00e8ce ou la classe d'animaux de ferme \u00e0 laquelle il est destin\u00e9 et le taux d'utilisation</li>\n</ul>\n</li>\n</ul>\n\n<p>Vous devrez enregistrer vos aliments pour animaux de ferme si\u00a0:</p>\n\n<ul>\n<li>votre aliment pour animaux r\u00e9pond \u00e0 un ou plusieurs des crit\u00e8res \u00e9nonc\u00e9s dans la liste des <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/fra/1329109265932/1329109385432#intro-14\">aliments devant \u00eatre enregistr\u00e9s</a></li>\t\n</ul>\n\t\n</div>\n\t\n<div class=\"col-sm-4 pull-right mrgn-tp-0 mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Informations suppl\u00e9mentaires</h2>\n</header>\n<div class=\"panel-body\">\n<p>Les directives r\u00e9glementaires suivantes pourraient vous aider \u00e0 remplir votre demande</p>\n<ul>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/fra/1329109265932/1329109385432\">RG-1 Proc\u00e9dures d'enregistrement et normes d'\u00e9tiquetage des aliments pour animaux</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-6/fra/1329275341920/1329275491608\">RG-6 Dr\u00eaches de distillerie provenant de la production d'\u00e9thanol utilis\u00e9es dans les aliments du b\u00e9tail</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-8/fra/1347383943203/1347384015909\">RG-8 Contaminants dans les aliments du b\u00e9tail</a></li>\n</ul>\n</div>\n</section>\n</div>\n\t\n</div>\n\t\n<h2>Exemptions</h2>\n\t\n<p>Vous n'avez pas besoin d'approuver ou d'enregistrer votre produit s'il\u00a0:</p>\n\n<ul class=\"lst-spcd\">\n<li>n'est pas r\u00e9glement\u00e9 en vertu de la Loi relative aux aliments du b\u00e9tail et son r\u00e8glement d'application;</li>\n<li>r\u00e9pond aux crit\u00e8res d'exemption en vertu de l'article\u00a04 de la <a href=\"https://laws-lois.justice.gc.ca/fra/lois/f-9/page-1.html#h-236318\"><i>Loi relative aux aliments du b\u00e9tail</i></a> et de l'article\u00a03 du <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-2.html\"><i>R\u00e8glement sur les aliments du b\u00e9tail</i></a>;</li>\n<li>r\u00e9pond aux crit\u00e8res d'exemption d'enregistrement tels qu'\u00e9nonc\u00e9s \u00e0 l'article 5 du <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-2.html#h-867367\"><i>R\u00e8glement sur les aliments du b\u00e9tail</i></a>.</li>\n</ul>\n\t\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Information suppl\u00e9mentaire</h2>\n</header>\n<div class=\"panel-body\">\n<p>Les ingr\u00e9dients \u00e9num\u00e9r\u00e9s dans la Partie 1 de l'annexe IV ou V sont exempt\u00e9s d'enregistrement, tant qu'ils\u00a0:</p>\n<ul class=\"lst-spcd\">\n<li>r\u00e9pondent aux normes de composition d\u00e9crites dans la d\u00e9finition des ingr\u00e9dients</li>\n<li>sont \u00e9tiquet\u00e9s de mani\u00e8re appropri\u00e9e</li>\n<li>les directives r\u00e9glementaires (par exemple, <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-8/fra/1347383943203/1347384015909\">RG-8 Contaminants dans les aliments du b\u00e9tail</a>)</li>\n</ul>\n<p>De plus, un grand nombre d'aliments complets pour animaux, suppl\u00e9ments et macro-pr\u00e9m\u00e9langes sont exempt\u00e9s d'enregistrement tant qu'ils respectent toute les normes et exigences d'\u00e9tiquetage \u00e9nonc\u00e9es dans le <i>R\u00e8glement sur les aliments du b\u00e9tail</i>.</p>\n<p>Pour plus d'informations sur les annexes IV et V et sur les aliments pour animaux de ferme qui doivent \u00eatre enregistr\u00e9s, veuillez consulter <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/fra/1329109265932/1329109385432#intro\">l'introduction</a> du document RG-1.</p>\n</div>\n</section>\n\t\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Notes de bas de page</h2>\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p><strong><i>caract\u00e8re nouveau</i></strong> est un caract\u00e8re d'un aliment pour animaux qui\u00a0:</p>\n<ol class=\"lst-lwr-alph\">\n<li>d'une part, a \u00e9t\u00e9 intentionnellement s\u00e9lectionn\u00e9, cr\u00e9\u00e9 ou incorpor\u00e9 dans celui-ci par une modification g\u00e9n\u00e9tique particuli\u00e8re;</li>\n<li>d'autre part, en ce qui a trait \u00e0 son usage particulier et \u00e0 son innocuit\u00e9 tant pour l'environnement que pour la sant\u00e9 humaine et animale, sur la foi d'une justification scientifique valable, n'est essentiellement \u00e9quivalent \u00e0 aucun caract\u00e8re d'un aliment semblable mentionn\u00e9 aux annexes IV ou V.</li>\n</ol>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote </span>1<span class=\"wb-inv\"> referrer</span></a></p>\n</dd>\n</dl>\n</aside>\n\t\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657223639320/1657223788955\" rel=\"prev\">Pr\u00e9c\u00e9dent\u00a0: Aper\u00e7u</a></li>\n<li class=\"next\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657309218484/1657309218702\" rel=\"next\">Suivant\u00a0: Avant de pr\u00e9senter une demande</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}