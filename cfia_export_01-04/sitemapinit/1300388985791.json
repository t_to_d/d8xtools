{
    "dcr_id": "1300388920375",
    "title": {
        "en": "Exporting terrestrial animals and animal products",
        "fr": "Exportation d'animaux terrestres et de produits d'origine animale"
    },
    "html_modified": "2015-04-02 11:10",
    "modified": "8-9-2023",
    "issued": "2-4-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/anim_terr_exp_1300388920375_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/anim_terr_exp_1300388920375_fra"
    },
    "parent_ia_id": "1300387824030",
    "ia_id": "1300388985791",
    "parent_node_id": "1300387824030",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Exporting terrestrial animals and animal products",
        "fr": "Exportation d'animaux terrestres et de produits d'origine animale"
    },
    "label": {
        "en": "Exporting terrestrial animals and animal products",
        "fr": "Exportation d'animaux terrestres et de produits d'origine animale"
    },
    "templatetype": "content page 1 column",
    "node_id": "1300388985791",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1300386686425",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Exporting terrestrial animals and animal products",
            "fr": "Exportation d'animaux terrestres et de produits d'origine animale"
        },
        "description": {
            "en": "The objective of the export program is to ensure that only healthy animals and animal products and by-products which meet the import health requirements of an importing country are exported from Canada, and that in the case of live animals, that they are transported in a humane manner.",
            "fr": "Le Programme zoosanitaire \u00e0 l'exportation vise \u00e0 assurer que seuls les animaux et les produits et sous-produits animaux sains et conformes aux exigences des pays importateurs sont export\u00e9s du Canada et que, dans le cas des animaux vivants, ceux-ci sont transport\u00e9s sans cruaut\u00e9."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, animal health, exports, policy, live animals, animal products, animal by-products, semen, embryos, germinal products",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, sant\u00e9 des animaux, importation, politique, animaux vivants, produits d'origine d'animaux, sous-produits d'origine d'animaux, semence, embryons, produits germinaux"
        },
        "dcterms.subject": {
            "en": "animal health,exports,inspection,livestock,policy,veterinary medicine",
            "fr": "sant\u00e9 animale,exportation,inspection,b\u00e9tail,politique,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-02 11:10:34",
            "fr": "2015-04-02 11:10:34"
        },
        "modified": {
            "en": "2023-09-08",
            "fr": "2023-09-08"
        },
        "type": {
            "en": "policy,reference material",
            "fr": "politique,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Exporting terrestrial animals and animal products",
        "fr": "Exportation d'animaux terrestres et de produits d'origine animale"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Effective January\u00a015,\u00a02022, exporters for food and animal commodities destined for the European Union (EU) where certification is required under the new EU <i>Animal Health Law</i> will be required to use updated export certificates accessed through the Trade Control and Expert System New Technology (<a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a>). Existing export certificates to the EU will be accepted until March\u00a015,\u00a02022 as long as they were <strong>signed before January\u00a015,\u00a02022</strong>.</p>\n<p>Exceptions include the Canadian Food Inspection Agency (CFIA) form Certificate of Free Sale (CFIA/ACIA\u00a05786) and the Animal Health Certificate for Entry into the Union of Dogs, Cats, and Ferrets: In-Transit and Commercial Movements (Model 'Canis-Felis-Ferrets') HA 3169).</p>\n<p>The Certificate of Free Sale (CFIA/ACIA 5786), is available for food products manufactured by licensed parties under the <i>Safe Food for Canadians Act</i> (SFCA) and <i>Safe Food for Canadians Regulations</i> (SFCR). The Certificate of Free Sale does not replace product or commodity specific certificates that have been negotiated with foreign countries, nor does it replace or supersede additional import requirements that may be established by the importing country.</p>\n<p>For information on the requirements and certification of in-transit and commercial movements of dogs, cats and ferrets to the EU please consult the following link: <a href=\"/animal-health/terrestrial-animals/exports/pets/eu-commercial-/eng/1321466489704/1321466637929\">Export of dogs, cats, and ferrets to the European Union and Northern Ireland: In-transit commercial movements</a>.</p>\n<p>The objective of the export program is to ensure that only healthy animals and animal products and by-products which meet the import health requirements of an importing country are exported from Canada, and that in the case of live animals, that they are transported in a humane manner.</p>\n<p>A list of valid export certificates for animals and birds can be found on the following links. If you are planning an export, please contact the <a href=\"/eng/1300462382369/1300462438912\">Animal health offices</a> in your area to get a copy of the export certificate and verify that the export certificate is still valid, that it applies to your export, and that there are no disease outbreaks or other events that may cause exports to be suspended. Please note that export conditions from some of the certificates to the USA are provided for information purposes only.</p>\n<h2 id=\"a1\">Live animals</h2>\n<h3 id=\"a1_1\">Animal health certificates</h3>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/exports/pets/eng/1321265624789/1321281361100\">Pets (Dogs, cats and ferrets)</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/birds/eng/1369502927678/1369503223662\">Birds </a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/bovine/eng/1369550608938/1369550610610\">Bovine </a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/camelidae/eng/1369552366448/1369552367886\">Camelidae</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/caprine-ovine/eng/1369624443315/1369624446143\">Caprine / Ovine</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/cervids/eng/1369625005118/1369625006399\">Cervids</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/embryos/eng/1369688877619/1369693157263\">Embryos</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/horses/eng/1369625441661/1369625442848\">Horses</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/other/eng/1369625978949/1369625980277\">Other</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/porcine/eng/1405795282433/1405795283542\">Porcine</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/semen/eng/1369629077115/1369688568770\">Semen</a></li>\n</ul>\n<h3 id=\"a1_2\">Policies</h3>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/exports/export-policies/eng/1377704017956/1377704102343\">Live animals: export policies</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/external-laboratories/eng/1334692613353/1334692757036\">Policy on the Use of External Laboratories for Export Testing</a></li>\n</ul>\n<h2 id=\"a2\">Animal products</h2>\n<h3 id=\"a2_1\">Animal health certificates</h3>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/exports/animal-products-by-products/eng/1369629863413/1369688204094\">Animal products</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/pet-food/eng/1372183019382/1372183063848\">Pet food</a></li>\n</ul>\n<h3 id=\"a2_2\">Policies</h3>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/exports/export-policies/eng/1321157334025/1321200339614\">Animal products and by-products</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/pet-food/eng/1321152787689/1321153110863\">Pet food</a></li>\n</ul>\n<h2 id=\"a3\">Notices</h2>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/exports/2023-09-08/eng/1694023534482/1694023535060\">2023-08-23\u00a0\u2013 Notice to industry: Verification of farmed cervids for export to the United States using export certificate HA1891</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/2022-09-27/eng/1664220306091/1664220669415\">2022-09-27\u00a0\u2013 Notice to industry: Service fee cap extended for certain animal health export certificates</a></li>\n<li><a href=\"/exporting-food-plants-or-animals/european-union/eng/1627578090394/1627578090779\">2021-07-29\u00a0\u2013 Exporting food and animal products to the European Union</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/2020-09-15/eng/1600199536162/1600199536772\">2020-09-15\u00a0\u2013 Notice to industry: Service fee cap extended for certain animal health export certificates</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/2020-02-05/eng/1581007779897/1581007877110\">2020-02-05\u00a0\u2013 Notice to the industry: New ABP approval number</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/2019-10-10/eng/1508431804928/1508431805505\">2019-10-10\u00a0\u2013 Notice to industry: Service fee cap extended for certain animal health export certificates</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/exports/notice-to-industry/eng/1534381553027/1534381553476\">2018-08-16\u00a0\u2013 Notice to industry: Changes to requirements for export to the US of animal consumption material other than shelf-stable thermally processed pet food, treats and chews</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>\u00c0 compter du\u00a015 janvier\u00a02022, les exportateurs de produits alimentaires et d'origine animale destin\u00e9s \u00e0 l'Union europ\u00e9enne (UE) o\u00f9 la certification est requise dans le cadre de la nouvelle <i>loi sur la sant\u00e9 animale</i> (LSA) devront utiliser des certificats d'exportation mis \u00e0 jour. Les certificats d'exportation sont accessibles par l'interm\u00e9diaire du syst\u00e8me <a href=\"https://webgate.ec.europa.eu/tracesnt/login\">TRACES NT</a> (<span lang=\"en\">Trade Control and Expert System New Technology</span>). Les certificats d'exportation actuels seront accept\u00e9s par l'UE jusqu'au\u00a015 mars\u00a02022, \u00e0 condition qu'ils soient <strong>sign\u00e9s avant le\u00a015 janvier\u00a02022</strong>.</p>\n<p>Les exceptions comprennent le formulaire Certificat de vente libre (CFIA/ACIA\u00a05786) de l'Agence canadienne d'inspection des aliments (ACIA) et le certificat zoosanitaire pour l'entr\u00e9e dans l'Union europ\u00e9enne (UE) des chiens, des chats et des furets\u00a0: Les mouvements en transit et commerciaux (mod\u00e8le \u00ab\u00a0Canis-Felis-Ferrets\u00a0\u00bb), HA\u00a03169.</p>\n<p>Le formulaire Certificat de vente libre (CFIA/ACIA\u00a05786) de l'Agence canadienne d'inspection des aliments (ACIA) qui est offert aux produits alimentaires fabriqu\u00e9s par des parties titulaires d'une licence au titre de la <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> (LSAC) et du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC). Le Certificat de vente libre ne remplace pas les certificats propres aux produits ou aux marchandises qui ont fait l'objet de n\u00e9gociations avec des pays \u00e9trangers. De plus, il ne remplace pas les exigences d'importation suppl\u00e9mentaires qui pourraient \u00eatre \u00e9tablies par le pays importateur et n'a pas pr\u00e9s\u00e9ance sur celles-ci.</p>\n<p>Pour obtenir de plus amples renseignements sur les exigences et la certification pour les mouvements en transit et commerciaux des chiens, des chats et des furets vers l'UE, veuillez cliquer sur le lien suivant\u00a0: <a href=\"/sante-des-animaux/animaux-terrestres/exportation/animaux-de-compagnie/ue-commercial-/fra/1321466489704/1321466637929\">Exportation des chiens, des chats et des furets vers l'Union europ\u00e9enne et l'Irlande du Nord\u00a0: Les mouvements en transit, commerciaux</a>.</p>\n<p>Le Programme zoosanitaire \u00e0 l'exportation vise \u00e0 assurer que seuls les animaux et les produits et sous-produits animaux sains et conformes aux exigences des pays importateurs sont export\u00e9s du Canada et que, dans le cas des animaux vivants, ceux-ci sont transport\u00e9s sans cruaut\u00e9.</p>\n<p>Une liste de certifications d'exportation valides pour animaux et oiseaux est disponible aux liens suivants. Si vous planifiez une exportation, veuillez contacter le <a href=\"/fra/1300462382369/1300462438912\">bureau de sant\u00e9 animale</a> de votre r\u00e9gion afin d'obtenir une copie du certificat d'exportation et v\u00e9rifier qu'il est toujours valide, qu'il s'applique bien \u00e0 votre exportation et qu'aucune \u00e9closion de maladie ou autre \u00e9v\u00e9nement ne pourrait emp\u00eacher le projet d'\u00eatre men\u00e9 \u00e0 bien. Veuillez noter que les conditions d'exportation pour certains certificats pour les \u00c9tats-Unis sont fournies pour information seulement.</p>\n<h2 id=\"a1\">Animaux vivants</h2>\n<h3 id=\"a1_1\">Certificats de sant\u00e9 animale</h3>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/animaux-de-compagnie/fra/1321265624789/1321281361100\">Animaux de compagnie (chiens, chats et furets)</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/autres/fra/1369625978949/1369625980277\">Autres</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/bovins/fra/1369550608938/1369550610610\">Bovins</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/camelides/fra/1369552366448/1369552367886\">Cam\u00e9lid\u00e9s</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/caprins-ovins/fra/1369624443315/1369624446143\">Caprins\u00a0/ Ovins</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/cervides/fra/1369625005118/1369625006399\">Cervid\u00e9s</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/chevaux/fra/1369625441661/1369625442848\">Chevaux</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/embryons/fra/1369688877619/1369693157263\">Embryons</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/oiseaux/fra/1369502927678/1369503223662\">Oiseaux</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/porcin/fra/1405795282433/1405795283542\">Porcin</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/semence/fra/1369629077115/1369688568770\">Semence</a></li>\n</ul>\n<h3 id=\"a1_2\">Politiques</h3>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/politiques-d-exportation/fra/1377704017956/1377704102343\">Animaux vivants\u00a0: politiques d'exportation</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/laboratoires-externes/fra/1334692613353/1334692757036\">Politique sur le recours aux services d'analyse de laboratoires externes pour la certification des exportations</a></li>\n</ul>\n<h2 id=\"a2\">Produits et sous-produits animaux</h2>\n<h3 id=\"a2_1\">Certificats de sant\u00e9 animale</h3>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/fra/1372183019382/1372183063848\">Aliments pour animaux de compagnie</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/produits-et-sous-produits-animaux/fra/1369629863413/1369688204094\">Produits animaux</a></li>\n</ul>\n<h3 id=\"a2_2\"> Politiques</h3>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/fra/1321152787689/1321153110863\">Aliments pour animaux de compagnie</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/politiques-d-exportation/fra/1321157334025/1321200339614\">Produits et sous-produits d'origine d'animaux</a></li>\n</ul>\n<h2 id=\"a3\">Avis</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/2023-09-07/fra/1694023534482/1694023535060\">2023-08-23\u00a0\u2013 Avis \u00e0 l'industrie\u00a0: V\u00e9rification des cervid\u00e9s d'\u00e9levage destin\u00e9s \u00e0 l'exportation vers les \u00c9tats-Unis \u00e0 l'aide du certificat d'exportation HA1891</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/2022-09-27/fra/1664220306091/1664220669415\">2022-09-27\u00a0\u2013 Avis \u00e0 l'industrie\u00a0: Prolongation du plafond des frais relatifs \u00e0 certains certificats de sant\u00e9 animale pour l'exportation</a></li>\n<li><a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/union-europeenne/fra/1627578090394/1627578090779\">2021-07-29\u00a0\u2013 L'exportation de produits alimentaires et d'origine animale vers l'Union europ\u00e9enne</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/2020-09-15/fra/1600199536162/1600199536772\">2020-09-15\u00a0\u2013 Avis \u00e0 l'industrie\u00a0: Prolongation du plafond des frais relatifs \u00e0 certains certificats de sant\u00e9 animale pour l'exportation</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/2020-02-05/fra/1581007779897/1581007877110\">2020-05-02\u00a0\u2013 Avis \u00e0 l'industrie\u00a0: Nouveau num\u00e9ro d'approbation SPA</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/2019-10-10/fra/1508431804928/1508431805505\">2019-10-10\u00a0\u2013 Avis \u00e0 l'industrie\u00a0: Prolongation du plafond des frais relatifs \u00e0 certains certificats de sant\u00e9 animale pour l'exportation</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/exportation/avis-a-l-industrie/fra/1534381553027/1534381553476\">2018-08-16\u00a0\u2013 Avis \u00e0 l'industrie\u00a0: Modification des exigences relatives \u00e0 la certification des exportations vers les \u00c9tats-Unis de produits destin\u00e9s \u00e0 la consommation animale autres que les aliments, les g\u00e2teries et les produits \u00e0 m\u00e2cher pour animaux de compagnie \u00e0 longue dur\u00e9e de conservation et soumis \u00e0 un traitement thermique</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}