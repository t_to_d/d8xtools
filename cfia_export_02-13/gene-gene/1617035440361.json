{
    "dcr_id": "1617035440361",
    "title": {
        "en": "Putting honey to the test",
        "fr": "L'analyse du miel"
    },
    "html_modified": "2024-02-13 9:55:05 AM",
    "modified": "2021-03-30",
    "issued": "2021-03-31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/c360_honey_test_20210329_1617035440361_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/c360_honey_test_20210329_1617035440361_fra"
    },
    "ia_id": "1617035440892",
    "parent_ia_id": "1565297581210",
    "chronicletopic": {
        "en": "Science and Innovation|Food Safety",
        "fr": "Science et Innovation|Salubrit\u00e9 des Aliments"
    },
    "chroniclecontent": {
        "en": "Video",
        "fr": "Vid\u00e9o"
    },
    "chronicleaudience": {
        "en": "Industry|Canadians",
        "fr": "Industrie|Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565297581210",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Putting honey to the test",
        "fr": "L'analyse du miel"
    },
    "label": {
        "en": "Putting honey to the test",
        "fr": "L'analyse du miel"
    },
    "templatetype": "content page 1 column",
    "node_id": "1617035440892",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565297580961",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/food-safety/putting-honey-to-the-test/",
        "fr": "/inspecter-et-proteger/salubrite-des-aliments/l-analyse-du-miel/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Putting honey to the test",
            "fr": "L'analyse du miel"
        },
        "description": {
            "en": "Canadians love their honey. But if sugars are added and not declared on the label, that is a form of food fraud. The CFIA tests honey to make sure it\u2019s authentic.",
            "fr": "Canadians love their honey. But if sugars are added and not declared on the label, that is a form of food fraud. The CFIA tests honey to make sure it\u2019s authentic."
        },
        "keywords": {
            "en": "Food safety, Science and Innovation, video, Putting honey to the test",
            "fr": "Salubrit\u00e9 des aliments, Science et innovation, vid\u00e9o, L'analyse du miel"
        },
        "dcterms.subject": {
            "en": "newsletters,communications,government communications,government publication",
            "fr": "bulletin,communications,communications gouvernementales,publication gouvernementale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-03-31",
            "fr": "2021-03-31"
        },
        "modified": {
            "en": "2021-03-30",
            "fr": "2021-03-30"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Putting honey to the test",
        "fr": "L'analyse du miel"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263124160\"></div>\n\n<p>Canadians love their honey. But if sugars are added and not declared on the label, that is a form of food fraud. The CFIA tests honey to make sure it\u2019s authentic.</p>\n\n\n<figure class=\"wb-mltmd mrgn-bttm-md\"><video width=\"560\" height=\"340\" title=\"Video - Putting honey to the test\">\n\n<source type=\"video/youtube\" src=\"https://youtu.be/t3c-SBAt8UQ\">\n</source></video>\n\n<figcaption>\n\n<details id=\"inline-transcript\">\n\n<summary>Putting honey to the test\u00a0\u2013 Transcript</summary>\n<p class=\"mrgn-tp-md\"><strong>Narrator:</strong> Honey is oh so sweet. But if sugars are added and not declared on the label, that is a form of food fraud.</p>\n<p>Canadians love their honey. In\u00a02019, Canada produced\u00a080\u00a0million pounds of honey worth\u00a0$173\u00a0million. And we imported another\u00a0$45\u00a0million worth of honey.</p>\n<p>As part of our work to test food for authenticity, the CFIA tests honey samples for added sugars.</p>\n<p>Testing done in\u00a02019 and\u00a02020 showed\u00a093%\u00a0of samples tested were authentic. The rest of the samples tested had added sugars that didn't meet regulatory requirements.</p>\n<p>From these latest efforts, we prevented more than\u00a083,000\u00a0kilograms of adulterated honey from being sold in Canada.</p>\n<p><strong><span lang=\"fr\">Jonathan Hach\u00e9</span>, Chemist, Food Chemistry, Ottawa Laboratory (Carling), CFIA:</strong> At CFIA, we have two different types of sampling. Monitoring sampling, which is random; it's used to establish baseline data and to identify areas of risk.</p>\n<p>And then we have targeted sampling and that's generally based on a history of non-compliance, on gaps in preventive controls and unusual trading patterns.</p>\n<p>So, in general, CFIA will test honey for added sugars, but also residues and contaminants, and also test to make sure they comply with regulations and standards.</p>\n<p>For this targeted survey we focused on authenticity. Only pure honey can be labelled and sold as honey in Canada.</p>\n<p><strong>Jodi White, Acting Director, Consumer Protection and Market Fairness Division, CFIA:</strong></p>\n<p>When we do find honey that is not authentic, we have the authority to respond in a number of different ways.</p>\n<p>We could seize, detain or remove the product from Canada or it could be thrown out. We can also work with our partners at the Canada Border Services Agency to put in place what we call a border look out for non-compliant products.</p>\n<p>In addition, the CFIA could recommend prosecution of companies that sell or import adulterated food products or we could revoke import licences.</p>\n<p>CFIA carries out inspection, compliance and enforcement activities to protect consumers from deceptive practices and maintain a fair marketplace for industry.</p>\n<p>But consumers also have a role to play. Check labels to see if information may be misleading and contact companies directly to ask questions.</p>\n<p><strong>Narrator:</strong> Whether you purchase your honey in-store or online, we all have a role to play.</p>\n<p>Visit our website to learn more and check out the latest report on our honey testing at inspection.gc.ca/foodfraud.</p>\n<p>[End of recording]</p>\n\n</details>\n</figcaption>\n</figure>\n\n<h2>Learn more</h2>\n\n<ul>\n<li><a href=\"/inspect-and-protect/food-safety/tackling-food-fraud/eng/1605887232284/1605887285440\">Video: Tackling food fraud</a></li>\n<li><a href=\"/food-labels/food-fraud/eng/1548444446366/1548444516192\">Food fraud</a></li>\n<li><a href=\"/food-labels/food-fraud/cfia-s-role/eng/1548444760712/1548444784124\">The CFIA's role in combatting food fraud</a></li>\n<li><a href=\"/food-labels/food-fraud/industry-s-role-in-combatting-food-fraud/eng/1548444869404/1548444893341\">Industry's role in combatting food fraud</a></li>\n<li><a href=\"/food-labels/food-fraud/how-food-fraud-impacts-consumers/eng/1548444986322/1548445033398\">How food fraud impacts consumers</a></li>\n</ul>\n\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263124160\"></div>\n\n<p>Les Canadiens adorent leur miel. Mais si des glucides sont ajout\u00e9s et ne sont pas d\u00e9clar\u00e9s sur l'\u00e9tiquette, c'est une forme de fraude alimentaire. L\u2019ACIA analyse le miel afin d\u2019assurer son authenticit\u00e9.</p>\n\n<figure class=\"wb-mltmd mrgn-bttm-md\"><video width=\"560\" height=\"340\" title=\"Vid\u00e9o - L'analyse du miel\">\n\n<source type=\"video/youtube\" src=\"https://youtu.be/zcuR_Vaj7bk\">\n</source></video>\n\n<figcaption>\n\n<details id=\"inline-transcript\">\n\n<summary>L'analyse du miel\u00a0\u2013 Transcription</summary>\n<p class=\"mrgn-tp-md\"><strong>Narrateur\u00a0:</strong> Que le miel est sucr\u00e9! Mais lorsque des glucides y sont ajout\u00e9s et que l'\u00e9tiquette n'en fait pas mention, c'est une forme de fraude alimentaire.</p>\n<p>Les Canadiens adorent leur miel. En\u00a02019, le Canada a produit plus de\u00a036\u00a0millions de kilos de miel d'une valeur de\u00a0173\u00a0millions de dollars. Et nos importations de miel se sont \u00e9lev\u00e9es \u00e0\u00a045\u00a0millions de dollars.</p>\n<p>Dans le cadre de son travail de v\u00e9rification de l'authenticit\u00e9 des aliments, l'ACIA proc\u00e8de \u00e0 l'analyse d'\u00e9chantillons de miel pour d\u00e9celer l'ajout de glucides.</p>\n<p>Les analyses effectu\u00e9es en\u00a02019 et en\u00a02020 ont montr\u00e9 que\u00a093\u00a0%\u00a0des \u00e9chantillons \u00e9taient authentiques. L'analyse des autres \u00e9chantillons a permis de d\u00e9celer la pr\u00e9sence de glucides ajout\u00e9s en contravention aux exigences r\u00e9glementaires.</p>\n<p>Gr\u00e2ce \u00e0 ces efforts r\u00e9cents, nous avons emp\u00each\u00e9 la vente au Canada de plus de\u00a083\u00a0000\u00a0kilos de miel frelat\u00e9.</p>\n<p><strong>Jonathan Hach\u00e9, chimiste, Chimie alimentaire, Laboratoire d'Ottawa (Carling), ACIA\u00a0:</strong> \u00c0 l'Agence, nous effectuons deux types d'\u00e9chantillonnage. L'\u00e9chantillonnage de v\u00e9rification, qui est al\u00e9atoire et qui aide \u00e0 \u00e9laborer des renseignements de base et \u00e0 d\u00e9terminer les secteurs de risque.</p>\n<p>L'\u00e9chantillonnage cibl\u00e9 qui met l'accent sur diff\u00e9rents facteurs de risque comme les ant\u00e9c\u00e9dents de non-conformit\u00e9 d'une entreprise, des lacunes dans les contr\u00f4les pr\u00e9ventifs ou des sch\u00e9mas commerciaux inhabituels.</p>\n<p>En g\u00e9n\u00e9ral, l'Agence effectue plusieurs tests sur le miel. On regarde pour le sucre ajout\u00e9 et aussi pour les r\u00e9sidus et les contaminants, et aussi pour assurer que les \u00e9chantillons de miel conforment aux normes et r\u00e8gles du Canada.</p>\n<p>Au Canada, seul le miel pur peut \u00eatre \u00e9tiquet\u00e9 et vendu comme du miel.</p>\n<p><strong>Jodi White, directrice int\u00e9rimaire, Division de la protection des consommateurs et de l'\u00e9quit\u00e9 des march\u00e9s, ACIA\u00a0:</strong></p>\n<p>Quand nous relevons un cas de miel falsifi\u00e9, l'Agence a le pouvoir de prendre un certain nombre de mesures.</p>\n<p>Nous pourrions saisir, retenir, \u00e9liminer le produit ou le renvoyer du Canada. Nous pourrions \u00e9galement collaborer avec nos partenaires de l'Agence des services frontaliers du Canada pour mettre en place une surveillance \u00e0 la fronti\u00e8re des produits non conformes.</p>\n<p>Nous pourrions aussi recommander la poursuite des entreprises qui vendent ou importent des produits alimentaires falsifi\u00e9s ou r\u00e9voquer leurs licences d'importation.</p>\n<p>L'ACIA effectue des activit\u00e9s d'inspection, de conformit\u00e9 et de mise en application de la loi afin de  prot\u00e9ger les consommateurs contre les pratiques trompeuses et pour maintenir un march\u00e9 \u00e9quitable pour l'industrie.</p>\n<p>Les consommateurs ont \u00e9galement un r\u00f4le \u00e0 jouer. V\u00e9rifiez les \u00e9tiquettes pour voir si les renseignements fournis pourraient \u00eatre trompeurs et communiquez directement avec les entreprises pour leur poser des questions.</p>\n<p><strong>Narrateur\u00a0:</strong> Que vous achetiez votre miel \u00e0 l'\u00e9picerie ou en ligne, nous avons tous un r\u00f4le \u00e0 jouer.</p>\n<p>Visitez notre site Web pour en savoir plus et consultez le dernier rapport sur nos analyses du miel \u00e0 inspection.gc.ca/fraudealimentaire.</p>\n<p>[Fin de l'enregistrement]</p>\n\n</details>\n</figcaption>\n</figure>\n\n<h2>Pour en savoir plus</h2>\n\n<ul>\n<li><a href=\"/inspecter-et-proteger/salubrite-des-aliments/la-lutte-contre-la-fraude-alimentaire/fra/1605887232284/1605887285440\">Video: La lutte contre la fraude alimentaire</a></li>\n<li><a href=\"/etiquetage-des-aliments/fraude-alimentaire/fra/1548444446366/1548444516192\">La fraude alimentaire</a></li>\n<li><a href=\"/etiquetage-des-aliments/fraude-alimentaire/role-de-l-acia/fra/1548444760712/1548444784124\">R\u00f4le de l'ACIA dans la lutte contre la fraude alimentaire</a></li>\n<li><a href=\"/etiquetage-des-aliments/fraude-alimentaire/role-de-l-industrie-dans-la-lutte-contre-la-fraude/fra/1548444869404/1548444893341\">R\u00f4le de l'industrie dans la lutte contre la fraude alimentaire</a></li>\n<li><a href=\"/etiquetage-des-aliments/fraude-alimentaire/repercussions-de-la-fraude-alimentaire-sur-les-con/fra/1548444986322/1548445033398\">R\u00e9percussions de la fraude alimentaire sur les consommateurs</a></li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}