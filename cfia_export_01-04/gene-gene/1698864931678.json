{
    "dcr_id": "1698864931678",
    "title": {
        "en": "Fish and seafood labelling and traceability requirements",
        "fr": "Exigences en mati\u00e8re d'\u00e9tiquetage et de tra\u00e7abilit\u00e9 du poisson et des fruits de mer"
    },
    "html_modified": "14-12-2023",
    "modified": "14-12-2023",
    "issued": "1-11-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/fish_seafood_lebel_trace_req_1698864931678_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/fish_seafood_lebel_trace_req_1698864931678_fra"
    },
    "parent_ia_id": "1698844888410",
    "ia_id": "1698864932021",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fish and seafood labelling and traceability requirements",
        "fr": "Exigences en mati\u00e8re d'\u00e9tiquetage et de tra\u00e7abilit\u00e9 du poisson et des fruits de mer"
    },
    "label": {
        "en": "Fish and seafood labelling and traceability requirements",
        "fr": "Exigences en mati\u00e8re d'\u00e9tiquetage et de tra\u00e7abilit\u00e9 du poisson et des fruits de mer"
    },
    "templatetype": "content page 1 column",
    "node_id": "1698864932021",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1698844887535",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/food-fraud/fish-labelling-and-traceability/requirements/",
        "fr": "/etiquetage-des-aliments/fraude-alimentaire/etiquetage-et-tracabilite-du-poisson/exigences/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fish and seafood labelling and traceability requirements",
            "fr": "Exigences en mati\u00e8re d'\u00e9tiquetage et de tra\u00e7abilit\u00e9 du poisson et des fruits de mer"
        },
        "description": {
            "en": "Canada has implemented robust food traceability and labelling requirements.",
            "fr": "Le Canada a adopt\u00e9 des exigences solides en mati\u00e8re de tra\u00e7abilit\u00e9 et d\u2019\u00e9tiquetage des aliments."
        },
        "keywords": {
            "en": "fish, seafood, labelling, traceability, food, fraud",
            "fr": "\u00e9tiquetage, tra\u00e7abilit\u00e9, poisson, fruits de mer, fraude, alimentaire"
        },
        "dcterms.subject": {
            "en": "food labelling,seafood",
            "fr": "\u00e9tiquetage des aliments,fruits de mer"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-12-14",
            "fr": "2023-12-14"
        },
        "modified": {
            "en": "2023-12-14",
            "fr": "2023-12-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fish and seafood labelling and traceability requirements",
        "fr": "Exigences en mati\u00e8re d'\u00e9tiquetage et de tra\u00e7abilit\u00e9 du poisson et des fruits de mer"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n<ul>\n<li><a href=\"#a1\">Labelling requirements</a></li>\n<li><a href=\"#a2\">Traceability requirements</a></li>\n<li><a href=\"#a3\">More information</a></li>\n</ul>\n<h2 id=\"a1\">Labelling requirements</h2>\n<p>It is industry's responsibility to properly label seafood and to provide information that is truthful and not misleading to consumers.</p>\n<p>Fish and fish products are subject to the provisions of the <a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i></a> and the <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a>, as well as those of the <a href=\"/english/reg/jredirect2.shtml?drga\"><i>Food and Drugs Act</i></a> and the <a href=\"/english/reg/jredirect2.shtml?drgr\"><i>Food and Drug Regulations</i></a>: </p>\n<ul class=\"lst-spcd\">\n<li>All foods, including fish and seafood products, must be labelled with information that is necessary for public health and consumer protection. This includes:\n<ul>\n<li>the common name of the fish</li>\n<li>net quantity</li>\n<li>ingredients list</li>\n<li>Nutrition Facts table</li>\n<li>lot code or other unique identifier</li>\n<li>principal place of business where the food was manufactured</li>\n</ul>\n</li>\n<li>For imported prepackaged fish, the <a href=\"/eng/1630326254350/1630326374266#a8\">country of origin</a> must be clearly identified on the label, that is, the country where the fish product was last substantially transformed, for example, filleted or frozen</li>\n<li>Additional information, such as the location of catch, the type of fishing gear used, or <a href=\"/eng/1630326254350/1630326374266#a11_3\">sustainability-related claims</a>, can be included on the label voluntarily, provided it is not false or misleading</li>\n<li>The <a href=\"#a2\">traceability requirements</a> in the SFCR must also be met so that unsafe foods can quickly be removed from the market through recalls, protecting the health of consumers</li>\n</ul>\n<p>See <a href=\"/eng/1393709636463/1393709677546?chap=0\">labelling requirements for fish and fish products</a> for more information.</p>\n<h2 id=\"a2\">Traceability requirements</h2>\n<p>Canada has implemented robust food traceability requirements under the <i>Safe Food for Canadians Regulations</i>, allowing the Canadian Food Inspection Agency to take action quickly to protect the health of consumers. This includes:</p>\n<ul class=\"lst-spcd\">\n<li>regulations bringing all food businesses that import, export, or trade between provinces or territories to the same international standard set by <i lang=\"la\">Codex Alimentarius</i>, to protect consumer health and facilitate fair trading practices</li>\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/traceability/eng/1427310329573/1427310330167\">traceability requirements for all food sectors</a> including fish and seafood, to prepare, keep and retain records \u2014 one step forward and one step back \u2014 so that the food product can be followed throughout the supply chain</li>\n<li>businesses keeping traceability records so they can track food products in the event of a food safety investigation or food recall in order to protect consumers and potentially minimize economic losses</li>\n</ul>\n<p>These requirements are in place so that unsafe foods can quickly be removed from the market through recalls, protecting the health of consumers.</p>\n<h2 id=\"a3\">More information</h2>\n<ul>\n<li><a href=\"/eng/1393709636463/1393709677546?chap=0\">Labelling requirements for fish and fish products</a></li>\n<li><a href=\"/food-safety-for-industry/traceability/eng/1526651817880/1526651951357\">Traceability for food</a> </li>\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/traceability/eng/1427310329573/1427310330167\">Traceability fact sheet</a></li>\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/traceability-requirements/eng/1560268397940/1560268398174\">Traceability requirements under the Safe Food for Canadians Regulations (video)</a></li>\n<li><a href=\"/preventive-controls/fish/eng/1526650634909/1526650685419\">Preventive controls for fish </a></li>\n<li><a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">Food safety for industry</a></li>\n<li><a href=\"/food-guidance-by-commodity/fish/eng/1526654771094/1526654771344\">Food-specific requirements and guidance for fish</a></li>\n<li><a href=\"/preventive-controls/fish/organoleptic-quality-of-fish-and-fish-products/eng/1580915794090/1580915794543\">Organoleptic quality of fish and fish products </a></li>\n<li><a href=\"https://recalls-rappels.canada.ca/en\">Recalls and safety alerts</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n<ul>\n<li><a href=\"#a1\">Exigences en mati\u00e8re d'\u00e9tiquetage</a></li>\n<li><a href=\"#a2\">Exigences en mati\u00e8re de tra\u00e7abilit\u00e9</a></li>\n<li><a href=\"#a3\">Plus d'information</a></li>\n</ul>\n<h2 id=\"a1\">Exigences en mati\u00e8re d'\u00e9tiquetage</h2>\n<p>Il appartient \u00e0 l'industrie de bien \u00e9tiqueter les produits de la mer et de fournir des renseignements qui sont exacts et qui n'induisent pas les consommatrices et consommateurs en erreur.</p>\n<p>Le poisson et les produits de poisson sont assujettis aux dispositions de la <a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i></a> et du <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a> (RSAC) de m\u00eame qu'\u00e0 celles de la <a href=\"/francais/reg/jredirect2.shtml?drga\"><i>Loi sur les aliments et drogues</i></a> et du <a href=\"/francais/reg/jredirect2.shtml?drgr\"><i>R\u00e8glement sur les aliments et drogues</i></a>.</p>\n<ul class=\"lst-spcd\">\n<li>Tous les aliments, y compris le poisson et les fruits de mer, doivent \u00eatre \u00e9tiquet\u00e9s et comporter des renseignements n\u00e9cessaires \u00e0 la sant\u00e9 du public et \u00e0 la protection des consommatrices et consommateurs, notamment\u00a0:\n<ul>\n<li>Le nom courant du poisson</li>\n<li>La quantit\u00e9 nette</li>\n<li>La liste des ingr\u00e9dients</li>\n<li>Le tableau de la valeur nutritive</li>\n<li>Le code de lot ou autre identificateur unique</li>\n<li>L'\u00e9tablissement principal o\u00f9 l'aliment a \u00e9t\u00e9 fabriqu\u00e9</li>\n</ul>\n</li>\n<li>Pour le poisson pr\u00e9emball\u00e9 import\u00e9, le <a href=\"/fra/1630326254350/1630326374266#a8\">pays d'origine</a> doit \u00eatre clairement indiqu\u00e9 sur l'\u00e9tiquette, c'est-\u00e0-dire le pays o\u00f9 le produit de poisson a subi pour la derni\u00e8re fois une importante transformation, par exemple o\u00f9 il a \u00e9t\u00e9 filet\u00e9 ou congel\u00e9.</li>\n<li>D'autres renseignements, comme le lieu de la prise, le type de mat\u00e9riel de p\u00eache utilis\u00e9 ou les <a href=\"/fra/1630326254350/1630326374266#a11_3\">all\u00e9gations relatives \u00e0 la m\u00e9thode de production</a>, peuvent \u00e9galement figurer sur l'\u00e9tiquette de fa\u00e7on volontaire, \u00e0 condition qu'ils ne soient ni faux ni trompeurs.</li>\n<li>Les <a href=\"#a2\">exigences de tra\u00e7abilit\u00e9</a> dans le RSAC doivent \u00e9galement \u00eatre respect\u00e9es pour que les aliments insalubres puissent rapidement \u00eatre retir\u00e9s du march\u00e9 par le biais de rappels, afin de prot\u00e9ger la sant\u00e9 des consommatrices et consommateurs.</li>\n</ul>\n<p>Pour des renseignements plus d\u00e9taill\u00e9s, consulter les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/poisson/fra/1630326254350/1630326374266?chap=0\">exigences en mati\u00e8re d'\u00e9tiquetage pour le poisson et les produits de poisson</a>.</p>\n<h2 id=\"a2\">Exigences en mati\u00e8re de tra\u00e7abilit\u00e9</h2>\n<p>Le Canada a adopt\u00e9 des exigences solides en mati\u00e8re de tra\u00e7abilit\u00e9 des aliments aux termes du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>, ce qui permet \u00e0 l'Agence canadienne d'inspection des aliments de prendre rapidement des mesures pour prot\u00e9ger la sant\u00e9 des consommatrices et consommateurs. Au rang de ces mesures figurent, notamment\u00a0:</p>\n<ul class=\"lst-spcd\">\n<li>Un r\u00e8glement qui assujettit toutes les entreprises alimentaires qui importent ou exportent des aliments ou font le commerce d'aliments entre des provinces ou des territoires \u00e0 la m\u00eame norme internationale fix\u00e9e par le <i lang=\"la\">Codex Alimentarius</i>, afin de prot\u00e9ger la sant\u00e9 des consommatrices et consommateurs, et de faciliter les pratiques commerciales.</li>\n<li>Des <a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/tracabilite/fra/1427310329573/1427310330167\">exigences en mati\u00e8re de tra\u00e7abilit\u00e9 pour tous les secteurs de l'alimentation</a>, y compris celui du poisson et des fruits de mer, afin de pr\u00e9parer, de tenir \u00e0 jour et de conserver des registres \u2013 une \u00e9tape en aval et une \u00e9tape en amont \u2013 pour que le produit alimentaire puisse faire l'objet d'un suivi tout au long de la cha\u00eene d'approvisionnement.</li>\n<li>La consignation des dossiers de tra\u00e7abilit\u00e9 par les entreprises afin de pouvoir suivre les produits alimentaires en cas d'enqu\u00eates sur la salubrit\u00e9 d'un aliment ou de rappels d'aliments afin de prot\u00e9ger les consommatrices et consommateurs et, \u00e9ventuellement, de minimiser les pertes \u00e9conomiques.</li>\n</ul>\n<p>Ces exigences sont en place pour que les aliments insalubres puissent rapidement \u00eatre retir\u00e9s du march\u00e9 par le biais de rappels, prot\u00e9geant ainsi la sant\u00e9 des consommatrices et consommateurs.</p>\n<h2 id=\"a3\">Plus d'information</h2>\n<ul>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/poisson/fra/1630326254350/1630326374266?chap=0\">Exigences en mati\u00e8re d'\u00e9tiquetage du poisson et des produits de poisson</a></li>\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/tracabilite/fra/1526651817880/1526651951357\">Tra\u00e7abilit\u00e9 des aliments</a> </li>\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/tracabilite/fra/1427310329573/1427310330167\">Fiche d'information sur la tra\u00e7abilit\u00e9</a></li>\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/exigences-en-matiere-de-tracabilite/fra/1560268397940/1560268398174\">Exigences en mati\u00e8re de tra\u00e7abilit\u00e9 en vertu du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a> (vid\u00e9o)</li>\n<li><a href=\"/controles-preventifs/poisson/fra/1526650634909/1526650685419\">Contr\u00f4les pr\u00e9ventifs pour le poisson</a></li>\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">Salubrit\u00e9 alimentaire pour l'industrie</a></li>\n<li><a href=\"/exigences-et-documents-d-orientation-relatives-a-c/poisson/fra/1526654771094/1526654771344\">Exigences et documents d'orientation pour le poisson</a></li>\n<li><a href=\"/controles-preventifs/poisson/qualite-organoleptique-du-poisson-et-des-produits-/fra/1580915794090/1580915794543\">Qualit\u00e9 organoleptique du poisson et des produits du poisson</a></li>\n<li><a href=\"https://recalls-rappels.canada.ca/fr\">Rappels et avis de s\u00e9curit\u00e9</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}