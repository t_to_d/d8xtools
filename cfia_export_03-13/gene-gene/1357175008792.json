{
    "dcr_id": "1357175008792",
    "title": {
        "en": "Senior management structure",
        "fr": "Structure de la haute direction"
    },
    "html_modified": "2024-03-12 3:56:01 PM",
    "modified": "2024-03-12",
    "issued": "2013-01-02 20:03:27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/agen_org_chart_1357175008792_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/agen_org_chart_1357175008792_fra"
    },
    "ia_id": "1357175139366",
    "parent_ia_id": "1323224814073",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1323224814073",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Senior management structure",
        "fr": "Structure de la haute direction"
    },
    "label": {
        "en": "Senior management structure",
        "fr": "Structure de la haute direction"
    },
    "templatetype": "content page 1 column",
    "node_id": "1357175139366",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323224617636",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/organizational-structure/senior-management/",
        "fr": "/a-propos-de-l-acia/structure-organisationnelle/haute-direction/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Senior management structure",
            "fr": "Structure de la haute direction"
        },
        "description": {
            "en": "Information about the senior management structure at the CFIA.",
            "fr": "Information sur la structure de la haute direction de l'ACIA."
        },
        "keywords": {
            "en": "senior management structure, branch, structure, organizational charts",
            "fr": "Structure de la haute direction, direction, structure, Structure organisationnelle"
        },
        "dcterms.subject": {
            "en": "flowcharts,government information,human resources,inspection,social development",
            "fr": "organigramme,information gouvernementale,ressources humaines,inspection,d\u00e9veloppement social"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-01-02 20:03:27",
            "fr": "2013-01-02 20:03:27"
        },
        "modified": {
            "en": "2024-03-12",
            "fr": "2024-03-12"
        },
        "type": {
            "en": "contact information",
            "fr": "information sur la personne-ressource"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Senior management structure",
        "fr": "Structure de la haute direction"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<figure>\n<p class=\"small\">Click on image for larger view\n<br>\n<span class=\"wb-lbx\"><a href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_org_chrt_march_2024_1710251411030_eng.jpg\" title=\"Senior Management Structure\"><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_org_chrt_march_2024_1710251411030_eng.jpg\" alt=\"Senior Management Structure. Description follows.\" class=\"img-responsive\"></a></span></p>\n</figure>\n\n<h2>Paul MacKinnon, President of the Canadian Food Inspection Agency</h2>\n<h3>Supporting the President</h3>\n<ul>\n<li><strong>Jean-Guy (J.-G.) Forgeron</strong>, Executive Vice-President</li>\n<li><strong>Linda Nguyen</strong>, Chief of Staff</li>\n</ul>\n<h3>Reporting to the President</h3>\n<ul>\n<li><strong>Debbie Beresford-Green</strong>, Vice-President, Operations</li>\n<li><strong>Scott Rattray</strong>, Associate Vice-President, Operations</li>\n<li><strong>Dr. David Nanang</strong>, Vice-President, Science</li>\n<li><strong>Robert Ianiro</strong>, Vice-President, Policy and Programs</li>\n<li><strong>Diane Allan</strong>, Associate Vice-President, Policy and Programs</li>\n<li><strong>Jane Hazel</strong>, Vice-President, Communications and Public Affairs</li>\n<li><strong>Raman Srivastava</strong>, Vice-President, Human Resources</li>\n<li><strong>Stanley Xu</strong>, Vice-President, Corporate Management and Chief Financial Officer</li>\n<li><strong>Kathleen Donohue</strong>, Assistant Deputy Minister and Vice-President, International Affairs</li>\n<li><strong>Martin Rubenstein</strong>, Chief, Audit and Evaluation</li>\n<li><strong>Kristine Allen</strong>, Executive Director and Senior General Council, Legal Services</li>\n<li><strong>Todd Cain</strong>, Vice President, Digital Services and Chief Information and Innovation Officer</li>\n</ul>\n<h3>Reporting to the Office of the President</h3>\n<ul>\n<li><strong>Natasha MacKenzie</strong>, Ministerial Liaison (Health Canada and Agriculture and Agri-Food Canada)</li>\n<li><strong>Tanya Day</strong>, Executive Director, Stakeholder Relations Bureau</li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<figure>\n<p class=\"small\">Cliquez sur l'image pour l'agrandir\n<br>\n<span class=\"wb-lbx\"><a href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_org_chrt_march_2024_1710251411030_fra.jpg\" title=\"Structure de la haute direction\"><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/hrrh_org_chrt_march_2024_1710251411030_fra.jpg\" alt=\"Structure de la haute direction. Description ci-dessous.\" class=\"img-responsive\"></a></span></p>\n</figure>\n\n<h2>Paul MacKinnon, Pr\u00e9sident de l'Agence canadienne d'inspection des aliments</h2>\n<h3>Les personnes suivantes appuient le pr\u00e9sident</h3>\n<ul>\n<li><strong>Jean-Guy (J.-G.) Forgeron</strong>, Premier vice-pr\u00e9sident</li>\n<li><strong>Linda Nguyen</strong>, Chef de cabinet</li>\n</ul>\n<h3>Les personnes suivantes rel\u00e8vent du pr\u00e9sident</h3>\n<ul>\n<li><strong>Debbie Beresford-Green</strong>, Vice-pr\u00e9sidente, Op\u00e9rations</li>\n<li><strong>Scott Rattray</strong>, Vice-pr\u00e9sident associ\u00e9, Op\u00e9rations</li>\n<li><strong>David Nanang</strong>, Vice-pr\u00e9sident, Sciences</li>\n<li><strong>Robert Ianiro</strong>, Vice-pr\u00e9sident, Politiques et programmes</li>\n<li><strong>Diane Allan</strong>, Vice-pr\u00e9sidente associ\u00e9e, Politiques et programmes</li>\n<li><strong>Jane Hazel</strong>, Vice-pr\u00e9sidente, Communications et affaires publiques</li>\n<li><strong>Raman Srivastava</strong>, Vice-pr\u00e9sident, Ressources humaines</li>\n<li><strong>Stanley Xu</strong>, Vice-pr\u00e9sident, Gestion int\u00e9gr\u00e9e et Dirigeant principal des finances</li>\n<li><strong>Kathleen Donohue</strong>, Sous-ministre adjointe et Vice-pr\u00e9sidente, Affaires internationales</li>\n<li><strong>Martin Rubenstein</strong>, Chef, V\u00e9rification et \u00e9valuation</li>\n<li><strong>Kristine Allen</strong>, Directrice ex\u00e9cutive et Avocate-g\u00e9n\u00e9rale principale, Services juridiques</li>\n<li><strong>Todd Cain</strong>, Vice-pr\u00e9sident, Services numeriques et Dirigeant principal de l'information et de l'innovation</li>\n</ul>\n<h3>Les personnes suivantes rel\u00e8vent du Bureau du pr\u00e9sident</h3>\n<ul>\n<li><strong>Natasha MacKenzie</strong>, Liaison minist\u00e9rielle (Sant\u00e9 Canada et Agriculture et Agroalimentaire Canada)</li>\n<li><strong>Tanya Day</strong>, Directrice ex\u00e9cutive, Bureau des relations avec les intervenants</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}