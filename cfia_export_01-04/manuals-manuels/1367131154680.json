{
    "dcr_id": "1367131154680",
    "title": {
        "en": "National Farm-Level Biosecurity Standard for the Goat Industry",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie caprine"
    },
    "html_modified": "2013-04-28 02:39",
    "modified": "5-7-2013",
    "issued": "28-4-2013",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/manuals-manuels/data/terr_biosec_standards_goat_1367131154680_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/manuals-manuels/data/terr_biosec_standards_goat_1367131154680_fra"
    },
    "parent_ia_id": "1344707981478",
    "ia_id": "1367131213133",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "National Farm-Level Biosecurity Standard for the Goat Industry",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie caprine"
    },
    "label": {
        "en": "National Farm-Level Biosecurity Standard for the Goat Industry",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie caprine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1367131213133",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "parent_dcr_id": "1344707905203",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "National Farm-Level Biosecurity Standard for the Goat Industry",
            "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie caprine"
        },
        "description": {
            "en": "Biosecurity standard for goat producers",
            "fr": "Norme de bios\u00e9curit\u00e9 pour \u00e9leveurs de caprine"
        },
        "keywords": {
            "en": "Biosecurity, Standard, Goat",
            "fr": "bios\u00e9curit\u00e9, norme, caprine"
        },
        "dcterms.subject": {
            "en": "animal health,best practices,consumer protection,standards",
            "fr": "sant\u00e9 animale,meilleures pratiques,protection du consommateur,norme"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Office of Animal Biosecurity",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-04-28 02:39:17",
            "fr": "2013-04-28 02:39:17"
        },
        "modified": {
            "en": "2013-07-05",
            "fr": "2013-07-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "National Farm-Level Biosecurity Standard for the Goat Industry",
        "fr": "Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme pour l'industrie caprine"
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=0\">Complete Text</a></p>\r\n\r\n\r\n<h2>Table of Contents</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=1\">1 Introduction</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=1#s1c1\">1.1 What is biosecurity?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=1#s2c1\">1.2 Why is biosecurity important to the Canadian goat industry?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=1#s3c1\">1.3 Developing the Standard</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=1#s4c1\">1.4 Purpose of the Standard</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=1#s5c1\">1.5 Developing a Biosecurity Plan</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2\">2 Key Areas of Concern</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2#s6c2\">2.1 Key Area of Concern 1: Sourcing and introducing animals</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2#s7c2\">2.2 Key Area of Concern 2: Animal health</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2#s8c2\">2.3 Key Area of Concern 3: Facility management and access controls</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2#s9c2\">2.4 Key Area of Concern 4: Movement of people, vehicles, and equipment</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2#s10c2\">2.5 Key Area of Concern 5: Monitoring and record keeping</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=2#s11c2\">2.6 Key Area of Concern 6: Communications and training</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=3\">3 Glossary of Terms</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=4\">4 Acknowledgements</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=5\">Appendix A: Target audiences for the Standard</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=6\">Appendix B: Examples of Modes of Disease Transmission</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=7\">Appendix C: Developing a Farm-Level Biosecurity Plan</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/goat-industry/eng/1367131154680/1367131213133?chap=8\">Appendix D: Summary of Key Areas of Concern</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=0\">Texte complet</a></p>\r\n\r\n\r\n<h2>Table des mati\u00e8res</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=1\">1 Introduction</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=1#s1c1\">1.1 Qu'est-ce que la bios\u00e9curit\u00e9?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=1#s2c1\">1.2 Pourquoi la bios\u00e9curit\u00e9 est-elle importante pour l'industrie caprine du Canada?</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=1#s3c1\">1.3 \u00c9laboration de la Norme</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=1#s4c1\">1.4 But de la Norme</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=1#s5c1\">1.5 \u00c9laboration d'un plan de bios\u00e9curit\u00e9</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2\">2 Sujets de pr\u00e9occupation principaux</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2#s6c2\">2.1 Sujet de pr\u00e9occupation principal <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr>\u00a01\u00a0: Provenance et introduction des animaux</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2#s7c2\">2.2 Sujet de pr\u00e9occupation principal <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr>\u00a02\u00a0: Sant\u00e9 des animaux</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2#s8c2\">2.3 Sujet de pr\u00e9occupation principal <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr>\u00a03\u00a0: Gestion des installations et restrictions d'acc\u00e8s</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2#s9c2\">2.4 Sujet de pr\u00e9occupation principal <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr>\u00a04\u00a0: D\u00e9placement de personnes, de v\u00e9hicules et d'\u00e9quipement</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2#s10c2\">2.5 Sujet de pr\u00e9occupation principal <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr>\u00a05\u00a0: Surveillance et tenue des registres</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=2#s11c2\">2.6 Sujet de pr\u00e9occupation principal <abbr title=\"num\u00e9ro\">n<sup>o</sup></abbr>\u00a06\u00a0: Communication et formation</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=3\">3 Glossaire</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=4\">4 Remerciements</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=5\">Annexe\u00a0A\u00a0: Public cible de la Norme</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=6\">Annexe B\u00a0: Exemples de modes de transmission de maladies</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=7\">Annexe C\u00a0: \u00c9laboration d'un plan de bios\u00e9curit\u00e9 \u00e0 la ferme</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/industrie-caprine/fra/1367131154680/1367131213133?chap=8\">Annexe\u00a0D\u00a0: Sommaire des sujets de pr\u00e9occupation principaux</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}