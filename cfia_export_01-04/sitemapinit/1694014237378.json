{
    "dcr_id": "1694014236784",
    "title": {
        "en": "Audit of Project Management",
        "fr": "Audit de la gestion de projets"
    },
    "html_modified": "30-11-2023",
    "modified": "30-11-2023",
    "issued": "4-10-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/audit_prject_managmnt_intro_2023_1694014236784_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/audit_prject_managmnt_intro_2023_1694014236784_fra"
    },
    "parent_ia_id": "1299843588592",
    "ia_id": "1694014237378",
    "parent_node_id": "1299843588592",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Audit of Project Management",
        "fr": "Audit de la gestion de projets"
    },
    "label": {
        "en": "Audit of Project Management",
        "fr": "Audit de la gestion de projets"
    },
    "templatetype": "content page 1 column",
    "node_id": "1694014237378",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1299843498252",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/project-management/",
        "fr": "/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/gestion-de-projets/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Audit of Project Management",
            "fr": "Audit de la gestion de projets"
        },
        "description": {
            "en": "Canadian Food Inspection Agency (CFIA) internal audit is an independent, objective assurance and consulting function designed to add value and improve the agency's operations.",
            "fr": "La v\u00e9rification interne de l'Agence canadienne d'inspection des aliments (ACIA) est une fonction d'assurance et de consultation ind\u00e9pendante et objective visant \u00e0 ajouter de la valeur et \u00e0 am\u00e9liorer les activit\u00e9s de l'agence."
        },
        "keywords": {
            "en": "Audit Review, policy, planning, implementation, Reporting and Accountability, audit reports, executive summary, auditor general, recommendations, project management, agency's response, internal draft audit report, introduction",
            "fr": "Examen de la v\u00e9rification, politique, planification, mise en \u0153uvre, rapports et responsabilisation, rapports d'audit, r\u00e9sum\u00e9, v\u00e9rificateur g\u00e9n\u00e9ral, recommandations, op\u00e9rations sur la paie, rapport provisoire d'audit interne, audit de la gestion de projets, introduction"
        },
        "dcterms.subject": {
            "en": "government information,legislation,policy",
            "fr": "information gouvernementale,l\u00e9gislation,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-11-30",
            "fr": "2023-11-30"
        },
        "modified": {
            "en": "2023-11-30",
            "fr": "2023-11-30"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Audit of Project Management",
        "fr": "Audit de la gestion de projets"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Internal audit</h2>\n\n<p>Canadian Food Inspection Agency (CFIA, the agency) internal audit is an independent, objective assurance and consulting function designed to add value and improve the agency's operations. It helps the CFIA accomplish its objectives by bringing a systematic, disciplined approach to evaluate and improve the effectiveness of risk management, control, and governance processes.</p>\n\n<p>Internal audit engagements are authorized as part of the CFIA's risk-based audit plan, which is updated at least annually for approval by the President. Internal audits are carried out in accordance with the Treasury Board Policy on Internal Audit and the Institute of Internal Auditors' International Professional Practices Framework. Final internal audit reports are approved by the President on the advice of the CFIA audit committee.</p>\n\n<h2>Overview</h2>\n\n<p>The Treasury Board Secretariat (TBS) Directive on the Management of Projects and Programmes sets the expectations for managing projects within Government of Canada departments and agencies. The CFIA has an established project management process, as defined in the enterprise management framework (ePMF). At CFIA, the enterprise project management office (ePMO) is responsible for providing the ePMF, tools and templates for management of projects.</p>\n\n<p>The audit objective was to determine if the CFIA is applying effective project management practices and has a framework that supports successful project outcomes in accordance with Government of Canada policies and directives. The audit scope period was from <span class=\"nowrap\">January 2021 to July 2022.</span></p>\n\n<h2>Key findings</h2>\n\n<p>The audit concluded that, overall, processes and procedures are in place to support project management at CFIA. However, key improvements are required to ensure the process is efficient to support successful project outcomes in the following areas: challenge function and course correction on projects, alignment of the agency's ePMF with the TBS Directive on the Management of Projects and Programmes, and project adherence to the documented ePMF process.</p>\n\n<h2>Complete report</h2>\n\n<ul>\n<li><a href=\"/about-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/project-management/complete-report/eng/1692290514996/1692290515824\">Audit of Project Management</a></li>\n<li><a href=\"/about-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/project-management/complete-report/eng/1692290514996/1692290515824#appi\">Management Response and Action Plan</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>V\u00e9rification interne</h2>\n\n<p>La v\u00e9rification interne de l'Agence canadienne d'inspection des aliments (ACIA, l'agence) est une fonction d'assurance et de consultation ind\u00e9pendante et objective visant \u00e0 ajouter de la valeur et \u00e0 am\u00e9liorer les activit\u00e9s de l'agence. Elle permet \u00e0 l'ACIA d'atteindre ses objectifs en ayant recours \u00e0 une approche syst\u00e9matique et m\u00e9thodique pour \u00e9valuer et am\u00e9liorer l'efficacit\u00e9 des processus de gestion des risques, de contr\u00f4le et de gouvernance.</p>\n\n<p>Les projets de v\u00e9rification interne sont autoris\u00e9s dans le cadre du Plan de v\u00e9rification interne bas\u00e9 sur les risques de l'ACIA, qui est mis \u00e0 jour au moins annuellement pour approbation par le pr\u00e9sident. Les v\u00e9rifications internes sont effectu\u00e9es conform\u00e9ment \u00e0 la Politique sur l'audit interne du Conseil tr\u00e9sor et au Cadre de r\u00e9f\u00e9rence international des pratiques professionnelles de l'Institut des auditeurs internes. Les rapports de v\u00e9rification interne finaux sont approuv\u00e9s par le pr\u00e9sident apr\u00e8s avoir re\u00e7u des conseils par le comit\u00e9 de v\u00e9rification.</p>\n\n<h2>Aper\u00e7u</h2>\n\n<p>La Directive sur la gestion de projets et programmes du Secr\u00e9tariat du Conseil du Tr\u00e9sor (SCT) d\u00e9finit des attentes en mati\u00e8re de gestion de projets au sein des minist\u00e8res et des organismes du gouvernement du Canada. L'ACIA dispose d'un processus de gestion de projet bien \u00e9tabli, comme il est d\u00e9fini dans le Cadre de gestion des projets de l'organisation (CGPO). \u00c0 l'ACIA, le Bureau de gestion des projets de l'organisation (BGPO) est charg\u00e9 de fournir le CGPO, les outils et les mod\u00e8les pour la gestion de projets.</p>\n\n<p>L'audit avait pour objectif de d\u00e9terminer si l'ACIA applique des pratiques efficaces de gestion de projets et si elle dispose d'un cadre qui favorise la r\u00e9ussite des projets conform\u00e9ment aux politiques et aux directives du gouvernement du Canada. La p\u00e9riode vis\u00e9e par l'audit allait de janvier 2021 \u00e0 juillet 2022.</p>\n\n<h2>Principales constatations</h2>\n\n<p>Dans l'ensemble, l'audit a permis de conclure que des processus et des proc\u00e9dures sont en place pour soutenir la gestion de projets \u00e0 l'ACIA. Cependant, des am\u00e9liorations cl\u00e9s sont n\u00e9cessaires pour veiller \u00e0 ce que le processus soit efficace et favorise ainsi la r\u00e9ussite du projet dans les domaines suivants\u00a0: la fonction de remise en question et la prise de mesures correctives concernant les projets, l'harmonisation du CGPO de l'agence avec la Directive sur la gestion de projets et programmes du SCT, et l'adh\u00e9sion des projets au processus du CGPO \u00e9tabli officiellement.</p>\n\n<h2>Rapport complet</h2>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/gestion-de-projets/rapport-complet/fra/1692290514996/1692290515824\">Audit de la gestion de projets</a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/gestion-de-projets/rapport-complet/fra/1692290514996/1692290515824#appi\">R\u00e9ponse et plan d'action de la direction</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}