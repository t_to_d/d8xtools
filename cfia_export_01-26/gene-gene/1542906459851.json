{
    "dcr_id": "1542906459851",
    "title": {
        "en": "Appendix I: Water reclaimed from the condensing of milk and milk products",
        "fr": "Annexe I : Eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d\u2019autres produits de lait"
    },
    "html_modified": "2024-01-26 2:24:25 PM",
    "modified": "2019-01-11",
    "issued": "2019-01-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/appi_wrecl_cmmp_1542906459851_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/appi_wrecl_cmmp_1542906459851_fra"
    },
    "ia_id": "1542906577287",
    "parent_ia_id": "1534954778164",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Applying preventive controls|Maintaining equipment|Making food - Manufacturing|processing|preparing|preserving|treating",
        "fr": "Application de mesures de contr\u00f4le pr\u00e9ventif|Entretien de l'\u00e9quipement|Production d'aliments - Fabrication|transformation|conditionnement|pr\u00e9servation et traitement"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1534954778164",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Appendix I: Water reclaimed from the condensing of milk and milk products",
        "fr": "Annexe I : Eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d\u2019autres produits de lait"
    },
    "label": {
        "en": "Appendix I: Water reclaimed from the condensing of milk and milk products",
        "fr": "Annexe I : Eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d\u2019autres produits de lait"
    },
    "templatetype": "content page 1 column",
    "node_id": "1542906577287",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1534954777758",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/dairy-products/appendix-i/",
        "fr": "/controles-preventifs/produits-laitiers/annexe-i/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Appendix I: Water reclaimed from the condensing of milk and milk products",
            "fr": "Annexe I : Eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d\u2019autres produits de lait"
        },
        "description": {
            "en": "This document outlines the control measures for the use of water reclaimed from the condensing of milk and milk products within the dairy establishment.",
            "fr": "Ce document \u00e9nonce les mesures de contr\u00f4le pour l'utilisation de l'eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d\u2019autres produits de lait dans l'\u00e9tablissement laitier."
        },
        "keywords": {
            "en": "Water, condensing of milk, milk products",
            "fr": "Eau, condensation de la fabrication du lait, produits de lait"
        },
        "dcterms.subject": {
            "en": "water conservation,guidelines,products,dairy products,food processing",
            "fr": "conservation de l'eau,lignes directrices,produit,produit laitier, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-12",
            "fr": "2019-01-12"
        },
        "modified": {
            "en": "2019-01-11",
            "fr": "2019-01-11"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "audience": {
            "en": "educators,business,government,media,scientists",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,m\u00e9dia,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Appendix I: Water reclaimed from the condensing of milk and milk products",
        "fr": "Annexe I : Eau r\u00e9cup\u00e9r\u00e9e de la condensation de la fabrication du lait concentr\u00e9 et d\u2019autres produits de lait"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n<p>Condensed water from milk evaporators and water reclaimed from milk and milk products that are re-used within the establishment must not jeopardize the safety of the product through the introduction of chemical, microbiological or physical hazards. Appropriate uses of this water fall into three categories described below.</p>\n\n<p><strong>Category 1:</strong> Reclaimed water used for potable water purposes, including the production of culinary steam.</p>\n\n<ol>\n<li class=\"mrgn-bttm-md\">Monitor and control water to ensure it is safe and suitable for use\n<ul>\n<li>sample water daily for two weeks following initial installation of the system</li>\n<li>sample water daily for one week following any repairs or alterations to the system</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Test water monthly to ensure it meets the following criteria, using acceptable test methodology\n<ul>\n<li>water is considered microbiologically safe if the maximum acceptable concentration (MAC) for total coliform and E. coli is non-detectable per 100 ml water sample</li>\n<li>verify the heterotrophic plate count (standard plate count) does not exceed 500 cfu/ml</li>\n<li>verify the standard turbidity is less than 5 nephelometric turbidity units (NTU)</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Assess the water weekly for off-odours and appearance (clarity, colour)\n<ul>\n<li>verify the water does not impart any off-taste and off-odours</li>\n<li>verify the water does not feel or appear slimy</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Where water treatment chemicals are used:\n<ul>\n<li>ensure they are safe and suitable for use in dairy processing establishments</li>\n<li>use an automatic metering device, prior to the water entering the storage tank, to assure satisfactory water quality in the storage tank at all times</li>\n<li>test chemical addition twice a day</li>\n<li>ensure they do not pose a contamination risk to the water or the product</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Have procedures in place in the event that the water exceeds safety standards and poses a microbiological and chemical risk\n<ul>\n<li>for example, install an automatic fail-safe monitoring device so that the water is automatically diverted to the sewer if the water exceeds the standards</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Water storage system design:\n<ul>\n<li>ensure storage facilities are designed, constructed and maintained to prevent contamination\n<ul>\n<li>for example, cover, construct out of materials that will not contaminate the water, and allow for periodic cleaning and sanitizing\n<ul>\n<li>use approved materials such as those from the Canadian Water and Wastewater Association (CWWA) or materials for such use as per manufacturer's guidelines</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>ensure the distribution system, within the establishment, is a separate system with no cross-connections to a municipal or private water system</li>\n</ul>\n</li>\n<li>Without proper design, operation, and maintenance, stored water may easily become stagnant and subject to loss of chlorine residual, as well as bacterial regrowth, contaminant entry, and a host of other water quality problems.</li>\n</ol>\n\n<p><strong>Category 2:</strong> Reclaimed water used for limited purposes including production of culinary steam, pre-rinsing of the product surfaces where pre-rinses will not be used in food products, cleaning solution make-up water.</p>\n\n<p>For these uses, in addition to items 3 to 6 above:</p>\n\n<ol>\n<li class=\"mrgn-bttm-md\">Test water monthly to ensure the standard turbidity is less than 5 NTU</li>\n<li class=\"mrgn-bttm-md\">Ensure there is no carry-over of water from one day to the next and\n<ul>\n<li>any water collected is used promptly, or</li>\n<li>the temperature of all water in the storage and distribution system is maintained at 63<sup>o</sup>C (145<sup>O</sup>F) or higher by automatic means, or</li>\n<li>the water is treated with a suitable chemical to suppress bacterial propagation by means of an automatic metering device prior to the water entering the storage tank</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Clearly identify distribution lines and hose stations as \u201climited use reclaimed water<strong>\u201d</strong></li>\n<li class=\"mrgn-bttm-md\">Clearly describe and prominently display water handling practices and guidelines at appropriate locations within the establishment</li>\n<li class=\"mrgn-bttm-md\">Ensure water lines are not permanently connected to product vessels, without a break to the atmosphere and sufficient automatic controls, to prevent the inadvertent addition of this water to product streams.</li>\n</ol>\n\n<p><strong>Category 3:</strong> Reclaimed water not meeting the above criteria may be used as feed water for boilers that is not used for generating culinary steam, or may be used in a thick, double walled, enclosed heat exchanger.</p>\n\n<h2>References</h2>\n\n<ul>\n<li>Grade A Pasteurized Milk Ordinance, 2015, U.S. Department of Health and Human Services Public Health Service Food and Drug Administration</li>\n<li>Health Canada's<a href=\"https://www.canada.ca/en/health-canada/services/environmental-workplace-health/reports-publications/water-quality/guidelines-canadian-drinking-water-quality-summary-table.html\"> Guidelines for Canadian Drinking Water Quality \u2013 Summary Table</a></li>\n<li>Standard Methods for the Examination of Water and Wastewater, latest edition</li>\n</ul>\n\n\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n<p>L'eau de condensation provenant des \u00e9vaporateurs qui servent \u00e0 concentrer le lait et l'eau r\u00e9cup\u00e9r\u00e9e du lait et des produits du lait qui sont r\u00e9utilis\u00e9s dans l'\u00e9tablissement ne doivent pas compromettre la salubrit\u00e9 du produit en y introduisant des dangers chimiques, microbiologiques ou physiques. Les utilisations appropri\u00e9es de cette eau sont r\u00e9parties en trois cat\u00e9gories d\u00e9crites ci-dessous.</p>\n\n<p><strong>Cat\u00e9gorie\u00a01\u00a0:</strong> L'eau r\u00e9cup\u00e9r\u00e9e pouvant servir \u00e0 tous les usages n\u00e9cessitant de l'eau potable, notamment la production de vapeur alimentaire.</p>\n\n<ol>\n<li class=\"mrgn-bttm-md\">Surveiller et contr\u00f4ler l'eau pour veiller \u00e0 ce qu'elle soit salubre et qu'elle poss\u00e8de la qualit\u00e9 voulue\u00a0:\n<ul>\n<li>analyser l'eau chaque jour pendant les deux semaines suivant l'installation initiale du syst\u00e8me;</li>\n<li>si le syst\u00e8me doit \u00eatre r\u00e9par\u00e9 ou modifi\u00e9, analyser l'eau chaque jour pendant la semaine qui suit les travaux.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Analyser l'eau chaque mois, en utilisant des m\u00e9thodes d'essai reconnues, pour s'assurer qu'elle r\u00e9pond aux crit\u00e8res suivants\u00a0:\n<ul>\n<li>l'eau est s\u00e9curitaire sur le plan microbiologique lorsque la concentration maximale acceptable pour les coliformes totaux et les bact\u00e9ries <em>E.\u00a0coli</em> n'est pas d\u00e9celable dans un \u00e9chantillon d'eau de 100\u00a0ml;</li>\n<li>v\u00e9rifier que la num\u00e9ration sur plaque des bact\u00e9ries h\u00e9t\u00e9rotrophes (comptage sur plaque normalis\u00e9) ne d\u00e9passe pas 500\u00a0UFC/ml;</li>\n<li>v\u00e9rifier que la turbidit\u00e9 standard est inf\u00e9rieure \u00e0 cinq unit\u00e9s de turbidit\u00e9 n\u00e9ph\u00e9l\u00e9m\u00e9triques (uTN).</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">\u00c9valuer les propri\u00e9t\u00e9s organoleptiques (limpidit\u00e9, couleur) de l'eau toutes les semaines\u00a0:\n<ul>\n<li>v\u00e9rifier que l'eau ne pr\u00e9sente aucun go\u00fbt ou aucune odeur atypique;</li>\n<li>v\u00e9rifier qu'il ne se forme pas de film biologique sur l'eau.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Lorsque des produits chimiques sont utilis\u00e9s pour le traitement de l'eau\u00a0:\n<ul>\n<li>s'assurer qu'ils sont sans danger et propres \u00e0 \u00eatre utilis\u00e9s dans des \u00e9tablissements de transformation des produits laitiers;</li>\n<li>utiliser un doseur automatique, avant que l'eau n'arrive au r\u00e9servoir de stockage, pour veiller \u00e0 ce qu'elle soit toujours de qualit\u00e9 satisfaisante;</li>\n<li>analyser les ajouts de produits chimiques deux fois par jour;</li>\n<li>veiller \u00e0 ce qu'ils n'entra\u00eenent aucun risque de contamination de l'eau ou du produit.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Mettre en place des proc\u00e9dures d'intervention en cas de d\u00e9passement des normes de salubrit\u00e9 de l'eau, ce qui entra\u00eenerait des risques de nature microbiologique et chimique\u00a0:\n<ul>\n<li>par\u00a0exemple, installer un dispositif de surveillance \u00e0 s\u00fbret\u00e9 int\u00e9gr\u00e9 permettant de d\u00e9tourner l'eau automatiquement vers l'\u00e9gout si elle n'est pas conforme \u00e0 la norme.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Conception du syst\u00e8me de stockage de l'eau\u00a0:\n<ul>\n<li>veiller \u00e0 ce que le syst\u00e8me de stockage soit con\u00e7u, construit et entretenu de fa\u00e7on \u00e0 pr\u00e9venir toute contamination\u00a0:\n<ul>\n<li>par exemple, les mat\u00e9riaux qui le composent et les rev\u00eatements utilis\u00e9s ne doivent pas contaminer l'eau et doivent pouvoir \u00eatre nettoy\u00e9s et d\u00e9sinfect\u00e9s p\u00e9riodiquement\u00a0:\n<ul>\n<li>utiliser des mat\u00e9riaux pouvant compter parmi ceux approuv\u00e9s par l'Association canadienne des eaux potables et us\u00e9es (ACEPU) ou ceux apparaissant dans les directives du fabricant;</li>\n</ul>\n</li>\n</ul>\n</li>\n<li>veiller \u00e0 ce que le syst\u00e8me de distribution dans l'\u00e9tablissement soit un syst\u00e8me distinct qui ne comporte aucune interconnexion au r\u00e9seau municipal ou \u00e0 un r\u00e9seau priv\u00e9.</li>\n</ul>\n</li>\n<li>La conception, l'exploitation et l'entretien inad\u00e9quats des installations de ce type peuvent favoriser la pr\u00e9sence d'eau stagnante et entra\u00eener de nombreux probl\u00e8mes de qualit\u00e9 de l'eau, notamment, des pertes de r\u00e9sidus de chlore, une nouvelle prolif\u00e9ration bact\u00e9rienne et l'introduction de contaminants dans le syst\u00e8me.</li>\n</ol>\n\n<p><strong>Cat\u00e9gorie\u00a02\u00a0:</strong> Eau r\u00e9cup\u00e9r\u00e9e \u00e0 usage restreint pouvant notamment servir \u00e0 la production de vapeur alimentaire, le pr\u00e9-rin\u00e7age des surfaces du produit lorsqu'il ne s'agit pas d'un produit alimentaire, et la pr\u00e9paration d'une solution de nettoyage.</p>\n\n<p>Pour ces utilisations, outre les conditions \u00e9nonc\u00e9es aux points\u00a03 \u00e0 6, les conditions suivantes doivent \u00eatre remplies\u00a0:</p>\n\n<ol>\n<li class=\"mrgn-bttm-md\">Analyser l'eau chaque mois pour v\u00e9rifier que la turbidit\u00e9 standard est inf\u00e9rieure \u00e0 5\u00a0uTN.</li>\n<li class=\"mrgn-bttm-md\">S'assurer que l'eau n'est pas gard\u00e9e pour le lendemain et que\u00a0:\n<ul>\n<li>l'eau r\u00e9cup\u00e9r\u00e9e est r\u00e9utilis\u00e9e rapidement;</li>\n<li>la temp\u00e9rature de l'eau du syst\u00e8me de stockage et de distribution est maintenue \u00e0 63\u00a0<sup>o</sup>C (145\u00a0<sup>o</sup>F) ou plus au moyen de dispositifs automatiques;</li>\n<li>l'eau est trait\u00e9e avant qu'elle arrive dans le r\u00e9servoir de stockage au moyen de produits chimiques appropri\u00e9s qui emp\u00eachent la propagation des bact\u00e9ries \u00e0 l'aide d'un dispositif de dosage automatique.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\">Identifier clairement les conduites de distribution et les robinets arm\u00e9s par la mention \u00ab\u00a0eau r\u00e9cup\u00e9r\u00e9e \u00e0 usage restreint\u00a0\u00bb.</li>\n<li class=\"mrgn-bttm-md\">\u00c9noncer clairement les pratiques et les lignes directrices \u00e0 suivre concernant la manipulation de l'eau et les afficher bien en vue \u00e0 des endroits appropri\u00e9s dans l'\u00e9tablissement.</li>\n<li class=\"mrgn-bttm-md\">Veiller \u00e0 ce que les conduites d'eau ne soient pas branch\u00e9es en permanence aux cuves de produits et qu'elles comportent des mises \u00e0 l'air libre et des syst\u00e8mes de commande automatiques suffisants pour emp\u00eacher le d\u00e9versement accidentel de l'eau \u00e0 r\u00e9utiliser dans les flux de produits.</li>\n</ol>\n\n<p><strong>Cat\u00e9gorie\u00a03\u00a0: </strong> L'eau r\u00e9cup\u00e9r\u00e9e ne remplissant pas les conditions \u00e9nonc\u00e9es ci-dessus peut servir pour l'alimentation des chaudi\u00e8res ne servant pas \u00e0 la production de vapeur alimentaire, ou peut servir dans des \u00e9changeurs de chaleur prot\u00e9g\u00e9s \u00e0 double paroi \u00e9paisse.</p>\n\n<h2>R\u00e9f\u00e9rences</h2>\n\n<ul>\n<li>Grade A Pasteurized Milk Ordinance, 2015, U.S. Department of Health and Human Services Public Health Service Food and Drug Administration (en anglais seulement)</li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/sante-environnement-milieu-travail/rapports-publications/qualite-eau/recommandations-qualite-eau-potable-canada-tableau-sommaire.html\">Recommandations pour la qualit\u00e9 de l'eau potable au Canada - Tableau sommaire</a> de Sant\u00e9 Canada</li>\n<li>Standard Methods for the Examination of Water and Wastewater (en anglais seulement), \u00e9dition la plus r\u00e9cente</li>\n</ul>\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}