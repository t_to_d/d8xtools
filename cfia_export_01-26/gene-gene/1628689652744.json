{
    "dcr_id": "1628689652744",
    "title": {
        "en": "Importing and handling live snails for commercial or personal purposes",
        "fr": "Importation et manipulation des escargots vivants \u00e0 des fins commerciales ou personnelles"
    },
    "html_modified": "2024-01-26 2:25:37 PM",
    "modified": "2021-08-11",
    "issued": "2021-08-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/import_live_snails_comm_purposes_1628689652744_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/import_live_snails_comm_purposes_1628689652744_fra"
    },
    "ia_id": "1628689923502",
    "parent_ia_id": "1299168989280",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299168989280",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importing and handling live snails for commercial or personal purposes",
        "fr": "Importation et manipulation des escargots vivants \u00e0 des fins commerciales ou personnelles"
    },
    "label": {
        "en": "Importing and handling live snails for commercial or personal purposes",
        "fr": "Importation et manipulation des escargots vivants \u00e0 des fins commerciales ou personnelles"
    },
    "templatetype": "content page 1 column",
    "node_id": "1628689923502",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299168913252",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/importing-live-snails-for-commercial-purposes/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/importation-des-escargots-vivants/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importing and handling live snails for commercial or personal purposes",
            "fr": "Importation et manipulation des escargots vivants \u00e0 des fins commerciales ou personnelles"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) regulates the import and handling of live terrestrial snails under the Plant Protection Act because of their potential to harm the Canadian economy and environment.",
            "fr": "L\u2019Agence canadienne d\u2019inspection des aliments (ACIA) r\u00e9glemente l'importation et la manipulation des escargots terrestres vivants en vertu de la Loi sur la protection des v\u00e9g\u00e9taux en raison de leur danger pour l\u2019\u00e9conomie et l\u2019environnement du Canada."
        },
        "keywords": {
            "en": "Importing, handling, Live snails, commercial, personal purposes, Canadian Food Inspection Agency",
            "fr": "Importation, manipulation, escargots vivants, fins commerciales, personnelles"
        },
        "dcterms.subject": {
            "en": "government communications,inspection,pests,plants,environmental protection,transport",
            "fr": "communications gouvernementales,inspection,organisme nuisible,plante,protection de l'environnement,transport"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-08-12",
            "fr": "2021-08-12"
        },
        "modified": {
            "en": "2021-08-11",
            "fr": "2021-08-11"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importing and handling live snails for commercial or personal purposes",
        "fr": "Importation et manipulation des escargots vivants \u00e0 des fins commerciales ou personnelles"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) <a href=\"/plant-health/invasive-species/plant-import/invertebrates-and-micro-organisms/eng/1528728938166/1528729097278\">regulates the import and handling of live terrestrial snails</a> under the <i>Plant Protection Act</i> because of their potential to harm the Canadian economy and environment.</p>\n<p>Snails may destroy vegetable and fruit crops, as well as impact operations and spaces such as nurseries, greenhouses, parks, and gardens. Some snails reproduce quickly which makes them difficult to contain.</p>\n<p>Because the degree of threats to plants by snails vary across snail species, the CFIA's plant health requirements are specific to the species of snail. While certain snails may only be imported for special purposes (for example, for research or exhibition) and be contained, other snails may be allowed for commercial or personal purposes (for example, for snail farming or for use as pets).</p>\n<p>The CFIA regulates certain terrestrial snails as quarantine pests for Canada. These pests are listed on the CFIA's <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">List of Pests Regulated by Canada</a>. The import or handling of the listed snails for commercial or personal use is <strong>prohibited</strong>. This applies to snails belonging to: <i lang=\"la\">Achatina achatina</i>, <i lang=\"la\">Achatina</i> (or <i lang=\"la\">Lissachatina</i>) <i lang=\"la\">fulica</i>, <i lang=\"la\">Archachatina degneri</i>, <i lang=\"la\">A. purpurea</i>, <i lang=\"la\">A. ventricosa</i>, <i lang=\"la\">Cornu aspersum</i>, <i lang=\"la\">Helix</i> spp. and <i lang=\"la\">Theba pisana</i><sup id=\"fn*-rf\"><a class=\"fn-lnk\" href=\"#fn*\"><span class=\"wb-inv\">Footnote\u00a0</span>*</a></sup> Snails commonly known as giant African land snails or giant African snails are included within this list.</p>\n\n<p>The import or handling for commercial or personal purposes may be allowed for snails not listed on the CFIA's List of Pests Regulated by Canada, provided that the snails:</p>\n<ol>\n<li>are collected from the natural environment in Canada and are not recorded for the first time in Canada</li>\n\n<li>belong to either <i lang=\"la\">Cepea nemoralis</i>, <i lang=\"la\">Otala lactea</i> or <i lang=\"la\">O. vermiculata</i>, and are imported under a permit issued under the <i>Plant Protection Act</i></li>\n\n<li>belong to a species of <a href=\"/eng/1433209372739/1433209373489#a10\">aquatic snails</a> which do not require a permit to import under the <i>Plant Protection Act</i></li>\n</ol>\n\n\n\n<p>You <strong>must</strong> <a href=\"/plant-health/invasive-species/plant-import/invertebrates-and-micro-organisms/eng/1528728938166/1528729097278#a3\">apply</a> to the CFIA for permission to import or handle all other snails.</p>\n<p>The above information concerns only requirements under the <i>Plant Protection Act</i>. <a href=\"/eng/1432586422006/1432586423037#s5\">Other requirements</a> may apply depending on the end use of the imported snails. For example, if snails are being imported as a food, <a href=\"/food-licences/eng/1523876882572/1523876882884\">requirements under the CFIA's <i>Safe Food for Canadians Regulations</i> may also apply</a>.</p>\n<p>For additional information on plant protection or other CFIA requirements that apply to the intentional import and handling of snails, please contact your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA inspection office</a>.</p>\n\n<aside class=\"wb-fnote\" role=\"note\"> <h2 id=\"fn\">Footnote</h2> <dl> \n    \n<dt>Footnote *</dt> <dd id=\"fn*\"> \n\n<p>The CFIA's List of Pests Regulated by Canada is subject to change if the CFIA receives new information or updates assessments. This list should be consulted for a most up-to-date listing of snails regulated as quarantine pests. Even if a snail is not listed on the CFIA's List of Pests Regulated by Canada, that does not mean it is allowed to be imported or handled in Canada. Snails not specified on this list may be prohibited or be allowed following review by the CFIA.</p>\n\n    \n    \n<p class=\"fn-rtn\"><a href=\"#fn*-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>*<span class=\"wb-inv\">\u00a0referrer</span></a></p> </dd>\n    \n</dl> </aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) <a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/invertebres-et-de-microorganismes/fra/1528728938166/1528729097278\">r\u00e9glemente l'importation et la manipulation des escargots terrestres vivants</a> en vertu de la <i lang=\"la\">Loi sur la protection des v\u00e9g\u00e9taux</i> en raison de leur danger pour l'\u00e9conomie et l'environnement du Canada.</p>\n<p>Les escargots peuvent d\u00e9truire des cultures de fruits et de l\u00e9gumes et entra\u00eener d'importantes r\u00e9percussions sur les activit\u00e9s et les espaces verts, comme les p\u00e9pini\u00e8res, les serres, les parcs et les jardins. Certains escargots se reproduisent rapidement, rendant difficile la lutte contre ces derniers.</p>\n<p>Comme le degr\u00e9 de la menace que posent les escargots pour les ressources v\u00e9g\u00e9tales varie selon l'esp\u00e8ce, les exigences phytosanitaires de l'ACIA sont propres \u00e0 chaque esp\u00e8ce d'escargots. Alors que certaines esp\u00e8ces d'escargots peuvent uniquement \u00eatre import\u00e9es \u00e0 des fins particuli\u00e8res (par exemple, recherche, d\u00e9monstration) et \u00eatre confin\u00e9es, l'importation d'autres esp\u00e8ces peut \u00eatre permise \u00e0 des fins commerciales ou personnelles (par exemple, h\u00e9liciculture, animaux de compagnie).</p>\n<p>L'ACIA r\u00e9glemente certains escargots terrestres \u00e0 titre d'organismes nuisibles justiciables de quarantaine au Canada. Ces ravageurs sont indiqu\u00e9s dans la <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Liste des organismes nuisibles r\u00e9glement\u00e9s par le Canada</a>. L'importation ou la manipulation des escargots \u00e9num\u00e9r\u00e9s dans la liste \u00e0 des fins commerciales ou personnelles est <strong>interdite</strong>. Cette interdiction vise les esp\u00e8ces suivantes\u00a0: <i lang=\"la\">Achatina achatina</i>, <i lang=\"la\">Achatina</i> (ou <i lang=\"la\">Lissachatina</i>) <i lang=\"la\">fulica</i>, <i lang=\"la\">Archachatina degneri</i>, <i lang=\"la\">A. purpurea</i>, <i lang=\"la\">A. ventricosa</i>, <i lang=\"la\">Cornu aspersum</i>, <i lang=\"la\">Helix</i> spp. et <i lang=\"la\">Theba pisana</i><sup id=\"fn*-rf\"><a class=\"fn-lnk\" href=\"#fn*\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>*</a></sup>. Les escargots commun\u00e9ment connus sous le nom d'escargot terrestre africain g\u00e9ant ou d'escargot g\u00e9ant africain sont inclus dans cette liste. </p>\n\n<p>L'importation ou la manipulation \u00e0 des fins commerciales ou personnelles des escargots qui ne figurent pas dans la liste des organismes nuisibles r\u00e9glement\u00e9s par le Canada de l'ACIA peut \u00eatre permise \u00e0 condition que les escargots\u00a0:</p>\n<ol>\n<li>soient recueillis dans leur milieu naturel au Canada et ne soient pas mentionn\u00e9s comme pr\u00e9sents pour la premi\u00e8re fois au Canada;</li>\n\n<li>appartiennent aux esp\u00e8ces <i lang=\"la\">Cepea nemoralis</i>, <i lang=\"la\">Otala lactea</i> ou <i lang=\"la\">O. vermiculata</i>, et soient import\u00e9s au moyen d'un permis d\u00e9livr\u00e9 en vertu de la <i lang=\"la\">Loi sur la protection des v\u00e9g\u00e9taux</i>;</li>\n\n<li>appartiennent \u00e0 une esp\u00e8ce d'<a href=\"/fra/1433209372739/1433209373489#a12\">escargots aquatiques</a> qui n'exige pas de permis d'importation en vertu de la <i lang=\"la\">Loi sur la protection des v\u00e9g\u00e9taux</i>.</li>\n</ol>\n<p>Vous <strong>devez</strong> <a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/invertebres-et-de-microorganismes/fra/1528728938166/1528729097278#a3\">demander</a> \u00e0 l'ACIA l'autorisation d'importer ou de manipuler tous les autres escargots.</p>\n<p>  Les renseignements ci-dessus portent uniquement sur les exigences de la <i lang=\"la\">Loi sur la protection des v\u00e9g\u00e9taux</i>. D'<a href=\"/fra/1432586422006/1432586423037#s5\">autres exigences</a> peuvent s'appliquer selon l'utilisation finale des escargots import\u00e9s. Par exemple, si les escargots sont import\u00e9s \u00e0 titre d'aliments, les <a href=\"/licences-pour-aliments/fra/1523876882572/1523876882884\"><i lang=\"la\">exigences du R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> de l'ACIA</a> peuvent s'appliquer.</p>\n<p>Pour de plus amples renseignements sur la protection des v\u00e9g\u00e9taux ou les autres exigences de l'ACIA qui s'appliquent \u00e0 l'importation intentionnelle et \u00e0 la manipulation des escargots, veuillez communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau d'inspection de l'ACIA de votre r\u00e9gion</a>.</p>\n\n<aside class=\"wb-fnote\" role=\"note\"> <h2 id=\"fn\">Note de bas de page</h2> <dl> <dt>Note de bas de page *</dt> <dd id=\"fn*\"> <p>La Liste des organismes nuisibles r\u00e9glement\u00e9s par le Canada de l'ACIA peut changer si l'ACIA re\u00e7oit de nouveaux renseignements ou de nouvelles mises \u00e0 jour ou \u00e9valuations. La liste doit \u00eatre consult\u00e9e pour obtenir la liste la plus \u00e0 jour des escargots r\u00e9glement\u00e9s \u00e0 titre d'organismes nuisibles justiciables de quarantaine au Canada. Le fait qu'un escargot ne soit pas indiqu\u00e9 dans La liste des organismes nuisibles r\u00e9glement\u00e9s par le Canada de l'ACIA ne signifie pas qu'il peut \u00eatre import\u00e9 ou manipul\u00e9 au Canada. L'importation ou la manipulation des escargots qui ne figurent pas dans la liste peut \u00eatre interdite ou permise suivant un examen de l'ACIA.</p>\n<p class=\"fn-rtn\"><a href=\"#fn*-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>*</a></p> </dd></dl> </aside>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}