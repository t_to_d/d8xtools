{
    "dcr_id": "1513698843154",
    "title": {
        "en": "Video: What is bovine <abbr title=\"bovine tuberculosis\">TB</abbr>?",
        "fr": "Vid\u00e9o : Qu'est-ce que la tuberculose bovine?"
    },
    "html_modified": "21-12-2017",
    "modified": "21-12-2017",
    "issued": "19-12-2017",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/video_what_is_bovine_tb_1513698843154_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/video_what_is_bovine_tb_1513698843154_fra"
    },
    "parent_ia_id": "1330206128556",
    "ia_id": "1513698922723",
    "parent_node_id": "1330206128556",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Video: What is bovine <abbr title=\"bovine tuberculosis\">TB</abbr>?",
        "fr": "Vid\u00e9o : Qu'est-ce que la tuberculose bovine?"
    },
    "label": {
        "en": "Video: What is bovine <abbr title=\"bovine tuberculosis\">TB</abbr>?",
        "fr": "Vid\u00e9o : Qu'est-ce que la tuberculose bovine?"
    },
    "templatetype": "content page 1 column",
    "node_id": "1513698922723",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1330205978967",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/bovine-tuberculosis/what-is-bovine-tb-/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tuberculose-bovine/qu-est-ce-que-la-tuberculose-bovine-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Video: What is bovine TB?",
            "fr": "Vid\u00e9o : Qu'est-ce que la tuberculose bovine?"
        },
        "description": {
            "en": "A behind-the-scenes look at what happened during the first phases of the bovine TB investigation. Watch the following video to learn about what goes on during an outbreak investigation and hear from the people who were part of it all.",
            "fr": "Entrez dans les coulisses des premi\u00e8res phases de l'enqu\u00eate sur la tuberculose bovine.  Regardez les vid\u00e9os suivantes pour en apprendre davantage sur ce qui se passe lors d'une enqu\u00eate sur une \u00e9pid\u00e9mie par les t\u00e9moignages des personnes qui en faisaient partie."
        },
        "keywords": {
            "en": "animal health, export, policies, cow, bovine, heifer, regulations, certificates, outbreak investigation, video, What is bovine TB?",
            "fr": "sant\u00e9 des animaux, exportation, politique, vache, bovin, g\u00e9nisse, r\u00e9glements, certificats, enqu\u00eate sur la tuberculose bovine, S\u00e9rie vid\u00e9o, Qu'est-ce que la tuberculose bovine?"
        },
        "dcterms.subject": {
            "en": "animal diseases,animal health,exports,inspection,veterinary medicine",
            "fr": "maladie animale,sant\u00e9 animale,exportation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-12-21",
            "fr": "2017-12-21"
        },
        "modified": {
            "en": "2017-12-21",
            "fr": "2017-12-21"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government",
            "fr": "gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Video: What is bovine TB?",
        "fr": "Vid\u00e9o : Qu'est-ce que la tuberculose bovine?"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Learn more about what bovine tuberculosis is and how this disease can spread.</p>\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://www.youtube.com/watch?v=xhyI5U8JyBg&amp;feature=youtu.be\"}'>\n<video title=\"What is Bovine TB?\">\n<source type=\"video/youtube\" src=\"https://www.youtube.com/watch?v=xhyI5U8JyBg&amp;feature=youtu.be\"></source>\n</video>\n<figcaption>\n<details id=\"inline-transcript1\">\n<summary>What is Bovine <abbr title=\"bovine tuberculosis\">TB</abbr>? \u2013 Transcript</summary>\n<p><strong>The Canadian Food Inspection Agency corporate introduction plays. It shows images that represent the work of the Agency, including a petri dish, strawberries, a growing plant, a chicken and a maple leaf.</strong></p>\n<p><strong>Text: <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> - Safeguarding with Science</strong></p>\n<p><strong>Text: What is Bovine Tuberculosis?</strong></p>\n<p><strong>Montage of Black Angus cattle in Alberta's prairies.</strong></p>\n<p><strong>Off screen speaker: Former Chief Veterinary Officer for Canada 2014-2017, <abbr title=\"Doctor\">Dr.</abbr> Harpreet Kochhar.</strong></p>\n<p>Bovine tuberculosis, referred to as <abbr title=\"bovine tuberculosis\">TB</abbr>, is a chronic contagious bacterial disease of livestock, and occasionally other species of mammals, resulting from infection with <span lang=\"la\">Mycobacterium bovis</span>.</p>\n<p><strong>On screen speaker: Former Chief Veterinary Officer for Canada 2014-2017, <abbr title=\"Doctor\">Dr.</abbr> Harpreet Kochhar.</strong></p>\n<p>Infected animals with progressive disease shed the bacteria in respiratory secretions and aerosols, feces, milk, and sometimes in urine, vaginal secretions, or semen. As a result, disease may be spread in a variety of ways, most commonly through the inhalation of micro-droplets in aerosols from already infected animals and from the ingestion of contaminated food and water.</p>\n<p><strong>Video footage of multiple Black Angus cattles eating from a feeder.</strong></p>\n<p>The bacteria associated with the disease may lie dormant in an infected animal for years without causing clinical signs or progressive disease symptoms. It can reactivate during periods of stress or in older animals. When disease becomes progressive, it generally results in enlarged lesions which may be found in a variety of tissues including lymph nodes of the head and thorax, lung, spleen, and liver. In countries with eradication programs such as Canada, advanced disease is rare as most cases are detected at an early stage when infection typically consists of few or small lesions in the lungs or lymph nodes associated with the respiratory system.</p>\n<p>Bovine <abbr title=\"bovine tuberculosis\">TB</abbr> is a zoonosis, that is, an infection that can be transmitted from affected animals to people, causing a condition similar to human <abbr title=\"bovine tuberculosis\">TB</abbr>. People are most commonly infected through the ingestion of unpasteurized dairy products derived from infected animals but also through inhalation of infectious aerosols or direct contact through breaks in the skin. Currently the risk to the general population in Canada is considered to be very low due to pasteurization of milk and livestock surveillance and testing programs.</p>\n<p><strong>Canada wordmark. Copyright Her Majesty the Queen in Right of Canada (Canadian Food Inspection Agency), 2017.</strong></p>\n</details>\n</figcaption>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>D\u00e9couvrez c'est quoi la tuberculose bovine et comment cette maladie peut se propager.</p>\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://www.youtube.com/watch?v=dwoDJPXebRg&amp;feature=youtu.be\"}'>\n<video title=\"Qu'est-ce que la tuberculose bovine?\">\n<source type=\"video/youtube\" src=\"https://www.youtube.com/watch?v=dwoDJPXebRg&amp;feature=youtu.be\"></source>\n</video>\n<figcaption>\n<details id=\"inline-transcript1\">\n<summary>Qu'est-ce que la tuberculose bovine? \u2013 Transcription</summary>\n<p><strong>Mise en contexte de l'Agence canadienne d'inspection des aliments. Le travail de l'Agence est pr\u00e9sent\u00e9 en images, notamment une bo\u00eete de P\u00e9tri, des fraises, un plant en croissance, un poulet et une feuille d'\u00e9rable.</strong></p>\n<p><strong>Texte\u00a0: <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> - Pr\u00e9server gr\u00e2ce \u00e0 la science</strong></p>\n<p><strong>Texte\u00a0: Qu'est-ce que la tuberculose bovine?</strong></p>\n<p><strong>Montage video pr\u00e9sentant du b\u00e9tail dans les prairies d'Alberta.</strong></p>\n<p><strong>Interlocuteur en voix hors champs\u00a0: <abbr title=\"Docteur\">Dr</abbr> Jaspinder Komal, directeur ex\u00e9cutif et v\u00e9t\u00e9rinaire en chef adjoint \u00e0 la direction sant\u00e9 des animaux de l'Agence canadienne d'inspection des aliments.</strong></p>\n<p>La tuberculose bovine est une maladie bact\u00e9rienne contagieuse chronique du b\u00e9tail ainsi que d'autres esp\u00e8ces de mammif\u00e8res. Elle r\u00e9sulte d'une infection par <span lang=\"la\">Mycobacterium bovis</span>.</p>\n<p>La bact\u00e9rie est excr\u00e9t\u00e9e dans les s\u00e9cr\u00e9tions et les a\u00e9rosols  respiratoires, les mati\u00e8res f\u00e9cales, le lait, les s\u00e9cr\u00e9tions vaginales ou le sperme. La maladie se propage par des micro-gouttelettes dans les a\u00e9rosols ou par l'ingestion d'eau et d'aliments contamin\u00e9s.</p>\n<p><strong>Interlocuteur \u00e0 l'\u00e9cran\u00a0: <abbr title=\"Docteur\">Dr</abbr> Jaspinder Komal, directeur ex\u00e9cutif et v\u00e9t\u00e9rinaire en chef adjoint \u00e0 la direction sant\u00e9 des animaux de l'Agence canadienne d'inspection des aliments.</strong></p>\n<p>\u00c7a peut prendre beaucoup de temps avant qu'un animal pr\u00e9sente des signes clinique de tuberculose bovine car la maladie peut rester dormante chez un animal infect\u00e9 durant des ann\u00e9es sans causer de signes cliniques ni de sympt\u00f4mes de maladie \u00e9volutive.</p>\n<p>La bact\u00e9rie peut \u00eatre r\u00e9activ\u00e9e avec le stress ou chez les animaux plus \u00e2g\u00e9s.  Lorsque la maladie devient \u00e9volutive, il y a une aggravation des l\u00e9sions dans les ganglions lymphatiques de la t\u00eate et du thorax, les poumons ainsi que dans la rate et le foie. Dans les pays o\u00f9 des programmes d'\u00e9radication existent, comme le Canada, le stade avanc\u00e9  de la maladie est rare, parce que la plupart des cas sont d\u00e9tect\u00e9s \u00e0 un stade pr\u00e9coce.</p>\n<p>La sant\u00e9 humaine est concern\u00e9e par la tuberculose bovine car c'est une maladie zoonotique. c'est-\u00e0-dire une infection qui peut \u00eatre transmise aux humains par les animaux. L'infection peut \u00eatre caus\u00e9e chez les humains par l'ingestion de produits laitiers non pasteuris\u00e9s provenant des animaux infect\u00e9s ou par l'inhalation d'a\u00e9rosols infectieux ou par un contact direct par des \u00e9corchures de la peau.</p>\n<p>Le risque pour la population canadienne tr\u00e8s faible parce qu'on consomme le lait pasteuris\u00e9 et qu'on a un tr\u00e8s bon programme de surveillance de troupeaux et de d\u00e9pistage de la maladie.</p>\n<p><strong>Mot-symbole \u00ab\u00a0Canada\u00a0\u00bb. Droit d'auteur Sa Majest\u00e9 la Reine du chef du Canada (Agence canadienne d'inspection des aliments), 2017.</strong></p>\n</details>\n</figcaption>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}