{
    "dcr_id": "1402950657191",
    "title": {
        "en": "Using Control Zones to Manage Animal Disease Risk",
        "fr": "Utilisation des zones de contr\u00f4le  pour g\u00e9rer le risque de maladie animale"
    },
    "html_modified": "30-9-2014",
    "modified": "16-6-2014",
    "issued": "16-6-2014",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_cwd_zone_manage_risk_factsheet_1402950657191_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_cwd_zone_manage_risk_factsheet_1402950657191_fra"
    },
    "parent_ia_id": "1300388449143",
    "ia_id": "1402950699325",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Using Control Zones to Manage Animal Disease Risk",
        "fr": "Utilisation des zones de contr\u00f4le  pour g\u00e9rer le risque de maladie animale"
    },
    "label": {
        "en": "Using Control Zones to Manage Animal Disease Risk",
        "fr": "Utilisation des zones de contr\u00f4le  pour g\u00e9rer le risque de maladie animale"
    },
    "templatetype": "content page 1 column",
    "node_id": "1402950699325",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1300388388234",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/control-zones/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/zones-de-controle/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Using Control Zones to Manage Animal Disease Risk",
            "fr": "Utilisation des zones de contr\u00f4le  pour g\u00e9rer le risque de maladie animale"
        },
        "description": {
            "en": "A control zone is a defined area that is established to prevent the spread of animal disease from an infected area to areas free of the disease.",
            "fr": "Une zone de contr\u00f4le est une zone \u00e9tablie pour pr\u00e9venir la propagation d'une maladie animale d'une r\u00e9gion infect\u00e9e aux r\u00e9gions indemnes."
        },
        "keywords": {
            "en": "Animals, Animal Health, Control Zones, Animal Disease, Risk",
            "fr": "Animaux, Sant\u00e9 des animaux, Utilisation des zones, maladies animales, risques"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,livestock,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,b\u00e9tail,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-09-30",
            "fr": "2014-09-30"
        },
        "modified": {
            "en": "2014-06-16",
            "fr": "2014-06-16"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Using Control Zones to Manage Animal Disease Risk",
        "fr": "Utilisation des zones de contr\u00f4le pour g\u00e9rer le risque de maladie animale"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>A control zone is a defined area that is established to prevent the spread of animal disease from an infected area to areas free of the disease. Movement restrictions may be placed on certain products leaving, going into, or moving within the control zone.</p>\n<p>Zoning is an internationally recognized practice to manage disease risk and maintain trade. It is one tool within a suite of measures that the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> uses to respond to animal diseases in Canada. A typical zoning strategy would include:</p>\n<ul>\n<li>a <strong>primary control zone</strong> where the disease has been detected, or is suspected to exist; and</li>\n<li>a <strong>secondary control zone</strong> where the disease is not suspected to exist and is a buffer between the primary control zone and the rest of Canada.</li>\n</ul>\n<figure>\n<img alt=\"Image - Control Zones.\" src=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/images-images/dis_cwd_zone_manage_risk_factsheet_1402951565861_eng.jpg\" width=\"480\" class=\"img-responsive\">\n<details>\n<summary> Description of image - Control Zones </summary>\n<p>The image illustrates one way how the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> can establish primary and secondary control zones to respond to animal disease in Canada, and prevent disease from spreading to unaffected areas. In this illustration, a primary control zone resides within a larger secondary control zone. This secondary control zone acts as a buffer from the rest of Canada and resides within a larger area that is free of disease.</p>\n</details>\n</figure>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> determines if zones are required based on the unique circumstances of each disease situation. Some factors included in this determination are the nature of the disease, presence of the disease in wildlife or the environment, potential for disease spread and geographical features in the area (<abbr title=\"that is to say\">i.e.</abbr>, waterways, roads, terrain).</p>\n<p>When a <strong>foreign animal disease</strong> is first detected, the goal of the zone is to rapidly contain and eradicate the disease. If the disease can be eradicated from an animal population, then the size of the zone could be slowly reduced or targeted until the disease is gone and the zone is no longer necessary.</p>\n<p>If a disease becomes <strong>established</strong> in a geographic area and cannot be eradicated, then the zone could remain in place for a long time or be enlarged. This often applies when a disease has become endemic in the local wildlife populations because it is more difficult to implement effective controls on wildlife.</p>\n<p>As described in the following tables, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> can implement a variety of disease measures based on the specific details of each disease investigation.</p>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption>Measures for the primary control zone</caption>\n<thead>\n<tr>\n<th>Primary Control Zone</th>\n<th>Established disease</th>\n<th>Foreign disease</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th>Movement controls - leaving the zone</th>\n<td>Yes</td>\n<td>Yes</td>\n</tr>\n<tr>\n<th>Movement controls - within the zone</th>\n<td>Yes, but minimal</td>\n<td>Yes, very strict</td>\n</tr>\n<tr>\n<th>Movement controls - into the zone</th>\n<td>Yes, but minimal</td>\n<td>Yes, very strict</td>\n</tr>\n<tr>\n<th>Disease controls (depopulation, cleaning and disinfection) within the zone</th>\n<td>Minimal or none</td>\n<td>Yes, aggressive response</td>\n</tr>\n<tr>\n<th>Disease surveillance</th>\n<td>No</td>\n<td>Yes</td>\n</tr>\n<tr>\n<th>Geographic area of zone</th>\n<td>May enlarge over time if the disease spreads to new areas</td>\n<td>Goal is to reduce size and eliminate</td>\n</tr>\n</tbody>\n</table>\n</div>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption>Measures for the secondary control zone</caption>\n<thead>\n<tr>\n<th>Secondary Control Zone</th>\n<th>Established disease</th>\n<th>Foreign disease</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th>Movement controls - leaving the zone</th>\n<td>No</td>\n<td>No</td>\n</tr>\n<tr>\n<th>Movement controls - within the zone</th>\n<td>No</td>\n<td>No</td>\n</tr>\n<tr>\n<th>Movement controls \u2013 into the zone</th>\n<td>No</td>\n<td>No</td>\n</tr>\n<tr>\n<th>Disease controls for new cases</th>\n<td>Yes, aggressive response</td>\n<td>Yes, aggressive response</td>\n</tr>\n<tr>\n<th>Surveillance</th>\n<td>Yes</td>\n<td>Yes</td>\n</tr>\n<tr>\n<th>Geographic area of zone</th>\n<td>May enlarge over time</td>\n<td>Goal is to reduce size and eliminate</td>\n</tr>\n</tbody>\n</table>\n</div>\n<h2>For more information:</h2>\n<ul>\n<li>\n<a href=\"/english/reg/jredirect2.shtml?heasana\">\n<i>Health of Animals Act</i>\n</a>\n</li>\n<li>\n<a href=\"/english/reg/jredirect2.shtml?rdmd\">\n<i>Reportable Diseases Regulations</i>\n</a>\n</li>\n<li>\n<a href=\"/animal-health/terrestrial-animals/diseases/reportable/eng/1303768471142/1303768544412\">Reportable diseases</a>\n</li>\n<li>\n<a href=\"/animal-health/terrestrial-animals/biosecurity/eng/1299868055616/1320534707863\">Animal Biosecurity</a>\n</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Une zone de contr\u00f4le est une zone \u00e9tablie pour pr\u00e9venir la propagation d'une maladie animale d'une r\u00e9gion infect\u00e9e aux r\u00e9gions indemnes. Des restrictions de mouvement peuvent \u00eatre impos\u00e9es sur certains produits qui quittent la zone de contr\u00f4le, y entrent ou s'y d\u00e9placent.</p>\n<p>Le zonage est une pratique reconnue \u00e0 l'\u00e9chelle mondiale pour g\u00e9rer le risque de maladie animale et le maintien du commerce. Il s'agit d'un outil parmi une s\u00e9rie de mesures que l'Agence canadienne d'inspection des aliments (ACIA) utilise pour intervenir dans les cas de maladie animale au Canada. Une strat\u00e9gie de zonage type pr\u00e9voit :</p>\n<ul>\n<li>une <strong>zone de contr\u00f4le primaire</strong>, o\u00f9 la maladie a \u00e9t\u00e9 d\u00e9cel\u00e9e ou la pr\u00e9sence de la maladie est soup\u00e7onn\u00e9e;</li>\n<li>une <strong>zone de contr\u00f4le secondaire</strong>, o\u00f9 la maladie n'est pas soup\u00e7onn\u00e9e, qui joue le r\u00f4le de zone tampon entre la zone de contr\u00f4le primaire et le reste du Canada.</li>\n</ul>\n<figure>\n<img alt=\"Image - Zones de contr\u00f4le.\" src=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/images-images/dis_cwd_zone_manage_risk_factsheet_1402951565861_fra.jpg\" width=\"480\" class=\"img-responsive\">\n<details>\n<summary>Description de l'image - Zones de contr\u00f4le </summary>\n<p>L'image montre une mani\u00e8re donc l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pourrait \u00e9tablir des zones de contr\u00f4le primaires et secondaires afin de r\u00e9pondre aux maladies animales au Canada, et pour pr\u00e9venir la maladie de se propager \u00e0 de nouvelles r\u00e9gions. Dans cette illustration, la zone de contr\u00f4le primaire r\u00e9side dans une zone de contr\u00f4le secondaire plus grand. Cette zone de contr\u00f4le secondaire joue le r\u00f4le de zone tampon avec le reste du Canada o\u00f9 la maladie n'est pas soup\u00e7onn\u00e9e. </p>\n</details>\n</figure>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d\u00e9termine si l'\u00e9tablissement de zones est requis en se fondant sur les circonstances propres \u00e0 chaque cas de maladie. Cette d\u00e9termination repose sur divers facteurs, notamment la nature de la maladie, sa pr\u00e9sence dans la faune ou l'environnement, son potentiel de propagation et les caract\u00e9ristiques g\u00e9ographiques de la r\u00e9gion (les cours d'eau, les routes et le terrain).</p>\n<p>Lorsqu'une <strong>maladie animale exotique</strong> est d\u00e9cel\u00e9e pour la premi\u00e8re fois, l'objectif de la zone est de contenir rapidement la maladie et de l'\u00e9radiquer. Lorsque la maladie peut \u00eatre \u00e9radiqu\u00e9e d'une population animale, l'\u00e9tendue de la zone peut alors \u00eatre r\u00e9duite lentement ou cibl\u00e9e jusqu'au moment o\u00f9 la maladie a disparu et que la zone n'a plus d'utilit\u00e9.</p>\n<p>Lorsqu'une maladie s'est <strong>\u00e9tablie</strong> dans une r\u00e9gion et qu'elle ne peut pas \u00eatre \u00e9radiqu\u00e9e, la zone peut demeurer en place pendant une longue p\u00e9riode ou m\u00eame \u00eatre \u00e9largie. Cette solution s'applique souvent lorsqu'une maladie est devenue end\u00e9mique dans les populations locales d'animaux sauvages parce qu'il est plus difficile de mettre en place des mesures de contr\u00f4le efficaces au sein de la faune.</p>\n<p>Comme le montrent les tableaux qui suivent, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut mettre en \u0153uvre certaines mesures contre la maladie en fonction des caract\u00e9ristiques particuli\u00e8res de chaque enqu\u00eate \u00e9pid\u00e9miologique.</p>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption>Des mesures pour la zone de contr\u00f4le primaire</caption>\n<thead>\n<tr>\n<th>Zone de contr\u00f4le primaire</th>\n<th>Maladie \u00e9tablie</th>\n<th>Maladie exotique</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th>Contr\u00f4le des d\u00e9placements \u2013 vers l'ext\u00e9rieur de la zone</th>\n<td>Oui</td>\n<td>Oui</td>\n</tr>\n<tr>\n<th>Contr\u00f4le des d\u00e9placements \u2013 \u00e0 l'int\u00e9rieur de la zone</th>\n<td>Oui, mais minimaux</td>\n<td>Oui, tr\u00e8s rigoureux</td>\n</tr>\n<tr>\n<th>Contr\u00f4le des d\u00e9placements \u2013 vers l'int\u00e9rieur de la zone</th>\n<td>Oui, mais minimaux</td>\n<td>Oui, tr\u00e8s rigoureux</td>\n</tr>\n<tr>\n<th>Contr\u00f4le de la maladie (abattage, nettoyage et d\u00e9sinfection) dans la zone</th>\n<td>Minimaux ou aucun</td>\n<td>Oui, une intervention \u00e9nergique</td>\n</tr>\n<tr>\n<th>Surveillance de la maladie</th>\n<td>Non</td>\n<td>Oui</td>\n</tr>\n<tr>\n<th>R\u00e9gion g\u00e9ographique de la zone</th>\n<td>Possibilit\u00e9 d'\u00e9largir la zone si la maladie se propage \u00e0 de nouvelles r\u00e9gions</td>\n<td>Le but est de r\u00e9duire l'\u00e9tendue de la zone et d'\u00e9liminer la zone</td>\n</tr>\n</tbody>\n</table>\n</div>\n<div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<caption>Des mesures pour la zone de contr\u00f4le secondaire</caption>\n<thead>\n<tr>\n<th>Zone de contr\u00f4le secondaire</th>\n<th>Maladie \u00e9tablie</th>\n<th>Maladie exotique</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<th>Contr\u00f4le des d\u00e9placements \u2013 vers l'ext\u00e9rieur de la zone</th>\n<td>Non</td>\n<td>Non</td>\n</tr>\n<tr>\n<th>Contr\u00f4le des d\u00e9placements \u2013 \u00e0 l'int\u00e9rieur de la zone</th>\n<td>Non</td>\n<td>Non</td>\n</tr>\n<tr>\n<th>Contr\u00f4le des d\u00e9placements \u2013 vers l'int\u00e9rieur de la zone</th>\n<td>Non</td>\n<td>Non</td>\n</tr>\n<tr>\n<th>Contr\u00f4le des nouveaux cas de la maladie</th>\n<td>Oui, une intervention \u00e9nergique</td>\n<td>Oui, une intervention \u00e9nergique</td>\n</tr>\n<tr>\n<th>Surveillance</th>\n<td>Oui</td>\n<td>Oui</td>\n</tr>\n<tr>\n<th>R\u00e9gion g\u00e9ographique de la zone</th>\n<td>Possibilit\u00e9 d'\u00e9largir la zone avec le temps</td>\n<td>Le but est de r\u00e9duire l'\u00e9tendue de la zone et d'\u00e9liminer la zone</td>\n</tr>\n</tbody>\n</table>\n</div>\n<h2>Pour en savoir davantage :</h2>\n<ul>\n<li>\n<a href=\"/francais/reg/jredirect2.shtml?heasana\">\n<i>Loi sur la sant\u00e9 des animaux</i>\n</a>\n</li>\n<li>\n<a href=\"/francais/reg/jredirect2.shtml?rdmd\">\n<i>R\u00e8glement sur les maladies d\u00e9clarables</i>\n</a>\n</li>\n<li>\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fra/1303768471142/1303768544412\">Maladies \u00e0 d\u00e9claration obligatoire</a>\n</li>\n<li>\n<a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/fra/1299868055616/1320534707863\">Bios\u00e9curit\u00e9 animale</a>\n</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}