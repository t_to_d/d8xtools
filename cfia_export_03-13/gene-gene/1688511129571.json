{
    "dcr_id": "1688511129571",
    "title": {
        "en": "Moving flocks and poultry products through control zones",
        "fr": "D\u00e9placement d'\u00e9levages et de produits avicoles \u00e0 travers les zones de contr\u00f4le"
    },
    "html_modified": "2024-03-12 3:59:31 PM",
    "modified": "2023-07-05",
    "issued": "2023-07-05",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/ai_move_flock_through_cz_1688511129571_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/ai_move_flock_through_cz_1688511129571_fra"
    },
    "ia_id": "1688511129931",
    "parent_ia_id": "1651075538958",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1651075538958",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Moving flocks and poultry products through control zones",
        "fr": "D\u00e9placement d'\u00e9levages et de produits avicoles \u00e0 travers les zones de contr\u00f4le"
    },
    "label": {
        "en": "Moving flocks and poultry products through control zones",
        "fr": "D\u00e9placement d'\u00e9levages et de produits avicoles \u00e0 travers les zones de contr\u00f4le"
    },
    "templatetype": "content page 1 column",
    "node_id": "1688511129931",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1651075538411",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/control-zones/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/zones-de-controle/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Moving flocks and poultry products through control zones",
            "fr": "D\u00e9placement d'\u00e9levages et de produits avicoles \u00e0 travers les zones de contr\u00f4le"
        },
        "description": {
            "en": "To help prevent the spread of highly pathogenic avian influenza in Canada, there are movement restrictions into, out of, within or through a primary control zone.",
            "fr": "Afin de pr\u00e9venir la propagation de l'influenza aviaire hautement pathog\u00e8ne au Canada, des restrictions sont impos\u00e9es aux d\u00e9placements \u00e0 destination ou en provenance d'une zone de contr\u00f4le primaire, ou \u00e0 l'int\u00e9rieur de celle-ci."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, Reportable Diseases Regulations, reportable diseases, Avian Influenza, bird flu",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e9glement sur la sant\u00e9 des animaux, R\u00e9glement sur les maladies d\u00e9clarables, maladies; d\u00e9claration obligatoire, Influenza aviaire, grippe aviaire"
        },
        "dcterms.subject": {
            "en": "animal diseases,animal health,infectious diseases,livestock",
            "fr": "maladie animale,sant\u00e9 animale,maladie infectieuse,b\u00e9tail"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-07-05",
            "fr": "2023-07-05"
        },
        "modified": {
            "en": "2023-07-05",
            "fr": "2023-07-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Moving flocks and poultry products through control zones",
        "fr": "D\u00e9placement d'\u00e9levages et de produits avicoles \u00e0 travers les zones de contr\u00f4le"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>To help prevent the spread of highly pathogenic avian influenza (HPAI) in Canada, there are movement restrictions into, out of, within or through a primary control zone.</p>\n<p>Restrictions by other countries vary based off control zones. See <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/restrictions-imposed-by-foreign-countries/eng/1666817801088/1666817801682\">the consolidated list of restrictions imposed by foreign countries</a>.</p>\n<section class=\"gc-srvinfo\">\n<h2>Services and information</h2>\n<div class=\"wb-eqht row\">\n<div class=\"col-md-4\">\n<h3><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/status-of-ongoing-avian-influenza-response/ai-zones/eng/1648851134912/1648851359195\">Map: Highly pathogenic avian influenza zones</a></h3>\n<p>Find out if your area is impacted or near an infected zone touched by the latest bird flu.</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/status-of-ongoing-avian-influenza-response/permits-and-conditions/eng/1648871137667/1648871138011\">Permits and conditions needed for movement control</a></h3>\n<p>What is needed for the transportation of birds and by-products in a primary control zone.</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/pcz-pos/eng/1655655067588/1655655068135\">Surveillance activities in a primary control zone</a></h3>\n<p>What to expect from provincial and territorial governments, livestock and poultry industries as they conduct surveillance.</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/the-path-to-revoking-a-primary-control-zone/eng/1654648457092/1654648457435\">Path to revoking a primary control zone</a></h3>\n<p>A process outlining steps to revoke a primary control zone (infected premises, infected zones).</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/status-of-ongoing-avian-influenza-response/2022-04-28/eng/1650999759061/1650999759803\">Notice to industry: Flock placement in infected zones</a></h3>\n<p>We are working to eliminate the disease as quickly as possible in order to resume normal business activities and trade.</p>\n</div>\n</div>\n</section>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Afin de pr\u00e9venir la propagation de l'influenza aviaire hautement pathog\u00e8ne (IAHP) au Canada, des restrictions sont impos\u00e9es aux d\u00e9placements \u00e0 destination ou en provenance d'une zone de contr\u00f4le primaire, ou \u00e0 l'int\u00e9rieur de celle-ci.</p>\n<p>Les restrictions impos\u00e9es par d'autres pays varient en fonction des zones de contr\u00f4le. Voir <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/restrictions-imposees-par-des-pays-etrangers/fra/1666817801088/1666817801682\">la liste consolid\u00e9e des restrictions impos\u00e9es par les pays \u00e9trangers</a>.</p>\n<section class=\"gc-srvinfo\">\n<h2>Services et information</h2>\n<div class=\"wb-eqht row\">\n<div class=\"col-md-4\">\n<h3><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/etat-de-reponse-en-cours-aux-detections-d-iahp/zone-de-l-ia/fra/1648851134912/1648851359195\">Carte\u00a0: Zones d'influenza aviaire hautement pathog\u00e8ne</a></h3>\n<p>D\u00e9couvrez si votre r\u00e9gion est touch\u00e9e ou proche d'une zone infect\u00e9e par la plus r\u00e9cente grippe aviaire.</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/etat-de-reponse-en-cours-aux-detections-d-iahp/permis-et-conditions-necessaires/fra/1648871137667/1648871138011\">Permis et conditions n\u00e9cessaires au contr\u00f4le des d\u00e9placements</a></h3>\n<p>Ce qui est n\u00e9cessaire pour le transport d'oiseaux et de sous-produits dans une zone de contr\u00f4le primaire.</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/zcp-spe/fra/1655655067588/1655655068135\">Activit\u00e9s de surveillance dans une zone de contr\u00f4le primaire</a></h3>\n<p>Ce que l'on peut attendre des gouvernements provinciaux et territoriaux, des industries du b\u00e9tail et de la volaille lorsqu'ils m\u00e8nent des activit\u00e9s de surveillance.</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/vers-la-revocation-d-une-zone-de-controle-primaire/fra/1654648457092/1654648457435\">Processus de r\u00e9vocation d'une zone de contr\u00f4le primaire</a></h3>\n<p>Processus d\u00e9crivant les \u00e9tapes \u00e0 suivre pour r\u00e9voquer une zone de contr\u00f4le primaire (\u00e9tablissements infect\u00e9s, zones infect\u00e9es).</p>\n</div>\n<div class=\"col-md-4\">\n<h3><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/etat-de-reponse-en-cours-aux-detections-d-iahp/2022-04-28/fra/1650999759061/1650999759803\">Avis \u00e0 l'industrie : Placement des \u00e9levages dans les zones infect\u00e9es</a></h3>\n<p>Nous nous effor\u00e7ons d'\u00e9liminer la maladie le plus rapidement possible afin de permettre la reprise des activit\u00e9s normales et du commerce.</p>\n</div>\n</div>\n</section>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}