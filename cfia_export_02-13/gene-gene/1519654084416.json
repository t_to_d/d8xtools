{
    "dcr_id": "1519654084416",
    "title": {
        "en": "Colombia\u00a0\u2013 Export requirements for milk and dairy products",
        "fr": "Colombie\u00a0\u2013 Exigences d'exportation pour le lait et les produits laitiers"
    },
    "html_modified": "2024-02-13 9:52:48 AM",
    "modified": "2022-10-19",
    "issued": "2018-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_columbia_1519654084416_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_columbia_1519654084416_fra"
    },
    "ia_id": "1519654084822",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Inspecting and investigating - Performing export certification activities|Exporting food",
        "fr": "Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation|Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Colombia\u00a0\u2013 Export requirements for milk and dairy products",
        "fr": "Colombie\u00a0\u2013 Exigences d'exportation pour le lait et les produits laitiers"
    },
    "label": {
        "en": "Colombia\u00a0\u2013 Export requirements for milk and dairy products",
        "fr": "Colombie\u00a0\u2013 Exigences d'exportation pour le lait et les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1519654084822",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/colombia-milk-and-dairy-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/colombie-lait-et-produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Colombia\u00a0\u2013 Export requirements for milk and dairy products",
            "fr": "Colombie\u00a0\u2013 Exigences d'exportation pour le lait et les produits laitiers"
        },
        "description": {
            "en": "Countries to which exports are currently made \u2013 Colombia.",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Colombie."
        },
        "keywords": {
            "en": "Columbia, Export requirements, milk and dairy products",
            "fr": "Colombie, Exigences d'exportation, lait et produits laitiers"
        },
        "dcterms.subject": {
            "en": "exports,agri-food industry,dairy products",
            "fr": "exportation,industrie agro-alimentaire,produit laitier"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "modified": {
            "en": "2022-10-19",
            "fr": "2022-10-19"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Colombia\u00a0\u2013 Export requirements for milk and dairy products",
        "fr": "Colombie\u00a0\u2013 Exigences d'exportation pour le lait et les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ol>\n<li><a href=\"#a1\">Eligible/ineligible product</a></li>\n<li><a href=\"#a2\">Pre-export approvals by the competent authority of the importing country</a></li>\n<li><a href=\"#a3\">Production controls and inspection requirements</a></li>\n<li><a href=\"#a4\">Labelling, packaging and marking requirements</a></li>\n<li><a href=\"#a5\">Required documents</a></li>\n<li><a href=\"#a6\">Other information</a></li>\n</ol>\n\n\n<h2 id=\"a1\">1. Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<ul>\n<li>All dairy products. However, it is recommended that you work with your importer to confirm this information through the \"Health Information System for the Import and Export of Agriculture and Livestock Products\" (SISPAP) of the <span lang=\"es\">Instituto Colombiano Agropecuarion</span> (ICA).</li>\n</ul>\n\n<h3>Ineligible</h3>\n<ul>\n<li>Information not available</li>\n</ul>\n\n\n<h2 id=\"a2\">2. Pre-export approvals by the competent authority of the importing country</h2>\n\n<h3>Establishments</h3>\n\n<ul>\n<li>The Colombian authorities have been provided a list of Canadian registered establishments</li>\n<li>The exporter must verify with the importer that the product being shipped comes from an establishment on this list</li>\n<li>To amend this list provided to the Columbian authorities the manufacturer must contact the <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">Canadian Food Inspection Agency (CFIA) area office</a> and complete the document following document: \n<ul>\n<li class=\"mrgn-tp-md\">request for addition to the Canadian dairy establishments list sent to third countries</li>\n</ul>\n</li>\n</ul>\n\n\n<h2 id=\"a3\">3. Production controls and inspection requirements</h2>\n\n<p>The inspector must verify during a preventive control inspection that the manufacturer is aware of the standards and requirements of Colombia and has a specific export procedure in place. Dairy products exported to Colombia must bear the registration number of the establishment which is approved by the Colombian authorities.</p>\n\n\n<h2 id=\"a4\">4. Labelling, packaging and marking requirements</h2>\n\n<ul>\n<li>The Colombian government has specific requirements for milk powder (skim milk powder, part skim milk powder and whole milk powder):\n<ul>\n<li class=\"mrgn-tp-md\">commercial name of the milk and its type</li>\n<li>country of origin</li>\n<li>production date and/or batch production number</li>\n<li>date of expiration, which has to be greater than 12 months after the time it arrives in Colombia</li>\n<li>storage recommendations</li>\n<li>gross and net contents expressed in grams or kilograms</li>\n<li>production date or batch number and expiry date must be printed on the original packaging in Canada. The use of stickers is prohibited.</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a5\">5. Documentation requirements</h2>\n\n<h3>Certificate</h3>\n\n<ul>\n<li>Health certificate for export of milk and dairy products from Canada to Colombia.<br>\n</li>\n</ul>\n\n<h2 id=\"a6\">6. Other information</h2>\n\n<p>Exported products transiting through a country may require transit documentation. It is the responsibility of the exporter to ensure that the shipment will be accompanied by all necessary certificates. Please work closely with your importer.</p>\n\n<p>Samples (personal or commercial) may be subject to the same requirements as a regular shipment.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<h2>Sur cette page</h2>\n\n<ol>\n<li><a href=\"#a1\">Produits admissibles/non admissibles</a></li>\n<li><a href=\"#a2\">Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</a></li>\n<li><a href=\"#a3\">Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</a></li>\n<li><a href=\"#a4\">Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</a></li>\n<li><a href=\"#a5\">Documents requis</a></li>\n<li><a href=\"#a6\">Autres renseignements</a></li>\n</ol>\n\n<h2 id=\"a1\">1. Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<ul>\n<li>Tous les produits laitiers. Cependant il est recommand\u00e9 de travailler avec son importateur afin de confirmer cette information par le biais du \"Syst\u00e8me d'information sanitaire pour l'importation et l'exportation de produits agricoles et d'\u00e9levage\" (SISPAP) de l'<span lang=\"es\">Instituto Colombiano Agropecuarion</span> (ICA).</li>\n</ul>\n\n<h3>Non admissibles</h3>\n\n<ul>\n<li>Information non disponible</li>\n</ul>\n\n<h2 id=\"a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<h3>\u00c9tablissements</h3>\n\n<ul>\n<li>Les autorit\u00e9s colombiennes ont la liste des \u00e9tablissements enregistr\u00e9s f\u00e9d\u00e9ral au Canada</li>\n<li>L'exportateur doit v\u00e9rifier avec l'importateur que le produit qui sera export\u00e9 provient d'un \u00e9tablissement qui est sur cette liste.</li>\n<li>Pour modifier cette liste fournie aux autorit\u00e9s colombiennes, le fabricant doit communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'Agence canadienne d'inspection des aliments (ACIA)</a> et compl\u00e9ter le document suivant\u00a0:\n<ul>\n<li class=\"mrgn-tp-md\">Demande d'ajout \u00e0 la liste des \u00e9tablissements laitiers canadiens envoy\u00e9e \u00e0 des pays tiers</li>\n</ul>\n</li>\n</ul>\n\n\n<h2 id=\"a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<p>L'inspecteur doit v\u00e9rifier au cours d'une inspection de contr\u00f4le pr\u00e9ventif que le fabricant est au courant des normes et exigences de la Colombie et qu'une proc\u00e9dure d'exportation sp\u00e9cifique est en place. Les produits laitiers export\u00e9s vers la Colombie doivent porter le num\u00e9ro d'agr\u00e9ment de l'\u00e9tablissement qui est approuv\u00e9 par les autorit\u00e9s colombiennes.</p>\n\n\n<h2 id=\"a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</h2>\n\n<ul>\n<li>Les autorit\u00e9s colombiennes ont des exigences particuli\u00e8res pour le lait en poudre (lait \u00e9cr\u00e9m\u00e9, partiellement \u00e9cr\u00e9m\u00e9 et lait en poudre entier)\u00a0:\n<ul>\n<li class=\"mrgn-tp-md\">Le nom commercial du lait et le type de lait</li>\n<li>Le pays d'origine</li>\n<li>La date de production et/ou le code de production</li>\n<li>La date d'expiration, qui doit \u00eatre de 12 mois ou plus au moment de l'entr\u00e9e du produit en Colombie</li>\n<li>Les instructions d'entreposage</li>\n<li>Le contenue brut et net exprim\u00e9 en gramme ou kilogramme</li>\n<li>La date de production ou le code de production ainsi que la date d'expiration doivent \u00eatre imprim\u00e9s sur l'emballage original au Canada. Les autocollants sont interdits</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a5\">5. Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Certificat sanitaire pour l'exportation de lait et de produits laitiers du Canada vers la Colombie.<br>\n</li>\n</ul>\n\n\n<h2 id=\"a6\">6. Autres renseignements</h2>\n\n<p>Des produits export\u00e9s transitant par un pays pourraient n\u00e9cessiter aussi des documents de transit. C'est la responsabilit\u00e9 de l'exportateur de s'assurer que son exp\u00e9dition sera accompagn\u00e9e de tous les certificats n\u00e9cessaires. Veuillez travailler en \u00e9troite collaboration avec votre importateur.</p>\n\n<p>Les \u00e9chantillons (personnels ou commerciaux) pourraient \u00eatre soumis aux m\u00eames exigences qu'une exp\u00e9dition r\u00e9guli\u00e8re.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}