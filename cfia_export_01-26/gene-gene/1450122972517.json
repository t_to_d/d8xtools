{
    "dcr_id": "1450122972517",
    "title": {
        "en": "Aquatic animal domestic movements",
        "fr": "D\u00e9placements d'animaux aquatiques en territoire canadien"
    },
    "html_modified": "2024-01-26 2:23:04 PM",
    "modified": "2022-03-17",
    "issued": "2015-12-31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anim_aqua_dmc_prog_1450122972517_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/anim_aqua_dmc_prog_1450122972517_fra"
    },
    "ia_id": "1450122973466",
    "parent_ia_id": "1320536294234",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1320536294234",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal domestic movements",
        "fr": "D\u00e9placements d'animaux aquatiques en territoire canadien"
    },
    "label": {
        "en": "Aquatic animal domestic movements",
        "fr": "D\u00e9placements d'animaux aquatiques en territoire canadien"
    },
    "templatetype": "content page 1 column",
    "node_id": "1450122973466",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299155892122",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/domestic-movements/",
        "fr": "/sante-des-animaux/animaux-aquatiques/territoire-canadien/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal domestic movements",
            "fr": "D\u00e9placements d'animaux aquatiques en territoire canadien"
        },
        "description": {
            "en": "Whether a permit is required depends on the declarations of the reportable disease status of the provinces, territories, marine waters of Canada and the specific businesses (compartments) within Canada.",
            "fr": "La bios\u00e9curit\u00e9 consiste \u00e0 prendre des pr\u00e9cautions afin de r\u00e9duire les risques d'introduction et de propagation d'organismes infectieux au sein des populations et entre elles."
        },
        "keywords": {
            "en": "aquatic animal health, National Aquatic Animal Health Program, NAAHP, biosecurity, diseases, facility",
            "fr": "associations de producteurs, production aquicole, propagation de maladies, L ACIA, bios&#233;curit&#233; efficace"
        },
        "dcterms.subject": {
            "en": "inspection,veterinary medicine,animal health",
            "fr": "inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-12-31",
            "fr": "2015-12-31"
        },
        "modified": {
            "en": "2022-03-17",
            "fr": "2022-03-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal domestic movements",
        "fr": "D\u00e9placements d'animaux aquatiques en territoire canadien"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA):</p>\n<ul>\n<li>safeguards Canada's aquatic animal resources</li>\n<li>implements controls, such as permits, to contain certain <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/federally-reportable-aquatic-animal-diseases/eng/1339174937153/1339175227861\">aquatic animal reportable diseases</a> within areas of Canada where they are known to occur</li>\n</ul>\n<p>Whether a permit is required depends on the declarations of the reportable disease status of the provinces, territories, marine waters of Canada and the specific businesses (compartments) within Canada. </p>\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> declarations of reportable disease status</a></li>\n</ul>\n<p>Each declaration lists the aquatic animal reportable disease to be controlled, as well as the following situations that require a permit:</p>\n<ul>\n<li>species of aquatic animals (finfish or mollusc)</li>\n<li>description of the things</li>\n<li>the end uses for the moved aquatic animal or thing</li>\n</ul>\n<h2>Domestic movement permits</h2>\n<p>A <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> movement permit is required if the declared aquatic animal or thing is moving from:</p>\n<ul>\n<li>a declared infected area to a declared buffer area, provisionally-free area or free area</li>\n<li>a declared buffer area to another declared buffer area, provisionally-free area or free area</li>\n<li>a declared provisionally-free area to a declared free area</li>\n</ul>\n<p>Commercial harvesters of wild molluscs will not be issued a permit to move susceptible species of molluscs out of an infected area for the purpose of <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl9\">depuration</a>, <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl10\">relay</a> (including <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl11\">salting up</a> and <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl12\">short term container relaying</a>), <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl13\">dry storage</a>, <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl14\">wet storage</a> or <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315#dl15\">processing commercial</a>. These movements are only allowed to occur within the infected area or to another infected area for the same diseases. Please note that all molluscs to be harvested for human consumption are subject to the <a href=\"/preventive-controls/fish/cssp/eng/1563470078092/1563470123546\">Canadian Shellfish Sanitation Program</a>.</p>\n<p>A <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> movement permit is not required if:</p>\n<ul>\n<li>the aquatic animal is a crustacean</li>\n<li>the animal, thing or end use is not listed on the declarations</li>\n<li>the movement will occur within the same declared area as where the animals or thing are located</li>\n<li>the movement is from a free area to any other area</li>\n<li>the movement is from a provisionally-free area to a buffer or infected area</li>\n<li>the movement is from a buffer area to an infected area</li>\n<li>the movement is from an infected area to another infected area</li>\n</ul>\n<p>The movement permit is either issued to whoever is sending or to whoever is receiving the aquatic animals or things.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> movement permit contains conditions that control disease risks associated with the aquatic animal or thing to be shipped or received, and the water, ice, or other medium, materials, equipment and conveyances used during the shipment.</p>\n<ul>\n<li>If the permit is issued to whoever is sending live or dead aquatic animals, the animals or their carcasses must be segregated, inspected and tested for the declared reportable diseases prior to the movement</li>\n<li>If the permit is issued to whoever is sending a thing, the thing will require decontamination and segregation prior to the movement</li>\n<li>If the permit is issued to whoever is receiving aquatic animals or things, their facility must be a Closed Premises, meaning that there are measures in place to prevent the spread of aquatic animal reportable diseases from the facility. Closed Premises may be inspected prior to receiving the aquatic animals or things to be moved</li>\n</ul>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is not currently charging a fee for issuing a movement permit. No fees will be charged for inspection and testing at this time. Fees for permit issuance, inspection and testing may be implemented in the future.</p>\n<h2>Application forms</h2>\n<p>The following application forms are available:</p>\n<ul>\n<li>To move finfish or declared things, complete the <a href=\"/about-cfia/find-a-form/form-cfia-acia-5743/eng/1410362945783/1410362946643\">Domestic Movement Permit Application to Move Finfish and/or Things within Canada (CFIA/ACIA 5743)</a></li>\n<li>To move molluscs or declared things, complete the <a href=\"/about-cfia/find-a-form/form-cfia-acia-5758/eng/1422645511277/1452537811113\">Domestic Movement Permit Application to Move Molluscs and/or Things within Canada (CFIA/ACIA 5758)</a></li>\n<li>To receive finfish, molluscs or declared things into a biosecure laboratory, research facility, culture facility, cryopreservation/storage facility, or net-cleaning facility, complete the <a href=\"/about-cfia/find-a-form/form-cfia-acia-5749/eng/1417633025090/1417633025996\">Domestic Movement Permit Application for Closed Facilities Within Canada Receiving Aquatic Animals and/or Things From Infected, Buffer or Provisionally Free Areas (CFIA/ACIA 5749)</a></li>\n</ul>\n<p>Prior to completing the application, the applicant should perform the following:</p>\n<ul>\n<li>review the <a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315\">declarations</a> or contact the <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a> or the <a href=\"/about-cfia/permits-licences-and-approvals/centre-of-administration-for-permissions/eng/1395348583779/1395348638922\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> National Centre of Permissions</a> to ensure that a permit is required for the aquatic animal or thing to be moved. Additional information, including the permit conditions, is also available in the industry guidance documents for <a href=\"/animal-health/aquatic-animals/diseases/laboratories/eng/1450414412433/1450414413308\">laboratories</a>, <a href=\"/animal-health/aquatic-animals/diseases/researchers/eng/1450450639150/1450450639931\">researchers</a>, <a href=\"/animal-health/aquatic-animals/diseases/growers-of-finfish/eng/1450464755067/1450464755835\">finfish culturists</a> and <a href=\"/animal-health/aquatic-animals/diseases/growers-of-molluscs/eng/1450705820843/1450705821906\">mollusc culturists</a></li>\n<li>investigate whether authorization or permission is required for the same movement from another federal department or a provincial or territorial ministry. Contact information for Introduction and Transfer Committees is available in the industry guidance documents</li>\n</ul>\n<p>The completed application for a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> movement permit should be sent by email, facsimile or regular mail to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> National Centre of Permissions. The relevant contact information is available on the application form.</p>\n<p>Please sign up to <a href=\"https://notification.inspection.canada.ca\">receive important updates</a> on this program via email.</p>\n<h2>Compartments</h2>\n<p>An aquatic animal business may choose to become a compartment recognized by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> if it is:</p>\n<ul>\n<li>located in a declared buffer or infected area for one or more of the reportable aquatic animal diseases AND</li>\n<li>regularly moving any of the listed aquatic animals or things to other declared buffer, provisionally-free or free areas</li>\n</ul>\n<p>Once recognized, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will declare the location of the aquatic animals or things as a free area, allowing movements from that facility without a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> movement permit.</p>\n<p>The <a href=\"/about-cfia/find-a-form/form-cfia-acia-5748/eng/1416315920388/1416315922263\">Application for Recognition as a Compartment Under the Domestic Movement Control Program (CFIA/ACIA 5748)</a> is completed and sent to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> National Centre of Permissions. </p>\n<h2>Information for the aquatic animal industry:</h2>\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/eng/1299156296625/1320599059508\">Aquatic Animal Diseases</a></li>\n<li><a href=\"/animal-health/aquatic-animals/imports/pathogens/facilities/eng/1377962925061/1377963021283\">Containment Standards for Facilities Handling Aquatic Animal Pathogens</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/declarations/eng/1450126558469/1450126559315\">Declarations</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/laboratories/eng/1450414412433/1450414413308\">Industry Guidance for Laboratories</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/researchers/eng/1450450639150/1450450639931\">Industry Guidance for Researchers</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/growers-of-finfish/eng/1450464755067/1450464755835\">Industry Guidance for Growers of Finfish</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/growers-of-molluscs/eng/1450705820843/1450705821906\">Industry Guidance for Growers of Molluscs</a></li>\n<li>Industry Guidance on Development of a Preventive Control Plan for a Compartment Recognized as a Free Area (contact the closest <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a> for a copy)</li>\n<li>National Standards for Compartment Recognition (contact the closest <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area Office</a> for a copy)</li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA)\u00a0:</p>\n<ul>\n<li>prot\u00e8ge les ressources animali\u00e8res aquatiques du pays;</li>\n<li>met en place des mesures, telles que des permis, pour contenir certaines <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/maladies-des-animaux-aquatiques-a-declaration-obli/fra/1339174937153/1339175227861\">maladies d\u00e9clarables des animaux aquatiques</a> dans certaines r\u00e9gions du Canada o\u00f9 elles sont pr\u00e9sentes.</li>\n</ul>\n<p>L'obligation d'obtenir un permis d\u00e9pend des d\u00e9clarations du statut de maladie d\u00e9clarable des provinces, des territoires, des eaux maritimes du Canada et des entreprises particuli\u00e8res (compartiments) au Canada. </p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315\">Les d\u00e9clarations de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> sur les maladies \u00e0 d\u00e9claration obligatoire</a></li>\n</ul>\n<p>Chaque d\u00e9claration r\u00e9pertorie les maladies d\u00e9clarables pour les animaux aquatiques \u00e0 \u00eatre contr\u00f4l\u00e9es, ainsi que les cas suivants qui exigent l'obtention d'un permis\u00a0:</p>\n<ul>\n<li>esp\u00e8ces d'animaux aquatiques (poissons ou mollusques);</li>\n<li>description des choses;</li>\n<li>utilisations finales de l'animal aquatique ou de la chose d\u00e9plac\u00e9e.</li>\n</ul>\n<h2>Permis de d\u00e9placements en territoire canadien</h2>\n<p>Un permis de d\u00e9placement d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> est requis si l'animal aquatique ou la chose est d\u00e9plac\u00e9e\u00a0:</p>\n<ul>\n<li>d'une zone d\u00e9clar\u00e9e contamin\u00e9e vers une zone d\u00e9clar\u00e9e tampon, temporairement exempte ou exempte;</li>\n<li>d'une zone d\u00e9clar\u00e9e tampon vers une autre zone d\u00e9clar\u00e9e tampon, temporairement exempte ou exempte;</li>\n<li>d'une zone d\u00e9clar\u00e9e temporairement exempte vers une zone d\u00e9clar\u00e9e exempte.</li>\n</ul>\n<p>Les p\u00eacheurs professionnels de mollusques sauvages ne recevront pas de permis pour d\u00e9placer les esp\u00e8ces vuln\u00e9rables de mollusques hors d'une zone d\u00e9clar\u00e9e contamin\u00e9e aux fins de <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl9\">d\u00e9puration</a>, <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl20\">reparcage</a> (y compris le <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl17\">resalage</a> et le <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl12\">reparcage \u00e0 court terme dans un conteneur</a>), l'<a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl13\">entreposage \u00e0 sec</a>, l'<a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl14\">entreposage humide</a> ou la <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315#dl15\">transformation commerciale</a>. Ces d\u00e9placements ne sont permis qu'au sein de la zone contamin\u00e9e ou vers une autre zone contamin\u00e9e avec les m\u00eames maladies. Veuillez noter que tous les mollusques pr\u00e9lev\u00e9s pour l'alimentation humaine sont soumis au <a href=\"/controles-preventifs/poisson/pccsm/fra/1563470078092/1563470123546\">Programme canadien de contr\u00f4le de la salubrit\u00e9 des mollusques</a>.</p>\n<p>Un permis de d\u00e9placement d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> n'est pas requis si\u00a0:</p>\n<ul>\n<li>l'animal aquatique est un crustac\u00e9;</li>\n<li>l'animal, la chose ou son utilisation finale ne sont pas r\u00e9pertori\u00e9s par les d\u00e9clarations;</li>\n<li>le d\u00e9placement se fait dans la m\u00eame zone d\u00e9clar\u00e9e o\u00f9 se trouvent les animaux ou choses;</li>\n<li>le d\u00e9placement se fait d'une zone exempte vers n'importe quelle autre zone;</li>\n<li>le d\u00e9placement se fait d'une zone temporairement exempte vers une zone tampon ou contamin\u00e9e;</li>\n<li>le d\u00e9placement se fait d'une zone tampon vers une zone contamin\u00e9e;</li>\n<li>le d\u00e9placement se fait d'une zone contamin\u00e9e vers une autre zone contamin\u00e9e.</li>\n</ul>\n<p>Le permis de d\u00e9placement est d\u00e9livr\u00e9 soit \u00e0 la personne qui exp\u00e9die animaux aquatiques ou les choses, soit \u00e0 celle qui les re\u00e7oit.</p>\n<p>Le permis de d\u00e9placement d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> comporte des conditions qui permettent de contr\u00f4ler les risques de maladies infectieuses associ\u00e9s aux animaux aquatiques ou aux choses \u00e0 exp\u00e9dier ou \u00e0 recevoir, et l'eau, la glace ou tout autre milieu, mat\u00e9riau, \u00e9quipement ou moyen de transport utilis\u00e9s pendant l'exp\u00e9dition.</p>\n<ul>\n<li>Si le permis est d\u00e9livr\u00e9 \u00e0 la personne qui exp\u00e9die les animaux aquatiques, vivants ou morts, les animaux ou leur carcasse doivent \u00eatre mis \u00e0 l'\u00e9cart, inspect\u00e9s et contr\u00f4l\u00e9s pour d\u00e9celer les maladies \u00e0 d\u00e9claration obligatoire d\u00e9sign\u00e9es avant le d\u00e9placement.</li>\n<li>Si le permis est d\u00e9livr\u00e9 \u00e0 la personne qui exp\u00e9die une chose, la chose doit \u00eatre d\u00e9contamin\u00e9e et mise \u00e0 l'\u00e9cart avant le d\u00e9placement.</li>\n<li>Si le permis est d\u00e9livr\u00e9 \u00e0 la personne qui re\u00e7oit les animaux aquatiques ou choses, son site doit \u00eatre de type ferm\u00e9, ce qui signifie que des mesures sont en place pour pr\u00e9venir la propagation des maladies \u00e0 d\u00e9claration obligatoire d'animaux aquatiques depuis l'installation. Les sites ferm\u00e9s peuvent faire l'objet d'une inspection avant la r\u00e9ception des animaux aquatiques ou choses \u00e0 d\u00e9placer.</li>\n</ul>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> n'impute pour le moment aucuns frais pour l'\u00e9mission d'un permis de d\u00e9placement. Aucuns frais ne s'appliquent pour le moment aux inspections et aux tests. Des frais pour l'\u00e9mission de permis, les inspections et les tests pourront \u00eatre appliqu\u00e9s \u00e0 l'avenir.</p>\n<h2>Formulaires de demande</h2>\n<p>Les formulaires de demande suivants sont offerts\u00a0:</p>\n<ul>\n<li>Pour d\u00e9placer des poissons ou des choses d\u00e9clar\u00e9es, remplir la <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5743/fra/1410362945783/1410362946643\">Demande de permis pour d\u00e9placer des poissons et/ou des choses \u00e0 l'int\u00e9rieur du Canada (CFIA/ACIA 5743)</a>.</li>\n<li>Pour d\u00e9placer des mollusques ou des choses d\u00e9clar\u00e9es, remplir la <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5758/fra/1422645511277/1452537811113\">Demande de permis pour d\u00e9placer des mollusques et/ou des choses \u00e0 l'int\u00e9rieur du Canada (CFIA/ACIA 5758)</a>.</li>\n<li>Pour recevoir des poissons, des mollusques ou des choses d\u00e9clar\u00e9es dans un laboratoire bios\u00e9curitaire, un \u00e9tablissement de recherche bios\u00e9curitaire, une installation d'\u00e9levage bios\u00e9curitaire, une installation de cryoconservation ou de conservation bios\u00e9curitaire, ou une installation de nettoyage de filets bios\u00e9curitaire, remplir la <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5749/fra/1417633025090/1417633025996\">Demande de permis pour les \u00e9tablissements ferm\u00e9s canadiens qui re\u00e7oivent des animaux aquatiques ou des choses de zones d\u00e9clar\u00e9es comme zone contamin\u00e9e, zone tampon, zone temporairement exempt ou zone exempte de maladie (CFIA/ACIA 5749)</a>.</li>\n</ul>\n<p>Avant de remplir le formulaire de demande, le demandeur devrait\u00a0:</p>\n<ul>\n<li>Examiner les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315\">d\u00e9clarations</a> ou communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">centre op\u00e9rationnel de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou le <a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/centre-d-administration-pour-les-permissions/fra/1395348583779/1395348638922\">Centre national des permissions de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> pour s'assurer qu'un permis est n\u00e9cessaire pour d\u00e9placer l'animal aquatique ou la chose. Consultez les lignes directrices pour l'industrie pour obtenir de plus amples renseignements, dont les conditions associ\u00e9es aux permis, pour les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/laboratoires/fra/1450414412433/1450414413308\">laboratoires</a>, les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/chercheurs/fra/1450450639150/1450450639931\">chercheurs</a>, les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/intention-des-pisciculteurs/fra/1450464755067/1450464755835\">pisciculteurs</a> et les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/intention-des-conchyliculteurs/fra/1450705820843/1450705821906\">conchyliculteurs</a>.</li>\n<li>Chercher \u00e0 savoir si une autorisation ou une permission est exig\u00e9e pour le m\u00eame d\u00e9placement par un autre minist\u00e8re f\u00e9d\u00e9ral ou un minist\u00e8re provincial ou territorial. Les coordonn\u00e9es pour les comit\u00e9s des introductions et des transferts se trouvent dans les lignes directrices pour l'industrie.</li>\n</ul>\n<p>La demande remplie pour l'obtention d'un permis de d\u00e9placement d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> devrait \u00eatre achemin\u00e9e par courriel, par t\u00e9l\u00e9copie ou par courrier ordinaire au Centre national des permissions de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Les coordonn\u00e9es pertinentes se trouvent dans le formulaire de demande.</p>\n<p>Inscrivez-vous pour <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">recevoir des mises \u00e0 jour importantes</a> sur ce programme par courriel.</p>\n<h2>Compartiments</h2>\n<p>Les entreprises d'animaux aquatiques peuvent choisir de devenir un compartiment reconnu par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> si\u00a0:</p>\n<ul>\n<li>elles sont situ\u00e9es dans une zone d\u00e9clar\u00e9e tampon ou contamin\u00e9e pour une ou plusieurs maladies \u00e0 d\u00e9claration obligatoire des animaux aquatiques;</li>\n<li>elles d\u00e9placent r\u00e9guli\u00e8rement n'importe lesquels des choses ou animaux aquatiques r\u00e9pertori\u00e9s vers d'autres zones d\u00e9clar\u00e9es exemptes ou provisoirement exemptes de maladies.</li>\n</ul>\n<p>Une fois le compartiment reconnu, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d\u00e9clare l'emplacement des choses ou animaux aquatiques comme zone exempte, ce qui autorise les d\u00e9placements depuis cette installation sans permis de d\u00e9placement d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>La <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5748/fra/1416315920388/1416315922263\">Demande de reconnaissance en tant que compartiment en vertu du programme de contr\u00f4le des d\u00e9placements en territoire canadien (CFIA/ACIA 5748)</a> doit \u00eatre remplie et achemin\u00e9e au Centre national des permissions de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<h2>Renseignements pour l'industrie des animaux aquatiques\u00a0:</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/declarations/fra/1450126558469/1450126559315\">D\u00e9clarations</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/laboratoires/fra/1450414412433/1450414413308\">Lignes directrices de l'industrie \u00e0 l'intention des laboratoires</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/chercheurs/fra/1450450639150/1450450639931\">Lignes directrices de l'industrie \u00e0 l'intention des chercheurs</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/intention-des-pisciculteurs/fra/1450464755067/1450464755835\">Lignes directrices de l'industrie \u00e0 l'intention des pisciculteurs</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/intention-des-conchyliculteurs/fra/1450705820843/1450705821906\">Lignes directrices de l'industrie \u00e0 l'intention des conchyliculteurs</a></li>\n<li>Lignes directrices pour l'industrie sur l'\u00e9laboration d'un plan de contr\u00f4le pr\u00e9ventif pour un compartiment d\u00e9sign\u00e9 comme zone exempte de maladie (communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">centre op\u00e9rationnel de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> le plus pr\u00e8s pour en obtenir un exemplaire)</li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/fra/1299156296625/1320599059508\">Maladies d'animaux aquatiques</a></li>\n<li>Normes nationales pour la reconnaissance d'un compartiment (communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">centre op\u00e9rationnel de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> le plus pr\u00e8s pour en obtenir un exemplaire)</li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/importation/pathogenes/installations/fra/1377962925061/1377963021283\">Normes relatives au confinement des installations manipulant des agents pathog\u00e8nes aquatiques</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}