{
    "dcr_id": "1646102406709",
    "title": {
        "en": "Fertilizer or supplement registration: Eligibility requirements",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "html_modified": "2024-03-12 3:59:13 PM",
    "modified": "2022-08-09",
    "issued": "2022-08-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-eligibility_reqs_1646102406709_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/fert_or_supp_registration-eligibility_reqs_1646102406709_fra"
    },
    "ia_id": "1646102407100",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fertilizer or supplement registration: Eligibility requirements",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "label": {
        "en": "Fertilizer or supplement registration: Eligibility requirements",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646102407100",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/eligibility-requirements/",
        "fr": "/protection-des-vegetaux/engrais/conditions-d-eligibilite/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fertilizer or supplement registration: Eligibility requirements",
            "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Conditions d'\u00e9ligibilit\u00e9"
        },
        "description": {
            "en": "All fertilizers and supplements imported into or sold in Canada require registration unless they meet 1 or more of the exemptions provided for in the Fertilizers Regulations.",
            "fr": "Tous les engrais et les suppl\u00e9ments qui sont import\u00e9s ou vendus au Canada doivent \u00eatre enregistr\u00e9s \u00e0 moins qu'ils r\u00e9pondent \u00e0 un ou plusieurs crit\u00e8res d'exemptions pr\u00e9vus dans le R\u00e8glement sur les engrais."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizer regulations, Fertilizer, supplement, registration, Eligibility requirements, new",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, Enregistrement, engrais, suppl\u00e9ment,\u00a0Conditions d'\u00e9ligibilit\u00e9, nouveau"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy",
            "fr": "cultures,engrais,inspection,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "modified": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fertilizer or supplement registration: Eligibility requirements",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment\u00a0: Conditions d'\u00e9ligibilit\u00e9"
    },
    "body": {
        "en": "        \r\n        \n\n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/overview/eng/1646095692928/1646095693725\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/before-you-apply/eng/1646274247575/1646274247919\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/how-to-apply/eng/1646282540949/1646282541291\">4. How to apply</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/after-you-apply/eng/1646347538968/1646347539296\">5. After you apply</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646359107928/1646359108272\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n\n<p class=\"mrgn-tp-0\">All fertilizers and supplements imported into or sold in Canada require registration unless they meet\u00a0one\u00a0or more of the exemptions provided for in the <i>Fertilizers Regulations</i>.</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n\n<header class=\"panel-heading\">\n\n<h2 class=\"panel-title\">Additional information</h2>\n</header>\n<div class=\"panel-body\">\n\n<p>Consult the <a href=\"/eng/1330932243713/1330933201778#a1\">fertilizer and supplement registration trigger flow-charts</a> to help determine if your product is exempt from registration.</p>\n\n<p>If you still aren't sure if your product is exempt from registration after consulting the flowcharts, you can <a href=\"/plant-health/fertilizers/overview/product-specific-inquiry/eng/1320385315947/1320385808224\">submit an inquiry</a> (IQ) to the <a href=\"/about-the-cfia/permits-licences-and-approvals/paso/eng/1403831598660/1403831599519\">Pre-market Application Submissions Office</a> (PASO).</p>\n\n</div>\n</section>\n\n</div>\n\n<h2>Exemptions</h2>\n\n<p><strong>Fertilizers exempt from registration (except seeds or growing media):</strong></p>\n\n<ul>\n<li>a fertilizer that <strong>doesn't</strong> contain:\n<ul>\n<li>a substance produced by or derived from a living organism</li>\n<li>a pesticide</li>\n<li>a supplement that isn't registered or set out in the <a href=\"/plant-health/fertilizers/regulatory-modernization/list-of-primary-fertilizer-and-supplement-material/eng/1533213691120/1533213691867\">list of primary fertilizer and supplement materials</a></li>\n<li>a registered supplement, if the directions for use of the product aren't consistent with those of the registered supplement</li>\n<li>a micronutrient fertilizer that isn't registered</li>\n<li>a registered micronutrient fertilizer, if the directions for use of the product aren't consistent with those of the registered micronutrient fertilizer</li>\n\n</ul>\n</li>\n\n<li>a fertilizer that is set out in the list of materials</li>\n<li>a customer-formula fertilizer</li>\n<li>a fertilizer whose active ingredient consists solely of a mixture of fertilizers or fertilizers and supplements, if those fertilizers and supplements are either:\n<ul>\n<li>exempt from registration</li>\n<li>registered for the proposed use of the mixture</li>\n</ul>\n</li>\n</ul>\n\n<p><strong>Supplements exempt from registration (except seeds or growing media):</strong></p>\n\n<ul>\n<li>a supplement set out in the list of materials</li>\n<li>a supplement whose active ingredients consist solely of supplements, if:\n<ul>\n<li>each supplement in the mixture is exempt from registration or is registered for the proposed use of the mixture</li>\n<li>in the case of a mixture containing\u00a0one\u00a0or more viable microorganisms as active ingredients, the mixture isn't further cultured or manipulated</li>\n</ul>\n</li>\n</ul>\n\n<p><strong>Seed and growing media exempt from registration:</strong></p>\n\n<p>Seeds that are treated with fertilizers, supplements, or both are exempt from registration if each of those fertilizers or supplements is either:</p>\n\n<ul>\n<li>exempt from registration</li>\n<li>registered for that specific use</li>\n</ul>\n\n<p>A growing medium that contains fertilizers or supplements or both is exempt from registration if each of those fertilizers or supplements is:</p>\n\n<ul>\n<li>either exempt from registration or registered for that use</li>\n<li>consistent with the direction for use of that media</li>\n</ul>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/plant-health/fertilizers/overview/eng/1646095692928/1646095693725\" rel=\"prev\">Previous</a></li>\n\n<li class=\"next\"><a href=\"/plant-health/fertilizers/before-you-apply/eng/1646274247575/1646274247919\" rel=\"next\">Next</a></li>\n\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apercu/fra/1646095692928/1646095693725\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646274247575/1646274247919\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646282540949/1646282541291\">4. Comment pr\u00e9senter une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apres-avoir-presente-une-demande/fra/1646347538968/1646347539296\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646359107928/1646359108272\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\n\n<p class=\"mrgn-tp-0\">Tous les engrais et les suppl\u00e9ments qui sont import\u00e9s ou vendus au Canada doivent \u00eatre enregistr\u00e9s \u00e0 <strong>moins qu'</strong>ils r\u00e9pondent \u00e0 une ou plusieurs des exemptions pr\u00e9vues par le <i>R\u00e8glement sur les engrais</i>.</p>\n\n\n<div class=\"col-sm-4 pull-right mrgn-tp-lg mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n\n<header class=\"panel-heading\">\n\n<h2 class=\"panel-title\">Informations suppl\u00e9mentaires</h2>\n</header>\n<div class=\"panel-body\">\n\n<p>Veuillez consulter les organigrammes \u00e0 la page <a href=\"/fra/1330932243713/1330933201778#a1\">D\u00e9clencheurs de l'enregistrement pour les engrais et les suppl\u00e9ments en vertu de la <i>Loi sur les engrais</i></a> pour vous aider \u00e0 d\u00e9terminer si votre produit est exempt de l'enregistrement.</p>\n\n<p>Si apr\u00e8s avoir consult\u00e9 l'organigramme, vous n'\u00eates pas encore certain si un produit est exempt de l'enregistrement, vous pouvez pr\u00e9senter une <a href=\"/protection-des-vegetaux/engrais/apercu/demande-d-information-sur-un-produit-specifique/fra/1320385315947/1320385808224\">demande d'information</a> au <a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/bpdpm/fra/1403831598660/1403831599519\">Bureau de pr\u00e9sentation de demandes pr\u00e9alables \u00e0 la mise en march\u00e9</a> (BPDPM).</p>\n\n</div>\n</section>\n\n</div>\n\n<h2>Exemptions</h2>\n\n<p><strong>Engrais exempts d'enregistrement (sauf les semences ou les milieux de culture)\u00a0:</strong></p>\n\n<ul>\n<li>un engrais qui <strong>ne</strong> contient <strong>pas\u00a0:</strong>\n<ul>\n<li>une substance produite par un organisme vivant ou issue d'un tel organisme</li>\n<li>un antiparasitaire</li>\n<li>un suppl\u00e9ment qui n'est pas enregistr\u00e9 ou qui ne figure pas dans la <a href=\"/protection-des-vegetaux/engrais/modernisation-de-la-reglementation/liste-des-composants-de-base-des-engrais-et-supple/fra/1533213691120/1533213691867\">liste des composants de base des engrais et suppl\u00e9ments</a></li>\n<li>un suppl\u00e9ment enregistr\u00e9 si le mode d'emploi du produit n'est pas conforme \u00e0 celui du suppl\u00e9ment enregistr\u00e9\n</li>\n</ul>\n</li>\n<ul>\n<li>un engrais d'oligo-\u00e9l\u00e9ment qui n'est pas enregistr\u00e9</li>\n<li>un engrais d'oligo-\u00e9l\u00e9ment enregistr\u00e9, si le mode d'emploi du produit n'est pas conforme \u00e0 celui de l'engrais d'oligo-\u00e9l\u00e9ment enregistr\u00e9.</li>\n</ul>\n<li>tout engrais figurant sur la liste des composants</li>\n<li>tout engrais pr\u00e9par\u00e9 selon la formule du client</li>\n<li>tout engrais dont les composants actifs consistent seulement en un m\u00e9lange soit d'engrais, soit d'engrais et de suppl\u00e9ments, si ces engrais et suppl\u00e9ments sont soit\u00a0:\n<ul>\n<li>exempt\u00e9s de l'enregistrement</li>\n<li>enregistr\u00e9s pour l'usage propos\u00e9 du m\u00e9lange</li>\n</ul>\n</li>\n</ul>\n\n<p><strong>Suppl\u00e9ments exempts d'enregistrement (sauf les semences ou les milieux de culture)\u00a0:</strong></p>\n\n<ul>\n\n<li>tout suppl\u00e9ment figurant sur la liste des composants</li>\n<li>tout suppl\u00e9ment dont les composants actifs consistent seulement de suppl\u00e9ments, si\n<ul>\n<li>chaque suppl\u00e9ment dans le m\u00e9lange est exempt\u00e9 de l'enregistrement ou est enregistr\u00e9 pour l'usage propos\u00e9 du m\u00e9lange</li>\n<li>dans le cas o\u00f9 le m\u00e9lange contient un ou plusieurs composants actifs qui sont des micro-organismes viables, il n'est pas de nouveau mis en culture ou manipul\u00e9</li>\n</ul>\n</li>\n\n</ul>\n\n<p><strong>Semences et milieux de culture exempts d'enregistrement\u00a0:</strong></p>\n\n<p>Les semences trait\u00e9es avec un engrais, ou un suppl\u00e9ment, ou les deux, sont exempt\u00e9es de l'enregistrement si chaque engrais ou suppl\u00e9ments utilis\u00e9 est soit\u00a0:</p>\n\n<ul>\n\n<li>exempt\u00e9 de l'enregistrement</li>\n<li>enregistr\u00e9 pour cet usage sp\u00e9cifique</li>\n</ul>\n\n<p>Les milieux de culture qui contiennent un engrais ou un suppl\u00e9ment, ou les deux, sont exempt\u00e9s de l'enregistrement si chaque engrais ou suppl\u00e9ment utilis\u00e9 est soit\u00a0:</p>\n\n<ul>\n\n<li>exempt\u00e9 de l'enregistrement, soit enregistr\u00e9 pour cet usage</li>\n<li>conforme aux directives d'utilisation de ce m\u00e9dia</li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n\n<li class=\"previous\"><a href=\"/protection-des-vegetaux/engrais/apercu/fra/1646095692928/1646095693725\" rel=\"prev\">Pr\u00e9c\u00e9dent</a></li>\n\n<li class=\"next\"><a href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646274247575/1646274247919\" rel=\"next\">Suivant</a></li>\n\n</ul>\n</nav>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}