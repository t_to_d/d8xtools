{
    "dcr_id": "1313385088157",
    "title": {
        "en": "Importing tree and shrub seed into Canada",
        "fr": "Importation de semences d'arbres et d'arbustes au Canada"
    },
    "html_modified": "2011-08-15 01:11",
    "modified": "23-11-2018",
    "issued": "15-8-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/forest_import_seed_1313385088157_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/forest_import_seed_1313385088157_fra"
    },
    "parent_ia_id": "1300381713474",
    "ia_id": "1313385195581",
    "parent_node_id": "1300381713474",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importing tree and shrub seed into Canada",
        "fr": "Importation de semences d'arbres et d'arbustes au Canada"
    },
    "label": {
        "en": "Importing tree and shrub seed into Canada",
        "fr": "Importation de semences d'arbres et d'arbustes au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1313385195581",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300381457610",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/forestry/imports/tree-and-shrub-seed/",
        "fr": "/protection-des-vegetaux/forets/importation/semences-d-arbres-et-d-arbustes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importing tree and shrub seed into Canada",
            "fr": "Importation de semences d'arbres et d'arbustes au Canada"
        },
        "description": {
            "en": "Importing tree and shrub seed into Canada",
            "fr": "Importer des semences d'arbres et d'arbustes au Canada"
        },
        "keywords": {
            "en": "Plant Protection Act, Plant Protection Regulations, directives, phytosanitary, import, phytosanitary, forestry, tree, seeds",
            "fr": "Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, directives, phytosanitaires, importation, foresti\u00e8re, arbres, semences"
        },
        "dcterms.subject": {
            "en": "trees,imports,inspection,plants",
            "fr": "arbre,importation,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-08-15 01:11:31",
            "fr": "2011-08-15 01:11:31"
        },
        "modified": {
            "en": "2018-11-23",
            "fr": "2018-11-23"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importing tree and shrub seed into Canada",
        "fr": "Importation de semences d'arbres et d'arbustes au Canada"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=10&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The import of any plant material, including seed, can present a plant health risk to Canadian agricultural and forestry resources. Each commodity is reviewed in light of plant health concerns and where required, appropriate import regulations are implemented to address the risk. Seed and seed debris can act as a pathway for the movement of pests and allow pests of quarantine significance to gain entry.</p>\n<p>The seed of some plants is prohibited as the plant health risk can not be mitigated. Seed of <span lang=\"la\">Prunus</span>, and <span lang=\"la\">Rhamus</span> is prohibited for Plum pox virus and Crown rust of oats, respectively. Prohibited seed will be identified when screening the application for a plant health import permit.</p>\n<h2>Canadian import requirements for tree and shrub seed</h2>\n<h3>From off-continent sources</h3>\n<p>A section 32 plant health permit to import for each country of propagation is required. Import permits will be valid for 3\u00a0years. Import permits will be issued for various exporters within the country of propagation.</p>\n<h3>From the continental United States</h3>\n<p>For tree and shrub seed propagated in the United States: Please consult the <a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">Automated Import Reference System (AIRS)</a> for specific requirements.</p>\n<h3>For research purposes</h3>\n<p>Section 43 plant health permits to import can be processed and issued for imports of tree and shrub seed for scientific research, educational, processing, industrial or exhibition.</p>\n<h2>Program information</h2>\n<ul>\n<li><a href=\"/plant-health/invasive-species/plant-import/before-you-apply-for-a-permit/eng/1343414402266/1343414628852\">Application for an Import Permit</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-import/importing-plants-and-related-matter/eng/1343413109997/1343413207770\">Information guide for importers of plant and related matter into Canada</a></li>\n<li><a href=\"/eng/1343631753070/1343631945361\">Prohibited material from outside the continental <abbr title=\"United States\">US</abbr></a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=10&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'importation de tout mat\u00e9riel v\u00e9g\u00e9tal, y compris les semences, peut poser un risque phytosanitaire pour les ressources agricoles et foresti\u00e8res canadiennes. Chaque produit est \u00e9valu\u00e9 en regard de ces risques, et, le cas \u00e9ch\u00e9ant, son importation est r\u00e9glement\u00e9e en cons\u00e9quence afin de ma\u00eetriser ces risques. Les semences et les d\u00e9bris des semences peuvent servir de v\u00e9hicules \u00e0 des phytoravageurs et permettre l'introduction d'organismes nuisibles justiciables de quarantaine.</p>\n<p>L'importation des semences de certains v\u00e9g\u00e9taux est interdite au Canada, car les risques phytosanitaires ne peuvent \u00eatre att\u00e9nu\u00e9s. L'importation de semences de <span lang=\"la\">Prunus</span> et de <span lang=\"la\">Rhamus</span> est interdite afin d'emp\u00eacher la propagation du portyvirus de la sharka du prunier et, la rouille couronn\u00e9e de l'avoine respectivement. Les semences interdites seront identifi\u00e9es lors de l'examen de la demande d'un permis d'importation.</p>\n<h2>Exigences du Canada en mati\u00e8re d'importation de semences d'arbres et d'arbustes</h2>\n<h3>Semences provenant de l'ext\u00e9rieur du continent</h3>\n<p>Un permis d\u00e9livr\u00e9 en vertu de l'article 32 du R\u00e8glement sur la protection des v\u00e9g\u00e9taux est requis pour chaque pays de multiplication. Il est g\u00e9n\u00e9ralement valide pendant trois ans. Des permis d'importation seront d\u00e9livr\u00e9s \u00e0 divers exportateurs dans le pays de multiplication.</p>\n<h3>Semences de la zone continentale des \u00c9tats-Unis</h3>\n<p>Pour les arbres et arbustes propag\u00e9s aux \u00c9tats-Unis\u00a0: Veuillez consulter le <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a> pour les exigences sp\u00e9cifiques.\n</p>\n<h3>Semences pour la recherche scientifique</h3>\n<p>Un permis peut \u00eatre d\u00e9livr\u00e9 en vertu de l'article\u00a043 du R\u00e8glement sur la protection des v\u00e9g\u00e9taux pour l'importation de semences d'arbres et d'arbustes \u00e0 des fins \u00e9ducatives, industrielles ou \u00e0 des fins d'exposition, pour la recherche scientifique ou pour la transformation.</p>\n<h2>Information sur les programmes</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/avant-de-demander-un-permis-pour-importer/fra/1343414402266/1343414628852\">Demande d'un permis d'importation</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/importer-des-vegetaux-et-des-matieres-connexes/fra/1343413109997/1343413207770\">Guide d'information \u00e0 l'intention des importateurs de v\u00e9g\u00e9taux et de mat\u00e9riel v\u00e9g\u00e9tal connexe au Canada</a></li>\n<li><a href=\"/fra/1343631753070/1343631945361\">Mat\u00e9riel v\u00e9g\u00e9tal interdit en provenance de pays autres que le territoire continental des \u00c9tats-Unis</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}