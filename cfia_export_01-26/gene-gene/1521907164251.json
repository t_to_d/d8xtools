{
    "dcr_id": "1521907164251",
    "title": {
        "en": "Importing plants with novel traits to Canada",
        "fr": "Importer des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "html_modified": "2024-01-26 2:24:04 PM",
    "modified": "2018-03-24",
    "issued": "2018-03-26",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pnts_imports_1521907164251_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pnts_imports_1521907164251_fra"
    },
    "ia_id": "1521907164658",
    "parent_ia_id": "1300208874046",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Applications Under Review or Approved",
        "fr": "Applications Under Review or Approved"
    },
    "commodity": {
        "en": "Plants with Novel Traits",
        "fr": "V\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1300208874046",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importing plants with novel traits to Canada",
        "fr": "Importer des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "label": {
        "en": "Importing plants with novel traits to Canada",
        "fr": "Importer des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1521907164658",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300208718953",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plants-with-novel-traits/applicants/imports/",
        "fr": "/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/importation/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importing plants with novel traits to Canada",
            "fr": "Importer des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
        },
        "description": {
            "en": "PNTs (and/or products derived from them) to be imported into Canada are subject to the CFIA regulatory review under the Plant Protection Act and Regulations.",
            "fr": "Les produits non autoris\u00e9s issus de la biotechnologie peuvent provenir des sources suivantes : mat\u00e9riel de recherche ( p. ex. les produits qui devraient rester confin\u00e9s dans un laboratoire, une serre ou un champ de recherche, mais qui se retrouvent inopin\u00e9ment dans un aliment destin\u00e9 \u00e0 la consommation humaine ou animale, ou dans lenvironnement) produits qui sont autoris\u00e9s dans dautres pays, mais pas autoris\u00e9s au Canada En vertu des lois canadiennes, la pr\u00e9sence dun produit non autoris\u00e9 sur le march\u00e9 ou dans lenvironnement constitue une situation de non-conformit\u00e9 r\u00e9glementaire."
        },
        "keywords": {
            "en": "biotechnology, plants with novel traits, imports",
            "fr": "biotechnologie, v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux, importation"
        },
        "dcterms.subject": {
            "en": "biotechnology,environment,imports,inspection,plants,scientific research",
            "fr": "biotechnologie,environnement,importation,inspection,plante,recherche scientifique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-26",
            "fr": "2018-03-26"
        },
        "modified": {
            "en": "2018-03-24",
            "fr": "2018-03-24"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government,media",
            "fr": "entreprises,grand public,gouvernement,m\u00e9dia"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importing plants with novel traits to Canada",
        "fr": "Importer des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=25&amp;ga=4#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p><abbr title=\"Plants with Novel Traits\">PNTs</abbr> (and/or products derived from them) to be imported into Canada are subject to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> regulatory review under the Plant Protection Act and Regulations. <abbr title=\"Plants with Novel Traits\">PNTs</abbr> (and/or the products derived from them) that have been authorized for unconfined release into the Canadian environment may not require an Import Permit. <abbr title=\"Plants with Novel Traits\">PNTs</abbr> that have not been authorized for unconfined release into the Canadian environment require an Import Permit issued under the Plant Protection Act and Regulations. In addition, these <abbr title=\"Plants with Novel Traits\">PNTs</abbr> (and/or products derived from them) are subject to the same phytosanitary import requirements as their unmodified counterparts.</p>\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-96-13/eng/1323855470406/1323855595412\">D-96-13: Import Requirements for Plants with Novel Traits, including Transgenic Plants and their Viable Plant Parts</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-import/eng/1324569244509/1324569331710\">Permit Application Process</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=25&amp;ga=4#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les <abbr title=\"V\u00e9g\u00e9taux \u00e0 Caract\u00e8res Nouveaux\">VCN</abbr> (et/ou les produits d\u00e9riv\u00e9s) destin\u00e9s \u00e0 l'importation au Canada sont assujettis \u00e0 l'examen r\u00e9glementaire de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, en application de la Loi sur la protection des v\u00e9g\u00e9taux et de son r\u00e8glement d'application. Il n'est pas n\u00e9cessaire obligatoire de poss\u00e9der un permis pour les <abbr title=\"V\u00e9g\u00e9taux \u00e0 Caract\u00e8res Nouveaux\">VCN</abbr> (et/ou les produits d\u00e9riv\u00e9s) qui ont \u00e9t\u00e9 autoris\u00e9s pour \u00eatre diss\u00e9min\u00e9s en milieu ouvert dans l'environnement canadien. Il est obligatoire de poss\u00e9der un permis conforme \u00e0 la Loi sur la protection des v\u00e9g\u00e9taux et \u00e0 son r\u00e8glement d'application pour les <abbr title=\"V\u00e9g\u00e9taux \u00e0 Caract\u00e8res Nouveaux\">VCN</abbr> qui n'ont pas \u00e9t\u00e9 autoris\u00e9s pour \u00eatre diss\u00e9min\u00e9s en milieu ouvert dans l'environnement canadien. En outre, ces <abbr title=\"V\u00e9g\u00e9taux \u00e0 Caract\u00e8res Nouveaux\">VCN</abbr> (et/ou les produits d\u00e9riv\u00e9s) sont assujettis aux m\u00eames exigences phytosanitaires d'importation que leurs \u00e9quivalents non modifi\u00e9s.</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-96-13/fra/1323855470406/1323855595412\">D-96-13\u00a0: Exigences phytosanitaires\u00a0: Permis d'importation de v\u00e9g\u00e9taux \u00e0  caract\u00e8res nouveaux, y compris les v\u00e9g\u00e9taux transg\u00e9niques, et de leurs  parties viables</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/fra/1324569244509/1324569331710\">Processus de demande de permis</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}