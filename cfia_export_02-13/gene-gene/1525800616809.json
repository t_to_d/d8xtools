{
    "dcr_id": "1525800616809",
    "title": {
        "en": "Japanese beetle (<span lang=\"la\">Popillia japonica</span>) - Fact Sheet ",
        "fr": "Scarab\u00e9e japonais (<span lang=\"la\">Popillia japonica</span>) - Fiche de renseignements"
    },
    "html_modified": "2024-02-13 9:53:01 AM",
    "modified": "2018-05-14",
    "issued": "2018-05-22",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_popjap_fact_sheet_vancouver_1525800616809_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_popjap_fact_sheet_vancouver_1525800616809_fra"
    },
    "ia_id": "1525800617075",
    "parent_ia_id": "1525800137983",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1525800137983",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Japanese beetle (<span lang=\"la\">Popillia japonica</span>) - Fact Sheet ",
        "fr": "Scarab\u00e9e japonais (<span lang=\"la\">Popillia japonica</span>) - Fiche de renseignements"
    },
    "label": {
        "en": "Japanese beetle (<span lang=\"la\">Popillia japonica</span>) - Fact Sheet ",
        "fr": "Scarab\u00e9e japonais (<span lang=\"la\">Popillia japonica</span>) - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1525800617075",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1525800137593",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/japanese-beetle/japanese-beetle-in-bc/fact-sheet/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/scarabee-japonais/scarabee-japonais-en-c-b/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Japanese beetle (Popillia japonica) - Fact Sheet",
            "fr": "Scarab\u00e9e japonais (Popillia japonica) - Fiche de renseignements"
        },
        "description": {
            "en": "Japanese beetle is an invasive plant pest that was first introduced to eastern North America from Japan in 1916.",
            "fr": "Le scarab\u00e9e japonais est un ravageur envahissant qui a \u00e9t\u00e9 introduit dans l\u2019est de l\u2019Am\u00e9rique du Nord en provenance du Japon en 1916."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Popillia japonica, Japanese Beetle, fact sheet",
            "fr": "phytoravageurs, enqu&#234;tes phytosanitaires, phytoparasites justiciables de quarantaine, pi&#233;geage, Popillia japonica, Scarab&#233;e japonais, questions et r&#233;ponses"
        },
        "dcterms.subject": {
            "en": "insects,inspection,plant diseases,plants",
            "fr": "insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-05-22",
            "fr": "2018-05-22"
        },
        "modified": {
            "en": "2018-05-14",
            "fr": "2018-05-14"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Japanese beetle (Popillia japonica) - Fact Sheet",
        "fr": "Scarab\u00e9e japonais (Popillia japonica) - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"text-right\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/japanese_beetle_factsheet_1525932075406_eng.pdf\" title=\"Fact sheet in portable document format\"> <abbr title=\"Portable Document Format\">PDF</abbr> (592 <abbr title=\"kilobytes\">kb</abbr>)</a></p>\n<div class=\"row\">\n<div class=\"col-sm-8\">\n<h2>Why is it regulated?</h2>\n<p>Japanese beetle, <span lang=\"la\">Popillia japonica</span>, is an invasive plant pest that was first introduced to eastern North America from Japan in 1916. The adults are active flyers, but natural spread is slow and they are not able to travel long distances on their own. These beetles may move long distances as hitchhikers on plant material, in roots or soil, or even on cars, trains or planes. The Canadian Food Inspection Agency (CFIA) is working with its partners to minimize the human assisted spread of this insect to new areas.</p>\n</div>\n<div class=\"col-sm-4\">\n<p><img alt=\"Adult Japanses beetle\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_beetle_fact1_1525903364217_eng.jpg\"></p>\n</div>\n</div>\n<h2>What does it look like?</h2>\n<div class=\"row\">\n<div class=\"col-sm-4\">\n<p><img alt=\"Japanese beetle showing lateral white hair tufts\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_beetle_fact2_1525903365262_eng.jpg\"></p>\n</div>\n<div class=\"col-sm-8\">\n<p>Adult Japanese beetles are oval-shaped and approximately 1 <abbr title=\"centimetre\">cm</abbr> long. They are metallic green with two bronze wing covers and six white tufts of hair on each side of the abdomen. The larvae look very similar to chafer beetle and other creamy white C-shaped grubs that are found in the soil.</p>\n</div>\n</div>\n<h2>What does it eat?</h2>\n<div class=\"row\">\n<div class=\"col-sm-4\">\n<p><img alt=\"Japanese beetle 'skeletonizing' a leaf\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_beetle_fact3_1525903366198_eng.jpg\"></p>\n</div>\n<div class=\"col-sm-8\">\n<p>Japanese beetle larvae feed on the roots of turf grass and other plants. Adults are heavy feeders, attacking the flowers, foliage and fruit of more than 250 plant species, including roses, blueberries and grapevines.</p>\n</div>\n</div>\n<h2>What are the impacts?</h2>\n<p>Adult Japanese beetles skeletonize leaves and eat flowers and fruit. They can significantly damage landscape plants, ornamental plants, fruit and vegetable gardens, nurseries, orchards, and agricultural crops. The larvae feed on the roots of turf and other plants and can seriously damage lawns, sports fields, golf courses and turf production.</p>\n<h2>Where is it now?</h2>\n<p>Japanese beetle is present throughout most of eastern North America. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> places traps across southern British Columbia each summer as part of a surveillance network to alert the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> of any new introductions of the beetle. Each trap includes a pheromone and a plant volatile lure to attract the insect. Japanese beetle was detected in British Columbia for the first time in 2017. Adult Japanese beetles were found in <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> traps placed around False Creek in Vancouver.</p>\n<h2>How can I help?</h2>\n<ol>\n<li>Report any insects that you suspect could be Japanese beetle to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> by calling 1-800-442-2342.</li>\n<li>Do not move or tamper with any of the Japanese beetle traps that you see this summer.</li>\n<li>Avoid moving soil, plants with soil, pruning waste and other plant debris from areas where this insect has been reported.</li>\n</ol>\n<h2>Questions?</h2>\n<p>Visit our website for more information: inspection.gc.ca/jb</p>\n<p class=\"text-right small\">Photo credits: J. Baker, D. Cappaert, S. Katovich (Bugwood.org), <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"text-right\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/japanese_beetle_factsheet_1525932075406_fra.pdf\" title=\"Fiche de renseignements en format de document portable\"> <abbr title=\"Portable Document Format\">PDF</abbr> (603 <abbr title=\"kilo-octet\">ko</abbr>)</a></p>\n<div class=\"row\">\n<div class=\"col-sm-8\">\n<h2>Pourquoi est-il r\u00e9glement\u00e9?</h2>\n<p>Le scarab\u00e9e japonais, <span lang=\"la\">Popillia japonica</span>, est un ravageur envahissant qui a \u00e9t\u00e9 introduit dans l'est de l'Am\u00e9rique du Nord en provenance du Japon en 1916. Les scarab\u00e9es japonais adultes peuvent voler mais ils ne parcourent habituellement pas de longues distances; la dispersion naturelle est plut\u00f4t lente. Ils peuvent toutefois se d\u00e9placer sur de longues distances accroch\u00e9s sur du mat\u00e9riel v\u00e9g\u00e9tal, des racines ou dans le sol, ou m\u00eame sur des voitures, des trains ou des avions. L'Agence canadienne d'inspection des aliments (ACIA) travaille avec ses partenaires pour r\u00e9duire au minimum la propagation de cet insecte dans de nouvelles zones par des activit\u00e9s humaines.</p>\n</div>\n<div class=\"col-sm-4\">\n<p><img alt=\"Scarab\u00e9e japonais adulte\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_beetle_fact1_1525903364217_fra.jpg\"></p>\n</div>\n</div>\n<h2>\u00c0 quoi ressemble-t-il?</h2>\n<div class=\"row\">\n<div class=\"col-sm-4\">\n<p><img alt=\"Scarab\u00e9e japonais montrant des touffes de poils blancs lat\u00e9rales\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_beetle_fact2_1525903365262_fra.jpg\"></p>\n</div>\n<div class=\"col-sm-8\">\n<p>Les scarab\u00e9es japonais adultes sont ovales et mesurent environ 1 <abbr title=\"centim\u00e8tre\">cm</abbr> de long. Ils sont vert m\u00e9tallique avec deux \u00e9lytres d'un brun m\u00e9tallique cuivr\u00e9 et six touffes de poils blancs de chaque c\u00f4t\u00e9 de l'abdomen. Les larves de scarab\u00e9es japonais ressemblent beaucoup \u00e0 celles d'autres scarab\u00e9es qu'on retrouve dans le sol, comme le hanneton commun. Elles sont d'un blanc laiteux et ont la forme d'un C.</p>\n</div>\n</div>\n<h2>Que mange-t-il?</h2>\n<div class=\"row\">\n<div class=\"col-sm-4\">\n<p><img alt=\"Scarab\u00e9e japonais \u201csquelettisant\u201d une feuille\" class=\"img-responsive\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_beetle_fact3_1525903366198_fra.jpg\"></p>\n</div>\n<div class=\"col-sm-8\">\n<p>Les larves de scarab\u00e9es japonais se nourrissent des racines de gazon et d'autres plantes. Les adultes sont tr\u00e8s gourmands, attaquant les fleurs, le feuillage et les fruits de plus de 250 esp\u00e8ces v\u00e9g\u00e9tales, y compris les roses, les bleuets et les vignes.</p>\n</div>\n</div>\n<h2>Quels sont les impacts?</h2>\n<p>Les scarab\u00e9es japonais adultes squelettisent les feuilles et mangent les fleurs et les fruits. Ils peuvent causer des dommages importants aux am\u00e9nagements paysagers, aux plantes ornementales, aux jardins de fruits et l\u00e9gumes, aux p\u00e9pini\u00e8res, aux vergers et aux cultures agricoles. Les larves de scarab\u00e9es japonais se nourrissent des racines du gazon et d'autres plantes et peuvent gravement endommager les pelouses, les terrains de sport, les terrains de golf et la production de gazon.</p>\n<h2>O\u00f9 le retrouve-t-on?</h2>\n<p>Le scarab\u00e9e japonais est pr\u00e9sent dans la majeure partie de l'est de l'Am\u00e9rique du Nord. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> place des pi\u00e8ges dans le sud de la Colombie-Britannique chaque \u00e9t\u00e9 dans le cadre d'un r\u00e9seau de surveillance pour alerter l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de toute nouvelle introduction de scarab\u00e9e japonais. Chaque pi\u00e8ge comprend une ph\u00e9romone et un leurre floral pour attirer le scarab\u00e9e japonais. Il a \u00e9t\u00e9 d\u00e9tect\u00e9 en Colombie-Britannique pour la premi\u00e8re fois en 2017. Des adultes ont \u00e9t\u00e9 trouv\u00e9s dans des pi\u00e8ges de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> plac\u00e9s autour de <span lang=\"en\">False Creek</span> \u00e0 Vancouver.</p>\n<h2>Comment puis-je aider?</h2>\n<ol>\n<li>Signalez tout insecte que vous soup\u00e7onnez \u00eatre un scarab\u00e9e japonais \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en composant le 1-800-442-2342.</li>\n<li>Ne bougez pas et ne modifiez pas les pi\u00e8ges de scarab\u00e9es japonais que vous voyez cet \u00e9t\u00e9.</li>\n<li>\u00c9viter de d\u00e9placer le sol, les plantes avec de la terre, les d\u00e9chets d'\u00e9lagage et les autres d\u00e9bris v\u00e9g\u00e9taux des zones o\u00f9 le scarab\u00e9e japonais a \u00e9t\u00e9 signal\u00e9.</li>\n</ol>\n<h2>Questions</h2>\n<p>Visitez notre site Web pour plus d'information\u00a0: inspection.gc.ca/sj</p>\n<p class=\"text-right small\">Cr\u00e9dits photo\u00a0: J. Baker, D. Cappaert, S. Katovich (<span lang=\"en\">Bugwood</span>.org), <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}