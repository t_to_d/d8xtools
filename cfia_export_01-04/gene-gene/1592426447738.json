{
    "dcr_id": "1592426447738",
    "title": {
        "en": "Bye bye beetle",
        "fr": "Bon vent longicorne"
    },
    "html_modified": "25-6-2020",
    "modified": "24-6-2020",
    "issued": "24-6-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/c360_bye_bye_beetle_1592426447738_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/c360_bye_bye_beetle_1592426447738_fra"
    },
    "parent_ia_id": "1565298134171",
    "ia_id": "1592426448066",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Bye bye beetle",
        "fr": "Bon vent longicorne"
    },
    "label": {
        "en": "Bye bye beetle",
        "fr": "Bon vent longicorne"
    },
    "templatetype": "content page 1 column",
    "node_id": "1592426448066",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1565298133875",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/plant-health/bye-bye-beetle/",
        "fr": "/inspecter-et-proteger/sante-des-vegetaux/bon-vent-longicorne/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Bye bye beetle",
            "fr": "Bon vent longicorne"
        },
        "description": {
            "en": "Have you heard the good news? After several years of close monitoring, the Asian longhorned beetle has been eradicated in Mississauga and Toronto.",
            "fr": "Avez\u2011vous entendu la bonne nouvelle? Apr\u00e8s plusieurs ann\u00e9es de surveillance \u00e9troite, le longicorne asiatique a \u00e9t\u00e9 \u00e9radiqu\u00e9 \u00e0 Mississauga et \u00e0 Toronto."
        },
        "keywords": {
            "en": "Chronicle 360, monitoring, the Asian longhorned beetle, eradicated, Mississauga, Toronto",
            "fr": "Chroniques 360, surveillance, longicorne asiatique, \u00e9radiqu\u00e9, Mississauga,Toronto"
        },
        "dcterms.subject": {
            "en": "government information,food inspection,government publications,regulation,food safety",
            "fr": "information gouvernementale,inspection des aliments,publication gouvernementale,r\u00e9glementation,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernment du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-06-25",
            "fr": "2020-06-25"
        },
        "modified": {
            "en": "2020-06-24",
            "fr": "2020-06-24"
        },
        "type": {
            "en": "reference material,news publication",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,publication d'information"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Bye bye beetle",
        "fr": "Bon vent longicorne"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img alt=\"Bye bye beetle\" class=\"img-thumbnail img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/c360_bye_bye_beetle_600x600_1592425149058_eng.jpg\">\n</div>\n\n<p>Have you heard the good news? After several years of close monitoring by the Canadian Food Inspection Agency (CFIA), the Asian longhorned beetle has been eradicated in Mississauga and Toronto.</p>\n\n<p>When it was detected in these areas in 2013, the CFIA established a regulated area to prevent the further spread of this pest. The goal was to remove infested trees and restrict the movement of all firewood, and materials like wood chips and nursery stock from certain species of trees, out of that regulated area\u00a0\u2013\u00a0and it worked. </p>\n\n<p>The Asian longhorned beetle population has now been eliminated, and the Minister of Agriculture and Agri-Food has declared the regulated area a thing of the past. </p>\n\n<p>This is a timely development as Canada celebrates the United Nation's <a href=\"http://www.science.gc.ca/eic/site/063.nsf/eng/h_97930.html\">International Year of Plant Health</a> in 2020. Invasive species and other plant pests damage our crops, ecosystems, forests and natural habitats, which can have damaging effects on food security, as well as our health and wellbeing.</p>\n\n<h2>More bugs, more problems</h2>\n\n<p>In Canada, the Asian longhorned beetle mainly attacks maple, but also many other trees such as poplar, birch and willow. </p>\n\n<p>The beetles feed on the leaves and twigs of host trees. They chew into the branches and trunk and emerge from the tree, leaving large holes in their path. These highly destructive pests infest and kill healthy trees, devastating urban and rural communities.</p>\n\n<p>Once in a new environment, they can disperse naturally or spread long-distance through the transport of infested wood products, including firewood and logs.</p>\n\n<h2>A team effort</h2>\n\n<p>Over the past few years, the CFIA led a large-scale surveillance program aimed at detecting the signs and damage caused by the Asian longhorned beetle.</p>\n\n<p>Mary Orr, a recently retired CFIA Plant Protection Officer who led the Asian longhorned beetle survey efforts, said, \"This was a multi-year removal and survey program based on science. It was designed to eliminate the Asian longhorned beetle in Mississauga and Toronto and its threat to our environment and Canada's hardwood lumber, nursery, and maple syrup industries.\"</p>\n\n<p>Collaboration with federal, provincial and municipal partners was key to eliminating this pest, including the cities of Toronto and Mississauga, the Region of Peel, Natural Resources Canada, Canadian Forest Service, and the Ontario Ministry of Natural Resources and Forestry.</p>\n\n<p>\"The various organizations involved provided resources based on their strengths that made our required survey and removal efforts effective and possible,\" said Orr. This included working with the CFIA to facilitate the eradication survey, sharing scientific information and keeping the public, industry and other stakeholders informed.</p>\n\n<h2>Out of sight, not out of mind</h2>\n\n<p>There's always a risk that the Asian longhorned beetle could return. In fact, it was originally discovered in Mississauga and Toronto shortly after being declared eradicated in a nearby area. </p>\n\n<p>To combat this threat, the CFIA has a number of requirements in place for bringing wood packaging, wood products and materials for planting into Canada. But we also need your help to protect our forests.</p>\n\n<h2>Don't move firewood: buy and burn it locally</h2>\n\n<p>One way you can join the fight against plant pests is by not moving firewood. Although you may not see them, invasive species like the Asian longhorned beetle can easily hide in firewood. When firewood moves, for example from a campground to a cottage, so do the pests that threaten our precious forest resources. </p>\n\n<p>Moving firewood poses a great risk to our economy and environment. A mass infestation can kill trees in our forests and neighbourhoods, affecting air and water quality. It can also lower the property value of your home.</p>\n\n<p>Learn more about the <a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">rules for moving firewood</a> and how you can prevent the spread of invasive species.</p>\n\n<h2>Report plant pests</h2>\n\n<p>Learn <a href=\"/plant-health/invasive-species/eng/1299168913252/1299168989280\">how to identify plant pests</a>, be on the lookout in your area, and report sightings or signs of an infestation. </p>\n\n<p>Orr said, \"Members of the public play an important role in detecting possible future presence of the Asian longhorned beetle. Many eyes surveying trees, in addition to CFIA inspectors, can be very helpful. Know what you are looking for by visiting the CFIA website and becoming familiar with the signs of an Asian longhorned beetle attack, what pest looks like, and which trees are susceptible. Contact the CFIA with insect samples or suspect trees of interest and photos.\"</p>\n\n<p>Let's protect Canada's trees together! Get in touch with us <a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">online</a> or contact your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a> to report a pest.</p>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img alt=\"Bon vent longicorne\" class=\"img-thumbnail img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/c360_bye_bye_beetle_600x600_1592425149058_eng.jpg\">\n</div>\n\n<p>Avez\u2011vous entendu la bonne nouvelle? Apr\u00e8s plusieurs ann\u00e9es de surveillance \u00e9troite par l'Agence canadienne d'inspection des aliments (ACIA), le longicorne asiatique a \u00e9t\u00e9 \u00e9radiqu\u00e9 \u00e0 Mississauga et \u00e0 Toronto.</p>\n\n<p>Lorsque cet insecte a \u00e9t\u00e9 d\u00e9tect\u00e9 dans ces r\u00e9gions en 2013, l'ACIA a \u00e9tabli une zone r\u00e9glement\u00e9e pour pr\u00e9venir la propagation accrue de ce ravageur. L'objectif \u00e9tait de retirer les arbres infest\u00e9s et de limiter le d\u00e9placement de tout le bois de chauffage ainsi que des mat\u00e9riaux comme les copeaux de bois et le mat\u00e9riel de p\u00e9pini\u00e8re de certaines esp\u00e8ces d'arbres, hors de cette zone r\u00e9glement\u00e9e\u00a0et cela a fonctionn\u00e9.</p>\n\n<p>La population de longicorne asiatique a maintenant \u00e9t\u00e9 \u00e9limin\u00e9e et le ministre de l'Agriculture et de l'Agroalimentaire a d\u00e9clar\u00e9 que la r\u00e9gion r\u00e9glement\u00e9e \u00e9tait chose du pass\u00e9.</p>\n\n<p>Il s'agit d'un \u00e9v\u00e9nement opportun, car en 2020, le Canada c\u00e9l\u00e8bre l'<a href=\"http://www.science.gc.ca/eic/site/063.nsf/fra/h_97930.html\">Ann\u00e9e internationale de la sant\u00e9 des v\u00e9g\u00e9taux</a> des Nations Unies. Les esp\u00e8ces envahissantes et les autres phytoravageurs endommagent nos cultures, nos \u00e9cosyst\u00e8mes, nos for\u00eats et nos habitats naturels, ce qui peut avoir des effets dommageables sur la s\u00e9curit\u00e9 alimentaire, ainsi que sur notre sant\u00e9 et notre bien\u2011\u00eatre.</p>\n\n<h2>Plus d'insectes, plus de probl\u00e8mes</h2>\n\n<p>Au Canada, le longicorne asiatique s'attaque principalement \u00e0 l'\u00e9rable, mais aussi \u00e0 de nombreux autres arbres comme le peuplier, le bouleau et le saule.</p>\n\n<p>Les longicornes se nourrissent des feuilles et des brindilles des arbres h\u00f4tes. Ils m\u00e2chent les branches et le tronc, puis ils \u00e9mergent de l'arbre, laissant de grands trous sur leur chemin. Ces ravageurs hautement destructeurs infestent et tuent des arbres sains, et d\u00e9vastent les communaut\u00e9s urbaines et rurales.</p>\n\n<p>Lorsqu'ils sont dans un nouvel environnement, ils peuvent se disperser naturellement ou se propager sur de longues distances par le transport de produits de bois infest\u00e9s, y compris le bois de chauffage et les billes.</p>\n\n<h2>Un travail d'\u00e9quipe</h2>\n\n<p>Au cours des derni\u00e8res ann\u00e9es, l'ACIA a dirig\u00e9 un programme de surveillance \u00e0 grande \u00e9chelle visant \u00e0 d\u00e9tecter les signes et les dommages caus\u00e9s par le longicorne asiatique.</p>\n\n<p>Mary Orr, une agente de phytoprotection sanitaire de l'ACIA r\u00e9cemment retrait\u00e9e qui a dirig\u00e9 les efforts d'enqu\u00eate sur le longicorne asiatique, a d\u00e9clar\u00e9 ce qui suit : \u00ab\u00a0Il s'agissait d'un programme pluriannuel d'\u00e9limination et d'enqu\u00eate fond\u00e9 sur les donn\u00e9es scientifiques. Il a \u00e9t\u00e9 con\u00e7u pour \u00e9liminer le longicorne asiatique \u00e0 Mississauga et \u00e0 Toronto, et sa menace pour notre environnement et les industries canadiennes du bois d'\u0153uvre de feuillus, de la p\u00e9pini\u00e8re et du sirop d'\u00e9rable.\u00a0\u00bb</p>\n\n<p>La collaboration avec les partenaires f\u00e9d\u00e9raux, provinciaux et municipaux \u00e9tait essentielle pour \u00e9liminer ce ravageur, y compris celle des villes de Toronto et de Mississauga, de la r\u00e9gion de Peel, de Ressources naturelles Canada, du Service canadien des for\u00eats et du minist\u00e8re des Richesses naturelles et des For\u00eats de l'Ontario.</p>\n\n<p>\u00ab\u00a0Les diverses organisations participantes ont fourni des ressources en fonction de leurs forces qui ont rendu nos efforts d'enqu\u00eate et d'\u00e9limination n\u00e9cessaires efficaces et possibles,\u00a0\u00bb a d\u00e9clar\u00e9 M<sup>me</sup> Orr. Cela comprenait la collaboration avec l'ACIA pour faciliter l'enqu\u00eate sur l'\u00e9radication, l'\u00e9change de renseignements scientifiques et le fait de tenir le public, l'industrie et d'autres intervenants inform\u00e9s.</p>\n\n<h2>Loin des yeux, mais non loin du c\u0153ur</h2>\n\n<p>Il y a toujours un risque que le longicorne asiatique revienne. En fait, il a \u00e9t\u00e9 d\u00e9couvert \u00e0 l'origine \u00e0 Mississauga et \u00e0 Toronto peu apr\u00e8s avoir \u00e9t\u00e9 d\u00e9clar\u00e9 \u00e9radiqu\u00e9 dans une r\u00e9gion voisine.</p>\n\n<p>Pour contrer cette menace, l'ACIA a mis en place un certain nombre d'exigences pour l'introduction de l'emballage du bois, des produits du bois et du mat\u00e9riel de plantation au Canada. Par contre, nous avons aussi besoin de votre aide pour prot\u00e9ger nos for\u00eats.</p>\n\n<h2>Ne d\u00e9placez pas le bois de chauffage : achetez\u2011le et br\u00fblez\u2011le localement</h2>\n\n<p>L'une des fa\u00e7ons de rejoindre la lutte contre les phytoravageurs est de ne pas d\u00e9placer le bois de chauffage. M\u00eame si vous ne les voyez pas, les esp\u00e8ces envahissantes comme le longicorne asiatique peuvent facilement se cacher dans le bois de chauffage. Lorsque le bois de chauffage est d\u00e9plac\u00e9, par exemple d'un terrain de camping \u00e0 un chalet, il en va de m\u00eame pour les ravageurs qui menacent nos pr\u00e9cieuses ressources foresti\u00e8res.</p>\n\n<p>D\u00e9placer du bois de chauffage repr\u00e9sente un risque important pour notre \u00e9conomie et notre environnement. Une infestation massive peut tuer des arbres dans nos for\u00eats et nos quartiers, affectant la qualit\u00e9 de l'air et de l'eau. Elle peut \u00e9galement r\u00e9duire la valeur de votre propri\u00e9t\u00e9.</p>\n\n<p>Apprenez\u2011en davantage au sujet des <a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">r\u00e8gles pour le d\u00e9placement du bois de chauffage</a> et sur la fa\u00e7on de pr\u00e9venir la propagation d'esp\u00e8ces envahissantes.</p>\n\n<h2>Signalez les phytoravageurs</h2>\n\n<p>Apprenez <a href=\"/protection-des-vegetaux/especes-envahissantes/fra/1299168913252/1299168989280\">la fa\u00e7on d'identifier les phytoravageurs</a>, d'\u00eatre \u00e0 l'aff\u00fbt dans votre r\u00e9gion et de signaler les observations ou signes d'infestation.</p>\n\n<p>M<sup>me</sup> Orr a d\u00e9clar\u00e9 ce qui suit : \u00ab\u00a0Les membres du public jouent un r\u00f4le important dans la d\u00e9tection d'une \u00e9ventuelle pr\u00e9sence \u00e0 venir du longicorne asiatique. De nombreux yeux qui inspectent les arbres, en plus des inspecteurs de l'ACIA, peuvent \u00eatre tr\u00e8s utiles. Sachez ce que vous cherchez en visitant le site Web de l'ACIA et en vous familiarisant avec les signes d'une attaque du longicorne asiatique, avec l'apparence d'un ravageur et les arbres qui y sont sensibles. Communiquez avec l'ACIA avec des \u00e9chantillons d'insectes ou des arbres suspects pr\u00e9sentant un int\u00e9r\u00eat et des photos.\u00a0\u00bb</p>\n\n<p>Prot\u00e9geons ensemble les arbres du Canada. Communiquez avec nous <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">en ligne</a> ou communiquez avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'ACIA</a> pour signaler un ravageur.</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": false,
    "chat_wizard": false
}