{
    "dcr_id": "1604675250185",
    "title": {
        "en": "Notice to Industry\u00a0\u2013\u00a0Transitional provisions",
        "fr": "Avis \u00e0 l'industrie \u2013 Dispositions transitoires"
    },
    "html_modified": "2024-02-13 9:54:49 AM",
    "modified": "2020-11-13",
    "issued": "2020-11-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/notice_transitional_provisions_1604675250185_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/notice_transitional_provisions_1604675250185_fra"
    },
    "ia_id": "1604675493951",
    "parent_ia_id": "1490849960260",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1490849960260",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice to Industry\u00a0\u2013\u00a0Transitional provisions",
        "fr": "Avis \u00e0 l'industrie \u2013 Dispositions transitoires"
    },
    "label": {
        "en": "Notice to Industry\u00a0\u2013\u00a0Transitional provisions",
        "fr": "Avis \u00e0 l'industrie \u2013 Dispositions transitoires"
    },
    "templatetype": "content page 1 column",
    "node_id": "1604675493951",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1490849930025",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/notices-to-industry/transitional-provisions/",
        "fr": "/protection-des-vegetaux/engrais/avis-a-l-industrie/dispositions-transitoires/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice to Industry\u00a0\u2013\u00a0Transitional provisions",
            "fr": "Avis \u00e0 l'industrie \u2013 Dispositions transitoires"
        },
        "description": {
            "en": "Effective November 10th, 2020, the amended Fertilizers Regulations are in force. The amendments introduce a risk-based approach, strengthen regulatory controls for product safety, environmental sustainability and consumer protection while reducing regulatory and administrative burden on regulated parties.",
            "fr": "plants, plant health"
        },
        "keywords": {
            "en": "Notice to Industry, Transitional provisions, Fertilizers Regulations, Applying for new registrations during the transitional period, Recommended timelines",
            "fr": "plants, plant health"
        },
        "dcterms.subject": {
            "en": "food,fertilizers,food inspection,plants,regulations",
            "fr": "aliment,engrais,inspection des aliments,plante,r\u00e9glementations"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-11-13",
            "fr": "2020-11-13"
        },
        "modified": {
            "en": "2020-11-13",
            "fr": "2020-11-13"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice to Industry\u00a0\u2013\u00a0Transitional provisions",
        "fr": "Avis \u00e0 l'industrie \u2013 Dispositions transitoires"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>November 13, 2020</p>\n<h2>Modernization of the <i>Fertilizers Regulations</i></h2>\n<p>Effective October 26th, 2020, the amended <i>Fertilizers Regulations</i> are in force. The amendments introduce a risk-based approach, strengthen regulatory controls for product safety, environmental sustainability and consumer protection while reducing regulatory and administrative burden on regulated parties. Read the <a href=\"/english/reg/jredirect2.shtml?ferengr\">\"new\" <i>Fertilizers Regulations</i> and their impacts</a>.</p>\n<h2>Transitional provisions</h2>\n<p>The amendments contain transitional provisions that allow regulated parties to comply with either the \"new\" regulations or the \"old\" regulations for a period of 3 years. This applies to the manufacture, sale, import or export of fertilizers and supplements regulated under the <i>Fertilizers Act</i>. Combining provisions from \"new\" and \"old\" regulations is not permitted. Product proponents must comply with either \"new\" or \"old\" regulations with respect to a single product.</p>\n<p>The transitional provisions are in place to allow regulated parties to exhaust the existing stock of products in the supply chain and minimize cost associated with adhering to the new regulatory requirements. Product proponents, are <strong>strongly</strong> encouraged to shift to the \"new\" regulatory regime as soon as possible and practical within their specific circumstances.</p>\n<h2>Choosing the \"old\" vs. the \"new\" regulations</h2>\n<p>The choice of either option largely depends on your particular business circumstance and your product's current regulatory status. Here are some recommendations when choosing:</p>\n<ul>\n<li>products currently registered but exempt from registration under the \"new\" regime require no immediate action - the registration will simply lapse. You must switch to a new label that complies with the new regulations by the end of the transition period</li>\n<li>products exempt from registration that require registration under the \"new\" regime- submit an application for new registration. Existing product could continue to be sold until the transition period ends</li>\n<li>re-registrations and amendments during the transitional period - submit an application to transition to the \"new\" regulations to take advantage of the 5 year re-registration period and reductions in burden</li>\n<li>new product registrations - apply only under the \"new\" regulations</li>\n</ul>\n<h2><strong>Applying for new registrations during the transitional period</strong></h2>\n<p>The regulatory status (registerable vs. exempt from registration) of certain products has changed in the new regulations.  This means that certain product categories that were exempt under the \"old\" regulations will now require registration and vice versa, some that required registration under the \"old\" regulations  are exempt moving forward. Please refer to the <a href=\"/plant-health/fertilizers/overview/fertilizers-and-supplements/eng/1330932243713/1330933201778\">registration triggers document</a> for details.</p>\n<p>Note: electronic submissions are now available for new registrations and research authorizations. This, however, applies <strong>only</strong> to product proponents choosing to comply with the \"new\" <i>Fertilizers Regulations</i>. The <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\">My CFIA</a> digital service delivery platform allows you to request, pay for and track the status of services online anytime, anywhere through a secure account that can be tailored to suit your business model. To apply for a new registration online, you must first create a My CFIA profile. Once enrolled, you will see the services available, including the Pre-market Application Submissions Office (PASO) service request of applying for a new registration. Guidance documents, videos and step-by-step walk-throughs for how to sign up for an account, manage an account and request services online can be found at: <a href=\"/about-the-cfia/my-cfia-account/user-guidance/eng/1545974154373/1545974178498\">My CFIA guidance</a>. Please note that registration renewals and amendments will be available through the electronic platform at a later date. Applications submitted under the old regulations will not have access to the My CFIA platform.</p>\n<p>Whether following the new or old regulations, if you submit an application by email or mail, please send your application package to the Pre-market Application Submissions Office (see contact information below). When submitting by mail, it is preferable that the submission (or as much of it as is possible) be provided in electronic format (CD-ROM, DVD, flash-drive, etc.) to facilitate rapid processing of your application.</p>\n<p>To facilitate transition to the new regulations, the CFIA has developed guidance for proponents seeking to submit applications to register or re-register products under the <i>Fertilizers Act </i>during the transitional period. This timeline was developed in recognition of the need to minimize impacts on products already in the supply chain, while allowing the regulated parties to take immediate advantage of burden reductions offered by the regulatory amendments. It is further intended to maximize productivity for the CFIA's pre-market registration team and minimize delays to market for new products.</p>\n<h2>Recommended timelines for submitting applications</h2>\n\n\n<figure class=\"pull-right mrgn-lft-md col-sm-6\">\n<p><small>Click on image for larger view</small><br>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/notice_transitional_provisions_1604674458136_eng.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/notice_transitional_provisions_1604674458136_eng.jpg\" class=\"img-responsive\" alt=\"Timelines for proponents submitting applications during the 3-year implementation period. Description follows.\"></a></p>\n<details>\n<summary>Timelines for proponents submitting applications during the 3-year implementation period</summary>\n<p>Publication of updated guidance: November 2020</p>\n<p>Annual fall CFIA regulatory workshops: November 2020, November 2021, November 2022, November 2023</p>\n<p>New registration (paper format, old regulations): applications accepted until May 2021.</p>\n<p>New registration (paper or My CFIA, new regulations): applications accepted from November 2020 to beyond November 2023</p>\n<p>Re-registration (old regulations): applications accepted until November 2021</p>\n<p>Re-registration (paper, new regulations): applications accepted November 2020 to beyond November 2023</p>\n<p>Re-registration (My CFIA, new regulations): applications accepted November 2021 to beyond November 2023</p>\n<p>CFIA submission training webinars for re-registration: November 2020 and November 2021</p>\n</details>\n</figure>\n\n\n<p>The timelines presented in this figure provide guidance for proponents to transition to submitting applications under the new regulations. These timelines are based on the fact that a typical new registration takes 1-1.5 years to process, while a renewal (with changes to the product, it's directions for use or claims) typically takes 1 year. Applications submitted during the transition period to register new products under the old regulations divert resources to review products that, if successful, would only be registered for a short period of time. Any registrations granted under the old regulations will be cancelled at the end of the transition period if they do not lapse before then.</p>\n<p>In recognition of these diminishing returns, the CFIA has developed these timelines to guide proponents in submitting their applicants for registration under the <i>Fertilizers Act</i>. Following these timelines will enable smooth transition to the new regulations and minimize resources expended on applications for permissions that would soon expire. For example: an application to register a new product under the old regulations that is received during the second year of the transitional period may result in a registration that only lasts 6 months (or less) prior to the transitional period ending and the registration being cancelled. An application to register the same product under the new regulations would receive a 5 year registration.</p>\n\n<h2>Contact information</h2>\n<p>Fertilizer Safety Section<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le 13 novembre 2020</p>\n<h2>Modernisation du <i>R\u00e8glement sur les engrais</i></h2>\n<p>En date du 26 octobre 2020, les modifications au <i>R\u00e8glement sur les engrais</i> sont en vigueur. Ces modifications introduisent une approche ax\u00e9e sur les risques et renforcent les contr\u00f4les r\u00e9glementaires pour assurer l'innocuit\u00e9 des produits, la durabilit\u00e9 de l'environnement et la protection des consommateurs, tout en r\u00e9duisant le fardeau r\u00e9glementaire et administratif pour les parties r\u00e9glement\u00e9es. Consultez le <a href=\"/francais/reg/jredirect2.shtml?ferengr\">\u00ab\u00a0nouveau\u00a0\u00bb <i>R\u00e8glement sur les engrais</i> et apprenez-en plus sur son impact</a>.</p>\n<h2>Dispositions transitoires</h2>\n<p>Les modifications sont accompagn\u00e9es de dispositions transitoires qui offrent aux parties r\u00e9glement\u00e9es la possibilit\u00e9, pendant une p\u00e9riode de 3 ans, de se conformer soit au \u00ab\u00a0nouveau\u00a0\u00bb ou soit \u00e0 l'\u00ab\u00a0ancien\u00a0\u00bb r\u00e8glement. Ces dispositions s'appliquent \u00e0 la fabrication, \u00e0 la vente, \u00e0 l'importation ou \u00e0 l'exportation d'engrais et de suppl\u00e9ments r\u00e9glement\u00e9s en vertu de la <i>Loi sur les engrais</i>. Il n'est cependant pas permis de combiner les dispositions du nouveau et de l'ancien r\u00e8glement. Pour chaque produit, les promoteurs doivent se conformer au nouveau ou \u00e0 l'ancien r\u00e8glement.</p>\n<p>Les dispositions transitoires sont en place pour permettre aux parties r\u00e9glement\u00e9es d'\u00e9couler leurs inventaires de produits existants dans la cha\u00eene d'approvisionnement et de minimiser les co\u00fbts associ\u00e9s \u00e0 la transition vers les nouvelles exigences r\u00e9glementaires. Les promoteurs de produits sont <strong>fortement </strong>encourag\u00e9s \u00e0 passer au \u00ab\u00a0nouveau\u00a0\u00bb r\u00e9gime r\u00e9glementaire d\u00e8s que possible et dans la mesure o\u00f9 leurs circonstances particuli\u00e8res le permettent.</p>\n<h2>Choisir le \u00ab\u00a0nouveau\u00a0\u00bb ou l'\u00ab\u00a0ancien\u00a0\u00bb r\u00e8glement</h2>\n<p>Le choix de l'une ou l'autre option d\u00e9pend en grande partie de la situation de votre entreprise et de l'\u00e9tat r\u00e9glementaire actuel de votre produit. Voici des recommandations pour \u00e9clairer votre choix\u00a0:</p>\n<ul>\n<li>Les produits qui sont enregistr\u00e9s actuellement, mais qui sont exempt\u00e9s de l'enregistrement dans le cadre du nouveau r\u00e8glement ne n\u00e9cessitent aucune action imm\u00e9diate - l'enregistrement deviendra tout simplement caduc. Vous devez passer \u00e0 une nouvelle \u00e9tiquette conforme au nouveau r\u00e8glement d'ici la fin de la p\u00e9riode de transition</li>\n<li>Les produits exempt\u00e9s de l'enregistrement aux termes de l'ancien r\u00e8glement, mais qui requi\u00e8rent un enregistrement en vertu du nouveau r\u00e8glement - soumettre une demande de nouvel enregistrement. Les produits existants peuvent continuer \u00e0 \u00eatre vendus jusqu'\u00e0 la fin de la p\u00e9riode de transition</li>\n<li>R\u00e9enregistrements et modifications durant la p\u00e9riode de transition - pr\u00e9sentez une demande afin de faire la transition vers le nouveau r\u00e8glement pour profiter de la p\u00e9riode de r\u00e9enregistrement de cinq\u00a0ans et du fardeau administratif r\u00e9duit.</li>\n<li>Enregistrements de nouveaux produits - pr\u00e9sentez une demande en vertu du nouveau r\u00e8glement seulement</li>\n</ul>\n\n<h2>Demande de nouveaux enregistrements durant la p\u00e9riode de transition</h2>\n<p>Le statut r\u00e9glementaire (exempt\u00e9 ou non exempt\u00e9) de l'enregistrement de certains produits a chang\u00e9 dans le nouveau r\u00e8glement. Cela signifie que certains produits qui \u00e9taient exempt\u00e9s en vertu de l'ancien r\u00e8glement devront maintenant \u00eatre enregistr\u00e9s et, inversement, que certains produits qui devaient \u00eatre enregistr\u00e9s en vertu de l'ancien r\u00e8glement seront dor\u00e9navant exempt\u00e9s. Veuillez consulter le document sur les <a href=\"/protection-des-vegetaux/engrais/apercu/engrais-et-les-supplements/fra/1330932243713/1330933201778\">d\u00e9clencheurs de l'enregistrement</a> pour plus de d\u00e9tails.</p>\n<p>Remarques: Les soumissions \u00e9lectroniques sont d\u00e9sormais disponibles pour les nouveaux enregistrements et les autorisations de recherche. Cette option est toutefois offerte <strong>seulement</strong> aux promoteurs de produits qui choisissent de se conformer au nouveau <i>R\u00e8glement sur les engrais</i>. La <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\">plateforme de prestation num\u00e9rique des services Mon ACIA</a> vous permet de pr\u00e9senter des demandes, de faire des paiements et de suivre l'\u00e9tat de vos demandes en ligne en tout temps o\u00f9 que vous soyez, au moyen d'un compte s\u00e9curis\u00e9 qui peut \u00eatre personnalis\u00e9 selon le mod\u00e8le de votre entreprise. Pour faire une nouvelle demande d'enregistrement, vous devez d'abord cr\u00e9er un profil dans Mon ACIA. Une fois inscrit, vous pourrez voir les services offerts, y compris ceux du Bureau de pr\u00e9sentation de demandes pr\u00e9alable \u00e0 la mise en march\u00e9 (BPDPM) pour faire une nouvelle demande d'enregistrement. Des documents d'orientation, des vid\u00e9os et des d\u00e9monstrations \u00e9tape par \u00e9tape sur la fa\u00e7on de cr\u00e9er et de g\u00e9rer un compte et de demander des services en ligne sont offerts sur la page d'<a href=\"/a-propos-de-l-acia/compte-mon-acia/orientation-de-l-utilisateur/fra/1545974154373/1545974178498\">orientation de l'utilisateur de Mon ACIA</a>. Veuillez noter que les demandes de renouvellements et les modifications d'enregistrement seront disponibles \u00e0 une date ult\u00e9rieure sur la plate-forme \u00e9lectronique. Les demandes en vertu de l'ancien r\u00e8glement ne pourront pas \u00eatre pr\u00e9sent\u00e9es sur la plateforme Mon ACIA.</p>\n<p>Que vous respectiez le nouveau ou l'ancien r\u00e8glement, si vous pr\u00e9sentez une demande par courriel ou par la poste, veuillez envoyer votre demande au Bureau de pr\u00e9sentation de demandes pr\u00e9alable \u00e0 la mise en march\u00e9 (voir les coordonn\u00e9es ci-dessous). Dans le cas des demandes par la poste, il est pr\u00e9f\u00e9rable que les documents (ou la plus grande partie possible de ceux-ci) soient fournis en format \u00e9lectronique (CD-ROM, DVD, cl\u00e9 USB, etc.) pour faciliter le traitement rapide de votre demande.</p>\n<p>Pour faciliter la transition vers le nouveau r\u00e8glement, l'ACIA a \u00e9labor\u00e9 des lignes directrices pour les promoteurs qui souhaitent pr\u00e9senter des demandes d'enregistrement ou de r\u00e9enregistrement de produits en vertu de la <i>Loi sur les engrais</i> durant la p\u00e9riode de transition. Cet \u00e9ch\u00e9ancier a \u00e9t\u00e9 \u00e9labor\u00e9 en tenant compte de la n\u00e9cessit\u00e9 de minimiser les impacts sur les produits d\u00e9j\u00e0 pr\u00e9sents dans la cha\u00eene d'approvisionnement, tout en permettant aux parties r\u00e9glement\u00e9es de profiter imm\u00e9diatement des r\u00e9ductions du fardeau apport\u00e9 par les modifications r\u00e9glementaires. Il vise \u00e9galement \u00e0 maximiser la productivit\u00e9 de l'\u00e9quipe de l'ACIA charg\u00e9e de l'enregistrement pr\u00e9alable \u00e0 la mise en march\u00e9 et \u00e0 minimiser les d\u00e9lais pour la mise en march\u00e9 de nouveaux produits.</p>\n<h2>\u00c9ch\u00e9ancier recommand\u00e9 pour la pr\u00e9sentation des demandes</h2>\n\n\n<figure class=\"pull-right mrgn-lft-md col-sm-6\">\n<p><small>Cliquez sur l'image pour l'agrandir</small><br>\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/notice_transitional_provisions_1604674458136_fra.jpg\"><img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/notice_transitional_provisions_1604674458136_fra.jpg\" class=\"img-responsive\" alt=\"\u00c9ch\u00e9ancier pour la pr\u00e9sentation des demandes durant la p\u00e9riode de transition de trois ans. Description ci-dessous.\"></a></p>\n<details>\n<summary>\u00c9ch\u00e9ancier pour la pr\u00e9sentation des demandes durant la p\u00e9riode de transition de trois ans</summary>\n<p>Publication des documents mis \u00e0 jour\u00a0: Novembre 2020</p>\n<p>Atelier automnal annuel de l'ACIA sur la r\u00e9glementation\u00a0: Novembre 2020, Novembre 2021, Novembre 2022, Novembre 2023</p>\n<p>Nouvel enregistrement (format papier \u2013 ancien r\u00e8glement)\u00a0: demandes accept\u00e9es jusqu'en mai 2021</p>\n<p>Nouvel enregistrement (papier ou Mon ACIA \u2013 nouveau r\u00e8glement)\u00a0: demandes accept\u00e9es \u00e0 partir de novembre 2020 et au-del\u00e0 de novembre 2023</p>\n<p>R\u00e9enregistrement (ancien r\u00e8glement): demandes accept\u00e9es jusqu'en novembre 2021</p>\n<p>R\u00e9enregistrement (papier \u2013 nouveau r\u00e8glement)\u00a0: demandes accept\u00e9es \u00e0 partir de novembre 2020 et au-del\u00e0 de novembre 2023</p>\n<p>R\u00e9enregistrement (Mon ACIA \u2013 nouveau r\u00e8glement)\u00a0: demandes accept\u00e9es \u00e0 partir de novembre 2021 et au-del\u00e0 de novembre 2023</p>\n<p>Webinaires de formation de l'ACIA sur les demandes pour le r\u00e9enregistrement\u00a0: Novembre 2020 et Novembre 2021</p>\n</details>\n</figure>\n\n\n<p>L'\u00e9ch\u00e9ancier pr\u00e9sent\u00e9 dans ce sch\u00e9ma offre aux promoteurs des conseils sur la fa\u00e7on de faire la transition vers le nouveau r\u00e8glement. Cet \u00e9ch\u00e9ancier est bas\u00e9 sur le fait qu'un nouvel enregistrement prend g\u00e9n\u00e9ralement entre un an et un an et demi pour \u00eatre trait\u00e9, tandis qu'un renouvellement (avec des modifications du produit, de son mode d'emploi ou de ses all\u00e9gations) prend g\u00e9n\u00e9ralement un an. Durant la p\u00e9riode de transition, les demandes d'enregistrement de nouveaux produits en vertu de l'ancien r\u00e8glement d\u00e9tournent des ressources vers des produits qui ne seront enregistr\u00e9s (si ils sont approuv\u00e9s) que pour une br\u00e8ve p\u00e9riode. En effet, tous les enregistrements accord\u00e9s en vertu de l'ancien r\u00e8glement seront annul\u00e9s \u00e0 la fin de la p\u00e9riode de transition, s'ils ne deviennent pas caducs avant cette date.</p>\n<p>Par cons\u00e9quent, l'ACIA a \u00e9labor\u00e9 cet \u00e9ch\u00e9ancier pour guider les promoteurs dans la pr\u00e9sentation de leurs demandes d'enregistrement en vertu de la <i>Loi sur les engrais</i>. Le respect de cet \u00e9ch\u00e9ancier permettra une transition en douceur vers le nouveau r\u00e8glement et r\u00e9duira au minimum les ressources consacr\u00e9es aux enregistrements vou\u00e9s \u00e0 expirer bient\u00f4t. Par exemple\u00a0: Un enregistrement pour un nouveau produit qui serait demand\u00e9 en vertu de l'ancien r\u00e8glement au cours de la deuxi\u00e8me ann\u00e9e de la p\u00e9riode de transition ne serait valide que pour six mois (ou moins), car la p\u00e9riode de transition prendra fin et l'enregistrement sera annul\u00e9. En revanche, un enregistrement pour le m\u00eame produit, demand\u00e9 cette fois en vertu du nouveau r\u00e8glement, serait valide pour cinq ans.</p>\n\n<h2>Coordonn\u00e9es</h2>\n<p>Section de l'innocuit\u00e9 des engrais<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">cfia.paso-bpdpm.acia@inspection.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}