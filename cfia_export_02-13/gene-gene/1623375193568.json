{
    "dcr_id": "1623375193568",
    "title": {
        "en": "Requirements for making a nutrient content claim",
        "fr": "Exigences pour faire des all\u00e9gations relatives \u00e0 la teneur nutritive"
    },
    "html_modified": "2024-02-13 9:55:12 AM",
    "modified": "2022-07-06",
    "issued": "2022-07-06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/lble_industry_claims_nutrient_requirement_1623375193568_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/lble_industry_claims_nutrient_requirement_1623375193568_fra"
    },
    "ia_id": "1623375305889",
    "parent_ia_id": "1389905991605",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling",
        "fr": "\u00c9tiquetage"
    },
    "commodity": {
        "en": "Egg, shell|Alcoholic beverages|Honey|Grain-based foods|Fruits and vegetables, processed products|Dairy products|Fruits and vegetables, fresh|Fish and seafood|Egg, processed products|Meat products and food animals|Maple products",
        "fr": "Oeufs, en coquille|Boissons alcoolis\u00e9es|Miel|Aliments \u00e0 base de grains|Fruits et l\u00e9gumes, produits  transform\u00e9s|Produits laitiers|Fruits et l\u00e9gumes, frais|Poissons et fruits de mer|Oeufs, produits transform\u00e9s|Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande|Produits d'\u00e9rable"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1389905991605",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Requirements for making a nutrient content claim",
        "fr": "Exigences pour faire des all\u00e9gations relatives \u00e0 la teneur nutritive"
    },
    "label": {
        "en": "Requirements for making a nutrient content claim",
        "fr": "Exigences pour faire des all\u00e9gations relatives \u00e0 la teneur nutritive"
    },
    "templatetype": "content page 1 column",
    "node_id": "1623375305889",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1389905941652",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/nutrient-content/making-a-nutrient-content-claim/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/allegations-relatives-a-la-teneur-nutritive/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Requirements for making a nutrient content claim",
            "fr": "Exigences pour faire des all\u00e9gations relatives \u00e0 la teneur nutritive"
        },
        "description": {
            "en": "Nutrient content claims must comply with the General principles for labelling and advertising. Specific nutrient content claims are permitted provided the corresponding conditions are met.",
            "fr": "Les all\u00e9gations relatives \u00e0 la teneur nutritive doivent respecter les Principes g\u00e9n\u00e9raux en mati\u00e8re d'\u00e9tiquetage et de publicit\u00e9. Les all\u00e9gations relatives \u00e0 la teneur nutritive sont autoris\u00e9es pourvu que les crit\u00e8res correspondants soient respect\u00e9s."
        },
        "keywords": {
            "en": "food, food health, food labels, advertisements, food products, requirements",
            "fr": "aliments, sant\u00e9 des aliments, &eacute;tiquetage, publicit&eacute; des aliments, produits alimentaires, exigences"
        },
        "dcterms.subject": {
            "en": "food,consumers,labelling,food labelling,inspection,food processing",
            "fr": "aliment,consommateur,\u00e9tiquetage,\u00e9tiquetage des aliments,inspection, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-07-06",
            "fr": "2022-07-06"
        },
        "modified": {
            "en": "2022-07-06",
            "fr": "2022-07-06"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Requirements for making a nutrient content claim",
        "fr": "Exigences pour faire des all\u00e9gations relatives \u00e0 la teneur nutritive"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>All aspects of food labels and advertisements are considered in the overall impression attributed to food products. Nutrient content claims that appear on food labels or advertisements contribute toward this overall impression. For this reason, nutrient content claims must comply with the <a href=\"/food-labels/labelling/industry/general-principles/eng/1627406110683/1627407577542\">general principles for labelling and advertising</a>.</p>\n<p>The <i>Food and Drug Regulations</i> (FDR) generally prohibit, in labelling or advertising, express or implied representations that characterize the energy value of a food or the amount of nutrient contained in the food (that is to say, it is not permissible to make a nutrient content claim) [B.01.502(1), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]. However, these regulations also provide the following exceptions to this overall rule:</p>\n\n<h2>Exceptions</h2>\n<p>The table following section B.01.513 sets out specific claims that are permitted provided the corresponding conditions are met [B.01.503, <abbr title=\"Food and Drug Regulations\">FDR</abbr>]. More information on these claims can be found in <a href=\"/food-labels/labelling/industry/nutrient-content/making-a-claim/eng/1389906998230/1389907154252\">Making a nutrient content claim</a> and <a href=\"/food-labels/labelling/industry/nutrient-content/specific-claim-requirements/eng/1627085614476/1627085788924\">Specific nutrient content claim requirements</a>.</p>\n<p>Permitted nutrient content claims with respect to vitamins and minerals are regulated by Part D of the <abbr title=\"Food and Drug Regulations\">FDR</abbr> and are not covered in the table following B.01.513. For more information on the conditions for making these claims, please refer to <a href=\"/food-labels/labelling/industry/nutrient-content/specific-claim-requirements/eng/1627085614476/1627085788924#a13\">Vitamin and mineral nutrient claims</a>.</p>\n<p>Some <a href=\"/food-labels/labelling/industry/nutrient-content/quantitative-declarations/eng/1389907377749/1389907436773\">quantitative declarations outside the Nutrition Facts table</a> are permitted, with certain associated conditions [B.01.301, <abbr title=\"Food and Drug Regulations\">FDR</abbr>]. Further information is available in the appropriate section.</p>\n<p>Statements with respect to proteins may also be permitted provided the food meets certain conditions [B.01.305, <abbr title=\"Food and Drug Regulations\">FDR</abbr>]. Please refer to <a href=\"/food-labels/labelling/industry/nutrient-content/specific-claim-requirements/eng/1627085614476/1627085788924#a3\">Protein claims</a> for further information.</p>\n<p>Other cases in which nutrient content claims may be permissible are: </p>\n<ul class=\"lst-spcd\">\n<li>representations for which there are provisions in the <abbr title=\"Food and Drug Regulations\">FDR</abbr>, such as prescribed common names like \"unsweetened chocolate\" or \"mineral water\" and prescribed statements like \"X% meat protein\" on meats with added phosphates [B.01.502(2)(a), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>statements required by section 273 of the <i>Safe Food for Canadians Regulations</i> (SFCR) (for example, \"packed in light syrup\" on canned fruit) [B.01.502(2)(b), FDR]</li>\n<li>statements required by Column 1 of  Table 2: Standards for Specific Edible Meat Products, <a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-standards-of-identity-volume-7/eng/1521204102134/1521204102836\">Canadian Standards of Identity, Volume 7 - Meat Products</a> (for example, \"extra lean ground beef\", \"lean ground pork\") [B.01.502(2)(c), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>statements that characterize the amount of lactose in a food (for example, \"lactose-free\" when lactose is non-detectable in the food) [B.01.502(2)(d), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]. Please refer to \"Lactose-free\" claims under <a href=\"/eng/1625516122300/1625516122800#s7c2\">Negative claims pertaining to the absence or non-addition of a substance</a> for more information</li>\n<li>statements regarding the addition of salt or sugars to a food (for example, \"salted nuts\", \"sweetened\", \"X% sugar added\", \"dextrose or glucose added\") other than any statement or claim set out in column 4 of the table following section B.01.513, <abbr title=\"Food and Drug Regulations\">FDR</abbr> [B.01.502(2)(e) and (f), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>a representation that characterizes the amount of starch in a food intended solely for children under\u00a02 years of age. Refer to <a href=\"/food-labels/labelling/industry/nutrient-content/children-under-two-years-of-age/eng/1389908330046/1389908390338\">Nutrient content claims on food intended solely for children under\u00a02 years of age</a> [B.01.502(2)(g), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>some established common names such as \"defatted soybeans\", \"high fructose corn syrup\" and \"demineralized water\" [B.01.502(2)(h), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>a representation that characterizes the amount of fatty acid in a vegetable oil and forms part of its common name (for example, \"high oleic sunflower oil\") [B.01.502(2)(i), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>statements that characterize the percentage alcohol in a beverage if it contains more than 0.5% alcohol (for example, \"14% alcohol by volume\") [B.01.502(2)(j), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n<li>the term \"light salted fish\" [B.01.502(2)(k), <abbr title=\"Food and Drug Regulations\">FDR</abbr>], and</li>\n<li>the term \"lean\" (in English only) when related to a prepackaged meal for use in weight-reduction or weight-maintenance diets [B.01.502(2)(l), <abbr title=\"Food and Drug Regulations\">FDR</abbr>]</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Tous les aspects li\u00e9s \u00e0 l'\u00e9tiquetage et \u00e0 la publicit\u00e9 des aliments sont pris en consid\u00e9ration dans l'impression globale attribu\u00e9e aux produits alimentaires. Les all\u00e9gations relatives \u00e0 la teneur nutritive qui figurent sur les \u00e9tiquettes ou les annonces d'aliments contribuent \u00e0 cette impression globale. Ainsi, les all\u00e9gations relatives \u00e0 la teneur nutritive doivent respecter les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/principes-generaux/fra/1627406110683/1627407577542\">principes g\u00e9n\u00e9raux en mati\u00e8re d'\u00e9tiquetage et de publicit\u00e9</a>.</p>\n\n<p>R\u00e8gle g\u00e9n\u00e9rale, le <i>R\u00e8glement sur les aliments et drogues</i> (RAD) interdit sur l'\u00e9tiquette ou dans l'annonce d'un aliment, toute d\u00e9claration, expresse ou implicite, caract\u00e9risant la valeur \u00e9nerg\u00e9tique de l'aliment ou sa teneur en un \u00e9l\u00e9ment nutritif (c'est-\u00e0-dire, il est interdit de formuler des all\u00e9gations relatives \u00e0 la teneur nutritive) [B.01.502(1), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>]. Toutefois, le r\u00e8glement pr\u00e9voit aussi les exceptions suivantes\u00a0:</p>\n\n<h2>Exceptions</h2>\n\n<p>Le tableau qui suit l'article B.01.513 du <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr> \u00e9num\u00e8re les all\u00e9gations autoris\u00e9es pourvu que les crit\u00e8res correspondants soient respect\u00e9s [B.01.503, <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>]. Des pr\u00e9cisions au sujet de ces all\u00e9gations sont fournies dans les sections <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/formuler-une-allegation/fra/1389906998230/1389907154252\">Formuler une all\u00e9gation relative \u00e0 la teneur nutritive</a> et <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-particulieres-concernant-les-allegations/fra/1627085614476/1627085788924\">Exigences particuli\u00e8res li\u00e9es aux all\u00e9gations relatives \u00e0 la teneur nutritive</a>.</p>\n<p>Les all\u00e9gations autoris\u00e9es relatives \u00e0 la teneur nutritive en ce qui concerne les vitamines et les min\u00e9raux sont r\u00e9gies par la partie D du <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr> et ne sont pas vis\u00e9es par le tableau suivant l'article B.01.513. Pour de plus amples renseignements sur les crit\u00e8res s'appliquant \u00e0 ces all\u00e9gations, consultez la section des <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-particulieres-concernant-les-allegations/fra/1627085614476/1627085788924#a13\">All\u00e9gations relatives aux vitamines et aux min\u00e9raux nutritifs</a>.</p>\n<p>Certaines <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/mentions-quantitatives/fra/1389907377749/1389907436773\">mentions quantitatives \u00e0 l'ext\u00e9rieur du tableau de la valeur nutritive</a> sont autoris\u00e9es sous r\u00e9serve de certains crit\u00e8res [B.01.301, <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>]. De plus amples renseignements sont fournis dans la section pertinente.</p>\n<p>Les all\u00e9gations relatives aux prot\u00e9ines peuvent aussi \u00eatre autoris\u00e9es si l'aliment r\u00e9pond \u00e0 certains crit\u00e8res [B.01.305, <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>]. Consultez <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/exigences-particulieres-concernant-les-allegations/fra/1627085614476/1627085788924#a3\">All\u00e9gations relatives aux prot\u00e9ines</a> pour de plus amples renseignements.</p>\n<p>Des all\u00e9gations relatives \u00e0 la teneur nutritive peuvent aussi \u00eatre admissibles dans les cas suivants\u00a0:</p>\n<ul class=\"lst-spcd\">\n<li>les mentions prescrites au <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>, dont les noms usuels prescrits tels que \u00ab\u00a0chocolat non sucr\u00e9\u00a0\u00bb ou \u00ab\u00a0eau min\u00e9rale\u00a0\u00bb, ou les mentions r\u00e9glementaires telles que \u00ab\u00a0X\u00a0% de prot\u00e9ine de viande\u00a0\u00bb sur les viandes avec sels de phosphate ajout\u00e9s [B.01.502(2)a), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n\n<li>les mentions exig\u00e9es \u00e0 l'article\u00a0273 du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC) (par exemple, \u00ab\u00a0emball\u00e9s dans du sirop l\u00e9ger\u00a0\u00bb pour les fruits en conserve) [B.01.502(2)b), RAD];</li>\n\n<li>les mentions requises \u00e0 la colonne 1 du Tableau 2\u00a0: Normes pour des produits de viande comestibles sp\u00e9cifiques, <a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/normes-d-identite-canadiennes-volume-7/fra/1521204102134/1521204102836\">Normes d'identit\u00e9 canadiennes, Volume 7 \u2013 Produits de viande</a> (par exemple, \u00ab\u00a0b\u0153uf hach\u00e9 extra maigre\u00a0\u00bb, \u00ab\u00a0porc hach\u00e9 maigre\u00a0\u00bb, <abbr title=\"et cetera\">etc.</abbr>) [B.01.502(2)c), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n\n<li>les mentions qui caract\u00e9risent la teneur en lactose dans un aliment (par exemple, \u00ab\u00a0sans lactose\u00a0\u00bb lorsque le lactose n'est pas d\u00e9tectable dans l'aliment) [B.01.502(2)d), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>]. Consultez les all\u00e9gations \u00ab\u00a0sans lactose\u00a0\u00bb dans la section <a href=\"/fra/1625516122300/1625516122800#s7c2\">All\u00e9gations n\u00e9gatives relatives \u00e0 l'absence ou au non-ajout d'une substance</a> pour de plus amples renseignements; </li>\n\n<li>les mentions relatives \u00e0 l'ajout de sel ou de sucre dans un aliment (par exemple, \u00ab\u00a0noix sal\u00e9es\u00a0\u00bb, \u00ab\u00a0sucr\u00e9\u00a0\u00bb, \u00ab\u00a0X\u00a0% de sucre ajout\u00e9\u00a0\u00bb, \u00ab\u00a0dextrose ou glucose ajout\u00e9\u00a0\u00bb) autres que les all\u00e9gations \u00e9num\u00e9r\u00e9es \u00e0 la colonne 4 du tableau suivant l'article B.01.513 du <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr> [B.01.502(2)e) et f), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n\n<li>une mention relative \u00e0 la teneur en amidon dans un aliment destin\u00e9 exclusivement aux enfants \u00e2g\u00e9s de moins de 2 ans. Consultez <a href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/enfants-ages-de-moins-de-deux-ans/fra/1389908330046/1389908390338\">All\u00e9gations relatives \u00e0 la teneur nutritive sur les aliments destin\u00e9s exclusivement aux enfants \u00e2g\u00e9s de moins de 2 ans</a> [B.01.502(2)g), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n<li>certains noms usuels reconnus comme \u00ab\u00a0graines de soja d\u00e9graiss\u00e9es\u00a0\u00bb, \u00ab\u00a0sirop de ma\u00efs \u00e0 teneur \u00e9lev\u00e9e en fructose\u00a0\u00bb et \u00ab\u00a0eau d\u00e9min\u00e9ralis\u00e9e\u00a0\u00bb [B.01.502(2)h), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n<li>les mentions qui caract\u00e9risent la teneur en acides gras d'une huile v\u00e9g\u00e9tale et qui font partie de son nom usuel (par exemple, \u00ab\u00a0huile de tournesol \u00e0 teneur \u00e9lev\u00e9e en acide ol\u00e9ique\u00a0\u00bb) [B.01.502(2)i), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n<li>les mentions qui caract\u00e9risent le pourcentage d'alcool dans une boisson si celle-ci contient plus de 0,5\u00a0% d'alcool (par exemple, \u00ab\u00a014\u00a0% d'alcool par volume\u00a0\u00bb) [B.01.502(2)j), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n<li>la d\u00e9claration \u00ab\u00a0l\u00e9g\u00e8rement sal\u00e9\u00a0\u00bb faite \u00e0 l'\u00e9gard du poisson [B.01.502(2)k), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>];</li>\n<li>le terme \u00ab\u00a0<span lang=\"en\">lean</span>\u00a0\u00bb lorsqu'il se rapporte \u00e0 un repas pr\u00e9emball\u00e9 consomm\u00e9 dans le cadre d'un r\u00e9gime amaigrissant ou de maintien du poids [B.01.502(2)l), <abbr title=\"R\u00e8glement sur les aliments et drogues\">RAD</abbr>].</li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}