{
    "dcr_id": "1664905618340",
    "title": {
        "en": "Veterinarians: Providing guidance on biosecurity and identifying African swine fever",
        "fr": "V\u00e9t\u00e9rinaires : Fournir des directives en mati\u00e8re de bios\u00e9curit\u00e9 et identifier la peste porcine africaine"
    },
    "html_modified": "14-10-2022",
    "modified": "13-10-2022",
    "issued": "4-10-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/african_swine_fever_topic_1664905618340_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/african_swine_fever_topic_1664905618340_fra"
    },
    "parent_ia_id": "1306983373952",
    "ia_id": "1664905717378",
    "parent_node_id": "1306983373952",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Veterinarians: Providing guidance on biosecurity and identifying African swine fever",
        "fr": "V\u00e9t\u00e9rinaires : Fournir des directives en mati\u00e8re de bios\u00e9curit\u00e9 et identifier la peste porcine africaine"
    },
    "label": {
        "en": "Veterinarians: Providing guidance on biosecurity and identifying African swine fever",
        "fr": "V\u00e9t\u00e9rinaires : Fournir des directives en mati\u00e8re de bios\u00e9curit\u00e9 et identifier la peste porcine africaine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1664905717378",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306983245302",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/veterinarians/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/veterinaires/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Veterinarians: Providing guidance on biosecurity and identifying African swine fever",
            "fr": "V\u00e9t\u00e9rinaires : Fournir des directives en mati\u00e8re de bios\u00e9curit\u00e9 et identifier la peste porcine africaine"
        },
        "description": {
            "en": "Veterinarians have an important role to play in detecting African swine fever (ASF) early.",
            "fr": "Les v\u00e9t\u00e9rinaires ont un r\u00f4le important \u00e0 jouer dans la d\u00e9tection pr\u00e9coce de la peste porcine africaine (PPA)."
        },
        "keywords": {
            "en": "African swine fever, veterinarians, reportable disease, reportable disease regulations,  contagious viral disease of swine",
            "fr": "Peste porcine africaine, v\u00e9t\u00e9rinaires, maladie a d\u00e9claration obligatoire, maladie virale contagieuse du porc"
        },
        "dcterms.subject": {
            "en": "animal inspection,animal diseases,animal health",
            "fr": "inspection des animaux,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-10-14",
            "fr": "2022-10-14"
        },
        "modified": {
            "en": "2022-10-13",
            "fr": "2022-10-13"
        },
        "type": {
            "en": "news publication",
            "fr": "publication d'information"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Veterinarians: Providing guidance on biosecurity and identifying African swine fever",
        "fr": "V\u00e9t\u00e9rinaires : Fournir des directives en mati\u00e8re de bios\u00e9curit\u00e9 et identifier la peste porcine africaine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Veterinarians have an important role to play in: </p>\n\t\n<ul>\n<li>detecting African swine fever (ASF) early</li>\n<li>supporting and educating their clients to reduce the impact of the disease if it is found in Canada. </li> \n</ul>\n\t\n<p>ASF is deadly for pigs and there are currently no effective treatments or vaccines. The disease continues to spread worldwide, threatening pig health and welfare. ASF can't be transmitted to humans, but a detection in Canada would have devastating impacts on Canada's swine population. </p>\n\t\n<p>Early detection is critical. If you suspect ASF, immediately contact one of CFIA's <a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a>.  </p>\n\t\n<p>For more information, read our <a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/fact-sheet/eng/1306993248674/1306993787261\">African swine fever fact sheet</a>. </p>\n\t\n<h2>Recommendations for veterinarians </h2>\n\t\n<h3>Recognize the signs of ASF</h3>\n\t\n<p>The clinical signs of ASF range from mild to severe and may suddenly appear or cause chronic illness. There are no signs of ASF that are unique. </p>\n\t\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-4\">\n<a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/close-your-gate-on-african-swine-fever/spot-the-signs-of-african-swine-fever-asf-/eng/1611086348221/1611086348456\"><img src=\"https://inspection.canada.ca/DAM/DAM-animals-animaux/STAGING/images-images/asf_biosecurity_know_the_signs_360x203_1611323297872_eng.jpg%20\" class=\"img-responsive thumbnail\" alt=\"Infographic: Spot the signs of African swine fever (ASF)\"></a></div>\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/close-your-gate-on-african-swine-fever/spot-the-signs-of-african-swine-fever-asf-/eng/1611086348221/1611086348456\">Infographic: Spot the signs of African swine fever (ASF)</a></strong></p>\n<div class=\"col-sm-8\">\t\n<p>ASF virus can cause the following symptoms:</p>\n<ul>\n<li>high fever</li>\n<li>loss of appetite</li>\n<li>weakness</li>\n<li>inability to stand</li>\n<li>reddening of the skin</li>\n<li>internal bleeding</li>\n<li>vomiting and diarrhea (sometimes bloody)</li>\n<li>abortions in pregnant sows</li>\n<li>death may occur suddenly or following a period of illness</li>\n</ul>\n\n</div>\n</div>\n\t\n<h3>Take part in ASF surveillance</h3>\n<p>The disease looks similar to classical swine fever and other endemic diseases. Risk-based early detection at approved laboratories is a surveillance tool that has been implemented as part of Canada's enhanced ASF surveillance through <a href=\"https://animalhealthcanada.ca/pillar-2-preparedness-planning\">CanSpotASF</a>. It was developed by federal and provincial governments, industry and veterinarians.</p>\n\t\n<p>Through increased presence and sampling on small-scale swine farms, veterinarians can help detect ASF and prepare for a potential outbreak. If you are a veterinary practitioner and would like more information contact the <a href=\"https://cahss.ca/cahss-networks/swine\">Canadian Animal Health Surveillance System (CAHSS) Swine Network</a> at <a href=\"mailto:info@cahss.ca\" class=\"nowrap\">info@cahss.ca</a></p>\n\t\n<h3>What to do if you suspect ASF</h3>\n\t\n<p>ASF is a reportable disease under the <em>Reportable Diseases Regulations</em>. Laboratory tests performed in a CFIA or another approved lab are necessary to confirm the disease. You must immediately report suspected cases to one of <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">CFIA's Animal Health Offices</a>. It is important to: </p>\n\t\n<ul>\n<li>Collect key epidemiological information about the farm and animals</li>\n<li>Arrange for confirmatory laboratory tests performed by the CFIA or another approved lab</li>\n<li>Quarantine all animals, feed and equipment until the cause of illness is known</li>\n<li>Avoid visiting other farms until you have consulted with a CFIA official at the <a href=\"/eng/1300462382369/1300462438912\">Animal Health Office</a> </li>\n</ul>\n\t\n<h3>Encourage and follow good biosecurity practices</h3>\n\t\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-4\">\n<a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/on-farm-biosecurity/eng/1547479486729/1547479538543%20\"><img src=\"https://inspection.canada.ca/DAM/DAM-animals-animaux/STAGING/images-images/on-farm_bio_asf_1556041285195_eng.png\" class=\"img-responsive thumbnail\" alt=\"\"></a>\n</div>\n<div class=\"col-sm-8\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/on-farm-biosecurity/eng/1547479486729/1547479538543\">Infographic: On-farm biosecurity</a></strong></p>\n\n<p>ASF can have a devastating effect on Canada's swine herd and the livelihood of farmers. <a href=\"/animal-health/terrestrial-animals/biosecurity/eng/1299868055616/1320534707863\">On-farm standards</a> and <a href=\"/animal-health/terrestrial-animals/biosecurity/tools/checklist/eng/1362944949857/1362945111651\">biosecurity practices</a> are key to mitigate risk and to prevent diseases from developing and spreading.</p>\n\t\n<p>ASF can spread (directly and indirectly) between sick and healthy pigs (domestic and wild), as well as from contaminated farm equipment, feed and clothing. Veterinarians moving between farms can also inadvertently spread infection if they were recently in an area that was infected with ASF. </p>\t</div>\n\n</div>\n\t\n<h2>Resources from third parties </h2>\n\t\n<ul>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">World Organisation for Animal Health awareness tools</a>:\n<ul>\n<li><a href=\"https://trello.com/c/SNXeEugp/56-infographic-en-es-fr-ru-ch\">Poster for veterinarians: ASF \u2013 You can stop the spread - PDF</a></li>\n</ul>\n</li>\n<li><a href=\"http://www.cfsph.iastate.edu/DiseaseInfo/disease-images.php?name=african-swine-fever&amp;lang=in\">Disease images of African swine fever</a></li>\n<li><a href=\"https://animalhealthcanada.ca/pillar-2-preparedness-planning\">CanSpotASF</a>:\n<ul>\n<li><a href=\"https://animalhealthcanada.ca/pdfs/CanSpotASFDescriptionforVeterinarians_EN%20Sep2020.pdf\">CanSpotASF description for veterinarians \u2013 PDF (1.26\u00a0mb)</a> </li>\n</ul>\n</li>\n<li><a href=\"https://animalhealthcanada.ca/asf-resources-\">Animal Health Canada \u2013 ASF Resources</a> </li>\n<li><a href=\"https://www.canadianveterinarians.net/veterinary-resources/practice-tools/african-swine-fever/\">Canadian Veterinary Medical Association: African swine fever</a></li>\n<li>Canadian Veterinary Medical Association: Three-Part Series for Veterinarians on ASF presented by the CFIA\n<ul>\n<li><a href=\"https://www.youtube.com/watch?v=Brv0gu39SQk\">Part 1: Disease Overview and Recognition</a></li>\n<li><a href=\"https://www.youtube.com/watch?v=P_yPtaVy-iA\">Part 2: Disease Response</a></li>\n<li><a href=\"https://www.youtube.com/watch?v=Yn2yh0HsDbA\">Part 3: Prevention and Preparedness</a></li>\n</ul>\n</li>\n<li><a href=\"https://www.cpc-ccp.com/biosecurity\">National Swine Farm-Level Biosecurity Standard</a></li>\n<li><a href=\"https://www.cpc-ccp.com/cshin\">Canadian Pork Council: Canadian Swine Health Intelligence Network</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les v\u00e9t\u00e9rinaires ont un r\u00f4le important \u00e0 jouer dans : </p>\n\t\n<ul>\n<li>la d\u00e9tection pr\u00e9coce de la peste porcine africaine (PPA)</li>\n<li>le soutien et l'\u00e9ducation de leurs clients afin de r\u00e9duire les r\u00e9percussions de la maladie si elle se trouve au Canada. </li> \n \n</ul>\n\t\n<p>La PPA est mortelle pour les porcs et il n'existe actuellement aucun traitement ou vaccin efficace. La maladie continue de se propager dans le monde entier, mena\u00e7ant la sant\u00e9 et le bien-\u00eatre des cochons. La PPA ne peut pas \u00eatre transmise aux humains, mais une d\u00e9tection au Canada entra\u00eenerait des r\u00e9percussions d\u00e9vastatrices sur la population porcine du pays. </p>\n\t\n<p>Une d\u00e9tection pr\u00e9coce est essentielle. Si vous soup\u00e7onnez la PPA, communiquez avec l'un des <a href=\"/fra/1300462382369/1300462438912\">bureaux de sant\u00e9 animale</a>.  </p>\n\t\n<p>Pour plus de renseignements, lisez notre <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fiche-de-renseignements/fra/1306993248674/1306993787261\">fiche de renseignements - peste porcine africaine</a>. </p>\n\t\n<h2>Recommandations \u00e0 l'intention des v\u00e9t\u00e9rinaires  </h2>\n\t\n<h3>Reconna\u00eetre les signes de la PPA</h3>\n\t\n<p>Les signes cliniques de la PPA vont de l\u00e9gers \u00e0 s\u00e9v\u00e8res et peuvent appara\u00eetre soudainement ou causer une maladie chronique. Il n'y a aucun signe de PPA qui soit unique. </p>\n\t\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-4\">\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fermez-votre-barriere-a-la-peste-porcine-africaine/reperez-les-signes-de-la-peste-porcine-africaine-p/fra/1611086348221/1611086348456\"><img src=\"https://inspection.canada.ca/DAM/DAM-animals-animaux/STAGING/images-images/asf_biosecurity_know_the_signs_360x203_1611323297872_fra.jpg%20\" class=\"img-responsive thumbnail\" alt=\"Infographie : Rep\u00e9rez les signes de la peste porcine africaine (PPA)\"></a></div>\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fermez-votre-barriere-a-la-peste-porcine-africaine/reperez-les-signes-de-la-peste-porcine-africaine-p/fra/1611086348221/1611086348456\">Infographie : Rep\u00e9rez les signes de la peste porcine africaine (PPA)</a></strong></p>\n<div class=\"col-sm-8\">\t\n<p>Le virus de la PPA peut provoquer les sympt\u00f4mes suivants\u00a0:</p>\n<ul>\n<li>fi\u00e8vre \u00e9lev\u00e9e</li>\n<li>perte d'app\u00e9tit</li>\n<li>faiblesse</li>\n<li>incapacit\u00e9 de se tenir debout</li>\n<li>rougissement de la peau</li>\n<li>saignement interne</li>\n<li>vomissements et diarrh\u00e9e (parfois sanglante)</li>\n<li>avortement chez les truies enceintes</li>\n<li>le d\u00e9c\u00e8s peut survenir soudainement ou apr\u00e8s une p\u00e9riode de maladie</li>\n\n</ul>\n\n</div>\n</div>\n\t\n<h3>Participer \u00e0 la surveillance de la PPA</h3>\n<p>La maladie ressemble \u00e0 la peste porcine classique et \u00e0 d'autres maladies end\u00e9miques.  La d\u00e9tection pr\u00e9coce fond\u00e9e sur le risque dans les laboratoires approuv\u00e9s est un outil de surveillance qui a \u00e9t\u00e9 mis en \u0153uvre dans le cadre de la surveillance accrue de la PPA au Canada par l'entremise de <a href=\"https://animalhealthcanada.ca/fr/pillar-2-preparedness-planning\">CanaVeillePPA</a>. Il a \u00e9t\u00e9 \u00e9labor\u00e9 par les gouvernements f\u00e9d\u00e9ral et provinciaux, l'industrie et les v\u00e9t\u00e9rinaires.</p>\n\t\n<p>Gr\u00e2ce \u00e0 une pr\u00e9sence et \u00e0 un \u00e9chantillonnage accrus dans les petites exploitations porcines, les v\u00e9t\u00e9rinaires peuvent aider \u00e0 d\u00e9tecter la peste porcine africaine et \u00e0 se pr\u00e9parer \u00e0 une \u00e9ventuelle \u00e9closion. Si vous \u00eates v\u00e9t\u00e9rinaire et que vous d\u00e9sirez obtenir plus de renseignements sur ce projet, veuillez communiquer avec le R\u00e9seau porcin du <a href=\"https://cahss.ca/cahss-networks/swine?l=fr-CA\">Syst\u00e8me canadien de surveillance de la sant\u00e9 animale (SCSSA) </a> \u00e0 l'adresse suivante\u00a0: <a href=\"mailto:info@cahss.ca\" class=\"nowrap\">info@cahss.ca</a></p>\n\t\n<h3>Que faire si vous soup\u00e7onnez la PPA</h3>\n\t\n<p>La PPA est une maladie \u00e0 d\u00e9claration obligatoire en vertu du <em>R\u00e8glement sur les maladies \u00e0 d\u00e9claration obligatoire</em>. Des tests de laboratoire effectu\u00e9s dans un laboratoire de l'ACIA ou un autre laboratoire approuv\u00e9 sont n\u00e9cessaires pour confirmer la maladie. Vous devez imm\u00e9diatement signaler les cas suspects \u00e0 l'un des <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureaux de sant\u00e9 animale</a> de l'ACIA. Il est important de :</p>\n\t\n<ul>\n<li>Recueillir des renseignements \u00e9pid\u00e9miologiques cl\u00e9s sur la ferme et les animaux</li>\n<li>Organiser des tests de laboratoire effectu\u00e9s par l'ACIA ou un autre laboratoire approuv\u00e9</li>\n<li>Mettre en quarantaine tous les animaux, les aliments pour animaux et l'\u00e9quipement jusqu'\u00e0 ce que la cause de la maladie soit connue</li>\n<li>\u00c9vitez de visiter d'autres fermes jusqu'\u00e0 ce que vous ayez communiqu\u00e9 avec un agent de l'ACIA au <a href=\"/fra/1300462382369/1300462438912\">Bureau de sant\u00e9 animale</a> </li>\n</ul>\n\t\n<h3>Encourager et suivre les bonnes pratiques de bios\u00e9curit\u00e9</h3>\n\t\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-4\">\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/biosecurite-pour-les-fermes/fra/1547479486729/1547479538543%20\"><img src=\"https://inspection.canada.ca/DAM/DAM-animals-animaux/STAGING/images-images/on-farm_bio_asf_1556041285195_fra.png\" class=\"img-responsive thumbnail\" alt=\"Infographie\u00a0: Bios\u00e9curit\u00e9 pour les fermes\"></a></div>\n<div class=\"col-sm-8\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/biosecurite-pour-les-fermes/fra/1547479486729/1547479538543\">Infographie\u00a0: Bios\u00e9curit\u00e9 pour les fermes</a></strong></p>\n\n<p>La PPA peut avoir un effet d\u00e9vastateur sur le troupeau de porcs du Canada et les moyens de subsistance des fermiers. Les <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/fra/1299868055616/1320534707863\">normes pour les fermes</a> et les  <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/outils/liste-de-verification/fra/1362944949857/1362945111651\">pratiques de bios\u00e9curit\u00e9</a> sont essentielles pour att\u00e9nuer les risques et pr\u00e9venir le d\u00e9veloppement et la propagation des maladies.</p>\n\t\n<p>La PPA peut se propager (directement et indirectement) entre des porcs malades et sains (domestiques et sauvages), ainsi qu'entre des \u00e9quipements agricoles, des aliments pour animaux et des v\u00eatements contamin\u00e9s. Les v\u00e9t\u00e9rinaires qui se d\u00e9placent d'une ferme \u00e0 l'autre peuvent \u00e9galement propager par inadvertance l'infection s'ils se trouvaient r\u00e9cemment dans une zone infect\u00e9e par la PPA </p>\t\n</div>\n</div>\n\n\t\n<h2>Ressources de tiers </h2>\n\t\n<ul>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">Outils de sensibilisation de l'Organisation mondiale de la sant\u00e9 animale</a>\n<ul>\n<li><a href=\"https://trello.com/c/SNXeEugp/56-infographic-en-es-fr-ru-ch\">Affiche pour les v\u00e9t\u00e9rinaires : Vous pouvez freiner la transmission de la PPA \u2013 PDF</a></li>\n</ul>\n</li>\n<li><a href=\"http://www.cfsph.iastate.edu/DiseaseInfo/disease-images.php?name=african-swine-fever&amp;lang=in\">Images de la maladie de la peste porcine africaine</a></li>\n<li><a href=\"https://animalhealthcanada.ca/fr/pillar-2-preparedness-planning\">CanaVeillePPA</a>\n<ul>\n<li><a href=\"https://animalhealthcanada.ca/pdfs/CanaVeillePPA_Description%20pour%20les%20veterinaires_FR%20Sep2020.pdf%20\">Description de CanaVeillePPA pour les v\u00e9t\u00e9rinaires \u2013 PDF (1,32\u00a0mo)</a> </li>\n</ul>\n</li>\n<li><a href=\"https://animalhealthcanada.ca/fr-asf-resources-\">Sant\u00e9 animale Canada \u2013 Ressources sur la PPA </a> </li>\n<li><a href=\"https://www.veterinairesaucanada.net/ressources-pour-les-medecins-veterinaires/outils-pour-la-pratique/peste-porcine-africaine/\">Association canadienne des m\u00e9decins v\u00e9t\u00e9rinaires : Peste porcine africaine</a></li>\n<li>Association canadienne des m\u00e9decins v\u00e9t\u00e9rinaires : S\u00e9rie en trois parties \u00e0 l'intention des v\u00e9t\u00e9rinaires sur la PPA pr\u00e9sent\u00e9e par l'ACIA (en anglais)\n<ul>\n<li><a href=\"https://www.youtube.com/watch?v=Brv0gu39SQk\">Partie 1 : Vue d'ensemble et reconnaissance de la maladie</a></li>\n<li><a href=\"https://www.youtube.com/watch?v=P_yPtaVy-iA\">Partie 2 : R\u00e9ponse \u00e0 la maladie</a></li>\n<li><a href=\"https://www.youtube.com/watch?v=Yn2yh0HsDbA\">Partie 3 : Pr\u00e9vention et pr\u00e9paration</a></li>\n</ul>\n</li>\n<li><a href=\"https://www.cpc-ccp.com/francais/biosecurity\">Norme nationale de bios\u00e9curit\u00e9 pour les fermes porcines</a></li>\n<li><a href=\"https://www.cpc-ccp.com/francais/cshin\">Conseil canadien du porc : R\u00e9seau canadien de surveillance de la sant\u00e9 porcine</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}