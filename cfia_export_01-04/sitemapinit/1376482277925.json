{
    "dcr_id": "1337025084336",
    "title": {
        "en": "Inspection modernization",
        "fr": "La modernisation de l'inspection"
    },
    "html_modified": "2012-05-14 15:51",
    "modified": "7-3-2018",
    "issued": "14-5-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_acco_modernization_1337025084336_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_acco_modernization_1337025084336_fra"
    },
    "parent_ia_id": "1374871197211",
    "ia_id": "1376482277925",
    "parent_node_id": "1374871197211",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Inspection modernization",
        "fr": "La modernisation de l'inspection"
    },
    "label": {
        "en": "Inspection modernization",
        "fr": "La modernisation de l'inspection"
    },
    "templatetype": "content page 1 column",
    "node_id": "1376482277925",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1374871172385",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/cfia-2025/inspection-modernization/",
        "fr": "/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Inspection modernization",
            "fr": "La modernisation de l'inspection"
        },
        "description": {
            "en": "Inspection modernization is having a single and consistent approach to inspection that can be applied to all regulated food, whether imported or produced domestically",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) s'appuie sur ses bases solides et renforce les approches et les outils du Canada en mati\u00e8re d'inspection \u00e0 l'\u00e9chelon f\u00e9d\u00e9ral."
        },
        "keywords": {
            "en": "accountability, inspection modernization, stakeholders",
            "fr": "Responsabilisation, Modernisation de l'inspection, intervenants"
        },
        "dcterms.subject": {
            "en": "food,consumers,government information,inspection,food inspection,government publication",
            "fr": "aliment,consommateur,information gouvernementale,inspection,inspection des aliments,publication gouvernementale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-14 15:51:27",
            "fr": "2012-05-14 15:51:27"
        },
        "modified": {
            "en": "2018-03-07",
            "fr": "2018-03-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Inspection modernization",
        "fr": "La modernisation de l'inspection"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) is building on its strong foundation and strengthening Canada's current federal inspection approaches and tools.</p>\n\n<h2>What is inspection modernization?</h2>\n\n<p>New technologies, tools and a more comprehensive approach to inspection are needed to properly manage today's risks to human, animal and plant health and the environment, and to enhance consumer protection.</p>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> held extensive consultation with front-line inspectors, consumer associations, industry and government stakeholders to develop a single and consistent inspection approach. The original intent was to consult first on food, then on a model that is applied to all inspection activities, whether related to human, animal or plant health or the environment.</p>\n\n<p>Initially it applied to all regulated food, whether imported, exported or prepared domestically for sale across provincial borders or internationally. In June 2013, the improved food inspection model, which supports the Safe Food for Canadians Action Plan to strengthen Canada's food safety system.</p>\n\n<p>Using the model as the foundation, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> has developed an <a href=\"/about-cfia/cfia-2025/inspection-modernization/integrated-agency-inspection-model/eng/1439998189223/1439998242489\">integrated Agency inspection model</a> which expands beyond food safety and consumer protection.</p>\n\n<p>To support this new approach, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is also</p>\n\n<ul>\n<li>investing in state-of-the-art training and development programs in order to maintain a professional and highly-skilled inspection workforce</li>\n\n<li>providing new technology and tools to support the work of industry and inspectors</li>\n</ul>\n\n<h2>How this affects consumers</h2>\n\n<p>Inspection modernization will allow the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to detect and prevent risks more effectively. Consumers will benefit from inspection activities that target the highest risk areas first (domestic or imported).</p>\n\n<h2>How this affects industry</h2>\n\n<p>Inspection modernization will provide Canadian companies the flexibility to design controls that demonstrate that their operations and products comply with all relevant federal regulations. It will also create a more level playing field for businesses by streamlining the inspection process.</p>\n\n<h2>What information is available?</h2>\n\n<ul>\n<li><a href=\"/about-cfia/cfia-2025/inspection-modernization/integrated-agency-inspection-model/eng/1439998189223/1439998242489\">Integrated Agency Inspection Model (iAIM)</a>\n<ul>\n<li><a href=\"/about-cfia/cfia-2025/inspection-modernization/integrated-agency-inspection-model/case-for-change/eng/1399849525345/1399849581198\">The Case for Change <abbr title=\"2\">II</abbr></a></li>\n<li><a href=\"/about-cfia/cfia-2025/inspection-modernization/integrated-agency-inspection-model/questions-and-answers/eng/1337028021452/1337028153631\">Questions and Answers: Integrated Agency Inspection Model (iAIM)</a></li>\n</ul></li>\n\n<li><a href=\"/about-cfia/cfia-2025/inspection-modernization/inspector-training/eng/1356144744048/1356145141989\">Inspector training</a></li>\n<li><a href=\"/about-cfia/cfia-2025/inspection-modernization/infographic/eng/1655322426971/1655323630495\">What to Expect When You're Inspected</a></li>\n</ul>\n\n<h2>How to stay involved</h2>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will continue to actively engage and consult all stakeholders as it finalizes the integrated Agency inspection model and implements the new inspection approach.</p>\n\n<p>To stay informed of this and other <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> initiatives to modernize its regulations, programs and services visit <a href=\"/about-cfia/cfia-2025/eng/1374871172385/1374871197211\">Transforming the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr></a>. Please subscribe to receive updates through the Agency's <a href=\"https://notification.inspection.canada.ca/\">email notification services</a>. Please select the subject \"Modernization initiatives\". If you are already a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> email notification subscriber, you will simply need to add this topic to your list.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) s'appuie sur ses bases solides et renforce les approches et les outils du Canada en mati\u00e8re d'inspection \u00e0 l'\u00e9chelon f\u00e9d\u00e9ral.</p>\n<h2>Qu'est-ce que la modernisation de l'inspection?</h2>\n<p>De nouvelles technologies, de nouveaux outils et une approche plus globale en mati\u00e8re d'inspection sont de rigueur pour bien g\u00e9rer les risques actuels qui menacent la sant\u00e9 humaine, animale et v\u00e9g\u00e9tale de m\u00eame que l'environnement, et pour augmenter la protection des consommateurs.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a men\u00e9 de vastes consultations aupr\u00e8s des inspecteurs de premi\u00e8re ligne, des associations de consommateurs, de l'industrie et des intervenants gouvernementaux pour \u00e9laborer une approche d'inspection unique et uniforme. L'objectif initial \u00e9tait de s'int\u00e9resser d'abord aux aliments, puis \u00e0 un mod\u00e8le qui s'applique \u00e0 l'ensemble des activit\u00e9s d'inspection, qu'elles se rapportent \u00e0 la sant\u00e9 humaine, animale ou v\u00e9g\u00e9tale, ou encore \u00e0 l'environnement.</p>\n<p>Elle s'appliquait initialement \u00e0 l'ensemble des aliments r\u00e9glement\u00e9s, qu'ils soient import\u00e9s, export\u00e9s ou pr\u00e9par\u00e9s au Canada \u00e0 des fins de vente dans d'autres provinces et \u00e0 l'\u00e9chelle internationale. En juin 2013, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a mis au point son mod\u00e8le am\u00e9lior\u00e9 d'inspection des aliments, qui soutient le Plan d'action pour assurer la salubrit\u00e9 des aliments au Canada visant le renforcement du syst\u00e8me de salubrit\u00e9 des aliments du Canada.</p>\n<p>En se servant de ce mod\u00e8le comme base, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a mis au point un <a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/modele-d-inspection-integre-de-l-agence/fra/1439998189223/1439998242489\">mod\u00e8le d'inspection int\u00e9gr\u00e9 de l'Agence</a> qui va au-del\u00e0 de la salubrit\u00e9 alimentaire et de la protection des consommateurs.</p>\n<p>Afin d'appuyer la nouvelle approche, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>&amp;nbsp:</p>\n<ul>\n<li>investira dans des programmes de formation et de perfectionnement \u00e0 la fine pointe afin de maintenir un effectif d'inspection professionnel et hautement qualifi\u00e9;</li>\n<li>fournira de nouvelles technologies et des outils afin d'appuyer le travail de l'industrie et des inspecteurs.</li>\n</ul>\n<h2>Quelle sera l'incidence des changements sur les consommateurs?</h2>\n<p>La modernisation de l'inspection permettra \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de d\u00e9tecter et de pr\u00e9venir les risques plus efficacement. Les consommateurs b\u00e9n\u00e9ficieront des activit\u00e9s d'inspection qui ciblent d'abord les secteurs \u00e0 risque \u00e9lev\u00e9 (produits canadiens ou import\u00e9s).</p>\n<h2>Quelle sera l'incidence des changements sur l'industrie?</h2>\n<p>La modernisation de l'inspection accordera aux entreprises canadiennes la latitude n\u00e9cessaire pour \u00e9laborer des mesures de contr\u00f4le qui d\u00e9montrent que leurs activit\u00e9s et leurs produits sont conformes \u00e0 tous les r\u00e8glements applicables du gouvernement f\u00e9d\u00e9ral. De plus, on uniformisera les r\u00e8gles du jeu auxquelles les entreprises sont assujetties en simplifiant le processus d'inspection.</p>\n<h2>Renseignements disponibles</h2>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/infographie/fra/1655322426971/1655323630495\">\u00c0 quoi s'attendre lorsque vous \u00eates inspect\u00e9</a></li>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/modele-d-inspection-integre-de-l-agence/fra/1439998189223/1439998242489\">Mod\u00e8le d'inspection int\u00e9gr\u00e9 de l'Agence</a>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/modele-d-inspection-integre-de-l-agence/bien-fonde-du-changement/fra/1399849525345/1399849581198\">Le bien-fond\u00e9 du changement <abbr title=\"2\">II</abbr></a></li>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/modele-d-inspection-integre-de-l-agence/questions-et-reponses/fra/1337028021452/1337028153631\">Questions et r\u00e9ponses&amp;nbsp: Le mod\u00e8le d'inspection int\u00e9gre\u00e9e de l'Agence (MIIA)</a></li>\n</ul>\n</li>\n<li><a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/formation-des-inspecteurs/fra/1356144744048/1356145141989\">Formation des inspecteurs</a></li>\n</ul>\n<h2>Rester inform\u00e9</h2>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> continuera de mobiliser et de consulter les intervenants tout au long de la mise au point du mod\u00e8le d'inspection int\u00e9gr\u00e9 de l'Agence et de la mise en \u0153uvre de la nouvelle approche en mati\u00e8re d'inspection.</p>\n<p>Pour rester au courant de cette initiative, ainsi que d'autres initiatives de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> visant \u00e0 moderniser ses r\u00e8glements, ses programmes et ses services, visitez <a href=\"/a-propos-de-l-acia/acia-2025/fra/1374871172385/1374871197211\">Transformer l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>. Veuillez vous inscrire au <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">service d'avis par courriel</a> de l'Agence pour recevoir des mises \u00e0 jour. Vous n'avez qu'\u00e0 s\u00e9lectionner \u00ab\u00a0initiatives de modernisation\u00a0\u00bb dans la liste des sujets. Si vous \u00eates d\u00e9j\u00e0 abonn\u00e9 \u00e0 la liste de diffusion de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, vous n'avez qu'\u00e0 ajouter ce sujet \u00e0 votre liste.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}