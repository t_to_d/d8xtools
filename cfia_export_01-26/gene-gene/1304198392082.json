{
    "dcr_id": "1304198392082",
    "title": {
        "en": "Zambia - Export requirements for fish and seafood",
        "fr": "Zambie - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "html_modified": "2024-01-26 2:20:28 PM",
    "modified": "2019-03-22",
    "issued": "2011-04-30 17:19:55",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/fispoi_exports_zambia_1304198392082_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/fispoi_exports_zambia_1304198392082_fra"
    },
    "ia_id": "1304198564576",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Fish and seafood",
        "fr": "Poissons et fruits de mer"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Zambia - Export requirements for fish and seafood",
        "fr": "Zambie - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "label": {
        "en": "Zambia - Export requirements for fish and seafood",
        "fr": "Zambie - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "templatetype": "content page 1 column",
    "node_id": "1304198564576",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/zambia-fish-and-seafood/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/zambie-le-poisson-et-les-produits-de-la-mer/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Zambia - Export requirements for fish and seafood",
            "fr": "Zambie - Exigences d'exportation pour le poisson et les produits de la mer"
        },
        "description": {
            "en": "Countries to which exports are currently made - Zambia",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Zambie"
        },
        "keywords": {
            "en": "fish, seafood, safety, QMP, HACCP, shellfish, exports, certification, Zambia",
            "fr": "poisson, produits de la mer et de la production, poisson, produits de la mer, PGQ, HACCP, mollusques, exportation, certification, Zambie"
        },
        "dcterms.subject": {
            "en": "certification,exports,seafood,agri-food industry,fish,agri-food products,food safety",
            "fr": "accr\u00e9ditation,exportation,fruits de mer,industrie agro-alimentaire,poisson,produit agro-alimentaire,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exportation d'aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-04-30 17:19:55",
            "fr": "2011-04-30 17:19:55"
        },
        "modified": {
            "en": "2019-03-22",
            "fr": "2019-03-22"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Zambia - Export requirements for fish and seafood",
        "fr": "Zambie - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"text-right\"><strong>Last update: April 30, 2018</strong></p>\n\n<h2>Product specifications</h2>\n\n<h3>Maximum levels for chemical contaminants</h3>\n\n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th>Contaminant</th>\n<th>Applicable fish products</th>\n<th>Maximum level permitted in <abbr title=\"parts per million\">ppm</abbr></th>\n</tr>\n</thead>\n<tbody>  \n<tr>\n<td>Mercury</td>\n<td>tuna</td>\n<td>0.3 (as methyl mercury)</td>\n</tr>\n<tr>\n<td>Mercury</td>\n<td>other fish</td>\n<td>0.2\u00a0(as methyl mercury)</td>\n</tr>\n<tr>\n<td>Arsenic</td>\n<td>fish protein</td>\n<td>3.5</td>\n</tr>\n<tr>\n<td>Arsenic</td>\n<td>other fish and fish products</td>\n<td>5.0</td>\n</tr>\n<tr>\n<td>Copper</td>\n<td>all fish and seafood products</td>\n<td>100.0</td>\n</tr>\n<tr>\n<td>Lead</td>\n<td>fish protein</td>\n<td>0.5</td>\n</tr>\n<tr>\n<td>Lead</td>\n<td>other fish and fish products</td>\n<td>10.0</td>\n</tr>\n<tr>\n<td>Fluorine</td>\n<td>fish protein</td>\n<td>150.0</td>\n</tr>\n<tr>\n<td>Fluorine</td>\n<td>other fish and fish products</td>\n<td>25.0</td>\n</tr>\n<tr>\n<td>Zinc</td>\n<td>all fish products</td>\n<td>100.0</td>\n</tr>\n<tr>\n<td>Sodium sulphite</td>\n<td>canned flaked tuna</td>\n<td>300.0</td>\n</tr>\n<tr>\n<td>Propyl-P-hydroxy benzoate</td>\n<td>when used as preservative</td>\n<td>1000.0</td>\n</tr>\n<tr>\n<td>Methyl-P-hydroxy benzoate</td>\n<td>when used as preservative</td>\n<td>1000.0</td>\n</tr>\n<tr>\n<td>Sodium tripolyphosphate</td>\n<td>frozen fish fillets, lobsters, crabs, clams, shrimps</td>\n<td>0.5% (total added phosphate)</td>\n</tr>\n<tr>\n<td>Sodium carbonate</td>\n<td>frozen fillets, lobsters, crabs, clams, shrimps</td>\n<td>15.0% (combination of sodium hexametaphosphate)</td>\n</tr>\n</tbody>\n</table>\n\n<h2>Documentation requirements</h2>\n\n<h3>Certificate</h3>\n\n<ul>\n<li>None specified</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"text-right\"><strong>Derni\u00e8re mise \u00e0 jour\u00a0: le 30\u00a0avril 2018</strong></p>\n\n<h2>Sp\u00e9cifications du produit</h2>\n\n<h3>Concentrations maximales des contaminants chimiques</h3>\n\n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th>Contaminant</th>\n<th>Produits du poisson concern\u00e9s</th>\n<th>Concentration maximale permise (en\u00a0ppm)</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Mercure</td>\n<td>thon</td>\n<td>0,3 (sous forme de m\u00e9thyle mercure)</td>\n</tr>\n<tr>\n<td>Mercure</td>\n<td>autres poissons</td>\n<td>0,2 (sous forme de m\u00e9thyle mercure)</td>\n</tr>\n<tr>\n<td>Arsenic</td>\n<td>prot\u00e9ine de poisson</td>\n<td>3,5</td>\n</tr>\n<tr>\n<td>Arsenic</td>\n<td>autres poissons et produits du poisson</td>\n<td>5,0</td>\n</tr>\n<tr>\n<td>Cuivre</td>\n<td>tous les poissons et produits du poisson</td>\n<td>100</td>\n</tr>\n<tr>\n<td>Plomb</td>\n<td>prot\u00e9ine de poisson</td>\n<td>0,5</td>\n</tr>\n<tr>\n<td>Plomb</td>\n<td>autres poissons et produits du poisson</td>\n<td>10,0</td>\n</tr>\n<tr>\n<td>Fluor</td>\n<td>prot\u00e9ine de poisson</td>\n<td>150</td>\n</tr>\n<tr>\n<td>Fluor</td>\n<td>autres poissons et produits du poisson</td>\n<td>25,0</td>\n</tr>\n<tr>\n<td>Zinc</td>\n<td>tous les produits du poisson</td>\n<td>100</td>\n</tr>\n<tr>\n<td>Sulfite de sodium</td>\n<td>thon \u00e9miett\u00e9 en conserve</td>\n<td>300</td>\n</tr>\n<tr>\n<td>Hydroxybenzoate de propyle</td>\n<td>employ\u00e9 comme agent de conservation</td>\n<td>1000</td>\n</tr>\n<tr>\n<td>Hydroxybenzoate de m\u00e9thyle</td>\n<td>employ\u00e9 comme agent de conservation</td>\n<td>1000</td>\n</tr>\n<tr>\n<td>Tripolyphosphate de sodium</td>\n<td>filets de poisson congel\u00e9, homard, crabe, clams et crevettes</td>\n<td>0,5\u00a0% (phosphate total ajout\u00e9)</td>\n</tr>\n<tr>\n<td>Carbonate de sodium</td>\n<td>filets congel\u00e9s, homard, crabe, clams et crevettes</td>\n<td>15,0\u00a0% (m\u00e9lange d'hexam\u00e9taphosphate de sodium)</td>\n</tr>\n</tbody>\n</table>\n\n<h2>Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Non pr\u00e9cis\u00e9e</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}