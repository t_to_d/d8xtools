{
    "dcr_id": "1607445603556",
    "title": {
        "en": "eDNA: another tool in the toolbox",
        "fr": "L'ADNe\u00a0: un autre outil de la bo\u00eete \u00e0 outils"
    },
    "html_modified": "17-12-2020",
    "modified": "17-12-2020",
    "issued": "8-12-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/c360_edna_20201211_1607445603556_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/c360_edna_20201211_1607445603556_fra"
    },
    "parent_ia_id": "1565298312402",
    "ia_id": "1607445604103",
    "parent_node_id": "1565298312402",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "eDNA: another tool in the toolbox",
        "fr": "L'ADNe\u00a0: un autre outil de la bo\u00eete \u00e0 outils"
    },
    "label": {
        "en": "eDNA: another tool in the toolbox",
        "fr": "L'ADNe\u00a0: un autre outil de la bo\u00eete \u00e0 outils"
    },
    "templatetype": "content page 1 column",
    "node_id": "1607445604103",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298312168",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/animal-health/edna/",
        "fr": "/inspecter-et-proteger/sante-animale/adne/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "eDNA: another tool in the toolbox",
            "fr": "L'ADNe\u00a0: un autre outil de la bo\u00eete \u00e0 outils"
        },
        "description": {
            "en": "eDNA, also known as environmental DNA, is the DNA naturally left behind in water or on land from such things as fallen feathers, skin particles, urine, and feces.",
            "fr": "L'ADNe, \u00e9galement appel\u00e9 ADN environnemental, est l'ADN naturellement laiss\u00e9 dans l'eau ou sur la terre qui provient d'\u00e9l\u00e9ments tels que des plumes tomb\u00e9es, des particules de peau, l'urine et les selles."
        },
        "keywords": {
            "en": "Food safety, Plant health, Animal health, Science and Innovation, video, interviews, Dr. Primal Silva",
            "fr": "Salubrit\u00e9 des aliments, Sant\u00e9 des v\u00e9g\u00e9taux, Sant\u00e9 animale, Science et innovation, vid\u00e9o, entrevues, Dr. Primal Silva"
        },
        "dcterms.subject": {
            "en": "communications,government communications,government publication,newsletters",
            "fr": "communications,communications gouvernementales,publication gouvernementale,bulletin"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-12-17",
            "fr": "2020-12-17"
        },
        "modified": {
            "en": "2020-12-17",
            "fr": "2020-12-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "eDNA: another tool in the toolbox",
        "fr": "L'ADNe\u00a0: un autre outil de la bo\u00eete \u00e0 outils"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263124160\"></div>\n\n<p>From the lab to the field, environmental DNA (eDNA) is a powerful technology that quickly detects the presence (or absence) of pathogens.</p>\n\n<figure class=\"wb-mltmd mrgn-bttm-md\"><video width=\"560\" height=\"340\" title=\"Video - eDNA: another tool in the toolbox\">\n\n<source type=\"video/youtube\" src=\"https://youtu.be/r84VIKcpEdI\">\n</source></video>\n\n<figcaption>\n\n<details id=\"inline-transcript\">\n\n<summary>eDNA: another tool in the toolbox\u00a0\u2013 Transcript</summary>\n<p class=\"mrgn-tp-md\"><strong>Narrator:</strong> eDNA, also known as environmental DNA, is the DNA naturally left behind in water or on land from such things as fallen feathers, skin particles, urine, and feces. They contain genetic footprints that can help detect the presence or absence of a plant or animal species or disease.</p>\n\n<p>CFIA, through a government program called Innovative Solutions Canada, has been working with a company to see how this technology will help our work in the future.</p>\n\n<p><strong>Dr. Primal Silva, Chief Science Operating Officer, CFIA:</strong> Programs like the Build in Canada Innovation Program, as well as Innovative Solutions Canada, they're really important programs because what it does is provides a way in which the innovation to really thrive in Canada.</p>\n\n<p>At the same time it helps, it does two things, it helps innovation and competitiveness in the country. But it also helps with the public good work, like the work that CFIA does in terms of protecting our animal resources, plant resources, and food safety. In stimulating innovation where our government and industries working together is a really good thing for our country.</p>\n\n<p><strong>Narrator:</strong> Using advancements from one innovator, Precision Biomonitoring, CFIA and Parks Canada will be able to consider gaining efficiencies by integrating environmental DNA, also known as eDNA, into surveillance programs. With eDNA solutions the presence of an individual plant or animal species or disease can be detected by analyzing the DNA of target organisms in aquatic or terrestrial environments.</p>\n\n<p><strong>Mario Thomas, Precision Biomonitoring:</strong> So what we offer is an on-site DNA solution that provides early surveillance and detection of biological threats.</p>\n\n<p>The key benefit of our solution is a rapid response time. We collect the sample on-site, we extract the DNA and we analyze it on-site and we have an answer under two hours.</p>\n\n<p><strong>Darrin Reid, Project Coordinator, Parks Canada:</strong> So we reached out to Precision Biomonitoring to see if they would help us with our invasive fish project looking at smallmouth bass and chain pickerel.</p>\n\n<p>Before eDNA, we would have to go to all of these lakes with all of our different equipment: nets, boats, angling. We also relied heavily on reports from anglers and volunteer anglers.</p>\n\n<p>Now that we have eDNA, we're still going to go in there with as much equipment as we can, nets, traps, all that. But eDNA is another tool in the toolbox, it's another confirmation.</p>\n\n<p><strong>Dr. Primal Silva:</strong> So in the work of CFIA where we are dealing with plant pathogens, we are dealing with animal pathogens and various insects that can carry diseases, and also food pathogens like salmonella, we thought this would be really good technology to try out in terms of, \"can we actually start detecting the presence of these pathogens in near real-time?\"</p>\n\n<p>So we have partnered with this company to actually test this out and the best thing about this is actually that the company has been funded by a government program in recognition of their quality of that work. Now we have a way of applying that technology for public good.</p>\n\n<p>So we are very excited about that in terms of moving ahead with this project.</p>\n\n<p><strong>Narrator:</strong> The work funded through the Innovative Solutions Canada program is now complete and has revealed that eDNA technology is a useful tool for governments and industry to expand their diagnostic capabilities.</p>\n\n<p>CFIA continues to explore how this technology can help us extend our detection work from the lab to the field, ultimately enhancing health and safety outcomes for Canadians.</p>\n\n<p>[End of recording]</p>\n\n</details>\n</figcaption>\n</figure>\n\n<h2>Learn more</h2>\n\n<ul>\n<li><a href=\"https://www.ic.gc.ca/eic/site/101.nsf/eng/00001.html\">Get funds to solve challenges</a> (Innovative Solutions Canada)</li>\n<li><a href=\"https://www.ic.gc.ca/eic/site/101.nsf/eng/00064.html\">Get funds to test your innovation</a> (Innovative Solutions Canada)</li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/biosafe-project-uses-genetic-blueprints-to-study-f/eng/1498767322868/1498767323246\">BioSAFE project uses genetic blueprints to study forest enemies</a></li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/barcode-of-life/eng/1488588993195/1488588993513\">CFIA scientists seek the Barcode of Life</a></li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263124160\"></div>\n\n<p>Du laboratoire au terrain, l'ADN environnemental (ADNe) est une technologie puissante qui permet de d\u00e9tecter rapidement la pr\u00e9sence (ou l'absence) d'agents pathog\u00e8nes.</p>\n\n<figure class=\"wb-mltmd mrgn-bttm-md\"><video width=\"560\" height=\"340\" title=\"Vid\u00e9o - L'ADNe : un autre outil de la bo\u00eete \u00e0 outils\">\n\n<source type=\"video/youtube\" src=\"https://youtu.be/9DxPdc2QFR4\">\n</source></video>\n\n<figcaption>\n\n<details id=\"inline-transcript\">\n\n<summary>L'ADNe\u00a0: un autre outil de la bo\u00eete \u00e0 outils\u00a0\u2013 Transcription</summary>\n\n<p class=\"mrgn-tp-md\"><strong>Narratrice\u00a0:</strong> L'ADNe, \u00e9galement appel\u00e9 ADN environnemental, est l'ADN naturellement laiss\u00e9 dans l'eau ou sur la terre qui provient d'\u00e9l\u00e9ments tels que des plumes tomb\u00e9es, des particules de peau, l'urine et les selles. Elles contiennent des empreintes g\u00e9n\u00e9tiques qui peuvent aider \u00e0 d\u00e9tecter la pr\u00e9sence ou l'absence d'une esp\u00e8ce ou d'une maladie v\u00e9g\u00e9tale ou animale.</p>\n\n<p>L'ACIA, par l'entremise d'un programme gouvernemental appel\u00e9 Solutions innovatrices Canada, a travaill\u00e9 avec une entreprise pour voir comment cette technologie aidera notre travail \u00e0 l'avenir.</p>\n\n<p><strong>D<sup>r</sup>\u00a0Primal Silva, chef des op\u00e9rations scientifiques, ACIA\u00a0:</strong> Des programmes comme le programme d'innovation Construire au Canada, ainsi que Solutions innovatrices Canada, sont des programmes d'une importance capitale parce qu'ils permettent de stimuler v\u00e9ritablement l'innovation au Canada.</p>\n\n<p>En fait, ils contribuent simultan\u00e9ment \u00e0 promouvoir tant l'innovation que la comp\u00e9titivit\u00e9 au pays. Sans parler de leur apport \u00e0 la r\u00e9alisation du bien public dans lequel s'inscrit le travail que l'ACIA fait en mati\u00e8re de protection de nos ressources animales et v\u00e9g\u00e9tales et de salubrit\u00e9 des aliments. La stimulation de l'innovation par la collaboration entre notre gouvernement et nos industries constitue tout un atout pour notre pays.</p>\n\n<p><strong>Narratrice\u00a0:</strong> Gr\u00e2ce aux avanc\u00e9es significatives d'un innovateur, <span lang=\"en\">Precision Biomonitoring</span>, l'ACIA et Parcs Canada pourront envisager la possibilit\u00e9 de faire des gains de productivit\u00e9 en int\u00e9grant l'ADN environnemental, aussi appel\u00e9 ADNe, aux programmes de surveillance. L'application de solutions reposant sur l'ADNe rend possible la d\u00e9tection de la pr\u00e9sence d'une plante ou d'une esp\u00e8ce animale ou encore d'une maladie en analysant l'ADN d'organismes cibles dans des milieux aquatiques ou terrestres.</p>\n\n<p><strong>Mario Thomas, <span lang=\"en\">Precision Biomonitoring</span>\u00a0:</strong> Ce que nous avons d\u00e9velopp\u00e9 est une plateforme ADN, sur le terrain, qui nous permet de faire la surveillance et la d\u00e9tection active de menaces biologiques.</p>\n\n<p>L'avantage principal de notre plateforme ADN sur le terrain c'est un temps de r\u00e9ponse tr\u00e8s rapide. Avec notre plateforme, nous allons sur le terrain. Nous prenons un pr\u00e9l\u00e8vement de l'\u00e9chantillon. Nous faisons l'extraction de l'ADN ou de l'ARN et l'analyse sur place, une identification sur place.</p>\n\n<p>En dessous de deux heures on peut avoir une r\u00e9ponse \u00e0 la question\u00a0: est-ce une menace biologique ou non?</p>\n\n<p><strong>Darrin Reid, coordonnateur de projet, Parcs Canada\u00a0:</strong> Nous avons donc approch\u00e9 <span lang=\"en\">Precision Biomonitoring</span> pour voir si cette entreprise pouvait nous aider avec notre projet de surveillance des poissons envahissants, notamment dans une perspective de protection des populations d'achigans \u00e0 petite bouche et de brochets maill\u00e9s.</p>\n\n<p>Avant l'ADNe, nous devions parcourir tous ces lacs, transportant avec nous des \u00e9quipements de toutes sortes\u00a0: embarcations, filets et agr\u00e8s de p\u00eache \u00e0 la ligne. De\u00a0plus, nous d\u00e9pendions beaucoup des signalements des p\u00eacheurs \u00e0 la ligne et des\u00a0b\u00e9n\u00e9voles.</p>\n\n<p>Maintenant que nous comptons avec l'ADNe, nous nous d\u00e9pla\u00e7ons toujours avec autant d'\u00e9quipement que nous pouvons\u00a0: des filets, des pi\u00e8ges et tout \u00e7a. Mais l'ADNe est un autre instrument de la bo\u00eete \u00e0 outils qui nous permet d'effectuer des confirmations.</p>\n\n<p><strong>D<sup>r</sup>\u00a0Primal Silva\u00a0:</strong> Donc, dans le cadre du travail de l'ACIA o\u00f9 nous avons affaire avec des agents phytopathog\u00e8nes, des agents zoopathog\u00e8nes et divers insectes susceptibles d'\u00eatre porteurs de maladies, et aussi d'agents pathog\u00e8nes d'origine alimentaire comme les salmonelles, nous avons pens\u00e9 que ce serait une technologie vraiment int\u00e9ressante \u00e0 tester pour vraiment d\u00e9celer en temps quasi r\u00e9el la pr\u00e9sence de ces agents pathog\u00e8nes.</p>\n\n<p>Nous avons donc conclu un partenariat avec cette entreprise afin de proc\u00e9der \u00e0 des tests. Ce qui a permis \u00e0 l'entreprise d'obtenir un financement dans le cadre d'un programme gouvernemental en reconnaissance de la qualit\u00e9 de son travail. Nous disposons maintenant d'une fa\u00e7on d'appliquer cette technologie dans une perspective de bien public.</p>\n\n<p>C'est vous dire jusqu'\u00e0 quel point nous sommes emball\u00e9s \u00e0 l'id\u00e9e de faire avancer ce\u00a0projet.</p>\n\n<p><strong>Narratrice\u00a0:</strong> Le travail financ\u00e9 par l'entremise du programme Solutions innovatrices Canada est maintenant termin\u00e9 et a r\u00e9v\u00e9l\u00e9 que la technologie de l'ADNe est un outil utile pour les gouvernements et l'industrie afin d'accro\u00eetre leurs capacit\u00e9s de diagnostic.\u00a0</p>\n\n<p>L'ACIA continue d'\u00e9tudier comment cette technologie peut nous aider \u00e0 \u00e9tendre notre travail de d\u00e9tection du laboratoire au terrain, ce qui, finalement, am\u00e9liorera les r\u00e9sultats en mati\u00e8re de sant\u00e9 et de s\u00e9curit\u00e9 pour les Canadiens.</p>\n\n<p>[Fin de l'enregistrement]</p>\n\n</details>\n</figcaption>\n</figure>\n\n<h2>Pour en savoir plus</h2>\n\n<ul>\n<li><a href=\"https://www.ic.gc.ca/eic/site/101.nsf/fra/00001.html\">Financement pour r\u00e9soudre des d\u00e9fis</a> (Solutions innovatrices Canada)</li>\n<li><a href=\"https://www.ic.gc.ca/eic/site/101.nsf/fra/00064.html\">Financement pour tester votre innovation</a> (Solutions innovatrices Canada)</li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/le-projet-biosafe-se-sert-de-patrons-genetiques-po/fra/1498767322868/1498767323246\">Le projet <span lang=\"en\">BioSAFE</span> se sert de patrons g\u00e9n\u00e9tiques pour \u00e9tudier les esp\u00e8ces envahissantes des for\u00eats</a></li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/code-barres-du-vivant/fra/1488588993195/1488588993513\">Les scientifiques de l'ACIA \u00e0 la recherche du Code-barres du vivant</a></li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}