{
    "dcr_id": "1391707650055",
    "title": {
        "en": "Biocontainment for facilities handling plant pests",
        "fr": "Bioconfinement pour les installations qui manipulent des phytoravageurs"
    },
    "html_modified": "2024-02-13 9:48:33 AM",
    "modified": "2019-12-10",
    "issued": "2014-02-06 12:27:31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_protect_biocon_1391707650055_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_protect_biocon_1391707650055_fra"
    },
    "ia_id": "1391707686040",
    "parent_ia_id": "1299168989280",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299168989280",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biocontainment for facilities handling plant pests",
        "fr": "Bioconfinement pour les installations qui manipulent des phytoravageurs"
    },
    "label": {
        "en": "Biocontainment for facilities handling plant pests",
        "fr": "Bioconfinement pour les installations qui manipulent des phytoravageurs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1391707686040",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299168913252",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/biocontainment/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/bioconfinement/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biocontainment for facilities handling plant pests",
            "fr": "Bioconfinement pour les installations qui manipulent des phytoravageurs"
        },
        "description": {
            "en": "Biocontainment for facilities handling plant pests",
            "fr": "Bioconfinement pour les installations qui manipulent des phytoravageurs"
        },
        "keywords": {
            "en": "Biohazard Containment and Safety, Plant Protection Act, Plant Protection Regulations, laboratories, testing, plant pests, containment standards, facility certification, imports",
            "fr": "Confinement des biorisques et s\u00e9curit\u00e9, Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, laboratoires, analyses, phytoravageurs, normes sur le confinement, certification des installations, importation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Biohazard Containment and Safety",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Confinement des biorisques et de la s\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-02-06 12:27:31",
            "fr": "2014-02-06 12:27:31"
        },
        "modified": {
            "en": "2019-12-10",
            "fr": "2019-12-10"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biocontainment for facilities handling plant pests",
        "fr": "Bioconfinement pour les installations qui manipulent des phytoravageurs"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Containment Standards for Facilities Handling Plant Pests (CSFHPP) describe the minimum acceptable physical and operational requirements for facilities working with plant pests (other than weeds). The <abbr title=\"Containment Standards for Facilities Handling Plant Pests\">CSFHPP</abbr> do not apply to soil, genetically modified plants, and biological control insects. The <abbr title=\"Containment Standards for Facilities Handling Plant Pests\">CSFHPP</abbr> outline the physical and operational requirements appropriate for the specific pest under consideration. It is important to note that while the <abbr title=\"Containment Standards for Facilities Handling Plant Pests\">CSFHPP</abbr> are primarily intended to address facilities importing plant pests, they also apply to facilities that collect or isolate plant pests within Canada.</p>\n<ul>\n<li><a href=\"/plant-health/invasive-species/biocontainment/containment-standards/eng/1412353866032/1412354048442\">Containment standards for facilities handling plant pests</a></li>\n<li><a href=\"/plant-health/invasive-species/biocontainment/addendum/eng/1359491565329/1359493049450\">Addendum for containment zones where low-risk exotic invertebrates are displayed</a></li>\n<li><a href=\"/plant-health/invasive-species/biocontainment/questions-and-answers/eng/1392497209673/1392497407493\">Containment standards</a></li>\n</ul>\n\n<p>The <abbr title=\"Containment Standards for Facilities Handling Plant Pests\">CSFHPP</abbr> are an integral part of the Canadian Food Inspection Agency's regulatory requirements for importing and handling potentially injurious organisms in Canada. These requirements are set out in the following plant protection policy directives:</p>\n\n<p><a href=\"/plant-health/invasive-species/directives/date/d-12-03/eng/1432656209220/1432751554580\">D-12-03 - Domestic requirements for potentially injurious organisms (other than plants) to prevent the spread of plant pests within Canada</a></p>\n<p class=\"mrgn-lft-xl\"><a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5851/eng/1560959683220/1560959683479\">Application for a written authorization to conduct activities on plant pests (CFIA/ACIA 5851)</a></p>\n\n<p><a href=\"/plant-health/invasive-species/directives/imports/d-12-02/eng/1432586422006/1432586423037\">D-12-02 - Import requirements for potentially injurious organisms (other than plants) to prevent the importation of plant pests in Canada</a></p>\n<p class=\"mrgn-lft-xl\"><a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5256/eng/1491930450091/1491930450578\">Application for permit to import plants and other things under the Plant Protection Act (CFIA/ACIA 5256)</a></p>\n\n<p>Import permits and written authorizations will only be issued to facilities certified as meeting the appropriate physical and operational requirements described in the <abbr title=\"Containment Standards for Facilities Handling Plant Pests\">CSFHPP</abbr>. We will work with stakeholders to assist with meeting the standard.</p>\n\n<h2>Biocontainment and certification program</h2>\n<p>The Biocontainment and Certification Program's objective is to ensure  high containment facilities are in compliance with Canadian physical and  operational containment requirements.  The purpose of the program is  to:</p>\n<ul>\n<li>Advise laboratories and other types of high containment facilities on the  appropriate handling and containment of plant pests;</li>\n<li>Advise on the planning, development, operation and certification of  containment facilities;</li>\n<li>Certify and perform inspections of high containment facilities handling or  importing  plant pest facilities (<abbr title=\"Plant Pest Containment\">PPC</abbr>2A and <abbr title=\"Plant Pest Containment\">PPC</abbr>3);</li>\n<li>Perform re-certification every 2 years for <abbr title=\"Plant Pest Containment\">PPC</abbr>2A and <abbr title=\"Plant Pest Containment\">PPC</abbr>3 facilities.</li>\n</ul>\n<p>If you are building a new facility, <a href=\"mailto:cfia.biocontainment-bioconfinement.acia@canada.ca\">please contact the Office of Biohazard Containment &amp; Safety (OBCS)</a> early in the planning phase. A Plant Pest Containment Level Assessment Checklist is available for your use in evaluating the physical components of your facility. Once completed and returned to the <abbr title=\"Office of Biohazard Containment and Safety\">OBCS</abbr>, a Biosafety Specialist will review it to determine the required <abbr title=\"Plant Pest Containment\">PPC</abbr> level and provide recommendations on how to attain the desired <abbr title=\"Plant Pest Containment\">PPC</abbr> level.</p>\n\n\n<h2>Additional information</h2>\n<p>For questions on the <abbr title=\"Plant Pest Containment\">PPC</abbr>-Display, 1 or 2 levels, including obtain a copy  of a self-assessment checklist, please contact your <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> inspection office</a> or the Invasive Alien Species  and Domestic Program section (CFIA-IAS_ACIA-EEE@inspection.gc.ca).</p>\n<p>  For questions on the <abbr title=\"Plant Pest Containment\">PPC</abbr>-2A or 3 levels, <a href=\"mailto:cfia.biocontainment-bioconfinement.acia@canada.ca\">please  contact the <abbr title=\"Office of Biohazard Containment and Safety\">OBCS</abbr></a>. </p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les Normes relatives au confinement des installations manipulant des phytoravageurs (NCIMP) d\u00e9crivent les exigences physiques et op\u00e9rationnelles minimales  que doivent respecter les installations manipulant des phytoravageurs  (autres que les mauvaises herbes). Les <abbr title=\"Normes relatives au confinement des installations manipulant des phytoravageurs\">NCIMP</abbr> ne s'appliquent pas \u00e0 la terre, aux v\u00e9g\u00e9taux g\u00e9n\u00e9tiquement modifi\u00e9s ni  aux insectes utilis\u00e9s comme agents de lutte biologique. Les exigences  physiques et op\u00e9rationnelles sont d\u00e9crites dans les <abbr title=\"Normes relatives au confinement des installations manipulant des phytoravageurs\"> NCIMP</abbr> pour chacun des ravageurs vis\u00e9s. Il est important de noter que, bien que les <abbr title=\"Normes relatives au confinement des installations manipulant des phytoravageurs\">NCIMP</abbr> visent principalement les installations qui importent des  phytoravageurs, elles s'appliquent \u00e9galement aux installations qui  recueillent ou isolent des phytoravageurs au Canada.</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/bioconfinement/normes-sur-le-confinement/fra/1412353866032/1412354048442\">Normes relatives au confinement des installations manipulant des phytoravageurs</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/bioconfinement/addenda/fra/1359491565329/1359493049450\">Addenda concernant les zones de confinement o\u00f9 les invert\u00e9br\u00e9s exotiques \u00e0 faible risque sont expos\u00e9s</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/bioconfinement/questions-et-reponses/fra/1392497209673/1392497407493\">Normes relatives au confinement</a></li>\n</ul>\n\n<p>Les <abbr title=\"Normes relatives au confinement des installations manipulant des phytoravageurs\">NCIMP</abbr> font partie int\u00e9grante des exigences de l'Agence canadienne d'inspection des aliments sur l'importation et la manipulation d'organismes potentiellement nuisibles au Canada. Ces exigences sont \u00e9nonc\u00e9es dans les Directives sur la protection des v\u00e9g\u00e9taux suivantes\u00a0:</p>\n\n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-12-03/fra/1432656209220/1432751554580\">D-12-03 - Exigences en territoire canadien, organismes potentiellement nuisibles (autres que des v\u00e9g\u00e9taux), afin d'emp\u00eacher la propagation de phytoravageurs au Canada</a></p>\n<p class=\"mrgn-lft-xl\"><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5851/fra/1560959683220/1560959683479\">Demande d'autorisation \u00e9crite de mener des Activit\u00e9s sur des phytoravageurs (CFIA/ACIA 5851)</a></p>\n\n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-12-02/fra/1432586422006/1432586423037\">D-12-02 - Exigences r\u00e9gissant l'importation d'organismes potentiellement nuisibles (autres que les v\u00e9g\u00e9taux) afin d'emp\u00eacher l'importation de phytoravageurs au Canada</a></p>\n<p class=\"mrgn-lft-xl\"><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5256/fra/1491930450091/1491930450578\">Demande de permis pour importer des v\u00e9g\u00e9taux et d'autre choses en vertu de la Loi sur la protection des v\u00e9g\u00e9taux (CFIA/ACIA 5256)</a></p>\n\n<p>Les permis d'importation et les autorisations \u00e9crites seront uniquement d\u00e9livr\u00e9s aux installations certifi\u00e9es qui, sur la foi d'une certification, respectent les exigences physiques et op\u00e9rationnelles appropri\u00e9es d\u00e9crites dans les <abbr title=\"Normes relatives au confinement des installations manipulant des phytoravageurs\">NCIMP</abbr>. Nous collaborerons avec les intervenants afin de les aider \u00e0 se conformer aux normes.</p>\n\n<h2>Programme de bioconfinement et certification</h2>\n<p>L'objectif du programme de Bioconfinement est de s'assurer que les \u00e9tablissements de haut confinement respectent les normes canadiennes de confinement. Ce programme a pour buts de :</p>\n<ul>\n<li>conseiller les laboratoires sur la manipulation et le confinement des phytoravageurs;</li>\n<li>conseiller sur la planification, le d\u00e9veloppement, le fonctionnement  et la certification des installations de confinement;</li>\n<li>inspecter et certifier les installations de confinement manipulant ou important des phytoravageurs (<abbr title=\"Confinement phytoravageus\">PPC</abbr>2 et <abbr title=\"Confinement phytoravageus\">PPC</abbr>3);</li>\n<li>d\u00e9livrer des certificats  tous les 2  ans pour les installations de confinement <abbr title=\"Confinement phytoravageus\">PPC</abbr>2 et <abbr title=\"Confinement phytoravageus\">PPC</abbr>3.</li>\n</ul>\n<p>Si vous construisez une nouvelle installation \u00e0 cette fin, <a href=\"mailto:cfia.biocontainment-bioconfinement.acia@canada.ca\">veuillez communiquer avec le Bureau du confinement des biorisques et de la s\u00e9curit\u00e9 (BCBS)</a> d\u00e8s l'\u00e9tape de la planification du projet. Vous pouvez utiliser la liste de v\u00e9rification du niveau de confinement des phytoravageurs (NCP) pour \u00e9valuer les composantes physiques de votre installation. D\u00e8s la r\u00e9ception du formulaire rempli, des sp\u00e9cialistes en bios\u00e9curit\u00e9 en feront l'examen en vue de d\u00e9terminer le <abbr title=\"niveau de confinement des phytoravageurs\">NCP</abbr> requis et formuleront des recommandations sur la fa\u00e7on d'obtenir le <abbr title=\"niveau de confinement des phytoravageurs\">NCP</abbr> vis\u00e9.</p>\n<h2>Information suppl\u00e9mentaire</h2>\n<p>Pour des questions concernant les niveaux <abbr title=\"Confinement phytoravageus\">PPC</abbr>-exposition, 1 ou 2, y  compris pour obtenir une copie d'une liste de contr\u00f4le d'auto \u00e9valuation, veuillez contacter <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">votre bureau local de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou la Section des esp\u00e8ces  exotiques envahissantes et programmes nationaux (CFIA-IAS_ACIA-EEE@inspection.gc.ca).</p>\n<p>Pour des questions concernant les niveaux <abbr title=\"Confinement phytoravageus\">PPC</abbr>-2A or 3, <a href=\"mailto:biocon@inspection.gc.ca\">veuillez communiquer avec le <abbr title=\"Bureau du confinement des biorisques et de la s\u00e9curit\u00e9\">BCBS</abbr></a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}