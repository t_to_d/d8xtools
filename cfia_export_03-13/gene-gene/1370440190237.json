{
    "dcr_id": "1370440190237",
    "title": {
        "en": "Use of partly (partially) skimmed milk from the butter making process in skim milk powder production",
        "fr": "Utilisation de lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre dans la production de lait \u00e9cr\u00e9m\u00e9 en poudre"
    },
    "html_modified": "2024-03-12 3:56:21 PM",
    "modified": "2019-01-15",
    "issued": "2013-06-05 09:49:50",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/dair_manual_products_decision_skim-milk-production_1370440190237_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/dair_manual_products_decision_skim-milk-production_1370440190237_fra"
    },
    "ia_id": "1370440192222",
    "parent_ia_id": "1624983674393",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling|Meeting a standard of identity",
        "fr": "\u00c9tiquetage|Respect d\u2019une norme d\u2019identit\u00e9"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1624983674393",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Use of partly (partially) skimmed milk from the butter making process in skim milk powder production",
        "fr": "Utilisation de lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre dans la production de lait \u00e9cr\u00e9m\u00e9 en poudre"
    },
    "label": {
        "en": "Use of partly (partially) skimmed milk from the butter making process in skim milk powder production",
        "fr": "Utilisation de lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre dans la production de lait \u00e9cr\u00e9m\u00e9 en poudre"
    },
    "templatetype": "content page 1 column",
    "node_id": "1370440192222",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1624983427586",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/dairy/skimmed-milk/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/produits-laitiers/lait-ecreme/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Use of partly (partially) skimmed milk from the butter making process in skim milk powder production",
            "fr": "Utilisation de lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre dans la production de lait \u00e9cr\u00e9m\u00e9 en poudre"
        },
        "description": {
            "en": "Various questions and decisions concerning the labelling requirements for partly skimmed milk, when used in the production of skim milk powder.",
            "fr": "Diverses questions et d\u00e9cisions concernant les exigences de  l'\u00e9tiquetage  du lait partiellement \u00e9cr\u00e9m\u00e9, lorsqu'on l'utilise pour la production de lait \u00e9cr\u00e9m\u00e9 en poudre."
        },
        "keywords": {
            "en": "skimmed, partly, powder",
            "fr": "partiellement, partie, poudre"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-06-05 09:49:50",
            "fr": "2013-06-05 09:49:50"
        },
        "modified": {
            "en": "2019-01-15",
            "fr": "2019-01-15"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Use of partly (partially) skimmed milk from the butter making process in skim milk powder production",
        "fr": "Utilisation de lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre dans la production de lait \u00e9cr\u00e9m\u00e9 en poudre"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Questions:</h2>\n<p>1) If a small proportion of the partly (partially) skimmed milk from the butter making process is used in the production of skim milk powder, does the resulting product meet the definition of <strong>skim milk powder</strong> in the <a href=\"/english/reg/jredirect2.shtml?drgr\">Food and Drug Regulations</a> (FDR)?<br>\n</p>\n<h2>Background:</h2>\n<p>The partly (partially) skimmed milk produced during the butter making process refers to the liquid fraction which is removed in the first stream in the continuous churn. Industry currently combines this partially skimmed milk from the butter making operation with skim milk. The resulting standardized skim milk is then processed into skim milk powder.</p>\n<h2>Regulatory requirements:</h2>\n<p>The definition of skim milk powder in the FDR states:</p>\n<p><strong>B.08.014 [S]. Skim Milk Powder </strong>or <strong>Dry Skim Milk</strong><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n<p>The standard for skim milk in the FDR only states requirements for total fat and vitamin A and D levels. There are no maximum or minimum values for protein, ash, or requirements for fatty acid composition.</p>\n<p><strong>B.08.004 [S]. Skim Milk</strong><br>\n<br>\n<br>\n</p>\n<p>The definition of partly (partially) skimmed milk in the FDR states:</p>\n<p><strong>B.08.005 [S]. Partly Skimmed Milk or Partially Skimmed Milk</strong> <br>\n<br>\n<br>\n</p>\n\n<p>The Codex standard for milk powders <strong>(CODEX <abbr title=\"standard\">STAN</abbr> 207)</strong> does have defined protein levels for skimmed milk powder at a minimum of 34% m/m milk protein in milk solids-not-fat (not including water of crystallization of the lactose).</p>\n<h2>Decisions:</h2>\n<p>1) Based on the definition of skim milk, as long as the fat level of the combined skim milk and partly (partially) skimmed milk results in a fluid product of less than 0.3% milk fat, partly (partially) skimmed milk from the butter making process can be combined with skim milk for the production of skim milk powder. It is recognized that this practice is self-limiting based on the compositional standards for fat content in skim milk.</p>\n<p>The skim milk powder must meet the requirements prescribed in section B.08.014 [S] of the FDR as specified in section 8 of the <a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-standards-of-identity-volume-1/eng/1521473554991/1521473555532\">Canadian Standards of Identity, Volume 1 \u2013 Dairy Products</a>, and meet the protein requirements of the Codex standard CODEX STAN 207.</p>\n\n<p>2) The <strong>skim milk powder</strong> is not required to be labelled with a list of ingredients including this partly (partially) skimmed milk from butter making because it is a pre-component of the liquid skim milk which is to be dried.</p>\n\n<p>(This page was amended from a record of decision signed February 15<sup>th</sup>, 2006, by the Agency.)</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Questions\u00a0:</h2>\n<p>1) Si une petite proportion du lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre est utilis\u00e9e dans la production de lait \u00e9cr\u00e9m\u00e9 en poudre, le produit qui en r\u00e9sulte r\u00e9pond-il \u00e0 la d\u00e9finition de <strong>lait \u00e9cr\u00e9m\u00e9 en poudre</strong> figurant dans le <a href=\"/francais/reg/jredirect2.shtml?drgr\">R\u00e8glement sur les aliments et drogues </a>(RAD)?<br>\n</p>\n<h2>Contexte\u00a0:</h2>\n<p>Le lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 produit durant la fabrication du beurre d\u00e9signe la fraction liquide enlev\u00e9e lors du premier passage dans la baratte en continu. Actuellement, l'industrie combine ce lait partiellement \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre avec du lait \u00e9cr\u00e9m\u00e9. Le lait \u00e9cr\u00e9m\u00e9 standardis\u00e9 qui en r\u00e9sulte est alors transform\u00e9 en lait \u00e9cr\u00e9m\u00e9 en poudre.</p>\n<h2>Exigences r\u00e9glementaires\u00a0:</h2>\n<p>La d\u00e9finition du lait \u00e9cr\u00e9m\u00e9 en poudre figurant dans le RAD se lit ainsi\u00a0:</p>\n<p><strong>B.08.014 [N].</strong> Le <strong>lait \u00e9cr\u00e9m\u00e9 en poudre</strong>, la <strong>poudre de lait \u00e9cr\u00e9m\u00e9</strong>, ou le <strong>lait \u00e9cr\u00e9m\u00e9 dess\u00e9ch\u00e9</strong><br>\n<br>\n<br>\n<br>\n<br>\n</p>\n<p>La norme relative au lait \u00e9cr\u00e9m\u00e9 inscrite dans le RAD \u00e9tablit des exigences ayant trait uniquement \u00e0 la teneur en mati\u00e8res grasses totales et en vitamines\u00a0A et D. Elle ne fixe pas de valeurs maximales ou minimales pour les prot\u00e9ines et les cendres ni d'exigences en ce qui concerne la composition des acides gras.</p>\n<p><strong>B.08.004 [N]. </strong>Le<strong> lait \u00e9cr\u00e9m\u00e9</strong><br>\n<br>\n<br>\n</p>\n<p>La d\u00e9finition du lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 figurant dans le RAD se lit ainsi\u00a0:</p>\n<p><strong>B.08.005 [N].</strong> Le <strong>lait partiellement \u00e9cr\u00e9m\u00e9</strong> ou <strong>en partie \u00e9cr\u00e9m\u00e9</strong><br>\n<br>\n<br>\n</p>\n\n<p>La norme du Codex pour les laits en poudre <strong>(CODEX <abbr lang=\"en\" title=\"standard\">STAN</abbr>\u00a0207)</strong> mentionne effectivement que la teneur minimale en prot\u00e9ines du lait pour le lait \u00e9cr\u00e9m\u00e9 en poudre est de 34\u00a0%\u00a0m/m dans l'extrait sec d\u00e9graiss\u00e9 du lait (excluant l'eau de cristallisation du lactose).</p>\n<h2>D\u00e9cisions\u00a0:</h2>\n<p>1) Selon la d\u00e9finition du lait \u00e9cr\u00e9m\u00e9, si la teneur en gras du lait \u00e9cr\u00e9m\u00e9 combin\u00e9 avec du lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 entra\u00eene la cr\u00e9ation d'un produit liquide contenant moins de 0,3\u00a0% de mati\u00e8re grasse du lait, le lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre peut \u00eatre combin\u00e9 avec du lait \u00e9cr\u00e9m\u00e9 afin de produire du lait \u00e9cr\u00e9m\u00e9 en poudre. Il est reconnu que cette pratique a ses limites en raison des normes de composition relatives \u00e0 la teneur en mati\u00e8res grasses du lait \u00e9cr\u00e9m\u00e9.</p>\n<p>Le lait \u00e9cr\u00e9m\u00e9 en poudre doit r\u00e9pondre aux exigences \u00e9nonc\u00e9es \u00e0 l\u2019article B.08.014 [N] du RAD, tel que sp\u00e9cifi\u00e9 par l\u2019article 8 des <a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/normes-d-identite-canadiennes-volume-1/fra/1521473554991/1521473555532\">Normes d\u2019identit\u00e9 canadiennes, Volume 1 \u2013 Produits laitiers</a> et il doit satisfaire aux exigences relatives aux prot\u00e9ines de la norme CODEX STAN\u00a0207.</p>\n\n<p>2) L'\u00e9tiquette du\u00a0<strong>lait \u00e9cr\u00e9m\u00e9 en poudre</strong>\u00a0ne doit pas comporter une liste d'ingr\u00e9dients incluant ce lait partiellement \u00e9cr\u00e9m\u00e9 ou en partie \u00e9cr\u00e9m\u00e9 provenant de la fabrication du beurre parce qu'il s'agit d'un composant pr\u00e9alable du lait \u00e9cr\u00e9m\u00e9 liquide qui doit \u00eatre s\u00e9ch\u00e9.</p>\n\n<p>(Cette page a \u00e9t\u00e9 modifi\u00e9e en se basant sur un rapport de d\u00e9cision sign\u00e9 le 15 f\u00e9vrier 2006 par l\u2019Agence.)</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}