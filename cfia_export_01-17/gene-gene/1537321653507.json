{
    "dcr_id": "1537321653507",
    "title": {
        "en": "Standard Permissions Procedure (SPP) overview",
        "fr": "Aper\u00e7u de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA)"
    },
    "html_modified": "25-9-2018",
    "modified": "25-9-2018",
    "issued": "18-9-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/std_permissions_proc_overview_1537321653507_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/std_permissions_proc_overview_1537321653507_fra"
    },
    "parent_ia_id": "1539876015036",
    "ia_id": "1537321653709",
    "parent_node_id": "1539876015036",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Standard Permissions Procedure (SPP) overview",
        "fr": "Aper\u00e7u de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA)"
    },
    "label": {
        "en": "Standard Permissions Procedure (SPP) overview",
        "fr": "Aper\u00e7u de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1537321653709",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1539876014831",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/overview/",
        "fr": "/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/apercu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Standard Permissions Procedure (SPP) overview",
            "fr": "Aper\u00e7u de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA)"
        },
        "description": {
            "en": "The purpose of this overview is to showcase this Standard Permissions Procedure (SPP) foundational operational guidance document.  The Canadian Food Inspection Agency (CFIA) is sharing this overview externally in the interest of transparency and CFIA's Openness by Design.  The SPP document is intended for CFIA staff use and was developed to outline the roles and responsibilities associated with permission processes.",
            "fr": "Le pr\u00e9sent aper\u00e7u vise \u00e0 pr\u00e9senter le document d'orientation op\u00e9rationnelle de base de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA). En tant qu'Agence \u00ab\u00a0bas\u00e9e sur l'ouverture\u00a0\u00bb, l'Agence canadienne d'inspection des aliments (ACIA) diffuse cet aper\u00e7u \u00e0 l'externe dans un souci de transparence et d'ouverture. Le document des PNA est destin\u00e9 au personnel de l'ACIA, et il a \u00e9t\u00e9 \u00e9labor\u00e9 pour d\u00e9crire les r\u00f4les et les responsabilit\u00e9s associ\u00e9s aux processus d'autorisations."
        },
        "keywords": {
            "en": "inspection, Safe Food for Canadians Regulations, SFCR, regulatory compliance, compliance verification,  integrated Agency Inspection Model, iAIM, preventive control plan, PCP, overview, Standard Permissions Procedure, SPP",
            "fr": "inspection, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, conformit\u00e9 r\u00e9glementaire, v\u00e9rification de la conformit\u00e9, mod\u00e8le d'inspection int\u00e9gr\u00e9 de l'Agence, MIIA, Plan de contr\u00f4le pr\u00e9ventif, PCP, aper\u00e7u, Proc\u00e9dure normalis\u00e9e des autorisations, PNA"
        },
        "dcterms.subject": {
            "en": "food,compliance,legislation,guidelines,agri-food products,regulation,food safety,food processing",
            "fr": "aliment,conformit\u00e9,l\u00e9gislation,lignes directrices,produit agro-alimentaire,r\u00e9glementation,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Operations",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Op\u00e9rations"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-09-25",
            "fr": "2018-09-25"
        },
        "modified": {
            "en": "2018-09-25",
            "fr": "2018-09-25"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Standard Permissions Procedure (SPP) overview",
        "fr": "Aper\u00e7u de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA)"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\n<h2 class=\"h4\">Notice</h2>\n\n<p>The purpose of this overview is to showcase this Standard Permissions Procedure (SPP) foundational operational guidance document.  The Canadian Food Inspection Agency (CFIA) is sharing this overview externally in the interest of transparency and CFIA's Openness by Design.  The SPP document is intended for CFIA staff use and was developed to outline the roles and responsibilities associated with permission processes.  For terminology and references that may be new to some readers, a glossary of terms, acronyms and definitions will be in the full version of the SPP, which is being prepared for posting on the CFIA website.  Further information on permissions can be found on the <a href=\"/eng/1512149177555/1512149203296\">SFCR landing page</a>.</p>\n</section>\n\n<h2>Permissions</h2>\n\n<p>CFIA grants permissions for regulated activities (for example export certification).  Permission procedures, both internal and external, have changed with Agency transformation, specifically the development of the online Digital Service Delivery Platform (DSDP).  The Standard Permissions Procedure (SPP) provides a common approach and general overview for processing service requests relating to online and offline permissions both with and without Preventive Control Plans (PCPs).</p>\n\n<p>The SPP applies across the three business lines (food, animal health and plant health) under the <a href=\"/about-cfia/cfia-2025/inspection-modernization/integrated-agency-inspection-model/eng/1439998189223/1439998242489\">integrated Agency Inspection Model (iAIM)</a> and identifies the connections, communications and relationships between internal and external users.  Complementary documents to the SPP include the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/sample-collection/standard-inspection-process/overview/eng/1537319653853/1537319724052\">Standard Inspection Procedures (SIP)</a> and the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/regulatory-response/overview/eng/1537322772198/1537322772432\">Standard Regulatory Response Procedures (SRRP)</a>.</p>\n\n<p>The Agency has begun automating the permission process across food, animal health, and plant health business lines.  An incremental approach will be used to allow staff and industry time to adjust to the new way of doing business and to minimize any disruption to trade.</p> \n      \n<h2>How does CFIA process permission service requests</h2>\n\n<p>CFIA is working towards a single window approach to permissions.  The Digital Service Delivery Platform (DSDP) will support profile users in their requests for services and permissions online.  The current offline permissions processes exist in parallel with the DSDP capabilities.  Regulated parties seeking permissions for certain commodities can submit requests electronically using their My CFIA profile.</p>  \n\n<p>The DSDP will serve as the central system for the Centre of Administration (CoA) in processing import, domestic and export related permissions from My CFIA.  The DSDP provides a forum for communication with CFIA personnel to ensure any required technical reviews and inspections are conducted for the approval of permission requests.</p> \n \n<h2>How does CFIA receive and verify permission service requests</h2>\n\n<p>Permission service requests are processed within DSDP when users enroll and create a profile in My CFIA.  Users then have access to permission related information for their use through this portal and will be able to monitor the status of their requests online.  CFIA will process permission service requests by alternate means when necessary.</p> \n\n<p>The CoA will respond to administrative queries related to permissions and will verify permission service requests for completeness, accuracy and invoicing/payment.</p>  \n\n<h2>How does CFIA validate and review profiles</h2>\n\n<p>Profile validations will be required for all service requests for individuals, alternate service providers, and organizations who conduct business with CFIA online for the first time.  The business' legal name, profile authority name and owner name will be validated.</p> \n\n<p>The CoA service agent will determine whether a technical review or inspection task is required for the approval of a permission service request.  Acceptable completion of any required review(s) or task(s) will permit the permission service request to proceed.</p>\n \n<h2>How does CFIA invoice for permissions</h2>\n\n<p>The CFIA charges fees in accordance with the Canadian Food Inspection Agency Fees Notice. CFIA service request application forms generally include the costs for the specific permission services and a method of payment section for the applicant to complete.  Payments are either processed upon receipt of the service request or upon issuance of the permission.</p>  \n\n<h2>How does CFIA enforce permissions</h2>\n\n<p>When necessary, an enforcement action may be taken on a permission.  Refer to the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/regulatory-response/overview/eng/1537322772198/1537322772432\">Standard Regulatory Response Process</a> for more information on the suspension or cancellation/revocation of a permission.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\n<h2 class=\"h4\">Avis</h2>\n\n<p>Le pr\u00e9sent aper\u00e7u vise \u00e0 pr\u00e9senter le document d'orientation op\u00e9rationnelle de base de la Proc\u00e9dure normalis\u00e9e des autorisations (PNA). En tant qu'Agence \u00ab\u00a0bas\u00e9e sur l'ouverture\u00a0\u00bb, l'Agence canadienne d'inspection des aliments (ACIA) diffuse cet aper\u00e7u \u00e0 l'externe dans un souci de transparence et d'ouverture. Le document des PNA est destin\u00e9 au personnel de l'ACIA, et il a \u00e9t\u00e9 \u00e9labor\u00e9 pour d\u00e9crire les r\u00f4les et les responsabilit\u00e9s associ\u00e9s aux processus d'autorisations. En ce qui concerne la terminologie et les r\u00e9f\u00e9rences qui peuvent \u00eatre nouvelles pour certains lecteurs, un glossaire des termes, des acronymes et des d\u00e9finitions figurera dans la version compl\u00e8te des PNA, qui est en cours de pr\u00e9paration en vue de sa publication dans le site Web de l'ACIA. De plus amples renseignements sur les autorisations se trouvent dans la page d'accueil du <a href=\"/fra/1512149177555/1512149203296\">R\u00e8glement sur la salubrit\u00e9 des aliments au Canada (RSAC)</a>.</p>\n</section>\n\n<h2>Les autorisations</h2>\n\n<p>L'ACIA accorde des autorisations pour les activit\u00e9s r\u00e9glement\u00e9es (par exemple, la certification des exportations). Les proc\u00e9dures d'autorisations, tant internes qu'externes, ont chang\u00e9 avec la transformation de l'Agence, en particulier avec la cr\u00e9ation de la Plateforme de prestation num\u00e9rique de services (PPNS) en ligne. La Proc\u00e9dure normalis\u00e9e des autorisations (PNA) pr\u00e9sente une approche commune et un aper\u00e7u g\u00e9n\u00e9ral du traitement des demandes de service relatives aux autorisations en ligne et hors ligne, avec ou sans plans de contr\u00f4les pr\u00e9ventifs (PCP).</p>\n\n<p>La PNA s'applique aux trois secteurs d'activit\u00e9 (les aliments, la sant\u00e9 animale et la protection des v\u00e9g\u00e9taux) dans le cadre du <a href=\"/a-propos-de-l-acia/acia-2025/modernisation-de-l-inspection/modele-d-inspection-integre-de-l-agence/fra/1439998189223/1439998242489\">mod\u00e8le d'inspection int\u00e9gr\u00e9 de l'Agence (MIIA)</a> et d\u00e9finit les liens, les communications et les relations entre les utilisateurs internes et externes. Les documents compl\u00e9mentaires \u00e0 la PNA comprennent la <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/prelevement-d-echantillons/processus-d-inspection-standardise/apercu/fra/1537319653853/1537319724052\">Proc\u00e9dure d'inspection standardis\u00e9e (PIS)</a> et le <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/intervention-reglementaire/apercu/fra/1537322772198/1537322772432\">Processus d'intervention r\u00e9glementaire normalis\u00e9 (PIRN)</a>.</p>\n\n<p>L'Agence a commenc\u00e9 \u00e0 automatiser le processus d'autorisation dans tous les secteurs d'activit\u00e9 li\u00e9s aux aliments, \u00e0 la sant\u00e9 animale et \u00e0 la protection des v\u00e9g\u00e9taux. Une approche progressive sera adopt\u00e9e pour permettre au personnel et \u00e0 l'industrie de s'adapter au nouveau mode de fonctionnement, ainsi que pour r\u00e9duire au minimum toute perturbation du commerce.</p> \n      \n<h2>Comment l'ACIA traite-t-elle les demandes de service relatives aux autorisations</h2>\n\n<p>L'ACIA travaille \u00e0 la mise au point d'une approche de guichet unique pour les autorisations. La Plateforme de prestation num\u00e9rique de services (PPNS) apportera un soutien aux utilisateurs de profils dans leurs demandes de services et d'autorisations en ligne. Les processus actuels d'autorisations hors ligne existent en parall\u00e8le avec les capacit\u00e9s de la PPNS. Les parties r\u00e9glement\u00e9es qui souhaitent obtenir des autorisations pour certains produits peuvent soumettre des demandes par voie \u00e9lectronique en utilisant leur profil dans Mon ACIA.</p>  \n\n<p>La PPNS servira de syst\u00e8me central pour le Centre d'administration (CdA) en vue du traitement, \u00e0 partir de Mon ACIA, des autorisations visant les produits canadiens, les produits import\u00e9s et les produits export\u00e9s. La PPNS offre un forum de communication avec le personnel de l'ACIA permettant de s'assurer que les examens techniques et les inspections n\u00e9cessaires sont effectu\u00e9s en vue de l'approbation des demandes d'autorisation.</p> \n \n<h2>Comment l'ACIA re\u00e7oit-elle et v\u00e9rifie-t-elle les demandes de service relatives aux autorisations</h2>\n\n<p>Les demandes de service relatives aux autorisations sont trait\u00e9es dans la PPNS lorsque les utilisateurs s'inscrivent et cr\u00e9ent un profil dans Mon ACIA. Les utilisateurs ont ensuite acc\u00e8s aux renseignements relatifs aux autorisations dont ils ont besoin au moyen de ce portail et seront en mesure de surveiller l'\u00e9tat de leurs demandes en ligne. L'ACIA traitera les demandes de service relatives aux autorisations par d'autres moyens, au besoin.</p> \n\n<p>Le CdA r\u00e9pondra aux demandes de renseignements administratifs concernant les autorisations et v\u00e9rifiera si les demandes de service relatives aux autorisations sont compl\u00e8tes, exactes, et factur\u00e9es/pay\u00e9es.</p>  \n\n<h2>Comment l'ACIA valide-t-elle et r\u00e9vise-t-elle les profils</h2>\n\n<p>Les validations des profils seront requises pour toutes les demandes de service pr\u00e9sent\u00e9es par les particuliers, les autres fournisseurs de services et les organisations qui font affaire avec l'ACIA en ligne pour la premi\u00e8re fois. La raison sociale, le nom de l'autorit\u00e9 du profil et le nom du propri\u00e9taire de l'entreprise seront valid\u00e9s.</p> \n\n<p>L'agent de services du CdA d\u00e9terminera si un examen technique ou une t\u00e2che d'inspection est n\u00e9cessaire pour l'approbation d'une demande de service relative \u00e0 une autorisation. La r\u00e9alisation acceptable des t\u00e2ches ou des examens requis permettra de donner suite \u00e0 la demande de service relative \u00e0 l'autorisation.</p>\n \n<h2>Comment l'ACIA \u00e9tablit-elle les factures des autorisations</h2>\n\n<p>L'ACIA impose des droits conform\u00e9ment \u00e0 l'Avis sur les prix de l'Agence canadienne d'inspection des aliments. Les formulaires de demande de service de l'ACIA comprennent habituellement les co\u00fbts des services relatifs \u00e0 une autorisation particuli\u00e8re et une section sur le mode de paiement que le demandeur doit remplir. Les paiements sont trait\u00e9s soit \u00e0 la r\u00e9ception de la demande de service, soit \u00e0 la d\u00e9livrance de l'autorisation.</p>  \n\n<h2>Comment l'ACIA fait-elle appliquer la loi \u00e0 l'\u00e9gard des autorisations</h2>\n\n<p>Au besoin, une mesure d'application de la loi peut \u00eatre prise \u00e0 l'\u00e9gard d'une autorisation. Pour obtenir de plus amples renseignements sur la suspension, l'annulation ou la r\u00e9vocation d'une autorisation, veuillez consulter le <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/intervention-reglementaire/apercu/fra/1537322772198/1537322772432\">Processus d'intervention r\u00e9glementaire normalis\u00e9</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}