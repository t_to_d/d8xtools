{
    "dcr_id": "1306640102681",
    "title": {
        "en": "Vesicular Stomatitis \u2013 Fact Sheet",
        "fr": "Stomatite v\u00e9siculeuse \u2013 Fiche de renseignements"
    },
    "html_modified": "2011-05-28 23:35",
    "modified": "19-6-2023",
    "issued": "28-5-2011",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_vesicstom_fact_1306640102681_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_vesicstom_fact_1306640102681_fra"
    },
    "parent_ia_id": "1306638597634",
    "ia_id": "1306848592941",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Vesicular Stomatitis \u2013 Fact Sheet",
        "fr": "Stomatite v\u00e9siculeuse \u2013 Fiche de renseignements"
    },
    "label": {
        "en": "Vesicular Stomatitis \u2013 Fact Sheet",
        "fr": "Stomatite v\u00e9siculeuse \u2013 Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1306848592941",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1306638312439",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/vesicular-stomatitis/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/stomatite-vesiculeuse/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Vesicular Stomatitis \u2013 Fact Sheet",
            "fr": "Stomatite v\u00e9siculeuse \u2013 Fiche de renseignements"
        },
        "description": {
            "en": "Fact Sheet - Vesicular stomatitis (VS) is a viral disease affecting horses, ruminants such as cattle, sheep and members of the deer and llama families, and swine.",
            "fr": "Fiche de renseignements - La stomatite v\u00e9siculeuse (SV) est une virose qui affecte les chevaux, les ruminants comme les bovins, les ovins et les membres de la famille du cerf et du lama, ainsi que les porcs."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, Vesicular stomatitis, cattle, sheep, deer, llama, swine, fact sheet",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, stomatite v\u00e9siculeuse, chevaux, bovins, ovins, cerf. lama, porcs, fiche de renseignements"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-28 23:35:09",
            "fr": "2011-05-28 23:35:09"
        },
        "modified": {
            "en": "2023-06-19",
            "fr": "2023-06-19"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Vesicular Stomatitis \u2013 Fact Sheet",
        "fr": "Stomatite v\u00e9siculeuse \u2013 Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\t<h2>What is vesicular stomatitis</h2>\n\n<p>Vesicular stomatitis (VS) is a viral disease affecting horses; ruminants such as cattle, sheep, members of the deer and llama families; and swine. While VS causes discomfort to affected animals and may result in loss of markets for live animals, meat and animal genetics, it is most significant because it closely resembles foot and mouth disease, which affects ruminants and swine and is a devastating disease for producers.</p>\n\n<h2>How is VS transmitted</h2>\n\n<p>The virus is spread by blood feeding insects such as midges and black flies, and by direct or indirect contact with saliva or fluid from lesions of clinically affected animals.</p>\n<p>Spread of the disease in dairy herds may also occur as a result of milking procedures.</p>\n<p>The disease may be transmitted to humans who come into contact with infected animals.</p>\n\n\n<h2>What are the signs of VS</h2>\n\n<p>Vesicular stomatitis causes a mild fever and the formation of blister like or crusting lesions on the inside of the mouth, the ears, on the lips, the nose, above the hooves and on the udder or sheath. The blisters break, leaving raw, sore areas. Affected animals often salivate profusely and are unwilling to eat or drink. Some animals, particularly swine, may become lame. Milking cows show a marked decrease in milk production. The incubation period (the time between infection with the virus and clinical signs) may range from 2\u00a0to 8\u00a0days, and animals generally recover completely in 3\u00a0to 4\u00a0days.</p>\n<p>In humans, the virus generally causes influenza like symptoms, though oral lesions are possible and meningitis can occur in rare cases.</p>\n\n\n<h2>How is VS diagnosed</h2>\n\n<p>Vesicular stomatitis is diagnosed by laboratory testing on samples of fluid or swabs from the vesicles (blister like lesions) of affected animals, or by a blood sample.</p>\n\n<h2>What should I do if I think my animal might have VS</h2>\n\n<ul>\n<li>Call your veterinarian immediately or call the nearest Canadian Food Inspection Agency (CFIA) <a href=\"/eng/1300462382369/1300462438912\">Animal Health Office</a></li>\n<li>All animals with lesions should be kept separate from healthy animals, preferably indoors.\n<ul><li>Do <strong>not</strong> move animals from your premises until a definitive diagnosis has been made</li></ul></li>\n<li>Wear protective clothing and gloves when handling suspect animals to help prevent exposure to the virus</li>\n</ul>\n\n<h2>Do we have VS in Canada</h2>\n\n<p>Vesicular stomatitis was last diagnosed in Canada in\u00a01949.</p>\n\n<h2>What does the CFIA do to prevent VS from entering Canada</h2>\n\n<p>The CFIA has taken the following measures to prevent the entry of this disease into Canada:</p>\n\n<ul>\n<li>Vesicular stomatitis is a reportable disease under the <i>Health of Animals Act</i>; this means that all suspected cases must be reported to the CFIA</li>\n<li>all reported suspect cases are immediately investigated by CFIA inspectors.</li>\n<li>should VS be diagnosed on a Canadian premises, a quarantine would be imposed to restrict movement of the animals this quarantine would be lifted 14 days after all clinical signs have disappeared</li>\n<li>when there is a VS outbreak in another country, the CFIA may require that all susceptible animals entering Canada from that country be examined by a federal veterinarian\n<ul>\n<li>The veterinary authorities of the exporting country are required to provide certification that the animal was found clinically healthy prior to departure and did not originate from an area where the disease is active</li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\t<h2>Qu'est-ce que la stomatite v\u00e9siculeuse</h2>\n\n<p>La stomatite v\u00e9siculeuse (SV) est une maladie virale qui affecte les chevaux; les ruminants comme les bovins, les ovins, les membres de la famille du cerf et du lama; ainsi que les porcs. En plus de causer de l'inconfort aux animaux infect\u00e9s et d'entra\u00eener des pertes de march\u00e9 pour les animaux vivants, la viande et le germoplasme, la maladie rev\u00eat une grande importance en raison de sa ressemblance avec la fi\u00e8vre aphteuse, qui affecte les ruminants et les porcs et qui peut d\u00e9vaster les \u00e9levages.</p>\n\n<h2>De quelle mani\u00e8re se transmet la SV</h2>\n\n<p>Le virus se propage par des insectes qui se nourrissent de sang comme les moucherons et les mouches noires, et par contact direct ou indirect avec la salive ou le liquide provenant de l\u00e9sions d'animaux cliniquement atteints.</p>\n<p>La maladie peut aussi se propager dans les troupeaux laitiers \u00e0 cause des m\u00e9thodes de traite.</p>\n<p>La SV peut se transmettre aux humains qui entrent en contact avec les animaux infect\u00e9s.</p>\n\n\n<h2>Quels sont les sympt\u00f4mes de la SV</h2>\n\n<p>La SV cause une faible fi\u00e8vre et la formation de l\u00e9sions semblables \u00e0 des cloques ou de cro\u00fbtes \u00e0 l'int\u00e9rieur de la bouche, des oreilles, sur les l\u00e8vres, le museau, au-dessus des sabots et sur le pis ou la gaine. En \u00e9clatant, les cloques laissent des plaies vives et douloureuses. Les animaux malades salivent abondamment et refusent de boire et de manger. La claudication est possible chez certains animaux, en particulier chez les porcs. Les vaches laiti\u00e8res produisent aussi beaucoup moins de lait. La p\u00e9riode d'incubation (le temps entre l'infection virale et l'apparition des signes cliniques) peut durer de 2\u00a0\u00e0 8\u00a0jours et, en g\u00e9n\u00e9ral, les animaux gu\u00e9rissent compl\u00e8tement en 3\u00a0ou 4\u00a0jours.</p>\n<p>Chez l'humain, le virus provoque g\u00e9n\u00e9ralement des sympt\u00f4mes pseudo-grippaux, bien que des l\u00e9sions buccales soient possibles et que la m\u00e9ningite puisse survenir dans de rares cas.</p>\n\n\n<h2>Comment diagnostique-t-on la SV</h2>\n\n<p>La SV est diagnostiqu\u00e9e gr\u00e2ce \u00e0 l'analyse en laboratoire d'\u00e9chantillons du fluide ou d'\u00e9couvillons provenant des v\u00e9sicules (l\u00e9sions ressemblant \u00e0 des cloques) qui se forment chez les animaux affect\u00e9s, ou des \u00e9chantillons du sang.</p>\n\n<h2>Que dois-je faire si je pense que mon animal pourrait avoir la SV</h2>\n\n<ul>\n<li>Appelez votre v\u00e9t\u00e9rinaire imm\u00e9diatement ou t\u00e9l\u00e9phonez un bureau de sant\u00e9 animale de l'Agence canadienne d'inspection des animaux (ACIA) pr\u00e8s de chez vous.</li>\n<li>S\u00e9parez toutes les b\u00eates ayant des l\u00e9sions des animaux sains et isolez les, de pr\u00e9f\u00e9rence \u00e0 l'int\u00e9rieur.\n<ul><li>Les animaux <strong>ne doivent pas</strong> quitter les lieux avant qu'un diagnostic d\u00e9finitif ne soit pos\u00e9.</li></ul></li>\n<li>Portez des v\u00eatements protecteurs et des gants pour manipuler les animaux pr\u00e9sum\u00e9s infect\u00e9s pour aider \u00e0 pr\u00e9venir l'exposition au virus.</li>\n</ul>\n\n<h2>La SV existe-t-elle au Canada</h2>\n\n<p>La SV a \u00e9t\u00e9 diagnostiqu\u00e9e pour la derni\u00e8re fois au Canada en\u00a01949.</p>\n\n<h2>Que fait l'ACIA pour pr\u00e9venir l'introduction de la SV au Canada</h2>\n\n<p>L'ACIA a pris les mesures suivantes pour pr\u00e9venir l'introduction de cette maladie au Canada\u00a0:</p>\n<ul>\n<li>La SV est une maladie d\u00e9clarable en vertu de la <i>Loi sur la sant\u00e9 des animaux</i>; cela signifie que tous les cas suspects doivent \u00eatre signal\u00e9s \u00e0 l'ACIA.</li>\n<li>les inspecteurs de l'ACIA enqu\u00eatent imm\u00e9diatement sur tous les cas suspects d\u00e9clar\u00e9s.</li>\n<li>Si la SV est diagnostiqu\u00e9e dans un \u00e9levage canadien, les autorit\u00e9s imposeront une quarantaine pour limiter la circulation des animaux la quarantaine serait lev\u00e9e 14\u00a0jours apr\u00e8s la disparition de tous les signes cliniques.</li>\n<li>en cas d'\u00e9pid\u00e9mie de SV dans un autre pays, l'ACIA peut exiger que tous les animaux sensibles qui entrent au Canada en provenance de ce pays soient examin\u00e9s par un v\u00e9t\u00e9rinaire f\u00e9d\u00e9ral.\n<ul><li>Les autorit\u00e9s v\u00e9t\u00e9rinaires du pays exportateur devront certifier que l'animal a \u00e9t\u00e9 trouv\u00e9 cliniquement sain avant son d\u00e9part et qu'il ne provenait pas d'une r\u00e9gion o\u00f9 s\u00e9vit la maladie.</li></ul></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}