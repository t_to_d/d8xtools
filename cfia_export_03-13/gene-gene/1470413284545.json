{
    "dcr_id": "1470413284545",
    "title": {
        "en": "Questions and Answers: <i>Weed Seeds Order</i>, 2016",
        "fr": "Questions et r\u00e9ponses\u00a0: <i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i>"
    },
    "html_modified": "2024-03-12 3:57:26 PM",
    "modified": "2017-08-05",
    "issued": "2016-08-31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/plant_seed_weed_seed_order_faq_1470413284545_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/plant_seed_weed_seed_order_faq_1470413284545_fra"
    },
    "ia_id": "1470413303224",
    "parent_ia_id": "1463453028410",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1463453028410",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and Answers: <i>Weed Seeds Order</i>, 2016",
        "fr": "Questions et r\u00e9ponses\u00a0: <i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i>"
    },
    "label": {
        "en": "Questions and Answers: <i>Weed Seeds Order</i>, 2016",
        "fr": "Questions et r\u00e9ponses\u00a0: <i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1470413303224",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1463453027786",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/weed-seeds-order/weed-seeds-order-2016/",
        "fr": "/protection-des-vegetaux/semences/arrete-sur-les-graines-de-mauvaises-herbes/arrete-de-2016-sur-les-graines-de-mauvaises-herbes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and Answers: Weed Seeds Order, 2016",
            "fr": "Questions et r\u00e9ponses\u00a0: Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes"
        },
        "keywords": {
            "en": "seeds, Regulations, Weed Seeds Order, United States Department of Agriculture, Seeds Act",
            "fr": "semences, r\u00e9gulation, Modernisation du Programme des semence, United States Department of Agriculture, Loi du semences"
        },
        "dcterms.subject": {
            "en": "plants,weeds,regulation",
            "fr": "plante,plante nuisible,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-08-31",
            "fr": "2016-08-31"
        },
        "modified": {
            "en": "2017-08-05",
            "fr": "2017-08-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and Answers: Weed Seeds Order, 2016",
        "fr": "Questions et r\u00e9ponses\u00a0: Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is the <i>Weed Seeds Order</i>?</h2>\n\n<p>The <i>Weed Seeds Order</i> (WSO) is a ministerial order made pursuant to subsection 4(2) of the <i>Seeds Act</i> and plays a critical role in the prevention of the introduction of new weed species into Canada by preventing or limiting the presence of weed species in seed sold in, or imported into Canada. The <abbr title=\"Weed Seeds Order\">WSO</abbr> is the tool used to identify and categorize weed species for the purposes of the import and sale of seed as well as for grading purposes.</p>\n\n<h2 class=\"h5\">How is the <abbr title=\"Weed Seeds Order\">WSO</abbr> used?</h2>\n\n<p>Schedule <abbr title=\"1\">I</abbr> to the <i>Seeds Regulations</i> provides the standards that must be met in order to sell or import seed into Canada as well as the standards that must be met in order to assign a grade to a seed lot. Schedule <abbr title=\"1\">I</abbr> contains twenty-two grade tables, which are each applicable to certain crop kinds. The grade tables indicate the maximum allowable levels of primary, secondary, noxious and other weed seeds, as well as other crop kinds and inert matter. The <abbr title=\"Weed Seeds Order\">WSO</abbr> is used in conjunction with the grade tables as the <abbr title=\"Weed Seeds Order\">WSO</abbr> provides the list of which species are primary noxious, secondary and noxious weed species.</p>\n\n<p>The <abbr title=\"Weed Seeds Order\">WSO</abbr> also provides the list of species which are considered Prohibited Noxious weed species in seed. <span class=\"nowrap\">Section 7(1)(a)</span> of the <i>Seeds Regulations</i> indicates that no seed shall contain Prohibited Noxious weed seeds. It is for this reason that the grade tables do not contain any standards for Class 1 Prohibited Noxious weed seeds; these species are prohibited in all seed through the Regulations.</p>\n\n<h2 class=\"h5\">How were the species listed on <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2016 determined?</h2>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> conducted extensive consultations with stakeholders in order to finalize <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2016. Consultations began with two <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>-led workshops; the first on October 29, 2008, and the second on March 11, 2009. All consultation documents were distributed by email to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Seed Section's general stakeholder list that included approximately 2000 recipients.</p>\n\n<p>Following the workshops, two consultative proposals were distributed to stakeholders for comment. The initial consultation began October 23, 2009, and closed February 15, 2010. Responses received were integrated into a revised proposal. The second consultation was conducted between June 17, 2011, and September 15, 2011.</p>\n\n<p>Proposed amendments to the <abbr title=\"Weed Seeds Order\">WSO</abbr> were pre-published in the <i>Canada Gazette</i>, Part <abbr title=\"1\">I</abbr> on June 30<sup>th</sup> 2016 for a 75-day comment period. The final version of the revised <abbr title=\"Weed Seeds Order\">WSO</abbr> was published in the <i>Canada Gazette</i>, Part <abbr title=\"2\">II</abbr> on May 18<sup>th</sup>, 2016 with a coming into force date of November 1, 2016.</p>\n<h2>Summary of changes</h2>\n\n<ol>\n<li class=\"mrgn-bttm-md\">Addition/Reclassification/Removal of species\n\n<ul>\n<li class=\"mrgn-bttm-md mrgn-tp-sm\">Over time, the geographic distribution of weed species can change. As it has been over ten years since the last full review of the <abbr title=\"Weed Seeds Order\">WSO</abbr>, many species on the <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2005 were no longer accurately classified within the correct <abbr title=\"Weed Seeds Order\">WSO</abbr> class. Therefore, all species listed on <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2005 were reviewed based on current information. The review resulted in the reclassification of 11 species from Class 1 (Prohibited Noxious Weed Seeds) to Class 2 (Primary Noxious Weed Seeds), 7 species from Class 2 to Class 3 (Secondary Noxious Weed Seeds).</li>\n\n<li class=\"mrgn-bttm-md\">During the review of the <abbr title=\"Weed Seeds Order\">WSO</abbr>, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and stakeholders proposed the addition of new weed species to the <abbr title=\"Weed Seeds Order\">WSO</abbr>. In the end, 16 species were added to Class 1, 16 species were added to Class 2 and 5 species were added to Class 3.</li>\n\n<li class=\"mrgn-bttm-md\">Two listings, <i lang=\"la\">Camelina</i> <abbr title=\"species\">spp.</abbr> and <i lang=\"la\">Chicorium intybus</i> were removed from Class 3.</li>\n</ul></li>\n\n<li class=\"mrgn-bttm-md\">Class 2 applying to all tables\n\n<ul>\n<li class=\"mrgn-bttm-md mrgn-tp-sm\">The <abbr title=\"Weed Seeds Order\">WSO</abbr> is used in conjunction with the grade tables within Schedule <abbr title=\"1\">I</abbr> to the <i>Seeds Regulations</i> in order to determine the grade of seed and whether a seed lot meets import requirements.</li>\n\n<li class=\"mrgn-bttm-md\">In <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2005, Class 2 (Primary Noxious Weed Seeds) did not apply to Tables <abbr title=\"14\">XIV</abbr> and <abbr title=\"15\">XV</abbr> of Schedule <abbr title=\"1\">I</abbr>. <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2016 includes the change that Class 2 (Primary Noxious Weed Seeds) applies to all of the grade tables.</li>\n\n<li class=\"mrgn-bttm-md\">This was an important change as, although some species were being re-classified from Class 1 to Class 2, it remains critical to minimize the risk of further introductions of these weed species. Without Class 2 applying to all of the grade tables, there was the risk that these Class 2 species could spread through seed.</li>\n</ul></li>\n</ol>\n\n<h2>Training resources</h2>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Seed Science and Technology Section (SSTS) has developed seed identification factsheets for all of the species listed on <abbr title=\"Weed Seeds Order\">WSO</abbr>, 2016. These training resources will soon be available online. The Commercial Seed Analysts' Association of Canada (CSAAC) is holding training workshops in 2016 across the country in order to train their seed analyst members.</p>\n\n<h2>Regulation of weed species under the <i>Plant Protection Act</i></h2>\n\n<p>In February 2013, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> added several weed species to the <i>List of Pests Regulated in Canada</i>. These species are regulated in all commodities, including seed, with the specific requirements outlined in \"<a href=\"/plant-health/invasive-species/directives/date/d-12-01/eng/1380720513797/1380721302921\">D-12-01: Phytosanitary requirements to prevent the introduction of plants regulated as pests in Canada</a>\".</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que l'<i>Arr\u00eat\u00e9 sur les graines de mauvaises herbes</i>?</h2>\n\n<p>L'<i>Arr\u00eat\u00e9 sur les graines de mauvaises herbes</i> (AGMH) est un arr\u00eat\u00e9 minist\u00e9riel pris en vertu du paragraphe 4(2) de la <i>Loi sur les semences</i> et joue un r\u00f4le essentiel dans la pr\u00e9vention de l'introduction de nouvelles esp\u00e8ces de mauvaises herbes au Canada, en emp\u00eachant ou limitant la pr\u00e9sence d'esp\u00e8ce de mauvaises herbes dans les semences vendues ou import\u00e9es au Canada. L'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> est un outil utilis\u00e9 pour identifier et cat\u00e9goriser les esp\u00e8ces de mauvaises herbes pour les besoins d'importation et de vente de semences de m\u00eame qu'\u00e0 des fins de classement.</p>\n\n<h2 class=\"h5\">De quelle fa\u00e7on l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> est-il utilis\u00e9?</h2>\n\n<p>L'annexe <abbr title=\"1\">I</abbr> du <i>R\u00e8glement sur les semences</i> fournit les normes qui doivent \u00eatre satisfaites pour vendre ou importer au Canada ainsi que les normes qui doivent \u00eatre respect\u00e9es afin d'attribuer une cat\u00e9gorie \u00e0 un lot de semences. L'annexe <abbr title=\"1\">I</abbr> comprend 22 tableaux de classification, dont chacun est applicable \u00e0 certains types de cultures. Les tableaux de classification indiquent les taux maximums de graines de mauvaises herbes principales, secondaires, nuisibles et autres; ainsi que les autres types de culture et les mati\u00e8res inertes. L'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> est utilis\u00e9 en concomitance avec les tableaux de classification puisque l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> fournit la liste des esp\u00e8ces qui sont des graines de mauvaises herbes nuisibles principales, des graines de mauvaises herbes nuisibles secondaires ou des graines de mauvaises herbes nuisibles.</p>\n\n<p>L'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> fournit \u00e9galement la liste des esp\u00e8ces qui sont consid\u00e9r\u00e9es comme \u00e9tant des graines de mauvaises herbes nuisibles interdites dans les semences. L'alin\u00e9a 7(1) a) du <i>R\u00e8glement sur les semences</i> indique qu'aucune semence ne devrait contenir des graines de mauvaises herbes nuisibles interdites. C'est pour cette raison que les tableaux de classification ne comprennent aucune norme pour la cat\u00e9gorie 1\u00a0\u2013\u00a0Graines de mauvaises herbes nuisibles interdites; ces esp\u00e8ces sont interdites dans toutes les semences par l'entremise du R\u00e8glement.</p>\n\n<h2 class=\"h5\">Comment les esp\u00e8ces inscrites \u00e0 l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2016 sont-elles d\u00e9termin\u00e9es?</h2>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) a men\u00e9 de vastes consultations aupr\u00e8s des intervenants afin mettre au point l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2016. Les consultations ont d\u00e9but\u00e9 par deux ateliers dirig\u00e9s par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>; le premier a eu lieu le 29 octobre 2008 et le deuxi\u00e8me le 11 mars 2009. Tous les documents de consultation ont \u00e9t\u00e9 distribu\u00e9s par courriel \u00e0 la liste g\u00e9n\u00e9rale d'intervenants de la Section des semences de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> qui compte environ 2000 destinataires.</p>\n\n<p>\u00c0 la suite des ateliers, deux propositions consultatives ont \u00e9t\u00e9 distribu\u00e9es aux intervenants \u00e0 des fins de commentaires. La premi\u00e8re consultation a d\u00e9but\u00e9 le 23 octobre 2009 et a pris fin le 15 f\u00e9vrier 2010. Les r\u00e9ponses re\u00e7ues ont \u00e9t\u00e9 incorpor\u00e9es \u00e0 une proposition r\u00e9vis\u00e9e. La deuxi\u00e8me consultation a \u00e9t\u00e9 men\u00e9e entre le 17 juin 2011 et le 15 septembre 2011.</p>\n\n<p>Les modifications propos\u00e9es \u00e0 l'Arr\u00eat\u00e9 ont \u00e9t\u00e9 pr\u00e9publi\u00e9es dans Partie <abbr title=\"1\">I</abbr> de la <i>Gazette du Canad</i>a le 30 juin 2016 pour une p\u00e9riode d'observation de 75 jours. La version d\u00e9finitive de l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> r\u00e9vis\u00e9 a \u00e9t\u00e9 publi\u00e9e dans la Partie <abbr title=\"2\">II</abbr> de la <i>Gazette du Canada</i> le 18 mai 2016 avec une date d'entr\u00e9e en vigueur le 1<sup>er</sup> novembre 2016.</p>\n\n<h2>R\u00e9sum\u00e9 des changements</h2>\n\n<ol>\n<li class=\"mrgn-bttm-md\">Ajout, reclassification et \u00e9limination d'esp\u00e8ces\n\n<ul>\n<li class=\"mrgn-bttm-md mrgn-tp-sm\">La distribution g\u00e9ographique des esp\u00e8ces de mauvaises herbes peut changer avec le temps. Comme il s'est \u00e9coul\u00e9 plus de dix ans depuis le dernier examen complet de l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr>, plusieurs esp\u00e8ces sur l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2005 n'\u00e9taient plus ad\u00e9quatement class\u00e9es dans la bonne cat\u00e9gorie de l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr>. Par cons\u00e9quent, toutes les esp\u00e8ces inscrites sur l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2005 ont \u00e9t\u00e9 examin\u00e9es d'apr\u00e8s les renseignements actuels. L'examen a donn\u00e9 lieu \u00e0 la reclassification de 11 esp\u00e8ces de la cat\u00e9gorie 1 (Graines de mauvaises herbes nuisibles interdites) \u00e0 la cat\u00e9gorie 2 (Graines de mauvaises herbes nuisibles principales), et de 7 esp\u00e8ces de la cat\u00e9gorie 2 \u00e0 la cat\u00e9gorie 3 (Graines de mauvaises herbes nuisibles secondaires).</li>\n\n<li class=\"mrgn-bttm-md\">Au cours de l'examen de l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr>, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et les intervenants ont propos\u00e9 l'ajout de nouvelles esp\u00e8ces de mauvaises herbes \u00e0 l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr>. En d\u00e9finitive, 16 esp\u00e8ces ont \u00e9t\u00e9 ajout\u00e9es \u00e0 la cat\u00e9gorie 1, 16 esp\u00e8ces ont \u00e9t\u00e9 ajout\u00e9es \u00e0 la cat\u00e9gorie 2 et 5 esp\u00e8ces ont \u00e9t\u00e9 ajout\u00e9es \u00e0 la cat\u00e9gorie 3.</li>\n\n<li class=\"mrgn-bttm-md\">Deux mauvaises herbes, <i lang=\"la\">Camelina</i> <abbr title=\"esp\u00e8ce\">spp.</abbr> et <i lang=\"la\">Chicorium intybus</i> ont \u00e9t\u00e9 retir\u00e9s de la cat\u00e9gorie 3.</li>\n</ul></li>\n\n<li class=\"mrgn-bttm-md\">Cat\u00e9gorie 2 s'appliquant \u00e0 tous les tableaux\n\n<ul>\n<li class=\"mrgn-bttm-md mrgn-tp-sm\">L'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> est utilis\u00e9 en concomitance avec les tableaux de classification de l'annexe <abbr title=\"1\">I</abbr> du <i>R\u00e8glement sur les semences</i> afin de classifier la semence et d\u00e9terminer si un lot de semence respecte les besoins d'importations.</li>\n\n<li class=\"mrgn-bttm-md\">Dans l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2005, la cat\u00e9gorie 2 (Graines de mauvaises herbes nuisibles principales) ne s'appliquait pas aux tableaux <abbr title=\"14\">XIV</abbr> et <abbr title=\"15\">XV</abbr> de l'annexe <abbr title=\"1\">I</abbr>. L'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2016 comprend les modifications que la cat\u00e9gorie 2 (Graines de mauvaises herbes nuisibles principales) applique \u00e0 tous les tableaux de classification.</li>\n\n<li class=\"mrgn-bttm-md\">Cela a repr\u00e9sent\u00e9 un changement important, bien que certaines esp\u00e8ces aient \u00e9t\u00e9 d\u00e9plac\u00e9es de la cat\u00e9gorie 1 \u00e0 la cat\u00e9gorie 2, puisqu'il demeure essentiel de r\u00e9duire au minimum les risques d'effectuer d'autres introductions de ces esp\u00e8ces de mauvaises herbes. Sans l'application de la cat\u00e9gorie 2 \u00e0 tous les tableaux de classification, il y avait un risque que ces esp\u00e8ces de cat\u00e9gorie 2 puissent se propager par l'entremise des semences.</li>\n</ul></li>\n</ol>\n\n<h2>Ressources de formation</h2>\n\n<p>La Section des sciences et technologies des semences (SSTS) de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a \u00e9tabli des fiches d'information sur l'identification des semences pour toutes les esp\u00e8ces inscrites \u00e0 l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> de 2016. Ces ressources de formation seront bient\u00f4t accessibles en ligne. L'Association des analystes de semences commerciales du Canada tient des ateliers de formation en 2016 partout au pays afin de former leurs membres-analystes des semences.</p>\n\n<h2>R\u00e8glementation des esp\u00e8ces de mauvaises herbes avec la <i>Loi sur la protection des v\u00e9g\u00e9taux</i></h2>\n\n<p>En f\u00e9vrier 2013, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a ajout\u00e9 plusieurs esp\u00e8ces de mauvaises herbes \u00e0 la <i>Liste des parasites r\u00e9glement\u00e9s par le Canada</i>. Ces esp\u00e8ces sont r\u00e9glement\u00e9es pour toutes les marchandises, y compris les semences, qui comportent des exigences particuli\u00e8res indiqu\u00e9es dans la directive \u00ab\u00a0<a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-12-01/fra/1380720513797/1380721302921\">D-12-01\u00a0: Exigences phytosanitaires visant \u00e0 pr\u00e9venir l'introduction de v\u00e9g\u00e9taux r\u00e9glement\u00e9s comme \u00e9tant des organismes nuisibles au Canada</a>.\u00a0\u00bb</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}