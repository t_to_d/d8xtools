Note: this readme is mostly complete.



Import & Export - Cheat Sheet 
*****change file name from _02-15 to _month-day*****
1- a partir du local
cd d8xtools/ 
//mkdir internet_export_02-15 following script will create it
php csv_to_json_export.php teamsite_data/Internet_General_Metadata-Take3.csv
php update_json.php /home/mo/d8xtools/internet_export_02-15/sitemapinit sitemap
exit

2- a partir du local
tar -pczf internet_export_02-15.tgz internet_export_02-15
scp -P52225 internet_export_02-15.tgz govaafcmo@9pro.ca:~


3- ssh govaafcmo@9pro.ca -p52225
cd docroot/
drush scr delete_all_news_empl.php
// ~ means /home/username
mv ~/internet_export_02-15.tgz ../d8xtools/
cd ../d8xtools/
tar -pxzf internet_export_02-15.tgz
cd ../docroot/

drush scr /var/www/clients/client3/web65/web/d8xtools/drupal_export_import/import.php /var/www/clients/client3/web65/web/d8xtools/internet_export_02-15 sitemapinit

Error Workaround. Manually: Go to site, create a node, delete that node and run
drush scr delete_all_news_empl.php
drush scr /var/www/clients/client3/web65/web/d8xtools/drupal_export_import/import.php /var/www/clients/client3/web65/web/d8xtools/internet_export_02-15 sitemapinit


from the AAFC network:

exporting steps:

cd d8xtools; #clone it from agr-cms gitlab if you haven't already.
git pull; #make sure up to date code
cd d8xtools/internetx/xml2json;
php -f teamsite_sitemap_xml2json.php ../sitemap_teamsite_COVID5sept28.xml
 #copy and paste json output into a file (clunky ,but can improve this step later).

# if any of these export scripts fails autoload of classes (which they will on first run) do edit the require at the top as follows:
require 'C:\Users\OlstadJ\d8xtools\vendor\autoload.php';
# change OlstadJ to your username
# on windows you'll also need to install php as this won't work without php (using php version 7.3.15).

php csv_to_json_export.php ~/d8xtools/Intranet_Database_dumpEN_FR_tab_adjusted_Sept25-2020.csv \t
  # output is json, have to copy paste, shrink fontsize to very small to avoid line wrap issues, paste text output into a new json file called scrape_internetx_COVID4.json , if needed use a json validator to find invalid lines and fix them, mostlikely caused by linewrap issues mitigate with shrinking font size of your window
php internetx/process_sitemap_COVID2.php ~/d8xtools/internetx/scrape_internetx_COVID4.json
  # due to some legacy sitemap garbage, some files json files were manually massaged so that gene-gene pages import correctly as they have dependency on these.

php update_json.php ~/d8xtools/export_sitemap_10-02 sitemap # see next line massaging, 3 records, added chmod 444 read only to those.
  # I put chmod 444 on the 3 files in ~/d8xtools/export/sitemap that I don't want overwritten, this seems to work.

php update_json.php ~/d8xtools/export/sitemap sitemap # I only check in the non-massaged ones here (export/sitemap). chmod 444 seems to work
  # update_json.php exporting sitemap (landing_page) data from legacy web page directly
  # After this is done I use git diff to compare old with new sitemap export to avoid changing those manually massaged files by doing a diff vs the previous run (git history is the export_sitemap folder).
php update_json.php ~/d8xtools/new_export_10-02/gene-gene teamsite ~/d8xtools/export/sitemap
  # above step is to fix broken references in gene-gene using the sitemap data as a comparison (IMPORTANT STEP!)
php update_json.php ~/d8xtools/new_export_10-02/gene-gene teamsite
  # update_json.php exporting gene-gene (page) data from legacy web page directly
php update_json.php ~/d8xtools/new_export_10-02/nwinit-nwinit teamsite
php update_json.php ~/d8xtools/new_export_10-02/home-accu teamsite

git add teamsite_export/gene-gene



Drupal Bug:
When you run any import or update script, you receive the error:
Drupal\Componet\Plugin\Exception\PlugninNOtFoundException: The "EntityHasField" plugin does not exist.
Valid plugin IDs for Drupal\Core\ValidationConstraintManger are: Callback...

In SqlContentEntiyStorage.php line 846:
The "EntityHasField" plugin does not exist. 

Go to /en/admin/content, add any fake node.
After that, run the script agian.

Run the script to convert the script again.




cd /var/www/path/to/web/d8xtools/convertTeamsiteIdToDrupal/;
php exportContentURL.php /var/www/path/to/web/d8xtools/convertTeamsiteIdToDrupal/export;
php retrieveFileContentFromAafcInternet.php export;
rm ../../docroot/html/sites/default/files/legacy -rf;
mv exportcontent ../../docroot/html/sites/default/files/legacy;
cd /var/www/path/to/web/docroot;

# Jun post import update scripts:
drush scr ../d8xtools/convertTeamsiteIdToDrupal/convert_teamsiteId_to_drupal.php FULL_MEAL_DEAL
drush scr ../d8xtools/convertTeamsiteIdToDrupal/updateContentURL.php /full/path/to/d8xtools/convertTeamsiteIdToDrupal/export/
# For the last command please use the full path.


# post-script instruction 
#@TODO


