{
    "dcr_id": "1669743992433",
    "title": {
        "en": "Assessing lameness for transport",
        "fr": "\u00c9valuation des boiteries pour le transport"
    },
    "html_modified": "2024-01-26 2:26:04 PM",
    "modified": "2023-06-27",
    "issued": "2022-11-30",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/assessing_lameness_for_transport_1669743992433_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/assessing_lameness_for_transport_1669743992433_fra"
    },
    "ia_id": "1669744280930",
    "parent_ia_id": "1658102690372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1658102690372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Assessing lameness for transport",
        "fr": "\u00c9valuation des boiteries pour le transport"
    },
    "label": {
        "en": "Assessing lameness for transport",
        "fr": "\u00c9valuation des boiteries pour le transport"
    },
    "templatetype": "content page 1 column",
    "node_id": "1669744280930",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1658102689888",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/humane-transport/guidance-and-resources/assessing-lameness-for-transport/",
        "fr": "/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/directives-et-ressources/evaluation-des-boiteries-pour-le-transport/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Assessing lameness for transport",
            "fr": "\u00c9valuation des boiteries pour le transport"
        },
        "description": {
            "en": "Anyone transporting an animal in Canada must meet specific requirements to make sure the animal is fit for the intended journey.",
            "fr": "Toute personne qui transporte un animal au Canada doit satisfaire \u00e0 des exigences sp\u00e9cifiques pour s'assurer que l'animal est apte \u00e0 endurer le voyage pr\u00e9vu."
        },
        "keywords": {
            "en": "Animals, Animal Health, Livestock, transport, Canada, Assessing lameness for transport",
            "fr": "Animaux, Sant\u00e9 des animaux, Transport, b\u00e9tail, Canada, \u00c9valuation des boiteries pour le transport"
        },
        "dcterms.subject": {
            "en": "animal,animal diseases,animal health,animal inspection,inspection,livestock,regulations,transport",
            "fr": "animal,maladie animale,sant\u00e9 animale,inspection des animaux,inspection,b\u00e9tail,r\u00e9glementations,transport"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-11-30",
            "fr": "2022-11-30"
        },
        "modified": {
            "en": "2023-06-27",
            "fr": "2023-06-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Assessing lameness for transport",
        "fr": "\u00c9valuation des boiteries pour le transport"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/assessing_lameness_for_transport_1669742815756_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Assessing lameness for transport in Portable document format\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Assessing lameness for transport</span> <span class=\"gc-dwnld-info nowrap\">(PDF \u2013 187 kb)</span></p>\n</div>\n</div>\n</div></a>\n</div>\n</div>\n\n<figure class=\"mrgn-bttm-lg\"> \n<img alt=\"Assessing lameness for transport - description follows\" class=\"mrgn-bttm-sm img-responsive\" src=\"/DAM/DAM-animals-animaux/STAGING/images-images/assessing_lameness_for_transport_1669743536430_eng.jpg\"> \n<details> \n<summary>Assessing lameness for transport\u00a0\u2013 Text version</summary>\n \n<p>Anyone transporting an animal in Canada must meet specific requirements to make sure the animal is fit for the intended journey.</p>\n<p><strong>Question 1: Is the animal ambulatory (able to stand and walk without assistance)?</strong></p>\n<ul>\n<li><strong>If no</strong>, the animal is unfit and must <strong>not</strong> be transported, unless to receive veterinary care as recommended by a veterinarian</li>\n<li><strong>If yes, </strong>move on to question 2</li>\n</ul>\n<p><strong>Question 2: Does the animal have a fracture that impedes their mobility or that causes pain or suffering?</strong></p>\n<ul>\n<li><strong>If no,</strong> move on to question 3</li>\n<li><strong>If yes,</strong> the animal is unfit and must <strong>not</strong> be transported, unless to receive veterinary care as recommended by a veterinarian</li>\n</ul>\n<p><strong>Question 3: Is the animal able to walk on all their legs?</strong></p>\n<ul>\n<li><strong>If no,</strong> the animal is unfit and must <strong>not</strong> be transported, unless to receive veterinary care as recommended by a veterinarian</li>\n<li><strong>If yes,</strong> move on to question 4</li>\n</ul>\n<p><strong>Question 4: Does the animal walk with a smooth and steady motion, a straight back and normal head carriage while bearing their weight evenly on all limbs?</strong></p>\n<ul>\n<li><strong>If no,</strong> move on to question 5</li>\n<li><strong>If yes,</strong> the animal isn't lame and may be transported</li>\n</ul>\n<p><strong>Question 5: Is the animal reluctant to walk or walks with halted movements due to pain or suffering (shown by an arched back, head bobs, reluctance to bear weight on a limb, etc.)? </strong></p>\n<ul>\n<li><strong>If no,</strong> the animal is compromised and must be transported as such</li>\n<li><strong>If yes,</strong> the animal is unfit and must <strong>not</strong> be transported, unless to receive veterinary care as recommended by a veterinarian</li>\n</ul>\n<p><strong>Definitions</strong></p>\n<p>An <strong>unfit</strong> animal can only be transported (with special provisions) directly to a place to receive veterinary care and under the recommendation of a veterinarian.</p>\n<p>A <strong>compromised</strong> animal can only be transported (with special provisions) directly for slaughter or to a place to receive care for their condition. They cannot\u00a0be transported to an assembly centre or auction market.</p>\n\n<p><strong>Requirements</strong></p>\n<p>Special provisions if transporting an unfit or compromised animal:</p>\n<ul>\n<li>isolate the animal</li>\n<li>load or unload the animal alone without using interior ramps</li>\n<li>take special measures to prevent unnecessary suffering</li>\n<li>transport directly to the nearest suitable place to provide care or to humanely kill the animal</li>\n</ul>\n</details>\n  \n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/assessing_lameness_for_transport_1669742815756_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"\u00c9valuation des boiteries pour le transport en format PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>\u00c9valuation des boiteries pour le transport</span> <span class=\"gc-dwnld-info nowrap\">(PDF \u2013 1 093 ko)</span></p>\n</div>\n</div>\n</div></a>\n</div>\n</div>\n\n\n<figure class=\"mrgn-bttm-lg\"> \n<img alt=\"\u00c9valuation des boiteries pour le transport - description ci-dessous\" class=\"mrgn-bttm-sm img-responsive\" src=\"/DAM/DAM-animals-animaux/STAGING/images-images/assessing_lameness_for_transport_1669743536430_fra.jpg\"> \n<details> \n<summary>\u00c9valuation des boiteries pour le transport\u00a0\u2013 Version texte</summary>\n\n<p>Toute personne qui transporte un animal au Canada doit satisfaire \u00e0 des exigences sp\u00e9cifiques pour s'assurer que l'animal est apte \u00e0 endurer le voyage pr\u00e9vu.</p>\n<p><strong>Question 1\u00a0: Est-ce que l'animal est ambulatoire (capable de marcher et de se tenir debout sans assistance)?</strong></p>\n<ul>\n<li><strong>Si non</strong>, l'animal est inapte et <strong>ne doit pas</strong> \u00eatre transport\u00e9, \u00e0 moins que ne ce soit pour recevoir des soins tel que recommand\u00e9 par un v\u00e9t\u00e9rinaire.</li>\n<li><strong>Si oui</strong>, passez \u00e0 la question 2 </li>\n</ul>\n<p><strong>Question 2\u00a0: Est-ce que l'animal a une fracture qui g\u00eane sa mobilit\u00e9 ou qui cause de la douleur ou de la souffrance?</strong></p>\n<ul>\n<li><strong>Si non</strong>, passez \u00e0 la question 3 </li>\n<li><strong>Si oui</strong>, l'animal est inapte et <strong>ne doit pas</strong> \u00eatre transport\u00e9, \u00e0 moins que ne ce soit pour recevoir des soins tel que recommand\u00e9 par un v\u00e9t\u00e9rinaire.</li>\n</ul>\n<p><strong>Question 3\u00a0: Est-ce que l'animal est capable de marcher sur tous ses membres?</strong></p>\n<ul>\n<li><strong>Si non</strong>, l'animal est inapte et <strong>ne doit pas</strong> \u00eatre transport\u00e9, \u00e0 moins que ne ce soit pour recevoir des soins tel que recommand\u00e9 par un v\u00e9t\u00e9rinaire.</li>\n<li><strong>Si oui</strong>, passez \u00e0 la question 4 </li>\n</ul>\n<p><strong>Question 4\u00a0: Est-ce que l'animal a une d\u00e9marche fluide et r\u00e9guli\u00e8re avec le dos droit, un port de t\u00eate normal et en portant son poids uniform\u00e9ment sur tous ses membres?</strong></p>\n<ul>\n<li><strong>Si non</strong>, passez \u00e0 la question 5 </li>\n<li><strong>Si oui</strong>, l'animal ne boite pas et peut \u00eatre transport\u00e9. </li>\n</ul>\n<p><strong>Question 5\u00a0: Est-ce que l'animal montre de la r\u00e9ticence \u00e0 marcher, ou marche par mouvements saccad\u00e9s, en raison de la douleur ou de la souffrance (dos arqu\u00e9, hochements de t\u00eate, r\u00e9ticence \u00e0 porter du poids sur un membre, entre autre)?</strong></p>\n<ul>\n<li><strong>Si non</strong>, l'animal est fragilis\u00e9 et doit \u00eatre transport\u00e9 comme tel.</li>\n<li><strong>Si oui</strong>, l'animal est inapte et <strong>ne doit pas</strong> \u00eatre transport\u00e9, \u00e0 moins que ne ce soit pour recevoir des soins tel que recommand\u00e9 par un v\u00e9t\u00e9rinaire.</li>\n</ul>\n\n<p><strong>D\u00e9finitions</strong></p>\n<p>Un animal <strong>inapte</strong> peut seulement \u00eatre transport\u00e9 (avec des dispositions sp\u00e9ciales) directement \u00e0 un endroit o\u00f9 il recevra des soins v\u00e9t\u00e9rinaires et sous la recommandation d'un v\u00e9t\u00e9rinaire. </p>\n<p>Un animal <strong>fragilis\u00e9</strong> peut seulement \u00eatre transport\u00e9 (avec des dispositions sp\u00e9ciales) directement aux fins d'abattage ou \u00e0 un endroit o\u00f9 il recevra des soins pour son \u00e9tat sp\u00e9cifique. Il ne peut \u00eatre transport\u00e9 vers un centre de rassemblement ou march\u00e9 de vente aux ench\u00e8res (encan).</p>\n<p><strong>Exigences</strong></p>\n<p><strong>Dispositions sp\u00e9ciales en cas de transport d'un animal inapte ou fragilis\u00e9</strong></p>\n<ul>\n<li>Isoler l'animal</li>\n<li>Embarquer ou d\u00e9barquer l'animal seul sans utiliser de rampes int\u00e9rieures</li>\n<li>Prenez des mesures sp\u00e9ciales pour pr\u00e9venir les souffrances inutiles</li>\n<li>Transportez directement au lieu appropri\u00e9 le plus proche o\u00f9 prodiguer des soins \u00e0 l\u2019animal ou le tuer sans cruaut\u00e9</li>\n</ul>\n</details>\n\n  \n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}