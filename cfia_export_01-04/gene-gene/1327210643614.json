{
    "dcr_id": "1327210643614",
    "title": {
        "en": "Viral Hemorrhagic Septicemia (VHS) - Fact Sheet",
        "fr": "Septic\u00e9mie h\u00e9morragique virale (SHV) - Fiche de renseignements"
    },
    "html_modified": "2012-01-22 00:43",
    "modified": "25-11-2013",
    "issued": "22-1-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/aqua_dis_vhs_questions_1327210643614_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/aqua_dis_vhs_questions_1327210643614_fra"
    },
    "parent_ia_id": "1327209371030",
    "ia_id": "1327210771329",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Viral Hemorrhagic Septicemia (VHS) - Fact Sheet",
        "fr": "Septic\u00e9mie h\u00e9morragique virale (SHV) - Fiche de renseignements"
    },
    "label": {
        "en": "Viral Hemorrhagic Septicemia (VHS) - Fact Sheet",
        "fr": "Septic\u00e9mie h\u00e9morragique virale (SHV) - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1327210771329",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1327208906158",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/vhs/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/shv/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Viral Hemorrhagic Septicemia (VHS) - Fact Sheet",
            "fr": "Septic\u00e9mie h\u00e9morragique virale (SHV) - Fiche de renseignements"
        },
        "description": {
            "en": "VHS is an infectious disease of finfish. It is caused by the viral hemorrhagic septicemia virus, which belongs to the family Rhabdoviridae.",
            "fr": "La SHV est une maladie infectieuse des poissons. Cette maladie est provoqu\u00e9e par le virus de la septic\u00e9mie h\u00e9morragique virale, qui appartient \u00e0 la famille des Rhabdovirid\u00e9s."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, diseases, imports, questions and answers, viral hemorrhagic septicemia, VHS, viral hemorrhagic septicemia virus, Rhabdoviridae, freshwater fish",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, maladies, importation, questions et r\u00e9ponses, Septic\u00e9mie h\u00e9morragique virale, SHV, virus de la septic\u00e9mie h\u00e9morragique virale, Rhabdovirid\u00e9s, poissons d'eau douce"
        },
        "dcterms.subject": {
            "en": "imports,inspection,fish,policy,animal health",
            "fr": "importation,inspection,poisson,politique,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Direction",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-22 00:43:29",
            "fr": "2012-01-22 00:43:29"
        },
        "modified": {
            "en": "2013-11-25",
            "fr": "2013-11-25"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Viral Hemorrhagic Septicemia (VHS) - Fact Sheet",
        "fr": "Septic\u00e9mie h\u00e9morragique virale (SHV) - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is viral hemorrhagic septicemia (VHS)?</h2>\n<p><abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> is an infectious disease of finfish. It is caused by the viral hemorrhagic septicemia virus, which belongs to the family <span lang=\"la\"><i>Rhabdoviridae</i></span>.</p>\n<h2 class=\"h5\">Is <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> a public health risk?</h2>\n<p>No, <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> does not pose a human health risk.</p>\n<h2 class=\"h5\">What are the clinical signs of <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr>?</h2>\n<p>Diseased freshwater fish may exhibit any or all of the following signs of <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr>:</p>\n<ul>\n<li>bulging eyes</li>\n<li>pale gills</li>\n<li>signs of bleeding around the eyes, bases of the fins, sides and head</li>\n<li>dark colouration</li>\n<li>distended (fluid-filled) belly</li>\n<li>gasping at the surface</li>\n<li>corkscrew swimming behaviour</li>\n<li>high death rate</li>\n</ul>\n<h2 class=\"h5\">Where is <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> found?</h2>\n<p>There are several known strains of <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> virus that affect many freshwater and marine fish species in the northern hemisphere of the world. In Canada, <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> virus infects certain species of finfish in the Pacific and Atlantic oceans and the Pacific Ocean watershed of British Columbia and the Atlantic Ocean watersheds in New Brunswick and Nova Scotia.</p>\n<p>The first occurrence of <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> in the Canadian Great Lakes was detected in Lake Ontario in 2005.</p>\n<p><abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> virus has been found in finfish from Lake Erie, Lake Huron, Lake Simcoe, Lake Superior, Lower Thames River, and the <abbr title=\"saint\">St.</abbr> Lawrence River west of the Moses-Saunders dam in Cornwall, Ontario.</p>\n<h2 class=\"h5\">How is <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> transmitted and spread?</h2>\n<p><abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> infection is known to spread from fish to fish in one of two ways: by feeding upon diseased fish or by living in contaminated water. Water becomes contaminated when infected fish shed the virus through their urine or when infected fish die and decompose releasing the virus. People can spread the virus by moving contaminated fish, water, boats and other equipment between bodies of water. Fish-eating birds, such as herons, may also transmit the virus.</p>\n<p>Fish do not have to be sick to transmit the virus. Healthy fish can also carry and shed the virus.</p>\n<h2 class=\"h5\">How is <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> diagnosed?</h2>\n<p><abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> may be suspected based on the clinical signs. Diagnosis must be confirmed by laboratory tests.</p>\n<h2 class=\"h5\">How is <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr> treated?</h2>\n<p>There is no specific treatment for <abbr title=\"Viral Hemorrhagic Septicemia\">VHS</abbr>.</p>\n<h2 class=\"h5\">What measures can be taken to prevent the introduction and spread of viral hemorrhagic septicemia?</h2>\n<p>If you frequently handle or work with finfish, be aware of the clinical signs of viral hemorrhagic septicemia.</p>\n<p>Do not introduce live finfish from another country or another province into the natural waters of Canada.</p>\n<ul>\n<li>People releasing finfish into the natural waters or in rearing facilities within Canada should check if federal or provincial and/or territorial permits are required.</li>\n</ul>\n<p>Do not use finfish that were bought in a grocery store as bait for catching finfish or other aquatic animals.</p>\n<p>When cleaning and gutting finfish, dispose of all waste in your municipal garbage.</p>\n<p>The Canadian Food Inspection Agency (CFIA) recommends that you do not visit Canadian aquaculture farms, zoos or aquariums for 14 days if you have travelled to another country and</p>\n<ul>\n<li>visited an aquaculture farm, or</li>\n<li>had contact with wild finfish.</li>\n</ul>\n<p>Once in Canada, wash and disinfect the footwear you wore to the farm or when you had contact with wild finfish. Also wash your clothing thoroughly and dry it at a high temperature.</p>\n<p>Do not import live infected finfish into Canada.</p>\n<ul>\n<li>An import permit will be required from the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for certain species of finfish as of December 2011.</li>\n</ul>\n<p>People bringing finfish into Canada should check other federal, provincial, and/or territorial requirements before entering the country.</p>\n<h2 class=\"h5\">What is done to protect Canadian aquatic animals from viral hemorrhagic septicemia?</h2>\n<p>Viral hemorrhagic septicemia is a reportable disease in Canada. This means that anyone who owns or works with aquatic animals, who knows of or suspects viral hemorrhagic septicemia in their fish, is required by law to notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p>If viral hemorrhagic septicemia is found in Canada, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would control its spread by implementing disease response activities. These may include</p>\n<ul>\n<li>controlling the movements of infected animals that people own or work with</li>\n<li>humanely destroying infected animals</li>\n<li>cleaning and disinfecting</li>\n</ul>\n<p>The control measures chosen would depend on the situation.</p>\r\n<h2 class=\"font-medium black\">How do I get more information</h2>\n<p>For more information about reportable diseases, visit the <a href=\"/animal-health/aquatic-animals/eng/1299155892122/1320536294234\">Aquatic Animal Health</a> page, contact your <a href=\"/eng/1300462382369/1365216058692\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Animal Health Office</a>,  or your <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area office:</p>\n\n<ul><li>Atlantic: 506-777-3939</li>\n<li>Quebec: 514-283-8888</li>\n<li>Ontario: 226-217-8555</li>\n<li>West: 587-230-2200</li></ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que la septic\u00e9mie h\u00e9morragique virale (SHV)?</h2>\n<p>La <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> est une maladie infectieuse des poissons. Cette maladie est provoqu\u00e9e par le virus de la septic\u00e9mie h\u00e9morragique virale, qui appartient \u00e0 la famille des Rhabdovirid\u00e9s.</p>\n<h2 class=\"h5\">La <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> pose-t-elle un risque pour la sant\u00e9 publique?</h2>\n<p>Non, la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> ne pr\u00e9sente aucun risque pour la sant\u00e9 humaine.</p>\n<h2 class=\"h5\">Quels sont les signes cliniques de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr>?</h2>\n<p>Les poissons d'eau douce infect\u00e9s par le virus peuvent montrer quelques-uns ou tous les sympt\u00f4mes suivants de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr>\u00a0:</p>\n<ul>\n<li>yeux exorbit\u00e9s</li>\n<li>branchies d\u00e9color\u00e9es</li>\n<li>signes de saignements autour des yeux, \u00e0 la base des nageoires, sur les flancs et sur la t\u00eate</li>\n<li>couleur fonc\u00e9e</li>\n<li>ventre protub\u00e9rant (gonfl\u00e9 de liquide)</li>\n<li>prise d'air \u00e0 la surface</li>\n<li>d\u00e9placement en tire-bouchon</li>\n<li>taux de mortalit\u00e9 \u00e9lev\u00e9\u00a0</li>\n</ul>\n<h2 class=\"h5\">O\u00f9 trouve-t-on la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr>?</h2>\n<p>Il existe plusieurs souches connues du virus de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> qui touchent de nombreuses esp\u00e8ces de poissons d'eau douce et de mer de l'h\u00e9misph\u00e8re Nord de la plan\u00e8te. Au Canada, le virus de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> infecte certaines esp\u00e8ces de poissons dans les oc\u00e9ans Pacifique et Atlantique ainsi que dans le bassin versant du Pacifique en Colombie-Britannique et les bassins versants de l'Atlantique au Nouveau-Brunswick et en Nouvelle-\u00c9cosse.</p>\n<p>Le premier cas de <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> dans les Grands Lacs canadiennes a \u00e9t\u00e9 d\u00e9tect\u00e9 dans le lac Ontario en 2005.</p>\n<p>Le virus de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> a \u00e9t\u00e9 d\u00e9tect\u00e9e chez des poissons dans le lac \u00c9ri\u00e9, le lac Huron, le lac Simcoe, et Sup\u00e9rieur, le cours inf\u00e9rieur de la rivi\u00e8re Thames et le fleuve Saint-Laurent, \u00e0 l'ouest du barrage Moses-Saunders, \u00e0 Cornwall, en Ontario.</p>\n<h2 class=\"h5\">Comment la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> est-elle transmise et propag\u00e9e?</h2>\n<p>On sait que la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> est transmise d'un poisson \u00e0 l'autre de l'une des deux fa\u00e7ons suivantes\u00a0: lorsque les poissons se nourrissent de poissons morts ou vivent dans des eaux contamin\u00e9es. Les poissons infect\u00e9s contaminent l'eau en propageant le virus par l'entremise de leur urine ou en se d\u00e9composant lorsqu'ils meurent. Les gens peuvent propager le virus en transportant d'un plan d'eau \u00e0 un autre des poissons, de l'eau, des bateaux ou d'autres pi\u00e8ces d'\u00e9quipement contamin\u00e9s. Les oiseaux piscivores, comme les h\u00e9rons, peuvent \u00e9galement transmettre le virus.</p>\n<p>Les poissons ne doivent pas n\u00e9cessairement \u00eatre malades pour transmettre le virus. Ceux en sant\u00e9 peuvent \u00e9galement \u00eatre porteurs du virus et le propager.</p>\n<h2 class=\"h5\">Comment la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> est-elle diagnostiqu\u00e9e?</h2>\n<p>On peut soup\u00e7onner la pr\u00e9sence de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr> en se fondant sur les signes cliniques. Le diagnostic doit \u00eatre confirm\u00e9 par des analyses en laboratoire.</p>\n<h2 class=\"h5\">Comment traite-t-on la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr>?</h2>\n<p>Il n'existe aucun traitement particulier pour la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr>.</p>\n<h2 class=\"h5\">Quelles mesures peut-on prendre pour emp\u00eacher l'introduction et la propagation de la <abbr title=\"Septic\u00e9mie h\u00e9morragique virale\">SHV</abbr>?</h2>\n<p>Si vous travaillez souvent avec des poissons ou les manipulez, vous devez \u00eatre capable de reconna\u00eetre les sympt\u00f4mes de la septic\u00e9mie h\u00e9morragique virale.</p>\n<p>N'introduisez pas de poisson vivant provenant d'un autre pays ou d'une autre province dans un plan d'eau naturel au Canada.</p>\n<ul>\n<li>Les personnes qui rel\u00e2chent des poissons dans des plans d'eau naturels ou des \u00e9tablissements d'\u00e9levage de poissons du Canada devraient v\u00e9rifier si elles ont besoin d'un permis f\u00e9d\u00e9ral, provincial ou territorial.</li>\n</ul>\n<p>N'utilisez pas de poisson achet\u00e9 au supermarch\u00e9 comme app\u00e2t pour attraper du poisson ou d'autres animaux aquatiques.</p>\n<p>Lors du nettoyage et de l'\u00e9visc\u00e9ration de poissons, jetez tous les d\u00e9chets aux\u00a0 ordures.</p>\n<p>L'Agence canadienne d'inspection des aliments (ACIA) vous recommande de ne pas visiter d'\u00e9tablissement aquacole, de zoo ou d'aquarium au Canada pendant 14\u00a0jours si vous avez voyag\u00e9 dans un autre pays et si vous\u00a0:</p>\n<ul>\n<li>avez visit\u00e9 un \u00e9tablissement aquacole;</li>\n<li>\u00eates entr\u00e9 en contact avec des poissons sauvages.</li>\n</ul>\n<p>De retour au Canada, lavez et d\u00e9sinfectez les chaussures que vous portiez pour vous rendre \u00e0 l'\u00e9tablissement ou lorsque vous \u00eates entr\u00e9 en contact avec des poissons sauvages. Lavez \u00e9galement soigneusement vos v\u00eatements et s\u00e9chez-les \u00e0 temp\u00e9rature \u00e9lev\u00e9e.</p>\n<p>N'importez pas au Canada de poisson infect\u00e9 vivant.</p>\n<ul>\n<li>\u00c0 compter de d\u00e9cembre\u00a02011, un permis d'importation sera requis par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pour certaines esp\u00e8ces de poissons.</li>\n</ul>\n<p>Les personnes qui rapportent des poissons au Canada devraient v\u00e9rifier les autres exigences f\u00e9d\u00e9rales, provinciales et territoriales avant de les importer.</p>\n<h2 class=\"h5\">Quelles sont les mesures prises pour prot\u00e9ger les animaux aquatiques du Canada contre la septic\u00e9mie h\u00e9morragique virale?</h2>\n<p>Au Canada, la septic\u00e9mie h\u00e9morragique virale est une maladie \u00e0 d\u00e9claration obligatoire. Par cons\u00e9quent, quiconque poss\u00e8de des animaux aquatiques ou travaille avec de tels animaux et soup\u00e7onne ou d\u00e9c\u00e8le la pr\u00e9sence de la septic\u00e9mie h\u00e9morragique virale chez les animaux qu'il poss\u00e8de ou avec lesquels il travaille est tenu par la loi d'en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Si l'on d\u00e9c\u00e8le la pr\u00e9sence de la septic\u00e9mie h\u00e9morragique virale, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> contr\u00f4lera sa propagation en mettant en \u0153uvre des activit\u00e9s d'intervention en cas de maladie, par exemple\u00a0:</p>\n<ul>\n<li>contr\u00f4ler les d\u00e9placements des animaux infect\u00e9s que les personnes poss\u00e8dent ou avec lesquels elles travaillent;</li>\n<li>d\u00e9truire sans cruaut\u00e9 les animaux infect\u00e9s;</li>\n<li>nettoyer et d\u00e9sinfecter.</li>\n</ul>\n<p>Les mesures de contr\u00f4le retenues d\u00e9pendront de la situation.</p>\r\n<h2 class=\"font-medium black\">Comment puis-je obtenir davantage de renseignements</h2>\n<p>Pour de plus amples renseignements sur les maladies d\u00e9clarables, consultez la page <a href=\"/sante-des-animaux/animaux-aquatiques/fra/1299155892122/1320536294234\">Sant\u00e9 des animaux aquatiques</a>,  communiquez avec votre <a href=\"/fra/1300462382369/1365216058692\">bureau local de sant\u00e9 des animaux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou  le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de votre centre op\u00e9rationnel\u00a0: </p>\n<ul><li>Atlantique : 506-777-3939</li>\n<li>Qu\u00e9bec : 514-283-8888</li>\n<li>Ontario : 226-217-8555</li>\n<li>Ouest : 587-230-2200</li></ul>\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}