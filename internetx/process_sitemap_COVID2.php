<?php
//require 'vendor/autoload.php';
error_reporting(E_ALL);

if (!isset($argv[1])) {
  echo "usage: " . $argv[0] . ' /path/to/sitemap.json' . "\n";
  echo "need to specify the path to sitemap.json.";
  die;
}

class AgrisourceJsonHelper {

  public static function getParentNodeId($parentElement) {
    return $parentElement[0]->{'@attribute'}->nodeId;
  }

  public static function popLastClassRef($selectorString) {
    $posEnd = strripos($selectorString, '->'); 
    $newSelectorString = substr($selectorString, 0, $posEnd + 1);
  }

}

global $debug_mode;
$debug_mode = FALSE; // default to disabled.

$json_string=file_get_contents($argv[1]/*"/var/www/clients/client1/web38/web/d8tools/agrisource/scrape_agrisource.json"*/);
$json_obj = json_decode($json_string);

$news = $json_obj->SiteMap->English->Node->Node[0]->Node[0];
$find_ppl = $json_obj->SiteMap->English->Node->Node[0]->Node[1];
$branches = $json_obj->SiteMap->English->Node->Node[0]->Node[2];
$human_res = $json_obj->SiteMap->English->Node->Node[0]->Node[3];
$our_dept = $json_obj->SiteMap->English->Node->Node[0]->Node[4];
$news_first = $news->Node[0]->{'@attributes'}->nodeId;


$new_array = array();
$cnt=0;
$oCnt=0;
$oiCnt=0;
$iCnt=0;
$iiCnt = 0;
$iiiCnt = 0;
$iiiiCnt = 0;
$iiiiiCnt = 0;
$type = 'news';
$js_node_parent = $json_obj->SiteMap->English->Node;
_node_extract($js_node_parent, NULL, $new_array, 'Home', $js_node_parent->{'@attributes'}->nodeId, $js_node_parent->{'@attributes'}->landingPageUrl);
foreach($json_obj->SiteMap->English->Node->Node as $js_node) {
  $type = 'Main';
  if ($oCnt > 0) {
    $type = 'Help';
  }
  _node_extract($js_node, NULL, $new_array, $type, $js_node_parent->{'@attributes'}->nodeId, $js_node_parent->{'@attributes'}->landingPageUrl);
  if (isset($js_node->Node)) {
    foreach ($js_node->Node as $js_o_inner_node) {
      if ($oCnt > 0) {
        _node_extract($js_o_inner_node, NULL, $new_array, $type, $js_node->{'@attributes'}->nodeId, $js_node->{'@attributes'}->landingPageUrl);
        if (isset($js_o_inner_node->Node)) {
          foreach ($js_o_inner_node->Node as $js_o_i_i_node) {
            _node_extract($js_o_i_i_node, NULL, $new_array, $type, $js_o_inner_node->{'@attributes'}->nodeId, $js_o_inner_node->{'@attributes'}->landingPageUrl);
            $oiCnt++;
          }
        }
        $oiCnt++;
      }
    }
  }
  $oCnt++;
}
$type = 'news';
foreach($json_obj->SiteMap->English->Node->Node[0]->Node as $json_node) {
  $json_node_fr = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt];
  $key = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->nodeId;
  if (empty($json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->extUrl)) {
    $new_array[$key]['has_dcr_id'] = true;
  } else {
    // Hack.
    $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->extUrl;
    $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->extUrl;
    $new_array[$key]['has_dcr_id'] = false;
  }
  $new_array[$key]['parent_node_id'] = $json_obj->SiteMap->English->Node->Node[0]->{'@attributes'}->nodeId;
  $new_array[$key]['node_id'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->nodeId;
  $new_array[$key]['dcr_id'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['landingPageUrl']['en'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['landingPageUrl']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['parent_page']['en'] = $json_obj->SiteMap->English->Node->Node[0]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['parent_page']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['title']['en'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->label;
  $new_array[$key]['title']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->label;
  $new_array[$key]['breadcrumb']['en'] = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt]->{'@attributes'}->label;
  $new_array[$key]['breadcrumb']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt]->{'@attributes'}->label;
  $new_array[$key]['temptype'] = $type;
  foreach ($json_node->Node as $json_inner_node) {
    $json_inner_node_fr = null;
    foreach ($json_node_fr->Node as $json_i_node_fr) {
      if ($json_inner_node->{'@attributes'}->nodeId == $json_i_node_fr->{'@attributes'}->nodeId) {
        $json_inner_node_fr = $json_i_node_fr;
      }
    }
    if ($cnt == 0) {
      $type = 'news';
    }
    if ($cnt == 1) {
      $type = 'find_people';
    }
    if ($cnt == 2) {
      $type = 'find_people';
    }
    if ($cnt == 3) {
      $type = 'branches';
    }
    if ($cnt == 3) {
      $type = 'hr';
    }
    if ($cnt == 4) {
      $type = 'our_dept';
    }
    $key = $json_inner_node->{'@attributes'}->nodeId;
    if (empty($json_inner_node->{'@attributes'}->extUrl)) {
      $new_array[$key]['has_dcr_id'] = true;
    } else {
      $json_inner_node->{'@attributes'}->landingPageUrl = $json_inner_node->{'@attributes'}->extUrl;
      $json_inner_node_fr->{'@attributes'}->landingPageUrl = $json_inner_node_fr->{'@attributes'}->extUrl;
      $new_array[$key]['has_dcr_id'] = false;
    }
    $new_array[$key]['node_id'] = $json_inner_node->{'@attributes'}->nodeId;
    $new_array[$key]['parent_node_id'] = $json_node->{'@attributes'}->nodeId;
    $new_array[$key]['dcr_id'] = $json_inner_node->{'@attributes'}->landingPageUrl;
    $new_array[$key]['landingPageUrl']['en'] = $json_inner_node->{'@attributes'}->landingPageUrl;
    $new_array[$key]['landingPageUrl']['fr'] = $json_inner_node_fr->{'@attributes'}->landingPageUrl;
    $new_array[$key]['title']['en'] = $json_inner_node->{'@attributes'}->label;
    $new_array[$key]['title']['fr'] = $json_inner_node_fr->{'@attributes'}->label;
    $new_array[$key]['breadcrumb']['en'] = $json_inner_node->{'@attributes'}->label;
    $new_array[$key]['breadcrumb']['fr'] = $json_inner_node_fr->{'@attributes'}->label;
    $new_array[$key]['type'] = $type;
    if (isset($json_inner_node->Node)) {
      $parent_node_id = NULL;
      $parent_dcr_id = NULL;
      if (is_array($json_inner_node->Node)) {
        $parent_node_id = $json_inner_node->{'@attributes'}->nodeId;
        $parent_dcr_id = $json_inner_node->{'@attributes'}->landingPageUrl;
      }
      else {
        $parent_node_id = $json_inner_node->{'@attributes'}->nodeId;
        $parent_dcr_id = $json_inner_node->{'@attributes'}->landingPageUrl;
      }
      if (empty($parent_dcr_id) && !empty($json_inner_node->{'@attributes'}->nodeId)) {
        $parent_node_id = $json_inner_node->{'@attributes'}->nodeId;
        $parent_dcr_id = $json_inner_node->{'@attributes'}->landingPageUrl;
      }
      foreach ($json_inner_node->Node as $json_inner_inner_en) {
        _node_extract($json_inner_inner_en, NULL, $new_array, $type, $parent_node_id, $parent_dcr_id);
        if (isset($json_inner_inner_en->Node)) {
          $parent_node_id = NULL;
          $parent_dcr_id = NULL;
          if (is_array($json_inner_inner_en->Node)) {
            $parent_node_id = $json_inner_inner_en->{'@attributes'}->nodeId;
            $parent_dcr_id = $json_inner_inner_en->{'@attributes'}->landingPageUrl;
          }
          else {
            $parent_node_id = $json_inner_inner_en->{'@attributes'}->nodeId;
            $parent_dcr_id = $json_inner_inner_en->{'@attributes'}->landingPageUrl;
          }
          if (empty($parent_dcr_id) && !empty($json_inner_node->{'@attributes'}->nodeId)) {
            $parent_node_id = $json_inner_node->{'@attributes'}->nodeId;
            $parent_dcr_id = $json_inner_node->{'@attributes'}->landingPageUrl;
          }
          foreach ($json_inner_inner_en->Node as $json_i_i_inner_en) {
           global $debug_mode;
           $debug_mode = FALSE;
            _node_extract($json_i_i_inner_en, NULL, $new_array, $type, $parent_node_id, $parent_dcr_id);
            echo "iCnt = $iCnt iiCnt = $iiCnt , iiiCnt = $iiiCnt , iiiiCnt = $iiiiCnt , iiiiiCnt = $iiiiiCnt" . "\n";
            echo "************************ " . $json_i_i_inner_en->{'@attributes'}->label  . ' dcr_id = ' . $json_i_i_inner_en->{'@attributes'}->landingPageUrl . "\n";
            if (isset($json_i_i_inner_en->Node)) {
              echo "iCnt = $iCnt iiCnt = $iiCnt , iiiCnt = $iiiCnt , iiiiCnt = $iiiiCnt , iiiiiCnt = $iiiiiCnt" . "\n";
              $parent_node_id = NULL;
              $parent_dcr_id = NULL;
              if (is_array($json_i_i_inner_en->Node)) {
                $parent_node_id = $json_i_i_inner_en->{'@attributes'}->nodeId;
                $parent_dcr_id = $json_i_i_inner_en->{'@attributes'}->landingPageUrl;
              }
              else {
                $parent_node_id = $json_i_i_inner_en->{'@attributes'}->nodeId;
                $parent_dcr_id = $json_i_i_inner_en->{'@attributes'}->landingPageUrl;
              }
              if (empty($parent_dcr_id) && !empty($json_inner_inner_en->{'@attributes'}->nodeId)) {
                $parent_node_id = $json_inner_inner_en->{'@attributes'}->nodeId;
                $parent_dcr_id = $json_inner_inner_en->{'@attributes'}->landingPageUrl;
              }
              echo "************************ " . $json_i_i_inner_en->Node->{'@attributes'}->label  . ' dcr_id = ' . $json_i_i_inner_en->Node->{'@attributes'}->landingPageUrl . "\n";
              foreach ($json_i_i_inner_en->Node as $json_i_i_i_inner_en) {
                $debug_mode = FALSE;
                _node_extract($json_i_i_i_inner_en, NULL, $new_array, $type, $parent_node_id, $parent_dcr_id);
                if (isset($json_i_i_i_inner_en->Node)) {
                  echo "iCnt = $iCnt iiCnt = $iiCnt , iiiCnt = $iiiCnt , iiiiCnt = $iiiiCnt , iiiiiCnt = $iiiiiCnt" . "\n";
                  echo "************************ " . $json_i_i_i_inner_en->Node->{'@attributes'}->label  . ' dcr_id = ' . $json_i_i_i_inner_en->Node->{'@attributes'}->landingPageUrl . "\n";
                  $parent_node_id = NULL;
                  $parent_dcr_id = NULL;
                  if (is_array($json_i_i_i_inner_en->Node)) {
                    $parent_node_id = $json_i_i_i_inner_en->{'@attributes'}->nodeId;
                    $parent_dcr_id = $json_i_i_i_inner_en->{'@attributes'}->landingPageUrl;
                  }
                  else {
                    $parent_node_id = $json_i_i_i_inner_en->{'@attributes'}->nodeId;
                    $parent_dcr_id = $json_i_i_i_inner_en->{'@attributes'}->landingPageUrl;
                  }
                  if (empty($parent_dcr_id) && !empty($json_i_i_inner_en->{'@attributes'}->nodeId)) {
                    $parent_node_id = $json_i_i_inner_en->{'@attributes'}->nodeId;
                    $parent_dcr_id = $json_i_i_inner_en->{'@attributes'}->landingPageUrl;
                  }
                  foreach ($json_i_i_i_inner_en->Node as $json_i_i_i_i_inner_en) {
                    $parent_node_id = NULL;
                    $parent_dcr_id = NULL;
                    if (is_array($json_i_i_i_i_inner_en->Node)) {
                      $parent_node_id = $json_i_i_i_i_inner_en->{'@attributes'}->nodeId;
                      $parent_dcr_id = $json_i_i_i_i_inner_en->{'@attributes'}->landingPageUrl;
                    }
                    else {
                      $parent_node_id = $json_i_i_i_i_inner_en->{'@attributes'}->nodeId;
                      $parent_dcr_id = $json_i_i_i_i_inner_en->{'@attributes'}->landingPageUrl;
                    }
                    if (empty($parent_dcr_id) && !empty($json_i_i_i_inner_en->{'@attributes'}->nodeId)) {
                      $parent_node_id = $json_i_i_i_inner_en->{'@attributes'}->nodeId;
                      $parent_dcr_id = $json_i_i_i_inner_en->{'@attributes'}->landingPageUrl;
                    }
                    _node_extract($json_i_i_i_i_inner_en, NULL, $new_array, $type, $parent_node_id, $parent_dcr_id);
                    $iiiiiCnt++;
                    if ($debug_mode)
                      echo "iiiiiCnt $iiiiiCnt type $type parent=" . $json_i_i_i_inner_en->{'@attributes'}->nodeId . "\n";
                  }
                }
                $iiiiCnt++;
                if ($debug_mode)
                  echo "iiiiCnt $iiiiCnt type $type parent=" . $json_i_i_inner_en->{'@attributes'}->nodeId . "\n";
              }
              $debug_mode = FALSE;
            }
            $iiiCnt++;
            if ($debug_mode)
              echo "iiiCnt $iiiCnt type $type parent=" . $json_inner_inner_en->{'@attributes'}->nodeId . "\n";
          }
        }
        $iiCnt++;
      }
    }
    $iCnt++;
  }
  $cnt++;
}
echo "oCnt = $oCnt\n";
echo "oiCnt = $oiCnt\n";
echo "iCnt = $iCnt\n";
echo "iiCnt = $iiCnt\n";
echo "iiiCnt = $iiiCnt\n";
echo "iiiiCnt = $iiiiCnt\n";
echo "iiiiiCnt = $iiiiiCnt\n";


// Now French - Maintenant, français!

$cnt_fr=0;
$oCnt_fr=0;
$oiCnt_fr=0;
$iCnt_fr=0;
$iiCnt_fr = 0;
$iiiCnt_fr = 0;
$iiiiCnt_fr = 0;
$iiiiiCnt_fr = 0;

$js_node_parent = $json_obj->SiteMap->French->Node;
_node_extract_french($js_node_parent,$new_array, 'Home');
foreach($json_obj->SiteMap->French->Node->Node as $js_node) {
  $type = 'Main';
  if ($oCnt_fr > 0) {
    $type = 'Help';
  }
  _node_extract_french($js_node, $new_array, $type);
  if (isset($json_node->Node)) {
    foreach ($json_node->Node as $js_o_inner_node) {
      if ($oCnt_fr > 0) {
        _node_extract_french($js_o_inner_node, $new_array, $type);
        if (isset($js_o_inner_node->Node)) {
          foreach ($js_o_inner_node->Node as $js_o_i_i_node) {
            _node_extract_french($js_o_i_i_node, $new_array, $type);
            $oiCnt_fr++;
          }
        }
        $oiCnt_fr++;
      }
    }
  }
  $oCnt_fr++;
}
$type = 'news';
foreach($json_obj->SiteMap->French->Node->Node[0]->Node as $json_node) {
  $json_node_fr = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt_fr];
  $key = $json_obj->SiteMap->English->Node->Node[0]->Node[$cnt_fr]->{'@attributes'}->nodeId;
  $new_array[$key]['landingPageUrl']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt_fr]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['parent_page']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->{'@attributes'}->landingPageUrl;
  $new_array[$key]['title']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt_fr]->{'@attributes'}->label;
  $new_array[$key]['breadcrumb']['fr'] = $json_obj->SiteMap->French->Node->Node[0]->Node[$cnt_fr]->{'@attributes'}->label;
  //$new_array[$key]['temptype'] = $type;
  foreach ($json_node->Node as $json_inner_node) {
    if ($cnt_fr == 0) {
      $type = 'news';
    }
    if ($cnt_fr == 1) {
      $type = 'find_people';
    }
    if ($cnt_fr == 2) {
      $type = 'find_people';
    }
    if ($cnt_fr == 3) {
      $type = 'branches';
    }
    if ($cnt_fr == 3) {
      $type = 'hr';
    }
    if ($cnt_fr == 4) {
      $type = 'our_dept';
    }
    $key = $json_inner_node->{'@attributes'}->nodeId;
    $new_array[$key]['landingPageUrl']['fr'] = $json_inner_node->{'@attributes'}->landingPageUrl;
    $new_array[$key]['title']['fr'] = $json_inner_node->{'@attributes'}->label;
    $new_array[$key]['breadcrumb']['fr'] = $json_inner_node->{'@attributes'}->label;
    if (isset($json_inner_node->Node)) {
      foreach ($json_inner_node->Node as $json_inner_inner_fr) {
        _node_extract_french($json_inner_inner_fr, $new_array, $type);
        if (isset($json_inner_inner_fr->Node)) {
          foreach ($json_inner_inner_fr->Node as $json_i_i_inner_fr) {
            _node_extract_french($json_i_i_inner_fr, $new_array, $type);
             if (isset($json_i_i_inner_fr->Node)) {
               foreach ($json_i_i_inner_fr->Node as $json_i_i_i_inner_fr) {
                 _node_extract_french($json_i_i_i_inner_fr, $new_array, $type);
                 if (isset($json_i_i_i_inner_fr->Node)) {
                   foreach ($json_i_i_i_inner_fr->Node as $json_i_i_i_i_inner_fr) {
                     _node_extract_french($json_i_i_i_i_inner_fr, $new_array, $type);
                     $iiiiiCnt_fr++;
                   }
                 }
                 $iiiiCnt_fr++;
               }
             }
            $iiiCnt_fr++;
          }
        }
        $iiCnt_fr++;
      }
    }
    $iCnt_fr++;
  }
  $cnt_fr++;
}
echo "oCnt_fr = $oCnt_fr\n";
echo "oiCnt_fr = $oiCnt_fr\n";
echo "iCnt_fr = $iCnt_fr\n";
echo "iiCnt_fr = $iiCnt_fr\n";
echo "iiiCnt_fr = $iiiCnt_fr\n";
echo "iiiiCnt_fr = $iiiiCnt_fr\n";
echo "iiiiiCnt_fr = $iiiiiCnt_fr\n";
echo "\n";
echo "\n";
echo "Total En + Fr = " . ($oCnt + $oiCnt + $iCnt + $iiCnt + $iiiCnt + $iiiiCnt + $iiiiiCnt + $oCnt_fr + $oiCnt_fr + $iCnt_fr + $iiCnt_fr + $iiiCnt_fr + $iiiiCnt_fr + $iiiiiCnt_fr) . "\n";

function _node_extract_french($json_fr, &$data, $type) {
  global $debug_mode;
  $cnt=0;
  $iCnt=0;
  $topLevel = null;
  if (isset($json_fr->{'@attributes'}->nodeId)) {
    $top_json = $json_fr->{'@attributes'};
  }
  else if (isset($json_fr->nodeId)) {
    $top_json = $json_fr;
  }
  else {
    foreach ($json_fr as $json) {
      if ($debug_mode) {
        echo print_r($json, TRUE) . __FUNCTION__ . " JOSEPH TEST?????? type= $type \n";
      }
      if (isset($json->{'@attributes'}->nodeId)) {
        $json = $json->{'@attributes'};
      }
      if (isset($data[$json->nodeId])) {
        $key = $json->nodeId;
        if (isset($data[$key])) {
          //$data[$key]['node_id'] = $top_json_en->nodeId;
          //$data[$key]['parent_node_id'] = $parent_node_id;
          //$data[$key]['dcr_id'] = $top_json_en->landingPageUrl;
          //$data[$key]['landingPageUrl']['en'] = $top_json_en->landingPageUrl;
	  if (!empty($json->extUrl)) {
            $json->landingPageUrl = $json->extUrl;
	  }
          $data[$key]['landingPageUrl']['fr'] = $json->landingPageUrl;
          //$data[$key]['parent_dcr_id'] = $parent_dcr_id;
          //$data[$key]['node_title']['en'] = $top_json_en->label;
          $data[$key]['breadcrumb']['fr'] = $json->label;
          $data[$key]['node_title']['fr'] = $json->label;
          //$data[$key]['title']['en'] = $top_json_en->label;
          $data[$key]['title']['fr'] = $json->label;
          //$data[$key]['type'] = $type;
        }
      }
    }
    if ($debug_mode)
      echo print_r($json_fr, TRUE) . __FUNCTION__ . " DEBUG?????? type= $type \n";
    }
  if (isset($top_json)) {
    if (isset($data[$top_json->nodeId])) {
      $key = $top_json->nodeId;
      if (isset($data[$key])) {
        //$data[$key]['node_id'] = $top_json_en->nodeId;
        //$data[$key]['parent_node_id'] = $parent_node_id;
        //$data[$key]['dcr_id'] = $top_json_en->landingPageUrl;
        //$data[$key]['landingPageUrl']['en'] = $top_json_en->landingPageUrl;
        if (!empty($top_json->extUrl)) {
          $top_json->landingPageUrl = $top_json->extUrl;
        }
        $data[$key]['landingPageUrl']['fr'] = $top_json->landingPageUrl;
        //$data[$key]['parent_dcr_id'] = $parent_dcr_id;
        //$data[$key]['node_title']['en'] = $top_json_en->label;
        $data[$key]['breadcrumb']['fr'] = $top_json->label;
        $data[$key]['node_title']['fr'] = $top_json->label;
        //$data[$key]['title']['en'] = $top_json_en->label;
        $data[$key]['title']['fr'] = $top_json->label;
        //$data[$key]['type'] = $type;
      }
    }
  }
  else if ($debug_mode) {
    echo "WTF??????????????????????????????? type=$type \n";
  }
}

function _node_extract($json_en, $json_fr, &$data, $type, $parent_node_id = null, $parent_dcr_id = null) {
  global $debug_mode;
  $debug_mode = FALSE;
  $cnt=0;
  $iCnt=0;
  $topLevel = null;
  if (isset($json_en->{'@attributes'}->nodeId)) {
    $top_json_en = $json_en->{'@attributes'};
  }
  else if (isset($json_en->nodeId)) {
    $top_json_en = $json_en;
  }
  else {
    if ($debug_mode)
      echo print_r($json_en, TRUE) . " DEBUG?????? parent_node_id = " . $parent_node_id . " type= $type \n";
    foreach ($json_en as $top_json) {
      _node_extract_this($top_json, $json_fr, $data, $type, $parent_node_id, $parent_dcr_id);
    }
  }
  if (isset($top_json_en)) {
    _node_extract_this($top_json_en, $json_fr, $data, $type, $parent_node_id, $parent_dcr_id);
  }
  else if ($debug_mode) {
    echo "WTF??????????????????????????????? type=$type \n";
  }
}

function _node_extract_this($top_json, $json_fr, &$data, $type, $parent_node_id = null, $parent_dcr_id = null) {
  if (isset($top_json->{'@attributes'}->nodeId)) {
    $top_json = $top_json->{'@attributes'};
  }
  if (!isset($data[$top_json->nodeId])) {
    $key = $top_json->nodeId;
    if (empty($top_json->extUrl)) {
      // Not all nodes on the sitemap have a dcr_id (aka landingPageUrl).
      $data[$key]['has_dcr_id'] = true;
    } else {
      $top_json->landingPage = $top_json->extUrl;
      $data[$key]['has_dcr_id'] = false;
    }
//    if ($parent_node_id == '1492538502840'/*Departmental Security Ser*/ && $top_json->nodeId != '1485266766613' && $top_json->nodeId == '1320677087463') {
//      $parent_node_id = '1320677087463';
//      $parent_dcr_id = '1320429524077';
//    }
//    if ($parent_node_id == '1313003659389' && $top_json->nodeId != '1492538182182') {
//      if ($parent_node_id == '1313003659389' && $top_json->nodeId != '1493063855649') {
//        $parent_node_id = '1320677087463'; /*Services*/
//        $parent_dcr_id = '1320429524077';
//      }
//    }
//    if ($top_json->nodeId == '1522247397123' /* News@work */) {
//      $parent_node_id = '1522242982779'; /* News */
//      $parent_dcr_id = '1522092668036';
//    }
//    if ($parent_node_id == '' /*  */ && $top_json->nodeId == ''/**/) {
//      $parent_node_id = ''; /* */;
//      $parent_dcr_id = '';
//    }
    $data[$key]['node_id'] = $top_json->nodeId;
    $data[$key]['parent_node_id'] = $parent_node_id;
    $data[$key]['dcr_id'] = $top_json->landingPageUrl;
    $data[$key]['landingPageUrl']['en'] = $top_json->landingPageUrl;
    $data[$key]['landingPageUrl']['fr'] = $top_json->landingPageUrl;
    $data[$key]['parent_dcr_id'] = $parent_dcr_id;
    $data[$key]['breadcrumb']['en'] = $top_json->label;
    $data[$key]['breadcrumb']['fr'] = $top_json->label;
    $data[$key]['node_title']['en'] = $top_json->label;
    $data[$key]['node_title']['fr'] = $top_json->label;
    $data[$key]['title']['en'] = $top_json->label;
    $data[$key]['title']['fr'] = $top_json->label;
    $data[$key]['type'] = $type;
  }
}

$month_day = date('m-d', time());
$dir = 'export_sitemap_' . $month_day;
if (!is_dir($dir)) {
  mkdir($dir);
}
chdir($dir);
$keep_existing = false;
$jsonCnt = 0;
unset($new_array[1522247502779]); // Get rid of second Deputy Ministers Messages, the other one is 1292442470473 , keep only one.
foreach($new_array as $key => $entity) {
  if ($keep_existing) {
    if (file_exists($key.'.json') || $key == 1522247502779) {
      continue;
    }
  }
  if ($fp = fopen($key.'.json', 'w')) {
    //echo "Write $key.json\n";
    $jsonCnt++;
    fwrite($fp, json_encode($entity, JSON_PRETTY_PRINT));
    fclose($fp);
  }
}
echo "Wrote $jsonCnt json files to $dir\n";
chdir('..');

//$test = json_encode($new_array, JSON_PRETTY_PRINT);
//echo $test;

//echo $test;

