{
    "dcr_id": "1532127211185",
    "title": {
        "en": "Regulation of cannabis seed",
        "fr": "R\u00e9glementation des semences de cannabis"
    },
    "html_modified": "2024-03-12 3:58:07 PM",
    "modified": "2022-01-27",
    "issued": "2018-07-23",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/reg_cannabis_seed_1532127211185_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/reg_cannabis_seed_1532127211185_fra"
    },
    "ia_id": "1532127233686",
    "parent_ia_id": "1531251514243",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1531251514243",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Regulation of cannabis seed",
        "fr": "R\u00e9glementation des semences de cannabis"
    },
    "label": {
        "en": "Regulation of cannabis seed",
        "fr": "R\u00e9glementation des semences de cannabis"
    },
    "templatetype": "content page 1 column",
    "node_id": "1532127233686",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1531250234189",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/cannabis/cannabis-seed/",
        "fr": "/protection-des-vegetaux/le-cannabis/semences-de-cannabis/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Regulation of cannabis seed",
            "fr": "R\u00e9glementation des semences de cannabis"
        },
        "description": {
            "en": "As of October 17, 2018, the cultivation of cannabis seed is permitted in accordance with the requirements of the Cannabis Act and Regulations.",
            "fr": "\u00c0 partir du 17 octobre 2018, la culture de semences de cannabis est autoris\u00e9e en conformit\u00e9 avec les exigences de la Loi sur le cannabis et le R\u00e8glement."
        },
        "keywords": {
            "en": "cannabis, Regulation of cannabis seed, CFIA's role, Cannabis with novel traits, government and industry information",
            "fr": "cannabis, R\u00e9glementation des semences de cannabis, Le r\u00f4le de l'ACIA, Le r\u00f4le de l'ACIA, Renseignements suppl\u00e9mentaires en provenance du gouvernement et de l'industrie"
        },
        "dcterms.subject": {
            "en": "inspection,legislation,regulation",
            "fr": "inspection,l\u00e9gislation,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-07-23",
            "fr": "2018-07-23"
        },
        "modified": {
            "en": "2022-01-27",
            "fr": "2022-01-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Regulation of cannabis seed",
        "fr": "R\u00e9glementation des semences de cannabis"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>As of October 17, 2018, the cultivation of cannabis seed is permitted in accordance with the requirements of the <a href=\"http://www.justice.gc.ca/eng/cj-jp/cannabis/\"><i>Cannabis Act</i> and Regulations</a>.</p>\n<h2>CFIA's role in the regulation of cannabis seed</h2>\n<p>The CFIA regulates the import, export, certification and grading of cannabis seed under the following acts and regulations:</p>\n<ul>\n<li><a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?seesemr\"><i>Seeds Regulations</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?plavega\"><i>Plant Protection Act</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?plavegr\"><i>Plant Protection Regulations</i></a></li>\n</ul>\n<p>The CFIA's <i>Seeds Act</i> and <i>Regulations</i> and <i>Plant Protection Act</i> and <i>Regulations</i> regulate cannabis seed in the following ways:</p>\n<ul>\n<li>delivery of the pedigreed seed crop inspection system</li>\n<li>labelling requirements of the <i>Seeds Act</i> and Regulations will apply to seed of cannabis</li>\n<li>import:\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/grains-and-field-crops/d-96-03/eng/1312380810386/1312381018330\">D-96-03 Plant Protection Import Requirements for Cannabis</a> sativa outlines the <a href=\"http://airs-sari.inspection.gc.ca/airs_external/english/decisions-eng.aspx\">import requirements</a> for plants and seeds</li>\n<li>The <i>Seeds Act</i> and <i>Regulations</i> will regulate the import of cannabis seed. <a href=\"/plant-health/seeds/seed-imports/abcs-of-seed-importation/eng/1347740952226/1347741389113\">The ABCs of Seed Importation into Canada</a> outline these requirements</li>\n</ul></li>\n</ul>\n<h2>Cannabis with novel traits</h2>\n<p>Cannabis could also be subject to assessment under the <i>Seeds Act</i> and <i>Seeds Regulations</i> as a plant with novel traits, if a <a href=\"/plant-varieties/plants-with-novel-traits/applicants/directive-2009-09/eng/1304466419931/1304466812439\">novel trait</a> was introduced in to the crop.</p>\n<p>For further information contact the <a href=\"/plant-health/cannabis/regulation-of-cannabis/eng/1551112125696/1551112267912\" title=\"Regulation of cannabis with novel traits\">Plant Biosafety Office</a>.</p>\n<h2>Additional government and industry information</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/en/services/health/campaigns/cannabis.html?utm_campaign=cannabis-18&amp;utm_medium=vurl-en&amp;utm_source=canada-ca_cannabis\">Health Canada \u2013 Cannabis in Canada</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>\u00c0 partir du 17\u00a0octobre 2018, la culture de semences de cannabis est autoris\u00e9e en conformit\u00e9 avec les exigences de la <a href=\"http://www.justice.gc.ca/fra/jp-cj/cannabis/\"><i>Loi sur le cannabis</i> et le <i>R\u00e8glement</i></a>.</p>\n<h2>Le r\u00f4le de l'ACIA dans la r\u00e9glementation des semences de cannabis</h2>\n<p>L'ACIA r\u00e9glemente l'importation, l'exportation, la certification et le classement des semences de cannabis en vertu des lois et des r\u00e8glements suivants\u00a0:</p><ul>\n<li><a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?seesemr\"><i>R\u00e8glement sur les semences</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?plavega\"><i>Loi sur la protection des v\u00e9g\u00e9taux</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?plavegr\"><i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i></a></li>\n</ul>\n<p>La <i>Loi sur les semences</i> et le <i>R\u00e8glement</i> de l'ACIA et la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> et le <i>r\u00e8glement</i> connexe r\u00e9glementent les semences de cannabis par les moyens suivants\u00a0:</p>\n<ul>\n<li>La mise en \u0153uvre du syst\u00e8me d'inspection des cultures de semences g\u00e9n\u00e9alogiques</li>\n<li>Les exigences en mati\u00e8re d'\u00e9tiquetage de la <i>Loi sur les semences</i> et du <i>R\u00e8glement</i> s'appliquent aux semences de cannabis</li>\n<li>L'importation\u00a0:\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/grains-et-grandes-cultures/d-96-03/fra/1312380810386/1312381018330\">D-96-03 Exigences phytosanitaires\u00a0: importation de Cannabis sativa</a> d\u00e9crit les <a href=\"http://airs-sari.inspection.gc.ca/airs_external/francais/decisions-fra.aspx\">exigences d'importation</a> pour les plants et les semences</li>\n<li>La <i>Loi sur les semences</i> et le <i>r\u00e8glement</i> connexe encadreront l'importation des semences de cannabis. <a href=\"/protection-des-vegetaux/semences/importation-de-semences/tout-sur-l-importation/fra/1347740952226/1347741389113\">Tout sur l'importation de semences au Canada</a> d\u00e9crit ces exigences</li>\n</ul></li>\n</ul>\n<h2>Cannabis avec des caract\u00e8res nouveaux</h2>\n<p>Le cannabis pourrait \u00eatre assujetti \u00e0 une \u00e9valuation en vertu de la <i>Loi sur les semences</i> et du <i>R\u00e8glement</i> sur les semences en tant que plante avec des caract\u00e8res nouveaux, si des <a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/directive-2009-09/fra/1304466419931/1304466812439\">caract\u00e8res nouveaux</a> sont diss\u00e9min\u00e9s dans la culture.</p>\n<p>Pour de plus amples renseignements, veuillez communiquer avec le <a href=\"/protection-des-vegetaux/le-cannabis/reglementation-du-cannabis/fra/1551112125696/1551112267912\" title=\"R\u00e9glementation du cannabis \u00e0 caract\u00e8res nouveaux\">Bureau de la bios\u00e9curit\u00e9 v\u00e9g\u00e9tale</a>.</p>\n<h2>Renseignements suppl\u00e9mentaires en provenance du gouvernement et de l'industrie</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/services/sante/campagnes/cannabis.html\">Sant\u00e9 Canada \u2013 Le cannabis au Canada</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}