{
    "dcr_id": "1395348583779",
    "title": {
        "en": "Centre of Administration for Permissions",
        "fr": "Centre d'administration pour les permissions"
    },
    "html_modified": "2024-02-13 9:49:45 AM",
    "modified": "2023-11-27",
    "issued": "2015-03-19 14:10:16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_administration_coa_1395348583779_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_administration_coa_1395348583779_fra"
    },
    "ia_id": "1395348638922",
    "parent_ia_id": "1395348237219",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1395348237219",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Centre of Administration for Permissions",
        "fr": "Centre d'administration pour les permissions"
    },
    "label": {
        "en": "Centre of Administration for Permissions",
        "fr": "Centre d'administration pour les permissions"
    },
    "templatetype": "content page 1 column",
    "node_id": "1395348638922",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1395348112901",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/permits-licences-and-approvals/centre-of-administration-for-permissions/",
        "fr": "/a-propos-de-l-acia/permis-licences-et-approbations/centre-d-administration-pour-les-permissions/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Centre of Administration for Permissions",
            "fr": "Centre d'administration pour les permissions"
        },
        "description": {
            "en": "Permissions issued by the Canadian Food Inspection Agency (CFIA) help to keep Canada's domestic food supply safe, protect the environment from invasive animal and plant diseases and plant pests, and ensure compliance with various regulatory requirements, enabling market access and trade.",
            "fr": "Les permissions \u00e9mises par l'Agence canadienne d'inspection des aliments (ACIA) contribuent \u00e0 prot\u00e9ger l'approvisionnement alimentaire au Canada, \u00e0 prot\u00e9ger le milieu naturel contre les maladies des animaux et des plantes et des phytoravageurs envahissants et \u00e0 assurer la conformit\u00e9 aux diverses exigences r\u00e9glementaires, qui elles, assurent un acc\u00e8s au march\u00e9 et rendent possible le commerce."
        },
        "keywords": {
            "en": "The Centre of Administration (CoA) , administrative services, import, permissions, licences, permits, registration",
            "fr": "Le Centre d'administration (CoA), services administratifs, importation, autorisations, licences, permis, enregistrement"
        },
        "dcterms.subject": {
            "en": "food,government information,inspection,plants,animal health",
            "fr": "aliment,information gouvernementale,inspection,plante,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Op\u00e9rations"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-19 14:10:16",
            "fr": "2015-03-19 14:10:16"
        },
        "modified": {
            "en": "2023-11-27",
            "fr": "2023-11-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Centre of Administration for Permissions",
        "fr": "Centre d'administration pour les permissions"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info mrgn-tp-lg\">\n<p><strong>Notice:</strong> Applying for an animal or plant import permit, or a Ministerial Exemption for fresh fruit and vegetables? As of July 4, 2022, the CFIA will be charging the fee for the delivery of its services, which must be paid when you apply.</p>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-8 mrgn-tp-0\">\n<p>Permissions issued by the Canadian Food Inspection Agency (CFIA) help to keep Canada's domestic food supply safe, protect the environment from invasive animal and plant diseases and plant pests, and ensure compliance with various regulatory requirements, enabling market access and trade.</p>\n\n<p>The Centre of Administration (CoA) receives and processes applications for domestic and import related permissions, which include licences, permits, ministerial exemptions, permissions to move, and test market authorizations.</p>\n\n<h2>Manage your application online</h2>\n\n<p>Most permissions are available online through My\u00a0CFIA, a convenient and secure way to do business with the CFIA. Sign up for a <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\">My\u00a0CFIA</a> account to apply and check the status of your application, and manage and track service requests.</p>\n\n<p>Should you have questions or need guidance in completing an application or renewal, <a href=\"#contact\">contact us</a>.</p>\n\n<h2 id=\"proc\">How long will it take to process my application</h2>\n\n<p>We are committed to achieving the stated processing times a minimum of <strong>90% of the time</strong> under normal operational circumstances. </p>\n<p>The processing time impacting the remaining 10% will vary based on:</p>\n<ul>\n<li>the type of application submitted (level of risk or complexity) </li>\n<li>how long you take to respond to any requests or concerns </li>\n<li>the volume of applications received </li>\n<li>technical reviews or inspections</li>\n<li>changes in policy and how we operate </li>\n<li>factors beyond our control (for example, natural disasters, conflicts in certain regions, animal diseases outbreaks, plant health pest restrictions </li>\n</ul>\n<p>Your processing time starts the day we receive your <strong>complete application</strong> and ends when we have issued a decision.</p>\n\n<p>If you applied through My CFIA and it's been longer than the time shown below, please check your application status in your My CFIA account. If you don't see an update, please call us at 1-800-442-2342.</p>\n\n<div class=\"row\"> \n<div class=\"col-sm-6\"><p class=\"mrgn-tp-sm\">Simple applications:</p></div>\n<div class=\"col-sm-6\"><p class=\"mrgn-tp-sm\"><strong>10</strong> business days</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\"><p class=\"mrgn-tp-sm\">Complex applications<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup> (require specialist review):</p></div>\n<div class=\"col-sm-6\"><p class=\"mrgn-tp-sm\">May exceed <strong>10</strong> business days</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\"><p class=\"mrgn-tp-sm\">Ministerial Exemptions for fresh<br>fruits and vegetables:</p></div>\n<div class=\"col-sm-6\"><p class=\"mrgn-tp-sm\"><strong>2</strong> business days</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\">Ministerial Exemptions and Permissions to Move for processed products:</p></div>\n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\"><strong>5</strong> business days</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\">Test Market Authorizations</p></div>\n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\"><strong>6</strong> to <strong>12</strong> weeks</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\">Safe Food for Canadians licence applications: </p></div>\n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\"><strong>48</strong> hours</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\">Safe Food for Canadians Licence applications that require pre-issuance verification<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>: </p></div>\n<div class=\"col-sm-6\">\n<p class=\"mrgn-tp-sm\"><strong>70</strong> business days</p></div>\n</div>\n</div>\n\n<div class=\"col-sm-4 mrgn-tp-0\">\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">What's new</h3>\n</header>\n<div class=\"panel-body\">\n<p>Learn about the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\"><i>Safe Food for Canadians Regulations</i> (SFCR)</a> and find out if your business needs a licence and when to apply.</p>\n<p>Submit and track the status of your applications online by signing up for <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\" class=\"nowrap\">My\u00a0CFIA</a>.</p>\n\n<div class=\"mrgn-bttm-sm mrgn-tp-md\"> \n<a href=\"https://services.inspection.gc.ca/setLangEn?url=https%3A%2F%2Fservices.inspection.gc.ca%2Fmycfia-monacia%2F\" title=\"Sign in or sign up for My\u00a0CFIA\" class=\"btn btn-primary btn-block mrgn-bttm-sm\" role=\"button\">Sign in / Sign up \n<img alt=\"My\u00a0CFIA\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/mycfia-monacia_1483807779108_eng.jpg\" class=\"img-responsive img-rounded mrgn-bttm-md mrgn-tp-sm\"></a>\n</div>\n</div>\n</section>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Permit requirements</h3>\n</header>\n<div class=\"panel-body\">\n<p><a href=\"https://ca1se.voxco.com/SE/93/SFCR_licence/?&amp;lang=en\"><abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr> Interactive tool: Do you need a food licence?</a></p>\n\n<p>The <a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System (AIRS)</a> shows the import requirements for CFIA regulated commodities</p>\n</div>\n</section>\n\n<p class=\"mrgn-bttm-lg text-center\"><a href=\"/eng/1530134673372/1530134725024\" class=\"btn btn-primary btn-block mrgn-bttm-sm\" role=\"button\">Find information<br>\n</a></p>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\" id=\"contact\">Contact us</h3>\n</header>\n<div class=\"panel-body\">\n<p>The <abbr title=\"Centre of Administration\">CoA</abbr> offers bilingual support services Monday to Friday between 7 <abbr lang=\"la\" title=\"Ante Meridiem\">a.m.</abbr> and 7 <abbr lang=\"la\" title=\"Post Meridiem\">p.m.</abbr> <abbr title=\"Eastern standard time\">EST</abbr>. We are closed for statutory holidays.</p>\n\n<p class=\"mrgn-tp-sm\">Telephone:<br>\n<br>\n</p>\n\n<p class=\"mrgn-tp-sm\">Email:<br>\n<a href=\"mailto:permission@inspection.gc.ca\" class=\"nowrap\">permission@inspection.gc.ca</a></p>\n\n<p class=\"mrgn-tp-sm\">Email and voicemail messages are returned within 3 business days.</p>\n\n<h4>Stay Connected!</h4>\n\n<p>Subscribe to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s <a href=\"https://notification.inspection.canada.ca/\">email notifications</a></p>\n</div>\n</section>\n</div>\n</div>\n\n<h2>Find an application form</h2>\n\n<p>Search by\u2026</p>\n\n<ul class=\"list-unstyled\">\n<li class=\"mrgn-bttm-md\"><a href=\"/about-the-cfia/permits-licences-and-approvals/centre-of-administration-for-permissions/application-forms-food/eng/1529708437469/1529708437719\">Food</a> (ministerial exemptions, permissions to move and test market authorizations)\n<p>Note: Apply for a Safe Food for Canadians licence by using <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\">My CFIA</a></p></li>\n<li class=\"mrgn-bttm-md\"><a href=\"/about-the-cfia/permits-licences-and-approvals/centre-of-administration-for-permissions/application-forms-animal/eng/1529708468073/1529708468291\">Animal Health</a> (animal pathogens, animal products and by-products (including pet food), live animals (aquatic and terrestrial, including amphibians and reptiles), hatching eggs, and animal germplasm)</li>\n<li class=\"mrgn-bttm-md\"><a href=\"/about-the-cfia/permits-licences-and-approvals/centre-of-administration-for-permissions/application-forms-plant/eng/1529708480533/1529708480759\">Plant</a> (plants, plant products, unprocessed/raw grains, oilseeds or plant-based meals, wood products, and wood packaging program registration)</li>\n</ul>\n\n<h3>Online payment</h3>\n\n<p>Sign up for a <a href=\"/about-the-cfia/my-cfia-account/eng/1482204298243/1482204318353\">My\u00a0CFIA</a> account to access secure online payment options.</p>\n\n<p>Businesses that manage high volumes of applications can select monthly invoicing as their online payment option in My\u00a0CFIA by completing an <a href=\"/eng/1328823628115/1328823702784#c0015\">Application for Credit (CFIA/ACIA 0015)</a>.</p>\n\n<h3>Transparency</h3>\n\n<h4><a href=\"/about-the-cfia/permits-licences-and-approvals/centre-of-administration-for-permissions/quarterly-reports-for-permissions-issued/eng/1529703280983/1529703315739\">Quarterly reports for permissions issued</a></h4>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Footnotes</h2>\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p>Complex applications include, but are not limited to, all requests for permits to import animal pathogens, animal products and by-products containing animal pathogens.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p></dd>\n\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p>Pre-issuance verification by CoA Case Management Officers may be triggered, for example, when information is missing from an application or has been entered in error. In some cases, an application may have to be referred to an Area Inspector for a pre-issuance inspection. When any of these scenarios occur, additional processing time will be required.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p></dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info mrgn-tp-lg\">\n<p><strong>Avis\u00a0:</strong> Vous demandez une autorisation d'importation d'animaux ou de v\u00e9g\u00e9taux, ou une exemption minist\u00e9rielle pour les fruits et l\u00e9gumes frais? \u00c0 compter du 4 juillet 2022, l'ACIA exigera des droits pour la prestation de ses services, qui devront \u00eatre pay\u00e9s au moment de la demande.</p>\n</div>\n\n<div class=\"row mrgn-tp-lg\"> \n<div class=\"col-sm-8 mrgn-tp-0\">\n<p>Les permissions \u00e9mises par l'Agence canadienne d'inspection des aliments (ACIA) contribuent \u00e0 prot\u00e9ger l'approvisionnement alimentaire au Canada, \u00e0 prot\u00e9ger le milieu naturel contre les maladies des animaux et des plantes et des phytoravageurs envahissants et \u00e0 assurer la conformit\u00e9 aux diverses exigences r\u00e9glementaires, qui elles, assurent un acc\u00e8s au march\u00e9 et rendent possible le commerce.</p>\n\n<p>Le Centre d'administration (CdA) re\u00e7oit et traite les demandes de permissions au pays et de permissions li\u00e9es aux importations, ce qui comprend des permis, des inscriptions, des exemptions minist\u00e9rielles, des permissions de transport, et des autorisations d'essais de mise en march\u00e9.</p>\n\n<h2>G\u00e9rez votre demande en ligne</h2>\n\n<p>La plupart des permissions sont disponibles en ligne par l'entremise de Mon\u00a0ACIA; un moyen pratique et s\u00e9curitaire de faire des affaires avec l'ACIA. Inscrivez-vous et obtenez un compte <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\">Mon\u00a0ACIA</a> pour pr\u00e9senter vos demandes et en v\u00e9rifier l'\u00e9tat de traitement, et g\u00e9rez et suivez les demandes de service en ligne.</p>\n\n<p>Si vous avez des questions ou avez besoin d'aide pour remplir une demande ou renouveler votre permis/licence, <a href=\"#contact\">communiquez avec nous</a>.</p>\n\n<h2 id=\"proc\">Quel sera le d\u00e9lai de traitement de ma demande</h2>\n\n<p>Nous nous sommes engag\u00e9s \u00e0 la r\u00e9alisation des d\u00e9lais de traitement indiqu\u00e9s au moins <strong>90% du temps</strong> dans des conditions op\u00e9rationnelles normales.</p>\n\n<p>Le temps de traitement impactant les 10% restants varie en fonction de\u00a0:</p>\n<ul>\n<li>le type de la demande pr\u00e9sent\u00e9e (niveau de risque ou de complexit\u00e9)</li>\n<li>le temps qu'il vous faut pour r\u00e9pondre \u00e0 des demandes ou \u00e0 questions suppl\u00e9mentaires</li>\n<li>le volume des demandes re\u00e7ues</li>\n<li>examen techniques ou inspections </li>\n<li>des changements dans la politique et la fa\u00e7on dont nous fonctionnons</li>\n<li>facteurs ind\u00e9pendants de notre volont\u00e9 (par exemple: catastrophes naturelles, conflits dans certaines r\u00e9gions, \u00e9pid\u00e9mies de maladies animales, restrictions phytosanitaires</li>\n</ul>\n<p>Votre d\u00e9lai de traitement commence le jour o\u00f9 nous recevons votre demande compl\u00e8te et se termine lorsque nous prenons une d\u00e9cision.</p>\n\n<p>Si vous avez pr\u00e9sent\u00e9 une demande en utilisant votre compte Mon ACIA et que le d\u00e9lai est plus long que le d\u00e9lai indiqu\u00e9 ci-dessous, veuillez v\u00e9rifier l'\u00e9tat de votre demande dans votre compte Mon ACIA. Si vous ne voyez pas de mise \u00e0 jour, veuillez nous appeler au 1-800-442-2342.</p>\n\n<p>Le <abbr title=\"Centre d'administration\">CdA</abbr> s'efforce de traiter les demandes dans les d\u00e9lais normaux. Les d\u00e9lais de traitement peuvent changer selon le type, la complexit\u00e9 et le nombre de demandes re\u00e7ues.</p>\n\n<p>Assurez-vous de v\u00e9rifier tr\u00e8s attentivement votre demande avant de la transmettre, car une pr\u00e9sentation incompl\u00e8te risque de prolonger le traitement.</p>\n\n<div class=\"row\"> \n<div class=\"col-sm-6\"><p>Demandes simples d'animaux et de v\u00e9g\u00e9taux\u00a0:</p></div>\n<div class=\"col-sm-6\"><p><strong>10</strong> jours ouvrables</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\"><p>Demandes complexes d'animaux et de <span class=\"nowrap\">v\u00e9g\u00e9taux<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0\u00a0</span>1</a></sup></span> (exigeant l'examen d'un sp\u00e9cialiste)\u00a0:</p></div>\n<div class=\"col-sm-6\"><p>Pourrait d\u00e9passer <strong>10</strong> jours ouvrables</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\"><p>Exemptions minist\u00e9rielles pour les fruits et l\u00e9gumes frais\u00a0:</p></div>\n<div class=\"col-sm-6\"><p><strong>2</strong> jours ouvrables</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p>Exemptions minist\u00e9rielles et Permissions de transport de produits transform\u00e9s\u00a0:</p></div>\n<div class=\"col-sm-6\"><p><strong>5</strong> jours ouvrables</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p>Autorisations d'essais de mise en march\u00e9\u00a0:</p></div>\n<div class=\"col-sm-6\"><p><strong>6</strong> \u00e0 <strong>12</strong> semaines</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p>Demandes des licences relatives \u00e0 la salubrit\u00e9 des aliments au Canada\u00a0:</p>\n</div>\n<div class=\"col-sm-6\"><p><strong>48</strong> heures</p></div>\n</div>\n<div class=\"clearfix\"> </div>\n<div class=\"row\"> \n<div class=\"col-sm-6\">\n<p>Demandes des licences relatives \u00e0 la salubrit\u00e9 des aliments au Canada qui ont besoin d'une v\u00e9rification pr\u00e9alable \u00e0 <span class=\"nowrap\">l'\u00e9mission<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0\u00a0</span>2</a></sup></span>\u00a0:</p></div>\n<div class=\"col-sm-6\"><p><strong>70</strong> jours ouvrables</p></div>\n</div>\n</div>\n\n<div class=\"col-sm-4 mrgn-tp-0\">\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Quoi de neuf</h3>\n</header>\n<div class=\"panel-body\">\n<p>Renseignez-vous au sujet du <a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC)</a> et d\u00e9terminez si votre entreprise a besoin d'une licence et apprenez quand pr\u00e9senter une demande.</p>\n\n<p>Pr\u00e9sentez votre demande en ligne et suivez-en le traitement en vous inscrivant \u00e0 <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\" class=\"nowrap\">Mon\u00a0ACIA</a>.</p>\n\n<div class=\"mrgn-bttm-sm mrgn-tp-md\"> \n<a href=\"https://services.inspection.gc.ca/setLangFr?url=https%3A%2F%2Fservices.inspection.gc.ca%2Fmycfia-monacia%2F\" title=\"Se connecter ou s'inscrire \u00e0 Mon\u00a0ACIA\" class=\"btn btn-primary btn-block mrgn-bttm-sm\" role=\"button\">Se connecter ou s'inscrire \u00e0 Mon\u00a0ACIA\n<img alt=\"My CFIA\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/mycfia-monacia_1483807779108_fra.jpg\" class=\"img-responsive img-rounded mrgn-bttm-md mrgn-tp-sm\"></a>\n</div>\n</div>\n</section>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Exigences en mati\u00e8re de permis</h3>\n</header>\n<div class=\"panel-body\">\n<p><a href=\"https://ca1se.voxco.com/SE/93/SFCR_licence/?&amp;lang=fr\">Outil interactif pour le <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr>\u00a0: Vous faut-il un permis pour les aliments?</a></p>\n\n<p>Le <a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a> pr\u00e9sente les exigences en mati\u00e8re d'importation des produits r\u00e9glement\u00e9s par l'ACIA</p>\n</div>\n</section>\n\n<p class=\"mrgn-bttm-lg text-center\"><a href=\"/fra/1530134673372/1530134725024\" class=\"btn btn-primary btn-block mrgn-bttm-sm\" role=\"button\">Trouver de l'information<br>sur les prix de l'ACIA</a></p>\n\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\" id=\"contact\">Communiquez avec nous</h3>\n</header>\n<div class=\"panel-body\">\n<p>Le <abbr title=\"Centre d'administration\">CdA</abbr> offre des services de soutien bilingue du lundi au vendredi, de <span class=\"nowrap\">7 <abbr title=\"heures\">h</abbr>\u200e \u00e0 19 <abbr title=\"heures\">h</abbr>\u200e (<abbr title=\"heure normale de l'Est\">HNE</abbr>).</span> Nos bureaux sont ferm\u00e9s les jours de cong\u00e9s f\u00e9ri\u00e9s.</p>\n\n<p class=\"mrgn-tp-sm\">T\u00e9l\u00e9phone\u00a0:<br>\n<br>\n</p>\n\n<p class=\"mrgn-tp-sm\">Courriel\u00a0:<br>\n<a href=\"mailto:permission@inspection.gc.ca\" class=\"nowrap\">permission@inspection.gc.ca</a></p>\n\n<p class=\"mrgn-tp-sm\">On r\u00e9pondra aux messages \u00e9lectroniques et aux messages laiss\u00e9s sur la bo\u00eete vocale au cours des trois jours ouvrables qui suivent.</p>\n\n<h4>Restez au courant des nouvelles!</h4>\n\n<p>Inscrivez-vous aux <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">avis par courriel</a> de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></p>\n</div>\n</section>\n</div>\n</div>\n\n<h2>Trouver un formulaire de demande</h2>\n\n<p>Recherche par\u2026</p>\n\n<ul class=\"list-unstyled\">\n<li class=\"mrgn-bttm-md\"><a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/centre-d-administration-pour-les-permissions/formulaires-de-demande-aliments/fra/1529708437469/1529708437719\">Aliments</a> (exemptions minist\u00e9rielles, permissions de transport et autorisations d'essais de mise en march\u00e9)\n\n<p>Note\u00a0: En acc\u00e9dant le portail <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\">Mon ACIA</a>, vous pourrez faire une demande d'une licence relative \u00e0 la salubrit\u00e9 des aliments au Canada.</p>\n</li>\n<li class=\"mrgn-bttm-md\"><a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/centre-d-administration-pour-les-permissions/formulaires-de-demande-animaux/fra/1529708468073/1529708468291\">Sant\u00e9 des animaux</a> (agents zoopathog\u00e8nes, produits animaliers et sous-produits (y compris les aliments pour animaux de compagnie), animaux vivants (animaux aquatiques et terrestres, y compris les amphibiens et reptiles), \u0153ufs d'incubation, et germoplasme d'animaux)</li>\n<li class=\"mrgn-bttm-md\"><a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/centre-d-administration-pour-les-permissions/formulaires-de-demande-vegetaux/fra/1529708480533/1529708480759\">V\u00e9g\u00e9taux</a> (v\u00e9g\u00e9taux, produits de v\u00e9g\u00e9taux, grains ou ol\u00e9agineuses non transform\u00e9es/brutes ou farines v\u00e9g\u00e9tales, produits du bois, et inscription au programme des mat\u00e9riaux d'emballage en bois)</li>\n</ul>\n\n<h3>Paiement en ligne</h3>\n\n<p>Inscrivez-vous pour obtenir un compte <a href=\"/a-propos-de-l-acia/compte-mon-acia/fra/1482204298243/1482204318353\">Mon\u00a0ACIA</a>, qui vous permettra d'acc\u00e9der \u00e0 des options de paiement en ligne s\u00e9curis\u00e9es.</p>\n\n<p>Les entreprises qui g\u00e8rent de nombreuses demandes peuvent choisir la facturation mensuelle comme option de paiement en ligne dans Mon\u00a0ACIA en remplissant une <a href=\"/fra/1328823628115/1328823702784#c0015\">Demande de cr\u00e9dit (CFIA/ACIA 0015)</a></p>\n\n<h3>Transparence</h3>\n\n<h4><a href=\"/a-propos-de-l-acia/permis-licences-et-approbations/centre-d-administration-pour-les-permissions/rapports-trimestriels/fra/1529703280983/1529703315739\">Rapports trimestriels pour les permissions qui ont \u00e9t\u00e9 \u00e9mises</a></h4>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Notes de bas de page</h2>\n<dl>\n<dt>Note de bas 1</dt>\n<dd id=\"fn1\">\n<p>Citons parmi les demandes complexes d'animaux et de v\u00e9g\u00e9taux, toutes les demandes de permis d'importation de zoonoses pathog\u00e8nes, de produits et de sous-produits animaliers contenant des zoonoses pathog\u00e8nes.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p></dd>\n\n<dt>Note de bas 2</dt>\n<dd id=\"fn2\">\n<p>Une v\u00e9rification avant d\u00e9livrance de la part d'un agent de gestion de cas du CdA pourrait \u00eatre d\u00e9clench\u00e9e, par exemple, en cas d'information manquante dans une demande ou d'information erron\u00e9e. Dans certains cas, une demande pourrait devoir \u00eatre confi\u00e9e \u00e0 un inspecteur du centre op\u00e9rationnel pour qu'il r\u00e9alise une inspection avant d\u00e9livrance. Si l'une de ces situations se produisait, le d\u00e9lai de traitement serait plus long.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p></dd>\n\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}