{
    "dcr_id": "1448047992567",
    "title": {
        "en": "Response to the 2011 Fukushima nuclear power plant incident",
        "fr": "R\u00e9ponse du Canada \u00e0 l'accident de la centrale nucl\u00e9aire de <span lang=\"en\">Fukushima</span> en 2011"
    },
    "html_modified": "2024-02-13 9:51:05 AM",
    "modified": "2022-06-01",
    "issued": "2022-06-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_imp_domestic_japan_1448047992567_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_imp_domestic_japan_1448047992567_fra"
    },
    "ia_id": "1448048041336",
    "parent_ia_id": "1363573894485",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1363573894485",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Response to the 2011 Fukushima nuclear power plant incident",
        "fr": "R\u00e9ponse du Canada \u00e0 l'accident de la centrale nucl\u00e9aire de <span lang=\"en\">Fukushima</span> en 2011"
    },
    "label": {
        "en": "Response to the 2011 Fukushima nuclear power plant incident",
        "fr": "R\u00e9ponse du Canada \u00e0 l'accident de la centrale nucl\u00e9aire de <span lang=\"en\">Fukushima</span> en 2011"
    },
    "templatetype": "content page 1 column",
    "node_id": "1448048041336",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1363573601756",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/chemical-hazards/response/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/dangers-chimiques/reponse/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Response to the 2011 Fukushima nuclear power plant incident",
            "fr": "R\u00e9ponse du Canada \u00e0 l'accident de la centrale nucl\u00e9aire de Fukushima en 2011"
        },
        "description": {
            "en": "Following the March 11, 2011 earthquake in Japan, the Canadian Food Inspection Agency (CFIA) took several measures to assess and protect the Canadian food supply from potential effects of Japan's nuclear crisis.",
            "fr": "\u00c0 la suite du tremblement de terre du 11 mars 2011 au Japon, l'Agence canadienne d'inspection des aliments (ACIA) a pris plusieurs mesures pour \u00e9valuer et prot\u00e9ger l'approvisionnement alimentaire canadien des effets potentiels de la crise nucl\u00e9aire japonaise."
        },
        "keywords": {
            "en": "apan, earthquake, imports, food, lookout, testing, radiation, response",
            "fr": "Japon, tremblement de terre, importations, aliments, surveillance, analyse, particules radioactives, lait, Colombie-Britannique, r\u00e9ponse"
        },
        "dcterms.subject": {
            "en": "consumers,imports,inspection,food safety",
            "fr": "consommateur,importation,inspection,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Government of Canada,Canadian Food Inspection Agency"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-06-01",
            "fr": "2022-06-01"
        },
        "modified": {
            "en": "2022-06-01",
            "fr": "2022-06-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Response to the 2011 Fukushima nuclear power plant incident",
        "fr": "R\u00e9ponse du Canada \u00e0 l'accident de la centrale nucl\u00e9aire de Fukushima en 2011"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Canadian Food Inspection Agency's response</h2>\n\n<p>Following the March 11, 2011 earthquake in Japan, the Canadian Food Inspection Agency (CFIA) took several measures to assess and protect the Canadian food supply from potential effects of Japan's nuclear crisis.</p>\n\n<p>In coordination with the Canada Border Services Agency and other government and international partners, the CFIA implemented enhanced import controls, which did not allow food and animal feed products from affected areas in Japan to enter Canada without acceptable documentation or test results verifying their safety.</p>\n\n<p>At the time, the CFIA also worked in collaboration with Health Canada's Radiation Surveillance Program on a sampling and testing strategy to monitor radiation levels in food imported from Japan, as well as in domestic milk and domestic fish off the coast of British Columbia. More than 300 food samples were tested and all were found to be below Health Canada's actionable levels for radioactivity.</p>\n\n<p>Following the implementation of robust control measures by Japan, such as monitoring, enforcement and decontamination, the CFIA lifted its enhanced import controls.</p>\n\n<p>Canada continues to collect and assess information from Japanese officials, Canada's mission abroad and international authorities.</p>\n\n<h2>How we protect you</h2>\n\n<h3>Health Canada</h3>\n\n<p>Health Canada sets policies and standards for the safety and nutritional quality of all food sold in Canada. They also continue to monitor for radionuclides and other chemical contaminants in food sold in Canada through its <a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/food-nutrition-surveillance/canadian-total-diet-study.html\">Total Diet Study</a>, including imports from Japan.</p>\n\n<p>The <a href=\"https://www.canada.ca/en/health-canada/corporate/about-health-canada/branches-agencies/healthy-environments-consumer-safety-branch/environmental-radiation-health-sciences-directorate/radiation-protection-bureau.html\">Radiation Protection Bureau</a> at Health Canada is responsible for delivering Canada's environmental and occupational radiation protection program. Key activities include monitoring environmental and occupational radiation, nuclear emergency preparedness and research.</p>\n\n<h3>Canadian Food Inspection Agency</h3>\n\n<p>The CFIA enforces the policies and standards set by Health Canada, including maximum residue limits for chemical contaminants in retail foods.</p>\n\n<p>The CFIA will use data and information from a variety of sources and will continue to coordinate with other federal departments to verify the safety of food in the Canadian marketplace.</p>\n\n<h2>Related links</h2>\n\n<ul>\n<li><a href=\"http://nuclearsafety.gc.ca/eng/resources/fukushima/index.cfm\">Canadian Nuclear Safety Commission\u00a0\u2013\u00a0Canada's Response to Fukushima</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/publications/health-risks-safety/summary-report-fukushima-accident-contaminants-canada.html\">Health Canada\u00a0\u2013\u00a0Summary report on Fukushima accident contaminants in Canada </a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/health-risks-safety/radiation/radiological-nuclear-emergencies/previous-incidents-accidents.html\">Health Canada\u00a0\u2013\u00a0Previous nuclear incidents and accidents</a></li>\n<li><a href=\"https://www.iaea.org/\">International Atomic Energy Agency</a></li>\n<li><a href=\"https://www.who.int/activities/overcoming-health-consequences-of-fukushima-nuclear-accident\">Overcoming health consequences of Fukushima nuclear accident (World Health Organization)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>R\u00e9ponse de l'Agence canadienne d'inspection des aliments</h2>\n\n<p>\u00c0 la suite du tremblement de terre du 11 mars 2011 au Japon, l'Agence canadienne d'inspection des aliments (ACIA) a pris plusieurs mesures pour \u00e9valuer et prot\u00e9ger l'approvisionnement alimentaire canadien des effets potentiels de la crise nucl\u00e9aire japonaise.</p>\n\n<p>En coordination avec l'Agence des services frontaliers du Canada et d'autres partenaires gouvernementaux et internationaux, l'ACIA a mis en \u0153uvre des contr\u00f4les d'importation renforc\u00e9s, qui n'ont pas permis l'entr\u00e9e au Canada de denr\u00e9es alimentaires et d'aliments pour animaux provenant des zones touch\u00e9es au Japon sans documents acceptables ou r\u00e9sultats de tests v\u00e9rifiant leur s\u00e9curit\u00e9.</p>\n\n<p>\u00c0 l'\u00e9poque, l'ACIA a \u00e9galement travaill\u00e9 en collaboration avec le Programme de surveillance des rayonnements de Sant\u00e9 Canada sur une strat\u00e9gie d'\u00e9chantillonnage et d'analyse visant \u00e0 contr\u00f4ler les niveaux de rayonnement dans les aliments import\u00e9s du Japon, ainsi que dans le lait et le poisson domestiques au large des c\u00f4tes de la Colombie-Britannique. Plus de 300 \u00e9chantillons d'aliments ont \u00e9t\u00e9 test\u00e9s et tous se sont r\u00e9v\u00e9l\u00e9s inf\u00e9rieurs aux seuils d'intervention de Sant\u00e9 Canada en mati\u00e8re de radioactivit\u00e9.</p>\n\n<p>\u00c0 la suite de la mise en \u0153uvre de mesures de contr\u00f4le rigoureuses par le Japon, telles que la surveillance, l'application de la loi et la d\u00e9contamination, l'ACIA a lev\u00e9 ses contr\u00f4les d'importation renforc\u00e9s.</p>\n\n<p>Le Canada continue de recueillir et d'\u00e9valuer les informations fournies par les autorit\u00e9s japonaises, la mission du Canada \u00e0 l'\u00e9tranger et les autorit\u00e9s internationales.</p>\n\n<h2>Comment nous vous prot\u00e9geons</h2>\n\n<h3>Sant\u00e9 Canada</h3>\n\n<p>Sant\u00e9 Canada d\u00e9finit des politiques et des normes pour la s\u00e9curit\u00e9 et la qualit\u00e9 nutritionnelle de tous les aliments vendus au Canada. Il continue \u00e9galement \u00e0 surveiller la pr\u00e9sence de radionucl\u00e9ides et d'autres contaminants chimiques dans les aliments vendus au Canada dans le cadre de son <a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/surveillance-aliments-nutrition/etude-canadienne-alimentation-totale.html\">\u00e9tude sur l'alimentation totale</a>, y compris les importations en provenance du Japon.</p>\n\n<p>Le <a href=\"https://www.canada.ca/fr/sante-canada/organisation/a-propos-sante-canada/directions-generales-agences/direction-generale-sante-environnementale-securite-consommateurs/direction-sciences-sante-environnementale-radioprotection/bureau-radioprotection.html\">Bureau de la radioprotection</a> de Sant\u00e9 Canada est charg\u00e9 de mettre en \u0153uvre le programme de radioprotection environnementale et professionnelle du Canada. Ses principales activit\u00e9s comprennent la surveillance des rayonnements environnementaux et professionnels, la pr\u00e9paration aux urgences nucl\u00e9aires et la recherche.</p>\n\n<h3>Agence canadienne d'inspection des aliments</h3>\n\n<p>L'ACIA applique les politiques et les normes \u00e9tablies par Sant\u00e9 Canada, y compris les limites maximales de r\u00e9sidus pour les contaminants chimiques dans les aliments vendus au d\u00e9tail.</p>\n\n<p>L'ACIA utilisera des donn\u00e9es et des informations provenant de diverses sources et continuera \u00e0 se coordonner avec d'autres minist\u00e8res f\u00e9d\u00e9raux pour v\u00e9rifier la s\u00e9curit\u00e9 des aliments sur le march\u00e9 canadien.</p>\n\n<h2>Liens connexes</h2>\n\n<ul>\n<li><a href=\"http://nuclearsafety.gc.ca/fra/resources/fukushima/index.cfm\">Commission canadienne de suret\u00e9 nucl\u00e9aire\u00a0\u2013\u00a0R\u00e9ponse du Canada \u00e0 l'accident de <span lang=\"en\">Fukushima</span></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/publications/securite-et-risque-pour-sante/sommaire-rapport-contaminants-canada-provenant-accident-fukushima.html\">Sant\u00e9 Canada\u00a0\u2013\u00a0Sommaire du rapport sur les contaminants au Canada provenant de l'accident de <span lang=\"en\">Fukushima</span></a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/securite-et-risque-pour-sante/radiation/urgences-radiologiques-nucleaires/incidents-accidents-anterieurs.html\">Sant\u00e9 Canada\u00a0\u2013\u00a0Incidents et accidents nucl\u00e9aires ant\u00e9rieurs</a></li>\n<li><a href=\"https://www.iaea.org/fr\">Agence internationale de l'\u00e9nergie atomique</a></li>\n<li><a href=\"https://www.who.int/activities/overcoming-health-consequences-of-fukushima-nuclear-accident\"><span lang=\"en\">Overcoming health consequences of Fukushima nuclear accident</span> (<span lang=\"en\">World Health Organization</span>) (en anglais seulement)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}