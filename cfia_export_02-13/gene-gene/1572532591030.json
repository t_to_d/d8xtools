{
    "dcr_id": "1572532591030",
    "title": {
        "en": "Fantastic feasts: How CFIA's Burnaby Lab guards your food against fiendish fungus",
        "fr": "Tour de force\u00a0: Comment le laboratoire de l'ACIA de <span lang=\"en\">Burnaby</span> prot\u00e8ge vos aliments contre les champignons"
    },
    "html_modified": "2024-02-13 9:54:14 AM",
    "modified": "2021-10-29",
    "issued": "2019-10-31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/fantastic_feasts_1572532591030_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/fantastic_feasts_1572532591030_fra"
    },
    "ia_id": "1572532591561",
    "parent_ia_id": "1495210111055",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1495210111055",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fantastic feasts: How CFIA's Burnaby Lab guards your food against fiendish fungus",
        "fr": "Tour de force\u00a0: Comment le laboratoire de l'ACIA de <span lang=\"en\">Burnaby</span> prot\u00e8ge vos aliments contre les champignons"
    },
    "label": {
        "en": "Fantastic feasts: How CFIA's Burnaby Lab guards your food against fiendish fungus",
        "fr": "Tour de force\u00a0: Comment le laboratoire de l'ACIA de <span lang=\"en\">Burnaby</span> prot\u00e8ge vos aliments contre les champignons"
    },
    "templatetype": "content page 1 column",
    "node_id": "1572532591561",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1495210110540",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-research-and-publications/fantastic-feasts/",
        "fr": "/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/tour-de-force/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fantastic feasts: How CFIA's Burnaby Lab guards your food against fiendish fungus",
            "fr": "Tour de force\u00a0: Comment le laboratoire de l'ACIA de Burnaby prot\u00e8ge vos aliments contre les champignons"
        },
        "description": {
            "en": "Fantastic feasts: How CFIA's Burnaby Lab guards your food against fiendish fungus.",
            "fr": "Catherine Brisson n'a pas toujours aim\u00e9 les sciences. \u00c0 14 ans, se souvient-elle, je n'aimais pas du tout la biologie et mes notes \u00e9taient mauvaises. Aujourd'hui, les choses ont chang\u00e9. Non seulement Catherine aime les sciences, mais elle a aussi beaucoup de succ\u00e8s dans son domaine"
        },
        "keywords": {
            "en": "research, publications, Fantastic feasts, CFIA, Burnaby Lab, fungus, Burnaby Laboratory",
            "fr": "recherche, publications, Tour de force, laboratoire de l'ACIA, Burnaby, champignons"
        },
        "dcterms.subject": {
            "en": "education,educational guidance,educational resources,food,food inspection,information",
            "fr": "\u00e9ducation,orientation scolaire,ressources p\u00e9dagogiques,aliment,inspection des aliments,information"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Science",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Science"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-10-31",
            "fr": "2019-10-31"
        },
        "modified": {
            "en": "2021-10-29",
            "fr": "2021-10-29"
        },
        "type": {
            "en": "educational material,guide",
            "fr": "mat\u00e9riel didactique,guide"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fantastic feasts: How CFIA's Burnaby Lab guards your food against fiendish fungus",
        "fr": "Tour de force\u00a0: Comment le laboratoire de l'ACIA de Burnaby prot\u00e8ge vos aliments contre les champignons"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Don't you hate it when 1 little thing causes a ton of trouble? It's always the small things that lead to the biggest problems.</p>\n\n<p>Take the classic example of forgetting to check some berries then finding white fluff covering them before you even get a taste, or missing a tiny blue spot on some bread and chucking it in the compost the next day because it is covered. Everyone has had something like that happen to them at some point, right? Well, that mold you are seeing is a fungus.</p>\n\n<p>While fungus on your food is an off-putting inconvenience nowadays, in the past it was enough to get you burned at the stake!</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md well well-sm\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/fantastic_feasts_ergot_1635534423863_eng.jpg\" alt=\"A plant with Claviceps fungus\" class=\"img-responsive\">\n<figcaption>A plant with <i lang=\"la\">Claviceps</i> fungus</figcaption>\n</figure>\n</div>\n\n<p>Centuries ago, a type of fungus called <i lang=\"la\">Claviceps</i> plagued European and North American farms. The fungus itself isn't that bad, but it produces chemicals called mycotoxins that have nasty effects on people, including delusions, psychosis, and seizure-like spasms. <i lang=\"la\">Claviceps</i> gained its infamous name, \"ergot of rye,\" because of its tendency to grow on rye grains. Since rye is a common baking ingredient, ergot has had a long history of creeping into the food people eat, leading to all types of trouble.</p>\n\n<p>In the fall of 1691, residents of Salem, Massachusetts, celebrated a successful rye harvest. However, that year's rainy spring made ideal conditions for ergot to grow. Suddenly, local women who made food from rye began to show \"signs of possession by evil spirits.\" These \"signs,\" mainly hallucinations and spasms, were suspiciously similar to symptoms of ergot poisoning.</p>\n\n<p>Most of these women were regularly eating and working with raw and cooked rye and did not know they were most likely in contact with ergot-contaminated foods. As a result, over\u00a0200 people were accused of witchcraft and 20 were executed. Interestingly, after a warm and dry spring the following year, the ergot dried up and Salem's \"bewitchments\" suddenly ended, too.</p>\n\n<p>The Salem witch trials are not the only time ergot wrought havoc on a community. In\u00a01518, residents of Strasbourg, France, were baffled when\u00a0400 residents began dancing uncontrollably. The town hired musicians to play for the dancers until they collapsed from exhaustion, believing that would calm the spirits possessing them. Little did they know, the victims had been exposed to a strain of ergot toxins found in the modern-day hallucinogenic LSD. It turns out LSD is a product of ergot fungus. Imagine how scary it was for early villagers to see their neighbours suddenly become zany, not knowing it was caused by fungus-filled food!</p>\n\n<p>Ergot outbreaks may seem like a thing of the past, but warming climates and changing weather have created a perfect environment for fungus to grow on grains and fruit. However, protecting plant and animal health and keeping <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">food safe</a> is what CFIA does. Fighting fiendish fungus is something scientists at our <a href=\"/science-and-research/our-laboratories/burnaby/eng/1549640328443/1549640495885\">Burnaby Laboratory</a> do regularly. To fight this foe, our scientists use a complex blend of chemistry and biology to root out even the tiniest traces of toxins, including ergot of rye.</p>\n\n<h2>Finding fungus</h2>\n\n<div class=\"col-sm-5 pull-right mrgn-lft-md well well-sm\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_burnaby_1498061272393_eng.jpg\" alt=\"The CFIA's Burnaby laboratory\" class=\"img-responsive\">\n<figcaption>The CFIA's Burnaby laboratory</figcaption>\n</figure>\n</div>\n\n<p>To test for ergot, our scientists at the Burnaby Laboratory separate the chemical components and toxins in a sample by binding them to special beads in a column. Once background components are washed away, the bound toxins are released from the beads by changing the conditions. Scientists can identify which components are in a sample based on the speed and conditions under which each chemical detaches from the column. Food samples that contain high amounts of ergot toxins are deemed unfit to eat.</p>\n\n<p>The scientists use similar chemical testing methods to spot and limit exposure to many other mycotoxins, including <i lang=\"la\">patulin</i>. Historically, ergot-infested grains were a major cause of illness. Now, fruit gardens can be an overlooked area for fungi to fester. Backyard apple and cherry trees aren't usually maintained to the same standard as commercial orchards, and without regular picking and disposal, rotting fruit may spread fungi. Fruit supports a variety of fungi, including species that can produce the toxin <i lang=\"la\">patulin</i>, known to cause genetic mutation at high levels and even cancer. Responding to simple and easily identifiable quality cues is still the best way to prevent <i lang=\"la\">patulin</i> poisoning: don't process or eat damaged or rotting fruit, simply compost it instead.</p>\n\n<p>While our scientists at the Burnaby Lab don't perform exorcisms or witch trials, the sheer number of tests they complete is nothing short of magic. CFIA scientists at the Burnaby Laboratory can test for a wide range of bugs and chemical contaminants. All this helps to ensure that our fruit, grains and food are safe to eat. With nearly\u00a038 million Canadians eating grain and fruit products each day, there is no wondering why our Burnaby scientists need a trove of tests to keep Canadians safe.</p>\n\n<h2>Additional information</h2>\n\n<ul>\n\n<li><strong>Find out what is happening at other</strong> <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a></li>\n\n<li><a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">Learn more about CFIA food safety programs</a></li>\n\n<li>Learn about <a href=\"/inspect-and-protect/science-and-innovation/cfia-s-trading-cards/eng/1568731957675/1568743720700\">CFIA Everyday Superheroes</a></li>\n\n<li>Learn about <a href=\"/science-and-research/our-research-and-publications/using-dna-science-to-fight-food-borne-illness/eng/1511282633357/1511282633638\">Using DNA science to fight food-borne illness</a> (fact sheet)</li>\n\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Quoi de pire que les petites choses qui cr\u00e9ent de gros probl\u00e8mes\u00a0? Ce sont toujours les petites choses qui cr\u00e9ent les plus gros probl\u00e8mes.</p>\n\n<p>Pensez au cas classique qui consiste \u00e0 oublier d'inspecter les petits fruits et d'y trouver du duvet blanc au moment de vouloir les consommer, ou \u00e0 ne pas remarquer un petit point bleu sur du pain et de devoir le mettre au compost la journ\u00e9e suivante parce qu'il est couvert de moisissures. Tout le monde a v\u00e9cu ce genre de situations, pas vrai\u00a0? Ces moisissures sont en fait des champignons.</p>\n\n<p>Les champignons sur vos aliments sont un inconv\u00e9nient rebutant de nos jours, mais dans le pass\u00e9, ils pouvaient vous envoyer au b\u00fbcher\u00a0!</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md well well-sm\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/fantastic_feasts_ergot_1635534423863_fra.jpg\" alt=\"Une plante avec le champignon Claviceps\" class=\"img-responsive\">\n<figcaption>Une plante avec le champignon <i lang=\"la\">Claviceps</i></figcaption>\n</figure>\n</div>\n\n<p>Il y a plusieurs si\u00e8cles, un type de champignon appel\u00e9 <i lang=\"la\">Claviceps</i> a envahi les exploitations agricoles europ\u00e9ennes et nord-am\u00e9ricaines. Le champignon en soi n'est pas si dangereux, mais il produit des substances chimiques appel\u00e9es mycotoxines qui ont des effets n\u00e9fastes sur les gens, notamment le d\u00e9lire, la psychose et les spasmes qui ressemblent \u00e0 des convulsions. Le champignon <i lang=\"la\">Claviceps</i> a \u00e9t\u00e9 appel\u00e9 \u00ab\u00a0ergot du seigle\u00a0\u00bb en raison de sa propension \u00e0 pousser sur le seigle. Comme le seigle est un ingr\u00e9dient courant de boulangerie, l'ergot s'immisce depuis longtemps dans les aliments, ce qui cause de nombreux probl\u00e8mes.</p>\n\n<p>\u00c0 l'automne\u00a01691, des r\u00e9sidents de Salem, au Massachusetts, ont c\u00e9l\u00e9br\u00e9 une r\u00e9colte fructueuse. Toutefois, le printemps pluvieux cette ann\u00e9e-l\u00e0 a cr\u00e9\u00e9 des conditions id\u00e9ales \u00e0 la croissance de l'ergot. Les femmes qui pr\u00e9paraient des aliments contenant du seigle se sont soudainement mises \u00e0 avoir l'air \u00ab\u00a0poss\u00e9d\u00e9es par de mauvais esprits\u00a0\u00bb. Leurs comportements, qui \u00e9taient principalement des hallucinations et des spasmes, \u00e9taient \u00e9trangement similaires aux sympt\u00f4mes de l'empoisonnement \u00e0 l'ergot.</p>\n\n<p>La plupart de ces femmes mangeaient et manipulaient r\u00e9guli\u00e8rement du seigle cru et cuit et ne savaient pas qu'elles touchaient probablement des aliments contamin\u00e9s \u00e0 l'ergot. Par cons\u00e9quent, plus de\u00a0200 personnes ont \u00e9t\u00e9 accus\u00e9es de sorcellerie et ex\u00e9cut\u00e9es. Fait int\u00e9ressant, apr\u00e8s un printemps chaud et sec l'ann\u00e9e suivante, l'ergot a disparu et les \u00ab\u00a0ensorcellements\u00a0\u00bb ont soudainement pris fin.</p>\n\n<p>Les proc\u00e8s des sorci\u00e8res de Salem ne sont pas l'unique cas o\u00f9 l'ergot a fait des ravages au sein d'une collectivit\u00e9. En\u00a01518, des r\u00e9sidents de Strasbourg, en France, ont \u00e9t\u00e9 d\u00e9concert\u00e9s lorsque\u00a0400 r\u00e9sidents se sont mis \u00e0 danser de mani\u00e8re incontr\u00f4lable. Le village a embauch\u00e9 des musiciens pour les faire danser jusqu'\u00e0 ce qu'ils croulent de fatigue. Les gens croyaient que ceci calmerait les esprits qui les poss\u00e9daient. Ils \u00e9taient loin de se douter que les victimes avaient \u00e9t\u00e9 expos\u00e9es \u00e0 une souche de toxines d'ergot que l'on retrouve aujourd'hui dans le LSD (hallucinog\u00e8ne). Il s'av\u00e8re que le LSD est un produit de l'ergot. Imaginez la peur des villageois en voyant leurs voisins devenir fous, sans toutefois se douter que ces derniers \u00e9taient intoxiqu\u00e9s pas des aliments contamin\u00e9s par un champignon\u00a0!</p>\n\n<p>Les \u00e9closions d'ergot semblent \u00eatre choses du pass\u00e9, mais le r\u00e9chauffement climatique et les conditions m\u00e9t\u00e9orologiques changeantes cr\u00e9ent un environnement optimal \u00e0 la croissance de l'ergot sur les grains et les fruits. Mais il y a l'ACIA, qui a pour mandat de prot\u00e9ger les v\u00e9g\u00e9taux, la sant\u00e9 des animaux et la <a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">salubrit\u00e9 des aliments</a>. La lutte contre les champignons dangereux fait partie des activit\u00e9s courantes des chercheurs de notre <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/burnaby/fra/1549640328443/1549640495885\">Laboratoire de <span lang=\"en\">Burnaby</span></a>. Pour combattre ces ennemis, nos chercheurs utilisent un m\u00e9lange complexe de chimie et de biologie pour d\u00e9tecter m\u00eame les plus infimes traces de toxines, incluant l'ergot du seigle.</p>\n\n<h2>D\u00e9pistage des champignons</h2>\n\n<div class=\"col-sm-5 pull-right mrgn-lft-md well well-sm\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_burnaby_1498061272393_fra.jpg\" alt=\"Le Laboratoire de Burnaby de l'ACIA\" class=\"img-responsive\">\n<figcaption>Le Laboratoire de Burnaby de l'ACIA</figcaption>\n</figure>\n</div>\n\n<p>Pour d\u00e9pister l'ergot, les chercheurs de notre Laboratoire de <span lang=\"en\">Burnaby</span> s\u00e9parent les composantes chimiques et les toxines d'un \u00e9chantillon en les liants \u00e0 des billes sp\u00e9ciales dans une colonne. Une fois les principales composantes lessiv\u00e9es, les toxines li\u00e9es sont lib\u00e9r\u00e9es des billes gr\u00e2ce \u00e0 la modification des conditions. Les chercheurs peuvent identifier les composantes d'un \u00e9chantillon selon la rapidit\u00e9 et les conditions dans lesquelles chaque composante chimique se d\u00e9tache de la colonne. Les \u00e9chantillons d'aliments qui contiennent une grande quantit\u00e9 de toxines d'ergot sont jug\u00e9s impropres \u00e0 la consommation.</p>\n\n<p>Les chercheurs utilisent des m\u00e9thodes d'essais similaires pour d\u00e9tecter et limiter l'exposition \u00e0 de nombreuses autres mycotoxines, incluant la <i lang=\"la\">patuline</i>. Dans le pass\u00e9, les grains infest\u00e9s par l'ergot \u00e9taient une cause majeure de maladie. De nos jours, les jardins fruitiers qui manquent de surveillance peuvent \u00eatre un endroit favorisant la croissance des champignons. Les pommiers et les cerisiers qui se trouvent dans l'arri\u00e8re-cour des maisons sont souvent moins bien entretenus que les vergers commerciaux. Les fruits qui ne sont pas cueillis et \u00e9limin\u00e9s r\u00e9guli\u00e8rement peuvent pourrir et contribuer \u00e0 la multiplication des champignons. Les fruits sont des h\u00f4tes de divers champignons, notamment de certaines esp\u00e8ces qui produisent la <i lang=\"la\">patuline</i>, qui, en grande quantit\u00e9, cause des mutations g\u00e9n\u00e9tiques et m\u00eame le cancer. L'observation d'indices simples et facilement rep\u00e9rables quant \u00e0 la qualit\u00e9 des fruits demeure le meilleur moyen de pr\u00e9venir les intoxications \u00e0 la <i lang=\"la\">patuline</i>. Il ne faut pas transformer ou manger les fruits endommag\u00e9s ou pourris, mais plut\u00f4t les composter.</p>\n\n<p>Les chercheurs de notre Laboratoire de <span lang=\"en\">Burnaby</span> n'effectuent pas d'exorcismes ni de chasse aux sorci\u00e8res, mais le nombre impressionnant d'analyses qu'ils r\u00e9alisent rel\u00e8ve presque de la magie. Les chercheurs de l'ACIA du laboratoire de <span lang=\"en\">Burnaby</span> proc\u00e8dent \u00e0 des \u00e9preuves de d\u00e9pistage d'un vaste ensemble d'insectes et de contaminants chimiques. Tous ces travaux assurent la salubrit\u00e9 des fruits, des grains et des aliments. Pr\u00e8s de\u00a038 millions de Canadiens consomment chaque jour des produits \u00e0 base de grains et de fruits, il n'est donc pas surprenant que les chercheurs du Laboratoire de <span lang=\"en\">Burnaby</span> doivent effectuer une multitude d'essais pour prot\u00e9ger les Canadiens.</p>\n\n<h2>Renseignements additionnels</h2>\n\n<ul>\n\n<li><strong>D\u00e9couvrez ce qui se passe dans d'autres</strong> <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a></li>\n\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">D\u00e9couvrez plus sur les programmes de salubrit\u00e9 alimentaire de l'ACIA</a></li>\n\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/cartes-a-echanger-de-l-acia/fra/1568731957675/1568743720700\">Apprends sur les superh\u00e9ros de l'ACIA</a></li>\n\n<li>Apprenez-en plus sur l'<a href=\"/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/recourir-a-la-science-de-l-adn-pour-lutter-contre-/fra/1511282633357/1511282633638\">utilisation de la science de l'ADN pour pr\u00e9venir les maladies d'origine alimentaire</a> (fiche de renseignements)</li>\n\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}