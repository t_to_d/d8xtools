{
    "dcr_id": "1325552911406",
    "title": {
        "en": "Horticultural pests of concern",
        "fr": "Organismes nuisibles de l'horticulture"
    },
    "html_modified": "2024-01-26 2:20:51 PM",
    "modified": "2020-11-19",
    "issued": "2015-04-14 10:45:22",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_hort_pests_1325552911406_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_hort_pests_1325552911406_fra"
    },
    "ia_id": "1325553121430",
    "parent_ia_id": "1299167874884",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299167874884",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Horticultural pests of concern",
        "fr": "Organismes nuisibles de l'horticulture"
    },
    "label": {
        "en": "Horticultural pests of concern",
        "fr": "Organismes nuisibles de l'horticulture"
    },
    "templatetype": "content page 1 column",
    "node_id": "1325553121430",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299167784070",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/horticulture/pests/",
        "fr": "/protection-des-vegetaux/horticulture/organismes-nuisibles/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Horticultural pests of concern",
            "fr": "Organismes nuisibles de l'horticulture"
        },
        "description": {
            "en": "List of horticultural pests of concern.",
            "fr": "Liste d'organismes nuisibles r\u00e9glement\u00e9s."
        },
        "keywords": {
            "en": "horticultural pests, citrus long-horned beetle, european grapevine moth, japanese beetle, plum pox virus, potato cyst nematode, bacterial wilt of geraniums, sudden oak death, tomato leafminer, Asian longhorned beetle, Asian long-horned beetle, ALHB, ALB, Starry sky beetle",
            "fr": "organismes nuisibles d'horticulture, sponieuse asiatique, eud\u00e9mis de la vigne, longicorne des agrumes, scarab\u00e9e japonais, potyvirus de la sharka du prunier, n\u00e9matode \u00e8a kyste de la pomme de terre, fl\u00e9trissure bact\u00e9rienne des p\u00e9largoniums, encre des ch\u00eanes rouges, mineuse de la tomate, Longicorne asiatique, Longicorne \u00e9toil\u00e9"
        },
        "dcterms.subject": {
            "en": "horticulture,insects,inspection,plant diseases,plants",
            "fr": "horticulture,insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-14 10:45:22",
            "fr": "2015-04-14 10:45:22"
        },
        "modified": {
            "en": "2020-11-19",
            "fr": "2020-11-19"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Horticultural pests of concern",
        "fr": "Organismes nuisibles de l'horticulture"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The following pests are regulated by the CFIA and may be transported on horticultural commodities, such as fresh fruit and vegetables, cut flowers and foliage, and plants for planting.</p>\n\n<p>This list is not exhaustive; many other pests may also be transported on these products. Please also see the full <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">list of pests regulated by Canada</a>.</p>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-11-01/eng/1322607792840/1322608036166\"><i lang=\"la\">Anoplophora</i> spp.\u00a0\u2013 Long-horned beetles</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/citrus-long-horned-beetle/eng/1326124573039/1326124817392\"><i lang=\"la\">Anoplophora chinensis</i>\u00a0\u2013 Citrus long-horned beetle</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/asian-longhorned-beetle/eng/1337792721926/1337792820836\"><i lang=\"la\">Anoplophora glabripennis</i>\u00a0\u2013 Asian longhorned beetle</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/apple-proliferation-phytoplasma/eng/1368189722179/1368189789199\">Apple proliferation phytoplasma</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-18-01/eng/1548961691046/1548961691358\">Black currant reversion virus</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/ginseng-anthracnose/eng/1495143860550/1495143913483\"><i lang=\"la\">Colletotrichum panacicola</i>\u00a0\u2013 Ginseng anthracnose</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/european-brown-garden-snail/eng/1326332798489/1326332875537\"><i lang=\"la\">Cornu aspersum</i>\u00a0\u2013 European brown garden snail</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/potato-rot-nematode/eng/1326330708349/1326330805394\"><i lang=\"la\">Ditylenchus destructor</i>\u00a0\u2013 Potato rot nematode </a></li>\n<li><a href=\"/plant-health/invasive-species/insects/light-brown-apple-moth/eng/1326465917763/1326466022858\"><i lang=\"la\">Epiphyas postvittana</i>\u00a0\u2013 Light brown apple moth</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/grapevine-yellows/eng/1326136953554/1326137125813\">Flavescence <span lang=\"fr\">dor\u00e9e</span> and <span lang=\"fr\">bois noir</span>\u00a0\u2013 Grapevine yellows</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/golden-nematode/eng/1336742692502/1336742884627\"><i lang=\"la\">Globodera pallida</i>\u00a0\u2013 Pale cyst nematode</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/golden-nematode/eng/1336742692502/1336742884627\"><i lang=\"la\">Globodera rostochiensis</i>\u00a0\u2013 Golden nematode</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/oriental-fruit-moth/eng/1326377901354/1326377984808\"><i lang=\"la\">Grapholita molesta</i>\u00a0\u2013 Oriental fruit moth</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/european-grapevine-moth/eng/1326827653939/1326827746437\"><i lang=\"la\">Lobesia botrana</i>\u00a0\u2013 European grapevine moth</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/columbia-root-knot-nematode/eng/1327330760232/1327331014894\"><i lang=\"la\">Meloidogyne chitwoodi</i>\u00a0\u2013 Columbia root-knot nematode</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/necrosis-of-grapevine/eng/1327585505591/1327585592895\"><i lang=\"la\">Phomopsis viticola</i>\u00a0\u2013 Necrosis of grapevine</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-00-08/eng/1312332936247/1312333485361\"><i lang=\"la\">Phytophthora alni</i></a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/sudden-oak-death/eng/1327587864375/1327587972647\"><i lang=\"la\">Phytophthora ramorum</i>\u00a0\u2013 Sudden oak death</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/plum-pox-virus/eng/1323888514908/1323889333540\">Plum pox virus</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/japanese-beetle/eng/1328048149161/1328048244390\"><i lang=\"la\">Popillia japonica</i>\u00a0\u2013 Japanese beetle</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/pstvd/eng/1517949934628/1517949935371\">Potato spindle tuber viroid (PSTVd)</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/pest-risk-management/rmd-10-27/eng/1304792813891/1304821766538\"><i lang=\"la\">Pseudomonas syringae</i>\u00a0\u2013 Horse-chestnut bleeding disease</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-01-04/eng/1333479606359/1337962643447\"><i lang=\"la\">Puccinia graminis</i>\u00a0\u2013 Black stem rust</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-97-05/eng/1312331283196/1312331704778\"><i lang=\"la\">Puccinia horiana</i>\u00a0\u2013 Chrysanthemum white rust</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/bacterial-wilt-of-geraniums/eng/1328249007083/1328249093356\"><i lang=\"la\">Ralstonia solanacearum</i>\u00a0\u2013 Bacterial wilt of geraniums</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/european-cherry-fruit-fly/eng/1467981423932/1467981769931\"><i lang=\"la\">Rhagoletis cerasi</i>\u00a0\u2013 European cherry fruit fly</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/blueberry-maggot/eng/1328325206503/1328325288221\"><i lang=\"la\">Rhagoletis mendax</i>\u00a0\u2013 Blueberry maggot</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/apple-maggot/eng/1329865069998/1329865149392\"><i lang=\"la\">Rhagoletis pomonella</i>\u00a0\u2013 Apple maggot</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/eng/1327933703431/1327933793006\"><i lang=\"la\">Synchytrium endobioticum</i>\u00a0\u2013 Potato wart or potato canker</a></li>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-10-01/eng/1304622464578/1312239626557\"><i lang=\"la\">Thaumatotibia leucotreta</i>\u00a0\u2013 False codling moth</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/tomato-leafminer/eng/1328557520161/1328557819263\"><i lang=\"la\">Tuta absoluta</i>\u00a0\u2013 Tomato leafminer</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les organismes nuisibles suivants sont r\u00e9glement\u00e9s par l'ACIA et peuvent \u00eatre transport\u00e9s avec des produits de l'horticulture, comme les fruits et l\u00e9gumes frais, les fleurs et feuilles coup\u00e9es, et les v\u00e9g\u00e9taux destin\u00e9s \u00e0 la plantation.</p>\n\n<p>Cette liste n'est pas exhaustive; plusieurs autres organismes nuisibles peuvent aussi \u00eatre transport\u00e9s avec ces produits. Veuillez vous r\u00e9f\u00e9rer aussi \u00e0 la <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">liste compl\u00e8te des organismes nuisibles r\u00e9glement\u00e9s par le Canada</a>.</p>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-11-01/fra/1322607792840/1322608036166\"><i lang=\"la\">Anoplophora</i> spp.\u00a0\u2013 Longicornes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-des-agrumes/fra/1326124573039/1326124817392\"><i lang=\"la\">Anoplophora chinensis</i>\u00a0\u2013 Longicorne des agrumes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-asiatique/fra/1337792721926/1337792820836\"><i lang=\"la\">Anoplophora glabripennis</i>\u00a0\u2013 Longicorne asiatique</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/maladie-des-proliferations-du-pommier/fra/1368189722179/1368189789199\"><span lang=\"en\">Apple proliferation phytoplasma</span>\u00a0\u2013 Maladie des prolif\u00e9rations du pommier</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/anthracnose-du-ginseng/fra/1495143860550/1495143913483\"><i lang=\"la\">Colletotrichum panacicola</i>\u00a0\u2013 Anthracnose du ginseng</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/escargot-petit-gris/fra/1326332798489/1326332875537\"><i lang=\"la\">Cornu aspersum</i>\u00a0\u2013 Escargot petit-gris</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-de-la-pourriture-des-racines/fra/1326330708349/1326330805394\"><i lang=\"la\">Ditylenchus destructor</i>\u00a0\u2013 N\u00e9matode de la pourriture des racines</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/pyrale-brun-pale-de-la-pomme/fra/1326465917763/1326466022858\"><i lang=\"la\">Epiphyas postvittana</i>\u00a0\u2013 Pyrale brun p\u00e2le de la pomme</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-dore/fra/1336742692502/1336742884627\"><i lang=\"la\">Globodera pallida</i>\u00a0\u2013 N\u00e9matode \u00e0 kystes p\u00e2les</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-dore/fra/1336742692502/1336742884627\"><i lang=\"la\">Globodera rostochiensis</i>\u00a0\u2013 N\u00e9matode dor\u00e9</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/tordeuse-orientale-du-pecher/fra/1326377901354/1326377984808\"><i lang=\"la\">Grapholita molesta</i>\u00a0\u2013 Tordeuse orientale du p\u00eacher</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/jaunisse-de-la-vigne/fra/1326136953554/1326137125813\">Jaunisse de la vigne\u00a0: Flavescence dor\u00e9e et bois noir</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/l-eudemis-de-la-vigne/fra/1326827653939/1326827746437\"><i lang=\"la\">Lobesia botrana</i>\u00a0\u2013 Eud\u00e9mis de la vigne</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-a-galles-du-columbia/fra/1327330760232/1327331014894\"><i lang=\"la\">Meloidogyne chitwoodi</i>\u00a0\u2013 N\u00e9matode \u00e0 galles du Columbia</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/excoriose-de-la-vigne/fra/1327585505591/1327585592895\"><i lang=\"la\">Phomopsis viticola</i>\u00a0\u2013 Excoriose de la vigne</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-00-08/fra/1312332936247/1312333485361\"><i lang=\"la\">Phytophthora alni</i></a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/encre-des-chenes-rouges/fra/1327587864375/1327587972647\"><i lang=\"la\">Phytophthora ramorum</i>\u00a0\u2013 Mort subite du ch\u00eane</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/potyvirus-de-la-sharka-du-prunier/fra/1323888514908/1323889333540\">Potyvirus de la sharka du prunier</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/scarabee-japonais/fra/1328048149161/1328048244390\"><i lang=\"la\">Popillia japonica</i>\u00a0\u2013 Scarab\u00e9e japonais</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/gestion-des-risques-phytosanitaire/dgr-10-27/fra/1304792813891/1304821766538\"><i lang=\"la\">Pseudomonas syringae</i>\u00a0\u2013 Maladie du chancre suintant du marronnier</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-01-04/fra/1333479606359/1337962643447\"><i lang=\"la\">Puccinia graminis</i>\u00a0\u2013 Rouille noire</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-97-05/fra/1312331283196/1312331704778\"><i lang=\"la\">Puccinia horiana</i>\u00a0\u2013 Rouille blanche du chrysanth\u00e8me</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/fletrissure-bacterienne-des-pelargoniums/fra/1328249007083/1328249093356\"><i lang=\"la\">Ralstonia solanacearum</i>\u00a0\u2013 Fl\u00e9trissure bact\u00e9rienne des p\u00e9largoniums</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-europeenne-des-cerises/fra/1467981423932/1467981769931\"><i lang=\"la\">Rhagoletis cerasi</i>\u00a0\u2013 Mouche europ\u00e9enne des cerises</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-du-bleuet/fra/1328325206503/1328325288221\"><i lang=\"la\">Rhagoletis mendax</i>\u00a0\u2013 Mouche du bleuet</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-de-la-pomme/fra/1329865069998/1329865149392\"><i lang=\"la\">Rhagoletis pomonella</i>\u00a0\u2013 Mouche de la pomme</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/fra/1327933703431/1327933793006\"><i lang=\"la\">Synchytrium endobioticum</i>\u00a0\u2013 Gale (tumeur) verruqueuse de la pomme de terre</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-10-01/fra/1304622464578/1312239626557\"><i lang=\"la\">Thaumatobia leucotreta</i>\u00a0\u2013 Fausse carpocapse</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mineuse-de-la-tomate/fra/1328557520161/1328557819263\"><i lang=\"la\">Tuta absoluta</i>\u00a0\u2013 Mineuse de la tomate</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/pstvd/fra/1517949934628/1517949935371\">Viroide de la filosit\u00e9 des tubercules de pommes de terre (PSTVd)</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-18-01/fra/1548961691046/1548961691358\">Virus de r\u00e9version feuille \u00e9troite</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}