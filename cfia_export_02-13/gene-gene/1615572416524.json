{
    "dcr_id": "1615572416524",
    "title": {
        "en": "Fact sheet - Feed retail outlets",
        "fr": "Le feuillet d'information\u00a0- D\u00e9taillants d'aliments du b\u00e9tail"
    },
    "html_modified": "2024-02-13 9:55:03 AM",
    "modified": "2021-06-11",
    "issued": "2021-06-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/feed_reg_mod_feed_rtl_factsheet_1615572416524_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/feed_reg_mod_feed_rtl_factsheet_1615572416524_fra"
    },
    "ia_id": "1615572416837",
    "parent_ia_id": "1612971995765",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1612971995765",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fact sheet - Feed retail outlets",
        "fr": "Le feuillet d'information\u00a0- D\u00e9taillants d'aliments du b\u00e9tail"
    },
    "label": {
        "en": "Fact sheet - Feed retail outlets",
        "fr": "Le feuillet d'information\u00a0- D\u00e9taillants d'aliments du b\u00e9tail"
    },
    "templatetype": "content page 1 column",
    "node_id": "1615572416837",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1612969567098",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/regulatory-modernization/feed-retail-outlets/",
        "fr": "/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/detaillants-d-aliments-du-betail/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fact sheet - Feed retail outlets",
            "fr": "Le feuillet d'information\u00a0- D\u00e9taillants d'aliments du b\u00e9tail"
        },
        "description": {
            "en": "Feed retail outlets are businesses that sell feed in the Canadian marketplace. They may sell pre-packaged and labeled feeds or they may re-package and label feeds on site.",
            "fr": "Les d\u00e9taillants d'aliments du b\u00e9tail sont des entreprises vendant des aliments du b\u00e9tail sur le march\u00e9 canadien. Ceux-ci pourraient vendre des aliments pr\u00e9emball\u00e9s et \u00e9tiquet\u00e9s ou r\u00e9emballer et \u00e9tiqueter des aliments sur place."
        },
        "keywords": {
            "en": "animals, disease, freedom, feed retail outlets",
            "fr": "animaux, sant\u00e9 des animaux, d\u00e9taillants d'aliments du b\u00e9tail"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-06-11",
            "fr": "2021-06-11"
        },
        "modified": {
            "en": "2021-06-11",
            "fr": "2021-06-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fact sheet - Feed retail outlets",
        "fr": "Le feuillet d'information\u00a0- D\u00e9taillants d'aliments du b\u00e9tail"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The information in this document is based on requirements set out in the proposed <i>Feeds Regulations,\u00a02022</i> (the \"regulations\"). The information is intended to help regulated parties understand the requirements within the regulations once they come into force. The proposed requirements are subject to change as the regulatory process advances through its various stages. In the interim, current laws applicable to livestock feed in Canada continue to apply.</p>\n</div>\n\n<p>The proposed <i>Feeds Regulations,\u00a02022</i> will impact a variety of stakeholders, including:</p>\n\n<ul>\n<li>single ingredient feed manufacturers and suppliers</li>\n<li>mixed feed manufacturers and suppliers (for example, commercial feed mills, specialty feed manufacturers, etc.)</li>\n<li>rendering facilities manufacturing livestock feed ingredients</li>\n<li>feed retail outlets</li>\n<li>livestock producers (on-farm feed mills)</li>\n<li>feed importers</li>\n<li>feed exporters</li>\n</ul>\n\n<p>Feed retail outlets are businesses that sell feed in the Canadian marketplace. They may sell pre-packaged and labelled feeds or they may re-package and label feeds on site. Feed retail outlets who manufacture mixed feeds would be considered a commercial feed mill and should also refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/commercial-feed-mills/eng/1615566739606/1615566739934\">commercial feed mills fact sheet</a>. Feed retail outlets who manufacture single ingredient feeds would be considered a single ingredient feed manufacturer and should refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/single-ingredient-feed-manufacturers/eng/1615558194427/1615558194848\">single ingredient feed manufacturers fact sheet</a>. Feed retail outlets who export feed or import feed, should also refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/feed-exporters/eng/1616429118122/1616429118418\">feed exporters fact sheet</a> or <a href=\"/animal-health/livestock-feeds/regulatory-modernization/feed-importers/eng/1615832860377/1615832860783\">feed importers fact sheet</a>, respectively.</p>\n\n<p>This fact sheet applies to you if:</p>\n\n<ul>\n<li>you sell feed that you did not manufacture</li>\n<li>you re-package, label, and sell feed that you did not manufacture</li>\n</ul>\n\n<h2 id=\"a1\">New regulatory requirements that apply to you</h2>\n\n<ul>\n<li><a href=\"#a1_1\">Hazard analysis and preventive control plans</a></li>\n<li><a href=\"#a1_2\">Licences</a></li>\n<li><a href=\"#a1_3\">Traceability and record-keeping</a></li>\n<li><a href=\"#a1_4\">Product registration</a></li>\n<li><a href=\"#a1_5\">Product labelling</a></li>\n</ul>\n\n<h3 id=\"a1_1\">Hazard analysis and preventive control plans</h3>\n\n<p>You must prepare, keep, maintain and implement a written preventive control plan  (PCP) which will include:</p>\n\n<ul>\n<li>the identification and analysis of hazards associated with your feed establishment, equipment used, incoming materials, feeds or your processes. This would include receiving, re-packaging, handling, storage or distribution, and measures to prevent cross contamination</li>\n<li>the control measures used to prevent, eliminate or reduce the hazards identified</li>\n<li>preventive controls you implement to meet other regulatory requirements such as general and safety standards, labelling, recalls, complaints, and record-keeping and traceability</li>\n</ul>\n\n<p>This is a new requirement. Please refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/preventive-feed-safety-controls/eng/1616416409398/1616416409898\">preventive feed safety controls fact sheet</a> and <a href=\"/animal-health/livestock-feeds/regulatory-modernization/hazard-identification-and-analysis/eng/1616421533834/1616421534162\">hazard identification and analysis fact sheet</a> for additional information.</p>\n\n<h3 id=\"a1_2\">Licences</h3>\n\n<p>You will require a licence if the feeds you re-package, label, or sell are sent or conveyed across provincial borders, are to be exported or if you import feeds for sale.</p>\n\n<p>This is a new requirement. Please refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/licensing/eng/1616419115431/1616419115759\">licensing fact sheet</a> for additional information.</p>\n\n<h3 id=\"a1_3\">Traceability and record-keeping</h3>\n\n<p>You will be required to keep records of the feeds you receive and sell. The records for both feeds you receive and those you sell must include the name of the feed, the lot number, and the date. Records for feeds that you receive must indicate from whom they came and for feeds that are sold, to whom they were sold. This requirement is similar to the record-keeping requirements for animal foods in the <i>Health of Animals Regulations</i>.</p>\n\n<p>This is a new requirement in the proposed <i>Feeds Regulations,\u00a02022</i>. Please refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/traceability/eng/1617040726269/1617040726644\">traceability fact sheet</a> for additional information.</p>\n\n<h3 id=\"a1_4\">Product registration</h3>\n\n<p>It is unlikely you will be required to register the feeds you sell. If you are re-packaging and rebranding a registered feed, you may also need to register it under your brand. This is not a change from current requirements.</p>\n\n<p>The manufacturer is usually responsible for product registration, but you should ensure that any feeds you are offering for sale are appropriately registered. You should be aware that some mixed feeds will still require mandatory product registration.</p>\n\n<p>This is an amended requirement. Please refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/feed-approval-and-product-registration-guidance/eng/1621369494906/1621369712780\">feed approval and product registration guidance for the proposed <i>Feeds Regulations, 2022</i></a> for additional information.</p>\n\n<h3 id=\"a1_5\">Product labelling</h3>\n\n<p>You will be required to properly label the feed that you offer for sale. Although the manufacturer is usually responsible for labelling their feeds, you should ensure that any feeds you are offering for sale are appropriately labelled. If you are re-packaging or re-labelling feed you will be responsible for ensuring the feed is properly labelled.</p>\n\n<p>This is an amended requirement. Please refer to the <a href=\"/animal-health/livestock-feeds/regulatory-modernization/labelling-guidance/eng/1617285809091/1617285931989\">labelling guidance for the proposed <i>Feeds Regulations, 2022</i></a> for additional information.</p>\n\n<h2>Benefits</h2>\n\n<p>The proposed <i>Feeds Regulations,\u00a02022</i> will introduce an outcome-based and risk-based approach to feed safety and compliance through modernized regulatory requirements (hazard identification and analysis, preventive control plans, traceability and labelling requirements) and permissions (feed ingredient assessments and approvals, product registration and licences). The benefits that this new regulatory framework will provide are to:</p>\n\n<ul>\n<li>safeguard feed and the food production continuum</li>\n<li>attain the most effective and efficient balance between fair and competitive trade in the market; and</li>\n<li>minimize regulatory burden</li>\n</ul>\n\n<p>The updates to the regulatory requirements will provide more flexibility to you. More feeds will be exempt from registration which means you may have a greater number of feeds available to sell. Improved labelling flexibilities will allow you to provide customers with more useful information on feed labels. Preventive control programs throughout the feed chain will provide your customers with greater confidence in the safety of the feeds they purchase.</p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n\n<p>Les renseignements contenus dans ce document s'appuient sur les dispositions \u00e9tablies dans le projet de <i>R\u00e8glement de\u00a02022 sur les aliments du b\u00e9tail</i> (le \u00ab\u00a0r\u00e8glement\u00a0\u00bb). Les renseignements visent \u00e0 aider les parties r\u00e9glement\u00e9es \u00e0 comprendre les dispositions contenues dans le r\u00e8glement lorsqu'elles entreront en vigueur. Les dispositions propos\u00e9es peuvent \u00eatre modifi\u00e9es au fur et \u00e0 mesure de l'\u00e9tat d'avancement du processus de r\u00e9glementation au cours de ses diverses \u00e9tapes. Dans l'intervalle, les lois actuelles applicables aux aliments du b\u00e9tail au Canada continueront de s'appliquer.</p>\n\n</div>\n\n<p>Le <i>R\u00e8glement de\u00a02022 sur les aliments du b\u00e9tail</i> propos\u00e9 touchera une vari\u00e9t\u00e9 d'intervenants, y compris</p>\n\n<ul>\n<li>les fabricants et les fournisseurs d'aliments \u00e0 ingr\u00e9dient unique</li>\n<li>les fabricants et les fournisseurs d'aliments m\u00e9lang\u00e9s (par exemple, les meuneries commerciales, fabricant d'aliments sp\u00e9ciaux etc.)</li>\n<li>les usines d'\u00e9quarrissage fabriquant des aliments du b\u00e9tail</li>\n<li>les d\u00e9taillants d'aliments du b\u00e9tail</li>\n<li>les \u00e9leveurs de b\u00e9tail (meuneries \u00e0 la ferme)</li>\n<li>les importateurs d'aliments du b\u00e9tail</li>\n<li>les exportateurs d'aliments du b\u00e9tail</li>\n</ul>\n\n<p>Les d\u00e9taillants d'aliments du b\u00e9tail sont des entreprises vendant des aliments du b\u00e9tail sur le march\u00e9 canadien. Ceux-ci pourraient vendre des aliments pr\u00e9emball\u00e9s et \u00e9tiquet\u00e9s ou r\u00e9emballer et \u00e9tiqueter des aliments sur place. Les d\u00e9taillants d'aliments du b\u00e9tail fabriquant des aliments m\u00e9lang\u00e9s seraient consid\u00e9r\u00e9s comme des meuneries commerciales et devraient \u00e9galement consulter le feuillet d'information <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/meuneries-commerciales/fra/1615566739606/1615566739934\">meuneries commerciales</a>. Les d\u00e9taillants d'aliments du b\u00e9tail fabriquant des aliments \u00e0 ingr\u00e9dient unique seraient consid\u00e9r\u00e9s comme des fabricants d'aliments \u00e0 ingr\u00e9dient unique et devraient consulter le feuillet d'information <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/fabricants-d-aliments-du-betail-a-ingredient-uniqu/fra/1615558194427/1615558194848\">fabricants d'aliments du b\u00e9tail \u00e0 ingr\u00e9dient unique</a>. Les d\u00e9taillants d'aliments du b\u00e9tail exportant ou important des aliments du b\u00e9tail devraient \u00e9galement consulter les feuillets d'information <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/exportateurs-d-aliments-du-betail/fra/1616429118122/1616429118418\">Exportateurs d'aliments du b\u00e9tail</a> ou <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/importateurs-d-aliments-du-betail/fra/1615832860377/1615832860783\">importateurs d'aliments du b\u00e9tail</a>, respectivement.</p>\n\n<p>Ce feuillet d'information s'applique \u00e0 vous si\u00a0:</p>\n\n<ul>\n<li>vous vendez des aliments du b\u00e9tail que vous n'avez pas fabriqu\u00e9\u00a0;</li>\n<li>vous r\u00e9emballez, \u00e9tiquetez et vendez des aliments du b\u00e9tail que vous n'avez pas fabriqu\u00e9.</li>\n</ul>\n\n<h2 id=\"a1\">Nouvelles exigences r\u00e9glementaires qui s'appliquent \u00e0 vous</h2>\n\n<ul>\n<li><a href=\"#a1_1\">Analyse des dangers et plans de contr\u00f4le pr\u00e9ventif</a></li>\n<li><a href=\"#a1_2\">Licences</a></li>\n<li><a href=\"#a1_3\">Tra\u00e7abilit\u00e9 et tenue de registres</a></li>\n<li><a href=\"#a1_4\">Enregistrement des produits</a></li>\n<li><a href=\"#a1_5\">\u00c9tiquetage des produits</a></li>\n</ul>\n\n<h3 id=\"a1_1\">Analyse des dangers et plans de contr\u00f4le pr\u00e9ventif</h3>\n\n<p>Vous devez pr\u00e9parer, maintenir, actualiser et mettre en \u0153uvre un plan de contr\u00f4le pr\u00e9ventif (PCP) \u00e9crit qui comprendra\u00a0:</p>\n\n<ul>\n<li>l'identification et l'analyse des dangers li\u00e9s \u00e0 votre \u00e9tablissement d'aliments du b\u00e9tail, \u00e0 l'\u00e9quipement utilis\u00e9, aux mati\u00e8res premi\u00e8res, aux aliments du b\u00e9tail ou \u00e0 d'autres proc\u00e9d\u00e9s. Cela comprendrait la r\u00e9ception, le r\u00e9emballage, la manipulation ou la distribution ainsi que les mesures visant \u00e0 pr\u00e9venir la contamination crois\u00e9e</li>\n<li>les mesures de contr\u00f4le utilis\u00e9es pour pr\u00e9venir, \u00e9liminer ou att\u00e9nuer les dangers identifi\u00e9s</li>\n<li>les mesures de contr\u00f4le pr\u00e9ventif instaur\u00e9es pour satisfaire \u00e0 d'autres exigences r\u00e9glementaires, telles que les normes g\u00e9n\u00e9rales et de s\u00e9curit\u00e9, l'\u00e9tiquetage, les rappels, les plaintes et la tenue de registres et la tra\u00e7abilit\u00e9</li>\n</ul>\n\n<p>Il s'agit l\u00e0 d'une nouvelle exigence. Veuillez consulter le feuillet d'information <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/mesures-de-controle-preventif-de-la-salubrite-alim/fra/1616416409398/1616416409898\">mesures de contr\u00f4le pr\u00e9ventif de la salubrit\u00e9 alimentaire</a> et le feuillet d'information <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/la-determination-et-l-analyse-des-dangers/fra/1616421533834/1616421534162\">la d\u00e9termination et l'analyse des dangers</a> pour de plus amples informations.</p>\n\n<h3 id=\"a1_2\">Licences</h3>\n\n<p>Vous aurez besoin d'une licence si les aliments du b\u00e9tail que vous r\u00e9emballez, \u00e9tiquetez ou vendez sont envoy\u00e9s ou transport\u00e9s d'une province \u00e0 une autre ou seront export\u00e9s, ou si vous importez des aliments aux fins de vente.</p>\n\n<p>Il s'agit l\u00e0 d'une nouvelle exigence. Veuillez consulter le feuillet d'information <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/octroi-de-licences/fra/1616419115431/1616419115759\">octroi de licences</a> pour de plus amples informations.</p>\n\n<h3 id=\"a1_3\">Tra\u00e7abilit\u00e9 et tenue de registres</h3>\n\n<p>Vous devrez tenir des registres des aliments du b\u00e9tail que vous fabriquez et vendez. Les registres pour les aliments que vous recevez et pour les aliments que vous vendez doivent inclure le nom des aliments, le num\u00e9ro de lot et la date. Les registres des aliments que vous recevez doivent indiquer leur provenance et ceux des aliments que vous vendez doivent indiquer leurs acheteurs. Cette exigence est semblable \u00e0 celles de la tenue de registres pour les aliments du b\u00e9tail contenues dans le <i>R\u00e8glement sur la sant\u00e9 des animaux</i>.</p>\n\n<p>Il s'agit l\u00e0 d'une nouvelle exigence dans le <i>R\u00e8glement de\u00a02022 sur les aliments du b\u00e9tail</i> propos\u00e9. Veuillez consulter le <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/tracabilite/fra/1617040726269/1617040726644\">feuillet d'information tra\u00e7abilit\u00e9</a> pour de plus amples informations.</p>\n\n<h3 id=\"a1_4\">Enregistrement des produits</h3>\n\n<p>Il est peu probable que vous soyez tenu d'enregistrer les aliments que vous vendez. Si vous r\u00e9emballez un aliment enregistr\u00e9 ou lui donnez une nouvelle image de marque, vous devrez peut-\u00eatre \u00e9galement enregistrer cet aliment sous votre marque. Cela ne diff\u00e8re pas des exigences actuelles.</p>\n\n<p>Le fabricant est habituellement responsable de l'enregistrement des produits, mais vous devriez vous assurer que tous les aliments que vous mettez en vente sont ad\u00e9quatement enregistr\u00e9s. Vous devriez savoir que certains aliments m\u00e9lang\u00e9s n\u00e9cessiteront toujours un enregistrement de produit.</p>\n\n<p>Il s'agit l\u00e0 d'une exigence modifi\u00e9e. Veuillez consulter les <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/directives-concernant-l-approbation-des-aliments-p/fra/1621369494906/1621369712780\">directives concernant l'approbation des aliments pour animaux de ferme et l'enregistrement des produits pour le projet de <i>R\u00e8glement de 2022 sur les aliments du b\u00e9tail</i></a> pour de plus amples informations.</p>\n\n<h3 id=\"a1_5\">\u00c9tiquetage des produits</h3>\n\n<p>Vous devrez \u00e9tiqueter ad\u00e9quatement les aliments du b\u00e9tail que vous mettez en vente. Bien que le fabricant soit habituellement responsable de l'\u00e9tiquetage de ses aliments, vous devriez vous assurer que tous les aliments que vous mettez en vente sont ad\u00e9quatement \u00e9tiquet\u00e9s. Si vous r\u00e9emballez ou r\u00e9\u00e9tiquetez des aliments, vous serez responsable de vous assurer que ceux-ci sont ad\u00e9quatement \u00e9tiquet\u00e9s.</p>\n\n<p>Il s'agit l\u00e0 d'une exigence modifi\u00e9e. Veuillez consulter les <a href=\"/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/directives-concernant/fra/1617285809091/1617285931989\">directives concernant l'\u00e9tiquetage pour le projet de <i>R\u00e8glement de 2022 sur les aliments du b\u00e9tail</i></a> pour de plus amples informations.</p>\n\n<h2>Avantages</h2>\n\n<p>Le <i>R\u00e8glement de\u00a02022 sur les aliments du b\u00e9tail</i> propos\u00e9 introduira une approche fond\u00e9e sur les r\u00e9sultats et les risques en mati\u00e8re de salubrit\u00e9 et de conformit\u00e9 des aliments du b\u00e9tail par l'entremise d'exigences r\u00e9glementaires modernis\u00e9es (identification et analyse des dangers, plans de contr\u00f4le pr\u00e9ventif, exigences li\u00e9es \u00e0 la tra\u00e7abilit\u00e9 et \u00e0 l'\u00e9tiquetage) et d'autorisations (\u00e9valuations et approbations des ingr\u00e9dients contenus dans les aliments du b\u00e9tail, enregistrement des produits et licences). Les avantages que ce nouveau cadre r\u00e9glementaire offrira auront pour but de\u00a0:</p>\n\n<ul>\n<li>pr\u00e9server la continuit\u00e9 de la production d'aliments du b\u00e9tail et d'aliments</li>\n<li>atteindre l'\u00e9quilibre le plus efficace et efficient entre le commerce \u00e9quitable et concurrentiel dans le march\u00e9; et</li>\n<li>minimiser le fardeau r\u00e9glementaire</li>\n</ul>\n\n<p>Les mises \u00e0 jour aux exigences r\u00e9glementaires vous offriront une meilleure souplesse. Un nombre accru d'aliments du b\u00e9tail seront exempt\u00e9s d'enregistrement, ce qui signifie que vous pourriez b\u00e9n\u00e9ficier d'un nombre accru d'aliments du b\u00e9tail \u00e0 vendre. Une plus grande souplesse en mati\u00e8re d'\u00e9tiquetage vous permettra d'offrir aux clients des informations plus utiles sur les \u00e9tiquettes d'aliments. Les programmes de contr\u00f4le pr\u00e9ventif tout au long de la cha\u00eene d'aliments du b\u00e9tail permettront de faire augmenter la confiance des clients \u00e0 l'\u00e9gard de la salubrit\u00e9 des aliments du b\u00e9tail qu'ils ach\u00e8tent.</p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}