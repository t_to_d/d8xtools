{
    "dcr_id": "1339422512964",
    "title": {
        "en": "Questions and Answers - Guidelines for Highlighted Ingredients and Flavours",
        "fr": "Questions et r\u00e9ponses - Lignes directrices sur la mise en \u00e9vidence d'ingr\u00e9dients et de saveurs"
    },
    "html_modified": "2012-06-11 09:48",
    "modified": "20-10-2014",
    "issued": "11-6-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/labeti_otherreq_claims_composition_highlightedguidelines_questionsandanswers_1339422512964_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/labeti_otherreq_claims_composition_highlightedguidelines_questionsandanswers_1339422512964_fra"
    },
    "parent_ia_id": "1339422145856",
    "ia_id": "1339422581861",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and Answers - Guidelines for Highlighted Ingredients and Flavours",
        "fr": "Questions et r\u00e9ponses - Lignes directrices sur la mise en \u00e9vidence d'ingr\u00e9dients et de saveurs"
    },
    "label": {
        "en": "Questions and Answers - Guidelines for Highlighted Ingredients and Flavours",
        "fr": "Questions et r\u00e9ponses - Lignes directrices sur la mise en \u00e9vidence d'ingr\u00e9dients et de saveurs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1339422581861",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1339421942081",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/composition-and-quality/highlighted-ingredients-and-flavours/questions-and-answers/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/composition-et-qualite/evidence-d-ingredients-et-de-saveurs/questions-et-reponses/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and Answers - Guidelines for Highlighted Ingredients and Flavours",
            "fr": "Questions et r\u00e9ponses - Lignes directrices sur la mise en \u00e9vidence d'ingr\u00e9dients et de saveurs"
        },
        "description": {
            "en": "To provide guidance to clarify existing requirements and policies on the use of highlighted ingredient or flavour claims on a label or advertisement",
            "fr": "d'offrir des avis et des conseils afin de clarifier les exigences et les politiques existantes surl'utilisation d'all\u00e9gations visant la mise en \u00e9vidence des ingr\u00e9dients et des saveurs sur les \u00e9tiquettes ou dans la publicit\u00e9"
        },
        "keywords": {
            "en": "label, labelling, advertising, claims, highlighted, ingredients, flavours, guidelines, questions, answers",
            "fr": "&#233;tiquette, &#233;tiquetage, annonce, mise en &#233;vidence, ingr&#233;dients, saveurs, lignes directrices"
        },
        "dcterms.subject": {
            "en": "labelling,food labeling,inspection,food inspection,legislation,policy,consumer protection,regulation",
            "fr": "\u00e9tiquetage,\u00e9tiquetage des aliments,inspection,inspection des aliments,l\u00e9gislation,politique,protection du consommateur,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Labelling and Claims Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'\u00e9tiquetage et des all\u00e9gations alimentaires"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-06-11 09:48:33",
            "fr": "2012-06-11 09:48:33"
        },
        "modified": {
            "en": "2014-10-20",
            "fr": "2014-10-20"
        },
        "type": {
            "en": "frequently asked questions,legislation and regulations,reference material,policy",
            "fr": "foire aux questions\u00a0,l\u00e9gislation et r\u00e8glements,mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and Answers - Guidelines for Highlighted Ingredients and Flavours",
        "fr": "Questions et r\u00e9ponses - Lignes directrices sur la mise en \u00e9vidence d'ingr\u00e9dients et de saveurs"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>What are highlighted ingredients and flavours?</h2>\n<p>Highlighted ingredients and flavours are words, pictures or graphics, which emphasize a product's ingredients, components or flavour. These claims can be found on a food product's label or in advertisements.</p>\n<p>For example, a product labelled as a \"Cranberry and apple juice beverage\" highlights the cranberry juice and apple juice ingredients in the food.</p>\n<p>When clearly presented, highlighted ingredients and flavours can provide consumers with useful information and show positive features of a product.</p>\n<p>The <i>Guidelines for Highlighted Ingredients and Flavours</i> provides information to industry when highlighting ingredients and flavours on food labels, to promote labelling that is truthful and not misleading.</p>\n<h2>Why did the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> create guidelines on highlighted ingredients and flavours?</h2>\n<p>Both consumers and industry have expressed concerns to the Canadian Food Inspection Agency (CFIA) regarding false representation of highlighted ingredients and flavours on food labels.</p>\n<p>Since 2003, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> has conducted three stakeholder consultations, as well as focus group and telephone surveys to obtain more information on these concerns.</p>\n<p>Based on these comments, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> drafted the <i>Guidelines on Highlighted Ingredients and Flavours</i> to provide further guidance to industry and clarify existing requirements.</p>\n<h2>What was the process by which these guidelines were developed?</h2>\n<p>The Guidelines are based on three extensive stakeholder consultations, numerous <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>-industry association bilateral meetings, internal consultations, international research, and three public opinion research studies.</p>\n<h2>Why are the guidelines being finalized now?</h2>\n<p>Industry associations have recently expressed renewed interest in finalizing and posting the updated Guidelines. Posting the Guidelines will promote truthful labelling, compliance and consistent assessment of claims.</p>\n<h2>When will these Guidelines be implemented?</h2>\n<p>Currently, <a href=\"/eng/1391025998183/1391026062752?chap=2#s6c2\">Highlighted Ingredient Claims</a> states that it is misleading to over-emphasize the importance, presence or absence of an ingredient or substance in a food because of its desirable or undesirable qualities, or for any other reason.</p>\n<p>This guidance also includes information on the use of certain ingredient names (butter, cream and malt) to describe characteristics of foods.</p>\n<p>The draft of the Guidelines has been shared with regulated parties.</p>\n<p>This consultation is now closed. Comments will be reviewed, after which time the guidelines will be finalized.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Qu'entend-on par mise en \u00e9vidence des ingr\u00e9dients et des saveurs?</h2>\n<p>La mise en \u00e9vidence d'ingr\u00e9dients et de saveurs se fait au moyen de mots, d'images ou de graphiques qui font ressortir des ingr\u00e9dients, des composantes ou une saveur du produit. On peut trouver ce genre de mentions sur l'\u00e9tiquette d'un produit ou dans la publicit\u00e9.</p>\n<p>Par exemple, un produit \u00e9tiquet\u00e9 comme suit \u00ab\u00a0Boisson au jus de canneberge et de pomme\u00a0\u00bb met en \u00e9vidence le jus de canneberge et le jus de pomme comme ingr\u00e9dients entrant dans la composition du produit.</p>\n<p>Lorsqu'ils sont clairement pr\u00e9sent\u00e9s, les ingr\u00e9dients et les saveurs mis en \u00e9vidence peuvent donner aux consommateurs des renseignements utiles et montrer des caract\u00e9ristiques positives d'un produit.</p>\n<p>Les <i>Lignes directrices sur la mise en \u00e9vidence des ingr\u00e9dients et des saveurs</i> donnent de l'information \u00e0 l'industrie en mettant en \u00e9vidence des ingr\u00e9dients et des saveurs sur les \u00e9tiquettes des aliments pour montrer que l'\u00e9tiquetage est v\u00e9ridique et non trompeur.</p>\n<h2>Quand l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a-t-elle cr\u00e9\u00e9 les lignes directrices sur la mise en \u00e9vidence des ingr\u00e9dients et des saveurs?</h2>\n<p>Les consommateurs et l'industrie ont fait part de leurs pr\u00e9occupations \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) concernant la fausse repr\u00e9sentation d'ingr\u00e9dients et de saveurs mis en \u00e9vidence sur les \u00e9tiquettes des aliments.</p>\n<p>Depuis 2003, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a r\u00e9alis\u00e9 trois consultations aupr\u00e8s des intervenants, a mis sur pied un groupe de r\u00e9flexion et effectu\u00e9 des sondages t\u00e9l\u00e9phoniques pour obtenir plus d'informations en lien avec ces pr\u00e9occupations.</p>\n<p>En se fondant sur les commentaires re\u00e7us, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a r\u00e9dig\u00e9 les lignes directrices sur la mise en \u00e9vidence des ingr\u00e9dients et des saveurs pour guider l'industrie et pr\u00e9ciser les exigences \u00e9tablies.</p>\n<h2>Quels processus a-t-on utilis\u00e9 pour l'\u00e9laboration de ces lignes directrices?</h2>\n<p>Les lignes directrices reposent sur de trois vastes consultations men\u00e9es aupr\u00e8s des intervenants, de nombreuses r\u00e9unions bilat\u00e9rales entre l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et les associations de l'industrie, des consultations internes, des recherches internationales et trois recherches sur l'opinion publique.</p>\n<h2>Pourquoi met-on la derni\u00e8re main \u00e0 ces lignes directrices maintenant?</h2>\n<p>Les associations de l'industrie ont r\u00e9cemment manifest\u00e9 un regain d'int\u00e9r\u00eat pour l'ach\u00e8vement et la publication des nouvelles lignes directrices. Leur publication favorisera l'\u00e9tiquetage v\u00e9ridique, la conformit\u00e9 et l'\u00e9valuation uniforme des all\u00e9gations.</p>\n<h2>Quand ces lignes directrices seront-elles mises en application?</h2>\n<p>Actuellement, selon les <a href=\"/fra/1391025998183/1391026062752?chap=2#s6c2\">Ingr\u00e9dients mis en \u00e9vidence</a>, il est jug\u00e9 trompeur d'insister ind\u00fbment sur l'importance, la pr\u00e9sence ou l'absence d'un ingr\u00e9dient ou d'une substance dans un aliment \u00e0 cause de ses qualit\u00e9s enviables ou ind\u00e9sirables, ou pour toute autre raison.</p>\n<p>Ces lignes directrices comprend \u00e9galement de l'information sur l'utilisation de certains noms d'ingr\u00e9dient (beurre, cr\u00e8me et malt) pour d\u00e9crire les caract\u00e9ristiques de produits alimentaires.</p>\n<p>L'\u00e9bauche des lignes directrices a \u00e9t\u00e9 transmise aux parties r\u00e9glement\u00e9es.</p>\n<p>Cette consultation est maintenant termin\u00e9e. Les commentaires feront l\u2019objet d\u2019un examen, apr\u00e8s quoi on finalisera les lignes directrices.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}