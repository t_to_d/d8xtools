{
    "dcr_id": "1563816897273",
    "title": {
        "en": "How to renew or amend an issued permission",
        "fr": "Comment renouveler ou modifier une autorisation \u00e9mise"
    },
    "html_modified": "2024-03-12 3:58:30 PM",
    "modified": "2019-10-23",
    "issued": "2019-07-23",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/how_to_make_amendments_1563816897273_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/how_to_make_amendments_1563816897273_fra"
    },
    "ia_id": "1563816979460",
    "parent_ia_id": "1545974178498",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1545974178498",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "How to renew or amend an issued permission",
        "fr": "Comment renouveler ou modifier une autorisation \u00e9mise"
    },
    "label": {
        "en": "How to renew or amend an issued permission",
        "fr": "Comment renouveler ou modifier une autorisation \u00e9mise"
    },
    "templatetype": "content page 1 column",
    "node_id": "1563816979460",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1545974154373",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/my-cfia-account/user-guidance/how-to-renew-or-amend-an-issued-permission/",
        "fr": "/a-propos-de-l-acia/compte-mon-acia/orientation-de-l-utilisateur/comment-renouveler-ou-modifier-une-autorisation-em/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "How to renew or amend an issued permission",
            "fr": "Comment renouveler ou modifier une autorisation \u00e9mise"
        },
        "description": {
            "en": "Once signed in to My CFIA, make sure the validated party that requires the permission renewal or amendment is selected by clicking the white drop down box under Party Administration, choosing the correct validated party and clicking Select",
            "fr": "Une fois connect\u00e9 \u00e0 Mon ACIA, assurez-vous que la partie valid\u00e9e qui n\u00e9cessite le renouvellement de la permission ou les modifications est s\u00e9lectionn\u00e9e en cliquant sur la zone de liste d\u00e9roulante blanche sous Administration de la partie, en choisissant la bonne partie valid\u00e9e et en cliquant sur S\u00e9lectionner"
        },
        "keywords": {
            "en": "My CFIA, service, business, online, electronic, secure, Information, amendments to an issued permission",
            "fr": "Mon ACIA, services, \u00e9lectroniques, op\u00e9rations, en ligne, Orientation de l'utilisateur, modifications \u00e0 une autorisation \u00e9mise"
        },
        "dcterms.subject": {
            "en": "government communications,government information,administrative services",
            "fr": "communications gouvernementales,information gouvernementale,services administratifs"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-07-23",
            "fr": "2019-07-23"
        },
        "modified": {
            "en": "2019-10-23",
            "fr": "2019-10-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "educators,business,government,general public",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "How to renew or amend an issued permission",
        "fr": "Comment renouveler ou modifier une autorisation \u00e9mise"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-warning\">\n<h2>Note</h2>\n<p>It is not possible to amend an already issued export certificate. If you wish to change your previously provided information you will need to re-apply for a new export certificate.</p>\n</section>\n\n<p>Once signed in to My CFIA, make sure the validated party that requires the permission renewal or amendment is selected by clicking the white drop down box under <strong>Party Administration</strong>, choosing the correct validated party and clicking <strong>Select</strong>:</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img1_1563813207013_eng.jpg\" alt=\"Screen capture of the My CFIA dashboard. Description follows\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-sm\">\n<details>\n<summary>Description of the Party Administration section:</summary>\n<p>If you would like to request services on behalf of a business or organization, select a party profile from the drop down list or click on the hyperlink 'Enrol New Party, Alternative Service Provider/Enter Invite Code' to create a new profile. If you would like to request service for personal use, select your contact.</p> \n</details>\n</figure>\n\n<p>Now scroll down to the <strong>Service Request</strong> section of your party profile dashboard. Select the <strong>Issued Permissions</strong> tab as shown below:</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img2_1563813226622_eng.jpg\" alt=\"Screen capture of the Service Request section. Description follows\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-sm\">\n<details>\n<summary>Description of the Service Request section:</summary>\n<p>Above the Service Request title are 7 tabs:</p>\n\n<ul>\n<li>Service Request</li>\n<li>Issued Permissions</li>\n<li>Issued Export Certificates</li>\n<li>Inspection Reports</li>\n<li>PASO Permission</li>\n<li>e-Invoices</li>\n<li>Payments</li>\n</ul>\n</details>\n</figure>\n\n<p>Under the <strong>Issued Permissions</strong> tab are all permissions your chosen Validated Party has received to date. Find the Permission you wish to amend or renew and click on the hyperlinked permission number located in the <strong>Permission</strong> column as shown below:</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img3_1563813245217_eng.jpg\" alt=\"Screen capture of the Issued Permissions section. Description follows\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-0\">\n<details>\n<summary>Description of the Issued Permissions section:</summary>\n<p>These are your Party's issued Permissions. Please consult the relevant Policies and Procedures that relate to your Permission as they may contain relevant information that may not be listed within the conditions on your permission.</p>\n\n<p>Issued Permissions information is arranged in a table with 5 headings:</p>\n\n<ul>\n<li>Permission </li>\n<li>Application Name</li>\n<li>Issuance Date</li>\n<li>Expiry Date</li>\n<li>Status</li>\n</ul>\n</details>\n</figure>\n\n<p>The following screen will display details about your chosen permission. Select the <strong>Renew </strong>or <strong>Amend </strong> button to begin either process for your issued permission.</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img4_1563813264468_eng.jpg\" alt=\"Screen capture of the Licence Permission Summary screen. Description follows\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-0\">\n<details>\n<summary class=\"mrgn-tp-0\">Description of the Licence Permission Summary screen:</summary>\n<p>The Permission information is displayed in a table with the following headings:</p>\n\n<ul>\n<li>Licence Number</li>\n<li>Licence Holder</li>\n<li>Permission Type</li>\n<li>Issuance Date </li>\n<li>Expiration Date</li>\n<li>Status</li>\n</ul>\n\n<p>You may renew or amend this permission when its status is Active by clicking the renew or amend buttons. For information about renewing or amending a Permission, visit <a href=\"http://inspection.gc.ca\" title=\"CFIA webpage\">http://inspection.gc.ca</a> or contact the CFIA by: Telephone: 1-800-442-2342.</p>\n\n<p>There are 3 clickable buttons:</p>\n\n<ul>\n<li>Renew</li>\n<li>Amend</li>\n<li>Print Licence</li>\n</ul>\n</details>\n</figure>\n\n<h2>When renewing a permission</h2>\n\n<p>If you are renewing a Safe Food for Canadians licence, selecting the renew button will bring you to the application screens which include the current information from your licence. Carefully review your application following the on-screen instructions. It is important to note that if your licence requires amendments to the licenced activities section this should be completed after the licence is renewed. Do not make any changes to your licence activities at this time. Once your renewal request is marked complete and your licence has the status issued and a new expiry date, you may make any necessary changes by following the amendment process. The system will notify you once the renewal process was successful.</p>\n\n<p>If you are renewing a plant or animal import permission you will be required to fill out and upload a new import application form.</p>\n\n<p>Upon completion, your request will be re-submitted for processing.</p>\n\n<h2>When amending a permission</h2>\n\n<p>If you are amending a Safe Food for Canadians licence, selecting the amend button will bring you to application screens which include the current information from your licence. Carefully review your application following the on-screen instructions and make any necessary amendments.</p>\n\n<p>If you are amending a plant or animal import permit permission you will be required to fill out a new import application form and upload the amended version.</p>\n\n<p>Upon completion, your request will be re-submitted for processing.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-warning\">\n<h2>Remarque</h2>\n<p>Il n'est pas possible de modifier un certificat d'exportation d\u00e9j\u00e0 d\u00e9livr\u00e9. Si vous souhaitez modifier vos renseignements pr\u00e9c\u00e9demment soumis, vous devrez pr\u00e9senter une nouvelle demande pour un nouveau certificat d'exportation.</p>\n</section>\n\n<p>Une fois connect\u00e9 \u00e0 Mon ACIA, assurez-vous que la partie valid\u00e9e qui n\u00e9cessite le renouvellement de la permission ou les modifications est s\u00e9lectionn\u00e9e en cliquant sur la zone de liste d\u00e9roulante blanche sous <strong>Administration de la partie</strong>, en choisissant la bonne partie valid\u00e9e et en cliquant sur <strong>S\u00e9lectionner</strong>\u00a0:</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img1_1563813207013_fra.jpg\" alt=\"Capture d'\u00e9cran du tableau de bord Mon ACIA. La description suit.\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-sm\">\n<details>\n<summary>Description de la section Administration de la partie\u00a0:</summary>\n<p>Si vous voulez demander des services au nom d'une entreprise ou un organisme, s\u00e9lectionnez un profil de partie dans la liste d\u00e9roulante ou cliquez sur \u00ab\u00a0inscrire une nouvelle partie, un autre fournisseur de services/entrer le code d'invitation\u00a0\u00bb afin de cr\u00e9er un nouveau profil. Si vous souhaitez demander des services pour une utilisation personnelle, s\u00e9lectionnez votre profil de personne-ressource.</p>\n</details>\n</figure>\n\n<p>D\u00e9filez maintenant l'\u00e9cran \u00e0 la section de <strong>Demande de service</strong> de votre tableau de bord de profil de partie. S\u00e9lectionnez l'onglet des <strong>Autorisations \u00e9mises</strong> comme indiqu\u00e9 ci-dessous\u00a0:</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img2_1563813226622_fra.jpg\" alt=\"Capture d'\u00e9cran de la section Demande de service. La description suit.\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-sm\">\n<details>\n<summary>Description de la section Demande de service\u00a0:</summary>\n<p>Au-dessus du titre Demande de service, vous trouverez 7 onglets\u00a0:</p>\n\n<ul>\n<li>Demande de service</li>\n<li>Autorisations \u00e9mises</li>\n<li>Certificat d'exportation \u00e9mis</li>\n<li>Rapports d'inspection</li>\n<li>Permission BPDPM</li>\n<li>E-factures</li>\n<li>Paiements</li>\n</ul>\n</details>\n</figure>\n\n<p>Sous l'onglet <strong>Autorisations \u00e9mises</strong> sont toutes les autorisations que votre Partie valid\u00e9e choisie a re\u00e7ues \u00e0 ce jour. Recherchez l'autorisation que vous souhaitez modifier ou renouveler et cliquez sur le num\u00e9ro d'autorisation en hyperlien situ\u00e9 dans la colonne <strong>Autorisation</strong>, comme cela est indiqu\u00e9 ci-dessous\u00a0:</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img3_1563813245217_fra.jpg\" alt=\"Capture d'\u00e9cran de la section Autorisations \u00e9mises. La description suit.\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-sm\">\n<details>\n<summary>Description de la section Autorisations \u00e9mises\u00a0:</summary>\n<p>Voici les autorisations \u00e9mises \u00e0 votre partie. Veuillez consulter les politiques et proc\u00e9dures pertinentes \u00e0 votre autorisation; elles peuvent contenir des renseignements pertinents qui pourraient ne pas \u00eatre \u00e9num\u00e9r\u00e9s dans les conditions de votre autorisation.</p>\n\n<p>Les renseignements sur les autorisations \u00e9mises sont class\u00e9s dans un tableau comportant 5 rubriques\u00a0:</p>\n\n<ul>\n<li>Autorisation</li>\n<li>Noms de la demande</li>\n<li>Date d'\u00e9mission</li>\n<li>Date d'\u00e9ch\u00e9ance</li>\n<li>\u00c9tat</li>\n</ul>\n</details>\n</figure>\n\n<p>L'\u00e9cran suivant affichera les d\u00e9tails \u00e0 l'\u00e9gard de l'autorisation s\u00e9lectionn\u00e9e. S\u00e9lectionnez les boutons <strong>Renouvellement</strong> ou <strong>Amender</strong> pour commencer l'un ou l'autre des processus de votre autorisation \u00e9mise.</p>\n\n<figure class=\"mrgn-bttm-lg\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/how_to_make_amendments_img4_1563813264468_fra.jpg\" alt=\"Capture d'\u00e9cran de l'\u00e9cran R\u00e9sum\u00e9 de la permission de licence. La description suit.\" class=\"img-responsive mrgn-tp-lg mrgn-bttm-sm\">\n<details>\n<summary>Description de l'\u00e9cran R\u00e9sum\u00e9 de la permission de licence\u00a0:</summary>\n<p>Les renseignements de permission sont affich\u00e9s dans un tableau avec les rubriques suivantes\u00a0:</p>\n\n<ul>\n<li>Num\u00e9ro de licence</li>\n<li>Titulaire de licence</li>\n<li>Type d'autorisation</li>\n<li>Date d'\u00e9mission</li>\n<li>Date d'expiration</li>\n<li>\u00c9tat</li>\n</ul>\n\n<p>Vous trouverez ci-dessous les renseignements pertinents relatifs au permis qui vous a \u00e9t\u00e9 d\u00e9livr\u00e9.</p>\n\n<p>Vous pouvez renouveler ou modifier cette permission lorsque son statut est actif en cliquant sur les boutons Renouveler ou Modifier. Pour en savoir plus sur le renouvellement ou la modification d'une permission, visitez <a href=\"http://inspection.gc.ca\" title=\"page web d'ACIA\">http://inspection.gc.ca</a> ou communiquez avec l'ACIA par\u00a0: t\u00e9l\u00e9phone\u00a0: 1\u00ad-800\u00ad-442\u00ad-2342</p>\n\n<p>Il y a 3 boutons cliquables\u00a0:</p>\n\n<ul>\n<li>Renouvellement</li>\n<li>Amender</li>\n<li>Imprimer la licence</li>\n</ul>\n</details>\n</figure>\n\n<h2>Lors du renouvellement d'une autorisation</h2>\n\n<p>Si vous renouvelez une licence pour la salubrit\u00e9 des aliments au Canada, le bouton de renouvellement vous am\u00e8nera aux \u00e9crans de demande qui renferment les renseignements \u00e0 jour de votre licence. Examinez attentivement votre demande en suivant les instructions \u00e0 l'\u00e9cran. Il est important de noter que si votre licence exige des modifications \u00e0 la section sur les activit\u00e9s autoris\u00e9es, il doit \u00eatre rempli apr\u00e8s le renouvellement de la licence. Ne modifiez pas vos activit\u00e9s de licence pour le moment. Une fois que votre demande de renouvellement est marqu\u00e9e comme \u00e9tant compl\u00e8te et que votre licence a le statut \u00ab\u00a0\u00e9mise\u00a0\u00bb et une nouvelle date d'expiration, vous pouvez apporter les modifications n\u00e9cessaires en suivant le processus de modification.</p>\n\n<p>Si vous renouvelez une autorisation d'importation d'une plante ou d'un animal, vous devrez remplir et t\u00e9l\u00e9charger un nouveau formulaire de demande d'importation.</p>\n\n<p>Apr\u00e8s avoir termin\u00e9 vos modifications, votre demande sera soumise \u00e0 nouveau afin d'\u00eatre trait\u00e9e.</p>\n\n<h2>Lors de la modification d'une autorisation</h2>\n\n<p>Si vous modifiez une licence pour la salubrit\u00e9 des aliments au Canada, la s\u00e9lection du bouton Amender vous guidera \u00e0 des \u00e9crans de demande qui comprennent les renseignements actuels de votre licence. Examinez attentivement votre demande en suivant les consignes \u00e0 l'\u00e9cran et apportez toutes les modifications n\u00e9cessaires.</p>\n\n<p>Si vous modifiez une autorisation de permis d'importation de v\u00e9g\u00e9taux ou d'animaux, vous devrez remplir une nouvelle demande d'importation et t\u00e9l\u00e9verser la version modifi\u00e9e.</p>\n\n<p>Apr\u00e8s avoir termin\u00e9 vos modifications, votre demande sera soumise \u00e0 nouveau afin d'\u00eatre trait\u00e9e.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}