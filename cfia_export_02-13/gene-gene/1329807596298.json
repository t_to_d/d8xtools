{
    "dcr_id": "1329807596298",
    "title": {
        "en": "Cysticercosis - Fact Sheet",
        "fr": "Cysticercose bovine - Fiche de renseignements"
    },
    "html_modified": "2024-02-13 9:47:20 AM",
    "modified": "2015-07-31",
    "issued": "2012-02-21 01:59:59",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_bovinecystic_fact_1329807596298_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_bovinecystic_fact_1329807596298_fra"
    },
    "ia_id": "1329808582354",
    "parent_ia_id": "1329795247033",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1329795247033",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Cysticercosis - Fact Sheet",
        "fr": "Cysticercose bovine - Fiche de renseignements"
    },
    "label": {
        "en": "Cysticercosis - Fact Sheet",
        "fr": "Cysticercose bovine - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1329808582354",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329795160088",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/cysticercosis/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/cysticercose/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Cysticercosis - Fact Sheet",
            "fr": "Cysticercose bovine - Fiche de renseignements"
        },
        "description": {
            "en": "Cysticercosis is a disease that affects the muscles of infected animals. The livestock species that are more commonly impacted are cattle and pigs. Cysticercosis is caused by the larvae of a human tapeworm.",
            "fr": "La cysticercose bovine est une maladie parasitaire qui s'attaque aux muscles des bovins. Elle est caus\u00e9e par les larves du t\u00e9nia de l'homme, Taenia Saginata."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, cysticercosis, cattle, pigs, human tapeworm, larvae , fact sheet",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, cysticercose bovine, bovins, hommes, Taenia Saginata, fiche de renseignements"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,regulation",
            "fr": "sant\u00e9 animale,inspection,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux terrestres"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-21 01:59:59",
            "fr": "2012-02-21 01:59:59"
        },
        "modified": {
            "en": "2015-07-31",
            "fr": "2015-07-31"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Cysticercosis - Fact Sheet",
        "fr": "Cysticercose bovine - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is cysticercosis?</h2>\n\n<p>Cysticercosis is a disease that affects the muscles of infected animals. The livestock species that are more commonly impacted are cattle and pigs. Cysticercosis is caused by the larvae of a human tapeworm.</p>\n\n<p>Porcine (<abbr title=\"that is to say\">i.e.</abbr> pig) cysticercosis has never been detected in Canada. It is caused by the larvae of the <a href=\"http://www.phac-aspc.gc.ca/lab-bio/res/psds-ftss/taenia-solium-eng.php\">human tapeworm <i lang=\"la\">Taenia solium</i></a>. More information about porcine cysticercosis can be found on the Public Health Agency website.</p>\n \n<p>Bovine (<abbr title=\"for exemple\">e.g.</abbr> cattle, bison) cysticercosis has been found in Canada. However, detections are rare. The last confirmed detection was in 2013. Bovine cysticercosis is caused by the larvae of the <a href=\"http://www.phac-aspc.gc.ca/lab-bio/res/psds-ftss/taenia-saginata-eng.php\">human tapeworm <i lang=\"la\">Taenia saginata</i></a>.</p>\n \n<p>The Canadian Food Inspection Agency (CFIA) has programs in place to help prevent the spread of the disease in Canada.</p>\n\n<h2 class=\"h5\">Is cysticercosis a risk to human health?</h2>\n\n<p>If people consume improperly cooked meat containing the parasite, they can acquire tapeworm infections. Anyone that believes they have been exposed to the parasite should contact their local health authority immediately.</p>\n\n<p>Properly cooking meat to <a href=\"http://healthycanadians.gc.ca/eating-nutrition/safety-salubrite/cook-temperatures-cuisson-eng.php\">safe internal temperatures</a> will inactivate any larvae if present.</p>\n\n<h2 class=\"h5\">What are the clinical signs of cysticercosis?</h2>\n\n<p>Cysticercosis infections in cattle and swine are unlikely to produce any clinical signs.</p>\n\n<p>Symptoms of tapeworm infection in people are often not apparent, but if present, typically include:</p>\n<ul>\n<li>abdominal pain;</li>\n<li>itchiness around the anus; and</li>\n<li>nausea.</li>\n</ul>\n<p>Less common signs can include:</p>\n<ul>\n<li>diarrhea or constipation;</li>\n<li>dizziness;</li>\n<li>headache;</li>\n<li>increased appetite;</li>\n<li>vomiting;</li>\n<li>weakness; and</li>\n<li>weight loss.</li>\n</ul>\n\n<h2 class=\"h5\">Where is cysticercosis found?</h2>\n\n<p>Cysticercosis has a worldwide distribution. It is most prevalent in countries where poor sanitation practices on farms are common and where cultural habits include eating undercooked meat.</p>\n\n<p>Bovine cysticercosis is found sporadically in Canada. Porcine cysticercosis has never been detected in Canada.</p>\n\n<h2 class=\"h5\">How is cysticercosis transmitted and spread?</h2>\n\n<p>Animals can become infected with cysticercosis when they ingest materials contaminated with tapeworm eggs originating from human feces.</p>\n\n<p>Cysticercosis is not transmitted directly from animal-to-animal nor from person-to-person.</p>\n\n<p>In a typical cattle barn climate, tapeworm eggs are estimated to survive about 18 months. Tapeworm eggs are also resistant to a number of common disinfectants. They can, however, be destroyed by drought, since they do not survive in a very dry environment.</p>\n\n<h2 class=\"h5\">How is cysticercosis diagnosed?</h2>\n\n<p>Diagnosis relies on the detection of cysts in the muscle tissue of animals during carcass inspection. Suspect lesions must be submitted to a laboratory in order to confirm diagnosis.</p>\n\n<h2 class=\"h5\">How is cysticercosis treated?</h2>\n\n<p>There is no treatment for this disease in live animals.</p>\n\n<h2 class=\"h5\">What roles and responsibilities exist to prevent cysticercosis?</h2>\n\n<p>From farm to fork, everyone has a role to play in preventing cysticercosis infection. At the farm level, it is important to avoid human fecal contamination of animal feed and feeding areas.</p>\n\n<p>Cysticercosis is a \"reportable disease\" under the <i>Health of Animals Act</i>. This means that all suspected cases must be reported to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for immediate investigation by inspectors.</p>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s National Cysticercosis Program is in place to protect human health through the detection of infected cattle and swine at slaughter. Carcasses are inspected at federally registered abattoirs under the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> <a href=\"/eng/1300125426052/1300125482318\">meat hygiene program</a>. All non-federally registered abattoirs must report any suspicion of bovine cysticercosis to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for investigation.</p>\n<p>If an infected animal is detected at slaughter, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, through its cysticercosis disease control program, would conduct an investigation at the farm level.</p>\n\n<p>Infected carcasses are either condemned or may enter the food chain following treatment.</p>\n\n<p>As always, at home, meat should be cooked to <a href=\"http://healthycanadians.gc.ca/eating-nutrition/safety-salubrite/cook-temperatures-cuisson-eng.php\">safe internal temperatures</a> to help eliminate the potential for foodborne illness.  Properly cooking meat will inactivate any larvae if present.</p>\n<h2 class=\"h5\">How would the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> respond to an outbreak of cysticercosis?</h2>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> investigates all positive cases and takes the following actions:</p>\n<ul>\n<li>Potential farms of origin, as well as all premises where the animals might have lived, are investigated.</li>\n<li>Premises determined to be the source of infection are immediately placed under <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> control.</li>\n<li>Under the oversight of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, owners are required to take certain actions (<abbr title=\"for exemple\">e.g.</abbr> cleaning and disinfection, removal of contaminated feed, <abbr title=\"et cetera\">etc.</abbr>) in order to remove the source of infection.</li>\n<li>Cattle or swine on the infected farms are moved under licence to a federally-inspected abattoir for slaughter when they reach market weight.</li>\n<li>Severely infected carcasses are condemned and disposed of accordingly while carcasses that are not severely infected are temperature treated by freezing for 10 days at -10\u00b0<abbr title=\"celcius\">C</abbr> or heat treated to at least 60\u00b0<abbr title=\"celcius\">C</abbr> to kill the parasite. Treated carcasses can enter the food chain once treatment (heat or cold) is completed.</li>\n<li>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> retains control of the infected premises until the source of infection has been eliminated and there is slaughter evidence that the herd is free of the parasite.</li>\n</ul>\r\n<h2 class=\"font-medium black\">Additional information</h2>\n<ul>\n<li><a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a></li>\n</ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est ce que la cysticercose?</h2>\n<p>La cysticercose est une maladie qui s'attaque aux muscles des animaux infect\u00e9s. Les animaux d'\u00e9levage qui sont les plus souvent touch\u00e9s par cette maladie sont les bovins et les porcs. Cette maladie est caus\u00e9e par les larves du t\u00e9nia de l'humain.</p>\n<p>La cysticercose porcine n'a jamais \u00e9t\u00e9 d\u00e9tect\u00e9e au Canada. Elle est caus\u00e9e par la larve du <a href=\"http://www.phac-aspc.gc.ca/lab-bio/res/psds-ftss/taenia-solium-fra.php\">ver solitaire humain <i lang=\"la\">Taenia solium.</i></a> De plus amples renseignements concernant la cysticercose porcine sont disponibles sur le site Web de l'Agence de la sant\u00e9 publique du Canada.</p>\n<p>Des cas de cysticercose bovine (<abbr title=\"par exemple\">p. ex.</abbr> b\u0153uf, bison) ont \u00e9t\u00e9 d\u00e9tect\u00e9s au Canada, mais ils sont rares. Le dernier cas d\u00e9tect\u00e9 remonte \u00e0 2013. La cysticercose bovine est caus\u00e9e par la larve du <a href=\"http://www.phac-aspc.gc.ca/lab-bio/res/psds-ftss/taenia-saginata-fra.php\">ver solitaire humain <i>Taenia saginata</i></a>.</p>\n<p>L'Agence canadienne d'inspection des aliments (ACIA) a des programmes en place pour aider \u00e0 pr\u00e9venir la propagation de la maladie au Canada.</p>\n<h2 class=\"h5\">La cysticercose pose-t-elle un risque pour la sant\u00e9 humaine?</h2>\n<p>Si une personne consomme de la viande insuffisamment cuite contamin\u00e9e par le parasite, elle peut contracter l'infection par le t\u00e9nia. Toute personne qui croit avoir \u00e9t\u00e9 expos\u00e9e au parasite devrait communiquer imm\u00e9diatement avec les autorit\u00e9s locales de la sant\u00e9.</p>\n<p>La cuisson appropri\u00e9e de la viande \u00e0 une <a href=\"http://canadiensensante.gc.ca/eating-nutrition/healthy-eating-saine-alimentation/safety-salubrite/tips-conseils/cook-temperatures-cuisson-fra.php?_ga=1.206999443.1860291589.1438360751\">temp\u00e9rature interne s\u00e9curitaire</a> inactive les larves qui pourraient \u00eatre pr\u00e9sentes.</p>\n<h2 class=\"h5\">Quels sont les signes cliniques de la cysticercose?</h2>\n<p>Il est peu probable que les infections de cysticercose contract\u00e9es chez les bovins et les porcs se manifestent par des signes cliniques.</p>\n<p>Chez l'humain, les sympt\u00f4mes associ\u00e9s au t\u00e9nia sont souvent imperceptibles, mais peuvent comprendre les suivants\u00a0:</p>\n<ul>\n<li>douleur abdominale</li>\n<li>prurit anal</li>\n<li>naus\u00e9es</li>\n</ul>\n<p>Parmi les sympt\u00f4mes plus rares, mentionnons :</p>\n<ul>\n<li>diarrh\u00e9e ou constipation</li>\n<li>\u00e9tourdissements</li>\n<li>maux de t\u00eate</li>\n<li>augmentation de l'app\u00e9tit</li>\n<li>vomissements</li>\n<li>faiblesse</li>\n<li>perte de poids</li>\n</ul>\n<h2 class=\"h5\">O\u00f9 trouve-t-on des cas de cysticercose?</h2>\n<p>La cysticercose s\u00e9vit partout dans le monde. La maladie est plus pr\u00e9sente dans les pays o\u00f9 les pratiques sanitaires laissent \u00e0 d\u00e9sirer dans les exploitations d'\u00e9levage et dans les pays o\u00f9 les habitants ont l'habitude de manger de la viande insuffisamment cuite.</p>\n<p>On trouve occasionnellement des cas de cysticercose bovine au Canada. Aucun cas de cysticercose porcine n'a encore \u00e9t\u00e9 d\u00e9tect\u00e9 au Canada.</p>\n<h2 class=\"h5\">Quels sont les modes de transmission et de propagation de la cysticercose?</h2>\n<p>Les animaux contractent la maladie en consommant des mati\u00e8res contamin\u00e9es par des oeufs de t\u00e9nia provenant de selles humaines.</p>\n<p>La cysticercose ne se transmet pas directement d'un animal \u00e0 un autre pas plus qu'elle ne se transmet directement d'une personne \u00e0 une autre.</p>\n<p>Dans une \u00e9table classique, les oeufs de t\u00e9nias peuvent survivre pendant environ 18\u00a0mois. Ces derniers sont \u00e9galement r\u00e9sistants \u00e0 un certain nombre de d\u00e9sinfectants courants. Ils peuvent toutefois \u00eatre d\u00e9truits par la s\u00e9cheresse \u00e9tant donn\u00e9 qu'ils ne peuvent survivre dans un environnement tr\u00e8s sec.</p>\n<h2 class=\"h5\">Comment diagnostique-t-on la cysticercose?</h2>\n<p>Le diagnostic de la cysticercose se fonde sur la d\u00e9tection des kystes dans les tissus musculaires des animaux pendant l'inspection de la carcasse. Les l\u00e9sions suspectes doivent \u00eatre envoy\u00e9es \u00e0 un laboratoire pour confirmer le diagnostic.</p>\n<h2 class=\"h5\">Comment traite-t-on la cysticercose?</h2>\n<p>Il n'existe aucun traitement pour cette maladie chez les animaux vivants.</p>\n<h2 class=\"h5\">Quels sont les r\u00f4les et les responsabilit\u00e9s \u00e9tablis pour pr\u00e9venir la cysticercose?</h2>\n<p>De la ferme \u00e0 l'assiette, chacun a un r\u00f4le \u00e0 jouer pour pr\u00e9venir la cysticercose. Au niveau de l'exploitation agricole, il est important d'\u00e9viter la contamination par des mati\u00e8res f\u00e9cales humaines dans les aires o\u00f9 il y a des aliments pour les animaux et les aires d'alimentation des animaux.</p>\n<p>La cysticercose est une maladie \u00e0 d\u00e9claration obligatoire aux termes de la <i>Loi sur la sant\u00e9 des animaux</i>. Cela signifie que tous les cas suspects doivent \u00eatre signal\u00e9s \u00e0 l'Agence canadienne d'inspection des aliments (ACIA) pour que des inspecteurs commencent imm\u00e9diatement une enqu\u00eate.</p>\n<p>Le programme national de lutte contre la cysticercose de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> est en place pour prot\u00e9ger la sant\u00e9 humaine par la d\u00e9tection des bovins et des porcs infect\u00e9s au moment de l'abattage. Conform\u00e9ment au <a href=\"/fra/1300125426052/1300125482318\">programme d'hygi\u00e8ne des viandes</a> de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> , l'inspection des carcasses doit \u00eatre faite dans les abattoirs agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral. Tous les abattoirs non agr\u00e9\u00e9s par le gouvernement f\u00e9d\u00e9ral doivent signaler les cas suspects de cysticercose \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> aux fins d'enqu\u00eate.</p>\n<p>Si un animal infect\u00e9 est d\u00e9tect\u00e9 au moment de l'abattage, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> , par l'entremise de son programme de lutte contre la cysticercose, m\u00e8ne une enqu\u00eate au niveau de l'exploitation agricole.</p>\n<p>Les carcasses infect\u00e9es sont \u00e9limin\u00e9es ou peuvent entrer dans la cha\u00eene alimentaire apr\u00e8s leur traitement.</p>\n<p>Comme toujours, \u00e0 la maison, la viande doit \u00eatre cuite \u00e0 une <a href=\"http://canadiensensante.gc.ca/eating-nutrition/healthy-eating-saine-alimentation/safety-salubrite/tips-conseils/cook-temperatures-cuisson-fra.php?_ga=1.207579795.1860291589.1438360751\">temp\u00e9rature interne s\u00e9curitaire</a> pour aider \u00e0 \u00e9liminer les risques de maladie d'origine alimentaire. La cuisson appropri\u00e9e de la viande inactive les larves qui pourraient \u00eatre pr\u00e9sentes.</p>\n<h2 class=\"h5\">Comment l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> interviendrait-elle en cas d'\u00e9closion de cysticercose?</h2>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> analyse tous les cas positifs et prend les mesures suivantes\u00a0:</p>\n<ul>\n<li>Enqu\u00eate aupr\u00e8s des exploitations agricoles pouvant \u00eatre \u00e0 l'origine de la maladie et de tous les endroits o\u00f9 les animaux ont pu s\u00e9journer.</li>\n<li>Les endroits o\u00f9 la source d'infection a \u00e9t\u00e9 d\u00e9tect\u00e9e seraient plac\u00e9s imm\u00e9diatement sous le contr\u00f4le de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> .</li>\n<li>Sous la surveillance de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> , les exploitants doivent prendre certaines mesures (<abbr title=\"par exemple\">p.\u00a0ex.</abbr> nettoyage et d\u00e9sinfection, enl\u00e8vement des aliments contamin\u00e9s, <abbr title=\"et cetera\">etc.</abbr>) pour \u00e9liminer la source d'infection.</li>\n<li>Les bovins et les porcs des exploitations infect\u00e9es sont d\u00e9plac\u00e9s conform\u00e9ment \u00e0 un permis de d\u00e9placement \u00e0 un abattoir inspect\u00e9 par le gouvernement f\u00e9d\u00e9ral pour y \u00eatre abattus lorsqu'ils atteignent leur poids de march\u00e9.</li>\n<li>Les carcasses gravement infect\u00e9es sont condamn\u00e9es et \u00e9limin\u00e9es de fa\u00e7on appropri\u00e9e, tandis que les carcasses qui ne sont pas gravement infect\u00e9es sont trait\u00e9es \u00e0 basse temp\u00e9rature (-10\u00a0\u00b0<abbr title=\"celsius\">C</abbr>, pendant 10\u00a0jours) ou \u00e0 haute temp\u00e9rature (au moins 60\u00a0\u00b0<abbr title=\"celsius\">C</abbr>), pour tuer le parasite. Les carcasses trait\u00e9es peuvent entrer dans la cha\u00eene d'alimentation humaine une fois le traitement (chaleur ou froid) termin\u00e9.</li>\n<li>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> garde le contr\u00f4le des lieux infect\u00e9s jusqu'\u00e0 ce que la source de l'infection soit \u00e9limin\u00e9e et que l'on confirme l'absence du parasite au sein du troupeau (v\u00e9rification \u00e0 l'abattoir).</li>\n</ul>\r\n<h2 class=\"font-medium black\">Renseignements additionnels</h2>\n\n<ul>\n<li><a href=\"/fra/1300462382369/1300462438912\">Bureaux de sant\u00e9 animale</a></li>\n</ul> \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}