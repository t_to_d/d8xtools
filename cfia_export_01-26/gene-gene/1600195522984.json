{
    "dcr_id": "1600195522984",
    "title": {
        "en": "VA Voxco SFCR PCP",
        "fr": "VA Voxco SFCR PCP"
    },
    "html_modified": "2024-01-26 2:25:13 PM",
    "modified": "2020-09-15",
    "issued": "2020-09-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/redirect_va_voxco_sfcr_pcp_1600195522984_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/redirect_va_voxco_sfcr_pcp_1600195522984_fra"
    },
    "ia_id": "1600195735653",
    "parent_ia_id": "1297965645317",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1297965645317",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "VA Voxco SFCR PCP",
        "fr": "VA Voxco SFCR PCP"
    },
    "label": {
        "en": "VA Voxco SFCR PCP",
        "fr": "VA Voxco SFCR PCP"
    },
    "templatetype": "content page 1 column",
    "node_id": "1600195735653",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1297964599443",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "redirected_uri": {
        "en": "https://na1se.voxco.com/SE/93/SFCR_PCP/?topic1=Do+the+SFCR+requirements+apply+to+your+business%3F&topic4=Does+your+business+require+a+preventive+control+plan%3F",
        "fr": "https://inspection.canada.ca/active/netapp/virtual-assistant/redirect/redirect-pcp.aspx"
    },
    "interwoven_url": {
        "en": "",
        "fr": ""
    },
    "meta": {
        "viewport": {
            "en": "width=device-width, initial-scale=1"
        },
        "og:title": {
            "en": "default"
        },
        "og:image": {
            "en": "http://"
        },
        "og:secure_url": {
            "en": "http://"
        },
        "twitter:image": {
            "en": "http://"
        },
        "twitter:card": {
            "en": "summary"
        }
    },
    "body": {
        "en": "<section><div data-variables=\"\" data-logic-qid=\"2796\" data-logic-variables=\"\" class=\"clear questionBackground\" data-enhance=\"false\" id=\"Q_FUNCTIONALITY_OF_THE_POP_UPS\">\r\n<div class=\"questionContainer description standard\" data-questiontype=\"description\">\r\n<div class=\"questionTextContainer\">\r\n<label class=\"questionText allQuestion\" id=\"questionText2796\"><p>Note: For optimal use of the functionalities of this tool, we recommend that you use Firefox or Google Chrome. If you are using Internet Explorer or another web browser, we recommend that you refer to the <span style=\"color:blue;\"><a href=\"http://inspection.gc.ca/about-the-cfia/acts-and-regulations/regulatory-initiatives/sfca/proposed-sfcr/tools-resources-and-guidance/draft-glossary-of-key-terms/eng/1430250286859/1430250287405\" target=\"_blank\">SFCR: Glossary of key terms - (Opens in a new window)</a></span> to view the applicable definitions.</p></label>\r\n</div><fieldset>\r\n<div class=\"questionContent\">\r\n<div style=\"clear:both;\">\r\n\r\n</div>\r\n</div>\r\n</fieldset>\r\n</div>\r\n</div></section><section><div data-variables=\"\" data-logic-qid=\"2797\" data-logic-variables=\"\" class=\"clear questionBackground\" data-enhance=\"false\" id=\"Q_INTRO_1\">\r\n<div class=\"questionContainer description standard\" data-questiontype=\"description\">\r\n<div class=\"questionTextContainer\">\r\n<label class=\"questionText allQuestion\" id=\"questionText2797\"><p>Some parts of your business may require a written Preventive Control Plan (PCP), while others may not. Find out if your business requires a written PCP by answering a few simple questions. It only takes about 5 minutes. The specific sections of the Safe Food for Canadians Regulations (SFCR) that outline the requirements are indicated where applicable.</p></label>\r\n</div><fieldset>\r\n<div class=\"questionContent\">\r\n<div style=\"clear:both;\">\r\n\r\n</div>\r\n</div>\r\n</fieldset>\r\n</div>\r\n</div></section><section><div data-variables=\"\" data-logic-qid=\"2798\" data-logic-variables=\"\" class=\"clear questionBackground\" data-enhance=\"false\" id=\"Q_INTRO_2\">\r\n<div class=\"questionContainer description standard\" data-questiontype=\"description\">\r\n<div class=\"questionTextContainer\">\r\n<label class=\"questionText allQuestion\" id=\"questionText2798\"><p><strong>What is a Preventive Control Plan (PCP)?</strong></p><p>A PCP is a written document that demonstrates how risks to <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Food and Drugs Act defines 'food' as meaning 'any article manufactured, sold or represented for use as food or drink for human beings, chewing gum, and any ingredient that may be mixed with food for any purpose whatever.'\">food</span> and <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Safe Food for Canadians Regulations define 'food animal' as meaning 'a bird or mammal, other than a marine mammal, from which an edible meat product may be derived.'\">food animals</span> are identified and controlled.</p><p>Even if you do not need a PCP, you may still need to comply with the <strong>preventive control</strong> requirements in Part 4 of the SFCR. Refer to the <a href=\"http://inspection.gc.ca/food/sfcr/general-requirements-and-guidance/preventive-controls-food-businesses/eng/1526472289805/1526472290070\" rel=\"noopener noreferrer\" target=\"_blank\">Preventive controls for food businesses</a> to help you determine if and how the preventive control requirements apply to your food business.</p></label>\r\n</div><fieldset>\r\n<div class=\"questionContent\">\r\n<div style=\"clear:both;\">\r\n\r\n</div>\r\n</div>\r\n</fieldset>\r\n</div>\r\n</div></section><section><div data-variables=\"\" data-logic-qid=\"2799\" data-logic-variables=\"\" class=\"clear questionBackground\" data-enhance=\"false\" id=\"Q_GENERAL_EXEMPTIONS\">\r\n<div class=\"questionContainer description standard\" data-questiontype=\"description\">\r\n<div class=\"questionTextContainer\">\r\n<label class=\"questionText allQuestion\" id=\"questionText2799\"><p>General exemptions from the Safe Food for Canadians Regulations are:</p><ul><li><span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Food and Drugs Act defines 'food' as meaning 'any article manufactured, sold or represented for use as food or drink for human beings, chewing gum, and any ingredient that may be mixed with food for any purpose whatever.'\">food</span> for personal use, when the food is not intended for commercial use, and<ul><li>the quantity of food is equal to or under the maximum quantity limits, found in the document \"<a href=\"http://inspection.gc.ca/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/personal-use-exemption/eng/1520439688578/1520439689098\" rel=\"noopener noreferrer\" target=\"_blank\">Maximum Quantity Limits for Personal Use Exemption - (Opens in a new window)</a>,\" and</li><li>the food is <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The term 'import' is not specifically defined in the Safe Food for Canadians Act nor in the Safe Food for Canadians Regulations. In general terms, 'import' refers to bringing food into Canada from a foreign state.\">imported</span>, <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The term 'export' is not specifically defined in the Safe Food for Canadians Act nor in the Safe Food for Canadians Regulations. In general terms, 'export' refers to sending food from Canada to a foreign state.\">exported</span>, <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The phrase 'send or convey from one province or territory to another' is not specifically defined in the Safe Food for Canadians Act nor in the Safe Food for Canadians Regulations. In general terms, this phrase refers to the trade of food from one province or territory to another, and is often referred to as inter-provincial trade.\">sent or conveyed from one province to another</span> by an individual other than in the course of business; or</li><li>the food is imported or exported as part of the personal effects of an immigrant or emigrant</li></ul></li><li>food that is carried on any <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Safe Food for Canadians Act defines 'conveyance' as meaning 'a vessel, aircraft, train, motor vehicle, trailer or other means of transportation, including a cargo container.'\">conveyance</span> that is intended for the crew or passengers</li><li>food that is intended and used for analysis, evaluation, research, or a trade show provided that the food is part of a shipment that weighs 100 kg or less or, in the case of <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Safe Food for Canadians Regulations define 'egg' as meaning 'an egg of a domestic chicken of the species Gallus domesticus or, in respect of a processed egg product, means that egg or an egg of a domestic turkey of the species Meleagris gallopavo. It does not include a balut.'\">eggs</span>, is part of a shipment of five or fewer cases</li><li>food that is not intended or <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Safe Food for Canadians Act defines 'sell' as meaning 'includes agree to sell, offer for sale, expose for sale or have in possession for sale \u2013 or distribute to one or more persons whether or not the distribution is made for consideration.'\">sold</span> for human consumption</li><li>food that is imported from the United States onto the Akwesasne Reserve by a permanent resident of the Reserve for their use</li><li>food that is imported in bond (in transit) for use by crew or passengers of a cruise ship or military ship in Canada</li><li>food that is traded between federal penitentiaries</li><li>transporting a <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"The Safe Food for Canadians Act defines 'food commodity' as meaning 'any food as defined in section 2 of the Food and Drugs Act; any animal or plant, or any of its parts, from which food may be derived; or anything prescribed to be a food commodity.'\">food commodity</span>, if that is the sole activity of a <span class=\"tool\" rel=\"tooltip\" style=\"cursor: help; color:#0000FF;\" title=\"'Person' has the same meaning as in the Criminal Code. A person can be an individual, including an employee or contractor or an organization, including an association, company or corporation.\">person</span></li></ul><p>[SFCR: 20(1); 21; 23(1) (a)-(c), (d)(i) and (e)]</p></label>\r\n</div><fieldset>\r\n<div class=\"questionContent\">\r\n<div style=\"clear:both;\">\r\n\r\n</div>\r\n</div>\r\n</fieldset>\r\n</div>\r\n</div></section><div class=\"sys\">\r\n</div>\r\n<div class=\"clear\"></div>\r\n",
        "fr": ""
    },
    "js": {
        "en": ""
    },
    "success": false,
    "chat_wizard": false
}