{
    "dcr_id": "1473681958406",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> (Yellow rocket)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Barbarea</i> <abbr title=\"esp\u00e8ces\">spp.</abbr> (Barbar\u00e9e vulgaire)"
    },
    "html_modified": "2024-01-26 2:23:23 PM",
    "modified": "2017-11-06",
    "issued": "2017-11-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_barbarea_spp_1473681958406_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_barbarea_spp_1473681958406_fra"
    },
    "ia_id": "1473681958935",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> (Yellow rocket)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Barbarea</i> <abbr title=\"esp\u00e8ces\">spp.</abbr> (Barbar\u00e9e vulgaire)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> (Yellow rocket)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Barbarea</i> <abbr title=\"esp\u00e8ces\">spp.</abbr> (Barbar\u00e9e vulgaire)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1473681958935",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/barbarea-spp-/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/barbarea-spp-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Barbarea spp. (Yellow rocket)",
            "fr": "Semence de mauvaises herbe : Barbarea spp. (Barbar\u00e9e vulgaire)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Barbarea spp., Brassicaceae, Yellow rocket, Winter cress",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Barbarea spp., Brassicaceae, Barbar\u00e9e vulgare ou cresson de terre"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-06",
            "fr": "2017-11-06"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Barbarea spp. (Yellow rocket)",
        "fr": "Semence de mauvaises herbe : Barbarea spp. (Barbar\u00e9e vulgaire)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Brassicaceae</i></p>\n\n<h2>Synonym</h2>\n<p>Winter cress</p>\n\n<h2>Common Name</h2>\n<p>Yellow rocket</p>\n\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 3 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> The genus includes 4 species found in Canada, distributed across all provinces and territories (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). The native species <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> orthoceras</i> occurs across Canada except <abbr title=\"Nova Scotia\">NS</abbr>, while the introduced species occur as follows: <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> stricta</i> in <abbr title=\"Ontario\">ON</abbr> and <abbr title=\"Quebec\">QC</abbr>; <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> verna</i> in <abbr title=\"British Columbia\">BC</abbr> and <abbr title=\"Newfoundland and Labrador\">NF</abbr>, and; <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> vulgaris</i> in all provinces but not the territories.</p>\n<p><strong>Worldwide:</strong>The genus contains about 20 species with the majority distributed in the   temperate regions of Eurasia and North America, and a few species in   subtropical regions of East Asia and Australia (Agerbirk et al. 2003<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). The type species, <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> vulgaris</i>, has a wide native distribution area in Eurasia, and is introduced as a noxious weed in North America, Africa, Australia and New Zealand (MacDonald and Cavers 1991<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, Agerbirk et al. 2003<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Biennial to short-lived perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seeds of <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> are medium-sized within the <i lang=\"la\">Brassicaceae</i>; similar size as <i lang=\"la\">Brassica</i> species</li>\n<li><i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> seed length varies from 1.3 - 2.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li><i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> seeds are oval or oblong; strongly compressed</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li><i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> seed surface is reticulate with deep cells; obscured by shining, yellowish film</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li><i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> seeds surface black; appears grey-brown due to obscuring film</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hilum of <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> is in a notch at one end of the seed</li>\n<li>A furrow extends part of the way along the seed, separating cotyledons from the radicle</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, gardens, pastures, meadows, old fields, shores and swamps, roadsides and disturbed areas  (Darbyshire 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> vulgaris</i> is an important weed of small-seeded grain and hay crops in Canada, legume-grass meadows in the U.S., and vegetable and fruit crops in Europe (MacDonald and Cavers 1991<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Yellow rocket produces abundant seed early in the growing season and behaves as an early spring dominant. Seeds may be spread as contaminants in seed lots of timothy and other similar sized grains; they may also be spread in hay and manure, and have been shown to remain viable after passing through a variety of animals.</p>\n<p>Individual plants may produce up to 88,000 seeds per plant, and seeds may remain dormant in the soil for 10-20 years. It can also reproduce vegetatively from the roots and rosettes (MacDonald and Cavers 1991<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). </p>\n\n\n<h2>Similar species</h2>\n<h3>Distinguishing <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> from other <i lang=\"la\">Brassicaceae</i></h3>\n\n<p>Seeds of <i lang=\"la\">Barbarea</i> <abbr title=\"species\">spp.</abbr> can be distinguished from <i lang=\"la\">Brassicaceae</i> genera with a similar oblong shape, furrow and surface reticulations such as: <i lang=\"la\">Descurainia</i> <abbr title=\"species\">spp.</abbr> and <i lang=\"la\">Sisymbrium</i> <abbr title=\"species\">spp.</abbr> by:</p>\n<ul>\n<li>Dark grey-brown colour</li>\n<li>Surface reticulation with deep cells</li>\n<li>Shining, yellowish film on the surface</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_verna_03cnsh_1475589153617_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Early yellow rocket (winter cress)  (<i lang=\"la\">Barbarea verna</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_verna_01cnsh_1475589126999_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Early yellow rocket (winter cress)  (<i lang=\"la\">Barbarea verna</i>) seed\n</figcaption>\n</figure>\n\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_03cnsh_1475589254438_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Early yellow rocket (winter cress)  (<i lang=\"la\">Barbarea vulgaris</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_01cnsh_1475589179499_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Early yellow rocket (winter cress)  (<i lang=\"la\">Barbarea vulgaris</i>) seed\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_02cnsh_1475589212551_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Early yellow rocket (winter cress)  (<i lang=\"la\">Barbarea vulgaris</i>) seed, side view\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_copyright_1475589300306_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Early yellow rocket (winter cress)  (<i lang=\"la\">Barbarea vulgaris</i>) seed\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong>Agerbirk, N., \u00d8rgaard, M. and Nielsen, J. K. 2003</strong>. Glucosinolates, flea beetle resistance, and leaf pubescence as taxonomic characters in the genus <i lang=\"la\">Barbarea</i> (<i lang=\"la\">Brassicaceae</i>). Phytochemistry 63 (1): 69-80.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>MacDonald, M. A. and Cavers, P. B. 1991</strong>. The biology of Canadian weeds. 97. <i lang=\"la\">Barbarea vulgaris</i> R. Br. Canadian Journal of Plant Science 71: 149-166.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Brassicaceae</i></p>\n\n<h2>Synonyme</h2>\n<p>Cresson de terre</p>\n\n<h2>Nom commun</h2>\n<p>Barbar\u00e9e vulgaire</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie 3 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Le genre <i lang=\"la\">Barbarea</i> compte quatre esp\u00e8ces au Canada qui sont r\u00e9parties dans l'ensemble des provinces et des territoires (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>). L'esp\u00e8ce indig\u00e8ne <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr>\u00a0orthoceras</i> est pr\u00e9sente partout au Canada, sauf en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr>, alors que les esp\u00e8ces introduites suivantes sont ainsi r\u00e9parties\u00a0: <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr>\u00a0stricta</i> est pr\u00e9sente en <abbr title=\"Ontario\">Ont.</abbr> et au <abbr title=\"Qu\u00e9bec\">Qc</abbr>; <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr>\u00a0verna</i> est pr\u00e9sente en <abbr title=\"Colombie-Britannique\">C.-B.</abbr> et \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr>; <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr>\u00a0vulgaris</i> est pr\u00e9sente dans toutes les provinces, mais elle est absente des territoires.</p>\n<p><strong>R\u00e9partition mondiale :</strong> Le genre compte environ 20\u00a0esp\u00e8ces, et la majorit\u00e9 est r\u00e9partie dans les r\u00e9gions temp\u00e9r\u00e9es d'Eurasie et d'Am\u00e9rique du Nord, et quelques esp\u00e8ces sont pr\u00e9sentes dans les r\u00e9gions subtropicales de l'Asie orientale et de l'Australie (Agerbirk et al., 2003<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>). L'esp\u00e8ce type, <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> vulgaris</i>, a une aire d'indig\u00e9nat \u00e9tendue en Eurasie, et a \u00e9t\u00e9 introduite comme mauvaise herbe nuisible en Am\u00e9rique du Nord, en Afrique, en Australie et en Nouvelle-Z\u00e9lande (MacDonald et Cavers, 1991<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>, Agerbirk et al., 2003<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Bisannuelle ou vivace \u00e0 vie courte</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n\n<ul class=\"mrgn-lft-lg\">\n<li>Les graines des <i lang=\"la\">Barbarea</i>\u00a0<abbr title=\"esp\u00e8ce\">spp.</abbr> sont de taille moyenne comparativement aux autres <i lang=\"la\">Brassicaceae</i>, et de taille semblable \u00e0 celle des <i lang=\"la\">Brassica\u00a0</i><abbr title=\"esp\u00e8ce\">spp.</abbr></li>\n<li>La longueur des graines des <i lang=\"la\">Barbarea</i>\u00a0<abbr title=\"esp\u00e8ce\">spp.</abbr> varie de 1,3 \u00e0\u00a02,0\u00a0<abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Les graines des <i lang=\"la\">Barbarea</i>\u00a0<abbr title=\"esp\u00e8ce\">spp.</abbr> ont un contour elliptique ou oblong; elles sont fortement comprim\u00e9es</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>La surface des graines des <i lang=\"la\">Barbarea</i>\u00a0<abbr title=\"esp\u00e8ce\">spp.</abbr> est r\u00e9ticul\u00e9e avec de profondes cellules; elle est obscurcie par un film cireux jaun\u00e2tre</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>La surface des graines des <i lang=\"la\">Barbarea</i>\u00a0<abbr title=\"esp\u00e8ce\">spp.</abbr> est noire, mais semble brun-gris en raison du film obscurcissant</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Le hile des graines des <i lang=\"la\">Barbarea</i>\u00a0<abbr title=\"esp\u00e8ce\">spp.</abbr> est situ\u00e9 dans une encoche \u00e0 l'une des extr\u00e9mit\u00e9s de la graine</li>\n<li>Un sillon s'\u00e9tend sur une partie de la longueur de la graine, s\u00e9parant les cotyl\u00e9dons de la radicule</li>\n</ul>\n\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, jardins, p\u00e2turages, pr\u00e9s, champs abandonn\u00e9s, rivages, mar\u00e9cages, bords de chemin et terrains perturb\u00e9s (Darbyshire, 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>). <i lang=\"la\"><abbr title=\"Barbarea\">B.</abbr> vulgaris</i> est une importante adventice des c\u00e9r\u00e9ales \u00e0 petits grains et des plantes fourrag\u00e8res au Canada, des pr\u00e9s de l\u00e9gumineuses-gramin\u00e9es aux \u00c9tats-Unis et des cultures l\u00e9gumi\u00e8res et fruiti\u00e8res en Europe (MacDonald et Cavers, 1991<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La barbar\u00e9e vulgaire produit une abondance de graines t\u00f4t en saison et se comporte comme une plante adventice dominante au d\u00e9but du printemps. Les graines peuvent \u00eatre diss\u00e9min\u00e9es par de la semence contamin\u00e9e de fl\u00e9ole et d'autres esp\u00e8ces de dimensions semblables, elles peuvent aussi \u00eatre propag\u00e9es dans le foin et le fumier; les graines de barbar\u00e9es peuvent demeurer viables m\u00eame apr\u00e8s avoir travers\u00e9 le syst\u00e8me digestif de diverses esp\u00e8ces animales.</p>\n<p>Chaque plante peut produire jusqu'\u00e0 88\u00a0000 graines, et les graines peuvent demeurer dormantes dans le sol de 10 \u00e0 20 ans. En outre, la barbar\u00e9e peut se reproduire par voie v\u00e9g\u00e9tative \u00e0 partir des racines et de rosettes (MacDonald et Cavers, 1991<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Caract\u00e8res distinctifs des barbar\u00e9es par rapport aux autres crucif\u00e8res</h3>\n<p>On peut distinguer les graines des <i lang=\"la\">Barbarea</i> <abbr title=\"esp\u00e8ce\">spp.</abbr> des autres genres de <i lang=\"la\">Brassicaceae</i> qui pr\u00e9sentent aussi un contour oblong, un sillon et des r\u00e9ticulations superficielles semblables, comme <i lang=\"la\">Descurainia</i> <abbr title=\"esp\u00e8ce\">spp.</abbr> et <i lang=\"la\">Sisymbrium</i> <abbr title=\"esp\u00e8ce\">spp.</abbr> par\u00a0:</p>\n<ul>\n<li>leur couleur brun-gris fonc\u00e9</li>\n<li>leur surface r\u00e9ticul\u00e9e avec de profondes cellules</li>\n<li>la pr\u00e9sence d\u2019un film jaun\u00e2tre cireux \u00e0 la surface</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_verna_03cnsh_1475589153617_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Barbar\u00e9e vulgaire (cresson de terre) (<i lang=\"la\">Barbarea verna</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_verna_01cnsh_1475589126999_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Barbar\u00e9e vulgaire (cresson de terre) (<i lang=\"la\">Barbarea verna</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_03cnsh_1475589254438_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Barbar\u00e9e vulgaire (cresson de terre) (<i lang=\"la\">Barbarea vulgaris</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_01cnsh_1475589179499_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Barbar\u00e9e vulgaire (cresson de terre) (<i lang=\"la\">Barbarea vulgaris</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_02cnsh_1475589212551_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Barbar\u00e9e vulgaire (cresson de terre) (<i lang=\"la\">Barbarea vulgaris</i>) graine, vue lat\u00e9rale\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_barbarea_vulgaris_copyright_1475589300306_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Barbar\u00e9e vulgaire (cresson de terre) (<i lang=\"la\">Barbarea vulgaris</i>) graine\n</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>Agerbirk, N., \u00d8rgaard, M. and Nielsen, J. K. 2003</strong>. Glucosinolates, flea beetle resistance, and leaf pubescence as taxonomic characters in the genus <i lang=\"la\">Barbarea</i> (<i lang=\"la\">Brassicaceae</i>). Phytochemistry 63 (1): 69-80.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>MacDonald, M. A. and Cavers, P. B. 1991</strong>. The biology of Canadian weeds. 97. <i lang=\"la\">Barbarea vulgaris</i> R. Br. Canadian Journal of Plant Science 71: 149-166.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}