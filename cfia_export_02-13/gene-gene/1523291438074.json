{
    "dcr_id": "1523291438074",
    "title": {
        "en": "Biosecurity Measures for Deadstock Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux"
    },
    "html_modified": "2024-02-13 9:52:55 AM",
    "modified": "2018-05-07",
    "issued": "2018-05-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/biosec_measures_deadstock_1523291438074_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/biosec_measures_deadstock_1523291438074_fra"
    },
    "ia_id": "1523302054670",
    "parent_ia_id": "1519235183948",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1519235183948",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biosecurity Measures for Deadstock Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux"
    },
    "label": {
        "en": "Biosecurity Measures for Deadstock Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1523302054670",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1519235033628",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/standards-and-principles/transportation/deadstock/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/transport/carcasses-d-animaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biosecurity Measures for Deadstock Transportation",
            "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux"
        },
        "description": {
            "en": "Deadstock transporters have a crucial role in preventing or reducing the spread of diseases during collection and transportation. Diseases in livestock and poultry can lead to animal health, human health, and economic consequences. This fact sheet highlights key biosecurity measures to contain deadstock and to prevent transmission or spread of diseases during transportation.",
            "fr": "Les transporteurs jouent un r\u00f4le crucial dans la pr\u00e9vention et la r\u00e9duction de la propagation de maladies au cours du transport. La pr\u00e9sence de maladies chez les animaux d'\u00e9levage et la volaille peut avoir des cons\u00e9quences sur la sant\u00e9 des animaux, sur la sant\u00e9 humaine et sur l'\u00e9conomie. Le pr\u00e9sent d\u00e9pliant d\u00e9crit les principales mesures de bios\u00e9curit\u00e9 \u00e0 adopter pour contenir les carcasses d'animaux et emp\u00eacher la transmission ou la propagation de maladies durant le transport."
        },
        "keywords": {
            "en": "biosecurity, animal health, diseases, standards, principles, measures, deadstock, transportation",
            "fr": "bios\u00e9curit\u00e9, sant\u00e9 des animaux, maladies, normes, principes, mesures, carcasses d'animaux, transport"
        },
        "dcterms.subject": {
            "en": "livestock,imports,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-05-07",
            "fr": "2018-05-07"
        },
        "modified": {
            "en": "2018-05-07",
            "fr": "2018-05-07"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biosecurity Measures for Deadstock Transportation",
        "fr": "Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"row\">\n<section class=\"mwsdownload-link section col-xs-12 col-sm-12 col-md-12 col-lg-10\">\n<div class=\"well well-sm gc-dwnld\">\n\n<div class=\"row\">\n\n<div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\">\n\n<p><a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_deadstock_1525476448120_eng.pdf\">\n<img class=\"img-responsive mrgn-lft-sm mrgn-tp-sm gc-dwnld-img\" src=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/images-images/bt-thumbnails-deadstock-trans_1525476194714_eng.jpg\" alt=\"PDF thumbnail: Biosecurity Measures for Deadstock Transportation\"></a></p>\n</div>\n\n<div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_deadstock_1525476448120_eng.pdf\">\n<span>Biosecurity Measures for Deadstock Transportation</span><br>\n<span class=\"gc-dwnld-info\"><abbr title=\"Portable Document Format\">PDF</abbr> (162\u00a0<abbr title=\"kilobytes\">KB</abbr>)</span></a>\n</p>\n\n</div>\n\n</div>\n\n</div>\n</section>\n</div>\n\n<p>Deadstock transporters have a crucial role in preventing or reducing the spread of diseases during collection and transportation. Diseases in livestock and poultry can lead to animal health, human health, and economic consequences. This fact sheet highlights key biosecurity measures to contain deadstock and to prevent transmission or spread of diseases during transportation.</p>\n\n<h2>What is biosecurity?</h2>\n\n<p>Biosecurity is a set of measures used to reduce the chance of the introduction and spread of disease-causing organism and pests. Animals can remain infectious long after they have died. Deadstock, their bodily fluids, and secretions can transmit and spread infection to live animals, and it is recommended that they be contained, transported, and disposed of securely.</p>\n\n<h2>Why is biosecurity important?</h2>\n\n<p>Deadstock transporters usually collect deadstock from several locations in a single transportation event (for example: multiple farms, slaughter operations, salvaging facility, or other approved provincial or municipal collection sites). These transportation events provide an opportunity to spread infectious diseases between different locations and may cause significant financial losses to the producers, industry and government.</p>\n\n<h2>Who is responsible for biosecurity?</h2>\n\n<p>Owners, caretakers, transporters and everyone else involved in the handling and transportation of deadstock are responsible for mitigating biosecurity risks. It is recommended that biosecurity measures be implemented at all times during loading, on the road and during unloading. Always discuss biosecurity with the deadstock owners or collection sites before transportation.</p>\n\n<h2>Biosecurity Recommendations: Deadstock Collection Sites</h2>\n\n<p><strong>Consider all deadstock collection sites as potential sources of infection.</strong></p>\n\n<ul>\n<li>Plan your transportation event. Acquire information if the animal may have died from an infectious disease. Modify the route or collection procedure to protect other animal production sites.</li>\n<li>Determine and follow biosecurity protocols for the collection sites. Keep deadstock collection vehicles as far away as possible from other livestock on the premises.</li>\n<li>When accessing sites, avoid muddy or manure contaminated pathways. Access pathways used to collect deadstock should be separate from those used by farm workers.</li>\n<li>Follow federal, provincial, and municipal requirements for deadstock removal.</li>\n<li>Remove deadstock as soon as possible, before the carcasses reach an advanced state of decomposition.</li>\n<li>Educate and inform owners of deadstock regarding steps to reduce biosecurity risks associated with deadstock removal and pick-up. Provide the fact sheet containing biosecurity recommendations for on-farm management of deadstock. This is available on the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> website at Inspection.gc.ca and Canadian Animal Health Coalition website at Animalhealth.ca.</li>\n</ul>\n\n<h2>Biosecurity Recommendations: Deadstock Transporters</h2>\n\n<h3>Transport unit and collection container design</h3>\n\n<ul>\n<li>Power unit, trailers, containers and equipment are constructed of materials and designed in a manner that allows repeated cleaning and disinfection.</li>\n<li>Containers and trailers are leak-proof and spill-proof.</li>\n<li>Trailers and containers are closed or covered to prevent access by scavengers.</li>\n</ul>\n\n<h3>Use clean and disinfected trailers</h3>\n\n<p>Dirty trailers can spread disease. To reduce this risk:</p>\n\n<ul>\n<li>Clean and disinfect trailers between transportation events. If possible, clean and disinfect at the destination site.</li>\n<li>Use effective and compatible cleaning (detergents, degreasers) and disinfection products; consult specialists as needed. Follow manufacturer\u2019s recommended water temperature, concentration and contact time. Ensure that trailer surfaces are visually clean and free of all organic matter before applying a disinfectant.</li>\n<li>If there is contamination to the exterior of the transport unit while loading or transporting deadstock, it is recommended that the deadstock transporter move the transport unit away from the production site or any pathway leading to the production site and clean and disinfect the affected part of the transport unit.</li>\n</ul>\n\n<h3>Minimize stops on the road</h3>\n\n<p>It is recommended that the stops be minimized and avoided when possible due to the high risk associated with transporting deadstock and rendering materials.</p>\n\n<ul>\n<li>If it is necessary to stop, park as far away as possible from live animal transport units.</li>\n<li>Whenever possible, avoid parking on gravel or loose surfaces. Park on a hard surface that can be decontaminated if leakage of fluids occurs.</li>\n<li>Maintain spill kit and contact information for emergencies during the transportation event.</li>\n</ul>\n\n<h3>People and equipment</h3>\n\n<p>People and equipment that have come into contact with deadstock or its environment can pose a risk of spreading disease. To minimize this risk:</p>\n\n<ul>\n<li>Use designated equipment (<abbr title=\"for example\">e.g.</abbr> shovels, chains, bins) for the transportation event. Clean and disinfect equipment after use.</li>\n<li>If the transporter is involved in handling of deadstock, it is recommended that they wear disposable personal protective clothing, gloves, and boots.</li>\n\n<li>Always clean and sanitize hands before and after contact with the deadstock, live animals or equipment.</li>\n<li>Discourage people not involved with the transportation event from coming into contact with the shipment, equipment, and trailer.</li>\n<li>Keep the truck cab clean to prevent cross-contamination between transportation events. For example, remove dirty coveralls and footwear before entering the cab.</li>\n</ul>\n\n<p>For more information, visit the <a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/transportation/eng/1519235033628/1519235183948\">biosecurity standard for livestock, poultry and deadstock transportation</a> on the <a href=\"/eng/1297964599443/1297965645317\">Canadian Food Inspection Agency website</a> at Inspection.gc.ca and <a href=\"http://www.animalhealth.ca/aspx/public/\">Canadian Animal Health Coalition</a> website at Animalhealth.ca.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n\n<section class=\"mwsdownload-link section col-xs-12 col-sm-12 col-md-12 col-lg-10\">\n<div class=\"well well-sm gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-2 col-sm-2 col-md-2 col-lg-2\">\n\n<p>\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_deadstock_1525476448120_fra.pdf\">\n<img class=\"img-responsive mrgn-lft-sm mrgn-tp-sm gc-dwnld-img\" src=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/images-images/bt-thumbnails-deadstock-trans_1525476194714_fra.jpg\" alt=\"Image de PDF : Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux\"></a></p>\n</div>\n\n<div class=\"col-xs-10 col-sm-10 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/biosec_measures_deadstock_1525476448120_fra.pdf\">\n<span>Mesures de bios\u00e9curit\u00e9 pour le transport de carcasses d'animaux</span><br>\n<span class=\"gc-dwnld-info\"><abbr title=\"Format de document portable\">PDF</abbr> (167\u00a0<abbr title=\"kilo-octets\">ko</abbr>)</span></a>\n</p>\n\n</div>\n</div>\n</div>\n\n</section>\n</div>\n\n\n<p>Les transporteurs jouent un r\u00f4le crucial dans la pr\u00e9vention et la r\u00e9duction de la propagation de maladies au cours du transport. La pr\u00e9sence de maladies chez les animaux d'\u00e9levage et la volaille peut avoir des cons\u00e9quences sur la sant\u00e9 des animaux, sur la sant\u00e9 humaine et sur l'\u00e9conomie. Le pr\u00e9sent fiche d'information d\u00e9crit les principales mesures de bios\u00e9curit\u00e9 \u00e0 adopter pour contenir les carcasses d'animaux et emp\u00eacher la transmission ou la propagation de maladies durant le transport.</p>\n\n<h2>Qu'est-ce que la bios\u00e9curit\u00e9?</h2>\n\n<p>La bios\u00e9curit\u00e9 est un ensemble de mesures utilis\u00e9es afin de r\u00e9duire les possibilit\u00e9s d'introduction et de propagation d'organismes infectieux et nuisibles. Les animaux peuvent demeurer infectieux longtemps apr\u00e8s \u00eatre d\u00e9c\u00e9d\u00e9s. Les carcasses d'animaux, leurs liquides corporels et leurs s\u00e9cr\u00e9tions peuvent transmettre et propager des infections \u00e0 des animaux vivants, et il est recommand\u00e9 qu'ils soient contenus, transport\u00e9s et \u00e9limin\u00e9s de fa\u00e7on s\u00e9curitaire.</p>\n\n<h2>Pourquoi la bios\u00e9curit\u00e9 est-elle importante?</h2>\n\n<p>Les transporteurs de carcasses d'animaux recueillent normalement des carcasses \u00e0 plusieurs endroits au cours d'une m\u00eame activit\u00e9 de transport (<abbr title=\"par exemple\">p.\u00a0ex.</abbr>, plusieurs fermes, abattoirs, installations de r\u00e9cup\u00e9ration ou autres sites de collecte autoris\u00e9s par les provinces ou les municipalit\u00e9s). Ces activit\u00e9s de transport permettent la propagation de maladies infectieuses entre diff\u00e9rents lieux et peuvent entrainer des pertes financi\u00e8res importantes aux producteurs, \u00e0 l'industrie et aux gouvernements.</p>\n\n<h2>Qui est responsable de la bios\u00e9curit\u00e9?</h2>\n\n<p>Les producteurs, les pr\u00e9pos\u00e9s aux soins des animaux, les transporteurs et tous ceux qui jouent un r\u00f4le dans la manipulation et le transport des carcasses d'animaux ont la responsabilit\u00e9 d'att\u00e9nuer les risques du point de vue de la bios\u00e9curit\u00e9. Il est recommand\u00e9 que des mesures de bios\u00e9curit\u00e9 rigoureuses soient appliqu\u00e9es en tout temps au cours du chargement, sur la route et au cours du d\u00e9chargement. Discutez toujours des mesures de bios\u00e9curit\u00e9 avec les propri\u00e9taires des carcasses d'animaux ou les responsables des sites de collecte avant le transport.</p>\n\n<h2>Recommandations en mati\u00e8re de bios\u00e9curit\u00e9\u00a0: sites de collecte de carcasses d'animaux</h2>\n\n<p><strong>Consid\u00e9rez tous les sites de collecte de carcasses d'animaux comme \u00e9tant des sources potentielles d'infection</strong></p>\n\n<ul>\n<li>Planifiez votre activit\u00e9 de transport. Renseignez-vous pour savoir si l'animal est d\u00e9c\u00e9d\u00e9 en raison d'une maladie infectieuse. Modifiez l'itin\u00e9raire ou la proc\u00e9dure de collecte afin de prot\u00e9ger les autres sites de production animale.</li>\n<li>D\u00e9terminez et respectez les protocoles de bios\u00e9curit\u00e9 des sites de collecte. Gardez le v\u00e9hicule de collecte de carcasses d'animaux le plus loin possible des autres animaux d'\u00e9levage se trouvant sur les lieux.</li>\n<li>Lorsque vous acc\u00e9dez \u00e0 un site, \u00e9vitez les voies d'acc\u00e8s boueuses ou contamin\u00e9es par du fumier. Les voies d'acc\u00e8s emprunt\u00e9es pour la collecte de carcasses d'animaux devraient \u00eatre distinctes de celles utilis\u00e9es par le personnel.</li>\n<li>Respectez les exigences f\u00e9d\u00e9rales, provinciales et municipales concernant l'\u00e9limination de carcasses d'animaux.</li>\n<li>\u00c9liminez les carcasses aussi vite que possible, avant qu'elles n'atteignent un \u00e9tat de d\u00e9composition avanc\u00e9.</li>\n<li>Sensibilisez et informez les propri\u00e9taires des  carcasses d'animaux sur les mesures qu'ils peuvent prendre pour att\u00e9nuer les  risques pour la bios\u00e9curit\u00e9 associ\u00e9s \u00e0 la collecte et \u00e0 l'\u00e9limination de  carcasses. Remettez aux propri\u00e9taires le d\u00e9pliant contenant des recommandations  en mati\u00e8re de bios\u00e9curit\u00e9 pour la gestion des carcasses d'animaux \u00e0 la ferme. Ce fiche d'information se trouve sur les sites Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> (Inspection.gc.ca) et sur  celui de la Coalition canadienne pour la sant\u00e9 des animaux \u00e0 l'adresse Animalhealth.ca.</li>\n</ul>\n\n<h2>Recommandations en mati\u00e8re de bios\u00e9curit\u00e9\u00a0: transporteurs de carcasses</h2>\n\n<h3>Conception de l'unit\u00e9 de transport et du conteneur de collecte</h3>\n\n<ul>\n<li>L'unit\u00e9 motrice, les conteneurs et l'\u00e9quipement sont con\u00e7us et construits \u00e0 partir de mat\u00e9riaux r\u00e9sistants de mani\u00e8re \u00e0 permettre des nettoyages et des d\u00e9sinfections r\u00e9p\u00e9t\u00e9s.</li>\n<li>Les conteneurs et les remorques sont \u00e9tanches et \u00e0 l'\u00e9preuve des fuites et d\u00e9versements.</li>\n<li>Les remorques et les conteneurs sont ferm\u00e9s ou couverts afin d'y emp\u00eacher l'acc\u00e8s aux charognards.</li>\n</ul>\n\n<h3>Utilisez des remorques propres et d\u00e9sinfect\u00e9es</h3>\n\n<p>Des remorques sales peuvent propager des maladies. Pour att\u00e9nuer ces risques\u00a0:</p>\n\n<ul>\n<li>Nettoyez et d\u00e9sinfectez les remorques apr\u00e8s chaque activit\u00e9 de transport. Dans la mesure du possible, nettoyez et d\u00e9sinfectez les remorques au site de destination.</li>\n<li>Utilisez des produits nettoyants (d\u00e9tergents et d\u00e9graissants) et d\u00e9sinfectants efficaces et compatibles; consultez des sp\u00e9cialistes au besoin. Suivez les recommandations du fabricant concernant la temp\u00e9rature de l'eau, la concentration du produit et le temps de contact. Veillez \u00e0 ce que les surfaces de la remorque soient visuellement propres et exemptes de mati\u00e8res organiques avant l'application d'un d\u00e9sinfectant.</li>\n<li>S'il y a contamination de l'ext\u00e9rieur de l'unit\u00e9 de transport au cours du chargement ou du transport de carcasses d'animaux, il est recommand\u00e9 que le transporteur de carcasses \u00e9loigne l'unit\u00e9 de transport du site de production ou de toute voie menant au site de production, puis qu'il nettoie et d\u00e9sinfecte la partie touch\u00e9e de l'unit\u00e9 de transport.</li>\n</ul>\n\n<h3>Minimisez les arr\u00eats sur la route</h3>\n\n<p>Il est recommand\u00e9 de faire le moins d'arr\u00eats possible, voire aucun, en raison des risques \u00e9lev\u00e9s associ\u00e9s au transport de carcasses d'animaux et de mat\u00e9riel d'\u00e9quarrissage.</p>\n\n<ul>\n<li>Si un arr\u00eat est n\u00e9cessaire, stationnez-vous le plus loin possible des unit\u00e9s de transport d'animaux vivants.</li>\n<li>Dans la mesure du possible, \u00e9vitez de vous stationner sur le gravier ou sur des surfaces meubles. Stationnez-vous sur des surfaces solides qui peuvent \u00eatre d\u00e9contamin\u00e9es en cas de fuites de fluides.</li>\n<li>Gardez une trousse de lutte contre les d\u00e9versements et les coordonn\u00e9es de personnes-ressources si une situation d'urgence survient durant le transport.</li>\n</ul>\n\n<h3>Personnes et \u00e9quipement</h3>\n\n<p>Les personnes et l'\u00e9quipement qui sont entr\u00e9s en contact avec les carcasses d'animaux ou leur milieu peuvent pr\u00e9senter un risque de propagation de maladies. Afin de minimiser ce risque, veuillez respecter les consignes suivantes\u00a0:</p>\n\n<ul>\n<li>Utilisez de l'\u00e9quipement (<abbr title=\"par exemple\">p.\u00a0ex.</abbr>, pelles, cha\u00eenes, conteneurs) d\u00e9sign\u00e9 pour l'activit\u00e9 de transport. Nettoyez et d\u00e9sinfectez l'\u00e9quipement apr\u00e8s chaque utilisation.</li>\n<li>Si le conducteur doit manipuler les carcasses d'animaux, il est recommand\u00e9 que celui-ci porte de l'\u00e9quipement de protection individuelle, des gants et des bottes jetables.</li>\n<li>Lavez et d\u00e9sinfectez toujours vos mains avant et apr\u00e8s \u00eatre entr\u00e9 en contact avec des carcasses, des animaux vivants ou de l'\u00e9quipement.</li>\n<li>Dissuadez toute personne qui ne prend pas part \u00e0 l'activit\u00e9 de transport d'entrer en contact avec le chargement, l'\u00e9quipement et la remorque.</li>\n<li>Gardez la cabine du camion propre afin d'\u00e9viter une contamination crois\u00e9e entre les activit\u00e9s de transport. Par exemple, retirez les chaussures et v\u00eatements sales avant d'entrer dans la cabine.</li>\n</ul>\n\n<p>Pour plus de renseignements, veuillez consulter les <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/transport/fra/1519235033628/1519235183948\">normes en mati\u00e8re de bios\u00e9curit\u00e9 pour le transport du b\u00e9tail, de la volaille et des carcasses d'animaux</a> sur le <a href=\"/fra/1297964599443/1297965645317\">site Web de l'Agence canadienne d'inspection des aliments</a> \u00e0 l'adresse Inspection.gc.ca et sur celui de la <a href=\"http://www.animalhealth.ca/aspx/public/index.aspx?languageid=fr\">Coalition canadienne pour la sant\u00e9 des animaux</a> \u00e0 l'adresse Animalhealth.ca.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}