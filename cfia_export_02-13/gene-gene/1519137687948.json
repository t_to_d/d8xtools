{
    "dcr_id": "1519137687948",
    "title": {
        "en": "<span class=\"nowrap\">Malaysia \u2013</span> Export requirements for milk and dairy products",
        "fr": "<span class=\"nowrap\">Malaisie \u2013</span> Exigences d'exportation pour le lait et les produits laitiers"
    },
    "html_modified": "2024-02-13 9:52:43 AM",
    "modified": "2023-12-05",
    "issued": "2018-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_malaysia_1519137687948_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_malaysia_1519137687948_fra"
    },
    "ia_id": "1519137688344",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Inspecting and investigating - Performing export certification activities|Exporting food",
        "fr": "Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation|Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "<span class=\"nowrap\">Malaysia \u2013</span> Export requirements for milk and dairy products",
        "fr": "<span class=\"nowrap\">Malaisie \u2013</span> Exigences d'exportation pour le lait et les produits laitiers"
    },
    "label": {
        "en": "<span class=\"nowrap\">Malaysia \u2013</span> Export requirements for milk and dairy products",
        "fr": "<span class=\"nowrap\">Malaisie \u2013</span> Exigences d'exportation pour le lait et les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1519137688344",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/malaysia-milk-and-dairy-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/malaisie-lait-et-produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Malaysia \u2013 Export requirements for milk and dairy products",
            "fr": "Malaisie \u2013 Exigences d'exportation pour le lait et les produits laitiers"
        },
        "description": {
            "en": "Countries to which exports are currently made \u2013 Malaysia.",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Malaisie."
        },
        "keywords": {
            "en": "Malaysia, export, requirements, milk, dairy products",
            "fr": "Malaisie, exportation, exigences, lait, produits laitiers"
        },
        "dcterms.subject": {
            "en": "certification,exports,agri-food industry,inspection,dairy products",
            "fr": "accr\u00e9ditation,exportation,industrie agro-alimentaire,inspection,produit laitier"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "modified": {
            "en": "2023-12-05",
            "fr": "2023-12-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Malaysia \u2013 Export requirements for milk and dairy products",
        "fr": "Malaisie \u2013 Exigences d'exportation pour le lait et les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">1. Eligible/ineligible product</a></li>\n<li><a href=\"#a2\">2. Pre-export approvals by the competent authority of the importing country</a></li>\n<li><a href=\"#a3\">3. Production controls and inspection requirements</a></li>\n<li><a href=\"#a4\">4. Labelling, packaging and marking requirements</a></li>\n<li><a href=\"#a5\">5. Required documents</a></li>\n<li><a href=\"#a6\">6. Other information</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n\n<ul>\n<li>Dairy products approved by Malaysian authorities. An import permit may be required.</li>\n</ul>\n\n<h3>Ineligible</h3>\n\n<ul>\n<li>Information not available.</li>\n</ul>\n\n<h2 id=\"a2\">2. Pre-export approvals by the competent authority of the importing country</h2>\n\n<h3>Establishments</h3>\n\n<p>In order to export dairy products to Malaysia, processing establishments must be approved by the Malaysian authorities. The form titled <a href=\"https://www.dvs.gov.my/dvs/resources/user_1/2023/BRV/BORANG/APPLICATION_FORM_FOR_EXPORT_MEAT,_POULTRY,_MILK,_EGG_AND_PRODUCTS_TO_MALAYSIA_NEW_REVISED_3_MARCH_2023.1a_.pdf\">\"Application form for export meat, poultry, milk, egg and products to Malaysia\" (PDF)</a> will need to be completed. This form is available on the Malaysian veterinary services website. Your importer should provide you with the most up-to date version. The application form and any other documents must be submitted in English.</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) will sign send the approval request to the Malaysian authorities. The approval process can take time depending upon circumstances beyond the control of the CFIA. The inspector will submit the request following the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/food-export-eligibility-lists/eng/1540923903657/1540923903887\">Operational procedure: Procedure for maintaining food export eligibility lists</a>.</p>\n\n<p>The <a href=\"https://www.dvs.gov.my/index.php\">Department of Veterinary Services</a> (DVS) of the Ministry of Agriculture and Food Industries maintains\u00a03 list of establishments depending on the following categories of products:</p>\n\n<ul>\n<li>dairy products</li>\n<li>liquid milk</li>\n<li>ice cream</li>\n</ul>\n\n<p>To access the different lists, please go to the website of the Department of Veterinary Services and follow the steps below:</p>\n\n<ul>\n<li>click on Import\u00a0/ Export</li>\n<li>click on Import</li>\n<li>go to point\u00a011, <i><span lang=\"ms\">Pendaftaran Loji Pemprosesan Bagi Pengeksportan Susu dan Produk Susu Ke Malaysia</span>\u00a0/ List of Dairy Establishments Approved for export to Malaysia</i></li>\n</ul>\n\n<h3>Products</h3>\n\n<p>Product registration is required with the DVS. This information must be included in the application form for the approval of the establishments. Approved products will be listed on the list of dairy establishments approved for export to Malaysia.</p>\n\n<h3>Import permit</h3>\n\n<ul>\n<li>Consignments of milk or milk products must be accompanied by a valid import permit issued by the Malaysian Quarantine and Inspection Services Department (MAQIS).</li>\n</ul>\n\n<h2 id=\"a3\">3. Production controls and inspection requirements</h2>\n\n<p>The manufacturer must be aware of the standards and requirements of the importing country. A specific export procedure must be in place.</p>\n\n<h3><strong>Manufacturer's declaration</strong></h3>\n\n<ul>\n<li>All export requests for milk and dairy products must be accompanied by a Manufacturer's declaration completed and signed by an authorized person of the food manufacturing establishment/facility.</li>\n<li>To request a template of the manufacturer's declaration, please contact your <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a>.</li>\n<li>The product(s) must be clearly identified on the manufacturer's declaration and match exactly the product(s) that are part of the export shipment.</li>\n</ul>\n\n<p><strong>Note:</strong> Inspectors will verify that the manufacturer's declaration is completed appropriately and reserve the right to request any other information that they think is necessary for the final certification of the product.</p>\n\n<h2 id=\"a4\">4. Labelling, packaging and marking requirements</h2>\n\n<p>It is the exporter's responsibility to meet all the requirements for labelling, packaging and marking requirements as per the importing country.</p>\n\n<ul>\n<li>Milk and milk products must be clearly labeled with following information:\n<ul>\n<li>name, address and establishment number of the manufacturer</li>\n<li>batch and/or lot number</li>\n<li>date of production</li>\n<li>date of expiry</li>\n<li>storage conditions</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a5\">5. Documentation requirements</h2>\n\n<p>Canada does not have a negotiated certificate with Malaysia. The certificate below is issued on the basis of commercial risk.</p>\n\n<h3>Certificate</h3>\n\n<ul>\n<li>Health certificate for the export of dairy products and dairy based products for human consumption CFIA/ACIA\u00a05813.\n<ul>\n<li>Certificate must be dated within seven (7) days of export and signed or endorsed by a CFIA veterinarian.</li>\n</ul>\n</li>\n<li>Halal certificate issued by an Islamic organization in Canada that has been recognized by the Malaysian authorities. The CFIA is not involved in the halal certification.</li>\n</ul>\n\n<p><strong>Note:</strong></p>\n\n<ul>\n<li>The health certificate is signed by the CFIA official veterinarian and official inspector.</li>\n<li>It is the responsibility of the exporter to check that the certificate above covers the requirements of Malaysia. Specific import conditions could be required. If this is the case, please forward these conditions to your local CFIA office who will forward them to the Food Import and Export Division (FIED) for review and action if necessary.</li>\n<li>Export certificates cannot be issued for products that have left Canada.</li>\n</ul>\n\n<h2 id=\"a6\">6. Other information</h2>\n\n<p>Exported products transiting through a country may require transit documentation. It is the responsibility of the exporter to ensure that the shipment will be accompanied by all necessary certificates.</p>\n\n<p>Samples (personal or commercial) may be subject to the same requirements as a regular shipment. It is strongly recommended that the exporter verify these requirements with their importer.</p>\n\n<h3>Relevant Links</h3>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"https://www.dvs.gov.my/\">Department of Veterinary Services (DVS)</a></li>\n<li><a href=\"https://www.maqis.gov.my/\">Malaysian Quarantine and Inspection Services Department (MAQIS)</a></li>\n<li><a href=\"https://halalmontreal.com/halal-accreditation/\">Halal Montreal Certification Authority (HMCA)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">1. Produits admissibles/non admissibles</a></li>\n<li><a href=\"#a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</a></li>\n<li><a href=\"#a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</a></li>\n<li><a href=\"#a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</a></li>\n<li><a href=\"#a5\">5. Documents requis</a></li>\n<li><a href=\"#a6\">6. Autres renseignements</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Produits admissibles/non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<ul>\n<li>Produits laitiers approuv\u00e9s par les autorit\u00e9s malaisiennes. Un permis d'importation pourrait \u00eatre n\u00e9cessaire.</li>\n</ul>\n\n<h3>Non Admissibles</h3>\n\n<ul>\n<li>Information non disponible</li>\n</ul>\n\n<h2 id=\"a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<h3>\u00c9tablissements</h3>\n\n<p>Pour exporter des produits laitiers, les \u00e9tablissements de transformation doivent \u00eatre approuv\u00e9s par les autorit\u00e9s malaisiennes. Le formulaire intitul\u00e9 <a href=\"https://www.dvs.gov.my/dvs/resources/user_1/2023/BRV/BORANG/APPLICATION_FORM_FOR_EXPORT_MEAT,_POULTRY,_MILK,_EGG_AND_PRODUCTS_TO_MALAYSIA_NEW_REVISED_3_MARCH_2023.1a_.pdf\">\u00ab\u00a0Application form for export Meat, Poultry, Milk, Egg and products to Malaysia\u00a0\u00bb (PDF)</a> devra \u00eatre compl\u00e8te. Ce formulaire est disponible sur le site des services v\u00e9t\u00e9rinaires malaisiens. Votre importateur devrait vous fournir la version la plus \u00e0 jour. Le formulaire d'application et tout autre document doivent \u00eatre en soumis anglais.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) signera et se chargera d'envoyer la demande d'approbation aux autorit\u00e9s malaisiennes. Le processus d'approbation peut prendre du temps selon des circonstances ind\u00e9pendantes de la volont\u00e9 de l'ACIA. L'inspecteur soumettra la demande selon la <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/les-listes-d-admissibilite-a-l-exportation-d-alime/fra/1540923903657/1540923903887\">Proc\u00e9dure op\u00e9rationnelle\u00a0: Proc\u00e9dure pour tenir \u00e0 jour les listes d'admissibilit\u00e9 \u00e0 l'exportation des aliments</a>.</p>\n\n<p>Le <a href=\"https://www.dvs.gov.my/index.php\">D\u00e9partement des Services V\u00e9t\u00e9rinaires</a> (DSV) du Minist\u00e8re de l'Agriculture et des Industries Alimentaires maintient\u00a03 listes d'\u00e9tablissements selon la cat\u00e9gorie des produits suivant\u00a0:</p>\n\n<ul>\n<li>produits laitiers.</li>\n<li>lait liquide.</li>\n<li>cr\u00e8me glac\u00e9e.</li>\n</ul>\n\n<p>Pour acc\u00e9der aux diff\u00e9rentes listes, veuillez vous rendre sur le site Internet du D\u00e9partement des Services V\u00e9t\u00e9rinaires en suivant les \u00e9tapes ci-dessous\u00a0:</p>\n\n<ul>\n<li>cliquer sur Import\u00a0/ Export</li>\n<li>cliquer sur Import</li>\n<li>Allez au point\u00a011, <i><span lang=\"ms\">Pendaftaran Loji Pemprosesan Bagi Pengeksportan Susu dan Produk Susu Ke Malaysia</span>\u00a0/ List of Dairy Establishments Approved for export to Malaysia</i>.</li>\n</ul>\n\n<h3>Produits</h3>\n\n<p>Un enregistrement des produits est n\u00e9cessaire aupr\u00e8s du DSV. Cette information doit \u00eatre fournie dans le formulaire d'approbation des \u00e9tablissements de transformation. Les produits approuv\u00e9s seront list\u00e9s sur la liste des \u00e9tablissements de produits laitiers approuv\u00e9s pour l'exportation vers la Malaisie.</p>\n\n<h3>Permis d'importation</h3>\n\n<ul>\n<li>Les envois de lait ou de produits laitiers doivent \u00eatre accompagn\u00e9s d'un permis d'importation valide d\u00e9livr\u00e9 par le D\u00e9partement des services de quarantaine et d'inspection malaisiens (MAQIS).</li>\n</ul>\n\n<h2 id=\"a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<p>L'\u00e9tablissement doit \u00eatre au courant des normes et exigences du pays importateur. Une proc\u00e9dure d'exportation sp\u00e9cifique doit \u00eatre en place.</p>\n\n<h3>D\u00e9claration du fabricant</h3>\n\n<ul>\n<li>Toutes les demandes finales d'exportation doivent \u00eatre accompagn\u00e9es d'une D\u00e9claration du fabricant compl\u00e9t\u00e9e et sign\u00e9e par une personne autoris\u00e9e de l'\u00e9tablissement/installation de fabrication d'aliments.</li>\n<li>Veuillez communiquer avec votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'ACIA</a> afin d'obtenir un mod\u00e8le de la d\u00e9claration du fabricant.</li>\n<li>Le(s) produit(s) doivent \u00eatre clairement identifi\u00e9s sur la D\u00e9claration du fabricant et correspondre exactement au(x) produit(s) faisant partie de l'exp\u00e9dition d'exportation.</li>\n</ul>\n\n<p><strong>Remarque\u00a0:</strong> Les inspecteurs v\u00e9rifieront que la d\u00e9claration du fabricant est remplie de fa\u00e7on appropri\u00e9e et se r\u00e9servent le droit de demander tout autre renseignement qu'ils jugent n\u00e9cessaire \u00e0 la certification finale du produit.</p>\n\n<h2 id=\"a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</h2>\n\n<p>Il incombe \u00e0 l'exportateur de satisfaire \u00e0 toutes les exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage selon le pays importateur.</p>\n\n<ul>\n<li>Le lait et les produits laitiers doivent \u00eatre clairement \u00e9tiquet\u00e9s avec les informations suivantes\u00a0:\n<ul>\n<li>nom, adresse et num\u00e9ro d'\u00e9tablissement du fabricant</li>\n<li>num\u00e9ro de lot</li>\n<li>date de production</li>\n<li>date d'expiration</li>\n<li>conditions d'entreposage</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a5\">5. Documents requis</h2>\n\n<p>Le Canada n'a pas de certificat n\u00e9goci\u00e9 avec la Malaisie. Le certificat ci-dessous est \u00e9mis sur la base du risque commercial.</p>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Certificat sanitaire pour l'exportation de produits laitiers et d'aliments lact\u00e9s pour consommation humaine CFIA/ACIA\u00a05813.\n<ul>\n<li>Le certificat doit \u00eatre dat\u00e9 dans les sept (7) jours suivant l'exportation et sign\u00e9 ou endoss\u00e9 par un v\u00e9t\u00e9rinaire de l'ACIA.</li>\n</ul>\n</li>\n<li>Certificat halal \u00e9mis par une organisation islamique enregistr\u00e9e au Canada et qui a \u00e9t\u00e9 approuv\u00e9e par les autorit\u00e9s malaisiennes. L'ACIA n'est pas impliqu\u00e9e dans la certification halal.</li>\n</ul>\n\n<p><strong>Remarque\u00a0:</strong></p>\n\n<ul>\n<li>Le certificat sanitaire est sign\u00e9 par le v\u00e9t\u00e9rinaire officiel et l'inspecteur officiel de l'ACIA.</li>\n<li>Il incombe \u00e0 l'exportateur de v\u00e9rifier que le certificat ci-dessus couvre les exigences de la Malaisie. Des conditions sp\u00e9cifiques pourraient \u00eatre exig\u00e9es. Si tel est le cas veuillez transmettre ces conditions \u00e0 votre bureau local qui les transmettra \u00e0 la Division d'importation et d'exportation des aliments (FIED) pour r\u00e9vision et action si n\u00e9cessaire.</li>\n<li>Les certificats d'exportation ne peuvent pas \u00eatre \u00e9mis pour des produits qui ont quitt\u00e9 le Canada.</li>\n</ul>\n\n<h2 id=\"a6\">6. Autres renseignements</h2>\n\n<p>Des produits export\u00e9s transitant par un pays pourraient n\u00e9cessiter aussi des documents de transit. C'est la responsabilit\u00e9 de l'exportateur de s'assurer que son exp\u00e9dition sera accompagn\u00e9e de tous les certificats n\u00e9cessaires.</p>\n\n<p>Les \u00e9chantillons (personnels ou commerciaux) pourraient \u00eatre soumis aux m\u00eames exigences qu'une exp\u00e9dition r\u00e9guli\u00e8re. Il est fortement recommand\u00e9 \u00e0 l'exportateur de v\u00e9rifier ces exigences aupr\u00e8s de son importateur.</p>\n\n<h3>Liens pertinents</h3>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"https://www.dvs.gov.my/\">D\u00e9partement des Services v\u00e9t\u00e9rinaires malaisiens (DVS) (en anglais seulement)</a></li>\n<li><a href=\"https://www.maqis.gov.my/\">D\u00e9partement des services de quarantaine et d'inspection malaisiens (MAQIS) (en anglais seulement)</a></li>\n<li><a href=\"https://halalmontreal.com/fr/halal-accreditation/\">Autorit\u00e9 de certification Halal Montr\u00e9al (HMCA)</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}