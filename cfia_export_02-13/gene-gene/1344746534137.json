{
    "dcr_id": "1344746534137",
    "title": {
        "en": "Biosecurity for Feedlots",
        "fr": "Bios\u00e9curit\u00e9 dans les parcs d'engraissement"
    },
    "html_modified": "2024-02-13 9:47:40 AM",
    "modified": "2012-08-12",
    "issued": "2012-08-12 00:42:18",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_feedlots_1344746534137_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_feedlots_1344746534137_fra"
    },
    "ia_id": "1344746731585",
    "parent_ia_id": "1344707981478",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Biosecurity",
        "fr": "Bios\u00e9curit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1344707981478",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biosecurity for Feedlots",
        "fr": "Bios\u00e9curit\u00e9 dans les parcs d'engraissement"
    },
    "label": {
        "en": "Biosecurity for Feedlots",
        "fr": "Bios\u00e9curit\u00e9 dans les parcs d'engraissement"
    },
    "templatetype": "content page 1 column",
    "node_id": "1344746731585",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1344707905203",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/standards-and-principles/feedlots/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/parcs-d-engraissement/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biosecurity for Feedlots",
            "fr": "Bios\u00e9curit\u00e9 dans les parcs d'engraissement"
        },
        "description": {
            "en": "Biosecurity is essential on feedlots, where animals originating from a variety of farms are raised together in single locations. This increases the risk of disease transmission.",
            "fr": "La bios\u00e9curit\u00e9 est essentielle dans les parcs d'engraissement, car des animaux provenant de diverses exploitations y sont r\u00e9unis et \u00e9lev\u00e9s dans un m\u00eame lieu, ce qui augmente le risque de transmission de maladies."
        },
        "keywords": {
            "en": "biosecurity, animal health, diseases, isolation, sanitation, traffic control, herd health, feedlots",
            "fr": "bios\u00e9curit\u00e9, sant\u00e9 des animaux, maladies, isolement, mesures sanitaires, sant\u00e9 du troupeau, parcs d'engraissement"
        },
        "dcterms.subject": {
            "en": "livestock,imports,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-08-12 00:42:18",
            "fr": "2012-08-12 00:42:18"
        },
        "modified": {
            "en": "2012-08-12",
            "fr": "2012-08-12"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biosecurity for Feedlots",
        "fr": "Bios\u00e9curit\u00e9 dans les parcs d'engraissement"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The Canadian feedlot sector produces more than half the cattle from the leading fed-cattle provinces, and is evolving towards larger, specialized, more mechanized operations. Biosecurity is essential on feedlots, where animals originating from a variety of farms are raised together in single locations. This increases the risk of disease transmission.</p>\n<h2>Bovine diseases</h2>\n<p>The global emergence and re-emergence of bovine diseases in recent years has had a major impact on the cattle industry, both within Canada and abroad. Outbreaks of contagious diseases such as Foot and Mouth Disease in cattle in other countries have resulted in significant economic losses for cattle industries, as well as animal health and environmental concerns. These kinds of incidents emphasize the need for a comprehensive, coordinated approach to bovine biosecurity.</p>\n<p>Led by the Canadian Food Inspection Agency (CFIA), federal, provincial and territorial governments are continuously collaborating with industry and the public to implement and augment bovine biosecurity programs aimed at reducing disease transmission and protecting the interests of Canadians.</p>\n<h2>Sources of bovine diseases</h2>\n<p>Bovine diseases can be spread in a number of ways, including:</p>\n<ul>\n<li>through diseased cattle or cattle carrying disease;</li>\n<li>through animals other than cattle (farm animals, pets, wild birds and other wildlife, vermin and insects);</li>\n<li>on the clothing and shoes of visitors and employees moving from site-to-site;</li>\n<li>in contaminated feed, water, bedding and soil;</li>\n<li>from the carcasses of dead animals;</li>\n<li>on contaminated farm equipment and vehicles; or</li>\n<li>in airborne particles and dust blown by the wind.</li>\n</ul>\n<h2>Biosecurity principles for feedlots</h2>\n<p>Some of the basic biosecurity principles for the feedlot sector include:</p>\n<ul>\n<li class=\"mrgn-bttm-md\"><strong>Isolation:</strong> \n<ul>\n<li>Only receive animals from reputable sources.</li>\n<li>Limit the frequency of introducing new animals to the herd.</li>\n<li>Move cattle through the pens using an all-in-all-out approach.</li>\n<li>Provide maximum pen space to avoid overcrowding and unnecessary stress.</li>\n<li>Protect feed and water sources from contamination.</li>\n<li>Isolate sick animals from the rest of the herd.</li>\n<li>Prevent nose-to-nose contact between animals in different pens by using double fencing or alleyways.</li>\n<li>Separate feed and water troughs between different pens.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Sanitation:</strong> \n<ul>\n<li>Routinely clean and disinfect buildings, barns, equipment, clothing and footwear.</li>\n<li>Designate equipment for clean jobs (feeding) and dirty jobs (manure handling and carcass disposal).</li>\n<li>Designate a cleaning area for vehicles and equipment.</li>\n<li>Promptly dispose of dead cattle.</li>\n<li>Implement a manure management program.</li>\n<li>Avoid borrowing equipment and vehicles from other farms or feedlots.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Traffic control:</strong> \n<ul>\n<li>Control visitors' access to the site.</li>\n<li>Prevent birds, rodents, pets and other animals from coming into contact with the herd.</li>\n<li>Require all visitors to wear clean boots, clothing and gloves.</li>\n<li>Maintain records of the movement of people, animals and equipment onto and off of the premises.</li>\n<li>Handle younger animals before older animals and healthy animals before sick or incoming animals.</li>\n<li>Make sure all suppliers and other site visitors follow your biosecurity measures.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Herd health management:</strong> \n<ul>\n<li>Monitor herd health daily.</li>\n<li>Identify all animals with proper ear tags for traceability.</li>\n<li>Employ veterinary services to help implement herd health programs.</li>\n<li>Vaccinate cattle against certain diseases.</li>\n<li>Immediately report any signs of illness to your veterinarian or the nearest <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Program maintenance:</strong> \n<ul>\n<li>Train all staff in the application of your biosecurity program.</li>\n<li>Regularly monitor the effectiveness of the program.</li>\n<li>Be aware of any diseases in your area and adjust your biosecurity program to meet specific needs, as required.</li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Au Canada, plus de la moiti\u00e9 des bovins des principales provinces productrices de bovins gras proviennent du secteur des parcs d'engraissement. Cette industrie prend de plus en plus d'essor, en se sp\u00e9cialisant et en m\u00e9canisant toujours plus ses op\u00e9rations. La bios\u00e9curit\u00e9 est essentielle dans les parcs d'engraissement, car des animaux provenant de diverses exploitations y sont r\u00e9unis et \u00e9lev\u00e9s dans un m\u00eame lieu, ce qui augmente le risque de transmission de maladies.</p>\n<h2>Maladies bovines</h2>\n<p>Ces derni\u00e8res ann\u00e9es, l'\u00e9mergence et la r\u00e9\u00e9mergence de maladies bovines ont eu de graves r\u00e9percussions sur l'industrie bovine, et ce, tant au Canada qu'\u00e0 l'\u00e9tranger. Des \u00e9closions de maladies animales comme la fi\u00e8vre aphteuse dans d'autres pays ont entra\u00een\u00e9 des pertes \u00e9conomiques importantes pour l'industrie bovine et soulev\u00e9 des pr\u00e9occupations en mati\u00e8re de sant\u00e9 animale et d'environnement. Ce type d'incident fait ressortir la n\u00e9cessit\u00e9 d'une approche globale et coordonn\u00e9e en mati\u00e8re de bios\u00e9curit\u00e9 bovine.</p>\n<p>Guid\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA), les gouvernements f\u00e9d\u00e9ral, provinciaux et territoriaux collaborent continuellement avec l'industrie et le grand public pour mettre en \u0153uvre et accro\u00eetre les programmes de bios\u00e9curit\u00e9 bovine afin de r\u00e9duire la transmission de maladies et de prot\u00e9ger les int\u00e9r\u00eats des Canadiens.</p>\n<h2>Sources de maladies bovines</h2>\n<p>Les maladies bovines peuvent se propager de nombreuses fa\u00e7ons, dont les suivantes\u00a0:</p>\n<ul>\n<li>par des bovins malades ou porteurs de maladies;</li>\n<li>par des animaux autres que les bovins (animaux d'\u00e9levage, animaux de compagnie, oiseaux sauvages ou autres animaux sauvages, vermine et insectes);</li>\n<li>par les v\u00eatements et les chaussures de visiteurs et d'employ\u00e9s qui se d\u00e9placent d'un lieu \u00e0 l'autre;</li>\n<li>par des aliments, de l'eau, de la liti\u00e8re ou de la terre contamin\u00e9s;</li>\n<li>par des carcasses d'animaux morts;</li>\n<li>par de l'\u00e9quipement ou des v\u00e9hicules contamin\u00e9s;</li>\n<li>par des particules et des poussi\u00e8res contamin\u00e9es en suspension dans l'air ou port\u00e9es par le vent.</li>\n</ul>\n<h2>Principes de bios\u00e9curit\u00e9 dans les parcs d'engraissement</h2>\n<p>Les principes de base de la bios\u00e9curit\u00e9 dans les parcs d'engraissement comprennent les \u00e9l\u00e9ments suivants\u00a0:</p>\n<ul>\n<li class=\"mrgn-bttm-md\"><strong>Isolement</strong> \n<ul>\n<li>Ne recevoir que des animaux de sources fiables.</li>\n<li>Limiter la fr\u00e9quence d'introduction de nouveaux animaux dans le troupeau.</li>\n<li>D\u00e9placer les bovins d'un enclos \u00e0 l'autre selon le syst\u00e8me d'\u00e9levage par renouvellement int\u00e9gral (par lots distincts).</li>\n<li>Offrir le maximum d'espace pour \u00e9viter le surpeuplement et le stress inutile.</li>\n<li>Prot\u00e9ger les sources d'aliments et d'eau de la contamination.</li>\n<li>Isoler les animaux malades du reste du troupeau.</li>\n<li>Pr\u00e9venir les contacts museau \u00e0 museau entre les animaux des diff\u00e9rents enclos \u00e0 l'aide de cl\u00f4tures doubles ou de corridors entre les enclos.</li>\n<li>S\u00e9parer les abreuvoirs et les mangeoires des diff\u00e9rents enclos.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Mesures sanitaires</strong> \n<ul>\n<li>Nettoyer et d\u00e9sinfecter r\u00e9guli\u00e8rement les b\u00e2timents, les \u00e9tables, l'\u00e9quipement, les v\u00eatements et les chaussures.</li>\n<li>R\u00e9server de l'\u00e9quipement pour les t\u00e2ches concernant les mati\u00e8res non souill\u00e9es ou non contamin\u00e9es (distribution des aliments) et d'autre pour celles concernant des mati\u00e8res souill\u00e9es ou contamin\u00e9es (manipulation du fumier, \u00e9limination des carcasses).</li>\n<li>R\u00e9server une zone pour le nettoyage des v\u00e9hicules et de l'\u00e9quipement.</li>\n<li>\u00c9liminer rapidement les animaux morts.</li>\n<li>Mettre en \u0153uvre un programme de gestion du fumier.</li>\n<li>\u00c9viter d'emprunter l'\u00e9quipement et les v\u00e9hicules d'autres exploitations ou parcs d'engraissement.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Contr\u00f4le de la circulation</strong> \n<ul>\n<li>Limiter l'acc\u00e8s des visiteurs.</li>\n<li>Pr\u00e9venir les contacts entre les animaux du troupeau et les oiseaux, les rongeurs, les animaux de compagnie ou d'autres animaux.</li>\n<li>Exiger que tous les visiteurs portent des gants, des bottes et des v\u00eatements propres.</li>\n<li>Tenir des registres sur les d\u00e9placements des personnes, des animaux et de l'\u00e9quipement qui arrivent sur les lieux ou les quittent.</li>\n<li>Manipuler d'abord les jeunes animaux, puis les animaux plus \u00e2g\u00e9s, et les animaux en bonne sant\u00e9 avant les animaux malades ou les animaux nouvellement arriv\u00e9s.</li>\n<li>Veiller \u00e0 ce que tous les fournisseurs et autres visiteurs se conforment aux mesures de bios\u00e9curit\u00e9.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Gestion de la sant\u00e9 du troupeau</strong> \n<ul>\n<li>Surveiller quotidiennement la sant\u00e9 du troupeau.</li>\n<li>Identifier tous les animaux \u00e0 l'aide d'\u00e9tiquettes d'oreille destin\u00e9es \u00e0 la tra\u00e7abilit\u00e9.</li>\n<li>Avoir recours aux services d'un v\u00e9t\u00e9rinaire pour la mise en \u0153uvre des programmes sanitaires.</li>\n<li>Vacciner les bovins contre certaines maladies.</li>\n<li>Signaler imm\u00e9diatement tout signe de maladie \u00e0 votre v\u00e9t\u00e9rinaire ou au bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> le plus pr\u00e8s.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Mise \u00e0 jour du programme</strong> \n<ul>\n<li>Former tout le personnel relativement \u00e0 l'application du programme de bios\u00e9curit\u00e9.</li>\n<li>V\u00e9rifier r\u00e9guli\u00e8rement l'efficacit\u00e9 du programme.</li>\n<li>Se tenir au courant des maladies qui touchent la r\u00e9gion et adapter le programme de bios\u00e9curit\u00e9 selon les besoins.</li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}