{
    "dcr_id": "1326264152750",
    "title": {
        "en": "Food Safety Management System",
        "fr": "Syst\u00e8me de gestion de la salubrit\u00e9 des aliments"
    },
    "html_modified": "2024-03-12 3:55:31 PM",
    "modified": "2013-03-18",
    "issued": "2012-01-11 01:42:35",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_fsep_onfarm_producers_1326264152750_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_fsep_onfarm_producers_1326264152750_fra"
    },
    "ia_id": "1326264302033",
    "parent_ia_id": "1299861042890",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299861042890",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food Safety Management System",
        "fr": "Syst\u00e8me de gestion de la salubrit\u00e9 des aliments"
    },
    "label": {
        "en": "Food Safety Management System",
        "fr": "Syst\u00e8me de gestion de la salubrit\u00e9 des aliments"
    },
    "templatetype": "content page 1 column",
    "node_id": "1326264302033",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299860970026",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-industry/recognition-program/management-system/",
        "fr": "/salubrite-alimentaire-pour-l-industrie/programme-de-reconnaissance/systeme-de-gestion/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food Safety Management System",
            "fr": "Syst\u00e8me de gestion de la salubrit\u00e9 des aliments"
        },
        "description": {
            "en": "The On-Farm Food Safety Recognition Program is a process of review, assessment, recognition and ongoing monitoring of the technical soundness and administrative effectiveness of on-farm food safety systems",
            "fr": "Le Programme de reconnaissance de la salubrit\u00e9 des aliments \u00e0 la ferme consiste \u00e0 examiner le fondement technique et l'efficacit\u00e9 administrative des syst\u00e8mes de salubrit\u00e9 des aliments \u00e0 la ferme"
        },
        "keywords": {
            "en": "On-Farm Food Safety Recognition Program, on-farm food safety program, management system, general management component, technical component, conformance compenent, auditor-training component",
            "fr": "Programme de reconnaissance de la salubrit\u00e9 des aliments \u00e0 la ferme, programme de salubrit\u00e9 des aliments \u00e0 la ferme, syst\u00e8me de gestion, composant de gestion g\u00e9n\u00e9rale, composant technique, composant de conformit\u00e9, composant d'auditeur-formation"
        },
        "dcterms.subject": {
            "en": "food inspection,food safety,inspection",
            "fr": "inspection des aliments,salubrit\u00e9 des aliments,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety Strategies Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des strat\u00e9gies de la salubrit\u00e9 des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-11 01:42:35",
            "fr": "2012-01-11 01:42:35"
        },
        "modified": {
            "en": "2013-03-18",
            "fr": "2013-03-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food Safety Management System",
        "fr": "Syst\u00e8me de gestion de la salubrit\u00e9 des aliments"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Led by the Canadian Food Inspection Agency (CFIA) with the participation of the provincial and territorial governments, the Food Safety Recognition Program is a process of review, assessment, recognition and ongoing monitoring of the technical soundness and administrative effectiveness of on-farm and post-farm food safety systems developed and implemented by Canada's national organizations.</p>\n<p>A food safety system is comprised of managerial and administrative structures and processes to facilitate food safety program's design and delivery, ongoing maintenance, evaluation and continual improvement. These structures and processes are detailed in the national organization's food safety program management system. This management system must be structured in a format to allow for the documentation of activities for audit purposes.</p>\n<p>To gain recognition for its food safety program, the national organization is expected to meet or exceed the government requirements for the management system. To ascertain it meets government requirements, the management system and associated documentation is submitted to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for Pre-Recognition Stage One - Part Two (Technical Review of Management System and Associated Documentation). A gap analysis is performed to identify deficiencies. Satisfactory fulfillment of the requirements is verified through the third-party audits of Pre-Recognition Stage Two (Implementation and Third-Party Audit) and the implementation assessments of Pre-Recognition Stage Three of the recognition process.</p>\n<p>Following attainment of recognition, ongoing monitoring by way of regularly scheduled ongoing assessments and triggered assessments will determine if the national producer organization's food safety management system is maintaining its administrative effectiveness.</p>\n<h2>The four-component food safety management system</h2>\n<p>An analogy of a food safety management system can be made with a four-drawer filing cabinet. This filing cabinet resides in the national organization's head office. Each drawer contains the procedures, records and other relevant documentation for the component's design and delivery, ongoing maintenance, evaluation, and continual improvement.</p>\n<h3>1. General Management Component</h3>\n<p>The General Management Component oversees the entire system. It chronicles the national organization's roles and responsibilities, policies and procedures regarding human and monetary resources; communication plans; preventive and corrective action plans; and evaluation and improvement mechanisms for the general management system as a whole.</p>\n<p>Required elements of the general management component:</p>\n<ol class=\"lst-upr-alph\">\n<li>Policy</li>\n<li>Objectives</li>\n<li>Plan</li>\n<li>Scope</li>\n<li>Organization, responsibility, and authority</li>\n<li>Provision of resources</li>\n<li>Documentation maintenance - review, approval, and revision (identification of changes)</li>\n<li>Management review</li>\n<li>Internal audit</li>\n<li>Corrective and preventive action</li>\n<li>Document and record control</li>\n<li>Outsourcing</li>\n<li>Conflict of interest and impartiality</li>\n<li>Confidentiality</li>\n<li>Training, education and communication</li>\n<li>Training, education and communication plans for the general management component</li>\n</ol>\n<h3>2. Technical Component</h3>\n<p>The Technical Component contains the plans and procedures for the range of activities required for the development, implementation, ongoing operation and management of the national organization's food safety program generic Hazard Analysis Critical Control Points model and commodity-specific producer manual. It covers such aspects as the design and delivery of the program; assembling a team of relevant experts; communication plans; contingency plans; producer and farm employee education; and evaluation and improvement mechanisms.</p>\n<h3>3. Conformance Component</h3>\n<p>The Conformance Component covers the procedures to assess individual producer conformity to the requirements of the commodity-specific producer manual. The procedures include producer requests for on-farm or post-farm audit initiation and audit-readiness assessments; auditor assignment and scheduling; performing the audit; producer-conformity assessment reporting; issuing of documents of conformance to the producer; and evaluation and improvement mechanisms.</p>\n<h3>4. Auditor-Training Component</h3>\n<p>The Auditor-Training Component focuses on the education of food safety auditors. The training material for this component is organized into a general module and commodity-specific module.</p>\n<p>The general module covers, but is not limited to, basic audit principles; Hazard Analysis Critical Control Points principles; information on and purpose of initial assessment audits, interim audits and re-assessment audits; confidentiality; conflict of interest and impartiality issues; and corrective and preventive action procedures.</p>\n<p>The commodity-specific module covers, but is not limited to, the intent of the food safety program; determining producer conformance to the food safety program; reporting the audit findings; evaluation of responses to corrective action requests; audit evaluation; and issuing of documents of conformance to the producer.</p>\n<p>Documentation for the auditor-training component includes course objectives, content and materials; examination and grading methods; training schedules; course evaluation and improvement mechanisms.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Dirig\u00e9 par l'Agence canadienne d'inspection des aliments avec la participation des gouvernements provinciaux et territoriaux, le Programme de reconnaissance de la salubrit\u00e9 des aliments consiste \u00e0 examiner le fondement technique et l'efficacit\u00e9 administrative des syst\u00e8mes de salubrit\u00e9 des aliments \u00e0 la ferme et en aval de celle-ci con\u00e7us et mis en \u0153uvre pardes organisations nationales du Canada, ainsi qu'\u00e0 effectuer le suivi continu de ces syst\u00e8mes.</p>\n<p>Un syst\u00e8me de salubrit\u00e9 des aliments comprend des structures et des m\u00e9thodes de gestion et d'administration visant \u00e0 faciliter l'\u00e9laboration, l'ex\u00e9cution, la mise \u00e0 jour constante, l'\u00e9valuation et l'am\u00e9lioration continue du programme de la salubrit\u00e9 des aliments. Ces structures et ces m\u00e9thodes sont d\u00e9crites en d\u00e9tail dans le syst\u00e8me de gestion du programme de la salubrit\u00e9 des aliments \u00e9tabli par une organisation nationale. Le syst\u00e8me doit \u00eatre con\u00e7u de mani\u00e8re \u00e0 ce que l'on puisse documenter les activit\u00e9s aux fins de v\u00e9rification.</p>\n<p>Pour obtenir la reconnaissance de son programme de salubrit\u00e9 des aliments, une organisation nationale doit atteindre ou d\u00e9passer les exigences du gouvernement relatives au syst\u00e8me de gestion. Pour s'assurer que le syst\u00e8me de gestion et la documentation satisfont aux exigences du gouvernement,l'organisation doit les pr\u00e9senter \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en vue d'obtenir la reconnaissance pr\u00e9alable (\u00e9tape 1 -, partie 2, Examen technique du syst\u00e8me de gestion et de la documentation connexe). L'Agence m\u00e8ne une analyse afin de cerner les lacunes. Elle d\u00e9termine si les exigences sont respect\u00e9es au moyen de v\u00e9rifications par des tierces parties de la reconnaissance pr\u00e9alable (\u00e9tape 2, Mise en \u0153uvre et v\u00e9rification par une tierce partie) et d'\u00e9valuations de la mise en \u0153uvre (\u00e9tape 3 du processus de reconnaissance, Reconnaissance pr\u00e9alable).</p>\n<p>Suivant l'obtention de la reconnaissance, une surveillance continue gr\u00e2ce \u00e0 des \u00e9valuations p\u00e9riodiques et \u00e0 des \u00e9valuations ponctuelles servira \u00e0 d\u00e9terminer si le syst\u00e8me de gestion de la salubrit\u00e9 des aliments de l'organisation nationale de producteurs conserve son efficacit\u00e9 administrative.</p>\n<h2>Syst\u00e8me de gestion de la salubrit\u00e9 des aliments \u00e0 quatre volets</h2>\n<p>On peut faire une analogie entre le syst\u00e8me de gestion du programme de la salubrit\u00e9 des aliments  et un classeur \u00e0 quatre tiroirs. Le classeur se trouve dans le bureau principal de l'organisation nationale. Chaque tiroir contient les m\u00e9thodes, dossiers et autres documents pertinents portant sur l'\u00e9laboration, l'ex\u00e9cution, la mise \u00e0 jour constante, l'\u00e9valuation et l'am\u00e9lioration continue de chaque volet.</p>\n<h3>1. Volet de la gestion g\u00e9n\u00e9ral</h3>\n<p>Le volet de la gestion g\u00e9n\u00e9ral couvre l'ensemble du syst\u00e8me. Il englobe le r\u00f4le et les responsabilit\u00e9s de l'organisme nationale, les politiques et proc\u00e9dures en mati\u00e8re de ressources humaines et financi\u00e8res, les plans de communication, les plans de mesures correctives et pr\u00e9ventives, ainsi que les m\u00e9canismes d'\u00e9valuation et d'am\u00e9lioration du syst\u00e8me de gestion g\u00e9n\u00e9rale dans son ensemble.</p>\n<p><strong>\u00c9l\u00e9ments prescrits du volet de la gestion g\u00e9n\u00e9rale :</strong></p>\n<ol class=\"lst-upr-alph\">\n<li>Politiques</li>\n<li>Objectifs</li>\n<li>Plan</li>\n<li>Port\u00e9e</li>\n<li>Organisation, responsabilit\u00e9 et pouvoirs</li>\n<li>Attribution des ressources</li>\n<li>Mise \u00e0 jour de la documentation \u2013 examen, approbation et r\u00e9vision (modifications indiqu\u00e9es)</li>\n<li>Examen de la gestion</li>\n<li>V\u00e9rification interne</li>\n<li>Mesures correctives et pr\u00e9ventives</li>\n<li>Contr\u00f4le des documents et des dossiers</li>\n<li>Approvisionnement externe</li>\n<li>Conflit d'int\u00e9r\u00eats et impartialit\u00e9</li>\n<li>Confidentialit\u00e9</li>\n<li>Formation et communication</li>\n<li>Plans de formation et de communication relatif au volet de la gestion g\u00e9n\u00e9rale.</li>\n</ol>\n<h3>2. Volet technique</h3>\n<p>Le volet technique englobe les proc\u00e9dures et les plans relatifs aux diverses activit\u00e9s n\u00e9cessaires \u00e0 l'\u00e9laboration, \u00e0 la mise en \u0153uvre, \u00e0 l'utilisation permanente et \u00e0 la gestion du mod\u00e8le g\u00e9n\u00e9rique d'analyse des risques et de ma\u00eetrise des points critiques (HACCP) ainsi que du manuel du producteur portant sur des produits pr\u00e9cis pour le programme de salubrit\u00e9 des aliments de l'organisation nationale. Il traite notamment des sujets suivants : \u00e9laboration et ex\u00e9cution du programme, constitution d'une \u00e9quipe de sp\u00e9cialistes comp\u00e9tents, plans de communication, plans d'urgence, formation des producteurs et des salari\u00e9s agricoles et m\u00e9canismes d'\u00e9valuation et d'am\u00e9lioration.</p>\n<h3>3. Volet de la conformit\u00e9</h3>\n<p>Le volet de la conformit\u00e9 traite des processus visant \u00e0 v\u00e9rifier que chaque producteur respecte les exigences \u00e9nonc\u00e9es dans le manuel du producteur portant sur des produits pr\u00e9cis. Parmi les processus, mentionnons les demandes des producteurs pour les v\u00e9rifications \u00e0 la ferme ou en aval de celle ci et les \u00e9valuations visant \u00e0 se pr\u00e9parer aux v\u00e9rifications, l'horaire et l'affectation des v\u00e9rificateurs, l'ex\u00e9cution des v\u00e9rifications, les rapports d'\u00e9valuation sur la conformit\u00e9 des producteurs, la transmission de documents sur la conformit\u00e9 aux producteurs ainsi que les m\u00e9canismes d'\u00e9valuation et d'am\u00e9lioration.</p>\n<h3>4. Formation des v\u00e9rificateurs</h3>\n<p>Le quatri\u00e8me volet porte sur la formation des v\u00e9rificateurs en mati\u00e8re de salubrit\u00e9 des aliments. Le mat\u00e9riel de formation comprend un module g\u00e9n\u00e9ral et un module propre \u00e0 chaque produit.</p>\n<p>Le module g\u00e9n\u00e9ral couvre, entre autres, les principes de base de la v\u00e9rification, les principes <abbr title=\"analyse des risques aux points critiques\">HACCP</abbr>, les v\u00e9rifications interm\u00e9diaires et les nouvelles v\u00e9rifications, la confidentialit\u00e9, les conflits d'int\u00e9r\u00eats et l'impartialit\u00e9 ainsi que les proc\u00e9dures relatives aux mesures correctives et pr\u00e9ventives.</p>\n<p>Le module propre \u00e0 chaque produit porte notamment sur l'objectif du programme de la salubrit\u00e9 des aliments, l'\u00e9tablissement du respect du programme de salubrit\u00e9 des aliments par les producteurs, la communication des r\u00e9sultats des v\u00e9rifications, l'\u00e9valuation des r\u00e9ponses aux demandes de mesures correctives, l'\u00e9valuation des v\u00e9rifications et la transmission de documents sur la conformit\u00e9 aux producteurs.</p>\n<p>La documentation se rapportant au volet de la formation des v\u00e9rificateurs comprend les objectifs, le contenu et le mat\u00e9riel p\u00e9dagogique du cours, les m\u00e9thodes d'examen et de classement, les calendriers de formation, ainsi que les m\u00e9canismes d'\u00e9valuation et d'am\u00e9lioration du cours.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}