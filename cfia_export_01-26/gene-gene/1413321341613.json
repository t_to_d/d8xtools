{
    "dcr_id": "1413321341613",
    "title": {
        "en": "Enhancements to horse welfare in Canada",
        "fr": "Am\u00e9lioration du bien-\u00eatre des chevaux au Canada"
    },
    "html_modified": "2024-01-26 2:22:39 PM",
    "modified": "2021-02-09",
    "issued": "2016-04-21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_transport_horse_enhancements_1413321341613_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_transport_horse_enhancements_1413321341613_fra"
    },
    "ia_id": "1413321342894",
    "parent_ia_id": "1300460096845",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Humane Transport",
        "fr": "Transport sans cruaut\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1300460096845",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Enhancements to horse welfare in Canada",
        "fr": "Am\u00e9lioration du bien-\u00eatre des chevaux au Canada"
    },
    "label": {
        "en": "Enhancements to horse welfare in Canada",
        "fr": "Am\u00e9lioration du bien-\u00eatre des chevaux au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1413321342894",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300460032193",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/humane-transport/horse-welfare/",
        "fr": "/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/bien-etre-des-chevaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Enhancements to horse welfare in Canada",
            "fr": "Am\u00e9lioration du bien-\u00eatre des chevaux au Canada"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) is responsible for ensuring industry follows regulations and requirements relating to the humane transportation and slaughter of animals, including horses.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) veille \u00e0 ce que l'industrie respecte la r\u00e9glementation et les exigences du transport et de l'abattage sans cruaut\u00e9 des animaux, y compris les chevaux."
        },
        "keywords": {
            "en": "animal welfare, humane transportation, horse welfare, humane slaughter, guidance, horses",
            "fr": "protection des animaux, Transport sans cruaut\u00e9, Abattage sans cruaut\u00e9, Orientation, chevaux, bien-\u00eatre des chevaux"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,regulation,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,r\u00e9glementation,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-04-21",
            "fr": "2016-04-21"
        },
        "modified": {
            "en": "2021-02-09",
            "fr": "2021-02-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Enhancements to horse welfare in Canada",
        "fr": "Am\u00e9lioration du bien-\u00eatre des chevaux au Canada"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=34#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The Canadian Food Inspection Agency (CFIA) is responsible for ensuring industry follows regulations and requirements relating to the humane transport and slaughter of animals, including horses.</p>\n<p>As part of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s commitment to animal welfare, over the past several years the Agency has taken a number of steps to enhance policies and regulations related to the human transportation and slaughter of horses in Canada.</p>\n<h2>Humane transport</h2>\n<ul>\n<li>Recent amendments to Part XII of the <i><a href=\"/english/reg/jredirect2.shtml?heasanr\">Health of Animal Regulations</a></i> came into force February 20, 2020. These       amendments will improve animal welfare during transport and harmonize       Canada's practices with current international standards.</li>\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/health-of-animals-regulations-part-xii/eng/1582126008181/1582126616914\"><i>Health of Animals Regulations</i>: Part XII: Transport of Animals-Regulatory Amendment Interpretive Guidance for Regulated Parties</a> is available to clarify expectations surrounding the transportation of animals that are vulnerable, compromised or otherwise unfit.</li>\n<li>Information on <a href=\"/animal-health/terrestrial-animals/humane-transport/horses/transporting-horses/eng/1363747385631/1363747449156\">humane transportation of horses</a> is available online for industry's use.</li>\n</ul>\n<h2>Inspection</h2>\n<ul>\n<li>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> has <a href=\"/animal-health/terrestrial-animals/humane-transport/horses/designated-border-ports/eng/1324090361423/1324310392596\">tightened border inspections</a> for imported slaughter and feeder horses from the <abbr title=\"United States\">U.S.</abbr></li>\n</ul>\n<h2>Humane slaughter</h2>\n<ul>\n<li>Updated procedures have been developed for handling and slaughter through the <a href=\"/food-guidance-by-commodity/meat-products-and-food-animals/guidelines-humane-care-and-handling/eng/1525374637729/1525374638088\">Guidelines       for the humane care and handling of food animals at slaughter</a>.</li>\n</ul>\n\n<h2>Guidance</h2>\n<ul>\n<li>The Agency has become a member of the <a href=\"https://www.nfacc.ca/\">National Farm Animal Care Council</a> to exchange advice and guidance with industry. The Agency has participated in, and continues to provide input to, specific committees dedicated to creating codes of practice for the care and handling of livestock, including horses.</li>\n</ul>\n<h2>Additional links</h2>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/humane-transport/eng/1300460032193/1300460096845\">Humane transport and animal welfare</a></li>\n<li><i><a href=\"/english/reg/jredirect2.shtml?safefood\">Safe Food for Canadians Act </a></i></li>\n<li><i><a href=\"/english/reg/jredirect2.shtml?sfcrrsac\">Safe  Food for Canadians Regulations</a></i></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=34#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'Agence canadienne d'inspection des aliments (ACIA) veille \u00e0 ce que l'industrie respecte la r\u00e9glementation et les exigences du transport et de l'abattage sans cruaut\u00e9 des animaux, y compris les chevaux.</p>\n<p>Dans le cadre de son engagement \u00e0 l'\u00e9gard du bien-\u00eatre des animaux, l'Agence a au cours des sept derni\u00e8res ann\u00e9es pris des mesures afin d'am\u00e9liorer les politiques et la r\u00e9glementation sur le transport et l'abattage sans cruaut\u00e9 au Canada.</p>\n<h2>Transport sans cruaut\u00e9</h2>\n<ul>\n<li>Des modifications r\u00e9centes \u00e0 la partie XII du <i><a href=\"/francais/reg/jredirect2.shtml?heasanr\">R\u00e8glement sur la sant\u00e9 des animaux</a></i> sont entr\u00e9es en vigueur le 20 f\u00e9vrier 2020. Ces modifications am\u00e9lioreront le bien-\u00eatre des animaux durant leur       transport et aligneront les pratiques du Canada sur les normes       internationales.</li>\n<li>Le <a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/reglement-sur-la-sante-des-animaux-partie-xii/fra/1582126008181/1582126616914\"><i>R\u00e8glement sur la sant\u00e9 des animaux</i> partie\u00a0XII\u00a0: modification au r\u00e8glement sur le transport des animaux - Document d'orientation \u00e0 l'intention des parties r\u00e9glement\u00e9es</a> est disponible afin de clarifier les attentes concernant le transport des animaux vuln\u00e9rables, fragilis\u00e9s ou inaptes.</li>\n<li>De l'information sur le <a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/chevaux/transport-de-chevaux/fra/1363747385631/1363747449156\">transport sans cruaut\u00e9 des chevaux</a> est offerte en ligne pour l'industrie.</li>\n</ul>\n\n<h2>Inspection</h2>\n<ul>\n<li>L'<abbr title=\"Agence canadienne d\u2019inspection des aliments\">ACIA</abbr> proc\u00e8de \u00e0 des <a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/chevaux/points-d-entree-designes/fra/1324090361423/1324310392596\">inspections plus strictes \u00e0 la fronti\u00e8re</a> dans le cas de chevaux d'abattage ou d'engraissement import\u00e9s des \u00c9tats-Unis.</li>\n</ul>\n<h2>Abattage sans cruaut\u00e9</h2>\n<ul>\n<li>De nouvelles proc\u00e9dures ont \u00e9t\u00e9 \u00e9labor\u00e9es pour la manipulation et l'abattage des animaux dans le document intitul\u00e9\u00a0: <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-de-viande-et-animaux-pour-alimentation-hu/lignes-directrices-les-soins-et-la-manipulation-sa/fra/1525374637729/1525374638088\">Lignes directrices sur les soins et la manipulation sans cruaut\u00e9 des animaux pour alimentation humaine \u00e0 l'abattoir</a>.</li>\n</ul>\n<h2>Orientation</h2>\n<ul>\n<li>L'Agence est devenue membre du <a href=\"https://www.nfacc.ca/francais\">Conseil national pour les soins aux animaux d'\u00e9levage</a> afin de partager conseils et encadrement avec l'industrie. L'Agence a particip\u00e9, et continue de participer, \u00e0 des comit\u00e9s sp\u00e9ciaux visant \u00e0 cr\u00e9er des codes de pratique pour les soins et la manutention du b\u00e9tail, y compris les chevaux.</li>\n</ul>\n<h2>Autres liens</h2>\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/transport-sans-cruaute/fra/1300460032193/1300460096845\">Transport sans cruaut\u00e9 et bien-\u00eatre des animaux</a></li>\n<li><i><a href=\"/francais/reg/jredirect2.shtml?safefood\">Loi sur la salubrit\u00e9 des aliments au Canada</a></i></li>\n<li><i><a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\">R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</a></i></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}