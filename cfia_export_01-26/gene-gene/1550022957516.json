{
    "dcr_id": "1550022957516",
    "title": {
        "en": "Aquatic animal export: certification requirements for Egypt",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'\u00c9gypte"
    },
    "html_modified": "2024-01-26 2:24:33 PM",
    "modified": "2020-01-07",
    "issued": "2019-02-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_egypt_1550022957516_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_egypt_1550022957516_fra"
    },
    "ia_id": "1550022957781",
    "parent_ia_id": "1331743129372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1331743129372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal export: certification requirements for Egypt",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'\u00c9gypte"
    },
    "label": {
        "en": "Aquatic animal export: certification requirements for Egypt",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'\u00c9gypte"
    },
    "templatetype": "content page 1 column",
    "node_id": "1550022957781",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331740343115",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/egypt/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/egypte/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal export: certification requirements for Egypt",
            "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'\u00c9gypte"
        },
        "description": {
            "en": "This page describes aquatic animal health export certification requirements for Egypt.",
            "fr": "Cette page d\u00e9crit les exigences de certification d'exportation de sant\u00e9 des animaux aquatiques pour l'\u00c9gypte."
        },
        "keywords": {
            "en": "aquatic animals, finfish, molluscs, crustacean, exports, human consumption, certification requirements, aquatic animal health, seafood products, NAAHP, Egypt",
            "fr": "animaux aquatiques, poisson \u00e0 nageoires, mollusques, crustac\u00e9s, exportation,  consommation humaine, exigences de certification, sant\u00e9 des animaux aquatiques, produits du poisson et des produits de la mer, PNSAA, \u00c9gypte"
        },
        "dcterms.subject": {
            "en": "crustaceans,exports,inspection,molluscs,fish,animal health",
            "fr": "crustac\u00e9,exportation,inspection,mollusque,poisson,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-14",
            "fr": "2019-02-14"
        },
        "modified": {
            "en": "2020-01-07",
            "fr": "2020-01-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal export: certification requirements for Egypt",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'\u00c9gypte"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Egypt has revised its requirements for imports of live aquatic animals and fish and seafood for human consumption.</p>\n\n<p>Canada is currently in negotiation with Egypt for export certification of live aquatic animals and fish and seafood for human consumption. For these specific commodities, until such time as a new certificate that includes aquatic animal health requirements is available, shipments exported without a new certificate may be detained in Egypt.</p>\n\n<p><strong>Important Notes:</strong></p>\n\n<ul>\n<li>There may be existing certificates or additional information from the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/egypt-fish-and-seafood/eng/1304301612428/1304301732015\">Fish and Seafood</a> program. However, for the specific commodities above, an aquatic animal health certificate is required.</li>\n<li>If you require any other information on aquatic animal health export certification, please contact your <a href=\"/eng/1300462382369/1365216058692\">CFIA Area Office</a>.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'\u00c9gypte a r\u00e9vis\u00e9 ses exigences sanitaires pour les animaux aquatiques s'appliquant \u00e0 l'importation d'animaux aquatiques vivants et de poissons et produits de la mer destin\u00e9s \u00e0 la consommation humaine.</p>\n\n<p>Le Canada et l'\u00c9gypte sont actuellement en n\u00e9gociation au sujet de la certification des exportations d'animaux aquatiques vivants et de poissons et produits de la mer destin\u00e9s \u00e0 la consommation humaine. En ce qui concerne ces produits particuliers, d'ici \u00e0 ce qu'un certificat comportant les exigences relatives \u00e0 la sant\u00e9 des animaux aquatiques soit disponible, les envois non accompagn\u00e9s d'un certificat sanitaire visant les animaux aquatiques pourraient \u00eatre retenus en \u00c9gypte.</p>\n\n<p><strong>Remarques importantes\u00a0:</strong></p>\n\n<ul>\n<li>Il est possible que d'autres certificats ou des renseignements suppl\u00e9mentaires soient disponibles par le biais du programme de <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/egypte-le-poisson-et-les-produits-de-la-mer/fra/1304301612428/1304301732015\">Poisson et produits de la mer</a>. Cependant, en ce qui a trait aux produits susmentionn\u00e9s, un certificat d'hygi\u00e8ne des animaux aquatiques est requis.</li>\n<li>Pour toute autre demande de renseignements concernant la certification sanitaire des animaux aquatiques destin\u00e9s \u00e0 l'exportation, veuillez communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de l'ACIA</a> de votre Centre op\u00e9rationnel.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}