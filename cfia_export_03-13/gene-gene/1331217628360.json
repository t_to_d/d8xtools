{
    "dcr_id": "1331217628360",
    "title": {
        "en": "Safety of Horse Meat",
        "fr": "Salubrit\u00e9 de la viande chevaline"
    },
    "html_modified": "2024-03-12 3:55:39 PM",
    "modified": "2020-01-20",
    "issued": "2020-01-13 09:40:30",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_specific_products_horsemeat_factsheet_1331217628360_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_specific_products_horsemeat_factsheet_1331217628360_fra"
    },
    "ia_id": "1331225704619",
    "parent_ia_id": "1363459937995",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1363459937995",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Safety of Horse Meat",
        "fr": "Salubrit\u00e9 de la viande chevaline"
    },
    "label": {
        "en": "Safety of Horse Meat",
        "fr": "Salubrit\u00e9 de la viande chevaline"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331225704619",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1363459871729",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/meat-and-poultry-products/horse-meat/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/produits-de-viande-et-de-volaille/viande-chevaline/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Safety of Horse Meat",
            "fr": "Salubrit\u00e9 de la viande chevaline"
        },
        "description": {
            "en": "Industry is responsible for making sure that all meat sold in Canada is safe, as required by the Safe Food for Canadians Act and Regulations (SFCA and SFCR) and the Food and Drugs Act and Regulations (FDA and FDR).",
            "fr": "L'industrie doit s'assurer que toute la viande vendue au Canada est salubre, conform\u00e9ment \u00e0 la Loi sur la salubrit\u00e9 des aliments au Canada et \u00e0 son R\u00e8glement (LSAC et RSAC) et \u00e0 la Loi sur les aliments et drogues et \u00e0 son R\u00e8glement (LAD et RAD)."
        },
        "keywords": {
            "en": "horse meat, phenylbutazone residues, Equine Information Document, Fact Sheet",
            "fr": "viande chevaline, Fiche de renseignements, r\u00e9sidus de ph\u00e9nylbutazone, Document d'information \u00e9quine"
        },
        "dcterms.subject": {
            "en": "consumer protection,consumers,food,food inspection,food labeling,food safety,government information,government publication,inspection,labelling",
            "fr": "protection du consommateur,consommateur,aliment,inspection des aliments,\u00e9tiquetage des aliments,salubrit\u00e9 des aliments,information gouvernementale,publication gouvernementale,inspection,\u00e9tiquetage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-01-13 09:40:30",
            "fr": "2020-01-13 09:40:30"
        },
        "modified": {
            "en": "2020-01-20",
            "fr": "2020-01-20"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Safety of Horse Meat",
        "fr": "Salubrit\u00e9 de la viande chevaline"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Industry's role</h2>\n\n<p>Industry is responsible for making sure that all meat sold in Canada is safe, as required by the <i>Safe Food for Canadians Act</i> and Regulations (SFCA and SFCR) and the <i>Food and Drugs Act</i> and Regulations (FDA and FDR).</p>\n\n<h2>CFIA's role</h2>\n\n<p>The Canadian Food Inspection Agency (CFIA) works closely with the meat industry to ensure they understand and comply with federal food safety requirements.</p>\n\n<p>The CFIA verifies compliance by:</p>\n\n<ul>\n<li class=\"mrgn-bttm-md\">performing daily inspections in all federally licenced slaughter establishments</li>\n<li class=\"mrgn-bttm-md\">randomly testing meat for the presence of pesticides, environmental contaminants and drug residues through a national monitoring program</li>\n<li class=\"mrgn-bttm-md\">observing animals before stunning and post-slaughter for clinical signs of conditions such as arthritis, which can indicate the animal may have been treated with phenylbutazone</li>\n</ul>\n\n<h2>Phenylbutazone (\"bute\")</h2>\n\n<p>Phenylbutazone is a pain reliever and anti-inflammatory commonly used to treat lameness in horses. It belongs to the class of drugs called \"non-steroidal anti-inflammatory drugs.\"</p>\n\n<p>Health Canada regulates the use of veterinary drugs in Canada. Phenylbutazone is approved by Health Canada to be used in horses but is not approved for use in food-producing animals (including horses slaughtered for human consumption).</p>\n\n<p>The agency has zero tolerance for phenylbutazone in food and monitors for residues of this and other veterinary drugs in food.</p>\n\n<p>If the agency determines that there may be a food safety concern related to chemical residues in a particular product, the agency investigates and takes appropriate action based on the human health risk. The decision to conduct a recall for a product containing an unapproved veterinary drug is based on Health Canada's health risk assessment.</p>\n\n<h2>Results of CFIA testing for phenylbutazone residues</h2>\n\n<p>Since 2002, the agency has been regularly testing horse meat for phenylbutazone.</p>\n\n<p>Results show a very high compliance rate for phenylbutazone residues.</p>\n\n<h2>Safeguards in place to verify that there is no phenylbutazone residues in horse meat</h2>\n\n<p>In July 2010, the agency made it mandatory for every horse (domestic or imported) presented for slaughter in Canadian federally regulated equine facilities to have a record of all illnesses, vaccinations and medications given in the previous 6 months. This is referred to as the <a href=\"/eng/1370023131206/1370023203607#e2\">Equine Information Document</a> (EID).</p>\n\n<p>The Equine Information Document is required under the <i>Safe Food for Canadians Regulations</i>. Each document must be reviewed and signed by a CFIA veterinarian.</p>\n\n<p>Horses presented for slaughter in Canada with incomplete EIDs are prevented from being slaughtered for human consumption.</p>\n\n<p>Other countries, including Japan and European Union countries, do their own testing of horse meat imported from Canada. Canada is informed if any food safety issues are identified by these importing countries.</p>\n\n<h2>CFIA enforcement</h2>\n\n<p>Industry is responsible for taking corrective action if phenylbutazone is detected in horse meat. The agency has a range of enforcement options, including product destruction, recall and licence suspension\u00a0\u2013\u00a0to ensure that industry takes effective action in response to residue findings.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>R\u00f4le de l'industrie</h2>\n\n<p>L'industrie doit s'assurer que toute la viande vendue au Canada est salubre, conform\u00e9ment \u00e0 la <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> et \u00e0 son R\u00e8glement (LSAC et RSAC) et \u00e0 la <i>Loi sur les aliments et drogues</i> et \u00e0 son R\u00e8glement (LAD et RAD).</p>\n\n<h2>R\u00f4le de l'ACIA</h2>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) collabore \u00e9troitement avec l'industrie de la viande afin de s'assurer que les intervenants comprennent les exigences f\u00e9d\u00e9rales en mati\u00e8re de salubrit\u00e9 des aliments et s'y conforment.</p>\n\n<p>L'ACIA v\u00e9rifie la conformit\u00e9 de la mani\u00e8re suivante\u00a0:</p>\n\n<ul>\n<li class=\"mrgn-bttm-md\">en effectuant des inspections quotidiennes dans tous les \u00e9tablissements d'abattage ayant obtenu une licence f\u00e9d\u00e9rale;</li>\n<li class=\"mrgn-bttm-md\">en analysant au hasard la viande pour d\u00e9tecter la pr\u00e9sence de pesticides, de contaminants environnementaux et de r\u00e9sidus de m\u00e9dicaments au moyen d'un programme national de surveillance;</li>\n<li class=\"mrgn-bttm-md\">en observant les animaux avant l'\u00e9tourdissement et apr\u00e8s l'abattage pour d\u00e9tecter des signes cliniques de maladies telles que l'arthrite, qui peuvent indiquer que l'animal a peut\u2011\u00eatre \u00e9t\u00e9 trait\u00e9 avec de la ph\u00e9nylbutazone.</li>\n</ul>\n\n<h2>Ph\u00e9nylbutazone</h2>\n\n<p>La ph\u00e9nylbutazone est un analg\u00e9sique et un anti\u2011inflammatoire utilis\u00e9 couramment pour traiter la boiterie chez le cheval. Ce produit appartient \u00e0 la cat\u00e9gorie des \u00ab\u00a0anti\u2011inflammatoires non st\u00e9ro\u00efdiens\u00a0\u00bb.</p>\n\n<p>Sant\u00e9 Canada r\u00e9glemente l'utilisation des m\u00e9dicaments v\u00e9t\u00e9rinaires au Canada. L'utilisation de la ph\u00e9nylbutazone chez le cheval est approuv\u00e9e par Sant\u00e9 Canada, sauf pour les animaux destin\u00e9s \u00e0 l'alimentation (y compris les chevaux abattus pour la consommation humaine).</p>\n\n<p>L'ACIA applique une politique de tol\u00e9rance z\u00e9ro \u00e0 l'\u00e9gard de la pr\u00e9sence de ph\u00e9nylbutazone dans les aliments, et surveille la pr\u00e9sence de r\u00e9sidus de ce m\u00e9dicament et d'autres m\u00e9dicaments v\u00e9t\u00e9rinaires dans les aliments.</p>\n\n<p>Si l'ACIA d\u00e9termine que la pr\u00e9sence de r\u00e9sidus chimiques dans un produit donn\u00e9 pourrait pr\u00e9senter un risque pour la salubrit\u00e9 des aliments, elle m\u00e8ne une enqu\u00eate et prend les mesures qui s'imposent en fonction des risques pour la sant\u00e9 humaine. La d\u00e9cision de proc\u00e9der au rappel d'un produit contenant un m\u00e9dicament v\u00e9t\u00e9rinaire non approuv\u00e9 est fond\u00e9e sur l'\u00e9valuation des risques pour la sant\u00e9 de Sant\u00e9 Canada.</p>\n\n<h2>R\u00e9sultats des analyses effectu\u00e9es par l'ACIA pour d\u00e9tecter les r\u00e9sidus de ph\u00e9nylbutazone</h2>\n\n<p>Depuis 2002, l'ACIA analyse r\u00e9guli\u00e8rement la viande chevaline aux fins de d\u00e9pistage de la ph\u00e9nylbutazone.</p>\n\n<p>Les r\u00e9sultats indiquent un taux de conformit\u00e9 tr\u00e8s \u00e9lev\u00e9 en ce qui a trait aux r\u00e9sidus de ce m\u00e9dicament.</p>\n\n<h2>Mesures de protection mises en place pour v\u00e9rifier que la viande chevaline ne contient pas de r\u00e9sidus de ph\u00e9nylbutazone</h2>\n\n<p>En juillet 2010, l'ACIA a rendu obligatoire l'\u00e9tablissement d'un dossier de toutes les maladies et de tous les vaccins et m\u00e9dicaments administr\u00e9s dans les six mois pr\u00e9c\u00e9dents, et ce, pour tous les chevaux (canadiens ou import\u00e9s) pr\u00e9sent\u00e9s \u00e0 l'abattage dans les installations \u00e9quines agr\u00e9\u00e9es par le gouvernement f\u00e9d\u00e9ral. On d\u00e9signe ce document comme le <a href=\"/fra/1370023131206/1370023203607#e2\">Document d'information \u00e9quine</a> (DIE).</p>\n\n<p>Le Document d'information \u00e9quine est exig\u00e9 en vertu du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>. Chaque document doit \u00eatre examin\u00e9 et sign\u00e9 par un v\u00e9t\u00e9rinaire de l'ACIA.</p>\n\n<p>Les chevaux pr\u00e9sent\u00e9s \u00e0 l'abattage au Canada dont les DIE sont incomplets ne seront pas abattus aux fins de consommation humaine.</p>\n\n<p>D'autres pays, dont le Japon et les pays de l'Union europ\u00e9enne, effectuent leurs propres analyses sur la viande chevaline import\u00e9e du Canada. D'ailleurs, le Canada sera mis au courant de tout probl\u00e8me aff\u00e9rent \u00e0 la salubrit\u00e9 des aliments d\u00e9cel\u00e9 par ces pays importateurs.</p>\n\n<h2>Mesures d'application de la loi de l'ACIA</h2>\n\n<p>Il incombe \u00e0 l'industrie de prendre les mesures qui s'imposent lorsque la pr\u00e9sence de ph\u00e9nylbutazone est d\u00e9tect\u00e9e dans de la viande chevaline. L'Agence dispose d'un \u00e9ventail de mesures d'application de la loi, notamment la destruction ou le rappel du produit et la suspension du permis, pour s'assurer que l'industrie prend des mesures efficaces \u00e0 la suite de la d\u00e9couverte de r\u00e9sidus de ph\u00e9nylbutazone.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}