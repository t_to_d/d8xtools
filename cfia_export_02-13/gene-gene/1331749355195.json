{
    "dcr_id": "1331749355195",
    "title": {
        "en": "Aquatic animal export: certification requirements for Australia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'Australie"
    },
    "html_modified": "2024-02-13 9:47:27 AM",
    "modified": "2020-01-07",
    "issued": "2012-04-02 09:47:02",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_aus_1331749355195_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_exp_cert_aus_1331749355195_fra"
    },
    "ia_id": "1331749501469",
    "parent_ia_id": "1331743129372",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exports",
        "fr": "Exportations"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1331743129372",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Aquatic animal export: certification requirements for Australia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'Australie"
    },
    "label": {
        "en": "Aquatic animal export: certification requirements for Australia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'Australie"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331749501469",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331740343115",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/exporting-aquatic-animals/certification-requirements-by-country/australia/",
        "fr": "/sante-des-animaux/animaux-aquatiques/exporter-des-animaux-aquatiques/exigences-de-certification-par-pays/australie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Aquatic animal export: certification requirements for Australia",
            "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'Australie"
        },
        "description": {
            "en": "This page describes aquatic animal health export certification requirements for Australia",
            "fr": "Cette page d\u00e9crit les exigences de certification d'exportation de sant\u00e9 des animaux aquatiques pour l'Australie"
        },
        "keywords": {
            "en": "aquatic animals, finfish, molluscs, crustacean, exports, human consumption, certification requirements, aquatic animal health, seafood products, NAAHP, Australia",
            "fr": "animaux aquatiques, poisson \u00e0 nageoires, mollusques, crustac\u00e9s, exportation,  consommation humaine,  exigences de certification, sant\u00e9 des animaux aquatiques, produits du poisson et des produits de la mer, PNSAA, Australie"
        },
        "dcterms.subject": {
            "en": "animal health,crustaceans,exports,fish,inspection,molluscs",
            "fr": "sant\u00e9 animale,crustac\u00e9,exportation,poisson,inspection,mollusque"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Aquatic Animal Health Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux aquatiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-04-02 09:47:02",
            "fr": "2012-04-02 09:47:02"
        },
        "modified": {
            "en": "2020-01-07",
            "fr": "2020-01-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Aquatic animal export: certification requirements for Australia",
        "fr": "Exportation d'animaux aquatiques : exigences de certification pour l'Australie"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2>The following certificates are available:</h2>\n\n<ul>\n\n<li>Aquatic Animal Health Certificate for the Export of Aquatic Animals for Use as Bait or Aquaculture Feed from Canada to Australia (AQAH-1012)</li>\n<li>Aquatic Animal Health and Sanitary Certificate for the Export of Fresh, Chilled or Frozen Salmonid Products for Human Consumption from Canada to Australia (AQAH-1063)</li>\n\n<li>Aquatic Animal Health Certificate for the Export of Live Finfish, Their Gametes or Germplasm for Research Purposes from Canada to Australia (AQAH-1075)</li>\n    \n<li>Aquatic Animal Health and Sanitary Certificate for the Export of Thermally Treated Salmonid Products for Human Consumption from Canada to Australia (AQAH-1098)</li>\n\n</ul>\n\n<h2>Important notes:</h2>\n\n<ul>\n\n<li>Certificate (AQAH-1063) only applies to the export of fresh, chilled and frozen salmonid products (not including thermally treated products) for human consumption. This commodity requires an inspection by a CFIA Veterinary Inspector of the products. Exporters are advised that in order to be eligible for certification to Australia they must contact their <a href=\"/eng/1300462382369/1365216058692\">CFIA Area Office</a> at least 5 business days in advance of export to determine the requirements and accommodate the scheduling of inspections.</li>\n<li>At this time, only fresh, chilled and frozen salmonid products of Canadian origin are eligible for export to Australia. Processors will need to provide documentation to demonstrate that they do not import salmon for further processing from countries other than Australia, Denmark, Norway, Republic of Ireland, the United Kingdom, and the United States of America <strong>or</strong> provide adequate records demonstrating effective controls to prevent substitution, comingling or contamination with imported salmonid products from countries other than those listed above. </li>\n<li>Australia has advised that starting September 1, 2019, salmonid products imported in a form that requires further processing in Australia will remain under biosecurity control after arrival at an establishment that has entered into an approved arrangement with the Australian authorities. Salmonid products subject to this requirement will only be released from biosecurity control after it has been transformed into a consumer ready form.</li>\n<li>Salmonid products that meet the Australian definition of a consumer ready form and are packaged and clearly labelled in accordance with this definition will not be subject to these additional biosecurity conditions.</li>\n<li>Exporters of salmonid products that are in a consumer ready form must assume full responsibility for ensuring their consignments meet Australia's consumer ready packaging and labelling conditions, and are advised to work with their importer in advance of export and ensure that their consignments are appropriately labelled to meet the Australian requirements.</li>\n<li>Australia's definition of a salmonid consumer ready form includes:\n<ul>\n<li>cutlets, including the central bone and external skin but excluding fins, each cutlet weighing no more than 450 grams;</li>\n<li>skin-on or skinless fillets, excluding the belly flap and all bone except the pin bones, of any weight;</li>\n<li>eviscerated, headless fish, each fish weighing no more than 450 grams; or</li>\n<li>product that is processed further than described above.</li>\n</ul>\n</li>\n<li>Certificate (AQAH-1098) only applies to the export of thermally treated salmonid products for human consumption. Exporters are advised that in order to be eligible for certification to Australia they must contact their <a href=\"/eng/1300462382369/1365216058692\">CFIA Area Office</a> at least 5 business days in advance of export to determine the requirements.</li>\n<li>Currently, certificate (AQAH-1012) only applies to the export of herring (<i lang=\"la\">Clupea</i> species) for use as bait or aquaculture feed. This commodity requires inspection of the animals prior to processing. Exporters are advised to contact their CFIA Area Office in advance of harvesting to determine the requirements and, if required, accommodate the scheduling of inspections which must occur prior to processing and freezing.</li>\n<li>Australia has aquatic animal health requirements for imports of certain aquatic animals and fish and seafood products. For these specific commodities, until such time as a negotiated aquatic animal health certificate is available, shipments exported without aquatic animal health certification may be detained in Australia.</li>\n<li>There may be existing certificates or additional information from the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/australia/eng/1304361811765/1304361943419\">Fish and Seafood program</a>. However, for the specific commodity above, an aquatic animal health certificate is required.</li>\n<li>To request an export certificate outlined above or if you require any other information on aquatic animal health export certification, please contact your <a href=\"/eng/1300462382369/1365216058692\">CFIA Area Office</a>.</li>\n  \n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=18#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2>Les certificats suivants sont disponibles\u00a0:</h2>\n\n<ul>\n\n<li>Certificat d'hygi\u00e8ne des animaux aquatiques pour l'exportation des animaux aquatiques destin\u00e9s \u00e0 servir d'app\u00e2t ou d'aliment aquacole du Canada vers l'Australie (AQAH-1012)</li>\n<li>Certificat d'hygi\u00e8ne et de sant\u00e9 des animaux aquatiques pour l'exportation des produits de salmonid\u00e9s frais, r\u00e9frig\u00e9r\u00e9s ou congel\u00e9s destin\u00e9s \u00e0 la consommation humaine du Canada vers l'Australie (AQAH-1063)</li>\n<li>Certificat d'hygi\u00e8ne des animaux aquatiques pour l'exportation, du Canada vers l'Australie, de poissons vivants ainsi que leurs gam\u00e8tes ou de leur mat\u00e9riel g\u00e9n\u00e9tique aux fins de recherche (AQAH-1075)</li>\n<li>Certificat d'hygi\u00e8ne et de sant\u00e9 des animaux aquatiques pour l'exportation des produits salmonid\u00e9s trait\u00e9s thermiquement \u00e0 des fins consommation humaine du Canada vers l'Australie (AQAH-1098)</li>\n\n</ul>\n\n<h2>Remarques importantes\u00a0:</h2>\n\n<ul>\n<li>Le certificat (AQAH-1063) s'applique seulement \u00e0 l'exportation de produits de salmonid\u00e9s frais, r\u00e9frig\u00e9r\u00e9s et congel\u00e9s (non compris les produits trait\u00e9s thermiquement) destin\u00e9s \u00e0 la consommation humaine. Ce produit exige qu'un v\u00e9t\u00e9rinaire-inspecteur de l'ACIA effectue une inspection des produits. Pour \u00eatre admissibles \u00e0 la certification des exportations vers l'Australie, les exportateurs doivent communiquer avec leur <a href=\"/fra/1300462382369/1365216058692\">bureau r\u00e9gional de l'ACIA</a> au moins 5 jours ouvrables avant l'exportation afin de d\u00e9terminer les exigences et de planifier les inspections.</li>\n<li>\u00c0 l'heure actuelle, seuls les produits de salmonid\u00e9s frais, r\u00e9frig\u00e9r\u00e9s ou congel\u00e9s d'origine canadienne sont admissibles \u00e0 l'exportation vers l'Australie. Les transformateurs devront fournir des documents pour d\u00e9montrer qu'ils n'importent pas de saumon \u00e0 des fins de transformation ult\u00e9rieure de pays autres que l'Australie, le Danemark, la Norv\u00e8ge, la R\u00e9publique d'Irlande, le Royaume-Uni et les \u00c9tats-Unis OU  fournir des documents ad\u00e9quats qui d\u00e9montrent que des mesures de contr\u00f4le  efficaces ont \u00e9t\u00e9 appliqu\u00e9es pour pr\u00e9venir toute substitution ou contamination  par des produits de salmonid\u00e9s import\u00e9s de pays autres que ceux susmentionn\u00e9s, ainsi que tout m\u00e9lange avec de tels produits. </li>\n<li>L'Australie a indiqu\u00e9 qu'\u00e0 partir du 1<sup>er</sup> septembre 2019 les produits de salmonid\u00e9s import\u00e9s dans une forme qui n\u00e9cessite une transformation suppl\u00e9mentaire en Australie demeureront sous contr\u00f4le de la bios\u00e9curit\u00e9 une fois arriv\u00e9s dans un \u00e9tablissement ayant conclu une \u00ab\u00a0entente approuv\u00e9e\u00a0\u00bb (\u00ab\u00a0approved arrangements\u00a0\u00bb) avec les autorit\u00e9s australiennes. Les produits de salmonid\u00e9s soumis \u00e0 cette exigence seront lib\u00e9r\u00e9s du contr\u00f4le de la bios\u00e9curit\u00e9 une fois transform\u00e9s en un produit pr\u00eat \u00e0 consommer.</li>\n<li>Les produits de salmonid\u00e9s dont le format satisfait \u00e0 la d\u00e9finition de \u00ab\u00a0format pr\u00eat \u00e0 consommer\u00a0\u00bb de l'Australie et qui sont emball\u00e9s et \u00e9tiquet\u00e9s de fa\u00e7on claire conform\u00e9ment \u00e0 cette d\u00e9finition ne seront pas assujettis \u00e0 ces conditions suppl\u00e9mentaires de bios\u00e9curit\u00e9.</li>\n<li>Il incombe enti\u00e8rement aux exportateurs de produits de salmonid\u00e9s dans un format pr\u00eat \u00e0 consommer de veiller \u00e0 ce que leurs envois soient conformes aux conditions de l'Australie en mati\u00e8re d'emballage et d'\u00e9tiquetage des produits pr\u00eats \u00e0 consommer. On recommande aux exportateurs de collaborer avec leurs importateurs avant l'exportation et de s'assurer que leurs envois sont \u00e9tiquet\u00e9s correctement afin de satisfaire aux exigences australiennes.</li>\n<li>Selon la d\u00e9finition australienne, un produit de salmonid\u00e9 pr\u00eat \u00e0 consommer comprend\u00a0:\n<ul>\n<li>des darnes, y compris l'os central et la peau, mais sans les nageoires, chacune pesant moins de 450\u00a0grammes;</li>\n<li>des filets avec ou sans peau, ce qui exclut le filet abdominal et les os, \u00e0 l'exception des ar\u00eates intramusculaires, quel que soit leur poids;</li>\n<li>des poissons \u00e9visc\u00e9r\u00e9s et sans t\u00eate, chacun pesant moins de 450\u00a0grammes; ou produit transform\u00e9 davantage que ce qui est d\u00e9crit pr\u00e9c\u00e9demment.</li>\n</ul>\n</li>\n<li>Le certificat (AQAH-1098) s'applique seulement \u00e0 l'exportation de produits de salmonid\u00e9s trait\u00e9s thermiquement destin\u00e9s \u00e0 la consommation humaine. Pour \u00eatre admissibles \u00e0 la certification des  exportations vers l'Australie, les exportateurs doivent communiquer avec leur <a href=\"/fra/1300462382369/1365216058692\">bureau r\u00e9gional de l'ACIA</a> au moins 5 jours ouvrables avant l'exportation afin de d\u00e9terminer les exigences.</li>\n<li>\u00c0 l'heure actuelle, le certificat (AQAH-1012) vise seulement les exportations de hareng (esp\u00e8ces <i lang=\"la\">Clupea</i>) destin\u00e9 \u00e0 servir d'app\u00e2t ou d'aliment aquacole. Ce produit exige une inspection des animaux avant la transformation. Les exportateurs doivent communiquer avec leur <a href=\"/fra/1300462382369/1365216058692\">bureau r\u00e9gional de l'ACIA</a> avant la r\u00e9cup\u00e9ration afin de d\u00e9terminer les exigences et, le cas \u00e9ch\u00e9ant, afin de faciliter l'\u00e9tablissement d'un calendrier des inspections qui doivent \u00eatre effectu\u00e9es avant la transformation et la cong\u00e9lation.</li>\n<li>L'Australie a des exigences relatives \u00e0 la sant\u00e9 des animaux aquatiques pour les importations de certains animaux aquatiques et poissons et des produits de la mer. Pour ces produits pr\u00e9cis, tant qu'un certificat de sant\u00e9 des animaux aquatiques n\u00e9goci\u00e9 ne sera pas disponible, les envois export\u00e9s sans certification sanitaire des animaux aquatiques peuvent \u00eatre retenus en Australie.</li>\n<li>Il est possible que d'autres certificats ou des renseignements suppl\u00e9mentaires soient disponibles par le biais du programme de <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/australie/fra/1304361811765/1304361943419\">Poisson et produits de la mer</a>. Cependant, en ce qui a trait au produit susmentionn\u00e9, un certificat d'hygi\u00e8ne des animaux aquatiques est requis.</li>\n<li>Pour faire une demande du certificat d'exportation indiqu\u00e9 ci-dessus ou pour toute autre demande de renseignements concernant la certification sanitaire des animaux aquatiques destin\u00e9s \u00e0 l'exportation, veuillez communiquer avec le <a href=\"/fra/1300462382369/1365216058692\">bureau de l'ACIA</a> de votre Centre op\u00e9rationnel.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}