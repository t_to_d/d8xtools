{
    "dcr_id": "1326465917763",
    "title": {
        "en": "Light brown apple moth\u00a0\u2013\u00a0Epiphyas postvittana",
        "fr": "Pyrale brun p\u00e2le de la pomme\u00a0\u2013\u00a0Epiphyas postvittana"
    },
    "html_modified": "2024-03-12 3:55:31 PM",
    "modified": "2022-07-13",
    "issued": "2012-01-13 09:45:18",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_lbampbpp_index_1326465917763_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_lbampbpp_index_1326465917763_fra"
    },
    "ia_id": "1326466022858",
    "parent_ia_id": "1307078272806",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1307078272806",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Light brown apple moth\u00a0\u2013\u00a0Epiphyas postvittana",
        "fr": "Pyrale brun p\u00e2le de la pomme\u00a0\u2013\u00a0Epiphyas postvittana"
    },
    "label": {
        "en": "Light brown apple moth\u00a0\u2013\u00a0Epiphyas postvittana",
        "fr": "Pyrale brun p\u00e2le de la pomme\u00a0\u2013\u00a0Epiphyas postvittana"
    },
    "templatetype": "content page 1 column",
    "node_id": "1326466022858",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1307077188885",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/light-brown-apple-moth/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/pyrale-brun-pale-de-la-pomme/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Light brown apple moth\u00a0\u2013\u00a0Epiphyas postvittana",
            "fr": "Pyrale brun p\u00e2le de la pomme\u00a0\u2013\u00a0Epiphyas postvittana"
        },
        "description": {
            "en": "Light brown apple moth (LBAM) is a regulated pest for Canada. It affects many different plant species and has successfully invaded new areas, including the State of California where it was a regulated pest until December 17, 2021.",
            "fr": "La pyrale brun p\u00e2le de la pomme est un organisme nuisible r\u00e9glement\u00e9 au Canada. Elle affecte de nombreuses esp\u00e8ces v\u00e9g\u00e9tales diff\u00e9rentes et a envahi avec succ\u00e8s de nouvelles r\u00e9gions, y compris l'\u00c9tat de Californie o\u00f9 elle \u00e9tait un organisme nuisible r\u00e9glement\u00e9 jusqu'au 17 d\u00e9cembre 2021."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, light brown apple moth, LBAM",
            "fr": "phytoravageur, enqu\u00eates phytosanitaires, organismes de quarantaine, pi\u00e9geage, pyrale brun p\u00e2le de la pomme"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-13 09:45:18",
            "fr": "2012-01-13 09:45:18"
        },
        "modified": {
            "en": "2022-07-13",
            "fr": "2022-07-13"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Light brown apple moth\u00a0\u2013\u00a0Epiphyas postvittana",
        "fr": "Pyrale brun p\u00e2le de la pomme\u00a0\u2013\u00a0Epiphyas postvittana"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Light brown apple moth (LBAM) is a regulated pest for Canada. It affects many different plant species and has successfully invaded new areas, including the State of California where it was a regulated pest until December 17, 2021. It is likely to be able to survive out-of-doors along the west coast of British Columbia, and in greenhouses across the country. If introduced, damage to Canada's commercial fruit, vegetable and ornamental industries is anticipated. Gardens in at-risk areas may also be affected. LBAM poses no threat to human or animal health.</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) is currently reviewing the regulatory status of light brown apple moth. During the summer of 2022 the CFIA will be consulting with stakeholders on a risk management document. The import requirements described in directive D-07-03 remain in effect until a regulatory decision is finalized.</p>\n\n<h2>What information is available?</h2>\n\n<ul>\n<li>Plant protection directive\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/horticulture/d-07-03/eng/1323873365233/1323873449501\">D-07-03: Phytosanitary Import Requirements to Prevent the Entry of <i lang=\"la\">Epiphyas postvittana</i> (light brown apple moth)</a></li>\n</ul></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La pyrale brun p\u00e2le de la pomme est un organisme nuisible r\u00e9glement\u00e9 au Canada. Elle affecte de nombreuses esp\u00e8ces v\u00e9g\u00e9tales diff\u00e9rentes et a envahi avec succ\u00e8s de nouvelles r\u00e9gions, y compris l'\u00c9tat de Californie o\u00f9 elle \u00e9tait un organisme nuisible r\u00e9glement\u00e9 jusqu'au 17 d\u00e9cembre 2021. Elle est susceptible de pouvoir survivre \u00e0 l'ext\u00e9rieur, le long de la c\u00f4te ouest de la Colombie-Britannique, et dans les serres \u00e0 travers le pays. Si elle est introduite, des dommages aux industries commerciales des fruits, des l\u00e9gumes et des plantes ornementales du Canada sont anticip\u00e9s. Les jardins situ\u00e9s dans des zones \u00e0 risque peuvent \u00e9galement \u00eatre touch\u00e9s. La pyrale brun p\u00e2le de la pomme ne pr\u00e9sente aucune menace pour la sant\u00e9 humaine ou animale.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) examine actuellement le statut r\u00e9glementaire de la pyrale brun p\u00e2le de la pomme. Au cours de l'\u00e9t\u00e9 2022 l'ACIA consultera les intervenants sur un document de gestion des risques. Les exigences d'importation d\u00e9crites dans la directive D-07-03 restent en vigueur jusqu'\u00e0 ce qu'une d\u00e9cision r\u00e9glementaire soit finalis\u00e9e.</p>\n\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition?</h2>\n\n<ul>\n<li>Directives sur la protection des v\u00e9g\u00e9taux\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/horticulture/d-07-03/fra/1323873365233/1323873449501\">D-07-03\u00a0: Exigences phytosanitaires d'importation visant \u00e0 pr\u00e9venir l'introduction de <i lang=\"la\">Epiphyas postvittana</i> (pyrale brun p\u00e2le de la pomme)</a></li>\n</ul></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}