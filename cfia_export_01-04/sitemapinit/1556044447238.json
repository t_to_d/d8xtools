{
    "dcr_id": "1556044447006",
    "title": {
        "en": "Travellers: Don't be a carrier of African swine fever",
        "fr": "Voyageurs\u00a0: Ne devenez pas transporteur de la peste porcine africaine"
    },
    "html_modified": "25-4-2019",
    "modified": "9-3-2022",
    "issued": "25-4-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/traveller_carrier_asf_1556044447006_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/traveller_carrier_asf_1556044447006_fra"
    },
    "parent_ia_id": "1306983373952",
    "ia_id": "1556044447238",
    "parent_node_id": "1306983373952",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Travellers: Don't be a carrier of African swine fever",
        "fr": "Voyageurs\u00a0: Ne devenez pas transporteur de la peste porcine africaine"
    },
    "label": {
        "en": "Travellers: Don't be a carrier of African swine fever",
        "fr": "Voyageurs\u00a0: Ne devenez pas transporteur de la peste porcine africaine"
    },
    "templatetype": "content page 1 column",
    "node_id": "1556044447238",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1306983245302",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/travellers/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/voyageurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Travellers: Don't be a carrier of African swine fever",
            "fr": "Voyageurs\u00a0: Ne devenez pas transporteur de la peste porcine africaine"
        },
        "description": {
            "en": "Whether you travel for work or pleasure, you have a role to play in preventing the spread of foreign animal diseases like African swine fever (ASF).",
            "fr": "Que vous voyagiez par plaisir ou par affaires, vous avez un r\u00f4le \u00e0 jouer pour pr\u00e9venir la propagation de maladies animales exotiques comme la peste porcine africaine (PPA)."
        },
        "keywords": {
            "en": "Reportable Diseases Regulations, African swine fever, African swine, contagious viral disease of swine, reportable disease, Government of Canada, travellers, carrier, farm workers, foreign exchange students, hunters",
            "fr": "R\u00e9glementation en mati\u00e8re de maladies \u00e0 d\u00e9claration obligatoire, Peste porcine africaine, Porc africain, maladie virale contagieuse du porc, maladie \u00e0 d\u00e9claration obligatoire, gouvernement du Canada, voyageurs, transporteur, ouvriers agricoles, \u00e9tudiants \u00e9trangers, chasseurs"
        },
        "dcterms.subject": {
            "en": "animal,inspection,animal inspection,animal diseases,animal health",
            "fr": "animal,inspection,inspection des animaux,maladie animale,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-04-25",
            "fr": "2019-04-25"
        },
        "modified": {
            "en": "2022-03-09",
            "fr": "2022-03-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Travellers: Don't be a carrier of African swine fever",
        "fr": "Voyageurs\u00a0: Ne devenez pas transporteur de la peste porcine africaine"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Whether you travel for work or pleasure, you have a role to play in preventing the spread of foreign animal diseases like African swine fever (ASF). People such as farm workers, foreign exchange students or hunters who travel to countries infected with ASF could bring back contaminated food, clothing and equipment.</p>\n\n<h2>Here's how you can help</h2>\n\n<ul>\n<li>Wash or dispose of all clothing and footwear worn while travelling to a country that is infected with ASF before re-entering Canada</li>\n<li>Declare all animal and food products at the border: neglecting to do so could result in monetary penalties of up to $1300</li>\n<li>Declare it at the border if you've visited a farm abroad</li>\n<li>If you've visited a farm in an ASF-infected country you should not visit a pig farm in Canada for at least 14 days</li>\n</ul>\n\n<p>Refer to our <a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/fact-sheet/eng/1306993248674/1306993787261\">frequently asked questions about ASF</a> to inform yourself about this animal disease.</p>\n\n<h2>Awareness toolkit</h2>\n\n<p>Everyone has a role to play in reducing the risk of animal diseases. Do your part by sharing this toolkit with your network. You'll find helpful videos, fact sheets, posters and images to help spread the word, not the animal disease.</p>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/traveller-pamphlet/eng/1548870266507/1548870266819\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/travellers_guide_asf_1556041579999_eng.png\" class=\"img-responsive thumbnail\" alt=\"Pamphlet: Traveller's guide to protect Canada from foreign animal diseases\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-list-alt\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/traveller-pamphlet/eng/1548870266507/1548870266819\">Pamphlet: Traveller's guide to protect Canada from foreign animal diseases</a></strong></p>\n<p>Declare all animal and food products at the border and take precautions when visiting farms.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/importing-food-plants-or-animals/pets/infographic-what-you-need-to-know-if-you-plan-to-t/eng/1516994052131/1516994052834\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/travelling_pigs_asf_1556033937376_eng.jpg\" class=\"img-responsive thumbnail\" alt=\"Infographic: What you need to know if you plan to travel with a pig\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/importing-food-plants-or-animals/pets/infographic-what-you-need-to-know-if-you-plan-to-t/eng/1516994052131/1516994052834\">Infographic: What you need to know if you plan to travel with a pig</a></strong></p>\n<p>All varieties of pigs, whether kept as pets, companion animals or farm animals, are susceptible to the same serious diseases.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/economic-impact/eng/1546889082597/1546889294408\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/economic_impact_asf_1556033444182_eng.png\" class=\"img-responsive thumbnail\" alt=\"Infographic: Economic impact\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/economic-impact/eng/1546889082597/1546889294408\">Infographic: Economic impact</a></strong></p>\n<p>African swine fever poses a significant risk to the Canadian pork industry and the Canadian economy.</p>\n</div>\n</div>\n\n<h2>Awareness tools from third parties</h2>\n\n<ul>\n<li><a href=\"https://www.woah.org/en/disease/african-swine-fever/#ui-id-2\">WOAH situation reports for African swine fever</a></li>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE)) awareness tools</a></li>\n<li><a href=\"https://www.youtube.com/watch?time_continue=7&amp;v=eyQ4t1wHl2M\">Video: African swine fever\u00a0\u2013\u00a0How to stay one step ahead</a></li>\n<li><a href=\"https://www.baf.com.fj/wp-content/uploads/2021/05/EN_Poster_ASF_OIE_Final.pdf\">Travellers: Don't be the carrier of a deadly pig disease\u00a0\u2013\u00a0PDF (357\u00a0kb)</a></li>\n<li><a href=\"https://trello-attachments.s3.amazonaws.com/5c501da3a4ba4c2c50ce308d/5c5024e676daee5cf787f762/b860e5071893f33b4b93c0e8096a6e48/EN_Poster_ASF_TransportAuthorities.pdf\">Transport authorities and check point staff: Don't overlook a deadly pig disease\u00a0\u2013\u00a0PDF (193 kb)</a></li>\n</ul>\n\n<h2>Resources for travellers</h2>\n\n<ul>\n<li><a href=\"/eng/1389648337546/1389648516990\">What can I bring into Canada in terms of food, plant, animal and related products</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/countries-recognized-as-free-from-the-disease/eng/1314998276518/1318975777754\">Countries recognized as being free from the disease</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Que vous voyagiez par plaisir ou par affaires, vous avez un r\u00f4le \u00e0 jouer pour pr\u00e9venir la propagation de maladies animales exotiques comme la peste porcine africaine (PPA). Des personnes comme les travailleurs de fermes d'\u00e9levage, les \u00e9tudiants participant \u00e0 un programme d'\u00e9change et les chasseurs, qui voyagent vers des pays qui sont infect\u00e9s de la PPA, pourraient rapporter des aliments, des v\u00eatements et de l'\u00e9quipement contamin\u00e9s.</p>\n\n<h2>Voici comment vous pouvez aider</h2>\n\n<ul>\n<li>Lavez ou \u00e9liminez tous les v\u00eatements et les chaussures port\u00e9s lors d'un voyage dans un pays qui est contamin\u00e9 de la PPA avant de revenir au Canada.</li>\n<li>D\u00e9clarez tous les produits alimentaires et d'origine animale \u00e0 la fronti\u00e8re. N\u00e9gliger de faire cela pourrait entra\u00eener une sanction s'\u00e9levant jusqu'\u00e0 <span class=\"nowrap\">1 300 $.</span></li>\n<li>D\u00e9clarez \u00e0 la fronti\u00e8re si vous avez visit\u00e9 une ferme \u00e0 l'\u00e9tranger.</li>\n<li>Si vous avez visit\u00e9 une ferme dans un pays contamin\u00e9 par la PPA, ne visitez pas d'exploitation porcine au Canada pendant au moins 14 jours.</li>\n</ul>\n\n<p>Veuillez consulter notre <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fiche-de-renseignements/fra/1306993248674/1306993787261\">Foire aux questions au sujet de la PPA</a> pour obtenir des renseignements au sujet de cette maladie animale.</p>\n\n<h2>Trousse d'outils de sensibilisation</h2>\n\n<p>Nous avons tous un r\u00f4le \u00e0 jouer dans la r\u00e9duction des risques associ\u00e9s aux maladies animales. Faites votre part en partageant cette trousse d'outils dans votre r\u00e9seau. Vous y retrouverez des vid\u00e9os utiles, des fiches de renseignements, des affiches et des images pour vous aider \u00e0 transmettre des renseignements\u00a0\u2013\u00a0plut\u00f4t que des maladies animales.</p>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/brochure-pour-les-voyageurs/fra/1548870266507/1548870266819\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/travellers_guide_asf_1556041579999_fra.png\" class=\"img-responsive thumbnail\" alt=\"Brochure : Guide pour les voyageurs pour prot\u00e9ger le Canada contre les maladies animales exotiques\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-list-alt\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/brochure-pour-les-voyageurs/fra/1548870266507/1548870266819\">Brochure\u00a0: Guide pour les voyageurs pour prot\u00e9ger le Canada contre les maladies animales exotiques</a></strong></p>\n<p>D\u00e9clarez tous les produits alimentaires et d'origine animale \u00e0 la fronti\u00e8re et prenez des pr\u00e9cautions lorsque vous visitez des fermes.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/infographie/fra/1516994052131/1516994052834\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/travelling_pigs_asf_1556033937376_fra.jpg\" class=\"img-responsive thumbnail\" alt=\"Infographie : Ce que vous devez savoir si vous pr\u00e9voyez voyager avec un cochon\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/infographie/fra/1516994052131/1516994052834\">Infographie\u00a0: Ce que vous devez savoir si vous pr\u00e9voyez voyager avec un cochon</a></strong></p>\n<p>Toutes les races de cochons, qu'il s'agisse d'animaux de compagnie ou de ferme, sont vuln\u00e9rables aux m\u00eames maladies graves.</p>\n</div>\n</div>\n\n<div class=\"row mrgn-tp-lg\">\n<div class=\"col-sm-5\"><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/impact-economique/fra/1546889082597/1546889294408\"><img src=\"/DAM/DAM-animals-animaux/STAGING/images-images/economic_impact_asf_1556033444182_fra.png\" class=\"img-responsive thumbnail\" alt=\"Infographie : Impact \u00e9conomique\"></a></div>\n<div class=\"col-sm-7\">\n<p><span class=\"glyphicon glyphicon-picture\"></span>\u00a0<strong><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/impact-economique/fra/1546889082597/1546889294408\">Infographie\u00a0: Impact \u00e9conomique</a></strong></p>\n<p>La peste porcine africaine pose un risque significatif \u00e0 l'industrie porcine canadienne et \u00e0 l'\u00e9conomie canadienne.</p>\n</div>\n</div>\n\n<h2>Autres outils de sensibilisation provenant de tiers</h2>\n\n<ul>\n<li><a href=\"https://www.woah.org/fr/maladie/peste-porcine-africaine/\">Rapports de situation de l\u2019Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)) sur la peste porcine africaine</a></li>\n<li><a href=\"https://trello.com/b/GloiZoik/african-swine-fever-oie\">Outils de sensibilisation de l'OMSA</a></li>\n<li><a href=\"https://www.youtube.com/watch?time_continue=7&amp;v=eyQ4t1wHl2M\">Vid\u00e9o\u00a0: <span lang=\"en\">African swine fever\u00a0\u2013\u00a0How to stay one step ahead</span> (en anglais seulement)</a></li>\n<li><a href=\"https://trello-attachments.s3.amazonaws.com/5c501da3a4ba4c2c50ce308d/5c5024ea824fb827abe2d4e9/34d7b1820b2aab24d0d50695f181f4de/FR_Poster_ASF_travellers.pdf\">Voyageurs\u00a0: Ne soyez pas le transporteur d'une maladie mortelle pour les porcs\u00a0\u2013\u00a0PDF (184 ko)</a></li>\n<li><a href=\"https://trello-attachments.s3.amazonaws.com/5c501da3a4ba4c2c50ce308d/5c5024e676daee5cf787f762/62ec716600232375de9880021d3cbe44/FR_Poster_ASF_TransportAuthorities.pdf\">Autorit\u00e9s des transports et agents des points de contr\u00f4le\u00a0: Ne laissez pas passer une maladie mortelle pour les porcs\u00a0\u2013\u00a0PDF (206 ko)</a></li>\n</ul>\n\n<h2>Ressources pour les voyageurs</h2>\n\n<ul>\n<li><a href=\"/fra/1389648337546/1389648516990\">Quels aliments, v\u00e9g\u00e9taux, animaux et produits connexes puis-je rapporter au Canada?</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/pays-reconnus-indemnes-de-la-maladie/fra/1314998276518/1318975777754\">Pays reconnus par le Canada comme \u00e9tant indemnes de la peste porcine africaine</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}