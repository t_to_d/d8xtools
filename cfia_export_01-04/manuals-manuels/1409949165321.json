{
    "dcr_id": "1409949165321",
    "title": {
        "en": "Nutrition labelling compliance test",
        "fr": "Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel"
    },
    "html_modified": "12-6-2015",
    "modified": "17-2-2023",
    "issued": "5-9-2014",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/manuals-manuels/data/lble_industry_nutrition_label_toc_additionalinfo_compliance_1409949165321_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/manuals-manuels/data/lble_industry_nutrition_label_toc_additionalinfo_compliance_1409949165321_fra"
    },
    "parent_ia_id": "1394548687164",
    "ia_id": "1409949250097",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Nutrition labelling compliance test",
        "fr": "Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel"
    },
    "label": {
        "en": "Nutrition labelling compliance test",
        "fr": "Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel"
    },
    "templatetype": "content page 1 column",
    "node_id": "1409949250097",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "parent_dcr_id": "1394548564013",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Nutrition labelling compliance test",
            "fr": "Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel"
        },
        "description": {
            "en": "The purpose of the CFIA Compliance Test is to provide a transparent, science-based system for verifying the accuracy of nutrient values on labels and in advertising via laboratory analysis as part of assessing compliance with the Food and Drug Regulations.",
            "fr": "Le Test de conformit\u00e9 utilis\u00e9 par l'ACIA a pour objectif d'offrir un syst\u00e8me scientifique et transparent, qui permet d'\u00e9valuer l'exactitude des donn\u00e9es nutritionnelles sur les \u00e9tiquettes et dans la publicit\u00e9 au moyen d'une analyse en laboratoire, dans le cadre de la v\u00e9rification de la conformit\u00e9 avec le R\u00e8glement sur les aliments et drogues."
        },
        "keywords": {
            "en": "labels, labelling, nutrition labelling compliance test, nutrition labelling, nutrient content claims, health claims, nutrient values",
            "fr": "etiquette, \u00e9tiquetage, \u00c9tiquetage nutritionnel, all\u00e9gations nutritionnelles,  all\u00e9gations relatives \u00e0 la sant\u00e9, test de conformit\u00e9 de l?\u00e9tiquetage nutritionnel"
        },
        "dcterms.subject": {
            "en": "food,retail trade,consumers,food labeling,inspection,agri-food products,consumer protection",
            "fr": "aliment,commerce de d\u00e9tail,consommateur,\u00e9tiquetage des aliments,inspection,produit agro-alimentaire,protection du consommateur"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-06-12",
            "fr": "2015-06-12"
        },
        "modified": {
            "en": "2023-02-17",
            "fr": "2023-02-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public",
            "fr": "entreprises,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Nutrition labelling compliance test",
        "fr": "Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=0\">Complete text</a></p>\r\n\r\n\r\n<h2>On this page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=1\">Summary</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2\">Part 1 - Nutrition labelling compliance test</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s1c2\">Purpose and scope</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s2c2\">Guiding principles</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s3c2\">Statistical basis</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s4c2\">Definitions</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s5c2\">Nutrient definitions</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s6c2\">Methods of analysis</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s7c2\">Application of rounding rules</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s8c2\">Classes of nutrients</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s9c2\">Sampling</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s10c2\">Acceptance criteria for lot compliance</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s11c2\">Tolerances (Criterion 2)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s12c2\">Use of data bases</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s13c2\">Implementation</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s14c2\">Table - Sampling plan and tolerances</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=2#s15c2\">Examples</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3\">Part 2 - Analysis and feedback</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s16c3\">Introduction and purpose</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s17c3\">Background</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s18c3\">International situation</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s19c3\">Description of nutrition labelling compliance test</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s20c3\">Statistical framework</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s21c3\">Nutrition labelling compliance test and rounding rules</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s22c3\">Criteria</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s23c3\">Alternatives considered</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s24c3\">Feedback</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=3#s25c3\">References</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=4\">Appendix 1 - International context</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=5\">Appendix 2 - Statistical framework</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=6\">Appendix 3 - Rounding of nutrition facts table information</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/food-labels/labelling/industry/nutrition-labelling/additional-information/compliance-test/eng/1409949165321/1409949250097?chap=7\">Appendix 4 - Laboratory issues</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=0\">Texte complet</a></p>\r\n\r\n\r\n<h2>Sur cette page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=1\">R\u00e9sum\u00e9</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2\">Partie 1 - Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s1c2\">Objet et port\u00e9e</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s2c2\">Principes directeurs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s3c2\">Fondements statistiques</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s4c2\">D\u00e9finitions</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s5c2\">D\u00e9finitions des \u00e9l\u00e9ment nutritifs\u00a0</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s6c2\">M\u00e9thodes d'analyse</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s7c2\">Application des r\u00e8gles d'arrondissement des valeurs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s8c2\">Classification des \u00e9l\u00e9ments nutritifs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s9c2\">\u00c9chantillonnage</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s10c2\">Crit\u00e8res d'acceptation de la conformit\u00e9 d'un lot</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s11c2\">Marges de tol\u00e9rance (crit\u00e8re\u00a02)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s12c2\">Utilisation des banques de donn\u00e9es</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s13c2\">Application</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s14c2\">Tableau - Plan d'\u00e9chantillonnage et marges de tol\u00e9rance</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=2#s15c2\">Exemples</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3\">Partie 2 - Analyse et consultations</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s16c3\">Introduction et objet</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s17c3\">Contexte</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s18c3\">Situation dans le monde</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s19c3\">Description du Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s20c3\">D\u00e9marche statistique</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s21c3\">Test de conformit\u00e9 de l'\u00e9tiquetage nutritionnel et r\u00e8gles d'arrondissement</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s22c3\">Crit\u00e8res</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s23c3\">Autres solutions \u00e0 l'\u00e9tude</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s24c3\">Consultations</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=3#s25c3\">R\u00e9f\u00e9rences bibliographiques</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=4\">Annexe 1 - Contexte international</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=5\">Annexe 2 - D\u00e9marche statistique</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=6\">Annexe 3 - R\u00e8gles d'arrondissement qui s'appliquent aux renseignements du tableau de la valeur nutritive</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/renseignements-additionnels/test-de-conformite/fra/1409949165321/1409949250097?chap=7\">Annexe 4 - L'\u00e9tiquetage nutritionnel et les laboratoires</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}