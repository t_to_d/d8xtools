{
    "dcr_id": "1328856118287",
    "title": {
        "en": "List of Responsible Administrators",
        "fr": "Liste des administrateurs responsables"
    },
    "html_modified": "2012-02-10 01:42",
    "modified": "9-6-2022",
    "issued": "10-2-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/terr_anima_trace_admin_1328856118287_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/terr_anima_trace_admin_1328856118287_fra"
    },
    "parent_ia_id": "1300461804752",
    "ia_id": "1328856205206",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "List of Responsible Administrators",
        "fr": "Liste des administrateurs responsables"
    },
    "label": {
        "en": "List of Responsible Administrators",
        "fr": "Liste des administrateurs responsables"
    },
    "templatetype": "content page 1 column",
    "node_id": "1328856205206",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1300461751002",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/traceability/administrators/",
        "fr": "/sante-des-animaux/animaux-terrestres/tracabilite/administrateurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "List of Responsible Administrators",
            "fr": "Liste des administrateurs responsables"
        },
        "description": {
            "en": "People who are in the possession, care or control of an animal, or the carcass of an animal, must report a certain type of animal identification and movement information to the administrator.",
            "fr": "Les personnes qui sont en possession d'un animal, ou qui sont charg\u00e9s du soin ou du contr\u00f4le d'un animal, ou d'une carcasse d'animal, doivent d\u00e9clarer \u00e0 l'administrateur certains types de renseignements sur l'identification et le d\u00e9placement de l'animal."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, adminstrators, traceability, surveillance, animal health, disease, detection, identification",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, programme d'identification du b\u00e9tail, tra\u00e7abilit\u00e9, surveillance, sant\u00e9 animal, maladie, d\u00e9tection, identification"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,standards,regulation,animal health",
            "fr": "b\u00e9tail,inspection,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-10 01:42:00",
            "fr": "2012-02-10 01:42:00"
        },
        "modified": {
            "en": "2022-06-09",
            "fr": "2022-06-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "List of Responsible Administrators",
        "fr": "Liste des administrateurs responsables"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/eng/1672964761819\"></div>\n\n<p>Federally, the Health of Animals Act governs livestock traceability activities in Canada. Part XV of the Health of Animals Regulations outlines the identification and movement reporting requirements for cattle, bison, sheep and pigs.</p>\n\n<p>Under the regulations, people who are in the possession, care or control of an animal, or the carcass of an animal, must report certain types of animal identification and movement information to the responsible administrator.</p>\n\n<p>Under Part XV, the \"responsible administrator\" is defined as \"<q>a person who is authorized by the Minister to receive information in relation to animals or things to which the Act or these Regulations apply, is listed on the Agency's web site as an administrator and administers a national identification program in relation to certain animals of all or part of one or more genera, species or subspecies that are located in one or more provinces</q>\" (definition amended <span class=\"nowrap\">on 1 July 2014).</span></p>\n\n<h2>Responsible Administrators</h2>\n\n<h3>Bison, beef bovine, and ovine</h3>\n\n<p>The Canadian Cattle Identification Agency (CCIA) is the administrator responsible for bison (animal, other than an embryo or a fertilized egg, of the subspecies <i lang=\"la\">Bison bison bison</i>, <i lang=\"la\">Bison bison athabascae</i> or <i lang=\"la\">Bison bison bonasus</i>), beef bovine (animal, other than an embryo or a fertilized egg, of the species <i lang=\"la\">Bos taurus</i> or <i lang=\"la\">Bos indicus</i>) and ovine (an animal, other than an embryo or a fertilized egg, of the genus <i lang=\"la\">Ovis</i>) under the livestock identification and traceability program. If you are a regulated party and would like more information on bison, beef bovine or ovine identification and movement reporting requirements, please contact the CCIA.</p>\n\n<p><a href=\"https://www.canadaid.ca/\">Canadian Cattle Identification Agency</a><br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@canadaid.ca\" class=\"nowrap\">info@canadaid.ca</a></p>\n\n<p>Regulated parties may report bison, bovine and ovine identification and movement information to the Canadian Livestock Tracking System (CLTS), which is the database managed by the CCIA.</p>\n\n<ul>\n\n<li><a href=\"https://www.clts.canadaid.ca/CLTS/secure/user/home.do\">Canadian Livestock Tracking System (CLTS)</a></li>\n<li><a href=\"http://support.canadaid.ca/\">Resource Centre: Canadian Livestock Tracking System</a></li>\n</ul>\n\n\n<h3>Dairy bovine</h3>\n\n<p>Lactanet Canada is the administrator responsible for dairy bovine (animal, other than an embryo or a fertilized egg, of the species <i lang=\"la\">Bos taurus</i> or <i lang=\"la\">Bos indicus</i>) under the livestock identification and traceability program. If you are a regulated party and would like more information on dairy bovine identification and movement reporting requirements, please contact DairyTrace.</p>\n\n<p><a href=\"https://www.lactanet.ca/en/home/\">Lactanet Canada</a></p>\n\n<p><span lang=\"fr\">Sainte-Anne-De-Bellevue</span> Office<br>\n<span lang=\"fr\">des Anciens-Combattants</span> Blvd.,<br>\n<span lang=\"fr\">Sainte-Anne-de-Bellevue</span>,<br>\n<br>\n<span lang=\"fr\">BON-LAIT</span><br>\n<a href=\"mailto:info@valacta.com\" class=\"nowrap\">info@valacta.com</a></p>\n\n<p>Guelph Office<br>\n<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:dcsupport@lactanet.ca\" class=\"nowrap\">dcsupport@lactanet.ca</a></p>\n\n<p>Regulated parties may report bovine identification and movement information to DairyTrace, which is the database managed by Lactanet Canada.</p>\n\n<ul>\n<li><a href=\"https://dairytrace.ca\">DairyTrace</a></li>\n<li><a href=\"https://dairytrace.ca/support/customer-service/\">DairyTrace Customer Service</a></li>\n</ul>\n\n<h3>Two administrators for bovine species</h3>\n\n<p>With the addition of Lactanet Canada as the responsible administrator for dairy bovine in Canada and CCIA as the responsible administrator for beef bovine, there are now two administrators for bovine species. This may seem confusing at first, but regulated parties should know:</p>\n\n<ol class=\"lst-spcd\">\n<li>regulated parties may continue to report bovine regulatory data to the CLTS database, or DairyTrace, or SimpliTrace (within Quebec); and</li>\n<li> approved indicators (tags) are approved for use in bovine species under the Livestock Identification and Tractability program, regardless of livestock practices.</li>\n</ol>\n\n<h3>Pigs</h3>\n\n<p>The Canadian Pork Council (CPC) is the administrator responsible for pigs (an animal, other than an embryo or a fertilized egg, of the genus <i lang=\"la\">Sus</i>) under the livestock identification and traceability program. If you are a regulated party and would like more information on pig identification and movement reporting requirements, please contact PigTrace Canada, which is a program managed by the CPC, or one of the provincial pork producers association listed below.</p>\n\n<p><a href=\"https://www.cpc-ccp.com/traceability\">PigTrace Canada</a></p>\n\n<p>Canadian Pork Council Head Office<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@cpc-ccp.com\" class=\"nowrap\">info@cpc-ccp.com</a></p>\n\n<p>Canadian Pork Council PigTrace Office<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@cpc-ccp.com\" class=\"nowrap\">info@cpc-ccp.com</a></p>\n\n<p>Customer Service and Tag Orders: 1-866-300-1825<br>\n<span class=\"nowrap\">9:30 a.m. to 4:30 p.m.</span> (Eastern Standard Time), Monday to Thursday</p>\n\n<p>Regulated parties may report pig identification and movement information to the PigTrace Canada information system:</p>\n\n<p><a href=\"https://pigtrace.traceability.ca/login\">PigTrace Canada information system</a></p>\n\n<h3>Provincial pork producers associations</h3>\n\n<p>British Columbia Pork Producers Association: 604-287-4647<br>\n<br>\n<br>\n<br>\n<br>\n<span lang=\"fr\">Les \u00c9leveurs de porcs du Qu\u00e9bec</span>: 450-679-0540<br>\n<br>\n<br>\n</p>\n\n<h2>Organization that manages an animal identification system</h2>\n\n<p>Under Part XV of the Health of Animals Regulations, references are made an \"organization that manages an animal identification system\". Such a reference is made to an organization that is not officially recognized as an administrator, as it has not signed an agreement with the Minister, nor does it deliver a national animal identification program, as stipulated by the aforementioned definition.</p>\n\n<p>Attestra meets the criteria for an \"organization that manages an animal identification and livestock traceability system\". As such, bovine, ovine and cervid producers located in Quebec report identification and movement information specified under Part XV to the Attestra database.</p>\n\n<p><a href=\"https://attestra.com/en/\">Attestra</a><br>\n<br>\n<span lang=\"fr\">Qu\u00e9bec</span>) Canada J4H 4E8</p>\n\n<p>Customer service provide by phone: 1-866-270-4319<br>\n<span class=\"nowrap\">from 7:30 a.m. to 4:30 p.m.</span> (Eastern Standard Time)</p>\n\n<p>Customer service provide by fax: 1-866-473-4033<br>\n</p>\n\n<p>Regulated parties in Quebec may report animal identification and movement information to the <a href=\"https://simplitrace.atq.qc.ca/login\">Attestra database</a>.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/fra/1672964761819\"></div>\n\n<p>Sur le plan f\u00e9d\u00e9ral, la Loi sur la sant\u00e9 des animaux r\u00e9git les activit\u00e9s de tra\u00e7abilit\u00e9 du b\u00e9tail au Canada. La partie XV du R\u00e8glement sur la sant\u00e9 des animaux d\u00e9crit les exigences d'identification et de d\u00e9claration de mouvements pour les bovins, les bisons, les moutons et les porcs.</p>\n\n<p>En vertu du R\u00e8glement, les personnes qui sont en possession d'un animal, ou qui sont charg\u00e9s du soin ou du contr\u00f4le d'un animal, ou d'une carcasse d'animal, doivent d\u00e9clarer \u00e0 l'administrateur responsable certains types de renseignements sur l'identification et le d\u00e9placement de l'animal.</p>\n\n<p>Dans la partie XV, l'\u00ab\u00a0administrateur\u00a0responsable\u00a0\u00bb est d\u00e9fini (r\u00e9vis\u00e9e <span class=\"nowrap\">le 1 juillet 2014)</span> comme une \u00ab\u00a0<q>personne autoris\u00e9e par le ministre \u00e0 recevoir des renseignements relatifs aux animaux ou aux choses vis\u00e9s par la loi ou ses r\u00e8glements, nomm\u00e9e sur le site Web de l'Agence, et qui administre un programme d'identification national visant tout ou partie d'un ou de plusieurs genres, esp\u00e8ces ou sous-esp\u00e8ces d'animaux situ\u00e9s dans une ou plusieurs provinces.</q>\u00a0\u00bb</p>\n\n<h2>Administrateurs responsables</h2>\n\n<h3>Bovins de boucherie, bisons et ovins</h3>\n\n<p>L'Agence canadienne d'identification des bovins (ACIB) est reconnue comme l'administrateur responsable des bovins de boucherie (animal, autre qu'un embryon ou un oeuf f\u00e9cond\u00e9, des esp\u00e8ces <i lang=\"la\">Bos taurus</i> ou <i lang=\"la\">Bos indicus</i>), des ovins (animal, autre qu'un embryon ou un oeuf f\u00e9cond\u00e9, du genre <i lang=\"la\">Ovis</i>) et des bisons (animal, autre qu'un embryon ou un oeuf f\u00e9cond\u00e9, des sous-esp\u00e8ces <i lang=\"la\">Bison bison bison</i>, <i lang=\"la\">Bison bison athabascae</i> ou <i lang=\"la\">Bison bison bonasus</i>) dans le cadre du programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage. Si vous \u00eates une partie r\u00e9glement\u00e9e et souhaitez obtenir plus d'information sur les exigences d'identification et de d\u00e9claration de mouvements des bovins de boucherie, des ovins et des bisons, veuillez communiquer avec l'ACIB.</p>\n\n<p><a href=\"https://www.canadaid.ca/fr/\">Agence canadienne d'identification des bovins</a><br>\n<br>\n<br>\n<br>\n<br>\n<span class=\"nowrap\">de 7 h \u00e0 17 h,</span> heure des Rocheuses)<br>\n<br>\n<a href=\"mailto:info@canadaid.ca\" class=\"nowrap\">info@canadaid.ca</a></p>\n\n<p>Les parties r\u00e9glement\u00e9es peuvent d\u00e9clarer les donn\u00e9es sur l'identification et le d\u00e9placement des bovins, des ovins et des bisons dans le Syst\u00e8me canadien de tra\u00e7abilit\u00e9 du b\u00e9tail (SCTB), qui est une base de donn\u00e9es g\u00e9r\u00e9e par l'ACIB.</p>\n\n<ul>\n\n<li><a href=\"https://www.clts.canadaid.ca/CLTS/secure/user/home.do\" lang=\"en\">Canadian Livestock Tracking System (CLTS)</a> (en anglais seulement)</li>\n\n<li><a href=\"http://support.canadaid.ca/\" lang=\"en\">Resource Centre: Canadian Livestock Tracking System</a> (en anglais seulement)</li>\n\n</ul>\n\n<h3>Bovins Laitiers</h3>\n\n<p>Lactanet Canada est l'administrateur responsable des bovins laitiers (animal, autre qu'un embryon ou un oeuf f\u00e9cond\u00e9, des esp\u00e8ces <i lang=\"la\">Bos taurus</i> or <i lang=\"la\">Bos indicus</i>) dans le cadre du programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage. Si vous \u00eates une partie r\u00e9glement\u00e9e et souhaitez plus d'informations sur l'identification des bovins laitiers et les exigences de d\u00e9claration de mouvement, veuillez contacter Trac\u00e9Laitier.</p>\n\n<p><a href=\"https://www.lactanet.ca/\">Lactanet Canada</a></p>\n\n<p>Bureau de Sainte-Anne-de-Bellevue<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@valacta.com\" class=\"nowrap\">info@valacta.com</a></p>\n\n<p>Bureau de Guelph<br>\n<span lang=\"en\">Speedvale</span> ouest,<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@canwestdhi.com\" class=\"nowrap\">info@canwestdhi.com</a></p>\n\n<p>Les parties r\u00e9glement\u00e9es peuvent d\u00e9clarer les informations d'identification et de mouvement des bovins \u00e0 Trac\u00e9Laitier, qui est la base de donn\u00e9es g\u00e9r\u00e9e par Lactanet Canada.</p>\n\n<ul>\n<li><a href=\"https://tracelaitier.ca/\">Trac\u00e9Laitier</a></li>\n<li><a href=\"https://tracelaitier.ca/soutien/service-a-la-clientele/\">Services \u00e0 la client\u00e8le de Trac\u00e9Laitier</a></li>\n</ul>\n\n\n<h3>Deux administrateurs pour l'esp\u00e8ce bovine</h3>\n\n<p>Avec l'ajout de Lactanet Canada en tant qu'administrateur responsable pour les bovins laitiers au Canada et de la CCIA en tant qu'administrateur responsable pour les bovins de boucherie, il y a maintenant deux administrateurs pour l'esp\u00e8ce bovine. Bien que cela puisse sembler d\u00e9routant au d\u00e9but, les parties r\u00e9glement\u00e9es doivent savoir que\u00a0:</p>\n\n<ol class=\"lst-spcd\">\n<li>les parties r\u00e9glement\u00e9es peuvent continuer \u00e0 d\u00e9clarer les donn\u00e9es r\u00e9glementaires relatives aux bovins dans la base de donn\u00e9es du SCTB, ou dans Trac\u00e9Laitier, ou dans <span lang=\"en\">SimpliTrace</span> (au Qu\u00e9bec); et</li>\n<li>dans le cadre du programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage, les identificateurs approuv\u00e9s (\u00e9tiquettes) sont approuv\u00e9(e)s pour une utilisation chez l'esp\u00e8ce bovine, quelle que soit la pratique d'\u00e9levage.</li>\n</ol>\n<h3>Porcs</h3>\n\n<p>Le Conseil canadien du porc (CCP) est l'administrateur responsable des porcs (animal, autre qu'un embryon ou un oeuf f\u00e9cond\u00e9, du genre <i lang=\"la\">Sus</i>) dans le cadre du programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage. Si vous \u00eates une partie r\u00e9glement\u00e9e et souhaitez obtenir plus d'information sur les exigences d'identification et de d\u00e9claration de mouvements des porcs et des sangliers d'\u00e9levage, veuillez communiquer avec PorcTrac\u00e9 Canada, un programme g\u00e9r\u00e9 par le CCP, ou avec une association provinciale de producteurs de porcs (voir liste ci-dessous).</p>\n\n<p><a href=\"http://pigtrace.ca/fr/\">PorcTrac\u00e9 Canada</a></p>\n\n<p>Conseil canadien du porc (bureau chef)<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@cpc-ccp.com\" class=\"nowrap\">info@cpc-ccp.com</a></p>\n\n<p>Conseil canadien du porc (bureau de PorcTrac\u00e9)<br>\n<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:info@cpc-ccp.com\" class=\"nowrap\">info@cpc-ccp.com</a></p>\n\n<p>Service \u00e0 la client\u00e8le et commande d'\u00e9tiquettes\u00a0: 1-866-300-1825<br>\n<span class=\"nowrap\">9h30 \u00e0 16h30</span> (heure normale de l'Est), lundi au jeudi</p>\n\n<p>Les parties r\u00e9glement\u00e9es peuvent d\u00e9clarer les donn\u00e9es sur l'identification et le d\u00e9placement des porcs dans PorcTrac\u00e9 Canada\u00a0:</p>\n\n<p><a href=\"https://pigtrace.traceability.ca/login?language=fr\">PorcTrac\u00e9</a></p>\n\n<h3>Associations provinciales de producteurs de porcs</h3>\n\n<p><span lang=\"en\">British Columbia Pork Producers Association</span>\u00a0: 604-287-4647<br>\n<span lang=\"en\">Alberta Pork</span>\u00a0: 780-474-8288<br>\n<span lang=\"en\">SaskPork</span>\u00a0: 306-244-7752<br>\n<span lang=\"en\">Manitoba Pork Council</span>\u00a0: 204-237-7447<br>\n<span lang=\"en\">Ontario Pork</span>\u00a0: 519-767-4600<br>\n<br>\n<span lang=\"en\">New Brunswick Pork</span>\u00a0: 506-458-8051<br>\n<span lang=\"en\">Pork Nova Scotia</span>\u00a0: 902- 895-0581<br>\n<span lang=\"en\">PEI Pork</span>\u00a0: 902-892-4201</p>\n\n<h2>Organisation qui g\u00e8re un syst\u00e8me d'identification des animaux</h2>\n\n<p>Dans la partie XV du R\u00e8glement sur la sant\u00e9 des animaux, il est fait mention d'un \u00ab\u00a0organisme de gestion d'un syst\u00e8me d'identification des animaux\u00a0\u00bb. Une telle mention est faite pour une organisation qui n'est pas officiellement reconnue comme administrateur, puisqu'elle n'a pas sign\u00e9 un accord avec le ministre ou n'administre pas un programme national d'identification d'animaux, comme l'exige la d\u00e9finition.</p>\n\n<p>Attestra r\u00e9pond aux crit\u00e8res d'un \u00ab\u00a0organisme de gestion d'un syst\u00e8me d'identification des animaux\u00a0\u00bb. Ainsi, les producteurs de bovins, d'ovins et de cervid\u00e9s du Qu\u00e9bec d\u00e9clarent les renseignements pr\u00e9cis\u00e9s dans la partie XV sur l'identification et le d\u00e9placement dans la base de donn\u00e9es Attestra.</p>\n\n<p><a href=\"https://attestra.com/\">Attestra</a><br>\n<br>\n</p>\n\n<p>Service \u00e0 la client\u00e8le (par t\u00e9l\u00e9phone)\u00a0: 1-866-270-4319<br>\n<span class=\"nowrap\">de 7 h 30 \u00e0 16 h 30</span> (heure normale de l'Est)</p>\n\n<p>Service \u00e0 la client\u00e8le (par fax)\u00a0: 1-866-473-4033<br>Num\u00e9ro de fax de l'administration\u00a0: 1-866-679-6547</p>\n\n<p>Les parties r\u00e9glement\u00e9es au Qu\u00e9bec peuvent signaler l'identification des animaux et l'information sur le d\u00e9placement dans la <a href=\"https://simplitrace.atq.qc.ca/login\">base de donn\u00e9es Attestra</a>.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}