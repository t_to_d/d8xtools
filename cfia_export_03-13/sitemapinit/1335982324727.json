{
    "dcr_id": "1335982252047",
    "title": {
        "en": "Audit of Real Property",
        "fr": "V\u00e9rification de la gestion des biens immobiliers"
    },
    "html_modified": "2024-03-12 3:55:40 PM",
    "modified": "2014-05-13",
    "issued": "2014-05-13 12:21:12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_audit_2011_realproperty_summary_1335982252047_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_audit_2011_realproperty_summary_1335982252047_fra"
    },
    "ia_id": "1335982324727",
    "parent_ia_id": "1299843588592",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299843588592",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Audit of Real Property",
        "fr": "V\u00e9rification de la gestion des biens immobiliers"
    },
    "label": {
        "en": "Audit of Real Property",
        "fr": "V\u00e9rification de la gestion des biens immobiliers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1335982324727",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299843498252",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/real-property/",
        "fr": "/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/gestion-des-biens-immobiliers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Audit of Real Property",
            "fr": "V\u00e9rification de la gestion des biens immobiliers"
        },
        "description": {
            "en": "In 2010, the Canadian Food Inspection Agency (CFIA) conducted an internal audit of real property.",
            "fr": "En 2010, l'Agence canadienne d'inspection des aliments (ACIA) a effectu\u00e9 une v\u00e9rification interne de la gestion de ses biens immobiliers."
        },
        "keywords": {
            "en": "Audit Committee, real property, Treasury Board, Directory of Federal Real Property",
            "fr": "Comit\u00e9 de la fonction de v\u00e9rification interne,  biens immobiliers, Conseil du Tr\u00e9sor, R\u00e9pertoire des biens immobiliers f\u00e9d\u00e9raux"
        },
        "dcterms.subject": {
            "en": "best practices",
            "fr": "meilleures pratiques"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Audit, Evaluation and Risk Oversight",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,V\u00e9rification, \u00e9valuation et surveillance du risque"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-05-13 12:21:12",
            "fr": "2014-05-13 12:21:12"
        },
        "modified": {
            "en": "2014-05-13",
            "fr": "2014-05-13"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Archived - Audit of Real Property",
        "fr": "Archiv\u00e9e - V\u00e9rification de la gestion des biens immobiliers"
    },
    "body": {
        "en": "        \r\n        \n<section id=\"archived\" class=\"alert alert-warning wb-inview\" data-inview=\"archived-bnr\">\r\n<h2>This page has been archived</h2>\r\n<p>Information identified as archived is provided for reference, research or record-keeping purposes. It is not subject to the Government of Canada Web Standards and has not been altered or updated since it was archived. Please contact us to request a format other than those available.</p>\r\n</section>\r\n\r\n<section id=\"archived-bnr\" class=\"wb-overlay modal-content overlay-def wb-bar-t\">\r\n<header>\r\n<h2 class=\"wb-inv\">Archived</h2>\r\n</header>\r\n<p><a href=\"#archived\">This page has been archived.</a></p>\r\n</section>\r\n\r\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section>\n<h2>Audit</h2>\n<p>The Canadian Food Inspection Agency's (CFIA) internal audit function provides the President, senior officials and agency managers with an independent capability to perform audits of the resources, systems, processes, structures and operational tasks of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. It helps the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> accomplish its objectives by bringing a systematic, disciplined approach to assessing and improving the effectiveness of risk</p>\n<p>The internal audit function is accountable to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Audit Committee, of which the President is a member. All internal audit findings and recommendations must be reported to the Audit Committee, and all audits must be carried out in accordance with federal policy and legislative requirements, including the <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=16484\"><i>2009 Policy on Internal Audit</i></a> and the <a href=\"https://www.tbs-sct.gc.ca/faa-lfi/index-eng.asp\">2006 <i>Federal Accountability Act</i></a>.</p>\n<p><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> internal audit projects are selected based on highest significance during an annual agency planning process, which are then reflected in the Agency's Audit Plan for review by Audit Committee and approval of the President.</p>\n</section>\n</div>\n<h2>Overview</h2>\n<p>In 2010, the Canadian Food Inspection Agency (CFIA) conducted an internal audit of real property. This audit notes that as of March 31, 2010, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> had land valued at $3.3 million, and buildings with a net book value of $81.2 million disclosed in the audited financial statements. The scope of this audit included the management of the 10 laboratories and 15 quarantine and inspection stations listed in the Treasury Board (TB) Directory of Federal Real Property.</p>\n<p>The objective of the audit was to provide assurance that the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, as a custodian of Agency owned and operated facilities, is in compliance with the <abbr title=\"Treasury Board\">TB</abbr> <i>Policy on Management of Real Property</i>. The audit opinion found opportunities for improvement and the report proposed five recommendations.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> continually improves its programs and protocols, and management's commitment to addressing recommendations made by internal audits like this one is a critical part of that continual improvement. Some of the issues identified in this audit have already been addressed, and the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is working on the additional actions needed to address the remaining issues.</p>\n<h2>Key Findings</h2>\n<p><strong>Need for clear oversight for the management of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s real property portfolio:</strong> As of March 2012, the Agency consolidated authorities and responsibilities for real property under the Corporate Management Branch portfolio to ensure compliance with all applicable legislation, policies, directives and standards.</p>\n<p><strong>Need for the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to complete, approve and communicate a Real Property Management Framework (RPMF) that complies with the <abbr title=\"Treasury Board\">TB</abbr> <i>Policy on Management of Real Property</i>: </strong>A new <abbr title=\"Real Property Management Framework\">RPMF</abbr> that reflects the Agency's updated governance structure for the management of real property is being developed in accordance with <abbr title=\"Treasury Board\">TB</abbr> policies (target: September 2012).</p>\n<p><strong>Need for the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Assets and Security Management Directorate and its Science and Operations branches to work together to develop and implement a risk management framework and processes for real property:</strong> A risk management framework and associated processes are being developed as part of the new <abbr title=\"Real Property Management Framework\">RPMF</abbr> (target: September 2012).</p>\n<p><strong>Need for a long-term investment plan that ensures that the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> has a life-cycle management process that complies with the <abbr title=\"Treasury Board\">TB</abbr> <i>Policy on Management of Real Property</i>:</strong> The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is working toward realigning resources under a single national service delivery model. Appropriate planning structures that integrate life-cycle management principles are being developed as part of the new <abbr title=\"Real Property Management Framework\">RPMF</abbr> (target: September 2012).</p>\n<p><strong>Need for an integrated information management (IM) system in place that meets applicable <abbr title=\"Treasury Board\">TB</abbr> policy requirements, implemented across all custodial assets:</strong> <abbr title=\"information management\">IM</abbr> requirements related to <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> financial information and asset performance, functionality and utilization are being identified as the new <abbr title=\"Real Property Management Framework\">RPMF</abbr> is developed. Existing systems continue to be used to manage real property asset and lease tombstone information (target: September 2012).</p>\n<p><strong>Complete report:</strong></p>\n<ul>\n<li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/real-property/real-property/eng/1332173597080/1335985412589\">Audit of Real Property</a></li>\n<li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/real-property/action-plan/eng/1335987098837/1335987167681\">Management response and action plan (MRAP)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section id=\"archived\" class=\"alert alert-warning wb-inview\" data-inview=\"archived-bnr\">\r\n<h2>Cette page a \u00e9t\u00e9 archiv\u00e9e</h2>\r\n<p>L'information dont il est indiqu\u00e9 qu'elle est archiv\u00e9e est fournie \u00e0 des fins de r\u00e9f\u00e9rence, de recherche ou de tenue de documents. Elle n'est pas assujettie aux normes Web du gouvernement du Canada et elle n'a pas \u00e9t\u00e9 modifi\u00e9e ou mise \u00e0 jour depuis son archivage. Pour obtenir cette information dans un autre format, veuillez communiquer avec nous.</p>\r\n</section>\r\n\r\n<section id=\"archived-bnr\" class=\"wb-overlay modal-content overlay-def wb-bar-t\">\r\n<header>\r\n<h2 class=\"wb-inv\">Archiv\u00e9e</h2>\r\n</header>\r\n<p><a href=\"#archived\">Cette page a \u00e9t\u00e9 archiv\u00e9e.</a></p>\r\n</section><div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section>\n<h2>V\u00e9rification</h2>\n<p>La fonction de v\u00e9rification interne de l'Agence canadienne d'inspection des aliments (ACIA) permet au pr\u00e9sident, aux hauts fonctionnaires et aux gestionnaires de l'Agence de faire des v\u00e9rifications ind\u00e9pendantes des ressources, syst\u00e8mes, processus, structures et t\u00e2ches de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Cette fonction facilite l'atteinte des objectifs de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en proposant une approche syst\u00e9matique et ordonn\u00e9e qui a pour objet d'\u00e9valuer et d'am\u00e9liorer l'efficacit\u00e9 des processus de contr\u00f4le, de gouvernance et de gestion du risque.</p>\n<p>Le service de v\u00e9rification interne rend compte au Comit\u00e9 de v\u00e9rification de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, dont le pr\u00e9sident fait partie. Toutes les conclusions et les recommandations d\u00e9coulant des v\u00e9rifications internes doivent \u00eatre pr\u00e9sent\u00e9es au Comit\u00e9 de v\u00e9rification, et toutes les v\u00e9rifications doivent \u00eatre effectu\u00e9es conform\u00e9ment \u00e0 la politique f\u00e9d\u00e9rale applicable et aux exigences l\u00e9gislatives, notamment la <a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=16484\"><i>Politique sur la v\u00e9rification interne de 2009</i></a> et la <a href=\"https://www.tbs-sct.gc.ca/faa-lfi/index-fra.asp\"><i>Loi f\u00e9d\u00e9rale sur la responsabilit\u00e9 de 2006</i></a>.</p>\n<p>Les projets de v\u00e9rification interne sont choisis en fonction de leur importance au cours du processus de planification annuelle de l'Agence. Ils figurent dans le plan de v\u00e9rification de l'Agence qui est soumis \u00e0 l'examen du Comit\u00e9 de v\u00e9rification et \u00e0 l'approbation du pr\u00e9sident.</p>\n</section>\n</div>\n<h2>Aper\u00e7u</h2>\n<p>En 2010, l'Agence canadienne d'inspection des aliments (ACIA) a effectu\u00e9 une v\u00e9rification interne de la gestion de ses biens immobiliers. Selon les chiffres apparaissant dans les \u00e9tats financiers v\u00e9rifi\u00e9s, au 31 mars 2010, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> poss\u00e9dait des terrains d'une valeur de 3,3\u00a0millions de dollars ainsi que des immeubles dont la valeur comptable nette \u00e9tait de \u00e0 81,2 millions de dollars. La v\u00e9rification a port\u00e9 sur la gestion des 10 laboratoires et des 15\u00a0postes de quarantaine et d'inspection figurant dans la liste du R\u00e9pertoire des biens immobiliers f\u00e9d\u00e9raux du Conseil du Tr\u00e9sor (CT).</p>\n<p>L'objectif de la v\u00e9rification \u00e9tait de s'assurer que l'Agence, en qualit\u00e9 de gardienne des installations qu'elle poss\u00e8de et g\u00e8re, respecte la <i>Politique sur la gestion des biens immobiliers du <abbr title=\"Conseil du Tr\u00e9sor\">CT</abbr></i>. De l'avis de l'\u00e9quipe de v\u00e9rification, il est possible d'am\u00e9liorer la gestion.\u00a0 Le rapport pr\u00e9sente cinq recommandations.</p>\n<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> am\u00e9liore continuellement ses programmes et protocoles. L'engagement de la direction \u00e0 donner suite aux recommandations formul\u00e9es dans des v\u00e9rifications internes comme celles\u2011ci est un \u00e9l\u00e9ment important de ce processus d'am\u00e9lioration continue. Certains des probl\u00e8mes relev\u00e9s dans la pr\u00e9sente v\u00e9rification ont d\u00e9j\u00e0 \u00e9t\u00e9 r\u00e9gl\u00e9s, et l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> travaille actuellement sur d'autres mesures qui sont n\u00e9cessaires pour r\u00e9gler les autres probl\u00e8mes.\n<h2>Principales constatations</h2>\n<p><strong>Un processus explicite de surveillance doit \u00eatre \u00e9tabli pour la gestion du portefeuille des biens immobiliers de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\u00a0:</strong> Depuis mars\u00a02012, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a regroup\u00e9 ses pouvoirs et ses responsabilit\u00e9s en mati\u00e8re de gestion de biens immobiliers et les a confi\u00e9s \u00e0 la Direction g\u00e9n\u00e9rale de la gestion int\u00e9gr\u00e9e pour assurer le respect de toutes les lois, politiques, directives et normes applicables.</p>\n<p><strong>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> doit terminer, approuver et communiquer un Cadre de gestion des biens immobiliers (CGBI) qui soit conforme \u00e0 la <i>Politique sur la gestion des biens immobiliers du <abbr title=\"Conseil du Tr\u00e9sor\">CT</abbr></i>\u00a0: </strong>Un nouveau <abbr title=\"Cadre de gestion des biens immobiliers\">CGBI</abbr> qui refl\u00e8te la structure de gouvernance actualis\u00e9e de l'Agence pour la gestion des biens immobiliers est en voie d'\u00e9laboration, conform\u00e9ment aux politiques du <abbr title=\"Conseil du Tr\u00e9sor\">CT</abbr> (\u00e9ch\u00e9ance\u00a0: septembre\u00a02012).</p>\n<p><strong>La Direction de la gestion des biens et de la s\u00e9curit\u00e9 et les directions g\u00e9n\u00e9rales des Sciences et des Op\u00e9rations de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> doivent travailler ensemble \u00e0 l'\u00e9laboration et la mise en \u0153uvre d'un cadre de gestion des risques et de processus connexes pour les biens immobiliers\u00a0: </strong>Un cadre de gestion des risques et des processus connexes sont en voie d'\u00e9laboration, dans le cadre du nouveau <abbr title=\"Cadre de gestion des biens immobiliers\">CGBI</abbr> (\u00e9ch\u00e9ance\u00a0: septembre\u00a02012).</p>\n<p><strong>Il faut \u00e9tablir un plan d'investissement \u00e0 long terme qui permettra de faire en sorte que l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> dispose d'un processus de gestion du cycle de vie conforme \u00e0 la <i>Politique sur la gestion des biens immobiliers du <abbr title=\"Conseil du Tr\u00e9sor\">CT</abbr>\u00a0: </i></strong>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> r\u00e9am\u00e9nage actuellement ses ressources pour cr\u00e9er un seul mod\u00e8le national de prestation des services. Les structures de planification appropri\u00e9es qui int\u00e8grent des principes de gestion du cycle de vie sont en voie d'\u00e9laboration dans le cadre du nouveau <abbr title=\"Cadre de gestion des biens immobiliers\">CGBI</abbr> (\u00e9ch\u00e9ance\u00a0: septembre\u00a02012).</p>\n<p><strong>Un syst\u00e8me de gestion int\u00e9gr\u00e9e de l'information qui respecte les exigences de la politique du <abbr title=\"Conseil du Tr\u00e9sor\">CT</abbr> doit \u00eatre mis en \u0153uvre pour tous les biens immobiliers dont l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a la garde\u00a0: </strong>Les exigences en gestion de l'information li\u00e9es aux renseignements financiers, au rendement, \u00e0 la fonctionnalit\u00e9 et \u00e0 l'utilisation des biens immobiliers sont d\u00e9finies \u00e0 mesure que le <abbr title=\"Cadre de gestion des biens immobiliers\">CGBI</abbr> est \u00e9labor\u00e9. On continue d'utiliser les syst\u00e8mes existants pour g\u00e9rer les biens immobiliers et les renseignements de base sur les baux (septembre\u00a02012).</p>\n<p><strong>Rapport int\u00e9gral :</strong></p>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/gestion-des-biens-immobiliers/verification/fra/1332173597080/1335985412589\">V\u00e9rification de la gestion des biens immobiliers </a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/gestion-des-biens-immobiliers/plan-d-action/fra/1335987098837/1335987167681\">R\u00e9ponse et plan d'action de la direction (RPAD)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}