{
    "dcr_id": "1368202165258",
    "title": {
        "en": "Apple Proliferation Phytoplasma (APP): Questions and Answers",
        "fr": "Maladie des prolif\u00e9rations du pommier (MPP) : Questions et r\u00e9ponses"
    },
    "html_modified": "2024-02-13 9:48:51 AM",
    "modified": "2014-02-04",
    "issued": "2013-05-10 12:09:26",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_appleproliferation_questions_1368202165258_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_appleproliferation_questions_1368202165258_fra"
    },
    "ia_id": "1368202243213",
    "parent_ia_id": "1368189789199",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1368189789199",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Apple Proliferation Phytoplasma (APP): Questions and Answers",
        "fr": "Maladie des prolif\u00e9rations du pommier (MPP) : Questions et r\u00e9ponses"
    },
    "label": {
        "en": "Apple Proliferation Phytoplasma (APP): Questions and Answers",
        "fr": "Maladie des prolif\u00e9rations du pommier (MPP) : Questions et r\u00e9ponses"
    },
    "templatetype": "content page 1 column",
    "node_id": "1368202243213",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1368189722179",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/apple-proliferation-phytoplasma/questions-and-answers/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/maladie-des-proliferations-du-pommier/questions-et-reponses/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Apple Proliferation Phytoplasma (APP): Questions and Answers",
            "fr": "Maladie des prolif\u00e9rations du pommier (MPP) : Questions et r\u00e9ponses"
        },
        "description": {
            "en": "APP (Candidatus Phytoplasma mali ['Ca. P. mali']) is a virus-like plant pest of apple trees that affects fruit quality and the overall viability of a tree. This pest is considered to be a quarantine pest by Canada and the United States (U.S).",
            "fr": "La MPP [Candidatus Phytoplasma mali ('Ca. P. mali')] est un phytoravageur du pommier semblable \u00e0 une bact\u00e9rie qui affecte la qualit\u00e9 des fruits et la viabilit\u00e9 g\u00e9n\u00e9rale de l'arbre. Le ravageur est consid\u00e9r\u00e9 comme un organisme de quarantaine par le Canada et les \u00c9tats-Unis (\u00c9.-U.)."
        },
        "keywords": {
            "en": "plants, plant health, apple proliferation phytoplasma, plant pest, apple trees",
            "fr": "v\u00e9g\u00e9taux, la sant\u00e9 des v\u00e9g\u00e9taux, maladie des prolif\u00e9rations du pommier, phytoravageur, pommier, fruits"
        },
        "dcterms.subject": {
            "en": "insects,inspection,plant diseases,plants",
            "fr": "insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-05-10 12:09:26",
            "fr": "2013-05-10 12:09:26"
        },
        "modified": {
            "en": "2014-02-04",
            "fr": "2014-02-04"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Apple Proliferation Phytoplasma (APP): Questions and Answers",
        "fr": "Maladie des prolif\u00e9rations du pommier (MPP) : Questions et r\u00e9ponses"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is apple proliferation phytoplasma?</h2>\n<p><abbr title=\"Apple Proliferation Phytoplasma\">APP</abbr> (<i lang=\"la\">Candidatus Phytoplasma mali</i> ['<i lang=\"la\"><abbr title=\"Candidatus\">Ca.</abbr> <abbr title=\"Phytoplasma\">P.</abbr> mali</i>']) is a bacteria-like plant pest of apple trees that affects fruit quality and the overall viability of a tree. This pest is considered to be a quarantine pest by Canada and the United States (U.S).</p>\n<h2 class=\"h5\">Does apple proliferation phytoplasma affect the health of humans or animals?</h2>\n<p><abbr title=\"Apple Proliferation Phytoplasma\">APP</abbr> is a plant disease that does not affect the health of humans or animals.</p>\n<h2 class=\"h5\">What are the symptoms of apple proliferation phytoplasma?</h2>\n<p>Symptoms include a broom-like appearance of branches, the development of abnormal leaf clusters (leaf rosetting), and reduced fruit size and sweetness. <abbr title=\"Apple Proliferation Phytoplasma\">APP</abbr> decreases overall tree growth and viability.</p>\n<h2 class=\"h5\">Where does apple proliferation phytoplasma occur in the world?</h2>\n<p><abbr title=\"Apple Proliferation Phytoplasma\">APP</abbr> has been reported from many countries in Europe, including in the Balkans and southern Russia, as well as in T\u00fcrkiye and Syria.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que la maladie des prolif\u00e9rations du pommier?</h2>\n<p>La <abbr title=\"Maladie des prolif\u00e9rations du pommier\">MPP</abbr> [<i lang=\"la\">Candidatus Phytoplasma mali</i> ('<i lang=\"la\"><abbr title=\"Candidatus\">Ca.</abbr> <abbr title=\"Phytoplasma\">P.</abbr> mali</i>')] est un phytoravageur du pommier semblable \u00e0 une bact\u00e9rie qui affecte la qualit\u00e9 des fruits et la viabilit\u00e9 g\u00e9n\u00e9rale de l'arbre. Le ravageur est consid\u00e9r\u00e9 comme un organisme de quarantaine par le Canada et les \u00c9tats-Unis (\u00c9.-U.).</p>\n<h2 class=\"h5\">La maladie des prolif\u00e9rations du pommier pr\u00e9sente-t-elle un risque \u00e0 la sant\u00e9 des humains ou des animaux?</h2>\n<p>La <abbr title=\"Maladie des prolif\u00e9rations du pommier\">MPP</abbr> est un pathog\u00e8ne v\u00e9g\u00e9tal qui ne repr\u00e9sente aucune menace pour la sant\u00e9 des humains ou des animaux.</p>\n<h2 class=\"h5\">Quels sont les sympt\u00f4mes de la maladie des prolif\u00e9rations du pommier?</h2>\n<p>Les sympt\u00f4mes comprennent entre autres des branches en forme de balais, le d\u00e9veloppement de groupes anormaux de feuilles (rosettes) et une diminution de la taille et de la teneur en sucre des pommes. La <abbr title=\"Maladie des prolif\u00e9rations du pommier\">MPP</abbr> r\u00e9duit la croissance de l'arbre ainsi que sa viabilit\u00e9.</p>\n<h2 class=\"h5\">O\u00f9 trouve-t-on la maladie des prolif\u00e9rations du pommier dans le monde?</h2>\n<p>La <abbr title=\"Maladie des prolif\u00e9rations du pommier\">MPP</abbr> a \u00e9t\u00e9 recens\u00e9e dans plusieurs pays d'Europe, y compris dans les Balkans et le sud de la Russie, ainsi qu'en T\u00fcrkiye et en Syrie.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}