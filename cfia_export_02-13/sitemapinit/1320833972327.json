{
    "dcr_id": "1320833881215",
    "title": {
        "en": "Import policies for live animals",
        "fr": "Politiques pour l'importation des animaux vivants"
    },
    "html_modified": "2024-02-13 9:47:04 AM",
    "modified": "2021-05-14",
    "issued": "2011-11-09 05:18:06",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_import_pol_live_1320833881215_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_import_pol_live_1320833881215_fra"
    },
    "ia_id": "1320833972327",
    "parent_ia_id": "1320806678417",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Imports",
        "fr": "Importations"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1320806678417",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Import policies for live animals",
        "fr": "Politiques pour l'importation des animaux vivants"
    },
    "label": {
        "en": "Import policies for live animals",
        "fr": "Politiques pour l'importation des animaux vivants"
    },
    "templatetype": "content page 1 column",
    "node_id": "1320833972327",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1320806430753",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/imports/import-policies/live-animals/",
        "fr": "/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Import policies for live animals",
            "fr": "Politiques pour l'importation des animaux vivants"
        },
        "description": {
            "en": "Canada has specific import requirements in place to avoid introducing animal diseases to protect its people, plants and animals.",
            "fr": "Le Canada a des exigences d'importation sp\u00e9cifiques en place pour \u00e9viter d'introduire des maladies animales afin de prot\u00e9ger sa population, ses plantes et ses animaux."
        },
        "keywords": {
            "en": "Import, policies, live animals, animal health",
            "fr": "Politiques, importation, animaux vivants, sant\u00e9 animale"
        },
        "dcterms.subject": {
            "en": "livestock,imports,inspection,veterinary medicine,policy,animal health",
            "fr": "b\u00e9tail,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,politique,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-11-09 05:18:06",
            "fr": "2011-11-09 05:18:06"
        },
        "modified": {
            "en": "2021-05-14",
            "fr": "2021-05-14"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Import policies for live animals",
        "fr": "Politiques pour l'importation des animaux vivants"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Canada has specific import requirements in place to avoid introducing animal diseases to protect its people, plants and animals. Specific import requirements, based on the applicable animal health policies, can be reviewed in the <a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System (AIRS)</a>.</p>\n\n\n<p>If you are travelling with a pet or planning to import an animal to Canada, you will need the right paperwork at the border to meet Canada's import requirements. Find out <a href=\"/importing-food-plants-or-animals/pets/eng/1326600389775/1326600500578\">what you need before you travel with your pet or import an animal</a>.</p>\n\n<h2>Birds</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2006-2/eng/1321034984869/1321035229279\">Import measures for live birds to prevent the introduction of Avian Influenza in domestic birds</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2011-8/eng/1320852296518/1320852383374\">Requirements for pet birds imported from countries other than the United States</a></li>\n</ul>\n\n<h2>Camelids</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2012-5/eng/1345055822789/1345055887897\">Requirements for camelids imported from the United States to Canada</a></li>\n</ul>\n\n<h2>Cattle</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2014-1-4/eng/1321029586168/1321029737815\">Import of restricted feeder cattle from the United States</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/ahdp-dsae-ie-2007-4-7/eng/1321034031770/1321034232389\">Requirements for breeding cattle imported from the United States to Canada</a></li>\n</ul>\n\n<h2>Cervids</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2008-6/eng/1508379830901/1508379831494\">Requirements for captive cervids imported from the United States to Canada</a></li>\n</ul>\n\n<!--<h2>Dogs (commercial dogs less than&nbsp;8&nbsp;months)</h2>\n<ul>\n<li><a href=\"/eng/1620079393889/1620079394576\">Requirements for importing commercial dogs less than&nbsp;8&nbsp;months of age (for the breeding and resale, which includes adoption, end uses)</a></li>\n</ul>\n-->\n\n<h2>Horses</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2011-7/eng/1325203896325/1325203970493\">Import conditions for Canadian horses returning from European Union Member States</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2011-6/eng/1325285649126/1325285766286\">Import conditions for horses from European Union Member States for temporary stay in Canada: horses for competition or racing, training and stallions for non-competitive entertainment</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2011-5/eng/1325285856958/1325285922473\">Import conditions for horses from the European Union Member States for permanent stay in Canada: breeding horses, racing horses, horses for riding and pleasure, horses for non-competitive entertainment, and Andalusian horses</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2010-14/eng/1320889560104/1320889734552\">Import conditions for horses from the United States for immediate slaughter</a></li>\n</ul>\n\n<h2>Primates</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2009-1/eng/1320891194183/1320892143537\">Requirements for non-human primates imported into Canada</a></li>\n</ul>\n\n<h2>Ruminants (other than cattle)</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2007-5/eng/1321032703935/1321032839418\">Requirements for small ruminants imported from the United States for breeding, domestic or captive purposes</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2007-11/eng/1321024425846/1321024888798\">Requirements for small ruminants Imported from the United States for feeding and subsequent slaughter</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2007-13/eng/1321023583017/1321023682571\">Requirements for small ruminants Imported from the United States for immediate slaughter</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2011-4/eng/1320853608999/1320853734081\">Requirements for the movement of small ruminants transiting Canada by land between the continental United States and Alaska</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2012-4/eng/1345052591704/1345052794644\">Requirements for captive non-domestic ruminants imported from the United States to Canada</a></li>\n</ul>\n\n<h2>Swine</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2008-3/eng/1320993175980/1320993708990\">Import conditions for breeding swine from European Union Member States</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/2019-3/eng/1553655002098/1553655002392\">Import conditions for swine imported from the United States for research</a></li>\n</ul>\n\n<h2>Criteria for quarantine facilities</h2>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/medium-security/eng/1321109923418/1321110259785\">Criteria for medium security quarantine facilities</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/live-animals/minimum-security/eng/1321109159175/1321109277600\">Criteria for minimum security quarantine facilities</a></li>\n<li><a href=\"/importing-food-plants-or-animals/pets/dogs/commercial-imports-8-months/criteria-for-quarantine-facilities/eng/1620147749836/1620147750461\">Criteria for quarantine facilities for commercial dogs less than\u00a08\u00a0months of age (breeding, show/exhibition (permanent stay), and resale/adoption end uses) entering Canada by air</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section>\n<p>Le Canada a des exigences d'importation sp\u00e9cifiques en place pour \u00e9viter d'introduire des maladies animales afin de prot\u00e9ger sa population, ses plantes et ses animaux. Exigences sp\u00e9cifiques \u00e0 l'importation, sur la base des politiques de sant\u00e9 animale applicables, peuvent \u00eatre examin\u00e9es dans le <a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a>.</p>\n\n<p>Si vous voyagez avec un animal de compagnie ou envisagez d'importer un animal au Canada, vous aurez besoin des bons documents \u00e0 la fronti\u00e8re pour r\u00e9pondre aux exigences d'importation du Canada. Informez-vous sur <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/fra/1326600389775/1326600500578\">ce que vous avez besoin avant de voyager avec votre animal de compagnie ou d'importer un animal</a>.</p>\n\n<h2>Bovins</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/ahdp-dsae-ie-2007-4-7/fra/1321034031770/1321034232389\">Exigences relatives \u00e0 l'importation de bovins de reproduction au Canada, en provenance des \u00c9tats-Unis</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2014-1-4/fra/1321029586168/1321029737815\">Importation des bovins d'engraissement sous restriction en provenance des \u00c9tats-Unis</a></li>\n</ul>\n\n<h2>Cam\u00e9lid\u00e9s</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2012-5/fra/1345055822789/1345055887897\">Exigences relatives aux cam\u00e9lid\u00e9s import\u00e9s des \u00c9tats Unis vers le Canada</a></li>\n</ul>\n\n<h2>Cervid\u00e9s</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2008-6/fra/1508379830901/1508379831494\">Exigences relatives \u00e0 l'importation au Canada de cervid\u00e9s captifs provenant des \u00c9tats-Unis</a></li>\n</ul>\n\n<h2>Chevaux</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2010-14/fra/1320889560104/1320889734552\">Conditions \u00e0 l'importation de chevaux des \u00c9tats-Unis pour abattage imm\u00e9diat</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2011-7/fra/1325203896325/1325203970493\">Conditions d'importation pour les chevaux canadiens qui reviennent d'\u00c9tats membres de l'Union europ\u00e9enne</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2011-5/fra/1325285856958/1325285922473\">Conditions d'importation pour les chevaux provenant d'\u00c9tats membres de l'Union europ\u00e9enne pour s\u00e9jour permanent au Canada\u00a0: les chevaux destin\u00e9s \u00e0 la reproduction, la course, l'\u00e9quitation et le plaisir, les chevaux pour le divertissement non comp\u00e9titif et les chevaux andalous</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2011-6/fra/1325285649126/1325285766286\">Conditions d'importation pour les chevaux provenant d'\u00c9tats membres de l'Union europ\u00e9enne pour s\u00e9jour temporaire\u00a0: les chevaux pour la comp\u00e9tition, la course, l'entra\u00eenement et \u00e9talons pour le divertissement non comp\u00e9titif</a></li>\n</ul>\n\n<!--<h2>Chiens</h2>\n<ul>\n<li><a href=\"/fra/1620079393889/1620079394576\">Exigences en mati&#232;re d'importation commerciale de chiens &#226;g&#233;s de moins de&nbsp;8&nbsp;mois destines &#224; l'&#233;levage et &#224; la revente (ce qui inclut l'adoption), les utilisations finales</a></li>\n</ul>\n-->\n\n<h2>Oiseaux</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2006-2/fra/1321034984869/1321035229279\">Mesures d'importation des oiseaux vivants pour emp\u00eacher la propagation de l'influenza aviaire aux oiseaux domestiques</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2011-8/fra/1320852296518/1320852383374\">Exigences relatives \u00e0 l'importation d'oiseaux de compagnie, en provenance de pays autres que les \u00c9tats-Unis</a></li>\n</ul>\n\n<h2>Porcs</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2019-3/fra/1553655002098/1553655002392\">Conditions d'importation pour les porcs en provenance des \u00c9tats-Unis destin\u00e9s \u00e0 la recherche</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2008-3/fra/1320993175980/1320993708990\">Conditions pour l'importation de porcs reproducteurs provenant d'\u00c9tats membres de l'Union europ\u00e9enne</a></li>\n</ul>\n\n<h2>Primates</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2009-1/fra/1320891194183/1320892143537\">Exigences concernant l'importation de primates non humains au Canada</a></li>\n</ul>\n\n<h2>Ruminants (autres que les bovins)</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2011-4/fra/1320853608999/1320853734081\">Exigences relatives aux d\u00e9placements de petits ruminants qui transitent par le Canada par voie terrestre entre les \u00e9tats continentaux des \u00c9tats-Unis et l'Alaska</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2007-11/fra/1321024425846/1321024888798\">Exigences d'importation pour les petits ruminants import\u00e9s des \u00c9tats-Unis \u00e0 des fins d'engraissage et d'abattage subs\u00e9quent</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2007-13/fra/1321023583017/1321023682571\">Exigences relatives aux petits ruminants import\u00e9s des \u00c9tats-Unis en vue d'un abattage imm\u00e9diat</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2012-4/fra/1345052591704/1345052794644\">Exigences relatives aux ruminants non domestiques en captivit\u00e9 import\u00e9s des \u00c9tats Unis vers le Canada</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/2007-5/fra/1321032703935/1321032839418\">Exigences relatives \u00e0 l'importation de petits ruminants de reproduction, domestiques ou de captivit\u00e9 en provenance des \u00c9tats-Unis</a></li>\n</ul>\n\n<h2>Crit\u00e8res applicables aux postes de quarantaine</h2>\n\n<ul>\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/securite-minimale/fra/1321109159175/1321109277600\">Crit\u00e8res applicables aux postes de quarantaine \u00e0 s\u00e9curit\u00e9 minimale</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/animaux-vivants/securite-moyenne/fra/1321109923418/1321110259785\">Crit\u00e8res applicables aux postes de quarantaine \u00e0 s\u00e9curit\u00e9 moyenne</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/animaux-de-compagnie/chiens/importation-commerciale-8-mois/criteres-relatifs-aux-installations-de-quarantaine/fra/1620147749836/1620147750461\">Crit\u00e8res relatifs aux installations de quarantaine pour les chiens commerciaux \u00e2g\u00e9s de moins de\u00a08\u00a0mois (reproduction, foire/exposition (s\u00e9jour permanent), et revente/adoption) entrant au Canada par voie a\u00e9rienne</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}