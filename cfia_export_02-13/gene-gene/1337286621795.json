{
    "dcr_id": "1337286621795",
    "title": {
        "en": "Emerald Ash Borer Regulated Articles",
        "fr": "Produits r\u00e9glement\u00e9s pour l'agrile du fr\u00eane"
    },
    "html_modified": "2024-02-13 9:47:34 AM",
    "modified": "2012-05-17",
    "issued": "2012-05-17 16:30:24",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_agrpla_regulated_articles_1337286621795_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pestrava_agrpla_regulated_articles_1337286621795_fra"
    },
    "ia_id": "1337286747578",
    "parent_ia_id": "1337273975030",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Regulated Pests",
        "fr": "Parasites r\u00e9glement\u00e9s"
    },
    "commodity": {
        "en": "Forestry",
        "fr": "For\u00eats"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1337273975030",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Emerald Ash Borer Regulated Articles",
        "fr": "Produits r\u00e9glement\u00e9s pour l'agrile du fr\u00eane"
    },
    "label": {
        "en": "Emerald Ash Borer Regulated Articles",
        "fr": "Produits r\u00e9glement\u00e9s pour l'agrile du fr\u00eane"
    },
    "templatetype": "content page 1 column",
    "node_id": "1337286747578",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1337273882117",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/emerald-ash-borer/regulated-articles/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/produits-reglementes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Emerald Ash Borer Regulated Articles",
            "fr": "Produits r\u00e9glement\u00e9s pour l'agrile du fr\u00eane"
        },
        "description": {
            "en": "The movement of regulated ash articles and firewood poses the greatest risk of spreading the emerald ash borer (EAB) to non-regulated areas.",
            "fr": "Le d\u00e9placement de produits du fr\u00eane r\u00e9glement\u00e9s et du bois de chauffage repr\u00e9sente le risque le plus important de propagation de l'agrile du fr\u00eane dans des r\u00e9gions non r\u00e9glement\u00e9es."
        },
        "keywords": {
            "en": "forestry, quarantine programs, import, export, regulated pests, emerald ash borer, Ontario, Quebec, Agrilus planipennis",
            "fr": "for&#234;ts, programmes phytosanitaires, importation, exportation, organismes justiciables de quarantaine, Agrile du fr&#234;ne, Ontario, Qu\u00e9bec, Agrilus planipennis"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-17 16:30:24",
            "fr": "2012-05-17 16:30:24"
        },
        "modified": {
            "en": "2012-05-17",
            "fr": "2012-05-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Emerald Ash Borer Regulated Articles",
        "fr": "Produits r\u00e9glement\u00e9s pour l'agrile du fr\u00eane"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=10&amp;ga=79#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The movement of regulated ash articles and firewood poses the greatest risk of spreading the emerald ash borer (EAB) to non-regulated areas. To reduce this risk, the Canadian Food Inspection Agency (CFIA) regulates the movement of articles made of ash and firewood of all species to prevent the spread of <abbr title=\"emerald ash borer\">EAB</abbr> from a regulated area.</p>\n<ul>\n<li><a href=\"/plant-health/invasive-species/insects/emerald-ash-borer/areas-regulated/eng/1347625322705/1367860339942\">Regulated Areas</a></li>\n</ul>\n<h2><abbr title=\"emerald ash borer\">EAB</abbr> Regulated Articles</h2>\n<p>Movement of any ash tree article from a regulated area or property with a prohibition of movement without the consent of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is prohibited. Ash tree articles include:</p>\n<ul>\n<li>ash trees (whole or parts)</li>\n<li>ash nursery stock</li>\n<li>ash logs and branches</li>\n<li>ash lumber</li>\n<li>wood packaging materials with an ash component</li>\n<li>ash wood or bark</li>\n<li>ash wood chips or bark chips</li>\n<li>firewood from all tree species</li>\n</ul>\n<h2>Transporting regulated articles</h2>\n<p>EAB regulated articles moving out of a regulated area must be accompanied by a Movement Certificate issued by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p>All vehicles used to transport regulated articles must be cleaned of debris prior to loading at origin and prior to departure from the receiving facility. The required treatment will depend upon the regulated article transported, but may include sweeping or power washing.</p>\n<p>For more information about transporting regulated articles, <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">contact your local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office</a>.</p>\n<h2>Firewood</h2>\n<p>Firewood is one way for emerald ash borer to spread. Moving ash firewood out of a regulated area is prohibited, whether for commercial or private purposes (such as taking firewood to the cottage or camping). Non-ash firewood can only be moved out of a regulated area by facilities registered under the <a href=\"/plant-health/invasive-species/directives/forest-products/d-03-08/compliance-program/eng/1349100647331/1355880297595\">Emerald Ash Borer Approved Facility Compliance Program</a> (EABAFCP).</p>\n<p>Learn more about the <a href=\"/plant-health/forestry/hazards-of-moving-firewood/eng/1500309474824/1500309544561\">impact of moving firewood</a>.</p>\n<h2>Tree trimmings and yard waste</h2>\n<p>Movement of yard waste outside of regulated areas is also prohibited, as it may contain ash tree bark, branches or trimmings.</p>\n<p>Municipalities with <abbr title=\"emerald ash borer\">EAB</abbr> infestations may have established special procedures for handling yard waste from regulated areas. Contact your municipality for the latest information on disposal of regulated yard waste. If you live in a regulated area or have been issued a Notice of Prohibition of Movement, please contact your local government for more information on what to do with any yard waste.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=10&amp;ga=79#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Le d\u00e9placement de produits du fr\u00eane r\u00e9glement\u00e9s et du bois de chauffage repr\u00e9sente le risque le plus important de propagation de l'agrile du fr\u00eane dans des r\u00e9gions non r\u00e9glement\u00e9es. Pour r\u00e9duire ce risque, l'Agence canadienne d'inspection des aliments (ACIA) r\u00e9glemente le d\u00e9placement de produits fabriqu\u00e9s \u00e0 partir de fr\u00eane et du bois de chauffage de toutes les essences pour pr\u00e9venir la propagation de l'agrile du fr\u00eane \u00e0 partir de r\u00e9gions r\u00e9glement\u00e9es.</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/zones-reglementees/fra/1347625322705/1367860339942\">Zones r\u00e9glement\u00e9es</a> </li>\n</ul>\n<h2>Produits du fr\u00eane r\u00e9glement\u00e9s</h2>\n<p>Il est interdit de d\u00e9placer des produits du fr\u00eane d'une r\u00e9gion ou d'une propri\u00e9t\u00e9 r\u00e9glement\u00e9e vis\u00e9e par une interdiction de d\u00e9placement sans le consentement de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Voici une liste non exhaustive des produits du fr\u00eane r\u00e9glement\u00e9s :</p>\n<ul>\n<li>fr\u00eanes entiers ou morceaux de fr\u00eane</li>\n<li>mat\u00e9riel de p\u00e9pini\u00e8re de fr\u00eane;</li>\n<li>billes ou branches de fr\u00eane;</li>\n<li>bois d'<span class=\"lig\">oe</span>uvre de fr\u00eane</li>\n<li>mat\u00e9riaux d'emballage en bois qui renferment du fr\u00eane</li>\n<li>bois ou \u00e9corce de fr\u00eane</li>\n<li>copeaux de bois ou d'\u00e9corce de fr\u00eane</li>\n<li>bois de chauffage de toutes les essences.</li>\n</ul>\n<h2>Transport de produits r\u00e9glement\u00e9s</h2>\n<p>Les produits r\u00e9glement\u00e9s pour l'agrile du fr\u00eane qui quittent une r\u00e9gion r\u00e9glement\u00e9e doivent \u00eatre accompagn\u00e9s d'un certificat de d\u00e9placement d\u00e9livr\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Tous les v\u00e9hicules servant \u00e0 transporter des produits r\u00e9glement\u00e9s doivent \u00eatre nettoy\u00e9s avant le chargement au point d'origine et avant le d\u00e9part des installations de destination. La m\u00e9thode de nettoyage \u00e0 utiliser d\u00e9pend du produit r\u00e9glement\u00e9 transport\u00e9 (balayage ou lavage sous pression).</p>\n<p>Pour en savoir davantage sur le transport des produits r\u00e9glement\u00e9s, <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">communiquez avec votre bureau local de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.</p>\n<h2>Bois de chauffage</h2>\n<p>Le bois de chauffage est \u00e9galement un moyen de propagation de l'agrile du fr\u00eane. Il est interdit de d\u00e9placer du bois de chauffage d'une r\u00e9gion r\u00e9glement\u00e9e, tant \u00e0 des fins commerciales que priv\u00e9es (par exemple en apportant du bois de chauffage au chalet ou \u00e0 un terrain de camping). Le bois de chauffage autre que le fr\u00eane peut \u00eatre d\u00e9plac\u00e9 d'une r\u00e9gion r\u00e9glement\u00e9e par des \u00e9tablissements approuv\u00e9s par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> seulement dans le cadre du <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/produits-forestiers/d-03-08/programme-de-conformite/fra/1349100647331/1355880297595\">Programme de conformit\u00e9 des \u00e9tablissements approuv\u00e9s \u00e0 l'\u00e9gard de l'agrile du fr\u00eane</a> (PCEAAF).</p>\n<p>Apprenez-en davantage sur les <a href=\"/protection-des-vegetaux/forets/dangers-lies-au-deplacement-du-bois-de-chauffage/fra/1500309474824/1500309544561\">r\u00e9percussions du d\u00e9placement de bois de chauffage</a>.</p>\n<h2>R\u00e9sidus de taille d'arbres et de jardin</h2>\n<p>Il est interdit de d\u00e9placer des r\u00e9sidus de jardin hors des r\u00e9gions r\u00e9glement\u00e9es parce que ces derniers pourraient contenir de l'\u00e9corce, des branches ou des r\u00e9sidus de fr\u00eane.</p>\n<p>Les municipalit\u00e9s touch\u00e9es par des infestations d'agrile du fr\u00eane ont des proc\u00e9dures sp\u00e9ciales bien \u00e9tablies pour la manipulation des r\u00e9sidus de jardin provenant des r\u00e9gions r\u00e9glement\u00e9es. Communiquez avec votre municipalit\u00e9 pour obtenir l'information la plus r\u00e9cente sur l'\u00e9limination des r\u00e9sidus de jardin r\u00e9glement\u00e9s. Si vous habitez dans une r\u00e9gion r\u00e9glement\u00e9e ou que vous avez re\u00e7u un avis de d'interdiction de d\u00e9placement, veuillez communiquer avec votre administration locale pour obtenir plus d'information sur ce que vous devez faire de vos r\u00e9sidus de jardin.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}