{
    "dcr_id": "1648492673379",
    "title": {
        "en": "Meet Ray Knight, CFIA ship inspector",
        "fr": "Voici Ray Knight, inspecteur des navires de l'ACIA"
    },
    "html_modified": "2024-03-12 3:59:14 PM",
    "modified": "2023-01-24",
    "issued": "2022-04-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_pro_meet_ray_knight_1648492673379_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_pro_meet_ray_knight_1648492673379_fra"
    },
    "ia_id": "1648492673692",
    "parent_ia_id": "1565298134171",
    "chronicletopic": {
        "en": "Science and Innovation|Plant Health",
        "fr": "Science et Innovation|Sant\u00e9 des plantes"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Canadians",
        "fr": "Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565298134171",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Meet Ray Knight, CFIA ship inspector",
        "fr": "Voici Ray Knight, inspecteur des navires de l'ACIA"
    },
    "label": {
        "en": "Meet Ray Knight, CFIA ship inspector",
        "fr": "Voici Ray Knight, inspecteur des navires de l'ACIA"
    },
    "templatetype": "content page 1 column",
    "node_id": "1648492673692",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298133875",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/plant-health/ray-knight/",
        "fr": "/inspecter-et-proteger/sante-des-vegetaux/ray-knight/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Meet Ray Knight, CFIA ship inspector",
            "fr": "Voici Ray Knight, inspecteur des navires de l'ACIA"
        },
        "description": {
            "en": "Many people are surprised to learn that the CFIA inspects ships to stop the spread of invasive species. One thing is for shore: it\u2019s an important job. Let me explain why.",
            "fr": "Bon nombre de personnes sont surprises d'apprendre que l'ACIA inspecte les navires pour emp\u00eacher la propagation des esp\u00e8ces envahissantes. Une chose est certaine : c'est un travail important. Laissez-moi vous expliquer pourquoi."
        },
        "keywords": {
            "en": "Ray Knight, Canadian Food Inspection Agency, CFIA, science specialist",
            "fr": "Ray Knight, l\u2019Agence canadienne d\u2019inspection des aliments, ACIA, sp\u00e9cialiste scientifique"
        },
        "dcterms.subject": {
            "en": "food,biology,food inspection,scientific research,animal health,sciences",
            "fr": "aliment,biologie,inspection des aliments,recherche scientifique,sant\u00e9 animale,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-04-01",
            "fr": "2022-04-01"
        },
        "modified": {
            "en": "2023-01-24",
            "fr": "2023-01-24"
        },
        "type": {
            "en": "news publication",
            "fr": "publication d'information"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Meet Ray Knight, CFIA ship inspector",
        "fr": "Voici Ray Knight, inspecteur des navires de l'ACIA"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_meet_ray_knight_600x600_1648492922364_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>My name is Ray Knight, and I've always had a strong connection to the outdoors.</p>\n\n<p>As a kid, I camped, hunted and fished with my family a lot. This inspired me to further my education and obtain a degree in the field of Forest Resources Management. After that, I started working in the beautiful coastal and interior forests of British Columbia (BC).</p>\n\n<p>I had the chance to explore some of the province's older coastal forests filled with huge trees\u2014and was thrilled to see a lot of new, healthy forests growing, too. Through this, I gained a deeper appreciation for nature and its diversity.</p>\n\n<p>In my current job as a plant health inspector with the Canadian Food Inspection Agency (CFIA), I carefully check over shipments from around the world to make sure what's inside won't damage the environment I care so much about.</p>\n\n<h2>From the forest to the port</h2>\n\n<p>During forestry training, I developed field skills and data collection techniques that I still use to this day. After my studies, I witnessed the aftermath of the <a href=\"https://www.nrcan.gc.ca/our-natural-resources/forests/wildland-fires-insects-disturbances/top-forest-insects-and-diseases-canada/mountain-pine-beetle/13381\">mountain pine beetle</a> outbreak in the BC. The clear-cutting of vast areas was necessary to stop the beetle infestations. We also completed surveys to record and quantify the damage to the forests.</p>\n\n<p>When I joined the CFIA in 1998, I continued my work on invasive pests by inspecting wood packaging in shipping containers as they arrived at the Port of Vancouver. We made sure the packaging was insect-free before any containers made their way to the importer in Canada.</p>\n\n<p>We saw all sorts of interesting things in those containers, from personal belongings, to classic cars, to heavy equipment. If it can fit in a container, it could end up in one! I also had a direct impact in changing how dunnage (loose wood used to keep a cargo in position in a ship's hold) moves around the world.</p>\n\n<p>During one of those dunnage inspections, I found the larvae of the <a href=\"/plant-health/invasive-species/insects/asian-longhorned-beetle/eng/1337792721926/1337792820836\">Asian longhorned beetle</a>. These are massive larvae\u2014close to the size of your thumb\u2014that use their mandibles to chew through wood. I made the mistake of bringing my finger a little too close, and one of them took the opportunity to chew on it like a piece wood. That hurt! Once I got my fingers out of the way, we fumigated the dunnage to kill the insects and refused the ship's entry into Canada. This beetle is invasive and not allowed into Canada because of the damage it can cause to Canada's forests.</p>\n\n<h2>These pests will knot get past us</h2>\n\n<p>I've worked on many different files at the CFIA, from <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/eng/1323990856863/1323991018946\">avian influenza (bird flu)</a> to inspecting plant and animal products imported into Canada to ensure they meet Canadian requirements\u2014both physically and on paper.</p>\n\n<p>While I enjoy those parts of the job, inspecting ships is the most interesting. Most people may not associate CFIA inspectors with marine vessels, but there are two reasons we are there: (1) to protect grain exports and (2) to prevent the introduction of an invasive moth known as the <a href=\"/plant-health/invasive-species/insects/spongy-moth/agm/eng/1330353359964/1330353499535\">AGM</a>.</p>\n\n<p>Canada exports a lot of grain like wheat, oats, canola and barley. To move such large quantities of grain, much of it goes into freighters. My job is to meet those freighters before they are loaded and verify the ship itself meets Canadian phytosanitary (plant health) standards. In other words, I check that it's clean enough to load. I climb down into the ship's holds, have a good look around and direct any necessary cleaning. At the same time, I interact with the crew and meet all kinds of interesting people.</p>\n\n<p>Inspections in search of the AGM on incoming ships are all about protecting Canada's environment\u2014and for good reason. If this moth becomes well-established here, it would pose a serious risk to our environment and our economy by impacting trade with other countries. Native to Japan, China, Korea and far east Russia, this pest likes to hitch a ride on ships. Female moths lay eggs throughout the year, so our team checks that eggs are removed before the ship arrives in Canada and takes quick action if some were missed.</p>\n\n<p>Ship inspections are our final line of defence before the products coming into our country are released. It's an important step in the process to ensure that the controls in place are effective.</p>\n\n<p>While I still get out to do some ship inspections, I spend most of my time these days on <a href=\"/plant-health/invasive-species/plant-pest-surveillance/eng/1344466499681/1344466638872\">plant pest surveillance</a>. I lead a dedicated team that collects and records data on a range of pests, including the Asian longhorned beetle, <a href=\"/plant-health/invasive-species/insects/blueberry-maggot/eng/1328325206503/1328325288221\">blueberry maggot</a>, <a href=\"/plant-health/invasive-species/insects/light-brown-apple-moth/eng/1326465917763/1326466022858\">light brown apple moth</a> and <a href=\"/plant-health/invasive-species/insects/emerald-ash-borer/eng/1337273882117/1337273975030\">emerald ash borer</a>. We want to know where they are and where they are not by trapping insects, collecting samples of soil or plant tissue, observing plants, using laboratory diagnostics, and more.</p>\n\n<p>We also administer surveys for the AGM, <a href=\"/plant-health/invasive-species/insects/spongy-moth/spongy-moth/eng/1329836269430/1329836504450\">the newly renamed spongy (LDD) moth</a> and <a href=\"/plant-health/invasive-species/insects/japanese-beetle/eng/1328048149161/1328048244390\">Japanese beetle</a>. The spongy moth and Japanese beetle are present in Ontario, Quebec, and most of the maritime provinces, and while they have not established in British Columbia, small populations do exist. Our surveys aim to pinpoint their location on the ground. We then share this information with the provincial government and other partners involved in eradication programs.</p>\n\n<h2>Protect your backyard, garden and community</h2>\n\n<p>While eradication programs have been effective, there is still work to do. All Canadians have a role to play in stopping the spread of invasive pests\u2014and I promise it doesn't need to involve climbing aboard a ship!</p>\n\n<p>The first step is awareness. Learn about <a href=\"/plant-health/invasive-species/eng/1299168913252/1299168989280\">invasive species</a> and the environment where you live. Knowing what to look for will help you protect your backyard, garden and community from the damage caused by invasive plants and plant pests.</p>\n\n<p>If you think you've come across something suspicious, <a href=\"/about-the-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">report it to the CFIA</a>.</p>\n\n<p>You can also prevent the spread of invasive insects, plants and other plant pests by only buying or gathering local firewood and by planting native species.</p>\n\n<h2>Learn more</h2>\n\n<ul>\n<li><a href=\"/inspect-and-protect/science-and-innovation/cfia-s-trading-cards/eng/1568731957675/1568743720700\">Do you have what it takes to be a superhero?</a></li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_meet_ray_knight_600x600_1648492922364_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>Je m'appelle Ray Knight, et j'ai toujours eu un lien \u00e9troit avec la nature.</p>\n\n<p>Enfant, je campais, chassais et p\u00eachais beaucoup avec ma famille. Cela m'a inspir\u00e9 \u00e0 poursuivre mes \u00e9tudes et \u00e0 obtenir un dipl\u00f4me dans le domaine de la gestion des ressources foresti\u00e8res. Ensuite, j'ai commenc\u00e9 \u00e0 travailler dans les belles for\u00eats c\u00f4ti\u00e8res et int\u00e9rieures de la Colombie-Britannique (C.-B.).</p>\n\n<p>J'ai eu l'opportunit\u00e9 d'explorer certaines des for\u00eats c\u00f4ti\u00e8res les plus anciennes de la province, remplies d'arbres immenses, et j'ai \u00e9galement eu le bonheur de voir se d\u00e9velopper de nombreuses for\u00eats nouvelles en bonne sant\u00e9. Gr\u00e2ce \u00e0 cette trajectoire, j'ai acquis une meilleure compr\u00e9hension de la nature et de sa diversit\u00e9.</p>\n\n<p>Dans le cadre de mon emploi actuel d'inspecteur phytosanitaire \u00e0 l'Agence canadienne d'inspection des aliments (ACIA), je v\u00e9rifie attentivement les exp\u00e9ditions provenant de partout dans le monde pour m'assurer que ce qu'elles contiennent ne nuira pas \u00e0 cette nature \u00e0 laquelle je tiens tant.</p>\n\n<h2>De la for\u00eat au port</h2>\n\n<p>Pendant ma formation en foresterie, j'ai acquis des comp\u00e9tences sur le terrain et des techniques de collecte de donn\u00e9es que j'utilise encore aujourd'hui. Apr\u00e8s mes \u00e9tudes, j'ai \u00e9t\u00e9 t\u00e9moin des cons\u00e9quences de l'<a href=\"https://www.rncan.gc.ca/nos-ressources-naturelles/forets/feux-insectes-perturbations/principaux-insectes-et-maladies-des-forets-au-canada/dendroctone-du-pin-ponderosa/13382\">\u00e9pid\u00e9mie du dendroctone du pin ponderosa</a> en Colombie-Britannique. L'\u00e9limination de vastes zones \u00e9tait n\u00e9cessaire pour arr\u00eater les infestations de col\u00e9opt\u00e8res. Nous avons \u00e9galement effectu\u00e9 des enqu\u00eates pour enregistrer et quantifier les dommages caus\u00e9s aux for\u00eats.</p>\n\n<p>Lorsque j'ai rejoint l'\u00e9quipe de l'ACIA en 1998, j'ai poursuivi mon travail sur les esp\u00e8ces envahissantes en inspectant les mat\u00e9riaux d'emballage en bois dans les conteneurs \u00e0 leur arriv\u00e9e au port de Vancouver. Notre \u00e9quipe devait s'assurer que ces mat\u00e9riaux \u00e9taient exempts d'insectes avant que les conteneurs ne soient achemin\u00e9s \u00e0 l'importateur au Canada.</p>\n\n<p>Dans ces conteneurs, nous avons vu des choses toutes plus int\u00e9ressantes les unes que les autres\u00a0: des effets personnels aux voitures de collection, en passant par la machinerie lourde. Il fallait s'attendre \u00e0 tout, tant que ce n'\u00e9tait pas trop gros pour entrer dans un conteneur! Par ailleurs, j'ai contribu\u00e9 directement \u00e0 changer la mani\u00e8re dont le bois de calage (mat\u00e9riau d'emballage en bois utilis\u00e9 pour maintenir une cargaison dans une position pr\u00e9cise dans la cale d'un navire) est d\u00e9plac\u00e9 \u00e0 travers le monde.</p>\n\n<p>Lors d'une de ces inspections de bois de calage, j'ai trouv\u00e9 des larves du <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/longicorne-asiatique/fra/1337792721926/1337792820836\">longicorne asiatique</a>. Ce sont des larves \u00e9normes \u2013 dont la taille est proche de celle du pouce \u2013 qui se servent de leurs mandibules pour ronger le bois. J'ai fait l'erreur d'approcher mon doigt d'un peu trop pr\u00e8s, et l'un d'eux en a profit\u00e9 pour le m\u00e2cher comme s'il s'agissait d'un morceau de bois. \u00c7a a fait mal! D\u00e8s que j'ai pu \u00e9loigner ma main, nous avons fumig\u00e9 le bois de calage pour tuer les insectes. Puis, nous avons refus\u00e9 l'entr\u00e9e au Canada \u00e0 ce navire. Le longicorne asiatique est une esp\u00e8ce envahissante qui est interdite au Canada en raison des dommages qu'il peut causer aux for\u00eats canadiennes.</p>\n\n<h2>Ces phytoravageurs ne passeront pas!</h2>\n\n<p>\u00c0 l'ACIA, j'ai travaill\u00e9 sur divers dossiers, de l'<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/fra/1323990856863/1323991018946\">influenza aviaire (grippe aviaire)</a> \u00e0 l'inspection des produits v\u00e9g\u00e9taux et animaux import\u00e9s au Canada pour m'assurer qu'ils r\u00e9pondent aux exigences canadiennes, aussi bien sur le terrain que dans un bureau.</p>\n\n<p>Bien que j'appr\u00e9cie ces aspects du travail, je trouve l'inspection des navires plus int\u00e9ressante. La plupart des gens n'associent peut-\u00eatre pas les inspecteurs de l'ACIA aux navires, mais il y a deux raisons pour lesquelles nous sommes l\u00e0\u00a0: (1) prot\u00e9ger les exportations de c\u00e9r\u00e9ales et (2) emp\u00eacher l'entr\u00e9e au pays d'un insecte envahissant appel\u00e9 <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse-asiatique/fra/1330353359964/1330353499535\">spongieuse asiatique</a>.</p>\n\n<p>Le Canada exporte beaucoup de c\u00e9r\u00e9ales comme le bl\u00e9, l'avoine et l'orge. Pour transporter des quantit\u00e9s aussi importantes de grain, une grande partie de celles-ci est achemin\u00e9e par des cargos. Mon travail consiste \u00e0 inspecter les transporteurs avant qu'ils ne soient charg\u00e9s, et \u00e0 v\u00e9rifier que les navires eux-m\u00eames respectent les normes phytosanitaires (protection des v\u00e9g\u00e9taux) canadiennes. En d'autres termes, je v\u00e9rifie que chaque navire est suffisamment propre pour \u00eatre charg\u00e9. Je descends dans les cales du navire, je jette un bon coup d'oeil et je dirige tout nettoyage n\u00e9cessaire. En m\u00eame temps, j'interagis avec l'\u00e9quipage et je rencontre toutes sortes de personnes int\u00e9ressantes.</p>\n\n<p>Les inspections visant \u00e0 v\u00e9rifier l'absence de la spongieuse asiatique dans les navires entrants sont destin\u00e9es \u00e0 prot\u00e9ger l'environnement du Canada \u2013 et pour cause. Si cette esp\u00e8ce \u00e9tablissait sa demeure ici, elle poserait un grave risque pour notre environnement et notre \u00e9conomie, en ayant un impact sur nos \u00e9changes avec d'autres pays. Originaire du Japon, de la Chine, de la Cor\u00e9e et de l'Extr\u00eame-Orient russe, cet insecte nuisible aime embarquer sur des navires. Les femelles pondent des \u0153ufs tout au long de l'ann\u00e9e. Notre \u00e9quipe v\u00e9rifie donc que les \u0153ufs ont \u00e9t\u00e9 enlev\u00e9s avant l'arriv\u00e9e du navire au Canada et prend des mesures rapides si certains ont pu \u00e9chapper au nettoyage.</p>\n\n<p>Les inspections de navires sont notre derni\u00e8re ligne de d\u00e9fense avant que les produits qui entrent dans notre pays ne soient mis en circulation. Il s'agit d'une \u00e9tape importante du processus pour s'assurer que les contr\u00f4les en place sont efficaces.</p>\n\n<p>Bien que j'aille encore sur le terrain pour des inspections de navires, je passe la plupart de mon temps ces jours-ci \u00e0 la <a href=\"/protection-des-vegetaux/especes-envahissantes/surveillance-phytosanitaire/fra/1344466499681/1344466638872\">surveillance phytosanitaire</a>. Je dirige une \u00e9quipe d\u00e9vou\u00e9e qui recueille et enregistre des donn\u00e9es sur un \u00e9ventail de phytoravageurs, notamment le longicorne asiatique, la <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/mouche-du-bleuet/fra/1328325206503/1328325288221\">mouche du bleuet</a>, la <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/pyrale-brun-pale-de-la-pomme/fra/1326465917763/1326466022858\">pyrale brun p\u00e2le de la pomme</a> et l'<a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/agrile-du-frene/fra/1337273882117/1337273975030\">agrile du fr\u00eane</a>. En posant des pi\u00e8ges pour attraper ces insectes, en pr\u00e9levant des \u00e9chantillons de sol ou de tissu v\u00e9g\u00e9tal, en observant les plantes, en ayant recours au diagnostic de laboratoire et plus encore, notre but est de d\u00e9terminer les endroits o\u00f9 ces ravageurs sont pr\u00e9sents et ceux o\u00f9 ils sont absents.</p>\n\n<p>Nous r\u00e9alisons \u00e9galement des enqu\u00eates relatives \u00e0 la spongieuse asiatique, \u00e0 la <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/la-spongieuse/la-spongieuse/fra/1329836269430/1329836504450\">spongieuse nord-am\u00e9ricaine (nouvellement baptis\u00e9e ainsi)</a> et au <a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/scarabee-japonais/fra/1328048149161/1328048244390\">scarab\u00e9e japonais</a>. La spongieuse nord-am\u00e9ricaine et le scarab\u00e9e japonais sont pr\u00e9sents en Ontario, au Qu\u00e9bec et dans la plupart des provinces maritimes, et bien qu'ils ne soient pas \u00e9tablis en Colombie-Britannique, il y existe de petites populations. Nos enqu\u00eates visent \u00e0 d\u00e9terminer leur emplacement sur le terrain. Ensuite, nous partageons ces renseignements avec le gouvernement provincial et avec d'autres partenaires qui participent aux programmes d'\u00e9radication.</p>\n\n<h2>Prot\u00e9gez votre arri\u00e8re-cour, votre jardin, votre communaut\u00e9</h2>\n\n<p>Bien que les programmes d'\u00e9radication se r\u00e9v\u00e8lent efficaces, il reste encore du travail \u00e0 faire. Tous les Canadiens ont un r\u00f4le \u00e0 jouer pour enrayer la propagation des ravageurs envahissants \u2013 et je vous promets qu'il n'est pas n\u00e9cessaire de grimper \u00e0 bord d'un navire!</p>\n\n<p>La premi\u00e8re \u00e9tape est la prise de conscience. Apprenez-en davantage sur les <a href=\"/protection-des-vegetaux/especes-envahissantes/fra/1299168913252/1299168989280\">esp\u00e8ces envahissantes</a> et sur l'environnement dans lequel vous vivez. Savoir quoi chercher vous aidera \u00e0 prot\u00e9ger votre arri\u00e8re-cour, votre jardin et votre communaut\u00e9 des dommages caus\u00e9s par les plantes envahissantes et les phytoravageurs.</p>\n\n<p>Si vous pensez avoir trouv\u00e9 quelque chose de suspect, <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">signalez-le \u00e0 l'ACIA</a>.</p>\n\n<p>Vous pouvez \u00e9galement pr\u00e9venir la propagation d'insectes, de plantes et autres phytoravageurs invasifs en achetant ou en ramassant du bois de chauffage local et en faisant pousser des plantes indig\u00e8nes.</p>\n\n<h2>Pour en savoir plus</h2>\n\n<ul>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/cartes-a-echanger-de-l-acia/fra/1568731957675/1568743720700\">Avez-vous l'\u00e9toffe d'un superh\u00e9ros?</a></li>\n</ul>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}