{
    "dcr_id": "1519396364559",
    "title": {
        "en": "United States of America - Export requirements for milk and dairy products",
        "fr": "\u00c9tats-Unis d'Am\u00e9rique - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "html_modified": "2024-03-12 3:57:56 PM",
    "modified": "2018-03-09",
    "issued": "2018-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_usofamerica_1519396364559_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_usofamerica_1519396364559_fra"
    },
    "ia_id": "1519396365078",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "United States of America - Export requirements for milk and dairy products",
        "fr": "\u00c9tats-Unis d'Am\u00e9rique - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "label": {
        "en": "United States of America - Export requirements for milk and dairy products",
        "fr": "\u00c9tats-Unis d'Am\u00e9rique - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1519396365078",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/usa-milk-and-dairy-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/etats-unis-d-amerique-lait-et-produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "United States of America - Export requirements for milk and dairy products",
            "fr": "\u00c9tats-Unis d'Am\u00e9rique - Exigences d'exportation pour le lait et les produits laitiers"
        },
        "description": {
            "en": "Countries to which exports are currently made \u2013 United States of America",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 \u00c9tats-Unis d'Am\u00e9rique"
        },
        "keywords": {
            "en": "United States of America, export, requirements, milk, dairy products",
            "fr": "\u00c9tats-Unis d'Am\u00e9rique, exportation, exigences, lait, produits laitiers"
        },
        "dcterms.subject": {
            "en": "exports,agri-food industry,dairy products",
            "fr": "exportation,industrie agro-alimentaire,produit laitier"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exportation d'aliments et de la protect. des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "modified": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "United States of America - Export requirements for milk and dairy products",
        "fr": "\u00c9tats-Unis d'Am\u00e9rique - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Eligible/ineligible product</h2>\n<h3>Eligible</h3>\n<ul>\n<li>All dairy products\n<ul>\n<li>\"<strong>Grade\u00a0A</strong>\" milk products</li>\n<li>\"<strong>Grade\u00a0B</strong>\" milk products\n<ul>\n<li>Manufacturing grade milk and cream</li>\n<li>Finished dairy products</li>\n</ul></li>\n</ul></li>\n</ul>\n<h2>Pre-export approvals by the competent authority of the importing country</h2>\n<h3>Product</h3>\n<ul>\n<li>\"<strong>Grade\u00a0A</strong>\" milk and milk products:\n<ul>\n<li>The <a href=\"https://www.fda.gov/downloads/Food/GuidanceRegulation/GuidanceDocumentsRegulatoryInformation/Milk/UCM513508.pdf\">Pasteurized Milk Ordinance (PMO) - <abbr title=\"Portable Document Format\">PDF</abbr> (8.1\u00a0<abbr title=\"megabytes\">mb</abbr>)</a> of the United States (US) Food and Drug Administration (US FDA) serves as the basic milk sanitation standard for <a href=\"https://www.fda.gov/Food/GuidanceRegulation/GuidanceDocumentsRegulatoryInformation/Milk/ucm2007966.htm\">National Conference on Interstate Milk Shipments (NCIMS)</a> members (all 50 states and <span lang=\"es\">Puerto Rico</span>). The <abbr title=\"Pasteurized Milk Ordinance\">PMO</abbr> is a code or guideline that individual state regulations are based on.</li>\n<li>Section 11 of the <abbr title=\"Pasteurized Milk Ordinance\">PMO</abbr> prohibits the importation of \"<strong>Grade\u00a0A</strong>\" milk and milk products from non-<abbr title=\"Pasteurized Milk Ordinance\">PMO</abbr> jurisdictions unless it has been processed under regulations which are \"substantially equivalent\" to the <abbr title=\"Pasteurized Milk Ordinance\">PMO</abbr>.</li>\n</ul></li>\n<li>Non \"<strong>Grade\u00a0A</strong>\" milk and milk products:\n<ul>\n<li>\"<strong>Grade\u00a0B</strong>\" manufacturing grade milk and cream may be shipped into the <abbr title=\"United States\">US</abbr> for further processing into ice cream, cheese <abbr title=\"et cetera\">etc.</abbr> Finished dairy products such as cheese may also move into the <abbr title=\"United States\">US</abbr>. Generally no certification is required.</li>\n</ul></li>\n</ul>\n\n<h3>Import Permit</h3>\n<ul>\n<li class=\"mrgn-bttm-md\">For certain products the Animal and Plant Health Inspection Services (APHIS) of the United States Department of Agriculture (USDA) require an import permit (VS Form 16-6A).\n\n<ul>\n<li class=\"mrgn-tp-md\"><strong>Note:</strong>\tDifferent requirements exist for products depending on the end use: human consumption, or for use as animal/livestock feed.</li>\n</ul></li>\n\n<li>More information can be found in the <a href=\"http://www.aphis.usda.gov/import_export/plants/manuals/ports/downloads/apm.pdf\"><abbr title=\"Animal and Plant Health Inspection Services\">APHIS</abbr> Animal Product Manual - <abbr title=\"Portable Document Format\">PDF</abbr> (13.3\u00a0<abbr title=\"megabytes\">mb</abbr>)</a></li>\n</ul>\n<h2>Product specifications</h2>\n<ul>\n<li>The <abbr title=\"National Conference on Interstate Milk Shipments\">NCIMS</abbr> Memoranda of Information (M-I), M-I-00-4: Importation of <abbr title=\"Pasteurized Milk Ordinance\">PMO</abbr> Defined Dairy Products found at the <abbr title=\"United States\">US</abbr> <abbr title=\"Food and Drug Administration\">FDA</abbr> website <a href=\"https://www.fda.gov/Food/GuidanceRegulation/GuidanceDocumentsRegulatoryInformation/Milk/ucm2007973.htm\">Coded Memoranda Issued by Milk Safety Branch</a> provides further information.</li>\n<li>M-I-00-4 identifies three options available which would allow importation of \"<strong>Grade\u00a0A</strong>\" milk and milk products into the <abbr title=\"United States of America\">USA</abbr>:\n\n<ol>\n<li class=\"mrgn-tp-md\">A dairy firm outside of the United States could contract with any current <abbr title=\"National Conference on Interstate Milk Shipments\">NCIMS</abbr> member's regulatory/rating agency to provide the \"<strong>Grade\u00a0A</strong>\" milk safety program in total. At this time this is the only option available to Canadian exporters wanting to ship \"Grade\u00a0A\" products to the <abbr title=\"United States of America\">USA</abbr>. In the past establishments wanting to ship had to find a state that would sponsor them. Recently the option for 3<sup>rd</sup> party certifiers has come into existence. See M-I-07-2: Selection of The Three Third Party Certifiers to Participate in the National Conference on Interstate Milk Shipments Voluntary International Certification Pilot Program for further information. <strong>No certification should be required for \"<strong>Grade\u00a0A</strong>\" products moving to the <abbr title=\"United States of America\">USA</abbr> when the processor is operating under this program.</strong></li>\n<li>The importing country, or a political subdivision thereof, may become a full member of the <abbr title=\"National Conference on Interstate Milk Shipments\">NCIMS</abbr> subject to all <abbr title=\"National Conference on Interstate Milk Shipments\">NCIMS</abbr> rules and enjoying all privileges of a <abbr title=\"United States\">US</abbr> state. <strong>Neither Canada nor any of its provinces are members of the <abbr title=\"National Conference on Interstate Milk Shipments\">NCIMS</abbr>.</strong></li>\n<li><abbr title=\"United States\">US</abbr> <abbr title=\"Food and Drug Administration\">FDA</abbr> can evaluate the importing country's system of assuring the safety of dairy products and compare the effect of that system with the effect of the United States system on the safety of dairy products produced domestically. <strong>Equivalency discussions between Canada and the <abbr title=\"United States of America\">USA</abbr> are ongoing.</strong>\n</li></ol></li>\n</ul>\n<h2>Documentation requirements</h2>\n<h3>Certificate</h3>\n<p><strong>Note:</strong>\tCertification is not required for \"Grade\u00a0A\" products moving to the <abbr title=\"United States of America\">USA</abbr> when the processor is operating under contract with any current <abbr title=\"National Conference on Interstate Milk Shipments\">NCIMS</abbr> member's regulatory/rating agency to provide the \"<strong>Grade\u00a0A</strong>\" milk safety program. \"<strong>Grade\u00a0B</strong>\" manufacturing grade milk and cream may be shipped into the <abbr title=\"United States\">US</abbr> for further processing into ice cream, cheese, <abbr title=\"et cetera\">etc.</abbr> Finished dairy products such as cheese may also move into the <abbr title=\"United States\">US</abbr>. Generally no certification is required. If requested the dairy standard certificate is used.</p>\n\n<ul>\n<li class=\"mrgn-bttm-md\">Health certificate for the export of dairy products and dairy based products for human consumption\n\n<ul>\n<li class=\"mrgn-tp-md\"><strong>Note:</strong>\tFor \"<strong>Grade\u00a0B</strong>\" milk and milk products</li>\n</ul></li>\n\n<li>Export certificate for the export of dairy products and dairy based foods from Canada to the United States of America\n\n<ul>\n<li class=\"mrgn-tp-md\"><strong>Note:</strong>\tFor milk and milk products with vitamin D3/cholesterol components derived from wool grease of sheep imported into Canada that originated in Argentina, Australia, <span lang=\"es\">Chile</span>, Falkland Islands, New Zealand, Uruguay and/or the United States.</li>\n</ul></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Produits admissibles/non admissibles</h2>\n<h3>Admissibles</h3>\n<ul><li>Tous les produits laitiers\n<ul>\n<li>Produits laitiers de \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb</li>\n<li>Produits laitiers de \u00ab\u00a0<strong>Grade\u00a0B</strong>\u00a0\u00bb\n<ul>\n<li>Lait et cr\u00e8me de qualit\u00e9 industrielle</li>\n<li>Produits laitiers finis</li>\n</ul></li>\n</ul></li>\n</ul>\n<h2>Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n<h3>Produit</h3>\n<ul>\n<li>Lait et produits laitiers de \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb\u00a0:\n<ul>\n<li>L'<a href=\"https://www.fda.gov/downloads/Food/GuidanceRegulation/GuidanceDocumentsRegulatoryInformation/Milk/UCM513508.pdf\">Ordonnance sur le lait pasteuris\u00e9 (PMO) (anglais seulement) - <abbr title=\"format de document portable\">PDF</abbr> (8,1\u00a0<abbr title=\"mega-octet\">mo</abbr>)</a> de la <span lang=\"en\">Food and Drug Administration</span> (US FDA) aux \u00c9tats-Unis (US) sert de norme de base pour l'hygi\u00e8ne du lait pour les membres (les 50 \u00c9tats et Porto Rico) de la <a href=\"https://www.fda.gov/Food/GuidanceRegulation/GuidanceDocumentsRegulatoryInformation/Milk/ucm2007966.htm\">Conf\u00e9rence nationale sur les livraisons de lait inter-\u00c9tats (NCIMS) (anglais seulement)</a>. Le <abbr title=\"Ordonnance sur le lait pasteuris\u00e9\">PMO</abbr> est un code ou une ligne directrice sur laquelle les r\u00e9glementations nationales sont bas\u00e9es.</li>\n<li>L'article 11 du <abbr title=\"Ordonnance sur le lait pasteuris\u00e9\">PMO</abbr> interdit l'importation de lait et de produits laitiers de \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb provenant de territoires autres que les <abbr title=\"Ordonnance sur le lait pasteuris\u00e9\">PMO</abbr>, \u00e0 moins qu'ils n'aient \u00e9t\u00e9 trait\u00e9s en vertu de r\u00e8glements \u00ab\u00a0substantiellement \u00e9quivalents\u00a0\u00bb au <abbr title=\"Ordonnance sur le lait pasteuris\u00e9\">PMO</abbr>.</li>\n</ul></li>\n<li>Lait et produits laitiers autres que de \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb\u00a0:\n<ul>\n<li>Le lait et la cr\u00e8me de qualit\u00e9 de \u00ab\u00a0<strong>Grade\u00a0B</strong>\u00a0\u00bb peuvent \u00eatre exp\u00e9di\u00e9s aux \u00c9tats-Unis pour \u00eatre transform\u00e9s en cr\u00e8me glac\u00e9e, en fromage, <abbr title=\"et cetera\">etc.</abbr> Les produits laitiers finis comme le fromage peuvent \u00e9galement \u00eatre import\u00e9s aux \u00c9tats-Unis. En r\u00e8gle g\u00e9n\u00e9rale, aucune certification n'est requise.</li>\n</ul></li>\n</ul>\n<h3>Permis d'importation</h3>\n<ul>\n<li class=\"mrgn-bttm-md\">Pour certains produits, les Services d'inspection zoosanitaire et phytosanitaire (APHIS) du D\u00e9partement de l'agriculture aux \u00c9tats-Unis (USDA) exigent un permis d'importation (formulaire 16-6A de VS).\n\n<ul>\n<li class=\"mrgn-tp-md\"><strong>Remarque\u00a0:</strong>\tDes exigences diff\u00e9rentes existent pour les produits en fonction de l'utilisation finale\u00a0: la consommation humaine, ou pour une utilisation en tant qu'aliment animal / b\u00e9tail.</li>\n</ul></li>\n\n<li>Plus d'informations peuvent \u00eatre trouv\u00e9es dans le Manuel du Produit Animal <abbr title=\"Services d'inspection zoosanitaire et phytosanitaire\">APHIS</abbr></li>\n</ul>\n<h2>Sp\u00e9cifications du produit</h2>\n<ul>\n<li>Le m\u00e9morandum d'information <abbr title=\"Conf\u00e9rence nationale sur les livraisons de lait inter-\u00c9tats\">NCIMS</abbr> (M-I), M-I-00-4\u00a0: Importation de produits laitiers d\u00e9finis par le <abbr title=\"Ordonnance sur le lait pasteuris\u00e9\">PMO</abbr> trouv\u00e9 sur le site Web de la <abbr title=\"\u00c9tats-Unis\">US</abbr> <abbr lang=\"en\" title=\"Food and Drug Administration\">FDA</abbr> fournit plus d'informations - <span lang=\"en\">Coded Memoranda Issued by Milk Safety Branch</span>.</li>\n<li>M-I-00-4 pr\u00e9sente trois options possibles permettant l'importation de lait et de produits laitiers de \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb aux \u00c9tats-Unis\u00a0:\n\n<ol>\n<li class=\"mrgn-tp-md\">Une firme laiti\u00e8re en dehors des \u00c9tats-Unis peut passer un contrat avec n'importe quelle firme membre de l'agence r\u00e9glementaire/\u00e9valuatrice de la <abbr title=\"commerce inter-\u00e9tat du lait\">NCIMS</abbr> pour se pr\u00e9valoir d'un programme complet de s\u00e9curit\u00e9 du lait \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb. C'est l'option disponible aux exportateurs canadiens d\u00e9sireux d'exp\u00e9dier des produits \u00ab\u00a0Grade\u00a0A\u00a0\u00bb aux \u00c9tats-Unis.  \u00c9galement, l'option d'une tierce partie pour obtenir la certification existe depuis r\u00e9cemment. Voir M-I-07-2 pour plus d'information sur la s\u00e9lection des trois tierces parties pour obtenir la certification en vue de participer au programme pilote international de certification volontaire de la Conf\u00e9rence nationale des \u00c9tats faisant partie du commerce inter-\u00e9tat du lait (NCIMS). <strong>Aucune certification ne devrait \u00eatre requise pour les produits <span class=\"nowrap\">\u00ab Grade A \u00bb</span> qui sont exp\u00e9di\u00e9s aux \u00c9tats-Unis lorsque le fabricant op\u00e8re sous ce programme.</strong></li>\n<li>Le pays importateur, ou une subdivision politique de ce pays, peut devenir un membre de plein droit de la <abbr title=\"commerce inter-\u00e9tat du lait\">NCIMS</abbr>, sujet \u00e0 toutes les r\u00e8gles de la <abbr title=\"commerce inter-\u00e9tat du lait\">NCIMS</abbr> et jouir de tous les privil\u00e8ges d'un \u00c9tat des \u00c9tats-Unis. <strong>Ni le Canada ni aucune de ses provinces sont membres de la <abbr title=\"commerce inter-\u00e9tat du lait\">NCIMS</abbr>.</strong></li>\n<li>La <abbr title=\"\u00c9tats-Unis\">US</abbr> <abbr lang=\"en\" title=\"Food and Drug Administration\">FDA</abbr> peut \u00e9valuer le syst\u00e8me assurant la s\u00e9curit\u00e9 des produits laitiers du pays importateur et  comparer les composantes de ce syst\u00e8me avec les composantes du syst\u00e8me aux \u00c9tats-Unis sur la s\u00e9curit\u00e9 des produits laitiers produits sur le march\u00e9 int\u00e9rieur. <strong>Des discussions d'\u00e9quivalence entre le Canada et les \u00c9tats-Unis sont en cours.</strong></li>\n</ol></li>\n</ul>\n<h2>Documents requis</h2>\n<h3>Certificat</h3>\n<p><strong>Remarque\u00a0:</strong>\tLa certification n'est pas exig\u00e9e pour les produits de \u00ab\u00a0Grade\u00a0A\u00a0\u00bb qui d\u00e9m\u00e9nagent aux \u00c9tats-Unis lorsque le transformateur exploite un contrat avec l'agence de r\u00e9glementation ou de notation du membre <abbr title=\"commerce inter-\u00e9tat du lait\">NCIMS</abbr> actuel pour fournir le programme de salubrit\u00e9 du lait de \u00ab\u00a0<strong>Grade\u00a0A</strong>\u00a0\u00bb. Le lait et la cr\u00e8me de qualit\u00e9 \u00ab\u00a0<strong>Grade\u00a0B</strong>\u00a0\u00bb peuvent \u00eatre exp\u00e9di\u00e9s aux \u00c9tats-Unis pour \u00eatre transform\u00e9s en cr\u00e8me glac\u00e9e, en fromage, <abbr title=\"et cetera\">etc.</abbr> Les produits laitiers finis comme le fromage peuvent \u00e9galement \u00eatre import\u00e9s aux \u00c9tats-Unis. G\u00e9n\u00e9ralement, aucune certification n'est requise. Si demand\u00e9, le certificat standard laitier est utilis\u00e9.</p>\n\n<ul>\n<li class=\"mrgn-bttm-md\">Certificat sanitaire pour l'exportation de produits laitiers et d'aliments lact\u00e9s pour consommation humaine\n\n<ul>\n<li class=\"mrgn-tp-md\"><strong>Remarque\u00a0:</strong>\tPour le lait et produits laitiers de \u00ab\u00a0<strong>Grade\u00a0B</strong>\u00a0\u00bb</li>\n</ul></li>\n\n<li>Certificat d'exportation pour l'exportation de produits laitiers et d'aliments lact\u00e9s en provenance du Canada vers les \u00c9tats-Unis d'Am\u00e9rique\n\n<ul>\n<li class=\"mrgn-tp-md\"><strong>Remarque\u00a0:</strong>\tPour le lait et les produits laitiers contenant des composants de la vitamine D3 / cholest\u00e9rol provenant de la laine de mouton import\u00e9e au Canada et originaire d'Argentine, d'Australie, du Chili, des \u00eeles <span lang=\"en\">Falkland</span>, de Nouvelle-Z\u00e9lande, d'Uruguay et / ou des \u00c9tats-Unis.</li>\n</ul></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}