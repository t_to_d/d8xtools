{
    "dcr_id": "1306892984827",
    "title": {
        "en": "Switzerland\u00a0- Disease freedom recognition",
        "fr": "Suisse\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "html_modified": "2024-02-13 9:46:47 AM",
    "modified": "2023-04-20",
    "issued": "2011-05-31 21:49:47",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_recognition_swiss_1306892984827_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_recognition_swiss_1306892984827_fra"
    },
    "ia_id": "1306893205140",
    "parent_ia_id": "1306649135327",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1306649135327",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Switzerland\u00a0- Disease freedom recognition",
        "fr": "Suisse\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "label": {
        "en": "Switzerland\u00a0- Disease freedom recognition",
        "fr": "Suisse\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "templatetype": "content page 1 column",
    "node_id": "1306893205140",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1306648587424",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/status-by-country/switzerland/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/statut-par-pays/suisse/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Switzerland\u00a0- Disease freedom recognition",
            "fr": "Suisse\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
        },
        "description": {
            "en": "Switzerland is officially recognized by Canada as free of certain diseases listed in the World Organisation for Animal Health (WOAH) Terrestrial Animal Code.",
            "fr": "Suisse est officiellement reconnus par le Canada comme \u00e9tant indemnes de certaines des maladies figurant dans le Code sanitaire pour les animaux terrestres de l'Organisation mondiale de la sant\u00e9 animale (OMSA)."
        },
        "keywords": {
            "en": "animals, disease, freedom, World Organisation for Animal Health, WOAH, Terrestrial Animal Code, Switzerland",
            "fr": "animaux, maladies d\u00e9clarables, indemnes, Organisation mondiale de la sant\u00e9 animale, OMSA, Code sanitaire pour les animaux terrestres, Suisse"
        },
        "dcterms.subject": {
            "en": "imports,inspection,veterinary medicine,animal health",
            "fr": "importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-31 21:49:47",
            "fr": "2011-05-31 21:49:47"
        },
        "modified": {
            "en": "2023-04-20",
            "fr": "2023-04-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Switzerland\u00a0- Disease freedom recognition",
        "fr": "Suisse\u00a0- Reconnaissance du statut indemne de certaines maladies animales"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Switzerland is officially recognized by Canada as free of certain diseases listed in the World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE)) Terrestrial animal code.</p>\n<ul class=\"colcount-sm-2\">\n<li>African horse sickness</li>\n<li>African swine fever</li>\n<li>Contagious bovine pleuropneumonia</li>\n<li>Foot-and-mouth disease</li>\n<li>Highly pathogenic avian influenza</li>\n<li>Lumpy skin disease</li>\n<li>Newcastle disease</li>\n<li><span lang=\"fr\">Peste des petits ruminants</span></li>\n<li>Rift valley fever</li>\n<li>Sheep pox and goat pox</li>\n<li>Swine vesicular disease</li>\n<li>Vesicular stomatitis</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>La Suisse est officiellement reconnue par le Canada comme \u00e9tant indemne de certaines des maladies figurant dans le Code sanitaire pour les animaux terrestres de l'Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)).</p>\n<ul class=\"colcount-sm-2\">\n<li>Clavel\u00e9e et variole caprine</li>\n<li>Dermatose nodulaire contagieuse</li>\n<li>Fi\u00e8vre aphteuse</li>\n<li>Fi\u00e8vre de la vall\u00e9e du <span lang=\"en\">Rift</span></li>\n<li>Influenza aviaire hautement pathog\u00e8ne</li>\n<li>Maladie de <span lang=\"en\">Newcastle</span></li>\n<li>Maladie v\u00e9siculeuse du porc</li>\n<li>Peste des petits ruminants</li>\n<li>Peste \u00e9quine</li>\n<li>Peste porcine africaine</li>\n<li>Pleuropneumonie contagieuse bovine</li>\n<li>Stomatite v\u00e9siculeuse</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}