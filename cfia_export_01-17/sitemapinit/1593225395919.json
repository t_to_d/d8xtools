{
    "dcr_id": "1593225395528",
    "title": {
        "en": "The Establishment-based Risk Assessment model for feed mills",
        "fr": "Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail"
    },
    "html_modified": "21-7-2020",
    "modified": "24-9-2021",
    "issued": "26-6-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/estblshmnt-based_risk_assessment_model_for_feed_mills_1593225395528_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/estblshmnt-based_risk_assessment_model_for_feed_mills_1593225395528_fra"
    },
    "parent_ia_id": "1387308992607",
    "ia_id": "1593225395919",
    "parent_node_id": "1387308992607",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The Establishment-based Risk Assessment model for feed mills",
        "fr": "Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail"
    },
    "label": {
        "en": "The Establishment-based Risk Assessment model for feed mills",
        "fr": "Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail"
    },
    "templatetype": "content page 1 column",
    "node_id": "1593225395919",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1387308991466",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/inspection-program/feed-mills/",
        "fr": "/sante-des-animaux/aliments-du-betail/inspection-des-aliments-du-betail/les-meuneries-d-aliments-du-betail/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The Establishment-based Risk Assessment model for feed mills",
            "fr": "Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail"
        },
        "description": {
            "en": "In a world of changing risks, innovation and new technologies, the Canadian Food Inspection Agency (CFIA) is adapting to be more efficient and responsive. Risk based decision making is at the core of the Agency's everyday work.",
            "fr": "Dans un monde d'innovation et de nouvelles technologies dans lequel les risques \u00e9voluent, l'Agence canadienne d'inspection des aliments (ACIA) s'adapte pour \u00eatre plus efficace et r\u00e9active. La prise de d\u00e9cisions fond\u00e9e sur le risque est au c\u0153ur du travail quotidien de l'Agence."
        },
        "keywords": {
            "en": "Industry Guidance, Establishment-based Risk Assessment model, Feed Mills, ERA",
            "fr": "Lignes directrices pour l'industrie, Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements, les meuneries d'aliments du b\u00e9tail, ERE"
        },
        "dcterms.subject": {
            "en": "food,quality control,government information,inspection,standards,regulation,animal health",
            "fr": "aliment,contr\u00f4le de la qualit\u00e9,information gouvernementale,inspection,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-07-21",
            "fr": "2020-07-21"
        },
        "modified": {
            "en": "2021-09-24",
            "fr": "2021-09-24"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Establishment-based Risk Assessment model for feed mills",
        "fr": "Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>In a world of changing risks, innovation and new technologies, the Canadian Food Inspection Agency (CFIA) is adapting to be more efficient and responsive. Risk-informed decision making is at the core of the agency's everyday work.</p>\n\n<p>The CFIA has been evolving the way we manage risk to further support industry's ability to compete globally. We've been embracing technology to offer more efficient and responsive services.</p>\n\n<p>The Establishment-based Risk Assessment Model for feed mills (ERA-Feed Mill) is a tool developed by the CFIA to evaluate commercial and on-farm feed mills based on the level of risk that the feed they distribute represents for the health of food animals in Canada. It also considers the risk the food products (such as milk, meat, egg) derived from these animals may represent to the consumers. The ERA-Feed Mill model uses establishment data and a mathematical algorithm to assess the feed safety risks of feed mills under CFIA's jurisdiction. It takes into consideration:</p>\n<ul>\n<li>risks associated with different types of feeds and specific operation processes</li>\n<li>mitigation strategies implemented by the industry to control their feed safety risks</li>\n<li>feed mill compliance information</li>\n</ul>\n\n<p>The ERA-Feed Mill model will be used, along with other factors, to inform where inspectors should spend more or less time and program planning to focus efforts on areas of highest risk.</p>\n\n<h2>How the Establishment-based Risk Assessment model for feed mills works</h2>\n\n<p>The ERA-Feed Mill model uses scientific data and establishment-specific information to evaluate a feed mill and determine its level of risk. How often an inspection occurs will be guided by the risk category in which a feed mill falls, as assigned by the ERA-Feed Mill model. Higher risk feed mills require more oversight while lower risk feed mills require less oversight.</p>\n\n<h2>Using collaboration and innovation</h2>\n\n<p>The ERA-Feed Mill model was developed by CFIA staff in collaboration with experts from academia, industry and other government departments. The development of this model also drew on the experience acquired while developing the ERA model for domestic food establishments and the ERA model for hatcheries.</p>\n\n<p>The ERA-Feed Mill model has already garnered attention on the international stage. Scientific journals that publish the latest studies on risk analysis related to food and feed safety such as <a href=\"https://www.sciencedirect.com/science/article/pii/S0956713520305582?via%3Dihub\">Food Control</a> and the <a href=\"https://meridian.allenpress.com/jfp/article/doi/10.4315/JFP-20-371/448758/Expert-elicitation-to-estimate-the-feed-safety\">Journal of Food Protection</a> have published articles detailing the development steps of the CFIA's ERA-Feed Mill model.</p>\n\n<h2>Resources</h2>\n\n<ul>\n<li><a href=\"/animal-health/livestock-feeds/inspection-program/feed-mills/the-science-behind-it/eng/1593223226607/1593223226951\">ERA-Feed Mill model: the science behind it</a></li>\n<li><a href=\"/animal-health/livestock-feeds/inspection-program/feed-mills/infographic/eng/1593555940878/1593555941190\">Infographic: ERA-Feed Mill model</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Dans un monde d'innovation et de nouvelles technologies dans lequel les risques \u00e9voluent, l'Agence canadienne d'inspection des aliments (ACIA) s'adapte pour \u00eatre plus efficace et r\u00e9active. La prise de d\u00e9cisions fond\u00e9e sur le risque est au c\u0153ur du travail quotidien de l'agence.</p>\n\n<p>La fa\u00e7on dont l'ACIA g\u00e8re les risques a \u00e9volu\u00e9 de fa\u00e7on \u00e0 appuyer davantage la capacit\u00e9 de l'industrie \u00e0 \u00eatre concurrentielle \u00e0 l'\u00e9chelle mondiale. Nous avons eu recours \u00e0 la technologie pour offrir des services plus efficaces et plus r\u00e9actifs.</p>\n\n<p>Le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail (ERE-Meunerie) est un outil d\u00e9velopp\u00e9 par l'ACIA pour \u00e9valuer les meuneries commerciales et les meuneries \u00e0 la ferme en fonction du niveau de risque que les aliments du b\u00e9tail distribu\u00e9s repr\u00e9sentent pour la sant\u00e9 des animaux au Canada. Il prend \u00e9galement en consid\u00e9ration le risque que les aliments (comme le lait, la viande, les \u0153ufs) provenant de ces animaux pourrait repr\u00e9senter pour les consommateurs. Le mod\u00e8le ERE-Meunerie utilise les donn\u00e9es des \u00e9tablissements et un algorithme math\u00e9matique pour \u00e9valuer les risques reli\u00e9s \u00e0 la salubrit\u00e9 des aliments du b\u00e9tail dans les meuneries sous la juridiction de l'ACIA. Il tient compte\u00a0:</p>\n<ul>\n<li>des risques associ\u00e9s \u00e0 diff\u00e9rents types d'aliments du b\u00e9tail et \u00e0 des processus d'exploitation sp\u00e9cifiques</li>\n<li>des strat\u00e9gies d'att\u00e9nuation mises en \u0153uvre par l'industrie pour ma\u00eetriser ses risques en mati\u00e8re de salubrit\u00e9 des aliments du b\u00e9tail</li>\n<li>des informations relatives \u00e0 la conformit\u00e9 des meuneries</li>\n</ul>\n\n<p>Le mod\u00e8le ERE-Meunerie sera utilis\u00e9, avec d'autres facteurs, pour mettre en \u00e9vidence les endroits o\u00f9 les inspecteurs devraient passer plus ou moins de temps et pour la planification des programmes, afin de concentrer les efforts sur les secteurs les plus \u00e0 risque.</p>\n\n<h2>Comment fonctionne le mod\u00e8le d'\u00e9valuation du risque ax\u00e9 sur les \u00e9tablissements pour les meuneries d'aliments du b\u00e9tail</h2>\n\n<p>Le mod\u00e8le ERE-Meunerie utilise des donn\u00e9es scientifiques et l'information sp\u00e9cifique \u00e0 l'\u00e9tablissement pour \u00e9valuer la meunerie et d\u00e9terminer son niveau de risque. La fr\u00e9quence des inspections sera orient\u00e9e par la cat\u00e9gorie de risque dans laquelle se situe la meunerie, tel qu'assign\u00e9e par le mod\u00e8le ERE-Meunerie. Les meuneries pr\u00e9sentant un risque \u00e9lev\u00e9 exigent davantage de surveillance alors que les meuneries pr\u00e9sentant un risque faible en exigent moins.</p>\n\n<h2>Recours \u00e0 la collaboration et l'innovation</h2>\n\n<p>Le mod\u00e8le ERE-Meunerie a \u00e9t\u00e9 d\u00e9velopp\u00e9 par le personnel de l'ACIA en collaboration avec des experts du milieu universitaire et de l'industrie. Ce mod\u00e8le a \u00e9galement \u00e9t\u00e9 \u00e9labor\u00e9 en s'appuyant sur l'exp\u00e9rience acquise lors du d\u00e9veloppement du mod\u00e8le ERE pour les \u00e9tablissements alimentaires domestiques et du mod\u00e8le ERE pour les couvoirs.</p>\n\n<p>Le mod\u00e8le ERE-Meunerie a d\u00e9j\u00e0 retenu l'attention sur la sc\u00e8ne internationale. Des revues scientifiques publiant les \u00e9tudes les plus r\u00e9centes en mati\u00e8re d'analyse des risques li\u00e9s \u00e0 la salubrit\u00e9 alimentaire et \u00e0 la salubrit\u00e9 des aliments du b\u00e9tail telles que <a href=\"https://www.sciencedirect.com/science/article/pii/S0956713520305582?via%3Dihub\"><span lang=\"en\">Food Control</span> (en anglais seulement)</a> et <a href=\"https://meridian.allenpress.com/jfp/article/doi/10.4315/JFP-20-371/448758/Expert-elicitation-to-estimate-the-feed-safety\"><span lang=\"en\">Journal of Food Protection</span> (en anglais seulement)</a> ont publi\u00e9 des articles d\u00e9taillant les \u00e9tapes d'\u00e9laboration du mod\u00e8le ERE-Meunerie de l'ACIA.</p>\n\n<h2>Ressources</h2>\n\n<ul>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/inspection-des-aliments-du-betail/les-meuneries-d-aliments-du-betail/le-principe-scientifique/fra/1593223226607/1593223226951\">Le mod\u00e8le ERE-Meunerie\u00a0: le principe scientifique</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/inspection-des-aliments-du-betail/les-meuneries-d-aliments-du-betail/infographie/fra/1593555940878/1593555941190\">Infographie\u00a0: le mod\u00e8le ERE-Meunerie</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}