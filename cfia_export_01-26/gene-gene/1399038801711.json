{
    "dcr_id": "1399038801711",
    "title": {
        "en": "Requirements for livestock carriers: Livestock Identification and Traceability Program",
        "fr": "Exigences pour les transporteurs d'animaux d'\u00e9levage : Programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage "
    },
    "html_modified": "2024-01-26 2:22:24 PM",
    "modified": "2018-11-20",
    "issued": "2014-06-04",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_trace_reg_description_carriers_1399038801711_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_trace_reg_description_carriers_1399038801711_fra"
    },
    "ia_id": "1399038908890",
    "parent_ia_id": "1374449599425",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Traceability",
        "fr": "Tra\u00e7abilit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1374449599425",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Requirements for livestock carriers: Livestock Identification and Traceability Program",
        "fr": "Exigences pour les transporteurs d'animaux d'\u00e9levage : Programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage "
    },
    "label": {
        "en": "Requirements for livestock carriers: Livestock Identification and Traceability Program",
        "fr": "Exigences pour les transporteurs d'animaux d'\u00e9levage : Programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage "
    },
    "templatetype": "content page 1 column",
    "node_id": "1399038908890",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1374449598457",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/traceability/description/requirements-for-livestock-carriers/",
        "fr": "/sante-des-animaux/animaux-terrestres/tracabilite/description/exigences-pour-les-transporteurs-d-animaux-d-eleva/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Requirements for livestock carriers: Livestock Identification and Traceability Program",
            "fr": "Exigences pour les transporteurs d'animaux d'\u00e9levage : Programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage"
        },
        "description": {
            "en": "This brochure provides an overview of federal livestock identification and traceability requirements. Note that provincial and territorial requirements may also apply.",
            "fr": "La pr\u00e9sente brochure donne un aper\u00e7u des exigences f\u00e9d\u00e9rales en mati\u00e8re d'identification et de tra\u00e7abilit\u00e9 des animaux d\u2019\u00e9levage. Pri\u00e8re de noter que des exigences provinciales et territoriales peuvent \u00e9galement s'appliquer."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, adminstrators, traceability, surveillance, animal health, disease, detection, identification, description, regulatory requirements, requirements for livestock carriers, livestock identification, traceability Program",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, programme d'identification du b\u00e9tail, tra\u00e7abilit\u00e9, surveillance, sant\u00e9 animal, maladie, d\u00e9tection, identification, bovins, bisons, moutons, porcs, exigences pour les transporteurs d'animaux d'\u00e9levage, programme d'identification, tra\u00e7abilit\u00e9 des animaux d'\u00e9levage"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,standards,regulation,animal health",
            "fr": "b\u00e9tail,inspection,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-06-04",
            "fr": "2014-06-04"
        },
        "modified": {
            "en": "2018-11-20",
            "fr": "2018-11-20"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Requirements for livestock carriers: Livestock Identification and Traceability Program",
        "fr": "Exigences pour les transporteurs d'animaux d'\u00e9levage : Programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/eng/1672964761819\"></div>\n\n<p class=\"text-right\">\n<a href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/terr_anima_trace_reg_description_carriers_1468936660272_eng.pdf\" title=\"Requirements for Livestock Carriers in portable document format\">PDF (257 kb)<br>(Second Version)</a>\n</p>\n\n<div class=\"well well-sm\">\n<p class=\"mrgn-bttm-sm\">The objective of the national Livestock Identification and Traceability program is to provide accurate and up-to-date livestock identity, movement and location information to mitigate the impact of disease outbreaks, food safety issues and natural disasters.</p>\n\n</div>\n\n<p>This brochure provides an overview of federal livestock identification and traceability requirements. Note that provincial and territorial requirements may also apply.</p>\n\n<p>The guidance in this brochure is not a substitute for the law. Therefore, it is important for regulated parties who use this guidance to apply it in accordance with and within the context of the applicable sections of Part XV (Animal Identification) of the Health of Animals Regulations.</p>\n\n<h2>Overview of general requirements</h2>\n\n<p><strong>It is prohibited to transport cattle, bison and sheep that do not bear an approved tag.</strong></p>\n\n<h3>Lost tags during transport</h3>\n\n<p>You can continue to transport an animal that has lost its approved tag while being transported.</p>\n\n<h3>Identifying dead stock</h3>\n\n<p><strong>Cattle, bison and sheep dead stock</strong> must be identified with an approved tag if they are moved off the farm of origin (or from any other site after having left the farm of origin) for disposal.</p>\n\n<p><strong>Pig dead stock</strong>do not need to be identified with an approved tag or approved slap tattoo, but they shall be accompanied by required information outlined in this brochure.</p>\n\n<h2>Overview of requirements specific to cattle, bison and sheep</h2>\n\n<p>Cattle, bison and sheep do not need to be identified with an approved tag while living on their farm of origin.</p>\n\n<p>However, you cannot transport cattle, bison or sheep off their farm of origin (or any other site after they have left their farm of origin) unless they bear an approved tag. </p>\n\n<p>The only exception to this requirement is for live cattle and bison that are sent to a tagging site listed on the web site of the Canadian Cattle Identification Agency.</p>\n\n<h2>Overview of requirements specific to pigs</h2>\n\n<h3>Transporting pigs</h3>\n\n<p>Please note that requirements for pigs also apply to farmed wild boars.</p>\n\n<h4>What are the requirements when I move pigs between contiguous part of the same farm?</h4>\n\n<p>The pigs are not required to be identified with an approved tag or approved slap tattoo nor being accompanied with information on a form.</p>\n\n<h4>What are the requirements when I move pigs between parts of a farm that are not contiguous or between different farms?</h4>\n\n<p>The pigs are not required to be identified with an approved tag if</p>\n\n<ul>\n<li>they are accompanied by a form that can be immediately read by an inspector, and</li>\n<li>the operators of both the departure and destination sites report the animals' movements to the Canadian Pork Council (their responsible administrator).</li>\n</ul>\n\n<p><strong>Exception:</strong> Bred pigs must be identified with approved tags with a number unique to the animal before they leave the departure site.</p>\n\n<h4>What are the requirements when I move pigs between farms that are registered as linked?</h4>\n\n<p>The pigs are not required to be identified with an approved tag unless they are bred pigs. There is no requirement to have information accompanying their movements</p>\n\n<h4>What are the requirements when I move pigs to sites such as auctions, fairs or insemination centres?</h4>\n\n<p>The pigs must be identified with an approved tag before they leave the departure site; the number on the approved tag must be unique to each pig.</p>\n\n<h4>What are the requirements when I move pigs to assembly yards used exclusively to collect pigs before sending them directly to slaughter?</h4>\n\n<p>The pigs must be identified either with an approved tag or an approved slap tattoo before they leave the farm.</p>\n\n<p>The approved tag can either be a herd mark (identification number that is unique to the site of departure) or an individual identification number that is unique to each pig.</p>\n\n<h4>What are the requirements when I move pigs directly from a farm to an abattoir?</h4>\n\n<p>The pigs must be identified either with an approved tag with a number unique to the animal, an approved tag with a herd mark or an approved slap tattoo.</p>\n\n<p>In all cases where you are transporting pigs that are required to be identified, you must make sure they are bearing the proper identification before loading them in the conveyance.</p>\n\n<h2>Overview of the requirements for the form accompanying pigs</h2>\n\n<p>There are two circumstances where information collected on a form must accompany pigs and pig carcasses:</p>\n\n<ul>\n<li>the transportation of pigs that are not bred pigs between non-contiguous parts of a farm or between farms</li>\n<li>the transportation of pig carcasses to any site.</li>\n</ul>\n\n<p>The form can be either electronic or paper, provided that</p>\n\n<ul>\n<li>all of the required information is clearly indicated, and</li>\n<li>it can be easily read by an inspector.</li>\n</ul>\n\n<p>The form needs to contain the following information:</p>\n\n<ul>\n<li>the location of the sites where you are loading the pigs AND where you are unloading them;</li>\n<li>the identification number on any approved tag applied to the pigs;</li>\n<li>the date and time you left the departure site;</li>\n<li>in the case of live animals, the number of pigs that you are transporting; and</li>\n<li>the license plate of the conveyance being used.</li>\n</ul>\n\n\n<h2>De\ufb01nitions</h2>\n\n<dl class=\"dl-horizontal\">\n<dt>Herd mark:</dt>\n<dd>An identification number unique to a group of animals that originate from the same site.</dd>\n\n<dt><a href=\"/animal-health/terrestrial-animals/traceability/indicators/eng/1331582406844/1331582476216\">Approved tag</a>:</dt>\n<dd>Refers to tags approved under the livestock identification and traceability program and listed on the CFIA website.</dd>\n\n<dt>Bred:</dt>\n<dd>Animals that are mated either naturally or artificially or that has provided semen, ova or embryos for reproduction.</dd>\n</dl>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=90#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><div data-ajax-replace=\"/navi/fra/1672964761819\"></div>\n\n<p class=\"text-right\"> <a href=\"/DAM/DAM-animals-animaux/WORKAREA/DAM-animals-animaux/text-texte/terr_anima_trace_reg_description_carriers_1468936660272_fra.pdf\" title=\"Exigences pour les transporteurs d'animaux d'\u00e9levage en format de document portable\">PDF (264 ko)<br>(Deuxi\u00e8me version)</a></p>\n<div class=\"well well-sm\">\n<p class=\"mrgn-bttm-sm\">Le programme national d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage vise \u00e0 communiquer une information exacte et \u00e0 jour sur l'identit\u00e9 et les d\u00e9placements des animaux d'\u00e9levage et sur les installations o\u00f9 ils se trouvent pour att\u00e9nuer l'impact des foyers de maladie, des probl\u00e8mes li\u00e9s \u00e0 la salubrit\u00e9 alimentaire et des catastrophes naturelles.</p>\n\n</div>\n\n<p>La pr\u00e9sente brochure donne un aper\u00e7u des exigences f\u00e9d\u00e9rales en mati\u00e8re d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage. Pri\u00e8re de noter que des exigences provinciales et territoriales peuvent \u00e9galement s'appliquer.</p>\n\n<p>Les directives contenues dans cette brochure ne remplace pas la loi. Par cons\u00e9quent, il est important pour les parties r\u00e9glement\u00e9es qui utilisent ces directives de les appliquer en conformit\u00e9 et dans le cadre des sections applicables de la partie XV (identification des animaux) du R\u00e8glement sur la sant\u00e9 des animaux.</p>\n\n<h2>Aper\u00e7u des exigences g\u00e9n\u00e9rales</h2>\n\n<p><strong>Il est interdit de transporter des bovins, des bisons et des moutons qui ne portent pas une \u00e9tiquette approuv\u00e9e.</strong></p>\n\n<h3>\u00c9tiquettes perdues pendant le transport</h3>\n\n<p>Vous pouvez poursuivre le transport d'un animal dont l'\u00e9tiquette approuv\u00e9e a \u00e9t\u00e9 perdue pendant le d\u00e9placement.</p>\n\n<h3>Identification des carcasses</h3>\n\n<p><strong>Les carcasses de bovins, de bisons et de moutons</strong> doivent \u00eatre identifi\u00e9es au moyen d'une \u00e9tiquette approuv\u00e9e si elles sont transport\u00e9es hors de la ferme d'origine (ou de toute autre installation apr\u00e8s avoir quitt\u00e9 la ferme d'origine) aux fins d'\u00e9limination.</p>\n\n<p><strong>Les carcasses de porcsPig dead stock</strong> n'ont pas besoin d'\u00eatre identifi\u00e9es \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e ou d'un tatouage au marteau approuv\u00e9, mais doivent \u00eatre accompagn\u00e9es des renseignements n\u00e9cessaires sp\u00e9cifi\u00e9s dans cette brochure.</p>\n\n<h2>Aper\u00e7u des exigences particuli\u00e8res pour les bovins, les bisons et les moutons</h2>\n\n<p>Il n'est pas n\u00e9cessaire d'identifier les bovins, les bisons et les moutons au moyen d'une \u00e9tiquette approuv\u00e9e pendant leur \u00e9levage dans la ferme d'origine.</p>\n\n<p>Toutefois, on ne peut d\u00e9placer de la ferme d'origine (ou de toute autre installation apr\u00e8s qu'ils aient quitt\u00e9 la ferme d'origine) des bovins, des bisons ou des moutons, \u00e0 moins qu'ils soient identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e.</p>\n\n<p>La seule exception \u00e0 cette exigence concerne les bovins et les bisons qui sont exp\u00e9di\u00e9s \u00e0 une installation d'\u00e9tiquetage dont le nom est repris sur le site Web de l'Agence canadienne d'identification des bovins.</p>\n\n<h2>Aper\u00e7u des exigences particuli\u00e8res pour les porcs</h2>\n\n<h3>Transport des porcs</h3>\n\n<p>Veuillez noter que les exigences relatives aux porcs s'appliquent \u00e9galement aux sangliers d'\u00e9levage.</p>\n\n<h4>Que dois-je faire si je d\u00e9place des porcs d'une ferme vers une partie contigu\u00eb de cette ferme?</h4>\n\n<p>Les porcs n'ont pas besoin d'\u00eatre identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e ou d'un tatouage au marteau approuv\u00e9 et le formulaire d'accompagnement n'est pas n\u00e9cessaire.</p>\n\n<h4>Que dois-je faire si je d\u00e9place des porcs entre les parties d'une ferme non contigu\u00ebs ou entre des fermes diff\u00e9rentes?</h4>\n\n<p>Les porcs n'ont pas besoin d'\u00eatre identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e si</p>\n\n<ul>\n<li>un formulaire d'accompagnement peut imm\u00e9diatement \u00eatre lu par un inspecteur, et</li>\n<li>les exploitants des installations d'exp\u00e9dition et de r\u00e9ception d\u00e9clarent les d\u00e9placements des animaux au Conseil canadien du porc (l'administrateur responsable).</li>\n</ul>\n\n<p><strong>Exception \u00a0:</strong> Les porcs saillis doivent \u00eatre identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e avant qu'ils quittent l'installation d'exp\u00e9dition. Le num\u00e9ro indiqu\u00e9 sur l'\u00e9tiquette approuv\u00e9e doit \u00eatre exclusif \u00e0 chaque animal.</p>\n\n<h4>Que dois-je faire si je d\u00e9place des porcs entre des installations inscrites comme \u00e9tant li\u00e9es?</h4>\n\n<p>Les porcs n'ont pas besoin d'\u00eatre identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e sauf s'il s'agit de porcs saillis. Aucun renseignement concernant leurs d\u00e9placements n'est requis.</p>\n\n<h4>Que dois-je faire si je transporte des porcs vers des installations comme une salle d'encan, une foire agricole ou un centre d'ins\u00e9mination?</h4>\n\n<p>Les porcs doivent \u00eatre identifi\u00e9s au moyen d'une \u00e9tiquette approuv\u00e9e avant qu'ils quittent l'installation d'exp\u00e9dition; le num\u00e9ro indiqu\u00e9 sur l'\u00e9tiquette approuv\u00e9e doit \u00eatre exclusif \u00e0 chaque animal.</p>\n\n<h4>Que dois-je faire si je transporte des porcs vers des parcs de rassemblement utilis\u00e9s exclusivement pour entreposer les porcs avant de les envoyer directement \u00e0 l'abattoir?</h4>\n\n<p>Les porcs doivent \u00eatre identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e ou d'un tatouage approuv\u00e9 appliqu\u00e9 au marteau avant leur d\u00e9part de la ferme.</p>\n\n<p>L'\u00e9tiquette approuv\u00e9e peut \u00eatre une marque de troupeau (un num\u00e9ro d'identification exclusif \u00e0 l'installation d'exp\u00e9dition) ou un num\u00e9ro d'identification individuel qui est exclusif \u00e0 chaque animal.</p>\n\n<h4>Que dois-je faire si je transporte des porcs directement d'une ferme \u00e0 un abattoir?</h4>\n\n<p>Les porcs doivent \u00eatre identifi\u00e9s \u00e0 l'aide d'une \u00e9tiquette approuv\u00e9e (le num\u00e9ro indiqu\u00e9 sur l'\u00e9tiquette approuv\u00e9e doit \u00eatre exclusif \u00e0 chaque animal) ou d'une \u00e9tiquette approuv\u00e9e portant une marque de troupeau ou d'un tatouage approuv\u00e9 appliqu\u00e9 au marteau.</p>\n\n<p>Dans tous les cas, lorsque vous transportez des porcs qui doivent \u00eatre identifi\u00e9s, vous devez vous assurer qu'ils portent l'identification ad\u00e9quate avant de les charger dans le v\u00e9hicule de transport.</p>\n\n<h2>Aper\u00e7u des exigences relatives au formulaire d'accompagnement pour le transport des porcs</h2>\n\n<p>Dans les deux cas suivants, un formulaire de renseignements doit \u00eatre joint aux porcs et aux carcasses de porcs\u00a0:</p>\n\n<ul>\n<li>le transport de porcs non saillis entre les parties d'une ferme non contigu\u00ebs ou entre des fermes diff\u00e9rentes;</li>\n<li>le transport de carcasses de porcs dans une installation, quelle qu'elle soit.</li>\n</ul>\n\n<p>Le formulaire peut \u00eatre sur support \u00e9lectronique ou sur support papier \u00e0 la condition que\u00a0:</p>\n\n<ul>\n<li>tous les renseignements n\u00e9cessaires soient clairement indiqu\u00e9s, et</li>\n<li>qu'ils puissent \u00eatre lus facilement par un inspecteur.</li>\n</ul>\n\n<p>Le formulaire doit contenir l'information suivante\u00a0:</p>\n\n<ul>\n<li>le lieu o\u00f9 vous chargez les porcs ET le lieu o\u00f9 vous les d\u00e9chargez;</li>\n<li>le num\u00e9ro d'identification figurant sur chaque \u00e9tiquette approuv\u00e9e appos\u00e9e sur les porcs;</li>\n<li>la date et l'heure auxquelles vous quittez l'installation d'exp\u00e9dition;</li>\n<li>en cas d'animaux vivants, le nombre de porcs que vous transportez;</li>\n<li>le num\u00e9ro d'immatriculation du v\u00e9hicule que vous utilisez pour transporter les animaux.</li>\n</ul>\n\n\n<h2>D\u00e9finitions</h2>\n\n<dl class=\"dl-horizontal\">\n<dt>Marque de troupeau\u00a0:</dt>\n<dd>Num\u00e9ro d'identification exclusif \u00e0 un groupe de porcs provenant d\u2019une installation.</dd>\n<dt><a href=\"/sante-des-animaux/animaux-terrestres/tracabilite/identificateurs/fra/1331582406844/1331582476216\">\u00c9tiquette approuv\u00e9e</a>\u00a0:</dt>\n<dd>D\u00e9signe les \u00e9tiquettes approuv\u00e9es dans le cadre du Programme d'identification et de tra\u00e7abilit\u00e9 des animaux d'\u00e9levage et \u00e9num\u00e9r\u00e9es sur le site Web de l'ACIA.</dd>\n<dt>Sailli\u00a0:</dt>\n<dd>Animal ins\u00e9min\u00e9 naturellement ou artificiellement ou qui a fourni de la semence, un ovaire ou un embryon pour la reproduction.</dd>\n</dl>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}