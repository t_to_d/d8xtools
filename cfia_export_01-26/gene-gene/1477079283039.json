{
    "dcr_id": "1477079283039",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Sonchus arvensis</i> (Perennial sow thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Sonchus arvensis</i> (Laiteron des champs)"
    },
    "html_modified": "2024-01-26 2:23:27 PM",
    "modified": "2017-11-01",
    "issued": "2017-11-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_sonchus_arvensis_1477079283039_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_sonchus_arvensis_1477079283039_fra"
    },
    "ia_id": "1477079283382",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Sonchus arvensis</i> (Perennial sow thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Sonchus arvensis</i> (Laiteron des champs)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Sonchus arvensis</i> (Perennial sow thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Sonchus arvensis</i> (Laiteron des champs)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1477079283382",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/sonchus-arvensis/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/sonchus-arvensis/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Sonchus arvensis (Perennial sow thistle)",
            "fr": "Semence de mauvaises herbe : Sonchus arvensis (Laiteron des champs)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Sonchus arvensis, Asteraceae, Perennial sow thistle",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, esp\u00e8ces Sonchus arvensis, Asteraceae, Laiteron des champs"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Sonchus arvensis (Perennial sow thistle)",
        "fr": "Semence de mauvaises herbe : Sonchus arvensis (Laiteron des champs)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Common Name</h2>\n<p>Perennial sow thistle</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in all provinces and territories (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to Europe and temperate Asia, as far as the Russian Far East. Introduced in North America, Argentina, Chile, Australia, New Zealand, Japan, Indonesia and the Philippines (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). In the United States it occurs throughout except for a few southern states (Kartesz 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Achene length: 2.5 - 3.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width:1.0 - 1.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene thickness: 0.5 - 1.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Long oval-shaped achene; flattened</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Achene has a dull surface with longitudinal folds and small transverse ridges</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene is dark brown</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Pappus of fine white filaments 2-3 times as long as the achene may be present</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, pastures, meadows, lawns, gardens, fences, shores, ditches, thickets, forests, roadsides and disturbed areas  (Lemna and Messersmith 1990<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). A weed of any annual crop in temperate areas, notably cereals, corn, oilseed crops, sugarbeet, potatoes and vegetables (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Perennial sow thistle was probably introduced into North America as a seed contaminant (Lemna and Messersmith 1990<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n<p>A single plant may produce 3000-9750 seeds (Lemna and Messersmith 1990<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>), which can remain viable in the soil for several years (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Annual sow thistle (<i lang=\"la\">Sonchus oleraceus</i>)</h3>\n<ul>\n<li>Annual sow thistle achenes are similar in size, oval shape, being compressed and having longitudinal ridges as perennial sow thistle.</li>\n\n<li>Annual sow thistle achenes are narrower at the base, a lighter colour with variable transverse wrinkling as perennial sow thistle.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_04cnsh_1476894175393_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Perennial sow-thistle (<i lang=\"la\">Sonchus arvensis</i>) achenes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_01cnsh_1476894111949_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Perennial sow-thistle (<i lang=\"la\">Sonchus arvensis</i>) achene</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_03cnsh_1476894140617_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Perennial sow-thistle (<i lang=\"la\">Sonchus arvensis</i>) achene, close-up of surface</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_copyright_1476894207797_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Perennial sow-thistle (<i lang=\"la\">Sonchus arvensis</i>) achene</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_oleraceus_02cnsh_1476894291886_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Annual sow thistle (<i lang=\"la\">Sonchus oleraceus</i>) achenes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_oleraceus_01cnsh_1476894261273_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Annual sow thistle (<i lang=\"la\">Sonchus oleraceus</i>) achene</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, N.C., www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Lemna, W. K. and Messersmith, C. G. 1990</strong>. The biology of Canadian weeds. 94. <i lang=\"la\">Sonchus arvensis</i> L. Canadian Journal of Plant Science 70: 509-532.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Laiteron des champs</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> L'esp\u00e8ce est pr\u00e9sente dans l'ensemble des provinces et des territoires (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne d'Europe et de l'Asie temp\u00e9r\u00e9e, atteignant l'Extr\u00eame-Orient russe. Introduite en Am\u00e9rique du Nord, en Argentine, au Chili, en Australie, en Nouvelle-Z\u00e9lande, au Japon, en Indon\u00e9sie et aux Philippines (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>). Aux \u00c9tats-Unis, elle est pr\u00e9sente partout, sauf dans quelques \u00c9tats du Sud (Kartesz, 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'ak\u00e8ne\u00a0: 2,5 \u00e0 3,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de l'ak\u00e8ne\u00a0: 1,0 \u00e0 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>\u00c9paisseur de l'ak\u00e8ne\u00a0: 0,5 \u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne de forme elliptique allong\u00e9e; aplati</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>L'ak\u00e8ne a une surface mate et pr\u00e9sente des c\u00f4tes longitudinales et de petites cr\u00eates transversales</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> <ul class=\"mrgn-lft-lg\">\n<li>L'ak\u00e8ne est brun fonc\u00e9</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Le pappus, parfois pr\u00e9sent, est form\u00e9 de fins filaments blancs qui font de deux \u00e0 trois fois la longueur de l'ak\u00e8ne</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, p\u00e2turages, pr\u00e9s, pelouses, jardins, cl\u00f4tures, rivages, foss\u00e9s, fourr\u00e9s, for\u00eats, bords de chemin et terrains perturb\u00e9s (<span lang=\"en\">Lemna</span> et <span lang=\"en\">Messersmith</span>, 1990<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>, <span lang=\"en\">Darbyshire</span>, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup>). Mauvaise herbe des cultures annuelles des r\u00e9gions temp\u00e9r\u00e9es, notamment dans les c\u00e9r\u00e9ales, le ma\u00efs, les ol\u00e9agineux, la betterave \u00e0 sucre, la pomme de terre et les l\u00e9gumes (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn6a-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le laiteron des champs a probablement \u00e9t\u00e9 introduit en Am\u00e9rique du Nord par de la semence contamin\u00e9e (<span lang=\"en\">Lemna</span> et <span lang=\"en\">Messersmith</span>, 1990<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>).</p>\n<p>Une seule plante peut produire de 3 000 \u00e0 9 750 graines (<span lang=\"en\">Lemna</span> et <span lang=\"en\">Messersmith</span>, 1990<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup>) qui peuvent demeurer viables dans le sol plusieurs ann\u00e9es (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn6b-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Laiteron potager (<i lang=\"la\">Sonchus oleraceus</i>)</h3>\n<ul>\n<li>Les ak\u00e8nes du laiteron potager ressemblent \u00e0 ceux du laiteron des champs par leurs dimensions, leur forme elliptique, leur profil comprim\u00e9 et leurs c\u00f4tes longitudinales.</li>\n\n<li>Les ak\u00e8nes du laiteron potager ont une base plus \u00e9troite que ceux du laiteron des champs, ils sont de couleur plus claire et sont orn\u00e9s de plis transversaux variables.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_04cnsh_1476894175393_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Laiteron des champs (<i lang=\"la\">Sonchus arvensis</i>) ak\u00e8nes </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_01cnsh_1476894111949_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Laiteron des champs (<i lang=\"la\">Sonchus arvensis</i>) ak\u00e8ne</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_03cnsh_1476894140617_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Laiteron des champs (<i lang=\"la\">Sonchus arvensis</i>) ak\u00e8ne, vue agrandie de la surface</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_arvensis_copyright_1476894207797_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Laiteron des champs (<i lang=\"la\">Sonchus arvensis</i>) ak\u00e8ne</figcaption>\n</figure>\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_oleraceus_02cnsh_1476894291886_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Laiteron potager (<i lang=\"la\">Sonchus oleraceus</i>) ak\u00e8nes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_sonchus_oleraceus_01cnsh_1476894261273_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Laiteron potager (<i lang=\"la\">Sonchus oleraceus</i>) ak\u00e8ne</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, N.C., www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Lemna, W. K. and Messersmith, C. G. 1990</strong>. The biology of Canadian weeds. 94. <i lang=\"la\">Sonchus arvensis</i> L. Canadian Journal of Plant Science 70: 509-532.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, ON.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}