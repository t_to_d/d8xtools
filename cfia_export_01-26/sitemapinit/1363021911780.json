{
    "dcr_id": "1363021740306",
    "title": {
        "en": "Disease caused by <span lang=\"la\"><i>Perkinsus olseni</i></span>",
        "fr": "Maladie caus\u00e9e par <span lang=\"la\"><i>Perkinsus olseni</i></span>"
    },
    "html_modified": "2024-01-26 2:21:44 PM",
    "modified": "2013-11-27",
    "issued": "2013-03-11 13:09:01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_perkinsus_olseni_index_1363021740306_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_perkinsus_olseni_index_1363021740306_fra"
    },
    "ia_id": "1363021911780",
    "parent_ia_id": "1322941111904",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Diseases",
        "fr": "Maladies"
    },
    "commodity": {
        "en": "Aquatic Animal Health",
        "fr": "Sant\u00e9 des animaux aquatiques"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1322941111904",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Disease caused by <span lang=\"la\"><i>Perkinsus olseni</i></span>",
        "fr": "Maladie caus\u00e9e par <span lang=\"la\"><i>Perkinsus olseni</i></span>"
    },
    "label": {
        "en": "Disease caused by <span lang=\"la\"><i>Perkinsus olseni</i></span>",
        "fr": "Maladie caus\u00e9e par <span lang=\"la\"><i>Perkinsus olseni</i></span>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1363021911780",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1322940971192",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/perkinsus-olseni/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/perkinsus-olseni/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Disease caused by Perkinsus olseni",
            "fr": "Maladie caus\u00e9e par Perkinsus olseni"
        },
        "description": {
            "en": "Perkinsus olseni is a protozoan that infects molluscs. Perkinsus olseni belongs to the phylum Apicomplexa.",
            "fr": "Perkinsus olseni est un protozoaire qui infecte les mollusques. Il appartient au phylum Apicomplexa."
        },
        "keywords": {
            "en": "Perkinsus olseni, reportable disease, aquatics, protozoan, clam, oysters, cockle",
            "fr": "Perkinsus olseni, maladie d\u00e9clarable, aquatique,  protozoaire, palourdes, hu\u00eetres, coques"
        },
        "dcterms.subject": {
            "en": "crustaceans,inspection,infectious diseases,molluscs",
            "fr": "crustac\u00e9,inspection,maladie infectieuse,mollusque"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-03-11 13:09:01",
            "fr": "2013-03-11 13:09:01"
        },
        "modified": {
            "en": "2013-11-27",
            "fr": "2013-11-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Disease caused by Perkinsus olseni",
        "fr": "Maladie caus\u00e9e par Perkinsus olseni"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p><span lang=\"la\"><i>Perkinsus olseni</i></span> is a protozoan that infects molluscs. <span lang=\"la\"><i>Perkinsus olseni</i></span> belongs to the phylum Apicomplexa.</p>\n<p>In Canada, <span lang=\"la\"><i>Perkinsus olseni</i></span> is a <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/eng/1322940971192/1322941111904\">federally reportable disease</a>. This means that anyone who owns or works with aquatic animals, who knows of or suspects <span lang=\"la\"><i>Perkinsus olseni</i></span> disease in the animals that they own or work with, is required by law to notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/perkinsus-olseni/fact-sheet/eng/1363022405323/1363022544913\">Fact Sheet \u2013 <span lang=\"la\"><i>Perkinsus olseni</i></span></a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=1&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p><span lang=\"la\"><i>Perkinsus olseni</i></span> est un protozoaire qui infecte les mollusques. Il appartient au phylum Apicomplexa.</p>\n<p>Au Canada, <span lang=\"la\"><i>Perkinsus olseni</i></span> est une <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/fra/1322940971192/1322941111904\">maladie \u00e0 d\u00e9claration obligatoire</a>. Par cons\u00e9quent, quiconque poss\u00e8de des animaux aquatiques, ou s'en occupe, et soup\u00e7onne, ou d\u00e9c\u00e8le, la pr\u00e9sence de <span lang=\"la\"><i>Perkinsus olseni</i></span> chez les animaux qu'il poss\u00e8de ou dont il s'occupe est tenu par la loi d'en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/perkinsus-olseni/fiche-de-renseignements/fra/1363022405323/1363022544913\">Fiche de renseignements \u2013 <span lang=\"la\"><i>Perkinsus olseni</i></span></a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}