{
    "dcr_id": "1476289254234",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Erucastrum gallicum</i> (Dog mustard)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Erucastrum gallicum</i> (Moutarde des cheins)"
    },
    "html_modified": "2024-01-26 2:23:26 PM",
    "modified": "2017-11-06",
    "issued": "2017-11-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_erucastrum_gallicum_1476289254234_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_erucastrum_gallicum_1476289254234_fra"
    },
    "ia_id": "1476289254702",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Erucastrum gallicum</i> (Dog mustard)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Erucastrum gallicum</i> (Moutarde des cheins)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Erucastrum gallicum</i> (Dog mustard)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Erucastrum gallicum</i> (Moutarde des cheins)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476289254702",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/erucastrum-gallicum/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/erucastrum-gallicum/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Erucastrum gallicum (Dog mustard)",
            "fr": "Semence de mauvaises herbe : Erucastrum gallicum (Moutarde des cheins)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Erucastrum gallicum, Brassicaceae, Dog mustard",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Erucastrum gallicum, Brassicaceae, Moutarde des cheins"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-06",
            "fr": "2017-11-06"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Erucastrum gallicum (Dog mustard)",
        "fr": "Semence de mauvaises herbe : Erucastrum gallicum (Moutarde des cheins)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Brassicaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Dog mustard</p>\n\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 3 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs across Canada except in NU and <abbr title=\"Yukon\">YT</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to southern and central Europe (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Widely introduced and naturalized in other parts of Europe, as well as North America, the Bahamas, and Korea (Warwick and Wall 1998<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, <abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). In the United States it is abundant in the midwest and sporadic but widespread elsewhere (Warwick and Wall 1998<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 1.0 - 1.5 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 0.8 - 1.0 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed oval to oblong with a groove between the cotyledons and the seed radicle</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed reticulate, with stipples inside of the cells</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed reddish-brown</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Reticulations are the same colour as seed surface</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, old fields, gardens and lawns, orchards, railway   yards, railway lines, ballast, waste ground, roadsides and disturbed   areas (<abbr title=\"Flora of North America\">FNA</abbr> 1993+<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, Warwick and Wall 1998<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). Also found in open rangelands, dry grassland areas, shores, beaches, river banks and floodplains (Warwick and Wall 1998<sup id=\"fn3d-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). Most often found in sparsely vegetated habitats. A weed of cereal and oilseed crops (Warwick and Wall 1998<sup id=\"fn3e-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Dog mustard was introduced to North America in the early 1900s and   spread across the continent via rail lines and contaminated grain   shipments  (Warwick and Wall 1998<sup id=\"fn3f-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). Dog mustard produces an average of approximately 11,000 seeds per plant (Warwick and Wall 1998<sup id=\"fn3g-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Wall-rocket (<i lang=\"la\">Diplotaxis tenuifolia</i>)</h3>\n<ul>\n<li>Wall-rocket seed is a similar size, oval to oblong shape, distinctive radicle and surface reticulation as dog mustard. Some wall-rocket seeds are reddish-brown like dog mustard.</li>\n\n<li>Wall-rocket is usually greenish-brown with dark cotyledons and the surface reticulations are silvery, while dog mustard has reddish-brown reticulations.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_04cnsh_1475601287056_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dog mustard (<i lang=\"la\">Erucastrum gallicum</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_01cnsh_1475601236414_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dog mustard (<i lang=\"la\">Erucastrum gallicum</i>) seed\n</figcaption>\n</figure>\n\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_02cnsh_1475601262160_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dog mustard (<i lang=\"la\">Erucastrum gallicum</i>) seed, side view\n</figcaption>\n</figure>\n\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_copyright_1475601347031_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dog mustard (<i lang=\"la\">Erucastrum gallicum</i>) seed\n</figcaption>\n</figure>\n\n\n<h3>Similar species</h3>\n\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_diplotaxis_tenuifolia_02cnsh_1475597335494_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Wall-rocket (<i lang=\"la\">Diplotaxis tenuifolia</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_diplotaxis_tenuifolia_01cnsh_1475597313999_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Wall-rocket (<i lang=\"la\">Diplotaxis tenuifolia</i>) seed\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Warwick, S. I. and Wall, D. A. 1998</strong>. The biology of Canadian Weeds. 108. <i lang=\"la\">Erucastrum gallicum</i> (Willd.) O.E. Schulz. Canadian Journal of Plant Science 78: 155\u2013165.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"Flora of North America\">FNA</abbr>. 1993+</strong>. Flora of North America North of Mexico. 19+ vols. Flora of North America Editorial Committee, eds. New York and Oxford, http://www.fna.org/FNA/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Brassicaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Moutarde des cheins</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie 3 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> L'esp\u00e8ce est pr\u00e9sente dans l'ensemble du Canada, sauf au <abbr title=\"Nunavut\">Nt</abbr> et au <abbr title=\"Yukon\">Yn</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Europe m\u00e9ridionale et centrale (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Largement introduite et naturalis\u00e9e dans d'autres parties d'Europe, ainsi qu'en Am\u00e9rique du Nord, aux Bahamas et en Cor\u00e9e (Warwick et Wall, 1998<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; <abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Aux \u00c9tats-Unis, elle est abondante dans le <span lang=\"en\">Midwest</span>, alors qu'elle est sporadique, mais g\u00e9n\u00e9ralis\u00e9e ailleurs (Warwick et Wall, 1998<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 1,0 \u00e0 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 0,8 \u00e0 1,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine elliptique \u00e0 oblongue, pr\u00e9sentant un sillon entre les cotyl\u00e9dons et la radicule de la graine</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine r\u00e9ticul\u00e9e, pr\u00e9sentant un aspect pointill\u00e9 \u00e0 l'int\u00e9rieur des cellules</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine brun rouge\u00e2tre</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Les r\u00e9ticulations sont de la m\u00eame couleur que la surface de la graine</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, champs abandonn\u00e9s, jardins et pelouses, vergers, gares   de triage, voies ferr\u00e9es, ballast, terrains vagues, bords de chemin et   terrains perturb\u00e9s (<abbr lang=\"en\" title=\"Flora of North America\">FNA</abbr>, 1993+<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>; Warwick et Wall, 1998<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; Darbyshire, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>). Aussi pr\u00e9sente dans les parcours d\u00e9gag\u00e9s, les prairies herbeuses s\u00e8ches, les rivages, les plages, les berges fluvialeset les plaines inondables (Warwick et Wall, 1998<sup id=\"fn3d-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>). Plus souvent pr\u00e9sente dans les milieux \u00e0 v\u00e9g\u00e9tation clairsem\u00e9e. Mauvaise herbe des c\u00e9r\u00e9ales et des ol\u00e9agineux (Warwick et Wall, 1998<sup id=\"fn3e-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La moutarde des chiens a \u00e9t\u00e9 introduite en Am\u00e9rique du Nord au d\u00e9but des   ann\u00e9es 1900 et a \u00e9t\u00e9 propag\u00e9e \u00e0 travers le continent par le transport   ferroviaire de grains contamin\u00e9s  (Warwick et Wall, 1998<sup id=\"fn3f-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>). Une seule plante de moutarde des chiens produit en moyenne environ 11\u00a0000\u00a0graines (Warwick et Wall, 1998<sup id=\"fn3g-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Diplotaxis \u00e0 feuilles t\u00e9nues (<i lang=\"la\">Diplotaxis tenuifolia</i>)</h3>\n<ul>\n<li>La graine du diplotaxe \u00e0 feuilles t\u00e9nues ressemble \u00e0 celle de la moutarde des chiens par ses dimensions, sa forme elliptique \u00e0 oblongue, son lobe radiculaire distinctif et sa surface r\u00e9ticul\u00e9e. Certaines graines du diplotaxe \u00e0 feuilles t\u00e9nues sont brun rouge\u00e2tre comme celles de la moutarde des chiens.</li>\n\n<li>Les graines du diplotaxe \u00e0 feuilles t\u00e9nues sont g\u00e9n\u00e9ralement brun verd\u00e2tre avec des cotyl\u00e9dons fonc\u00e9s et ont une surface orn\u00e9e de r\u00e9ticulations argent\u00e9es, alors que les r\u00e9ticulations de la moutarde des chiens sont brun rouge\u00e2tre.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_04cnsh_1475601287056_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Moutarde des cheins (<i lang=\"la\">Erucastrum gallicum</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_01cnsh_1475601236414_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Moutarde des cheins (<i lang=\"la\">Erucastrum gallicum</i>) graine\n</figcaption>\n</figure>\n\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_02cnsh_1475601262160_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Moutarde des cheins (<i lang=\"la\">Erucastrum gallicum</i>) graine, la vue de c\u00f4t\u00e9\n</figcaption>\n</figure>\n\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_erucastrum_gallicum_copyright_1475601347031_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Moutarde des cheins (<i lang=\"la\">Erucastrum gallicum</i>) graine\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_diplotaxis_tenuifolia_02cnsh_1475597335494_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Diplotaxe \u00e0 feuilles t\u00e9nues (<i lang=\"la\">Diplotaxis tenuifolia</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_diplotaxis_tenuifolia_01cnsh_1475597313999_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Diplotaxe \u00e0 feuilles t\u00e9nues (<i lang=\"la\">Diplotaxis tenuifolia</i>) graines\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Warwick, S. I. and Wall, D. A. 1998</strong>. The biology of Canadian Weeds. 108. <i lang=\"la\">Erucastrum gallicum</i> (Willd.) O.E. Schulz. Canadian Journal of Plant Science 78: 155\u2013165.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"Flora of North America\">FNA</abbr>. 1993+</strong>. Flora of North America North of Mexico. 19+ vols. Flora of North America Editorial Committee, eds. New York and Oxford, http://www.fna.org/FNA/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}