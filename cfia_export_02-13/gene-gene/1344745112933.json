{
    "dcr_id": "1344745112933",
    "title": {
        "en": "Biosecurity for Dairy Herds",
        "fr": "Bios\u00e9curit\u00e9 pour les troupeaux laitiers"
    },
    "html_modified": "2024-02-13 9:47:40 AM",
    "modified": "2012-08-12",
    "issued": "2012-08-12 00:18:36",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_dairy_1344745112933_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_dairy_1344745112933_fra"
    },
    "ia_id": "1344745311222",
    "parent_ia_id": "1344707981478",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Biosecurity",
        "fr": "Bios\u00e9curit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1344707981478",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Biosecurity for Dairy Herds",
        "fr": "Bios\u00e9curit\u00e9 pour les troupeaux laitiers"
    },
    "label": {
        "en": "Biosecurity for Dairy Herds",
        "fr": "Bios\u00e9curit\u00e9 pour les troupeaux laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1344745311222",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1344707905203",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/biosecurity/standards-and-principles/dairy-herds/",
        "fr": "/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/troupeaux-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Biosecurity for Dairy Herds",
            "fr": "Bios\u00e9curit\u00e9 pour les troupeaux laitiers"
        },
        "description": {
            "en": "Biosecurity is becoming increasingly important to the Canadian dairy sector, which continues to evolve toward fewer, larger farms with high-producing animals.",
            "fr": "La bios\u00e9curit\u00e9 joue un r\u00f4le de plus en plus important dans le secteur de la production laiti\u00e8re au Canada, qui continue d'\u00e9voluer vers des exploitations moins nombreuses et de plus grandes envergures dot\u00e9es d'animaux tr\u00e8s productifs."
        },
        "keywords": {
            "en": "biosecurity, animal health, diseases, isolation, sanitation, traffic control, herd health, dairy herds",
            "fr": "bios\u00e9curit\u00e9, sant\u00e9 des animaux, maladies, isolement, mesures sanitaires, sant\u00e9 du troupeau, troupeaux laitiers"
        },
        "dcterms.subject": {
            "en": "livestock,imports,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-08-12 00:18:36",
            "fr": "2012-08-12 00:18:36"
        },
        "modified": {
            "en": "2012-08-12",
            "fr": "2012-08-12"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Biosecurity for Dairy Herds",
        "fr": "Bios\u00e9curit\u00e9 pour les troupeaux laitiers"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Biosecurity is becoming increasingly important to the Canadian dairy sector, which continues to evolve toward fewer, larger farms with high-producing animals. This sector provides Canadians with wholesome milk and dairy products, and is also the source of valuable genetics for the international marketing of semen and embryos.</p>\n<h2>Bovine diseases</h2>\n<p>The global emergence and re-emergence of bovine diseases in recent years has had a major impact on the cattle industry, both within Canada and abroad. Outbreaks of contagious diseases such as Foot and Mouth Disease in cattle in other countries have resulted in significant economic losses for cattle industries, as well as animal health and environmental concerns. These kinds of incidents emphasize the need for a comprehensive, coordinated approach to bovine biosecurity.</p>\n<p>Led by the Canadian Food Inspection Agency (CFIA), federal, provincial and territorial governments are continuously collaborating with industry and the public to implement and augment bovine biosecurity programs aimed at reducing disease transmission and protecting the interests of Canadians.</p>\n<h2>Sources of bovine diseases</h2>\n<p>Disease in dairy cattle and other bovine animals can be spread in a number of ways, including:</p>\n<ul>\n<li>through diseased cattle or healthy cattle incubating disease;</li>\n<li>through animals other than cattle (farm animals, pets, wild birds and other wildlife, vermin and insects);</li>\n<li>on the clothing and shoes of visitors and employees moving from farm-to-farm;</li>\n<li>in contaminated feed, water, bedding and soil;</li>\n<li>from the carcasses of dead animals;</li>\n<li>on contaminated farm equipment and vehicles; or</li>\n<li>in airborne particles and dust blown by the wind.</li>\n</ul>\n<h2>Biosecurity principles for dairy herds</h2>\n<p>Some of the basic biosecurity principles for the dairy sector include:</p>\n<ul>\n<li class=\"mrgn-bttm-md\"><strong>Isolation:</strong> \n<ul>\n<li>Only obtain new animals from reputable sources.</li>\n<li>Isolate sick animals from the rest of the herd.</li>\n<li>Limit the frequency of introducing new animals to the herd.</li>\n<li>Isolate any new animals or animals returning to the herd.</li>\n<li>Separate cattle by age or production groups.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Sanitation:</strong> \n<ul>\n<li>Routinely clean and disinfect buildings, barns, equipment, clothing and footwear.</li>\n<li>Designate a cleaning area for vehicles and equipment.</li>\n<li>Promptly dispose of dead animals.</li>\n<li>Implement a manure management program.</li>\n<li>Avoid borrowing equipment and vehicles from other farms.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Traffic control:</strong> \n<ul>\n<li>Control visitors' access to the herd.</li>\n<li>Prevent birds, rodents, pets and other animals from coming into contact with the herd.</li>\n<li>Require all visitors to wear clean boots, clothing and gloves.</li>\n<li>Maintain records of the movement of people, animals and equipment on and off the premises.</li>\n<li>Make sure all suppliers and other farm visitors follow your biosecurity measures.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Herd health management:</strong> \n<ul>\n<li>Monitor herd health daily.</li>\n<li>Identify all animals with proper ear tags for traceability.</li>\n<li>Employ veterinary services to help implement herd health programs.</li>\n<li>Vaccinate cattle against certain diseases.</li>\n<li>Immediately report any signs of illness to your veterinarian or the nearest CFIA office.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Program maintenance:</strong> \n<ul>\n<li>Train all staff in the application of your biosecurity program.</li>\n<li>Regularly monitor the effectiveness of the program.</li>\n<li>Be aware of any diseases in your area and adjust your biosecurity program to meet specific needs, as required.</li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>La bios\u00e9curit\u00e9 joue un r\u00f4le de plus en plus important dans le secteur de la production laiti\u00e8re au Canada, qui continue d'\u00e9voluer vers des exploitations moins nombreuses et de plus grandes envergures dot\u00e9es d'animaux tr\u00e8s productifs. Ce secteur fournit aux Canadiens du lait et des produits laitiers sains en plus de constituer une source pr\u00e9cieuse d'animaux reproducteurs pour le march\u00e9 international des semences et des embryons.</p>\n<h2>Maladies bovines</h2>\n<p>Ces derni\u00e8res ann\u00e9es, l'\u00e9mergence et la r\u00e9\u00e9mergence de maladies bovines ont eu des r\u00e9percussions importantes sur l'industrie bovine, et ce, tant au Canada qu'\u00e0 l'\u00e9tranger. Des \u00e9closions de maladies contagieuses comme la fi\u00e8vre aphteuse dans d'autres pays ont entra\u00een\u00e9 des pertes \u00e9conomiques importantes pour l'industrie bovine et soulev\u00e9 des pr\u00e9occupations en mati\u00e8re de sant\u00e9 animale et d'environnement. Ce type d'incidents fait ressortir la n\u00e9cessit\u00e9 d'une approche globale et coordonn\u00e9e en mati\u00e8re de bios\u00e9curit\u00e9 bovine.</p>\n<p>Guid\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA), les gouvernements f\u00e9d\u00e9ral, provinciaux et territoriaux collaborent continuellement avec l'industrie et le grand public pour mettre en oeuvre et accro\u00eetre les programmes de bios\u00e9curit\u00e9 bovine afin de r\u00e9duire la transmission des maladies et de prot\u00e9ger les int\u00e9r\u00eats des Canadiens.</p>\n<h2>Sources de maladies bovines</h2>\n<p>Les maladies chez les vaches laiti\u00e8res peuvent se propager de nombreuses fa\u00e7ons, dont les suivantes\u00a0:</p>\n<ul>\n<li>par des bovins malades ou des porteurs sains;</li>\n<li>par des animaux autres que les bovins (animaux d'\u00e9levage, animaux de compagnie, oiseaux sauvages ou autres animaux sauvages, vermine et insectes);</li>\n<li>par les v\u00eatements et les chaussures de visiteurs et d'employ\u00e9s qui se d\u00e9placent d'une exploitation \u00e0 l'autre;</li>\n<li>par des aliments, de l'eau, de la liti\u00e8re ou de la terre contamin\u00e9s;</li>\n<li>par des carcasses d'animaux morts;</li>\n<li>par de l'\u00e9quipement ou des v\u00e9hicules contamin\u00e9s;</li>\n<li>par des particules et des poussi\u00e8res contamin\u00e9es en suspension dans l'air ou port\u00e9es par le vent.</li>\n</ul>\n<h2>Principes de bios\u00e9curit\u00e9 pour les troupeaux laitiers</h2>\n<p>Les principes de base de la bios\u00e9curit\u00e9 pour le secteur laitier comprennent les \u00e9l\u00e9ments suivants :</p>\n<ul>\n<li><strong>Isolement</strong> \n<ul>\n<li>Ne recevoir que des animaux de sources fiables.</li>\n<li>Isoler les animaux malades du reste du troupeau.</li>\n<li>Limiter la fr\u00e9quence d'introduction de nouveaux animaux dans le troupeau.</li>\n<li>Isoler les nouveaux animaux ou les animaux r\u00e9introduits dans le troupeau.</li>\n<li>S\u00e9parer les animaux selon leur \u00e2ge ou leur groupe de production.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Mesures sanitaires</strong> \n<ul>\n<li>Nettoyer et d\u00e9sinfecter r\u00e9guli\u00e8rement les b\u00e2timents, les \u00e9tables, l'\u00e9quipement, les v\u00eatements et les chaussures.</li>\n<li>R\u00e9server une zone pour le nettoyage des v\u00e9hicules et de l'\u00e9quipement.</li>\n<li>\u00c9liminer rapidement les animaux morts.</li>\n<li>Mettre en <span class=\"lig\">oe</span>uvre un programme de gestion du fumier.</li>\n<li>\u00c9viter d'emprunter l'\u00e9quipement et les v\u00e9hicules d'autres exploitations.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Contr\u00f4le de la circulation</strong> \n<ul>\n<li>Limiter l'acc\u00e8s des visiteurs au troupeau.</li>\n<li>Pr\u00e9venir les contacts entre les animaux du troupeau et les oiseaux, les rongeurs, les animaux de compagnie ou d'autres animaux.</li>\n<li>Exiger que tous les visiteurs portent des gants, des bottes et des v\u00eatements propres.</li>\n<li>Tenir des registres sur les d\u00e9placements des personnes, des animaux et de l'\u00e9quipement qui arrivent sur les lieux ou les quittent.</li>\n<li>Veiller \u00e0 ce que tous les fournisseurs et autres visiteurs se conforment aux mesures de bios\u00e9curit\u00e9.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Gestion de la sant\u00e9 du troupeau</strong> \n<ul>\n<li>Surveiller quotidiennement la sant\u00e9 du troupeau.</li>\n<li>Identifier tous les animaux \u00e0 l'aide d'\u00e9tiquettes d'oreille destin\u00e9es \u00e0 la tra\u00e7abilit\u00e9.</li>\n<li>Avoir recours aux services d'un v\u00e9t\u00e9rinaire pour la mise en <span class=\"lig\">oe</span>uvre des programmes sanitaires.</li>\n<li>Vacciner les bovins contre certaines maladies.</li>\n<li>Signaler imm\u00e9diatement tout signe de maladie \u00e0 votre v\u00e9t\u00e9rinaire ou au bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> le plus pr\u00e8s.</li>\n</ul>\n</li>\n<li class=\"mrgn-bttm-md\"><strong>Mise \u00e0 jour du programme</strong> \n<ul>\n<li>Former tout le personnel relativement \u00e0 l'application du programme de bios\u00e9curit\u00e9.</li>\n<li>V\u00e9rifier r\u00e9guli\u00e8rement l'efficacit\u00e9 du programme.</li>\n<li>Se tenir au courant des maladies qui touchent la r\u00e9gion et adapter le programme de bios\u00e9curit\u00e9 selon les besoins.</li>\n</ul>\n</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}