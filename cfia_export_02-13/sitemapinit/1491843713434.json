{
    "dcr_id": "1491843712800",
    "title": {
        "en": "Executive Summary",
        "fr": "Sommaire"
    },
    "html_modified": "2024-02-13 9:52:07 AM",
    "modified": "2017-04-11",
    "issued": "2017-04-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/chem_testing_report_2015-2016_glyphosate_1491843712800_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/chem_testing_report_2015-2016_glyphosate_1491843712800_fra"
    },
    "ia_id": "1491843713434",
    "parent_ia_id": "1453327843364",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1453327843364",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Executive Summary",
        "fr": "Sommaire"
    },
    "label": {
        "en": "Executive Summary",
        "fr": "Sommaire"
    },
    "templatetype": "content page 1 column",
    "node_id": "1491843713434",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1453324778043",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-industry/food-chemistry-and-microbiology/food-safety-testing-reports-and-journal-articles/executive-summary/",
        "fr": "/salubrite-alimentaire-pour-l-industrie/chimie-et-microbiologie-alimentaires/rapports-d-analyse-et-articles-de-revues-sur-la-sa/sommaire/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Executive Summary",
            "fr": "Sommaire"
        },
        "description": {
            "en": "Chemical hazards may occur in foods either from their deliberate use in food production or by accidental contamination from the environment, during processing, or due to the presence of natural toxins.",
            "fr": "Un danger chimique li\u00e9 aux aliments peut survenir lorsqu'une toxine naturelle est pr\u00e9sente ou en raison d'une utilisation intentionnelle dans la production alimentaire, d'une contamination environnementale accidentelle ou d'une contamination issue de la transformation des aliments."
        },
        "keywords": {
            "en": "food, food safety, food safety requirements, sampling, testing, food safety risks, Chemical Residue Reports, Glyphosate Testing, Executive Summary",
            "fr": "aliments, salubrit\u00e9 des aliments, exigences f\u00e9d\u00e9rales en mati\u00e8re de salubrit\u00e9 des aliments, \u00e9chantillonnage, analyse, risques pour la salubrit\u00e9 des aliments, Glyphosate Testing, Rapports sur les r\u00e9sidus de produits chimiques, sommaire"
        },
        "dcterms.subject": {
            "en": "agri-food products,food,food inspection,food safety,regulation,safety,testing",
            "fr": "produit agro-alimentaire,aliment,inspection des aliments,salubrit\u00e9 des aliments,r\u00e9glementation,s\u00e9curit\u00e9,test"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-04-13",
            "fr": "2017-04-13"
        },
        "modified": {
            "en": "2017-04-11",
            "fr": "2017-04-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Executive Summary",
        "fr": "Sommaire"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Glyphosate Testing in 2015-2016</h2>\n\n<p>Chemical hazards may occur in foods either from their deliberate use in food production or by accidental contamination from the environment, during processing, or due to the presence of natural toxins. The Canadian Food Inspection Agency's (CFIA) tests foods for pesticides to detect food safety risks and ensure that the food supply meets Canadian standards.</p>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> tested 3188 samples from a variety of imported and domestic food products for residues of the herbicide, Glyphosate. This was done to see what levels of residues were in these products and to verify that the levels of residues found meet Canadian guidelines. The results obtained from the testing were compared to the limits set by Health Canada.</p>\n\n<p>The results of the survey showed that 70.3% of samples tested did not contain detectable residues of Glyphosate. The overall sample compliance rate to Canadian standards for Glyphosate was found to be 98.7%. None of the samples of fruits and vegetables or children's food products were found to contain residue levels which exceeded limits. Non-compliance data were evaluated by Health Canada and no human health concerns were identified. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will continue to monitor for the presence of this commonly used herbicide to ensure the safety of the Canadian food supply.</p>\n\n<p><a href=\"/food-safety-for-industry/food-chemistry-and-microbiology/food-safety-testing-reports-and-journal-articles/executive-summary/glyphosate-testing/eng/1491846907641/1491846907985\">Complete Report: Safeguarding with Science: Glyphosate Testing in 2015-2016</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>D\u00e9pistage du glyphosate en 2015-2016</h2>\n\n<p>Un danger chimique li\u00e9 aux aliments peut survenir lorsqu'une toxine naturelle est pr\u00e9sente ou en raison d'une utilisation intentionnelle dans la production alimentaire, d'une contamination environnementale accidentelle ou d'une contamination issue de la transformation des aliments. L'Agence canadienne d'inspection des aliments (ACIA) effectue des \u00e9preuves de d\u00e9pistage des pesticides dans les aliments pour d\u00e9tecter les risques li\u00e9s \u00e0 la salubrit\u00e9 des aliments et s'assurer que l'approvisionnement alimentaire r\u00e9pond aux normes canadiennes.</p>\n\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a effectu\u00e9 des \u00e9preuves de d\u00e9pistage de r\u00e9sidus de l'herbicide glyphosate sur 3 188 \u00e9chantillons provenant de divers produits alimentaires canadiens et import\u00e9s. Ces \u00e9preuves ont \u00e9t\u00e9 r\u00e9alis\u00e9es en vue de d\u00e9terminer les concentrations de r\u00e9sidus dans ces produits et v\u00e9rifier que les concentrations relev\u00e9es sont conformes aux lignes directrices du Canada. Les r\u00e9sultats des \u00e9preuves ont \u00e9t\u00e9 compar\u00e9s aux limites fix\u00e9es par Sant\u00e9 Canada.</p>\n\n<p>Les r\u00e9sultats des \u00e9preuves d\u00e9montrent que 70,3\u00a0% des \u00e9chantillons analys\u00e9s ne contenaient pas de r\u00e9sidus d\u00e9tectables de glyphosate. Le taux global de conformit\u00e9 des \u00e9chantillons aux normes canadiennes sur le glyphosate est de 98,7\u00a0%. Aucun \u00e9chantillon de fruit, de l\u00e9gume ou de produit alimentaire pour enfants ne contenait des niveaux de r\u00e9sidus exc\u00e9dant les limites. Les donn\u00e9es sur la non-conformit\u00e9 ont \u00e9t\u00e9 \u00e9valu\u00e9es par Sant\u00e9 Canada, qui n'a d\u00e9cel\u00e9 aucun risque pour la sant\u00e9 humaine. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> continue de surveiller la pr\u00e9sence du glyphosate, un herbicide couramment utilis\u00e9, pour assurer la salubrit\u00e9 de l'approvisionnement alimentaire canadien.</p>\n\n<p><a href=\"/salubrite-alimentaire-pour-l-industrie/chimie-et-microbiologie-alimentaires/rapports-d-analyse-et-articles-de-revues-sur-la-sa/sommaire/depistage-du-glyphosate/fra/1491846907641/1491846907985\">Rapport complet\u00a0: Sauvegarder gr\u00e2ce \u00e0 la science\u00a0: D\u00e9pistage du glyphosate en 2015-2016</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}