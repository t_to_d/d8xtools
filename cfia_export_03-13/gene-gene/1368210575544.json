{
    "dcr_id": "1368210575544",
    "title": {
        "en": "Bahamas - Export requirements for meat and poultry products",
        "fr": "Bahamas - Exigences d'exportation pour viande et volaille"
    },
    "html_modified": "2024-03-12 3:56:15 PM",
    "modified": "2024-01-10",
    "issued": "2013-05-10 14:29:36",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter11_export_bahamas_1368210575544_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter11_export_bahamas_1368210575544_fra"
    },
    "ia_id": "1368210611276",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "|Agency personnel|Industry",
        "fr": "|Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Bahamas - Export requirements for meat and poultry products",
        "fr": "Bahamas - Exigences d'exportation pour viande et volaille"
    },
    "label": {
        "en": "Bahamas - Export requirements for meat and poultry products",
        "fr": "Bahamas - Exigences d'exportation pour viande et volaille"
    },
    "templatetype": "content page 1 column",
    "node_id": "1368210611276",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/bahamas-meat-and-poultry/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/bahamas-viande-et-volaille/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Bahamas - Export requirements for meat and poultry products",
            "fr": "Bahamas - Exigences d'exportation pour viande et volaille"
        },
        "description": {
            "en": "Countries to which exports are currently made - Bahamas",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Bahamas"
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, meat inspection, slaughter, processing, procedures, manual, establishments, facility, equipment, products, monitoring, controls, antemortem, postmortem, exports, Bahamas - Meat and poultry",
            "fr": "Loi sur l&#39;inspection des viandes, R&#232;glement de 1990 sur l&#39;inspection des viandes, inspection des viandes, produits, certificat, Bahamas - viande et volaille"
        },
        "dcterms.subject": {
            "en": "food labeling,food inspection,handbooks,food safety,food processing,meat,poultry",
            "fr": "\u00e9tiquetage des aliments,inspection des aliments,manuel,salubrit\u00e9 des aliments, transformation des aliments,viande,volaille"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exporation d'aliments et de la protect. des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-05-10 14:29:36",
            "fr": "2013-05-10 14:29:36"
        },
        "modified": {
            "en": "2024-01-10",
            "fr": "2024-01-10"
        },
        "type": {
            "en": "guide,reference material",
            "fr": "guide,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Bahamas - Export requirements for meat and poultry products",
        "fr": "Bahamas - Exigences d'exportation pour viande et volaille"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">1. Eligible/ineligible products</a></li>\n<li><a href=\"#a2\">2. Pre-export approvals by competent authority of importing country</a></li>\n<li><a href=\"#a3\">3. Product specifications, controls and inspection requirements</a></li>\n<li><a href=\"#a4\">4. Labelling, marking and packaging requirements</a></li>\n<li><a href=\"#a5\">5. Documentation requirements</a></li>\n<li><a href=\"#a6\">6. Other information</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Eligible/ineligible products</h2>\n\n<h3>Eligible products</h3>\n\n<ul>\n<li>beef, beef products, and beef offal</li>\n<li>pork</li>\n<li>poultry</li>\n</ul>\n\n<h3>Ineligible products</h3>\n\n<ul>\n<li>Meat and meat products other than beef, beef products, beef offal, pork and poultry.</li>\n</ul>\n\n<h2 id=\"a2\">2. Pre-export approvals by competent authority of importing country</h2>\n\n<p>There are no pre-export approvals by competent authority of importing country at this time.</p>\n\n<h2 id=\"a3\">3. Products specifications, production controls and inspection requirements</h2>\n\n<p>There are no product specifications, production controls and inspection requirements at this time.</p>\n\n<h2 id=\"a4\">4. Labelling, marking and packaging requirements</h2>\n\n<p>There are no country-specific labelling, marking and packaging requirements at this time.</p>\n\n<h2 id=\"a5\">5. Documentation requirements</h2>\n\n<p>Certificate of Inspection Covering Meat Products (CFIA/ACIA 1454) must be issued.</p>\n\n<p>In addition:</p>\n<ul>\n<li>Beef, beef products and beef offal:\n<ul>\n<li>Annex A must be issued in addition to CFIA/ACIA 1454</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a6\">6. Other information</h2>\n\n<p>There is no additional information at this time.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">1. Produits admissibles ou non admissibles</a></li>\n<li><a href=\"#a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</a></li>\n<li><a href=\"#a3\">3. Sp\u00e9cifications des produits, mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</a></li>\n<li><a href=\"#a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</a></li>\n<li><a href=\"#a5\">5. Exigences relatives \u00e0 la documentation</a></li>\n<li><a href=\"#a6\">6. Autres renseignements</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Produits admissibles ou non admissibles</h2>\n\n<h3>Produits admissibles</h3>\n\n<ul>\n<li>viande et produits de viande de boeuf et abats de b\u0153uf </li>\n<li>viande de porc</li>\n<li>viande de volaille</li>\n</ul>\n\n<h3>Produits non admissibles</h3>\n\n<ul>\n<li>Viande et produits de viande autres que la viande et les produits de boeuf, les abats de boeuf, la viande de porc et de volaille.</li>\n</ul>\n\n<h2 id=\"a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<p>Il n'y a pas d'approbation pr\u00e9alable \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur pour le moment.</p>\n\n<h2 id=\"a3\">3. Sp\u00e9cifications des produits, mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<p>Il n'y a pas de sp\u00e9cifications de produits, de contr\u00f4les de production et d'exigences d'inspection pour le moment.</p>\n\n<h2 id=\"a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</h2>\n\n<p>Il n'existe pas pour l'instant d'exigences en mati\u00e8re d'\u00e9tiquetage, de marquage et d'emballage requises pour le moment.</p>\n\n<h2 id=\"a5\">5. Exigences relatives \u00e0 la documentation</h2>\n\n<p>Le certificat d'inspection couvrant les produits \u00e0 base de viande (CFIA/ACIA 1454) doit \u00eatre d\u00e9livr\u00e9.</p>\n\n<p>En outre\u00a0:</p>\n\n<ul>\n<li>viande et produits de viande de b\u0153uf et abats de b\u0153uf\u00a0:\n<ul>\n<li>l'annexe A doit \u00eatre utilis\u00e9e avec l'ACIA 1454.</li>\n</ul>\n</li>\n</ul>\n\n<h2 id=\"a6\">6. Autres renseignements</h2>\n\n<p>Il n'y a pas d'informations suppl\u00e9mentaires pour le moment.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}