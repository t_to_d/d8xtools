{
    "dcr_id": "1521087460487",
    "title": {
        "en": "Audit of the National Chemical Residue Monitoring Program",
        "fr": "V\u00e9rification du Programme national de surveillance des r\u00e9sidus chimiques"
    },
    "html_modified": "23-3-2018",
    "modified": "23-3-2018",
    "issued": "15-3-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/audit_national_chem_program_overview_1521087460487_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/audit_national_chem_program_overview_1521087460487_fra"
    },
    "parent_ia_id": "1299843588592",
    "ia_id": "1521087530596",
    "parent_node_id": "1299843588592",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Audit of the National Chemical Residue Monitoring Program",
        "fr": "V\u00e9rification du Programme national de surveillance des r\u00e9sidus chimiques"
    },
    "label": {
        "en": "Audit of the National Chemical Residue Monitoring Program",
        "fr": "V\u00e9rification du Programme national de surveillance des r\u00e9sidus chimiques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1521087530596",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1299843498252",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/ncrmp-overview/",
        "fr": "/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/pnsrc-apercu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Audit of the National Chemical Residue Monitoring Program",
            "fr": "V\u00e9rification du Programme national de surveillance des r\u00e9sidus chimiques"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) is responsible for monitoring the food supply for chemical residues and contaminants and enforcing compliance with maximum residue limits (MRLs), tolerances and maximum levels established by Health Canada.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) est responsable de surveiller la pr\u00e9sence de r\u00e9sidus chimiques et de contaminants dans l'approvisionnement alimentaire, ainsi que d'assurer le respect des limites maximales de r\u00e9sidus (LMR), des limites de tol\u00e9rance et des concentrations maximales \u00e9tablies par Sant\u00e9 Canada."
        },
        "keywords": {
            "en": "audit, review, policy, planning, implementation, reporting, accountability, Internal Audit Report, maximum residue limits, MRLs, National Chemical Residue Monitoring Program, NCRMP",
            "fr": "V\u00e9rification, revues, politique, planification, Plan de mise en \u0153uvre, Rapports, Responsabilisation, Rapport de v\u00e9rification interne, limites maximales de r\u00e9sidus, LMR, Programme national de surveillance des r\u00e9sidus chimiques, PNSRC"
        },
        "dcterms.subject": {
            "en": "government information,legislation,policy,audit",
            "fr": "information gouvernementale,l\u00e9gislation,politique,v\u00e9rification"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-23",
            "fr": "2018-03-23"
        },
        "modified": {
            "en": "2018-03-23",
            "fr": "2018-03-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Audit of the National Chemical Residue Monitoring Program",
        "fr": "V\u00e9rification du Programme national de surveillance des r\u00e9sidus chimiques"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Internal Audit</h2>\n\n<p>Canadian Food Inspection Agency (CFIA) internal audit is an independent, objective assurance and consulting function designed to add value and improve the Agency's operations. It helps the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> accomplish its objectives by bringing a systematic, disciplined approach to evaluate and improve the effectiveness of risk management, control, and governance processes.</p> \n\n<p>Internal audit engagements are authorized as part of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s risk-based audit plan, which is updated at least annually for approval by the President. Internal audits are carried out in accordance with the Treasury Board Policy on Internal Audit and the Institute of Internal Auditors' International Professional Practices Framework. Final internal audit reports are approved by the President on the recommendation of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> audit committee.</p>\n\n<h2>Overview</h2>\n\n<p>The Canadian Food Inspection Agency (CFIA) is responsible for monitoring the food supply for chemical residues and contaminants and enforcing compliance with maximum residue limits (MRLs), tolerances and maximum levels established by Health Canada.</p> \n\n<p>The National Chemical Residue Monitoring Program (NCRMP) provides information about the residue levels and compliance status of the Canadian food supply. The <abbr title=\"National Chemical Residue Monitoring Program\">NCRMP</abbr> allows the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to identify trends over time, gauge the effectiveness of policies and programs, and develop strategic plans to minimize potential health risks to Canadians. It also enables the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to identify where follow-up and enforcement action is necessary. In addition, the <abbr title=\"National Chemical Residue Monitoring Program\">NCRMP</abbr> supports the export of Canadian products to trading partners such as the United States and the European Union by demonstrating the equivalence of Canada's residue control system.</p>\n\n<p>The objective of the audit was to provide reasonable assurance that there are adequate processes and controls in place for the management and delivery of the <abbr title=\"National Chemical Residue Monitoring Program\">NCRMP</abbr>.</p>\n \n<h2>Key Findings</h2>\n\n<p>The audit concluded that process and control improvements are required, most notably with respect to establishing clear accountability, roles and responsibilities, and program objectives; and strengthening the management of follow-up on identified violations to ensure that actions and decisions are taken and documented. The audit provides seven recommendations aimed at addressing these findings.</p> \n\n<h2>Complete Report</h2>\n\n<ul><li><a href=\"/about-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/ncrmp-overview/ncrmp-report/eng/1521081149940/1521081245039\">Audit of the National Chemical Residue Monitoring Program</a></li>\n<li><a href=\"/about-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/ncrmp-overview/ncrmp-report/eng/1521081149940/1521081245039#appb\">Management Response and Action Plan</a></li></ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>V\u00e9rification interne</h2>\n\n<p>La v\u00e9rification interne de l'Agence canadienne d'inspection des aliments (ACIA) est une fonction d'assurance et de consultation ind\u00e9pendante et objective visant \u00e0 ajouter de la valeur et \u00e0 am\u00e9liorer les activit\u00e9s de l'Agence. Elle permet \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d'atteindre ses objectifs en ayant recours \u00e0 une approche syst\u00e9matique et m\u00e9thodique pour \u00e9valuer et am\u00e9liorer l'efficacit\u00e9 des processus de gestion des risques, de contr\u00f4le et de gouvernance.</p> \n\n<p>Les projets de v\u00e9rification interne sont autoris\u00e9s dans le cadre du Plan de v\u00e9rification interne bas\u00e9 sur les risques de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, qui est mis \u00e0 jour au moins annuellement pour approbation par le pr\u00e9sident. Les v\u00e9rifications internes sont effectu\u00e9es conform\u00e9ment \u00e0 la Politique sur l'audit interne du Conseil tr\u00e9sor et au Cadre de r\u00e9f\u00e9rence international des pratiques professionnelles de l'Institut des auditeurs internes. Les rapports de v\u00e9rification interne finaux sont approuv\u00e9s par le pr\u00e9sident apr\u00e8s avoir \u00e9t\u00e9 recommand\u00e9 par le comit\u00e9 de v\u00e9rification.</p>\n\n<h2>Aper\u00e7u</h2>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est responsable de surveiller la pr\u00e9sence de r\u00e9sidus chimiques et de contaminants dans l'approvisionnement alimentaire, ainsi que d'assurer le respect des limites maximales de r\u00e9sidus (LMR), des limites de tol\u00e9rance et des concentrations maximales \u00e9tablies par Sant\u00e9 Canada.</p> \n\n<p>Le Programme national de surveillance des r\u00e9sidus chimiques (PNSRC) permet de fournir des renseignements sur les concentrations de r\u00e9sidus et l'\u00e9tat de conformit\u00e9 de l'approvisionnement alimentaire du Canada.  Le <abbr title=\"Programme national de surveillance des r\u00e9sidus chimiques\">PNSRC</abbr> permet \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de d\u00e9gager des tendances au fil du temps, d'\u00e9valuer l'efficacit\u00e9 des politiques et des programmes et d'\u00e9laborer des plans strat\u00e9giques pour limiter les risques potentiels pour la sant\u00e9 des Canadiens. Il lui permet \u00e9galement de d\u00e9terminer si des mesures de suivi et d'application de la loi s'imposent. En outre, le <abbr title=\"Programme national de surveillance des r\u00e9sidus chimiques\">PNSRC</abbr> favorise l'exportation de produits canadiens vers des partenaires commerciaux comme les \u00c9tats-Unis et l'Union europ\u00e9enne en prouvant l'\u00e9quivalence du syst\u00e8me canadien de contr\u00f4le des r\u00e9sidus.</p>\n\n<p>L'objectif de la v\u00e9rification consistait \u00e0 obtenir une assurance raisonnable que des mesures de contr\u00f4le et des processus ad\u00e9quats sont en place pour la gestion et la prestation du <abbr title=\"Programme national de surveillance des r\u00e9sidus chimiques\">PNSRC</abbr>.</p>\n \n<h2>Principales constatations</h2>\n\n<p>La v\u00e9rification a constat\u00e9 que des am\u00e9liorations des processus et des mesures de contr\u00f4le sont n\u00e9cessaires, surtout en ce qui concerne l'\u00e9tablissement d'une imputabilit\u00e9, de r\u00f4les et de responsabilit\u00e9s, d'objectifs de programme clairs, ainsi que le renforcement de la gestion du suivi des infractions, pour s'assurer que des interventions et des d\u00e9cisions soient prises et document\u00e9es. La v\u00e9rification pr\u00e9sente sept recommandations afin de r\u00e9pondre aux constatations.</p> \n\n<h2>Rapport complet</h2>\n\n<ul><li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/pnsrc-apercu/pnsrc-rapport/fra/1521081149940/1521081245039\">V\u00e9rification du Programme national de surveillance des r\u00e9sidus chimiques</a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/pnsrc-apercu/pnsrc-rapport/fra/1521081149940/1521081245039#appb\">R\u00e9ponse et plan d'action de la direction</a></li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}