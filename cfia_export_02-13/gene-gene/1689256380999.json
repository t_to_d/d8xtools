{
    "dcr_id": "1689256380999",
    "title": {
        "en": "What to expect when a premises is declared a high risk contact premises",
        "fr": "\u00c0 quoi s'attendre lorsqu'un lieu est d\u00e9clar\u00e9 lieu de contact \u00e0 haut risque"
    },
    "html_modified": "2024-02-13 9:56:15 AM",
    "modified": "2023-07-19",
    "issued": "2023-07-19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/high_risk_contact_premise_1689256380999_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/high_risk_contact_premise_1689256380999_fra"
    },
    "ia_id": "1689256624632",
    "parent_ia_id": "1688511747401",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1688511747401",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "What to expect when a premises is declared a high risk contact premises",
        "fr": "\u00c0 quoi s'attendre lorsqu'un lieu est d\u00e9clar\u00e9 lieu de contact \u00e0 haut risque"
    },
    "label": {
        "en": "What to expect when a premises is declared a high risk contact premises",
        "fr": "\u00c0 quoi s'attendre lorsqu'un lieu est d\u00e9clar\u00e9 lieu de contact \u00e0 haut risque"
    },
    "templatetype": "content page 1 column",
    "node_id": "1689256624632",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1688511747041",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/infected-and-high-risk-premises/control-of-premises/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/lieux-infectes-et-a-haut-risque/control-des-lieux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "What to expect when a premises is declared a high risk contact premises",
            "fr": "\u00c0 quoi s'attendre lorsqu'un lieu est d\u00e9clar\u00e9 lieu de contact \u00e0 haut risque"
        },
        "description": {
            "en": "When the Canadian Food Inspection Agency (CFIA) is aware of HPAI infections, we need to control the risk of infection to other premises.",
            "fr": "Lorsque l'Agence canadienne d'inspection des aliments (ACIA) sait qu'il y a infection par l'IAHP, elle doit contr\u00f4ler le risque d'infection vers d'autres lieux."
        },
        "keywords": {
            "en": "high risk contact premises, HRCP, highly pathogenic avian influenza, HPAI, Health Act of Animals, Health Regulations of animals, Reportable Diseases Regulations, diseases",
            "fr": "lieux de contact \u00e0 haut risque, HRCP, influenza aviaire hautement pathog\u00e8ne, IAHP, loi sur la sant\u00e9 des animaux, r\u00e8glement sanitaire des animaux, r\u00e8glement sur les maladies \u00e0 d\u00e9claration obligatoire, maladies"
        },
        "dcterms.subject": {
            "en": "livestock,animal diseases,infectious diseases,animal health",
            "fr": "b\u00e9tail,maladie animale,maladie infectieuse,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-07-19",
            "fr": "2023-07-19"
        },
        "modified": {
            "en": "2023-07-19",
            "fr": "2023-07-19"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "What to expect when a premises is declared a high risk contact premises",
        "fr": "\u00c0 quoi s'attendre lorsqu'un lieu est d\u00e9clar\u00e9 lieu de contact \u00e0 haut risque"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>A high risk contact premises (HRCP) is declared when a premises is at risk of being exposed or has been exposed to <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/eng/1651075538411/1651075538958\">highly pathogenic avian influenza</a> (HPAI). Quarantine and movement restrictions apply to HRCPs, even if HPAI has not been detected through laboratory testing.</p>\n\n<h2>Why restrictions are required</h2>\n\n<p>When the Canadian Food Inspection Agency (CFIA) is aware of HPAI infections, we need to control the risk of infection to other premises.</p>\n\n<p>HRCP restrictions are established to limit the spread of disease. These controls are issued under the <i>Health of Animals Act and</i> regulations.</p>\n\n<p>This prohibits the movement of animals or things that may have been exposed to HPAI on, off or within the premises, unless movement permissions have been issued by the CFIA.</p>\n\n<h2>HRCP process</h2>\n\n<h3>Step 1: How premises are declared HRCP</h3>\n\n<p>Poultry operations are classified as high risk contact premises (HRCP) when:</p>\n\n<ul>\n<li>direct or indirect contact is identified and relevant epidemiological information supports a high risk classification</li>\n<li>any poultry productions on a premises are located within 200 meters from a poultry production building on an infected premises</li>\n<li>a premises is part of an anseriformes production network associated with an infected anseriformes premises</li>\n</ul>\n\n<h3>Step 2: Quarantine and movement restrictions</h3>\n\n<p>The premises is issued a Declaration of an Infected Place and a Requirement to Quarantine. This applies to poultry operations inside or outside a declared <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/status-of-ongoing-avian-influenza-response/ai-zones/eng/1648851134912/1648851359195\">Primary Control Zone</a> (PCZ).</p>\n\n<p>HRCP restrictions remain in place until 14 days from the last date of contact with the infected premises. Restrictions are released once all required surveillance activities have been completed with negative results.</p>\n\n<p>Individual premises movement restrictions will apply during the surveillance period. The CFIA will assign a case officer to review HRCP requirements and provide information on specific movement criteria and conditions.</p>\n\n<p>The CFIA has the right to change movement requirements and apply additional criteria or conditions if a risk is determined to be present on the individual premises.</p>\n\n<h3>Step 3: Apply for a movement permissions</h3>\n\n<p>You must get permission from the CFIA before moving controlled commodities. This applies to poultry, poultry products or by-products, and things such as vehicles and equipment of service providers entering the poultry premises.</p>\n\n<p>The CFIA will grant permissions to a HCRP by issuing:</p>\n\n<ul>\n<li>License for Removal of Animals or Things; and/or</li>\n<li>License to Transport Animals or Things</li>\n</ul>\n\n<p>Movement permissions for a HRCP within a Primary Control Zone (PCZ) differ from the permit requirements that apply to non-HRCP premises within a PCZ. For more details, please see <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/status-of-ongoing-avian-influenza-response/permits-and-conditions/eng/1648871137667/1648871138011\">Avian influenza \u2013 permits and conditions needed for movement control</a>.</p>\n\n<h4>How to request a movement permission</h4>\n\n<ul>\n<li>complete the <a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5973/eng/1651680834470/1651680834877\">Permission Request Form for the Movement of Animals and/or Things</a> (CFIA/ACIA 5973) for each movement request (if assigned a case officer, work with your case officer to complete this request)</li>\n<li>attach required documentation to the request (that is biosecurity standard operating procedures)</li>\n<li>requests are reviewed by the local district office or by the area/regional emergency operations centre movement control group</li>\n<li>licenses for movement are issued by CFIA movement control group inspectors once all conditions for movement have been met</li>\n</ul>\n\n<h3>Step 4: When a HRCP is released from restrictions</h3>\n\n<p>HRCP requirements will stay in place for 14 days after the last at-risk contact with the infected premises (IP).</p>\n\n<p>The last at-risk contact is defined as:</p>\n\n<ul>\n<li>HRCP located within 200 meters from the IP: Capping milestone completed on the IP</li>\n<li>HRCP located within the rest of the infected zone, restricted zone or outside of the primary control zone: Last direct or indirect contact with the IP</li>\n</ul>\n\n<p>Once individual premises movement restrictions are released on a HRCP, a premises within a PCZ will be under the <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/latest-bird-flu-situation/status-of-ongoing-avian-influenza-response/permits-and-conditions/eng/1648871137667/1648871138011\">conditions for movement permits</a> within the respective zone. Premises outside of a current PCZ will no longer require movement permissions.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Un lieu de contact \u00e0 haut risque est d\u00e9clar\u00e9 lorsqu'un lieu est \u00e0 risque d'\u00eatre expos\u00e9 ou a \u00e9t\u00e9 expos\u00e9 \u00e0 l'influenza aviaire hautement pathog\u00e8ne (IAHP). Une quarantaine s'effectue\u00a0et des restrictions en mati\u00e8re de d\u00e9placements s'appliquent aux lieux de contact \u00e0 haut risque, et ce, m\u00eame si l'IAHP n'a pas \u00e9t\u00e9 d\u00e9tect\u00e9e par des tests de laboratoire.</p>\n\n<h2>Raisons pour lesquelles les restrictions sont n\u00e9cessaires</h2>\n\n<p>Lorsque l'Agence canadienne d'inspection des aliments (ACIA) sait qu'il y a infection par l'IAHP, elle doit contr\u00f4ler le risque d'infection vers d'autres lieux.</p>\n\n<p>Les restrictions \u00e9mises dans les lieux de contact \u00e0 haut risque sont \u00e9tablies afin de limiter la propagation des maladies. Ces mesures de contr\u00f4le sont \u00e9mises aux termes de la <i>Loi sur la sant\u00e9 des animaux</i> et du <i>R\u00e8glement sur la sant\u00e9 des animaux</i>.</p>\n\n<p>Ces dispositions permettent d'interdire les d\u00e9placements d'animaux ou de choses qui auraient pu \u00eatre expos\u00e9s \u00e0 l'IAHP sur ou vers les lieux, \u00e0 l'int\u00e9rieur ou \u00e0 l'ext\u00e9rieur de ces derniers, \u00e0 moins d'avoir obtenu un permis de d\u00e9placement \u00e9mis par l'ACIA.</p>\n\n<h2>Processus relatif aux lieux de contact \u00e0 haut risque</h2>\n\n<h3>\u00c9tape 1\u00a0: De quelle fa\u00e7on un lieu est d\u00e9clar\u00e9 lieu de contact \u00e0 haut risque</h3>\n\n<p>Les exploitations avicoles sont consid\u00e9r\u00e9es comme des lieux de contact \u00e0 haut risque lorsque\u00a0:</p>\n\n<ul>\n<li>un contact direct ou indirect a \u00e9t\u00e9 \u00e9tabli et des renseignements \u00e9pid\u00e9miologiques pertinents appuient une classification \u00e0 haut risque;</li>\n<li>toute production de volaille sur un lieu est situ\u00e9e \u00e0 moins de 200 m\u00e8tres d'un \u00e9tablissement de production de volaille se trouvant sur un lieu infect\u00e9;</li>\n<li>un lieu fait partie d'un r\u00e9seau de production ans\u00e9riforme associ\u00e9 \u00e0 un lieu ans\u00e9riforme infect\u00e9.</li>\n</ul>\n\n<h3>\u00c9tape 2\u00a0: Quarantaine et restrictions en mati\u00e8re de d\u00e9placements</h3>\n\n<p>Une D\u00e9claration d'un lieu contamin\u00e9 et une ordonnance de quarantaine sont \u00e9mises au propri\u00e9taire du lieu. Cette mesure s'applique aux exploitations avicoles \u00e0 l'int\u00e9rieur ou \u00e0 l'ext\u00e9rieur d'une <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/etat-de-reponse-en-cours-aux-detections-d-iahp/zone-de-l-ia/fra/1648851134912/1648851359195\">zone de contr\u00f4le primaire</a> (ZCP) d\u00e9clar\u00e9e.</p>\n\n<p>Les restrictions \u00e9mises dans un lieu de contact \u00e0 haut risque demeurent en vigueur jusqu'\u00e0 14 jours apr\u00e8s la date du dernier contact avec le lieu infect\u00e9. Les restrictions sont lev\u00e9es une fois que toutes les activit\u00e9s de surveillance requises ont \u00e9t\u00e9 effectu\u00e9es et dont les r\u00e9sultats sont n\u00e9gatifs.</p>\n\n<p>Des restrictions individuelles visant les d\u00e9placements au sein des lieux s'appliqueront pendant la p\u00e9riode de surveillance. L'ACIA affectera une agente ou un agent responsable du dossier pour examiner les exigences relatives au lieu de contact \u00e0 haut risque et fournir des renseignements sur les conditions et les crit\u00e8res de d\u00e9placements.</p>\n\n<p>L'ACIA est en droit de modifier les exigences relatives aux d\u00e9placements et d'appliquer des crit\u00e8res ou des conditions suppl\u00e9mentaires s'il est \u00e9tabli qu'un risque est pr\u00e9sent sur un lieu en particulier.</p>\n\n<h3>\u00c9tape 3\u00a0: Soumettre une demande de permission de d\u00e9placements</h3>\n\n<p>Vous devez obtenir la permission de l'ACIA avant de d\u00e9placer des marchandises contr\u00f4l\u00e9es. Cette mesure s'applique \u00e0 la volaille, aux produits ou aux sous-produits de volaille, et \u00e0 des objets comme les v\u00e9hicules et l'\u00e9quipement des fournisseurs de services qui entrent dans les installations avicoles.</p>\n\n<p>L'ACIA octroiera une permission de d\u00e9placements dans les lieux de contact \u00e0 haut risque en d\u00e9livrant l'un des deux permis suivants, ou les 2\u00a0:</p>\n\n<ul>\n<li>permis d'enl\u00e8vement d'animaux ou de choses</li>\n<li>permis de transporter des animaux ou des choses</li>\n</ul>\n\n<p>Les autorisations de d\u00e9placements pour un lieu de contact \u00e0 haut risque qui se trouve dans une ZCP diff\u00e8rent des exigences de permis pour les lieux qui ne sont pas \u00e0 haut risque. Pour obtenir plus de d\u00e9tails, veuillez consulter <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/etat-de-reponse-en-cours-aux-detections-d-iahp/permis-et-conditions-necessaires/fra/1648871137667/1648871138011\">Influenza aviaire \u2013 permis et conditions n\u00e9cessaires au contr\u00f4le des d\u00e9placements</a>.</p>\n\n<h4>Comment soumettre une demande de permission de d\u00e9placements</h4>\n\n<ul>\n<li>Le <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5973/fra/1651680834470/1651680834877\">formulaire de demande de permis pour le d\u00e9placement d'animaux et/ou de choses (ACIA/CFIA 5973)</a> doit \u00eatre rempli pour chaque demande de d\u00e9placement (si une agente ou un agent responsable du dossier vous est affect\u00e9, faites-vous aider par cette personne pour remplir la demande).</li>\n<li>Toute documentation n\u00e9cessaire \u00e0 votre demande doit \u00eatre jointe (par exemple les proc\u00e9dures op\u00e9rationnelles normalis\u00e9es sur la bios\u00e9curit\u00e9).</li>\n<li>Les demandes sont examin\u00e9es par le bureau de district local de la r\u00e9gion ou par le groupe responsable du contr\u00f4le des d\u00e9placements du centre r\u00e9gional des op\u00e9rations d'urgence.</li>\n<li>Les permis de d\u00e9placement sont \u00e9mis par les inspectrices et inspecteurs du groupe de contr\u00f4le des d\u00e9placements de l'ACIA une fois que toute les conditions relatives aux d\u00e9placements ont \u00e9t\u00e9 respect\u00e9es.</li>\n</ul>\n\n<h3>\u00c9tape 4\u00a0: Moment o\u00f9 les restrictions \u00e9mises dans un lieu de contact \u00e0 haut risque sont lev\u00e9es</h3>\n\n<p>Les exigences relatives au lieu de contact \u00e0 haut risque demeureront en vigueur 14\u00a0jours suivant le dernier contact \u00e0 risque avec le lieu infect\u00e9.</p>\n\n<p>Le dernier contact \u00e0 risque est d\u00e9fini comme suit\u00a0:</p>\n\n<ul>\n<li>le lieu de contact \u00e0 haut risque est situ\u00e9 \u00e0 moins de 200 m\u00e8tres du lieu infect\u00e9\u00a0: \u00e9tape de plafonnement atteinte sur le lieu infect\u00e9;</li>\n<li>le lieu de contact \u00e0 haut risque est situ\u00e9 dans le reste de la zone infect\u00e9e, dans la zone d'acc\u00e8s restreinte ou \u00e0 l'ext\u00e9rieur de la zone de contr\u00f4le primaire\u00a0: dernier contact direct ou indirect avec le lieu infect\u00e9.</li>\n</ul>\n\n<p>Une fois que les restrictions individuelles li\u00e9es aux d\u00e9placements sur le lieu de contact \u00e0 haut risque sont lev\u00e9es, un lieu situ\u00e9 dans une ZCP sera assujetti aux <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/situation-actuelle-de-la-grippe-aviaire/etat-de-reponse-en-cours-aux-detections-d-iahp/permis-et-conditions-necessaires/fra/1648871137667/1648871138011\">conditions des permis de d\u00e9placement</a> dans la zone respective. Les lieux situ\u00e9s \u00e0 l'ext\u00e9rieur d'une ZCP ne n\u00e9cessiteront plus de permissions de d\u00e9placements.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}