{
    "dcr_id": "1366999160254",
    "title": {
        "en": "Blister Beetles",
        "fr": "M&eacute;lo&eacute;s"
    },
    "modified": "2015-08-07",
    "issued": "2014-06-10",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2014-06-10",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/blistbeet-melo_1366999160254_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/blistbeet-melo_1366999160254_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Blister Beetles",
        "fr": "M&eacute;lo&eacute;s"
    },
    "breadcrumb": {
        "en": "Blister Beetles",
        "fr": "M&eacute;lo&eacute;s"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2014-06-10",
            "fr": "2014-06-10"
        },
        "modified": {
            "en": "2015-08-07",
            "fr": "2015-08-07"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Blister Beetles",
            "fr": "M&eacute;lo&eacute;s"
        },
        "subject": {
            "en": "pest management;diseases",
            "fr": "plantes nuisibles;plantes-maladies et fl\u00e9aux"
        },
        "dcterms:subject": {
            "en": "trees;plant diseases",
            "fr": "maladie des plantes;arbre"
        },
        "description": {
            "en": "There are three types of blister beetles that may cause damage to trees and shrubs on the prairies. Larvae overwinter in the soil while adult beetles are present from May to July. The adults are long, slender, beetles measuring 12 to 28 mm in length. They may be grey, shiny black or a metallic purple with a green tinge. In the fall, eggs are deposited in the soil where the larvae develop by feeding on grasshopper and other insect eggs.",
            "fr": "Trois types de m\u00e9lo\u00e9s peuvent endommager les arbres et les arbustes des prairies. La larve hiverne dans le sol tandis que les insectes adultes sont pr\u00e9sents de mai \u00e0 juillet. L'insecte adulte a une forme allong\u00e9e et filiforme et mesure de 12 \u00e0 28 mm de longueur. Il peut \u00eatre de couleur grise, noir brillant ou pourpre m\u00e9tallique teint\u00e9e de vert. \u00c0 l'automne, les \u0153ufs sont d\u00e9pos\u00e9s dans le sol o\u00f9 les larves se d\u00e9veloppent en se nourrissant d'\u0153ufs de sauterelles ou d'autres insectes."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " Caragana and other trees and shrubs; Ash;Epicauta spp.",
            "fr": " caragana et autres arbres et arbustes; Fr\u00eane;Epicauta spp."
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Blister Beetles",
        "fr": "M\u00e9lo\u00e9s"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p><i>Epicauta <abbr title=\"species\">spp.</abbr></i></p>\n \n<h2>Hosts</h2>\n<p>Ash, Caragana and other trees and shrubs</p>\n \n<h2>Appearance and Life Cycle</h2>\n\n<div class=\"row\">\n\t<div class=\"col-md-4 pull-right\">\n        <figure>\n            <img class=\"img-responsive\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/blistbeet-melo_1.jpg\" alt=\"Description of this image follows\">\n            <figcaption><i>Blister beetles mating on Texas mountain laurel.<br>Photo credit: Drees, insects.tamu.edu</i></figcaption>\n        </figure>\n\t</div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n            <p>There are three types of blister beetles that may cause damage to trees and shrubs on the prairies. Larvae overwinter in the soil while adult beetles are present from May to July. The adults are long, slender, beetles measuring 12 to 28 <abbr title=\"millimeters\">mm</abbr> in length. They may be grey, shiny black or a metallic purple with a green tinge. In the fall, eggs are deposited in the soil where the larvae develop by feeding on grasshopper and other insect eggs.</p>\n             \n            <h2>Damage</h2>\n            <p>Blister beetles can be both destructive and beneficial insects. The insect is destructive during the adult stage when it causes defoliation, and beneficial during the larval stage when it feeds on grasshopper eggs. The adult beetles seem to invade in swarms and devour the foliage of host plants. Infestations are often localized and the beetles can disappear as quickly as they appear. The beetles can cause complete defoliation of young ash and caragana shelterbelts, which results in a reduction of annual growth but no permanent damage. Blister beetles can also cause extensive damage to garden plants.</p>\n            \n            \n            <h2>Control</h2>\n            \n            <p>If an infestation occurs in a garden or on a few trees, control can be achieved by picking the beetles off the plants. Products are available to control the insect, but before spraying, consider the potential loss of pollinating bees during spraying and the beneficial aspects of the beetle during the larval stage. If chemical control is used, spray foliage and insects with: carbaryl; deltamethrin or methoxychlor.</p>\n\t</div>\n</div>\n\r\n    ",
        "fr": "\r\n    <p><i>Epicauta <abbr title=\"esp\u00e8ces\">spp.</abbr></i></p>\n \n<h2>H\u00f4tes</h2>\n\n<p>Fr\u00eane, caragana et autres arbres et arbustes</p>\n \n<h2>Aspect et cycle de vie</h2>\n\n<div class=\"row\">\n\t<div class=\"col-md-4 pull-right\">\n            <figure>\n            <img class=\"mrgn-bttm-0 img-responsive\" alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/blistbeet-melo_1.jpg\">\n            <figcaption><i>M\u00e9lo\u00e9s s'accouplant sur un plant de kalmia \u00e0 feuilles larges. <br>Source : Drees, insects.tamu.edu</i></figcaption>\n                </figure>\n\t</div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n            <p>Trois types de m\u00e9lo\u00e9s peuvent endommager les arbres et les arbustes des prairies. La larve hiverne dans le sol tandis que les insectes adultes sont pr\u00e9sents de mai \u00e0 juillet. L'insecte adulte a une forme allong\u00e9e et filiforme et mesure de 12 \u00e0 28\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> de longueur. Il peut \u00eatre de couleur grise, noir brillant ou pourpre m\u00e9tallique teint\u00e9e de vert. \u00c0 l'automne, les \u0153ufs sont d\u00e9pos\u00e9s dans le sol o\u00f9 les larves se d\u00e9veloppent en se nourrissant d'\u0153ufs de sauterelles ou d'autres insectes.</p>\n             \n            <h2>Dommages</h2>\n            \n            <p>Les m\u00e9lo\u00e9s sont \u00e0 la fois des insectes ravageurs et b\u00e9n\u00e9fiques. L'insecte est nuisible au stade adulte, parce qu'il entra\u00eene la d\u00e9foliation, et b\u00e9n\u00e9fique au stade larvaire, lorsqu'il se nourrit d'\u0153ufs de sauterelles. Les insectes adultes attaquent en essaims et d\u00e9vorent le feuillage de la plante h\u00f4te. Les infestations sont souvent localis\u00e9es et les m\u00e9lo\u00e9s peuvent dispara\u00eetre aussi vite qu'ils sont apparus. Les m\u00e9lo\u00e9s peuvent entra\u00eener la d\u00e9foliation des jeunes fr\u00eanes et des caraganas utilis\u00e9s en brise-vent, ce qui en ralentit la croissance annuelle, mais ne cause aucun dommage permanent. Les m\u00e9lo\u00e9s peuvent aussi causer des dommages importants aux plantes de jardin.</p>\n             \n            <h2>Lutte</h2>\n            \n            <p>Il est possible de contr\u00f4ler l'infestation d'un jardin ou de quelques arbres en enlevant les insectes des v\u00e9g\u00e9taux. Certains produits permettent de combattre l'insecte, mais il faut tenir compte du fait que la vaporisation peut entra\u00eener la destruction d'abeilles pollinisatrices et att\u00e9nuer la nature b\u00e9n\u00e9fique de l'insecte \u00e0 son stade larvaire. Lorsqu'on utilise des produits chimiques pour lutter contre les m\u00e9lo\u00e9s, il faut vaporiser le feuillage et les insectes au moyen des produits suivants : carbaryl, deltam\u00e9thrine ou m\u00e9thoxychlore.</p>\n\t</div>\n</div>\n\r\n    "
    }
}