{
    "dcr_id": "1657558817043",
    "title": {
        "en": "Livestock feed approval or registration: After you apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "html_modified": "2024-03-12 3:59:19 PM",
    "modified": "2022-07-11",
    "issued": "2022-07-19",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/5_after_apply_lvstck_fd_apprvl_1657558817043_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/5_after_apply_lvstck_fd_apprvl_1657558817043_fra"
    },
    "ia_id": "1657558817433",
    "parent_ia_id": "1627997833424",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1627997833424",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Livestock feed approval or registration: After you apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "label": {
        "en": "Livestock feed approval or registration: After you apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1657558817433",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1627997831971",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/approval-and-registration/livestock-feed/",
        "fr": "/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Livestock feed approval or registration: After you apply",
            "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
        },
        "description": {
            "en": "Application evaluation process\nThere are 3 main steps to the overall evaluation process of a livestock feed approval or registration:",
            "fr": "Processus de l\u2019\u00e9valuation d\u2019une demande\nLe processus d\u2019\u00e9valuation global d\u2019une approbation ou d\u2019un enregistrement des aliments pour animaux de ferme comporte 3 \u00e9tapes principales :"
        },
        "keywords": {
            "en": "Animals, Animal Health, livestock, feed, after you apply, application, regulation, service",
            "fr": "aliments, animaux de ferme, Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande, informations, entreprise, service"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "modified": {
            "en": "2022-07-11",
            "fr": "2022-07-11"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Livestock feed approval or registration: After you apply",
        "fr": "Approbation ou enregistrement des aliments pour animaux de ferme\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657223639320/1657223788955\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657306963131/1657306963740\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657309218484/1657309218702\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657557647452/1657557647827\">4. How to apply</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\" href=\"\">5. After you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657559631753/1657559632175\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<h2>Application evaluation process</h2>\n\t\n<p>There are 3 main steps to the overall evaluation process of a livestock feed approval or registration:</p>\n\t\n<h3>1. Preliminary screening</h3>\n\t\n<p>On receipt of your application, the CFIA performs a preliminary screening of the materials in your submission. The purpose of this screening is to determine if all necessary elements in your submission are present (that is signing authority, company information, application form, fees and supporting data).</p>\n\t\n<p>Based on the preliminary screening, the CFIA will notify you if your submission has been accepted for evaluation or rejected. If the CFIA accepts your submission, the CFIA will log it in the queue for evaluation and your payment will be processed. If the CFIA rejects your submission, the reasons will be detailed in return correspondence to you.</p>\n\t\n<h3>2. Evaluation</h3>\n\t\n<p>If an evaluator identifies any deficiencies during the evaluation, the CFIA will either:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>place your submission on hold with a specified timeline for receipt of the requested information or</li>\n<li>return your submission</li>\n</ul>\n\t\n<h3>3. Decision</h3>\n\t\n<p>Once the evaluation is completed, the CFIA will communicate to the applicant a decision of either:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>approval or</li>\n<li>rejection</li>\n</ul>\n\t\n<p>To complete the registration process, the applicant must submit a final label to the Pre-market Application Submissions Office (PASO). The final label must include any changes that the CFIA has requested. If the applicant does not submit the final label or make the changes as requested, the livestock feed will not be registered.</p>\n\t\n<p>For more information on the application evaluation process, refer to the <a href=\"/eng/1329109265932/1329109385432#intro\">introduction</a> and Chapter\u00a01 sections <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-1/eng/1329291099314/1329291708583?chap=0#s8c3\">1.3.5</a> to <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-1/eng/1329291099314/1329291708583?chap=0#s11c4\">1.4.3</a> of the RG-1 document.</p>\n\t\n<h2>Temporary registration</h2>\n\t\n<p>The CFIA may issue a temporary registration for a livestock feed. Temporary registrations are issued for livestock feeds that have demonstrated both safety and potential usefulness, but require more evidence in support of efficacy or validation of an analytical method.</p>\n\t\n<p>The CFIA issues temporary registrations as an interim measure to allow companies time to gather this evidence to fulfill  the conditions outlined in their approval letter. Applicants can't apply for a temporary registration. The CFIA only considers this option once an evaluation is complete.</p>\n\t\n<p>For more information about temporary registration, refer to <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-1/eng/1329291099314/1329291708583?chap=4#s12c4\">Chapter\u00a01 section 1.4.4</a> of the RG-1 document.</p>\n\t\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657557647452/1657557647827\" rel=\"prev\">Previous: How to apply</a></li>\n<li class=\"next\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657559631753/1657559632175\" rel=\"next\">Next: Amending and renewing</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657223639320/1657223788955\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657306963131/1657306963740\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657309218484/1657309218702\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657557647452/1657557647827\">4. Comment pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\" href=\"\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657559631753/1657559632175\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\t\n<h2>Processus de l'\u00e9valuation d'une demande</h2>\n\t\n<p>Le processus d'\u00e9valuation global d'une approbation ou d'un enregistrement des aliments pour animaux de ferme comporte\u00a03 \u00e9tapes principales\u00a0:</p>\n\t\n<h3>1. V\u00e9rification pr\u00e9liminaire</h3>\n\t\n<p>\u00c0 la r\u00e9ception de votre demande, l'ACIA proc\u00e8de \u00e0 une v\u00e9rification pr\u00e9liminaire des documents contenus dans votre dossier de demande. Le but de cette v\u00e9rification est de d\u00e9terminer si tous les \u00e9l\u00e9ments n\u00e9cessaires dans votre demande sont pr\u00e9sents (c'est-\u00e0-dire le pouvoir de signature, les informations sur l'entreprise, le formulaire de demande, les frais et les donn\u00e9es \u00e0 l'appui).</p>\n\t\n<p>Suite \u00e0 la v\u00e9rification pr\u00e9liminaire, l'ACIA vous informera si votre demande a \u00e9t\u00e9 accept\u00e9e pour l'\u00e9valuation ou rejet\u00e9e. Si l'ACIA accepte votre demande, l'ACIA la placera dans la file d'attente pour \u00e9valuation et votre paiement sera trait\u00e9. Si l'ACIA rejette votre demande, les raisons vous seront d\u00e9taill\u00e9es dans une correspondance de retour.</p>\n\t\n<h3>2. \u00c9valuation</h3>\n\t\n<p>Si des lacunes sont relev\u00e9es au cours de l'\u00e9valuation, l'ACIA va soit\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>mettre votre demande en attente avec un d\u00e9lai sp\u00e9cifi\u00e9 pour la r\u00e9ception des informations demand\u00e9es, ou</li>\n<li>retourner votre demande</li>\n</ul>\n\t\n<h3>3. Avis de d\u00e9cision</h3>\n\t\n<p>Une fois l'\u00e9valuation termin\u00e9e, l'ACIA communiquera au demandeur une d\u00e9cision soit\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>approbation, ou</li>\n<li>rejet</li>\n</ul>\n\t\n<p>Pour terminer le processus d'enregistrement, le demandeur doit soumettre une \u00e9tiquette finale au Bureau de pr\u00e9sentation de demandes pr\u00e9alables \u00e0 la mise en march\u00e9 (BPDPM). L'\u00e9tiquette finale doit inclure tous les changements demand\u00e9s par l'ACIA. Si le demandeur ne soumet pas l'\u00e9tiquette finale ou n'apporte pas les changements demand\u00e9s, l'aliment pour animaux de ferme ne sera pas enregistr\u00e9.</p>\n\t\n<p>Pour plus d'informations sur le processus d'\u00e9valuation des demandes, veuillez consulter l'<a href=\"/fra/1329109265932/1329109385432#intro\">introduction</a> et les section <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-1/fra/1329291099314/1329291708583?chap=0#s8c3\">1.3.5</a> \u00e0 <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-1/fra/1329291099314/1329291708583?chap=0#s11c4\">1.4.3</a> du chapitre 1 du document RG-1.</p>\n\t\n<h2>Enregistrement temporaire</h2>\n\t\n<p>L'ACIA peut d\u00e9livrer un enregistrement temporaire pour un aliment pour animaux de ferme. Des enregistrements temporaires sont d\u00e9livr\u00e9s pour des aliments pour animaux de ferme qui ont d\u00e9montr\u00e9 \u00e0 la fois leur innocuit\u00e9 et leur utilit\u00e9 potentielle, mais qui n\u00e9cessitent plus de preuves \u00e0 l'appui de l'efficacit\u00e9 ou de la validation d'une m\u00e9thode d'analyse.</p>\n\t\n<p>L'ACIA d\u00e9livre des enregistrements temporaires \u00e0 titre de mesure provisoire pour donner aux entreprises le temps de rassembler les preuves pour remplir les conditions \u00e9nonc\u00e9es dans leur lettre d'approbation. Les demandeurs ne peuvent pas demander un enregistrement temporaire. L'ACIA n'envisage cette option qu'une fois l'\u00e9valuation termin\u00e9e.</p>\n\t\n<p>Pour plus d'informations sur l'enregistrement temporaire, veuillez consulter la <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-1/fra/1329291099314/1329291708583?chap=4#s12c4\">section 1.4.4 du chapitre\u00a01</a> du document RG-1.</p>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657557647452/1657557647827\" rel=\"prev\">Pr\u00e9c\u00e9dent\u00a0: Avant de pr\u00e9senter une demande</a></li>\n<li class=\"next\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657559631753/1657559632175\" rel=\"next\">Suivant\u00a0: Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}