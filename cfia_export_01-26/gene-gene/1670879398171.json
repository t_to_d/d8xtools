{
    "dcr_id": "1670879398171",
    "title": {
        "en": "Regulatory oversight of pet food, treats and chews in Canada",
        "fr": "Surveillance r\u00e9glementaire des aliments, g\u00e2teries et produits \u00e0 m\u00e2cher pour animaux de compagnie au Canada"
    },
    "html_modified": "2024-01-26 2:26:04 PM",
    "modified": "2022-12-13",
    "issued": "2022-12-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_reg_oversight_pet_food_treats_chews_1670879398171_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_export_reg_oversight_pet_food_treats_chews_1670879398171_fra"
    },
    "ia_id": "1670879399109",
    "parent_ia_id": "1321153110863",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1321153110863",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Regulatory oversight of pet food, treats and chews in Canada",
        "fr": "Surveillance r\u00e9glementaire des aliments, g\u00e2teries et produits \u00e0 m\u00e2cher pour animaux de compagnie au Canada"
    },
    "label": {
        "en": "Regulatory oversight of pet food, treats and chews in Canada",
        "fr": "Surveillance r\u00e9glementaire des aliments, g\u00e2teries et produits \u00e0 m\u00e2cher pour animaux de compagnie au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1670879399109",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1321152787689",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/exports/pet-food/pet-food-treats-and-chews/",
        "fr": "/sante-des-animaux/animaux-terrestres/exportation/aliments-pour-animaux-de-compagnie/aliments-gateries-et-produits-a-macher-pour-animau/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Regulatory oversight of pet food, treats and chews in Canada",
            "fr": "Surveillance r\u00e9glementaire des aliments, g\u00e2teries et produits \u00e0 m\u00e2cher pour animaux de compagnie au Canada"
        },
        "description": {
            "en": "Pet food is not a comprehensively regulated commodity in Canada compared to food for human consumption or livestock feeds.",
            "fr": "Les aliments pour animaux de compagnie ne sont pas un produit r\u00e9glement\u00e9 de mani\u00e8re exhaustive au Canada par rapport aux aliments destin\u00e9s \u00e0 la consommation humaine ou aux aliments pour le b\u00e9tail."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, animal health, export, regulation, pet, Regulatory oversight, pet food, treats, chews, Canada",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, sant\u00e9 des animaux, exportation, politique, Surveillance r\u00e9glementaire, aliments, g\u00e2teries, produits \u00e0 m\u00e2cher, animaux de compagnie, Canada"
        },
        "dcterms.subject": {
            "en": "animal nutrition,exports,inspection,animal health",
            "fr": "alimentation animale,exportation,inspection,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-12-13",
            "fr": "2022-12-13"
        },
        "modified": {
            "en": "2022-12-13",
            "fr": "2022-12-13"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Regulatory oversight of pet food, treats and chews in Canada",
        "fr": "Surveillance r\u00e9glementaire des aliments, g\u00e2teries et produits \u00e0 m\u00e2cher pour animaux de compagnie au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Pet food is not a comprehensively regulated commodity in Canada compared to food for human consumption or livestock feeds.</p>\n\n<p>The oversight of pet food by the Canadian Food Inspection Agency (CFIA) is limited to compliance with regulatory requirements for import and export of animal products and by-products.</p>\n\n<p>Pet food can contain animal products and by-products. Therefore, the <a href=\"/importing-food-plants-or-animals/animal-animal-product-and-by-product-imports/eng/1574095222197/1574095273924\">rules for importing live animals, animal products and by-products</a> may apply.</p>\n\n<p>However, certain ingredients used in pet food may be regulated by different regulatory departments in Canada. Depending on the intended purpose of the ingredient or the final product, the following departments may be involved in regulatory oversight:</p>\n\n<ul>\n<li><a href=\"https://www.canada.ca/en/environment-climate-change/services/managing-pollution/evaluating-new-substances.html\">Environment and Climate Change Canada\u00a0\u2013 New substances</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/consumer-product-safety/pesticides-pest-management.html\">Health Canada\u00a0\u2013 Pesticides and Pest Management</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/corporate/about-health-canada/branches-agencies/health-products-food-branch/veterinary-drugs-directorate.html\">Health Canada\u00a0\u2013 Veterinary Drugs</a></li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/antibiotic-antimicrobial-resistance/animals/veterinary-health-products.html\">Health Canada\u00a0\u2013 Veterinary Health Products</a></li>\n</ul>\n\n<p>Other pertinent information related to pet food:</p>\n\n<ul>\n<li><a href=\"https://ised-isde.canada.ca/site/competition-bureau-canada/en/how-we-foster-competition/education-and-outreach/publications/guide-labelling-and-advertising-pet-foods\">Competition Bureau\u00a0\u2013 Pet food labelling</a></li>\n<li><a href=\"https://pfac.com/\">Pet Food Association of Canada</a></li>\n</ul>\n\n<p>You can also refer to the <a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">Automated Import Reference System</a> (AIRS) to provide accurate and timely information on import requirements.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Les aliments pour animaux de compagnie ne sont pas un produit r\u00e9glement\u00e9 de mani\u00e8re exhaustive au Canada par rapport aux aliments destin\u00e9s \u00e0 la consommation humaine ou aux aliments pour le b\u00e9tail.</p>\n\n<p>La surveillance des aliments pour animaux de compagnie par l'Agence canadienne d'inspection des aliments (ACIA) est limit\u00e9e \u00e0 la conformit\u00e9 aux exigences r\u00e9glementaires pour l'importation et l'exportation des produits et sous-produits d'origine animale.</p>\n\n<p>Les aliments pour animaux de compagnie peuvent contenir des produits et sous-produits d'origine animale. Par cons\u00e9quent, les <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-d-animaux-de-produits-et-de-sous-prod/fra/1574095222197/1574095273924\">r\u00e8gles pour les importations d'animaux, de produits et de sous-produits d'origine animale</a> peuvent s'appliquer.</p>\n\n<p>Cependant, certains ingr\u00e9dients utilis\u00e9s dans les aliments pour animaux de compagnie peuvent \u00eatre r\u00e9glement\u00e9s par diff\u00e9rents minist\u00e8res de r\u00e9glementation au Canada. Selon le but de l'ingr\u00e9dient ou le produit final, les minist\u00e8res suivants peuvent \u00eatre concern\u00e9s par la surveillance r\u00e9glementaire\u00a0:</p>\n\n\n<ul>\n<li><a href=\"https://www.canada.ca/fr/environnement-changement-climatique/services/gestion-pollution/evaluation-substances-nouvelles.html\">Environnement et Changement climatique Canada\u00a0\u2013 Substances nouvelles</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/securite-produits-consommation/pesticides-lutte-antiparasitaire.html\">Sant\u00e9 Canada\u00a0\u2013 Pesticides et lutte antiparasitaire</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/organisation/a-propos-sante-canada/directions-generales-agences/direction-generale-produits-sante-aliments/direction-medicaments-veterinaires.html\">Sant\u00e9 Canada\u00a0\u2013 M\u00e9dicaments v\u00e9t\u00e9rinaires</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/resistance-aux-antibiotiques-antimicrobiens/animaux/produits-veterinaires-sante.html\">Sant\u00e9 Canada\u00a0\u2013 Produits de sant\u00e9 animale</a></li>\n</ul>\n\n<p>Voici d'autres renseignements pertinents concernant les aliments pour animaux de compagnie\u00a0:</p>\n\n<ul>\n<li><a href=\"https://ised-isde.canada.ca/site/bureau-concurrence-canada/fr/comment-nous-favorisons-concurrence/education-sensibilisation/publications/guide-letiquetage-publicite-concernant-aliments-pour-animaux-familiers\">Bureau de la concurrence\u00a0\u2013 \u00c9tiquetage des aliments pour animaux de compagnie</a></li>\n<li><a href=\"https://pfac.com/\"><span lang=\"en\">Pet Food Association of Canada</span> (en anglais seulement)</a></li>\n</ul>\n\n<p>Vous pouvez \u00e9galement consulter le <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation</a> (SARI) pour fournir des renseignements corrects et en temps opportun sur les exigences d'importation.</p>\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}