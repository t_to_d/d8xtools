{
    "dcr_id": "1526564977758",
    "title": {
        "en": "Organic aquaculture products",
        "fr": "Produits aquacoles biologiques"
    },
    "html_modified": "2024-03-12 3:58:03 PM",
    "modified": "2023-03-14",
    "issued": "2018-06-13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_orgnc_aqucltr_prdcts_1526564977758_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_orgnc_aqucltr_prdcts_1526564977758_fra"
    },
    "ia_id": "1526565100440",
    "parent_ia_id": "1526652186496",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "Organic foods",
        "fr": "Aliments biologiques"
    },
    "activity": {
        "en": "Certifying organic products|Labelling",
        "fr": "Certification de produits biologiques|\u00c9tiquetage"
    },
    "commodity": {
        "en": "Fruits and vegetables, processed products|Fruits and vegetables, fresh|Fish and seafood|Other",
        "fr": "Fruits et l\u00e9gumes, produits  transform\u00e9s|Fruits et l\u00e9gumes, frais|Poissons et fruits de mer|Autre"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1526652186496",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Organic aquaculture products",
        "fr": "Produits aquacoles biologiques"
    },
    "label": {
        "en": "Organic aquaculture products",
        "fr": "Produits aquacoles biologiques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1526565100440",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526652186199",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/organic-products/aquaculture-products/",
        "fr": "/etiquetage-des-aliments/produits-biologiques/produits-aquacoles/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Organic aquaculture products",
            "fr": "Produits aquacoles biologiques"
        },
        "description": {
            "en": "Organic certification requirements are extended to aquaculture products under the SFCR and must comply with the Organic Aquaculture Standard.",
            "fr": "En vertu du RSAC, les exigences en mati\u00e8re de certification biologique sont \u00e9tendues aux produits aquacoles qui se conforment \u00e0 la norme d\u2019aquaculture biologique."
        },
        "keywords": {
            "en": "Safe Food for Canadians Regulations, SFCR, controls, organic, organic aquaculture products, seaweed, aquaponic products, organic foods",
            "fr": "R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RACF, contr\u00f4les, produits biologiques, biologiques de l'aquaculture, algues, produits aquaponiques, aliments biologiques"
        },
        "dcterms.subject": {
            "en": "food,organic foods,agri-food industry,food inspection,agri-food products,regulations,food safety",
            "fr": "aliment,aliment biologique,industrie agro-alimentaire,inspection des aliments,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2023-03-14",
            "fr": "2023-03-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Organic aquaculture products",
        "fr": "Produits aquacoles biologiques"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Under the <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a> (SFCR), organic certification requirements apply to <a href=\"/eng/1526564977758/1526565100440#def\">aquaculture products</a>. Organic aquaculture products, including seaweed products, are required to comply with the Organic Aquaculture Standard <a href=\"https://publications.gc.ca/site/eng/9.919789/publication.html\">CAN/CGSB-32.312 Organic Production Systems\u00a0\u2013\u00a0Aquaculture\u00a0\u2013\u00a0General  principles, management standards and permitted substances lists</a> incorporated by reference in the SFCR. An aquaculture product that is sent or conveyed from one province to another or that is imported and that is labelled with an organic expression must be certified by a CFIA accredited Certification Body. For more information on organic certification bodies, please visit <a href=\"/food-labels/organic-products/certification-bodies/eng/1327860541218/1327860730201\">Certification bodies providing organic certification services under the Canada Organic Regime</a>.</p>\n\n<p>If the product meets the certification requirements it could also display the <a href=\"/eng/1389725994094/1389726052482?chap=5\" title=\"Use of the Organic Logo on Organic Products\">Canada Organic Logo</a> on the label when sold within or outside of Canada.</p>\n<h2>Aquaponic products</h2>\n\n<p>With the incorporation by reference into the <abbr title=\"Safe Food for Canadians Regulations\">SFCR</abbr> of <a href=\"https://publications.gc.ca/site/eng/9.919789/publication.html\">CAN/CGSB-32.312 Organic Production Systems\u00a0\u2013\u00a0Aquaculture\u00a0\u2013\u00a0General principles, management  standards and permitted substances lists</a>, there is potential overlap of organic commodities certified under this standard and of organic commodities certified under the <a href=\"http://www.publications.gc.ca/site/eng/9.894375/publication.html\">CAN/CGSB-32.310 Organic Production Systems\u00a0\u2013\u00a0General principles and management standards</a>. Some fruits and vegetables may be certified as organic if they meet the requirements of either standard. Therefore, from a regulatory perspective, there is no distinction between an organic product grown through aquaculture practices or agriculture practices. For example, a tomato produced \u00a0following aquaculture practices and a tomato produced following \u00a0agriculture practices, provided it meets the requirements prescribed in their specific standards, will be certified and can be labelled as organic with no distinctions.</p>\n<p>In order to provide consumers with accurate information when making their purchases, producers may include voluntary information on the label of these commodities about which specific organic certification standard their product was certified under, as long as <a href=\"/eng/1389725994094/1389726052482\" title=\"Organic Claims\">labelling requirements for organic products</a> are met.</p>\n\n<h2>Equivalency arrangements for aquaculture products</h2>\n\n<p>Organic <a href=\"/eng/1526564977758/1526565100440#def\">aquaculture products</a> are not within the scope of all Canada's current <a href=\"/food-labels/organic-products/equivalence-arrangements/eng/1311987562418/1311987760268\" title=\"Organic Equivalency Arrangements with Other Countries\">equivalency arrangements</a> with other countries for organic products.</p>\n<p>As of January 15, 2021 the exporters of organic aquaculture products from countries with which Canada does not have an organic equivalency arrangement for organic aquaculture products have to find suppliers that meet Canadian organic requirements.</p>\n<p>The US-Canada Organic Equivalency Arrangement covers the trade of organic seaweed products including chlorella and spirulina certified under the United States Department of Agriculture (USDA) organic regulations (<a href=\"https://gcc02.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.ecfr.gov%2Fcgi-bin%2FretrieveECFR%3Fgp%3D%26SID%3D78677a815fe58318e1eb547019f26a26%26mc%3Dtrue%26n%3Dpt7.3.205%26r%3DPART%26ty%3DHTML&amp;data=04%7C01%7C%7Cceaea08486b64798f08908d88caf0a83%7Ced5b36e701ee4ebc867ee03cfa0d4697%7C0%7C0%7C637414030702327092%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&amp;sdata=W4B2YOKl95zCFhdCgU3h0cBMPRl4Dy68EwlmLB1jgaQ%3D&amp;reserved=0\">7 CFR 205</a>) or Canada Organic Regime (COR) organic agriculture standard (Organic production systems: general principles and management standards, <a href=\"http://www.publications.gc.ca/site/eng/9.894375/publication.html\">CAN/CGSB-32.310</a>) (<a href=\"https://www.inspection.gc.ca/organic-products/guidance-documents/united-states-canada-organic-equivalency-arrangeme/eng/1608524164580/1608524165033\">2020-12-21\u00a0\u2013\u00a0Notice to the Canadian organic industry regarding trade of seaweed, chlorella and spirulina under the United States-Canada Organic Equivalency Arrangement</a>).</p>\n<p> Historically, Canadian exports of organic seaweed certified to the agriculture standard, have been accepted by the EU under the existing EU-Canada organic equivalency agreements. CFIA allows organic seaweed traded between Canada and the EU <strong>to continue to be certified</strong> to the agriculture standard (32.310) to prevent trade disruptions while work continues on equivalency with EU. </p>\n<h3>Definition</h3>\n\n<p id=\"def\">Aquaculture products have the same meaning as in the Canadian General Standard Board standard CAN/CGSB-32.312:</p>\n\n<p>Aquaculture products are crops and livestock, or a product wholly or partly derived therefrom, cultivated in a controlled or managed aquatic environment. The products of fishing of wild animals are not considered part of this definition.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>En vertu du <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a> (RSAC), les exigences en mati\u00e8re de certification biologique s'appliquent aux <a href=\"/fra/1526564977758/1526565100440#def\">produits aquacoles</a>. Les produits aquacoles biologiques, incluant les produits d'algues, doivent se conformer \u00e0 la norme d'aquaculture biologique <a href=\"https://publications.gc.ca/site/fra/9.919792/publication.html\">CAN/CGSB-32.312-Syst\u00e8mes de production biologique\u00a0: aquaculture\u00a0\u2013\u00a0principes g\u00e9n\u00e9raux, normes de gestion et listes des substances permises</a>, qui est incorpor\u00e9e par renvoi au RSAC. Un produit aquacole \u00e9tiquet\u00e9 avec l'expression \u00ab\u00a0biologique\u00a0\u00bb et exp\u00e9di\u00e9 dans une autre province, transport\u00e9 d'une province \u00e0 une autre ou import\u00e9 doit \u00eatre certifi\u00e9 par un organisme de certification agr\u00e9\u00e9 par l'ACIA. Pour de plus amples renseignements au sujet des organismes de certification biologique, veuillez consulter la page intitul\u00e9e <a href=\"/etiquetage-des-aliments/produits-biologiques/organismes-de-certification/fra/1327860541218/1327860730201\">Organismes de certification pr\u00e9voyant la certification biologiques sous le r\u00e9gime biologique du Canada</a>. </p>\n<p> Si le produit satisfait aux exigences de certification, il peut aussi porter le <a href=\"/fra/1389725994094/1389726052482?chap=5\" title=\"Utilisation du logo Biologique sur les \u00e9tiquettes de produits biologiques\">logo Biologique Canada</a>  sur son \u00e9tiquette lorsqu'il est vendu au Canada ou \u00e0 l'\u00e9tranger.</p>\n<h2>Produits issus de la culture aquaponique</h2>\n\n<p>En raison de l'incorporation par renvoi dans le <abbr title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</abbr> de la norme <a href=\"https://publications.gc.ca/site/fra/9.919792/publication.html\">CAN/CGSB-32.312-Syst\u00e8mes de production biologique\u00a0: aquaculture\u00a0\u2013\u00a0principes g\u00e9n\u00e9raux, normes  de gestion et listes des substances permises</a>, il pourrait y avoir un chevauchement des produits biologiques certifi\u00e9s en vertu de cette norme et de ceux certifi\u00e9s en vertu de la norme <a href=\"http://www.publications.gc.ca/site/eng/9.894378/publication.html\">CAN/CGSB-32.310-Syst\u00e8mes de production biologique\u00a0\u2013\u00a0Principes g\u00e9n\u00e9raux et normes de gestion</a>. Certains fruits et l\u00e9gumes peuvent \u00eatre certifi\u00e9s biologiques s'ils rencontrent les exigences de l'une ou l'autre norme. Donc, d'un point de vue r\u00e9glementaire, il n'y a pas de distinction entre un produit biologique d'aquaculture ou un produit biologique d'agriculture. Par exemple, une tomate issue de l'aquaculture et une tomate issue de l'agriculture, \u00e0 condition qu'elle r\u00e9ponde aux exigences prescrites dans leurs normes sp\u00e9cifiques, sera certifi\u00e9e et peut \u00eatre \u00e9tiquet\u00e9e \u00ab\u00a0biologique\u00a0\u00bb sans aucune distinction.</p>\n<p>Pour que les consommateurs disposent de renseignements exacts lors de leurs achats, les producteurs peuvent choisir de pr\u00e9ciser sur l'\u00e9tiquette de ces produits la norme de certification biologique selon laquelle leur produit a \u00e9t\u00e9 certifi\u00e9, \u00e0 condition de respecter les <a href=\"/fra/1389725994094/1389726052482\" title=\"All\u00e9gations biologiques\">exigences d'\u00e9tiquetage pour les produits biologiques</a>.</p>\n\n<h2>Ententes d'\u00e9quivalence pour les produits aquacoles</h2>\n\n<p>Les <a href=\"#def\">produits aquacoles</a> biologiques ne sont pas couverts par toutes les <a href=\"/etiquetage-des-aliments/produits-biologiques/ententes-d-equivalence/fra/1311987562418/1311987760268\" title=\"Ententes d'\u00e9quivalence relatifs aux produits biologiques avec d'autres pays\">ententes d'\u00e9quivalence</a> des produits biologiques en vigueur entre le Canada et d'autres pays. </p>\n<p>\u00c0 partir du 15 janvier 2021, les exportateurs de produits d'aquaculture biologique de pays avec lesquels le Canada n'a pas d'entente d'\u00e9quivalence biologique pour les produits d'aquaculture doivent trouver des fournisseurs qui rencontrent les exigences canadiennes biologiques.</p>\n<p> L'accord sur l'\u00e9quivalence des produits biologiques entre le Canada et les \u00c9tats-Unis couvre le commerce des produits d'algues, incluant la chlorelle et spiruline biologiques certifi\u00e9es sous le r\u00e8glement biologique de l'USDA (<a href=\"https://gcc02.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.ecfr.gov%2Fcgi-bin%2FretrieveECFR%3Fgp%3D%26SID%3D78677a815fe58318e1eb547019f26a26%26mc%3Dtrue%26n%3Dpt7.3.205%26r%3DPART%26ty%3DHTML&amp;data=04%7C01%7C%7Cceaea08486b64798f08908d88caf0a83%7Ced5b36e701ee4ebc867ee03cfa0d4697%7C0%7C0%7C637414030702327092%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&amp;sdata=W4B2YOKl95zCFhdCgU3h0cBMPRl4Dy68EwlmLB1jgaQ%3D&amp;reserved=0\">7 CFR 205</a>) (en anglais seulement) ou des normes canadiennes sur la culture biologique du R\u00e9gime Bio-Canada (RBC) (Syst\u00e8mes de production biologique\u00a0: principes g\u00e9n\u00e9raux et normes de gestion, <a href=\"http://www.publications.gc.ca/site/fra/9.894378/publication.html\">CAN/CGSB-32.310</a>) (2020-12-21\u00a0\u2013\u00a0 <a href=\"/etiquetage-des-aliments/produits-biologiques/documents-d-orientation/accord-d-equivalence-biologique-entre-les-etats-un/fra/1608524164580/1608524165033\">Avis \u00e0 l'industrie biologique canadienne concernant le commerce de produits d'algues, chlorelle et spiruline sous l'accord d'\u00e9quivalence biologique entre les \u00c9tats-Unis et le Canada</a>).</p>\n<p>Historiquement, les exportations d'algues biologiques certifi\u00e9es \u00e0 la norme agricole ont \u00e9t\u00e9 accept\u00e9es par l'UE sous l'entente d'\u00e9quivalence existante entre le Canada et l'UE. L'ACIA permet aux algues biologiques commercialis\u00e9s entre le Canada et l'UE de continuer d'\u00eatre certifi\u00e9es selon la norme agricole (32.310) pour emp\u00eacher les perturbations dans les \u00e9changes commerciaux pendant que le travail se poursuit sur l'\u00e9quivalence avec l'UE. </p>\n<h3>D\u00e9finition</h3>\n\n<p id=\"def\">Les produits aquacoles ont la m\u00eame d\u00e9finition que dans la norme CAN/CGSB-32.312 de l'Office des normes g\u00e9n\u00e9rales du Canada\u00a0:</p>\n\n<p>Cultures et animaux d'\u00e9levage produits dans un milieu aquatique contr\u00f4l\u00e9 ou g\u00e9r\u00e9, ou un produit enti\u00e8rement ou partiellement issu \u00e0 partir de ceux-ci. Les produits issus de la p\u00eache d'animaux sauvages ne sont pas consid\u00e9r\u00e9s comme faisant partie de cette d\u00e9finition.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}