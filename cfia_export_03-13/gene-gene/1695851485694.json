{
    "dcr_id": "1695851485694",
    "title": {
        "en": "Potato wart facts and figures",
        "fr": "Faits et chiffres concernant la galle verruqueuse de la pomme de terre"
    },
    "html_modified": "2024-03-12 3:59:33 PM",
    "modified": "2023-12-04",
    "issued": "2023-12-04",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/potato_wart_pei_facts-figures_1695851485694_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/potato_wart_pei_facts-figures_1695851485694_fra"
    },
    "ia_id": "1695851486412",
    "parent_ia_id": "1644012033480",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1644012033480",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Potato wart facts and figures",
        "fr": "Faits et chiffres concernant la galle verruqueuse de la pomme de terre"
    },
    "label": {
        "en": "Potato wart facts and figures",
        "fr": "Faits et chiffres concernant la galle verruqueuse de la pomme de terre"
    },
    "templatetype": "content page 1 column",
    "node_id": "1695851486412",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1644011638795",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/potato-wart/potato-wart-in-pei/facts-and-figures/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/la-galle-verruqueuse-de-la-pomme-de-terre-a-ipe/faits-et-chiffres/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Potato wart facts and figures",
            "fr": "Faits et chiffres concernant la galle verruqueuse de la pomme de terre"
        },
        "description": {
            "en": "About potato wart in PEI, value of potatoes, about PEI potato exports to the U.S., next steps",
            "fr": "\u00c0 propos de la galle verruqueuse de la pomme de terre \u00e0 l'\u00ce. P. \u00c9.,  valeur des pommes de terre,  au sujet des exportations de pommes de terre de l'\u00ce.-P.-\u00c9. vers les \u00c9tats-Unis, prochaines \u00e9tapes"
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Synchytrium endobioticum, Potato Wart, Potato Canker, Prince Edward Island",
            "fr": "v\u00e9g\u00e9taux, phytoravageurs, enqu\u00eates phytosanitaires, phytoparasites justiciables de quarantaine, cartes, Galle (tumeur) verruqueuse de la pomme de terre, ile-du-Prince-Edouard"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-12-04",
            "fr": "2023-12-04"
        },
        "modified": {
            "en": "2023-12-04",
            "fr": "2023-12-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Potato wart facts and figures",
        "fr": "Faits et chiffres concernant la galle verruqueuse de la pomme de terre"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Potato wart is a soil-borne fungus, <a href=\"/plant-health/invasive-species/plant-diseases/potato-wart/eng/1327933703431/1327933793006\"><i lang=\"la\">Synchytrium endobioticum</i></a>, that can remain dormant in a field for more than 40 years.</p>\n<p>Although potato wart poses no threat to human health or food safety, it does have a financial impact on potato growers by reducing their yield and making potatoes unmarketable. While potato wart naturally spreads slowly within a field, it often spreads rapidly within and to other fields by the movement of soil in association with farm equipment, cultivation practices, movement and planting of seed potato tubers from infected fields.</p>\n<p>In Canada and many other countries, potato wart is a regulated pest. The Canadian Food Inspection Agency (CFIA) implemented control measures to help contain, control, and prevent the spread of potato wart both domestically and internationally.</p>\n<p>Plant pests, such as potato wart, are regulated under the <a href=\"/english/reg/jredirect2.shtml?plavega\"><i>Plant Protection Act</i></a> (PPA). When a regulated pest is detected a written notice is issued and outlines requirements under the <a href=\"/english/reg/jredirect2.shtml?plavegr\"><i>Plant Protection Regulations</i></a>. For example, it may trigger land-use restrictions, movement controls, requirements for equipment to be free from soil, soil sampling, and testing. The fungus is extremely persistent and the only efficient way to control the disease is to prevent the spread to new locations.</p>\n\t\n<h2>About potato wart in PEI</h2>\n<ul class=\"lst-spcd\">\n<li>Seed potatoes from infected fields are at the highest risk for spreading potato wart.</li>\n<li>When left unmanaged, potato wart can cause field losses between 50% and 100%.</li>\n<li>As of 2021, PEI has approximately 175 potato farms.</li>\n<li>Since 2000, potato wart has been found in 37 fields across all three counties of PEI.</li>\n<li>The CFIA began its most recent investigations after potato wart was detected in two separate fields on two separate farms in October 2021. This was the largest series of investigations since the fungus was first detected in PEI in 2000.\n<ul class=\"lst-spcd\">\n<li>The CFIA confirmed four detections: February 2022, July 2022, December 2022, and April 2023. Each detection was from a separate farm.</li>\n<li>The fields with detections in February 2022 and April 2023 last produced potatoes for processing in 2021, while the fields with detection from July 2022 and December 2022, last grew potatoes for processing in 2022. No off-island movement risk was identified during the trace-back and trace-forward activities of these detections.</li>\n<li>The field with the July 2022 detection had been planted with a potato wart resistant variety prior to the detection and the crop was harvested and processed with risk mitigation measures in place. The field with the December 2022 detection was harvested, stored, and processed with risk mitigation measures in place.</li>\n<li>In August 2023, the CFIA announced that it concluded its investigation into potato wart. By the end of the investigation,\u00a0 nearly 50,000 soil samples had been collected and analyzed concluding CFIA's recent investigations into potato wart.</li>\n</ul></li>\n<li>The Ministerial Order and the PEI seed potato domestic movement requirements remain in effect.</li>\n<li>The decision to implement the Ministerial Order was necessary to help further contain, control, and prevent the spread of potato wart in a manner that protects plant life and Canada's important agriculture sector.\n<ul class=\"lst-spcd\">\n<li>The Ministerial Order restricts the domestic movement of potatoes, soil, and other high-risk things (for example: root crops, nursery stock, sod, or bulbs) out of PEI.</li>\n</ul></li>\n<li>The end goal for managing infected fields is eradication. To accomplish this, stringent long term phytosanitary controls are applied.</li>\n</ul>\n\t\n<h2>Value of potatoes</h2>\n\t\n<ul class=\"lst-spcd\">\n<li>According to Statistics Canada, potatoes are the fifth largest primary agriculture crop in Canada (after wheat, canola, soybean, and corn). Potatoes contributed approximately $1.5 billion in income in 2021 and $2.6 billion in exports of potatoes and potato products in 2021/2022.</li>\n<li>In 2021/2022, Canada exported $37 million in seed potatoes, $421 million in fresh potatoes, and $1.9 billion worth of processed potato products to all countries.</li>\n<li>Potatoes make a significant contribution to the agricultural sector in some provinces; for example, in 2021, potatoes contributed over 44% of the total farm cash receipts in Prince Edward Island and 17% of the total farm cash receipts in New Brunswick.\n<ul class=\"lst-spcd\">\n<li>Manitoba and Alberta have higher total potato farm cash receipts than both Prince Edward Island and New Brunswick; however, potato farm cash receipts represent only 4% and 2%, respectively, of the total farm cash receipts for Manitoba and Alberta.</li>\n</ul></li>\n</ul>\n\t\n<h2>About PEI potato exports to the U.S.</h2>\n<ul class=\"lst-spcd\">\n<li>In 2021/2022, the U.S. export market represented 92% ($34 million) of seed potatoes, 97% ($403 million) of fresh potatoes, and 89% ($1.7 billion) of processed potato products.</li>\n<li>Currently, over 95% of PEI potatoes are eligible for movement to the rest of Canada as well as PEI's primary export market, the U.S.</li>\n<li>In November 2021, the United States Department of Agriculture's Animal and Plant Health Inspection Service (APHIS) decided to no longer accept potatoes from PEI into the U.S.\n<ul class=\"lst-spcd\">\n<li>Countries are responsible for setting their own import requirements.</li>\n<li>The CFIA may only certify export shipments that meet the import requirements of the jurisdiction receiving the product.</li>\n</ul></li>\n<li>U.S. imports of PEI potatoes for consumption resumed under certain conditions in April 2022, when APHIS introduced new requirements to reduce the risk of bringing in potato wart.</li>\n<li>The current U.S. Federal Order prohibits the import of seed potatoes originating from PEI. Currently, the U.S. does not permit the import of field-grown seed potatoes from PEI and requires all imported fresh PEI potatoes to be washed free from soil, sprout inhibited, and graded to a U.S. No. 1 standard.</li>\n<li>The U.S. would need to change or repeal their federal order to resume importing seed potatoes from PEI.</li>\n</ul>\n\t\n<h2>Next steps for PEI potatoes</h2>\n<p>While the CFIA investigations into the two 2021 potato wart detections on PEI are now complete and the <a href=\"/plant-health/invasive-species/domestic-plant-protection-measures/potato-wart-order/eng/1637345349606/1637345350810\">Ministerial Order</a> remains in effect, there is still more work to do. CFIA continues to engage with stakeholders on the next steps including:</p>\n<ul class=\"lst-spcd\">\n<li>Putting in place enhanced control measures, such as biosecurity and traceability activities.</li>\n<li>Additional surveillance of unrestricted fields in PEI that will help inform the development of long term activities designed to continue to mitigate risk and support the economic sustainability of the potato sector in PEI and across Canada.</li>\n</ul>\n\n<p>Learn more about <a href=\"/eng/1646687516726/1646687517101#a3a\">potato wart and the investigations in Prince Edward Island</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La galle verruqueuse de la pomme de terre est un champignon pr\u00e9sent dans le sol, le\u00a0<a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/fra/1327933703431/1327933793006\"><i lang=\"la\">synchytrium endobioticum</i></a>, qui peut rester \u00e0 l'\u00e9tat de dormance dans un champ pendant plus de 40 ans.</p>\n<p>M\u00eame si la galle verruqueuse ne pr\u00e9sente aucun danger pour la sant\u00e9 humaine ou la salubrit\u00e9 des aliments, elle a un impact financier sur les producteurs de pommes de terre en r\u00e9duisant les r\u00e9coltes et en rendant les pommes de terre invendables. Bien que la galle verruqueuse de la pomme de terre se propage lentement de mani\u00e8re naturelle dans un champ, elle se propage souvent rapidement \u00e0 l'int\u00e9rieur d'un champ et \u00e0 d'autres champs par le d\u00e9placement du sol en association avec l'\u00e9quipement agricole, les pratiques de culture, le d\u00e9placement et la plantation de tubercules de pommes de terre de semence provenant de champs infect\u00e9s.</p>\n<p>Au Canada et dans de nombreux autres pays, la galle verruqueuse de la pomme de terre est un organisme nuisible r\u00e9glement\u00e9. L'Agence canadienne d'inspection des aliments (ACIA) a mis en place des mesures de lutte pour aider \u00e0 confiner, \u00e0 contr\u00f4ler et \u00e0 pr\u00e9venir la propagation de la galle verruqueuse de la pomme de terre, tant \u00e0 l'\u00e9chelle nationale qu'internationale.</p>\n<p>Les organismes nuisibles aux v\u00e9g\u00e9taux, tels que la galle verruqueuse de la pomme de terre, sont r\u00e9gis par la <a href=\"/francais/reg/jredirect2.shtml?plavega\"><i>Loi sur la protection des v\u00e9g\u00e9taux</i></a> (LPV). Lorsqu'un organisme nuisible r\u00e9glement\u00e9 est d\u00e9tect\u00e9, un avis \u00e9crit est \u00e9mis et \u00e9nonce les exigences pr\u00e9vues par le <a href=\"/francais/reg/jredirect2.shtml?plavegr\"><i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i></a>. Par exemple, cela peut entra\u00eener des restrictions d'utilisation des terres, des contr\u00f4les des d\u00e9placements, des exigences concernant l'\u00e9quipement qui doit \u00eatre exempt de terre, l'\u00e9chantillonnage du sol et des tests. Le champignon est extr\u00eamement persistant et le seul moyen efficace de lutter contre la maladie est d'emp\u00eacher sa propagation \u00e0 de nouveaux endroits.</p>\n\t\n<h2>\u00c0 propos de la galle verruqueuse de la pomme de terre \u00e0 l'\u00ce. P. \u00c9.</h2>\n<ul class=\"lst-spcd\">\n<li>Les pommes de terre de semence provenant de champs infect\u00e9s pr\u00e9sentent le risque le plus \u00e9lev\u00e9 de propagation de la galle verruqueuse de la pomme de terre.</li>\n<li>Si elle n'est pas g\u00e9r\u00e9e, la galle verruqueuse de la pomme de terre peut entra\u00eener des pertes dans les champs allant de 50 % \u00e0 100 %.</li>\n<li>En date de 2021, l'\u00ce.-P.-\u00c9. comptait environ 175 exploitations de pommes de terre.</li>\n<li>Depuis 2000, la galle verruqueuse a \u00e9t\u00e9 d\u00e9tect\u00e9e dans 37 champs dans les trois comt\u00e9s de l'\u00cele-du-Prince-\u00c9douard.</li>\n<li>L'ACIA a commenc\u00e9 ses enqu\u00eates les plus r\u00e9centes \u00e0 la suite de la d\u00e9tection de la galle verruqueuse de la pomme de terre dans deux champs distincts de deux exploitations agricoles diff\u00e9rentes en octobre 2021. Il s'agit de la plus importante s\u00e9rie d'enqu\u00eates depuis que le champignon a \u00e9t\u00e9 d\u00e9tect\u00e9 pour la premi\u00e8re fois \u00e0 l'\u00ce.-P.-\u00c9. en 2000.\n<ul class=\"lst-spcd\">\n<li>L'ACIA a confirm\u00e9 quatre d\u00e9tections : en f\u00e9vrier 2022, en juillet 2022, en d\u00e9cembre 2022 et en avril 2023. Chaque d\u00e9tection provenait d'une ferme distincte.</li>\n<li>Les champs ayant fait l'objet de d\u00e9tections en f\u00e9vrier 2022 et avril 2023 ont produit pour la derni\u00e8re fois des pommes de terre destin\u00e9es \u00e0 la transformation en 2021, tandis que les champs ayant fait l'objet de d\u00e9tections en juillet 2022 et d\u00e9cembre 2022 ont produit pour la derni\u00e8re fois des pommes de terre destin\u00e9es \u00e0 la transformation en 2022. Aucun risque de d\u00e9placement hors de l'\u00eele n'a \u00e9t\u00e9 identifi\u00e9 au cours des activit\u00e9s de tra\u00e7age en amont et en aval de ces d\u00e9tections.</li>\n<li>Le champ o\u00f9 a \u00e9t\u00e9 d\u00e9tect\u00e9e la galle verruqueuse de la pomme de terre en juillet 2022 avait \u00e9t\u00e9 plant\u00e9 avec une vari\u00e9t\u00e9 r\u00e9sistante \u00e0 la galle verruqueuse avant la d\u00e9tection, et la culture a \u00e9t\u00e9 r\u00e9colt\u00e9e et transform\u00e9e avec des mesures d'att\u00e9nuation des risques en place. Le champ ayant fait l'objet de la d\u00e9tection de d\u00e9cembre 2022 a \u00e9t\u00e9 r\u00e9colt\u00e9, stock\u00e9 et transform\u00e9 avec des mesures d'att\u00e9nuation des risques en place.</li>\n<li>En ao\u00fbt 2023, l'ACIA a annonc\u00e9 qu'elle avait conclu son enqu\u00eate sur la galle verruqueuse de la pomme de terre. \u00c0 la fin de l'enqu\u00eate, pr\u00e8s de 50 000 \u00e9chantillons de sol avaient \u00e9t\u00e9 pr\u00e9lev\u00e9s et analys\u00e9s, concluant les r\u00e9centes enqu\u00eates de l'ACIA sur la galle verruqueuse de la pomme de terre.</li>\n</ul></li>\n<li>L'arr\u00eat\u00e9 minist\u00e9riel et les exigences relatives au transport domestique des pommes de terre de semence de l'\u00cele-du-Prince-\u00c9douard demeurent en vigueur.</li>\n<li>La d\u00e9cision de faire appliquer l'arr\u00eat\u00e9 minist\u00e9riel \u00e9tait rendue n\u00e9cessaire pour aider \u00e0 mieux confiner, contr\u00f4ler et pr\u00e9venir la propagation de la galle verruqueuse de la pomme de terre de fa\u00e7on \u00e0 prot\u00e9ger les v\u00e9g\u00e9taux et l'important secteur agricole du Canada.\n<ul class=\"lst-spcd\">\n<li>L'arr\u00eat\u00e9 minist\u00e9riel limite le d\u00e9placement domestique des pommes de terre, du sol et d'autres choses \u00e0 risque \u00e9lev\u00e9 (par exemple\u00a0: les l\u00e9gumes racines, le mat\u00e9riel de p\u00e9pini\u00e8re, le gazon en plaques ou les bulbes) \u00e0 l'ext\u00e9rieur de l'\u00ce.-P.-\u00c9.</li>\n</ul></li>\n<li>L'objectif final de la gestion des champs infect\u00e9s est l'\u00e9radication. Pour y parvenir, des contr\u00f4les phytosanitaires rigoureux \u00e0 long terme sont appliqu\u00e9s.</li>\n</ul>\n\t\n<h2>Valeur des pommes de terre</h2>\n\t\n<ul class=\"lst-spcd\">\n<li>Selon Statistique Canada, les pommes de terre sont la cinqui\u00e8me plus importante culture agricole primaire au Canada (apr\u00e8s le bl\u00e9, le canola, le soya et le ma\u00efs). Les pommes de terre ont produit environ 1,5 milliard de dollars en recettes agricoles en 2021 et 2,6 milliards de dollars en exportations de pommes de terre et de produits de la pomme de terre en 2021-2022.</li>\n<li>En 2021/2022, le Canada a export\u00e9 vers tous les pays 37 millions de dollars de pommes de terre de semence, 421 millions de dollars de pommes de terre fra\u00eeches et 1,9 milliard de dollars de produits transform\u00e9s \u00e0 base de pommes de terre.</li>\n<li>Les pommes de terre apportent une contribution importante au secteur agricole dans certaines provinces. Par exemple, en 2021, la pomme de terre a contribu\u00e9 \u00e0 plus de 44\u00a0% des recettes agricoles totales de l'\u00cele-du-Prince-\u00c9douard et \u00e0 17 % des recettes agricoles totales du Nouveau-Brunswick.\n<ul class=\"lst-spcd\">\n<li>Le Manitoba et l'Alberta ont des recettes agricoles totales tir\u00e9es de la pomme de terre plus \u00e9lev\u00e9es que l'\u00cele-du-Prince-\u00c9douard et le Nouveau-Brunswick. Toutefois, les recettes agricoles tir\u00e9es des pommes de terre ne repr\u00e9sentent que 4 % et 2 %, respectivement, du total des recettes agricoles du Manitoba et de l'Alberta.</li>\n</ul></li>\n</ul>\n\t\n<h2>Au sujet des exportations de pommes de terre de l'\u00ce.-P.-\u00c9. vers les \u00c9tats-Unis</h2>\n<ul class=\"lst-spcd\">\n<li>En 2021/2022, le march\u00e9 d'exportation am\u00e9ricain repr\u00e9sentait 92 % (34 millions de dollars) des pommes de terre de semence, 97 % (403 millions de dollars) des pommes de terre fra\u00eeches et 89 % (1,7 milliard de dollars) des produits transform\u00e9s \u00e0 base de pommes de terre.</li>\n<li>\u00c0 l'heure actuelle, plus de 95 % des pommes de terre de l'\u00ce.-P.-\u00c9. peuvent \u00eatre achemin\u00e9es vers le reste du Canada ainsi que vers le principal march\u00e9 d'exportation de l'\u00ce.-P.-\u00c9., \u00e0 savoir les \u00c9tats-Unis.</li>\n<li>En novembre 2021, le Service d'inspection de la sant\u00e9 animale et v\u00e9g\u00e9tale (<span lang=\"en\">Animal and Plant Health Inspection Service-APHIS</span>) du d\u00e9partement am\u00e9ricain de l'Agriculture a d\u00e9cid\u00e9 de ne plus accepter les pommes de terre de l'\u00ce.-P.-\u00c9. aux \u00c9tats-Unis.\n<ul class=\"lst-spcd\">\n<li>La responsabilit\u00e9 d'\u00e9tablir des exigences d'importation incombe \u00e0 chaque pays.</li>\n<li>L'ACIA peut seulement certifier les envois destin\u00e9s \u00e0 l'exportation qui satisfont aux exigences d'importation du pays qui re\u00e7oit le produit.</li>\n</ul></li>\n<li>Les importations am\u00e9ricaines de pommes de terre de l'\u00cele-du-Prince-\u00c9douard destin\u00e9es \u00e0 la consommation ont repris sous certaines conditions en avril 2022, lorsque l'APHIS a adopt\u00e9 de nouvelles exigences visant \u00e0 r\u00e9duire le risque d'introduction de la galle verruqueuse de la pomme de terre.</li>\n<li>Le d\u00e9cret f\u00e9d\u00e9ral am\u00e9ricain actuel interdit l'importation de pommes de terre de semence originaires de l'\u00ce.-P.-\u00c9. Actuellement, les \u00c9tats-Unis n'autorisent pas l'importation de pommes de terre de semence cultiv\u00e9es en plein champ en provenance de l'\u00ce.-P.-\u00c9. et exigent que toutes les pommes de terre fra\u00eeches import\u00e9es de l'\u00ce.-P.-\u00c9. soient lav\u00e9es et exemptes de terre, qu'elles soient prot\u00e9g\u00e9es contre la germination et qu'elles soient class\u00e9es selon la norme am\u00e9ricaine no 1.</li>\n<li>Il faudrait que les \u00c9tats-Unis modifient ou abrogent leur d\u00e9cret f\u00e9d\u00e9ral afin de pouvoir reprendre l'importation de pommes de terre de semence de l'\u00ce.-P.-\u00c9.</li>\n</ul>\n\t\n<h2>Prochaines \u00e9tapes pour les pommes de terre de l'\u00ce.-P.-\u00c9.</h2>\n<p>Bien que les enqu\u00eates de l'ACIA sur les deux d\u00e9tections de la galle verruqueuse de la pomme de terre en 2021 \u00e0 l'\u00ce.-P.-\u00c9. soient maintenant termin\u00e9es et que <a href=\"/protection-des-vegetaux/especes-envahissantes/mesures-de-protection-des-vegetaux-en-territoire-c/arrete-relatif-a-la-galle-verruqueuse-de-la-pomme-/fra/1637345349606/1637345350810\">l'arr\u00eat\u00e9 minist\u00e9riel</a> soit toujours en vigueur, il reste encore du travail \u00e0 faire. L'ACIA continue de travailler avec les intervenants sur les prochaines \u00e9tapes, y compris\u00a0:</p>\n<ul class=\"lst-spcd\">\n<li>Mettre en place des mesures de contr\u00f4le renforc\u00e9es, telles que des activit\u00e9s de bios\u00e9curit\u00e9 et de tra\u00e7abilit\u00e9.</li>\n<li>Renforcer la surveillance des champs non r\u00e9glement\u00e9s en \u00ce.-P.-\u00c9. afin de contribuer \u00e0 l'\u00e9laboration d'activit\u00e9s \u00e0 long terme destin\u00e9es \u00e0 continuer \u00e0 att\u00e9nuer les risques et \u00e0 soutenir la viabilit\u00e9 \u00e9conomique du secteur de la pomme de terre en \u00ce.-P.-\u00c9. et dans l'ensemble du Canada.</li>\n</ul>\n\n<p>Pour en savoir plus sur la <a href=\"/fra/1646687516726/1646687517101#a3a\">galle verruqueuse de la pomme de terre et les enqu\u00eates \u00e0 l'\u00cele-du-Prince-\u00c9douard</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}