{
    "dcr_id": "1647610174277",
    "title": {
        "en": "Imported seed advisory for non-authorized importers",
        "fr": "Avis sur les semences import\u00e9es pour les importateurs non autoris\u00e9s"
    },
    "html_modified": "2024-01-26 2:25:49 PM",
    "modified": "2023-04-24",
    "issued": "2022-03-21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/imprtd_seed_advsry_non_auth_imprtrs_1647610174277_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/imprtd_seed_advsry_non_auth_imprtrs_1647610174277_fra"
    },
    "ia_id": "1647610174964",
    "parent_ia_id": "1299174773794",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299174773794",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Imported seed advisory for non-authorized importers",
        "fr": "Avis sur les semences import\u00e9es pour les importateurs non autoris\u00e9s"
    },
    "label": {
        "en": "Imported seed advisory for non-authorized importers",
        "fr": "Avis sur les semences import\u00e9es pour les importateurs non autoris\u00e9s"
    },
    "templatetype": "content page 1 column",
    "node_id": "1647610174964",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299174165725",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-imports/imported-seed-advisory/",
        "fr": "/protection-des-vegetaux/semences/importation-de-semences/avis-sur-les-semences-importees/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Imported seed advisory for non-authorized importers",
            "fr": "Avis sur les semences import\u00e9es pour les importateurs non autoris\u00e9s"
        },
        "description": {
            "en": "Requirements of the Plant Protection Act and regulations and other acts and regulations are not addressed here that may apply to your import. A list of exemptions from the Seeds Regulations can be found in the ABC's of seed Importation into Canada (small seed lots, lots for conditioning or research).",
            "fr": "Les exigences de la Loi et du R\u00e8glement sur la protection des v\u00e9g\u00e9taux et des autres lois et r\u00e8glements qui peuvent s'appliquer \u00e0 votre importation ne sont pas abord\u00e9es ici. Une liste des exemptions du R\u00e8glement sur les semences se trouve dans la Tout sur l'importation de semences au Canada (petits lots de semences, lots pour le conditionnement ou la recherche)."
        },
        "keywords": {
            "en": "seeds, Plant Protection Act, Plant Protection Regulations, Seeds Regulations, imported seed advisory, non-authorized importers, import",
            "fr": "semences, Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur les semences, avis sur les semences import\u00e9es, importateurs non autoris\u00e9s, importation"
        },
        "dcterms.subject": {
            "en": "imports,inspection,plants,weeds,policy,regulation",
            "fr": "importation,inspection,plante,plante nuisible,politique,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-03-21",
            "fr": "2022-03-21"
        },
        "modified": {
            "en": "2023-04-24",
            "fr": "2023-04-24"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Imported seed advisory for non-authorized importers",
        "fr": "Avis sur les semences import\u00e9es pour les importateurs non autoris\u00e9s"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>It is important to follow the requirements for importing seed under the <i>Seeds Regulations</i>. The information provided applies to importers who are <strong>not authorized</strong> under Part IV of the <i>Seeds Regulations</i>.</p>\n\n<section class=\"alert alert-warning\">\n<h2>Requirement to keep foreign seed separate and intact</h2>\n\n<p>The <i>Seeds Regulations</i> state:<br>\n<strong>must be kept separate and intact in the original package(s) until a notice of import conformity has been issued by the Canadian Food Inspection Agency</strong>, indicating that the seed meets the requirements of the <i>Seeds Regulations</i>.\"</p>\n\n<p>Using the seed before CFIA approval is a violation of the <i>Seeds Regulations</i> and may result in enforcement, up to including prosecution.</p>\n</section>\n\n<p>Requirements of the <i>Plant Protection Act </i>and regulations and other acts and regulations are not addressed here that may apply to your import.</p>\n\n<h2>How to import seed into Canada</h2>\n\n<p><strong>Step 1.</strong> Refer to the <a href=\"/plant-health/seeds/seed-imports/abcs-of-seed-importation/eng/1347740952226/1347741389113\">ABC's of seed Importation into Canada</a> and the <a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">Automated Import Reference System</a> for requirements and harmonized system (HS) codes.</p>\n\n<p><strong>Step 2.</strong> Submit all required information at the time of import:</p>\n\n<ul>\n<li><a href=\"/about-cfia/find-a-form/eng/1328823628115/1328823702784?form=c4560\">Import Declaration form (CFIA/ACIA\u00a04560)</a></li>\n<li><a href=\"/about-cfia/find-a-form/form-cfia-acia-5272/eng/1467026113151/1467026113683\">Request for Documentation Review (CFIA/ACIA\u00a05272)</a></li>\n\n<li>Seed analysis certificate</li>\n<li>Notice of import conformity (for pre-clearance only)</li>\n</ul>\n\n<p><strong>Note:</strong> Refer to the <a href=\"/plant-health/seeds/seed-imports/abcs-of-seed-importation/eng/1347740952226/1347741389113#l\">ABC's of seed Importation into Canada</a> (Section L) for a list of exemptions from the required import documentation.</p>\n\n<p><strong>Step 3.</strong> The Canadian Border Services Agency assigns a transaction number. Submitted documentation is accounted for by the National Import Service Center (NISC).</p>\n\n<ul>\n<li>If all paperwork is submitted correctly, seed moves to location</li>\n</ul>\n\n<p><strong>Note:</strong> foreign seed has not yet been cleared for use in Canada and <strong>must</strong> remain separate and intact.</p>\n\n<p><strong>Step 4:</strong> The CFIA's Import Conformity Assessment (ICA) office assesses documentation to confirm seed conformity. Seed must meet minimum import requirements for purity and germination. Information required for assessment:</p>\n\n<ul>\n<li>The name of the crop kind or species of seed being imported (importers are strongly encouraged to provide both the common name and the scientific name of the species or crop kind being imported)</li>\n<li>The weight of seed being imported</li>\n<li>The lot designation of the seed which must be included on the seed analysis certificate (SAC) and import declaration form</li>\n<li>The name and address of the exporter</li>\n<li>The name, telephone number, address of the importer</li>\n<li>The importers' fax number or email address</li>\n<li>The variety name of seed for all kinds, species and varieties referred to in the <i>Seed Regulations</i> <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/C.R.C.,_c._1400/page-16.html#h-512255\">Schedule III</a>; other than common seed of forage species</li>\n<li>The country where the crop from which the seed is derived was grown</li>\n<li>The intended purpose of the imported seed (sale, own use-seeding by the importer), research, conditioning, sale pursuant to subsection 5(4) of the <i>Seeds Regulations</i>)</li>\n<li>Seed analysis certificate showing the seed complies with the standards set out in the <i>Seeds Regulations</i> (freedom from prohibited noxious weed seeds, minimum purity standards and acceptable germination percentages)</li>\n</ul>\n\n<p><strong>Step 5: </strong>The ICA office issues a letter of conformity to the importer.</p>\n<ul>\n<li>Notice of conformity clears the seed</li>\n<li>Notice of non-conformity means follow up action is required by the importer</li>\n</ul>\n\n<p><strong>Step 6:</strong> Seed is cleared and approved for end use in Canada.</p>\n\n<p>To <strong>avoid common errors</strong> that delay the clearance of imported seed or could prevent the use of seed entirely, ensure the following is completed:</p>\n\n<ul>\n<li>The unique lot designation appears on all import documents (such as the invoice, import declaration form, seed analysis certificate)</li>\n<li>The unique lot designation on the seed analysis certificate represents and corresponds to the lot designation of the seed being imported</li>\n<li>The information on the seed analysis certificate meets Canadian Standards (in other words, Canadian <i>Weed Seeds Order</i>, correct purity analysis weights)</li>\n<li>Imports of crop types listed in Schedule III of the <i>Seeds Regulations</i> are of a registered variety\n<ul>\n<li>Unregistered varieties cannot be imported except for limited purposes specified in the Regulations</li>\n</ul></li>\n<li>The end use of the seed is clearly stated (that is, sale, own use (seeding by the importer), research, conditioning, sale under section 5(4) of the <i>Seed Regulations</i>)</li>\n</ul>\n\n<p>For more details on what is an acceptable certificate of analysis and other seed import requirements, please refer to the <a href=\"/plant-health/seeds/seed-imports/abcs-of-seed-importation/eng/1347740952226/1347741389113\">ABC's of seed Importation into Canada</a>.</p>\n\n<h2>Contacts</h2>\n\n<p>General import inquiries should be sent to your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a>.</p>\n\n<p>For questions on the clearance of a specific shipment, contact the Import Conformity Assessment (ICA) office at <a href=\"mailto:ICA-VCI@inspection.gc.ca\" class=\"nowrap\">ICA-VCI@inspection.gc.ca</a>.</p>\n\n<p>For questions regarding this advisory, contact the Seed Section at <a href=\"mailto:SeedSemence@inspection.gc.ca\" class=\"nowrap\">SeedSemence@inspection.gc.ca</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Il est important de respecter les exigences relatives \u00e0 l'importation de semences au titre du <i>R\u00e8glement sur les semences</i>. Les renseignements fournis s'appliquent aux importateurs qui ne sont <strong>pas autoris\u00e9s</strong> en vertu de la partie IV du <i>R\u00e8glement sur les semences</i>.</p>\n\n<section class=\"alert alert-warning\">\n<h2>Obligation de conserver les semences \u00e9trang\u00e8res intactes et \u00e0 part</h2>\n\n<p>Le <i>R\u00e8glement sur les semences</i> stipule ce qui suit\u00a0:<br>\n<strong>doit \u00eatre conserv\u00e9e intacte et \u00e0 part dans les emballages originaux jusqu'\u00e0 ce que l'Agence d\u00e9livre l'avis de conformit\u00e9</strong>, qui atteste que la semence satisfait aux exigences du pr\u00e9sent r\u00e8glement.\u00a0\u00bb</p>\n\n<p>L'utilisation des semences avant l'approbation de l'Agence canadienne d'inspection des aliments (ACIA) constitue une violation du <i>R\u00e8glement sur les semences</i> et peut entra\u00eener l'application de la loi, y compris des poursuites.</p>\n</section>\n\n<p>Les exigences de la Loi et du <i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i> et des autres lois et r\u00e8glements qui peuvent s'appliquer \u00e0 votre importation ne sont pas abord\u00e9es ici.</p>\n\n<h2>Comment importer des semences au Canada</h2>\n\n<p><strong>\u00c9tape 1\u00a0:</strong> Pour conna\u00eetre les exigences et les codes Syst\u00e8me harmonis\u00e9 (SH), veuillez consulter le document <a href=\"/protection-des-vegetaux/semences/importation-de-semences/tout-sur-l-importation/fra/1347740952226/1347741389113\">Tout sur l'importation de semences au Canada</a> et le <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation</a>.</p>\n\n<p><strong>\u00c9tape 2\u00a0:</strong> Transmettre tous les renseignements requis au moment de l'importation\u00a0:</p>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/fra/1328823628115/1328823702784?form=c4560\">D\u00e9claration d'importation (ACIA/CFIA\u00a04560)</a></li>\n<li><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5272/fra/1467026113151/1467026113683\">Demande de r\u00e9vision de documents (ACIA/CFIA\u00a05272)</a></li>\n\n<li>Certificat d'analyse de semences</li>\n<li>Avis de conformit\u00e9 d'importation (pour le pr\u00e9d\u00e9douanement uniquement)</li>\n</ul>\n\n<p><strong>Remarque\u00a0:</strong> Consultez <a href=\"/protection-des-vegetaux/semences/importation-de-semences/tout-sur-l-importation/fra/1347740952226/1347741389113#l\">Tout sur l'importation de semences au Canada</a> (section L) pour obtenir une liste des exemptions des documents d'importation requis.</p>\n\n<p><strong>\u00c9tape 3\u00a0:</strong> L'Agence des services frontaliers du Canada assigne un num\u00e9ro de transaction. Les documents soumis sont comptabilis\u00e9s par le Centre de service national \u00e0 l'importation.</p>\n\n<ul>\n<li>Si tous les documents sont soumis correctement, les semences sont envoy\u00e9es vers l'emplacement</li>\n</ul>\n\n<p><strong>Remarque\u00a0:</strong> Les semences \u00e9trang\u00e8res n'ont pas encore \u00e9t\u00e9 d\u00e9douan\u00e9es pour utilisation au Canada et <strong>doivent </strong>\u00eatre conserv\u00e9es intactes et \u00e0 part.</p>\n\n<p><strong>\u00c9tape 4\u00a0:</strong> Le Bureau d'\u00e9valuation de la conformit\u00e9 des importations (ECI) de l'ACIA \u00e9value les documents pour confirmer si les semences sont conformes. Les semences doivent satisfaire aux exigences minimales de puret\u00e9 et de germination relatives \u00e0 l'importation. Information demand\u00e9e en vue de l'\u00e9valuation\u00a0:</p>\n\n<ul>\n<li>Le nom de sorte ou d'esp\u00e8ce de la semence import\u00e9e (les importateurs sont fortement encourag\u00e9s \u00e0 fournir \u00e0 la fois le nom commun et le nom scientifique de la sorte ou de l'esp\u00e8ce de la semence import\u00e9e)</li>\n<li>Le poids de la semence qui est import\u00e9e</li>\n<li>La d\u00e9signation du lot de la semence qui doit figurer sur le certificat d'analyse de semences (CAS) et la d\u00e9claration d'importation</li>\n<li>Le nom et l'adresse de l'exportateur</li>\n<li>Le nom, l'adresse et le num\u00e9ro de t\u00e9l\u00e9phone de l'importateur</li>\n<li>Le num\u00e9ro de t\u00e9l\u00e9copieur ou l'adresse \u00e9lectronique de l'importateur</li>\n<li>Le nom de vari\u00e9t\u00e9 de la semence pour tous les types, esp\u00e8ces et vari\u00e9t\u00e9s mentionn\u00e9s \u00e0 l'<a href=\"https://laws-lois.justice.gc.ca/fra/reglements/C.R.C.%2C_ch._1400/page-16.html\">Annexe III</a> du <i>R\u00e8glement sur les semences</i>; autres que les semences communes des esp\u00e8ces fourrag\u00e8res</li>\n<li>Le pays o\u00f9 a \u00e9t\u00e9 cultiv\u00e9e la r\u00e9colte de laquelle provient la semence</li>\n<li>L'utilisation pr\u00e9vue de la semence import\u00e9e [vente, utilisation personnelle, ensemencement par l'importateur, recherche, conditionnement, vente/revente conform\u00e9ment au paragraphe 5(4) du <i>R\u00e8glement sur les semences</i>]</li>\n<li>Le certificat d'analyse de semences montrant que la semence est conforme aux normes \u00e9tablies dans le <i>R\u00e8glement sur les semences</i> (absence de mauvaises herbes nuisibles interdites et pourcentages acceptables de germination)</li>\n</ul>\n\n<p><strong>\u00c9tape 5\u00a0:</strong> Le Bureau d'ECI envoie une lettre de conformit\u00e9 \u00e0 l'importateur.</p>\n\n<ul>\n<li>L'avis de conformit\u00e9 d\u00e9douane la semence</li>\n<li>L'avis de non\u2011conformit\u00e9 signifie que l'importateur doit prendre des mesures de suivi</li>\n</ul>\n\n<p><strong>\u00c9tape 6\u00a0:</strong> La semence est d\u00e9douan\u00e9e et approuv\u00e9e pour une utilisation finale au Canada.</p>\n\n<p><strong>Pour \u00e9viter les erreurs courantes</strong> qui retardent le d\u00e9douanement des semences import\u00e9es ou qui pourraient emp\u00eacher l'utilisation compl\u00e8te des semences, assurez\u2011vous de remplir ce qui suit\u00a0:</p>\n\n<ul>\n<li>La d\u00e9signation unique du lot figure sur tous les documents d'importation (c'est-\u00e0-dire, la facture, la d\u00e9claration d'importation, le certificat d'analyse de semences)</li>\n<li>La d\u00e9signation unique du lot sur le certificat d'analyse de semences correspond \u00e0 la d\u00e9signation du lot de la semence import\u00e9e</li>\n<li>L'information sur le certificat d'analyse de semences est conforme aux normes canadiennes (c'est-\u00e0-dire, <i>Arr\u00eat\u00e9 sur les graines de mauvaises herbes</i> du Canada, poids exacts de l'analyse de la puret\u00e9)</li>\n<li>Les importations de types de cultures figurant \u00e0 l'annexe III du <i>R\u00e8glement sur les semences</i> sont des vari\u00e9t\u00e9s enregistr\u00e9es.\n<ul>\n<li>Les vari\u00e9t\u00e9s non enregistr\u00e9es ne peuvent pas \u00eatre import\u00e9es, sauf \u00e0 des fins limit\u00e9es pr\u00e9cis\u00e9es dans le <i>R\u00e8glement</i></li>\n</ul></li>\n<li>L'utilisation finale de la semence est clairement \u00e9nonc\u00e9e [c'est-\u00e0-dire, vente, utilisation personnelle [ensemencement par l'importateur], recherche, conditionnement, vente/revente conform\u00e9ment au paragraphe 5(4) du <i>R\u00e8glement sur les semences</i>]</li>\n</ul>\n\n<p>Pour en savoir plus sur ce qui constitue un certificat d'analyse acceptable et d'autres exigences relatives \u00e0 l'importation de semences, veuillez consulter le document <a href=\"/protection-des-vegetaux/semences/importation-de-semences/tout-sur-l-importation/fra/1347740952226/1347741389113\">Tout sur l'importation de semences au Canada</a>.</p>\n\n<h2>Personnes\u2011ressources</h2>\n\n<p>Les demandes de renseignements g\u00e9n\u00e9raux sur les importations doivent \u00eatre envoy\u00e9es \u00e0 votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'ACIA</a>.</p>\n\n<p>Pour toute question concerna sur le d\u00e9douanement d'un envoi en particulier, communiquez avec le Bureau de la conformit\u00e9 des importations \u00e0 <a href=\"mailto:ICA-VCI@inspection.gc.ca\" class=\"nowrap\">ICA\u2011VCI@inspection.gc.ca</a>.</p>\n\n<p>Pour toute question concernant cette fiche d'information, veuillez communiquer avec la Section des semences \u00e0 <a href=\"mailto:SeedSemence@inspection.gc.ca\" class=\"nowrap\">SeedSemence@inspection.gc.ca</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}