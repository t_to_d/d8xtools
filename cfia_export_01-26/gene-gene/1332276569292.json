{
    "dcr_id": "1332276569292",
    "title": {
        "en": "Natural toxins in fresh fruit and vegetables",
        "fr": "Toxines naturelles dans les fruits et l\u00e9gumes frais"
    },
    "html_modified": "2024-01-26 2:21:05 PM",
    "modified": "2019-12-09",
    "issued": "2012-03-20 16:49:31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_specific_products_naturaltoxins_1332276569292_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_specific_products_naturaltoxins_1332276569292_fra"
    },
    "ia_id": "1332276685336",
    "parent_ia_id": "1363562475361",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1363562475361",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Natural toxins in fresh fruit and vegetables",
        "fr": "Toxines naturelles dans les fruits et l\u00e9gumes frais"
    },
    "label": {
        "en": "Natural toxins in fresh fruit and vegetables",
        "fr": "Toxines naturelles dans les fruits et l\u00e9gumes frais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1332276685336",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1363562389536",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/fruits-and-vegetables/natural-toxins/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/fruits-et-legumes/toxines-naturelles/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Natural toxins in fresh fruit and vegetables",
            "fr": "Toxines naturelles dans les fruits et l\u00e9gumes frais"
        },
        "description": {
            "en": "Fresh fruit and vegetables are an important part of a healthy diet, however several fruits and vegetables consumed in Canada contain small amounts of natural toxins.",
            "fr": "Les fruits et l\u00e9gumes frais font partie d'un r\u00e9gime alimentaire \u00e9quilibr\u00e9. Toutefois, plusieurs fruits et l\u00e9gumes consomm\u00e9s au Canada contiennent de petites quantit\u00e9s de toxines naturelles."
        },
        "keywords": {
            "en": "food, consumers, food safety, foodborne illness, illness, health, fresh, fruit, vegetables, wash, store, natural toxins",
            "fr": "aliments, consommateurs, salubrit&#233; des aliments, toxi-infection alimentaire, sant&#233;, frais, fruit, l&#233;gumes, nettoyage, toxines naturelles"
        },
        "dcterms.subject": {
            "en": "agri-food products,best practices,consumer protection,consumers,food safety,fruits,vegetables",
            "fr": "produit agro-alimentaire,meilleures pratiques,protection du consommateur,consommateur,salubrit\u00e9 des aliments,fruit,l\u00e9gume"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Communications et Affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-20 16:49:31",
            "fr": "2012-03-20 16:49:31"
        },
        "modified": {
            "en": "2019-12-09",
            "fr": "2019-12-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Natural toxins in fresh fruit and vegetables",
        "fr": "Toxines naturelles dans les fruits et l\u00e9gumes frais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Fresh fruit and vegetables are an important part of a healthy diet, however several fruits and vegetables consumed in Canada contain small amounts of natural toxins. These natural toxins help protect the plants and create resistance to diseases and certain types of insects. The public should be aware of the presence of natural toxins in these fruits and vegetables. The following safety tips can help reduce or avoid exposure to toxins, which could potentially have harmful effects on human health.</p>\n<h2>Fruit and vegetables that produce cyanide</h2>\n<h3>Stone fruits</h3>\n<p>The kernels within the pits of some stone fruits contain a natural toxin called cyanogenic glycoside. These fruits include apricots, cherries, peaches, pears, plums and prunes. The flesh of the fruit itself is not toxic. Normally, the presence of cyanogenic glycoside alone is not dangerous. However, when kernels are chewed cyanogenic glycoside can transform into hydrogen cyanide - which is poisonous to humans. The lethal dose of cyanide ranges from 0.5 to 3.0 <abbr title=\"milligram\">mg</abbr> per kilogram of body weight. This is why it is not recommended to eat the kernels inside the pits of stone fruits.</p>\n<p>Although it is not recommended, some people use ground or whole bitter apricot kernels to flavour foods, as a health food, or for medicinal purposes. <a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/cyanide-bitter-apricot-kernels.html\"> More information on bitter apricot kernels</a> is available from Health Canada.</p>\n<h3>Cassava root and bamboo shoots</h3>\n<p>Cyanogenic glycoside toxin is also found in the cassava root and fresh bamboo shoots, making it necessary for them to be cooked before canning or eating. Cassava is classified into two main types - sweet and bitter. Sweet cassava is defined as having a concentration of cyanide less than 50 <abbr title=\"milligram\">mg</abbr> per kilogram of fresh weight, while bitter cassava has a concentration greater than 50 <abbr title=\"milligram\">mg</abbr> per kilogram. The sweet cassava only requires cooking in order to reduce the cyanide content to non-toxic levels. However, the bitter cassava contains more toxins and should be prepared and cooked properly prior to consumption. Grating the root and prolonged soaking of the gratings in water will leach out the cyanide, reducing the levels of toxin. In addition to soaking, cooking will further detoxify the roots before consumption.</p>\n<p>Cyanogenic glycoside found in fresh bamboo decomposes quickly when placed in boiling water, rendering the bamboo shoots safe for consumption. It has been found that boiling bamboo shoots for 20 minutes at 98 <abbr title=\"Celsius\">C</abbr> removes nearly 70 percent of the cyanide, while higher temperatures and longer intervals remove up to 96 percent. The highest concentrations are detoxified by cooking for two hours.</p>\n<h2>Natural toxins found in ackee fruit</h2>\n<div>\n<p><img alt=\"ackee fruit\" class=\"img-responsive\" src=\"/DAM/DAM-food-aliments/STAGING/images-images/ctre_specific_products_NaturalToxinsjpg_1332277584678_eng.jpg\"></p>\n<div>Ackee, akee or achee - <span lang=\"la\">Blinghia sapida</span> - is a food staple in many Western Africa, Jamaican and Caribbean diets. There are two main varieties, hard and soft ackees, that are available for consumption. Both canned and fresh forms of this fruit are consumed. However, unripe fruit contains natural toxins called hypoglycin that can cause serious health effects. The only part of this fruit that is edible, is the properly harvested and prepared ripe golden flesh around the shiny black seeds. The fruit is poisonous unless ripe and after being opened naturally on the tree.</div>\n</div>\n<h2>Potatoes that can cause burning sensations</h2>\n<p>Several different glycoalkaloids are produced naturally by potatoes, the most common being solanine and chaconine. Low levels of glycoalkaloids produce desirable flavour in potatoes. However, exposure to elevated levels of glycoalkaloids when eating potatoes can cause a bitter taste or a burning sensation in the mouth - indicating a state of toxicity. Glycoalkaloids are not destroyed by cooking; even by frying in hot oil. The majority of this natural toxin found in potatoes is in the peel, or just below the peel. Greening of the potatoes may be indicative of the presence of the toxin. Red skinned or russet potatoes may camouflage the greening.</p>\n<p>Consumers should avoid eating potatoes that show signs of greening, physical damage, rotting or sprouting. Potatoes should be stored in a cool, dark, dry place at home, such as a basement, and away from the sun or artificial light. Wash potatoes before cooking and peel or cut away green areas prior to cooking. Potatoes with pronounced greening or damage should be discarded. If potatoes taste bitter or cause a burning sensation after cooking, do not consume them.</p>\n<h2>Poisoning from fiddleheads</h2>\n<p>There have been documented reports of poisoning from consuming raw or undercooked fiddleheads. Symptoms usually begin 30 minutes to 12 hours subsequent to consumption and may include diarrhea, nausea, vomiting, abdominal cramps and headaches. Illness generally lasts less than 24 hours. Studies to date have not determined the cause of these illnesses. More information on <a href=\"https://www.canada.ca/en/health-canada/services/food-safety-fruits-vegetables/fiddlehead-safety-tips.html\">Fiddleheads</a> is available from Health Canada.</p>\n<p>Fresh fiddleheads must be carefully washed in several changes of cold water. They should then be thoroughly cooked, either through steaming for 10 to 12 minutes - until tender - or in boiling water for at least 15 minutes. Water used for boiling or steaming fiddleheads should be discarded. Fiddleheads should also be boiled or steamed prior to sauteing, frying or baking.</p>\n<h2>Off-flavour in fresh carrots</h2>\n<p>Off-flavours such as a bitter taste, aftertaste and/or petroleum-like flavour have been associated with the consumption of fresh carrots. In contrast to sweet flavour, these off-flavours are usually as a result of stored carrots being exposed to ethylene. Ethylene is a normal fruit ripening hormone that may react with natural chemical compounds found in carrots creating off-flavour sensory attributes. Thus, carrots should not be stored with ethylene-producing commodities such as apples, avocados, bananas, pears, peaches, plums, cantaloupes, honeydew melons and tomatoes. Carrots properly handled and stored in perforated plastic bags at a low temperature retain the most acceptable taste.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les fruits et l\u00e9gumes frais font partie d'un r\u00e9gime alimentaire \u00e9quilibr\u00e9. Toutefois, plusieurs fruits et l\u00e9gumes consomm\u00e9s au Canada contiennent de petites quantit\u00e9s de toxines naturelles. Ces derni\u00e8res contribuent \u00e0 la protection des v\u00e9g\u00e9taux et cr\u00e9ent une r\u00e9sistance \u00e0 des maladies et \u00e0 certains types d'insectes. En outre, la population doit \u00eatre sensibilis\u00e9e \u00e0 la pr\u00e9sence de toxines naturelles dans ces fruits et l\u00e9gumes. Les conseils suivants peuvent aider \u00e0 r\u00e9duire ou \u00e0 \u00e9viter l'exposition \u00e0 des toxines qui risquent d'avoir des effets nuisibles sur la sant\u00e9 humaine.</p>\n<h2>Fruits et l\u00e9gumes qui produisent du cyanure</h2>\n<h3>Fruits \u00e0 noyaux</h3>\n<p>Les amandes contenues dans les noyaux de certains fruits renferment une toxine naturelle appel\u00e9e glycoside cyanog\u00e8ne. Les abricots, les cerises, les p\u00eaches, les prunes et les prunes \u00e0 pruneaux sont tous des fruits \u00e0 noyaux. La chair du fruit en soi n'est pas toxique. Normalement, la seule pr\u00e9sence de glycoside cyanog\u00e8ne n'est pas dangereuse. Toutefois, lorsque l'on m\u00e2che les amandes contenues dans le noyaux de ces fruits, le glycoside cyanog\u00e8ne peut se transformer en acide cyanhydrique, qui est toxique pour les humains. La dose l\u00e9tale de cyanure varie entre 0,5 et 3,0 <abbr title=\"milligramme\">mg</abbr> par kilogramme de poids corporel. C'est pour cette raison que l'on ne recommande pas de manger les amandes qui se trouvent \u00e0 l'int\u00e9rieur des noyaux de ces fruits.</p>\n<p>Bien qu'il ne soit pas recommand\u00e9 de le faire, certaines personnes ont recours aux amandes d'abricots am\u00e8res enti\u00e8res ou moulues pour aromatiser les aliments, en tant qu'aliment sant\u00e9 ou \u00e0 des fins m\u00e9dicinales. <a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/presence-cyanure-amandes-abricots-ameres.html\">Pour en savoir plus sur les amandes d'abricots am\u00e8res</a>, veuillez consulter le site Web de Sant\u00e9 Canada.</p>\n<h3>Racine de manioc et pousses de bambou</h3>\n<p>La toxine du glycoside cyanog\u00e8ne se trouve \u00e9galement dans la racine de manioc et les pousses fra\u00eeches de bambou, ce qui rend leur cuisson essentielle avant la mise en conserve ou la consommation. Le manioc comporte deux grands types : doux et amer. Le premier poss\u00e8de une teneur en cyanure inf\u00e9rieure \u00e0 50 <abbr title=\"milligramme\">mg</abbr> par kilogramme de poids frais, tandis que celle du manioc amer est sup\u00e9rieure \u00e0 50\u00a0<abbr title=\"milligramme\">mg</abbr> par kilogramme. En cuisant le manioc doux, on en r\u00e9duira la teneur en cyanure sous les niveaux jug\u00e9s non toxiques. Toutefois, le manioc amer contient plus de toxines et doit \u00eatre pr\u00e9par\u00e9 et cuit ad\u00e9quatement avant d'\u00eatre consomm\u00e9. En r\u00e2pant la racine et en trempant les r\u00e2pures dans l'eau pour une dur\u00e9e prolong\u00e9e, il y aura lessivage du cyanure et r\u00e9duction des teneurs en toxines. En plus du trempage, la cuisson servira \u00e0 d\u00e9toxifier davantage les racines avant leur consommation.</p>\n<p>Le glycoside cyanog\u00e8ne que l'on trouve dans le bambou frais se d\u00e9compose rapidement lorsque le bambou est plac\u00e9 dans l'eau bouillante, ce qui \u00e9limine les risques associ\u00e9s \u00e0 sa consommation. Les pousses de bambou qui sont mises \u00e0 bouillir pendant 20 minutes \u00e0 98\u00a0\u00b0<abbr title=\"Celsius\">C</abbr> perdent pr\u00e8s de 70 pour cent de leur teneur en cyanure, et peuvent en perdre jusqu'\u00e0 96 pour cent si l'on augmente la temp\u00e9rature et la dur\u00e9e de cuisson. Pour les teneurs \u00e9lev\u00e9es, la d\u00e9toxication maximale est atteinte apr\u00e8s deux heures de cuisson.</p>\n<h2>Toxines naturelles dans le fruit de l'ak\u00e9e</h2>\n<div>\n<p><img alt=\"le fruit de l'ak\u00e9e\" class=\"img-responsive\" src=\"/DAM/DAM-food-aliments/STAGING/images-images/ctre_specific_products_NaturalToxinsjpg_1332277584678_fra.jpg\"></p>\n<div>L'ak\u00e9e - Blinghia sapida est un produit de consommation courante pour de nombreux r\u00e9sidants de la Jama\u00efque, Cara\u00efbes et de l'ouest de l'Afrique. Il y a deux vari\u00e9t\u00e9s principales qui sont offertes \u00e0 la consommation : une ferme et une molle. Les deux vari\u00e9t\u00e9s sont consomm\u00e9s fra\u00eeches ou en conserve. Toutefois, les fruits immatures contiennent des toxines naturelles appel\u00e9es hypoglycine qui peuvent nuire gravement \u00e0 la sant\u00e9. La seule partie du fruit qui est comestible est l'arille charnue m\u00fbre de couleur jaun\u00e2tre qui entoure les graines. Le fruit est toujours toxique \u00e0 moins qu'il ne soit m\u00fbr et qu'il se soit ouvert naturellement sur l'arbre.</div>\n</div>\n<h2>Les pommes de terre peuvent causer une sensation de br\u00fblure</h2>\n<p>Plusieurs types diff\u00e9rents de glycoalcalo\u00efdes sont produits naturellement par les pommes de terre, les plus courants \u00e9tant la solanine et la chaconine. De faibles teneurs en glycoalcalo\u00efdes produisent une saveur d\u00e9sirable chez les pommes de terre. Toutefois, l'ingestion de teneurs \u00e9lev\u00e9es de ce compos\u00e9 peut causer un go\u00fbt amer ou une sensation de br\u00fblure dans la bouche - ce qui est un signe de sa toxicit\u00e9. Les glycoalcalo\u00efdes ne sont pas d\u00e9truits par la cuisson, m\u00eame par la friture dans de l'huile chaude. La plus grande partie de cette toxine naturelle est pr\u00e9sente dans la pelure ou juste sous cette derni\u00e8re dans les pommes de terre. Le verdissement des pommes de terre peut \u00eatre un signe indicateur de la pr\u00e9sence de la toxine. Les pommes de terre \u00e0 pelure rouge et Russet peuvent camoufler le verdissement.</p>\n<p>Les consommateurs devraient \u00e9viter de consommer des pommes de terre qui montrent des signes de verdissement, de dommage, de pourriture ou de germination. Les pommes de terre doivent \u00eatre entrepos\u00e9es dans un endroit frais, sombre et sec de la maison, comme le sous-sol, loin des rayons du soleil et de la lumi\u00e8re artificielle. Lavez les pommes de terre et pelez ou enlevez les parties vertes avant la cuisson. Jetez les pommes de terre qui montrent un verdissement avanc\u00e9 ou qui sont endommag\u00e9es. D\u00e9barrassez-vous des pommes de terre qui sont am\u00e8res ou causent une sensation de br\u00fblure apr\u00e8s la cuisson, ne les consommez pas.</p>\n<h2>Intoxication par des crosses de foug\u00e8res</h2>\n<p>On a signal\u00e9 des cas d'intoxication \u00e0 la suite de la consommation de crosses de foug\u00e8res crues ou insuffisamment cuites. Les sympt\u00f4mes se manifestent habituellement de 30 minutes \u00e0 12 heures apr\u00e8s la consommation et peuvent prendre la forme de diarrh\u00e9e, de naus\u00e9es, de vomissements, de douleurs abdominales et de maux de t\u00eate. Les sympt\u00f4mes se r\u00e9sorbent g\u00e9n\u00e9ralement en moins de 24 heures. Les \u00e9tudes r\u00e9alis\u00e9es jusqu'\u00e0 pr\u00e9sent n'ont pas d\u00e9termin\u00e9 la cause de ces maladies. Vous trouverez plus d'informations sur des <a href=\"https://www.canada.ca/fr/sante-canada/services/salubrite-legumes-et-fruits/conseils-salubrite-crosses-fougere.html\">crosses de foug\u00e8res</a> aupr\u00e8s de Sant\u00e9 Canada.</p>\n<p>Les crosses de foug\u00e8res fra\u00eeches doivent \u00eatre soigneusement lav\u00e9es \u00e0 plusieurs reprises dans un bol d'eau froide. Elles doivent ensuite \u00eatre bien cuites, soit \u00e0 la vapeur pendant 10 \u00e0 12 minutes - jusqu'\u00e0 ce qu'elles soient tendres - ou dans l'eau bouillante pendant au moins 15 minutes. L'eau utilis\u00e9e pour faire bouillir les crosses de foug\u00e8res ou les cuire \u00e0 la vapeur doit \u00eatre jet\u00e9e. Les crosses de foug\u00e8res doivent \u00e9galement \u00eatre cuites \u00e0 la vapeur ou \u00e0 l'eau bouillante avant de les faire sauter, de les frire ou de les cuire au four.</p>\n<h2>Saveur atypique chez des carottes fra\u00eeches</h2>\n<p>Des saveurs atypiques comme un go\u00fbt amer, un mauvais go\u00fbt persistant ou une saveur de p\u00e9trole ont \u00e9t\u00e9 associ\u00e9es \u00e0 la consommation de carottes fra\u00eeches. Contrairement \u00e0 la saveur douce, la saveur atypique des carottes est habituellement le r\u00e9sultat de l'exposition \u00e0 l'\u00e9thyl\u00e8ne de carottes entrepos\u00e9es. L'\u00e9thyl\u00e8ne est une hormone normale de maturation des fruits qui peut r\u00e9agir avec des substances chimiques naturelles pr\u00e9sentes dans les carottes, ce qui cr\u00e9e des caract\u00e9ristiques sensorielles atypiques. Par cons\u00e9quent, les carottes ne doivent pas \u00eatre entrepos\u00e9es avec des produits d\u00e9gageant de l'\u00e9thyl\u00e8ne comme les pommes, les avocats, les bananes, les poires, les p\u00eaches, les prunes, les cantaloups, les melons miel et les tomates. En manipulant ad\u00e9quatement les carottes et en les entreposant dans des sacs de plastique perfor\u00e9s \u00e0 de faibles temp\u00e9ratures, elles conserveront le meilleur go\u00fbt possible.</p>\n<p>Pour en savoir plus, n'h\u00e9sitez pas \u00e0 <a href=\"/fra/1297964599443/1297965645317\">nous contacter \u00e0 : www.inspection.gc.ca</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}