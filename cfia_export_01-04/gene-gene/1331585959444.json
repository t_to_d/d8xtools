{
    "dcr_id": "1331585959444",
    "title": {
        "en": "Pets and petting zoos",
        "fr": "Animaux de compagnie et zoos pour enfants"
    },
    "html_modified": "2012-03-12 16:59",
    "modified": "12-3-2012",
    "issued": "12-3-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/ctre_tip_zoo_1331585959444_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/ctre_tip_zoo_1331585959444_fra"
    },
    "parent_ia_id": "1331871695247",
    "ia_id": "1331586128326",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pets and petting zoos",
        "fr": "Animaux de compagnie et zoos pour enfants"
    },
    "label": {
        "en": "Pets and petting zoos",
        "fr": "Animaux de compagnie et zoos pour enfants"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331586128326",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1331871496701",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/food-handling/pets-and-petting-zoos/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/manipulation-des-aliments/animaux-de-compagnie-et-zoos-pour-enfants/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pets and petting zoos",
            "fr": "Animaux de compagnie et zoos pour enfants"
        },
        "description": {
            "en": "Au Canada, les animaux de compagnie font souvent partie de la famille. Pourtant, comme tous les autres animaux, ces derniers peuvent \u00eatre porteurs d'organismes dangereux.",
            "fr": "Au Canada, les animaux de compagnie font souvent partie de la famille. Pourtant, comme tous les autres animaux, ces derniers peuvent \u00eatre porteurs d'organismes dangereux."
        },
        "keywords": {
            "en": "conseils sur la salubrit\u00e9 des aliments, Centre des consommateurs,  toxi-infection alimentaire, sant&#233;, animaux familiers, zoo apprivois&#233;, maladie animale, zoonose, maladie",
            "fr": "conseils sur la salubrit\u00e9 des aliments, Centre des consommateurs,  toxi-infection alimentaire, sant&#233;, animaux familiers, zoo apprivois&#233;, maladie animale, zoonose, maladie"
        },
        "dcterms.subject": {
            "en": "food,consumers,inspection,food inspection,agri-food products,consumer protection,food safety",
            "fr": "aliment,consommateur,inspection,inspection des aliments,produit agro-alimentaire,protection du consommateur,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Affaires publiques",
            "fr": "Agence canadienne d'inspection des aliments,Gouvernement du Canada,Affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-12 16:59:21",
            "fr": "2012-03-12 16:59:21"
        },
        "modified": {
            "en": "2012-03-12",
            "fr": "2012-03-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pets and petting zoos",
        "fr": "Animaux de compagnie et zoos pour enfants"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>Pets are like family members in many Canadian households, but like all animals, they can carry dangerous organisms. You can come into contact with harmful bacteria, viruses or parasites when you play with and clean up after your pets, handle pet food, and when you visit a petting zoo.</p>\n<p>Diseases called \"zoonoses\" can be passed from animals to humans or from humans to animals. Some zoonoses are very dangerous to humans including:</p>\n<ul>\n<li>infection with <i><span lang=\"la\"><abbr title=\"escherichia\">E.</abbr> coli</span></i> O157:H7</li>\n<li>salmonellosis</li>\n<li>toxoplasmosis</li>\n<li>tuberculosis</li>\n<li>rabies</li>\n</ul>\n<p>These diseases can be especially dangerous for young children, the elderly, pregnant women and people with weakened immune systems. If left untreated, they can even be deadly.</p>\n\n<h2>How zoonoses are transmitted from animals to humans</h2>\n<p>Animals can transmit harmful organisms in several ways:</p>\n<ul>\n<li>scratches, bites and saliva</li>\n<li>feces and fecal dust</li>\n<li>coughs, sneezes or mucus</li>\n</ul>\n<p>You can also get sick from handling your pet's food and treats. This can happen when the food is contaminated with <span lang=\"la\"><i><abbr title=\"Escherichia\">E.</abbr> coli</i></span> or <span lang=\"la\"><i>Salmonella</i></span> bacteria.</p>\n<p>If you get these harmful organisms on your hands, you could spread them to your face, mouth, eyes or your food. If you have touched an animal, their toys, their food and treats or cleaned up after them, wash your hands right away before touching anything else!</p>\n<h2>Four ways to protect yourself and your family</h2>\n<h3>1. Keep your pet healthy</h3>\n<ul>\n<li>Take your pet to the veterinarian regularly and have it checked for diseases.</li>\n<li>Keep your pet clean and its claws trimmed (if applicable).</li>\n<li>Beware of what your pet eats: garbage, raw meat and water from the toilet bowl can be dangerous.</li>\n<li>Keep away from wild animals: you and your pet can catch diseases from wild animals and their feces. Don't forget that squirrels and other rodents can be dangerous too.</li>\n</ul>\n<h3>2. Watch out for animal waste</h3>\n<ul>\n<li>Use waterproof, disposable gloves and wear a mask when cleaning cages, litter boxes, animal pens and fish tanks.</li>\n<li>Clean cages, boxes and pens daily. Put feces in a plastic bag then in the garbage.</li>\n<li>Never use pet feces for fertilizer or compost: it can carry dangerous organisms!</li>\n<li>Cover your child's sandbox since cats like to use sandboxes as litter boxes.</li>\n</ul>\n<p>Pregnant women should not be exposed to cat litter boxes since these might be contaminated with parasites harmful to the fetus.</p>\n<h3>3. Keep clean</h3>\n<ul>\n<li>After touching your pet and its toys, water from its container or aquarium, or its treats or food, wash your hands well with soap and warm water for 20 seconds. Do this before touching your face and before handling food.</li>\n<li>Keep pets away from food preparation, storage and dining areas.</li>\n</ul>\n<p>Some reptile foods such as frozen or defrosted mice, rats and chicks can contain <span lang=\"la\"><i>Salmonella</i></span> and be a potential source of infection for both the reptile and its owners. Handwashing is very important to protect yourself.</p>\n<h3>4. Take care at petting zoos</h3>\n<ul>\n<li>Handwashing is your best defence! After you touch, always wash your hands!</li>\n</ul>\n<p>Reptiles (like turtles, lizards and snakes) and amphibians (like newts, frogs and toads) can shed <span lang=\"la\"><i>Salmonella</i></span> bacteria on their skin or in the water.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Au Canada, les animaux de compagnie font souvent partie de la famille. Pourtant, comme tous les autres animaux, ces derniers peuvent \u00eatre porteurs d'organismes dangereux. Vous pouvez entrer en contact avec des bact\u00e9ries, des virus ou des parasites nuisibles, lorsque vous jouez avec vos animaux de compagnie, ramassez leurs excr\u00e9ments ou manipulez leurs aliments, ou encore lorsque vous visitez un zoo pour enfants.</p>\n<p>Les maladies appel\u00e9es \u00ab zoonoses \u00bb peuvent se transmettre des animaux aux humains ou des humains aux animaux. Certaines zoonoses sont tr\u00e8s dangereuses pour les humains, notamment :</p>\n<ul>\n<li>l'infection \u00e0 la bact\u00e9rie <i><span lang=\"la\"><abbr title=\"escherichia\">E.</abbr> coli</span></i> O157:H7</li>\n<li>la salmonellose</li>\n<li>la toxoplasmose</li>\n<li>la tuberculose</li>\n<li>la rage</li>\n</ul>\n<p>Ces maladies sont particuli\u00e8rement dangereuses pour les jeunes enfants, les personnes \u00e2g\u00e9es, les femmes enceintes et les personnes dont le syst\u00e8me immunitaire est affaibli. Si ces maladies ne sont pas trait\u00e9es, elles peuvent s'av\u00e9rer mortelles.</p>\n\n<h2>Comment les zoonoses sont transmises d'animaux aux humains</h2>\n\n<p>Les animaux peuvent transmettre des organismes dangereux de diff\u00e9rentes fa\u00e7ons :</p>\n<ul>\n<li>par des \u00e9gratignures, des morsures ou la salive</li>\n<li>par leurs excr\u00e9ments</li>\n<li>par la toux, les \u00e9ternuements ou le mucus</li>\n</ul>\n<p>Vous pouvez aussi devenir malade si vous manipulez les aliments ou les g\u00e2teries de votre animal et que ceux-ci sont contamin\u00e9s par la bact\u00e9rie <span lang=\"la\"><i><abbr title=\"Escherichia\">E.</abbr> coli</i></span> ou <span lang=\"la\"><i>Salmonella</i></span>.</p>\n<p>Si vos mains entrent en contact avec ces organismes dangereux, vous pourriez alors les propager \u00e0 votre visage, votre bouche ou vos yeux ou encore contaminer vos aliments. Si vous avez touch\u00e9 \u00e0 un animal, ses jouets, ses aliments, ses g\u00e2teries, ou si vous avez ramass\u00e9 ses excr\u00e9ments, lavez-vous les mains avant de toucher \u00e0 quoi que ce soit!</p>\n<h2>Quatre fa\u00e7ons de prot\u00e9ger toute la famille</h2>\n<h3>1. Veillez \u00e0 la sant\u00e9 de votre animal de compagnie</h3>\n<ul>\n<li>Pr\u00e9voyez des visites de routine chez le v\u00e9t\u00e9rinaire afin de vous assurer que votre animal n'a pas de maladie.</li>\n<li>Gardez votre animal propre et, le cas \u00e9ch\u00e9ant, lui faire couper les griffes r\u00e9guli\u00e8rement.</li>\n<li>Surveillez ce que votre animal mange et boit, car les d\u00e9chets, la viande crue et l'eau de la cuvette de la toilette peuvent s'av\u00e9rer dangereux.</li>\n<li>Tenez-le \u00e0 distance des animaux sauvages : les membres de votre famille et votre animal de compagnie pourraient attraper des maladies au contact d'animaux sauvages et de leurs excr\u00e9ments. N'oubliez pas que les \u00e9cureuils et les autres rongeurs peuvent aussi \u00eatre dangereux.</li>\n</ul>\n<h3>2. Manipulation des excr\u00e9ments d'animaux</h3>\n<ul>\n<li>Portez des gants imperm\u00e9ables et jetables, ainsi qu'un masque, lorsque vous nettoyez les cages, les bacs \u00e0 liti\u00e8re, les enclos et les aquariums.</li>\n<li>Nettoyez les cages, les bacs \u00e0 liti\u00e8re et les enclos tous les jours. Placez les excr\u00e9ments dans un sac de plastique et jetez-le \u00e0 la poubelle.</li>\n<li>N'utilisez jamais les excr\u00e9ments d'animaux comme engrais ou dans le compost, car ils peuvent contenir des organismes dangereux!</li>\n<li>Couvrez le bac \u00e0 sable de votre enfant, car les chats aiment s'en servir comme liti\u00e8re.</li>\n</ul>\n<p>Les femmes enceintes ne devraient pas \u00eatre expos\u00e9es au contenu des bacs \u00e0 liti\u00e8re, puisqu'ils pourraient contenir des parasites dangereux pour le foetus.</p>\n<h3>3. Veillez \u00e0 la propret\u00e9</h3>\n<ul>\n<li>Apr\u00e8s avoir touch\u00e9 \u00e0 votre animal de compagnie, \u00e0 l'eau dans son bol ou aquarium, ou \u00e0 ses jouets, g\u00e2teries ou aliments, lavez-vous les mains avec de l'eau chaude et du savon pendant 20 secondes. Il ne faut pas vous toucher le visage ou manipuler de la nourriture avant de vous \u00eatre lav\u00e9 les mains.</li>\n<li>Gardez les animaux de compagnie \u00e0 distance des surfaces de travail de la cuisine, des armoires o\u00f9 sont rang\u00e9s les aliments et des aires de repas.</li>\n</ul>\n<p>Certains aliments pour reptiles comme les souris, les rats et les poussins congel\u00e9s ou d\u00e9gel\u00e9s peuvent contenir la bact\u00e9rie <span lang=\"la\"><i>Salmonella</i></span> et \u00eatre une source d'infection possible pour le reptile et son propri\u00e9taire. Il est tr\u00e8s important de vous laver les mains afin de vous prot\u00e9ger.</p>\n<h3>4. Soyez vigilant au zoo pour enfants</h3>\n<ul>\n<li>Vous laver les mains est votre meilleur moyen de pr\u00e9vention! Apr\u00e8s avoir touch\u00e9 \u00e0 un animal ou \u00e0 quoi que ce soit, lavez-vous toujours les mains!</li>\n</ul>\n<p>Les reptiles (tortues, l\u00e9zards et serpents) et les amphibiens (tritons, grenouilles et crapauds) peuvent \u00eatre porteurs de bact\u00e9ries du genre Salmonella ou de l'eau peut en contenir.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}