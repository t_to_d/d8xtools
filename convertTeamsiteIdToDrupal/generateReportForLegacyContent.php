<?php
$myargs = array_slice($argv, 1);
//$export_root;
$export_root = array_shift($myargs);
echo $export_root . "\n";

$count = 0;
$fileCount = 0;
$successCount = 0;
$failCount = 0;
$totalSize = 0;
$fp = fopen('data1.csv', 'w');
$headers = array('File Name', 'Extension', 'Full Path', 'Size (KB)', 'Title 1', 'URL 1', 'Title 2', 'URL 2', 'Title 3', 'URL 3');
fputcsv($fp, $headers);

foreach (readFileSubDir($export_root) as $entry) {
    $jfile = $entry;
    $fileSizeBytes = filesize($jfile);
    echo "file:" . $jfile . ", size: " . $fileSizeBytes . "\n";
    $path_parts = pathinfo($jfile);
    //$fileArray[] = $path_parts['dirname'];
    $fileArray = (array) null;
    $fileArray[] = $path_parts['basename'];
    $fileArray[] = $path_parts['extension'];
    // $fileArray[] = $path_parts['filename'];
    $fileArray[] = str_replace("/app/proto/html/sites/default/files/legacy/","",$jfile); // fullpath
    $fileArray[] = $fileSizeBytes / 1024; //kb
    $contentfilename = $path_parts['basename'];
    $linuxcmd = 'grep -H -r "' . rawurlencode($contentfilename) . '" /app/d8tools/export/ | cut -d: -f1';
    $output = shell_exec($linuxcmd);
    //$output = shell_exec('grep -H -r \"' . $contentfilename .'\" /app/d8tools/export/ | cut -d: -f1');
    $array = explode("\n", $output);
    print_r($array);

    foreach($array as $jsonfile){
      if(empty($jsonfile)){
        continue;
      }
      $data = json_decode(file_get_contents($jsonfile));
      if (strpos($data->body->en, $contentfilename) !== false) {
        $fileArray[]=$data->meta->title->en;
        $fileArray[]=path_to_teamsite_content($data, 'en');

      }
      if(strpos($data->body->fr, $contentfilename) !== false) {
        $fileArray[]=$data->meta->title->fr;
        $fileArray[]=path_to_teamsite_content($data, 'fr');

      }



    }

    fputcsv($fp, $fileArray);


    $count++;
    $totalSize = $totalSize + $fileSizeBytes;
}

fclose($fp);
echo "count:" . $count . "\n";
$fileSizeMB = round($totalSize / 1024 / 1024);
echo "fileSize MB:" . $fileSizeMB . "\n";

function readFileSubDir($scanDir) {

	$handle = opendir($scanDir);

	while (($fileItem = readdir($handle)) !== false) {
		// skip '.' and '..'
		if (($fileItem == '.') || ($fileItem == '..')) continue;
		$fileItem = rtrim($scanDir,'/') . '/' . $fileItem;

		// if dir found call again recursively
		if (is_dir($fileItem)) {
			foreach (readFileSubDir($fileItem) as $childFileItem) {
				yield $childFileItem;
			}

		} else {
			yield $fileItem;
		}
	}

	closedir($handle);
}


function path_to_teamsite_content($data, $langcode) {
  if (!isset($data->dcr_id) || empty($data->dcr_id)) {
    return ' empty dcr_id ?';
  }
  if ($langcode == 'en') {
    //return 'https://agriculture-qa.9pro.ca/en/branches-and-offices/corporate-management-branch';
    return 'https://intranet.agr.gc.ca/agrisource/eng?id=' . $data->dcr_id;
  } else {
    //return 'https://agriculture-qa.9pro.ca/fr/directions-generales-et-bureaux/direction-generale-de-la-gestion-integree';
    return 'https://intranet.agr.gc.ca/agrisource/fra?id=' . $data->dcr_id;
  }
}
?>
