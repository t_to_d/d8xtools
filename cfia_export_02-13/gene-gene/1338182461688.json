{
    "dcr_id": "1338182461688",
    "title": {
        "en": "Stewardship of plants with novel traits",
        "fr": "La gestion des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux"
    },
    "html_modified": "2024-02-13 9:47:35 AM",
    "modified": "2014-03-06",
    "issued": "2012-05-28 01:21:04",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pnts_public_factsheets_stewardship_1338182461688_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pnts_public_factsheets_stewardship_1338182461688_fra"
    },
    "ia_id": "1338183836785",
    "parent_ia_id": "1337384231869",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1337384231869",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Stewardship of plants with novel traits",
        "fr": "La gestion des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux"
    },
    "label": {
        "en": "Stewardship of plants with novel traits",
        "fr": "La gestion des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1338183836785",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1337380923340",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plants-with-novel-traits/general-public/stewardship/",
        "fr": "/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/gestion/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Stewardship of plants with novel traits",
            "fr": "La gestion des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux"
        },
        "description": {
            "en": "Two examples of stewardship plans that product developers are asked to submit to the CFIA as part of the safety assessment are; stewardship plans to prevent the development of insects resistant to insect-resistant crop plants, and stewardship plans to prevent the development of herbicide resistant weeds, control herbicide tolerant volunteers and reduce the occurrence of volunteers with resistance to multiple herbicides.",
            "fr": "Voici deux exemples de plans de gestion que les cr\u00e9ateurs de produits sont tenus de soumettre \u00e0 l'ACIA dans le cadre de l'\u00e9valuation de l'innocuit\u00e9 : un plan de gestion visant \u00e0 pr\u00e9venir l'apparition d'insectes qui r\u00e9sistent \u00e0 des cultures r\u00e9sistantes aux insectes, et un plan de gestion visant \u00e0 pr\u00e9venir l'apparition de mauvaises herbes r\u00e9sistantes aux herbicides, \u00e0 lutter contre les plantes spontan\u00e9es tol\u00e9rantes aux herbicides et \u00e0 r\u00e9duire l'apparition de plantes spontan\u00e9es r\u00e9sistantes \u00e0 plusieurs herbicides."
        },
        "keywords": {
            "en": "Bt protein, stewardship plans, certain insect pests, novel trait, multiple herbicides",
            "fr": "caract\u00e8res nouveaux, exploitants agricoles, plan de gestion, plans de gestion, mauvaises herbes"
        },
        "dcterms.subject": {
            "en": "inspection",
            "fr": "inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-28 01:21:04",
            "fr": "2012-05-28 01:21:04"
        },
        "modified": {
            "en": "2014-03-06",
            "fr": "2014-03-06"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Stewardship of plants with novel traits",
        "fr": "La gestion des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is a plant with novel trait (PNT)?</h2>\n<p>The Canadian Food Inspection Agency (CFIA) defines a plant with a novel trait (PNT) as a new variety of a species that has one or more traits that are novel to that species in Canada. A <strong>trait is considered to be novel</strong> when the trait is:</p>\n<ul>\n<li>new to the plant species in Canada, and</li>\n<li>has the potential to have an impact on environmental safety.</li>\n</ul>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> assesses the safety of plants with novel traits in Canada. This assessment must take place no matter what process has been used to introduce the novel trait, whether it be genetic engineering, mutagenesis, conventional breeding, or any other method. This focus on plants with novel traits, instead of selected methods of production, means the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> may assess the safety of a broader array of products than do other countries.</p>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/general-public/assessment-process/eng/1338189630096/1338189929476\">Safety Assessment Process for Novel Foods and Agricultural Products of Biotechnology</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/general-public/novelty/eng/1338181110010/1338181243773\">\"Novelty\" and Plants with Novel Traits</a>.</li>\n</ul>\n<h2 class=\"h5\">What does \"stewardship\" mean?</h2>\n<p>Stewardship is the careful and responsible management of something under one's care. This can include a product or the use of a technology.</p>\n<p>The Canadian Food Inspection Agency (CFIA) applies this concept to <abbr title=\"plants with novel traits\">PNTs</abbr>. In the case of <abbr title=\"plants with novel traits\">PNTs</abbr>, there is a need to pay attention to the entire life-cycle of these products \u2014 including the part that follows product assessment and approval for environmental release.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> defines a stewardship plan as a document that outlines how the product or technology will be managed, or under what conditions it will be used. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> requires stewardship plans for some types of <abbr title=\"plants with novel traits\">PNTs</abbr>.</p>\n<h2 class=\"h5\">For what <abbr title=\"plants with novel traits\">PNTs</abbr> are stewardship plans submitted as part of the safety assessment?</h2>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> requires developers to address the post-approval part of a product's life-cycle. Product developers may do this through stewardship plans. Product developers must support the implementation of these plans and farmers have to comply with the requirements under these plans. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> aids in the development, use, and monitoring of the stewardship plans.</p>\n<p>Two examples of stewardship plans that product developers are asked to submit to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> as part of the safety assessment are; stewardship plans to prevent the development of insects resistant to insect-resistant crop plants, and stewardship plans to prevent the development of herbicide resistant weeds, control herbicide tolerant volunteers and reduce the occurrence of volunteers with resistance to multiple herbicides.</p>\n<h2 class=\"h5\">What are some examples of insect-resistant plants?</h2>\n<p>One widely-used insecticide is <i lang=\"la\">Bacillus thuringiensis</i> (Bt), a naturally occurring bacterium that is found in soil. This bacterium produces a toxin that can work as an insecticide. <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> is effective for the control of several insects, such as flies, mosquitoes, Colorado potato beetles, and corn borers.</p>\n<p>Farmers have been using <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> since the 1920s, and it has been commercially available since the 1950s. More recently, plants have been developed to produce a <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> protein, enabling the plant itself to resist certain insect pests. Plants genetically modified to express the <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> protein are capable of resisting certain insect pests (as opposed to farmers spraying this insecticide on their crops to protect them). Currently, in Canada, corn and potatoes expressing the <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> protein have been approved for unconfined environmental release.</p>\n<h2 class=\"h5\">What are insect resistance management plans?</h2>\n<p>An <a href=\"/plant-varieties/plants-with-novel-traits/approved-under-review/eng/1300208455751/1300208520765\">insect resistance management (IRM)</a> plan is a stewardship plan to delay the development of resistance of insects to plants expressing pesticidal properties; that is, to delay the evolution of a susceptible insect population into one that is no longer controlled by a pesticide.</p>\n<p>Insects developing resistance to pesticides is not unique to biotechnology-derived plants. Scientists have long known that such resistance can only be delayed and managed, but not avoided altogether. The <abbr title=\"insect resistance management\">IRM</abbr> plans are designed to delay the development of resistance in insect populations. In the case of plants that have been developed to be resistant to certain insect pests, such as <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> corn, the <abbr title=\"insect resistance management\">IRM</abbr> plans require a refuge strategy.</p>\n<p>The refuge strategy involves exposing one portion of the insect population to <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> corn plants, while maintaining another part of the insect population in an area (a \"refuge\") planted with non-<abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> corn. This is done by planting, in blocks or strips, a certain percentage of the area of a commercial field with unsprayed varieties not expressing the <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> protein. This allows one portion of the susceptible insect population to survive and mate with resistant individuals in the insect population, thus delaying the development of a resistant population.</p>\n<h2 class=\"h5\">What are herbicide tolerance management plans?</h2>\n<p>Similar to insect resistance, where an insect becomes resistant to a pesticide, plants may become resistant, or tolerant, to certain herbicides after repeated exposure to the herbicide. This means weeds can become resistant to herbicides, leaving farmers with fewer options to control weeds. This could result in a farmer tilling the field, which increases soil erosion, or using more herbicides to control the weeds.</p>\n<p>Herbicide tolerance management (HTM) plans are designed to delay weeds and species related to a herbicide-tolerant crop plant from developing tolerance to herbicides. <abbr title=\"Herbicide tolerance management\">HTM</abbr> plans are also designed to address the occurrence of herbicide tolerant volunteers and volunteers with resistance to multiple herbicides.</p>\n<h2 class=\"h5\">What is included in a <abbr title=\"Herbicide tolerance management\">HTM</abbr> plan?</h2>\n<p>An effective <abbr title=\"Herbicide tolerance management\">HTM</abbr> plan is expected to include, but is not limited to, elements such as:</p>\n<ul>\n<li>guidelines for rotation of crops and chemicals</li>\n<li>identification of potential changes in usual farming practices that could result in reduced agricultural sustainability</li>\n<li>an efficient way to allow growers to report any problems they have while growing the crop</li>\n<li>a monitoring plan to assess the effectiveness of the stewardship plan and to identify areas that need improvement</li>\n</ul>\n<h2 class=\"h5\">Are stewardship plans effective?</h2>\n<p>Stewardship plans are part of a strategy to monitor and manage long-term environmental effects of certain crop plants. They also serve as information tools for farmers.</p>\n<p>Compliance with stewardship plans is being monitored by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and its partners. Evaluations done to date indicate a high compliance rate. For example, evaluations of farmer compliance with <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> corn stewardship plans are done on behalf of the Canadian Corn Pest Coalition (CCPC). The results of these can be found on the <a href=\"http://www.cornpest.ca/\"><abbr title=\"Canadian Corn Pest Coalition\">CCPC</abbr> website</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce qu'un v\u00e9g\u00e9tal \u00e0 caract\u00e8res nouveaux (VCN)?</h2>\n<p>Par v\u00e9g\u00e9tal \u00e0 caract\u00e8res nouveaux (VCN), l'Agence canadienne d'inspection des aliments (ACIA) entend une nouvelle vari\u00e9t\u00e9 d'une esp\u00e8ce qui poss\u00e8de un ou plusieurs caract\u00e8res nouveaux pour l'esp\u00e8ce en question au Canada. Un <strong>caract\u00e8re est consid\u00e9r\u00e9 comme \u00e9tant nouveau</strong> quand il pr\u00e9sente les deux caract\u00e9ristiques suivantes :</p>\n<ul>\n<li>il est nouveau pour l'esp\u00e8ce v\u00e9g\u00e9tale au Canada et</li>\n<li>il peut avoir un effet sur l'environnement.</li>\n</ul>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e9value l'innocuit\u00e9 des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada. Cette \u00e9valuation doit se faire peu importe la m\u00e9thode employ\u00e9e pour introduire le caract\u00e8re nouveau, par exemple le g\u00e9nie g\u00e9n\u00e9tique, la mutag\u00e9n\u00e8se et la s\u00e9lection g\u00e9n\u00e9tique classique. En mettant l'accent sur les v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux plut\u00f4t que sur des m\u00e9thodes de production, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut, contrairement \u00e0 d'autres pays, \u00e9valuer l'innocuit\u00e9 d'un plus grand \u00e9ventail de produits.</p>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/processus-d-evaluation/fra/1338189630096/1338189929476\">Processus d'\u00e9valuation de l'innocuit\u00e9 des aliments nouveaux et des produits agricoles issus de la biotechnologie</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/nouveaute/fra/1338181110010/1338181243773\">\u00ab Nouveaut\u00e9 \u00bb et v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a></li>\n</ul>\n<h2 class=\"h5\">Qu'entend-on par \u00ab gestion \u00bb?</h2>\n<p>Il s'agit de la gestion minutieuse et responsable d'une chose dont quelqu'un a la charge, par exemple, un produit ou l'utilisation d'une technologie.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> applique ce concept aux <abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux\">VCN</abbr>. Elle doit pr\u00eater attention au cycle de vie de ces produits, et ce, m\u00eame durant la partie suivant l'\u00e9valuation et l'approbation des produits en vue de leur diss\u00e9mination dans l'environnement.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d\u00e9finit un plan de gestion comme un document qui d\u00e9crit bri\u00e8vement comment le produit ou la technologie sera g\u00e9r\u00e9 ou dans quelles conditions il sera utilis\u00e9. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> exige des plans de gestion pour certains types de <abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux\">VCN</abbr>.</p>\n<h2 class=\"h5\">Pour quels <abbr title=\"v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux\">VCN</abbr> faut-il pr\u00e9senter un plan de gestion dans le cadre de l'\u00e9valuation de l'innocuit\u00e9?</h2>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> exige que les cr\u00e9ateurs prennent en main la partie suivant l'approbation du cycle de vie de leur produit. Les cr\u00e9ateurs de produits peuvent le faire \u00e0 l'aide de plans de gestion. Ils doivent appuyer la mise en oeuvre de ces plans tandis que les exploitants agricoles sont tenus de se soumettre aux exigences \u00e9nonc\u00e9es dans les plans. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> fournit une aide \u00e0 l'\u00e9laboration, \u00e0 l'utilisation et au suivi des plans de gestion.</p>\n<p>Voici deux exemples de plans de gestion que les cr\u00e9ateurs de produits sont tenus de soumettre \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> dans le cadre de l'\u00e9valuation de l'innocuit\u00e9 : un plan de gestion visant \u00e0 pr\u00e9venir l'apparition d'insectes qui r\u00e9sistent \u00e0 des cultures r\u00e9sistantes aux insectes, et un plan de gestion visant \u00e0 pr\u00e9venir l'apparition de mauvaises herbes r\u00e9sistantes aux herbicides, \u00e0 lutter contre les plantes spontan\u00e9es tol\u00e9rantes aux herbicides et \u00e0 r\u00e9duire l'apparition de plantes spontan\u00e9es r\u00e9sistantes \u00e0 plusieurs herbicides.</p>\n<h2 class=\"h5\">Pouvez-vous citer des exemples de v\u00e9g\u00e9taux r\u00e9sistants aux insectes?</h2>\n<p>Un insecticide populaire est le <i lang=\"la\">Bacillus thuringiensis</i> (Bt). Il s'agit d'une bact\u00e9rie d'origine naturelle pr\u00e9sente dans le sol qui produit une toxine pouvant servir d'insecticide. Le <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> permet de lutter efficacement contre certains insectes tels que la mouche, le moustique, le doryphore de la pomme de terre et la pyrale du ma\u00efs.</p>\n<p>Les exploitants agricoles ont recours au <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> depuis les ann\u00e9es 1920; cet insecticide est offert sur le march\u00e9 depuis les ann\u00e9es 1950. Plus r\u00e9cemment, des v\u00e9g\u00e9taux ont \u00e9t\u00e9 cr\u00e9\u00e9s pour produire eux-m\u00eames une prot\u00e9ine <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> et ainsi \u00eatre en mesure de r\u00e9sister \u00e0 certains insectes nuisibles. Les v\u00e9g\u00e9taux g\u00e9n\u00e9tiquement modifi\u00e9s pour exprimer la prot\u00e9ine <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> peuvent r\u00e9sister \u00e0 certains insectes nuisibles (les exploitants agricoles n'ont donc pas \u00e0 appliquer l'insecticide sur leurs cultures pour les prot\u00e9ger). \u00c0 l'heure actuelle, le ma\u00efs et les pommes de terre du Canada exprimant la prot\u00e9ine <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> ont \u00e9t\u00e9 approuv\u00e9s pour la diss\u00e9mination en milieu ouvert.</p>\n<h2 class=\"h5\">Qu'est-ce qu'un plan de gestion de la r\u00e9sistance des insectes (GRI)?</h2>\n<p>Un <a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/approuves-cours-d-evaluation/fra/1300208455751/1300208520765\">plan de gestion de la r\u00e9sistance des insectes (GRI)</a> est un plan de gestion \u00e9labor\u00e9 pour retarder l'apparition chez les insectes d'une r\u00e9sistance aux v\u00e9g\u00e9taux dot\u00e9s de propri\u00e9t\u00e9s antiparasitaires. Il sert donc \u00e0 retarder l'\u00e9volution d'une population d'insectes sensible vers une population devenue impossible \u00e0 r\u00e9primer \u00e0 l'aide d'un pesticide.</p>\n<p>Les insectes qui d\u00e9veloppent une r\u00e9sistance aux insecticides ne sont pas uniques aux v\u00e9g\u00e9taux issus de la biotechnologie. Les scientifiques savent depuis longtemps qu'une telle r\u00e9sistance est in\u00e9vitable; elle ne peut qu'\u00eatre retard\u00e9e et g\u00e9r\u00e9e. Les plans de <abbr title=\"gestion de la r\u00e9sistance des insectes\">GRI</abbr> servent \u00e0 retarder l'apparition d'une r\u00e9sistance chez les insectes. Dans le cas de v\u00e9g\u00e9taux cr\u00e9\u00e9s pour r\u00e9sister \u00e0 certains insectes, comme le ma\u00efs <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr>, les plans de <abbr title=\"gestion de la r\u00e9sistance des insectes\">GRI</abbr> doivent comprendre une strat\u00e9gie de refuge.</p>\n<p>Une strat\u00e9gie de refuge consiste \u00e0 exposer une partie de la population d'insectes au ma\u00efs <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> tout en maintenant l'autre partie dans une zone o\u00f9 l'on cultive du ma\u00efs non-<abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> (la \u00ab zone refuge \u00bb). Pour ce faire, il suffit de semer, en carr\u00e9s ou en bandes, des vari\u00e9t\u00e9s non trait\u00e9es aux insecticides et qui ne produisent pas de prot\u00e9ine <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> sur une superficie donn\u00e9e d'un champ commercial. On permet ainsi \u00e0 une partie de la population sensible d'insectes de survivre et de se reproduire avec des insectes r\u00e9sistants, ce qui retarde l'apparition d'une population r\u00e9sistante.</p>\n<h2 class=\"h5\">Qu'est-ce qu'un plan de gestion de la tol\u00e9rance aux herbicides (GTH)?</h2>\n<p>\u00c0 l'instar des insectes, qui peuvent d\u00e9velopper une tol\u00e9rance aux insecticides, les v\u00e9g\u00e9taux peuvent devenir r\u00e9sistants, ou tol\u00e9rants, \u00e0 certains herbicides apr\u00e8s y avoir \u00e9t\u00e9 expos\u00e9s \u00e0 r\u00e9p\u00e9tition. Les mauvaises herbes peuvent donc devenir r\u00e9sistantes aux herbicides, ce qui limitent les options des exploitants agricoles pour lutter contre elles. Un exploitant agricole pourrait devoir cultiver le sol, ce qui augmente l'\u00e9rosion de ce dernier, ou encore employer une plus grande quantit\u00e9 d'herbicides pour lutter contre les mauvaises herbes.</p>\n<p>Le plan de gestion de la tol\u00e9rance aux herbicides (GTH) sert \u00e0 retarder l'apparition d'une tol\u00e9rance aux herbicides chez les mauvaises herbes et les esp\u00e8ces apparent\u00e9es \u00e0 un v\u00e9g\u00e9tal tol\u00e9rant aux herbicides. Il permet \u00e9galement de r\u00e9duire le nombre de plantes spontan\u00e9es r\u00e9sistantes \u00e0 un ou plusieurs herbicides.</p>\n<h2 class=\"h5\">Que doit comprendre un plan de <abbr title=\"gestion de la tol\u00e9rance aux herbicides\">GTH</abbr>?</h2>\n<p>Pour \u00eatre efficace, le plan de <abbr title=\"gestion de la tol\u00e9rance aux herbicides\">GTH</abbr> doit comprendre, entre autres, les \u00e9l\u00e9ments suivants :</p>\n<ul>\n<li>Des lignes directrices sur la rotation des cultures et des produits chimiques</li>\n<li>Un recensement des changements possibles dans les pratiques agricoles habituelles qui pourraient constituer une menace pour la durabilit\u00e9 de l'agriculture</li>\n<li>Une moyen efficace de permettre aux exploitants agricoles de signaler tout probl\u00e8me rencontr\u00e9 durant la culture</li>\n<li>Un plan de suivi servant \u00e0 \u00e9valuer l'efficacit\u00e9 du plan de gestion et \u00e0 mettre en lumi\u00e8re les points \u00e0 am\u00e9liorer</li>\n</ul>\n<h2 class=\"h5\">Les plans de gestion sont-ils efficaces?</h2>\n<p>Les plans de gestion font partie int\u00e9grante d'une strat\u00e9gie visant \u00e0 surveiller et \u00e0 g\u00e9rer les effets \u00e0 long terme de certaines cultures pour l'environnement. Ils constituent \u00e9galement une source de renseignements pour les exploitants agricoles.</p>\n<p>La conformit\u00e9 aux plans de gestion fait l'objet d'un suivi par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et ses partenaires. Les \u00e9valuations effectu\u00e9es jusqu'\u00e0 ce jour r\u00e9v\u00e8lent d'ailleurs un taux de conformit\u00e9 \u00e9lev\u00e9. \u00c0 titre d'exemple, les \u00e9valuations du taux de respect des plans de gestion par les agriculteurs de ma\u00efs <abbr lang=\"la\" title=\"Bacillus thuringiensis\">Bt</abbr> sont effectu\u00e9es pour le compte de la Coalition canadienne contre les ravageurs du ma\u00efs (CCRM). Vous pouvez consulter les r\u00e9sultats de ces \u00e9valuations sur le <a href=\"http://french.cornpest.ca/\"> site Internet de la <abbr title=\"Coalition canadienne contre les ravageurs du ma\u00efs\">CCRM</abbr></a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}