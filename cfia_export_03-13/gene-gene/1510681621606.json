{
    "dcr_id": "1510681621606",
    "title": {
        "en": "Preventive controls for canned peaches - Peach pit fragments",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les p\u00eaches en conserve - Fragments de noyau"
    },
    "html_modified": "2024-03-12 3:57:49 PM",
    "modified": "2018-06-13",
    "issued": "2017-11-14 12:42:01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_prev_control_canned_peaches_1510681621606_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_prev_control_canned_peaches_1510681621606_fra"
    },
    "ia_id": "1510681709720",
    "parent_ia_id": "1526472290070",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Applying preventive controls",
        "fr": "Application de mesures de contr\u00f4le pr\u00e9ventif"
    },
    "commodity": {
        "en": "Fruits and vegetables, processed products",
        "fr": "Fruits et l\u00e9gumes, produits  transform\u00e9s"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1526472290070",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Preventive controls for canned peaches - Peach pit fragments",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les p\u00eaches en conserve - Fragments de noyau"
    },
    "label": {
        "en": "Preventive controls for canned peaches - Peach pit fragments",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les p\u00eaches en conserve - Fragments de noyau"
    },
    "templatetype": "content page 1 column",
    "node_id": "1510681709720",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526472289805",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/canned-peaches/",
        "fr": "/controles-preventifs/peches-en-conserve/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Preventive controls for canned peaches - Peach pit fragments",
            "fr": "Contr\u00f4les pr\u00e9ventifs pour les p\u00eaches en conserve - Fragments de noyau"
        },
        "description": {
            "en": "Guidance on the types of injuries and preventive controls for pit fragments in canned peaches.",
            "fr": "Directives sur les types de dommages et les contr\u00f4les pr\u00e9ventifs pour les fragments de noyau dans les p\u00eaches en conserve."
        },
        "keywords": {
            "en": "Preventive controls, canned peaches, Peach pit fragments, food safety, SFCR",
            "fr": "Contr\u00f4les pr\u00e9ventifs, p\u00eaches en conserve, Fragments de noyau, salubrit\u00e9 des aliments, RSAC"
        },
        "dcterms.subject": {
            "en": "food,inspection,food safety",
            "fr": "aliment,inspection,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-14 12:42:01",
            "fr": "2017-11-14 12:42:01"
        },
        "modified": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Preventive controls for canned peaches - Peach pit fragments",
        "fr": "Contr\u00f4les pr\u00e9ventifs pour les p\u00eaches en conserve - Fragments de noyau"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>On this page</h2>\n<ul class=\"list-unstyled mrgn-lft-lg\">\n<li><a href=\"#a1\">Introduction</a></li>\n<li><a href=\"#a2\">Definitions</a></li>\n<li><a href=\"#a3\">Guidance</a></li>\n</ul>\n<h2 id=\"a1\">Introduction</h2>\n<p>Even though pit fragments are considered natural and intrinsic components of peaches, they may pose a risk of injury depending on their hardness, sharpness, size or shape.</p>\n<h2 id=\"a2\">Definitions</h2>\n<p>For the purpose of this document, the following definitions apply.</p>\n<dl>\n<dt><dfn>Hard pit fragment</dfn></dt>\n<dd>means pits or pieces of pit that are firm, solid, and do not crush or crumble under pressure applied by fingertips.</dd>\n<dt><dfn>Sharp pit fragments</dfn></dt>\n<dd>means pits or pieces of pit having a thin edge or a point able to cut or pierce which can cause injuries to teeth, gum, and intestinal lining.</dd>\n</dl>\n<h2 id=\"a3\">Guidance</h2>\n<p>The types of injuries that can be caused include injury to the gums, lips and teeth that can result from physical damage (cuts, breakage), in addition to other types of injury such as choking.</p>\n<p>Canned peaches containing hard and sharp pit fragment(s) larger than or equal to (\u2265) 2.0 <abbr title=\"millimetre\">mm</abbr> may be considered out of compliance with section 4(1)(a) of the Food and Drugs Act.</p>\n<p>Three different shapes of pit fragments are commonly found:</p>\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_prev_control_canned_peaches_image1_1510680813288_eng.jpg\" class=\"img-responsive\" alt=\"Three different shapes of pit fragments: first one shaped as a shark tooth, the second has a pointed shape and the third has a sharp/cutting edge to it\"></p>\n<p>Appropriate action(s) should be taken in a timely manner to prevent exposure of the population to the product and to prevent further distribution of the product. Follow-up action should include determining the cause of the problem.</p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>Sur cette page</h2>\n<ul class=\"list-unstyled mrgn-lft-lg\">\n<li><a href=\"#a1\">Introduction</a></li>\n<li><a href=\"#a2\">D\u00e9finitions</a></li>\n<li><a href=\"#a3\">Lignes directrices</a></li>\n</ul>\n<h2 id=\"a1\">Introduction</h2>\n<p>M\u00eame si les fragments de noyau sont des composantes intrins\u00e8ques naturelles des p\u00eaches, ils peuvent constituer un risque de blessure du fait de leur duret\u00e9, de leur asp\u00e9rit\u00e9, de leur taille ou de leur forme.</p>\n<h2 id=\"a2\">D\u00e9finitions</h2>\n<p>Dans le cadre du pr\u00e9sent document, les d\u00e9finitions suivantes s'appliquent.</p>\n<dl>\n<dt><dfn>Dur</dfn></dt>\n<dd>qualifie le fragment ou le morceau de noyau qui est ferme, solide et ne se brise pas sous la pression des doigts.</dd>\n<dt><dfn>Pointu</dfn></dt>\n<dd>qualifie le fragment ou le morceau de noyau qui a un c\u00f4t\u00e9 mince ou une pointe capable de couper ou de causer des blessures aux dents, aux gencives ou aux parois intestinales.</dd>\n</dl>\n<h2 id=\"a3\">Lignes directrices</h2>\n<p>Les blessures qui peuvent \u00eatre caus\u00e9es comprennent les coupures, cassures et autres dommages physiques inflig\u00e9s aux gencives, aux l\u00e8vres ou aux dents ainsi que d'autres types de dommages, comme la suffocation.</p>\n<p>Les p\u00eaches en conserve contenant des fragments de noyau durs et tranchants de taille \u00e9gale ou sup\u00e9rieure (\u2265) \u00e0 2,0 <abbr title=\"millimetre\">mm</abbr> peuvent \u00eatre consid\u00e9r\u00e9es comme non conforme \u00e0 l'alin\u00e9a 4(1)(a) de la Loi sur les aliments et drogues.</p>\n<p>Trois diff\u00e9rentes formes de fragments ou morceaux de noyaux sont habituellement retrouv\u00e9s\u00a0:</p>\n<p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_prev_control_canned_peaches_image1_1510680813288_fra.jpg\" class=\"img-responsive\" alt=\"Trois diff\u00e9rentes formes de fragments ou morceaux de noyaux : le premier en forme de dent de requin, le deuxi\u00e8me est pointu et le troisi\u00e8me a le bord coupant\"></p>\n<p>Il faut prendre en temps utile les mesures qui s'imposent, afin d'emp\u00eacher que la population soit expos\u00e9e au produit et que le produit continue d'\u00eatre distribu\u00e9. Les mesures de suivi devraient viser \u00e0 \u00e9tablir la cause du probl\u00e8me.</p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}