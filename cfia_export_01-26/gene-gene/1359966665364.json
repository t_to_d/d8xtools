{
    "dcr_id": "1359966665364",
    "title": {
        "en": "Cancellation of Variety Registration by Request in Canada",
        "fr": "Retrait d'enregistrement des vari\u00e9t\u00e9s par demande au Canada"
    },
    "html_modified": "2024-01-26 2:21:40 PM",
    "modified": "2022-02-22",
    "issued": "2013-02-04 03:31:15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/variety_rcancel_request_1359966665364_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/variety_rcancel_request_1359966665364_fra"
    },
    "ia_id": "1359967053183",
    "parent_ia_id": "1299176203043",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Registration Procedures",
        "fr": "Modalit\u00e9s d'enregistrement"
    },
    "commodity": {
        "en": "Variety Registration",
        "fr": "Enregistrement des vari\u00e9t\u00e9s"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299176203043",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Cancellation of Variety Registration by Request in Canada",
        "fr": "Retrait d'enregistrement des vari\u00e9t\u00e9s par demande au Canada"
    },
    "label": {
        "en": "Cancellation of Variety Registration by Request in Canada",
        "fr": "Retrait d'enregistrement des vari\u00e9t\u00e9s par demande au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1359967053183",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299176130568",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/variety-registration/registration-procedures/cancellation-of-registration/",
        "fr": "/varietes-vegetales/enregistrement-des-varietes/modalites-d-enregistrement/retrait-d-enregistrement/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Cancellation of Variety Registration by Request in Canada",
            "fr": "Retrait d'enregistrement des vari\u00e9t\u00e9s par demande au Canada"
        },
        "description": {
            "en": "The CFIA has adopted a protocol for cancellation of variety registrations by request of the Canadian Representative and Breeder of a given variety.",
            "fr": "L'ACIA en consultation avec la Commission canadienne des grains (CCG) a adopt\u00e9 un protocole de retrait d'enregistrement des vari\u00e9t\u00e9s \u00e0 la demande du repr\u00e9sentant et du s\u00e9lectionneur canadien de v\u00e9g\u00e9taux d'une vari\u00e9t\u00e9 donn\u00e9e."
        },
        "keywords": {
            "en": "Seeds Act, Seeds Regulations, variety registration, registration procedures, cancellation, Canadian Grain Commission",
            "fr": "Loi sur les semences, R\u00e8glement sur les semences, enregistrement des vari\u00e9t\u00e9s, modalit\u00e9s d'enregistrement, retrait, Commission canadienne des grains"
        },
        "dcterms.subject": {
            "en": "crops,grains,inspection,standards,plants,policy,regulation",
            "fr": "cultures,grain,inspection,norme,plante,politique,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-02-04 03:31:15",
            "fr": "2013-02-04 03:31:15"
        },
        "modified": {
            "en": "2022-02-22",
            "fr": "2022-02-22"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Cancellation of Variety Registration by Request in Canada",
        "fr": "Retrait d'enregistrement des vari\u00e9t\u00e9s par demande au Canada"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=31&amp;ga=77#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The Canadian Food Inspection Agency (CFIA) in consultation with the Canadian Grain Commission (CGC) has adopted a protocol for cancellation of variety registrations by request of the Canadian Representative and Breeder of a given variety. This applies to crop kinds listed in Schedule <abbr title=\"3\">III</abbr> of the <i>Seeds Regulations</i>. This is an extension, to all crops subject to registration, of the previous <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>/<abbr title=\"Canadian Grain Commission\">CGC</abbr> joint \"National Wheat Variety Deregistration Protocol\" adopted October 16, 2009. At the time it was a new communications protocol to provide timely information to grain producers and industry representatives on upcoming changes to the registration status of wheat varieties.</p>\n<p>Under this new, extended protocol, a three year notification of cancellation period will apply to varieties of all crop kinds except hybrid canola and rapeseed. Hybrid canola and rapeseed will require a one year notification period. This timeline enables the Canadian Representative and Breeder to ensure that seed stocks of the variety have been cleared from the market and that growers have been duly notified, well in advance, in order to clear seed stocks in farmers' operations. This will help farmers to plan for the future and minimize any financial risk to their businesses. Notifications will be posted August 1<sup>st</sup> in each calendar year and the notification period is from that date forward.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and <abbr title=\"Canadian Grain Commission\">CGC</abbr> are committed to communicating to farmers well before varieties are cancelled. Standardizing the period of cancellation will help to prevent financial risk to farmers by avoiding the planting of varieties of field crops which will no longer be registered for sale in Canada.</p>\n<p>Variety registration cancellation for cause (<abbr title=\"for example\">e.g.</abbr>, non-compliance, fraud, loss of varietal integrity, <abbr title=\"et cetera\">etc.</abbr>) is not part of this policy and remains an enforcement tool available to the Registrar, Variety Registration Office.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> publishes the <a href=\"/plant-varieties/variety-registration/registered-varieties-and-cancellations/registration-cancellations/eng/1410971290935/1410971291748\">Proposed List of Variety Registration Cancellations</a> with the date of cancellation. On August 1st of each year this list is revised and released by the <abbr title=\"Variety Registration Office\">VRO</abbr>. The <abbr title=\"Canadian Grain Commission\">CGC</abbr> revises their <a href=\"https://www.grainscanada.gc.ca/en/grain-quality/variety-lists/\">Variety Designation Lists</a> throughout the year as changes occur.</p>\n<p>For more information, please read section 6.2 of the guidance document <a href=\"/plant-varieties/variety-registration/registration-procedures/guidance-document/eng/1411564219182/1411564268800\">Procedures for the Registration of Crop Varieties in Canada</a>.</p>\n<p>The <abbr title=\"Canadian Grain Commission\">CGC</abbr> publishes <a href=\"https://www.grainscanada.gc.ca/en/grain-quality/variety-lists/\">variety designation lists</a> that list the varieties of wheat, durum, barley, and flax which are eligible for specified classes of grain at the elevator.</p>\n<p>Contact <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> at: <a href=\"mailto:VRO-BEV@inspection.gc.ca\">VRO-BEV@inspection.gc.ca</a></p>\n<p>Contact <abbr title=\"Canadian Grain Commission\">CGC</abbr> at: <a href=\"mailto:contact@grainscanada.gc.ca\">contact@grainscanada.gc.ca</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=31&amp;ga=77#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>L'Agence canadienne d'inspection des aliments (ACIA) en consultation avec la Commission canadienne des grains (CCG) a adopt\u00e9 un protocole de retrait d'enregistrement des vari\u00e9t\u00e9s \u00e0 la demande du repr\u00e9sentant et du s\u00e9lectionneur canadien de v\u00e9g\u00e9taux d'une vari\u00e9t\u00e9 donn\u00e9e. Cela s'applique aux esp\u00e8ces cultiv\u00e9es r\u00e9pertori\u00e9es \u00e0 l'annexe <abbr title=\"3\">III</abbr> du <i>R\u00e8glement sur les semences</i>. Il s'agit d'une extension s'appliquant \u00e0 toutes les cultures assujetties  \u00e0 l'enregistrement, du pr\u00e9c\u00e9dent protocole conjoint de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>/<abbr title=\"Commission canadienne des grains\">CCG</abbr> \u00ab\u00a0Protocole national sur le retrait d'enregistrement des vari\u00e9t\u00e9s\u00a0\u00bb adopt\u00e9 le 16 octobre 2009. \u00c0  l'\u00e9poque, il s'agissait d'un nouveau protocole de communication destin\u00e9 \u00e0 procurer de l'information en temps utile aux producteurs de grains et aux repr\u00e9sentants de l'industrie pour les aviser \u00e0 l'avance des prochains changements qui seront apport\u00e9s \u00e0 l'enregistrement de vari\u00e9t\u00e9s de bl\u00e9.</p>\n<p>En vertu de ce nouveau protocole \u00e9largi, une p\u00e9riode de notification de trois ans pour le retrait s'appliquera aux vari\u00e9t\u00e9s de toutes les esp\u00e8ces cultiv\u00e9es sauf d'hybride de canola et de colza. L'hybride de canola et de colza n\u00e9cessitera une p\u00e9riode de notification d'un an. Ce d\u00e9lai permet au repr\u00e9sentant et au s\u00e9lectionneur canadien de v\u00e9g\u00e9taux de s'assurer que les collections de semence de la vari\u00e9t\u00e9 ont \u00e9t\u00e9 retir\u00e9es du march\u00e9 et que les producteurs ont \u00e9t\u00e9 d\u00fbment inform\u00e9s, longtemps d'avance, afin de retirer les collections de semence des exploitations agricoles. Cela va aider les agriculteurs \u00e0 mieux planifier l'avenir et \u00e0 minimiser tout risque financier pour leur exploitation. Les notifications seront affich\u00e9es le 1<sup>er</sup> ao\u00fbt de chaque ann\u00e9e civile et la p\u00e9riode de notification commence \u00e0 partir de cette date.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et la <abbr title=\"Commission canadienne des grains\">CCG</abbr> se sont engag\u00e9es \u00e0 communiquer aux agriculteurs longtemps d'avance le retrait des vari\u00e9t\u00e9s. La normalisation de la p\u00e9riode de retrait va contribuer \u00e0 pr\u00e9venir les risques financiers pour les agriculteurs en leur \u00e9vitant de planter des vari\u00e9t\u00e9s de plantes de grande culture qui ne sont plus enregistr\u00e9es pour la vente au Canada.</p>\n<p>Le retrait de l'enregistrement d'une vari\u00e9t\u00e9 pour un motif (<abbr title=\"par exemple\">p.\u00a0ex.</abbr> non-conformit\u00e9, fraude, perte de l'int\u00e9grit\u00e9 vari\u00e9tale, <abbr title=\"et cetera\">etc.</abbr>) n'est pas vis\u00e9 par cette politique et demeure un outil d'ex\u00e9cution de la loi \u00e0 la disposition du registraire du Bureau d'enregistrement des vari\u00e9t\u00e9s.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a publi\u00e9 la <a href=\"/varietes-vegetales/enregistrement-des-varietes/varietes-enregistrees-et-annulations/proposee-des-annulations/fra/1410971290935/1410971291748\">Liste propos\u00e9e des annulations d'enregistrement des vari\u00e9t\u00e9s</a> avec la date d'annulation. Le 1er Ao\u00fbt de chaque ann\u00e9e, cette liste est r\u00e9vis\u00e9e et publi\u00e9e par le <abbr title=\"La Bureau d'enregistrement des vari\u00e9t\u00e9s\">BEV</abbr>. La <abbr title=\"Commission canadienne des grains\">CCG</abbr> r\u00e9vise les <a href=\"https://www.grainscanada.gc.ca/fr/qualite-grains/listes-des-varietes-designees/\">Listes des vari\u00e9t\u00e9s d\u00e9sign\u00e9es</a> quand des changements se produisent pendant l'ann\u00e9e.</p>\n<p>Pour obtenir de plus amples renseignements, veuillez lire la section 6.2 du document d'orientation intitul\u00e9 <a href=\"/varietes-vegetales/enregistrement-des-varietes/modalites-d-enregistrement/document-d-orientation/fra/1411564219182/1411564268800\">Modalit\u00e9s d'enregistrement des vari\u00e9t\u00e9s au Canada</a>.</p>\n<p>La <abbr title=\"Commission canadienne des grains\">CCG</abbr> publie des <a href=\"https://www.grainscanada.gc.ca/fr/qualite-grains/listes-des-varietes-designees/\">listes des vari\u00e9t\u00e9s d\u00e9sign\u00e9es</a> qui dressent la liste des vari\u00e9t\u00e9s de bl\u00e9 tendre, de bl\u00e9 dur, d'orge et de lin qui sont admissibles aux classes sp\u00e9cifi\u00e9es de grain au silo.</p>\n<p>Communiquez avec l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e0 l'adresse\u00a0: <a href=\"mailto:VRO-BEV@inspection.gc.ca\">VRO-BEV@inspection.gc.ca</a></p>\n<p>Communiquez avec la <abbr title=\"Commission canadienne des grains\">CCG</abbr> \u00e0 l'adresse\u00a0: <a href=\"mailto:contact@grainscanada.gc.ca\">contact@grainscanada.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}