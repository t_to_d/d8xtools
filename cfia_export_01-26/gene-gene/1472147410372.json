{
    "dcr_id": "1472147410372",
    "title": {
        "en": "Questions and Answers \u2013 Whirling Disease",
        "fr": "Questions et r\u00e9ponses - Tournis des truites"
    },
    "html_modified": "2024-01-26 2:23:22 PM",
    "modified": "2018-03-04",
    "issued": "2016-08-25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_whirling_qa_1472147410372_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_whirling_qa_1472147410372_fra"
    },
    "ia_id": "1472147410851",
    "parent_ia_id": "1336685826959",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1336685826959",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and Answers \u2013 Whirling Disease",
        "fr": "Questions et r\u00e9ponses - Tournis des truites"
    },
    "label": {
        "en": "Questions and Answers \u2013 Whirling Disease",
        "fr": "Questions et r\u00e9ponses - Tournis des truites"
    },
    "templatetype": "content page 1 column",
    "node_id": "1472147410851",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1336685663723",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/whirling-disease/questions-and-answers/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/tournis-des-truites/questions-et-reponses/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and Answers \u2013 Whirling Disease",
            "fr": "Questions et r\u00e9ponses - Tournis des truites"
        },
        "description": {
            "en": "The fish samples collected by Parks Canada staff came from Johnson Lake in Banff National Park.",
            "fr": "Les \u00e9chantillons de poissons collect\u00e9s par le personnel de Parcs Canada provenaient du lac Johnson dans le parc national Banff."
        },
        "keywords": {
            "en": "infectious disease, finfish, reportable disease, aquatic animals, whirling disease, Johnson Lake, Banff National Park, questions and answers",
            "fr": "maladie, touche les poissons, animaux aquatiques, Maladies d\u00e9clarables, tournis des truites, Fiche de renseignements, Questions et r\u00e9ponses, lac Johnson, parc national Banff"
        },
        "dcterms.subject": {
            "en": "inspection,infectious diseases,fish,fisheries products,animal health",
            "fr": "inspection,maladie infectieuse,poisson,produit de la peche,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Aquatic Animal Health Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux aquatiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-08-25",
            "fr": "2016-08-25"
        },
        "modified": {
            "en": "2018-03-04",
            "fr": "2018-03-04"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and Answers \u2013 Whirling Disease",
        "fr": "Questions et r\u00e9ponses - Tournis des truites"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">Where has whirling disease been found?</h2>\n<p>Whirling disease was first confirmed in a sample from Johnson Lake in Banff National Park, Alberta.\u00a0</p>\n<p>A collaborative surveillance plan has been developed and testing of samples collected by Parks Canada and by Alberta Environment and Parks is ongoing.\u00a0<a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/whirling-disease/eng/1336685663723/1336685826959#con\">Confirmed detections are being posted to the CFIA web site</a> as updated information becomes available.</p>\n<p>Additional detections of whirling disease from the ongoing sampling and testing do not mean the disease is spreading. Whirling disease may have been present for several years and the ongoing sampling will help determine the extent of the distribution and the most appropriate disease response.</p>\n<h2 class=\"h5\">How did whirling disease get introduced to Alberta?</h2>\n<p>It is not known how the parasite (<i lang=\"la\">Myxobolus cerebralis</i>) which causes whirling disease was introduced to Alberta.</p>\n<h2 class=\"h5\">What fish are affected by whirling disease?</h2>\n<p>Whirling disease is an infectious disease of finfish that affects trout and salmon. Species found in Alberta that are susceptible to the disease are:</p>\n<ul>\n<li><i lang=\"la\">Oncorhynchus clarkii</i> (cutthroat trout)</li>\n<li><i lang=\"la\">Oncorhynchus mykiss</i> (rainbow trout)</li>\n<li><i lang=\"la\">Prosopium williamsoni</i> (mountain whitefish)</li>\n<li><i lang=\"la\">Salmo trutta</i> (brown trout)</li>\n<li><i lang=\"la\">Salvelinus confluentus</i> (bull trout)</li>\n<li><i lang=\"la\">Salvelinus fontinalis</i> (brook trout)<strong> </strong></li>\n</ul>\n<h2 class=\"h5\">What is being done to control the spread of whirling disease in Alberta?</h2>\n<p>The Canadian Food Inspection Agency, Parks Canada and Alberta Environment and Parks are working closely together to determine the appropriate disease control response.</p>\n<p>The <a href=\"https://www.pc.gc.ca/en/pn-np/ab/banff/nature/conservation/aquatics/restoring/lac-johnson-lake\">Parks Canada web site</a> provides information about whirling disease related restrictions or requirements in Banff National Park.</p>\n<p>The <a href=\"https://www.alberta.ca/whirling-disease.aspx\">Alberta Environment and Parks web site</a> provides information about whirling disease related restrictions or requirements in the Province of Alberta.</p>\n<p>The Canadian Food Inspection Agency will determine the need for any further disease declarations on the status of whirling disease when the complete results of this year\u2019s surveillance plan sampling and testing have been received.</p>\n<h2 class=\"h5\">What can the public do to help control the spread of the disease?</h2>\n<p>People can spread whirling disease by moving any of the following:</p>\n<ul>\n<li>infected live or dead finfish,</li>\n<li>infected worms,</li>\n<li>contaminated equipment, or</li>\n<li>contaminated water, aquatic plants and soil or mud.</li>\n</ul>\n<p>The public should avoid transporting any of these items from one body of water to another and clean equipment between uses.</p>\n<p>More information on <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/whirling-disease/fact-sheet/eng/1336686597267/1336686806593\">whirling disease precautions</a> can be found on the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> web site.</p>\n<p>Parks Canada will be providing guidance to park visitors on steps they can take to limit the spread of the disease.</p>\n<h2 class=\"h5\">Can the parasite that causes whirling disease be eliminated from Alberta?</h2>\n<p>There is no treatment for whirling disease in fish. The elimination of the parasite in wild finfish populations is not possible.</p>\n<h2 class=\"h5\">Is there a health risk for humans or pets?</h2>\n<p>No. The parasite that causes whirling disease does not affect humans or animals other than trout and salmon. There is no risk associated with swimming or eating fish caught from infected waters.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">O\u00f9 a-t-on d\u00e9couvert le tournis des truites?</h2>\n<p>La pr\u00e9sence du tournis des truites a \u00e9t\u00e9 confirm\u00e9e pour la premi\u00e8re fois dans un \u00e9chantillon du lac Johnson dans le parc national Banff (Alberta).</p>\n<p>Un plan de surveillance collaborative a \u00e9t\u00e9 \u00e9labor\u00e9 et l'analyse d'\u00e9chantillons pr\u00e9lev\u00e9s par Parcs Canada et par <span lang=\"en\">Alberta Environment and Parks</span> est en cours. <a href=\"/fra/1492020203162/1492020203648\">Les cas confirm\u00e9s sont affich\u00e9s sur le site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> \u00e0 mesure que l'information \u00e0 jour est disponible.</p>\n<p>Des d\u00e9tections suppl\u00e9mentaires du tournis des truites \u00e0 partir de l'\u00e9chantillonnage continu et de l'analyse ne signifient pas que la maladie se propage. Le tournis des truites peut avoir \u00e9t\u00e9 pr\u00e9sent pendant plusieurs ann\u00e9es et l'\u00e9chantillonnage continu aidera \u00e0 d\u00e9terminer l'\u00e9tendue de la r\u00e9partition et l'intervention la plus appropri\u00e9e.</p>\n<h2 class=\"h5\">Comment le tournis des truites est-il arriv\u00e9 \u00e0 l'Alberta?</h2>\n<p>Nous ne savons pas comment le parasite (<i lang=\"la\">Myxobolus cerebralis</i>) qui cause le tournis des truites est arriv\u00e9 \u00e0 l'Alberta</p>\n<h2 class=\"h5\">Quelles esp\u00e8ces de poisson peuvent \u00eatre infect\u00e9es par le tournis des truites?</h2>\n<p>Le tournis des truites est une maladie infectieuse des poissons qui touche la truite et le saumon. Les esp\u00e8ces pr\u00e9sentes en Alberta qui sont vuln\u00e9rables au tournis des truites sont les suivantes\u00a0:</p>\n<ul>\n<li><i lang=\"la\">Oncorhynchus clarkii</i> (truite fard\u00e9e)</li>\n<li><i lang=\"la\">Oncorhynchus mykiss</i> (truite arc-en-ciel)</li>\n<li><i lang=\"la\">Prosopium williamsoni</i> (m\u00e9nomini de montagnes)</li>\n<li><i lang=\"la\">Salmo trutta</i> (truite brune)</li>\n<li><i lang=\"la\">Salvelinus confluentus</i> (omble \u00e0 t\u00eate plate)</li>\n<li><i lang=\"la\">Salvelinus fontinalis</i> (omble de fontaine)</li>\n</ul>\n<h2 class=\"h5\">Quelles sont les mesures prises pour limiter la propagation du tournis des truites en Alberta?</h2>\n<p>Le <a href=\"https://www.pc.gc.ca/fr/pn-np/ab/banff/nature/conservation/aquatics/restoring/lac-johnson-lake\">site Web de Parcs Canada</a> contient de l'information sur les restrictions et exigences li\u00e9es au tournis des truites qui s'appliquent au parc national Banff.</p>\n<p>Le <a href=\"https://www.alberta.ca/whirling-disease.aspx\">site Web d'<span lang=\"en\">Alberta Environment and Parks</span> (en anglais seulement)</a> contient de l'information sur les restrictions et exigences li\u00e9es au tournis des truites qui s'appliquent dans la province de l'Alberta.</p>\n<p>L'Agence canadienne d'inspection des aliments d\u00e9terminera si toute autre d\u00e9claration relative \u00e0 l'\u00e9tat du tournis des truites est n\u00e9cessaire lorsqu'elle aura re\u00e7u les r\u00e9sultats complets des analyses et du plan d'\u00e9chantillonnage de surveillance de cette ann\u00e9e.</p>\n<h2 class=\"h5\">Que peut faire la population pour limiter la propagation de la maladie?</h2>\n<p>Les gens peuvent propager le tournis des truites en d\u00e9pla\u00e7ant\u00a0:</p>\n<ul>\n<li>des poissons infect\u00e9s, qu'ils soient vivants ou morts,</li>\n<li>des vers infect\u00e9s,</li>\n<li>de l'\u00e9quipement contamin\u00e9,</li>\n<li>de l'eau, de la terre, de la boue ou des plantes aquatiques contamin\u00e9es.</li>\n</ul>\n<p>La population doit \u00e9viter de transporter ces articles d'un plan d'eau \u00e0 un autre et doit nettoyer l'\u00e9quipement entre les utilisations.</p>\n<p>Vous trouverez de plus amples renseignements sur les pr\u00e9cautions \u00e0 prendre pour <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/tournis-des-truites/fiche-de-renseignements/fra/1336686597267/1336686806593\">pr\u00e9venir le tournis des truites</a> dans le site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Parcs Canada donnera des conseils aux visiteurs du parc sur les moyens \u00e0 prendre pour pr\u00e9venir la propagation de la maladie.</p>\n<h2 class=\"h5\">Est-ce que le parasite qui cause le tournis des truites peut \u00eatre \u00e9limin\u00e9 de l'Alberta?</h2>\n<p>Il n'existe pas de traitement contre le tournis des truites chez les poissons. L'\u00e9limination du parasite parmi les populations de poissons sauvages n'est pas possible.</p>\n<h2 class=\"h5\">Est-ce qu'il existe un risque pour la sant\u00e9 humaine ou la sant\u00e9 des animaux de compagnie?</h2>\n<p>Non. Le parasite qui cause le tournis des truites ne touche pas les \u00eatres humains ni d'autres animaux que la truite et le saumon. Il n'a aucun risque de nager ou de consommer du poisson p\u00each\u00e9 dans des eaux infect\u00e9es.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}