<?php

use Drush\Drush;

class Empl extends UpdateNode
{
  public $success = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile)
  {
    $this->loadData($jfile);
    //staffadvisor
    if ($this->data->staffadvisor) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_opportunityapproval');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->dcr_id);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
    //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }
    
    //submitername
    if ($this->data->submitername) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_name');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     // Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //submiteremail
    if ($this->data->submiteremail) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_email');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->node_title);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
    //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //submiterphoneNumber
    if ($this->data->contactname) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_employmentrequestorname');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }
    
    //contactemail
    if ($this->data->contactemail) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_employmentrequestoremail');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     //  Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }

    //contactphonenumber
    if ($this->data->contactphonenumber) {
      $this->loadNodeByDcrId($this->legacy_id());
      if ($this->node) {
        $this->updateField('field_employmentrequestorphone');
        $this->success = TRUE;
        Drush::output()->writeln( "updated " . $this->data->dcr_id);
        //$this->save(); // when you are ready, uncomment this.
      } else {
        $this->could_not_load = TRUE;
        Drush::output()->writeln( "could not load: " . $this->data->node_title);
        Drush::output()->writeln('d8 node could not be loaded, d8nid could not be found by the dcr_id=' . $this->legacy_id());
      }
    } else {
     // Drush::output()->writeln( "no twitter override for " . $this->data->node_title);
    }
    if ($this->success) { $this->save(); }// when you are ready, uncomment this.
  }
}
