{
    "dcr_id": "1490995155973",
    "title": {
        "en": "Checklist for completing a request for fertilizer research authorizations",
        "fr": "Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'autorisations de recherche sur les engrais"
    },
    "html_modified": "2024-01-26 2:23:37 PM",
    "modified": "2023-10-26",
    "issued": "2017-04-20",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/plan_fert_trials_checklist_authorization_1490995155973_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/plan_fert_trials_checklist_authorization_1490995155973_fra"
    },
    "ia_id": "1490995340847",
    "parent_ia_id": "1646095693725",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1646095693725",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Checklist for completing a request for fertilizer research authorizations",
        "fr": "Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'autorisations de recherche sur les engrais"
    },
    "label": {
        "en": "Checklist for completing a request for fertilizer research authorizations",
        "fr": "Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'autorisations de recherche sur les engrais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1490995340847",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1646095692928",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/overview/checklist/",
        "fr": "/protection-des-vegetaux/engrais/apercu/liste-de-controle/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Checklist for completing a request for fertilizer research authorizations",
            "fr": "Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'autorisations de recherche sur les engrais"
        },
        "description": {
            "en": "Submission Completeness Checklist for Research Authorizations - Documentation Required for all Research Trials.",
            "fr": "Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'Autorisations de recherche - Documents n\u00e9cessaires pour tous les essais de recherche."
        },
        "keywords": {
            "en": "plant protection, fertilizers, biotechnology, microbial supplements, novel supplements, supplements, submission completeness checklist, research authorizations, documentation",
            "fr": "protection des v\u00e9g\u00e9taux, engrais, biotechnologie, suppl\u00e9ments microbiens, suppl\u00e9ments nouveaux, suppl\u00e9ments, Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te, Autorisations de recherche, documents"
        },
        "dcterms.subject": {
            "en": "animal,fertilizers,inspection,research,research and development",
            "fr": "animal,engrais,inspection,recherche,recherche et d\u00e9veloppement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Government of Canada,Canadian Food Inspection Agency"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-04-20",
            "fr": "2017-04-20"
        },
        "modified": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Checklist for completing a request for fertilizer research authorizations",
        "fr": "Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'autorisations de recherche sur les engrais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"text-right\"><a href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/plan_fert_trials_checklist_authorization_1492554197689_eng.pdf\" title=\"Submission Completeness Checklist for Research Authorizations in portable document format\">PDF (129 <abbr title=\"kilobyte\">kb</abbr>)</a></p>\n\n<h2>Documentation required for all research trials (categories A, B and C)</h2>\n\n<ul><li class=\"mrgn-bttm-md\">A completed <a href=\"/eng/1328823628115/1328823702784#c5475\">Application for Research Authorization CFIA/ACIA 5475</a> form.</li>\n\n<li class=\"mrgn-bttm-md\">Information as required in <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-95/eng/1305610090191/1307854346166\">T-4-95: Signing authority, delegated representatives and Canadian agents</a>, or written indication that information is unchanged from previously provided documentation.</li>\n\n<li class=\"mrgn-bttm-md\">A copy of the proposed research label (see <a href=\"/plant-health/fertilizers/trade-memoranda/t-4-103/eng/1307855399697/1320243929540\">T-4-103: Guidelines for research authorizations for testing of novel supplements</a>, section 6b) for information that is mandatory on research labels).</li>\n\n<li class=\"mrgn-bttm-md\">Trial Maps, within 21 days of trial establishment</li></ul>\n \n<h2>Additional documentation required for categories A and B</h2>\n\n<ul><li class=\"mrgn-bttm-md\">Description of trial design and the treatment regime for the trials.  This should include specific details on the rates and method of application of the novel supplement.</li>\n\n<li class=\"mrgn-bttm-md\">Constituent materials: identification and description of <strong>all</strong> materials used in the production of the end-product, the source and proportion of these materials (include all extenders, carriers, <abbr title=\"et cetera\">etc.</abbr>).</li>\n\n<li class=\"mrgn-bttm-md\"><abbr title=\"Safety Data Sheet\">SDS</abbr> for ingredients and/or final product, including precautions and protective measures.</li>\n\n<li class=\"mrgn-bttm-md\">For microorganisms: provide the purpose of the microbial strain; taxonomic identification of the microorganism to the genus and species level, subspecies, and strain; methodology used to identify all microorganisms found in the final product; relationship to known pathogens; and the origin/source from which they were obtained (environmental isolate or strain bank accession numbers, if the strain has been deposited in a recognized culture collection).</li>\n\n<li class=\"mrgn-bttm-md\">Method of manufacture and quality control procedures in place to ensure consistency in production and freedom of the final product from contaminants at levels which may be harmful to human health, plant health or the environment.</li>\n\n<li class=\"mrgn-bttm-md\">Detailed method(s) of crop disposal to be implemented following the trial period.  As per T-4-103, crop destruction waivers are administered outside of the research authorization process and will not be accelerated to meet service delivery timelines associated with research applications.</li></ul>\n  \n<h2>Additional documentation required for category B</h2>\n\n<ul><li class=\"mrgn-bttm-md\"><p>Rationale for product safety: identify potential risks the supplement may pose towards human health and the environment, supported by data or literature.</p>\n\n<p><strong>Hazard Characterization:</strong> Human, Terrestrial plants/crops, and Non-target species. Please refer to T-4-103 for more information.</p>  \n\n<p><strong>Exposure Characterization:</strong> Product use pattern, Natural occurrence, Description of life cycle, and Physiological properties.  Please refer to T-4-103 for more information.</p> \n\n<p>For additional guidance, please see <a href=\"/plant-health/fertilizers/fertilizer-or-supplement-registration/old-regulations-guide/eng/1456415167275/1456415380069\">Tab 5 of the Guide to Submitting Applications for Registration</a>.</p></li>\n\n<li class=\"mrgn-bttm-md\">Monitoring Plan and Procedure: A monitoring plan of the spread and establishment of the supplement in the environment throughout the duration of the trial must be provided.</li> \n\n<li class=\"mrgn-bttm-md\">Confinement Procedures: This includes but are not limited to descriptions of the packaging of the supplement during shipment and storage, timing and application methods, implementation of buffer zones (if required), research area entry restrictions, equipment cleanup and sterilization, <abbr title=\"et cetera\">etc.</abbr></li>\n\n<li class=\"mrgn-bttm-md\">Contingency Plan:  This includes a detailed description of cleanup and disposal procedures in cases of accidental spill or release of the product, treated plant material or growing medium contaminated with the supplement</li></ul>\n\n<p>The Fertilizer Safety Section reserves the right to require additional information, data, rationale or results of analysis to support the issuance of a research authorization for any novel supplement as regulated under the <i>Fertilizers Act</i> and <i>Regulations</i>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"text-right\"><a href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/plan_fert_trials_checklist_authorization_1492554197689_fra.pdf\" title=\"Liste de contr\u00f4le pour le d\u00e9p\u00f4t d'une soumission compl\u00e8te d'Autorisations de recherche en format de document portable\">PDF (199 <abbr title=\"kilo-octet\">ko</abbr>)</a></p>\n\n<h2>Documents n\u00e9cessaires pour tous les essais de recherche (cat\u00e9gories A, B et C)</h2>\n\n<ul><li class=\"mrgn-bttm-md\">Un formulaire de <a href=\"/fra/1328823628115/1328823702784#c5475\">Demande d'autorisation de recherche CFIA/ACIA 5475</a> d\u00fbment rempli.</li>\n\n<li class=\"mrgn-bttm-md\">Les renseignements exig\u00e9s dans la Circulaire \u00e0 la profession <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-95/fra/1305610090191/1307854346166\">T-4-95\u2013Fond\u00e9 de signature, repr\u00e9sentants d\u00e9l\u00e9gu\u00e9s et agent r\u00e9sidant au Canada</a> ou une attestation \u00e9crite que les renseignements demeurent inchang\u00e9s depuis que ces derniers documents ont \u00e9t\u00e9 transmis.</li>\n\n<li class=\"mrgn-bttm-md\">Une copie de l'\u00e9tiquette exp\u00e9rimentale (voir la section 6 b) du <a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-103/fra/1307855399697/1320243929540\">T-4-103\u00a0: Lignes directrices sur les autorisations de recherche pour l'essai de suppl\u00e9ments nouveaux</a> pour les \u00e9l\u00e9ments obligatoires qui doivent para\u00eetre sur ces \u00e9tiquettes).</li>\n\n<li class=\"mrgn-bttm-md\">Les cartes du lieu de l'essai, dans les 21 jours apr\u00e8s la mise en place de l'essai.</li></ul>\n \n<h2>Documents suppl\u00e9mentaire exig\u00e9s pour les cat\u00e9gories A et B</h2>\n\n<ul><li class=\"mrgn-bttm-md\">Description du dispositif exp\u00e9rimental et de la structure des traitements de l'essai. Ceci devrait comprendre les d\u00e9tails sp\u00e9cifiques sur les taux et les m\u00e9thodes d'application du suppl\u00e9ment nouveau.</li>\n\n<li class=\"mrgn-bttm-md\">Substances constituantes\u00a0: identification et description de <strong>tous</strong> les mat\u00e9riaux utilis\u00e9s dans la production du produit final ainsi que leur source et leur concentration (inclure tous les prolongateurs, les transporteurs, <abbr title=\"et cetera\">etc.</abbr>).</li>\n\n<li class=\"mrgn-bttm-md\">Les fiches signal\u00e9tiques pour les ingr\u00e9dients et/ou pour le produit final, y compris les mesures de pr\u00e9caution et de protection.</li>\n\n<li class=\"mrgn-bttm-md\">Pour les microorganismes\u00a0: veuillez indiquer l'utilit\u00e9 de la souche microbienne dans le produit; l'identification taxonomique du microorganisme au niveau de l'esp\u00e8ce et du genre, la sous-esp\u00e8ce et la souche; les m\u00e9thodes utilis\u00e9es afin d'identifier tous les microorganismes retrouv\u00e9s dans le produit final; les relations avec des agents pathog\u00e8nes connus; et l'origine/source du microorganisme (l'endroit, le moment et la substance de laquelle il a \u00e9t\u00e9 isol\u00e9 s'il s'agit d'un isolat environnemental, ou le num\u00e9ro d'acc\u00e8s de la banque de souches si la souche a \u00e9t\u00e9 d\u00e9pos\u00e9e dans une souchoth\u00e8que reconnue).</li>\n\n<li class=\"mrgn-bttm-md\">La m\u00e9thode de fabrication et les proc\u00e9dures de contr\u00f4le de la qualit\u00e9 en place afin d'assurer une uniformit\u00e9 au niveau de la production et la puret\u00e9 du produit final \u00e0 des concentrations qui pourraient \u00eatre n\u00e9fastes \u00e0 la sant\u00e9 humaine, \u00e0 la sant\u00e9 des v\u00e9g\u00e9taux ou \u00e0 l'environnement.</li>\n\n<li class=\"mrgn-bttm-md\">Les m\u00e9thodes d\u00e9taill\u00e9es d'\u00e9limination de la r\u00e9colte \u00e0 \u00eatre mises en \u0153uvre \u00e0 la fin de l'essai. Conform\u00e9ment \u00e0 la Circulaire \u00e0 la profession T-4-103, les exemptions de l'obligation de d\u00e9truire la r\u00e9colte sont accord\u00e9es s\u00e9par\u00e9ment du processus d'autorisation de recherche et ne seront pas acc\u00e9l\u00e9r\u00e9es pour respecter les \u00e9ch\u00e9anciers de prestation des services associ\u00e9s aux demandes d'autorisation de recherche.</li></ul>  \n\n<h2>Autres documents exig\u00e9s pour la cat\u00e9gorie B</h2>\n\n<ul><li class=\"mrgn-bttm-md\"><p>Justification scientifique supportant l'innocuit\u00e9\u00a0: identifier tous les risques potentiels que posent le suppl\u00e9ment pour la sant\u00e9 humaine et l'environnement, appuy\u00e9s sur des donn\u00e9es ou par des \u00e9tudes.</p>\n\n<p><strong>Caract\u00e9risation des dangers\u00a0:</strong> Humains, Plantes et cultures terrestres et les esp\u00e8ces non cibl\u00e9es. Veuillez consulter le tableau 1.</p>\n\n<p><strong>Caract\u00e9risation de l'exposition\u00a0:</strong> Mode d'emploi du produit, Pr\u00e9sence dans la nature, Description du cycle de vie et Propri\u00e9t\u00e9s physiologiques. Veuillez consulter le tableau 2.</p>\n\n<p>Pour plus d'information, veuillez consulter l'<a href=\"/protection-des-vegetaux/engrais/enregistrement-d-engrais-ou-de-supplement/ancien-reglement-guide/fra/1456415167275/1456415380069\">Onglet 5 du Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la <i>Loi sur les engrais</i></a>.</p></li>\n\n<li class=\"mrgn-bttm-md\">Plan et op\u00e9rations de surveillance\u00a0: Un plan de surveillance de la propagation et de l'\u00e9tablissement du suppl\u00e9ment dans l'environnement durant l'essai doit \u00eatre fourni.</li> \n\n<li class=\"mrgn-bttm-md\">Proc\u00e9dures d'isolement ou de confinement\u00a0: Ceci comprend, sans toutefois s'y limiter, des descriptions de l'emballage du suppl\u00e9ment pendant l'exp\u00e9dition et l'entreposage, calendrier et m\u00e9thodes d'application, l'am\u00e9nagement de zones tampons (au besoin), le contr\u00f4le de l'entr\u00e9e dans le p\u00e9rim\u00e8tre de recherche, le nettoyage et la st\u00e9rilisation de l'\u00e9quipement, <abbr title=\"et cetera\">etc.</abbr></li>\n\n<li class=\"mrgn-bttm-md\">Plan d'urgence\u00a0: Ceci comprend la description d\u00e9taill\u00e9e des m\u00e9thodes de nettoyage et d'\u00e9limination en cas de d\u00e9versement ou de rejet accidentel du produit, des mat\u00e9riels v\u00e9g\u00e9taux trait\u00e9s ou des milieux de croissance contamin\u00e9s par le suppl\u00e9ment.</li></ul>\n\n<p>Le Bureau de l'innocuit\u00e9 des engrais se r\u00e9serve le droit d'exiger d'autres renseignements, donn\u00e9es, justifications ou r\u00e9sultats d'analyses afin d'appuyer une demande d'autorisation de recherche de tout suppl\u00e9ment nouveau en vertu de la <i>Loi sur les engrais</i> et du <i>R\u00e8glement sur les engrais</i>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}