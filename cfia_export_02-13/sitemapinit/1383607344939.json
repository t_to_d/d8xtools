{
    "dcr_id": "1383607266489",
    "title": {
        "en": "Food labelling for industry",
        "fr": "L'\u00e9tiquetage des aliments pour l'industrie"
    },
    "html_modified": "2024-02-13 9:47:31 AM",
    "modified": "2022-07-06",
    "issued": "2015-03-18 06:58:49",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_lble_industry_1383607266489_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_lble_industry_1383607266489_fra"
    },
    "ia_id": "1383607344939",
    "parent_ia_id": "1299879939872",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Labelling",
        "fr": "\u00c9tiquetage"
    },
    "commodity": {
        "en": "Relevant to All",
        "fr": "Pertinents pour tous"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299879939872",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Food labelling for industry",
        "fr": "L'\u00e9tiquetage des aliments pour l'industrie"
    },
    "label": {
        "en": "Food labelling for industry",
        "fr": "L'\u00e9tiquetage des aliments pour l'industrie"
    },
    "templatetype": "content page 1 column",
    "node_id": "1383607344939",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299879892810",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/industry/",
        "fr": "/etiquetage-des-aliments/etiquetage/industrie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Food labelling for industry",
            "fr": "L'\u00e9tiquetage des aliments pour l'industrie"
        },
        "description": {
            "en": "The Industry Labelling Tool is the food labelling reference for all food inspectors and stakeholders in Canada.",
            "fr": "L'Outil d'\u00e9tiquetage pour l'industrie est la r\u00e9f\u00e9rence en mati\u00e8re d'\u00e9tiquetage des aliments pour tous les inspecteurs des aliments et les intervenants au Canada."
        },
        "keywords": {
            "en": "labels, industry, labelling",
            "fr": "\u00e9tiquetage, \u00e9tiquette, industrie"
        },
        "dcterms.subject": {
            "en": "consumer protection,food labeling,inspection,labelling",
            "fr": "protection du consommateur,\u00e9tiquetage des aliments,inspection,\u00e9tiquetage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-18 06:58:49",
            "fr": "2015-03-18 06:58:49"
        },
        "modified": {
            "en": "2022-07-06",
            "fr": "2022-07-06"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Food labelling for industry",
        "fr": "L'\u00e9tiquetage des aliments pour l'industrie"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Industry Labelling Tool</h2>\n\n<div class=\"row\">\n<div class=\"col-sm-8\">The Industry Labelling Tool is the food labelling reference for all food inspectors and stakeholders in Canada. It replaces the Guide to Food Labelling and Advertising, and the Decisions page to provide consolidated, reorganized and expanded labelling information. This tool provides information on:</div>\n\n<div class=\"col-sm-4\">\n\n<div data-ajax-replace=\"/navi/eng/1417721591812\"></div>\n\n\n</div>\n</div>\n\n<ul>\n<li><a href=\"/food-labels/labelling/industry/food-products/eng/1624290469261/1624290680899\">Food products that require a label</a></li>\n\n<li>\n<a href=\"/food-labels/labelling/industry/general-principles/eng/1627406110683/1627407577542\">General principles for labelling and advertising</a>\n<ul>\n<li><a href=\"/food-labels/labelling/industry/true-nature/eng/1626787394135/1626787986254\">True nature of food</a></li>\n<li><a href=\"/food-labels/labelling/industry/allergen-labelling/eng/1462980868322/1462980957443\">Factsheet - allergen labelling tips for food industry </a></li>\n<li><a href=\"/food-labels/labelling/industry/how-to-label-allergens/eng/1462469921395/1462472833650\">Infographic: food allergen labelling</a></li>\n</ul></li>\n\n<li>\n<a href=\"/food-labels/labelling/industry/food-labelling-requirements-checklist/eng/1393275252175/1393275314581\">Labelling requirements checklist</a></li>\n<li>\n<a href=\"/food-labels/labelling/industry/industry-labelling-tool/eng/1426607200890/1426607709333\">Frequently asked questions: Industry Labelling Tool (ILT)</a></li>\n</ul>\n\n<h3 id=\"a1\" class=\"mrgn-bttm-md\">Core labelling requirements</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-2\">\n<img alt=\"\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/food_lble_industry_core_1395254537633_eng.jpg\" class=\"img-responsive\">\n</div>\n\n<div class=\"col-sm-10\">\n<ul class=\"colcount-sm-2 list-unstyled\">\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/bilingual-food-labelling/eng/1627499530063/1627499821742\">Bilingual labelling</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/common-name/eng/1625662827512/1625662978189\">Common name</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/country-of-origin/eng/1334599362133/1334601061354\">Country of origin</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/date-markings-and-storage-instructions/eng/1627497005658/1627497193134\">Date markings and storage instructions</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/name-and-principal-place-of-business/eng/1624031910385/1624031958465\">Name and principal place<br>\n</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/irradiated-foods/eng/1334594151161/1334596074872\">Irradiated foods</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/legibility-and-location/eng/1627064615681/1627064668279\">Legibility and location</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/list-of-ingredients-and-allergens/eng/1628716222800/1628716311275\">List of ingredients and allergens</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/net-quantity/eng/1625703526033/1625703680279\">Net quantity</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/nutrition-labelling/eng/1386881685057/1386881685870\">Nutrition labelling</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/sweeteners/eng/1387749708758/1387750396304\">Sweeteners</a></li>\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/food-additives/eng/1623872657859/1623873217667\">Food additives</a></li>\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/fortification-of-food/eng/1468504433692/1468504697186\">Fortification</a></li>\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/grades-for-food/eng/1468508117774/1468508381597\">Grades</a></li>\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/standards-of-identity-for-food/eng/1468511768544/1468511932838\">Standards of identity</a></li>\n</ul>\n</div>\n\n</div>\n\n<div class=\"clearfix\"></div>\n\n<h3 id=\"a2\" class=\"mrgn-bttm-md\">Claims and statements</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-2\">\n<img alt=\"\" class=\"img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/food_lble_industry_core2_1400172501394_eng.jpg\">\n</div>\n\n<div class=\"col-sm-10\"><ul class=\"colcount-sm-2 list-unstyled\">\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/food-advertising-requirements/eng/1623969383781/1623969384171\">Advertising</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/allergens-and-gluten/eng/1388152325341/1388152326591\">Allergens and gluten</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/composition-and-quality/eng/1625516122300/1625516122800\">Composition and quality</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/health-claims-on-food-labels/eng/1392834838383/1392834887794\">Health claims</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/method-of-production-claims/eng/1633011251044/1633011867095\">Method of production</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/pictures-vignettes-logos-and-trademarks-on-food-la/eng/1388352402139/1388352491636\">Pictures, vignettes, logos<br>\n<br></a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/organic-claims/eng/1623967517085/1623967517522\">Organic</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/origin-claims/eng/1626884467839/1626884529209\">Origin</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/nutrient-content/eng/1389905941652/1389905991605\">Nutrient content</a>\n</li>\n</ul></div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<h3 id=\"a3\" class=\"mrgn-bttm-md\">Food-specific labelling requirements</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-2\">\n<img alt=\"\" class=\"img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/food_lble_industry_core3_1400172547785_eng.jpg\">\n</div>\n\n<div class=\"col-sm-10\">\n<ul class=\"colcount-sm-2 list-unstyled\">\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/alcoholic-beverages/eng/1624281662154/1624281662623\">Alcohol</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/confectionery-chocolate-and-snacks/eng/1626445328650/1626446614878\">Confectionery, chocolate and snack foods</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/dairy/eng/1624983427586/1624983674393\">Dairy</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/processed-egg-products/eng/1625836871571/1625837136204\">Eggs-processed</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/shelled-eggs-products/eng/1652210659272/1652210659585\">Eggs-shell</a></li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/fats-and-oils/eng/1626115110031/1626115225080\">Fats and oils</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/fish/eng/1630326254350/1630326374266\">Fish and fish products</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/foods-for-special-dietary-use/eng/1393627685223/1393637610720\">Foods for special dietary use</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/fresh-fruits-or-vegetables/eng/1631894552081/1631895352383\">Fresh fruits or vegetables</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/grain-and-bakery-products/eng/1623965206880/1623965322041\">Grain and bakery</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/honey/eng/1625512297254/1625512297753\">Honey</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/infant-foods-infant-formula-and-human-milk/eng/1627585195109/1627664041736\">Infant food and infant formula</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/maple-products/eng/1625684529252/1625684669979\">Maple</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/meat-and-poultry-products/eng/1632494849908/1632495277194\">Meat and poultry</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/processed-fruit-or-vegetable-products/eng/1631562544919/1631562709087\">Processed fruit or vegetable products</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/retail-foods/eng/1625519284497/1625519675180\">Retail foods</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/salt/eng/1391790253201/1391795959629\">Salt</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/shipping-containers/eng/1625072963936/1625073023844\">Shipping containers</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/food-labels/labelling/industry/prepackaged-water-and-ice/eng/1625516050962/1625516051400\">Water and ice</a>\n</li>\n</ul></div>\n</div>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n\t<h2>L'Outil d'\u00e9tiquetage pour l'industrie</h2>\n\n<div class=\"row\">\n<div class=\"col-sm-8\">L'Outil d'\u00e9tiquetage pour l'industrie est la r\u00e9f\u00e9rence en mati\u00e8re d'\u00e9tiquetage des aliments pour tous les inspecteurs des aliments et les intervenants au Canada. Il remplace le Guide d'\u00e9tiquetage et de publicit\u00e9 sur les aliments et la page Web D\u00e9cision de l'\u00e9tiquetage et publicit\u00e9 en vue de fournir des renseignements d'\u00e9tiquetage regroup\u00e9s, r\u00e9organis\u00e9s et d\u00e9taill\u00e9s. L'Outil pr\u00e9sente entre autres\u00a0:</div>\n<div class=\"col-sm-4\">\n\n<div data-ajax-replace=\"/navi/fra/1417721591812\"></div>\n\n\n</div>\n</div>\n\n<ul>\n<li>de l'information sur les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/produits-alimentaires/fra/1624290469261/1624290680899\">produits alimentaires qui doivent porter une \u00e9tiquette</a>;\n</li>\n\n<li>les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/principes-generaux/fra/1627406110683/1627407577542\">principes g\u00e9n\u00e9raux relatifs \u00e0 l'\u00e9tiquetage et \u00e0 la publicit\u00e9</a>;\n\n<ul>\n<li>La <a href=\"/etiquetage-des-aliments/etiquetage/industrie/nature-veritable/fra/1626787394135/1626787986254\">nature v\u00e9ritable d'un aliment</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/l-etiquetage-des-allergenes/fra/1462980868322/1462980957443\">Fiche de renseignements - Conseils \u00e0 l'industrie alimentaire visant l'\u00e9tiquetage des allerg\u00e8nes</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/comment-etiqueter-les-allergenes/fra/1462469921395/1462472833650\">Infographie\u00a0: \u00c9tiquetage des allerg\u00e8ne  alimentaires</a></li>\n</ul></li>\n\n<li>une <a href=\"/etiquetage-des-aliments/etiquetage/industrie/liste-de-verification-des-exigences-en-matiere-d-e/fra/1393275252175/1393275314581\">liste de v\u00e9rification des exigences en mati\u00e8re d'\u00e9tiquetage</a>;\n</li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/outil-d-etiquetage-de-l-industrie-apercu/fra/1426607200890/1426607709333\">foire aux questions\u00a0: Outil d'\u00e9tiquetage de l'industrie (OEI)</a>.</li>\n</ul>\n\n<h3 id=\"a1\" class=\"mrgn-bttm-md\">Exigences de base en mati\u00e8re d'\u00e9tiquetage</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-2\">\n<img alt=\"\" class=\"img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/food_lble_industry_core_1395254537633_fra.jpg\">\n</div>\n\n<div class=\"col-sm-10\">\n<ul class=\"colcount-sm-2 list-unstyled\">\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-bilingue-des-aliments/fra/1627499530063/1627499821742\">\u00c9tiquetage bilingue</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/nom-usuel/fra/1625662827512/1625662978189\">Nom usuel</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/declaration-du-pays-d-origine/fra/1334599362133/1334601061354\">Pays d'origine</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/datation-et-directives-d-entreposage/fra/1627497005658/1627497193134\">Datation et directives d'entreposage</a></li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/nom-et-principal-lieu-d-affaires/fra/1624031910385/1624031958465\">Nom et principal lieu<br> d'affaires</a>\n</li>\n\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/irradiation/fra/1334594151161/1334596074872\">Aliments irradi\u00e9s</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/lisibilite-et-emplacement/fra/1627064615681/1627064668279\">Lisibilit\u00e9 et emplacement</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/liste-d-ingredients-et-allergenes/fra/1628716222800/1628716311275\">Liste d'ingr\u00e9dients et allerg\u00e8nes</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/quantite-nette/fra/1625703526033/1625703680279\">Quantit\u00e9 nette</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-nutritionnel/fra/1386881685057/1386881685870\">\u00c9tiquetage nutritionnel</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/edulcorants/fra/1387749708758/1387750396304\">\u00c9dulcorants</a>\n</li>\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/additifs-alimentaires/fra/1623872657859/1623873217667\">Additifs alimentaires</a></li>\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/enrichissement-des-aliments/fra/1468504433692/1468504697186\">Enrichissement</a></li>\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/categories-pour-les-produits-alimentaires/fra/1468508117774/1468508381597\">Cat\u00e9gories</a></li>\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/normes-d-identite-pour-les-produits-alimentaires/fra/1468511768544/1468511932838\">Normes d'identit\u00e9</a></li>\n</ul>\n</div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<h3 id=\"a2\" class=\"mrgn-bttm-md\">All\u00e9gations et mentions</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-2\">\n<img alt=\"\" class=\"img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/food_lble_industry_core2_1400172501394_fra.jpg\">\n</div>\n\n<div class=\"col-sm-10\">\n<ul class=\"colcount-sm-2 list-unstyled\">\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/exigences-relatives-a-la-publicite-d-aliments/fra/1623969383781/1623969384171\">Publicit\u00e9</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/allergenes-et-gluten/fra/1388152325341/1388152326591\">Allerg\u00e8nes et gluten</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/composition-et-qualite/fra/1625516122300/1625516122800\">Composition et qualit\u00e9</a></li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/allegations-sante/fra/1392834838383/1392834887794\">All\u00e9gations sant\u00e9</a></li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/allegations-relatives-a-la-methode-de-production/fra/1633011251044/1633011867095\">M\u00e9thode de production</a></li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/images-vignettes-logos-et-marques-de-commerce-sur-/fra/1388352402139/1388352491636\">Images, vignettes, logos<br>\n</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/allegations-biologiques/fra/1623967517085/1623967517522\">Biologique</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/allegations-concernant-l-origine/fra/1626884467839/1626884529209\">Origine</a></li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/teneur-nutritive/fra/1389905941652/1389905991605\">Teneur nutritive</a>\n</li>\n</ul></div>\n</div>\n\n<div class=\"clearfix\"></div>\n\n<h3 id=\"a3\" class=\"mrgn-bttm-md\">Exigences en mati\u00e8re d'\u00e9tiquetage sp\u00e9cifiques \u00e0 certains produits</h3>\n\n<div class=\"row\">\n<div class=\"col-sm-2\">\n<img alt=\"\" class=\"img-responsive\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/food_lble_industry_core3_1400172547785_fra.jpg\">\n</div>\n\n<div class=\"col-sm-10\">\n<ul class=\"colcount-sm-2 list-unstyled\">\n<li><a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/boissons-alcoolisees/fra/1624281662154/1624281662623\">Alcool</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/confiseries-chocolat-et-grignotines/fra/1626445328650/1626446614878\">Confiseries, chocolat et grignotines</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/produits-laitiers/fra/1624983427586/1624983674393\">Produits laitiers</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/produits-d-oeufs-transformes/fra/1625836871571/1625837136204\">Oeufs transform\u00e9s</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/exigences-en-matiere-d-etiquetage-des-%C5%93ufs-en-coqu/fra/1652210659272/1652210659585\">Oeufs en coquille</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/matieres-grasses-et-huiles/fra/1626115110031/1626115225080\">Graisses et huiles</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/poisson/fra/1630326254350/1630326374266\">Poisson et les produits de poisson</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/aliments-a-usage-dietetique-special/fra/1393627685223/1393637610720\">Aliments \u00e0 usage di\u00e9t\u00e9tique sp\u00e9cial</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/fruits-ou-legumes-frais/fra/1631894552081/1631895352383\">Fruits ou l\u00e9gumes frais</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/cereales-et-les-produits-de-boulangerie/fra/1623965206880/1623965322041\">Produits c\u00e9r\u00e9aliers et de boulangerie</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/miel/fra/1625512297254/1625512297753\">Miel</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/aliments-pour-bebes-des-preparations-pour-nourriss/fra/1627585195109/1627664041736\">Aliments pour b\u00e9b\u00e9s et<br>pr\u00e9parations pour nourrissons</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/produits-de-l-erable/fra/1625684529252/1625684669979\">\u00c9rable</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/produits-de-viande-et-de-volaille/fra/1632494849908/1632495277194\">Viande et volaille</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/produits-transformes/fra/1631562544919/1631562709087\">Produits de fruits ou de l\u00e9gumes transform\u00e9s</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/aliments-au-detail/fra/1625519284497/1625519675180\">Aliments au d\u00e9tail</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/sel/fra/1391790253201/1391795959629\">Sel</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/contenants-d-expedition/fra/1625072963936/1625073023844\">Contenants d'exp\u00e9dition</a>\n</li>\n\n<li>\n<a class=\"btn btn-default col-sm-12 mrgn-bttm-sm\" href=\"/etiquetage-des-aliments/etiquetage/industrie/eau-et-glace-preemballees/fra/1625516050962/1625516051400\">Eau et glace</a>\n</li>\n\n</ul></div>\n</div>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "success": true,
    "chat_wizard": true
}