{
    "dcr_id": "1549570125940",
    "title": {
        "en": "The Ottawa Laboratory (Fallowfield)",
        "fr": "Le laboratoire d'Ottawa (Fallowfield)"
    },
    "html_modified": "11-2-2019",
    "modified": "23-8-2023",
    "issued": "7-2-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_ottawa_fallowfield_1549570125940_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/cfia_science_laboratories_ottawa_fallowfield_1549570125940_fra"
    },
    "parent_ia_id": "1494878085588",
    "ia_id": "1549570207759",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "The Ottawa Laboratory (Fallowfield)",
        "fr": "Le laboratoire d'Ottawa (Fallowfield)"
    },
    "label": {
        "en": "The Ottawa Laboratory (Fallowfield)",
        "fr": "Le laboratoire d'Ottawa (Fallowfield)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1549570207759",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1494878032804",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/our-laboratories/ottawa-fallowfield-/",
        "fr": "/les-sciences-et-les-recherches/nos-laboratoires/ottawa-fallowfield-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "The Ottawa Laboratory (Fallowfield)",
            "fr": "Le laboratoire d'Ottawa (Fallowfield)"
        },
        "description": {
            "en": "The Ottawa (Fallowfield) Laboratory is the CFIA's largest laboratory facility serving the CFIA's three business lines (food, plant and animal health). The laboratory is located on 2000 acres of land in the Ottawa greenbelt, with a working farm, as well as unique bioinformatics and level 2 and 3 biocontainment facilities.",
            "fr": "Le Laboratoire d'Ottawa (Fallowfield) est le laboratoire de l'ACIA le plus grand offrant des services dans les trois secteurs d'activit\u00e9 de l'ACIA (la salubrit\u00e9 des aliments, la protection des v\u00e9g\u00e9taux et la sant\u00e9 animale)."
        },
        "keywords": {
            "en": "Ottawa (Fallowfield) Laboratory, Laboratory",
            "fr": "Laboratoire d'Ottawa (Fallowfield), Laboratoire"
        },
        "dcterms.subject": {
            "en": "education,information,guidelines,educational guidance,scientific research,sciences",
            "fr": "\u00e9ducation,information,lignes directrices,orientation scolaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Strategic Communications Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction des communications strat\u00e9giques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2023-08-23",
            "fr": "2023-08-23"
        },
        "type": {
            "en": "guide",
            "fr": "guide"
        },
        "audience": {
            "en": "educators,business,government,general public,media,scientists",
            "fr": "\u00e9ducateurs,entreprises,gouvernement,grand public,m\u00e9dia,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "The Ottawa Laboratory (Fallowfield)",
        "fr": "Le laboratoire d'Ottawa (Fallowfield)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_fllwfld_text_1556740835551_eng.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">CFIA science laboratory brochure: The Ottawa Laboratory  (Fallowfield) <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2,400&nbsp;kb)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n-->\n<div class=\"clearfix\"></div>\n\n<p>The Ottawa Laboratory (Fallowfield) is located on the traditional unceded territory of the Algonquin Anishinaabeg People.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">About the Canadian Food Inspection Agency</h2></summary>\n\n<p>The Canadian Food Inspection Agency (CFIA) is a science-based regulator with a mandate to safeguard the <a href=\"/food-safety-for-industry/eng/1299092387033/1299093490225\">food</a> supply, protect the health of <a href=\"/plant-health/eng/1299162629094/1299162708850\">plants</a> and <a href=\"/animal-health/eng/1299155513713/1299155693492\">animals</a>, and support market access. The Agency relies on high-quality, timely and relevant science as the basis of its program design and regulatory decision-making. Scientific activities inform the Agency's understanding of risks, provide evidence for developing mitigation measures, and confirm the effectiveness of these measures.</p>\n\n<p>CFIA scientific activities include laboratory testing, research, surveillance, test method development, risk assessments and expert scientific advice. Agency scientists maintain strong partnerships with universities, industry, and federal, provincial and international counterparts to effectively carry out the CFIA's mandate.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_fallowfileld_1498061373921_eng.jpg\" class=\"mrgn-bttm-md img-responsive\" alt=\"Photograph - Ottawa Laboratory (Fallowfield) building\">\n</div>\n<p>The Ottawa Laboratory (Fallowfield) is the CFIA's largest laboratory facility serving the CFIA's three business lines (food, plant and animal health). The laboratory is located on 2000 acres of land in the Ottawa greenbelt, with a working farm, as well as bioinformatics expertise and level 2 and 3 biocontainment facilities.</p>\n\n<p>The laboratory has approximately 200 staff in addition to a number of visiting scientists, contract workers and students providing multi-disciplinary research, technology development and accredited diagnostic services to their clients.</p>\n\n<p>The Ottawa Laboratory (Fallowfield) consists of 3 laboratories (food science, plant and animal health) that share the facility's services.</p>\n\n<h2>What we do</h2>\n\n<h3>Food Science Laboratory</h3>\n\n<p>Research scientists in this laboratory work cooperatively with other food scientists in the Ontario laboratory network to help develop cutting-edge methodology to meet the CFIA's food testing challenges.</p>\n\n<h3>Ottawa Plant Laboratory</h3>\n\n<p>The <a href=\"/science-and-research/our-laboratories/ottawa-plant/eng/1549573322559/1549573449538\">Ottawa Plant Laboratory</a> houses a group of diagnostic and research labs that specialize in genotyping, botany, entomology, nematology, plant pathology and seed science.</p>\n\n<h3>Ottawa Animal Health Laboratory</h3>\n\n<p>The <a href=\"/science-and-research/our-laboratories/ottawa-animal/eng/1549573531296/1549573616506\">Ottawa Animal Health Laboratory</a> is comprised of diagnostic units supported by research scientists that specialize in serodiagnosis, rabies, microbiology, transmissible spongiform encephalopathies, biocontainment Level-3, pathology and molecular diagnosis.</p>\n\n<h3>Reference Laboratories</h3>\n<p>The Ottawa Laboratory (Fallowfield) is designated as a <a href=\"/science-and-research/our-laboratories/cfia-reference-laboratories-and-collaborating-cent/eng/1654548734146/1654548734678\">reference laboratory</a> by the World Organisation for Animal Health (WOAH; founded as Office International des \u00c9pizooties (OIE)), with the following areas of expertise:</p>\n<ul>\n<li>rabies</li>\n<li>chronic wasting disease</li>\n<li>scrapie</li>\n</ul>\n<p>WOAH Reference Laboratories are designated to pursue the scientific and technical problems that relate to a specific disease or topic. Its role is to function as a centre of expertise and standardisation of diagnostic techniques for its designated disease. The reference laboratory is involved in cutting-edge research and training of highly skilled personnel from countries all over the world.</p>\n\n<h3>WHO Collaborating Centre</h3>\n<p>The Ottawa Laboratory (Fallowfield) is also designated as a <a href=\"/science-and-research/our-laboratories/cfia-reference-laboratories-and-collaborating-cent/eng/1654548734146/1654548734678\">collaborating centre</a> by the World Health Organization (WHO) with expertise in the control and epidemiology of rabies in carnivores.\n</p>\n\n\n<h2>Quality management</h2>\n\n<p>All CFIA laboratories are accredited in accordance with the International Standard ISO/IEC 17025, <i>General requirements for the competence of testing and calibration laboratories</i>. The Standards Council of Canada (SCC) provides accreditation for routine testing, test method development and non-routine testing, as identified on the laboratory's Scope of Accreditation on the <a href=\"https://www.scc.ca/en/accreditation/laboratories/canadian-food-inspection-agency-4\" title=\"Standards Council of Canada\">SCC</a> website. Accreditation formally verifies the CFIA's competence to produce accurate and reliable results. The results are supported by the development, validation and implementation of scientific methods, conducted by highly qualified personnel, using reliable products, services, and equipment, in a quality controlled environment. Participation in international proficiency testing programs further demonstrates that our testing is comparable to laboratories across Canada and around the world.</p>\n\n<h2>Physical address</h2>\n\n<p>Ottawa Laboratory (Fallowfield)<br>\n<br>\n<br>\n</p>\n\n<h2>More information</h2>\n\n<ul>\n<li><a href=\"/science-and-research/our-laboratories/ottawa-plant/eng/1549573322559/1549573449538\">Ottawa Plant Laboratory</a></li>\n<li><a href=\"/science-and-research/our-laboratories/ottawa-animal/eng/1549573531296/1549573616506\">Ottawa Animal Health Laboratory</a></li>\n<li><a href=\"/eng/1572460555864/1573251325108\">Podcast with Dr.\u00a0Susan Nadin-Davis, Research Scientist</a></li>\n<li>Article: <a href=\"/science-and-research/our-research-and-publications/animal-safety/eng/1559338637978/1559338638243\">Animal safety deserves a big \"appaws\"</a></li>\n</ul>\n\n<p>Learn about other <a href=\"/science-and-research/our-laboratories/eng/1494878032804/1494878085588\">CFIA laboratories</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--\n<div class=\"row\">\n<div class=\"col-xs-12\">\n<div class=\"well gc-dwnld\"> <a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cfia_scnc_lbrtrs_fllwfld_text_1556740835551_fra.pdf\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-md-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-md-10\">\n<p class=\"gc-dwnld-txt\">Brochure du laboratoire scientifique de l'ACIA&nbsp;: Le Laboratoire d'Ottawa (Fallowfield) <span class=\"gc-dwnld-info\">(PDF&nbsp;&#8211; 2 400&nbsp;ko)</span></p>\n</div>\n</div>\n</a> </div>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n-->\n<p>Le laboratoire d'Ottawa (Fallowfield) est situ\u00e9 sur le territoire traditionnel non c\u00e9d\u00e9 du peuple algonquin Anishinaabeg.</p>\n\n<details class=\"mrgn-bttm-lg\">\n<summary><h2 class=\"h4\">\u00c0 propos de l'Agence canadienne d'inspection des aliments</h2></summary>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est un organisme de r\u00e9glementation \u00e0 vocation scientifique dont le mandat est de pr\u00e9server l'<a href=\"/salubrite-alimentaire-pour-l-industrie/fra/1299092387033/1299093490225\">approvisionnement alimentaire</a>, de prot\u00e9ger la sant\u00e9 des <a href=\"/protection-des-vegetaux/fra/1299162629094/1299162708850\">v\u00e9g\u00e9taux</a> et des <a href=\"/sante-des-animaux/fra/1299155513713/1299155693492\">animaux</a> et de favoriser l'acc\u00e8s aux march\u00e9s. L'Agence s'appuie sur des donn\u00e9es scientifiques de grande qualit\u00e9, opportunes et pertinentes pour concevoir ses programmes et prendre ses d\u00e9cisions r\u00e9glementaires. Les activit\u00e9s scientifiques permettent \u00e0 l'Agence de mieux comprendre les risques, de fournir des preuves pour \u00e9laborer des mesures d'att\u00e9nuation et de confirmer l'efficacit\u00e9 de ces mesures.</p>\n\n<p>Les activit\u00e9s scientifiques de l'ACIA comprennent les essais en laboratoire, la recherche, la surveillance, l'\u00e9laboration de m\u00e9thodes d'essai, l'\u00e9valuation des risques et les conseils d'experts scientifiques. Les scientifiques de l'Agence entretiennent des partenariats solides avec les universit\u00e9s, l'industrie et leurs homologues f\u00e9d\u00e9raux, provinciaux et internationaux afin de remplir efficacement le mandat de l'ACIA.</p>\n</details>\n\n<div class=\"col-sm-7 col-md-6 pull-right mrgn-lft-md row\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/cfia_lab_fallowfileld_1498061373921_eng.jpg\" class=\"img-responsive\" alt=\"Photograph - B\u00e2timent du Laboratoire d'Ottawa (Fallowfield)\">\n</div>\n<p>Le Laboratoire d'Ottawa (Fallowfield) est le plus grand laboratoire de l'ACIA, qui dessert les trois secteurs d'activit\u00e9 de l'Agence (aliments, protection des v\u00e9g\u00e9taux et sant\u00e9 animale). Le laboratoire est situ\u00e9 sur un terrain de 2000 acres dans la ceinture verte d'Ottawa, avec une ferme en activit\u00e9, ainsi qu'une expertise en bio-informatique et des installations de bioconfinement des niveaux 2 et 3.</p>\n<p>Le laboratoire compte environ 200 employ\u00e9s, en plus d'un certain nombre de chercheurs invit\u00e9s, de travailleurs contractuels et d'\u00e9tudiants qui fournissent des services de recherche multidisciplinaire, de d\u00e9veloppement technologique et des services de diagnostic accr\u00e9dit\u00e9s \u00e0 leurs clients.</p>\n\n<p>Le Laboratoire d'Ottawa (Fallowfield) est compos\u00e9 de 3 laboratoires (science alimentaire, v\u00e9g\u00e9taux et sant\u00e9 animale) qui partagent les services de l'installation.</p>\n\n<h2>Nos activit\u00e9s</h2>\n\n<h3>Laboratoire de la science alimentaire</h3>\n\n<p>Les chercheurs de ce laboratoire travaillent en collaboration avec d'autres chercheurs des produits alimentaires du r\u00e9seau de laboratoires de l'Ontario afin d'aider \u00e0 d\u00e9velopper une m\u00e9thodologie de pointe pour relever les d\u00e9fis de l'ACIA en mati\u00e8re d'analyse des aliments.</p>\n\n<h3>Laboratoire des v\u00e9g\u00e9taux d'Ottawa</h3>\n\n<p>Le <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/vegetaux-d-ottawa/fra/1549573322559/1549573449538\">laboratoire des v\u00e9g\u00e9taux d'Ottawa</a> abrite un groupe de laboratoires de diagnostic et de recherche sp\u00e9cialis\u00e9s dans le g\u00e9notypage, la botanique, l'entomologie, la n\u00e9matologie, la phytopathologie et la science des semences.</p>\n\n<h3>Laboratoire de sant\u00e9 animale d'Ottawa</h3>\n\n<p>Le <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/sante-animale-d-ottawa/fra/1549573531296/1549573616506\">laboratoire de sant\u00e9 animale d'Ottawa</a> est compos\u00e9 des unit\u00e9s de diagnostic soutenues par des chercheurs scientifiques sp\u00e9cialis\u00e9s dans le s\u00e9rodiagnostic, la rage, la microbiologie, les enc\u00e9phalopathies spongiformes transmissibles, le bioconfinement de niveau\u00a03, la pathologie et le diagnostic mol\u00e9culaire.</p>\n\n<h3>Laboratoires de r\u00e9f\u00e9rence</h3>\n<p>Le laboratoire de sant\u00e9 animale d'Ottawa est d\u00e9sign\u00e9 comme <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/laboratoires-de-reference-et-centres-collaborateur/fra/1654548734146/1654548734678\">laboratoire de r\u00e9f\u00e9rence</a> par l'Organisation mondiale de la sant\u00e9 animale (OMSA; fond\u00e9e sous le nom d'Office international des \u00e9pizooties (OIE)), avec les domaines d'expertise suivants\u00a0:</p>\n<ul>\n<li>la rage;</li>\n<li>la maladie d\u00e9bilitante chronique; et</li>\n<li>la tremblante du mouton.</li>\n</ul>\n<p>Les laboratoires de r\u00e9f\u00e9rence de l'OMSA sont d\u00e9sign\u00e9s pour \u00e9tudier les probl\u00e8mes scientifiques et techniques li\u00e9s \u00e0 une maladie ou \u00e0 un sujet sp\u00e9cifique. Leur r\u00f4le est de fonctionner comme un centre d'expertise et de normalisation des techniques de diagnostic pour la maladie d\u00e9sign\u00e9e. Le laboratoire de r\u00e9f\u00e9rence participe \u00e0 des recherches de pointe et \u00e0 la formation de personnel hautement qualifi\u00e9 provenant de pays du monde entier.</p>\n<h3>Centre collaborateur de l'OMS</h3>\n<p>Le laboratoire d'Ottawa (Fallowfield) est \u00e9galement d\u00e9sign\u00e9 comme <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/laboratoires-de-reference-et-centres-collaborateur/fra/1654548734146/1654548734678\">centre collaborateur</a> par l'Organisation mondiale de la sant\u00e9 (OMS) pour son expertise dans le contr\u00f4le et l'\u00e9pid\u00e9miologie de la rage chez les carnivores.</p>\n<p>Les centres collaborateurs de l'OMS d\u00e9veloppent et partagent des outils et des informations et participent \u00e0 des recherches en collaboration dans leur domaine d'expertise afin de soutenir les programmes de l'OMS aux niveaux national, r\u00e9gional et international.</p>\n\n\n<h2>Gestion de la qualit\u00e9</h2>\n\n<p>Tous les laboratoires de l'ACIA sont accr\u00e9dit\u00e9s conform\u00e9ment \u00e0 la norme ISO/IEC 17025, <i>Exigences g\u00e9n\u00e9rales concernant la comp\u00e9tence des laboratoires d'\u00e9talonnage et d'essais</i>. Le Conseil canadien des normes (CCN) accorde l'accr\u00e9ditation pour les essais de routine, l'\u00e9laboration de m\u00e9thodes d'essai et les essais non routiniers, comme l'indique la port\u00e9e d'accr\u00e9ditation du laboratoire sur le site Web du <a href=\"https://www.scc.ca/fr/accreditation/laboratories/agence-canadienne-dinspection-des-aliments-laboratoire-dottawa-fallowfield\" title=\"Conseil canadien des normes\">CCN</a>. L'accr\u00e9ditation v\u00e9rifie officiellement la comp\u00e9tence de l'ACIA \u00e0 produire des r\u00e9sultats pr\u00e9cis et fiables. Ces r\u00e9sultats sont \u00e9tay\u00e9s par l'\u00e9laboration, la validation et la mise en \u0153uvre de m\u00e9thodes scientifiques, men\u00e9es par un personnel hautement qualifi\u00e9, \u00e0 l'aide de produits, de services et d'\u00e9quipement fiables, dans un environnement de qualit\u00e9 contr\u00f4l\u00e9e. La participation \u00e0 des programmes internationaux d'essais d'aptitude d\u00e9montre en outre que nos analyses sont comparables \u00e0 celles de laboratoires du Canada et du monde entier.</p>\n\n<h2>Adresse physique</h2>\n\n<p>Laboratoire d'Ottawa (Fallowfield)<br>\n<br>\n<br>\n</p>\n\n<h2>Plus d'informations</h2>\n<ul>\n<li>Le <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/vegetaux-d-ottawa/fra/1549573322559/1549573449538\">laboratoire des v\u00e9g\u00e9taux d'Ottawa</a></li>\n<li>Le <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/sante-animale-d-ottawa/fra/1549573531296/1549573616506\">laboratoire de sant\u00e9 animale d'Ottawa</a></li>\n<li><a href=\"/inspecter-et-proteger/sante-animale/dre-susan-nadin-davis/fra/1572460555864/1572460556208\">Balado avec le Dr Susan Nadin-Davis, chercheuse scientifique</a></li>\n<li>Article\u00a0: <a href=\"/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/la-securite-animale/fra/1559338637978/1559338638243\">La s\u00e9curit\u00e9 animale m\u00e9rite un gros bravo</a></li>\n</ul>\n\n<p>Renseignez-vous sur les autres <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/fra/1494878032804/1494878085588\">laboratoires de l'ACIA</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}