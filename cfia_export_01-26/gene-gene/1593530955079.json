{
    "dcr_id": "1593530955079",
    "title": {
        "en": "Animal material in products containing meat",
        "fr": "Mati\u00e8res animales dans les produits contenant de la viande"
    },
    "html_modified": "2024-01-26 2:25:08 PM",
    "modified": "2020-07-16",
    "issued": "2020-07-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/animal_mtrl_cntng_meat_1593530955079_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/animal_mtrl_cntng_meat_1593530955079_fra"
    },
    "ia_id": "1593530955564",
    "parent_ia_id": "1592232381996",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1592232381996",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Animal material in products containing meat",
        "fr": "Mati\u00e8res animales dans les produits contenant de la viande"
    },
    "label": {
        "en": "Animal material in products containing meat",
        "fr": "Mati\u00e8res animales dans les produits contenant de la viande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1593530955564",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1592232381574",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/commonly-occurring-issues-in-food/animal-material/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/problemes-courants-dans-les-aliments/matieres-animales/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Animal material in products containing meat",
            "fr": "Mati\u00e8res animales dans les produits contenant de la viande"
        },
        "description": {
            "en": "Occasionally, you may find animal material, such as skin, parts of blood vessels or bone fragments in meat or poultry (or food made with meat or poultry).",
            "fr": "Vous pouvez parfois trouver des mati\u00e8res animales, comme de la peau, des parties de vaisseaux sanguins ou des fragments d'os dans la viande ou la volaille (ou des aliments faits avec de la viande ou de la volaille)."
        },
        "keywords": {
            "en": "food safety tips, labels, food recalls, food borne illess, food packaging, storage, food handling, specific products, risks, animal material, meat",
            "fr": "Conseils sur la salubrit\u00e9 des aliments, \u00e9tiquettes, Rappels d'aliments, toxi-infections alimentaire, emballage, entreposage, Produits, risques sp\u00e9cifiques, \u00c9tiquetage, Mati\u00e8res animales, viande"
        },
        "dcterms.subject": {
            "en": "food,consumers,labelling,food labeling,government information,inspection,food inspection,consumer protection,government publication,food safety",
            "fr": "aliment,consommateur,\u00e9tiquetage,\u00e9tiquetage des aliments,information gouvernementale,inspection,inspection des aliments,protection du consommateur,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-07-16",
            "fr": "2020-07-16"
        },
        "modified": {
            "en": "2020-07-16",
            "fr": "2020-07-16"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Animal material in products containing meat",
        "fr": "Mati\u00e8res animales dans les produits contenant de la viande"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Occasionally, you may find animal material, such as skin, parts of blood vessels or bone fragments in meat or poultry (or food made with meat or poultry).</p>\n\n<!-- IMAGE PLACEMENT\n<div class=\"pull-right mrgn-lft-md col-sm-4\"> <p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/\" class=\"img-responsive\" alt=\"\"></p> </div>\n-->\n\n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">Product safety</a></li>\n<li><a href=\"#a2\">What to do if you find bone fragments</a></li>\n</ul>\n\n<h2 id=\"a1\">Product safety</h2>\n\n<p>Bone fragments may pose a health risk if their size, hardness and sharpness could lead to an injury. Finding other animal material, such as skin or blood vessels, in your food may be unpleasant, but does not pose a health risk.</p>\n\n<h2 id=\"a2\">What to do if you find bone fragments</h2>\n\n<p>Finding other animal material in your food such as skin or blood vessels might be unappealing. However, it is safe to eat. You may consider trimming it, throwing it out or returning it to the store where you purchased it.</p>\n\n<p>If you find bone fragments in your food, you should <a href=\"/food-safety-for-consumers/where-to-report-a-complaint/eng/1364500149016/1364500195684\">report it to the CFIA</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Vous pouvez parfois trouver des mati\u00e8res animales, comme de la peau, des parties de vaisseaux sanguins ou des fragments d'os dans la viande ou la volaille (ou des aliments faits avec de la viande ou de la volaille).</p>\n\n<!-- IMAGE PLACEMENT\n<div class=\"pull-right mrgn-lft-md col-sm-4\"> <p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/\" class=\"img-responsive\" alt=\"\"></p> </div>\n-->\n\n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Salubrit\u00e9 des produits</a></li>\n<li><a href=\"#a2\">Que faire si vous trouvez des fragments d'os</a></li>\n</ul>\n\n<h2 id=\"a1\">Salubrit\u00e9 des produits</h2>\n\n<p>Les fragments d'os peuvent pr\u00e9senter un risque pour la sant\u00e9 si leur taille, leur duret\u00e9 et leur tranchant peuvent entra\u00eener une blessure. M\u00eame si le fait de trouver des mati\u00e8res animales, comme de la peau ou des vaisseaux sanguins, dans vos aliments peut \u00eatre d\u00e9sagr\u00e9able, cela ne pr\u00e9sente aucun risque pour la sant\u00e9.</p>\n\n<h2 id=\"a2\">Que faire si vous trouvez des fragments d'os</h2>\n\n<p>Le fait de trouver des mati\u00e8res animales dans vos aliments, comme de la peau ou des vaisseaux sanguins, peut \u00eatre peu attrayant. Par contre, ces aliments sont propres \u00e0 la consommation. Vous pouvez envisager de les retirer, de les jeter ou de les retourner au magasin o\u00f9 vous les avez achet\u00e9s.</p>\n\n<p>Si vous trouvez des fragments d'os dans vos aliments, vous devriez <a href=\"/salubrite-alimentaire-pour-les-consommateurs/ou-signaler-une-plainte/fra/1364500149016/1364500195684\">signaler l'incident \u00e0 l'Agence canadienne d'inspection des aliments (ACIA)</a>.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}