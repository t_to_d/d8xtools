{
    "dcr_id": "1370353094751",
    "title": {
        "en": "Seed sample requirements for Plant Breeders' Rights application",
        "fr": "Exigences relatives aux \u00e9chantillons de semences pour la demande de certificat d'obtention v\u00e9g\u00e9tale"
    },
    "html_modified": "2024-01-26 2:21:56 PM",
    "modified": "2021-05-05",
    "issued": "2013-06-04 09:53:59",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/bred_app_seed_req_1370353094751_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/bred_app_seed_req_1370353094751_fra"
    },
    "ia_id": "1370353850896",
    "parent_ia_id": "1299169942667",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Application Process",
        "fr": "Processus de demande"
    },
    "commodity": {
        "en": "Plant Breeders Rights",
        "fr": "Protection des obtentions v\u00e9g\u00e9tales"
    },
    "program": {
        "en": "Plants",
        "fr": "V\u00e9g\u00e9taux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1299169942667",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Seed sample requirements for Plant Breeders' Rights application",
        "fr": "Exigences relatives aux \u00e9chantillons de semences pour la demande de certificat d'obtention v\u00e9g\u00e9tale"
    },
    "label": {
        "en": "Seed sample requirements for Plant Breeders' Rights application",
        "fr": "Exigences relatives aux \u00e9chantillons de semences pour la demande de certificat d'obtention v\u00e9g\u00e9tale"
    },
    "templatetype": "content page 1 column",
    "node_id": "1370353850896",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299169812146",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plant-breeders-rights/application-process/seed-sample-requirements/",
        "fr": "/varietes-vegetales/protection-des-obtentions-vegetales/processus-de-demande/exigences-relatives-aux-echantillons/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Seed sample requirements for Plant Breeders' Rights application",
            "fr": "Exigences relatives aux \u00e9chantillons de semences pour la demande de certificat d'obtention v\u00e9g\u00e9tale"
        },
        "description": {
            "en": "Seed Sample Requirements",
            "fr": "Exigences relatives aux \u00e9chantillons"
        },
        "keywords": {
            "en": "Plant Breeders' Rights Act, Plant Breeders' Rights Regulations, PBR, Grant of Rights, Plant Breeders' Rights",
            "fr": "Loi sur la protection des obtentions v\u00e9g\u00e9tales, R\u00e8glement sur la protection des obtentions V\u00e9g\u00e9tales, POV, d\u00e9livrance du certificat d'obtention, protection des obtentions v\u00e9g\u00e9tales"
        },
        "dcterms.subject": {
            "en": "agri-food products,plants,samples",
            "fr": "produit agro-alimentaire,plante,\u00e9chantillon"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-06-04 09:53:59",
            "fr": "2013-06-04 09:53:59"
        },
        "modified": {
            "en": "2021-05-05",
            "fr": "2021-05-05"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Seed sample requirements for Plant Breeders' Rights application",
        "fr": "Exigences relatives aux \u00e9chantillons de semences pour la demande de certificat d'obtention v\u00e9g\u00e9tale"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=23&amp;ga=3#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Quantity of seed has been determined based on the relative size and weight of seed. If the crop kind for which a Plant Breeders' Rights application is being filed is not included in the following list, either determine the quantity of seed based on a crop kind of similar seed size or <a href=\"/plant-varieties/plant-breeders-rights/contacts/eng/1299170853878/1299171440944\">contact the Plant Breeders' Rights Office</a> for further instruction.</p>\n\n<table class=\"table table-bordered mrgn-bttm-lg\">\n<caption class=\"text-left\"><strong>Seed sample requirements for agriculture crops</strong></caption>\n<thead>\n<tr class=\"active\">\n<th>Agricultural crops</th>\n<th>Quantity of seed</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Bentgrass / Redtop / Tobacco</td>\n<td class=\"text-center\">1 gram</td>\n</tr>\n<tr>\n<td>Black Mustard / Bluegrass / Clover - Alsike, Hop or Yellow, Large, Small or Suckling, White, Ladino / Crested Dogtail / Fescue - Chewing's, Hard, Red, Creeping Red, Sheep's / Meadow Foxtail / Orchardgrass / Reed Canarygrass / Timothy</td>\n<td class=\"text-center\">10 grams</td>\n</tr>\n<tr>\n<td>Canola / Oilseed Rape (inbreds)</td>\n<td class=\"text-center\">10 grams</td>\n</tr>\n<tr>\n<td>Canola / Oilseed Rape (other than inbreds)</td>\n<td class=\"text-center\">40 grams</td>\n</tr>\n<tr>\n<td>Fescue - Meadow, Tall / Ryegrass / Wheatgrass / Wildrye</td>\n<td class=\"text-center\">50 grams</td>\n</tr>\n<tr>\n<td>Alfalfa / Bird's-foot Trefoil / Black Medick / Bromegrass / Clover - Crimson, Red, Strawberry, Subterranean, Sweet / Crownvetch / Kidneyvetch/ Millet / Mustard - White, Indian / Switchgrass / Tall Oatgrass</td>\n<td class=\"text-center\">75 grams</td>\n</tr>\n<tr>\n<td>Canarygrass / Flax / Hemp / Quinoa / Sorghum</td>\n<td class=\"text-center\">125 grams</td>\n</tr>\n<tr>\n<td>Barley / Buckwheat / Emmer / Lentil / Oat / Rye / Safflower / Sainfoin / Spelt / Sunflower / Triticale / Wheat / Vetch</td>\n<td class=\"text-center\">250 grams</td>\n</tr>\n<tr>\n<td>Corn / Cowpea / Lupine / Pea / Soybean</td>\n<td class=\"text-center\">500 grams</td>\n</tr>\n<tr>\n<td>Bean - Field, Broad, Horse, Lima, Runner, Tick or Faba / Chickpea</td>\n<td class=\"text-center\">1000 grams</td>\n</tr>\n</tbody>\n</table>\n\n<table class=\"table table-bordered mrgn-bttm-lg\">\n<caption class=\"text-left\"><strong>Seed sample requirements for fruit, vegetable and herb crops</strong></caption>\n<thead>\n<tr class=\"active\">\n<th>Fruit, vegetable and herb crops</th> <th>Quantity of seed</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Celeriac / Celery / Chicory / Water Cress / Savory / Sorrel / Thyme / Husk Tomato</td>\n<td class=\"text-center\">5 grams</td>\n</tr>\n<tr>\n<td>Tomato</td>\n<td class=\"text-center\">10 grams</td>\n</tr>\n<tr>\n<td>Cornsalad / Garden Cress / Lettuce / Parsnip / Pepper / Sage</td>\n<td class=\"text-center\">25 grams</td>\n</tr>\n<tr>\n<td>Carrot / Celtuce / Dill / Endive / Parsley</td>\n<td class=\"text-center\">50 grams</td>\n</tr>\n<tr>\n<td>Artichoke / Asparagus / Broccoli / Brussel Sprouts / Cabbage / Cauliflower / Collards / Chives / Eggplant / Kale / Kohlrabi / Leek / Onion / Pak-choi / Pe-tsai / Spinach / Spinach Mustard / Turnip</td>\n<td class=\"text-center\">75 grams</td>\n</tr>\n<tr>\n<td>Beet / Mangel / Swiss Chard / Cantaloupe / Cucumber / Gerkin / Muskmelon / New Zealand Spinach / Okra / Radish / Rhubarb / Salsify</td>\n<td class=\"text-center\">125 grams</td>\n</tr>\n<tr>\n<td>Citron / Squash / Watermelon</td>\n<td class=\"text-center\">250 grams</td>\n</tr>\n<tr>\n<td>Pumpkin / Vegetable Marrow</td>\n<td class=\"text-center\">500 grams</td>\n</tr>\n</tbody>\n</table>\n\n<table class=\"table table-bordered\">\n<caption class=\"text-left\"><strong>Sample requirements for seed reproduced ornamental flowers</strong></caption>\n<thead>\n<tr class=\"active\">\n<th>Crop kind</th> <th>Quantity of seed</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Seed Reproduced Ornamental Flowers</td>\n<td class=\"text-center\">1000 seeds</td>\n</tr></tbody>\n</table>\n\n<p>All reference samples submitted to the Plant Breeders' Rights Office must meet the following requirements:</p>\n<ol>\n<li>At the time of submitting the sample, the applicant must sign a statement certifying that the reference sample is representative of the variety.</li>\n<li class=\"mrgn-bttm-md\">The reference samples are legal samples which may be used in the event a holder's right is challenged. The submission of a non-representative sample for a variety could result in the loss of a right.</li>\n<li class=\"mrgn-bttm-md\">All reference samples originating from outside Canada must be sent via the Canadian agent.</li>\n<li class=\"mrgn-bttm-md\">The seed sample should be untreated. It is illegal to send treated seed in the mail and seed treatments may cause problems with certain types of laboratory analysis.</li>\n<li class=\"mrgn-bttm-md\">Samples submitted should be fastened in a tamper-proof manner. The container should be one of the following types of material:   <ol class=\"lst-lwr-alph\">\n<li>Cotton or polyjute bags (most desirable);</li>\n<li>Manilla seed envelopes;</li>\n<li>Paper bags (double bagged);</li> \n<li>Plastic bags (least desirable).</li>\n</ol> </li>\n<li class=\"mrgn-bttm-md\">Sample bags shall contain an inner and an outer label. The labels should contain the following information:   <ol class=\"lst-lwr-alph\">\n<li>proposed denomination;</li>\n<li>crop kind;</li>\n<li>name of the applicant;</li>\n<li>signature of the applicant or agent;</li>\n<li>the date the sample was drawn.</li>\n</ol> </li>\n</ol>\n<p>For import requirements, please consult the <a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">Automated Import Reference System (AIRS)</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=23&amp;ga=3#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>La quantit\u00e9 de semence exig\u00e9e pour chaque esp\u00e8ce est bas\u00e9e sur le poids et la taille relative de la semence. Si l'esp\u00e8ce pour laquelle vous voulez soumettre une demande de certificat d'obtention n'est pas comprise dans le tableau ci-dessous, d\u00e9terminez la quantit\u00e9 en faisant une comparaison avec une esp\u00e8ce qui poss\u00e8de une taille de semence semblable ou contactez le bureau de la protection des obtentions v\u00e9g\u00e9tales pour obtenir autres directions.</p>\n<table class=\"table table-bordered mrgn-bttm-lg\">\n<caption class=\"text-left\"><strong>Exigences relatives aux \u00e9chantillons de semences pour les esp\u00e8ces agricoles</strong></caption>\n<thead>\n<tr class=\"active\">\n<th>Esp\u00e8ces agricoles</th>\n<th>Quantit\u00e9 de semences</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Agrostides / Tabac</td>\n<td class=\"text-center\">1 gramme</td>\n</tr>\n<tr>\n<td>Moutarde noire / P\u00e2turins / Tr\u00e8fle - alsike, jaune, des champs (grand et petit), blanc / Cr\u00e9telle des pr\u00e9s / F\u00e9tuque - de Chewing, durette, rouge, rouge tra\u00e7ante / Ovine / Vulpin de pr\u00e9s / Alpiste roseau des canaries / Fl\u00e9ole ou mil</td>\n<td class=\"text-center\">10 grammes</td>\n</tr>\n<tr>\n<td>Canola / Colza ol\u00e9agineux (lign\u00e9e consanguine)</td>\n<td class=\"text-center\">10 grammes</td>\n</tr>\n<tr>\n<td>Canola / Colza ol\u00e9agineux (autres que lign\u00e9e consanguine)</td>\n<td class=\"text-center\">40 grammes</td>\n</tr>\n<tr>\n<td>F\u00e9tuque - des pr\u00e9s, \u00e9lev\u00e9e / Ray-grass / Agropyres / \u00c9lymes</td>\n<td class=\"text-center\">50 grammes</td>\n</tr>\n<tr>\n<td>Luzerne / Lotier cornicul\u00e9 / Lupuline / Bromes / Tr\u00e8fle - incarnat, rouge, port-fraise, souterrain, d'odeur ou m\u00e9lilot / Coronille bigarr\u00e9 / Anthyllide vuln\u00e9raire / Millets / Moutarde - blanche (jaune), de l'Inde / Panic Raide / Avoine \u00e9lev\u00e9e</td>\n<td class=\"text-center\">75 grammes</td>\n</tr>\n<tr>\n<td>Alpiste des canaries / Lin \u00e0 filasse et ol\u00e9agineux / Chanvre / Quinoa / Sorgho</td>\n<td class=\"text-center\">125 grammes</td>\n</tr>\n<tr>\n<td>Orge / Sarrasins / Bl\u00e9 amidonnier / Lentille / Avoine / Seigle / Carthame des teinturiers / Sainfoin / \u00c9peautre / Tournesol / Triticale / Bl\u00e9 / Vesces</td>\n<td class=\"text-center\">250 grammes</td>\n</tr>\n<tr>\n<td>Ma\u00efs / Pois \u00e0 vache / Lupin des champs / Pois / Soja</td>\n<td class=\"text-center\">500 grammes</td>\n</tr>\n<tr>\n<td>Haricot - de grande culture et de jardin, de Lima, d'Espagne / Gourgane / F\u00e9verole / Pois chiche</td>\n<td class=\"text-center\">1000 grammes</td>\n</tr>\n</tbody>\n</table>\n\n<table class=\"table table-bordered mrgn-bttm-lg\">\n<caption class=\"text-left\"><strong>Exigences relatives aux \u00e9chantillons de semences pour les esp\u00e8ces des fruits, des l\u00e9gumes et des fines herbes</strong></caption>\n<thead>\n<tr class=\"active\">\n<th>Esp\u00e8ces des fruits, des l\u00e9gumes et des fines herbes</th>\n<th>Quantit\u00e9 de semences</th>\n</tr> \n</thead>\n<tbody>\n<tr>\n<td>C\u00e9leri-rave / C\u00e9leri / Chicor\u00e9e / Cresson de fontaine / Sarriette / Oseille / Thym</td>\n<td class=\"text-center\">5 grammes</td>\n</tr>\n<tr>\n<td>Tomate</td>\n<td class=\"text-center\">10 grammes</td>\n</tr>\n<tr>\n<td>M\u00e2che / Cresson al\u00e9nois / Laitue / Panais / Piment commun / Sauge</td>\n<td class=\"text-center\">25 grammes</td>\n</tr>\n<tr>\n<td>Carotte / Celtuce / Anethe / Chicor\u00e9e / Endive / Persil</td>\n<td class=\"text-center\">50 grammes</td>\n</tr>\n<tr>\n<td>Artichaut / Asperge / Brocoli / Choux de Bruxelles / Chou / Chou-fleur / Chou \u00e0 rosette ou collard / Ciboulette / Aubergine / Chou fris\u00e9 ou chou vert / Chou rave / Poireau / Oignon / \u00c9pinard / Moutarde \u00e9pinard / Navet</td>\n<td class=\"text-center\">75 grammes</td>\n</tr>\n<tr>\n<td>Betterave / Betterave fourrag\u00e8re / Bette \u00e0 carde (poir\u00e9e) / Cantaloupe / Concombre / Cornichon / Melon brod\u00e9 / \u00c9pinard de Nouvelle-Z\u00e9lande / Gombo / Radis / Rhubarbe / Salsifis</td>\n<td class=\"text-center\">125 grammes</td>\n</tr>\n<tr>\n<td>Melon citron / Courges / Melon d'eau</td>\n<td class=\"text-center\">250 grammes</td>\n</tr>\n<tr>\n<td>Citrouille / Courge \u00e0 la mo\u00eblle ou courgette</td>\n<td class=\"text-center\">500 grammes</td>\n</tr>\n</tbody>\n</table>\n\n<table class=\"table table-bordered\">\n<caption class=\"text-left\"><strong>Exigences relatives aux \u00e9chantillons pour les fleurs d'ornements multipli\u00e9es par semence</strong></caption>\n<thead>\n<tr class=\"active\">\n<th>Esp\u00e8ces</th> <th>Quantit\u00e9 de semences</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>Fleurs d'ornements multipli\u00e9es par semence</td>\n<td class=\"text-center\">1000 graines</td>\n</tr>\n</tbody>\n</table>\n\n<p>Tout \u00e9chantillon de r\u00e9f\u00e9rence soumis au bureau de la protection des obtentions v\u00e9g\u00e9tales doit satisfaire les conditions suivantes\u00a0:</p>\n<ol>\n<li class=\"mrgn-bttm-md\">Quand il pr\u00e9sente un \u00e9chantillon, le demandeur doit signer une formulaire d\u00e9clarant que celui-ci est repr\u00e9sentatif de la vari\u00e9t\u00e9.</li>\n<li class=\"mrgn-bttm-md\">Les \u00e9chantillons de r\u00e9f\u00e9rence sont consid\u00e9r\u00e9s comme des \u00e9chantillons l\u00e9gaux qui peuvent \u00eatre utilis\u00e9s au moment d'une r\u00e9cusation des droits du titulaire. La soumission d'\u00e9chantillons non repr\u00e9sentatifs peut entra\u00eener la perte des droits.</li>\n<li class=\"mrgn-bttm-md\">Tous les \u00e9chantillons de r\u00e9f\u00e9rence provenant de l'ext\u00e9rieur du Canada doivent \u00eatre transmis par le mandataire canadien.</li>\n<li class=\"mrgn-bttm-md\">Les \u00e9chantillons de r\u00e9f\u00e9rence ne doivent pas \u00eatre trait\u00e9s. Il va \u00e0 l'encontre de la Loi d'envoyer par la poste des semences trait\u00e9es et il est possible que la semence trait\u00e9e pose des probl\u00e8mes quand on la fait analyser.</li>\n<li class=\"mrgn-bttm-md\">Les \u00e9chantillons soumis doivent l'\u00eatre dans un emballage inviolable, soit\u00a0:\n<ol class=\"lst-lwr-alph\">\n<li>des sacs de coton ou de jute-poly\u00e9thyl\u00e8ne (le meilleur choix);</li>\n<li>des enveloppes de papier manille pour semences;</li>\n<li>des sacs de papier (doubles);</li>\n<li>des sacs de plastique (le dernier choix).</li>\n</ol> </li> \n<li class=\"mrgn-bttm-md\">Les sacs d'\u00e9chantillon doivent porter des \u00e9tiquettes int\u00e9rieure et ext\u00e9rieure donnant les renseignements suivants\u00a0:   <ol class=\"lst-lwr-alph\">\n<li>la d\u00e9nomination propos\u00e9e;</li>\n<li>le nom de l'esp\u00e8ce;</li>\n<li>le nom du requ\u00e9rant;</li>\n<li>la signature du requ\u00e9rant ou du mandataire;</li>\n<li>la date de pr\u00e9l\u00e8vement de l'\u00e9chantillon.</li>\n</ol> </li>\n</ol>\n<p>Pour les exigences d\u2019importation, veuillez consulter le <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l\u2019importation (SARI)</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}