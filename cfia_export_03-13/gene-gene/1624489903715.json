{
    "dcr_id": "1624489903715",
    "title": {
        "en": "Disposing of unsolicited seeds",
        "fr": "\u00c9limination de semences non sollicit\u00e9es"
    },
    "html_modified": "2024-03-12 3:59:01 PM",
    "modified": "2023-08-08",
    "issued": "2021-06-29",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/reporting_disposing_unsolicited_seeds_1624489903715_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/reporting_disposing_unsolicited_seeds_1624489903715_fra"
    },
    "ia_id": "1624490577542",
    "parent_ia_id": "1299173306579",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299173306579",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Disposing of unsolicited seeds",
        "fr": "\u00c9limination de semences non sollicit\u00e9es"
    },
    "label": {
        "en": "Disposing of unsolicited seeds",
        "fr": "\u00c9limination de semences non sollicit\u00e9es"
    },
    "templatetype": "content page 1 column",
    "node_id": "1624490577542",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299173228771",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/disposing-of-unsolicited-seeds/",
        "fr": "/protection-des-vegetaux/semences/elimination-de-semences-non-sollicitees/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Disposing of unsolicited seeds",
            "fr": "\u00c9limination de semences non sollicit\u00e9es"
        },
        "description": {
            "en": "Since 2020, a number of Canadians have reported receiving packages of seeds by mail that they did not order. The Canadian Food Inspection Agency has concluded that the seeds most likely originate from a brushing scam, where a business tries to boost online sales by sending unrequested products to customers and posting fake positive reviews.",
            "fr": "Depuis 2020, bon nombre de Canadiens ont rapport\u00e9 avoir re\u00e7u par la poste des paquets de semences qu'ils n'avaient pas command\u00e9s. L'Agence canadienne d'inspection des aliments a conclu que ces semences \u00e9taient probablement li\u00e9es \u00e0 une arnaque o\u00f9 une entreprise essaie de faire grimper ses ventes en ligne en envoyant des produits non sollicit\u00e9s aux consommateurs, pour ensuite publier de faux commentaires positifs."
        },
        "keywords": {
            "en": "Global Invasive Species Database Plant, Facilities Handling Plant Pests Identi, plant resources, scientific community, List of Pests Regulated, unsolicited seeds",
            "fr": "Liste des parasites, protection des v\u00e9g\u00e9taux, Esp\u00e8ces envahissantes, Longicorne brun de l\u00e9pinette, programmes de l'Agence canadienne d'inspection des aliments, semences non sollicit\u00e9es"
        },
        "dcterms.subject": {
            "en": "exports,imports,plant diseases,plants",
            "fr": "exportation,importation,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-06-29",
            "fr": "2021-06-29"
        },
        "modified": {
            "en": "2023-08-08",
            "fr": "2023-08-08"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Disposing of unsolicited seeds",
        "fr": "\u00c9limination de semences non sollicit\u00e9es"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Since\u00a02020, a number of Canadians have reported receiving packages of seeds by mail that they did not order. Packages were postmarked as being from several different countries, and often did not properly describe their contents.</p>\n \n<p>These seeds most likely originate from a brushing scam, where a business tries to boost online sales by sending unrequested products to customers and posting fake positive reviews.</p>\n\n \n<p>Do not plant unrequested seeds.</p>\n \n<p>Certain seeds could become invasive plants or carry plant pests, which can be harmful when introduced into Canada. They can seriously damage forests as well as agricultural and natural areas.</p>\n\n<h2>What to do</h2>\n\n<p>If you receive seeds that you did not order:</p>\n\n<ul>\n<li>put the seeds and packaging in a sealed bag inside a second sealed bag</li>\n<li>place in your regular garbage that goes to a landfill</li>\n<li>refrain from planting, flushing, or composting the seeds to avoid them sprouting and spreading</li>\n</ul>\n\n<p>If you have already planted them or put them in the compost:</p>\n\n<ul>\n<li>remove the seeds and any plants that may have grown from them</li>\n<li>double-bag the seeds or plants in tightly sealed plastic bags, and place in your regular garbage that goes to a landfill</li>\n</ul>\n\n<p>Learn more about the impact that invasive plants can have:</p>\n\n<ul>\n<li>in the Inspect and Protect article <a href=\"/inspect-and-protect/plant-health/tiny-seeds-you-buy-online-can-cause-big-trouble/eng/1616006891913/1616006892273\">Tiny seeds you buy online can cause big trouble</a> and</li>\n<li>on the <a href=\"/plant-health/invasive-species/eng/1299168913252/1299168989280\">CFIA's plant pests and invasive species page</a></li>\n</ul>\n\n<p>For questions, please contact your <a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local\u00a0CFIA\u00a0office</a> or <a href=\"/about-the-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">submit them online to the CFIA</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Depuis\u00a02020, bon nombre de Canadiens ont rapport\u00e9 avoir re\u00e7u par la poste des paquets de semences qu'ils n'avaient pas command\u00e9s. Le cachet de la poste des emballages indiquait que ceux-ci proviennent de plusieurs pays diff\u00e9rents, et souvent ne d\u00e9crivait pas correctement leur contenu.</p>\n\n<p>Ces semences sont probablement li\u00e9es \u00e0 une arnaque o\u00f9 une entreprise essaie de faire grimper ses ventes en ligne en envoyant des produits non sollicit\u00e9s aux consommateurs, pour ensuite publier de faux commentaires positifs.</p>\n\n<p>Ne plantez pas de semences non sollicit\u00e9es.</p>\n\n<p>Certaines semences pourraient devenir des plantes envahissantes ou \u00eatre porteuses de phytoravageurs qui peuvent \u00eatre nuisibles lorsqu'ils sont introduits au Canada. Elles peuvent causer des dommages s\u00e9rieux aux for\u00eats, de m\u00eame qu'aux zones agricoles et naturelles.</p>\n\n<h2>Que faire</h2>\n\n<p>Si vous recevez des semences que vous n'avez pas command\u00e9es\u00a0:</p>\n\n<ul>\n<li>placez les semences et leur emballage dans un sac scell\u00e9 \u00e0 l'int\u00e9rieur d'un deuxi\u00e8me sac scell\u00e9;</li>\n<li>jetez-le avec les ordures m\u00e9nag\u00e8res destin\u00e9es \u00e0 un site d'enfouissement;</li>\n<li>\u00e9vitez de planter les semences, de les jeter aux toilettes ou de les composter afin d'emp\u00eacher leur germination et leur propagation.</li>\n</ul>\n\n<p>Si vous les avez d\u00e9j\u00e0 plant\u00e9es ou compost\u00e9es\u00a0:</p>\n\n<ul>\n<li>retirez les semences ainsi que toute plante qu'elles auraient pu produire;</li>\n<li>emballez les semences ou les plantes dans deux sacs en plastique bien scell\u00e9s, puis jetez-les avec les ordures m\u00e9nag\u00e8res destin\u00e9es \u00e0 un site d'enfouissement.</li>\n</ul>\n\n<p>Pour en apprendre davantage sur l'incidence que peuvent avoir les plantes envahissantes, vous pouvez consulter\u00a0:</p>\n\n<ul>\n<li>l'article de Inspecter et prot\u00e9ger, \u00ab <a href=\"/inspecter-et-proteger/sante-des-vegetaux/les-minuscules-semences-que-vous-achetez-en-ligne-/fra/1616006891913/1616006892273\">Les minuscules semences que vous achetez en ligne peuvent causer de gros probl\u00e8mes</a> \u00bb;</li>\n<li>la page \u00ab\u00a0<a href=\"/protection-des-vegetaux/especes-envahissantes/fra/1299168913252/1299168989280\">Les phytoravageurs et les esp\u00e8ces envahissantes</a>\u00a0\u00bb de l'ACIA</li>\n</ul>\n\n<p>Si vous avez des questions, veuillez communiquer avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau de l'ACIA de votre r\u00e9gion</a> ou bien les <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">soumettre en ligne \u00e0 l'ACIA</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}