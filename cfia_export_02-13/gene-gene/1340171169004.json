{
    "dcr_id": "1340171169004",
    "title": {
        "en": "Management Accountability Framework",
        "fr": "Cadre de responsabilisation de gestion"
    },
    "html_modified": "2024-02-13 9:47:36 AM",
    "modified": "2020-06-02",
    "issued": "2012-06-20 01:46:17",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_sound_maf_1340171169004_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_sound_maf_1340171169004_fra"
    },
    "ia_id": "1340171378857",
    "parent_ia_id": "1299845904097",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299845904097",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Management Accountability Framework",
        "fr": "Cadre de responsabilisation de gestion"
    },
    "label": {
        "en": "Management Accountability Framework",
        "fr": "Cadre de responsabilisation de gestion"
    },
    "templatetype": "content page 1 column",
    "node_id": "1340171378857",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299845842522",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/regulatory-transparency-and-openness/sound-agency-management/maf/",
        "fr": "/a-propos-de-l-acia/transparence/transparence-et-ouverture/une-saine-gestion-de-l-agence/crg/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Management Accountability Framework",
            "fr": "Cadre de responsabilisation de gestion"
        },
        "description": {
            "en": "The Management Accountability Framework(MAF) sets out Treasury Board's expectations of senior public service managers for good public service management.",
            "fr": "Le Cadre de responsabilisation de gestion (CRG) \u00e9nonce les attentes du Conseil du Tr\u00e9sor envers les cadres sup\u00e9rieurs de la fonction publique en vue d'une saine gestion des minist\u00e8res et des organismes."
        },
        "keywords": {
            "en": "Audit Review, policy, planning, implementation, Planning, Reporting &amp; Accountability, audit reports, executive summary, Management Accountability Framework, MAF",
            "fr": "Audit et examen, politiques, planification, implementation, rapport annuel, Plan d&#39;entreprise, Rapport sur le plans et les priorit&#233;s, Rapport sur le plans et les priorit&#233;s, Rapport sur le rendement, Structure de Planification, de Rapport et de Responsabilisation, comit&#233; permanent, Cadre de responsabilistaion de gestion, CRG"
        },
        "dcterms.subject": {
            "en": "house of commons,food labeling,government information,inspection,food inspection,parliament,government publication,food safety,animal health",
            "fr": "chambre des communes,\u00e9tiquetage des aliments,information gouvernementale,inspection,inspection des aliments,parlement,publication gouvernementale,salubrit\u00e9 des aliments,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-06-20 01:46:17",
            "fr": "2012-06-20 01:46:17"
        },
        "modified": {
            "en": "2020-06-02",
            "fr": "2020-06-02"
        },
        "type": {
            "en": "assessment,reference material",
            "fr": "\u00e9valuation,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Management Accountability Framework",
        "fr": "Cadre de responsabilisation de gestion"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Management Accountability Framework (MAF) sets out Treasury Board's expectations of senior public service managers for good public service management.</p>\n<p><abbr title=\"Management Accountability Framework\">MAF</abbr> is structured around ten key elements that collectively define \"management\" and establish expectations for the good management of a department or agency. To measure these expectations, Treasury Board conducts yearly assessments to identify where management practices are doing well and where they can be enhanced:</p>\n<ul><li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/maf-2017-18/eng/1561435075184/1561435075418\">Management Accountability Framework 2017-18 Departmental Report</a></li></ul>\n\n<p>Management excellence and the continuous improvement of programs and services are a priority for the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. As such, the Agency applies the principles of <abbr title=\"Management Accountability Framework\">MAF</abbr> to its daily management practices.</p>\n<p>In addition to MAF assessments, the Agency's management priorities can be found in its annual <a href=\"/eng/1299845094675/1299845189161#rpp\">Departmental Plan</a>. The CFIA's performance against the management expectations and commitments set out in the DP is reported in its annual <a href=\"/eng/1299845094675/1299845189161#dpr\">Departmental Results Report (DRR)</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le Cadre de responsabilisation de gestion (CRG) \u00e9nonce les attentes du Conseil du Tr\u00e9sor envers les cadres sup\u00e9rieurs de la fonction publique en vue d'une saine gestion des minist\u00e8res et des organismes.</p>\n<p>Le <abbr title=\"Cadre de responsabilisation de gestion\">CRG</abbr> s'articule autour de dix \u00e9l\u00e9ments cl\u00e9s qui d\u00e9finissent dans l'ensemble ce qu'on entend par \u00ab\u00a0gestion\u00a0\u00bb et \u00e9tablissent les attentes en vue d'une saine gestion des minist\u00e8res et des organismes. Pour \u00e9valuer ces attentes, le Conseil du Tr\u00e9sor proc\u00e8de \u00e0 des \u00e9valuations annuelles afin de rep\u00e9rer les bonnes pratiques de gestion et celles qui pourraient \u00eatre am\u00e9lior\u00e9es:</p>\n<ul><li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/crg-2017-2018/fra/1561435075184/1561435075418\">Rapport minist\u00e9riel du Cadre de responsabilisation de gestion 2017-2018</a></li></ul>\n\n<p>L'excellence en gestion et l'am\u00e9lioration continue des programmes et des services sont une priorit\u00e9 pour l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. \u00c0 ce titre, l'Agence applique les principes du <abbr title=\"Cadre de responsabilisation de gestion\">CRG</abbr> \u00e0 ses pratiques de gestion quotidiennes.</p>\n\n<p>En plus des \u00e9valuations fond\u00e9es sur le CRG, on peut trouver les priorit\u00e9s en mati\u00e8re de gestion de l'Agence dans le <a href=\"/fra/1299845094675/1299845189161#rpp\">plan minist\u00e9riel</a> publi\u00e9 annuellement. Le rendement de l'ACIA par rapport aux attentes et aux engagements en gestion \u00e9nonc\u00e9s dans le RPP figure dans le <a href=\"/fra/1299845094675/1299845189161#dpr\">Rapport sur les r\u00e9sultats minist\u00e9riels</a> du minist\u00e8re publi\u00e9 annuellement.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}