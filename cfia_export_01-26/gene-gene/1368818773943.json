{
    "dcr_id": "1368818773943",
    "title": {
        "en": "Jordan - Export requirements for meat and poultry products",
        "fr": "Jordanie - Exigences d'exportation pour viande et volaille"
    },
    "html_modified": "2024-01-26 2:21:51 PM",
    "modified": "2021-09-27",
    "issued": "2013-05-17 15:26:14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter11_export_jordan_1368818773943_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter11_export_jordan_1368818773943_fra"
    },
    "ia_id": "1368818818006",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Jordan - Export requirements for meat and poultry products",
        "fr": "Jordanie - Exigences d'exportation pour viande et volaille"
    },
    "label": {
        "en": "Jordan - Export requirements for meat and poultry products",
        "fr": "Jordanie - Exigences d'exportation pour viande et volaille"
    },
    "templatetype": "content page 1 column",
    "node_id": "1368818818006",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/jordan-meat-and-poultry/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/jordanie-viande-et-volaille/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Jordan - Export requirements for meat and poultry products",
            "fr": "Jordanie - Exigences d'exportation pour viande et volaille"
        },
        "description": {
            "en": "Countries to which exports are currently made - Jordan",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Jordanie"
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, meat inspection, products, certificate, Jordan - Meat and poultry",
            "fr": "Loi sur l&#39;inspection des viandes, R&#232;glement de 1990 sur l&#39;inspection des viandes, inspection des viandes, produits, certificat, Jordanie - viande et volaille"
        },
        "dcterms.subject": {
            "en": "exports,handbooks,standards,agri-food products,food safety,animal health,food processing,meat,poultry",
            "fr": "exportation,manuel,norme,produit agro-alimentaire,salubrit\u00e9 des aliments,sant\u00e9 animale, transformation des aliments,viande,volaille"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Import Export and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de l'importation/l'exporation d'aliments et de la protect. des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-05-17 15:26:14",
            "fr": "2013-05-17 15:26:14"
        },
        "modified": {
            "en": "2021-09-27",
            "fr": "2021-09-27"
        },
        "type": {
            "en": "guide,reference material",
            "fr": "guide,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Jordan - Export requirements for meat and poultry products",
        "fr": "Jordanie - Exigences d'exportation pour viande et volaille"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>1. Eligible/ineligible product</h2>\n\n<h3>1.1 Eligible</h3>\n\n<ul>\n<li>beef</li>\n<li>poultry meat and offal</li>\n</ul>\n\n<h3>1.2 Ineligible</h3>\n\n<p>It is doubtful if pork or pork products would be acceptable, but this fact should be checked with the appropriate authorities.</p>\n\n<h2>2. Documentation requirements</h2>\n\n<h3>Certificate</h3>\n\n<ul>\n<li>Certificate of Inspection Covering Meat Products (CFIA/ACIA 1454)</li>\n<li>Annex A \u2013 Veterinary Certificate for Fresh Meat and Meat Products Derived from Cattle</li>\n<li>Annex B \u2013 Veterinary Certificate for Meat and Meat Products derived from Poultry for Export to Jordan</li>\n</ul>\n\n<h2>3. Other information</h2>\n\n<ul>\n<li>A certificate in respect of ritual slaughter is required. The exporter is responsible to make the necessary arrangements with an Islamic organization to obtain certification acceptable to Jordanian authorities.</li>\n<li>The certification required may have to be endorsed by the Embassy of Jordan. The exporter and importer are responsible to ensure that the applicable requirement is met to the satisfaction of the Jordanian authorities.</li>\n<li>The exporter and importer are responsible to label the product according to applicable Jordanian requirements, for example, Arabic language labelling.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>1. Produits admissibles/non admissibles</h2>\n\n<h3>1.1 Admissibles</h3>\n\n<ul>\n<li>b\u0153uf</li>\n<li>viande de volaille et abats</li>\n</ul>\n\n<h3>1.2 Non admissibles</h3>\n\n<p>Il est douteux que les produits et sous-produits de porc soient accept\u00e9s. Toutefois, cette hypoth\u00e8se devrait \u00eatre v\u00e9rifi\u00e9e avec les autorit\u00e9s concern\u00e9es.</p>\n\n<h2>2. Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<ul>\n<li>Certificat d'inspection pour les produits carn\u00e9s (CFIA/ACIA 1454) </li>\n<li>Annexe A \u2013 Certificat v\u00e9t\u00e9rinaire pour la viande fra\u00eeche et les produits de viande provenant de bovins </li>\n<li>Annexe B \u2013 Certificat v\u00e9t\u00e9rinaire pour les viandes et produits \u00e0 base de viande  d\u00e9riv\u00e9s de volailles pour l'exportation vers la Jordanie</li>\n</ul>\n\n<h2>3. Autres information</h2>\n\n<ul>\n<li>Un certificat relativement \u00e0 l'abattage rituel est requis. L'exportateur est responsable de faire les d\u00e9marches n\u00e9cessaires aupr\u00e8s d'une association islamique en vue d'obtenir une certification qui sera acceptable aux autorit\u00e9s jordaniennes.</li>\n<li>La certification requise pourrait devoir \u00eatre endoss\u00e9e par l'ambassade de la Jordanie. L'exportateur/importateur est responsable de prendre les mesures n\u00e9cessaires pour s'assurer que les exigences jordaniennes en la mati\u00e8re soient rencontr\u00e9es \u00e0 la satisfaction des autorit\u00e9s du pays importateur.</li>\n<li>L'exportateur et l'importateur sont responsables de s'assurer que les produits portent une \u00e9tiquette conforme aux exigences jordaniennes, par exemple, \u00e9tiquetage en langue arabe.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}