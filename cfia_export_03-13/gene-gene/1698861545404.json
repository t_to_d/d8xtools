{
    "dcr_id": "1698861545404",
    "title": {
        "en": "What to look for when buying fish and seafood",
        "fr": "Ce qu'il faut v\u00e9rifier quand on ach\u00e8te du poisson et des fruits de mer"
    },
    "html_modified": "2024-03-12 3:59:35 PM",
    "modified": "2023-12-14",
    "issued": "2023-12-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/before_buying_seafood_1698861545404_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/before_buying_seafood_1698861545404_fra"
    },
    "ia_id": "1698861545794",
    "parent_ia_id": "1698844888410",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1698844888410",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "What to look for when buying fish and seafood",
        "fr": "Ce qu'il faut v\u00e9rifier quand on ach\u00e8te du poisson et des fruits de mer"
    },
    "label": {
        "en": "What to look for when buying fish and seafood",
        "fr": "Ce qu'il faut v\u00e9rifier quand on ach\u00e8te du poisson et des fruits de mer"
    },
    "templatetype": "content page 1 column",
    "node_id": "1698861545794",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1698844887535",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/food-fraud/fish-labelling-and-traceability/what-to-look-for/",
        "fr": "/etiquetage-des-aliments/fraude-alimentaire/etiquetage-et-tracabilite-du-poisson/ce-qu-il-faut-verifier/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "What to look for when buying fish and seafood",
            "fr": "Ce qu'il faut v\u00e9rifier quand on ach\u00e8te du poisson et des fruits de mer"
        },
        "description": {
            "en": "Canada\u2019s labelling requirements are different for fresh fish from a retailer or restaurant versus what you might find for prepackaged fish.",
            "fr": "Les exigences du Canada en mati\u00e8re d\u2019\u00e9tiquetage sont diff\u00e9rentes selon qu\u2019il s\u2019agit de poisson frais vendu par un d\u00e9taillant ou un restaurant, ou de poisson pr\u00e9emball\u00e9."
        },
        "keywords": {
            "en": "fish, seafood, labelling, traceability, food, fraud",
            "fr": "\u00e9tiquetage, tra\u00e7abilit\u00e9, poisson, fruits de mer, fraude, alimentaire"
        },
        "dcterms.subject": {
            "en": "food labelling,seafood",
            "fr": "\u00e9tiquetage des aliments,fruits de mer"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-12-14",
            "fr": "2023-12-14"
        },
        "modified": {
            "en": "2023-12-14",
            "fr": "2023-12-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "What to look for when buying fish and seafood",
        "fr": "Ce qu'il faut v\u00e9rifier quand on ach\u00e8te du poisson et des fruits de mer"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Fish and seafood mislabelling and species substitution may result in <a href=\"/food-labels/food-fraud/what-is-food-fraud/eng/1648661693364/1648661694161\">food fraud</a>. Examples include labelling farmed Salmon as wild or claiming a fish is Cod when it's really Pollock. In Canada, food labels and advertising must be truthful and not misleading. When food is misrepresented, it prevents consumers from making an informed choice, and can create an uneven and unfair market. Industry is responsible for properly representing and labelling food products and providing consumers with information that is not false or misleading.</p>\n\n<p>Traceability requirements are in place so that unsafe foods can quickly be removed from the market through recalls, protecting the health of Canadians. They also allow the CFIA to verify that foods and ingredients are not being misrepresented to consumers.</p>\n\n<p>Canada is recognized as having one of the best food safety systems in the world, including a robust approach to ensuring that consumers have access to high-quality, safe, and healthy fish and seafood. Learn more on <a href=\"/food-labels/food-fraud/fish-labelling-and-traceability/our-plan/eng/1698863128939/1698863129318\">Canada's plan when it comes to fish traceability and labelling accuracy</a>.</p>\n\n<h2>Understand the information on the label</h2>\n\n<p>Canada's labelling requirements are different for fresh fish from a retailer or restaurant versus what you might find for fish prepackaged at other levels of trade. Wherever you buy your fish and seafood, Canadian law requires all label information to be truthful and not misleading. <a href=\"/food-labels/labelling/industry/bilingual-food-labelling/eng/1627499530063/1627499821742\">Bilingual labelling</a> may be required, based on where the food is prepared and sold.</p>\n\n<h3>Fresh fish packaged at retail</h3>\n\n<p>Required:</p>\n\n<ul>\n<li>Type of fish (for example, common name)</li>\n<li>Net quantity</li>\n<li>Date label (best before or packaged on)</li>\n<li>Name and principal place of business by or for whom it was manufactured or produced</li>\n<li>List any added ingredients or allergens</li>\n</ul>\n\n<p>Can be voluntarily provided:</p>\n\n<ul>\n<li>Country of origin</li>\n<li>Species name</li>\n<li>Claims (for example, fresh, catch method, catch location) and certification labels (for example, sustainable)</li>\n<li>Nutrition information</li>\n</ul>\n\n<h3>Prepackaged fish (other than packaged at retail)</h3>\n\n<p>Required:</p>\n\n<ul>\n<li>Type of fish (that is, common name)</li>\n<li>Net quantity</li>\n<li>List of ingredients and allergens</li>\n<li>Best before (if shelf life <strong>is \u2264</strong> 90 days)</li>\n<li>Country of origin</li>\n<li>Nutrition labelling for multi-ingredient fish products</li>\n<li>Name and principal place of business by or for whom it was manufactured or produced</li>\n<li><a href=\"/food-safety-for-industry/traceability/lot-code/eng/1607618442777/1607618443168\">Lot Code</a></li>\n</ul>\n\n<p>Can be voluntarily provided:</p>\n\n<ul>\n<li>Claims (for example, fresh) and certification labels (for example, sustainable)</li>\n<li>Best before if shelf life is <strong>more</strong> than 90 days</li>\n</ul>\n\n<p>You can use the <a href=\"/food-labels/labelling/industry/fish/list/eng/1352923480852/1352923563904\">CFIA fish list</a> to learn more about the commonly used and scientific names of fish species.</p>\n\n<p>Additional <a href=\"/food-labels/labelling/industry/fish/eng/1630326254350/1630326374266\">labelling requirements for fish and fish products</a> may apply depending on the product.</p>\n\n<h2>More information</h2>\n\n<ul>\n<li><a href=\"/food-labels/food-fraud/what-is-food-fraud/eng/1648661693364/1648661694161\">What is food fraud</a></li>\n<li><a href=\"/inspect-and-protect/science-and-innovation/putting-fish-dna-to-the-test/eng/1661442399060/1661442399774\">Putting fish DNA to the test</a></li>\n<li><a href=\"/food-labels/labelling/industry/fish/list/eng/1352923480852/1352923563904\">CFIA fish list</a></li>\n<li><a href=\"/food-labels/labelling/consumers/eng/1400426541985/1400455563893\">Food labelling for consumers</a></li>\n</ul>\n\n<p>If you suspect a food is mislabelled, tell the retailer or <a href=\"/food-safety-for-consumers/where-to-report-a-complaint/eng/1364500149016/1364500195684\">report it to the CFIA</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'\u00e9tiquetage trompeur du poisson et des fruits de mer et la substitution d'esp\u00e8ces peuvent se traduire par une <a href=\"/etiquetage-des-aliments/fraude-alimentaire/qu-est-ce-que-la-fraude-alimentaire/fra/1648661693364/1648661694161\">fraude alimentaire</a>. Par exemple, on peut \u00e9tiqueter le saumon d'\u00e9levage comme \u00e9tant du saumon sauvage ou pr\u00e9tendre qu'un poisson est de la morue alors qu'il s'agit r\u00e9ellement de goberge. Au Canada, les \u00e9tiquettes et les publicit\u00e9s alimentaires doivent \u00eatre v\u00e9ridiques et non trompeuses. La repr\u00e9sentation trompeuse des aliments emp\u00eache la consommatrice ou le consommateur de faire un choix \u00e9clair\u00e9 et peut mener \u00e0 la cr\u00e9ation d'un march\u00e9 in\u00e9gal et injuste. L'industrie est charg\u00e9e de repr\u00e9senter et d'\u00e9tiqueter correctement les produits alimentaires et de fournir aux consommateurs des renseignements qui ne sont ni mensongers ni trompeurs.</p>\n\n<p>Des exigences en mati\u00e8re de tra\u00e7abilit\u00e9 sont en place de mani\u00e8re \u00e0 ce que les aliments insalubres puissent rapidement \u00eatre retir\u00e9s du march\u00e9 par le biais de rappels, afin de prot\u00e9ger la sant\u00e9 de la population canadienne. Ces exigences permettent \u00e9galement \u00e0 l'ACIA de v\u00e9rifier que les aliments et leurs ingr\u00e9dients ne font pas l'objet de fausses d\u00e9clarations \u00e0 l'intention des consommatrices et consommateurs.</p>\n\n<p>Le Canada est r\u00e9put\u00e9 avoir l'un des meilleurs syst\u00e8mes de salubrit\u00e9 des aliments au monde, notamment une approche rigoureuse qui permet de veiller \u00e0 ce que les consommatrices et consommateurs aient acc\u00e8s \u00e0 du poisson et \u00e0 des fruits de mer de grande qualit\u00e9, salubres et sains. Renseignez-vous sur <a href=\"/etiquetage-des-aliments/fraude-alimentaire/etiquetage-et-tracabilite-du-poisson/notre-plan/fra/1698863128939/1698863129318\">le plan du Canada en mati\u00e8re de tra\u00e7abilit\u00e9 du poisson et d'exactitude de l'\u00e9tiquetage</a>.</p>\n\n<h2>Comprendre l'information figurant sur l'\u00e9tiquette</h2>\n\n<p>Les exigences du Canada en mati\u00e8re d'\u00e9tiquetage sont diff\u00e9rentes selon qu'il s'agit de poisson frais vendu par un d\u00e9taillant ou un restaurant, ou de poisson pr\u00e9emball\u00e9 \u00e0 d'autres niveaux de commerce. La l\u00e9gislation canadienne stipule que tous les renseignements figurant sur l'\u00e9tiquette doivent \u00eatre v\u00e9ridiques et non trompeurs, et ce, peu importe o\u00f9 vous vous procurez votre poisson et vos fruits de mer. Un <a href=\"/etiquetage-des-aliments/etiquetage/industrie/etiquetage-bilingue-des-aliments/fra/1627499530063/1627499821742\">\u00e9tiquetage bilingue</a> peut \u00eatre exig\u00e9 en fonction du lieu de pr\u00e9paration et de vente des aliments.</p>\n\n<h3>Poisson frais emball\u00e9 chez le d\u00e9taillant</h3>\n\n<p>Obligatoire\u00a0:</p>\n\n<ul>\n<li>Type de poisson (nom courant)</li>\n<li>Quantit\u00e9 nette</li>\n<li>Date indiqu\u00e9e sur l'\u00e9tiquette (date \u00ab\u00a0meilleur avant\u00a0\u00bb ou date d'emballage) </li>\n<li>Nom et \u00e9tablissement principal de l'entreprise par ou pour laquelle le produit a \u00e9t\u00e9 fabriqu\u00e9 ou produit</li>\n<li>Liste des ingr\u00e9dients ajout\u00e9s ou des allerg\u00e8nes</li>\n</ul>\n\n<p>Facultatif (volontaire)\u00a0:</p>\n\n<ul>\n<li>Pays d'origine</li>\n<li>Nom de l'esp\u00e8ce</li>\n<li>All\u00e9gations (par exemple, frais, m\u00e9thode de la capture, emplacement de la capture) et certifications (par exemple, durable)</li>\n<li>Renseignements nutritionnels</li>\n</ul>\n\n<h3>Poisson pr\u00e9emball\u00e9 (autre que emball\u00e9 en d\u00e9tail)</h3>\n\n<p>Obligatoire\u00a0:</p>\n\n<ul>\n<li>Type de poisson (nom courant)</li>\n<li>Quantit\u00e9 nette</li>\n<li>Liste des ingr\u00e9dients et allergens</li>\n<li>Date \u00ab\u00a0meilleur avant\u00a0\u00bb si la dur\u00e9e de conservation <strong>est \u2264</strong>\u00a0 90 jours</li>\n<li>Pays d'origine</li>\n<li>\u00c9tiquette nutritionnelle des produits de poisson \u00e0 ingr\u00e9dients multiples</li>\n<li>Le nom et le lieu principal de l'entreprise par ou pour laquelle le produit a \u00e9t\u00e9 fabriqu\u00e9 ou produit</li>\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/tracabilite/code-de-lot/fra/1607618442777/1607618443168\">Code de lot</a></li>\n</ul>\n\n<p>Facultatif (volontaire)\u00a0:</p>\n\n<ul>\n<li>All\u00e9gations (par exemple, frais) et certifications (par exemple, durable)</li>\n<li>Date \u00ab\u00a0meilleur avant\u00a0\u00bb si la dur\u00e9e de conservation est &gt; 90 jours</li>\n</ul>\n\n<p>Vous pouvez consulter la\u00a0<a href=\"/etiquetage-des-aliments/etiquetage/industrie/poisson/fra/1630326254350/1630326374266#a2_1\">liste des poissons de l'ACIA</a>\u00a0pour en savoir davantage sur les noms couramment utilis\u00e9s et les noms scientifiques des esp\u00e8ces de poisson.</p>\n\n<p>D'autres <a href=\"/etiquetage-des-aliments/etiquetage/industrie/poisson/fra/1630326254350/1630326374266\">exigences en mati\u00e8re d'\u00e9tiquetage pour le poisson et les produits de poisson</a> peuvent s'appliquer selon le produit.</p>\n\n<h2>Plus d'information</h2>\n\n<ul>\n<li><a href=\"/etiquetage-des-aliments/fraude-alimentaire/qu-est-ce-que-la-fraude-alimentaire/fra/1648661693364/1648661694161\">Qu'est-ce que la fraude alimentaire?</a></li>\n<li><a href=\"/inspecter-et-proteger/science-et-innovation/l-adn-des-poissons-a-l-epreuve/fra/1661442399060/1661442399774\">L'ADN des poissons \u00e0 l'\u00e9preuve</a></li>\n<li><a href=\"/eng/1630326254350/1630326374266#a2_1\">Liste des poissons de l'ACIA</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/consommateurs/fra/1400426541985/1400455563893\">L'\u00e9tiquetage des aliments pour les consommatrices et consommateurs</a></li>\n</ul>\n\n<p>Si vous soup\u00e7onnez qu'un aliment est mal \u00e9tiquet\u00e9, <a href=\"/salubrite-alimentaire-pour-les-consommateurs/ou-signaler-une-plainte/fra/1364500149016/1364500195684\">signalez-le \u00e0 l'ACIA</a> au d\u00e9taillant.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}