{
    "dcr_id": "1328225508353",
    "title": {
        "en": "VB-GL-3.1: Preparation of new product licensing (registration) submissions for veterinary biologics",
        "fr": "LD-PBV-3.1\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires"
    },
    "html_modified": "2012-02-02 18:31",
    "modified": "15-2-2023",
    "issued": "2-2-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/vetbio_guide_31_1328225508353_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/vetbio_guide_31_1328225508353_fra"
    },
    "parent_ia_id": "1320704254070",
    "ia_id": "1328225600916",
    "parent_node_id": "1320704254070",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "VB-GL-3.1: Preparation of new product licensing (registration) submissions for veterinary biologics",
        "fr": "LD-PBV-3.1\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires"
    },
    "label": {
        "en": "VB-GL-3.1: Preparation of new product licensing (registration) submissions for veterinary biologics",
        "fr": "LD-PBV-3.1\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires"
    },
    "templatetype": "content page 1 column",
    "node_id": "1328225600916",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299160285341",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/veterinary-biologics/guidelines-forms/3-1/",
        "fr": "/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-1/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "VB-GL-3.1: Preparation of new product licensing (registration) submissions for veterinary biologics",
            "fr": "LD-PBV-3.1\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires"
        },
        "description": {
            "en": "The purpose of this guideline is to provide information to Canadian veterinary biologics (VB) manufacturers regarding the preparation and submission of documents for the licensing (registration) of VB in Canada.",
            "fr": "Ces lignes directrices ont \u00e9t\u00e9 pr\u00e9par\u00e9es comme guide de travail pour les fabricants canadiens de produits biologiques v\u00e9t\u00e9rinaires (PBV) qui doivent pr\u00e9parer et pr\u00e9senter une demande d'homologation (d'enregistrement) de PBV au Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, veterinary biologics, licensing requirements, checklists, guidelines",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glements sur la sant\u00e9 des animaux, produits biologiques v\u00e9t\u00e9rinaires, d\u00e9livrance des permis, lignes directrices, listes de contr\u00f4le"
        },
        "dcterms.subject": {
            "en": "inspection,veterinary medicine,standards,regulation,animal health",
            "fr": "inspection,m\u00e9decine v\u00e9t\u00e9rinaire,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-02 18:31:51",
            "fr": "2012-02-02 18:31:51"
        },
        "modified": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "type": {
            "en": "reference material,standard,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,norme,politique"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "VB-GL-3.1: Preparation of new product licensing (registration) submissions for veterinary biologics",
        "fr": "LD-PBV-3.1\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=29#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2>Guidelines</h2>\n<ul>\n<li><a href=\"/animal-health/veterinary-biologics/guidelines-forms/3-1/3-1-1e/eng/1328588172336/1328588294122\">VB-GL-3.1.1: Preparation of new product licensing (registration) submissions for veterinary biologics manufactured in Canada</a></li>\n<li><a href=\"/animal-health/veterinary-biologics/guidelines-forms/3-1/3-1-2e/eng/1328588942405/1328589059540\">VB-GL-3.1.2: Preparation of new product licensing (registration) submissions for veterinary biologics manufactured and/or licensed in the United States</a></li>\n<li><a href=\"/animal-health/veterinary-biologics/guidelines-forms/3-1/3-1-3e/eng/1328589715402/1328589788740\">VB-GL-3.1.3: Preparation of new product licensing (registration) submissions for veterinary biologics manufactured in foreign countries other than the United States</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=29#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2>Lignes directrices</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-1/3-1-1f/fra/1328588172336/1328588294122\">LD-PBV-3.1.1\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires fabriqu\u00e9s au Canada</a></li>\n<li><a href=\"/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-1/3-1-2f/fra/1328588942405/1328589059540\">LD-PBV-3.1.2\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires fabriqu\u00e9s et/ou homologu\u00e9s aux \u00c9tats-Unis</a></li>\n<li><a href=\"/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-1/3-1-3f/fra/1328589715402/1328589788740\">LD-PBV-3.1.3\u00a0: Pr\u00e9paration des demandes d'homologation (d'enregistrement) de nouveaux produits biologiques v\u00e9t\u00e9rinaires fabriqu\u00e9s dans un pays \u00e9tranger autre que les \u00c9tats-Unis</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}