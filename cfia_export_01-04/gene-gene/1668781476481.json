{
    "dcr_id": "1668781476481",
    "title": {
        "en": "Join the fight against antimicrobial resistance",
        "fr": "Joignez-vous \u00e0 la lutte contre la r\u00e9sistance aux antimicrobiens"
    },
    "html_modified": "18-11-2022",
    "modified": "18-11-2022",
    "issued": "18-11-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/ins_pro_join_agaisnt_antimicrobial_1668781476481_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/ins_pro_join_agaisnt_antimicrobial_1668781476481_fra"
    },
    "parent_ia_id": "1565298312402",
    "ia_id": "1668781477221",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Join the fight against antimicrobial resistance",
        "fr": "Joignez-vous \u00e0 la lutte contre la r\u00e9sistance aux antimicrobiens"
    },
    "label": {
        "en": "Join the fight against antimicrobial resistance",
        "fr": "Joignez-vous \u00e0 la lutte contre la r\u00e9sistance aux antimicrobiens"
    },
    "templatetype": "content page 1 column",
    "node_id": "1668781477221",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1565298312168",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/animal-health/antimicrobial-resistance/",
        "fr": "/inspecter-et-proteger/sante-animale/resistance-aux-antimicrobiens/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Join the fight against antimicrobial resistance",
            "fr": "Joignez-vous \u00e0 la lutte contre la r\u00e9sistance aux antimicrobiens"
        },
        "description": {
            "en": "Antimicrobial drugs are sometimes overused and misused in medicine, the agri-food industry and even cleaning products. Follow these tips to keep yourself, your animals and the environment healthy.",
            "fr": "Les antimicrobiens sont parfois trop ou mal utilis\u00e9s en m\u00e9decine ainsi que dans l\u2019industrie agroalimentaire et m\u00eame dans les produits de nettoyage. Suivez ces conseils pour assurer votre sant\u00e9, celle des animaux et la protection de l\u2019environnement."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, CFIA, CFIA veterinarian, Celebrating Animal Health Week",
            "fr": "Agence canadienne d'inspection des aliments, ACIA, v\u00e9t\u00e9rinaire de l'ACIA, Semaine de la sant\u00e9 animale"
        },
        "dcterms.subject": {
            "en": "food,food inspection,veterinary medicine,scientific research,animal health,sciences",
            "fr": "aliment,inspection des aliments,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sant\u00e9 animale,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-11-18",
            "fr": "2022-11-18"
        },
        "modified": {
            "en": "2022-11-18",
            "fr": "2022-11-18"
        },
        "type": {
            "en": "news publication",
            "fr": "publication d'information"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Join the fight against antimicrobial resistance",
        "fr": "Joignez-vous \u00e0 la lutte contre la r\u00e9sistance aux antimicrobiens"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_join_agaisnt_antimicrobial_600x600_1668782483931_eng.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>Antimicrobial resistance is an ongoing threat to public health in Canada and around the world.</p>\n\n<p>It happens when microbes (like bacteria, viruses, fungi and parasites) adapt and make antimicrobial drugs less effective, or not effective at all. This means the drugs can't stop or slow down microbial growth to properly treat infections.</p>\n\n<p>Antimicrobial drugs are sometimes overused and misused in human medicine, animal medicine, the agri-food industry and even cleaning products.</p>\n\n<p>People and animals can be exposed to these resistant antimicrobials and microbes unintentionally, for example when they enter the air, water, soil or living organisms. This is also called environment contamination.</p>\n\n<h2>A global concern</h2>\n\n<p>Every year, the international community comes together to mark <a href=\"https://www.woah.org/en/event/world-antimicrobial-awareness-week-2022/\">World Antimicrobial Awareness Week</a>. The 2022 theme is <i>Preventing antimicrobial resistance together</i>.</p>\n\n<p>The Canadian Food Inspection Agency (CFIA) contributes to the Government of Canada's <a href=\"https://www.canada.ca/en/health-canada/services/publications/drugs-health-products/tackling-antimicrobial-resistance-use-pan-canadian-framework-action.html\">Tackling Antimicrobial Resistance and Antimicrobial Use: A Pan-Canadian Framework for Action</a>, which outlines how federal departments and agencies are making progress when it comes to surveillance, stewardship, infection prevention, and control and innovation.</p>\n\n<h2>What can you do?</h2>\n\n<p>While scientists, policy makers and industry are making progress towards reducing the burden of antimicrobial resistance, everyone has a role to play.</p>\n\n<p>Staying informed and taking small actions on a daily basis can reduce the risks\u2014and the need for antimicrobials in the first place.</p>\n\n<p>If you care for animals or have pets at home, here are tips for joining the global fight against antimicrobial resistance.</p>\n\n<h3>Tip 1: Keep a watchful eye</h3>\n\n<p>Regularly keep an eye on how animals look and behave to help protect their health.</p>\n\n<ul class=\"lst-spcd\">\n<li>If you notice any cuts or scrapes, look for signs of infection and changes in behaviour. Your vigilance can prevent the infection or illness from getting worse.</li>\n<li>If you notice any signs of disease, consult with a veterinarian as soon as possible. </li>\n</ul>\n\n<h3>Tip 2: Practice good biosecurity</h3>\n\n<p>Whether you look after a pet or a whole farm, practicing good biosecurity (like handwashing and avoiding exposure to wild animals) reduces the risk of illness and infection.</p>\n\n<ul class=\"lst-spcd\">\n<li>If you're a farm operator, consult the CFIA's guidance on <a href=\"/animal-health/terrestrial-animals/biosecurity/eng/1299868055616/1320534707863\">animal biosecurity</a>.</li>\n<li>If you're a pet owner, wash your hands for 20 seconds with soap and water immediately before and after handling your pet's food. Limit exposure to wild animals and clean litterboxes and rest areas properly.</li>\n</ul>\n\n<h3>Tip 3: Vaccinate when possible</h3>\n\n<p>Vaccines help provide immunity against a disease. Vaccination reduces the chance that an animal becomes ill and the likelihood that it will require antimicrobials in the future.</p>\n\n<ul class=\"lst-spcd\">\n<li>Animals diagnosed with preventable diseases can become very ill, which could lead to a decrease in quality of life and even death.</li>\n<li>Ensure your animals' vaccinations are up to date.</li>\n<li>Ask your veterinarian about flea, tick, and intestinal parasite-preventive medications.</li>\n</ul>\n\n<h3>Tip 4: Consult a veterinarian</h3>\n\n<p>Regular visits with a veterinarian make it less likely that your animal will need to be treated with antimicrobials in the future.</p>\n\n<ul class=\"lst-spcd\">\n<li>Consult a veterinarian before treating your animal for any injury or illness.</li>\n<li>When it comes to medications, use as directed, properly dispose of any unused amounts and do not reuse.</li>\n</ul>\n\n<p>By using the tips above, you can help preserve the effectiveness of treatment options for animals (and people) when they're most needed.</p>\n\n<h2>Learn more</h2>\n\n<ul>\n<li><a href=\"/science-and-research/our-research-and-publications/science-fact-sheet-antimicrobial-resistance/eng/1509647559865/1509647560617\">Science fact sheet: Antimicrobial resistance</a></li>\n<li><a href=\"/animal-health/livestock-feeds/antimicrobials-in-animals/eng/1521478929119/1521478929658\">How CFIA\u00a0is contributing to the responsible use of medically important antimicrobials in animals</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/publications/drugs-health-products/federal-action-plan-antimicrobial-resistance-canada.html\">Federal Action Plan on Antimicrobial Resistance and Use in Canada: Building on the Federal Framework for Action</a> (Health Canada)</li>\n<li><a href=\"https://www.canada.ca/en/public-health/services/antibiotic-antimicrobial-resistance/prevention-antibiotic-resistance.html\">Antibiotic (antimicrobial) resistance: Protecting yourself and your family</a> (Canada.ca)</li>\n</ul>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263070682\"></div>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_join_agaisnt_antimicrobial_600x600_1668782483931_fra.jpg\" class=\"img-thumbnail img-responsive\" alt=\"\">\n</div>\n\n<p>La r\u00e9sistance aux antimicrobiens constitue une menace persistante pour la sant\u00e9 publique au Canada et partout dans le monde.</p>\n\n<p>Elle se produit lorsque les microbes (comme les bact\u00e9ries, les virus, les champignons et les parasites) s'adaptent et rendent les m\u00e9dicaments antimicrobiens moins efficaces ou compl\u00e8tement inefficaces. Cela signifie que les m\u00e9dicaments ne peuvent pas arr\u00eater ou ralentir la croissance microbienne pour traiter correctement les infections.</p>\n\n<p>Les antimicrobiens sont parfois trop ou mal utilis\u00e9s en m\u00e9decine humaine et v\u00e9t\u00e9rinaire, ainsi que dans l'industrie agroalimentaire et m\u00eame dans les produits de nettoyage.</p>\n\n<p>Les personnes et les animaux peuvent \u00eatre expos\u00e9s involontairement \u00e0 ces microbes r\u00e9sistants aux antimicrobiens, par exemple lorsqu'ils p\u00e9n\u00e8trent dans l'air, l'eau, le sol ou les organismes vivants. On parle aussi de contamination de l'environnement.</p>\n\n<h2>Une pr\u00e9occupation mondiale</h2>\n\n<p>Chaque ann\u00e9e, la communaut\u00e9 internationale se r\u00e9unit pour souligner la <a href=\"https://www.woah.org/fr/evenement/semaine-mondiale-de-sensibilisation-aux-antimicrobiens-2022/\">Semaine mondiale pour un bon usage des antimicrobiens</a>. Le th\u00e8me de 2022 est <i>Ensemble, pr\u00e9venons la r\u00e9sistance aux antimicrobiens</i>.</p>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) contribue au document du gouvernement du Canada <a href=\"https://www.canada.ca/fr/sante-canada/services/publications/medicaments-et-produits-sante/lutter-contre-resistance-antimicrobiens-optimiser-utilisation-cadre-action-pancanadien.html\">Lutter contre la r\u00e9sistance aux antimicrobiens et optimiser leur utilisation\u00a0: un cadre d'action pancanadien</a>, qui d\u00e9crit les progr\u00e8s r\u00e9alis\u00e9s par les minist\u00e8res et organismes f\u00e9d\u00e9raux en mati\u00e8re de surveillance, d'intendance, de pr\u00e9vention des infections, de lutte et d'innovation.</p>\n\n<h2>Que pouvez-vous faire?</h2>\n\n<p>Les scientifiques, les d\u00e9cideurs et l'industrie progressent vers la r\u00e9duction du fardeau que repr\u00e9sente la r\u00e9sistance aux antimicrobiens, mais chacun a un r\u00f4le \u00e0 jouer.</p>\n\n<p>Avant tout, le fait de rester inform\u00e9 et d'entreprendre de petites actions au quotidien peut r\u00e9duire les risques et le besoin d'antimicrobiens.</p>\n\n<p>Si vous prenez soin d'animaux ou avez des animaux de compagnie \u00e0 la maison, voici des conseils pour vous joindre \u00e0 la lutte mondiale contre la r\u00e9sistance aux antimicrobiens.</p>\n\n<h3>Conseil\u00a01\u00a0: Soyez attentif</h3>\n\n<p>Surveillez r\u00e9guli\u00e8rement l'apparence et le comportement des animaux pour prot\u00e9ger leur sant\u00e9.</p>\n\n<ul class=\"lst-spcd\">\n<li>Si vous remarquez des coupures ou des \u00e9raflures, recherchez des signes d'infection et des changements de comportement. Votre vigilance peut emp\u00eacher l'infection ou la maladie de s'aggraver.</li>\n<li>Si vous remarquez des signes de maladie, consultez un v\u00e9t\u00e9rinaire d\u00e8s que possible. </li>\n</ul>\n\n<h3>Conseil\u00a02\u00a0: Ayez de bonnes pratiques en mati\u00e8re de bios\u00e9curit\u00e9</h3>\n\n<p>Que vous vous occupiez d'un animal de compagnie ou de l'ensemble d'une exploitation agricole, le fait d'avoir de bonnes pratiques en mati\u00e8re de bios\u00e9curit\u00e9 (comme se laver les mains et \u00e9viter l'exposition aux animaux sauvages) r\u00e9duit le risque de maladie et d'infection.</p>\n\n<ul class=\"lst-spcd\">\n<li>Si vous \u00eates exploitant agricole, consultez les directives de l'ACIA sur la <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/fra/1299868055616/1320534707863\">bios\u00e9curit\u00e9 animale</a>.</li>\n<li>Si vous \u00eates propri\u00e9taire d'un animal de compagnie, lavez-vous les mains pendant 20\u00a0secondes avec de l'eau et du savon imm\u00e9diatement avant et apr\u00e8s avoir manipul\u00e9 les aliments de votre animal de compagnie. Limitez votre exposition aux animaux sauvages et nettoyez correctement les bacs et les aires de repos. </li>\n</ul>\n\n<h3>Conseil\u00a03\u00a0: Faire vacciner si possible</h3>\n\n<p>Les vaccins favorisent l'acquisition d'une immunit\u00e9 contre une maladie donn\u00e9e. La vaccination r\u00e9duit le risque qu'un animal tombe malade et la probabilit\u00e9 qu'il ait besoin d'antimicrobiens \u00e0 l'avenir.</p>\n\n<ul class=\"lst-spcd\">\n<li>Les animaux ayant re\u00e7u un diagnostic de maladies \u00e9vitables peuvent devenir tr\u00e8s malades, ce qui peut entra\u00eener une diminution de leur qualit\u00e9 de vie et m\u00eame la mort.</li>\n<li>Assurez-vous que la vaccination de vos animaux est \u00e0 jour.</li>\n<li>Renseignez-vous aupr\u00e8s de votre v\u00e9t\u00e9rinaire au sujet des m\u00e9dicaments contre les puces, les tiques et les parasites intestinaux. </li>\n</ul>\n\n<h3>Conseil\u00a04\u00a0: Consulter un v\u00e9t\u00e9rinaire</h3>\n\n<p>En visitant r\u00e9guli\u00e8rement le v\u00e9t\u00e9rinaire, il est moins probable que votre animal doive \u00eatre trait\u00e9 avec des antimicrobiens \u00e0 l'avenir.</p>\n\n<ul class=\"lst-spcd\">\n<li>Consultez un v\u00e9t\u00e9rinaire avant de traiter votre animal pour toute blessure ou maladie.</li>\n<li>Utilisez les m\u00e9dicaments conform\u00e9ment aux indications, \u00e9liminez correctement toute quantit\u00e9 inutilis\u00e9e et ne pas r\u00e9utilisez.</li>\n</ul>\n\n<p>En utilisant les conseils ci-dessus, vous pouvez contribuer \u00e0 pr\u00e9server l'efficacit\u00e9 des options th\u00e9rapeutiques destin\u00e9es aux animaux (et aux personnes) lorsqu'ils en ont le plus besoin.</p>\n\n<h2>Pour en savoir plus</h2>\n\n<ul>\n<li><a href=\"/les-sciences-et-les-recherches/travaux-de-recherche-et-publications/fiche-d-information-scientifique-la-resistance-aux/fra/1509647559865/1509647560617\">Fiche d'information scientifique\u00a0: La r\u00e9sistance aux antimicrobiens</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/antimicrobiens-chez-les-animaux/fra/1521478929119/1521478929658\">Comment l'ACIA contribue \u00e0 l'utilisation responsable des antimicrobiens importants sur le plan m\u00e9dical chez les animaux</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/publications/medicaments-et-produits-sante/plan-action-federal-resistance-recours-antimicrobiens-canada.html\">Plan d'action f\u00e9d\u00e9ral sur la r\u00e9sistance et le recours aux antimicrobiens au Canada\u00a0: Prolongement du cadre d'action f\u00e9d\u00e9ral</a> (Sant\u00e9 Canada) </li>\n<li><a href=\"https://www.canada.ca/fr/sante-publique/services/resistance-aux-antibiotiques-antimicrobiens/prevention-resistance-aux-antibiotiques.html\">R\u00e9sistance aux antibiotiques (antimicrobiens)\u00a0: Se prot\u00e9ger et prot\u00e9ger sa famille</a> (Canada.ca) </li>\n</ul>\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": false,
    "chat_wizard": false
}