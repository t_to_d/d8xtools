{
    "dcr_id": "1593718645505",
    "title": {
        "en": "Decision document: Vespa mandarinia (northern giant hornet)",
        "fr": "Document de d\u00e9cision\u00a0: Vespa mandarinia (frelon g\u00e9ant du nord)"
    },
    "html_modified": "7-7-2020",
    "modified": "29-7-2022",
    "issued": "6-7-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_vmandarina_decision_doc_1593718645505_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_vmandarina_decision_doc_1593718645505_fra"
    },
    "parent_ia_id": "1593714756971",
    "ia_id": "1593718645899",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Decision document: Vespa mandarinia (northern giant hornet)",
        "fr": "Document de d\u00e9cision\u00a0: Vespa mandarinia (frelon g\u00e9ant du nord)"
    },
    "label": {
        "en": "Decision document: Vespa mandarinia (northern giant hornet)",
        "fr": "Document de d\u00e9cision\u00a0: Vespa mandarinia (frelon g\u00e9ant du nord)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1593718645899",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1593714756377",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/insects/asian-giant-hornet/decision-document/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/insectes/frelon-geant-asiatique/document-de-decision/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Decision document: Vespa mandarinia (northern giant hornet)",
            "fr": "Document de d\u00e9cision\u00a0: Vespa mandarinia (frelon g\u00e9ant du nord)"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) will not regulate Vespa mandarinia (Asian giant hornet) as a quarantine pest for Canada. Restrictions will therefore not be placed on the import or movement of any commodities that may harbour the Asian giant hornet.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) ne r\u00e9glementera pas V. mandarinia comme un organisme de quarantaine au Canada. Aucune restriction ne sera donc impos\u00e9e \u00e0 l'importation ou au d\u00e9placement de marchandises susceptibles de loger le frelon g\u00e9ant asiatique."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Asian giant hornet, Vespa mandarinia, Decision document",
            "fr": "phytoravageurs, enqu\u00eates phytosanitaires, phytoparasites justiciables de quarantaine, pi\u00e9geage, Frelon g\u00e9ant asiatique,Vespa mandarinia, Document de d\u00e9cision"
        },
        "dcterms.subject": {
            "en": "insects,inspection,plant diseases,plants",
            "fr": "insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-07-07",
            "fr": "2020-07-07"
        },
        "modified": {
            "en": "2022-07-29",
            "fr": "2022-07-29"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Decision document: Vespa mandarinia (northern giant hornet)",
        "fr": "Document de d\u00e9cision\u00a0: Vespa mandarinia (frelon g\u00e9ant du nord)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><strong>Effective date: February 2020</strong></p>\n \n<h2>Decision</h2>\n\n<p>The Canadian Food Inspection Agency (CFIA) will not regulate <i lang=\"la\">Vespa mandarinia</i> (northern giant hornet, formerly known as Asian giant hornet) as a quarantine pest for Canada. Restrictions will therefore not be placed on the import or movement of any commodities that may harbour this insect.</p>\n\n<h2>Background</h2>\n\n<p>In August 2019, northern giant hornet was detected for the first time in Canada in Nanaimo, British Columbia (B.C.). The CFIA confirmed the identification. In September 2019, a northern giant hornet's nest, located as a result of public reports of the detection in Nanaimo, was destroyed by local beekeepers and B.C. government officials. Other detections have since occurred.</p>\n\n<p>The hornet is native to temperate and tropical eastern Asia. It is a predator of other wasps, large insects and honeybees. It is not known how the northern giant hornet made its way to Canada.</p>\n\n<p>The CFIA conducted a pest risk categorization on the northern giant hornet to determine the nature and level of risk it presents to plant health in Canada and to obtain information on pathways of entry. While it is known to consume fruit and sap, the northern giant hornet's main harm to plant health is anticipated to be indirect because of its highly predacious behaviour to other wasps, insects and honey bees.</p>\n\n<p>In other parts of the world, products such as empty plant pots have been suggested as potential pathways. However, evidence is generally only circumstantial. There is some evidence that the northern giant hornet could have been intentionally carried into the country. A live brood suspected of belonging to northern giant hornet was intercepted in passenger luggage at the Canadian border in 2013. The shipment was deemed non-compliant with plant protection requirements because it was not accompanied by an import permit.</p>\n\n<h2>Considerations</h2>\n\n<p>The focus of the CFIA's mandate under the <i>Plant Protection Act</i> is protecting agriculture, forestry and natural plant sectors, and the economy that relies on them, from plant pests, and in particular from quarantine pests.</p>\n\n<p>Under the <a href=\"https://www.ippc.int/static/media/files/publication/en/2019/02/1329129099_ippc_2011-12-01_reformatted.pdf\">International Plant Protection Convention (IPPC)\u00a0PDF\u00a0(104\u00a0kb)</a>, a quarantine pest is defined as a \"pest of potential economic importance to the area endangered thereby and not yet present there, or present but not widely distributed and being officially controlled\".</p>\n\n<p>Based on international obligations under the IPPC, nations that regulate an organism as a quarantine pest must put in place the necessary measures to prevent their entry into the country, as well as officially control them when present in the country.</p>\n\n<p>While both direct and indirect impacts can factor into a decision to regulate an organism as a quarantine pest, the CFIA has traditionally regulated quarantine pests based primarily on significant direct threats to plant health. This authority does not extend to human health impacts.</p>\n\n<p>The decision to regulate an organism as a quarantine pest must consider whether preventative or control measures would be feasible, effective and cost justifiable. Effective prevention depends on knowing and controlling how the pest may enter the country. High uncertainties about the pathways of entry puts into question the ability to manage this risk, and ultimately the ability and feasibility of regulating <i lang=\"la\">V. mandarinia</i> as a quarantine pest.</p>\n\n<p>In addition to preventative or control programs concerning quarantine pests, the CFIA regulates the <a href=\"/plant-health/invasive-species/plant-import/invertebrates-and-micro-organisms/eng/1528728938166/1528729097278\">intentional import and handling</a> of organisms that have the potential to cause harm to plant health. This includes indirect plant pests, such as the northern giant hornet. Individuals wishing to intentionally import this organism into Canada must <a href=\"/about-cfia/find-a-form/form-cfia-acia-5256/eng/1491930450091/1491930450578\">apply to the CFIA</a> for a plant protection permit.</p>\n\n<p><strong>For inquiries about this issue, please contact</strong> <a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\" class=\"nowrap\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><strong>Date en vigueur\u00a0: f\u00e9vrier 2020</strong></p>\n\n<h2>D\u00e9cision</h2>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) ne r\u00e9glementera pas <i lang=\"la\">Vespa mandarinia</i> (frelon g\u00e9ant du nord, anciennement nomm\u00e9 frelon g\u00e9ant asiatique) comme un organisme de quarantaine au Canada. Aucune restriction ne sera donc impos\u00e9e \u00e0 l'importation ou au d\u00e9placement de marchandises susceptibles de loger cet insecte.</p>\n\n<h2>Contexte</h2>\n\n<p>En ao\u00fbt 2019, le frelon g\u00e9ant du nord a \u00e9t\u00e9 d\u00e9tect\u00e9 pour la premi\u00e8re fois au Canada \u00e0 Nanaimo, en Colombie-Britannique (C.-B.). L'identification a \u00e9t\u00e9 confirm\u00e9e par l'ACIA. En septembre 2019, un nid de frelons g\u00e9ants asiatiques, rep\u00e9r\u00e9 \u00e0 la suite de rapports publics de d\u00e9tection \u00e0 Nanaimo, a \u00e9t\u00e9 d\u00e9truit par des apiculteurs locaux et des repr\u00e9sentants du gouvernement de la C.-B. D\u2019autres d\u00e9tections ont suivi.</p>\n\n<p>Le frelon g\u00e9ant du nord est indig\u00e8ne des r\u00e9gions temp\u00e9r\u00e9es et tropicales de l'Asie orientale. Il s'agit d'un pr\u00e9dateur d'autres gu\u00eapes, de gros insectes et d'abeilles domestiques. On ignore comment le frelon g\u00e9ant du nord s'est retrouv\u00e9 au Canada.</p>\n\n<p>L'ACIA a proc\u00e9d\u00e9 \u00e0 une classification du risque phytosanitaire sur le frelon g\u00e9ant du nord afin de d\u00e9terminer la nature et le niveau de risque phytosanitaire que celui-ci pr\u00e9sente au Canada, ainsi que pour obtenir des renseignements sur les voies d'entr\u00e9e. Bien qu'on sache qu'il consomme des fruits et de la s\u00e8ve, on s'attend \u00e0 ce que le frelon g\u00e9ant du nord pr\u00e9sente un risque phytosanitaire indirect en raison de son comportement tr\u00e8s pr\u00e9dateur envers les autres gu\u00eapes, les insectes et les abeilles domestiques.</p>\n\n<p>Dans d'autres parties du monde, il a \u00e9t\u00e9 sugg\u00e9r\u00e9 que des produits tels que des pots \u00e0 fleurs vides puissent \u00eatre des voies potentielles. Cependant, les preuves ne sont g\u00e9n\u00e9ralement que circonstancielles. Il existe des preuves que le frelon g\u00e9ant du nord pourrait \u00eatre introduit intentionnellement dans le pays. Un couvain vivant, soup\u00e7onn\u00e9 d'appartenir au frelon g\u00e9ant du nord, a \u00e9t\u00e9 intercept\u00e9 dans les bagages de passagers \u00e0 la fronti\u00e8re canadienne en 2013. L'envoi a \u00e9t\u00e9 jug\u00e9 non conforme aux exigences phytosanitaires, car il n'\u00e9tait pas accompagn\u00e9 d'un permis d'importation.</p>\n\n<h2>Aspects pris en consid\u00e9ration</h2>\n\n<p>Le mandat de l'ACIA en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> vise \u00e0 prot\u00e9ger les secteurs de l'agriculture, de la foresterie et des v\u00e9g\u00e9taux naturels, et l'\u00e9conomie qui en d\u00e9pend, contre les phytoravageurs, et en particulier contre les organismes de quarantaine.</p>\n\n<p>La <a href=\"https://www.ippc.int/static/media/files/publications/fr/2013/06/03/1034340690890_frippc_201304232117fr.pdf\">Convention internationale pour la protection des v\u00e9g\u00e9taux (CIPV)\u00a0PDF\u00a0(104\u00a0ko)</a> a d\u00e9fini un organisme de quarantaine comme \u00ab\u00a0un organisme nuisible qui a une importance \u00e9conomique potentielle pour l'\u00e9conomie de la zone menac\u00e9e et qui n'est pas encore pr\u00e9sent dans cette zone ou bien qui y est pr\u00e9sent mais n'y est pas largement diss\u00e9min\u00e9 et fait l'objet d'une lutte officielle\u00a0\u00bb.</p>\n\n<p>Sur la base des obligations internationales d\u00e9coulant de la CIPV, les pays qui r\u00e9glementent un organisme comme organisme de quarantaine doivent mettre en place les mesures n\u00e9cessaires pour emp\u00eacher son entr\u00e9e dans le pays, ainsi que pour le contr\u00f4ler officiellement lorsqu'il est pr\u00e9sent dans le pays.</p>\n\n<p>Bien que les r\u00e9percussions directes et indirectes puissent entrer en ligne de compte dans la d\u00e9cision de r\u00e9glementer un organisme comme organisme de quarantaine, l'ACIA a toujours r\u00e9glement\u00e9 les organismes de quarantaine en se basant principalement sur les menaces phytosanitaires directes qui sont importantes. Ce pouvoir ne s'\u00e9tend pas aux r\u00e9percussions sur la sant\u00e9 humaine.</p>\n\n<p>La d\u00e9cision de r\u00e9glementer un organisme comme organisme de quarantaine doit tenir compte de la faisabilit\u00e9, de l'efficacit\u00e9 et des co\u00fbts d\u00e9coulant des mesures de pr\u00e9vention ou de contr\u00f4le. Pr\u00e9venir de fa\u00e7on efficace d\u00e9pend du niveau auquel on connaisse et on puisse contr\u00f4ler les moyens d'entr\u00e9e d'un phytoravageur au pays. Un degr\u00e9 \u00e9lev\u00e9 d'incertitude concernant les voies d'entr\u00e9e remet en question la g\u00e9rabilit\u00e9 du risque et, en fin de compte, la capacit\u00e9 et la faisabilit\u00e9 de r\u00e9glementer <i lang=\"la\">V. mandarinia</i> comme organisme de quarantaine.</p>\n\n<p>En plus des programmes de pr\u00e9vention ou de contr\u00f4le relatifs aux organismes de quarantaine, l'ACIA r\u00e9glemente l'<a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/invertebres-et-de-microorganismes/fra/1528728938166/1528729097278\">importation intentionnelle et la manipulation</a> d'organismes susceptibles de poser des risques phytosanitaires. Cela inclut les phytoravageurs indirects, tels que le frelon g\u00e9ant du nord. Les personnes qui souhaitent importer intentionnellement cet organisme au Canada doivent <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5256/fra/1491930450091/1491930450578\">pr\u00e9senter une demande \u00e0 l'ACIA</a> pour obtenir un permis phytosanitaire.</p>\n\n<p><strong>Pour toute demande de renseignements \u00e0 ce sujet, veuillez faire parvenir un courriel \u00e0 l'adresse suivante\u00a0:</strong> <a href=\"mailto:CFIA-IAS_ACIA-EEE@inspection.gc.ca\" class=\"nowrap\">CFIA-IAS_ACIA-EEE@inspection.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}