{
    "dcr_id": "1591285963863",
    "title": {
        "en": "Crystal shards in wine (wine diamonds)",
        "fr": "Cristaux dans le vin (diamants du vin)"
    },
    "html_modified": "2024-02-13 9:54:36 AM",
    "modified": "2020-07-27",
    "issued": "2020-07-16",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/crystal_shards_in_wine_wine_diamonds_1591285963863_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/crystal_shards_in_wine_wine_diamonds_1591285963863_fra"
    },
    "ia_id": "1591286095609",
    "parent_ia_id": "1592232381996",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1592232381996",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Crystal shards in wine (wine diamonds)",
        "fr": "Cristaux dans le vin (diamants du vin)"
    },
    "label": {
        "en": "Crystal shards in wine (wine diamonds)",
        "fr": "Cristaux dans le vin (diamants du vin)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1591286095609",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1592232381574",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/specific-products-and-risks/commonly-occurring-issues-in-food/wine-diamonds/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/produits-et-risques/problemes-courants-dans-les-aliments/diamants-du-vin/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Crystal shards in wine (wine diamonds)",
            "fr": "Cristaux dans le vin (diamants du vin)"
        },
        "description": {
            "en": "Occasionally, you may find tiny crystal shards in your wine that you could mistake for small pieces of broken glass. These are called wine diamonds, otherwise known as tartrates. Although they may leave a slight gritty texture on your tongue, they are harmless and safe to consume.",
            "fr": "Parfois, vous pouvez trouver de minuscules \u00e9clats de cristal dans votre vin que vous pourriez confondre avec de petits morceaux de verre cass\u00e9. On les appelle diamants du vin, autrement appel\u00e9s tartrates. M\u00eame s'ils peuvent laisser une l\u00e9g\u00e8re texture grasse sur votre langue, ils sont inoffensifs et sans danger \u00e0 consommer."
        },
        "keywords": {
            "en": "food safety tips, labels, food recalls, food borne illess, food packaging, storage, food handling, specific products, risks, Crystal shards in wine, wine diamonds",
            "fr": "Conseils sur la salubrit&#233; des aliments, &#233;tiquettes, Rappels d&#39;aliments, toxi-infections alimentaire, emballage, entreposage, Produits, risques sp&#233;cifiques, &#201;tiquetage, Cristaux dans le vin, diamants du vin"
        },
        "dcterms.subject": {
            "en": "food,consumers,labelling,food labeling,government information,inspection,food inspection,consumer protection,government publication,food safety",
            "fr": "aliment,consommateur,\u00e9tiquetage,\u00e9tiquetage des aliments,information gouvernementale,inspection,inspection des aliments,protection du consommateur,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-07-16",
            "fr": "2020-07-16"
        },
        "modified": {
            "en": "2020-07-27",
            "fr": "2020-07-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public",
            "fr": "grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Crystal shards in wine (wine diamonds)",
        "fr": "Cristaux dans le vin (diamants du vin)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Occasionally, you may find tiny crystal shards in your wine that you could mistake for small pieces of broken glass. These are called wine diamonds, otherwise known as tartrates. Although they may leave a slight gritty texture on your tongue, they are harmless and safe to consume.</p>\n\n<div class=\"pull-right mrgn-lft-md col-sm-4\"> <p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/crystal_shards_in_wine_wine_diamonds_img1_1591808899950_eng.jpg\" class=\"img-responsive\" alt=\"wine diamonds next to a ruler measuring less than 3 centimetres\"></p> </div>\n\n<h2>On this page</h2>\n<ul>\n<li><a href=\"#a1\">Occurrence of wine diamonds</a></li>\n<li><a href=\"#a2\">Product safety</a></li>\n<li><a href=\"#a3\">How to prevent wine diamonds</a></li>\n<li><a href=\"#a4\">What to do if you find wine diamonds</a></li>\n</ul>\n<h2 id=\"a1\">Occurrence of wine diamonds</h2>\n<p>Wine diamonds are tiny, crystalline deposits found in some wine. They can occur if the wine is exposed to temperatures below 4\u00b0C, when potassium and tartaric acid, both naturally occurring products of grapes, bind together to form a crystal. The crystals can be found at the bottom of a wine bottle or cork.</p>\n<p>The presence of wine diamonds is viewed by many winemakers as a sign of high quality because it means that the wine was not over processed. Wine diamonds do not impact the taste of the wine.</p>\n<h2 id=\"a2\">Product safety</h2>\n<p>Although wine diamonds may leave a slight gritty texture on your tongue, they do not pose a health risk and are safe to consume.</p>\n<h2 id=\"a3\">How to prevent wine diamonds</h2>\n<p>Commercial wineries can prevent wine diamonds through a process called cold stabilization. After fermentation, they drop the temperature of the wine to near freezing for 3 to 4 days. This causes the wine diamonds to separate from the wine and stick to the sides of the holding vessel. When the wine is drained from the vessels, the wine diamonds are left behind.</p>\n<h2 id=\"a4\">What to do if you find wine diamonds</h2>\n<p>Wine diamonds are completely natural, harmless and safe to consume. However, you  can take the following measures if you find them in your wine:</p>\n<ul>\n<li>On a cork: simply wipe the wine diamonds away with a cloth</li>\n<li>In the bottle: decant the wine by pouring through a cheesecloth</li>\n</ul>\n<p>If you think the crystals may not be wine diamonds, you should <a href=\"/food-safety-for-consumers/where-to-report-a-complaint/eng/1364500149016/1364500195684\">report it to the CFIA</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Parfois, vous pouvez trouver de minuscules \u00e9clats de cristal dans votre vin que vous pourriez confondre avec de petits morceaux de verre cass\u00e9. On les appelle diamants du vin, autrement appel\u00e9s tartrates. M\u00eame s'ils peuvent laisser une l\u00e9g\u00e8re texture granuleuse sur votre langue, ils sont inoffensifs et peuvent \u00eatre consomm\u00e9s sans danger.</p>\n\n<div class=\"pull-right mrgn-lft-md col-sm-4\"> <p><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/crystal_shards_in_wine_wine_diamonds_img1_1591808899950_eng.jpg\" class=\"img-responsive\" alt=\"diamants du vin \u00e0 c\u00f4t\u00e9 d'une r\u00e8gle mesurant moins de 3 centim\u00e8tres\"></p> </div>\n\n<h2>Sur cette page</h2>\n<ul>\n<li><a href=\"#a1\">Occurrence de diamants du vin</a></li>\n<li><a href=\"#a2\">Salubrit\u00e9 des produits</a></li>\n<li><a href=\"#a3\">Comment pr\u00e9venir les diamants du vin</a></li>\n<li><a href=\"#a4\">Que faire si vous trouvez des diamants du vin</a></li>\n</ul>\n<h2 id=\"a1\">Occurrence de diamants du vin</h2>\n<p>Les diamants du vin sont de minuscules d\u00e9p\u00f4ts cristallins que l'on trouve dans certains vins. Ils peuvent se produire si le vin est expos\u00e9 \u00e0 des temp\u00e9ratures inf\u00e9rieures \u00e0 4\u00a0\u00b0C, lorsque le potassium et l'acide tartrique, produits naturels du raisin, se lient ensemble pour former un cristal. Les cristaux se trouvent au fond d'une bouteille de vin ou sur un bouchon de li\u00e8ge.</p>\n<p>De nombreux vignerons consid\u00e8rent la pr\u00e9sence de diamants comme un signe de haute qualit\u00e9, car cela signifie que le vin n'a pas \u00e9t\u00e9 trop transform\u00e9. Les diamants du vin n'ont pas d'incidence sur le go\u00fbt du vin.</p>\n<h2 id=\"a2\">Salubrit\u00e9 des produits</h2>\n<p>M\u00eame si les diamants du vin peuvent laisser une texture l\u00e9g\u00e8rement granuleuse sur votre langue, ils ne pr\u00e9sentent aucun risque pour la sant\u00e9 et peuvent \u00eatre consomm\u00e9s sans danger.</p>\n<h2 id=\"a3\">Comment pr\u00e9venir les diamants du vin</h2>\n<p>Les \u00e9tablissements vinicoles commerciaux peuvent pr\u00e9venir les diamants du vin par un processus appel\u00e9 stabilisation \u00e0 froid. Apr\u00e8s la fermentation, ils laissent descendre la temp\u00e9rature du vin \u00e0 la limite du point de cong\u00e9lation pendant trois \u00e0 quatre\u00a0jours. Les diamants du vin se s\u00e9parent alors du vin et se collent aux parois de la cuve de stockage. Lorsque les cuves sont vid\u00e9es du vin, les diamants sont laiss\u00e9s derri\u00e8re.</p>\n<h2 id=\"a4\">Que faire si vous trouvez des diamants du vin</h2>\n<p>Les diamants du vin sont tout \u00e0 fait naturels, inoffensifs et sans danger pour la consommation. Cependant, vous pouvez prendre les mesures suivantes si vous les trouvez dans votre vin\u00a0:</p>\n<ul>\n<li>Sur un bouchon de li\u00e8ge\u00a0: essuyer simplement les diamants \u00e0 l'aide d'un chiffon.</li>\n<li>Dans la bouteille\u00a0: d\u00e9canter le vin en le versant \u00e0 travers un coton \u00e0 fromage.</li>\n</ul>\n<p>Si vous pensez que les cristaux ne sont peut-\u00eatre pas des diamants du vin, vous <a href=\"/salubrite-alimentaire-pour-les-consommateurs/ou-signaler-une-plainte/fra/1364500149016/1364500195684\">devriez signaler l'incident \u00e0 l'ACIA</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}