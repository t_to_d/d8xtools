{
    "dcr_id": "1675785682656",
    "title": {
        "en": "Share your thoughts: Winter 2023 Seed Regulatory Modernization Consultation (excluding seed potatoes)",
        "fr": "Faites-nous part de vos commentaires : Consultation sur la modernisation de la r\u00e9glementation des semences (\u00e0 l'exception des pommes de terre de semence) de l'hiver 2023"
    },
    "html_modified": "15-2-2023",
    "modified": "23-10-2023",
    "issued": "2-2-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/share_your_thoughts_winter_2023_seed_modernization_1675785682656_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/WORKAREA/aboutcfia-sujetacia/templatedata/comn-comn/gene-gene/data/share_your_thoughts_winter_2023_seed_modernization_1675785682656_fra"
    },
    "parent_ia_id": "1330978369105",
    "ia_id": "1675785683110",
    "parent_node_id": "1330978369105",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Share your thoughts: Winter 2023 Seed Regulatory Modernization Consultation (excluding seed potatoes)",
        "fr": "Faites-nous part de vos commentaires : Consultation sur la modernisation de la r\u00e9glementation des semences (\u00e0 l'exception des pommes de terre de semence) de l'hiver 2023"
    },
    "label": {
        "en": "Share your thoughts: Winter 2023 Seed Regulatory Modernization Consultation (excluding seed potatoes)",
        "fr": "Faites-nous part de vos commentaires : Consultation sur la modernisation de la r\u00e9glementation des semences (\u00e0 l'exception des pommes de terre de semence) de l'hiver 2023"
    },
    "templatetype": "content page 1 column",
    "node_id": "1675785683110",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1330978174872",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/transparency/consultations-and-engagement/completed/seed-regulatory-modernization/",
        "fr": "/a-propos-de-l-acia/transparence/consultations-et-participation/terminees/modernisation-de-la-reglementation-des-semences/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Share your thoughts: Winter 2023 Seed Regulatory Modernization Consultation (excluding seed potatoes)",
            "fr": "Faites-nous part de vos commentaires : Consultation sur la modernisation de la r\u00e9glementation des semences (\u00e0 l'exception des pommes de terre de semence) de l'hiver 2023"
        },
        "description": {
            "en": "The purpose of this consultation is to update stakeholders on progress made to date towards modernizing the Seeds Regulations.",
            "fr": "L'objectif de cette consultation est d'informer les intervenants des progr\u00e8s r\u00e9alis\u00e9s \u00e0 ce jour en vue de la modernisation du R\u00e8glement sur les semences."
        },
        "keywords": {
            "en": "information, consultation, seeds regulations, winter 2023 seed regulatory modernization consultation",
            "fr": "information, consultation, modernisation de la r\u00e9glementation des semences, R\u00e8glement sur les semences"
        },
        "dcterms.subject": {
            "en": "consumers,government information,inspection,consumer protection,government publication,food safety",
            "fr": "consommateur,information gouvernementale,inspection,protection du consommateur,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "modified": {
            "en": "2023-10-23",
            "fr": "2023-10-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Share your thoughts: Winter 2023 Seed Regulatory Modernization Consultation (excluding seed potatoes)",
        "fr": "Faites-nous part de vos commentaires : Consultation sur la modernisation de la r\u00e9glementation des semences (\u00e0 l'exception des pommes de terre de semence) de l'hiver 2023"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Current status: Closed</h2>\n\n<p>This consultation ran from February 15, 2023 to May 1, 2023.</p>\n\n<h2>About the consultation</h2>\n\n<p>The purpose of this consultation was to update stakeholders on progress made to date towards modernizing the <i>Seeds Regulations.</i> Your feedback will inform decision-making.</p>\n\n<p>This consultation was the first of two consultations planned for seed regulatory modernization (SEED-RM) related to seed crops only; it did not include updates or questions specific to seed potatoes or Part\u00a0II of the <i>Seeds Regulations</i>. </p>\n<p>A separate consultation process for the seed potato industry is planned and will include several in-person meetings with a wide cross-section of seed potato stakeholders.</p>\n\n\n<h2>Who was the focus of this consultation</h2>\n<p>The CFIA sought comments from businesses and other organizations in the farming and grain industries, as well as farmers, producers, seed growers, seed analysts, seed developers, seed conditioners, the general public and others who have an interest in the following aspects of the seed regulatory lifecycle:</p>\n<ul>\n<li>variety registration </li>\n<li>seed crop certification, including crop inspection and crop certificates </li>\n<li>harvesting, cleaning and conditioning of seed and </li>\n<li>sampling, testing and grading of seed </li>\n</ul>\n<p>The next consultation will include these and other stages of the seed regulatory lifecycle, including seed certification and labeling, sale of seed to farmers, as well as seed import and export.</p>\n<h2>What we heard</h2>\n\n<h3>Final report</h3>\n\n<ul>\n<li><a href=\"/about-cfia/transparency/consultations-and-engagement/completed/seed-regulatory-modernization/what-we-heard-winter-seed-rm-consultation/eng/1697038000571/1697038001711\">Winter 2023 Seed Regulatory Modernization Consultation (excluding seed potatoes): What we heard</a></li>\n</ul>\n\n<h2>Related information</h2>\n<ul>\n<li><a href=\"/plant-health/seeds/canada-s-current-seed-regulatory-framework-seed-re/eng/1610740002787/1610740003016\">Canada's current seed regulatory framework</a> introduces the reports available at the initiation of SEED-RM and describes in sections 2.0 and 2.1 the current seed regulatory framework for seed (excluding seed potatoes).\u00a0 It identifies the current regulations, standards and policies, and players involved at each stage of the seed regulatory lifecycle.</li>\n\n<li>The <a href=\"/plant-health/seeds/seed-regulatory-modernization/eng/1610723659167/1610723659636\">seed regulatory modernization webpage</a> provides information on the purpose of SEED-RM, the purpose of the SEED-RM working groups, scope of SEED-RM, consultation activities to date, including the Needs Assessment Survey and SEED-RM Task Teams, and information on key SEED-RM activities and next steps.</li>\n</ul>\n<h2>Contact information</h2>\n<p>Wendy Jahn<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:cfia.seedregmod-modregsem.acia@inspection.gc.ca\">cfia.seedregmod-modregsem.acia@inspection.gc.ca</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Statut actuel\u00a0: Ferm\u00e9</h2>\n\n<p>Cette consultation eu lieu du 15 f\u00e9vrier 2023 au 1 mai 2023</p>\n\n<h2>\u00c0 propos de la consultation</h2>\n\n<p>L'objectif de cette consultation \u00e9tait d'informer les intervenants des progr\u00e8s r\u00e9alis\u00e9s \u00e0 ce jour en vue de la modernisation du <i>R\u00e8glement sur les semences</i>. Vos commentaires contribueront \u00e0 la prise de d\u00e9cision. </p>\n<p>Cette consultation \u00e9tait la premi\u00e8re de deux consultations pr\u00e9vues pour la modernisation de la r\u00e9glementation des semences (MR-SEMENCES) concernant les cultures de semences seulement; elle ne comprenait pas de mises \u00e0 jour ou de questions propres aux pommes de terre de semence ou \u00e0 la partie\u00a0II du <i>R\u00e8glement sur les semences</i>. </p>\n<p>Un processus de consultation distinct pour l'industrie des pommes de terre de semence est pr\u00e9vu et comprendra plusieurs r\u00e9unions en personne avec un large \u00e9ventail d'intervenants dans le domaine des pommes de terre de semence.</p>\n\n\n<h2>Qui \u00e9tait vis\u00e9 par cette consultation</h2>\n<p>L'ACIA a sollicit\u00e9 les commentaires d'entreprises et d'autres organisations des secteurs agricole et c\u00e9r\u00e9alier, ainsi que d'agriculteurs, de producteurs, de cultivateurs de semences, d'analystes de semences, de d\u00e9veloppeurs de semences, de conditionneurs de semences, du grand public et d'autres personnes qui s'int\u00e9ressent aux aspects suivants du cycle de vie de la r\u00e9glementation des semences\u00a0:</p>\n<ul>\n<li>l'enregistrement des vari\u00e9t\u00e9s; </li>\n<li>la certification des cultures de semences, y compris l'inspection des cultures et les certificats de culture;</li>\n<li>la r\u00e9colte, le nettoyage et le conditionnement des semences; et </li>\n<li>l'\u00e9chantillonnage, les essais et le classement des semences. </li>\n</ul>\n\n<p>La prochaine consultation portera sur ces \u00e9tapes et d'autres du cycle de vie de la r\u00e9glementation des semences, y compris la certification et l'\u00e9tiquetage des semences, la vente de semences aux agriculteurs, ainsi que l'importation et l'exportation de semences.</p>\n<h2>Ce que nous avons entendu</h2>\n\n<h3>Rapports finaux</h3>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/terminees/modernisation-de-la-reglementation-des-semences/modernisation-de-la-reglementation-des-semences/fra/1697038000571/1697038001711\">Consultation sur la modernisation de la r\u00e9glementation des semences (\u00e0 l'exception des pommes de terre de semence) de l'hiver 2023\u00a0: Ce que nous avons entendu</a></li>\n</ul>\n<h2>Informations connexes</h2>\n<ul>\n<li>Le <a href=\"/protection-des-vegetaux/semences/cadre-reglementaire-actuel-du-canada-sur-les-semen/fra/1610740002787/1610740003016\">cadre r\u00e9glementaire actuel du Canada sur les semences</a> pr\u00e9sente les rapports disponibles au lancement de MR-SEMENCES et d\u00e9crit dans les sections 2.0 et 2.1 le cadre r\u00e9glementaire actuel des semences (\u00e0 l'exclusion des pommes de terre de semence). Il identifie les r\u00e8glements, normes et politiques actuels, ainsi que les acteurs impliqu\u00e9s \u00e0 chaque \u00e9tape du cycle de vie de la r\u00e9glementation des semences.</li>\n\n<li>La <a href=\"/protection-des-vegetaux/semences/modernisation-du-reglement-sur-les-semences/fra/1610723659167/1610723659636\">page Web sur la modernisation du R\u00e8glement sur les semences</a> fournit des informations sur l'objectif de MR-SEMENCES, l'objectif des groupes de travail MR-SEMENCES, la port\u00e9e de MR-SEMENCES, les activit\u00e9s de consultation \u00e0 ce jour, y compris le sondage sur l'\u00e9valuation des besoins et les \u00e9quipes de travail MR-SEMENCES, ainsi que des informations sur les activit\u00e9s cl\u00e9s de MR-SEMENCES et les prochaines \u00e9tapes.</li>\n</ul>\n\n<h2>Personne-ressource</h2>\n<p>Wendy Jahn<br>\n<br>\n<br>\n<br>\n<a href=\"mailto:cfia.seedregmod-modregsem.acia@inspection.gc.ca\">cfia.seedregmod-modregsem.acia@inspection.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}