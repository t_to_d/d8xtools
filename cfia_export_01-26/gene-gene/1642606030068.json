{
    "dcr_id": "1642606030068",
    "title": {
        "en": "Terms of reference for the Canadian Biological Control Review Committee",
        "fr": "Le mandat du Comit\u00e9 d'examen de la lutte biologique au Canada"
    },
    "html_modified": "2024-01-26 2:25:46 PM",
    "modified": "2022-01-25",
    "issued": "2022-01-25",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/bcrc_terms_1642606030068_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/bcrc_terms_1642606030068_fra"
    },
    "ia_id": "1642606030615",
    "parent_ia_id": "1514956212112",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1514956212112",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Terms of reference for the Canadian Biological Control Review Committee",
        "fr": "Le mandat du Comit\u00e9 d'examen de la lutte biologique au Canada"
    },
    "label": {
        "en": "Terms of reference for the Canadian Biological Control Review Committee",
        "fr": "Le mandat du Comit\u00e9 d'examen de la lutte biologique au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1642606030615",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1514956211166",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/biological-control-agents/bcrc/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/agents-de-lutte-biologique/celb/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Terms of reference for the Canadian Biological Control Review Committee",
            "fr": "Le mandat du Comit\u00e9 d'examen de la lutte biologique au Canada"
        },
        "description": {
            "en": "The main purpose of the Canadian Biological Control Review Committee (BCRC) is to provide science-based advice and recommendations concerning petitions for the first release in Canada of non-indigenous biological control agents (BCA) that are presented to the Canadian Food Inspection Agency (CFIA).",
            "fr": "Le principal objectif du Comit\u00e9 d'examen de la lutte biologique au Canada (CELB) consiste \u00e0 formuler des conseils et des recommandations fond\u00e9s sur la science en vue des p\u00e9titions concernant la premi\u00e8re diss\u00e9mination d'agents de lutte biologique (ALB) non indig\u00e8nes qui sont pr\u00e9sent\u00e9es \u00e0 l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "biological control agents, insects, mites, nematodes, organisms, control, weeds, insects, plant pests, Canadian Biological Control Review Committee, BCRC, terms",
            "fr": "agents de lutte biologiques, insectes, acariens, n\u00e9matodes, organismes, contr\u00f4ler, mauvaises herbes, phytoravageurs, Comit\u00e9 d'examen de la lutte biologique au Canada, CELB, termes"
        },
        "dcterms.subject": {
            "en": "government information,government publication,imports,inspection,pests,plants,policy",
            "fr": "information gouvernementale,publication gouvernementale,importation,inspection,organisme nuisible,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-01-25",
            "fr": "2022-01-25"
        },
        "modified": {
            "en": "2022-01-25",
            "fr": "2022-01-25"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Terms of reference for the Canadian Biological Control Review Committee",
        "fr": "Le mandat du Comit\u00e9 d'examen de la lutte biologique au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Mission</h2>\n\n<p>The main purpose of the Canadian Biological Control Review Committee (BCRC) is to provide science-based advice and recommendations concerning <a href=\"/plant-health/invasive-species/biological-control-agents/eng/1514956211166/1514956212112\">petitions for the first release in Canada of non-indigenous biological control agents</a> (BCA) that are presented to the Canadian Food Inspection Agency <span class=\"nowrap\">(CFIA)<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>.</span></p>\n\n<h2>Background</h2>\n\n<p>The CFIA regulates BCAs under the <i>Plant Protection Act</i>; requirements are detailed in CFIA's plant protection directive <a href=\"/plant-health/invasive-species/directives/imports/d-12-02/eng/1432586422006/1432586423037\">D-12-02: Import requirements for potentially injurious organisms (other than plants) to prevent the importation of plant pests in Canada</a>. This directive describes BCAs as potentially injurious organisms used to control other organisms considered to be pests. BCAs may be insects, mites, fungi, bacteria, viruses or nematodes that may be directly or indirectly injurious to plant health.</p>\n\n<p>All non-indigenous BCAs require approval from the CFIA before their first release into the Canadian environment. Petitions must consider potential risks to the environment and economy in Canada, and follow criteria provided in the <a href=\"https://nappo.org/english/products/regional-standards-phytosanitary-measures-rspm\">North American Plant Protection Organization's Regional Standards for Phytosanitary Measures</a>:</p>\n\n<ul>\n<li>RSPM 07: Guidelines for Petition for First Release of Non-indigenous Phytophagous Biological Control Agents, and</li>\n<li>RSPM 12: Guidelines for Petition for First Release of Non-indigenous Entomophagous Biological Control Agents</li>\n</ul>\n\n<p>While decisions of the CFIA are based on plant health impacts, the BCRC also considers impacts on humans and vertebrates as called for by RSPM 7 and 12. The results of the petition process may therefore trigger the regulatory oversight of other federal authorities.</p>\n\n<h2>Membership</h2>\n\n<p>The BCRC is composed of representatives from the following federal departments and agencies with mandates related to plant health or environmental protection (the \"member organizations\"):</p>\n\n<ul>\n<li>Agriculture and Agri-Food Canada</li>\n<li>Canadian Food Inspection Agency</li>\n<li>Pest Management Regulatory Agency\u00a0\u2013\u00a0Health Canada</li>\n<li>Natural Resources Canada</li>\n</ul>\n\n<h3>Ad-hoc participation</h3>\n\n<p>To ensure appropriate expertise (such as in taxonomy) or account for local or regional implications, other persons may be invited to participate in a review process. This could include representatives from academia, industry or federal or provincial government departments and agencies.</p>\n\n<h2>Roles and responsibilities</h2>\n\n<h3>a. Member organizations</h3>\n\n<p>Member organizations shall designate a contact person to represent their organizations on the BCRC. In response to a request to review a petition, contact persons will designate a person with the appropriate expertize from within their respective organizations that will:</p>\n\n<ul>\n<li>review the petition from their organization's perspective on biological control and applicable regulatory authorities</li>\n<li>evaluate risks to plant health and the environment, in particular as measured against the criteria provided in the RSPMs 7 and 12</li>\n<li>provide written recommendations to the BCRC Secretary before the deadline</li>\n</ul>\n\n<h3>b. Specific to each member</h3>\n\n<h4>Canadian Food Inspection Agency</h4>\n\n<ul>\n<li>Administers and enforces the <i>Plant Protection Act</i></li>\n<li>Ensures the functioning of the BCRC, including by appointing and communicating with its chair (or A/chair) and by appointing its secretary (upon recommendation of the chair)</li>\n<li>Provides the BCRC with submitted petitions for their review and recommendations</li>\n<li>Considers the recommendations of the BCRC when making determinations concerning the proposed first releases of BCAs in Canada</li>\n</ul>\n\n<h4>Agriculture and Agri-Food Canada</h4>\n\n<ul>\n<li>Supports the biocontrol-related mandate of federal authorities by enabling personnel to serve as chair and secretary (upon appointment) and providing resources that facilitate the function of the BCRC</li>\n</ul>\n\n<h4>Pest Management Regulatory Agency</h4>\n\n<ul>\n<li>Considers <i>Pest Control Product Act</i> provisions, particularly as they pertain to human health, environment and value</li>\n</ul>\n\n<h4>Natural Resources Canada</h4>\n\n<ul>\n<li>Supports the biocontrol-related mandate of federal authorities by providing resources that facilitate the function of the BCRC</li>\n</ul>\n\n<h3>c. Administrative functions</h3>\n\n<h4>Chair</h4>\n\n<ul>\n<li>Receives from the CFIA and acknowledges receipt of petitions and proposed lists of non-target species (test plants or other organisms) to be tested</li>\n<li>Reviews the petition or proposed test list, and determines the need for reviewers outside of the Member organizations</li>\n<li>Helps build consensus among reviewers, and as needed, may call meetings with interested parties to discuss petitions or related processes</li>\n<li>Reviews the recommendations of the member organizations and provides a consolidated recommendation to the CFIA</li>\n<li>Maintains records of the BCRC recommendations</li>\n</ul>\n\n<h4>Secretary</h4>\n\n<ul>\n<li>Provides reviewers with the petition and test plant lists, along with a deadline for review (normally 1 month)</li>\n<li>Receives and compiles reviews of petitions and test plant lists and forwards to the Chair</li>\n<li>Facilitates the distribution of petitions and test plant lists to member organizations</li>\n<li>Maintains a filing system and performs other archival functions for the BCRC</li>\n<li>Receives and responds to requests to extend review deadlines</li>\n</ul>\n\n<h4>Process assumptions</h4>\n\n<p>The BCRC interprets a member's or other party's lack of response to a petition as acknowledgment that they have no need to be involved.</p>\n\n<p>If a member or other party does not provide comments or request an extension to the designated deadline, the BCRC interprets this to mean the member or party does not oppose the petition.</p>\n\n<h4>Review</h4>\n\n<p>These terms of reference will be subject to revision, as needed.</p>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Footnote</h2>\n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p>Matters that fall outside the plant health mandate of the CFIA under the <i>Plant Protection Act</i> may be considered by the BCRC upon request by a regulatory organization.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd> \n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Mission</h2>\n\n<p>Le principal objectif du Comit\u00e9 d'examen de la lutte biologique au Canada (CELB) consiste \u00e0 formuler des conseils et des recommandations fond\u00e9s sur la science en vue des <a href=\"/protection-des-vegetaux/especes-envahissantes/agents-de-lutte-biologique/fra/1514956211166/1514956212112\">p\u00e9titions concernant la premi\u00e8re diss\u00e9mination d'agents de lutte biologique (ALB) non indig\u00e8nes</a> qui sont pr\u00e9sent\u00e9es \u00e0 l'Agence canadienne d'inspection des aliments <span class=\"nowrap\">(ACIA)<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup></span>.</p>\n\n<h2>Contexte</h2>\n\n<p>L'ACIA r\u00e9glemente les ALB en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>, dont les d\u00e9tails figurent dans la Directive phytosanitaire <a href=\"/protection-des-vegetaux/especes-envahissantes/directives/importation/d-12-02/fra/1432586422006/1432586423037\">D-12-02\u00a0: Exigences r\u00e9gissant l'importation d'organismes potentiellement nuisibles (autres que les v\u00e9g\u00e9taux) afin d'emp\u00eacher l'importation de phytoravageurs au Canada</a>. Cette directive d\u00e9crit les ALB comme \u00e9tant des organismes potentiellement nuisibles utilis\u00e9s pour lutter contre d'autres organismes nuisibles. Les ALB peuvent \u00eatre des insectes, des acariens, des champignons, des bact\u00e9ries, des virus ou des n\u00e9matodes pouvant nuire, directement ou indirectement, aux v\u00e9g\u00e9taux.</p>\n\n<p>Tous les ALB non indig\u00e8nes doivent \u00eatre approuv\u00e9s par l'ACIA avant leur premi\u00e8re diss\u00e9mination dans l'environnement au Canada. Les p\u00e9titions doivent tenir compte des risques pour l'environnement et l'\u00e9conomie du Canada, et r\u00e9pondre aux crit\u00e8res des <a href=\"https://nappo.org/english/products/regional-standards-phytosanitary-measures-rspm\">normes r\u00e9gionales pour les mesures phytosanitaires</a> (en anglais seulement) de l'Organisation nord-am\u00e9ricaine pour la protection des plantes (NAPPO).</p>\n\n<ul>\n<li>NRMP 07\u00a0: Guidelines for Petition for First Release of Non-indigenous Phytophagous Biological Control Agents (en anglais seulement).</li>\n<li>NRMP 12\u00a0: Guidelines for Petition for First Release of Non-indigenous Entomophagous Biological Control Agents (en anglais seulement).</li>\n</ul>\n\n<p>Les d\u00e9cisions de l'ACIA sont fond\u00e9es sur les r\u00e9percussions sur la sant\u00e9 des v\u00e9g\u00e9taux, mais le CELB tient \u00e9galement compte de l'incidence sur les humains et les vert\u00e9br\u00e9s, comme l'exigent les NRMP 7 et 12. Les r\u00e9sultats du processus de p\u00e9tition peuvent donc d\u00e9clencher le processus de surveillance r\u00e9glementaire d'autres autorit\u00e9s f\u00e9d\u00e9rales.</p>\n\n<h2>Composition</h2>\n\n<p>Le CELB est compos\u00e9 de repr\u00e9sentants des minist\u00e8res et organismes gouvernementaux f\u00e9d\u00e9raux suivants dont le mandat touche la protection des v\u00e9g\u00e9taux ou de l'environnement (appel\u00e9s \u00ab\u00a0organisations membres\u00a0\u00bb)\u00a0:</p>\n\n<ul>\n<li>Agriculture et Agroalimentaire Canada</li>\n<li>Agence canadienne d'inspection des aliments</li>\n<li>Agence de r\u00e9glementation de la lutte antiparasitaire\u00a0\u2013\u00a0Sant\u00e9 Canada</li>\n<li>Ressources naturelles Canada</li>\n</ul>\n\n<h3>Participation ponctuelle</h3>\n\n<p>Pour s'assurer de disposer de l'expertise appropri\u00e9e (par exemple en taxonomie) et de tenir compte des enjeux locaux ou r\u00e9gionaux, d'autres personnes pourraient \u00eatre invit\u00e9es \u00e0 participer au processus d'examen. Il pourrait notamment s'agir d'intervenants du milieu universitaire, de l'industrie ou de gouvernements et organismes gouvernementaux f\u00e9d\u00e9raux et provinciaux.</p>\n\n<h2>R\u00f4les et responsabilit\u00e9s</h2>\n\n<h3>a. Organisations membres</h3>\n\n<p>Les organisations membres doivent d\u00e9signer une personne-ressource de leur organisation pour repr\u00e9senter cette derni\u00e8re au CELB. En r\u00e9ponse aux demandes d'examen d'une p\u00e9tition, les personnes-ressources d\u00e9signeront une personne ayant l'expertise voulue au sein de leur organisation pour\u00a0:</p>\n\n<ul>\n<li>examiner la p\u00e9tition du point de vue de leur organisation \u00e0 l'\u00e9gard de la lutte biologique et des autorit\u00e9s r\u00e9glementaires applicables;</li>\n<li>\u00e9valuer les risques pour les v\u00e9g\u00e9taux et l'environnement, plus particuli\u00e8rement en fonction des crit\u00e8res des NRMP 7 et 12;</li>\n<li>formuler des recommandations \u00e9crites au secr\u00e9tariat du CELB avant la date limite.</li>\n</ul>\n\n<h3>b. Chaque membre</h3>\n\n<h4>Agence canadienne d'inspection des aliments</h4>\n\n<ul>\n<li>Administrer et assurer la mise en application de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>.</li>\n<li>Assurer le bon fonctionnement du CELB, notamment en nommant son pr\u00e9sident et en communiquant avec lui (ou le pr\u00e9sident par int\u00e9rim) et nommer un secr\u00e9taire (sur recommandation du pr\u00e9sident).</li>\n<li>Pr\u00e9senter au CELB les p\u00e9titions re\u00e7ues aux fins de leur examen et de la formulation de recommandations.</li>\n<li>Tenir compte des recommandations du CELB au moment de prendre des d\u00e9cisions \u00e0 l'\u00e9gard des premi\u00e8res diss\u00e9minations propos\u00e9es d'ALB au Canada.</li>\n</ul>\n\n<h4>Agriculture et Agroalimentaire Canada</h4>\n\n<ul>\n<li>Appuyer la r\u00e9alisation du mandat des autorit\u00e9s f\u00e9d\u00e9rales en mati\u00e8re de lutte biologique en permettant au personnel d'agir \u00e0 titre de pr\u00e9sident et de secr\u00e9taire (sur nomination) et en fournissant des ressources qui facilitent les activit\u00e9s de CELB.</li>\n</ul>\n\n<h4>Agence de r\u00e9glementation de la lutte antiparasitaire</h4>\n\n<ul>\n<li>Tenir compte des dispositions de la <i>Loi sur les produits antiparasitaires</i>, particuli\u00e8rement en ce qui concerne la sant\u00e9 humaine, l'environnement et la valeur.</li>\n</ul>\n\n<h4>Ressources naturelles Canada</h4>\n\n<ul>\n<li>Appuyer la r\u00e9alisation du mandat des autorit\u00e9s f\u00e9d\u00e9rales en mati\u00e8re de lutte biologique en fournissant des ressources qui facilitent les activit\u00e9s de CELB.</li>\n</ul>\n\n<h3>c. Fonctions administratives</h3>\n\n<h4>Pr\u00e9sident</h4>\n\n<ul>\n<li>Recevoir les p\u00e9titions et les listes propos\u00e9es des esp\u00e8ces non cibl\u00e9es de l'ACIA (plantes et autres organismes d'essai) \u00e0 mettre \u00e0 l'essai, et en accuse r\u00e9ception.</li>\n<li>Examiner les p\u00e9titions et les listes propos\u00e9es de mise \u00e0 l'essai et d\u00e9terminer la n\u00e9cessit\u00e9 ou non de recourir aux services d'examinateurs externes aux organisations membres.</li>\n<li>Faciliter l'atteinte d'un consensus parmi les examinateurs et, au besoin, organiser des r\u00e9unions avec les parties int\u00e9ress\u00e9es pour discuter des p\u00e9titions ou des processus connexes.</li>\n<li>Examiner les recommandations des organisations membres et formuler une recommandation globale \u00e0 l'ACIA.</li>\n<li>Tenir des registres des recommandations du CELB.</li>\n</ul>\n\n<h4>Secr\u00e9taire</h4>\n\n<ul>\n<li>Pr\u00e9senter aux examinateurs les p\u00e9titions et les listes des esp\u00e8ces d'essai et les informer de la date limite d'examen (d\u00e9lai habituel d'un mois).</li>\n<li>Recevoir et compiler les examens des p\u00e9titions et des listes des esp\u00e8ces d'essai et les envoyer au pr\u00e9sident.</li>\n<li>Faciliter la distribution des p\u00e9titions et des listes d'esp\u00e8ces d'essai aux organisations membres.</li>\n<li>Maintenir un syst\u00e8me de classement et effectuer d'autres t\u00e2ches d'archivage pour le CELB.</li>\n<li>Recevoir les demandes de prolongation des d\u00e9lais d'examen et y r\u00e9pondre.</li>\n</ul>\n\n<h4>Suppositions relatives au processus</h4>\n\n<p>Si une organisation membre ou une autre partie ne r\u00e9pond pas \u00e0 une p\u00e9tition, le CELB tiendra pour acquis que l'organisation membre ou l'autre partie renonce \u00e0 participer au processus. </p>\n\n<p>Si une organisation membre ou une autre partie ne formule pas de commentaires ou de demande de prolongation du d\u00e9lai d'examen, le CELB tiendra pour acquis que l'organisation membre ou la partie ne s'oppose pas \u00e0 la p\u00e9tition.</p>\n\n<h4>Examen</h4>\n\n<p>Le pr\u00e9sent mandat fera l'objet d'une r\u00e9vision au besoin.</p>\n\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Note de bas de page</h2>\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p>Les questions qui ne s'inscrivent pas dans le mandat de la protection des v\u00e9g\u00e9taux de l'ACIA en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> pourraient \u00eatre trait\u00e9es par le CELB sur demande d'un organisme de r\u00e9glementation.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p>\n</dd> \n</dl>\n</aside> \r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}