{
    "dcr_id": "1489416362659",
    "title": {
        "en": "Checklist for submitting fertilizer or supplement registration applications: major amendment",
        "fr": "Liste de contr\u00f4le pour la pr\u00e9sentation des demandes d'enregistrement d'engrais ou de suppl\u00e9ments\u00a0: modification majeure"
    },
    "html_modified": "23-10-2020",
    "modified": "2-11-2023",
    "issued": "23-10-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/sub_major_amndmnt_fert_supp_chcklst_1489416362659_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/sub_major_amndmnt_fert_supp_chcklst_1489416362659_fra"
    },
    "parent_ia_id": "1646095693725",
    "ia_id": "1489417253330",
    "parent_node_id": "1646095693725",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Checklist for submitting fertilizer or supplement registration applications: major amendment",
        "fr": "Liste de contr\u00f4le pour la pr\u00e9sentation des demandes d'enregistrement d'engrais ou de suppl\u00e9ments\u00a0: modification majeure"
    },
    "label": {
        "en": "Checklist for submitting fertilizer or supplement registration applications: major amendment",
        "fr": "Liste de contr\u00f4le pour la pr\u00e9sentation des demandes d'enregistrement d'engrais ou de suppl\u00e9ments\u00a0: modification majeure"
    },
    "templatetype": "content page 1 column",
    "node_id": "1489417253330",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1646095692928",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/overview/major-amendment/",
        "fr": "/protection-des-vegetaux/engrais/apercu/modification-majeure/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Checklist for submitting fertilizer or supplement registration applications: major amendment",
            "fr": "Liste de contr\u00f4le pour la pr\u00e9sentation des demandes d'enregistrement d'engrais ou de suppl\u00e9ments\u00a0: modification majeure"
        },
        "description": {
            "en": "Each submission package must be complete including all the information requirements as indicated below. Information must be organized in the form of sections or tabs as per the Guide to Submitting Applications for Registration Under the Fertilizers Act.",
            "fr": "Un dossier de soumission complet doit inclure toutes les d'information tel qu'indiqu\u00e9 ci-dessous et doit \u00eatre organis\u00e9 sous la forme de sections ou d'onglets selon le Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la Loi sur les engrais."
        },
        "keywords": {
            "en": "Fertilizers Act, Fertilizers Regulations, fertilizers, supplements, checklist, registration applications, major amendment",
            "fr": "Loi sur les engrais, R\u00e8glement sur les engrais, engrais, suppl\u00e9ments, liste de contr\u00f4le, demande d'enregistrement, changement majeur"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy,regulation",
            "fr": "cultures,engrais,inspection,plante,politique,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-10-23",
            "fr": "2020-10-23"
        },
        "modified": {
            "en": "2023-11-02",
            "fr": "2023-11-02"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Checklist for submitting fertilizer or supplement registration applications: major amendment",
        "fr": "Liste de contr\u00f4le pour la pr\u00e9sentation des demandes d'enregistrement d'engrais ou de suppl\u00e9ments\u00a0: modification majeure"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The 3 year regulatory transition period (October 26, 2020 to October 26, 2023) has now ended. As a result, regulated parties, including all manufacturers, importers, distributors and sellers of fertilizers and supplements must adhere to the amended <a href=\"/english/reg/jredirect2.shtml?ferengr\"><i>Fertilizers Regulations</i></a>. There are few notable exceptions for some product categories. Learn more about the <a href=\"/plant-health/fertilizers/notices-to-industry/2023-05-03/eng/1682633127754/1682633128598\">implementation of the amended <i>Fertilizers Regulations</i></a>.</p>\n</div>\n\n<!--\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/2020sub_major_amndmnt_fert_supp_chcklst_1603477708935_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Checklist for submitting fertilizer or supplement registration applications: major amendment</span> <span class=\"gc-dwnld-info nowrap\">(PDF &ndash; 126 kb)</span></p>\n</div></div>\n</div></a>\n</div>\n</div>\n\n<div class=\"clearfix\"></div>\n-->\n<p>Each submission package must be complete including all the information requirements as indicated below. Information must be organized in the form of sections or tabs as per the\u00a0<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0\">Guide to Submitting Applications for Registration Under the <i>Fertilizers Act</i></a>.</p>\n\n<p>For products that are already registered (for example, amendments), previous submissions may be referenced, but all supporting documentation must be provided even if there is no change since the last registration.</p>\n\n<p>Please note that requirements vary depending on the product type. Please consult appendix 2 of the guide (linked above) to determine which tabs are required for each product type. The Fertilizer Safety Section reserves the right to require additional information, data, fees, rationale or results of analysis to support the registration of any product regulated under the <i>Fertilizers Act</i> and regulations.</p>\n\n<h2>Required information</h2>\n\n<h3>Tab 1: administrative forms and fees</h3>\n\n<ul class=\"lst-spcd\">\n<li>Cover letter (including the intent of the submission and outlining what change(s) is requested and for what reasons)</li>\n<li>Completed and signed\u00a0<a href=\"/about-cfia/find-a-form/form-cfia-acia-5919/eng/1611243691743/1611243692165\">Fertilizer or Supplement Registration Application (CFIA/ACIA 5919)</a> form according to the <a href=\"/plant-health/fertilizers/fertilizer-or-supplement-registration/guidelines/eng/1346531775900/1346537405987\">Guidelines to Completing the Fertilizer or Supplement Registration Application Form</a> document</li>\n<li>Designation of Signing Authority</li>\n<li>Designation of Delegated Representatives (optional)</li>\n<li>Declaration of Resident Canadian Agent (required if the applicant/registrant does not reside in Canada). Please ensure that the Canadian Agent signs and completes applicable section of the Registration Application form</li>\n<li>Registration fee (described in part 5 of the Canadian Food Inspection Agency's (CFIA) <a href=\"/eng/1582641645528/1582641871296#c6\">Fees Notice</a>)</li>\n</ul>\n\n<h3>Tab 2: marketplace label</h3>\n\n<ul class=\"lst-spcd\">\n<li>Proposed market place label in both official languages (English and French)</li>\n</ul>\n\n<h3>Tab 3: product specification</h3>\n\n<ul class=\"lst-spcd\">\n<li>List of ingredients: input materials (preferably in table-format; always include name of the material, Chemical Abstracts Service (CAS) number (if available), manufacturer, country of origin, source, manufacturing/purification processes, concentration in the final product, purpose, and safety data sheets)</li>\n<li>List of ingredients: microbial inocula, when product contains viable microorganism (always include purpose of the microroganism; its taxonomic identification to the strain level if possible; analytical results; relationship to known pathogens; strain bank accession number or environmental isolate's origin/proof of purity)</li>\n<li>List of ingredients: composition of the final product (preferably in table-format; always include identity of the ingredient, relative proportion as weight/weight percentage of the final product, and safety data sheet)</li>\n<li>Method of manufacture</li>\n<li>Quality assurance and quality control procedures</li>\n<li>Physical characteristics</li>\n</ul>\n\n<h3>Tab 4: results of analysis (required for level 2 and 3 safety assessment only)</h3>\n\n<ul class=\"lst-spcd\">\n<li>Total number of batches/lots produced within a 5 year period (if applicable)</li>\n<li>Sets of metal analyses as required</li>\n<li>Results of analysis for guaranteed micronutrients (if applicable)</li>\n<li>1 set of dioxins and furans (if applicable; only required with new product application or if source of material with a dioxins and furans concern is changed)</li>\n<li>4 sets of indicator organisms (if applicable)</li>\n</ul>\n\n<h3>Tab 5: safety rationale and supplemental data (required for level 3 safety assessment only)</h3>\n\n<ul class=\"lst-spcd\">\n<li>Toxicological risk profile (hazard, exposure, risk assessment)</li>\n<li>Microbial risk profile (hazard, exposure, risk assessment)</li>\n<li>References and supplemental documentation</li>\n</ul>\n\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n<p>La p\u00e9riode de transition r\u00e9glementaire de 3 ans (26 octobre 2020 au 26 octobre 2023) est termin\u00e9e. Par cons\u00e9quent, les parties r\u00e9glement\u00e9es, y compris tous les fabricants, importateurs, distributeurs et vendeurs d'engrais et de suppl\u00e9ments, doivent adh\u00e9rer au <a href=\"/francais/reg/jredirect2.shtml?ferengr\"><i>R\u00e8glement sur les engrais</i></a> modifi\u00e9. Il existe quelques exceptions notables pour certaines cat\u00e9gories de produits. Apprenez-en davantage sur la <a href=\"/protection-des-vegetaux/engrais/avis-a-l-industrie/2023-05-03/fra/1682633127754/1682633128598\">mise en \u0153uvre de la modification du <i>R\u00e8glement sur les engrais</i></a>.</p>\n</div>\n\n<!--\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/2020sub_major_amndmnt_fert_supp_chcklst_1603477708935_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/STAGING/images-images/download_pdf_1558729521562_fra.png\" alt=\"T&#233;l&#233;charger le PDF\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>Liste de contr&#244;le pour la pr&#233;sentation des demandes d'enregistrement d'engrais ou de suppl&#233;ments&nbsp;: modification majeure</span> <span class=\"gc-dwnld-info nowrap\">(PDF &ndash; 206 ko)</span></p>\n</div></div>\n</div></a>\n</div>\n</div>\n\n<div class=\"clearfix\"></div>\n-->\n\n<p>Un dossier de soumission complet doit inclure toutes les d'information tel qu'indiqu\u00e9 ci-dessous et doit \u00eatre organis\u00e9 sous la forme de sections ou d'onglets selon le\u00a0<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0\">Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la\u00a0<i>Loi sur les engrais</i></a>.</p>\n\n<p>Pour les produits d\u00e9j\u00e0 enregistr\u00e9s (par exemple pour les r\u00e9enregistrements et les modifications), les soumissions ant\u00e9rieures peuvent \u00eatre r\u00e9f\u00e9renc\u00e9es, mais toutes les pi\u00e8ces justificatives doivent \u00eatre fournies m\u00eame s'il n'y a pas eu de changement depuis le dernier enregistrement.</p>\n\n<p>Veuillez noter que les exigences varient en fonction du type de produit. Consultez l'annexe 2 du guide afin de d\u00e9terminer quels onglets sont requis pour chaque produit. La Section de l'innocuit\u00e9 des engrais se r\u00e9servent le droit d'exiger des informations, des donn\u00e9es, des frais, un raisonnement scientifique ou des analyses suppl\u00e9mentaires pour enregistrer n'importe quel produit r\u00e9glement\u00e9 en vertu de la loi et du <i>R\u00e8glement sur les engrais</i>.</p>\n\n<h2>Information requise</h2>\n\n<h3>Onglet 1\u00a0: formulaires administratifs et frais</h3>\n\n<ul class=\"lst-spcd\">\n<li>Lettre d'accompagnement (incluant l'intention de la soumission et le ou les changements et les raisons de ces changements)</li>\n<li><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5919/fra/1611243691743/1611243692165\">Formulaire de demande d'enregistrement pour les engrais et les suppl\u00e9ments (CFIA/ACIA 5919)</a>\u00a0d\u00fbment remplis et sign\u00e9 selon les\u00a0<a href=\"/protection-des-vegetaux/engrais/enregistrement-d-engrais-ou-de-supplement/directives/fra/1346531775900/1346537405987\">Lignes directrices pour remplir le formulaire de demande d'enregistrement d'engrais ou de suppl\u00e9ment</a></li>\n<li>D\u00e9signation du fond\u00e9 de signature</li>\n<li>D\u00e9signation des repr\u00e9sentants d\u00e9l\u00e9gu\u00e9s (optionnel)</li>\n<li>D\u00e9signation d'un agent r\u00e9sidant au Canada (exig\u00e9 si le requ\u00e9rant/l'inscrit ne r\u00e9side pas au Canada). Veuillez-vous assurer que l'agent r\u00e9sidant au Canada signe et remplit la section applicable du formulaire de demande d'enregistrement</li>\n<li>Frais d'enregistrement (frais d\u00e9crits dans la partie 5 de l'<a href=\"/fra/1582641645528/1582641871296#c6\">Avis sur les prix</a> de l'Agence canadienne d'inspection des aliments (ACIA))</li>\n</ul>\n\n<h3>Onglet 2\u00a0: \u00e9tiquette pour le produit</h3>\n\n<ul class=\"lst-spcd\">\n<li>\u00c9tiquette propos\u00e9e pour le produit dans les deux langues officielles (en anglais et en fran\u00e7ais)</li>\n</ul>\n\n<h3>Onglet 3\u00a0: caract\u00e9ristiques du produit</h3>\n\n<ul class=\"lst-spcd\">\n<li>Liste des ingr\u00e9dients\u00a0: ingr\u00e9dients utilis\u00e9s dans la fabrication du produit (de pr\u00e9f\u00e9rence sous forme de tableau; toujours inclure le nom de l'ingr\u00e9dient, le num\u00e9ro du Chemical Abstracts Service (num\u00e9ro CAS) si disponible, le fabricant, le pays d'origine, la source, le processus de fabrication ou de purification de l'ingr\u00e9dient, la concentration de l'ingr\u00e9dient dans le produit final, l'utilit\u00e9 de l'ingr\u00e9dient et la fiche signal\u00e9tique)</li>\n<li>Liste des ingr\u00e9dients\u00a0: inoculum microbien, pour les produits qui contiennent des microorganismes viables (toujours inclure l'utilit\u00e9 de la souche microbienne; l'identification taxonomique du microorganisme au niveau de la souche si possible; les r\u00e9sultats analytiques; les relations avec des agents pathog\u00e8nes connus; le num\u00e9ro d'acc\u00e8s de la banque de souches ou origine de l'isolat environnemental/preuve de puret\u00e9)</li>\n<li>Liste des ingr\u00e9dients\u00a0: composition du produit final (de pr\u00e9f\u00e9rence sous forme de tableau; inclure l'identit\u00e9 et les proportions relatives selon un pourcentage de poids/poids (totalisant 100 %) de tous les ingr\u00e9dients pr\u00e9sents dans le produit final et la fiche signal\u00e9tique du produit final)</li>\n<li>M\u00e9thode de fabrication</li>\n<li>Proc\u00e9dures de contr\u00f4le de la qualit\u00e9 et d'assurance de la qualit\u00e9</li>\n<li>Caract\u00e9ristiques physiques du produit final</li>\n</ul>\n\n<h3>Onglet 4\u00a0: r\u00e9sultats d'analyses (requis pour les \u00e9valuations de l'innocuit\u00e9 de niveau 2 et 3)</h3>\n\n<ul class=\"lst-spcd\">\n<li>Nombre de lots fabriqu\u00e9s au cours des 5 ann\u00e9es pr\u00e9c\u00e9dant la soumission (le cas \u00e9ch\u00e9ant)</li>\n<li>Ensembles d'analyses pour les m\u00e9taux tel qu'exig\u00e9s</li>\n<li>R\u00e9sultats d'analyse pour les oligo-\u00e9l\u00e9ments garantis (le cas \u00e9ch\u00e9ant)</li>\n<li>1 ensemble d'analyses des dioxines et furanes (le cas \u00e9ch\u00e9ant; requis seulement pour les nouvelles demandes d'enregistrement ou si la source de mati\u00e8re pr\u00e9occupante pour les dioxines et les furannes est modifi\u00e9e)</li>\n<li>4 ensembles de r\u00e9sultats pour les organismes indicateurs (le cas \u00e9ch\u00e9ant)</li>\n</ul>\n\n<h3>Onglet 5\u00a0: justificatifs de l'innocuit\u00e9 et donn\u00e9es compl\u00e9mentaires (requis pour les \u00e9valuations de l'innocuit\u00e9 de niveau 3)</h3>\n\n<ul>\n<li>Profil du risque toxicologique (\u00e9valuation du danger, de l'exposition, et du risque)</li>\n<li>Profil du risque microbiologique (\u00e9valuation du danger, de l'exposition, et du risque</li>\n<li>R\u00e9f\u00e9rences et documents suppl\u00e9mentaires</li>\n</ul>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}