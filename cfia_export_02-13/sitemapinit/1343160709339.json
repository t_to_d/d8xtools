{
    "dcr_id": "1343159961820",
    "title": {
        "en": "Evaluation of Administrative Monetary Penalties (AMPs)",
        "fr": "\u00c9valuation des sanctions administratives p\u00e9cuniaires (SAP) "
    },
    "html_modified": "2024-02-13 9:47:33 AM",
    "modified": "2012-08-03",
    "issued": "2012-07-24 16:00:11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_audit_2012_amps_summary_1343159961820_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/acco_audit_2012_amps_summary_1343159961820_fra"
    },
    "ia_id": "1343160709339",
    "parent_ia_id": "1299843588592",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299843588592",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Evaluation of Administrative Monetary Penalties (AMPs)",
        "fr": "\u00c9valuation des sanctions administratives p\u00e9cuniaires (SAP) "
    },
    "label": {
        "en": "Evaluation of Administrative Monetary Penalties (AMPs)",
        "fr": "\u00c9valuation des sanctions administratives p\u00e9cuniaires (SAP) "
    },
    "templatetype": "content page 1 column",
    "node_id": "1343160709339",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299843498252",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/evaluation-of-amps/",
        "fr": "/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/evaluation-des-sap/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Evaluation of Administrative Monetary Penalties (AMPs)",
            "fr": "\u00c9valuation des sanctions administratives p\u00e9cuniaires (SAP)"
        },
        "description": {
            "en": "As part of the Agency's five-year evaluation plan, the CFIA conducted an internal evaluation of Administrative Monetary Penalties (AMPs).",
            "fr": "Dans le cadre du plan d?\u00e9valuation quinquennal de l?Agence, l?ACIA r\u00e9alise une \u00e9valuation interne des sanctions  administratives p\u00e9cuniaires (SAP)."
        },
        "keywords": {
            "en": "audit, evaluation, AMPs, Administrative Monetary Penalties",
            "fr": "V\u00e9rification, \u00e9valuation, sanctions  administratives p\u00e9cuniaires, SAP"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Audit, Evaluation and Risk Oversight",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,V\u00e9rification et \u00e9valuation"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-07-24 16:00:11",
            "fr": "2012-07-24 16:00:11"
        },
        "modified": {
            "en": "2012-08-03",
            "fr": "2012-08-03"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Archived - Evaluation of Administrative Monetary Penalties (AMPs)",
        "fr": "Archiv\u00e9e - \u00c9valuation des sanctions administratives p\u00e9cuniaires (SAP)"
    },
    "body": {
        "en": "        \r\n        \n<section id=\"archived\" class=\"alert alert-warning wb-inview\" data-inview=\"archived-bnr\">\r\n<h2>This page has been archived</h2>\r\n<p>Information identified as archived is provided for reference, research or record-keeping purposes. It is not subject to the Government of Canada Web Standards and has not been altered or updated since it was archived. Please contact us to request a format other than those available.</p>\r\n</section>\r\n\r\n<section id=\"archived-bnr\" class=\"wb-overlay modal-content overlay-def wb-bar-t\">\r\n<header>\r\n<h2 class=\"wb-inv\">Archived</h2>\r\n</header>\r\n<p><a href=\"#archived\">This page has been archived.</a></p>\r\n</section>\r\n\r\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section>\n<h2>Evaluation</h2>\n<p>The Canadian Food Inspection Agency's (CFIA) Evaluation Directorate is responsible for evaluating the relevance and performance of Agency programs, policies and initiatives. This effort supports informed decision-making and enhances performance and accountability.</p>\n<p>The Evaluation Directorate is accountable to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Evaluation Committee, chaired by the President. All evaluations must be reported to the Evaluation Committee and must be conducted in accordance with the Treasury Board's <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=15024\">Policy on Evaluation</a>. Evaluation projects are selected based on higher risk or significance during an annual Agency planning process, and then reflected in the Agency's Evaluation Plan, which is approved by the Evaluation Committee.</p>\n</section>\n</div>\n<h2>Overview</h2>\n<p>As part of the Agency's five-year evaluation plan, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> conducted an internal evaluation of Administrative Monetary Penalties (AMPs). <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> are intended to address non-compliance in areas in which corrective action requests are not enough, but where prosecution, seizure, or license suspension or revocation are not considered appropriate.</p>\n<p>At the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> are currently applied to select areas of the <i>Health of Animals Act</i> and <i>Health of Animals Regulations</i> and of the <i>Plant Protection Act</i> and <i>Plant Protection Regulations</i>.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> evaluated <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> to assess their effectiveness and efficiency, and to provide guidance on the merits of expanding their application to other acts and regulations, as the <i><abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> Act</i> currently allows. The evaluation assessed the relevance and performance of <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr>, by conducting</p>\n<ul>\n<li>literature, document, file and case reviews,</li>\n<li>analysis, and</li>\n<li>interviews.</li>\n</ul>\n<p>The scope of the study included the application of <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> at the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> from 2000 to 2010.</p>\n<h2>Key Findings</h2>\n<p>The evaluation found that <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> are often an effective compliance tool, successfully employed at the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> as a response to cases for which prosecutions would be excessively time-consuming and costly. The evaluation also found some opportunities for improvement and made two recommendations.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> continually improves its programs and protocols. Management's commitment to addressing recommendations made by internal evaluations like this one is a critical part of that continual improvement.</p>\n<p><strong>Recommendation 1:</strong> The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> should establish a plan to build learning into the delivery of current <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> for their ongoing development, and for any expansion of the use of the tool. The plan should include the following elements:</p>\n<ul>\n<li>a strategy to calibrate the level of investigation according to the risk of appeal;</li>\n<li>a performance measurement strategy; and</li>\n<li>a plan to incorporate ongoing performance measurement in the development of training of and communication with inspectors and investigators across areas.</li>\n</ul>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is developing a plan to incorporate ongoing learning into the delivery of <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr>, with the following elements:</p>\n<ul>\n<li>Evaluation of training and training need assessments on a continuing basis, which will result in updated training material (ongoing).</li>\n<li>A national approach to <abbr title=\"Enforcement and Investigation Services\">EIS</abbr> file triage that recognizes the harm, history and intent of the violation as well as the afforded time dictated by statute to apply either a notice of violation (<abbr title=\"Administrative Monetary Penalties\">AMP</abbr>) or prosecution. A component of this project will be to explore expectations for the appropriate use of <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr>, while taking into account the current and legislated categorization system and the need to adjust the current level of investigation based on risk of appeal (target completion date: December 2012).</li>\n<li>A Performance Measurement Framework with the following indicators: number of appeals, number of paid <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> and number of non-paid <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> (target completion date: December 2012).</li>\n<li>A strategy to improve information sharing. This strategy will include an analysis of each reported Tribunal and Ministerial Review (target completion date: December 2012).</li>\n</ul>\n<p><strong>Recommendation 2:</strong> The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> needs to ensure that all <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> are operating under the key conditions identified, and that <abbr title=\"Administrative Monetary Penalties\">AMPs</abbr> processes, guidelines and strategies are included in program specific enforcement strategies, where relevant.</p>\n<ul>\n<li>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will review Schedule 1 of the Agriculture and Agri-Food Administrative Monetary Penalties (AAMPs) Regulations. The goal of this review is to ensure clarity in the language used and to ensure that the classification of violations reflect current practices (target completion date: December 2012).</li>\n<li>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is developing a suite of commodity-specific enforcement strategies that identify enforcement actions available to inspectors and provide guidance on what enforcement actions to apply. Enforcement actions are based on harm, history and intent of the violation. These strategies will give specific guidance on the enforcement tools available to inspectors under the Agency's various programs and legislation. The objective of the strategies is to promote a fair, graduated and consistent approach to addressing non-compliance identified by inspection staff. The strategies include food; agricultural inputs; and animal and plant health (target completion date: September 2012).</li>\n</ul>\n<h2>Complete report:</h2>\n<ul>\n<li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/evaluation-of-amps/amps/eng/1337024520304/1337025417391\">Evaluation of Administrative Monetary Penalties (AMPs)</a></li>\n<li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/evaluation-of-amps/action-plan/eng/1337027478461/1337027622695\">Management response and action plan</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section id=\"archived\" class=\"alert alert-warning wb-inview\" data-inview=\"archived-bnr\">\r\n<h2>Cette page a \u00e9t\u00e9 archiv\u00e9e</h2>\r\n<p>L'information dont il est indiqu\u00e9 qu'elle est archiv\u00e9e est fournie \u00e0 des fins de r\u00e9f\u00e9rence, de recherche ou de tenue de documents. Elle n'est pas assujettie aux normes Web du gouvernement du Canada et elle n'a pas \u00e9t\u00e9 modifi\u00e9e ou mise \u00e0 jour depuis son archivage. Pour obtenir cette information dans un autre format, veuillez communiquer avec nous.</p>\r\n</section>\r\n\r\n<section id=\"archived-bnr\" class=\"wb-overlay modal-content overlay-def wb-bar-t\">\r\n<header>\r\n<h2 class=\"wb-inv\">Archiv\u00e9e</h2>\r\n</header>\r\n<p><a href=\"#archived\">Cette page a \u00e9t\u00e9 archiv\u00e9e.</a></p>\r\n</section><div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section>\n<h2>\u00c9valuation</h2>\n<p>La Direction de l'\u00e9valuation de l'Agence canadienne d'inspection des aliments (ACIA) est charg\u00e9e d'\u00e9valuer la pertinence et le rendement des programmes, des politiques et des initiatives de l'Agence. Ce processus facilite la prise de d\u00e9cisions \u00e9clair\u00e9es et am\u00e9liore le rendement ainsi que la reddition de comptes.</p>\n<p>La Direction de l'\u00e9valuation rel\u00e8ve du Comit\u00e9 d'\u00e9valuation de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, qui est pr\u00e9sid\u00e9 par le pr\u00e9sident de l'Agence. Toutes les \u00e9valuations doivent \u00eatre pr\u00e9sent\u00e9es au Comit\u00e9 d'\u00e9valuation et effectu\u00e9es conform\u00e9ment \u00e0 la <a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=15024\">Politique sur l'\u00e9valuation</a> du Conseil du Tr\u00e9sor. Les projets d'\u00e9valuation sont choisis, en fonction de leur importance, au cours du processus de planification annuel de l'Agence. Ils figurent dans le Plan d'\u00e9valuation de l'Agence qui est soumis \u00e0 l'approbation du Comit\u00e9 d'\u00e9valuation.</p>\n</section>\n</div>\n<h2>Aper\u00e7u</h2>\n<p>Dans le cadre du plan d'\u00e9valuation quinquennal de l'Agence, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> r\u00e9alise une \u00e9valuation interne des sanctions administratives p\u00e9cuniaires (SAP). Les <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> visent \u00e0 corriger les \u00e9carts \u00e0 la conformit\u00e9 lorsque les demandes de mesures correctives et de simples avertissements ne suffisent pas, mais lorsque les poursuites, la saisie, la suspension ou la r\u00e9vocation de permis sont consid\u00e9r\u00e9es comme \u00e9tant trop s\u00e9v\u00e8res.</p>\n<p>\u00c0 l'Agence canadienne d'inspection des aliments (ACIA), les <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> sont actuellement appliqu\u00e9es pour rem\u00e9dier aux \u00e9carts \u00e0 la conformit\u00e9 qui touchent certaines dispositions de la <i>Loi sur la sant\u00e9 des animaux</i> et du <i>R\u00e8glement sur la sant\u00e9 des animaux</i> ainsi que de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i> et du <i>R\u00e8glement sur la protection des v\u00e9g\u00e9taux</i>.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> a \u00e9valu\u00e9 les <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> pour d\u00e9terminer leur efficacit\u00e9 et leur efficience et pour donner des indications sur le bien-fond\u00e9 d'\u00e9largir l'application de ces sanctions \u00e0 d'autres lois et r\u00e8glements, comme le permet la <abbr title=\"Loi des sanctions administratives p\u00e9cuniaires\">LSAP</abbr>. L'\u00e9valuation a port\u00e9 sur la pertinence et l'application des <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr>. Elle englobait les activit\u00e9s suivantes :</p>\n<ul>\n<li>examen de documents, de dossiers et de cas</li>\n<li>analyses</li>\n<li>entrevues.</li>\n</ul>\n<p>L'\u00e9tude portait sur l'application des <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de 2000 \u00e0 2010.</p>\n<h2>Conclusions principales</h2>\n<p>Selon l'\u00e9valuation, les <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> sont souvent des moyens efficaces que l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> utilise pour assurer la conformit\u00e9 dans les cas o\u00f9 une poursuite monopoliserait trop de temps et serait trop co\u00fbteuse. L'\u00e9valuation a \u00e9galement permis de cerner des possibilit\u00e9s d'am\u00e9lioration et de formuler deux recommandations.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> am\u00e9liore constamment ses programmes et protocoles. L'engagement de la direction \u00e0 donner suite aux recommandations d\u00e9coulant d'\u00e9valuations internes comme celle-ci est au c\u0153ur de ce processus d'am\u00e9lioration continue.</p>\n<p><strong>Recommandation 1 :</strong> L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> devrait dresser un plan afin d'int\u00e9grer l'apprentissage \u00e0 la mise en \u0153uvre des SAP pour permettre leur d\u00e9veloppement continu et \u00e9largir le recours \u00e0 ces instruments. Le plan devrait contenir les \u00e9l\u00e9ments suivants :</p>\n<ul>\n<li>une strat\u00e9gie pour d\u00e9terminer le niveau de l'enqu\u00eate en fonction du risque d'appel;</li>\n<li>une strat\u00e9gie de mesure du rendement;</li>\n<li>un plan visant \u00e0 int\u00e9grer la mesure du rendement continue dans l'\u00e9laboration d'un programme de formation et de communication avec les inspecteurs et enqu\u00eateurs dans les centres op\u00e9rationnels.</li>\n</ul>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e9labore un plan pour incorporer l'apprentissage permanent \u00e0 la mise en application des <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr>. Ce plan comprend les \u00e9l\u00e9ments suivants :</p>\n<ul>\n<li>\u00c9valuation de la formation et des besoins en formation de fa\u00e7on continue, ce qui permettra d'avoir du mat\u00e9riel de formation \u00e0 jour (en cours).</li>\n<li>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e9labore un syst\u00e8me national de tri des dossiers des <abbr title=\"Services d'enqu\u00eate et d'application de la loi\">SEAL</abbr> qui tient compte du tort caus\u00e9 par le contrevenant, de ses ant\u00e9c\u00e9dents et de son intention de contrevenir \u00e0 la loi, ainsi que des d\u00e9lais prescrits par les lois et r\u00e8glements, en vue de produire un proc\u00e8s-verbal (sanction administrative p\u00e9cuniaire) ou d'intenter des poursuites. Un volet de ce projet consistera \u00e0 explorer les attentes quant \u00e0 l'utilisation appropri\u00e9e des <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> tout en tenant compte du syst\u00e8me de cat\u00e9gorisation actuel impos\u00e9 par la loi et du besoin d'ajuster le niveau actuel d\u2019enqu\u00eate en fonction du risque d'appel (date d'ach\u00e8vement vis\u00e9e : d\u00e9cembre 2012).</li>\n<li>Un cadre de mesure du rendement dot\u00e9 des indicateurs suivants : nombre d'appels, nombre de <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> r\u00e9gl\u00e9es et nombre de <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> non r\u00e9gl\u00e9es (date d'ach\u00e8vement pr\u00e9vue : d\u00e9cembre 2012).</li>\n<li>Une strat\u00e9gie pour am\u00e9liorer la communication de l'information. Cette strat\u00e9gie inclura une analyse de chaque examen des <abbr title=\"sanctions administratives p\u00e9cuniaires\">SAP</abbr> par le tribunal et par le Minist\u00e8re (date d'ach\u00e8vement pr\u00e9vue : d\u00e9cembre 2012).</li>\n</ul>\n<p><strong>Recommandation 2 :</strong> L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> doit s'assurer que le recours aux SAP s'inscrit dans les conditions cl\u00e9s d\u00e9termin\u00e9es, et que des processus, lignes directrices et strat\u00e9gies relativement aux SAP sont inscrits dans les strat\u00e9gies d'ex\u00e9cution de la loi propres aux programmes, s'il y a lieu.</p>\n<ul>\n<li>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> examinera l'Annexe 1 du <i>R\u00e8glement sur les sanctions administratives p\u00e9cuniaires en mati\u00e8re d'agriculture et d'agroalimentaire</i> (RSAPAA). L'objectif de cet examen est d'assurer la clart\u00e9 du langage utilis\u00e9 et de faire en sorte que la classification des infractions refl\u00e8te les pratiques courantes (date d'ach\u00e8vement pr\u00e9vue : d\u00e9cembre 2012).</li>\n<li>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u00e9labore une s\u00e9rie de strat\u00e9gies d'application de la loi propres aux produits qui pr\u00e9sentent les mesures d'application de la loi \u00e0 la disposition des inspecteurs et des lignes directrices sur les mesures d'application de la loi \u00e0 mettre en \u0153uvre. Ces derni\u00e8res reposent sur le tort caus\u00e9 par le contrevenant, ses ant\u00e9c\u00e9dents et son intention. Ces strat\u00e9gies pr\u00e9senteront des directives concr\u00e8tes aux inspecteurs en ce qui concerne les outils d'application de la loi \u00e0 leur disposition, conform\u00e9ment aux divers programmes de l'Agence, et des lois applicables. Ces strat\u00e9gies ont pour objet de pr\u00e9senter au personnel un cadre favorisant une d\u00e9marche juste, progressive et coh\u00e9rente de la gestion des \u00e9carts \u00e0 la conformit\u00e9 constat\u00e9s par le personnel d'inspection. Les strat\u00e9gies portent sur les aliments, les intrants agricoles, la sant\u00e9 des animaux et la protection des v\u00e9g\u00e9taux (date d'ach\u00e8vement pr\u00e9vue\u00a0: septembre 2012).</li>\n</ul>\n<h2>Rapport complet :</h2>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/evaluation-des-sap/sap/fra/1337024520304/1337025417391\">\u00c9valuation des sanctions p\u00e9cuniaires administratives</a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/evaluation-des-sap/plan-d-action/fra/1337027478461/1337027622695\">R\u00e9ponse et plan d'action de la direction</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}