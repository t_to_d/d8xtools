{
    "dcr_id": "1387076028743",
    "title": {
        "en": "Conditions for importing meat products from Israel",
        "fr": "Conditions pour l'importation des produits de viande en provenance d'Isra\u00ebl"
    },
    "html_modified": "2024-01-26 2:22:14 PM",
    "modified": "2019-12-04",
    "issued": "2013-12-14 21:53:51",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexa_israel_1387076028743_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexa_israel_1387076028743_fra"
    },
    "ia_id": "1387076096419",
    "parent_ia_id": "1336319720090",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food",
        "fr": "Importation d\u2019aliments"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1336319720090",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Conditions for importing meat products from Israel",
        "fr": "Conditions pour l'importation des produits de viande en provenance d'Isra\u00ebl"
    },
    "label": {
        "en": "Conditions for importing meat products from Israel",
        "fr": "Conditions pour l'importation des produits de viande en provenance d'Isra\u00ebl"
    },
    "templatetype": "content page 1 column",
    "node_id": "1387076096419",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1336318487908",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/approved-countries/israel/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/pays-approuves/israel/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Conditions for importing meat products from Israel",
            "fr": "Conditions pour l'importation des produits de viande en provenance d'Isra\u00ebl"
        },
        "description": {
            "en": "Conditions for importing meat products from Israel",
            "fr": "Conditions pour l'importation des produits de viande en provenance d'Isra\u00ebl"
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, 1990, meat inspection, meat products, fresh meat, raw processed meat products, Israel",
            "fr": "Loi sur l'inspection des viandes, R\u00e8glement de 1990 sur l'inspection des viandes, inspection des viandes, produits de viande crus transform\u00e9s, Isra\u00ebl"
        },
        "dcterms.subject": {
            "en": "inspection,food inspection,agri-food products,regulation,food safety",
            "fr": "inspection,inspection des aliments,produit agro-alimentaire,r\u00e9glementation,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-12-14 21:53:51",
            "fr": "2013-12-14 21:53:51"
        },
        "modified": {
            "en": "2019-12-04",
            "fr": "2019-12-04"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Conditions for importing meat products from Israel",
        "fr": "Conditions pour l'importation des produits de viande en provenance d'Isra\u00ebl"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul class=\"lst-none mrgn-bttm-md\">\n<li>1. <a href=\"#a1\">Meat inspection systems approved</a></li>\n<li>2. <a href=\"#a2\">Types of meat products accepted for import (based on animal health restrictions)</a></li>\n<li>3. <a href=\"#a3\">Additional certification statements/attestations required on the OMIC</a></li>\n<li>4. <a href=\"#a4\">Additional certificates (documents) required</a></li>\n<li>5. <a href=\"#a5\">Establishments eligible for export to Canada</a></li>\n<li>6. <a href=\"#a6\">Specific import and final use conditions and restrictions</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Meat Inspection systems approved</h2>\n<p><strong>1.1</strong> Poultry: slaughter, cutting, deboning and offal.</p>\n<p><strong>1.2</strong> Processing: comminuting, formulating, curing, cooking and canning.</p>\n<h2 id=\"a2\">2. Types of meat products accepted for import (based on animal health restrictions)</h2>\n<p><strong>2.1</strong> Fresh meat and raw processed meat products (chilled or frozen):</p>\n<p><strong>2.1.1</strong> Meat and meat products derived from poultry - the country is not recognised by <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> as being free from Newcastle Disease (END) nor from Highly Pathogenic Avian Influenza (HPAI) - import not allowed.</p>\n<p><strong>2.2</strong> Heat treated meat products, other than shelf stable commercially sterile meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes and meat extract:</p>\n<p><strong>2.2.1</strong> Meat and meat products derived from poultry and all other birds - see section 3.1 for additional required certification statements.</p>\n<p>2.3 Shelf stable commercially sterile meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes, meat extract:</p>\n<p><strong>2.3.1</strong> Meat and meat products derived from poultry from <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> approved sources  - no animal health restriction</p>\n<h2 id=\"a3\">3. Additional certification statements/attestations required on the <abbr title=\"Official Meat inspection Certificate\">OMIC</abbr></h2>\n<div class=\"well well-sm\">\n<p class=\"mrgn-bttm-sm\">The additional certification statements in this section are official statements which must be provided in both official languages, in the order of English first, followed by French.</p>\n</div>\n<p><strong>3.1</strong> For all poultry and all other bird derived meat products, other than shelf stable commercially sterile poultry meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes and meat extract:/<span lang=\"fr\">Dans le cas de tous les produits de viande de volaille et de tous les autres oiseaux, autres que les produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilisables), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation\u00a0:</span></p>\n<p>I hereby certify that/<span lang=\"fr\">Par la pr\u00e9sente je certifie que\u00a0:</span></p>\n<p><strong>3.1.1</strong> The poultry meat products are derived from birds which have been subjected to ante-mortem and post-mortem inspections for Newcastle Disease and Avian Influenza with favourable results/<span lang=\"fr\">Les produits de viande de volaille proviennent d'oiseaux qui ont \u00e9t\u00e9 soumis, avec r\u00e9sultat favorable, \u00e0 une inspection ante mortem et post mortem pour la recherche de l'influenza aviaire et la maladie de Newcastle;</span></p>\n<p><strong>3.1.2</strong> The poultry meat products were cooked to an internal temperature of at least 70 degrees Celsius for a minimum of 30 minutes/<span lang=\"fr\">Les produits de viande de volaille ont \u00e9t\u00e9 cuits jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne de 70 degr\u00e9s Celsius pour un minimum de 30 minutes;</span></p>\n<p><strong>3.1.3</strong> Every precaution was taken to prevent any direct or indirect contact during the slaughter, handling, processing and packaging of the poultry meat products with any meat or animal product that does not fulfill the requirements of this certificate./<span lang=\"fr\">Toutes les pr\u00e9cautions ont \u00e9t\u00e9 prises afin de pr\u00e9venir tout contact direct ou indirect durant l'abattage, la manipulation, la transformation et l'emballage des produits de viande de volaille avec toute viande ou produit animal qui ne rencontre pas les exigences du pr\u00e9sent certificat.</span></p>\n<h2 id=\"a4\">4. Additional certificates (documents) required</h2>\n<p><strong>4.1</strong> None.</p>\n<h2 id=\"a5\">5. Establishments eligible for export to Canada</h2>\n<p><strong>5.1</strong> Refer to the <a href=\"/active/netapp/meatforeign-viandeetranger/forliste.aspx\">List of foreign countries establishments eligible to export meat products to Canada</a></p>\n<h2 id=\"a6\">6. Specific import and final use conditions and restrictions</h2>\n<p><strong>6.1</strong> Consult the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/eng/1507329098491/1507329098850\">Export Requirements Library</a> for possible export restrictions.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul class=\"lst-none mrgn-bttm-md\">\n<li>1. <a href=\"#a1\">Syst\u00e8mes d'inspection accept\u00e9s</a></li>\n<li>2. <a href=\"#a2\">Types de produits de viande accept\u00e9s pour l'importation (en fonction des restrictions pour raisons de sant\u00e9 animale)</a></li>\n<li>3. <a href=\"#a3\">Libell\u00e9s ou attestations de certification suppl\u00e9mentaires devant figurer sur le COIV</a></li>\n<li>4. <a href=\"#a4\">Certificats (documents) suppl\u00e9mentaires requis</a></li>\n<li>5. <a href=\"#a5\">Les \u00e9tablissements \u00e9ligibles pour exportation au Canada</a></li>\n<li>6. <a href=\"#a6\">Conditions et restrictions pr\u00e9cises portent sur l'importation et l'utilisation finale</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Syst\u00e8mes d'inspection accept\u00e9s</h2>\n<p><strong>1.1</strong> Volaille\u00a0: abattage, d\u00e9coupage, d\u00e9sossage et les abats.</p>\n<p><strong>1.2</strong> Transformation\u00a0: hachage, formulation, salaison, cuisson, mise en conserve.</p>\n<h2 id=\"a2\">2. Types de produits de viande accept\u00e9s pour l'importation (en fonction des restrictions pour raisons de sant\u00e9 animale)</h2>\n<p><strong>2.1</strong> Viande fra\u00eeche et produits de viande transform\u00e9s crus (r\u00e9frig\u00e9r\u00e9s ou congel\u00e9s)\u00a0:</p>\n<p><strong>2.1.1</strong> Viande et produits de viande issus de volailles \u2013 L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> ne reconna\u00eet pas le pays comme \u00e9tant indemne de maladie de Newcastle (MN) et Influenza aviaire hautement pathog\u00e8ne (IAHP) \u2013 Importation non autoris\u00e9e.</p>\n<p><strong>2.2</strong> Produits de viande trait\u00e9s \u00e0 la chaleur, autres que les produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilisables), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation\u00a0:</p>\n<p><strong>2.2.1</strong> Viande et produits de viande issus de volailles et de tous les autres oiseaux \u2013 Voir la section 3.1 pour les libell\u00e9s ou attestations de certification suppl\u00e9mentaires.</p>\n<p><strong>2.3</strong> Produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilisables), et m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, cubes de bouillon et extraits de viande de longue conservation\u00a0:</p>\n<p><strong>2.3.1</strong> Tous les produits de viande issus de volailles provenant de sources approuv\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u2013 Aucune restriction pour raisons de sant\u00e9 animale.</p>\n<h2 id=\"a3\">3. Libell\u00e9s ou attestations de certification suppl\u00e9mentaires devant figurer sur le <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr></h2>\n<div class=\"well well-sm\">\n<p class=\"mrgn-bttm-sm\">Les attestations de certification suppl\u00e9mentaires dans cette section sont des attestations officielles qui doivent \u00eatre fournies dans les deux langues officielles, soit l'anglais en premier, suivi du fran\u00e7ais.</p>\n</div>\n<p><strong>3.1</strong> <span lang=\"en\">For all poultry and all other bird derived meat products, other than shelf stable commercially sterile poultry meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup-mix products, bouillon cubes and meat extract:</span>/Dans le cas de tous les produits de viande de volaille et de tous les autres oiseaux, autres que les produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilisables), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation\u00a0:</p>\n<p><span lang=\"en\">I hereby certify that</span>/Par la pr\u00e9sente je certifie que\u00a0:</p>\n<p><strong>3.1.1</strong> <span lang=\"en\">The poultry meat products are derived from birds which have been subjected to ante-mortem and post-mortem inspections for Newcastle Disease and Avian Influenza with favourable results</span>/Les produits de viande de volaille proviennent d'oiseaux qui ont \u00e9t\u00e9 soumis, avec r\u00e9sultat favorable, \u00e0 une inspection ante mortem et post mortem pour la recherche de l'influenza aviaire et la maladie de Newcastle;</p>\n<p><strong>3.1.2</strong> <span lang=\"en\">The poultry meat products were cooked to an internal temperature of at least 70 degrees Celsius for a minimum of 30 minutes</span>/Les produits de viande de volaille ont \u00e9t\u00e9 cuits jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne de 70 degr\u00e9s Celsius pour un minimum de 30 minutes;</p>\n<p><strong>3.1.3</strong> <span lang=\"en\">Every precaution was taken to prevent any direct or indirect contact during the slaughter, handling, processing and packaging of the poultry meat products with any meat or animal product that does not fulfill the requirements of this certificate.</span>/Toutes les pr\u00e9cautions ont \u00e9t\u00e9 prises afin de pr\u00e9venir tout contact direct ou indirect durant l'abattage, la manipulation, la transformation et l'emballage des produits de viande de volaille avec toute viande ou produit animal qui ne rencontre pas les exigences du pr\u00e9sent certificat.</p>\n<h2 id=\"a4\">4. Certificats (documents) suppl\u00e9mentaires requis</h2>\n<p><strong>4.1</strong> Aucun.</p>\n<h2 id=\"a5\">5. Les \u00e9tablissements \u00e9ligibles pour exportation au Canada</h2>\n<p><strong>5.1</strong> Consultez la <a href=\"/active/netapp/meatforeign-viandeetranger/forlistf.aspx\">Liste des \u00e9tablissements des pays \u00e9trangers autoris\u00e9s \u00e0 exporter les produits de viande au Canada</a>.</p>\n<h2 id=\"a6\">6. Conditions et restrictions pr\u00e9cises portent sur l'importation et l'utilisation finale</h2>\n<p><strong>6.1</strong> Consultez la <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/fra/1507329098491/1507329098850\">biblioth\u00e8que des exigences en mati\u00e8re d'exportation</a> pour d'\u00e9ventuelles contraintes \u00e0 l'exportation.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}