{
    "dcr_id": "1537805605250",
    "title": {
        "en": "Processing peas herbicide screening trial",
        "fr": "Essai de contr\u00f4le des herbicides pour les pois de transformation"
    },
    "modified": "2018-10-12",
    "issued": "2018-10-12",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1535122427752",
    "dc_date_created": "2018-10-12",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/scr07-002_1537805605250_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Group-Ensemble/ProgServ-ProgServ/WORKAREA/ProgServ-ProgServ/templatedata/comm-comm/gene-gene/data/09/scr07-002_1537805605250_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Processing peas herbicide screening trial",
        "fr": "Essai de contr\u00f4le des herbicides pour les pois de transformation"
    },
    "breadcrumb": {
        "en": "Processing peas herbicide screening trial",
        "fr": "Essai de contr\u00f4le des herbicides pour les pois de transformation"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "modified": {
            "en": "2018-10-12",
            "fr": "2018-10-12"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Processing peas herbicide screening trial",
            "fr": "Essai de contr\u00f4le des herbicides pour les pois de transformation"
        },
        "subject": {
            "en": "safflower;weed control;viral diseases",
            "fr": "lin;mauvaises herbes;pommes de terre-transformation"
        },
        "dcterms:subject": {
            "en": "geomatics;pesticides;agriculture",
            "fr": "organisme nuisible;programme;\u0153uf"
        },
        "description": {
            "en": "MUP-PUL Minor Use Pesticides Program submissions_and_registrations Minor Use Pesticides - Submissions. clopyralid. AAFC15-002. Submitted to PMRA*. Updated information since August\u00a016, 2017.Submission Fiscal Year: 2016-17 (Total 55). Crop. Rust, leaf and stem (Puccinia graminis); Rust (Puccinia sp.); Rust, stripe (Puccinia striiformis). Tilt 250E fungicide.",
            "fr": "MUP-PUL Programme des pesticides \u00e0 usage limit\u00e9 Malherbologie et r\u00e9gulateurs de croissance - Priorit\u00e9s \u00ab A \u00bb pour 2017. 06. Haricot sec. Mauvaises herbes \u00e0 l'\u00e9tiquette. 18-012. 19. Lavande. Mauvaises herbes \u00e0 l'\u00e9tiquette."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": "Caneberry;Allamanda;cardinalis castor-bean",
            "fr": "Lhonorable;Gaucheti\u00e8re"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "abstract",
            "fr": "accord"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Processing peas herbicide screening trial",
        "fr": "Essai de contr\u00f4le des herbicides pour les pois de transformation"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <h2>Project Code: SCR07-002</h2>\n\n<h3>Project Lead</h3>\n<p><b>Rob Grohs</b> - University of Guelph</p>\n\n<h3>Objective</h3>\n<p><b>To evaluate reduced-risk herbicides for potential to control broadleaf weeds (including Kochia, Russian Thistle, and Nightshade) in processing peas, and to provide recommendations for reduced-risk candidate herbicides to be pursued for registration in Canada</b></p>\n\n<h3>Summary of Results</h3>\n\n<p>Growers have expressed a need for more products for weed control in processing peas. A number of reduced-risk herbicides that may be suitable for this crop have never been evaluated for this purpose. It would therefore be desirable to identify one or more herbicides for possible minor use label expansion or that could be registered under Agriculture and Agri-Food Canada's Minor Use Pesticides.</p>\n\n<p>This project evaluated the efficacy of a number of reduced-risk herbicides in controlling broadleaf weeds in processing peas (<i>Pisum</i> sp., var. Encore). The phytotoxicity of those herbicides was also evaluated.</p>\n\n<p>Field trials were conducted in 2007 at the Simcoe Research Station of the University of Guelph, in Ontario. The experimental design was a randomized complete block design with four replications. Plots were 2.25 meters by 8 meters. The seven herbicides evaluated were applied at three rates: 0.5\u00d7, 1\u00d7 and 2\u00d7. The herbicide treatments were either pre-emergence or post-emergence to the weeds. Data were collected on injury to peas and efficacy in controlling broadleaf weeds.</p>\n\n<p>The data obtained show that four of the herbicides evaluated in this study are considered safe to peas. These four herbicides are: Odyssey (imazamox + imazethapyr), Chateau (flumioxazin), Outlook (dimethenamid-P) and Sandea (halosulfuron-methyl). Odyssey (imazamox + imazethapyr) and Chateau (flumioxazin) caused no crop injury and were very effective in controlling broadleaf weeds. Outlook (dimethenamid-P) did not cause any crop injury, but was not effective in controlling redroot pigweed (<i>Amaranthus retroflexus</i>) or lamb's quarters (<i>Chenopodium album</i>), while Sandea (halosulfuron-methyl) was effective in controlling redroot pigweed (<i>Amaranthus retroflexus</i>) only.</p>\n\r\n    ",
        "fr": "\r\n    <h2>Code de projet : SCR07-002</h2>\n\n<h3>Chef de projet</h3>\n<p><b>Rob Grohs</b> - Universit\u00e9 de Guelph</p>\n\n<h3>Objectif</h3>\n\n<p><b>\u00c9valuer des herbicides \u00e0 risque r\u00e9duit quant \u00e0 leur efficacit\u00e9 contre les dicotyl\u00e9dones (y compris le kochia \u00e0 balais, la soude roulante et les morelles) dans les cultures de pois de transformation et formuler des recommandations \u00e0 l'\u00e9gard des herbicides \u00e0 risque r\u00e9duit qui pourraient \u00eatre homologu\u00e9s au Canada</b></p>\n\n<h3>Sommaire de r\u00e9sultats</h3>\n\n<p>Le besoin de disposer de plus de moyens de contr\u00f4le des mauvaises herbes dans la culture du pois de transformation a \u00e9t\u00e9 exprim\u00e9 par les producteurs. Plusieurs herbicides \u00e0 risques r\u00e9duits qui pourraient \u00eatre appropri\u00e9e pour cette culture n'ont jamais \u00e9t\u00e9 \u00e9valu\u00e9s \u00e0 cet effet. Il serait donc souhaitable d'identifier un ou plusieurs herbicides qui pourraient faire l'objet d'une extension du profil d'emploi ou qui pourraient \u00eatre homologu\u00e9s dans le cadre des Pesticides \u00e0 usage limit\u00e9 d'Agriculture et Agroalimentaire Canada.</p>\n\n<p>Ce projet a permis d'\u00e9valuer l'efficacit\u00e9 de plusieurs herbicides \u00e0 risques r\u00e9duits pour supprimer les mauvaises herbes \u00e0 feuilles larges dans la culture du pois de transformation (<i>Pisum</i> sp., var. Encore). La phytotoxicit\u00e9 des herbicides a aussi \u00e9t\u00e9 \u00e9valu\u00e9e.</p>\n\n<p>Des essais en champ ont \u00e9t\u00e9 effectu\u00e9s en 2007 \u00e0 la station de recherche Simcoe, de l'universit\u00e9 de Guelph, en Ontario. Le plan exp\u00e9rimental utilis\u00e9 fut un bloc al\u00e9atoire complet, comportant 4 r\u00e9p\u00e9titions et utilisant des parcelles de 2.25 meters par 8 meters. Les sept herbicides \u00e9valu\u00e9s ont \u00e9t\u00e9 utilis\u00e9s \u00e0 3 doses diff\u00e9rentes, soient 0.5\u00d7, 1\u00d7 et 2\u00d7. Les herbicides ont \u00e9t\u00e9 appliqu\u00e9s en pr\u00e9-lev\u00e9e ou en post-lev\u00e9e par rapport aux mauvaises herbes. Des donn\u00e9es ont \u00e9t\u00e9 recueillies relativement au pourcentage de dommages fait aux pois et au pourcentage d'efficacit\u00e9 \u00e0 r\u00e9primer les mauvaises herbes \u00e0 feuilles larges.</p>\n\n<p>Les donn\u00e9es obtenues d\u00e9montrent que quatre herbicides \u00e9valu\u00e9s dans cette \u00e9tude sont consid\u00e9r\u00e9s comme s\u00fbrs pour les pois. Ces quatres herbicides sont\u00a0: Odyssey (imazamox + imaz\u00e9thapyr), Chateau (flumioxazine), Outlook (dim\u00e9thenamide-P) et Sandea (halosulfuron-m\u00e9thyle). Les herbicides Odyssey (imazamox + imaz\u00e9thapyr) et Chateau (flumioxazine) n'ont pas caus\u00e9 de dommages aux plants de pois et ont \u00e9t\u00e9 tr\u00e8s efficace pour supprimer les mauvaises herbes \u00e0 feuilles larges. L'herbicide Outlook (dim\u00e9thenamide-P) n'a pas caus\u00e9 de dommages aux pois mais n'a pas \u00e9t\u00e9 efficace pour le cont\u00f4le de l'amaranthe \u00e0 racines rouges (<i>Amaranthus retroflexus</i>) et du ch\u00e9nopode blanc (<i>Chenopodium album</i>) tandis que l'herbicide Sandea (halosulfuron-m\u00e9thyle) fut efficace pour r\u00e9primer l'amaranthe \u00e0 racines rouges (<i>Amaranthus retroflexus</i>) seulement.</p>\n\r\n    "
    }
}