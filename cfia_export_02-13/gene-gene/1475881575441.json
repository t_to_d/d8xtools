{
    "dcr_id": "1475881575441",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Heracleum sosnowskyi</i> (Hogweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Heracleum sosnowskyi</i> (Berce de Sosnowskyi)"
    },
    "html_modified": "2024-02-13 9:51:46 AM",
    "modified": "2017-05-23",
    "issued": "2017-07-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_heracleum_sosnowskyi_1475881575441_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_heracleum_sosnowskyi_1475881575441_fra"
    },
    "ia_id": "1475881575830",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Heracleum sosnowskyi</i> (Hogweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Heracleum sosnowskyi</i> (Berce de Sosnowskyi)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Heracleum sosnowskyi</i> (Hogweed)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Heracleum sosnowskyi</i> (Berce de Sosnowskyi)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1475881575830",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/heracleum-sosnowskyi/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/heracleum-sosnowskyi-berce-de-sosnowskyi-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Heracleum sosnowskyi (Hogweed)",
            "fr": "Semence de mauvaises herbe : Heracleum sosnowskyi (Berce de Sosnowskyi)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Galium verrucosum, Apiaceae, Hogweed",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Galium verrucosum, Berce de Sosnowskyi"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-07-14",
            "fr": "2017-07-14"
        },
        "modified": {
            "en": "2017-05-23",
            "fr": "2017-05-23"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Heracleum sosnowskyi (Hogweed)",
        "fr": "Semence de mauvaises herbe : Heracleum sosnowskyi (Berce de Sosnowskyi)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Family</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Hogweed</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Absent from Canada (Brouillet <abbr title=\"and others\">et al.</abbr> 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to the Caucasus region of Asia (Azerbaijan, Georgia, Russia) and cultivated in eastern Europe and Korea (<abbr title=\"United States Department of Agriculture - Agricultural Research Service\">USDA-ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Monocarpic perennial (dies after flowering)</p>\n\n<h2>Seed or fruit type</h2>\n<p>Schizocarp, divided into 2 mericarps</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Mericarp length: <span class=\"nowrap\">10.0 - 16.0 <abbr title=\"millimetres\">mm</abbr></span></li>\n<li>Mericarp width: <span class=\"nowrap\">5.0 - 9.0 <abbr title=\"millimetres\">mm</abbr></span></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Mericarp is long oval, flattened</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Dull, hairs on dorsal side of mericarp</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Light yellow mericarp with brown oil ducts</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Teeth along the edge, especially near the top of the mericarp.</li>\n<li>The oil ducts extend \u00be down the mericarp and have enlarged ends.</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Fields, old gardens, farmyards, pastures, meadows, grasslands, bushlands, abandoned orchards, forest edges, parks, river valleys, roadsides, railway lines, and disturbed areas (CABI 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Hogweed has been cultivated as an ornamental, for fodder, and for honey production (CABI 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). Seed dispersal can occur by collection of dried fruiting heads for decoration (Kabuce and Priede 2010<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>), or by movement on agricultural and forestry vehicles (CABI 2016<sup id=\"fn3d-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n<p>Seeds are efficiently spread over long distances by water, following streams or floods (Kabuce and Priede 2010<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). An average plant can produce approximately 9000 seeds (CABI 2016<sup id=\"fn3e-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>)</h3>\n<ul><li>The mericarps of giant hogweed are a similar oval shape, flattened, enlarged oil ducts and marginal teeth to hogweed.</li>\n<li>Giant hogweed mericarps tend  to be more narrow, have few or no hairs, are more yellow-coloured and the oil ducts are red compared  to hogweed's brown oil ducts. Hogweed is also more shade tolerant than giant hogweed (Kabuce and Priede 2010<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosnowskyi_03cnsh_1475602567767_eng.jpg\" alt=\"Figure 1 - Hogweed (Heracleum sosnowskyi) mericarps\" class=\"img-responsive\">\n<figcaption>Hogweed (<i lang=\"la\">Heracleum <em>sosnowskyi</em></i>) mericarps\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_06cnsh_1475602614633_eng.jpg\" alt=\"Figure 2 - Hogweed (Heracleum sosnowskyi) mericarp, outer side\" class=\"img-responsive\">\n<figcaption>Hogweed (<i lang=\"la\">Heracleum sosnowskyi</i>) mericarp, outer side\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_05cnsh_1475602594241_eng.jpg\" alt=\"Figure 3 - Hogweed (Heracleum sosnowskyi) mericarp, inner side\" class=\"img-responsive\">\n<figcaption>Hogweed (<i lang=\"la\">Heracleum sosnowskyi</i>) mericarp, inner side\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_07cnsh_1475602635686_eng.jpg\" alt=\"Figure 4 - Hogweed (Heracleum sosnowskyi) mericarps, inner side (R) and outer side (L)\" class=\"img-responsive\">\n<figcaption>Hogweed (<i lang=\"la\">Heracleum sosnowskyi</i>) mericarps, inner side (<abbr title=\"Right\">R</abbr>) and outer side (<abbr title=\"Left\">L</abbr>)\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_08cnsh_1475602705412_eng.jpg\" alt=\"Figure 5 - Hogweed (Heracleum sosnowskyi) mericarp, brown oil duct in centre\" class=\"img-responsive\">\n<figcaption>Hogweed (<i lang=\"la\">Heracleum sosnowskyi</i>) mericarp, brown oil duct in centre</figcaption>\n</figure>\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_04cnsh_1475602546069_eng.jpg\" alt=\"Figure 6 - Similar species: Giant hogweed (Heracleum mantegazzianum) mericarps\" class=\"img-responsive\">\n<figcaption>Similar species: Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>) mericarps\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_01cnsh_1475602526750_eng.jpg\" alt=\"Figure 7 - Similar species: Giant hogweed (Heracleum mantegazzianum) mericarp, outer side\" class=\"img-responsive\">\n<figcaption>Similar species: Giant hogweed (<i lang=\"la\">Heracleum mantegazzianum</i>) mericarp, outer side\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture - Agricultural Research Service\">USDA-ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>CABI. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Kabuce, N. and Priede, N. 2010</strong>. NOBANIS \u2013 Invasive alien species fact sheet \u2013 <i lang=\"la\">Heracleum sosnowskyi</i>. Online Database of the European Network on Invasive Alien Species \u2013 NOBANIS, http://www.nobanis.org/Factsheets.asp [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Famille</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Berce de Sosnowskyi</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> L'esp\u00e8ce n'est pas pr\u00e9sente au Canada (Brouillet <abbr title=\"et autres\">et al.</abbr>, 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Plante indig\u00e8ne du Caucase en Asie (Azerba\u00efdjan, G\u00e9orgie, Russie), cultiv\u00e9e dans l'est de l'Europe et en Cor\u00e9e (<abbr title=\"United States Department of Agriculture - Agricultural Research Service\" lang=\"en\">USDA-ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace monocarpique (meurt apr\u00e8s la floraison)</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Schizocarpe, divis\u00e9 en deux m\u00e9ricarpes</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur du m\u00e9ricarpe\u00a0: <span class=\"nowrap\">10,0 - 16,0 <abbr title=\"millim\u00e8tre\">mm</abbr></span></li>\n<li>Larguer du m\u00e9ricarpe\u00a0: <span class=\"nowrap\">5,0 - 9,0 <abbr title=\"millim\u00e8tre\">mm</abbr></span></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe longuement elliptique et aplati</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>M\u00e9ricarpe \u00e0 la surface mate; c\u00f4t\u00e9 dorsal pubescent</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe jaune clair; tubes ol\u00e9if\u00e8res bruns</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Dents visibles sur la marge du m\u00e9ricarpe, particuli\u00e8rement pr\u00e8s de son sommet.</li>\n<li>Tubes ol\u00e9if\u00e8res dilat\u00e9s \u00e0 la base, pr\u00e9sents sur les trois quarts sup\u00e9rieurs du fruit.</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs, jardins abandonn\u00e9s, fermes, p\u00e2turages, pr\u00e9s, prairies, aires arbustives,  vergers abandonn\u00e9s, bords de for\u00eat, parcs, vall\u00e9es fluviales, bords de chemin, voies ferr\u00e9es et terrains perturb\u00e9s (CABI, 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La berce  de Sosnowskyi est cultiv\u00e9e  comme plante ornementale, fourrag\u00e8re et mellif\u00e8re (CABI, 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>). Les  graines peuvent \u00eatre dispers\u00e9es par les ombelles s\u00e9ch\u00e9es utilis\u00e9es \u00e0 des fins d\u00e9coratives (Kabuce et Priede,   2010<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>) ou par les v\u00e9hicules agricoles et forestiers (CABI, 2016<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>). </p>\n<p>En outre, les graines peuvent \u00eatre transport\u00e9es sur de longues distances par l'eau, par le courant ou les inondations (Kabuce et Priede, 2010<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>). Une plante produit  en moyenne <span class=\"nowrap\">9 000</span> graines (CABI, 2016<sup id=\"fn3d-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n\n<h3>Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>)</h3>\n<ul>\n<li>Les m\u00e9ricarpes de la berce du  Caucase ressemblent \u00e0 ceux de la berce de Sosnowskyi par leur forme elliptique  et aplatie, leurs tubes ol\u00e9if\u00e8res et leur marge dent\u00e9e.</li>\n<li>Ils pr\u00e9sentent des tubes  ol\u00e9if\u00e8res rouges et peu ou pas de poils et sont g\u00e9n\u00e9ralement plus \u00e9troits et  plus jaun\u00e2tres que ceux de la berce de Sosnowskyi, dont les tubes ol\u00e9if\u00e8res  sont bruns.<br>\n<sup id=\"fn4c-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosnowskyi_03cnsh_1475602567767_fra.jpg\" alt=\"Figure 1 - Berce de Sosnowskyi (Heracleum sosynowskyi) m\u00e9ricarpes Berce de Sosnowskyi (Heracleum sosnowskyi) m\u00e9ricarpes\" class=\"img-responsive\">\n<figcaption>Berce de Sosnowskyi (<i lang=\"la\">Heracleum sosnowskyi</i>) m\u00e9ricarpes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_06cnsh_1475602614633_fra.jpg\" alt=\"Figure 2 - Berce de Sosnowskyi (Heracleum sosnowskyi) m\u00e9ricarpe, la face ext\u00e9rieure\" class=\"img-responsive\">\n<figcaption>Berce de Sosnowskyi (<i lang=\"la\">Heracleum sosnowskyi</i>) m\u00e9ricarpe, la face ext\u00e9rieure\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_05cnsh_1475602594241_fra.jpg\" alt=\"Figure 3 - Berce de Sosnowskyi (Heracleum sosnowskyi) m\u00e9ricarpe, la face int\u00e9rieure\" class=\"img-responsive\">\n<figcaption>Berce de Sosnowskyi (<i lang=\"la\">Heracleum sosnowskyi</i>) m\u00e9ricarpe, la face int\u00e9rieure\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_07cnsh_1475602635686_fra.jpg\" alt=\"Figure 4 - Berce de Sosnowskyi (Heracleum sosnowskyi) m\u00e9ricarpes, la face int\u00e9rieure (D) et la face ext\u00e9rieure (G)\" class=\"img-responsive\">\n<figcaption>Berce de Sosnowskyi (<i lang=\"la\">Heracleum sosnowskyi</i>) m\u00e9ricarpes, la face int\u00e9rieure (<abbr title=\"Droite\">D</abbr>) et la face ext\u00e9rieure (<abbr title=\"Gauche\">G</abbr>)</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_sosynowkyi_08cnsh_1475602705412_fra.jpg\" alt=\"Figure 5 - Berce de Sosnowskyi (Heracleum sosnowskyi) m\u00e9ricarpe, canal ol\u00e9if\u00e8re brun au centre\" class=\"img-responsive\">\n<figcaption>Berce de Sosnowskyi (<i lang=\"la\">Heracleum sosnowskyi</i>) m\u00e9ricarpe, canal ol\u00e9if\u00e8re brun au centre</figcaption>\n</figure>\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_04cnsh_1475602546069_fra.jpg\" alt=\"Figure 6 - Esp\u00e8ce semblable : Berce du Caucase (Heracleum mantegazzianum) m\u00e9ricarpes\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>) m\u00e9ricarpes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_heracleum_mantegazzianum_01cnsh_1475602526750_fra.jpg\" alt=\"Figure 7 - Esp\u00e8ce semblable : Berce du Caucase (Heracleum mantegazzianum) m\u00e9ricarpe, la face ext\u00e9rieure\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Berce du Caucase (<i lang=\"la\">Heracleum mantegazzianum</i>) m\u00e9ricarpe, la face ext\u00e9rieure\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture - Agricultural Research Service\" lang=\"en\">USDA-ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>CABI. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Kabuce, N. and Priede, N. 2010</strong>. NOBANIS \u2013 Invasive alien species fact sheet \u2013 <i lang=\"la\">Heracleum sosnowskyi</i>. Online Database of the European Network on Invasive Alien Species \u2013 NOBANIS, http://www.nobanis.org/Factsheets.asp [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}