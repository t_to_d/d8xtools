{
    "dcr_id": "1521217897188",
    "title": {
        "en": "Emamectin benzoate (EMA) \u2013 Medicating Ingredient Brochure",
        "fr": "Benzoate d'\u00e9mamectine (EMA) \u2013 Notices sur les substances m\u00e9dicatrices"
    },
    "html_modified": "2024-02-13 9:52:51 AM",
    "modified": "2019-07-30",
    "issued": "2018-04-04",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/feed_med_cmib_ema_1521217897188_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/feed_med_cmib_ema_1521217897188_fra"
    },
    "ia_id": "1521217949734",
    "parent_ia_id": "1320602461227",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1320602461227",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Emamectin benzoate (EMA) \u2013 Medicating Ingredient Brochure",
        "fr": "Benzoate d'\u00e9mamectine (EMA) \u2013 Notices sur les substances m\u00e9dicatrices"
    },
    "label": {
        "en": "Emamectin benzoate (EMA) \u2013 Medicating Ingredient Brochure",
        "fr": "Benzoate d'\u00e9mamectine (EMA) \u2013 Notices sur les substances m\u00e9dicatrices"
    },
    "templatetype": "content page 1 column",
    "node_id": "1521217949734",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300212600464",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/medicating-ingredients/emamectin-benzoate/",
        "fr": "/sante-des-animaux/aliments-du-betail/substances-medicatrices/benzoate-d-emamectine/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Emamectin benzoate (EMA) \u2013 Medicating Ingredient Brochure",
            "fr": "Benzoate d'\u00e9mamectine (EMA) \u2013 Notices sur les substances m\u00e9dicatrices"
        },
        "description": {
            "en": "Emamectin benzoate medicating ingredient brochure.",
            "fr": "Notices sur les substances m\u00e9dicatrices du benzoate d'\u00e9mamectine."
        },
        "keywords": {
            "en": "Feeds Act, Feeds regulations, feed, animal feeds, medicated feeds, ingredients, toxicology, Medicating Ingredient Brochures, Emamectin benzoate, EMA",
            "fr": "Loi relative aux aliments du b\u00e9tail, R\u00e8glements sur les aliments du b\u00e9tail, aliment, aliments du b\u00e9tail, aliments m\u00e9dicament\u00e9s, ingr\u00e9dients, toxicologiques, notices sur les substances m\u00e9dicatrices, benzoate d'\u00e9mamectine, EMA"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,animal health",
            "fr": "b\u00e9tail,inspection,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-04-04",
            "fr": "2018-04-04"
        },
        "modified": {
            "en": "2019-07-30",
            "fr": "2019-07-30"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Emamectin benzoate (EMA) \u2013 Medicating Ingredient Brochure",
        "fr": "Benzoate d'\u00e9mamectine (EMA) \u2013 Notices sur les substances m\u00e9dicatrices"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-danger\">\n<p><strong>Status:</strong> Veterinary prescription required for approved claims</p>\n</div>\n\n<p class=\"text-right\"><strong>Date of revision: July 2019</strong></p>\n\n<figure>\n<table class=\"table table-bordered table-responsive\">\n<caption class=\"text-left\">Table of approved species and claims</caption>\n<thead>\n<tr class=\"active\">\n<th class=\"text-center\">Approved livestock species</th>\n<th class=\"text-center\">Approved claim(s)<br>\n<sup id=\"fn1-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>1</a></sup></th>\n<th class=\"text-center\">Withdrawal time</th>\n<th class=\"text-center\">Name of approved brand(s)</th>\n</tr>\n</thead>\n\n<tbody>\n<tr>\n<td class=\"text-center\"><a href=\"#a1\">Atlantic salmon</a></td>\n<td><ol><li>Treatment of parasitic infestations of sea louse</li></ol></td>\n<td class=\"text-center\">0 days</td>\n<td class=\"text-center\">Slice 0.2% Premix</td>\n</tr>\n</tbody>\n</table>\n\n<div class=\"wb-fnote\" role=\"note\">\n<p id=\"fn-tnotes\" class=\"wb-inv\">Table Note</p>\n<dl>\n<dt>Table Note 1</dt>\n<dd id=\"fn1\">\n<p>In order to be compliant with the <i>Feeds Regulations</i>, the complete claim must appear on the medicated feed label.</p>\n<p class=\"fn-rtn\"> <a href=\"#fn1-rf\"> <span class=\"wb-inv\">Return to table note\u00a0</span>1 <span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>\n</dl>\n</div>\n</figure>\n\n<h2 id=\"a1\">Atlantic salmon</h2>\n\n<table class=\"table table-bordered table-responsive\">\n<caption class=\"text-left\">Table of approved claims and brands</caption>\n<thead>\n<tr class=\"active\">\n<th class=\"text-center\">Approved claims</th>\n<th class=\"text-center\">Name of approved brand(s)</th>\n<th class=\"text-center\">Drug concentration in <abbr title=\"drug identification number\">DIN</abbr> product</th>\n</tr>\n</thead>\n\n<tbody>\n<tr class=\"text-center\">\n<td>Claim 1</td>\n<td>Slice 0.2% Premix</td>\n<td>Emamectin benzoate at<br> \n<abbr title=\"grams per kilogram\">g/kg</abbr></td>\n</tr>\n</tbody>\n</table>\n\n<p>Approved for use on complete fish feeds that are cylindrical pellets of varying length and thickness (<abbr title=\"for example\">e.g.</abbr>, 2.4 to 11\u00a0<abbr title=\"millimetres\">mm</abbr>). To be applied to the exterior of the feed pellets post-pelleting.</p>\n\n<h3>Claim 1</h3>\n\n<p>As an aid in the treatment of parasitic infestations caused by all parasitic stages (chalimus <abbr title=\"1\">I</abbr>\u2013<abbr title=\"4\">IV</abbr>, pre-adult <abbr title=\"1\">I</abbr>\u2013<abbr title=\"2\">II</abbr> and adult) of the sea louse (<i lang=\"la\">Lepeophtheirus salmonis</i>), on Atlantic salmon (<i lang=\"la\">Salmo salar</i>).</p>\n\n<h4>Level of medicating ingredient in a complete feed</h4>\n\n<p>1.25\u00a0<abbr title=\"milligrams per kilogram\">mg/kg</abbr> to 20\u00a0<abbr title=\"milligrams per kilogram\">mg/kg</abbr> (0.000125% to 0.0020%) of emamectin benzoate in the complete feed. </p>\n\n<div class=\"alert alert-info\">\n<h4>Additional information (not to appear on feed labels)</h4>\n\n<h5>For feed manufacturers</h5>\n\n<ul>\n<li>10\u00a0<abbr title=\"milligrams per kilogram\">mg/kg</abbr> (0.0010%) is the correct concentration level when the suggested feeding rate is 0.5% of fish biomass per day. If the feeding rate differs from 0.5% of the biomass per/day, then the concentration of emamectin benzoate in the feed must be adjusted accordingly (see Table\u00a01).</li>\n<li>Feed labels must state one specific drug level.</li>\n</ul>\n</div>\n\n<h4>Directions for use in a complete feed</h4>\n\n<p>Administer to deliver 50 <abbr title=\"microgram\">\u03bcg</abbr> emamectin benzoate<abbr title=\"per kilogram\">/kg</abbr> fish biomass/day for 7 consecutive days.</p>\n\n<div class=\"alert alert-info\">\n<h4>Additional information (not to appear on feed labels)</h4>\n\n<h5>For feed manufacturers</h5>\n\n<ul>\n<li>Feed labels must state one specific feeding rate (% biomass), one specific drug level and the biomass of the fish being medicated per unit of feed (metric tonne) per 7-day-treatment period.</li>\n<li>Do not feed Slice 0.2% Premix undiluted</li>\n<li>Slice 0.2% Premix applied post-pelleting by <strong>coating it onto</strong> the surface of a non-medicated pelleted fish feed as follows:\n<ol class=\"lst-lwr-alph\"><li>A pre-weighed amount of feed pellets is transferred to a mixer.</li>\n<li>The pellets are dry-mixed and coated with a premeasured amount of Slice 0.2% Premix and are mixed for a period of up to 2\u00a0minutes.</li>\n<li>0.5% to 1% fish oil or vegetable oil is added to the pellets and mixing is continued for up to an additional 5\u00a0minutes. This second mixing step is important and should not be skipped as the added oil will bind and seal the Slice premix onto the surface of the feed pellets.</li>\n<li>Once mixing is completed, the product can be bagged.</li></ol></li>\n<li>Careful dispersion of the premix on the feed pellets and longer mixing times should be considered when preparing medicated feeds using low concentrations of Slice 0.2% Premix.</li>\n</ul>\n\n<table class=\"table table-bordered\"> \n<caption class=\"text-left\">Table 1: Usage rates of Slice 0.2% Premix (emamectin benzoate) to be used for the preparation of medicated salmon feeds</caption> \n<thead> \n<tr> \n<th class=\"active\">Feeding rate<br>\n</th> \n<th class=\"active\">Concentration of emamectin benzoate in complete feed (<abbr title=\"parts per million\">ppm</abbr>)</th> \n<th class=\"active\">Amount of Slice 0.2% Premix per unit of feed (<abbr title=\"kilograms per\">kg/</abbr>metric tonne)</th> \n<th class=\"active\">Biomass of fish treated per unit of medicated feed per 7-day-treatment period<br> \n<br>\n</th> \n</tr> \n</thead> \n<tbody> \n<tr> \n<td>0.25</td> \n<td>20</td> \n<td>10</td> \n<td>57.14</td> \n</tr> \n<tr> \n<td>0.5</td> \n<td>10</td> \n<td>5</td> \n<td>28.57</td> \n</tr>\n<tr> \n<td>1.0</td> \n<td>5</td> \n<td>2.5</td> \n<td>14.28</td> \n</tr>\n<tr> \n<td>2.0</td> \n<td>2.5</td> \n<td>1.25</td> \n<td>7.14</td> \n</tr>\n<tr> \n<td>3.0</td> \n<td>1.67</td> \n<td>0.83</td> \n<td>4.76</td> \n</tr>\n<tr> \n<td>4.0</td> \n<td>1.25</td> \n<td>0.625</td> \n<td>3.57</td> \n</tr> \n</tbody> \n</table> \n\n<ul><li>Wear mask and goggles while incorporating the premix into the feed.</li> \n<li>Wear gloves and do not smoke or eat while handling the product or medicated feed.</li>\n<li>Thoroughly clean all equipment used in the manufacturing and preparation of medicating feed.</li>\n<li>Slice 0.2% Premix should be stored at a temperature between 2\u00b0<abbr title=\"Celsius\">C</abbr> and 30\u00b0<abbr title=\"Celsius\">C</abbr>.</li></ul>\n\n</div>\n\n<h4>Warning</h4>\n\n<ol>\n<li>No pre-slaughter withdrawal period is required when this medicated feed is used according to the label directions.</li>\n<li>To ensure tissue residues do not exceed the maximum residue limit, Atlantic salmon should not be treated more than once in the 60\u00a0days prior to the first fish being harvested for human consumption.</li>\n<li>Avoid inhalation, oral exposure, and direct contact with skin or eyes. Operators mixing and handling emamectin benzoate should use protective clothing, impervious gloves, goggles, and an approved dust mask. Do not smoke or eat while handling medicated feed.  Wash hands with soap and water after handling.</li>\n<li>Keep out of reach of children.</li>\n</ol>\n\n<h4>Caution</h4>\n\n<ol>\n<li>Feed pellets medicated with emamectin benzoate are intended for use in Atlantic salmon only.</li>\n<li>The concurrent use of emamectin benzoate with other veterinary drugs has not been investigated.</li> \n<li>No data are available regarding the effects of emamectin benzoate on vitellogenesis and spermatogenesis in Atlantic salmon. Consequently, do not administer this product to brood stock.</li> \n<li>Do not dispose of this product where it may become accessible to fish not under treatment or to any other animal species, including wildlife.</li>\n</ol>\n\n<div class=\"alert alert-info\">\n<h4>Additional information to be added as a note to feed labels</h4>\n\n<p>Notes</p>\n\n<ul>\n<li>Emamectin benzoate will remain stable in the finished feed for at least 3\u00a0months.</li>\n</ul>\n</div>\n\n<h4>Accepted Compatibilities</h4>\n\n<p>Nil</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-danger\">\n<p><strong>Statut\u00a0:</strong> Ordonnance v\u00e9t\u00e9rinaire requise pour les all\u00e9gations approuv\u00e9es</p>\n</div>\n\n<p class=\"text-right\"><strong>Date de r\u00e9vision\u00a0: Juillet 2019</strong></p>\n\n<figure>\n<table class=\"table table-bordered table-responsive\">\n<caption class=\"text-left\">Tableau des all\u00e9gations approuv\u00e9es par esp\u00e8ce de b\u00e9tail approuv\u00e9e </caption>\n<thead>\n<tr class=\"active\">\n<th class=\"text-center\">Esp\u00e8ce de b\u00e9tail approuv\u00e9e</th>\n<th class=\"text-center\">All\u00e9gation(s) approuv\u00e9e(s) (abr\u00e9g\u00e9es)<sup id=\"fn1-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Note de tableau\u00a0</span>1</a></sup></th>\n<th class=\"text-center\">P\u00e9riode de retrait</th>\n<th class=\"text-center\">Nom(s) de marque approuv\u00e9(s)</th>\n</tr>\n</thead>\n\n<tbody>\n<tr>\n<td class=\"text-center\"><a href=\"#a1\">Saumon de l'Atlantique</a></td>\n<td><ol><li>Traitement du saumon de l'Atlantique contre les infestations parasitaires du pou de mer</li></ol></td>\n<td class=\"text-center\">0 jours</td>\n<td class=\"text-center\">Pr\u00e9m\u00e9lange Slice \u00e0 0,2\u00a0%</td>\n</tr>\n</tbody>\n</table>\n\n<div class=\"wb-fnote\" role=\"note\">\n<p id=\"fn-tnotes\" class=\"wb-inv\">Note de tableau</p>\n<dl>\n<dt>Note de tableau 1</dt>\n<dd id=\"fn1\"> \n<p>Pour \u00eatre conforme aux exigences indiqu\u00e9es dans le <i>R\u00e8glement sur les aliments du b\u00e9tail</i>, l'all\u00e9gation compl\u00e8te doit appara\u00eetre sur l'\u00e9tiquette de l'aliment du b\u00e9tail m\u00e9dicament\u00e9.</p>\n\n<p class=\"fn-rtn\"> <a href=\"#fn1-rf\"> <span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de tableau\u00a0</span>1 </a></p></dd>\n</dl> \n</div> \n</figure>\n\n<h2 id=\"a1\">Saumon de l'Atlantique</h2>\n\n<table class=\"table table-bordered table-responsive\">\n<caption class=\"text-left\">Tableau des all\u00e9gations approuv\u00e9es et des marques</caption>\n<thead>\n<tr class=\"active\">\n<th class=\"text-center\">All\u00e9gations approuv\u00e9es</th>\n<th class=\"text-center\">Nom(s) de marque approuv\u00e9(s)</th>\n<th class=\"text-center\">Concentration de la drogue dans le produit assign\u00e9 l'identification num\u00e9rique attribu\u00e9e \u00e0 la drogue (num\u00e9ro <abbr lang=\"en\" title=\"drug identification number\">DIN</abbr>)</th>\n</tr>\n</thead>\n\n<tbody>\n<tr class=\"text-center\">\n<td>All\u00e9gation 1</td>\n<td>Pr\u00e9m\u00e9lange<br> \n</td>\n<td>Benzoate d'\u00e9mamectine<br> \n<abbr title=\"grammes par kilogramme\">g/kg</abbr></td>\n</tr>\n</tbody>\n</table>\n\n<p>Approuv\u00e9 l'utilisation sur les aliments complets granul\u00e9s cylindriques qui sont de longueur et d'\u00e9paisseur vari\u00e9es, par <abbr title=\"exemple\">ex.</abbr> de 2,4 \u00e0 11\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr>. \u00c0 appliquer sur la surface ext\u00e9rieure de l\u2019aliment cub\u00e9 suite \u00e0 la granulation.</p>\n\n<h3>All\u00e9gation 1</h3>\n\n<p>Pour aider \u00e0 traiter le saumon de l'Atlantique (<i lang=\"la\">Salmo salar</i>) contre les infestations parasitaires attribuables au pou de mer (<i lang=\"la\">Lepeophtheirus salmonis</i>) \u00e0 tous les stades de d\u00e9veloppement (chalimus <abbr title=\"1\">I</abbr>-<abbr title=\"4\">IV</abbr>, pr\u00e9-adulte <abbr title=\"1\">I</abbr>-<abbr title=\"2\">II</abbr>, et adulte).</p>\n\n<h4>Concentration de la substance m\u00e9dicatrice dans l'aliment complet</h4>\n\n<p>1,25\u00a0<abbr title=\"milligrammes per kilogramme\">mg/kg</abbr> \u00e0 20\u00a0<abbr title=\"milligrammes per kilogramme\">mg/kg</abbr> (0,000125% \u00e0 0,0020%) de benzoate d'\u00e9mamectine dans l'aliment complet.</p>\n\n<div class=\"alert alert-info\">\n<h4>Informations additionnelles (qui ne doivent pas appara\u00eetre sur les \u00e9tiquettes pour les aliments de b\u00e9tail)</h4>\n\n<h5>Pour les fabricants d'aliments du b\u00e9tail</h5>\n\n<ul>\n<li>La concentration de 10\u00a0<abbr title=\"milligrammes per kilogramme\">mg/kg</abbr> (0,0010%) est la bonne concentration \u00e0 utiliser lorsque que le taux d'alimentation sugg\u00e9r\u00e9 est de 0,5% de la biomasse de poisson par jour. Si le taux d'alimentation est autre que 0,5% de la biomasse par jour, la concentration de benzoate d'\u00e9mamectine doit \u00eatre ajust\u00e9e en cons\u00e9quence (voir le Tableau 1).</li>\n<li>Les \u00e9tiquettes des aliments du b\u00e9tail doivent indiquer un niveau de m\u00e9dicament sp\u00e9cifique.</li>\n</ul>\n</div>\n\n<h4>Mode d'emploi pour l'utilisation dans un aliment complet</h4>\n\n\n<p>Administrer de fa\u00e7on \u00e0 d\u00e9livrer 50 \u03bcg de benzoate d'\u00e9mamectine/<abbr title=\"kilogramme\">kg</abbr> de biomasse de poisson/jour, pour 7\u00a0jours cons\u00e9cutifs.</p>\n\n<div class=\"alert alert-info\">\n<h4>Informations additionnelles (qui ne doivent pas appara\u00eetre sur les \u00e9tiquettes pour les aliments de b\u00e9tail)</h4>\n\n<h5>Pour les fabricants d'aliments du b\u00e9tail</h5>\n\n<ul>\n<li>Les \u00e9tiquettes pour les aliments doivent indiquer un taux d'alimentation sp\u00e9cifique (% de biomasse), le niveau sp\u00e9cifique de m\u00e9dicament et la biomasse des poissons trait\u00e9s par unit\u00e9 d'aliment (tonne m\u00e9trique) par p\u00e9riode de traitement de 7\u00a0jours.</li>\n<li>Ne pas administrer sous forme concentr\u00e9e.</li>\n<li>Le Pr\u00e9m\u00e9lange Slice \u00e0 0,2% est appliqu\u00e9 aux aliments granul\u00e9s non-m\u00e9dicament\u00e9s pour le saumon apr\u00e8s la pelletisation et selon les indications suivantes\u00a0:\n<ol class=\"lst-lwr-alph\"><li>Une quantit\u00e9 mesur\u00e9e de granul\u00e9s est transf\u00e9r\u00e9e dans un m\u00e9langeur.</li>\n<li>Les granul\u00e9s sont m\u00e9lang\u00e9s \u00e0 sec et enrob\u00e9s avec une quantit\u00e9 pr\u00e9-mesur\u00e9e u Pr\u00e9m\u00e9lange Slice \u00e0 0,2% pour une dur\u00e9e de jusqu'\u00e0 deux minutes.</li>\n<li>0,5% \u00e0 1% d'huile de poisson ou d'huile v\u00e9g\u00e9tale est ajout\u00e9e aux granul\u00e9s. Continuer de m\u00e9langer jusqu'\u00e0 5\u00a0minutes de plus. Ce m\u00e9lange avec l'huile permet de sceller et enrober le Pr\u00e9m\u00e9lange Slice sur la surface des granul\u00e9s.</li>\n<li>\u00c0 la fin du m\u00e9lange, l'aliment m\u00e9dicament\u00e9 peut \u00eatre ensach\u00e9.</li></ol></li>\n<li>Une bonne dispersion du pr\u00e9m\u00e9lange sur les granul\u00e9s de l'aliment et une plus longue p\u00e9riode de m\u00e9lange devraient \u00eatre consid\u00e9r\u00e9e lors de la pr\u00e9paration d'aliments m\u00e9dicament\u00e9s contenant une faible concentration du Pr\u00e9m\u00e9lange Slice \u00e0 0,2%.</li>\n</ul>\n\n<table class=\"table table-bordered\"> \n<caption class=\"text-left\">Tableau 1\u00a0: Taux d'utilisation du Pr\u00e9m\u00e9lange Slice \u00e0 0,2% (benzoate d'\u00e9mamectine) pour la pr\u00e9paration d'aliments pour saumon m\u00e9dicament\u00e9s</caption> \n<thead> \n<tr> \n<th class=\"active\">Taux d'alimentation<br>  \n</th> \n<th class=\"active\">Concentration de benzoate d'\u00e9mamectine dans l'aliment complet (<abbr title=\"parties par million\">ppm</abbr>)</th> \n<th class=\"active\">Quantit\u00e9 de Pr\u00e9m\u00e9lange Slice \u00e0 0,2% par tonne m\u00e9trique d'aliment (<abbr title=\"kilogramme\">kg</abbr>/tonne m\u00e9trique)</th> \n<th class=\"active\">Biomasse (<abbr title=\"kilogramme\">kg</abbr>) de saumons trait\u00e9s par tonne m\u00e9trique d'aliment m\u00e9dicament\u00e9 par p\u00e9riode de traitement de 7\u00a0jours<br>\n<br>\n</th> \n</tr> \n</thead> \n<tbody> \n<tr> \n<td>0,25</td> \n<td>20</td> \n<td>10</td> \n<td>57,14</td> \n</tr> \n<tr> \n<td>0,5</td> \n<td>10</td> \n<td>5</td> \n<td>28,57</td> \n</tr>\n<tr> \n<td>1,0</td> \n<td>5</td> \n<td>2,5</td> \n<td>14,28</td> \n</tr>\n<tr> \n<td>2,0</td> \n<td>2,5</td> \n<td>1,25</td> \n<td>7,14</td> \n</tr>\n<tr> \n<td>3,0</td> \n<td>1,67</td> \n<td>0,83</td> \n<td>4,76</td> \n</tr>\n<tr> \n<td>4,0</td> \n<td>1,25</td> \n<td>0,625</td> \n<td>3,57</td> \n</tr> \n</tbody> \n</table> \n\n<ul><li>Portez un masque et des lunettes de protection pour incorporer ce pr\u00e9m\u00e9lange aux aliments. \n</li><li>Portez des gants et s'abstenir de fumer ou de manger pendant la manipulation du pr\u00e9m\u00e9lange ou des aliments m\u00e9dicament\u00e9s.\n</li><li>Tout l'\u00e9quipement utilis\u00e9 pour la pr\u00e9paration des aliments m\u00e9dicament\u00e9s doit \u00eatre nettoy\u00e9 soigneusement.\n</li><li>Le Pr\u00e9m\u00e9lange Slice \u00e0 0,2% doit \u00eatre entrepos\u00e9 \u00e0 une temp\u00e9rature situ\u00e9e entre 2 et 30\u00b0<abbr title=\"Celsius\">C</abbr>.</li></ul>\n\n</div>\n\n<h4>Mise en garde</h4>\n\n<ol>\n<li>Un d\u00e9lai d'attente avant l'abattage n'est pas requis pour les saumons de l'Atlantique quand les directives de l'\u00e9tiquette sont suivies.</li> \n<li>Afin de s'assurer que le niveau de r\u00e9sidus dans les tissus n'exc\u00e8de pas la limite maximale de r\u00e9sidus, le saumon de l'Atlantique ne devrait pas \u00eatre trait\u00e9 plus d'une seule fois dans les 60\u00a0jours pr\u00e9c\u00e9dant la r\u00e9colte du premier poisson destin\u00e9 \u00e0 la consommation humaine.</li> \n<li>\u00c9viter toute inhalation et exposition buccale, ainsi que tout contact direct avec la peau ou les yeux. Les utilisateurs doivent porter des v\u00eatements protecteurs, des gants imperm\u00e9ables, des lunettes de protection et un masque anti-poussi\u00e8re approuv\u00e9 pendant le m\u00e9lange et la manipulation du benzoate d'\u00e9mamectine. Porter des gants et s'abstenir de fumer ou de manager pendant la manipulation des aliments m\u00e9dicament\u00e9s. Bien se laver les mains apr\u00e8s avoir la manipulation.</li>\n<li>Garder hors de la port\u00e9e des enfants.</li>\n</ol>\n\n<h4>Pr\u00e9caution</h4>\n\n<ol>\n<li>Les aliments m\u00e9dicament\u00e9s renfermant le benzoate d'\u00e9mamectine ne doivent \u00eatre utilis\u00e9s que pour le saumon de l'Atlantique.</li>\n<li>L'emploi du benzoate d'\u00e9mamectine en conjonction avec d'autres m\u00e9dicaments n'a pas fait l'objet d'aucune \u00e9tude.</li> \n<li>On ne dispose d'aucune donn\u00e9e sur l'effet du benzoate d'\u00e9mamectine sur la vitellogen\u00e8se ni sur la spermatogen\u00e8se. Par cons\u00e9quent, ne pas utiliser ce produit chez les poissons g\u00e9niteurs.</li> \n<li>Ne pas \u00e9liminer ce produit dans un endroit o\u00f9 il peut entrer en contact avec des poissons non soumis au traitement ni d'autres esp\u00e8ces animales, y compris les poissons et animaux sauvages.</li>\n</ol>\n\n<div class=\"alert alert-info\">\n<h4>Informations additionnelles qui doivent \u00eatre ajout\u00e9es aux \u00e9tiquettes pour les aliments du b\u00e9tail sous forme de notes</h4>\n\n<p>Note</p>\n\n<ul>\n<li>Une fois incorpor\u00e9 dans les aliments, le benzoate d'\u00e9mamectine demeure stable pendant au moins 3 mois.</li>\n</ul>\n</div>\n\n<h4>Compatibilit\u00e9s accept\u00e9es</h4>\n\n<p>Aucune</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}