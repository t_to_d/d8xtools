<?php

use \Symfony\Component\BrowserKit\HttpBrowser;
use \Symfony\Component\HttpClient\HttpClient;


#[\AllowDynamicProperties]
class UpdateExport
{
  public $could_not_load = FALSE;
  public $pageNotFound = FALSE;
  public $success = FALSE;
  protected $node;
  protected $trnode;
  public $data;
  protected $preserveNid = false;
  protected $connection = NULL;
  protected $jsonfilename = '';

  public function loadData($jsonfile)
  {
    $this->jsonfilename = $jsonfile;
    echo "loadData($jsonfile)\n";
    echo "getcwd()= " . getcwd() . ";\n";
    $this->data = json_decode(file_get_contents($jsonfile));
    $this->data->could_not_load = 0;
    if (empty($this->data)) {
      $this->data->could_not_load = 1;
    }
    if (!empty($this->data)) {
      $this->data->could_not_load = 0;
    }
  }

  public function nid()
  {
    // not yet implemented.
    return null;
  }

  public function id()
  {
    return $this->data->node_id;
  }

  public function dcr_id()
  {
    //drush_print('debug d7nid ' . (int) $this->data->nid);
    return (int) $this->data->dcr_id;
  }

  public function loadPageById() {
  }

  /**
   * Import files (images) from the Drupal 7 site
   * @param int $d7fid
   */
  public function importFile($d7fid, &$d8fid=null)
  {
  }

  public function updateField($field, $d8_field=null)
  {
    global $nid_old_to_new;
    global $nid_new_to_old;

    if (!$d8_field) {
      $d8_field = $field;
    }

    if($this->data->language == "en") {
      $origLang = "en";
      $oppLang = "fr";
    }
    else {
      $origLang = "fr";
      $oppLang = "en";
    }

    switch ($field) {
      case 'field_something':
        break;
      default:
	      echo 'Switch default, no action taken.';
        break;
    } //End switch

  } // End updateField function.

  public function validationOfExport() {
    // @TODO Add more validation criteria as needed.
    if (!property_exists($this->data, 'body') && !is_object($this->data)) {
      echo "Validation = false for: " . $this->dcr_id();
      echo "\n";
      return FALSE;
    }
    if (property_exists($this->data, 'body') && is_array($this->data->body)) {
      if (!isset($this->data->body['en']) || !isset($this->data->body['fr'])) {
        echo "Validation = false for: " . $this->dcr_id();
        echo "\n";
        return FALSE;
      }
      if (isset($this->data->body['en']) && isset($this->data->body['fr'])) {
        return TRUE;
      }
    }
    if (property_exists($this->data, 'body') &&
    (is_object($this->data->body) && property_exists($this->data->body, 'en') && property_exists($this->data->body, 'fr'))
    ) {
      return TRUE;
    }
    return FALSE;
  }

  public function save()
  {
    // Write success into the json data, this is basically the final step in the export.
    if ($this->validationOfExport()) {
      $this->data->success = TRUE;
    }
    else {
      $this->data->success = FALSE;
    }
    $this->write_to_json();
  }

  /**
   * Replace double quotes <\"> of json data in an attribute with <&quot;>.
   */
  public function fixJsonAttributeEncoding($lang = 'en') {
    if (property_exists($this->data, 'body') && is_object($this->data->body)) {
      $body_markup = $this->data->body->{$lang};
    }
    else if (property_exists($this->data, 'body') && is_array($this->data->body)) {
      $body_markup = $this->data->body[$lang];
    }
    if (empty($body_markup)) {
      return;
    }
    $regex_dt = '/data-toggle=(\'.*?\')/m';
    preg_match_all($regex_dt, $body_markup, $matches, PREG_SET_ORDER, 0);
    $number_of_groups = count($matches);
    if ($number_of_groups > 0) {
      foreach($matches as $match) {
        $json_to_clean = $match[1];
        $cleaned_json = str_replace('"', '&quot;', $json_to_clean);
        $body_markup = str_replace($json_to_clean, $cleaned_json, $body_markup);
      }
    }

    $regex_dwt = '/data-wb-tables=(\'.*?\')/m';
    preg_match_all($regex_dwt, $body_markup, $matches, PREG_SET_ORDER, 0);
    $number_of_groups = count($matches);
    if ($number_of_groups > 0) {
      foreach($matches as $match) {
        $json_to_clean = $match[1];
        $cleaned_json = str_replace('"', '&quot;', $json_to_clean);
        $body_markup = str_replace($json_to_clean, $cleaned_json, $body_markup);
      }
    }
    $regex_dwt = '/data-wb-boew=(\'.*?\')/m';
    preg_match_all($regex_dwt, $body_markup, $matches, PREG_SET_ORDER, 0);
    $number_of_groups = count($matches);
    if ($number_of_groups > 0) {
      foreach($matches as $match) {
        $json_to_clean = $match[1];
        $cleaned_json = str_replace('"', '&quot;', $json_to_clean);
        $body_markup = str_replace($json_to_clean, $cleaned_json, $body_markup);
      }
    }
    $regex_dwt = '/data-wb-fieldflow=(\'.*?\')/m';
    preg_match_all($regex_dwt, $body_markup, $matches, PREG_SET_ORDER, 0);
    $number_of_groups = count($matches);
    if ($number_of_groups > 0) {
      foreach($matches as $match) {
        $json_to_clean = $match[1];
        $cleaned_json = str_replace('"', '&quot;', $json_to_clean);
        $body_markup = str_replace($json_to_clean, $cleaned_json, $body_markup);
      }
    }
    if (property_exists($this->data, 'body') && is_object($this->data->body)) {
      $this->data->body->{$lang} = $body_markup;
    }
    else if (property_exists($this->data, 'body') && is_array($this->data->body)) {
      $this->data->body[$lang] = $body_markup;
    }
  }

  public function removeChatWizard($lang = 'en') {
    $this->data->chat_wizard = FALSE;
    $re = '/(<div id="sfcr-chtwzrd"?.*)<\/div>/mis';
    $subst = "<div class=\"replaced-chat-wizard\"></div>";
    if (property_exists($this->data, 'body') && is_object($this->data->body)) {
      $this->data->body->{$lang} = preg_replace($re, $subst, $this->data->body->{$lang});
      if (strpos($this->data->body->{$lang}, 'replaced-chat-wizard') > 1) { 
        $this->data->chat_wizard = TRUE;
      }
    }
    else if (property_exists($this->data, 'body') && is_array($this->data->body)) {
      $this->data->body[$lang] = preg_replace($re, $subst, $this->data->body[$lang]);
      if (strpos($this->data->body[$lang], 'replaced-chat-wizard') > 1) { 
        $this->data->chat_wizard = TRUE;
      }
    }
  }

  public function write_to_json() {
    if (file_exists($this->id().'.json')) {
      if ($fp = fopen($this->id().'.json', 'w')) {
        $this->removeChatWizard('en');
        $this->removeChatWizard('fr');
        $this->fixJsonAttributeEncoding('en');
        $this->fixJsonAttributeEncoding('fr');
        echo "Write " . $this->id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        //echo "\n";
        //echo json_last_error_msg();
        //echo "\n";

        fclose($fp);
      }
    } else if (file_exists($this->dcr_id().'.json')) {
      if ($fp = fopen($this->dcr_id().'.json', 'w')) {
        $this->removeChatWizard('en');
        $this->removeChatWizard('fr');
        echo "Write " . $this->dcr_id() .  ".json\n";
        fwrite($fp, json_encode($this->data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        //echo "\n";
        //echo json_last_error_msg();
        //echo "\n";
        fclose($fp);
      }
    } else {
      echo "file does not exist? where is it writing to? what folder is this?.\n";
    }
  }

  public function capture_js_css($html, $lang='en'){
    $re = '/<script\b[^>]*>([\s\S]*?)<\/script>/m';
    preg_match_all($re, $html, $matches, PREG_SET_ORDER, 0);
    // Print the entire match result
    $javascript = [];
    foreach ($matches as $cle => $match) {
      if (isset($matches[$cle][1]) && !empty($matches[$cle][1])) {
        $javascript[] = '<script>' . $matches[$cle][1] . '</script>';
      }
    }

    $javascript = implode('/* --------------------- MORE JAVASCRIPT ------------------- */', $javascript);
    global $js;
    $js[$lang] = $javascript;
    if (is_object(@$this->data->js)) {
      if (!property_exists($this->data->js, $lang) || !$this->data->js->{$lang} == $js[$lang]) {
        $this->data->js->{$lang} = $js[$lang];
      }
    }
    else {
    // echo print_r($this->data, TRUE);die;
      if (!property_exists($this->data, 'js') ||
      !isset($this->data->js[$lang]) ||
      $this->data->js[$lang] != $js[$lang]) {
        $this->data->js[$lang] = $js[$lang];
      }
    }
  }


  public function clean_up_dairy_date(&$html) {
    // For some reason dairyinfo has end of content marker after the date modified (which should be part of the template, not content duh).
    $subst = '';
    $re = '/<dl id="wb-dtmd">(.|\n)*?<\/dl>/m';
    $html = preg_replace($re, $subst, $html);

    // Get rid of footer, some reason 4 of these were in.
    $subst = '';
    $re = '/<footer role="contentinfo" id="wb-info"(.|\n)*?<\/footer>/m';
    $html = preg_replace($re, $subst, $html);

    // Get rid of inferior jquery.min.js from content, wowzers, how bad is that, use the version included with Drupal/wxt.
    $subst = '';
    $re = '/<script src="\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/2\.1\.1\/jquery.min.js">.*?<\/script>/m';
    $html = preg_replace($re, $subst, $html);

    // Get rid of wet-boew.min.js from content, wowzers, how bad is that.
    $subst = '';
    $re = '/<script src="\/res\/wet-boew4.*?<\/script>/m';
    $html = preg_replace($re, $subst, $html);
  }

  public function clean_up_noscript(&$html) {
    $subst = '$1';
    $re = '/<noscript\b[^>]*>([\s\S]*?)<\/noscript>/m';
    $html = preg_replace($re, $subst, $html);
  }

  public function clean_up_js_css(&$html){
   // $re = '/<noscript\b[^>]*>([\s\S]*?)<\/noscript>/gm';
   // $html = preg_replace($re, $subst, $html);
   // $re = '/<style\b[^>]*>([\s\S]*?)<\/style>/m';
   // $html = preg_replace($re, $subst, $html);
    $findres = '/<script src="(\/res\/.*?|\/res.*?|)"><\/script>/m';
    preg_match_all($findres, $html, $resfound, PREG_SET_ORDER, 0);

    $findform = '/<form\b[^>]*>(.*?)/';
    preg_match_all($findform, $html, $formfound, PREG_SET_ORDER, 0);
   
    //mac_feedback
    // $findscript = $re = '/<script\b[^>]*>([\s\S]*?)<\/script>/m';
    // preg_match_all($findscript, $html, $scriptfound, PREG_SET_ORDER, 0);

    $findjson = '/\/resource[\S]*?\/[\S]*?.json/m';
    preg_match_all($findjson, $html, $jsonfound, PREG_SET_ORDER, 0);

    if (!empty($formfound) || !empty($jsonfound) || !empty($resfound)) {
      // Remove jquery 2.2.4
      $regexRemoveJquery = '/<script src="https:\/\/ajax.googleapis.com\/ajax\/libs\/jquery\/2.2.4\/jquery.js">?.*<\/script>/m';
      $str = '
      <strong><script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.js">
      </script></strong>';
      $subst = '<span class="removed jqry224"></span>';

      $html = preg_replace($regexRemoveJquery, $subst, $html);
      // End of remove jquery 2.2.4
      $this->data->mode_html = 'no_editor_full';
      //$this->data->body_format['en'] = 'no_editor_full';
      //$this->data->body_format['fr'] = 'no_editor_full';
      if (!empty($jsonfound)){
        $this->data->json_found = 1;
      }
      if (!empty($resfound)){
        $this->data->jsres_found = 1;
      }
      if (!empty($formfound)) {
        echo $this->data->dcr_id;
        //$findformaction = '/action=\"http(.*?)\?id=('.$this->data->dcr_id.')\"/mi';
        $findformaction = '/action="http(.*?)\?id=('.$this->data->dcr_id.')"/mi';
        //$selfaction = 'action=""';
        $selfaction = ''; // WCAG fix see agrcms/d8#453, needs testing but should work knock on wood.
        $html = preg_replace($findformaction, $selfaction, $html);
      }
    }
    else {
      if (stripos($html, 'arcgis.com') > 0 || stripos($html, "class=\"map\"") > 0 ) {
        echo "Found a map for arcgis.com dcr_id=". $this->data->dcr_id ." json=". $this->jsonfilename ." \n";
        $atlasregex = '/(\/atlas\/API\/js\/embeddedMap_1.3.js)/m';
        $subst = '/mapjs/API/js/embeddedMap_1.3.js';

        $html = preg_replace($atlasregex, $subst, $html);
      }
      else {
        $findscript = '/<script\b[^>]*>([\s\S]*?)<\/script>/m';
        $subst = '';
        $html = preg_replace($findscript, $subst, $html);
        echo "************************************ No Forms here ************************************************\n";  
      }
    }
    $findcomments = '/<!--(.*?)-->/m';
    preg_match_all($findcomments, $html, $commentfound, PREG_SET_ORDER, 0);
    $deletecomments = '';
    if (!empty($commentfound)) {
      $html = preg_replace($findcomments, $deletecomments, $html);
    }
  }

  public function clean_up_content($html, $lang='en') {
    opcache_reset();
    self::clean_up_dairy_date($html);

    $begin_text = '<!-- MainContentStart -->';

    $pos_begin = strpos($html, $begin_text) + strlen($begin_text);
    $end_text = '<!-- Fin du contenu -->';
    if (!self::text_exists($html, $end_text)) {
      $end_text = '<!-- End content -->';
    }
    $pos_end = strpos($html, $end_text) + strlen($end_text);
    //echo "11111111111:" . $pos_end . "\n";
    if ($pos_end == strlen($end_text)) {
      // Fin du contenu not found, use End content instead.
      $end_text = '<!-- End content -->';
      $pos_end = strpos($html, $end_text) + strlen($end_text);
    }

    if ($pos_end <= $pos_begin) {
      $end_text = '<!--Fin du contenu -->';
      if (!self::text_exists($html, $end_text)) {
        $end_text = '<!--End content -->';
      }
      $pos_end = strpos($html, $end_text) + strlen($end_text);
      //echo "22222222222:" . $pos_end . "\n";
      //echo "***************************************************************************************************************************\n";
      // global $clean_this_content_array;
      // echo print_r($clean_this_content_array, true);
      // die;
    }
    // <!-- End content -->

    $html_length = strlen($html);
    $html = substr($html, $pos_begin, ($pos_end-$pos_begin));
    $html = $this->auto_clean_broken_html($html);
    $html = self::remove_junk_cfia1($html);
    $this->clean_up_noscript($html);
    $this->capture_js_css($html, $lang);

    // if ($lang == 'fr') { 
    //  $this->clean_up_js_css_fr($html);
    //   echo "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF:" . "\n";
    // }
    //else {
    $this->clean_up_js_css($html);
      echo "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA:" . "\n";
    //}

    return $html;
   
  }

  public static function remove_junk_cfia1($html) {
    return $html;
    // Attempt to remove leading junk at the beginning of the string as appears on the home page extract, it's not working fully here;
    $re = '/^(.*?)</m';
    $subst = "<";

    $result = preg_replace($re, $subst, $html);
    return $result;
  }

  public static function text_exists($html, $chaine = '-- Fin du contenu --') {
    if (strpos($html, $chaine) >= 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function clean_up_nav_content($html, $lang='en', &$trouvee = FALSE) {
    //return $html;
    opcache_reset();
    $begin_test = 'col-md-pull-9" typeof="SiteNavigationElement" id="wb-sec"';
    $begin_text = '<nav class="wb-sec col-md-3 col-md-pull-9" typeof="SiteNavigationElement" id="wb-sec"';
    $pos_test = stripos($html, $begin_test) + strlen($begin_test);
    $pos_begin = stripos($html, $begin_text);
    // echo $pos_begin;
    // die;
    $end_text = '<!-- Side menu ends -->';
    $end_text_orig = $end_text;
    $pos_end = strrpos($html, $end_text) + strlen($end_text);
    if ($pos_end <= ($pos_begin + strlen($end_text) + 100)) {
      $end_text = '</nav>';
      $pos_end = strrpos($html, $end_text) + strlen($end_text);
      global $clean_this_content_array_nav;
      //echo print_r($clean_this_content_array_nav, true);
      // echo print_r('+++++++++++++++++++++++++++++++++++'.$html.'+++++++++++++++++++++++++++++++++++', true);
      // echo strlen($html);
      // echo print_r('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', true);
      
      // echo print_r($pos_begin, true);
      //echo print_r('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', true);
       echo print_r($pos_end, true);

    }
    if($pos_test > 3) {
      $trouvee=TRUE;
      if (is_object(@$this->data->left_menu_nav)) {
        $this->data->left_menu_nav->{$lang} = 'trouvee';
      }
      else {
        if (!isset($this->data->left_menu_nav[$lang])) {
          $this->data->left_menu_nav[$lang] = 'trouvee';
        }
      }
      // else {
      //   return FALSE;
      // }

      $html = substr($html, $pos_begin, $pos_end-$pos_begin);
      echo print_r('+++++++++++++++++++++++++++++++++++'.$html.'+++++++++++++++++++++++++++++++++++', true);
      //die;
      $html = $this->auto_clean_broken_html($html);
      $this->clean_up_noscript($html);
      $this->capture_js_css($html, $lang);
      $this->clean_up_js_css($html);
      return $begin_text . "\n" .  $html . "\n" . $end_text_orig;
    }
    else {
      echo $lang . " nav not found:\n";
      return FALSE;
    }
  }
  /**
   * Make sure our html is not broken.
   */
  public function auto_clean_broken_html($content) {
    return $content;

    // Detect the string encoding
    $encoding = mb_detect_encoding($content);

    // pass it to the DOMDocument constructor
    $doc = new DOMDocument('', $encoding);

    // Must include the content-type/charset meta tag with $encoding
    // Bad HTML will trigger warnings, suppress those
    @$doc->loadHTML('<html><head>'
      . '<meta http-equiv="content-type" content="text/html; charset='
      . $encoding . '"></head><body>' . trim($content) . '</body></html>');

    // extract the components we want
    $nodes = $doc->getElementsByTagName('body')->item(0)->childNodes;
    $html = '';
    $len = $nodes->length;
    for ($i = 0; $i < $len; $i++) {
      $html .= $doc->saveHTML($nodes->item($i));
    }
    return $html;
  }


  public function path_to_teamsite_content($langcode) {
    if (!isset($this->data->dcr_id) || empty($this->data->dcr_id)) {
      return ' empty dcr_id ?';
    }
    $manuals = FALSE;
    $param = '';
    $param2 = '';
    if (isset($this->data->dcr_type)) {
      if ($this->data->dcr_type == 'manuals-manuels') {
        $manuals = TRUE;
        $param = '?chap=0';
        $param2 = '&chap=0';
      }
    }
    if ($langcode == 'en') {
      if (strtoupper($this->data->managing_branch) == 'DAIRY') {
        if (property_exists($this->data, 'refaire') && $this->data->refaire) {
          return 'https://www.dairyinfo.gc.ca/eng/home/?id=' . $this->data->dcr_id . '&refaire=' . time();
	} else {
          return 'https://www.dairyinfo.gc.ca/eng/home/?id=' . $this->data->dcr_id;
        }
      }
      else {
        if (property_exists($this->data, 'refaire') && $this->data->refaire) {
          return 'https://inspection.canada.ca/eng/' . $this->data->dcr_id . '?refaire=' . time() . $param2;
	} else {
          return 'https://inspection.canada.ca/eng/' . $this->data->dcr_id . $param;
        }
      }
    } else {
      if (strtoupper($this->data->managing_branch) == 'DAIRY') {
        if (isset($this->data->refaire) && $this->data->refaire) {
          return 'https://www.dairyinfo.gc.ca/fra/accueil/?id=' . $this->data->dcr_id . '&refaire=' . time();
	} else {
          return 'https://www.dairyinfo.gc.ca/fra/accueil/?id=' . $this->data->dcr_id . '&test=' . time();
        }
      }
      else {
        if (isset($this->data->refaire) && $this->data->refaire) {
          return 'https://inspection.canada.ca/fra/' . $this->data->dcr_id . '?refaire=' . time() . $param2;
	} else {
          return 'https://inspection.canada.ca/fra/' . $this->data->dcr_id . $param;
        }
      }
    }
  }


  public function add_or_update_body_field() {
    global $section_count_en;
    $section_count_en = 0;
    global $section_count_fr;
    $section_count_fr = 0;
    global $clean_this_content_array;
    $clean_this_content_array = array();
    global $section_array;
    $section_array = array();
    global $css;
    $css = array();
    global $js;
    $js = array();
    global $h1_tmp;
    $h1_tmp = '';

    //sleep(1);
    $client = HttpClient::create(
      ['timeout' => 120,
       'verify_peer' => FALSE
      ]);

    $browser = new HttpBrowser($client, NULL);
    $lang = 'en';
    $language = $lang;
    $otherLang = 'fr';
    $crawler = $browser->request('GET', $this->path_to_teamsite_content($lang));

    $crawler_uri = $crawler->getUri();
    $this->setRedirectedUri($crawler_uri, $lang);
    // We'll need the previous friendly url during the import, this will help.
    $this->data->interwoven_url = $this->data->interwoven_url ?? new stdClass();
    $uri_cleaned = $this->cleanLegacyPath($crawler_uri, $lang);
    $this->data->interwoven_url->{$lang} = $uri_cleaned;

    // Go to the cfia inspection website in english
    echo $this->path_to_teamsite_content('en') . "\n";
    $pageNotFound = FALSE; // Default to false.
    $crawler->filter('head title')->each(function ($node) {
      $title = $node->text();
      $test404string = 'Error 404';
      if (stripos($title, $test404string) > 0) {
        $this->data->pageNotFound = TRUE;
        $this->data->has_dcr_id = FALSE;
      }
    });
    if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
      $test404string = 'Error 404';
      $this->data->pageNotFound = TRUE;
      if (isset($body) && is_object(@$this->data->body) != $test404string) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        @$this->data->body->{$lang} = $test404string;
      } else if (isset($body)) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        $this->data->body[$lang] = $test404string;
      }
      if (isset($body) && is_object(@$this->data->body) != $test404string) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        @$this->data->body->{$lang} = $test404string;
      } else if (isset($body)) {
        // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
        $this->data->body[$lang] = $test404string;
      }
      if (isset($this->data->body->{$otherLang}) && is_object(@$this->data->body) != $test404string) {
        @$this->data->body->{$lang} = $test404string;
      } else if (!isset($this->data->body->fr)) {
        $this->data->body[$otherLang] = $test404string;
      }
      $section_array = array();
      $section_count_en = 0;
      $section_count_fr = 0;
      $this->success = TRUE;
      $this->pageNotFound = TRUE;
      return FALSE;
    }
    //$crawler->filter('main section')->each(function ($node) {
    $this->data->meta = array();
    $crawler->filter('meta')->each(function ($node) {
      $lang = 'en';
      // For aafc/agr.gc.ca, attr_name is 'property'.
      $attr_name = 'property';
      if (empty($node->attr('property'))) {
        // For dairyinfo.gc.ca, attr_name is 'name'.
        $attr_name = 'name';
      }
      if (!empty($node->attr($attr_name))) {
        $attr = $node->attr($attr_name);
        if (strpos($attr, 'subject', 2) <= 0) {
          $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name, eg: dcterms:description = text.
          $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name, eg: dcterms.description = text.
        }
        else {
          echo "Process $attr subject dcr_id " . $this->data->dcr_id . "\n";
          echo "Process $attr subject node_id " . $this->data->node_id . "\n";
        }
        $value = $node->attr('content');
        if (empty($value)) {
          $value = $node->attr('title');
        }
        if (!empty($attr)) {
          $this->data->meta[$attr][$lang] = $value;
        }
        echo $attr . ' ' . $value . "\n";
      } else {
        echo ' empty property english? ' . "\n";
        echo $node->html();
        echo "\n";
      }
    });
    global $h1_tmp;
    $h1_tmp = '';
    $crawler
      ->filter('body main h1')
      ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
          foreach ($crawler as $node) {
            $crawler->filter('h1')->each(function ($n) {
              $lang = 'en';
              if (is_object($n) && !empty($n->text())) {
                if (!isset($this->data->h1) ||
                is_object($this->data->h1)) {
                  global $h1_tmp;
                  if (empty($h1_tmp)){
                    $this->data->h1 = $this->data->h1 ?? new stdClass();
                    $this->data->h1->{$lang} = $n->text();
                    $h1_tmp = $n->text();
                  }
                }
                else {
                  if (!empty($h1_tmp)){
                    $this->data->h1[$lang] = $n->text();
                    $h1_tmp = $n->text();
                  }
                }
              }
              else {
                if (is_object($this->data->h1)) {
                  $this->data->h1->{$lang} = '';
                }
                else {
                  $this->data->h1[$lang] = '';
                }
              }
            });
            $node->parentNode->removeChild($node); 
          }
          $h1_tmp = '';
        }
      );
    $crawler->filter('body main section:first-child')->each(function ($node) {
      $lang = 'en';
      $this->data->body = array();
      $this->data->body['en'] = $this->clean_up_content($node->html(), 'en');
    });
    $crawler->filter('body main section')->each(function ($node) {
      $lang = 'en';
      global $section_count_en;
      global $section_array;
      $section_count_en++;
      $section_array[$lang][] = $this->clean_up_content($node->html(), 'en');
    });
    $crawler
      ->filter('body main')
      ->each(function ($node) {
      global $clean_this_content_array;
      $clean_this_content_array['en'][] = $this->clean_up_content($node->html());
    });

  $crawler
  ->filter('body main section script')
  ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
    foreach ($crawler as $node) {
      $node->parentNode->removeChild($node);
      echo 'IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII';
    }  
  });

    if ($section_count_en >=1) {
      echo $this->data->dcr_id . " debug1 section workaround en\n";
      if (is_object(@$this->data->body)) {
        @$this->data->body->{$lang} = implode("\n\n", $clean_this_content_array[$lang]);
      } else {
        $this->data->body[$lang] = implode("\n\n", $clean_this_content_array[$lang]);
      }
    }
/*   // Internet breadcrumb is different from the intranet breadcrumb, doesn't have the last element, can't use that.
     $crawler
      ->filter('ol.breadcrumb li:last-child')
      ->each(function ($node) {
      if (is_object(@$this->data->breadcrumb)) {
        if (!$this->data->breadcrumb->{'en'} == $node->text()) {
          $this->data->breadcrumb->{'en'} = $node->text();
        }
      }
      else {
        if (!$this->data->breadcrumb->{'en'} == $node->text()) {
          $this->data->breadcrumb['en'] = $node->text();
        }
      }
    });
*/
    // sleep(1);

    $crawler = $browser->request('GET', $this->path_to_teamsite_content($otherLang));

    $crawler_uri = $crawler->getUri();
    $this->setRedirectedUri($crawler_uri, $otherLang);
    // We'll need the previous friendly url during the import, this will help.
    $this->data->interwoven_url = $this->data->interwoven_url ?? new stdClass();
    $uri_cleaned = $this->cleanLegacyPath($crawler_uri, $otherLang);
    $this->data->interwoven_url->{$otherLang} = $uri_cleaned;

    $crawler->filter('meta')->each(function ($node) {
      $otherLang = 'fr';
      // For aafc/agr.gc.ca, attr_name is 'property'.
      $attr_name = 'property';
      if (empty($node->attr('property'))) {
        // For dairyinfo.gc.ca, attr_name is 'name'.
        $attr_name = 'name';
      }
      if (!empty($node->attr($attr_name))) {
        $attr = $node->attr($attr_name);
        if (strpos($attr, 'subject', 2) <= 0) {
          $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name, eg: dcterms:description = text.
          $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name, eg: dcterms.description = text.
        }
        else {
          echo "Process $attr subject dcr_id " . $this->data->dcr_id . "\n";
          echo "Process $attr subject node_id " . $this->data->node_id . "\n";
        }
        $value = $node->attr('content');
        if (empty($value)) {
          $value = $node->attr('title');
        }
        if (!empty($attr)) {
          $this->data->meta[$attr][$otherLang] = $value;
        }
      } else {
        echo ' empty property Français? ' . "\n";
        echo $node->outerHtml();
        echo "\n";
      }
    });
    global $h1_tmp;
    $h1_tmp = '';
    $crawler
      ->filter('body main h1')
      ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
          foreach ($crawler as $node) {
            $crawler->filter('h1')->each(function ($n) {
              $lang = 'fr';
              if (is_object($n) && !empty($n->text())) {
                if (!isset($this->data->h1) ||
                is_object($this->data->h1)) {
                  global $h1_tmp;
                  if (empty($h1_tmp)){
                    $this->data->h1 = $this->data->h1 ?? new stdClass();
                    $this->data->h1->{$lang} = $n->text();
                    $h1_tmp = $n->text();
                  }
                }
                else {
                  if (!empty($h1_tmp)){
                    $this->data->h1[$lang] = $n->text();
                    $h1_tmp = $n->text();
                  }
                }
              }
              else {
                if (is_object($this->data->h1)) {
                  $this->data->h1->{$lang} = '';
                }
                else {
                  $this->data->h1[$lang] = '';
                }
              }
            });
            $node->parentNode->removeChild($node); 
          }
          $h1_tmp = '';
        }
      );
    $crawler
      ->filter('body main section:first-child')
      ->each(function ($node) {
      $otherLang = 'fr';
      if (is_object(@$this->data->body)) {
        @$this->data->body->{$otherLang} = $this->clean_up_content($node->html(), 'fr');
      }
      else {
        $this->data->body[$otherLang] = $this->clean_up_content($node->html(), 'fr');
      }
    });
    $crawler
      ->filter('body main section')
      ->each(function ($node) {
      global $section_count_fr;
      global $section_array;
      $section_count_fr++;
      $section_array['fr'][] = $this->clean_up_content($node->html(), 'fr');
    });
    $crawler
      ->filter('body main')
      ->each(function ($node) {
      global $clean_this_content_array;
      $clean_this_content_array['fr'][] = $this->clean_up_content($node->html(), 'fr');
    });

  if ($section_count_fr > 1/* && strlen($this->data->body['fr']) < 500*/) {
      $otherLang = 'fr';
      echo $this->data->dcr_id . " debug1 section workaround $otherLang\n";
      if (is_object(@$this->data->body)) {
        //@$this->data->body->{$otherLang} = implode($section_array[$otherLang]);
        @$this->data->body->{$otherLang} = implode("\n\n", $clean_this_content_array[$otherLang]);
      } else {
        //$this->data->body[$otherLang] = implode($section_array[$otherLang]);
        $this->data->body[$otherLang] = implode("\n\n", $clean_this_content_array[$otherLang]);
        if (!isset($this->data->body[$lang])) {
          //$this->data->body[$lang] = implode($section_array[$lang]);
          $this->data->body[$lang] = implode("\n\n", $clean_this_content_array[$lang]);
        }
      }
    }
   // Internet breadcrumb is different from the intranet breadcrumb, doesn't have the last element, can't use that.
    $crawler
      ->filter('ol.breadcrumb li:last-child a')
      ->each(function ($node) {
      if (is_object(@$this->data->breadcrumb)) {
        if (!$this->data->breadcrumb->{'fr'} == $node->attr('href') && $this->data->breadcrumb->{'en'}!=$node->attr('href')) {
          $this->data->breadcrumb->{'fr'} = $node->attr('href');
        }
      }
      else {
        if (!$this->data->breadcrumb->{'fr'} == $node->attr('href') && $this->data->breadcrumb->{'en'}!=$node->attr('href')) {
          $this->data->breadcrumb['fr'] = $node->attr('href');
        }
      }
    });
    //$this->data->templatetype = $this->data->templatetype . ' test1223456789';


  }


  public function add_or_update_body_field_no_section() {
      //echo 'here';
      //die;
      global $section_count_en;
      $section_count_en = 0;
      global $section_count_fr;
      $section_count_fr = 0;
      global $clean_this_content_array;
      $clean_this_content_array = array();
      global $clean_this_content_array_nav;
      $clean_this_content_array_nav = array();
      global $section_array;
      $section_array = array();
      global $css;
      $css = array();
      global $js;
      $js = array();

      $lang = 'en';
      $language = $lang;
      $otherLang = 'fr';

      $client = HttpClient::create(
        ['timeout' => 120,
         'verify_peer' => FALSE
        ]);
      $browser = new HttpBrowser($client, NULL);
      $crawler = $browser->request('GET', $this->path_to_teamsite_content($lang));

      // Go to the cfia website in english
      echo $this->path_to_teamsite_content('en') . "\n";
      $pageNotFound = FALSE; // Default to false.
      $crawler->filter('head title')->each(function ($node) {
        $title = $node->text();
        $test404string = 'Error 404';
        if (stripos($title, $test404string) > 0) {
          $this->data->pageNotFound = TRUE;
          $this->data->has_dcr_id = FALSE;
        }
      });
      if (isset($this->data->pageNotFound) && $this->data->pageNotFound) {
        $test404string = 'Error 404';
        $this->data->pageNotFound = TRUE;
        if (isset($body) && is_object(@$this->data->body) != $test404string) {
          // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
          @$this->data->body->{$lang} = $test404string;
        } else if (isset($body)) {
          // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
          $this->data->body[$lang] = $test404string;
        }
        if (isset($body) && is_object(@$this->data->body) != $test404string) {
          // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
          @$this->data->body->{$lang} = $test404string;
        } else if (isset($body)) {
          // REMOVE THE ITEMS FROM THE BODY THAT WE ARE TAKING INTO FIELDS.
          $this->data->body[$lang] = $test404string;
        }
        if (isset($this->data->body->{$otherLang}) && is_object(@$this->data->body) != $test404string) {
          @$this->data->body->{$lang} = $test404string;
        } else if (!isset($this->data->body->fr)) {
          $this->data->body[$otherLang] = $test404string;
        }
        $section_array = array();
        $section_count_en = 0;
        $section_count_fr = 0;
        $this->success = TRUE;
        $this->pageNotFound = TRUE;
        return FALSE;
      }
      //$crawler->filter('main section')->each(function ($node) {
      $this->data->meta = array();
      $crawler->filter('meta')->each(function ($node) {
        $lang = 'en';
        // For aafc/agr.gc.ca, attr_name is 'property'.
        $attr_name = 'property';
        if (empty($node->attr('property'))) {
          // For dairyinfo.gc.ca, attr_name is 'name'.
          $attr_name = 'name';
        }
        if (!empty($node->attr($attr_name))) {
          $attr = $node->attr($attr_name);
          if (strpos($attr, 'subject', 2) <= 0) {
            $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name, eg: dcterms:description = text.
            $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name, eg: dcterms.description = text.
          }
          else {
            echo "Process $attr subject dcr_id " . $this->data->dcr_id . "\n";
            echo "Process $attr subject node_id " . $this->data->node_id . "\n";
          }
          $value = $node->attr('content');
          if (empty($value)) {
            $value = $node->attr('title');
          }
          if (!empty($attr)) {
            $this->data->meta[$attr][$lang] = $value;
          }
          echo $attr . ' ' . $value . "\n";
        } else {
          echo ' empty property english? ' . "\n";
          echo $node->html();
          echo "\n";
        }
      });
      $crawler
        ->filter('body main h1')
        ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
            foreach ($crawler as $node) {
              $node->parentNode->removeChild($node);
            }
          });
      $crawler->filter('body main')->each(function ($node) {
        $lang = 'en';
        $this->data->body = array();
        $this->data->body['en'] = $this->clean_up_content($node->html(), 'en');
      });
     $crawler->filter('body main')->each(function ($node) {
        $lang = 'en';
        global $section_count_en;
        global $section_array;
        $section_count_en++;
        $section_array[$lang][] = $this->clean_up_content($node->html(), 'en');
      });
      $crawler
        ->filter('body main')
        ->each(function ($node) {
        global $clean_this_content_array;
        $clean_this_content_array['en'][] = $this->clean_up_content($node->html());
      });
      $crawler
        ->filter('body nav#wb-sec')
        ->each(function ($node) {
        global $clean_this_content_array_nav;
        global $navfound;
        $navfound= FALSE;
        $show_nav_content = $this->clean_up_nav_content($node->outerHtml(), 'en', $navfound);
        if ($navfound) {
          $clean_this_content_array_nav['en'] = $show_nav_content;
        }
      });

      global $navfound;
      if (!$navfound && $section_count_en >=1) {
        echo $this->data->dcr_id . " debug1 section workaround en\n";
        if (is_object(@$this->data->body)) {
          @$this->data->body->{$lang} = implode("\n\n", $clean_this_content_array[$lang]);
        } else {
          $this->data->body[$lang] = implode("\n\n", $clean_this_content_array[$lang]);
        }
      }
      else if ($navfound) {
        global $clean_this_content_array_nav;
        echo $this->data->dcr_id . " nav found workaround en\n";
        echo print_r($clean_this_content_array_nav, TRUE);
        if (is_object(@$this->data->body)) {
          @$this->data->body->{$lang} = '<section property="mainContentOfPage" typeof="WebPageElement" class="col-md-9 col-md-push-3" id="wb-cont">' . 
          implode("\n\n", $clean_this_content_array[$lang]) . "</section>\n" . implode("\n\n", $clean_this_content_array_nav[$lang]) ;
        } else {
          $this->data->body[$lang] = '<section property="mainContentOfPage" typeof="WebPageElement" class="col-md-9 col-md-push-3" id="wb-cont">' . 
          implode("\n\n", $clean_this_content_array[$lang]) . "</section>\n" . implode("\n\n", $clean_this_content_array_nav[$lang]) ;
        }
        $navfound=FALSE; // Reset the navfound flag, it's a global so needs a reset.
      }

   // Internet breadcrumb is different from the intranet breadcrumb, doesn't have the last element, can't use that.
      $crawler
        ->filter('ol.breadcrumb li:last-child a')
        ->each(function ($node) {
        if (is_object(@$this->data->breadcrumb)) {
          if (!$this->data->breadcrumb->{'en'} == $node->attr('href')) {
            $this->data->breadcrumb->{'en'} = $node->attr('href');
          }
        }
        else {
          if (!$this->data->breadcrumb->{'en'} == $node->attr('href')) {
            $this->data->breadcrumb['en'] = $node->attr('href');
          }
        }
      });

      $crawler = $browser->request('GET', $this->path_to_teamsite_content($otherLang));
      $crawler->filter('meta')->each(function ($node) {
        $otherLang = 'fr';
        // For aafc/agr.gc.ca, attr_name is 'property'.
        $attr_name = 'property';
        if (empty($node->attr('property'))) {
          // For dairyinfo.gc.ca, attr_name is 'name'.
          $attr_name = 'name';
        }
        if (!empty($node->attr($attr_name))) {
          $attr = $node->attr($attr_name);
          if (strpos($attr, 'subject', 2) <= 0) {
            $attr = str_replace('dcterms:', '', $attr); // Remove dcterms: from property name, eg: dcterms:description = text.
            $attr = str_replace('dcterms.', '', $attr); // Remove dcterms. from property name, eg: dcterms.description = text.
          }
          else {
            echo "Process $attr subject dcr_id " . $this->data->dcr_id . "\n";
            echo "Process $attr subject node_id " . $this->data->node_id . "\n";
          }
          $value = $node->attr('content');
          if (empty($value)) {
            $value = $node->attr('title');
          }
          if (!empty($attr)) {
            $this->data->meta[$attr][$otherLang] = $value;
          }
        } else {
          echo ' empty property Français? ' . "\n";
          echo $node->outerHtml();
          echo "\n";
        }
      });
      $crawler
        ->filter('body main h1')
        ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
            foreach ($crawler as $node) {
              $node->parentNode->removeChild($node);
            }
          }
        );
        $crawler
        ->filter('body main div.pagedetails')
        ->each(function (Symfony\Component\DomCrawler\Crawler $crawler) {
            foreach ($crawler as $node) {
              $node->parentNode->removeChild($node);
            }
          }
        );
      $crawler
        ->filter('body main')
        ->each(function ($node) {
        $otherLang = 'fr';
        if (is_object(@$this->data->body)) {
          @$this->data->body->{$otherLang} = $this->clean_up_content($node->html(), 'fr');
        }
        else {
          $this->data->body[$otherLang] = $this->clean_up_content($node->html(), 'fr');
        }
      });
      $crawler
        ->filter('body main')
        ->each(function ($node) {
        global $section_count_fr;
        global $section_array;
        $section_count_fr++;
        $section_array['fr'][] = $this->clean_up_content($node->html(), 'fr');
      });
      $crawler
        ->filter('body main')
        ->each(function ($node) {
        global $clean_this_content_array;
        $clean_this_content_array['fr'][] = $this->clean_up_content($node->html(), 'fr');
        echo "44444444444444444444444444:" . $node->html() . "\n";
        //die;
      });
      $crawler
        ->filter('body nav#wb-sec')
        ->each(function ($node) {
        global $clean_this_content_array_nav;
        global $navfound;
        $navfound= FALSE;
        $show_nav_content = $this->clean_up_nav_content($node->outerHtml(), 'fr', $navfound);
        //echo strlen($navfound);
        //echo $node->outerHtml();
        if ($navfound) {
          $clean_this_content_array_nav['fr'] = $show_nav_content;
        }
      });

      $otherLang = 'fr';

      global $navfound;
      if (!$navfound && $section_count_fr >=1) {
        echo $this->data->dcr_id . " debug1 section workaround fr\n";
        if (is_object(@$this->data->body)) {
          @$this->data->body->{$otherLang} = implode("\n\n", $clean_this_content_array[$otherLang]);
        } else {
          $this->data->body[$otherLang] = implode("\n\n", $clean_this_content_array[$otherLang]);
        }
      }
      else if ($navfound) {
        global $clean_this_content_array_nav;
        echo $this->data->dcr_id . " nav found workaround fr\n";
        //echo print_r($clean_this_content_array_nav, TRUE);
        if (is_object(@$this->data->body)) {
          @$this->data->body->{$otherLang} = '<section property="mainContentOfPage" typeof="WebPageElement" class="col-md-9 col-md-push-3" id="wb-cont">' . 
          implode("\n\n", $clean_this_content_array[$otherLang]) . "</section>\n" . implode("\n\n", $clean_this_content_array_nav[$otherLang]);
        } else {
          $this->data->body[$otherLang] = '<section property="mainContentOfPage" typeof="WebPageElement" class="col-md-9 col-md-push-3" id="wb-cont">' . 
          implode("\n\n", $clean_this_content_array[$otherLang]) . "</section>\n" . implode("\n\n", $clean_this_content_array_nav[$otherLang]);
        }
        $navfound=FALSE; // Reset the navfound flag, it's a global so needs a reset.
      }
      // Internet breadcrumb is different from the intranet breadcrumb, doesn't have the last element, can't use that.
      $crawler
        ->filter('ol.breadcrumb li:last-child a')
        ->each(function ($node) {
        if (is_object(@$this->data->breadcrumb)) {
          if (!$this->data->breadcrumb->{'fr'} == $node->attr('href') && $this->data->breadcrumb->{'en'}!=$node->attr('href')) {
            $this->data->breadcrumb->{'fr'} = $node->attr('href');
          }
        }
        else {
          if (!$this->data->breadcrumb->{'fr'} == $node->attr('href') && $this->data->breadcrumb->{'en'}!=$node->attr('href')) {
            $this->data->breadcrumb['fr'] = $node->attr('href');
          }
        }
      });

  }

  public function cleanLegacyPath($uri, $lang = 'en') {
    // Example uri value:
    // https://inspection.canada.ca/plant-health/invasive-species/directives/potatoes/d-96-05/eng/1323791878398/1323792597204?test=1702246403
    $uri = str_replace('https://inspection.canada.ca', '', $uri);
    if ($lang == 'en') {
      $end_position = stripos($uri, 'eng/' . $this->dcr_id() . '/');
      $uri = substr($uri, 0, $end_position);
    }
    else {
      $end_position = stripos($uri, 'fra/' . $this->dcr_id() . '/');
      $uri = substr($uri, 0, $end_position);
    }
    return $uri;
  }

  public function setRedirectedUri($redirected_uri, $lang = 'en') {
    if (stripos($this->data->teamsite_location->{$lang}, 'edirect_') > 0) {
      $position_of_test_param = stripos($redirected_uri, '?test=');
      $redirected_uri = substr($redirected_uri, 0, $position_of_test_param);
      // Redirects, we want to know what they're redirecting to.
      $this->data->redirected_uri = $this->data->redirected_uri ?? new stdClass();
      $this->data->redirected_uri->{$lang} = $redirected_uri;
    }
  }
}


