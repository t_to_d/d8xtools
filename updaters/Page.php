<?php

use Drush\Drush;

class Page extends UpdateNode
{
  public $success = FALSE;
  public $update_success = FALSE;
  //public $could_not_load = FALSE; // inherited.
  function update($jfile)
  {
    $this->loadData($jfile);
    $success_en = FALSE;
    $success_fr = FALSE;
    $this->updateBodyJson('en');
    $success_en = $this->update_success;
    $this->updateBodyJson('fr');
    $success_fr = $this->update_success;
    if ($success_en || $success_fr) {
      $this->success = TRUE;
      $this->write_to_json();
    }
/*    if () {
    }
    else {
      Drush::output()->writeln( "something for " . $this->data->node_title);
    }*/

  }
}
