{
    "dcr_id": "1606747563503",
    "title": {
        "en": "What to consider when shopping online for food, plants or animal products",
        "fr": "Facteurs \u00e0 consid\u00e9rer quand vous achetez des aliments ou des produits d'origine v\u00e9g\u00e9tale ou animale en ligne"
    },
    "html_modified": "30-11-2020",
    "modified": "22-1-2021",
    "issued": "30-11-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/shopping_online_food_plant_animal_prods_1606747563503_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/shopping_online_food_plant_animal_prods_1606747563503_fra"
    },
    "parent_ia_id": "1297965645317",
    "ia_id": "1606747563894",
    "parent_node_id": "1297965645317",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "What to consider when shopping online for food, plants or animal products",
        "fr": "Facteurs \u00e0 consid\u00e9rer quand vous achetez des aliments ou des produits d'origine v\u00e9g\u00e9tale ou animale en ligne"
    },
    "label": {
        "en": "What to consider when shopping online for food, plants or animal products",
        "fr": "Facteurs \u00e0 consid\u00e9rer quand vous achetez des aliments ou des produits d'origine v\u00e9g\u00e9tale ou animale en ligne"
    },
    "templatetype": "content page 1 column",
    "node_id": "1606747563894",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1297964599443",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "",
        "fr": ""
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "E-commerce",
            "fr": "Cybercommerce"
        },
        "description": {
            "en": "More people are shopping online than ever before, but did you know that the same health and safety requirements apply to online products as those sold in store?",
            "fr": "Le fait qu'une chose soit vendue en ligne ne signifie pas qu'elle peut \u00eatre import\u00e9e au Canada ou exp\u00e9di\u00e9e \u00e0 l'int\u00e9rieur du pays."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, Safe Food for Canadians Regulations, SFCR, food products, General e-commerce, food, plant, animal",
            "fr": "Agence canadienne d'inspection des aliments, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, produit alimentaire, Commerce \u00e9lectronique, aliments, v\u00e9g\u00e9taux, animaux"
        },
        "dcterms.subject": {
            "en": "food,animal,electronic commerce,information,plants,agri-food products,regulations,food safety",
            "fr": "aliment,animal,commerce \u00e9lectronique,information,plante,produit agro-alimentaire,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-02-01",
            "fr": "2021-02-01"
        },
        "modified": {
            "en": "2023-03-07",
            "fr": "2023-03-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "educators,business,general public",
            "fr": "\u00e9ducateurs,entreprises,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "E-commerce",
        "fr": "Cybercommerce"
    },
    "js": {
        "en": "<script> $( \".wb-chtwzrd\" ).trigger( \"wb-init.wb-chtwzrd\" ); </script>",
        "fr": "<script> $( \".wb-chtwzrd\" ).trigger( \"wb-init.wb-chtwzrd\" ); </script>"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-right mrgn-lft-md mrgn-tp-md\"> \n<p><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/e-commerce_web_520X200_1636993389030_eng.png\" class=\"img-responsive\" alt=\"E-commerce \"></p> \n</div>\n<p>More people are shopping online than ever before, but did you know that the same health and safety requirements apply to online products as those sold in store?</p>\n\t\n<p>Products bought or sold online could pose a risk to the health and well-being of Canadians, our livestock, our environment and our economy; and some foreign products sold online may not meet all applicable legal requirements.</p>\n\n<ul>\n<li>Food products can be contaminated with pathogens such as <i lang=\"la\">E. coli</i> or <i lang=\"la\">Listeria</i>, or contain undeclared allergens.</li>\n<li>Plant-based goods can carry <a href=\"/plant-health/invasive-species/eng/1299168913252/1299168989280\">invasive pests</a>, such as the emerald ash borer, or <a href=\"/plant-health/invasive-species/plant-diseases/eng/1322807235798/1322807340310\">diseases</a>, like potato wart.</li>\n<li>Live animals, animal products and animal by-products can transmit serious foreign animal diseases, such as <a href=\"/animal-health/terrestrial-animals/diseases/reportable/african-swine-fever/eng/1306983245302/1306983373952\">African swine fever</a>.</li>\n</ul>\n\n<p>What should you consider when buying or selling food, plants or animal products online?</p>\n\n<p><span class=\"h2 text-success\"><span class=\"glyphicon glyphicon-ok\"></span> </span> <span class=\"text-success\"><strong>DO</strong></span> follow our tips when buying or selling online.</p>\n\n<p><span class=\"h2 text-danger\"><span class=\"glyphicon glyphicon-remove\"></span></span> <span class=\"text-danger\"><strong>DON'T</strong></span> assume that all products available online meet CFIA regulatory requirements.</p>\n\n\n<p>Here is specific information for:</p>\n\n<ul class=\"lst-none lst-spcd\">\n<li><a href=\"/importing-food-plants-or-animals/e-commerce/food/eng/1611779786433/1611779786683\">Food</a> (includes food safety, allergens and fraud)</li>\n<li><a href=\"/importing-food-plants-or-animals/e-commerce/plants/eng/1611780200013/1611780200247\">Plants</a> (includes seeds, plants, bulbs, wood items, insects and other invertebrates)</li>\n<li><a href=\"/importing-food-plants-or-animals/e-commerce/animals/eng/1611780025367/1611780025585\">Animals</a> (includes pets, animal products and by-products)</li>\n\n</ul>\n\n\n<h2>More information</h2>\n\t\n\t\n<div class=\"pattern-demo mrgn-bttm-md\">\n<details>\n<summary><h3 class=\"h5\">Shoppers</h3></summary>\n\n<ul>\n<li><a href=\"/importing-food-plants-or-animals/eng/1573836795867/1573836899201\">Importing food, plants or animals</a></li>\n<li><a href=\"/inspect-and-protect/food-safety/riding-the-e-commerce-wave/eng/1611103500562/1611103704259\">Riding the e-commerce wave: be aware of risks of some online purchases</a> (Inspect and Protect)</li>\n<li><a href=\"/inspect-and-protect/animal-health/meet-lacy/eng/1604423085049/1604423086785\">Meet Lacy, a Canada Border Services Agency detector dog</a> (Inspect and Protect)</li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html\">Online shopping</a> (Innovation, Science and Economic Development Canada)\n<ul>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s1\">Finding legitimate online merchants</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s2\">Checking online product information</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s3\">Entering an online purchase contract</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s4\">Making international online purchases</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s5\">Recognizing potential online transaction scams</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s6\">Protecting yourself from online auctions and bidding scams</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/eng/ca03058.html#s7\">Online safety resources</a></li>\n</ul></li>\n<li><a href=\"http://www.ic.gc.ca/eic/site/cb-bc.nsf/eng/04336.html\">Anti-fraud toolbox</a> (Competition Bureau Canada)</li>\n<li><a href=\"https://www.getcybersafe.gc.ca/en/blogs/covid-19-pandemic-why-cyber-security-more-essential-ever\">COVID-19 pandemic: Why cyber security is more essential than ever</a> (GetCyberSafe.ca)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/personal-personnel/postal-postale-eng.html\">Importing by mail</a> (Canada Border Services Agency)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/courier/lvs-efv/prsn-eng.html\">Importing goods for personal use by courier</a> (Canada Border Services Agency)</li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/consumer-product-safety/advisories-warnings-recalls/subscribe.html\">Consumer Product Safety News</a> (Health Canada)</li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/buying-consumer-products-online.html\">Other consumer products</a> (Health Canada)</li>\n<li><a href=\"https://www.dfo-mpo.gc.ca/species-especes/ais-eae/prevention/index-eng.html\">Preventing aquatic invasive species</a> (Fisheries and Oceans Canada)</li>\n\n\t\n</ul>\n</details>\n\n</div>\n\n<div class=\"pattern-demo mrgn-bttm-md\">\n<details>\n<summary><h3 class=\"h5\">Sellers</h3></summary>\n<ul>\n<li><a href=\"/importing-food-plants-or-animals/eng/1573836795867/1573836899201\">Importing food, plants or animals</a></li>\n<li><a href=\"https://www.getcybersafe.gc.ca/en/blogs/e-commerce-cyber-security-introduction-online-merchants\">E-commerce cyber security: An introduction for online merchants</a> (GetCyberSafe.ca)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/personal-personnel/postal-postale-eng.html\">Importing by mail</a> (Canada Border Services Agency)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/courier/lvs-efv/prsn-eng.html\">Importing goods for personal use by courier</a> (Canada Border Services Agency)</li>\n\n</ul>\n\n</details>\n\n</div>\n\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-right mrgn-lft-md mrgn-tp-md\"> \n<p><img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/e-commerce_web_520X200_1636993389030_fra.png\" class=\"img-responsive\" alt=\"Cybercommerce\"></p> \n</div>\n\n<p>Les gens ach\u00e8tent plus que jamais en ligne, mais saviez-vous que les exigences en mati\u00e8re de sant\u00e9 et de s\u00e9curit\u00e9 sont les m\u00eames pour les produits vendus en ligne et en magasin?</p>\n\n<p>Les produits achet\u00e9s ou vendus en ligne peuvent pr\u00e9senter un risque pour la sant\u00e9 et le bien-\u00eatre des Canadiens, pour celle de nos animaux d'\u00e9levage, pour notre environnement et pour notre \u00e9conomie; certains produits \u00e9trangers offerts en ligne peuvent ne pas respecter toutes les exigences juridiques pertinentes.</p>\n<ul>\n<li>Les produits alimentaires peuvent \u00eatre contamin\u00e9s par des agents pathog\u00e8nes comme <i lang=\"la\">E. coli</i> ou <i lang=\"la\">Listeria</i>, ou encore contenir des allerg\u00e8nes non d\u00e9clar\u00e9s.</li>\n<li>Les produits \u00e0 base de plantes peuvent porter des <a href=\"/protection-des-vegetaux/especes-envahissantes/fra/1299168913252/1299168989280\">ravageurs envahissants</a>, comme l'agrile du fr\u00eane, ou des <a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/fra/1322807235798/1322807340310\">maladies</a>, comme la gale verruqueuse.</li>\n<li>Les animaux vivants, les produits et les sous-produits d'origine animale peuvent transmettre de graves maladies animales \u00e9trang\u00e8res, comme la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/peste-porcine-africaine/fra/1306983245302/1306983373952\">peste porcine africaine</a>.</li>\t\n</ul>\n\n<p>Que devez-vous consid\u00e9rer lors de l'achat ou de la vente de produits alimentaires, v\u00e9g\u00e9taux ou animaux en ligne? </p>\n\n<p><span class=\"h2 text-success\"><span class=\"glyphicon glyphicon-ok\"></span></span> <span class=\"text-success\"><strong>SUIVEZ</strong></span> nos conseils lorsque vous achetez ou vous vendez en ligne.</p>\n\n<p><span class=\"h2 text-danger\"><span class=\"glyphicon glyphicon-remove\"></span></span> <span class=\"text-danger\"><strong>NE pr\u00e9sumez PAS</strong></span> que tous les produits disponibles en ligne satisfont aux exigences r\u00e9glementaires de l'ACIA.</p>\n\n\n<p>Voici de l'information sp\u00e9cifique aux\u00a0:</p>\n\t\n<ul class=\"lst-none lst-spcd\">\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/cybercommerce/aliments/fra/1611779786433/1611779786683\">Aliments</a> (inclut la salubrit\u00e9 des aliments, les allerg\u00e8nes et la fraude alimentaire)</li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/cybercommerce/vegetaux/fra/1611780200013/1611780200247\">V\u00e9g\u00e9taux</a> (inclut les semences, les plantes, les bulbes, les articles en bois, les insectes et autres invert\u00e9br\u00e9s)</li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/cybercommerce/animaux/fra/1611780025367/1611780025585\">Animaux</a> (inclut les animaux de compagnie, les produits et les sous-produits d'origine animale)</li>\t\n</ul>\t\n\t\n<h2>Pour obtenir plus de renseignements</h2>\n\t\n\t\n\t\n<div class=\"pattern-demo mrgn-bttm-md\">\n<details>\n<summary><h3 class=\"h5\">Consommateurs</h3></summary>\n<ul>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/fra/1573836795867/1573836899201\">Importation d'aliments, de v\u00e9g\u00e9taux ou d'animaux</a></li>\n<li><a href=\"/inspecter-et-proteger/salubrite-des-aliments/la-vague-du-commerce-electronique/fra/1611103500562/1611103704259\">La vague du commerce \u00e9lectronique\u00a0: soyez conscient des risques li\u00e9s \u00e0 certains achats en ligne</a> (Inspecter et prot\u00e9ger)</li>\n<li><a href=\"/inspecter-et-proteger/sante-animale/rencontrez-lacy/fra/1604423085049/1604423086785\">Rencontrez Lacy, un chien d\u00e9tecteur de l'Agence des services frontaliers du Canada</a> (Inspecter et prot\u00e9ger)</li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html\">Achats en ligne</a> (Innovation, Sciences et D\u00e9veloppement \u00e9conomique Canada)\n<ul>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s1\">Trouver des commer\u00e7ants en ligne l\u00e9gitimes</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s2\">V\u00e9rifier les renseignements sur les produits en ligne</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s3\">Conclure un contrat d'achat en ligne</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s4\">R\u00e9aliser des achats en ligne \u00e0 l'international</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s5\">Reconna\u00eetre les escroqueries potentielles li\u00e9es aux transactions en ligne</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s6\">Se prot\u00e9ger contre les escroqueries associ\u00e9es \u00e0 la vente aux ench\u00e8res en ligne</a></li>\n<li><a href=\"https://ic.gc.ca/eic/site/oca-bc.nsf/fra/ca03058.html#s7\">Ressources concernant la s\u00e9curit\u00e9 en ligne</a></li>\n</ul>\n\t\n</li>\n<li><a href=\"http://www.ic.gc.ca/eic/site/cb-bc.nsf/fra/04336.html\">Bo\u00eete \u00e0 outils contre la fraude</a> (Bureau de la concurrence Canada)</li>\n<li><a href=\"https://www.pensezcybersecurite.gc.ca/fr/blogues/pandemie-de-la-covid-19-pourquoi-la-cybersecurite-est-plus-importante-que-jamais\">Pand\u00e9mie de la COVID-19\u00a0: Pourquoi la cybers\u00e9curit\u00e9 est plus importante que jamais</a> (Pensez cybers\u00e9curit\u00e9)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/personal-personnel/postal-postale-fra.html\">Importation par la poste</a> (Agence des services frontaliers du Canada)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/courier/lvs-efv/prsn-fra.html\">Importation de marchandises pour usage personnel par messagerie</a> (Agence des services frontaliers du Canada)</li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/securite-produits-consommation/avis-mises-garde-retraits/abonnez-vous.html\">Abonnez-vous au bulletin de nouvelles de la S\u00e9curit\u00e9 des produits de consommation</a> (Sant\u00e9 Canada)</li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/acheter-produits-consommation-ligne.html\">Acheter des produits de consommation en ligne</a> (Sant\u00e9 Canada)</li>\n<li><a href=\"https://www.dfo-mpo.gc.ca/species-especes/ais-eae/prevention/index-fra.html\">Pr\u00e9vention des esp\u00e8ces aquatiques envahissantes</a> (P\u00eaches et Oc\u00e9ans Canada)</li>\n</ul>\n</details>\n</div>\n\n<div class=\"pattern-demo mrgn-bttm-md\">\n<details>\n<summary><h3 class=\"h5\">Vendeurs</h3></summary>\n\n<ul>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/fra/1573836795867/1573836899201\">Importation d'aliments, de v\u00e9g\u00e9taux ou d'animaux</a></li>\n<li><a href=\"https://www.pensezcybersecurite.gc.ca/fr/blogues/cybersecurite-des-sites-de-commerce-en-ligne-une-introduction-lintention-des-commercants\">Cybers\u00e9curit\u00e9 des sites de commerce en ligne\u00a0: une introduction \u00e0 l'intention des commer\u00e7ants</a> (Pensez cybers\u00e9curit\u00e9)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/personal-personnel/postal-postale-fra.html\">Importation par la poste</a> (Agence des services frontaliers du Canada)</li>\n<li><a href=\"https://www.cbsa-asfc.gc.ca/import/courier/lvs-efv/prsn-fra.html\">Importation de marchandises pour usage personnel par messagerie</a> (Agence des services frontaliers du Canada)</li>\n</ul>\n</details>\n\n</div>\n\n\n\n\n\t\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "success": true,
    "chat_wizard": true
}