{
    "dcr_id": "1468878400208",
    "title": {
        "en": "Agriculture Emergency Management",
        "fr": "Gestion des urgences en agriculture"
    },
    "modified": "2018-12-18",
    "issued": "2016-07-21",
    "templatetype": "content page 1 column",
    "page_type": "1",
    "node_id": "1509721848089",
    "dc_date_created": "2016-07-21",
    "managing_branch": "PAB",
    "parent_node_id": "1579021763702",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/CCB-DGCC/WORKAREA/CCB-DGCC/templatedata/comm-comm/gene-gene/data/ag_emerg_mgmt_1468878400208_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/CCB-DGCC/WORKAREA/CCB-DGCC/templatedata/comm-comm/gene-gene/data/ag_emerg_mgmt_1468878400208_fr"
    },
    "type_name": "sitemapinit",
    "has_dcr_id": true,
    "label": {
        "en": "Agriculture Emergency Management",
        "fr": "Gestion des urgences en agriculture"
    },
    "breadcrumb": {
        "en": "Agriculture Emergency Management",
        "fr": "Gestion des urgences en agriculture"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2016-07-21",
            "fr": "2016-07-21"
        },
        "modified": {
            "en": "2018-12-18",
            "fr": "2018-12-18"
        },
        "format": {
            "en": "gcformat",
            "fr": "gcformat"
        },
        "title": {
            "en": "Agriculture Emergency Management",
            "fr": "Gestion des urgences en agriculture"
        },
        "subject": {
            "en": "beef;architectural heritage;grains",
            "fr": "biocarburants;pommes de terre-production;pommes de terre-producteurs"
        },
        "dcterms:subject": {
            "en": "agricultural assistance;animal health;grains",
            "fr": "terre agricole;d\u00e9veloppement communautaire;m\u00e9dicament"
        },
        "description": {
            "en": "Final Report of the Livestock Market Interruption Strategy Steering Committee. Contents. Executive Summary. I. Introduction. II. As a result, the Canadian livestock industry is highly dependent on continued access to international markets, with nearly 70% of hogs and pork products and approximately 50% of cattle and beef products being exported.Given the importance of the sector and a number of inter-related economic, social and environmental considerations at play, large scale interruptions to the livestock market could have broad impacts for the livestock sector beyond the capacity of governments and industry to effectively manage with existing preparations (e.g., beyond what current policies and programs are designed to mitigate). The experience of the 2003 discovery of bovine spongiform encephalopathy (BSE), which resulted in the immediate closure of international markets for Canadian cattle and beef (the impacts of which rippled throughout the entire value chain), highlighted the need for greater preparedness across players.",
            "fr": "Rapport sur les plans et les priorit\u00e9s 2015-2016 Rapport sur les plans et les priorit\u00e9s 2015-2016. Sous-programme 2.1.3\u00a0: Recherche, d\u00e9veloppement et transfert des connaissances. Loi sur la croissance du secteur agricole. .Au Canada, nos investissements dans les provinces et les territoires par le truchement du cadre strat\u00e9gique pour l'agriculture."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": null,
            "fr": null
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "notice",
            "fr": "page administrative"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Agriculture Emergency Management",
        "fr": "Gestion des urgences en agriculture"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n      <p>Emergency events are growing in both number and impact. These events could have significant effects on the agriculture and agri-food sector.</p>\n \n<p>An emergency is a present or imminent event that needs immediate or coordinated action to protect the health, safety or welfare of people, or to limit damage to property or the environment.</p>\n \n<h2>What we are doing about it</h2>\n\n<p>Federal, provincial and territorial (FPT) governments are working together to protect Canada\u2019s agricultural resources and to improve current emergency management practices. They are also collaborating with industry to develop tools and approaches to help prevent or lessen the impact of, prepare for, respond to and recover from agricultural emergencies.</p>\n\n<h3>Emergency Management Bulletin</h3>\n\n<p>We are supporting the agricultural sector as it continues to face an increasing threat landscape.  To support stakeholders, we have developed the Agriculture Emergency Management Bulletin.  The bulletin will be shared bi-monthly via email and will provide information on:</p>\n\n<ul>\n<li>specific risks to the agriculture sector</li>\n<li>resources to support your preparedness</li>\n<li>links to emergency events or emerging issues affecting the sector</li>\n</ul>\n\n<p>Subscribe to receive the latest bulletin in your email inbox and/or share information that could be included in future bulletins: <a href=\"mailto:aafc.agem-guag.aac@canada.ca\">aafc.agem-guag.aac@canada.ca</a></p> \n\n\n<h3>Emergency Management Framework for Agriculture in Canada</h3>\n\n<p>The <a href=\"?id=1471644257525\">Emergency Management Framework for Agriculture in Canada</a> (the Framework) was developed as a foundation for improving Canada's approach to emergency management in agriculture. It sets out national objectives and activities that reflect the complex needs of the sector.</p>\n\n<p>Consultations were held in early 2016 to seek feedback on a draft of the Framework and raise awareness of the work underway to improve emergency management.</p>\n\n<h3>Livestock Market Interruption Strategy </h3>\n\n<p>This strategy helps governments and the livestock sector prepare for and respond to market interruption emergencies. It was developed by federal, provincial and territorial governments along with the industry.</p>\n\n<ul>\n<li><a href=\"?id=1468011698989\">Livestock Market Interruption Strategy Report</a></li>\n</ul>\n\n<h3>Plant and Animal Health Strategy</h3>\n\n<p>This <a href=\"http://www.inspection.gc.ca/about-the-cfia/accountability/consultations-and-engagement/pahs/eng/1490917160508/1490917161242\">Plant and Animal Health Strategy</a> will bring FPT governments and stakeholders together to collectively develop a comprehensive and cohesive approach for managing risks to plant and animal resources in Canada.</p>\n\n<h2>What you can do</h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"?id=1525195829757\">Emergency planning is for producers too</a> </li>\n<li><a href=\"?id=1545150334621\">Emergency Planning is for Industry Organizations too</a> </li>\n</ul>\n\n<h2>Contact information</h2>\n<p>For more information about the work on agriculture emergency management, please send an email to\u00a0<a href=\"mailto:aafc.agem-guag.aac@canada.ca\">aafc.agem-guag.aac@canada.ca</a>.</p>\n\n\n\r\n    ",
        "fr": "\r\n      <p>On remarque une augmentation de la fr\u00e9quence et de l\u2019incidence des  situations d'urgence. Ces situations pourraient avoir des r\u00e9percussions importantes sur le secteur agricole et agroalimentaire.</p>\n \n<p>Une urgence est une situation pr\u00e9sente ou  imminente requ\u00e9rant des actions rapides et coordonn\u00e9es touchant des personnes ou des biens, pour prot\u00e9ger la sant\u00e9, la s\u00e9curit\u00e9 et le bien-\u00eatre des personnes ou limiter les dommages aux biens ou \u00e0 l'environnement.</p>\n\n<h2>Ce que nous faisons  \u00e0 ce sujet</h2>\n\n<p>Les gouvernements f\u00e9d\u00e9ral, provinciaux et territoriaux (FPT) travaillent ensemble pour prot\u00e9ger les ressources agricoles du Canada et am\u00e9liorer les pratiques actuelles de gestion des urgences. Ils collaborent \u00e9galement avec l'industrie pour cr\u00e9er des outils et des m\u00e9thodes visant \u00e0 aider \u00e0 pr\u00e9venir ou att\u00e9nuer les cons\u00e9quences des urgences en agriculture, \u00e0 s'y pr\u00e9parer, \u00e0 intervenir et \u00e0 s'en r\u00e9tablir.</p>\n\n<h3>Bulletin sur la gestion des urgences</h3>\n\n<p>Nous sommes l\u00e0 pour soutenir le secteur agricole qui fait face \u00e0 de plus en plus de menaces. Afin d\u2019aider les intervenants, nous avons cr\u00e9\u00e9 le Bulletin sur la gestion des urgences en agriculture. Ce bulletin sera diffus\u00e9 tous les deux mois par courriel et fournira de l\u2019information sur\u00a0:</p>\n\n<ul>\n<li>les risques pour le secteur agricole</li>\n<li>les ressources qui peuvent vous aider \u00e0 vous pr\u00e9parer en cas d\u2019urgence</li>\n<li>les situations d\u2019urgence ou les nouvelles pr\u00e9occupations dans le secteur agricole (liens Internet)</li>\n</ul>\n\n<p>Pour vous abonner et recevoir le plus r\u00e9cent bulletin par courriel ou pour fournir de l\u2019information qui pourrait \u00eatre incluse dans les prochains bulletins\u00a0: <a href=\"mailto:aafc.agem-guag.aac@canada.ca\">aafc.agem-guag.aac@canada.ca</a></p> \n\n<h3>Cadre de gestion des urgences en agriculture au Canada</h3>\n\n<p>Le <a href=\"?id=1471644257525\">Cadre de gestion des urgences en agriculture au Canada</a> se veut la base pour am\u00e9liorer l'approche du Canada en mati\u00e8re de gestion des urgences en agriculture. Il propose des activit\u00e9s et des objectifs nationaux qui sont adapt\u00e9s aux besoins complexes du secteur.</p>\n \n<p>Des consultations ont \u00e9t\u00e9 men\u00e9es au d\u00e9but de l'ann\u00e9e 2016 afin de solliciter des commentaires sur le cadre provisoire et de faire conna\u00eetre les travaux en cours pour am\u00e9liorer la gestion des urgences.</p>\n\n<h3>Strat\u00e9gie d\u2019intervention en cas de perturbation des march\u00e9s du b\u00e9tail</h3>\n\n<p class=\"mrgn-tp-0\">Cette strat\u00e9gie aide les gouvernements et le secteur de l'\u00e9levage \u00e0 se pr\u00e9parer aux urgences qui entra\u00eenent une perturbation des march\u00e9s et \u00e0 intervenir. Elle a \u00e9t\u00e9 mise au point par les gouvernements FPT et l'industrie.</p>\n\n<ul>\n<li><a href=\"?id=1468011698989\">Strat\u00e9gie d'intervention en cas de perturbation des march\u00e9s du b\u00e9tail - Rapport</a></li>\n</ul>\n\n<h3>Strat\u00e9gie sur la sant\u00e9 des v\u00e9g\u00e9taux et des animaux</h3>\n\n<p><a href=\"http://www.inspection.gc.ca/au-sujet-de-l-acia/responsabilisation/consultation-et-participation/ssva/fra/1490917160508/1490917161242\">Strat\u00e9gie sur la sant\u00e9 des v\u00e9g\u00e9taux et des animaux</a> r\u00e9unira les gouvernements FPT et les intervenants pour qu\u2019ils \u00e9laborent une approche compl\u00e8te et concert\u00e9e en mati\u00e8re de gestion des risques pour les ressources v\u00e9g\u00e9tales et animales au Canada.</p>\n\n<h2>Ce que vous pouvez faire </h2>\n\n<ul class=\"list-unstyled lst-spcd\">\n<li><a href=\"?id=1525195829757\">La planification d\u2019urgences s\u2019adresse aussi aux producteurs</a></li>\n<li><a href=\"?id=1545150334621\">La planification des mesures d\u2019urgence, c\u2019est aussi l\u2019affaire des organisations de l\u2019industrie</a> </li>\n</ul>\n\n\n<h2>Pour nous joindre</h2>\n<p>Pour plus de renseignements au sujet du travail sur la gestion des urgences en agriculture,  veuillez envoyer un courriel \u00e0 <a href=\"mailto:aafc.agem-guag.aac@canada.ca\">aafc.agem-guag.aac@canada.ca</a>.</p>\n\n\r\n    "
    }
}