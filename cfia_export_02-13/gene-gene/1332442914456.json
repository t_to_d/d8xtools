{
    "dcr_id": "1332442914456",
    "title": {
        "en": "Before you shop: food allergies and allergen labelling",
        "fr": "Avant <span class=\"nowrap\">d'acheter :</span> allergies alimentaires et \u00e9tiquetage des allerg\u00e8nes"
    },
    "html_modified": "2024-02-13 9:47:30 AM",
    "modified": "2021-11-03",
    "issued": "2012-03-22 15:01:56",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_tip_labelstoragepackaging_allergen_1332442914456_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_tip_labelstoragepackaging_allergen_1332442914456_fra"
    },
    "ia_id": "1332442980290",
    "parent_ia_id": "1400455563893",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1400455563893",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Before you shop: food allergies and allergen labelling",
        "fr": "Avant <span class=\"nowrap\">d'acheter :</span> allergies alimentaires et \u00e9tiquetage des allerg\u00e8nes"
    },
    "label": {
        "en": "Before you shop: food allergies and allergen labelling",
        "fr": "Avant <span class=\"nowrap\">d'acheter :</span> allergies alimentaires et \u00e9tiquetage des allerg\u00e8nes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1332442980290",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1400426541985",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/labelling/consumers/food-allergies/",
        "fr": "/etiquetage-des-aliments/etiquetage/consommateurs/allergenes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Before you shop: food allergies and allergen labelling",
            "fr": "Avant d'acheter : allergies alimentaires et \u00e9tiquetage des allerg\u00e8nes"
        },
        "description": {
            "en": "The CFIA enforces Canada's labelling laws and works with associations, distributors, food manufacturers and importers to ensure complete and appropriate labelling of all foods.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) veille \u00e0 ce que les lois canadiennes sur l'\u00e9tiquetage soient respect\u00e9es et travaille avec les associations, fabricants, importateurs et distributeurs d'aliments afin de s'assurer que les \u00e9tiquettes de tous les aliments sont compl\u00e8tes et ad\u00e9quates."
        },
        "keywords": {
            "en": "fact sheet, Sesame Seeds, milk, eggs, labelling, allergen, controls, peanuts, fish, crustaceans, shellfish",
            "fr": "produits alimentaires, allergies, rappels, allerg\u00e8ne, aliments, fabricant, arachides, \u00e9valuation sectorielle, \u00e9tiquette"
        },
        "dcterms.subject": {
            "en": "consumers,best practices,agri-food products,consumer protection,food safety",
            "fr": "consommateur,meilleures pratiques,produit agro-alimentaire,protection du consommateur,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-22 15:01:56",
            "fr": "2012-03-22 15:01:56"
        },
        "modified": {
            "en": "2021-11-03",
            "fr": "2021-11-03"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Before you shop: food allergies and allergen labelling",
        "fr": "Avant d'acheter : allergies alimentaires et \u00e9tiquetage des allerg\u00e8nes"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) enforces Canada's labelling laws and works with associations, distributors, food manufacturers and importers to ensure complete and appropriate labelling of all foods. The CFIA recommends that food companies establish effective allergen controls to minimize the potential for allergic reactions.</p>\n<p>Enhanced labelling requirements for food allergen, gluten sources and sulphites came into force on <span class=\"nowrap\">August 4, 2012</span>.</p>\n<h2>Food recalls</h2>\n<p>When the agency becomes aware of a potential serious hazard associated with a food, such as undeclared allergens, the CFIA investigates and takes all appropriate action to protect consumers, which may include a recall of the food product.</p>\n<p><a href=\"/food-recall-warnings-and-allergy-alerts/eng/1351519587174/1351519588221\">Food recalls and allergy alerts</a></p>\n<p>If you are interested in receiving <a href=\"https://notification.inspection.canada.ca/\">e-mail notifications</a> when products are recalled, you can register on the CFIA's website.</p>\n<h2 id=\"facfic\">Food allergies fact sheets</h2>\n<ul>\n<li>Common food allergies and related disorders in Canada\n<ul>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/seafood-fish-crustaceans-shellfish-priority-food-allergens.html\">Crustaceans and molluscs</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/eggs-priority-food-allergen.html\">Eggs</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/seafood-fish-crustaceans-shellfish-priority-food-allergens.html\">Fish</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/gluten-pamphlet.html\">Gluten</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/milk-priority-food-allergen.html\">Milk </a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/mustard-priority-food-allergen.html\">Mustard </a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/peanuts-priority-food-allergen.html\">Peanuts</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/sesame-priority-food-allergen.html\">Sesame</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/priority-food-allergen.html\">Soy</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/sulphites-priority-allergens.html\">Sulphites</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/tree-nuts-priority-food-allergens.html\">Tree Nuts (almonds, Brazil nuts, cashews, hazelnuts, macadamia nuts, pecans, pine nuts, pistachio nuts and walnuts)</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/reports-publications/food-safety/wheat-priority-food-allergen.html\">Wheat and triticale</a></li>\n</ul></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/seasonal-food-safety/safe-school-lunches.html\">Safe school lunches</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-allergies-intolerances/avoiding-allergens-food/tips-avoiding-common-allergens-food.html\">Tips for avoiding common allergens in food</a></li>\n</ul>\n<h2 id=\"indref\">Reference material</h2>\n<ul>\n<li><a href=\"/food-labels/labelling/industry/allergens-and-gluten/gluten-free-claims/eng/1340194596012/1340194681961\">CFIA compliance and enforcement of gluten-free claims</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/food-labelling/allergen-labelling.html\">Food allergen labelling: Health Canada</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/food-safety/food-allergies-intolerances.html\">Food allergen program: Health Canada</a></li>\n<li><a href=\"/eng/1299076382077/1299076493846\">Food recalls allergy alerts</a></li>\n<li><a href=\"/food-labels/labelling/consumers/food-labelling-requirements/eng/1302802599765/1302802702946\">Interactive food label</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/healthy-living/your-health/medical-information/severe-allergic-reactions.html\">Severe allergic reactions: Health Canada</a></li>\n</ul>\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/eng/1383612857522/1383612932341?chap=2#s7c2\">Food allergens, gluten and added sulphites</a></li>\n<li><a href=\"/inspect-and-protect/food-safety/science-in-action/eng/1525791112231/1525791112746\">Video: Science in action: protecting people with allergies</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) veille \u00e0 ce que les lois canadiennes sur l'\u00e9tiquetage soient respect\u00e9es et travaille avec les associations, fabricants, importateurs et distributeurs d'aliments afin de s'assurer que les \u00e9tiquettes de tous les aliments sont compl\u00e8tes et ad\u00e9quates. L'ACIA recommande que toutes les entreprises alimentaires mettent sur pied des mesures efficaces pour contr\u00f4ler les allerg\u00e8nes de mani\u00e8re \u00e0 r\u00e9duire le plus possible le risque de r\u00e9actions allergiques.</p>\n<p>Les exigences d'\u00e9tiquetage am\u00e9lior\u00e9es en ce qui concerne les allerg\u00e8nes alimentaires, les sources de gluten et les sulfites est entr\u00e9 en vigueur le <span class=\"nowrap\">4 ao\u00fbt 2012</span>.</p>\n<h2>Rappels d'aliments</h2>\n<p>Lorsque l'agence est inform\u00e9e de l'existence d'un risque possible associ\u00e9 \u00e0 un aliment, comme la pr\u00e9sence non d\u00e9clar\u00e9e d'allerg\u00e8nes, l'ACIA enqu\u00eate et fait tout ce qu'elle peut pour prot\u00e9ger les consommateurs, ce qui peut inclure le retrait du produit du march\u00e9.</p>\n<p><a href=\"/avertissements/fra/1351519587174/1351519588221\">Rappels d'aliments et alertes \u00e0 l'allergie</a></p>\n<p>Quiconque souhaite recevoir les <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">avis par courriel</a> lorsque des produits font l'objet d'un rappel peut s'inscrire sur le site de l'ACIA.</p>\n<h2 id=\"facfic\">Fiches de renseignements concernant les allergies alimentaires</h2>\n<ul>\n<li>Allergies alimentaires courantes et troubles connexes au Canada\n<ul>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/arachide-allergene-alimentaire-prioritaire.html\">Arachides</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/allergene-alimentaire-prioritaire.html\">Bl\u00e9 ou triticale</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/fruits-mer-poissons-crustaces-mollusques-allergenes-alimentaires-prioritaires.html\">Crustac\u00e9s et mollusques</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/gluten-brochure.html\">Gluten</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/lait-allergene-alimentaire-prioritaire.html\">Lait</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/moutarde-allergenes-alimentaires-prioritaires.html\">Moutarde</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/noix-allergenes-alimentaires-prioritaires.html\">Noix</a> (amandes, noix du Br\u00e9sil, noix de cajou, noisettes, noix de macadamia, pacanes, pignons, pistaches et noix) </li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/oeufs-allergene-alimentaire-prioritaire.html\">Oeufs</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/fruits-mer-poissons-crustaces-mollusques-allergenes-alimentaires-prioritaires.html\">Poisson</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/sesame-allergene-alimentaire-prioritaire.html\">S\u00e9same</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/soja-allergene-alimentaire-prioritaire.html\">Soja</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/rapports-publications/salubrite-aliments/sulfites-allergenes-prioritaires.html\">Sulfites</a></li>\n</ul>\n</li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/salubrite-aliments-saisonniers/conseils-securite-diners-emporter-ecole.html\">Conseils de s\u00e9curit\u00e9\u00a0: d\u00eeners \u00e0 emporter \u00e0 l'\u00e9cole</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/allergies-alimentaires-et-intolerances-alimentaires/eviter-allergenes-aliments/conseils-pour-eviter-allergenes-courants-aliments.html\">Conseils pour \u00e9viter les allerg\u00e8nes courants dans les aliments</a></li>\n</ul>\n<h2 id=\"indref\">Mat\u00e9riel de r\u00e9f\u00e9rence</h2>\n<ul>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/etiquetage-aliments/etiquetage-allergenes.html\">\u00c9tiquetage des allerg\u00e8nes alimentaires\u00a0: Sant\u00e9 Canada</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/consommateurs/exigences-en-matiere-d-etiquetage-des-aliments/fra/1302802599765/1302802702946\">\u00c9tiquette alimentaire interactive</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/salubrite-aliments/allergies-alimentaires-intolerances-alimentaires.html\">Les allergies alimentaires et les intol\u00e9rances alimentaires</a></li>\n<li><a href=\"/etiquetage-des-aliments/etiquetage/industrie/allergenes-et-gluten/allegations-sans-gluten/fra/1340194596012/1340194681961\">Mesures d'application de la loi et de conformit\u00e9 de l'ACIA concernant les all\u00e9gations sans gluten</a></li>\n<li><a href=\"/fra/1299076382077/1299076493846\">Rappels d'aliments alertes \u00e0 l'allergie</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/vie-saine/votre-sante-vous/aspect-medical/reactions-allergiques-severes.html\">R\u00e9actions allergiques s\u00e9v\u00e8res\u00a0: Sant\u00e9 Canada</a></li>\n</ul>\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/fra/1383612857522/1383612932341?chap=2#s7c2\">All\u00e8genes alimentaires, gluten et sulfites ajout\u00e9s</a></li>\n<li><a href=\"/inspecter-et-proteger/salubrite-des-aliments/la-science-en-action/fra/1525791112231/1525791112746\">Vid\u00e9o\u00a0: La science en action\u00a0: prot\u00e9ger les personnes souffrant d'allergies</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}