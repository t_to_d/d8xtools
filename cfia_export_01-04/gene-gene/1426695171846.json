{
    "dcr_id": "1426695171846",
    "title": {
        "en": "Frequently Asked Questions: <i>Seeds Regulations</i>",
        "fr": "Foire aux questions\u00a0: <i>R\u00e8glement sur les semences</i>"
    },
    "html_modified": "16-6-2015",
    "modified": "18-3-2015",
    "issued": "18-3-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_seeds_regs_1426695171846_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_seeds_regs_1426695171846_fra"
    },
    "parent_ia_id": "1419029097256",
    "ia_id": "1426695189678",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Frequently Asked Questions: <i>Seeds Regulations</i>",
        "fr": "Foire aux questions\u00a0: <i>R\u00e8glement sur les semences</i>"
    },
    "label": {
        "en": "Frequently Asked Questions: <i>Seeds Regulations</i>",
        "fr": "Foire aux questions\u00a0: <i>R\u00e8glement sur les semences</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1426695189678",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1419029096537",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/acts-and-regulations/list-of-acts-and-regulations/faq-seeds-regulations/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/faq-reglement-sur-les-semences/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Frequently Asked Questions: Seeds Regulations",
            "fr": "Foire aux questions\u00a0: R\u00e8glement sur les semences"
        },
        "description": {
            "en": "The Frequently Asked Questions below are meant to provide Canadians and businesses with general information about the Canadian Food Inspection Agency\u2019s regulations.",
            "fr": "La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les r\u00e8glements qu'applique l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "frequently asked questions, regulations, Seeds Regulations, seed potatoes, standards, variety, crops",
            "fr": "foire aux questions, renseignements, R\u00e8glement sur les semences, pommes de terre de semence, normes, vari\u00e9t\u00e9, r\u00e9coltes"
        },
        "dcterms.subject": {
            "en": "crops,environment,labelling,exports,imports,agri-food products",
            "fr": "cultures,environnement,\u00e9tiquetage,exportation,importation,produit agro-alimentaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-06-16",
            "fr": "2015-06-16"
        },
        "modified": {
            "en": "2015-03-18",
            "fr": "2015-03-18"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Frequently Asked Questions: Seeds Regulations",
        "fr": "Foire aux questions\u00a0: R\u00e8glement sur les semences"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The <abbr title=\"Frequently Asked Questions\">FAQs</abbr> below are meant to provide Canadians and businesses with general information about the Canadian Food Inspection Agency's regulations.</p>\n<h2 class=\"h5\">1. What is the purpose of these regulations?</h2>\n<p>The <i>Seeds Regulations</i>, under the authority of the <i>Seeds Act</i>, regulate seeds and seed potatoes in Canada that are sold, imported, or exported, as well as seeds released into the environment. They must meet established standards for quality and be labelled so that they are properly represented in the marketplace.  Varieties of most major agricultural field crops must be registered prior to import or sale of seed.</p>\n<h2 class=\"h5\">2. What are the key elements of these regulations?</h2>\n<p><strong>Part <abbr title=\"1\">I</abbr>  - Seeds Other Than Seed Potatoes:</strong>  establishes standards and prescribes the use of variety names for seed; sets requirements for sampling, testing, grading and labelling of seed; provides for the accreditation of graders and licensing of samplers; and sets out requirements for advertising, inspection and importation of seed.</p>\n<p><strong>Part <abbr title=\"2\">II</abbr> \u2013 Seed Potatoes:</strong>  prescribes classes, standards and specific requirements for each class of seed potato; Nuclear Stock, Pre-elite, Elite <abbr title=\"1\">I</abbr>, Elite <abbr title=\"2\">II</abbr>, Elite <abbr title=\"3\">III</abbr>, Elite <abbr title=\"4\">IV</abbr>, Foundation and Certified.  It sets out requirements for inspection and testing performed on specific classes of seed potatoes and provides specific minimum quality standards and grade sizes for seed potato tubers. Also established are requirements for the acceptance of interested seed potato growers' applications for crop inspection and for the issuance of crop certificates, seed potato tags, records of bulk movement, and certificates of authorization. Requirements are provided for tuber storage, packaging, tuber damage, and potato eyes and cut-seed potatoes. Certain provisions regulate the importation and re-certification of seed potatoes, the detention of seized seed potatoes, re-inspection, breeder's selection, non-registered varieties and the application of associated fees.</p>\n<p><strong>Part <abbr title=\"3\">III</abbr> \u2013 Variety Registration:</strong> sets out requirements for the approval of recommending committees, applications for registration, eligibility of varieties and for the registration of varieties of both seed and seed potatoes.</p>\n<p><strong>Part <abbr title=\"4\">IV</abbr> \u2013 Registration of Establishments that Prepare Seed and the Licensing of Operators:</strong> sets out requirements for the registration of an establishment as an approved conditioner, an authorized importer or a bulk storage facility for seed, as well as conditions of registration and operation of these establishments. Also provided are requirements for the licensing of operators of a registered establishment.</p>\n<p><strong>Part <abbr title=\"5\">V</abbr> \u2013 Release of Seed:</strong> requires notification of an intended release of the seed into the environment and allows the Minister to authorize the confined and unconfined release of seed. It also sets out the information to be included with the notification.</p>\n<p><strong>Schedule <abbr title=\"1\">I</abbr>:</strong> sets out standards for specific kinds/species of seed.</p>\n<p><strong>Schedule <abbr title=\"2\">II</abbr>:</strong> sets out the kinds/species of seed that must be graded with a Canada pedigreed grade name when a variety name is used.</p>\n<p><strong>Schedule <abbr title=\"3\">III</abbr>:</strong> sets out the common and scientific names for specific kinds, species or types of crops that are subject to variety registration.</p>\n<h2 class=\"h5\">3. How do these regulations affect Canadian businesses?</h2>\n<p>These regulations help to prevent the importation and spread of noxious weed seeds and provide seed and seed potato buyers with assurances that minimum standards for seed purity, germination and varietal purity have been met. All businesses involved in the sale, import or export of seed and seed potatoes have clear, specific, minimum requirements to meet and transaction costs are minimized by establishing standards and a language for trade.</p>\n<p>These regulations allow for the federal registration of establishments involved in the processing of certified seed or the import of seed as well the licensing of personnel of those establishments. They also allow for the safety assessment of plants with novel traits (including genetically modified plants) prior to their release into the environment.</p>\n<h2 class=\"h5\">4. When did these regulations come-into-force?</h2>\n<p>The <i>Seeds Regulations</i> came into force on July 15, 1960 and have been amended multiple times since then. Part <abbr title=\"5\">V</abbr> came into force on <span class=\"nowrap\">December 19, 1996</span>.</p>\n<h2 class=\"h5\">5. Where can I get more information?</h2>\n<p>Please refer to the appropriate section of the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s website for more information: <a href=\"/plant-health/seeds/eng/1299173228771/1299173306579\">Seeds</a>, <a href=\"/plant-health/potatoes/eng/1299171929218/1299172039964\">Potatoes</a>, <a href=\"/plant-varieties/plants-with-novel-traits/eng/1300137887237/1300137939635\">Plants with Novel Traits</a>.</p>\n<p>Questions relating to seeds and seed potatoes may be directed to the Seed and Potato Specialists at the <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Area offices</a>.</p>\n\n<p>Questions about environmental release of seed may be directed through the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s <a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">Contact Us</a> webpage.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La foire aux questions ci-dessous vise \u00e0 fournir aux Canadiens et aux entreprises des renseignements g\u00e9n\u00e9raux sur les r\u00e8glements qu'applique l'Agence canadienne d'inspection des aliments (ACIA).</p>\n<h2 class=\"h5\">1. Quel est l'objectif de ce r\u00e8glement?</h2>\n<p>Le <i>R\u00e8glement sur les semences</i>, en application de la <i>Loi sur les semences</i>, r\u00e9git les semences et les pommes de terre de semence au Canada qui sont vendues, import\u00e9es ou export\u00e9es ainsi que les semences diss\u00e9min\u00e9es dans l'environnement. Celles ci doivent satisfaire aux normes \u00e9tablies en mati\u00e8re de qualit\u00e9 et \u00eatre \u00e9tiquet\u00e9es de mani\u00e8re \u00e0 \u00eatre bien pr\u00e9sent\u00e9es sur les march\u00e9s. Les vari\u00e9t\u00e9s de la majorit\u00e9 des principales grandes cultures doivent \u00eatre enregistr\u00e9es avant l'importation ou la vente des semences.</p>\n<h2 class=\"h5\">2. Quels sont les principaux \u00e9l\u00e9ments de ce r\u00e8glement?</h2>\n<p><strong>La partie <abbr title=\"1\">I</abbr> (Semences autres que les pommes de terre de semence)</strong> \u00e9tablit des normes, prescrit l'utilisation des noms de vari\u00e9t\u00e9s pour les semences, \u00e9nonce les exigences relatives \u00e0 l'\u00e9chantillonnage, \u00e0 l'analyse, \u00e0 la classification, \u00e0 l'\u00e9tiquetage, \u00e0 l'inspection et \u00e0 l'importation des semences ainsi qu'\u00e0 la publicit\u00e9 concernant celles-ci, pr\u00e9voit l'agr\u00e9ment des classificateurs et la d\u00e9livrance de permis aux \u00e9chantillonneurs.</p>\n<p><strong>La partie <abbr title=\"2\">II</abbr> (Pommes de terre de semence)</strong> prescrit des classes, des normes et des exigences pr\u00e9cises pour chaque classe de pommes de terre de semence, soit Mat\u00e9riel nucl\u00e9aire, Pr\u00e9 \u00c9lite, \u00c9lite <abbr title=\"1\">I</abbr>, \u00c9lite <abbr title=\"2\">II</abbr>, \u00c9lite <abbr title=\"3\">III</abbr>, \u00c9lite <abbr title=\"4\">IV</abbr>, Fondation et Certifi\u00e9e. Elle \u00e9tablit des exigences relatives aux inspections et aux analyses men\u00e9es sur des classes de pommes de terre de semence en particulier, en plus d'\u00e9tablir des normes de qualit\u00e9 minimale pr\u00e9cises et des tailles de tubercules de pommes de terre de semence selon la classe. Cette partie comprend \u00e9galement des exigences concernant l'acceptation des demandes d'inspection des cultures pr\u00e9sent\u00e9es par les producteurs de pommes de terre de semence int\u00e9ress\u00e9s, la d\u00e9livrance de certificats de r\u00e9coltes, les \u00e9tiquettes des pommes de terre de semence, les registres de transport en vrac, les certificats d'autorisation. Elle pr\u00e9cise les exigences relatives \u00e0 l'entreposage des tubercules, l'emballage, les dommages aux tubercules ainsi que les yeux et fragments de pommes de terre. Certaines dispositions r\u00e9gissent l'importation et la recertification des pommes de terre de semence, la retenue de celles qui ont \u00e9t\u00e9 saisies, la r\u00e9inspection, les pommes de terre de semence Choix du s\u00e9lectionneur, les vari\u00e9t\u00e9s non enregistr\u00e9es et l'application des droits connexes.</p>\n<p><strong>La partie <abbr title=\"3\">III</abbr></strong> (Enregistrement des vari\u00e9t\u00e9s) pr\u00e9cise les exigences relatives \u00e0 l'approbation des comit\u00e9s de recommandation, aux demandes d'enregistrement, \u00e0 l'admissibilit\u00e9 des vari\u00e9t\u00e9s et \u00e0 l'enregistrement des vari\u00e9t\u00e9s des semences et des pommes de terre de semence.</p>\n<p><strong>La partie <abbr title=\"4\">IV</abbr> (Agr\u00e9ment des \u00e9tablissements qui conditionnent les semences et agr\u00e9ment des exploitants)</strong> comprend ce qui suit\u00a0: exigences concernant l'agr\u00e9ment des \u00e9tablissements \u00e0 titre de conditionneur agr\u00e9\u00e9, d'importateur autoris\u00e9 ou d'installation d'entreposage en vrac pour les semences; conditions relatives \u00e0 l'agr\u00e9ment et \u00e0 l'exploitation de ces \u00e9tablissements; exigences relatives \u00e0 la d\u00e9livrance de permis aux exploitants d'un \u00e9tablissement agr\u00e9\u00e9.</p>\n<p><strong>La partie <abbr title=\"5\">V</abbr> (Diss\u00e9mination des semences)</strong> exige qu'on \u00e9mette un avis lors de la diss\u00e9mination pr\u00e9vue de semences dans l'environnement et permet au ministre d'autoriser la diss\u00e9mination en milieu confin\u00e9 ou en milieu ouvert des semences, en plus d'\u00e9noncer les renseignements qui doivent accompagner l'avis.</p>\n<p><strong>L'annexe <abbr title=\"1\">I</abbr></strong> prescrit les normes visant des sortes ou des esp\u00e8ces pr\u00e9cises de semences.</p>\n<p><strong>L'annexe <abbr title=\"2\">II</abbr></strong> prescrit les sortes et esp\u00e8ces de semences qui doivent recevoir une d\u00e9nomination de la cat\u00e9gorie Canada g\u00e9n\u00e9alogique lorsqu'un nom de vari\u00e9t\u00e9 est utilis\u00e9.</p>\n<p><strong>L'annexe <abbr title=\"3\">III</abbr></strong> pr\u00e9sente les noms usuels et scientifiques des sortes, esp\u00e8ces ou types de cultures pr\u00e9cis dont les vari\u00e9t\u00e9s doivent \u00eatre enregistr\u00e9es.</p>\n<h2 class=\"h5\">3. Quelle incidence ce r\u00e8glement a-t-il sur les entreprises canadiennes?</h2>\n<p>Ce r\u00e8glement aide \u00e0 emp\u00eacher l'importation et la propagation de mauvaises herbes nuisibles et garantit aux acheteurs de semences et de pommes de terre de semence que les normes minimales relatives \u00e0 la puret\u00e9 des semences, \u00e0 la germination et \u00e0 la puret\u00e9 vari\u00e9tale ont \u00e9t\u00e9 respect\u00e9es. Toutes les entreprises qui vendent, importent ou exportent des semences et des pommes de terre de semence doivent respecter des exigences minimales claires et pr\u00e9cises, et les frais de transaction sont minimis\u00e9s gr\u00e2ce \u00e0 l'\u00e9tablissement de normes et d'un langage pour le commerce.</p>\n<p>Ce r\u00e8glement permet non seulement l'agr\u00e9ment aupr\u00e8s du gouvernement f\u00e9d\u00e9ral des \u00e9tablissements qui participent \u00e0 la transformation des semences certifi\u00e9es ou \u00e0 l'importation de semences, mais aussi la d\u00e9livrance de permis au personnel de ces \u00e9tablissements. Il permet \u00e9galement l'\u00e9valuation de l'innocuit\u00e9 des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux (y compris les v\u00e9g\u00e9taux g\u00e9n\u00e9tiquement modifi\u00e9s) avant leur diss\u00e9mination dans l'environnement.</p>\n<h2 class=\"h5\">4. \u00c0 quel moment ce r\u00e8glement est-il entr\u00e9 en vigueur?</h2>\n<p>Le <i>R\u00e8glement sur les semences</i> est entr\u00e9 en vigueur le 15 juillet 1960; il a \u00e9t\u00e9 modifi\u00e9 de nombreuses fois depuis. La partie <abbr title=\"5\">V</abbr> a pris effet le <span class=\"nowrap\">19 d\u00e9cembre 1996</span>.</p>\n<h2 class=\"h5\">5. O\u00f9 puis-je obtenir plus d'information?</h2>\n<p>Pour obtenir de plus amples renseignements, veuillez consulter les pages appropri\u00e9es sur le site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\u00a0: <a href=\"/protection-des-vegetaux/semences/fra/1299173228771/1299173306579\">Semences</a>, <a href=\"/protection-des-vegetaux/pommes-de-terre/fra/1299171929218/1299172039964\">Pommes de terre</a> ou <a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/fra/1300137887237/1300137939635\">V\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a>.</p>\n<a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureaux des centres op\u00e9rationnels de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a>.\n\n<p>Les questions concernant la diss\u00e9mination des semences dans l'environnement peuvent \u00eatre transmises par le biais de la page Web <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">Contactez-nous</a> de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}