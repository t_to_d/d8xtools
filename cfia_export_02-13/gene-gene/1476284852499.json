{
    "dcr_id": "1476284852499",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Linaria genistifolia</i> (Broomleaf toadflax)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria genistifolia</i> (Linarie \u00e0 feuilles de gen\u00eat)"
    },
    "html_modified": "2024-02-13 9:51:47 AM",
    "modified": "2017-11-01",
    "issued": "2017-11-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_linaria_genistifolia_1476284852499_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_linaria_genistifolia_1476284852499_fra"
    },
    "ia_id": "1476284852905",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Linaria genistifolia</i> (Broomleaf toadflax)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria genistifolia</i> (Linarie \u00e0 feuilles de gen\u00eat)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Linaria genistifolia</i> (Broomleaf toadflax)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria genistifolia</i> (Linarie \u00e0 feuilles de gen\u00eat)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476284852905",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/linaria-genistifolia/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/linaria-genistifolia/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Linaria genistifolia (Broomleaf toadflax)",
            "fr": "Semence de mauvaises herbe : Linaria genistifolia (Linarie \u00e0 feuilles de gen\u00eat)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Linaria genistifolia, Plantaginaceae, Broomleaf toadflax",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Linaria genistifolia, Plantaginaceae, Linarie \u00e0 feuilles de gen\u00eat"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Linaria genistifolia (Broomleaf toadflax)",
        "fr": "Semence de mauvaises herbe : Linaria genistifolia (Linarie \u00e0 feuilles de gen\u00eat)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<h2>Family</h2>\n<p><i lang=\"la\">Plantaginaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Broomleaf toadflax</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Absent from Canada (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to central and eastern Europe and temperate Asia (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Introduced in France, Germany, Norway, and the United States, where it   is present in Oregon, Minnesota, and some of the northeastern states (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length:\u00a00.6 - 0.8 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width:  0.4 - 0.5 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed blocky, often with one tapered end</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed covered in ridges that form a wavy-wrinkled pattern</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed black with a bluish or silver sheen, immature seeds are greyish</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>The seed edges are reticulated and winged, approximately 0.2 <abbr title=\"millimetres\">mm</abbr> wide</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Pastures, roads and disturbed areas (Darbyshire 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>)</h3>\n<ul>\n<li>Dalmatian toadflax seeds are a similar size, blackish colour, rough surface and sharp winged edges as broomleaf toadflax.</li>\n\n<li>The shape tends to be more blocky without one tapered end and the winged edges do not have a strong microscopic pattern compared to broomleaf toadflax.</li>\n</ul>\n\n<h3>Striped toadflax (<i lang=\"la\">Linaria repens</i>)</h3>\n<ul>\n<li>Striped toadflax seeds are a similar size, dark colour and rough surface as broomleaf toadflax.</li>\n\n<li>Striped toadflax seeds lack a winged edge and have a dull brownish surface compared to broomleaf toadflax.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_04cnsh_1475604320280_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Broomleaf toadflax (<i lang=\"la\">Linaria genistifolia</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_01cnsh_1475604274431_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Broomleaf toadflax (<i lang=\"la\">Linaria genistifolia</i>) seeds\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_04cnsh_1475604190946_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_03cnsh_1475604170033_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seed\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_07cnsh_1475604431517_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Striped toadflax (<i lang=\"la\">Linaria repens</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_02cnsh_1475604398256_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Striped toadflax (<i lang=\"la\">Linaria repens</i>) seed\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<h2>Famille</h2>\n<p><i lang=\"la\">Plantaginaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Linarie \u00e0 feuilles de gen\u00eat</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Absente du Canada (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l\u2019Europe centrale et orientale et de l'Asie temp\u00e9r\u00e9e (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Introduite en France, en Allemagne, en Norv\u00e8ge et aux \u00c9tats-Unis, o\u00f9   elle est pr\u00e9sente en Oregon, au Minnesota et dans certains \u00c9tats du   Nord-Est (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 0,6 \u00e0 0,8 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 0,4 \u00e0 0,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine cubo\u00efde, pr\u00e9sentant souvent une extr\u00e9mit\u00e9 effil\u00e9e</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine recouverte de cr\u00eates qui forment un motif pliss\u00e9 ondul\u00e9</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine noire avec des reflets bleut\u00e9s ou argent\u00e9s, les graines immatures sont gris\u00e2tres</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Les bords de la graine sont r\u00e9ticul\u00e9s et ail\u00e9s, mesurant environ 0,2 <abbr title=\"millimetres\">mm</abbr> de largeur</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>P\u00e2turages, chemins et terrains perturb\u00e9s (Darbyshire, 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>)</h3>\n<ul>\n<li>Les graines de la linaire \u00e0 feuilles larges ressemblent \u00e0 celles de la linaire \u00e0 feuilles de gen\u00eat par leurs dimensions, leur couleur noir\u00e2tre, leur surface rugueuse et leurs bords nettement ail\u00e9s.</li>\n\n<li>Comparativement \u00e0 la linaire \u00e0 feuilles de gen\u00eat, la graine de la linaire \u00e0 feuilles larges est g\u00e9n\u00e9ralement plus cubo\u00efde sans avoir une extr\u00e9mit\u00e9 effil\u00e9e et ses bords ail\u00e9s ne pr\u00e9sentent pas un motif microscopique bien marqu\u00e9.</li>\n</ul>\n\n<h3>Linarie stri\u00e9e (<i lang=\"la\">Linaria repens</i>)</h3>\n<ul>\n<li>Les graines de la linaire stri\u00e9e ressemblent \u00e0 celles de la linaire \u00e0 feuilles de gen\u00eat par leurs dimensions, leur couleur fonc\u00e9e et leur surface rugueuse.</li>\n\n<li>Les graines de la linaire stri\u00e9e sont d\u00e9pourvues de bords ail\u00e9s et leur surface est brun\u00e2tre et mate comparativement \u00e0 celles de la linaire \u00e0 feuilles de gen\u00eat.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_04cnsh_1475604320280_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linarie \u00e0 feuilles de gen\u00eat (<i lang=\"la\">Linaria genistifolia</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_01cnsh_1475604274431_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linarie \u00e0 feuilles de gen\u00eat (<i lang=\"la\">Linaria genistifolia</i>) graine\n</figcaption>\n</figure>\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_04cnsh_1475604190946_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_03cnsh_1475604170033_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_07cnsh_1475604431517_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linarie stri\u00e9e (<i lang=\"la\">Linaria repens</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_02cnsh_1475604398256_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linarie stri\u00e9e (<i lang=\"la\">Linaria repens</i>) graine\n</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}