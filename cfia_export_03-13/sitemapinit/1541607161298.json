{
    "dcr_id": "1541607160939",
    "title": {
        "en": "Overview: importing dairy products",
        "fr": "Aper\u00e7u : importation de produits laitiers"
    },
    "html_modified": "2024-03-12 3:58:13 PM",
    "modified": "2021-12-07",
    "issued": "2018-11-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_imp_dairy_prdcts_1541607160939_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_imp_dairy_prdcts_1541607160939_fra"
    },
    "ia_id": "1541607161298",
    "parent_ia_id": "1536170495320",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food",
        "fr": "Importation d\u2019aliments"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1536170495320",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Overview: importing dairy products",
        "fr": "Aper\u00e7u : importation de produits laitiers"
    },
    "label": {
        "en": "Overview: importing dairy products",
        "fr": "Aper\u00e7u : importation de produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1541607161298",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1536170455757",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/dairy/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Overview: importing dairy products",
            "fr": "Aper\u00e7u : importation de produits laitiers"
        },
        "description": {
            "en": "This document outlines requirements specific to importing dairy products.",
            "fr": "Le pr\u00e9sent document donne un aper\u00e7u des exigences particuli\u00e8res qui visent l'importation des produits laitiers."
        },
        "keywords": {
            "en": "Overview, importing dairy products, Specific requirements for imported dairy products, Grades and standards for dairy products, Labelling requirements, Organic dairy products, Export and Imports Permit Act",
            "fr": "Aper\u00e7u, importation de produits laitiers, Exigences particuli\u00e8res relatives \u00e0 l'importation de produits laitiers, Normes d'identit\u00e9 et classification des produits laitiers, Exigences en mati\u00e8re d'\u00e9tiquetage, Produits laitiers biologiques, Loi sur les licences d'exportation et d'importation"
        },
        "dcterms.subject": {
            "en": "cream,agri-food industry,milk,dairy products,regulations,food safety",
            "fr": "cr\u00e8me,industrie agro-alimentaire,lait,produit laitier,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-11-09",
            "fr": "2018-11-09"
        },
        "modified": {
            "en": "2021-12-07",
            "fr": "2021-12-07"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Overview: importing dairy products",
        "fr": "Aper\u00e7u : importation de produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>On this page</h2>\n<ul class=\"list-unstyled mrgn-lft-0\">\n<li><a href=\"#a1\">Introduction</a></li>\n<li><a href=\"#a2\">General information</a></li>\n<li><a href=\"#a3\">Specific requirements for imported dairy products</a></li>\n<li><a href=\"#a4\">Grades and standards for dairy products</a></li>\n<li><a href=\"#a5\">Labelling requirements</a> </li>\n<li><a href=\"#a6\">Organic dairy products</a></li>\n<li><a href=\"#a7\">Export and Imports Permit Act</a></li>\n</ul>\n<h2 id=\"a1\">Introduction</h2>\n<p>This document outlines requirements specific to importing <a href=\"/eng/1430250286859/1430250287405#a68\">dairy products</a>. In order to ensure that you will also meet the <a href=\"/importing-food-plants-or-animals/food-imports/general-requirements/eng/1526656464895/1526656465161\">general import requirements</a> for importing food and the preparation of your preventive control plan, please visit the <a href=\"/importing-food-plants-or-animals/food-imports/step-by-step-guide/eng/1523979839705/1523979840095\">Importing Food: A Step by Step Guide</a> and <a href=\"/preventive-controls/preventive-control-plans/for-importers/eng/1480084425374/1480084519065\">A guide for preparing a preventive control plan\u00a0\u2013\u00a0For importers</a>.</p>\n<p>The specific import requirements for importing dairy products can be found in the <a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System (AIRS)</a>. AIRS information is updated frequently so prior to importing you should verify AIRS to ensure that the import requirements have not changed.</p>\n<h2 id=\"a2\">General information</h2>\n\n\n<p>The Health of Animals Act restricts the importation of milk products from countries where the presence of animal diseases pose a threat to Canadian agriculture and health. Depending on the country of origin, there are certain requirements that must be met in order to import milk products that carry the risk of introducing foot-and-mouth disease into Canada. In most cases you will be required to obtain a zoosanitary export certificate that describes the product and country of origin of the animals from which the milk is obtained. Detailed information on these requirements can be found on the Animal Health <a href=\"/animal-health/terrestrial-animals/imports/import-policies/animal-products-and-by-products/framework/eng/1616507117282/1616507282914\">Terrestrial Animal Products and By-products: Import Policy Framework</a> page.</p>\n\n\n\n<h2 id=\"a3\">Specific requirements for imported dairy products</h2>\n<ul class=\"lst-spcd\">\n<li>The Import for Re-export Program (IREP) facilitates the entry of milk/cream from the United States (US) into Canada for processing and ultimate re-export <a href=\"/exporting-food-plants-or-animals/food-exports/food-specific-export-requirements/irep/eng/1507333026490/1507333072859\">Import for re-export program (IREP)</a> outlines the details related to IREP.</li>\n\n<li>Cheese made from raw milk must be aged more than 60 days before being made available to the public. The exception is raw milk cheese from France coming from a recognized facility. Refer to the <a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/dairy/cheese-policy-and-sample-certificate-france-/eng/1546533329509/1546533514138\">Raw, soft and semi soft cheese policy and sample certificate (France)</a>.</li>\n</ul>\n<h2 id=\"a4\">Grades and standards for dairy products</h2>\n<p>There are grades and standards referred to in the regulations for dairy products. These standards of identity and grades have been combined into a collection of <a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/eng/1518625951131/1518625952071\">Documents incorporated by reference\u00a0\u2013\u00a0Safe Food for Canadians Regulations</a> . Imported dairy products must meet the requirements set out in the following:  </p>\n<ul>\n<li><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-standards-of-identity-volume-1/eng/1521473554991/1521473555532\">Canadian Standards of Identity Volume 1, Dairy Products</a></li>\n<li><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-4/eng/1521118213588/1521118214322\">Canadian Grade Compendium Volume 4, Dairy Products</a></li>\n<li><a href=\"/about-the-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-9/eng/1520647701525/1520647702274?chap=1#s2c1\">Dairy Products Grade Designations for Imported Food</a> (refer to items 1-2 in this table for information relevant to dairy products)</li>\n</ul>\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Resources</h3>\n</header>\n<div class=\"panel-body\">\n<p>Refer to the CFIA web page <a href=\"/food-guidance-by-commodity/dairy/eng/1526653268334/1526653268554\">Food-specific requirements and guidance\u00a0\u2013 Dairy products</a> to view various guidance documents that explain the SFCR requirements. </p>\n</div>\n</section>\n<h2 id=\"a5\">Labelling requirements</h2>\n<p>Imported dairy products must also meet the labelling and packing requirements outlined in the SFCR. The <a href=\"/food-labels/labelling/industry/eng/1383607266489/1383607344939\">Industry labelling tool</a> is a food labelling reference for all industry that outlines the requirement for food labelling and advertising.</p>\n<p><a href=\"/eng/1393082289862/1393082368941\">Labelling requirements for dairy products</a> outline the labelling requirements specific for dairy products.</p>\n<h2 id=\"a6\">Organic dairy products</h2>\n<p>Imported organic dairy products may be certified to the Canadian Organic Standard by a CFIA accredited Certification Body or be certified in accordance with an equivalency arrangement established between Canada and the exporting country. Where an equivalency arrangement is in place, organic products may be certified by a certification body accredited by that country and recognized by Canada. All relevant Canadian legislation would also continue to apply for the imported product.</p>\n<p>Any person who imports a product or markets it in Canada as an organic product must be able to demonstrate, at all times, that the product meets one of the requirements set out above and must retain the documents attesting that the product is organic.</p>\n<p>For further information about organic products refer to <a href=\"/food-labels/organic-products/eng/1526652186199/1526652186496\">Organic products</a>.</p>\n<h2 id=\"a7\">Export and Imports Permit Act</h2>\n<p>Dairy products are one of the many agricultural commodities that are subject to controls under Canada's Export and Imports Permit Act (EIPA). Accordingly, an import permit is required for shipments of dairy products to enter Canada. Import permits for shipments of dairy products destined to the Canadian market are issued to allocation holders under Canada's tariff rate quota (TRQ) for dairy products, which is administered by Global Affairs Canada (GAC). Meaning that under the EIPA, a quota holder can import a specific quantity of dairy products at a lower rate of duty while imports not covered by a quota are subject to higher rates of duty. Further information regarding the importation of dairy products can be found on found on the GAC webpage for <a href=\"http://www.international.gc.ca/controls-controles/prod/agri/dairy-laitiers/index.aspx?lang=eng\">Dairy products</a>.</p>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<h2>Sur cette page</h2>\n<ul class=\"list-unstyled mrgn-lft-0\">\n<li><a href=\"#a1\">Introduction</a></li>\n<li><a href=\"#a2\">Renseignements g\u00e9n\u00e9raux</a></li>\n<li><a href=\"#a3\">Exigences particuli\u00e8res relatives \u00e0 l'importation de produits laitiers</a></li>\n<li><a href=\"#a4\">Normes d'identit\u00e9 et classification des produits laitiers</a></li>\n<li><a href=\"#a5\">Exigences en mati\u00e8re d'\u00e9tiquetage</a> </li>\n<li><a href=\"#a6\">Produits laitiers biologiques</a></li>\n<li><a href=\"#a7\">Loi sur les licences d'exportation et d'importation</a></li>\n</ul>\n<h2 id=\"a1\">Introduction</h2>\n<p>Le pr\u00e9sent document donne un aper\u00e7u des exigences particuli\u00e8res qui visent l'importation des <a href=\"/fra/1430250286859/1430250287405#a68\">produits laitiers</a>. Pour faire en sorte de respecter \u00e9galement les <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-generales/fra/1526656464895/1526656465161\">exigences g\u00e9n\u00e9rales en mati\u00e8re d'importation</a> pour l'importation d'aliments et la pr\u00e9paration du plan de contr\u00f4le pr\u00e9ventif, veuillez consulter les pages <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/guide-par-etape/fra/1523979839705/1523979840095\">Importation d'aliments\u00a0: Un guide par \u00e9tape</a> et <a href=\"/controles-preventifs/plans-de-controle-preventif/guide-pour-les-importateurs/fra/1480084425374/1480084519065\">Pr\u00e9paration d'un plan de contr\u00f4le pr\u00e9ventif\u00a0\u2013 Guide pour les importateurs</a>.</p>\n<p> Les exigences particuli\u00e8res pour l'importation de produits laitiers sont indiqu\u00e9es dans le <a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a>. Les renseignements qui figurent dans le SARI sont mis \u00e0 jour fr\u00e9quemment; par cons\u00e9quent, avant d'importer des produits, nous vous encourageons \u00e0 le v\u00e9rifier pour vous assurer que des changements n'ont pas \u00e9t\u00e9 apport\u00e9s aux exigences en mati\u00e8re d'importation.</p>\n<h2 id=\"a2\">Renseignements g\u00e9n\u00e9raux</h2>\n\n\n\n\n<p>La Loi sur la sant\u00e9 des animaux restreint l'importation de produits laitiers provenant de pays o\u00f9 la pr\u00e9sence de maladies animales constitue une menace pour l'agriculture au Canada et la sant\u00e9 des Canadiens. Selon le pays d'origine, certaines exigences doivent \u00eatre respect\u00e9es pour \u00eatre en mesure d'importer des produits laitiers qui pr\u00e9sentent un risque d'introduire la fi\u00e8vre aphteuse au Canada. Dans la plupart des cas, il faut se procurer un certificat d'exportation zoosanitaire qui d\u00e9crit le produit et indique le pays d'origine des animaux dont provient le lait. Vous trouverez des renseignements d\u00e9taill\u00e9s sur ces exigences sur la page de Sant\u00e9 des animaux <a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/produits-et-sous-produits/cadre/fra/1616507117282/1616507282914\">Produits et sous-produits d'animaux terrestres\u00a0: Cadre sur la politique d'importation</a>.</p>\n\n\n\n\n<h2 id=\"a3\">Exigences particuli\u00e8res relatives \u00e0 l'importation de produits laitiers</h2>\n<ul class=\"lst-spcd\">\n<li>Le Programme d'importation pour r\u00e9exportation (PIR) facilite l'entr\u00e9e de lait ou de cr\u00e8me des \u00c9tats-Unis (\u00c9.-U.) au Canada pour le traitement et la r\u00e9exportation finale. La page <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences-d-exportation-particulieres-aux-produits/pir/fra/1507333026490/1507333072859\">Programme d'importation pour r\u00e9exportation (PIR)</a> fournit des d\u00e9tails sur le PIR.</li>\n<li>Le fromage fait de lait cru doit \u00eatre \u00e2g\u00e9 de plus de 60\u00a0jours avant de pouvoir \u00eatre offert au public, \u00e0 l'exception du fromage au lait cru de France fabriqu\u00e9 dans un \u00e9tablissement reconnu. Se reporter \u00e0 la <a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/produits-laitiers/politique-et-modele-de-certificat-pour-le-fromage-/fra/1546533329509/1546533514138\">Politique et mod\u00e8le de certificat pour le fromage au lait cru \u00e0 p\u00e2te molle ou demi-ferme de France</a>.</li>\n</ul>\n<h2 id=\"a4\">Normes d'identit\u00e9 et classification des produits laitiers</h2>\n<p>La r\u00e9glementation fait \u00e9tat des cat\u00e9gories et des normes qui s'appliquent aux produits laitiers. Ces normes d'identit\u00e9 et ces classifications ont \u00e9t\u00e9 regroup\u00e9es dans un d\u00e9p\u00f4t de <a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/fra/1518625951131/1518625952071\">documents incorpor\u00e9s par renvoi\u00a0\u2013 </a><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/fra/1518625951131/1518625952071\">R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</a>. Les produits laitiers import\u00e9s doivent satisfaire aux exigences \u00e9tablies dans les documents suivants\u00a0: </p>\n<ul>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/normes-d-identite-canadiennes-volume-1/fra/1521473554991/1521473555532\">Normes d'identit\u00e9 canadiennes, Volume\u00a01\u00a0\u2013 Produits laitiers</a></li>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1521118213588/1521118214322\">Recueil des normes canadiennes de classification, Volume\u00a04\u00a0\u2013 Produits laitiers</a></li>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1520647701525/1520647702274?chap=1#s2c1\">D\u00e9signations de cat\u00e9gories applicables aux aliments import\u00e9s</a> (se reporter au point\u00a01 et 2 du tableau pour obtenir des renseignements concernant les produits laitiers)</li>\n</ul>\n<section class=\"panel panel-info\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Ressources</h3>\n</header>\n<div class=\"panel-body\">\n<p>Se reporter \u00e0 la page <span lang=\"en\">Web</span> <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/produits-laitiers/fra/1526653268334/1526653268554\">Exigences et documents d'orientation relatifs \u00e0 certains aliments\u00a0\u2013 Produits laitiers</a> de l'ACIA pour consulter diff\u00e9rents documents d'orientation expliquant les exigences du RSAC.</p></div>\n</section>\n<h2 id=\"a5\">Exigences en mati\u00e8re d'\u00e9tiquetage</h2>\n<p>Les produits laitiers doivent \u00e9galement satisfaire aux exigences en mati\u00e8re d'\u00e9tiquetage et d'emballage \u00e9nonc\u00e9es dans le R\u00e8glement sur la salubrit\u00e9 des aliments au Canada (RSAC). L'<a href=\"/etiquetage-des-aliments/etiquetage/industrie/fra/1383607266489/1383607344939\">Outil d'\u00e9tiquetage de l'industrie</a> est l'outil de r\u00e9f\u00e9rence en mati\u00e8re d'\u00e9tiquetage des aliments qui d\u00e9crit les exigences d'\u00e9tiquetage et de publicit\u00e9.</p>\n<p>Les <a href=\"/fra/1393082289862/1393082368941\">Exigences en mati\u00e8re d'\u00e9tiquetage des produits laitiers</a> d\u00e9crivent les exigences en mati\u00e8re d'\u00e9tiquetage qui s'appliquent aux produits laitiers. </p>\n<h2 id=\"a6\">Produits laitiers biologiques</h2>\n<p>Les produits laitiers biologiques import\u00e9s peuvent \u00eatre certifi\u00e9s en vertu des normes canadiennes sur l'agriculture biologique par un organisme de certification agr\u00e9\u00e9 par l'ACIA ou d'un accord d'\u00e9quivalence entre le Canada et le pays exportateur. Lorsqu'un accord d'\u00e9quivalence est en place, les produits biologiques peuvent \u00eatre certifi\u00e9s par un organisme de certification accr\u00e9dit\u00e9 par ce pays et reconnu par le Canada. Les produits import\u00e9s sont \u00e9galement assujettis \u00e0 toutes les lois canadiennes applicables.</p>\n<p>Quiconque importe un produit ou le commercialise au Canada en tant que produit biologique doit \u00eatre en mesure d'\u00e9tablir, en tout temps, que le produit satisfait \u00e0 l'une ou l'autre des exigences pr\u00e9vues pr\u00e9c\u00e9demment et doit d\u00e9tenir les documents attestant qu'il est biologique.</p>\n<p>Pour de plus amples renseignements sur les produits biologiques, pri\u00e8re de consulter la page <a href=\"/etiquetage-des-aliments/produits-biologiques/fra/1526652186199/1526652186496\">Produits biologiques</a>.</p>\n<h2 id=\"a7\">Loi sur les licences d'exportation et d'importation</h2>\n<p>Les produits laitiers sont l'un des nombreux produits agricoles qui sont assujettis \u00e0 des contr\u00f4les en vertu de la Loisur les licences d'exportation et d'importation (LLEI) du Canada. Par cons\u00e9quent, un permis d'importation doit accompagner toutes les exp\u00e9ditions de produits laitiers \u00e0 destination du Canada. Les permis d'importation pour les exp\u00e9ditions de produits laitiers destin\u00e9es au march\u00e9 canadien sont d\u00e9livr\u00e9s aux d\u00e9tenteurs de parts du contingent tarifaire (CT) du Canada pour les produits laitiers, qui est administr\u00e9 par Affaires mondiales Canada (AMC). Cela signifie qu'en vertu de la LLEI, un titulaire de permis peut importer une quantit\u00e9 donn\u00e9e de produits laitiers \u00e0 des taux de droits de douane inf\u00e9rieurs alors que les importations non vis\u00e9es par un tarif sont assujetties \u00e0 des droits de douane plus \u00e9lev\u00e9s. Pour de plus amples renseignements sur l'importation de produits laitiers, consultez la page <span lang=\"en\">Web</span> d'AMC sur les <a href=\"http://www.international.gc.ca/controls-controles/prod/agri/dairy-laitiers/index.aspx?lang=fra\">Produits laitiers</a>.</p>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}