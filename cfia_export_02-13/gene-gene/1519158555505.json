{
    "dcr_id": "1519158555505",
    "title": {
        "en": "New Zealand - Export requirements for milk and dairy products",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "html_modified": "2024-02-13 9:52:44 AM",
    "modified": "2021-06-14",
    "issued": "2018-03-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_newzealand_1519158555505_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_milk_dairy_newzealand_1519158555505_fra"
    },
    "ia_id": "1519158556034",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Inspecting and investigating - Performing export certification activities|Exporting food",
        "fr": "Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation|Exportation d\u2019aliments"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "New Zealand - Export requirements for milk and dairy products",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "label": {
        "en": "New Zealand - Export requirements for milk and dairy products",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "templatetype": "content page 1 column",
    "node_id": "1519158556034",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/new-zealand-milk-and-dairy-products/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/nouvelle-zelande-lait-et-produits-laitiers/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "New Zealand - Export requirements for milk and dairy products",
            "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le lait et les produits laitiers"
        },
        "description": {
            "en": "Countries to which exports are currently made \u2013 New Zealand",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Nouvelle-Z\u00e9lande"
        },
        "keywords": {
            "en": "New Zealand, export, requirements, milk, dairy products",
            "fr": "Nouvelle-Z\u00e9lande, exportation, exigences, lait, produits laitiers"
        },
        "dcterms.subject": {
            "en": "agri-food industry,certification,dairy products,exports,inspection",
            "fr": "industrie agro-alimentaire,accr\u00e9ditation,produit laitier,exportation,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-03-09",
            "fr": "2018-03-09"
        },
        "modified": {
            "en": "2021-06-14",
            "fr": "2021-06-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "New Zealand - Export requirements for milk and dairy products",
        "fr": "Nouvelle-Z\u00e9lande - Exigences d'exportation pour le lait et les produits laitiers"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Canada does not have a negotiated certificate with New Zealand (NZ). Export certification is based on the requirements of this country.</p>\n<h2>1. Eligible/ineligible product</h2>\n<h3>Eligible</h3>\n<ul>\n<li>There is no eligibility list for dairy products. However, the Import health standards of NZ, define dairy products as all products manufactured from the milk and cream of animals and intended for human consumption.</li>\n</ul>\n<h3>Ineligible</h3>\n<ul>\n<li>Information not available.</li>\n</ul>\n<h2>2. Pre-export approvals by the competent authority of the importing country</h2>\n<p>No. However, it is recommended to confirm that with the importer.</p>\n<h2>3.\tProduction controls and inspection requirements</h2>\n<p>The inspector must verify during a preventive control inspection that the establishment is aware of the dairy standards and requirements of New Zealand and has a specific export procedure in place.</p>\n<p>As per the manufacturer's declaration required by the NZ, the statements below must be met.</p>\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p><strong>(On manufacturer's letterhead)</strong></p>\n<h3 class=\"text-center\">Manufacturer's declaration</h3>\n<h4 class=\"text-center\">For the export of dairy products or product containing dairy ingredients for human consumption from Canada to New Zealand</h4>\n<p class=\"mrgn-tp-lg\">I, <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"200\" height=\"12\"> being the Manager of the facility where the product identified in this Zoosanitary Certificate has been processed, certify that;</p>\n<ol class=\"lst-spcd\">\n<li>The dairy ingredient used to manufacture the product has been heat treated in conformity with one of the following procedures:\n<p class=\"mrgn-tp-md\"><strong>Either</strong><br>\n1.1\ta minimum temperature of 132\u00b0C for at least one second (ultra-high temperature [UHT]);\n</p>\n<p><strong>Or</strong><br>\n1.2\tIf the milk has a pH less then 7.0, a minimum temperature of 72\u00b0C for at least 15 seconds (high temperature-short time pasteurisation [HTST]);\n</p>\n<p><strong>Or</strong><br>\n1.3\tIf the milk has pH of 7.0 or over, the HTST process applied twice.\n</p>\n<p class=\"text-center\">(Delete 1.1, 1.2 or 1.3 as applicable)</p>\n</li>\n<li>The dairy product or product containing dairy ingredients have been produced in premises that are registered or approved by the Competent Government Authority as having acceptable food safety standards governing the processing of dairy products for export.</li>\n<li>The dairy product or product containing dairy ingredients are in sealed packaging.</li>\n</ol>\n<div class=\"row mrgn-tp-xl\">\n<p class=\"col-sm-6\"><img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"200\" height=\"12\"><br>Signature of Manager</p>\t\n<p class=\"col-sm-6\"><img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"200\" height=\"12\"><br>Date</p>\t\n</div>\n<p class=\"mrgn-tp-lg\">Name of Manager: <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"350\" height=\"12\"></p>\t\n<p>Title of Manager: <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"350\" height=\"12\"></p>\t\n<p>Address of Manager: <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"350\" height=\"12\"></p>\t\n\n</div>\n<h2>4.\tLabelling, packaging and marking requirements</h2>\n<ul>\n<li>It is the exporter's responsibility to meet all the requirements for labelling, packaging and marking requirements as per the importing country.</li>\n<li>The packaging must be impervious and sealed at the point of manufacture. The original packaging must be intact i.e. has not been opened.</li>\n</ul>\n<h2>5. Export documents available upon request</h2>\n<h3>Certificate</h3>\n<ul>\n<li>Export health certificate for dairy products or products containing dairy ingredients for human consumption. <strong>CFIA/ACIA\u00a05946</strong>.\n<p class=\"mrgn-tp-md\"><strong>Note:</strong></p>\n<ul>\n<li>The export health certificate CFIA/ACIA\u00a05946 must be accompanied by the Manufacturer's declaration (in English). See\u00a03 above. This document will be endorsed by the CFIA. </li>\n<li>It is the responsibility of the manufacturer to develop, complete and provide the Manufacturer's declaration (in English) as per the model described above in section\u00a03.</li>\n</ul>\n</li>\n</ul>\n<h2>6.\tAdditional information</h2>\n<ul>\n<li>Samples (personal or commercial) of dairy products may be subject to the same requirements as a regular shipment. It is strongly recommended that the exporter verify these requirements with his importer. </li>\n<li>Exported dairy products transiting through a country may require transit documentation. It is the responsibility of the exporter to ensure that the shipment will be accompanied by all the necessary certificates. Please work closely with your importer.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le Canada n'a pas de certificat n\u00e9goci\u00e9 avec la Nouvelle-Z\u00e9lande (NZ). La certification \u00e0 l'exportation est faite sur la base des exigences de ce pays. </p>\n<h2>1.\tProduits admissibles/non admissibles</h2>\n<h3>Admissibles</h3>\n<ul>\n<li>Il n'existe pas de liste d'\u00e9ligibilit\u00e9 de produits laitiers. Cependant, les standards sanitaires d'importation de la  NZ, d\u00e9finissent les produits laitiers comme tous les produits fabriqu\u00e9s \u00e0 partir de lait et de cr\u00e8me d'animaux et qui sont destin\u00e9s \u00e0 la consommation humaine.</li>\n</ul>\n<h3>Non admissibles</h3>\n<ul>\n<li>Information non disponible.</li>\n</ul>\n<h2>2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n<p>Non. Cependant, il est recommand\u00e9 de confirmer cela aupr\u00e8s de son importateur. </p>\n<h2>3.\tMesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n<p>L'inspecteur doit v\u00e9rifier au cours d'une inspection de contr\u00f4le pr\u00e9ventif que l'\u00e9tablissement est au courant des normes et exigences des produits laitiers de la Nouvelle-Z\u00e9lande et qu'une proc\u00e9dure d'exportation sp\u00e9cifique est en place.</p>\n<p>Selon la d\u00e9claration du fabricant exig\u00e9e par la NZ, les \u00e9nonc\u00e9s ci-dessous doivent \u00eatre rencontr\u00e9s. </p>\n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<p><strong>(Avec en-t\u00eate du fabricant)</strong></p>\n<h3 class=\"text-center\">D\u00e9claration du fabricant</h3>\n<h4 class=\"text-center\">Pour l'exportation de produits laitiers ou de produits contenant des ingr\u00e9dients laitiers destin\u00e9s \u00e0 la consommation humaine du Canada vers la Nouvelle Z\u00e9lande</h4>\n<p class=\"mrgn-tp-lg\">Je, <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_fra.gif\" width=\"200\" height=\"12\"> gestionnaire de l'\u00e9tablissement o\u00f9 a \u00e9t\u00e9 trait\u00e9 le produit identifi\u00e9 dans le pr\u00e9sent  certificat zoosanitaire, certifie que;</p>\n<ol class=\"lst-spcd\">\n<li>L'ingr\u00e9dient laitier utilis\u00e9 pour la fabrication du produit a \u00e9t\u00e9 trait\u00e9 conform\u00e9ment \u00e0 l'un des proc\u00e9d\u00e9s suivants\u00a0:\n<p class=\"mrgn-tp-md\"><strong>Soit</strong><br>\n1.1\tune temp\u00e9rature minimale de 132\u00b0\u00a0C pendant au moins\u00a01 seconde (ultra-haute temp\u00e9rature [UHT]);\n</p>\n<p><strong>Ou</strong><br>\n1.2\tsi le lait a un pH inf\u00e9rieur \u00e0\u00a07.0, une temp\u00e9rature minimale de\u00a072\u00b0\u00a0C pendant au moins\u00a015 secondes (pasteurisation \u00e0 haute temp\u00e9rature-courte dur\u00e9e [HTST]);\n</p>\n<p><strong>Ou</strong><br>\n1.3 si le lait a un pH de\u00a07.0 ou plus, le proc\u00e9d\u00e9 HTST est appliqu\u00e9 deux fois.\n</p>\n<p class=\"text-center\">(Supprimer 1.1, 1.2 ou 1.3 selon le cas)</p>\n</li>\n<li>Le produit laitier ou le produit contenant des ingr\u00e9dients laitiers a \u00e9t\u00e9 fabriqu\u00e9 dans des locaux qui sont enregistr\u00e9s ou approuv\u00e9s par l'autorit\u00e9 gouvernementale comp\u00e9tente comme ayant des normes de s\u00e9curit\u00e9 alimentaire acceptables r\u00e9gissant la transformation des produits laitiers pour l'exportation.</li>\n<li>Le produit laitier ou le produit contenant des ingr\u00e9dients laitiers est dans un emballage scell\u00e9.</li>\n</ol>\n<div class=\"row mrgn-tp-xl\">\n<p class=\"col-sm-6\"><img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_fra.gif\" width=\"200\" height=\"12\"><br>Signature du manager</p>\t\n<p class=\"col-sm-6\"><img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_fra.gif\" width=\"200\" height=\"12\"><br>Date</p>\t\n</div>\n<p class=\"mrgn-tp-lg\">Nom du manager\u00a0: <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"350\" height=\"12\"></p>\t\n<p>Titre du manager\u00a0: <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"350\" height=\"12\"></p>\t\n<p>Adresse du manager\u00a0: <img alt=\"space\" src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/code-space_1384467519696_eng.gif\" width=\"350\" height=\"12\"></p>\t\n</div>\n<h2>4. Exigences en mati\u00e8re d'\u00e9tiquetage, emballage et de marquage</h2>\n<ul>\n<li>Il incombe \u00e0 l'exportateur de satisfaire \u00e0 toutes les exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage du pays importateur.</li>\n<li>L'emballage doit \u00eatre imperm\u00e9able et scell\u00e9 au point de fabrication. L'emballage d'origine doit \u00eatre intact, c'est-\u00e0-dire qu'il n'a pas \u00e9t\u00e9 ouvert. </li>\n</ul>\n<h2>5.\tDocuments d'exportation disponibles sur demande</h2>\n<h3>Certificat</h3>\n<ul>\n<li>Certificat sanitaire d'exportation pour les produits laitiers ou les produits contenant des ingr\u00e9dients laitiers destin\u00e9s \u00e0 la consommation humaine. <strong>CFIA/ACIA\u00a05946</strong>.\n<p class=\"mrgn-tp-md\"><strong>Remarque\u00a0:</strong></p>\n<ul>\n<li>Le certificat sanitaire d'exportation CFIA/ACIA 5946 doit \u00eatre accompagn\u00e9 de la D\u00e9claration du fabricant (en anglais). Voir\u00a03 ci-dessus. Ce document sera endoss\u00e9 par l'ACIA.</li>\n<li>C'est la responsabilit\u00e9 du fabricant de d\u00e9velopper, de compl\u00e9ter et de fournir la D\u00e9claration du fabricant (en anglais) selon le mod\u00e8le d\u00e9crit \u00e0 la section\u00a03 ci-dessus.</li>\n</ul>\n</li>\n</ul>\n<h2>6.\tInformations additionnelles</h2>\n<ul>\n<li>Les \u00e9chantillons (personnels ou commerciaux) de produits laitiers pourraient \u00eatre soumis aux m\u00eame exigences qu'une exp\u00e9dition r\u00e9guli\u00e8re. Il est fortement recommand\u00e9 \u00e0 l'exportateur de v\u00e9rifier ces exigences aupr\u00e8s de son importateur. </li>\n<li>Des produits export\u00e9s transitant par un pays pourraient n\u00e9cessiter des documents de transit. C'est la responsabilit\u00e9 de l'exportateur de s'assurer que son exp\u00e9dition sera accompagn\u00e9e de tous les certificats n\u00e9cessaires. Veuillez travailler en \u00e9troite collaboration avec votre importateur.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}