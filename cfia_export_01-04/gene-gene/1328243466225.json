{
    "dcr_id": "1328243466225",
    "title": {
        "en": "VB product submission checklist \u2013 Diagnostic kits",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV - Trousses diagnostiques"
    },
    "html_modified": "2012-02-02 23:31",
    "modified": "15-2-2023",
    "issued": "2-2-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/vetbio_guide_31_appd_1328243466225_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/vetbio_guide_31_appd_1328243466225_fra"
    },
    "parent_ia_id": "1328225600916",
    "ia_id": "1328243689123",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "VB product submission checklist \u2013 Diagnostic kits",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV - Trousses diagnostiques"
    },
    "label": {
        "en": "VB product submission checklist \u2013 Diagnostic kits",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV - Trousses diagnostiques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1328243689123",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1328225508353",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/veterinary-biologics/guidelines-forms/3-1/checklist-diagnostic-kits/",
        "fr": "/sante-des-animaux/produits-biologiques-veterinaires/lignes-directrices-formulaires/3-1/liste-de-controle-trousses-diagnostiques/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "VB product submission checklist \u2013 Diagnostic kits",
            "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV - Trousses diagnostiques"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency's Canadian Centre for Veterinary Biologics is responsible for regulating the manufacturing, importation, testing, distribution, and use of veterinary biologics in Canada.",
            "fr": "Le Centre canadien des produits biologiques v\u00e9t\u00e9rinaires (CCPBV) de l'Agence canadienne d'inspection des aliments (ACIA) est charg\u00e9e de la r\u00e9glementation pertinente \u00e0 la fabrication, \u00e0 l'importation, \u00e0 l'analyse, \u00e0 la distribution et \u00e0 l'utilisation des produits biologiques v\u00e9t\u00e9rinaires au Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, veterinary biologics, licensing requirements, checklists, guidelines, new product, diagnostic kits",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glements sur la sant\u00e9 des animaux, produits biologiques v\u00e9t\u00e9rinaires, d\u00e9livrance des permis, lignes directrices, listes de contr\u00f4le, nouveaux produits, trousses diagnostiques"
        },
        "dcterms.subject": {
            "en": "inspection,veterinary medicine,standards,regulation,animal health",
            "fr": "inspection,m\u00e9decine v\u00e9t\u00e9rinaire,norme,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-02 23:31:09",
            "fr": "2012-02-02 23:31:09"
        },
        "modified": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "type": {
            "en": "reference material,standard,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,norme,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "VB product submission checklist \u2013 Diagnostic kits",
        "fr": "Liste de contr\u00f4le des pr\u00e9sentations de PBV - Trousses diagnostiques"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th>No.</th>\n<th>Documentation requirement</th>\n<th>Canadian applicant</th>\n<th>US or foreign applicant</th>\n<th>File or document name, version and date (yyyymmdd), if applicable</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>00.</td>\n<td>Index of submission contents</td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>01.</td>\n<td><p>Cover letter introducing the licensing submission and identifying regulatory contact </p>\n<p>Justification for use of VB in Canada</p></td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>02.</td>\n<td>Veterinary Biologics information - Form CFIA/ACIA\u00a01503</td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>03.</td>\n<td>Application for permit to import Veterinary Biologics into Canada - Form CFIA/ACIA\u00a01493</td>\n<td>N/A</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>04.</td>\n<td><p>Copy of US Veterinary Biologics Establishment License, Manufacturing Authorization, or equivalent from country of origin, if not previously filed with the CCVB</p>\n<p>Copy of US Veterinary Biologics Product License, Authorization or permission to manufacture diagnostic kit in country of origin</p></td>\n<td>N/A</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>05.</td>\n<td><p>Outline of Production (OP) for the VB</p>\n<p>(If applicable) Referenced OP, Special Outlines (SO) and SOPs, if a current version is not on file with the CCVB.</p>\n<p>(If applicable) Validation data referenced in\u00a0OP\u00a0including data for reference standards and antigen inactivation</p></td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>06.</td>\n<td><p>Bilingual draft or final labels </p>\n<p>(If applicable) Photocopies of approved labels in the country-of-manufacture</p></td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>07.</td>\n<td><p>Declaration of Compliance regarding\u00a0TSE\u00a0</p>\n<p>Material of Animal Origin (MAO) Special Outline</p></td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>08.</td>\n<td><p>Antigen and/or antibody </p>\n<p>Master seed(s): purity and identity; genetic characterization data if derived from biotechnology</p>\n<p>Cell line(s) for viral antigen(s)</p>\n<p>Hybridomas for monoclonal antibodies</p></td>\n<td>If applicable</td>\n<td>If applicable</td>\n<td></td>\n</tr>\n<tr>\n<td>09.</td>\n<td><p>Efficacy and kit performance: preliminary data and proof of concept; protocols and final reports on reproducibility (potency), repeatability, field performance, estimates of specificity and sensitivity</p>\n<p>Copies of correspondence with regulatory authorities of country of origin</p></td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>10.</td>\n<td>Non-cross-reactivity: Data and related correspondence with regulatory authorities of country of origin</td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>11.</td>\n<td>Safety: laboratory biosafety studies, handling and disposal</td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>12.</td>\n<td>Stability: data to support expiry date, and related correspondence with regulatory authorities of country of origin</td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>13.</td>\n<td>Environmental Assessment</td>\n<td>If applicable</td>\n<td>If applicable</td>\n<td></td>\n</tr>\n<tr>\n<td>14.</td>\n<td><p>Test results on 3 pre-licensing serials</p>\n<p>(If applicable) Template of Manufacturer's Serial Release Test Report to CCVB</p></td>\n<td>Required</td>\n<td>Required</td>\n<td></td>\n</tr>\n<tr>\n<td>15.</td>\n<td>Other supporting documentation (identify) </td>\n<td>If applicable</td>\n<td>If applicable</td>\n<td></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=32&amp;ga=1#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><div class=\"table-responsive\">\n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th>No.</th>\n<th>Documents requis </th>\n<th>Canada (CAN)</th>\n<th>\u00c9tats-Unis <br>\n</th>\n<th>Nom du fichier ou du document, version et date (aaaammjj), le cas \u00e9ch\u00e9ant</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>00.</td>\n<td>Index du contenu de la soumission</td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>01.</td>\n<td><p>Lettre d'introduction de la demande d'homologation (d'enregistrement) et du responsable des affaires r\u00e9glementaires</p>\n<p>Justification de l'utilisation du produit biologique v\u00e9t\u00e9rinaire au Canada</p></td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>02.</td>\n<td>Donn\u00e9es sur les produits biologiques v\u00e9t\u00e9rinaires - formulaire CFIA/ACIA\u00a01503 </td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>03.</td>\n<td>Demande de permis pour l'importation de produits biologiques v\u00e9t\u00e9rinaires au Canada - formulaire CFIA/ACIA\u00a01493 </td>\n<td>S/O</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>04.</td>\n<td><p>Copie du \u00ab <span lang=\"en\">United States Veterinary Biologics Establishment License</span> \u00bb, autorisation de fabrication, ou \u00e9quivalent d\u00e9livr\u00e9 par les autorit\u00e9s r\u00e9glementaires du pays d'origine, si une copie n'est pas d\u00e9j\u00e0 soumise au CCPBV</p>\n<p>Copie du \u00ab <span lang=\"en\">United States Veterinary Biologics Product License</span> \u00bb, de l'autorisation ou permission de fabrication de trousse diagnostique du pays d'origine</p></td>\n<td>S/O</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>05.</td>\n<td><p>Protocole de production (PP) du produit biologique v\u00e9t\u00e9rinaire</p>\n<p>(Le cas \u00e9ch\u00e9ant) PP, protocoles sp\u00e9ciaux (PS) et autres proc\u00e9dures standardis\u00e9es cit\u00e9es, si une version \u00e0 jour n'est pas d\u00e9j\u00e0 au dossier du CCPBV</p>\n<p>(Le cas \u00e9ch\u00e9ant) Donn\u00e9es de validation cit\u00e9es dans le PP incluant les donn\u00e9es pour les r\u00e9actifs-\u00e9talons de r\u00e9f\u00e9rence et l'inactivation de l'antig\u00e8ne </p></td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>06.</td>\n<td><p>\u00c9bauches ou \u00e9tiquettes finales bilingues </p>\n<p>(Le cas \u00e9ch\u00e9ant) Copies des \u00e9tiquettes approuv\u00e9es par le pays d'origine</p></td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>07.</td>\n<td><p>D\u00e9claration de conformit\u00e9 concernant les enc\u00e9phalopathies spongiformes transmissibles (EST)</p>\n<p>Substances d'origine animale (SOA) - PS</p></td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>08.</td>\n<td><p>Antig\u00e8nes et/ou anticorps</p>\n<p>Souche(s) m\u00e8re(s) : puret\u00e9 et identit\u00e9; donn\u00e9es de caract\u00e9risation g\u00e9n\u00e9tique si produit d\u00e9riv\u00e9 de la biotechnologie</p>\n<p>Lign\u00e9e(s) cellulaire(s) pour antig\u00e8nes viraux</p>\n<p>Hybridomes cellulaires pour antig\u00e8nes viraux</p></td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td></td>\n</tr>\n<tr>\n<td>09.</td>\n<td><p>Efficacit\u00e9 et performance de la trousse diagnostique: donn\u00e9es pr\u00e9liminaires &amp; preuve du concept; protocole(s) et rapports finaux sur la reproductibilit\u00e9 (puissance), r\u00e9p\u00e9tabilit\u00e9, performance sur le terrain, estimation de la sp\u00e9cificit\u00e9 et de la sensibilit\u00e9</p>\n<p>Copies de la correspondance pertinente avec les autorit\u00e9s r\u00e9glementaires au pays d'origine</p></td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>10.</td>\n<td>Donn\u00e9es sur l'absence de r\u00e9action crois\u00e9es et copies de la correspondance pertinente avec les autorit\u00e9s r\u00e9glementaires au pays d'origine</td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>11.</td>\n<td>Innocuit\u00e9: \u00e9tudes de bios\u00e9curit\u00e9 en laboratoire, manipulation et \u00e9limination</td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>12.</td>\n<td>Stabilit\u00e9\u00a0: donn\u00e9es sur la stabilit\u00e9 \u00e0 l'appui de la date de p\u00e9remption et correspondance pertinente avec les autorit\u00e9s r\u00e9glementaires au pays d'origine</td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>13.</td>\n<td>\u00c9valuation environnementale </td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td></td>\n</tr>\n<tr>\n<td>14.</td>\n<td><p>R\u00e9sultats d'\u00e9preuves sur 3 s\u00e9ries avant homologation</p>\n<p>(Le cas \u00e9ch\u00e9ant) Mod\u00e8le du Rapport du fabricant pour la mise en circulation de s\u00e9rie au CCPBV</p></td>\n<td>Exig\u00e9</td>\n<td>Exig\u00e9</td>\n<td></td>\n</tr>\n<tr>\n<td>15.</td>\n<td>Autre documentation \u00e0 l'appui (identifier) </td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td>Le cas \u00e9ch\u00e9ant</td>\n<td></td>\n</tr>\n</tbody>\n</table>\n</div>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}