{
    "dcr_id": "1306601411551",
    "title": {
        "en": "Invasive plants in Canada",
        "fr": "Plantes envahissantes"
    },
    "html_modified": "2015-03-27 13:27",
    "modified": "29-6-2021",
    "issued": "27-3-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/plan_invasive_plants_1306601411551_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/plan_invasive_plants_1306601411551_fra"
    },
    "parent_ia_id": "1299168989280",
    "ia_id": "1306601522570",
    "parent_node_id": "1299168989280",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Invasive plants in Canada",
        "fr": "Plantes envahissantes"
    },
    "label": {
        "en": "Invasive plants in Canada",
        "fr": "Plantes envahissantes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1306601522570",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299168913252",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Invasive plants in Canada",
            "fr": "Plantes envahissantes"
        },
        "description": {
            "en": "Invasive plants are plant species that can be harmful when introduced into new areas.",
            "fr": "Les plantes envahissantes sont des esp\u00e8ces v\u00e9g\u00e9tales qui peuvent \u00eatre nuisibles lorsqu'elles sont introduites dans de nouveaux milieux."
        },
        "keywords": {
            "en": "invasive plants, plant species, harmful, damage, economy, environment",
            "fr": "plantes envahissantes, esp\u00e8ces v\u00e9g\u00e9tales, nuisibles, r\u00e9percussions n\u00e9fastes, \u00e9conomie, environnement"
        },
        "dcterms.subject": {
            "en": "imports,inspection,plants,weeds",
            "fr": "importation,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-27 13:27:12",
            "fr": "2015-03-27 13:27:12"
        },
        "modified": {
            "en": "2021-06-29",
            "fr": "2021-06-29"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Invasive plants in Canada",
        "fr": "Plantes envahissantes"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Invasive plants are plant species that can be harmful when introduced into new areas. These species can invade agricultural and natural areas, causing serious damage to Canada's economy and environment.</p>\n\n<p>Invasive plants in crops and pastures cost an estimated $2.2 billion each year by reducing crop yields and quality, and increasing costs of weed control and harvesting.</p>\n\n<h2>What the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> is doing about invasive plants</h2>\n\n<p>As Canada's national plant protection organization, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>:</p>\n\n<ul>\n<li>regulates the import, sale and movement of plants into, and within, Canada;</li>\n<li>monitors imports to prevent entry of invasive plants; and</li>\n<li>conducts <a href=\"/plant-health/invasive-species/plant-pest-surveillance/eng/1344466499681/1344466638872\">surveillance</a> to determine if an invasive plant is here, or to confirm that an area is free of a specific invasive plant.</li>\n</ul>\n\n<p>The invasive plants regulated under the Plant Protection Act are included in the list of <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">Pests Regulated by Canada</a>. The invasive plants regulated under the Seeds Act are listed in the <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\">Weed Seeds Order, 2016</a>.</p>\n\n<h2>What information is available?</h2>\n\n<ul>\n<li><a href=\"/plant-health/invasive-species/directives/date/d-12-01/eng/1380720513797/1380721302921\">D-12-01: Phytosanitary requirements to prevent the introduction of plants regulated as pests in Canada</a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/invasive-plants/eng/1331614724083/1331614823132\">Fact sheets</a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/policy/eng/1328298038970/1328298211382\">Invasive Plants Policy</a></li>\n<li><a href=\"http://publications.gc.ca/site/eng/9.839275/publication.html\">Invasive Plants Field Guide</a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/notice-of-decision-2018-01-05/eng/1515019724560/1515019725086\">Notice of decision:  <span lang=\"la\">Arundo donax</span> (giant reed) a regulated pest plant for Canada</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-pest-surveillance/eng/1344466499681/1344466638872\">Plant Pest Surveillance</a></li>\n<li><a href=\"/plant-health/invasive-species/insects/plant-pest-cards/eng/1548085757491/1548085933224\">Plant pest cards</a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/questions-and-answers/eng/1329760438133/1329760617115\">Questions and Answers</a></li>\n<li>Reports\n<ul>\n<li><a href=\"http://epe.lac-bac.gc.ca/100/206/301/cfia-acia/2011-09-21/www.inspection.gc.ca/english/plaveg/invenv/techrpt/summrese.shtml\">Invasive Alien Plants in Canada - Summary Report (Library and Archives Canada)</a></li>\n<li><a href=\"http://epe.lac-bac.gc.ca/100/206/301/cfia-acia/2011-09-21/www.inspection.gc.ca/english/plaveg/invenv/techrpt/techrese.shtml\">Invasive Alien Plants in Canada - Technical Report (Library and Archives Canada)</a></li>\n</ul>\n</li>\n<li><a href=\"/plant-health/seeds/disposing-of-unsolicited-seeds/eng/1624489903715/1624490577542\">Reporting and disposing of unsolicited seeds</a></li>\n<li><a href=\"/plant-health/invasive-species/invasive-plants/weed-risk-analysis-documents/eng/1427387489015/1427397156216\">Weed Risk Analysis Documents</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les plantes envahissantes sont des esp\u00e8ces v\u00e9g\u00e9tales qui peuvent \u00eatre nuisibles lorsqu'elles sont introduites dans de nouveaux milieux. Ces esp\u00e8ces peuvent envahir les zones agricoles et naturelles, entra\u00eenant ainsi des r\u00e9percussions n\u00e9fastes sur l'\u00e9conomie et l'environnement du Canada.</p>\n\n<p>La pr\u00e9sence de plantes envahissantes dans les cultures et les p\u00e2turages co\u00fbte au Canada environ 2,2\u00a0milliards de dollars par ann\u00e9e, en raison des pertes de productivit\u00e9 et de qualit\u00e9, ainsi que des frais croissants li\u00e9s \u00e0 la lutte contre les mauvaises herbes et \u00e0 la r\u00e9colte.</p>\n\n<h2>Ce que fait l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pour lutter contre les plantes envahissantes</h2> \n\n<p>En tant qu'organisme national de la protection des v\u00e9g\u00e9taux du Canada, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>\u00a0:</p>\n\n<ul>\n<li>r\u00e9glemente l'importation, la vente et les d\u00e9placements de v\u00e9g\u00e9taux vers le Canada et \u00e0 l'int\u00e9rieur du pays;</li>\n<li>surveille les importations afin d'emp\u00eacher l'introduction de plantes envahissantes;</li>\n<li>assure une <a href=\"/protection-des-vegetaux/especes-envahissantes/surveillance-phytosanitaire/fra/1344466499681/1344466638872\">surveillance</a> afin de d\u00e9terminer si une plante envahissante s'est \u00e9tablie au pays, ou confirmer qu'une r\u00e9gion est exempte d'une plante envahissante particuli\u00e8re.</li>\n</ul>\n\n<p>Les plantes envahissantes r\u00e9glement\u00e9es en vertu de la Loi sur la protection des v\u00e9g\u00e9taux figurent dans la liste <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">Parasites r\u00e9glement\u00e9s par le Canada</a>. Les plantes envahissantes r\u00e9glement\u00e9es en vertu de la Loi sur les semences figurent dans l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\">Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</a>.</p>\n\n<h2>Quels types de renseignements sont mis \u00e0 ma disposition?</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/avis-de-decision-2018-01-05/fra/1515019724560/1515019725086\">Avis de d\u00e9cision\u00a0: <span lang=\"la\">Arundo donax</span> (canne de Provence) un phytoravageur r\u00e9glement\u00e9 pour le Canada</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/insectes/cartes-de-phytoravageurs/fra/1548085757491/1548085933224\">Cartes de phytoravageurs</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/date/d-12-01/fra/1380720513797/1380721302921\">D-12-01\u00a0: Exigences phytosanitaires visant \u00e0 pr\u00e9venir l'introduction de v\u00e9g\u00e9taux r\u00e9glement\u00e9s comme \u00e9tant des organismes nuisibles au Canada</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/documents-d-evaluation-des-risques-associes-aux-ma/fra/1427387489015/1427397156216\">Documents d'\u00e9valuation des risques associ\u00e9s aux mauvaises herbes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/fra/1331614724083/1331614823132\">Fiches de renseignements</a></li>\n<li><a href=\"http://publications.gc.ca/site/fra/9.839276/publication.html\" title=\"Invasive plants field guide\">Guide des plantes envahissantes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/politique/fra/1328298038970/1328298211382\">Politique sur les plantes envahissantes</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/questions-et-reponses/fra/1329760438133/1329760617115\">Questions et r\u00e9ponses</a></li>\n<li>Rapports\n<ul>\n<li><a href=\"http://epe.lac-bac.gc.ca/100/206/301/cfia-acia/2011-09-21/www.inspection.gc.ca/francais/plaveg/invenv/techrpt/summresf.shtml\">Les esp\u00e8ces exotiques envahissantes au Canada - Rapport sommaire (Biblioth\u00e8que et Archives Canada)</a></li>\n<li><a href=\"http://epe.lac-bac.gc.ca/100/206/301/cfia-acia/2011-09-21/www.inspection.gc.ca/francais/plaveg/invenv/techrpt/techresf.shtml\">Les esp\u00e8ces exotiques envahissantes au Canada - Rapport technique (Biblioth\u00e8que et Archives Canada)</a></li>\n</ul>\n</li>\n<li><a href=\"/protection-des-vegetaux/semences/elimination-de-semences-non-sollicitees/fra/1624489903715/1624490577542\">Signalement et \u00e9limination de semences non sollicit\u00e9es</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/surveillance-phytosanitaire/fra/1344466499681/1344466638872\">Surveillance phytosanitaire</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}