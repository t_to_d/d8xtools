{
    "dcr_id": "1657719012688",
    "title": {
        "en": "Research with livestock feeds: Before you apply",
        "fr": "Recherche sur les aliments pour animaux de ferme: Avant de pr\u00e9senter une demande"
    },
    "html_modified": "19-7-2022",
    "modified": "18-7-2022",
    "issued": "13-7-2022",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/2_before_apply_research_livestock_feed_1657719012688_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/2_before_apply_research_livestock_feed_1657719012688_fra"
    },
    "parent_ia_id": "1627997833424",
    "ia_id": "1657719013016",
    "parent_node_id": "1627997833424",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Research with livestock feeds: Before you apply",
        "fr": "Recherche sur les aliments pour animaux de ferme: Avant de pr\u00e9senter une demande"
    },
    "label": {
        "en": "Research with livestock feeds: Before you apply",
        "fr": "Recherche sur les aliments pour animaux de ferme: Avant de pr\u00e9senter une demande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1657719013016",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1627997831971",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/approval-and-registration/livestock-feed/",
        "fr": "/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Research with livestock feeds: Before you apply",
            "fr": "Recherche sur les aliments pour animaux de ferme: Avant de pr\u00e9senter une demande"
        },
        "description": {
            "en": "Determine if you need a research authorization or exemption\nBefore you apply for a research authorization or exemption, you must:",
            "fr": "D\u00e9terminer si vous avez besoin d'une autorisation ou d'une exemption de recherche\nAvant de faire une demande pour une autorisation de recherche ou une exemption, vous devez :"
        },
        "keywords": {
            "en": "Animals, Animal Health, livestock, feed, before you apply, application, regulation, service, Research",
            "fr": "aliments, animaux de ferme, Avant de pr\u00e9senter une demande, informations, entreprise, service, recherche"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-07-19",
            "fr": "2022-07-19"
        },
        "modified": {
            "en": "2022-07-18",
            "fr": "2022-07-18"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Research with livestock feeds: Before you apply",
        "fr": "Recherche sur les aliments pour animaux de ferme: Avant de pr\u00e9senter une demande"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657718557807/1657718558166\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\" href=\"\">2. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657719584148/1657719584523\">3. How to apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657720034627/1657720034978\">4. After you apply</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657720392834/1657720393201\">5. Amending</a></li>\n</ul>\n</div>\n</div>\n\t\n\n\t\n<h2>Determine if you need a research authorization or exemption</h2>\n\t\n<div class=\"col-md-8\">\n\t\n<p>Before you apply for a research authorization or exemption, you must:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>determine if your research feed meets any of the exemption criteria outlined in section\u00a03\u00a0(c) of the <a href=\"https://laws-lois.justice.gc.ca/eng/regulations/SOR-83-593/page-2.html\"><i>Feeds Regulations</i></a>\n<ul>\n<li>if <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=1#s2c1\">an application is required</a>, your submission type will be auto-selected by completing the form <a href=\"/about-cfia/find-a-form/form-cfia-acia-5916/eng/1616793026236/1616793467452\">CFIA/ACIA\u00a05916\u00a0\u2013 Application for research authorization\u00a0/\u00a0exemption under the <i>Feeds Act</i> and <i>Feeds Regulations</i></a></li>\t\n</ul>\n</li>\n<li>if you don't need to submit an application but you still wish to get a permission, you can apply for a research exemption</li>\n</ul>\n\t\n<p>The <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584\">Chapter\u00a05</a> of the regulatory guidance RG-1: Feed Registration Procedures and Labelling Standards\u00a0\u2013 Canadian Food Inspection Agency and the decision tree on page\u00a02 of the application form provide more information on submission types.</p>\n\t\n</div>\n\t\n<div class=\"col-md-4 pull-right mrgn-tp-0 mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Additional information</h2>\n</header>\n<div class=\"panel-body\">\n<p>To help determine your submission type and data requirements, you can review the following definitions found in <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584\">Chapter\u00a05</a> of the regulatory guidance RG-1: Feed Registration Procedures and Labelling Standards:</p>\n<ul>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=0#s1c1\">Feeds other than novel feeds</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=0#s1c1\">Novel Feed</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=0#s1c1\">Research Feed</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=0#s3c1\">Safe disposal</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=0#c4\">Contained</a></li>\n</ul>\n</div>\n</section>\n</div>\n\t\n<div class=\"clearfix\"></div>\n\t\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Additional information</h2>\n</header>\n<div class=\"panel-body\">\n<p>Other regulatory obligations and/or oversight from other programs in the CFIA or other government departments may still apply to your research feed.</p>\n<p>The regulatory guidance <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-10/eng/1619126672753/1619126673487\">RG-10 Importing livestock feeds (mixed feeds and single ingredient feeds) into Canada</a> outlines general information about importing livestock feeds to Canada.</p>\n</div>\n</section>\n\t\n<h2 id=\"a1\">Get your information ready</h2>\n\t\n<p>To submit an application for a research authorization or exemption you'll need:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>a completed form <a href=\"/about-cfia/find-a-form/form-cfia-acia-5916/eng/1616793026236/1616793467452\">CFIA/ACIA\u00a05916\u00a0\u2013 Application for research authorization\u00a0/\u00a0exemption under the <i>Feeds Act</i> and <i>Feeds Regulations</i></a></li>\t\n<li>your <a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-5/eng/1329351291996/1329351387584?chap=2\">required data</a></li>\n<li>the applicable application fees\n<ul>\n<li>you can determine your applicable fees by completing section\u00a010 of the form <a href=\"/about-cfia/find-a-form/form-cfia-acia-5916/eng/1616793026236/1616793467452\">CFIA/ACIA\u00a05916\u00a0\u2013 Application for research authorization\u00a0/\u00a0exemption under the <i>Feeds Act</i> and <i>Feeds Regulations</i></a> and in <a href=\"/eng/1582641645528/1582641871296#c5\">Part\u00a04</a> of the CFIA Fees Notice</li>\n</ul>\n</li>\n</ul>\n\t\n<p><strong>Additional information:</strong> For a trial with multiple research feeds, you must submit a separate application form for each research feed. However, you may submit one research protocol with the application package that clearly identifies each of the research feeds.</p>\n\t\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657718557807/1657718558166\" rel=\"prev\">Previous: Overview</a></li>\n<li class=\"next\"><a href=\"/animal-health/livestock-feeds/approval-and-registration/livestock-feed/eng/1657719584148/1657719584523\" rel=\"next\">Next: How to apply</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657718557807/1657718558166\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\" href=\"\">2. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657719584148/1657719584523\">3. Comment pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657720034627/1657720034978\">4. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657720392834/1657720393201\">5. Modifier une demande</a></li>\n</ul>\n</div>\n</div>\n\t\n\n\t\n<h2>D\u00e9terminer si vous avez besoin d'une autorisation ou d'une exemption de recherche</h2>\n\t\n<div class=\"col-md-8\">\n\t\n<p>Avant de faire une demande pour une autorisation de recherche ou une exemption, vous devez\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>D\u00e9terminer si votre recherche sur l'aliment pour animaux r\u00e9pond \u00e0 l'un des crit\u00e8res d'exemption \u00e9nonc\u00e9s dans la section\u00a03\u00a0(c) du <a href=\"https://laws-lois.justice.gc.ca/fra/reglements/DORS-83-593/page-2.html\"><i>R\u00e8glement sur les aliments du b\u00e9tail</i></a>\n<ul>\n<li>Si <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=1#s2c1\">une demande est n\u00e9cessaire</a>, le type de celle-ci sera automatiquement s\u00e9lectionn\u00e9 en remplissant le formulaire <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5916/fra/1616793026236/1616793467452\">CFIA/ACIA\u00a05916\u00a0\u2013 Demande d'autorisation de recherche\u00a0/\u00a0exemption en vertu de la <i>Loi relative aux aliments du b\u00e9tail</i> et du <i>R\u00e8glement sur les aliments du b\u00e9tail</i></a>;</li>\n<li>Si vous n'avez pas besoin d'une demande, mais que vous souhaitez tout de m\u00eame obtenir une autorisation, vous pouvez demander une exemption de recherche.</li>\n</ul>\n</li>\t\n</ul>\n\t\n<p>Le <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=0\">Chapitre\u00a05</a> des directives r\u00e9glementaires RG-1: Proc\u00e9dures d'enregistrement et normes d'\u00e9tiquetage des aliments pour animaux et l'arbre de d\u00e9cision \u00e0 la page\u00a02 du formulaire de demande fournissent des informations sur les types de demande.</p>\n\t\n</div>\n\t\n<div class=\"col-md-4 pull-right mrgn-tp-0 mrgn-rght-0\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Informations suppl\u00e9mentaires</h2>\n</header>\n<div class=\"panel-body\">\n<p>Pour vous aider \u00e0 d\u00e9terminer votre type de demande et les exigences en mati\u00e8re de donn\u00e9es, vous pouvez consulter les d\u00e9finitions suivantes qui se trouvent dans le <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584\">Chapitre\u00a05</a> des directives r\u00e9glementaires RG-1\u00a0: Proc\u00e9dures d'enregistrement et normes d'\u00e9tiquetage des aliments pour animaux\u00a0:</p>\n<ul>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=0#s1c1\">Aliment du b\u00e9tail autre qu'un aliment nouveau</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=0#s1c1\">Aliment nouveau</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=0#s1c1\">Aliments de recherche</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=0#s3c1\">L'\u00e9limination s\u00e9curitaire</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=0#c4\">Isolement</a></li>\n</ul>\n</div>\n</section>\n</div>\n\t\n<div class=\"clearfix\"></div>\n\t\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Information suppl\u00e9mentaire</h2>\n</header>\n<div class=\"panel-body\">\n<p>D'autres obligations r\u00e9glementaires et/ou la surveillance de la part des autres programmes de l'ACIA ou d'autres minist\u00e8res gouvernementaux peuvent encore s'appliquer \u00e0 votre aliment de recherche pour animaux.</p>\n<p>La directive r\u00e9glementaire <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-10/fra/1619126672753/1619126673487\">RG-10 Importation d'aliments du b\u00e9tail (aliments m\u00e9lang\u00e9s et aliments \u00e0 ingr\u00e9dient unique) au Canada</a> pr\u00e9sente des informations g\u00e9n\u00e9rales sur l'importation des aliments pour animaux de ferme au Canada.</p>\n</div>\n</section>\n\t\n<h2 id=\"a1\">Pr\u00e9parez vos informations</h2>\n\t\n<p>Pour soumettre une demande d'autorisation ou d'exemption de recherche, vous aurez besoin\u00a0:</p>\n\t\n<ul class=\"lst-spcd\">\n<li>du formulaire compl\u00e9t\u00e9 <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5916/fra/1616793026236/1616793467452\">CFIA/ACIA\u00a05916\u00a0\u2013 Demande d'autorisation de recherche\u00a0/\u00a0exemption en vertu de la <i>Loi relative aux aliments du b\u00e9tail</i> et du <i>R\u00e8glement sur les aliments du b\u00e9tail</i></a>\u00a0;</li>\t\n<li>de vos <a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-5/fra/1329351291996/1329351387584?chap=2\">donn\u00e9es requises</a>;</li>\n<li>des frais de demandes applicables;\n<ul>\n<li>vous pouvez d\u00e9terminer vos frais en remplissant la section\u00a010 du formulaire <a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5916/fra/1616793026236/1616793467452\">CFIA/ACIA\u00a05916\u00a0\u2013 Demande d'autorisation de recherche\u00a0/\u00a0exemption en vertu de la <i>Loi relative aux aliments du b\u00e9tail</i> et du <i>R\u00e8glement sur les aliments du b\u00e9tail</i></a> et \u00e0 la <a href=\"/fra/1582641645528/1582641871296#c5\">Partie\u00a04</a> de l'Avis sur les prix de l'ACIA.</li>\n</ul>\n</li>\n</ul>\n\t\n<p><strong>Information suppl\u00e9mentaire:</strong> Pour un essai de recherche comportant plusieurs aliments pour animaux de ferme, vous devez soumettre un formulaire de demande distinct pour chaque aliment de recherche pour animaux. Toutefois, vous pouvez soumettre un seul protocole de recherche avec le dossier de demande qui identifie clairement chacun des aliments de recherche pour animaux.</p>\n\t\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n<li class=\"previous\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657718557807/1657718558166\" rel=\"prev\">Pr\u00e9c\u00e9dent\u00a0: Aper\u00e7u</a></li>\n<li class=\"next\"><a href=\"/sante-des-animaux/aliments-du-betail/approbation-et-enregistrement/aliments-animaux/fra/1657719584148/1657719584523\" rel=\"next\">Suivant\u00a0: Comment pr\u00e9senter une demande</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}