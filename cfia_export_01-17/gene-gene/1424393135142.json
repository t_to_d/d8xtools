{
    "dcr_id": "1424393135142",
    "title": {
        "en": "Canada remains a controlled <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> risk country",
        "fr": "Le Canada continue d'\u00eatre reconnu comme pays \u00e0 risque ma\u00eetris\u00e9 pour l'<abbr title=\"enc\u00e9phalopathie spongiforme bovine\">ESB</abbr>"
    },
    "html_modified": "19-2-2015",
    "modified": "27-5-2021",
    "issued": "19-2-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_bse_risk_fact_sheet_1424393135142_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_bse_risk_fact_sheet_1424393135142_fra"
    },
    "parent_ia_id": "1323991912972",
    "ia_id": "1424393135877",
    "parent_node_id": "1323991912972",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Canada remains a controlled <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> risk country",
        "fr": "Le Canada continue d'\u00eatre reconnu comme pays \u00e0 risque ma\u00eetris\u00e9 pour l'<abbr title=\"enc\u00e9phalopathie spongiforme bovine\">ESB</abbr>"
    },
    "label": {
        "en": "Canada remains a controlled <abbr title=\"Bovine Spongiform Encephalopathy\">BSE</abbr> risk country",
        "fr": "Le Canada continue d'\u00eatre reconnu comme pays \u00e0 risque ma\u00eetris\u00e9 pour l'<abbr title=\"enc\u00e9phalopathie spongiforme bovine\">ESB</abbr>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1424393135877",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323991831668",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/bovine-spongiform-encephalopathy/bse-risk-country/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/encephalopathie-spongiforme-bovine/pays-a-risque-maitrise-pour-l-esb/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Canada remains a controlled BSE risk country",
            "fr": "Le Canada continue d'\u00eatre reconnu comme pays \u00e0 risque ma\u00eetris\u00e9 pour l'ESB"
        },
        "description": {
            "en": "The World Organisation for Animal Health (WOAH) classifies the bovine spongiform encephalopathy (BSE) risk status of the cattle population of a country on the basis of a risk assessment and other criteria.",
            "fr": "L'Organisation mondiale de la sant\u00e9 animale (OMSA) \u00e9tablit le statut sanitaire du cheptel bovin d'un pays \u00e0 l'\u00e9gard de l'enc\u00e9phalopathie spongiforme bovine (ESB) en se fondant sur l'\u00e9valuation des risques et sur d'autres crit\u00e8res."
        },
        "keywords": {
            "en": "Animals, Animal Health, bovine spongiform encephalopathy, BSE, beef, cow,  World Organisation for Animal Health, WOAH, risk assessment, fact sheet",
            "fr": "Animaux, Sant\u00e9 des animaux, enc\u00e9phalopathie spongiforme bovine, ESB, vache, boucherie, Alberta, Organisation mondiale de la sant\u00e9 animale, OMSA, \u00e9valuation des risques, fiche de renseignements"
        },
        "dcterms.subject": {
            "en": "assessment,inspection,infectious diseases,microorganisms,agri-food products,consumer protection,animal health,meat",
            "fr": "\u00e9valuation,inspection,maladie infectieuse,microorganisme,produit agro-alimentaire,protection du consommateur,sant\u00e9 animale,viande"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-02-19",
            "fr": "2015-02-19"
        },
        "modified": {
            "en": "2021-05-27",
            "fr": "2021-05-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,media",
            "fr": "entreprises,gouvernement,grand public,m\u00e9dia"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Canada remains a controlled BSE risk country",
        "fr": "Le Canada continue d'\u00eatre reconnu comme pays \u00e0 risque ma\u00eetris\u00e9 pour l'ESB"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE)) classifies the bovine spongiform encephalopathy (BSE) risk status of the cattle population of a country on the basis of a risk assessment and other criteria. The cattle population of a country can be classified into three categories:</p>\n<ul>\n<li>negligible BSE risk</li>\n<li>controlled BSE risk</li>\n<li>or undetermined BSE risk</li>\n</ul>\n<p>In\u00a02007, Canada was recognized by the WOAH as a controlled BSE risk country. Canada was unable to obtain a negligible BSE risk status at that time mainly because of occurrence of cases of classical BSE in the domestic cattle population born less than\u00a011\u00a0years ago.</p>\n<p>To date, all domestic cases of classical BSE have been born more than\u00a011\u00a0years ago (the youngest domestic case of classical BSE was born on\u00a025\u00a0March\u00a02009<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>*</a></sup>), and Canada can now be recognized with a negligible BSE risk status.</p>\n<h2>The requirements for negligible BSE risk status</h2>\n<p>According to the provisions of the WOAH, a country meets the requirements for a negligible BSE risk status if it complies with the following:</p>\n<ul>\n<li>a risk assessment identifies all potential risk factors for BSE to occur, and measures are in place to properly control and reduce those factors,</li>\n<li>a prohibition to feed ruminants with ruminants is appropriately implemented and monitored,</li>\n<li>BSE awareness, education and reporting programs have been continuously implemented,</li>\n<li>there is diagnostic competency within the laboratory system,</li>\n<li>BSE surveillance has been conducted in accordance with the WOAH guidelines,</li>\n<li>any cases of classical BSE were born\u00a011\u00a0years ago or more, and all BSE cases have been properly dealt with.</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl><dt>Footnote *</dt>\n<dd id=\"fn1\">\n<p>Canadian born cattle have been found through the Canadian surveillance system.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>*<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd></dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)) \u00e9tablit le statut sanitaire du cheptel bovin d'un pays \u00e0 l'\u00e9gard de l'enc\u00e9phalopathie spongiforme bovine (ESB) en se fondant sur l'\u00e9valuation des risques et sur d'autres crit\u00e8res. Le cheptel bovin d'un pays peut \u00eatre class\u00e9 dans trois cat\u00e9gories\u00a0:</p>\n<ul>\n<li>risque n\u00e9gligeable \u00e0 l'\u00e9gard de l'ESB;</li>\n<li>risque ma\u00eetris\u00e9 \u00e0 l'\u00e9gard de l'ESB;</li>\n<li>risque ind\u00e9termin\u00e9 \u00e0 l'\u00e9gard de l'ESB.</li>\n</ul>\n<p>En\u00a02007, le Canada a \u00e9t\u00e9 reconnu par l'OMSA comme un pays pr\u00e9sentant un risque ma\u00eetris\u00e9 \u00e0 l'\u00e9gard de l'ESB. \u00c0 ce moment-l\u00e0 le Canada ne pouvait pas obtenir un statut de risque n\u00e9gligeable \u00e0 l'\u00e9gard de cette maladie, principalement en raison de l'apparition de cas d'ESB classique dans la population bovine domestique n\u00e9e dans les\u00a011\u00a0derni\u00e8res ann\u00e9es.</p>\n<p>\u00c0 ce jour, tous les cas d'ESB classique au sein du cheptel domestique sont n\u00e9s il y a plus de\u00a011\u00a0ans (le plus jeune cas d'ESB classique au niveau domestique est n\u00e9 le\u00a025\u00a0mars\u00a02009<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>*</a></sup>), et le Canada peut maintenant \u00eatre reconnu comme un pays avec un statut de risque n\u00e9gligeable \u00e0 l'\u00e9gard de l'ESB.</p>\n<h2>Les exigences relatives au statut de risque d'ESB n\u00e9gligeable</h2>\n<p>Selon les dispositions de l'OMSA, un pays satisfait aux exigences d'un statut de risque d'ESB n\u00e9gligeable s'il remplit les conditions suivantes\u00a0:</p>\n<ul>\n<li>une \u00e9valuation des risques qui identifie tous les facteurs de risque potentiels d'ESB et des mesures mises en place pour contr\u00f4ler et r\u00e9duire correctement ces facteurs,</li>\n<li>une interdiction de nourrir les ruminants avec des ruminants est mise en \u0153uvre et surveill\u00e9e de mani\u00e8re appropri\u00e9e,</li>\n<li>des programmes de sensibilisation, d'\u00e9ducation et de d\u00e9claration de l'ESB ont \u00e9t\u00e9 mis en \u0153uvre de fa\u00e7on permanente,</li>\n<li>des capacit\u00e9s de diagnostic existent au sein du syst\u00e8me de laboratoire,</li>\n<li>la surveillance de l'ESB a \u00e9t\u00e9 men\u00e9e conform\u00e9ment aux lignes directrices de l'OMSA,</li>\n<li>tous les cas d'ESB classique sont n\u00e9s il y a\u00a011\u00a0ans ou plus, et tous les cas d'ESB ont \u00e9t\u00e9 correctement g\u00e9r\u00e9.</li>\n</ul>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page *</dt>\n<dd id=\"fn1\">\n<p>Bovins n\u00e9s au canada qui ont \u00e9t\u00e9 trouv\u00e9s \u00e0 travers le syst\u00e8me de surveillance canadien.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>*</a></p> \n</dd></dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}