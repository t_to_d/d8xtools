{
    "dcr_id": "1528316168759",
    "title": {
        "en": "Exporting food: understand the regulations (video)",
        "fr": "Exporter des aliments : comprendre la r\u00e9glementation (vid\u00e9o)"
    },
    "html_modified": "13-6-2018",
    "modified": "9-8-2019",
    "issued": "6-6-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/sfcr_exporting_food_video_1528316168759_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/sfcr_exporting_food_video_1528316168759_fra"
    },
    "parent_ia_id": "1528314376740",
    "ia_id": "1528316169118",
    "parent_node_id": "1528314376740",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Exporting food: understand the regulations (video)",
        "fr": "Exporter des aliments : comprendre la r\u00e9glementation (vid\u00e9o)"
    },
    "label": {
        "en": "Exporting food: understand the regulations (video)",
        "fr": "Exporter des aliments : comprendre la r\u00e9glementation (vid\u00e9o)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1528316169118",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1528314348383",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-industry/video/exporting-food-video-/",
        "fr": "/salubrite-alimentaire-pour-l-industrie/video/exporter-des-aliments-video-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Exporting food: understand the regulations (video)",
            "fr": "Exporter des aliments : comprendre la r\u00e9glementation (vid\u00e9o)"
        },
        "description": {
            "en": "Exporting food: understand the regulations (video)",
            "fr": "Exporter des aliments : comprendre la r\u00e9glementation (vid\u00e9o)"
        },
        "keywords": {
            "en": "Exporting Food, Safe Food, SFCR, Food Safety, Food Regulations, Safe Food for Canadians Regulations",
            "fr": "Exportation d'aliments, Aliments salubres, RSAC, Salubrit\u00e9 des aliments, R\u00e9glementation sur les aliments, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
        },
        "dcterms.subject": {
            "en": "food,food labeling,inspection,food inspection,legislation,regulation,food safety,food processing",
            "fr": "aliment,\u00e9tiquetage des aliments,inspection,inspection des aliments,l\u00e9gislation,r\u00e9glementation,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "modified": {
            "en": "2019-08-09",
            "fr": "2019-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Exporting food: understand the regulations (video)",
        "fr": "Exporter des aliments : comprendre la r\u00e9glementation (vid\u00e9o)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1526992762174\"></div>\n\n\n<p>The <i>Safe Food for Canadians Regulations</i> came into force on <span class=\"nowrap\">January 15, 2019</span>. The regulations are based on international standards and help ensure that Canada's food businesses continue to have access to the U.S. and other key markets that have implemented similar preventive regulatory frameworks. Learn more on how the new regulations apply to exports.</p>\n\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://www.youtube.com/watch?v=fllCOKWwTEo&amp;feature=youtu.be\"}'>\n\n<video title=\"Exporting Food from Canada\">\n\n<source type=\"video/youtube\" src=\"https://www.youtube.com/watch?v=fllCOKWwTEo&amp;feature=youtu.be\">\n\n</source></video>\n\n<figcaption>\n\n<details id=\"inline-transcript\">\n\n<summary>Exporting Food from Canada\u00a0\u2013 Transcript</summary>\n\n<p>[Text on screen: CFIA Safeguarding with Science]</p>\n\n<p> [Graphic of the earth.]</p>\n\n<p>Canada is a global leader in food production and a top exporter of food to the world.</p>\n\n<p>[Meat, grains, chocolate, berries, and nuts appear within the earth. International monuments appear and circle the earth.]</p>\n\n<p>From meat and grains to chocolate, fruit and nuts, Canadian food is consumed by people around the globe every day.</p>\n\n<p>[Graphic of Parliament Building appears with many copies of <i>Safe Food for Canadians Regulations</i> around it.]</p>\n\n<p>[Text on screen: <i>Safe Food for Canadians Regulations</i>]</p>\n\n<p>In\u00a02019, Canada began a new era of food safety regulation with the introduction of the <i>Safe Food for Canadians Regulations</i>.</p>\n\n<p>[Text on screen: Licensing, traceability, preventive controls]</p>\n\n<p>These regulations are built on three key elements: licensing, traceability and preventive controls.</p>\n\n<p>[Graphic of business owner on laptop with warehouse in the background.]</p>\n\n<p>As an exporter, here are some of the things you should consider for each element.</p>\n\n<p>[A computer on a desk. An envelope appears on the computer screen saying [SFC Licence LIC #123AB].]</p>\n\n<p>[Text on screen: Licensing]</p>\n\n<p>The Canadian Food Inspection Agency, or CFIA, issues licences to food exporters.</p>\n\n<p>[Computer screen switches to a production lab where food is being packaged.]</p>\n\n<p>A Safe Food for Canadians licence is required to conduct any activities related to preparing food for export from Canada or,</p>\n\n<p>[Screen changes to search engine. [Which countries require export certificates?] is typed into the search bar. [CFIA export eligibility] is typed into the search bar. Search results appear and the page scrolls down.]</p>\n\n<p>to receive an export certificate or other trade permission \u2013 such as being placed on an export eligibility list.</p>\n\n<p>[Three physical establishments appear on screen.]</p>\n\n<p>[Text on screen: SFC LIC #123AB | SFC LIC #123CD | SFC LIC #123EF]</p>\n\n<p>[Each separate licence is moved into one records folder.]</p>\n\n<p>In keeping with current best practices, CFIA recommends that food exporters apply for one SFC licence per physical establishment for all activities conducted at that address. This will give you one licence number that will ensure consistency across all your export records.</p>\n\n<p>[Text on screen: Traceability]</p>\n\n<p>[Computer screen changes to various clips of food production]</p>\n\n<p>Traceability requires that you document where you grew or produced food, or who you bought it from, along with who you are selling it to.</p>\n\n<p>[Computer screen changes to email. [Foreign counterparts] is typed in the recipient box. [Recall] is typed into subject.]</p>\n\n<p>This can reduce the time it takes to notify foreign counterparts in the event that a product needs to be recalled.</p>\n\n<p>[Text on screen: Preventive controls]</p>\n\n<p>[Graphic of port is shown with planes, trucks, and cargo ships transporting food products.]</p>\n\n<p>To be eligible to export food under your licence, you need to have preventive controls in place to make sure your food is safe before it leaves Canada. </p>\n\n<p>[Text on screen: 3]</p>\n\n<p>[Three graphics appear on screen. A chef, a business woman, and a preventive control plan.]</p>\n\n<p>Three guiding principles are at the heart of preventive controls: one, know your food; two, know your foreign market; and three, have a written plan.</p>\n\n<p>[Graphic of chef cooking.]</p>\n\n<p>[Text on screen: Know your food]</p>\n\n<p>[Text on screen (appears as checklist): Safety|Grading|Standards|Labelling|Net quantity]</p>\n\n<p>[A checkmark appears beside each category.]</p>\n\n<p>As an exporter, you need to know your food, so it meets requirements for safety, grading, standards, labelling and net quantity.</p>\n\n<p>[A hand is mashing food. A checklist appears and checks items off the list.]</p>\n\n<p>Verify that your food product matches information provided on the export certificate and other shipping documents.</p>\n\n<p>[Business woman on her computer emailing her importer.]</p>\n\n<p>[Text on screen: Know your foreign market]</p>\n\n<p>[Text on screen (appears as checklist): Check market eligibility|Work with importer|Work with TCS|Obtain certification from CFIA if required by importing country]</p>\n\n<p>[A checkmark appears beside each category.]</p>\n\n<p>You are also responsible for knowing your foreign market. Check that you are eligible for specific markets and work with your importer or the Canadian Trade Commissioner Service to ensure your food meets the importing country\u2019s requirements. Be sure to obtain required inspection or certification services from CFIA before your food leaves Canada.</p>\n\n<p>[Tablet displaying a Preventive Control Plan]</p>\n\n<p>[Text on screen: Have a written plan]</p>\n\n<p>[Text on screen (appears as checklist): Understand risks|Set out preventive controls|Keeps food safe]</p>\n\n<p>[A checkmark appears beside each category.]</p>\n\n<p>Finally, make sure you have a written preventive control plan. A written plan demonstrates you understand the risks associated with your food, and sets out the preventive controls you are taking to keep your food safe.</p>\n\n<p>[Text on screen: <i>Safe Food for Canadians Regulations</i>]</p>\n\n<p>[Graphic of globe. Different icons such as pig, nuts, apples, fish and other food products appear in different locations across the globe.]</p>\n\n<p>The <i>Safe Food for Canadians Regulations</i> position Canadian exporters to meet growing foreign demand for their foods.</p>\n\n<p>[Businesswoman standing proudly in front of her warehouse.]</p>\n\n<p>[Text on screen: Food safety]</p>\n\n<p>Remember that safe food is your responsibility and good business.</p>\n\n<p>[Text on screen: inspection.gc.ca/MyCFIA]</p>\n\n<p>[My\u00a0CFIA Logo changes to graphic of licences, export certificate, and smartphone.]</p>\n\n<p>Sign up for My\u00a0CFIA today to access a growing number of online services including, licences, export certificates, as well as electronic payment options.</p>\n\n<p>[Text on screen: inspection.gc.ca/SafeFood]</p>\n\n<p>Visit the <i>Safe Food for Canadians Regulations</i> webpage to learn more.</p>\n\n<p>[Canada Logo]</p>\n\n<p>[Text on screen: \u00a9 Her Majesty the Queen in Right of Canada (Canadian Food Inspection Agency), 2018.]</p>\n\n</details>\n</figcaption>\n\n</figure>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1526992762174\"></div>\n\n\n<p>Le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> est entr\u00e9 en vigueur <span class=\"nowrap\">le 15 janvier 2019</span>. Le R\u00e8glement est fond\u00e9 sur les normes internationales et contribue \u00e0 veiller \u00e0 ce que les entreprises alimentaires canadiennes continuent d'avoir acc\u00e8s au march\u00e9 am\u00e9ricain ainsi qu'aux autres march\u00e9s cl\u00e9s qui ont mis en \u0153uvre des cadres r\u00e9glementaires pr\u00e9ventifs similaires. Renseignez-vous sur la nouvelle r\u00e9glementation visant les exportations.</p>\n\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://www.youtube.com/watch?v=fuLdHPIuEf0&amp;feature=youtu.be\"}'>\n\n<video title=\"Exportation d'aliments depuis le Canada\">\n\n<source type=\"video/youtube\" src=\"https://www.youtube.com/watch?v=fuLdHPIuEf0&amp;feature=youtu.be\">\n\n</source></video>\n\n<figcaption>\n\n<details id=\"inline-transcript\">\n\n<summary>Exportation d'aliments depuis le Canada\u00a0\u2013 Transcription</summary>\n\n<p>[Texte sur l'\u00e9cran\u00a0: ACIA Pr\u00e9server gr\u00e2ce \u00e0 la science]</p>\n\n<p>[Image de la Terre.]</p>\n\n<p>Le Canada est un chef de file mondial de la production alimentaire et l'un des principaux exportateurs d'aliments dans le monde.</p>\n\n<p>[De la viande, des grains, du chocolat, des baies et des noix figurent \u00e0 l'int\u00e9rieur de la Terre. Des monuments internationaux encerclent la Terre.]</p>\n\n<p>Qu'il s'agisse de viande, de c\u00e9r\u00e9ales, de chocolat, de fruits ou de noix, les aliments canadiens sont consomm\u00e9s chaque jour, partout dans le monde.</p>\n\n<p>[Des monuments internationaux encerclent la Terre.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>.]</p>\n\n<p>En\u00a02019, le Canada a amorc\u00e9 une nouvelle \u00e8re de r\u00e9glementation de la salubrit\u00e9 des aliments par la mise en \u0153uvre du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>.</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: la d\u00e9livrance de licences, la tra\u00e7abilit\u00e9 et les contr\u00f4les pr\u00e9ventifs.]</p>\n\n<p>Ce r\u00e8glement repose sur trois principaux \u00e9l\u00e9ments\u00a0: la d\u00e9livrance de licences, la tra\u00e7abilit\u00e9 et les contr\u00f4les pr\u00e9ventifs.</p>\n\n<p>[L'\u00e9difice du Parlement entour\u00e9 de plusieurs copies de r\u00e8glements qui fusionnent en une copie du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>.]</p>\n\n<p>En tant qu'exportateur, voici quelques points \u00e0 prendre en consid\u00e9ration pour chaque \u00e9l\u00e9ment, en commen\u00e7ant par la d\u00e9livrance de licences.</p>\n\n<p>[Propri\u00e9taire d'entreprise \u00e0 l'ordinateur et entrep\u00f4t en arri\u00e8re-plan.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: Licences]</p>\n\n<p>L'Agence canadienne d'inspection des aliments, ou ACIA, accorde des licences aux exportateurs d'aliments.</p>\n\n<p>[Ordinateur sur un bureau. \u00c0 l'\u00e9cran, une enveloppe sur laquelle est \u00e9crit [Licence SAC LIC #123AB].]</p>\n\n<p>Une licence relative \u00e0 la salubrit\u00e9 des aliments au Canada est requise pour\u00a0:</p>\n\n<p>[L'image \u00e0 l'\u00e9cran change pour celle d'un laboratoire de production o\u00f9 l'on emballe de la nourriture.]</p>\n<ul>\n<li>mener toute activit\u00e9 li\u00e9e \u00e0 la pr\u00e9paration d'aliments destin\u00e9s \u00e0 l'exportation du Canada;</li>\n<li>recevoir un certificat d'exportation ou une autre autorisation commerciale, comme l'inscription sur la liste des exportateurs admissibles.</li>\n</ul>\n\n\n<p>Conform\u00e9ment aux pratiques exemplaires actuelles, l'ACIA recommande que les exportateurs d'aliments obtiennent une licence SAC par \u00e9tablissement physique pour toutes les activit\u00e9s men\u00e9es \u00e0 cette adresse.</p>\n\n<p>[L'\u00e9cran passe \u00e0 un moteur de recherche. [Quels pays exigent des certificats d'exportation?] est entr\u00e9 dans la barre de recherche. [Admissibilit\u00e9 \u00e0 l'exportation ACIA] est entr\u00e9 dans la barre de recherche. Le r\u00e9sultat de la recherche appara\u00eet, et la page d\u00e9file vers le bas.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: Lic SAC #123AB, Lic SAC #123CD, Lic SAC #123EF]</p>\n\n<p>[Trois \u00e9tablissements physiques apparaissent \u00e0 l'\u00e9cran.]</p>\n\n<p>Vous obtiendrez ainsi un num\u00e9ro de licence unique qui assurera l'uniformit\u00e9 de tous vos dossiers d'exportation.</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: La tra\u00e7abilit\u00e9]</p>\n\n<p>[Chaque permis est d\u00e9plac\u00e9 vers un dossier de registres.]</p>\n\n<p>La tra\u00e7abilit\u00e9 vous oblige \u00e0 tenir des registres sur l'endroit o\u00f9 vous avez cultiv\u00e9 ou produit un aliment ou de qui vous l'avez achet\u00e9, et \u00e0 qui vous l'avez vendu. La tra\u00e7abilit\u00e9 peut r\u00e9duire le temps n\u00e9cessaire pour aviser les homologues \u00e9trangers lorsqu'un produit doit faire l'objet d'un rappel.</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: le contr\u00f4le pr\u00e9ventif.]</p>\n\n<p>Le troisi\u00e8me \u00e9l\u00e9ment est le contr\u00f4le pr\u00e9ventif.]</p>\n\n<p>[L'\u00e9cran passe \u00e0 divers vid\u00e9oclips de pr\u00e9paration d'aliments.]</p>\n\n<p>Pour \u00eatre admissible \u00e0 exporter des aliments aux termes de votre licence, vous devez mettre en place des contr\u00f4les pr\u00e9ventifs pour vous assurer que vos aliments sont salubres avant qu'ils ne quittent le Canada.</p>\n\n<p>[L'\u00e9cran passe \u00e0 un courriel. [Homologues \u00e9trangers] est entr\u00e9 dans la case \u00ab\u00a0Destinataire\u00a0\u00bb. [Rappel] est entr\u00e9 dans la case \u00ab\u00a0Objet\u00a0\u00bb.]</p>\n\n<p>Trois principes directeurs sont au c\u0153ur des contr\u00f4les pr\u00e9ventifs\u00a0: un, conna\u00eetre ses aliments; deux, conna\u00eetre son march\u00e9 \u00e9tranger; et trois, avoir un plan \u00e9crit.</p>\n\n<p>[Image d'un port o\u00f9 des avions, des camions et des cargos transportent des produits alimentaires.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: conna\u00eetre vos aliments]</p>\n\n<p>[Un chef cuisine.]</p>\n\n<p>En tant qu'exportateur, vous devez conna\u00eetre vos aliments et vous assurer qu'ils r\u00e9pondent aux exigences en mati\u00e8re de salubrit\u00e9, de classification, de normes, d'\u00e9tiquetage et de quantit\u00e9 nette.</p>\n\n<p>[Un crochet figure aupr\u00e8s de chaque cat\u00e9gorie.]</p>\n\n<p>De plus, v\u00e9rifiez que votre produit alimentaire correspond bien aux renseignements indiqu\u00e9s sur le certificat d'exportation et sur les autres documents d'exp\u00e9dition.</p>\n\n<p>[Main qui m\u00e9lange de la nourriture. Une liste de v\u00e9rification appara\u00eet, puis les points sont coch\u00e9s.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: Conna\u00eetre votre march\u00e9 \u00e9tranger]</p>\n\n<p>Vous \u00eates \u00e9galement responsable de conna\u00eetre votre march\u00e9 \u00e9tranger. V\u00e9rifiez que vous avez acc\u00e8s au march\u00e9 en question et collaborez avec votre importateur ou le Service des d\u00e9l\u00e9gu\u00e9s commerciaux du Canada pour vous assurer que vos aliments r\u00e9pondent aux exigences du pays importateur.</p>\n\n<p>[Une femme d'affaires \u00e0 l'ordinateur envoie un courriel \u00e0 son importateur.]</p>\n\n<p>Assurez-vous d'obtenir les services d'inspection ou de certification n\u00e9cessaires de l'ACIA avant que vos aliments ne quittent le Canada.</p>\n\n<p>[Un crochet appara\u00eet aupr\u00e8s de chaque cat\u00e9gorie.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: Plan de contr\u00f4le pr\u00e9ventif \u00e9crit]</p>\n\n<p>Enfin, assurez-vous d'avoir un plan de contr\u00f4le pr\u00e9ventif \u00e9crit. Un plan \u00e9crit d\u00e9montre que vous comprenez les risques associ\u00e9s \u00e0 vos aliments et d\u00e9crit les contr\u00f4les pr\u00e9ventifs que vous avez mis en place pour assurer la salubrit\u00e9 de vos aliments.</p>\n\n<p>[Une tablette affiche \u00ab\u00a0Plan de contr\u00f4le pr\u00e9ventif\u00a0\u00bb.]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>]</p>\n\n<p>[Un crochet appara\u00eet aupr\u00e8s de chaque cat\u00e9gorie.]</p>\n\n<p>Le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> permet aux exportateurs canadiens de r\u00e9pondre \u00e0 la demande \u00e9trang\u00e8re croissante pour leurs aliments.</p>\n\n<p>[Image d'un globe terrestre. Diff\u00e9rentes ic\u00f4nes, comme un cochon, des noix, des pommes, du poisson et d'autres produits alimentaires figurent \u00e0 diff\u00e9rents endroits sur le globe.]</p>\n\n<p>N'oubliez pas\u00a0: la salubrit\u00e9 des aliments est votre responsabilit\u00e9 et synonyme de bonnes pratiques commerciales.</p>\n\n<p>[Femme d'affaires pose fi\u00e8rement devant son entrep\u00f4t.]</p>\n\n<p>Inscrivez-vous aujourd'hui \u00e0 Mon\u00a0ACIA pour acc\u00e9der \u00e0 un nombre croissant de services en ligne, y compris les licences, les certificats d'exportation et les options de paiement \u00e9lectronique.</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: inspection.gc.ca/MonACIA]</p>\n\n<p>[Le logo <span class=\"nowrap\">\u00ab Mon ACIA \u00bb</span> se transforme en une image d'un permis, d'un certificat d'exportation et d'un t\u00e9l\u00e9phone intelligent.]</p>\n\n<p>Visitez le site Web de l'ACIA pour en apprendre davantage sur le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>.</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: inspection.gc.ca/AlimentsSalubres]</p>\n\n<p>[Mot-symbole Canada]</p>\n\n<p>[Texte sur l'\u00e9cran\u00a0: \u00a9 Sa Majest\u00e9 la Reine du chef du Canada (Agence canadienne d'inspection des aliments), 2018.]</p>\n</details>\n</figcaption>\n</figure>\r\n\n\n\n\n<div class=\"replaced-chat-wizard\"></div>"
    },
    "success": true,
    "chat_wizard": true
}