{
    "dcr_id": "1545507617988",
    "title": {
        "en": "Operational directive: Retention of Official Meat Inspection Certificate accompanying meat shipments",
        "fr": "Directive op\u00e9rationnelle : Conservation du certificat officiel d'inspection de viande accompagnant les cargaisons de viande"
    },
    "html_modified": "9-1-2019",
    "modified": "17-4-2023",
    "issued": "22-12-2018",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/sfcr_reg_op_spp_retention_meat_import_cert_1545507617988_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/sfcr_reg_op_spp_retention_meat_import_cert_1545507617988_fra"
    },
    "parent_ia_id": "1539876015036",
    "ia_id": "1545507771758",
    "parent_node_id": "1539876015036",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Operational directive: Retention of Official Meat Inspection Certificate accompanying meat shipments",
        "fr": "Directive op\u00e9rationnelle : Conservation du certificat officiel d'inspection de viande accompagnant les cargaisons de viande"
    },
    "label": {
        "en": "Operational directive: Retention of Official Meat Inspection Certificate accompanying meat shipments",
        "fr": "Directive op\u00e9rationnelle : Conservation du certificat officiel d'inspection de viande accompagnant les cargaisons de viande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1545507771758",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1539876014831",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/retention-of-omic/",
        "fr": "/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/conservation-du-coiv/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Operational directive: Retention of Official Meat Inspection Certificate accompanying meat shipments",
            "fr": "Directive op\u00e9rationnelle : Conservation du certificat officiel d'inspection de viande accompagnant les cargaisons de viande"
        },
        "description": {
            "en": "The purpose of this document is to provide direction to Canadian Food Inspection Agency (CFIA) inspection staff on requirement to shred Official Meat Inspection Certificate (OMIC) upon completion of a meat import transaction",
            "fr": "L'objectif du pr\u00e9sent document est de fournir au personnel d'inspection de l'Agence canadienne d'inspection des aliments (ACIA) des directives sur l'exigence de d\u00e9chiqueter le certificat officiel d'inspection des viandes (COIV) une fois la transaction d'importation de viandes termin\u00e9e."
        },
        "keywords": {
            "en": "Operational Guideline, Retention of Official Meat Import Certificate, meat shipments",
            "fr": "Directive op\u00e9rationnelle, Conservation, certificat officiel d'importation de viande, cargaisons de viande"
        },
        "dcterms.subject": {
            "en": "agri-food industry,food inspection,meat",
            "fr": "industrie agro-alimentaire,inspection des aliments,viande"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice president, Operations",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sident, Op\u00e9rations"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-09",
            "fr": "2019-01-09"
        },
        "modified": {
            "en": "2023-04-17",
            "fr": "2023-04-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government",
            "fr": "gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Operational directive: Retention of Official Meat Inspection Certificate accompanying meat shipments",
        "fr": "Directive op\u00e9rationnelle : Conservation du certificat officiel d'inspection de viande accompagnant les cargaisons de viande"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n<ul>\n<li><a href=\"#a1\">1.0 Purpose</a></li>\n<li><a href=\"#a2\">2.0 Authorities</a></li>\n<li><a href=\"#a3\">3.0 Background</a></li>\n<li><a href=\"#a4\">4.0 Reference documents</a></li>\n<li><a href=\"#a5\">5.0 Definitions</a></li>\n<li><a href=\"#a6\">6.0 Acronyms</a></li>\n<li><a href=\"#a7\">7.0 Operational directive</a></li>\n</ul>\n\n<h2 id=\"a1\">1.0 Purpose</h2>\n<p>The purpose of this document is to provide direction to Canadian Food Inspection Agency (CFIA) inspection staff on requirement to shred Official Meat Inspection Certificate (OMIC) upon completion of a meat import transaction.</p>\n<p>This document is intended to be used in conjunction with other guidance documents as referenced in Section 4.0.</p>\n\n<h2 id=\"a2\">2.0 Authorities</h2>\n<ul>\n<li><a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i></a> (SFCA)</li>\n<li><a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a> (SFCR) </li>\n</ul>\n<p>The inspection powers, control actions and enforcement actions authorized by the food legislations are identified and explained in the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/regulatory-response/food-regulatory-response-guidelines/eng/1540500175506/1540500221843\">Operational guideline\u00a0\u2013 Food regulatory response guidelines</a>.</p>\n\n<h2 id=\"a3\">3.0 Background</h2>\n<p>SFCR 167(d) requires the importer of meat products to provide an official document issued by the foreign state that states the meat product meets the requirements of the Acts and the Regulations. To date, this requirement has been satisfied through the use of OMICs, which are issued by the competent authority of the exporting country. As such, a paper copy of the OMIC accompanies each shipment of meat products imported into Canada.</p>\n<p>Importers provide the original OMIC in electronic format to National Import Service Centre (NISC) as part of the documentation package sent via the Integrated Import Declaration (IID) to request release of the imports. The original OMIC in paper format accompanies each shipment of meat and is presented to the inspector at the time of inspection.</p>\n<p>Currently there are two copies of the OMIC, a paper and electronic version, kept by the Agency. A policy decision was made in March 2012 to consider the electronic copy of OMIC stored in the DDS as the \"ORIGINAL\" version submitted at the time of import transaction processing. Therefore, it is not necessary to keep original OMICs accompanying meat shipments.</p>\n\n<h2 id=\"a4\">4.0 Reference documents</h2>\n<ul>\n<li><a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/regulatory-response/food-regulatory-response-guidelines/eng/1540500175506/1540500221843\">Operational guideline\u00a0\u2013 Food regulatory response guidelines</a></li>\n<li><a href=\"/importing-food-plants-or-animals/food-imports/food-specific-requirements/importing-meat-products/eng/1545799257612/1545799287057\">Industry guidance\u00a0\u2013 Overview\u00a0\u2013 Importing meat products</a></li>\n<li><a href=\"/importing-food-plants-or-animals/shipment-tracker/eng/1376414554482/1376414624555\">Industry guidance\u00a0\u2013 CFIA Shipment Tracker for Food, Plant and Animal</a></li>\n</ul>\n\n<h2 id=\"a5\">5.0 Definitions</h2>\n<p>Unless specified below, definitions are located in either the:</p>\n<ul>\n<li><a href=\"/food-safety-for-industry/toolkit-for-food-businesses/glossary-of-key-terms/eng/1430250286859/1430250287405\"><i>Safe Food for Canadians Regulations</i>: Glossary of Key Terms</a></li>\n<li><a href=\"/about-cfia/my-cfia/glossary/eng/1482260424585/1482260425091\">My CFIA Glossary of Terms</a></li>\n</ul>\n\n<h2 id=\"a6\">6.0 Acronyms</h2>\n<p>Acronyms are spelled out the first time they are used in this document and are consolidated in the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/food-business-line-acronyms-list/eng/1554135533744/1554135534038\">Food business line acronyms list</a>.</p>\n\n<h2 id=\"a7\">7.0 Operational directive</h2>\n<p>The official (digital submission) OMIC must be verified by NISC staff in the review process for the import transaction. When the review is complete, a message indicating release of the shipment will be sent to the Canada Border Services Agency (CBSA). If NISC is unable to complete the review due to, for example, the lack of an official OMIC, the shipment will not be permitted entry into Canada.</p>\n<p>If the inspector receives a meat import shipment accompanied by a paper copy of the OMIC, the official (digital version) OMIC has previously been verified by NISC staff and is available in the Digital Document Storage (DDS) system.</p>\n<p>All original OMICs in paper format must be shredded by the inspection staff once the inspection results have been entered in the Import Control Tracking System (ICTS Citrix).</p>\n<p>For skip lots of meat shipment that are not subject to inspection, importers are responsible to forward the original certificates, within\u00a010 days, to the CFIA (the responsible inspector at the import inspection establishment or to a regional office).</p>\n<p>For general inquiries related to this Operational Guidance Document, please follow established communication channels, including submitting an <a href=\"https://collab.cfia-acia.inspection.gc.ca/apps/acfia-dacia/Lists/eRAF%20Staging%20List/NewForm.aspx?Source=https%3A%2F%2Fcollab%2Ecfia%2Dacia%2Einspection%2Egc%2Eca%2Fapps%2Facfia%2Ddacia%2FSitePages%2FHome%2Easpx&amp;RootFolder=\">electronic Request for Action Form (e-RAF) - (accessible only on the Government of Canada network)</a>.</p>\r\n<!--<div class=\"alert alert-info\">\n-->\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n<ul>\n<li><a href=\"#a1\">1.0 Objectif</a></li>\n<li><a href=\"#a2\">2.0 Lois et r\u00e8glements</a></li>\n<li><a href=\"#a3\">3.0 Contexte</a></li>\n<li><a href=\"#a4\">4.0 Documents de r\u00e9f\u00e9rence</a></li>\n<li><a href=\"#a5\">5.0 D\u00e9finitions</a></li>\n<li><a href=\"#a6\">6.0 Acronymes</a></li>\n<li><a href=\"#a7\">7.0 Directive op\u00e9rationnelle</a></li>\n</ul>\n\n<h2 id=\"a1\">1.0 Objectif</h2>\n<p>L'objectif du pr\u00e9sent document est de fournir au personnel d'inspection de l'Agence canadienne d'inspection des aliments (ACIA) des directives sur l'exigence de d\u00e9chiqueter le certificat officiel d'inspection des viandes (COIV) une fois la transaction d'importation de viandes termin\u00e9e.</p>\n<p>Le pr\u00e9sent document accompagne d'autres documents d'orientation cit\u00e9s en r\u00e9f\u00e9rence dans la section\u00a04.0.</p>\n<h2 id=\"a2\">2.0 Lois et r\u00e8glements</h2>\n<ul>\n<li><a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i></a> (LSAC)</li>\n<li><a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a> (RSAC)</li>\n</ul>\n<p>Les pouvoirs d'inspection, les mesures de contr\u00f4le et les mesures de mise en application autoris\u00e9s par la loi sur les aliments sont d\u00e9finis et expliqu\u00e9s dans la <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/intervention-reglementaire/lignes-directrices-en-matiere-d-intervention-regle/fra/1540500175506/1540500221843\">Orientation op\u00e9rationnelle\u00a0\u2013 Lignes directrices en mati\u00e8re d'intervention r\u00e9glementaire relative aux aliments</a>.</p>\n\n<h2 id=\"a3\">3.0 Contexte</h2>\n<p>L'alin\u00e9a\u00a0167d) du RSAC exige que l'importateur fournisse \u00e0 l'inspecteur un document officiel d\u00e9livr\u00e9 par l'\u00c9tat \u00e9tranger, selon lequel le produit de viande satisfait aux exigences de la Loi et du pr\u00e9sent r\u00e8glement. Jusqu'\u00e0 pr\u00e9sent, cette exigence \u00e9tait satisfaite au moyen d'un COIV d\u00e9livr\u00e9 par l'autorit\u00e9 comp\u00e9tente du pays exportateur. En vertu du processus normal d'importation, une copie papier du COIV accompagne chaque cargaison de viande import\u00e9e au Canada.</p>\n\n\n<p>Pour demander la mainlev\u00e9e des marchandises import\u00e9es, les importateurs fournissent le COIV original sous forme \u00e9lectronique au Centre de service national \u00e0 l'importation (CSNI) avec l'ensemble des documents envoy\u00e9s par l'entremise de la D\u00e9claration int\u00e9gr\u00e9e des importations (DII). Le COIV papier original accompagne chaque cargaison de viande et est pr\u00e9sent\u00e9 \u00e0 l'inspecteur au moment de l'inspection.</p>\n\n<p>\u00c0 l'heure actuelle, l'Agence conserve deux\u00a0copies du COIV\u00a0: une copie papier et une copie \u00e9lectronique. Une d\u00e9cision strat\u00e9gique a \u00e9t\u00e9 prise en mars\u00a02012 selon laquelle la copie \u00e9lectronique du COIV conserv\u00e9e dans le syst\u00e8me de SDN est consid\u00e9r\u00e9e comme la version \u00ab\u00a0ORIGINALE\u00a0\u00bb soumise au moment du traitement de la transaction d'importation. Par cons\u00e9quent, il n'est pas n\u00e9cessaire de conserver le COIV original qui accompagne les cargaisons de viande.</p>\n\n<h2 id=\"a4\">4.0 Documents de r\u00e9f\u00e9rence</h2>\n<ul>\n<li><a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/intervention-reglementaire/lignes-directrices-en-matiere-d-intervention-regle/fra/1540500175506/1540500221843\">Orientation op\u00e9rationnelle\u00a0\u2013 Lignes directrices en mati\u00e8re d'intervention r\u00e9glementaire relative aux aliments</a>.</li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/importer-des-produits-de-viande/fra/1545799257612/1545799287057\">Directive de l'industrie\u00a0\u2013 Aper\u00e7u\u00a0\u2013 Importation de produits de viande</a></li>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/systeme-de-suivi-des-envois/fra/1376414554482/1376414624555\">Directive de l'industrie\u00a0\u2013 Outil de suivi des envois d'aliments, de v\u00e9g\u00e9taux et d'animaux de l'ACIA </a></li>\n</ul>\n\n<h2 id=\"a5\">5.0 Definitions</h2>\n<p>Sauf indication ci-dessous, les d\u00e9finitions figurent dans un des documents suivants\u00a0:</p>\n<ul>\n<li><a href=\"/salubrite-alimentaire-pour-l-industrie/trousse-d-outils-pour-les-entreprises-alimentaires/glossaire-des-termes-cles/fra/1430250286859/1430250287405\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>\u00a0: Glossaire des termes cl\u00e9s</a></li>\n<li><a href=\"/a-propos-de-l-acia/mon-acia/glossaire/fra/1482260424585/1482260425091\">Glossaire des termes de Mon\u00a0ACIA</a></li>\n</ul>\n\n<h2 id=\"a6\">6.0 Acronymes</h2>\n\n<p>Les acronymes sont indiqu\u00e9s dans leur forme longue la premi\u00e8re fois qu'ils sont utilis\u00e9s et se trouvent dans la <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/liste-des-acronymes-du-secteur-d-activite-des-alim/fra/1554135533744/1554135534038\">liste des acronymes du secteur d'activit\u00e9 des aliments</a>.</p>\n\n<h2 id=\"a7\">7.0 Directive op\u00e9rationnelle</h2>\n\n<p>Le personnel du CSNI doit v\u00e9rifier la copie officielle du COIV (copie \u00e9lectronique) dans le cadre du processus d'examen visant la transaction d'importation. Lorsque l'examen est termin\u00e9, un message recommandant la mainlev\u00e9e de la cargaison est envoy\u00e9 \u00e0 l'Agence des services frontaliers du Canada (ASFC). Si le personnel du CSNI ne peut terminer l'examen, par exemple en l'absence d'un COIV, la cargaison ne sera pas autoris\u00e9e \u00e0 entrer au Canada.</p>\n\n<p>Lorsqu'un inspecteur re\u00e7oit une cargaison de viande import\u00e9e accompagn\u00e9e d'un COIV papier, cela signifie que la copie \u00e9lectronique de ce COIV a \u00e9t\u00e9 d\u00e9j\u00e0 \u00e9t\u00e9 v\u00e9rifi\u00e9e par le personnel du CSNI et qu'elle est stock\u00e9e dans le syst\u00e8me de stokage des documents num\u00e9rique (SDN).</p>\n<p>Le personnel d'inspection doit d\u00e9chiqueter tous les COIV papier originaux une fois que le Rapport d'inspection \u00e0 l'importation a \u00e9t\u00e9 enregistr\u00e9 dans le Syst\u00e8me de contr\u00f4le et de suivi des importation (SCSI Citrix).</p>\n<p>Pour ce qui est des lots d'une cargaison de viande qui n'ont pas \u00e0 subir d'inspection importateur faire parvenir les certificats originaux \u00e0 l'ACIA (l'inspecteur responsable \u00e0 l'\u00e9tablissement d'inspection de l'importation ou \u00e0 un bureau r\u00e9gional) dans un d\u00e9lai de 10 jours.</p>\n<p>Pour toute demande de renseignements g\u00e9n\u00e9raux sur la pr\u00e9sente orientation op\u00e9rationnelle, veuillez suivre les voies de communication \u00e9tablies, notamment en envoyant un\u00a0<a href=\"https://collab.cfia-acia.inspection.gc.ca/apps/acfia-dacia/Lists/eRAF%20Staging%20List/NewForm.aspx?Source=https%3A%2F%2Fcollab%2Ecfia%2Dacia%2Einspection%2Egc%2Eca%2Fapps%2Facfia%2Ddacia%2FSitePages%2FHome%2Easpx&amp;RootFolder=\">formulaire \u00e9lectronique de demande de suivi (FEDS) - (accessible uniquement sur le r\u00e9seau du gouvernement du Canada)</a>.</p>\r\n<!--<div class=\"alert alert-info\">\n-->\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}