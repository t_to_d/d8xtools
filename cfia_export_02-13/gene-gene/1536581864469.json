{
    "dcr_id": "1536581864469",
    "title": {
        "en": "Appendix M: Pressure switch settings",
        "fr": "Annexe M\u00a0: R\u00e9glages de pressostat"
    },
    "html_modified": "2024-02-13 9:53:15 AM",
    "modified": "2019-01-11",
    "issued": "2019-01-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_preventive_control_business_dairy_switch_appM_1536581864469_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_preventive_control_business_dairy_switch_appM_1536581864469_fra"
    },
    "ia_id": "1536581864719",
    "parent_ia_id": "1534954778164",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Making food - Manufacturing|processing|preparing|preserving|treating|Applying preventive controls|Maintaining equipment",
        "fr": "Production d'aliments - Fabrication|transformation|conditionnement|pr\u00e9servation et traitement|Application de mesures de contr\u00f4le pr\u00e9ventif|Entretien de l'\u00e9quipement"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1534954778164",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Appendix M: Pressure switch settings",
        "fr": "Annexe M\u00a0: R\u00e9glages de pressostat"
    },
    "label": {
        "en": "Appendix M: Pressure switch settings",
        "fr": "Annexe M\u00a0: R\u00e9glages de pressostat"
    },
    "templatetype": "content page 1 column",
    "node_id": "1536581864719",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1534954777758",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/dairy-products/appendix-m/",
        "fr": "/controles-preventifs/produits-laitiers/annexe-m/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Appendix M: Pressure switch settings",
            "fr": "Annexe M\u00a0: R\u00e9glages de pressostat"
        },
        "description": {
            "en": "This chart shows the operating temperatures of different pressure switch settings (PSIG) at sea level.",
            "fr": "Ce tableau indique les temp\u00e9ratures de fonctionnement de diff\u00e9rents r\u00e9glages du pressostat (lb/po2) au niveau de la mer."
        },
        "keywords": {
            "en": "food, guidance, Making Food, SFCR, Applying Preventive Controls, Manufacturing, Processing, Preparing, Preserving, Treating, Dairy Products, Appendix M, Pressure switch settings, operating temperatures of different pressure switch settings at sea level",
            "fr": "aliments, salubrit\u00e9 des aliments, contr\u00f4le pr\u00e9ventif, entreprises, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada, RSAC, entreprises alimentaires, application de mesures de contr\u00f4le pr\u00e9ventif, fabrication, transformation, conditionnement, pr\u00e9servation et traitemen, produits laitiers, temp\u00e9ratures de fonctionnement de diff\u00e9rents r\u00e9glages du pressostat au niveau de la mer"
        },
        "dcterms.subject": {
            "en": "access to information,food,agri-food industry,agri-food products,dairy products,government publication,food safety",
            "fr": "acc\u00e8s \u00e0 l'information,aliment,industrie agro-alimentaire,produit agro-alimentaire,produit laitier,publication gouvernementale,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-01-12",
            "fr": "2019-01-12"
        },
        "modified": {
            "en": "2019-01-11",
            "fr": "2019-01-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Appendix M: Pressure switch settings",
        "fr": "Annexe M\u00a0: R\u00e9glages de pressostat"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<figure><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_preventive_control_business_dairy_switch_appM_img1_1536582442855_eng.jpg\" alt=\"Chart of pressure switch settings. Description follows.\" class=\"img-responsive\">\n<details> <summary>Description of image - Chart: Pressure switch settings</summary>\n<p>This chart shows the operating temperatures of different pressure switch settings at sea level.</p>\n<ul>\n<li>At temperatures of 88\u00b0Celsius(C) (191\u00b0Fahrenheit (F)) to 99\u00b0<abbr title=\"Celsius\">C</abbr> (210\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 10 pounds per square inch gauge (PSIG) at sea level.</li>\n<li>At 104\u00b0<abbr title=\"Celsius\">C</abbr> (220\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 13 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 110\u00b0<abbr title=\"Celsius\">C</abbr> (230\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 16 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 116\u00b0<abbr title=\"Celsius\">C</abbr> (240\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 20 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 121\u00b0<abbr title=\"Celsius\">C</abbr> (250\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 25 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 127\u00b0<abbr title=\"Celsius\">C</abbr> (260\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 31 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 132\u00b0<abbr title=\"Celsius\">C</abbr> (270\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 37 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 138\u00b0<abbr title=\"Celsius\">C</abbr> (280\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 45 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 143\u00b0<abbr title=\"Celsius\">C</abbr> (290\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 53 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n<li>At 149\u00b0<abbr title=\"Celsius\">C</abbr> (300\u00b0<abbr title=\"Fahrenheit\">F</abbr>) the pressure switch setting is 62 <abbr title=\"pounds per square inch gauge\">PSIG</abbr> at sea level</li>\n</ul>\n</details> </figure>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n<figure><img src=\"/DAM/DAM-food-aliments/STAGING/images-images/sfcr_preventive_control_business_dairy_switch_appM_img1_1536582442855_fra.jpg\" alt=\"Tableau pour r\u00e9glages de pressostat. Description ci-dessous.\" class=\"img-responsive\">\n<details> <summary>Description de l'image - Tableau\u00a0: R\u00e9glages de pressostat</summary>\n<p>Ce tableau indique les temp\u00e9ratures de fonctionnement de diff\u00e9rents r\u00e9glages du pressostat au niveau de la mer.</p>\n<ul>\n<li>\u00c0 des temp\u00e9ratures entre 88\u00b0Celsius (C) (191\u00b0Fahrenheit (F)) et 99\u00b0<abbr title=\"Celsius\">C</abbr> (210\u00b0<abbr title=\"Fahrenheit\">F</abbr>), le r\u00e9glage du pressostat est \u00e0 10 livres par pouce carr\u00e9 (lb/po\u00b2) au niveau de la mer.</li>\n<li>\u00c0 104\u00b0<abbr title=\"Celsius\">C</abbr> (220\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 13\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 110\u00b0<abbr title=\"Celsius\">C</abbr> (230\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 16\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 116\u00b0<abbr title=\"Celsius\">C</abbr> (240\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 20\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 121\u00b0<abbr title=\"Celsius\">C</abbr> (250\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 25\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 127\u00b0<abbr title=\"Celsius\">C</abbr> (260\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 31\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 132\u00b0<abbr title=\"Celsius\">C</abbr> (270\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 37\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 138\u00b0<abbr title=\"Celsius\">C</abbr> (280\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 45\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 143\u00b0<abbr title=\"Celsius\">C</abbr> (290\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 53\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n<li>\u00c0 149\u00b0<abbr title=\"Celsius\">C</abbr> (300\u00b0<abbr title=\"Fahrenheit\">F</abbr>)\u00a0le r\u00e9glage de la pression est \u00e0 62\u00a0<abbr title=\"livres par pouce carr\u00e9\">lb/po\u00b2</abbr>\u00a0au niveau de la mer</li>\n</ul>\n</details> </figure>\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}