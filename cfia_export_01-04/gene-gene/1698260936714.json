{
    "dcr_id": "1698260936714",
    "title": {
        "en": "Notice to <span class=\"nowrap\">industry \u2013</span> End of three-year transition period for Fertilizers Regulations",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013\u00a0Fin de la p\u00e9riode de transition de trois ans pour le R\u00e8glement sur les engrais"
    },
    "html_modified": "26-10-2023",
    "modified": "26-10-2023",
    "issued": "26-10-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/notind_end_fert_regs_trstn_20231026_1698260936714_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/notind_end_fert_regs_trstn_20231026_1698260936714_fra"
    },
    "parent_ia_id": "1490849960260",
    "ia_id": "1698260937055",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice to <span class=\"nowrap\">industry \u2013</span> End of three-year transition period for Fertilizers Regulations",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013\u00a0Fin de la p\u00e9riode de transition de trois ans pour le R\u00e8glement sur les engrais"
    },
    "label": {
        "en": "Notice to <span class=\"nowrap\">industry \u2013</span> End of three-year transition period for Fertilizers Regulations",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013\u00a0Fin de la p\u00e9riode de transition de trois ans pour le R\u00e8glement sur les engrais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1698260937055",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1490849930025",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/notices-to-industry/notice-end-of-3-year-transition/",
        "fr": "/protection-des-vegetaux/engrais/avis-a-l-industrie/avis-fin-de-la-transition-de-3-ans/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice to industry \u2013 End of three-year transition period for Fertilizers Regulations",
            "fr": "Avis \u00e0 l'industrie\u00a0\u2013\u00a0Fin de la p\u00e9riode de transition de trois ans pour le R\u00e8glement sur les engrais"
        },
        "description": {
            "en": "October 26, 2023, marks the end of the three-year transition period for the updated Fertilizers Regulations.",
            "fr": "La p\u00e9riode de transition de trois ans pour la mise \u00e0 jour du R\u00e8glement sur les engrais prend fin le 26 octobre 2023."
        },
        "keywords": {
            "en": "notice, industry, 3, year, transition, fertilizers",
            "fr": "Avis, industrie, 3, ans, transition, engrais"
        },
        "dcterms.subject": {
            "en": "horticulture,plants",
            "fr": "horticulture,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "modified": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice to industry \u2013 End of three-year transition period for Fertilizers Regulations",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013\u00a0Fin de la p\u00e9riode de transition de trois ans pour le R\u00e8glement sur les engrais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-bttm-lg\">October 26, 2023</p>\n\n<p>Today marks the end of the three-year transition period for the updated <i>Fertilizers Regulations</i> that were published in the <a href=\"https://gazette.gc.ca/rp-pr/p2/2020/2020-11-11/html/sor-dors232-eng.html\">Canada Gazette, Part II on November 11, 2020</a>.</p>\n\n<p>Regulated parties must now comply with updated regulations. Guidance on how to meet the regulatory requirements are available on the <a href=\"/plant-health/fertilizers/eng/1299165827648/1299165914316\" title=\"Fertilizers\">CFIA</a> website.</p>\n\n<p>The CFIA has made it easier for industry to submit, track and pay for new product registrations through <a href=\"/about-cfia/my-cfia/eng/1482204298243/1482204318353\">My CFIA</a>, the CFIA's secure and convenient digital platform. In order to take advantage of the fertilizer and supplement online services, industry will need to <a href=\"/about-cfia/my-cfia/before-you-sign-up/eng/1539706469438/1539706470034\">sign up for a My CFIA account</a>.</p>\n\n<h2>Contact</h2>\n\n<p>Fertilizer Safety Section<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">Pre-market Application Submissions Office</a> (PASO)<br>\n<br>\n<br>\n<br>\n<br>\n</p>\n\n<h2>Additional information</h2>\n\n<ul>\n<li><a href=\"/plant-health/fertilizers/notices-to-industry/2023-05-03/eng/1682633127754/1682633128598\">Implementation of the amended <i>Fertilizers Regulations</i>\u00a0\u2013\u00a0prioritization of compliance activities after the end of the transitional period</a></li>\n<li><a href=\"/plant-health/fertilizers/service-standards/eng/1678885223308/1678885224011\">Update on fertilizer service standards</a></li>\n<li><a href=\"/about-cfia/my-cfia/user-guidance/eng/1545974154373/1545974178498\">My CFIA\u00a0\u2013\u00a0User guidance</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-bttm-lg\">26 octobre 2023</p>\n\n<p>C'est aujourd'hui que prend fin la p\u00e9riode de transition de trois ans pour la mise \u00e0 jour du R\u00e8glement sur les engrais qui a \u00e9t\u00e9 publi\u00e9 dans <a href=\"https://gazette.gc.ca/rp-pr/p2/2020/2020-11-11/html/sor-dors232-fra.html\">la Gazette du Canada, Partie II, le 11 novembre 2020</a>. </p>\n\n<p>Les parties r\u00e9glement\u00e9es doivent d\u00e9sormais se conformer aux r\u00e9glementations mises \u00e0 jour. Des conseils sur la mani\u00e8re de satisfaire aux exigences r\u00e9glementaires sont disponibles sur le site web de l'<a href=\"/protection-des-vegetaux/engrais/fra/1299165827648/1299165914316\" title=\"Engrais \">ACIA</a>.</p>\n\n<p>L'ACIA facilite \u00e9galement la soumission, le suivi et le paiement par l'industrie des enregistrements de nouveaux produits par l'interm\u00e9diaire de <a href=\"/a-propos-de-l-acia/mon-acia/fra/1482204298243/1482204318353\">Mon ACIA</a>, la plateforme num\u00e9rique s\u00e9curis\u00e9e et pratique de l'Agence. Pour profiter des services en ligne sur les engrais et les suppl\u00e9ments, les acteurs de l'industrie devront <a href=\"/a-propos-de-l-acia/mon-acia/avant-de-vous-inscrire/fra/1539706469438/1539706470034\">cr\u00e9er un compte Mon ACIA</a>.</p>\n\n<h2>Coordonn\u00e9es</h2>\n\n<p>Section de l'innocuit\u00e9 des engrais<br>\n<a href=\"mailto:cfia.paso-bpdpm.acia@inspection.gc.ca\">Bureau de pr\u00e9sentation de demandes pr\u00e9alables \u00e0 la mise en march\u00e9</a> (BPDPM)<br>\n<br>\n<span lang=\"en\">Camelot</span><br>\n<br>\n<br>\n</p>\n\n<h2>Renseignements suppl\u00e9mentaires</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/avis-a-l-industrie/2023-05-03/fra/1682633127754/1682633128598\">Avis \u00e0 l'industrie\u00a0: Mise en \u0153uvre de la modification du R\u00e8glement sur les engrais\u00a0\u2013\u00a0\u00e9tablissement des priorit\u00e9s li\u00e9es aux activit\u00e9s de conformit\u00e9 qui seront r\u00e9alis\u00e9es au-del\u00e0 de la p\u00e9riode de transition</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/service-des-engrais/fra/1678885223308/1678885224011\">Mise \u00e0 jour des normes de service des engrais</a></li>\n<li><a href=\"/a-propos-de-l-acia/mon-acia/orientation-de-l-utilisateur/fra/1545974154373/1545974178498\">Mon ACIA\u00a0\u2013\u00a0Orientation de l'utilisateur</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}