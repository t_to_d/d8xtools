{
    "dcr_id": "1299171929218",
    "title": {
        "en": "Potatoes",
        "fr": "Pommes de terre"
    },
    "html_modified": "2024-01-26 2:20:25 PM",
    "modified": "2021-11-30",
    "issued": "2011-03-03 12:05:31",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_pota_1299171929218_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_pota_1299171929218_fra"
    },
    "ia_id": "1299172039964",
    "parent_ia_id": "1299162708850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299162708850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Potatoes",
        "fr": "Pommes de terre"
    },
    "label": {
        "en": "Potatoes",
        "fr": "Pommes de terre"
    },
    "templatetype": "content page 1 column",
    "node_id": "1299172039964",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299162629094",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/potatoes/",
        "fr": "/protection-des-vegetaux/pommes-de-terre/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Potatoes",
            "fr": "Pommes de terre"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency is responsible for the federal Seed Potato Certification Program, and policy directives to prevent the introduction and spread of diseases and regulated quarantine pests of potatoes in Canada.",
            "fr": "L'Agence canadienne d'inspection des aliments est responsable pour le programme f\u00e9d\u00e9ral de certification des pommes de terre,d'\u00e9tablir et de tenir \u00e0 jour des directives afin de pr\u00e9venir l'introduction et la propagation des organismes nuisibles r\u00e9glement\u00e9s justiciables de quarantaine des pommes de terre au Canada."
        },
        "keywords": {
            "en": "seed potato certification, phytosanitary, export certification, imports",
            "fr": "certification des pommes de terre, phytosanitaire, certification \u00e0 l'exportation, importation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-03-03 12:05:31",
            "fr": "2011-03-03 12:05:31"
        },
        "modified": {
            "en": "2021-11-30",
            "fr": "2021-11-30"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Potatoes",
        "fr": "Pommes de terre"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The Canadian Food Inspection Agency (CFIA) is responsible for the federal Seed Potato Certification Program.</p>\n\n<p>The objective of the Seed Potato Certification Program is to supply Canadian growers of seed, table stock and processing potatoes with certified seed which is of high varietal integrity and is relatively free of tuber borne diseases. The legal bases of the Seed Potato Certification Program are the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a> and the <a href=\"/english/reg/jredirect2.shtml?seesemr\"><i>Seeds Regulations</i></a> Part II.</p>\n\n<p>Access CFIA's <a href=\"/plant-health/seeds/seed-regulatory-modernization/eng/1610723659167/1610723659636\">seed regulatory modernization</a> to learn more about this initiative.</p>\n\n<h2>Available information</h2>\n<ul class=\"lst-spcd\">\n<li><a href=\"/about-cfia/find-a-form/form-cfia-acia-1317/eng/1426264529452/1426264530124\">Application for Seed Potato Crop Inspection - Growers Declaration (CFIA/ACIA\u00a01317)</a></li>\n<li><a href=\"/eng/1328823628115/1328823702784#c5295\">Application form for Issuance of Nuclear Stock Tags and Certificates (CFIA/ACIA\u00a05295)</a></li>   \n<li><a href=\"/plant-health/potatoes/approved-laboratories/eng/1313199075408/1313199137952\">Approved laboratories</a></li>\n<li><a href=\"/plant-health/potatoes/potato-varieties/eng/1299172436155/1299172577580\">Canadian potato varieties</a></li>\n<li><a href=\"/plant-health/potatoes/guidance-documents/eng/1328485865509/1328485925503\">Guidance documents</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-import/primer/eng/1324568450671/1324569734910\">Importing plants and plant products: what you need to know</a></li>    \n<li><a href=\"/plant-health/potatoes/guidance-documents/national-farm-level-biosecurity-standard/eng/1351685363578/1351685528151\">National farm-level biosecurity standard</a></li>\n<li>Pest information\n<ul>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/golden-nematode/eng/1336742692502/1336742884627\">Potato cyst nematodes</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-diseases/pmtv/eng/1327447877833/1327447959911\">Potato mop-top virus, (PMTV)</a></li>\n<li><a href=\"/plant-health/invasive-species/nematodes-snails-and-others/potato-rot-nematode/eng/1326330708349/1326330805394\">Potato rot nematode</a></li> \n<li><a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/eng/1327933703431/1327933793006\">Potato wart or potato canker</a></li>\n</ul>\n</li>\n<li><a href=\"/plant-health/invasive-species/directives/potatoes/eng/1312235432509/1312239255997\">Plant Protection Policy Directives</a></li>\n</ul>\n<h2>Additional government and industry information</h2>\n<ul>\n<li><a href=\"https://agriculture.canada.ca/en/sector/horticulture/market-information-infohort/storage-reports/potato\">Agriculture and Agri-Food Canada - potato reports</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-protection-organizations/eng/1312732291086/1312732417251\">Plant protection organizations</a></li>\n<li><a href=\"https://inspection.canada.ca/apps/eng/guidance\">Potatoes for human consumption</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>L'Agence canadienne d'inspection des aliments (ACIA) est responsable du programme f\u00e9d\u00e9ral de certification des pommes de terre.</p>\n\n<p>L'objectif du Programme f\u00e9d\u00e9ral de certification des pommes de terre de semence est de fournir aux producteurs canadiens de pommes de terre de semence, de pommes de terre de consommation et de pommes de terre de transformation, des semences qui r\u00e9pondent aux exigences du programme de certification, qui poss\u00e8dent une grande int\u00e9grit\u00e9 vari\u00e9tale et qui sont relativement exemptes de maladies transmises par les tubercules. Le fondement juridique du Programme f\u00e9d\u00e9ral de certification des pommes de terre de semence est la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a> et la partie II du <a href=\"/francais/reg/jredirect2.shtml?seesemr\"><i>R\u00e8glement sur les semences</i></a>.</p>\n\n<p>Consultez l'information sur la <a href=\"/protection-des-vegetaux/semences/modernisation-du-reglement-sur-les-semences/fra/1610723659167/1610723659636\">modernisation du <i>R\u00e8glement sur les semences</i></a> pour en savoir plus sur cette initiative.</p>\n\n<h2>Renseignements disponibles </h2>\n\n<ul class=\"lst-spcd\">\n<li><a href=\"/fra/1328823628115/1328823702784#c5295\">Demande de d\u00e9livrance d'\u00e9tiquettes et de certificats Mat\u00e9riel nucl\u00e9aire (CFIA/ACIA\u00a05295)</a></li>\n<li><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-1317/fra/1426264529452/1426264530124\">Demande d'inspection sur pied pour pommes de terre de semence - D\u00e9claration du producteur (CFIA/ACIA\u00a01317)</a> </li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/directives/pommes-de-terre/fra/1312235432509/1312239255997\">Directives sur la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"/protection-des-vegetaux/pommes-de-terre/documents-d-orientation/fra/1328485865509/1328485925503\">Documents d'orientation</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-dore/fra/1336742692502/1336742884627\">Importation de v\u00e9g\u00e9taux et de produits v\u00e9g\u00e9taux : Ce que vous devez savoir</a></li>\n<li><a href=\"/protection-des-vegetaux/pommes-de-terre/laboratoires-approuves/fra/1313199075408/1313199137952\">Laboratoires approuv\u00e9s</a></li>\n<li><a href=\"/protection-des-vegetaux/pommes-de-terre/documents-d-orientation/norme-nationale-de-biosecurite-a-la-ferme/fra/1351685363578/1351685528151\">Norme nationale de bios\u00e9curit\u00e9 \u00e0 la ferme</a></li>\n<li>Phytoravageurs\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/fra/1327933703431/1327933793006\">Gale (tumeur) verruqueuse de la pomme de terre</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-dore/fra/1336742692502/1336742884627\">N\u00e9matodes \u00e0 kyste de la pomme de terre</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/nematode-de-la-pourriture-des-racines/fra/1326330708349/1326330805394\">N\u00e9matode de la pourriture des racines</a></li> \n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/vstpt/fra/1327447877833/1327447959911\">Virus du sommet touffu de la pomme de terre (VSTPT)</a></li>\n</ul>\n</li>\n<li><a href=\"/protection-des-vegetaux/pommes-de-terre/varietes-de-pomme-de-terre/fra/1299172436155/1299172577580\">Vari\u00e9t\u00e9s de pomme de terre au Canada</a></li>\n\n</ul>\n\n\n<h2>Autres renseignements du gouvernement et de l'industrie</h2>\n<ul>\n<li><a href=\"https://agriculture.canada.ca/fr/secteur/horticulture/information-marches-infohort/rapports-entreposages/pommes-terre\">Agriculture et agroalimentaire Canada - Rapports sur les pommes de terre</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/organisations-de-la-protection-des-vegetaux/fra/1312732291086/1312732417251\">Organisations de la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"https://inspection.canada.ca/apps/fra/orientation\">Pommes de terre destin\u00e9es \u00e0 l'alimentation humaine</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}