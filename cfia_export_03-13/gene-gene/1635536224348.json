{
    "dcr_id": "1635536224348",
    "title": {
        "en": "Framework on mandatory COVID-19 testing",
        "fr": "Cadre relatif au d\u00e9pistage obligatoire de la COVID\u201119"
    },
    "html_modified": "2024-03-12 3:59:10 PM",
    "modified": "2021-11-08",
    "issued": "2021-11-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/covid19_framework_mandatory_testing_1635536224348_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/covid19_framework_mandatory_testing_1635536224348_fra"
    },
    "ia_id": "1635536224754",
    "parent_ia_id": "1636300603491",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1636300603491",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Framework on mandatory COVID-19 testing",
        "fr": "Cadre relatif au d\u00e9pistage obligatoire de la COVID\u201119"
    },
    "label": {
        "en": "Framework on mandatory COVID-19 testing",
        "fr": "Cadre relatif au d\u00e9pistage obligatoire de la COVID\u201119"
    },
    "templatetype": "content page 1 column",
    "node_id": "1635536224754",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1636300602616",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/covid-19/latest-updates-for-cfia-employees/vaccination-for-cfia-employees/mandatory-testing/",
        "fr": "/covid-19/les-dernieres-mises-a-jour-pour-les-employe-e-s-de/vaccinations-des-employes-de-l-acia/cadre-relatif-au-depistage-obligatoire-de-la-covid/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Framework on mandatory COVID-19 testing",
            "fr": "Cadre relatif au d\u00e9pistage obligatoire de la COVID\u201119"
        },
        "description": {
            "en": "CFIA's Incident Command System on COVID-19 continues to monitor the situation.",
            "fr": "Le Syst\u00e8me de commandement des interventions de l'ACIA sur le COVID-19 continue de surveiller la situation."
        },
        "keywords": {
            "en": "Canadian Food Inspection Agency, Update on 2019 novel coronavirus, COVID-19",
            "fr": "Agence canadienne d'inspection des aliments, Mise \u00e0 jour au sujet du nouveau coronavirus 2019, COVID-19"
        },
        "dcterms.subject": {
            "en": "communications,government communications,government information,health care,health indicators,safety",
            "fr": "communications,communications gouvernementales,information gouvernementale,soins de sant\u00e9,indicateur de sant\u00e9,s\u00e9curit\u00e9"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Vice President, Communications and Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Vice pr\u00e9sidente, Communications et affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-11-07",
            "fr": "2021-11-07"
        },
        "modified": {
            "en": "2021-11-08",
            "fr": "2021-11-08"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,educators,general public,government,media,scientists",
            "fr": "entreprises,\u00e9ducateurs,grand public,gouvernement,m\u00e9dia,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Framework on mandatory COVID-19 testing",
        "fr": "Cadre relatif au d\u00e9pistage obligatoire de la COVID\u201119"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n<ul>\n<li><a href=\"#a1\">Context</a></li>\n<li><a href=\"#a2\">Timeline for implementation</a></li>\n<li><a href=\"#a3\">Stakeholders roles and responsibilities</a></li>\n<li><a href=\"#a4\">References</a></li>\n</ul>\n\n<h2 id=\"a1\">Context</h2>\n<p>This document sets out the Canadian Food Inspection Agency's administrative approach to Human Resources matters related to the implementation of the <a href=\"/covid-19/latest-updates-for-cfia-employees/vaccination-for-cfia-employees/policy-on-covid-19-vaccination/eng/1635536165716/1635536166388\"><i>Policy on COVID-19 Vaccination for the Canadian Food Inspection Agency</i></a> (the Policy on Vaccination) and the <a href=\"/covid-19/latest-updates-for-cfia-employees/vaccination-for-cfia-employees/framework-for-implementation-of-the-policy/eng/1635536189439/1635536189830\"><i>Framework for Implementation of the Policy on COVID-19 Vaccination for the Canadian Food Inspection Agency</i></a> (vaccination framework).</p>\n\n<p>To support mandatory vaccination, mandatory testing will be offered as an accommodation measure for employees unable to be vaccinated or who are partially vaccinated and  must report to work on-site as per the vaccination framework.</p>\n<p>Refusal to undergo testing, disclose testing status and results to the employer will be considered a refusal to comply with the Policy on Vaccination.</p>\n<h2 id=\"a2\">Timeline for implementation</h2>\n<p>December 6, 2021 \u2013 Full implementation date of the Policy on Vaccination</p>\n<ul>\n<li>Testing begins, at least 3 times per week, for those unable to be vaccinated and who must report to work on-site as per the vaccination framework.</li>\n<li>Testing also begins, at least 3 times per week, for employees who must report to work on-site, who have their first dose as of the attestation deadline and will get their required second dose within a period of up to 10 weeks. Testing will be mandatory, as a temporary measure, until 2 weeks after their second required dose, per the vaccination framework.</li>\n</ul>\n<h2 id=\"a3\">Stakeholder roles and responsibilities</h2>\n<p>In addition to responsibilities under the Policy on Vaccination:</p>\n<h3>The CFIA, as the employer</h3>\n<ul>\n<li>Consult with the Health Canada Testing Secretariat on the Health Canada testing protocol requirements and supply management of tests (procurement, inventory, and distribution).</li>\n<li>Establish testing schedule (for example: 3 times a week, Monday-Wednesday-Friday or at regular intervals if working a variable schedule) for employees unable to be vaccinated that must report to work, on-site aligned with Health Canada's testing protocol and based on operational requirements.</li>\n<li>Ensure adequate distribution of self-administered tests to their employees if provided, manage inventory and test results.</li>\n<li>Determine a site-specific or departmental process to report testing results of employees when required.</li>\n</ul>\n<h3>Managers</h3>\n<ul>\n<li>Communicate with employees regarding the mandatory testing program requirements, and procedures, including the established testing schedule, and testing reporting process.</li>\n<li>Monitor compliance of employees with all aspects of the mandatory testing program.</li>\n</ul>\n<h3>Employees</h3>\n<ul>\n<li>Employees unable to be vaccinated, and who must report to work on-site even occasionally will be required to be tested including reporting their testing results at least 3 times per week (for example: positive or negative) per established departmental/agency protocols and Health Canada's Testing Protocol.</li>\n<li>Notify the manager if the instructions pertaining to testing administration or if employee rights are unclear.</li>\n<li>In addition to testing, follow local public health guidelines and any other required COVID-19 preventative measures in the workplace (for example: masks, physical distancing, as required).</li>\n</ul>\n<h3>Public Services and Procurement Canada</h3>\n<ul>\n<li>Provides support for the implementation of the testing framework such as procurement of tests via the Health Canada Testing Secretariat, facilities, and logistics.</li>\n</ul>\n<h2 id=\"a4\">References</h2>\n<h3>Legislation</h3>\n<ul>\n<li><a href=\"/english/reg/jredirect2.shtml?clc\"><i>Canada Labour Code</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?osh\"><i>Canada Occupational Health and Safety Regulations</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?geca\"><i>Government Employees Compensation Act</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?pa\"><i>Privacy Act</i></a></li>\n<li><a href=\"/english/reg/jredirect2.shtml?pr\"><i>Privacy Regulations</i></a></li>\n</ul>\n<h3>Related policy instruments</h3>\n<ul>\n<li><i><a href=\"/covid-19/latest-updates-for-cfia-employees/vaccination-for-cfia-employees/policy-on-covid-19-vaccination/eng/1635536165716/1635536166388\">Policy on COVID-19 Vaccination for the Canadian Food Inspection Agency</a></i></li>\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=25049\"><i>Values and Ethics Code for the Public Sector</i></a></li>\n<li><a href=\"/about-the-cfia/job-opportunities/hr-information/code-of-conduct/eng/1341156915280/1341157034490\"><i>CFIA Code of Conduct</i></a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n<ul>\n<li><a href=\"#a1\">Contexte</a></li>\n<li><a href=\"#a2\">D\u00e9lais de mise en \u0153uvre</a></li>\n<li><a href=\"#a3\">R\u00f4les et responsabilit\u00e9s des intervenants</a></li>\n<li><a href=\"#a4\">R\u00e9f\u00e9rences</a></li>\n</ul>\n\n<h2 id=\"a1\">Contexte</h2>\n<p>Le pr\u00e9sent document d\u00e9crit l'approche de l'Agence canadienne d'inspection des aliments \u00e0 l'\u00e9gard des questions de ressources humaines li\u00e9es \u00e0 la mise en \u0153uvre de la <a href=\"/covid-19/les-dernieres-mises-a-jour-pour-les-employe-e-s-de/vaccinations-des-employes-de-l-acia/politique-de-l-agence-canadienne-d-inspection-des-/fra/1635536165716/1635536166388\"><i>Politique sur la vaccination contre la COVID\u201119 de l'Agence canadienne d'inspection des aliments</i></a> (politique sur la vaccination) et le <a href=\"/covid-19/les-dernieres-mises-a-jour-pour-les-employe-e-s-de/vaccinations-des-employes-de-l-acia/cadre-de-mise-en-%C5%93uvre-de-la-politique-sur-la-vacc/fra/1635536189439/1635536189830\"><i>Cadre de mise en \u0153uvre de la Politique sur la vaccination contre la COVID\u201119 de l'Agence canadienne d'inspection des aliments</i></a> (cadre sur la vaccination).</p>\n<p>Conform\u00e9ment au cadre sur la vaccination, pour appuyer la vaccination obligatoire, un d\u00e9pistage obligatoire sera propos\u00e9 comme mesure d'adaptation aux employ\u00e9s qui ne peuvent pas \u00eatre vaccin\u00e9s ou qui sont partiellement vaccin\u00e9s et qui doivent se pr\u00e9senter physiquement \u00e0 leur lieu de travail.</p>\n<p>Le refus de se soumettre au d\u00e9pistage, de divulguer son statut et les r\u00e9sultats en lien avec un d\u00e9pistage \u00e0 l'employeur sera consid\u00e9r\u00e9 comme un refus de se conformer \u00e0 la politique sur la vaccination.</p>\n<h2 id=\"a2\">D\u00e9lais de mise en \u0153uvre</h2>\n<p>Le 6 d\u00e9cembre\u00a02021 \u2013 Date de mise en \u0153uvre compl\u00e8te de la politique sur la vaccination</p>\n<ul>\n<li>Le d\u00e9pistage d\u00e9bute, au moins 3 fois par semaine, pour les employ\u00e9s qui ne peuvent pas \u00eatre vaccin\u00e9s et qui doivent se pr\u00e9senter physiquement au lieu de travail, selon le\u00a0cadre sur la vaccination.</li>\n<li>Les tests de d\u00e9pistage commenceront \u00e9galement \u00e0 \u00eatre administr\u00e9s, au moins 3 fois par semaine, aux employ\u00e9s qui ont re\u00e7u leur premi\u00e8re dose \u00e0 la date limite de pr\u00e9sentation de l'attestation, qui recevront leur deuxi\u00e8me dose dans les dix semaines suivant leur premi\u00e8re dose et qui doivent se pr\u00e9senter physiquement au lieu de travail. Les d\u00e9pistages seront obligatoires \u00e0 titre de mesure temporaire jusqu'\u00e0 ce qu'une p\u00e9riode de 2 semaines se soit \u00e9coul\u00e9e apr\u00e8s la r\u00e9ception de leur deuxi\u00e8me dose, conform\u00e9ment au\u00a0cadre sur la vaccination. </li>\n</ul>\n<h2 id=\"a3\">R\u00f4les et responsabilit\u00e9s des intervenants</h2>\n<p>Les dispositions suivantes s'ajoutent aux responsabilit\u00e9s pr\u00e9vues dans la politique sur la vaccination\u00a0:</p>\n<h3>L'ACIA, en tant qu'employeur</h3>\n<ul>\n<li>Consulter le Secr\u00e9tariat du d\u00e9pistage de Sant\u00e9 Canada au sujet des exigences du protocole de d\u00e9pistage de Sant\u00e9 Canada et de la gestion de l'approvisionnement des tests (approvisionnement, inventaire et distribution).</li>\n<li>\u00c9tablir un calendrier de d\u00e9pistage (par exemple, 3 fois par semaines, lundi, mercredi et vendredi, ou \u00e0 des intervalles r\u00e9guli\u00e8res si les employ\u00e9s travaillent selon un horaire variable) fond\u00e9 sur le protocole de d\u00e9pistage de Sant\u00e9 Canada et sur les exigences op\u00e9rationnelles pour les employ\u00e9s qui ne peuvent pas \u00eatre vaccin\u00e9s et qui doivent se pr\u00e9senter physiquement au travail.</li>\n<li>Assurer la distribution ad\u00e9quate des tests de d\u00e9pistage auto-administr\u00e9s aux employ\u00e9s, le cas \u00e9ch\u00e9ant, et g\u00e9rer l'inventaire et les r\u00e9sultats des d\u00e9pistages.</li>\n<li>D\u00e9terminer un processus propre au lieu de travail ou au minist\u00e8re pour communiquer les r\u00e9sultats des d\u00e9pistages des employ\u00e9s lorsque cela est n\u00e9cessaire.</li>\n</ul>\n\n<h3>Gestionnaires</h3>\n<ul>\n<li>Communiquer avec les employ\u00e9s au sujet des exigences et des proc\u00e9dures en mati\u00e8re de d\u00e9pistage obligatoire, y compris le calendrier de d\u00e9pistage \u00e9tabli et le processus de rapport de d\u00e9pistage.</li>\n<li>Surveiller la conformit\u00e9 des employ\u00e9s par rapport \u00e0 tous les aspects du programme de d\u00e9pistage obligatoire.</li>\n</ul>\n\n<h3>Employ\u00e9s</h3>\n<ul>\n<li>Les employ\u00e9s qui doivent se pr\u00e9senter \u00e0 leur lieu de travail, m\u00eame si ce n'est qu'occasionnellement, et qui ne peuvent pas \u00eatre vaccin\u00e9s devront passer des tests de d\u00e9pistage selon les exigences de leur minist\u00e8re ou organisme et communiquer les r\u00e9sultats de ces tests au moins 3 fois par semaine (par exemple\u00a0: positive ou n\u00e9gative) conform\u00e9ment au protocole pour l'administration de tests de d\u00e9pistage de Sant\u00e9 Canada.</li>\n<li>Informer leur gestionnaire en cas de questions concernant les instructions sur le d\u00e9pistage ou leurs droits.</li>\n<li>En plus du d\u00e9pistage, respecter les directives locales de sant\u00e9 publique et toute autre mesure de pr\u00e9vention contre la COVID\u201119 requise sur le lieu de travail (par exemple\u00a0: masque, distanciation, etc.).</li>\n</ul>\n<h3>Services publics et Approvisionnement Canada</h3>\n<ul>\n<li>Fournir du soutien pour la mise en \u0153uvre du cadre de d\u00e9pistage, y compris l'approvisionnement des tests par le biais du Secr\u00e9tariat du d\u00e9pistage de Sant\u00e9 Canada, des installations et des logistiques.</li>\n</ul>\n<h2 id=\"a4\">R\u00e9f\u00e9rences</h2>\n<h3>Dispositions l\u00e9gislatives</h3>\n<ul>\n<li><a href=\"/francais/reg/jredirect2.shtml?clc\"><i>Code canadien du travail</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?geca\"><i>Loi sur l'indemnisation des agents de l'\u00c9tat</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?pa\"><i>Loi sur la protection des renseignements personnels</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?osh\"><i>R\u00e8glement canadien sur la sant\u00e9 et la s\u00e9curit\u00e9 au travail</i></a></li>\n<li><a href=\"/francais/reg/jredirect2.shtml?pr\"><i>R\u00e8glement sur la protection des renseignements personnels</i></a></li>\n</ul>\n<h3>Instruments de politique connexes</h3>\n<ul>\n<li><i><a href=\"/covid-19/les-dernieres-mises-a-jour-pour-les-employe-e-s-de/vaccinations-des-employes-de-l-acia/politique-de-l-agence-canadienne-d-inspection-des-/fra/1635536165716/1635536166388\">Politique sur la vaccination contre la COVID-19 pour l'Agence canadienne d'inspection des aliments</a></i></li>\n<li><a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=25049\"><i>Code de valeurs et d'\u00e9thique du secteur public</i></a></li>\n<li><a href=\"/a-propos-de-l-acia/possibilites-d-emploi/renseignements-sur-les-rh/code-de-conduite/fra/1341156915280/1341157034490\"><i>Code de conduite de l'ACIA</i></a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}