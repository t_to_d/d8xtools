<?php

$myargs = array_slice($argv, 1);
//$export_root;
$export_root = array_shift($myargs);
if (empty($export_root)) {
  echo "Please provide a path parameter to content processed json export folder after running updateContentURL.php.\n";
  echo "\n";
  echo "example: php retrieveFileContentFromAafcInternet.php /path/to/d8tools/convertTeamsiteIdToDrupal/export\n";
  echo "OR SIMPLY:    cd path/to/convertTeamsiteIdToDrupal; php retrieveFileContentFromAafcInternet.php export";
  echo "\n";
  echo "\n";
  echo "cd /path/to/d8tools/convertTeamsiteIdToDrupal;\n";
  echo "php convertTeamsiteIdToDrupal.php export";
  echo "\n";
  echo "\n(assuming export contains folders containing json files that were created by updateContentURL.php.";
  echo "\n";
  exit;
}
echo $export_root . "\n";

$count = 0;
$fileCount = 0;
$successCount = 0;
$failCount = 0;

global $failed_array;
$failed_array = array();

$url='https://agriculture.canada.ca/sites/default/files/legacy/special_title520x200.png';

$path = $export_root . "content/" . "special_title520x200.png";
if (file_put_contents( $path,file_get_contents($url))) {
  echo "$url for special title downloaded successfully";
  $successCount++;
}

$url='https://design.canada.ca/coded-layout/images/theme-topic-img-825x200.jpg';
$path = $export_root . "content/" . "special_title825x200.jpg";
if (file_put_contents( $path,file_get_contents($url))) {
  echo "$url for special title downloaded successfully";
  $successCount++;
}
$path = $export_root . "content/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_linkedin30x30_1647230511213_eng.png";
$url = "https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_linkedin30x30_1647230511213_eng.png";
if (file_put_contents( $path,file_get_contents($url))) {
  echo $url  . " downloaded successfully into $path";
  $successCount++;
}
$path = $export_root . "content/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_twitter30x30_1647230476448_eng.png";
$url = "https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_twitter30x30_1647230476448_eng.png";
if (file_put_contents( $path,file_get_contents($url))) {
  echo $url  . " downloaded successfully into $path";
  $successCount++;
}

$path = $export_root . "content/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_facebook30x30_1647230447415_eng.png";
$url = "https://inspection.canada.ca/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/inspect_protect_followus_facebook30x30_1647230447415_eng.png";
if (file_put_contents( $path,file_get_contents($url))) {
  echo $url  . " downloaded successfully into $path";
  $successCount++;
}


if ($dirh = opendir($export_root)) {
    while (($entry = readdir($dirh)) !== false) {
        if (! preg_match('/json$/', $entry))
            continue;

        $jfile = $entry;
        $data = json_decode(file_get_contents($export_root . '/' . $jfile));
        if (! empty($data->urlEn))
        foreach ($data->urlEn as $url) {
          echo $url . "\n";
          $count ++;
          retrieveAndSaveContent($url, $export_root, $data);
        }
        if (! empty($data->urlFr))
        foreach ($data->urlFr as $url) {
          echo $url . "\n";
          $count ++;
          retrieveAndSaveContent($url, $export_root, $data);
        }
    }
}
  echo "Total files : " . $count . "\n";
  echo "Successfully downloaded files : " . $successCount . "\n";
  echo "Failed downloaded files : " . $failCount . "\n";
  print_r($failed_array);

  function retrieveAndSaveContent($url, $export_root, $data){
    global $failed_array;
    if (startsWith($url,"/")) {
      if ($data->managing_branch == "DAIRY"){
        $url = "https://www.dairyinfo.gc.ca" . $url;
      }
      else { 
        $url = "https://inspection.canada.ca" . $url;
      }
      echo $url . "\n";
    };
    if(!startsWith( $url, "http" ) )
      return;
    global $successCount,$failCount;
    $parsedURL = parse_url ($url);
    echo $parsedURL['path'] . "\n";
    $path = str_replace("//", "/", $parsedURL['path']);
    $path = $export_root . "content" . $path;
    $path = urldecode($path);
    echo $path . "\n";
    $dir_to_save = dirname($path);
    echo $dir_to_save . "\n";
    if (!is_dir($dir_to_save)) {
      mkdir($dir_to_save, 0777, true);
    }
    //     1000000 = 1 second
    //     100000 = 100 milliseconds
    //     10000 = 10 milliseconds
    //     5000 = 5 milliseconds
    usleep(10000);
    if (file_put_contents($path,file_get_contents($url))) {
      // echo "File downloaded successfully";
      $successCount++;
    }
    else {
      usleep(100000);
      // Retry once after a short snooze.
      if (file_put_contents($path,file_get_contents($url))) {
        $successCount++;
      }
      else {
        // echo "File downloading failed.";
        $failed_array[] = $url;
        $failCount++;
      }
    }
    //file_get_contents($url);
  }

  function startsWith ($string, $startString)
{
    $len = strlen($startString);
    return (substr($string, 0, $len) === $startString);
}

?>
