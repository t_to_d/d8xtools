{
    "dcr_id": "1299873345668",
    "title": {
        "en": "Fertilizer or supplement registration",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment"
    },
    "html_modified": "2024-01-26 2:20:26 PM",
    "modified": "2022-08-09",
    "issued": "2015-04-09 10:52:17",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_fert_proc_1299873345668_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_fert_proc_1299873345668_fra"
    },
    "ia_id": "1299873426289",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Fertilizer or supplement registration",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment"
    },
    "label": {
        "en": "Fertilizer or supplement registration",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment"
    },
    "templatetype": "content page 1 column",
    "node_id": "1299873426289",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/fertilizer-or-supplement-registration/",
        "fr": "/protection-des-vegetaux/engrais/enregistrement-d-engrais-ou-de-supplement/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Fertilizer or supplement registration",
            "fr": "Enregistrement d'engrais ou de suppl\u00e9ment"
        },
        "description": {
            "en": "Registering fertilizers and supplements for sale in Canada.",
            "fr": "Enregistrement des engrais et des suppl\u00e9ments pour la vente au Canada."
        },
        "keywords": {
            "en": "plant protection, fertilizers, biotechnology, microbial supplements, novel supplements, supplements, registration, pre-market assessment, registration requirements",
            "fr": "protection des v\u00e9g\u00e9taux, engrais, biotechnologie, suppl\u00e9ments microbiens, suppl\u00e9ments nouveaux, suppl\u00e9ments, exigences pour l'enregistrement, enregistr\u00e9s, \u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9"
        },
        "dcterms.subject": {
            "en": "samples,fertilizers,plants,testing",
            "fr": "\u00e9chantillon,engrais,plante,test"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-09 10:52:17",
            "fr": "2015-04-09 10:52:17"
        },
        "modified": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Fertilizer or supplement registration",
        "fr": "Enregistrement d'engrais ou de suppl\u00e9ment"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"alert alert-info\">\n<p>The 3 year regulatory transition period (October 26, 2020 to October 26, 2023) has now ended. As a result, regulated parties, including all manufacturers, importers, distributors and sellers of fertilizers and supplements must adhere to the amended <a href=\"/english/reg/jredirect2.shtml?ferengr\"><i>Fertilizers Regulations</i></a>. There are few notable exceptions for some product categories. Learn more about the <a href=\"/plant-health/fertilizers/notices-to-industry/2023-05-03/eng/1682633127754/1682633128598\">implementation of the amended <i>Fertilizers Regulations</i></a>.</p>\n</div>\n\n<p>For more information on when and how to apply for fertilizer or supplement registration consult:</p>\n\n\n<div class=\"row wb-eqht mrgn-tp-lg mrgn-bttm-lg\">\n<div class=\"col-sm-6 hght-inhrt\">\n\n<p><a href=\"/plant-health/fertilizers/overview/eng/1646095692928/1646095693725\">Fertilizer or supplement registration</a> <span class=\"label label-default\">New</span><br>\n</p>\n\n</div>\n<div class=\"col-sm-6 hght-inhrt\">\n\n<p><a href=\"/eng/1646592107871/1646592108605\">Amending a fertilizer or supplement registration under the \"old\" <i>Fertilizers Regulations</i></a> <span class=\"label label-default\">Old</span><br>\n</p>\n\n</div>\n</div>\n\n\n<h2>Related links</h2>\n\n<ul>\n<li><a href=\"/plant-health/fertilizers/overview/fertilizers-and-supplements/eng/1330932243713/1330933201778\">Registration triggers for fertilizers and supplements</a></li>\n<li>\"<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498\">New regulations: guide to submitting applications for registration under the <i>Fertilizers Act</i></a>\"</li>\n\n\n<li><a href=\"/plant-health/fertilizers/overview/checklist/eng/1311313337062/1311313432185\">Submission checklists for completing your fertilizer or supplement registration application</a></li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"alert alert-info\">\n<p>La p\u00e9riode de transition r\u00e9glementaire de 3 ans (26 octobre 2020 au 26 octobre 2023) est termin\u00e9e. Par cons\u00e9quent, les parties r\u00e9glement\u00e9es, y compris tous les fabricants, importateurs, distributeurs et vendeurs d'engrais et de suppl\u00e9ments, doivent adh\u00e9rer au <a href=\"/francais/reg/jredirect2.shtml?ferengr\"><i>R\u00e8glement sur les engrais</i></a> modifi\u00e9. Il existe quelques exceptions notables pour certaines cat\u00e9gories de produits. Apprenez-en davantage sur la <a href=\"/protection-des-vegetaux/engrais/avis-a-l-industrie/2023-05-03/fra/1682633127754/1682633128598\">mise en \u0153uvre de la modification du <i>R\u00e8glement sur les engrais</i></a>.</p>\n</div>\n\n<p>Pour plus d'information sur quand et comment pr\u00e9senter une demande d'enregistrement d'engrais ou de suppl\u00e9ment consultez\u00a0:</p>\n\n<div class=\"row wb-eqht mrgn-tp-lg mrgn-bttm-lg\">\n<div class=\"col-sm-6 hght-inhrt\">\n\n<p><a href=\"/protection-des-vegetaux/engrais/apercu/fra/1646095692928/1646095693725\">Enregistrement d'engrais ou de suppl\u00e9ment</a> <span class=\"label label-default\">Nouveau</span><br>\n</p>\n\n</div>\n<div class=\"col-sm-6 hght-inhrt\">\n\n<p><a href=\"/fra/1646592107871/1646592108605\">Modification de l'enregistrement d'un engrais ou d'un suppl\u00e9ment en vertu de \u00ab\u00a0l'ancien\u00a0\u00bb <i>R\u00e8glement sur les engrais</i></a> <span class=\"label label-default\">Ancien</span><br>\n</p>\n\n</div>\n</div>\n\n<h2>Liens pertinents</h2>\n\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/engrais-et-les-supplements/fra/1330932243713/1330933201778\">D\u00e9clencheurs de l'enregistrement pour les engrais et les suppl\u00e9ments en vertu de la <i>Loi sur les engrais</i></a></li>\n\n<li>\u00ab\u00a0<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498\">Nouveau r\u00e8glement\u00a0: Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la <i>Loi sur les engrais</i></a>\u00a0\u00bb</li>\n\n\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/liste-de-controle/fra/1311313337062/1311313432185\">Liste de contr\u00f4le pour remplir votre demande d'enregistrement d'engrais ou de suppl\u00e9ments</a></li>\n</ul>\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}