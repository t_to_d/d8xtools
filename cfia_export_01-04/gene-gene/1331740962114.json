{
    "dcr_id": "1331740962114",
    "title": {
        "en": "Devil's-tail tearthumb - <i lang=\"la\">Persicaria perfoliata</i>",
        "fr": "Renou\u00e9e perfoli\u00e9e - <i lang=\"la\">Persicaria perfoliata</i>"
    },
    "html_modified": "2012-03-14 12:02",
    "modified": "9-8-2016",
    "issued": "14-3-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_persicaria_1331740962114_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_persicaria_1331740962114_fra"
    },
    "parent_ia_id": "1331614823132",
    "ia_id": "1331741252346",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Devil's-tail tearthumb - <i lang=\"la\">Persicaria perfoliata</i>",
        "fr": "Renou\u00e9e perfoli\u00e9e - <i lang=\"la\">Persicaria perfoliata</i>"
    },
    "label": {
        "en": "Devil's-tail tearthumb - <i lang=\"la\">Persicaria perfoliata</i>",
        "fr": "Renou\u00e9e perfoli\u00e9e - <i lang=\"la\">Persicaria perfoliata</i>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331741252346",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1331614724083",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/devil-s-tail-tearthumb/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/renouee-perfoliee/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Devil's-tail tearthumb - Persicaria perfoliata",
            "fr": "Renou\u00e9e perfoli\u00e9e - Persicaria perfoliata"
        },
        "description": {
            "en": "Devil?s-tail tearthumb is an invasive vine that forms tangled mats over other vegetation.",
            "fr": "La renou\u00e9e perfoli\u00e9e est une vigne envahissante qui recouvre d?autres v\u00e9g\u00e9taux d?un \u00e9pais tapis."
        },
        "keywords": {
            "en": "invasive plants, plant protection, Plant Protection Act, weed control, Devil?s-tail Tearthumb, persicaria perfoliata",
            "fr": "plantes envahissantes, protection des v\u00e9g\u00e9taux, Loi sur la protection des v\u00e9g\u00e9taux, contre les mauvaises herbes, Renou\u00e9e perfoli\u00e9e, persicaria perfoliata"
        },
        "dcterms.subject": {
            "en": "crops,environment,environmental protection,inspection,plants,weeds",
            "fr": "cultures,environnement,protection de l'environnement,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-14 12:02:44",
            "fr": "2012-03-14 12:02:44"
        },
        "modified": {
            "en": "2016-08-09",
            "fr": "2016-08-09"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Devil's-tail tearthumb - Persicaria perfoliata",
        "fr": "Renou\u00e9e perfoli\u00e9e - Persicaria perfoliata"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/plant-health/seeds/seed-testing-and-grading/seeds-identification/persicaria-perfoliata/eng/1397752966598/1397753000895\">Weed Seed - Devil's-tail tearthumb (<i lang=\"la\">Persicaria perfoliata</i>)</a></p>\n<p>Devil's-tail tearthumb is an invasive vine that forms tangled mats over other vegetation. It out-competes native plants, impoverishes wildlife habitat, restricts wildlife movement and reduces the aesthetic value of properties and public areas. This plant has a negative impact on industries that produce trees and shrubs.</p>\n<h2>Where it's found</h2>\n<p>Devil's-tail tearthumb was previously detected in British Columbia, but did not persist there. Native to eastern Asia, it is now highly invasive in the north-eastern United States. Habitats include riverbanks and a wide variety of disturbed areas, including roadsides, hedges, fields, pastures and forest edges, early forests, plantations, gardens and parks.</p>\n<h2>What it looks like</h2>\n<p>Devil's-tail tearthumb is a sprawling annual or perennial vine with thin, prickly stems. It has small white or pink flowers that give rise to metallic blue berries. Also distinctive are its triangular leaves and cup-shaped ocreae (leafy sheaths) surrounding its flowers and nodes. Backward-curved barbs are present on stems and leaves.</p>\n<h2>How it spreads</h2>\n<p>Devil's-tail tearthumb reproduces by seed. It is known for its remarkably rapid vegetative growth. People may unintentionally transport devil's-tail tearthumb with nursery stock. The seeds may be transported in root balls or the vines may be wound around stems. People may also transport the seeds in association with ornamental seed, hay, mulch, vehicles, equipment, clothing and baggage. Natural means of dispersal include water, ants, birds, small animals and deer.</p>\n<h2>Legislation</h2>\n<p>Devil's-tail tearthumb is regulated as a pest in Canada under the <i>Plant Protection Act</i>. It is also listed as a prohibited noxious weed on the <i>Weed Seeds Order</i>, 2016 under the <i>Seeds Act</i>. Importation and domestic movement of regulated plants and their propagative parts is prohibited.</p>\n<h2>What you can do about it</h2>\n<ul>\n<li>Leave natural items in their natural habitat.</li>\n<li>Use clean, high-quality seed that is certified if possible.</li>\n<li>Ensure machinery, vehicles and tools are free of soil and plants before moving them from one area to another.</li>\n<li>Contact your local Canadian Food Inspection Agency (CFIA) office if you suspect you have found this invasive plant. The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will follow up and determine if further action is needed.</li>\n</ul>\n<p>Learn more about <a href=\"/eng/1328325263410/1328325333845\">invasive species</a>.</p>\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Devil's-tail tearthumb weed fruits and recurved barbs\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_persicariaa_1331668905272_eng.jpg\"><figcaption>Devil's-tail tearthumb weed fruits and recurved barbs<br>Attribution: Todd Mervosh, Connecticut Agricultural Experiment Station</figcaption> </figure>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Devil's-tail tearthumb weed leaf\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_persicariab_1331669099784_eng.jpg\"><figcaption>Devil's-tail tearthumb weed leaf<br>Attribution: Todd Mervosh, Connecticut Agricultural Experiment Station</figcaption> </figure>\n\n<figure><img alt=\"Devil's-tail tearthumb\u00a0 weed foliage\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_persicariac_1331669146455_eng.jpg\"><figcaption>Devil's-tail tearthumb weed foliage<br> Attribution: L.J. Mehrhoff, University of Connecticut, Invasive.org (Bugwood.ca)</figcaption> </figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/persicaria-perfoliata/fra/1397752966598/1397753000895\">Semence de mauvaises herbe - Renou\u00e9e perfoli\u00e9e (<i lang=\"la\">Persicaria perfoliata</i>)</a></p>\n<p>La renou\u00e9e perfoli\u00e9e est une vigne envahissante qui recouvre d'autres v\u00e9g\u00e9taux d'un \u00e9pais tapis. Elle d\u00e9loge des plantes indig\u00e8nes, appauvrit l'habitat de la faune, restreint les mouvements des animaux sauvages et r\u00e9duit la valeur esth\u00e9tique des propri\u00e9t\u00e9s et des aires publiques. Cette plante nuit aux industries de la production d'arbres et d'arbustes.</p>\n<h2>O\u00f9 trouve-t-on cette esp\u00e8ce?</h2>\n<p>La renou\u00e9e perfoli\u00e9e a d\u00e9j\u00e0 \u00e9t\u00e9 observ\u00e9e en Colombie-Britannique, mais n'y a pas persist\u00e9. Originaire de l'est de l'Asie, cette plante est devenue tr\u00e8s envahissante dans le nord-est des \u00c9tats-Unis. Parmi ses habitats, notons les rives et une grande vari\u00e9t\u00e9 de zones perturb\u00e9es, dont le bord des routes, les haies, les champs, les p\u00e2turages et l'or\u00e9e des for\u00eats, les jeunes for\u00eats, les plantations, les jardins et les parcs.</p>\n<h2>\u00c0 quoi ressemble-t-elle?</h2>\n<p>La renou\u00e9e perfoli\u00e9e est une vigne annuelle ou vivace qui s'\u00e9tend, et ses hampes sont minces et \u00e9pineuses. Ses petites fleurs blanches ou roses font na\u00eetre de petits fruits d'un bleu m\u00e9tallique. La plante se distingue aussi \u00e0 ses feuilles triangulaires et \u00e0 l'ochr\u00e9a cupuliforme (gaine feuillue) qui entoure ses fleurs et ses n\u0153uds. Sa hampe et ses feuilles portent des barbes courb\u00e9es vers l'arri\u00e8re.</p>\n<h2>Comment se propage-t-elle?</h2>\n<p>Connue pour sa croissance v\u00e9g\u00e9tative incroyablement rapide, la renou\u00e9e perfoli\u00e9e se reproduit par germination. Les gens la transportent parfois par m\u00e9garde avec le mat\u00e9riel de p\u00e9pini\u00e8re. Les graines peuvent voyager dans des pelotes racinaires, ou les vignes s'enrouler autour des hampes. Il arrive aussi que les graines voyagent avec des semences de plantes ornementales, du foin, du paillis, des v\u00e9hicules, de l'\u00e9quipement, des v\u00eatements et des bagages. Elles sont naturellement dispers\u00e9es par l'eau, les fourmis, les oiseaux, les petits animaux et les chevreuils.</p>\n<h2>Lois</h2>\n<p>Renou\u00e9e perfoli\u00e9e est r\u00e8glement\u00e9 comme \u00e9tant des organismes nuisibles en vertu de la <i>Loi sur la protection des v\u00e9g\u00e9taux</i>. Elle figure aussi \u00e0 la liste des mauvaises herbes nuisibles interdites de l'<i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i>, qui s'inscrit dans la <i>Loi sur les semences</i>. L'importation et la circulation sur le territoire canadien de plantes r\u00e9glement\u00e9s et de leurs parties servant \u00e0 la multiplication sont interdites.</p>\n<h2>Que pouvez-vous faire?</h2>\n<ul>\n<li>Laissez les \u00e9l\u00e9ments naturels dans leur habitat naturel.</li>\n<li>Utilisez des semences propres, de haute qualit\u00e9 et si possible certifi\u00e9es.</li>\n<li>Assurez-vous que la machinerie, les v\u00e9hicules et les outils sont exempts de terre et de mat\u00e9riel v\u00e9g\u00e9tal avant de les d\u00e9placer d'un endroit \u00e0 l'autre.</li>\n<li>Communiquez avec votre bureau local de l'Agence canadienne d'inspection des aliments (ACIA) si vous croyez avoir aper\u00e7u cette plante envahissante. L'<abbr title=\"Agence canadienne d\u2019inspection des aliments\">ACIA</abbr> fera un suivi et d\u00e9terminera si d'autres mesures s'imposent.</li>\n</ul>\n<p>En savoir plus sur les <a href=\"/fra/1328325263410/1328325333845\">esp\u00e8ces envahissantes</a>.</p>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Fruits et barbes incurv\u00e9es de la renou\u00e9e perfoli\u00e9e\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_persicariaa_1331668905272_fra.jpg\"><figcaption>Fruits et barbes incurv\u00e9es de la renou\u00e9e perfoli\u00e9e<br>Source\u00a0: Todd Mervosh, <span lang=\"en\">Connecticut Agricultural Experiment Station</span></figcaption> </figure>\n\n<figure class=\"mrgn-bttm-lg\"><img alt=\"Feuille de la renou\u00e9e perfoli\u00e9e\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_persicariab_1331669099784_fra.jpg\"><figcaption>Feuille de la renou\u00e9e perfoli\u00e9e<br>Source\u00a0: Todd Mervosh, <span lang=\"en\">Connecticut Agricultural Experiment Station</span></figcaption> </figure>\n\n<figure><img alt=\"Devil\u2019s-tail tearthumb\u00a0 weed foliage\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_persicariac_1331669146455_fra.jpg\"><figcaption><span id=\"result_box\" lang=\"fr\">Feuillage des mauvaises herbes</span> de la renou\u00e9e perfoli\u00e9e <br><span lang=\"en\">Attribution\u00a0: L.J. Mehrhoff, University of Connecticut, Invasive.org</span> (Bugwood.ca)</figcaption> </figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}