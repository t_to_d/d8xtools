{
    "dcr_id": "1332280009004",
    "title": {
        "en": "Pathogen: <i lang=\"la\">Clostridium perfringens</i> - bacteria",
        "fr": "Pathog\u00e8ne : <i lang=\"la\">Clostridium perfringens</i> - bact\u00e9rie"
    },
    "html_modified": "2024-03-12 3:55:41 PM",
    "modified": "2012-03-20",
    "issued": "2012-03-20 17:52:33",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_causes_foodborne_perfringens_1332280009004_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/ctre_causes_foodborne_perfringens_1332280009004_fra"
    },
    "ia_id": "1332280082990",
    "parent_ia_id": "1331152055552",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1331152055552",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pathogen: <i lang=\"la\">Clostridium perfringens</i> - bacteria",
        "fr": "Pathog\u00e8ne : <i lang=\"la\">Clostridium perfringens</i> - bact\u00e9rie"
    },
    "label": {
        "en": "Pathogen: <i lang=\"la\">Clostridium perfringens</i> - bacteria",
        "fr": "Pathog\u00e8ne : <i lang=\"la\">Clostridium perfringens</i> - bact\u00e9rie"
    },
    "templatetype": "content page 1 column",
    "node_id": "1332280082990",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331151916451",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-safety-for-consumers/fact-sheets/food-poisoning/clostridium-perfringens/",
        "fr": "/salubrite-alimentaire-pour-les-consommateurs/fiches-de-renseignements/empoisonnements-alimentaires/clostridium-perfringens/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pathogen: Clostridium perfringens - bacteria",
            "fr": "Pathog\u00e8ne : Clostridium perfringens - bact\u00e9rie"
        },
        "description": {
            "en": "This document contains information on the bacteria Clostridium perfringens such as symptoms, how long they last, how you can get sick, foods commonly associated",
            "fr": "Ce document contient des informations sur la bact\u00e9rie Clostridium perfringens comme les sympt\u00f4mes, combien de temps ils dureront, comment vous pouvez tomber malade, d'aliments commun\u00e9ment associ\u00e9s"
        },
        "keywords": {
            "en": "pathogens, food safety, symptoms, health impacts, Clostridium perfringens, bacteria",
            "fr": "pathog\u00e8nes, salubrit\u00e9 des aliments, sympt\u00f4mes, effets sur la sant\u00e9, Clostridium perfringens, bact\u00e9rie"
        },
        "dcterms.subject": {
            "en": "inspection,food inspection,food safety,food processing",
            "fr": "inspection,inspection des aliments,salubrit\u00e9 des aliments, transformation des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Public Affairs",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Affaires publiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-20 17:52:33",
            "fr": "2012-03-20 17:52:33"
        },
        "modified": {
            "en": "2012-03-20",
            "fr": "2012-03-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pathogen: Clostridium perfringens - bacteria",
        "fr": "Pathog\u00e8ne : Clostridium perfringens - bact\u00e9rie"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Symptoms</h2>\n<p>Symptoms may include:</p>\n<ul>\n<li>abdominal bloating and increased gas</li>\n<li>fatigue</li>\n<li>loss of appetite and weight loss</li>\n<li>muscle ache</li>\n<li>nausea</li>\n<li>profuse, watery diarrhea</li>\n<li>severe abdominal pain and stomach cramps</li>\n</ul>\n<h2>Start of symptoms / how long they last</h2>\n<p>Symptoms of <i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i> may occur within 6 to 24 hours after eating contaminated food. The usual onset time is 10 to 12 hours.</p>\n<p>Most symptoms subside within 24 hours. However, some can last for up to two weeks.</p>\n<h2>Potential health impacts</h2>\n<p><i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i> are a spore-forming bacteria. They are found in soil, dust, sewage, and human and animal intestines. If consumed, these spores produce toxins (poison) in the intestinal tract, which can make you sick.</p>\n<h2>Food commonly associated</h2>\n<p>A wide variety of food can become contaminated with <i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i>. Typically this bacterium will grow in foods that are high in starch or high in protein, such as cooked beans, meat products, thick soups, and gravy. Leftovers that aren't cooled and reheated properly may contain a lot of the bacteria.</p>\n<p>The toxins are most commonly associated with foodborne illness, which can happen where food is</p>\n<ul>\n<li>made in large amounts, then allowed to cook slowly for several hours before consumption, and is</li>\n<li>allowed to cool slowly or is improperly refrigerated</li>\n</ul>\n<p>This pattern is common in cafeterias, hospitals, nursing homes and prisons.</p>\n<h2>How to protect yourself</h2>\n<ul>\n<li>Refrigerate all leftovers promptly in uncovered, shallow containers so they cool quickly.</li>\n<li>Very hot items can first be cooled at room temperature. Refrigerate once steaming stops.</li>\n<li>Leave the lid off or wrap loosely until the food is cooled to refrigeration temperature.</li>\n<li>Avoid overstocking the refrigerator to allow cool air to circulate freely.</li>\n</ul>\n<i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i> spores because some strains can survive at the boiling point (100\u00b0<abbr title=\"Celsius\">C</abbr> or 212\u00b0<abbr title=\"Fahrenheit\">F</abbr>) for up to an hour.\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sympt\u00f4mes</h2>\n<p>Parmi les sympt\u00f4mes, mentionnons les suivants :</p>\n<ul>\n<li>ballonnement et flatulences</li>\n<li>fatigue</li>\n<li>perte d'app\u00e9tit et de poids</li>\n<li>douleurs musculaires</li>\n<li>naus\u00e9e</li>\n<li>diarrh\u00e9e aqueuse et abondante</li>\n<li>douleurs et crampes abdominales importantes</li>\n</ul>\n<h2>Apparition et dur\u00e9e des sympt\u00f4mes</h2>\n<p>Les sympt\u00f4mes d'une infection \u00e0 <i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i> peuvent se manifester dans les 6 \u00e0 24 heures suivant l'ingestion d'aliments contamin\u00e9s, mais g\u00e9n\u00e9ralement ils apparaissent dans les 10 \u00e0 12 heures.</p>\n<p>La plupart des sympt\u00f4mes disparaissent apr\u00e8s 24 heures. Certains peuvent toutefois persister pendant deux semaines.</p>\n<h2>Effets possibles sur la sant\u00e9</h2>\n<p><i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i> est une bact\u00e9rie sporul\u00e9e. On la retrouve dans le sol, la poussi\u00e8re, les eaux d'\u00e9gout et les intestins des animaux et des humains. Une fois consomm\u00e9es, les spores produisent dans le tractus intestinal des toxines (poison) qui peuvent vous rendre malade.</p>\n<h2>Aliments g\u00e9n\u00e9ralement en cause</h2>\n<p>Divers produits alimentaires peuvent \u00eatre contamin\u00e9s par <i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i>. De fa\u00e7on g\u00e9n\u00e9rale, la bact\u00e9rie se d\u00e9veloppe dans des aliments \u00e0 forte teneur en amidon ou en prot\u00e9ines, comme les haricots cuits, les produits de viande, les potages li\u00e9s et les sauces. Les restes qui ne sont pas refroidis ou r\u00e9chauff\u00e9s ad\u00e9quatement peuvent contenir un grand nombre de bact\u00e9ries.</p>\n<p>Les toxines sont g\u00e9n\u00e9ralement li\u00e9es \u00e0 des toxi-infections alimentaires, qui peuvent survenir lorsque des aliments sont\u00a0:</p>\n<ul>\n<li>pr\u00e9par\u00e9s en grande quantit\u00e9 et laiss\u00e9s \u00e0 mijoter lentement pendant plusieurs heures avant d'\u00eatre consomm\u00e9s;</li>\n<li>laiss\u00e9s \u00e0 refroidir trop lentement ou insuffisamment r\u00e9frig\u00e9r\u00e9s.</li>\n</ul>\n<p>Cette situation est fr\u00e9quente dans les caf\u00e9t\u00e9rias, les h\u00f4pitaux, les centres de soins et les prisons.</p>\n<h2>Comment se prot\u00e9ger</h2>\n<ul>\n<li>R\u00e9frig\u00e9rez tous les restes sans tarder dans des contenants peu profonds non couverts pour qu'ils refroidissent rapidement.</li>\n<li>On peut laisser refroidir les aliments tr\u00e8s chauds \u00e0 la temp\u00e9rature ambiante. R\u00e9frig\u00e9rez-les d\u00e8s que la vapeur \u00e0 cess\u00e9 de s'en \u00e9chapper.</li>\n<li>Gardez le couvercle sur les aliments ou emballez-les l\u00e2chement jusqu'\u00e0 ce qu'ils soient compl\u00e8tement refroidis.</li>\n<li>\u00c9vitez de trop remplir le r\u00e9frig\u00e9rateur de fa\u00e7on \u00e0 ne pas g\u00eaner la circulation de l'air froid.</li>\n</ul>\n<i lang=\"la\"><abbr title=\"Clostridium\">C.</abbr> perfringens</i>, car certaines souches peuvent r\u00e9sister au point d'\u00e9bullition (100\u00a0\u00b0<abbr title=\"Celsius\">C</abbr> ou 212\u00a0\u00b0<abbr title=\"Fahrenheit\">F</abbr>) pendant jusqu'\u00e0 une heure.\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}