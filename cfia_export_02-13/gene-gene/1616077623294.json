{
    "dcr_id": "1616077623294",
    "title": {
        "en": "Notice to industry \u2013 Change to variety registration requirement for coloured beans",
        "fr": "Avis \u00e0 l'industrie \u2013 Modification de l'exigence d'enregistrement des vari\u00e9t\u00e9s de haricots de couleur"
    },
    "html_modified": "2024-02-13 9:55:03 AM",
    "modified": "2021-03-23",
    "issued": "2021-03-23",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/notice_change_to_variety_registration_20210318_1616077623294_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/notice_change_to_variety_registration_20210318_1616077623294_fra"
    },
    "ia_id": "1616077700791",
    "parent_ia_id": "1364064312276",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1364064312276",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice to industry \u2013 Change to variety registration requirement for coloured beans",
        "fr": "Avis \u00e0 l'industrie \u2013 Modification de l'exigence d'enregistrement des vari\u00e9t\u00e9s de haricots de couleur"
    },
    "label": {
        "en": "Notice to industry \u2013 Change to variety registration requirement for coloured beans",
        "fr": "Avis \u00e0 l'industrie \u2013 Modification de l'exigence d'enregistrement des vari\u00e9t\u00e9s de haricots de couleur"
    },
    "templatetype": "content page 1 column",
    "node_id": "1616077700791",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1364064277710",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-industry-advisories/2021-03-23/",
        "fr": "/protection-des-vegetaux/semences/avis-a-l-industrie-des-semences/2021-03-23/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice to industry \u2013 Change to variety registration requirement for coloured beans",
            "fr": "Avis \u00e0 l'industrie \u2013 Modification de l'exigence d'enregistrement des vari\u00e9t\u00e9s de haricots de couleur"
        },
        "description": {
            "en": "This notice is intended for members of the pulse crop industry who have an interest in the production, importation and sale of coloured beans (field beans, Phaseolus vulgaris) in Canada.",
            "fr": "Le pr\u00e9sent avis s'adresse aux membres de l'industrie des l\u00e9gumineuses qui ont un int\u00e9r\u00eat dans la production, l'importation et la vente de haricots de couleur (haricots de grande culture, Phaseolus vulgaris) au Canada."
        },
        "keywords": {
            "en": "notice, growers, seeds, variety form, requirement, coloured beans",
            "fr": "avis, producteurs, semences, vari\u00e9t\u00e9, enregistrement, haricots de couleur"
        },
        "dcterms.subject": {
            "en": "vegetable crops,labelling,inspection,plants",
            "fr": "cultures mara\u00eech\u00e8res,\u00e9tiquetage,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-03-23",
            "fr": "2021-03-23"
        },
        "modified": {
            "en": "2021-03-23",
            "fr": "2021-03-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice to industry \u2013 Change to variety registration requirement for coloured beans",
        "fr": "Avis \u00e0 l'industrie \u2013 Modification de l'exigence d'enregistrement des vari\u00e9t\u00e9s de haricots de couleur"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>March 23, 2021</p>\n<h2>Who this notice affects</h2>\n<p>This notice is intended for members of the pulse crop industry who have an interest in the production, importation and sale of coloured beans (field beans, <i lang=\"la\">Phaseolus vulgaris</i>) in Canada.</p>\n<h2>What is changing and when</h2>\n<p>Effective October 1, 2021, the Canadian Food Inspection Agency (CFIA) is removing a temporary exemption from <a href=\"/plant-varieties/variety-registration/eng/1299175847046/1299175906353\">variety registration</a> for coloured beans. This means that all field beans will revert back to being subject to variety registration prior to import and sale in Canada.</p>\n<p>Coloured beans include but are not limited to black beans, cranberry beans, pinto beans, red kidney beans, red Mexican beans, and yellow eye beans.</p>\n<p>The CFIA continues to work with the pulse industry to inform affected variety developers, field bean importers and other stakeholders of the impending changes so they can minimize any effects on their activities.</p>\n<p>Canada's variety registration system provides assurances that varieties grown in Canada will produce high-quality crops that are adapted to Canadian production systems and the Canadian growing environment.</p>\n<h2>Further information</h2>\n<p>For more information, please contact:</p>\n<p><a href=\"mailto:vro-bev@canada.ca\">Variety Registration Office</a><br>\n<br>\n<br>\n<br>\n<a href=\"mailto:VRO-BEV@inspection.gc.ca\">VRO-BEV@inspection.gc.ca</a></p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le 23 mars 2021</p>\n<h2>Personnes vis\u00e9es par le pr\u00e9sent avis</h2>\n<p>Le pr\u00e9sent avis s'adresse aux membres de l'industrie des l\u00e9gumineuses qui ont un int\u00e9r\u00eat dans la production, l'importation et la vente de haricots de couleur (haricots de grande culture, <i lang=\"la\">Phaseolus vulgaris</i>) au Canada.</p>\n<h2>Modification et entr\u00e9e en vigueur</h2>\n<p>\u00c0 compter du 1er octobre 2021, l'Agence canadienne d'inspection des aliments (ACIA) abroge l'exemption temporaire d'<a href=\"/varietes-vegetales/enregistrement-des-varietes/fra/1299175847046/1299175906353\">enregistrement des vari\u00e9t\u00e9s</a> visant les haricots de couleur. Cela signifie que tous les haricots de grande culture devront \u00e0 nouveau faire l'objet d'un enregistrement de vari\u00e9t\u00e9 avant leur importation et leur vente au Canada.</p>\n<p>Les haricots de couleur comprennent, entre autres, les haricots noirs, les haricots canneberges, les haricots Pinto, les haricots rouges, les haricots rouges mexicains et les haricots \u00e0 \u0153il jaune.</p>\n<p>L'ACIA continue de collaborer avec l'industrie des l\u00e9gumineuses pour informer les s\u00e9lectionneurs de vari\u00e9t\u00e9s concern\u00e9s, les importateurs de haricots de grande culture et les autres intervenants des modifications pr\u00e9vues afin qu'ils puissent en r\u00e9duire les effets sur leurs activit\u00e9s.</p>\n<p>Le syst\u00e8me canadien d'enregistrement des vari\u00e9t\u00e9s donne l'assurance que les vari\u00e9t\u00e9s cultiv\u00e9es au Canada produiront des cultures de haute qualit\u00e9, adapt\u00e9es aux syst\u00e8mes de production et au milieu de culture canadiens.</p>\n<h2>Renseignements</h2>\n<p>Pour de plus amples renseignements, veuillez communiquer avec le :</p>\n<p><a href=\"mailto:vro-bev@canada.ca\">Bureau d'enregistrement des vari\u00e9t\u00e9s</a><br>\n<br>\n<br>\n<br>\n<a href=\"mailto:VRO-BEV@inspection.gc.ca\">VRO-BEV@inspection.gc.ca</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}