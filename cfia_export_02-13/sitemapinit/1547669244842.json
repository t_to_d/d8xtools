{
    "dcr_id": "1547669199666",
    "title": {
        "en": "Audit of Emergency Management",
        "fr": "V\u00e9rification de la gestion des urgences"
    },
    "html_modified": "2024-02-13 9:53:36 AM",
    "modified": "2019-03-15",
    "issued": "2019-03-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/audit_of_emergency_management_1547669199666_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/audit_of_emergency_management_1547669199666_fra"
    },
    "ia_id": "1547669244842",
    "parent_ia_id": "1299843588592",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299843588592",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Audit of Emergency Management",
        "fr": "V\u00e9rification de la gestion des urgences"
    },
    "label": {
        "en": "Audit of Emergency Management",
        "fr": "V\u00e9rification de la gestion des urgences"
    },
    "templatetype": "content page 1 column",
    "node_id": "1547669244842",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299843498252",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/audit-of-emergency-management/",
        "fr": "/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/verification-de-la-gestion-des-urgences/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Audit of Emergency Management",
            "fr": "V\u00e9rification de la gestion des urgences"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency is responsible for the management of mandate-specific emergencies, that is, emergencies related directly to its mandate in Canada (food safety, animal or plant health). Within each of the agency's 3 business lines, events can escalate to emergencies with direct impacts on human and financial resources.",
            "fr": "L'ACIA est responsable de la gestion des urgences sp\u00e9cifiques \u00e0 son mandat, c'est-\u00e0-dire des urgences qui sont en lien direct avec son mandat au Canada\u00a0(salubrit\u00e9 des aliments et sant\u00e9 des animaux et des v\u00e9g\u00e9taux). Dans chacun des secteurs d'activit\u00e9 de l'agence, des incidents peuvent se transformer en urgences ayant des r\u00e9percussions directes sur les ressources humaines et financi\u00e8res."
        },
        "keywords": {
            "en": "Audit Review, policy, planning, implementation, Planning, Reporting &amp;amp; Accountability, audit reports, Audit of Emergency Management",
            "fr": "Examen de la v&#233;rification, politique, planification, mise en \u0153uvre, planification, reporting et Responsabilisation, rapports d&#39;audit, r&#233;sum&#233;, V\u00e9rification de la gestion des urgences"
        },
        "dcterms.subject": {
            "en": "government information,legislation,policy,audit",
            "fr": "information gouvernementale,l\u00e9gislation,politique,v\u00e9rification"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Audit and Evaluation",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,V\u00e9rification et \u00e9valuation"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-03-15",
            "fr": "2019-03-15"
        },
        "modified": {
            "en": "2019-03-15",
            "fr": "2019-03-15"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Audit of Emergency Management",
        "fr": "V\u00e9rification de la gestion des urgences"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section> \n<h2 class=\"mrgn-tp-sm\">Internal Audit</h2>\n\n<p>Canadian Food Inspection Agency (CFIA) internal audit is an independent, objective assurance and consulting function designed to add value and improve the Agency's operations. It helps the CFIA accomplish its objectives by bringing a systematic, disciplined approach to evaluate and improve the effectiveness of risk management, control, and governance processes.</p>\n\n<p class=\"mrgn-bttm-sm\">Internal audit engagements are authorized as part of the CFIA's risk-based audit plan, which is updated at least annually for approval by the President. Internal audits are carried out in accordance with the Treasury Board Policy on Internal Audit and the Institute of Internal Auditors' International Professional Practices Framework. Final internal audit reports are approved by the President on the recommendation of the CFIA audit committee.</p>\n</section>\n</div>\n\n<h2>Overview</h2>\n\n<p>The Canadian Food Inspection Agency is responsible for the management of mandate-specific emergencies, that is, emergencies related directly to its mandate in Canada (food safety, animal or plant health). Within each of the agency's 3 business lines, events can escalate to emergencies with direct impacts on human and financial resources.</p>\n<p>Planning for and responding appropriately to emergencies are key agency responsibilities. As such, robust emergency management in relation to the agency's mandate is critical to maintain its credibility and to minimize negative impacts on Canadians and the Canadian economy associated with a food, animal or plant related emergency. Since 2014, the CFIA has responded to a number of animal health-related emergencies as a result of Avian Influenza, Bovine Spongiform Encephalitis and Bovine Tuberculosis.</p>\n<p>The objective of the audit was to assess whether the management control framework for CFIA mandate-specific emergency management is designed adequately and operating effectively. The audit covered the period April 2016 to October 2017.</p>\n<h2>Key Findings</h2>\n\n<p>The audit noted that the CFIA's responses to mandate-specific emergencies are based on the Incident Command System, an internationally recognized standard system and best practice for emergency management. Nonetheless, improvements are required with respect to emergency management governance, roles, responsibilities, and accountabilities; preparedness through training and exercises; and the capability improvement process.</p>\n\n<h2>Complete Report</h2>\n\n<ul><li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/audit-of-emergency-management/audit-of-emergency-management/eng/1547668077649/1547668317057\">Audit of Emergency Management</a></li>\n<li><a href=\"/about-the-cfia/transparency/corporate-management-reporting/audits-reviews-and-evaluations/audit-of-emergency-management/audit-of-emergency-management/eng/1547668077649/1547668317057#appf\">Management Response and Action Plan</a></li></ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"well-sm brdr-tp brdr-rght brdr-bttm brdr-lft mrgn-bttm-md\">\n<section> \n<h2 class=\"mrgn-tp-sm\">V\u00e9rification interne</h2>\n\n<p>La  v\u00e9rification interne de l'Agence canadienne d'inspection des aliments (ACIA)  est une fonction d'assurance et de consultation ind\u00e9pendante et objective  visant \u00e0 ajouter de la valeur et \u00e0 am\u00e9liorer les activit\u00e9s de l'Agence. Elle  permet \u00e0 l'ACIA d'atteindre ses objectifs en ayant recours \u00e0 une approche  syst\u00e9matique et m\u00e9thodique pour \u00e9valuer et am\u00e9liorer l'efficacit\u00e9 des processus  de gestion des risques, de contr\u00f4le et de gouvernance.</p>\n\n<p class=\"mrgn-bttm-sm\">Les projets de v\u00e9rification interne sont autoris\u00e9s dans le cadre du Plan de v\u00e9rification interne bas\u00e9 sur les risques de l'ACIA, qui est mis \u00e0 jour au moins annuellement pour approbation par le pr\u00e9sident. Les v\u00e9rifications internes sont effectu\u00e9es conform\u00e9ment \u00e0 la Politique sur l'audit interne du Conseil tr\u00e9sor et au Cadre de r\u00e9f\u00e9rence international des pratiques professionnelles de l'Institut des auditeurs internes. Les rapports de v\u00e9rification interne finaux sont approuv\u00e9s par le pr\u00e9sident apr\u00e8s avoir \u00e9t\u00e9 recommand\u00e9 par le comit\u00e9 de v\u00e9rification.</p>\n</section>\n</div>\n\n<h2>Aper\u00e7u</h2>\n\n<p>L'ACIA est responsable de la gestion des urgences  sp\u00e9cifiques \u00e0 son mandat, c'est-\u00e0-dire des urgences qui sont en lien direct  avec son mandat au Canada\u00a0(salubrit\u00e9 des aliments et sant\u00e9 des animaux et  des v\u00e9g\u00e9taux). Dans chacun des secteurs d'activit\u00e9 de l'agence, des incidents  peuvent se transformer en urgences ayant des r\u00e9percussions directes sur les  ressources humaines et financi\u00e8res.</p>\n<p>La planification  et l'intervention appropri\u00e9e en cas d'urgence sont des responsabilit\u00e9s cl\u00e9s de  l'agence. \u00c0 ce titre, une gestion solide des urgences sp\u00e9cifiques au  mandat\u00a0 de l'agence est essentielle pour  pr\u00e9server sa cr\u00e9dibilit\u00e9 et minimiser les cons\u00e9quences n\u00e9gatives pour les  Canadiens ainsi que pour l'\u00e9conomie associ\u00e9e aux aliments, aux animaux et aux  v\u00e9g\u00e9taux touch\u00e9s par une urgence. Depuis\u00a02014, l'ACIA est intervenue dans  un certain nombre de situations d'urgence en sant\u00e9 des animaux en lien avec  l'influenza\u00a0aviaire, l'enc\u00e9phalite spongiforme bovine et la tuberculose  bovine.</p>\n<p>L'objectif de la v\u00e9rification \u00e9tait d'\u00e9valuer si le  cadre de contr\u00f4le de gestion\u00a0 des  urgences sp\u00e9cifiques au mandat de l'ACIA est con\u00e7u ad\u00e9quatement et fonctionne  efficacement. La v\u00e9rification a port\u00e9 sur la p\u00e9riode qui s'\u00e9chelonnait d'avril  2016 \u00e0 octobre 2017.</p>\n<h2>Principales constatations</h2>\n\n<p>La v\u00e9rification a permis de constater que les interventions de l'ACIA lors des urgences sp\u00e9cifiques \u00e0 son mandat sont fond\u00e9es sur le Syst\u00e8me de commandement d'intervention, un syst\u00e8me normalis\u00e9 reconnu \u00e0 l'\u00e9chelle internationale qui constitue une pratique exemplaire en gestion des urgences. N\u00e9anmoins, des am\u00e9liorations sont n\u00e9cessaires sur le plan de la gouvernance, des r\u00f4les, des responsabilit\u00e9s et de la reddition de comptes en mati\u00e8re de gestion des urgences; de la pr\u00e9paration par la formation et les exercices; et du processus d'am\u00e9lioration de la capacit\u00e9.</p>\n\n<h2>Rapport complet</h2>\n\n<ul><li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/verification-de-la-gestion-des-urgences/verification-de-la-gestion-des-urgences/fra/1547668077649/1547668317057\">V\u00e9rification de la gestion des urgences</a></li>\n<li><a href=\"/a-propos-de-l-acia/transparence/rapports-de-gestion/verifications-evaluations-et-revues/verification-de-la-gestion-des-urgences/verification-de-la-gestion-des-urgences/fra/1547668077649/1547668317057#appf\">R\u00e9ponse et plan d'action de la direction</a></li></ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}