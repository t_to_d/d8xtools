{
    "dcr_id": "1356066354790",
    "title": {
        "en": "Sheep genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique du mouton et tremblante"
    },
    "html_modified": "2024-01-26 2:21:30 PM",
    "modified": "2021-04-22",
    "issued": "2012-12-21 00:05:59",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_scrapie_genetics_1356066354790_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_scrapie_genetics_1356066354790_fra"
    },
    "ia_id": "1356066497693",
    "parent_ia_id": "1329723572482",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Diseases",
        "fr": "Maladies"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1329723572482",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Sheep genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique du mouton et tremblante"
    },
    "label": {
        "en": "Sheep genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique du mouton et tremblante"
    },
    "templatetype": "content page 1 column",
    "node_id": "1356066497693",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329723409732",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/scrapie/sheep-genetics-and-scrapie/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/genetique-du-mouton-et-tremblante/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Sheep genetics and scrapie",
            "fr": "G\u00e9n\u00e9tique du mouton et tremblante"
        },
        "description": {
            "en": "The genetic makeup of sheep has been determined to be a significant factor in their susceptibility to infection with classical scrapie. As a result, sheep genotyping is a disease control measure used in Canada's National Scrapie Eradication Program.",
            "fr": "Il a \u00e9t\u00e9 d\u00e9termin\u00e9 que la constitution g\u00e9n\u00e9tique du mouton pouvait grandement influencer sa sensibilit\u00e9 \u00e0 la tremblante classique. Il en r\u00e9sulte que le g\u00e9notypage du mouton est une mesure de lutte contre la maladie utilis\u00e9e par le Programme national d\u2019\u00e9radication de la tremblante du Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, scrapie, eradicate, eradication, surveillance, genotyping, genetics, selective breeding, genetic resistance, sheep",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire,maladie, tremblante, \u00e9radication, \u00e9radiquer, g\u00e9notypage, g\u00e9n\u00e9tique, \u00e9levage s\u00e9lectif, r\u00e9sistance g\u00e9n\u00e9tique, mouton"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-12-21 00:05:59",
            "fr": "2012-12-21 00:05:59"
        },
        "modified": {
            "en": "2021-04-22",
            "fr": "2021-04-22"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Sheep genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique du mouton et tremblante"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>The genetic makeup of sheep is a significant factor in their susceptibility to infection with classical scrapie. As a result, sheep genotyping is a disease control measure used in Canada's National Scrapie Eradication Program (NSEP). Testing for scrapie susceptibility in sheep is performed routinely at the Canadian Food Inspection Agency's (CFIA) National and World Organisation for Animal Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE)) reference laboratory for scrapie and chronic wasting disease (CWD) and at <a href=\"https://scrapiecanada.ca/scrapie-genotyping/genotyping-labs/\">other animal health laboratories in Canada and the United States.</a></p>\n\n<p>Recently, enough scientific information has become available on <a href=\"/animal-health/terrestrial-animals/diseases/reportable/scrapie/goat-genetics-and-scrapie/eng/1469836852107/1469836924249\">goat genetics</a> and scrapie susceptibility both internationally and in Canada, to allow the goat industry to consider using genetics as a risk management tool for scrapie. As a result, the CFIA will pilot genotyping goats for susceptibility to scrapie infection as a disease control measure in conjunction with the other tools currently available in Canada's NSEP.</p> \n<h2>Scrapie genotyping in sheep</h2>\n\n<p>A genotype is an individual's collection of genes. Like all mammals, sheep receive 1 allele for each gene from their dam (ewe) and 1 allele from their sire (ram). Alleles are the different versions of a gene. Scrapie genotyping refers to testing that reveals the specific alleles inherited for the animals' prion gene that makes an animal more or less susceptible to scrapie.</p> \n\n<p>The different alleles inherited for a sheep's prion gene determine which particular amino acids will be included at particular locations of the sheep's prion protein. Current scientific literature indicates that the presence of certain combinations of amino acids at 3 specific locations (known as codons) on the sheep's prion gene influence a sheep's relative susceptibility to scrapie.</p>\n<p>In North America codons at positions 136 and 171 are of primary importance in association with classical scrapie.</p>\n<ul>\n<li>Codon 136 codes for either the amino acid valine (V) or alanine (A).</li>\n<li>Codon 171 codes for the amino acid glutamine (Q) or arginine (R). </li>\n</ul>\n<p>1 common way to write genotypes for sheep is by the codon number followed by the corresponding amino acid: at 136 V for valine or A for alanine, at 171 R for arginine and Q for glutamine. The possible amino acid combinations at these 2 locations on the sheep prion gene and their impact on susceptibility to scrapie are listed below:</p>\n\n<h2>Susceptibility to classical scrapie based on genotype</h2>\n\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Table 1: Genotype and scrapie susceptibility in sheep.</caption>\n<thead>\n<tr>\n<th class=\"active\">Sheep's genotype (136, 171)</th> \n<th class=\"active\">Susceptibility to classical scrapie</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"alanine\">A</abbr> 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr></td>\n<td>Negligible</td>\n</tr>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"alanine\">A</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr></td>\n<td>Very low</td>\n</tr>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"valine\">V</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr></td>\n<td>Intermediate</td>\n</tr>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"alanine\">A</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr><br>\n<abbr title=\"alanine\">A</abbr><abbr title=\"valine\">V</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr><br>\n<abbr title=\"valine\">V</abbr><abbr title=\"valine\">V</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr></td>\n<td>High</td>\n</tr>\n</tbody>\n</table>\n\n<p>It is important to understand that scrapie genotyping is <strong>not</strong> disease testing. A 171QQ sheep does not automatically have scrapie, just as it is not an absolute guarantee that a 171RR sheep cannot get scrapie.</p>\n\n<ul><li>Scrapie genotyping is a tool used by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> during disease control actions. All mature exposed sheep in a scrapie infected flock are subject to a blood test to determine their susceptibility to scrapie infection. Typically, only the intermediate and highly susceptible sheep are ordered destroyed and this minimizes the number of sheep ordered destroyed on the scrapie infected premises.</li>\n<li>Scrapie genotyping is a tool that can be used by a producer in an overall plan to manage the risk of scrapie on their farm. Whether or not a particular producer should use scrapie genotyping is a decision based on individual factors.</li></ul>\n\n<h2>Who might consider selective breeding for genetic resistance to scrapie</h2>\n\n<ul>\n<li>A producer who provides a large number of breeding ewes to other producers</li>\n<li>A producer who purchases breeding ewes from multiple sources of unknown scrapie status</li>\n<li>A producer who has a significant number of 171 <abbr title=\"arginine\">RR</abbr> breeding animals in their flock, thus breeding for resistance would be easy, achieved relatively quick and would not have a significant impact on breeding for other production traits</li>\n</ul>\n\n<h3>Consider</h3>\n\n<p>A very effective way to breed for genetic resistance for scrapie is to select only rams that are 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> genotype. All lambs from 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> rams will inherit at least one <abbr title=\"arginine\">R</abbr> and will be more resistant to scrapie.</p>  \n\n<p>As well, recent studies related to the transmission of scrapie have shown that the genotype of the fetus influences the accumulation of the scrapie prion in the placenta of a scrapie infected ewe. A 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr> scrapie infected ewe, carrying a 171 <abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr> fetus (high susceptibility), results in the accumulation of large quantities of scrapie prion in the placenta, which is subsequently shed during birth. However, when a scrapie infected ewe carries a 171 <abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr> fetus (very low susceptibility), the scrapie prion does not cross the placenta and the shedding of scrapie prion is prevented at lambing. The use of a 171 <abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> ram for breeding, therefore, can prevent the spreading of infectious scrapie prion during lambing from infected ewes.</p>\n\n<h2>Who might not consider selective breeding for genetic resistance to scrapie</h2>\n\n<ul>\n<li>A producer with a flock that has been closed for a long time and has no evidence of scrapie.</li>\n<li>A producer that has a breed of sheep or a flock with few or no animals with a 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr> or 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> genotypes.</li>\n<li>A producer that does not want to deviate from their breeding plan for selection of other production traits.</li>\n</ul>\n<p>It is highly recommended that producers that chose not to selectively breed for scrapie resistance either close their ewe flock or purchase ewes from flocks in a <a href=\"https://scrapiecanada.ca/sfcp/\">scrapie flock certification program</a> and commence scrapie testing in mature deadstock.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>La constitution g\u00e9n\u00e9tique du mouton est un facteur significatif afin de d\u00e9terminer leur sensibilit\u00e9 \u00e0 l\u2019infection par la tremblante classique. Il en r\u00e9sulte que le g\u00e9notypage du mouton est une mesure de lutte contre la maladie utilis\u00e9e par le Programme national d'\u00e9radication de la tremblante du Canada (PNET). Des analyses visant \u00e0 d\u00e9terminer la sensibilit\u00e9 des moutons \u00e0 la tremblante sont r\u00e9guli\u00e8rement effectu\u00e9es par le laboratoire national de l'Agence canadienne d'inspection des aliments (ACIA) et de r\u00e9f\u00e9rence de l'Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE)) pour la tremblante et la maladie d\u00e9bilitante chronique (MDC), ainsi que par d'<a href=\"https://scrapiecanada.ca/fr/genetique-de-la-tremblante/information-sur-les-laboratoires/\">autres laboratoires de sant\u00e9 animale au Canada et aux \u00c9tats-Unis</a>.</p>\n\n<p>R\u00e9cemment, un nombre suffisant de renseignements scientifiques sont devenus disponibles concernant la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/genetique-caprine-et-tremblante/fra/1469836852107/1469836924249\">g\u00e9n\u00e9tique chez les ch\u00e8vres</a> et la sensibilit\u00e9 \u00e0 la tremblante, tant \u00e0 l'\u00e9chelle internationale qu'au Canada, permettant ainsi \u00e0 l'industrie caprine d'envisager de l'utiliser comme outil de gestion du risque pour la tremblante. Par cons\u00e9quent, l'ACIA conduira un projet pilote pour le g\u00e9notypage des ch\u00e8vres et la sensibilit\u00e9 \u00e0 l'infection par la tremblante en tant que mesure de lutte contre la maladie, et ce, parall\u00e8lement avec les autres outils actuellement disponibles dans le PNET du Canada.</p> \n\n<h2>G\u00e9notypage chez le mouton</h2>\n\n<p>Le g\u00e9notype est l'ensemble des g\u00e8nes d'un individu. Comme tous les animaux, le mouton re\u00e7oit un all\u00e8le de chacun des g\u00e8nes de sa m\u00e8re (brebis) et de son p\u00e8re (b\u00e9lier). Les all\u00e8les sont les diff\u00e9rentes versions d'un g\u00e8ne. Le g\u00e9notypage de la sensibilit\u00e9 \u00e0 la tremblante fait r\u00e9f\u00e9rence \u00e0 des analyses r\u00e9v\u00e9lant les all\u00e8les sp\u00e9cifiques h\u00e9rit\u00e9s du g\u00e8ne du prion des animaux qui rendent un animal plus ou moins susceptible de contracter la tremblante.</p> \n\n<p>Les diff\u00e9rents all\u00e8les h\u00e9rit\u00e9s pour un g\u00e8ne du prion d'un mouton d\u00e9terminent les acides amin\u00e9s particuliers qui seront inclus \u00e0 des emplacements pr\u00e9cis de la prot\u00e9ine prion du mouton. La litt\u00e9rature scientifique actuelle indique que la pr\u00e9sence de certaines combinaisons d'acides amin\u00e9s \u00e0 trois sites sp\u00e9cifiques (codons) du g\u00e8ne du prion du mouton a un effet sur la sensibilit\u00e9 relative de l'animal \u00e0 la tremblante. En Am\u00e9rique du Nord, les codons aux positions 136 et 171 rev\u00eatent une importance particuli\u00e8re pour ce qui est de la tremblante classique.</p>\n<ul>\n<li>Le codon 136 code l'acide amin\u00e9 valine (V) ou alanine (A).</li>\n<li>Le codon 171 code l'acide amin\u00e9 glutamine (Q) ou arginine (R).</li>\n</ul>\n<p>1 des fa\u00e7ons courantes d'exprimer par \u00e9crit le g\u00e9notype du mouton est d'indiquer le num\u00e9ro du codon, puis l'acide amin\u00e9 correspondant\u00a0: @136V pour valine ou A pour alanine, @ 171R pour arginine ou Q pour glutamine. Les combinaisons d'acides amin\u00e9s possibles \u00e0 ces deux endroits du g\u00e8ne du prion du mouton et leur effet sur la sensibilit\u00e9 \u00e0 la tremblante sont indiqu\u00e9s ci-dessous.</p>\n\n<h2>Sensibilit\u00e9 \u00e0 la tremblante classique d'apr\u00e8s le g\u00e9notype</h2>\n\n<table class=\"table table-bordered\">\n<caption class=\"text-left\">Tableau 1\u00a0: G\u00e9notype et susceptibilit\u00e9 \u00e0 la tremblante chez le mouton.</caption>\n<thead>\n<tr>\n<th class=\"active\">G\u00e9notype du mouton (136, 171)</th> \n<th class=\"active\">Sensibilit\u00e9 \u00e0 la tremblante classique</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"alanine\">A</abbr> 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr></td>\n<td>N\u00e9gligeable</td>\n</tr>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"alanine\">A</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr></td>\n<td>Tr\u00e8s faible</td>\n</tr>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"valine\">V</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr></td>\n<td>Interm\u00e9diaire</td>\n</tr>\n<tr>\n<td>136<abbr title=\"alanine\">A</abbr><abbr title=\"alanine\">A</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr><br>\n<abbr title=\"alanine\">A</abbr><abbr title=\"valine\">V</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr><br>\n<abbr title=\"valine\">V</abbr><abbr title=\"valine\">V</abbr> 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr></td>\n<td>\u00c9lev\u00e9e</td>\n</tr>\n</tbody>\n</table>\n\n<p>Il est important de comprendre que le g\u00e9notypage de la tremblante n'est pas un test de d\u00e9pistage de la maladie. Un mouton 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr> n'a pas automatiquement la tremblante, et il n'est pas absolument garanti qu'un mouton 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> ne puisse pas contracter la tremblante.</p>\n\n<ul><li>Le g\u00e9notypage de la sensibilit\u00e9 \u00e0 la tremblante est un outil utilis\u00e9 par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> lorsque des mesures de lutte contre la maladie sont prises. Tous les moutons adultes expos\u00e9s \u00e0 la tremblante au sein d'un troupeau infect\u00e9 sont soumis \u00e0 une analyse sanguine pour d\u00e9terminer leur sensibilit\u00e9 \u00e0 la tremblante. En g\u00e9n\u00e9ral, seuls les moutons dont la sensibilit\u00e9 est moyenne ou \u00e9lev\u00e9e font l'objet d'une ordonnance de destruction; cela permet de r\u00e9duire le nombre de moutons faisant l'objet d'une telle ordonnance dans les installations infect\u00e9es par la tremblante.</li>\n<li>Le g\u00e9notypage de la sensibilit\u00e9 \u00e0 la tremblante peut \u00eatre utilis\u00e9 par un producteur dans le cadre d'un plan de gestion globale du risque de tremblante dans son exploitation. La d\u00e9cision d'un \u00e9leveur d'avoir recours au g\u00e9notypage de la tremblante doit \u00eatre fond\u00e9e sur des facteurs individuels.</li>\n</ul>\n\n<h2>Qui aurait avantage \u00e0 envisager l'\u00e9levage s\u00e9lectif visant la r\u00e9sistance g\u00e9n\u00e9tique \u00e0 la tremblante</h2>\n\n<ul><li>Un \u00e9leveur qui fournit un grand nombre de brebis de reproduction \u00e0 d'autres \u00e9leveurs.</li>\n<li>Un \u00e9leveur qui ach\u00e8te des brebis de reproduction de diff\u00e9rentes sources dont le statut \u00e0 l'\u00e9gard de la tremblante est inconnu.</li>\n<li>Un \u00e9leveur ayant un nombre important d'animaux de reproduction 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> dans son troupeau, qui pourrait ainsi facilement proc\u00e9der \u00e0 un \u00e9levage s\u00e9lectif visant la r\u00e9sistance \u00e0 la tremblante, relativement rapidement, sans effet important sur la s\u00e9lection visant \u00e0 am\u00e9liorer d'autres caract\u00e8res de production.</li>\n</ul>\n\n<h3>\u00c9l\u00e9ments \u00e0 prendre en compte</h3>\n\n<p>Une des fa\u00e7ons les plus efficaces de proc\u00e9der \u00e0 un \u00e9levage s\u00e9lectif visant la r\u00e9sistance \u00e0 la tremblante est de ne s\u00e9lectionner que des b\u00e9liers de g\u00e9notype 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr>. Tous les moutons issus de b\u00e9liers de g\u00e9notype 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> h\u00e9riteront ainsi d'au moins un codon <abbr title=\"arginine\">R</abbr> et ils seront plus r\u00e9sistants \u00e0 la tremblante.</p> \n\n<p>En outre, des \u00e9tudes r\u00e9centes li\u00e9es \u00e0 la transmission de la tremblante ont montr\u00e9 que le g\u00e9notype du f\u0153tus influen\u00e7ait l'accumulation du prion de la tremblante dans le placenta d'une brebis infect\u00e9e par la tremblante. Chez une brebis 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr> infect\u00e9e par la tremblante portant un f\u0153tus 171 <abbr title=\"glutamine\">Q</abbr><abbr title=\"glutamine\">Q</abbr> (sensibilit\u00e9 \u00e9lev\u00e9e), il y a accumulation dans le placenta de grandes quantit\u00e9s de prions de la tremblante qui seront transmis \u00e0 la naissance. Cependant, lorsqu'une brebis infect\u00e9e porte un f\u0153tus 171 <abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr> (sensibilit\u00e9 tr\u00e8s faible), le prion de la tremblante ne traverse pas le placenta et la transmission du prion de la tremblante est emp\u00each\u00e9e \u00e0 la naissance. L'utilisation d'un b\u00e9lier 171 <abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr> pour la reproduction peut par cons\u00e9quent pr\u00e9venir la propagation du prion de tremblante infectieux \u00e0 la naissance lorsque la brebis est infect\u00e9e.</p>\n\n<h2>Qui n'aurait pas avantage \u00e0 envisager l'\u00e9levage s\u00e9lectif visant la r\u00e9sistance g\u00e9n\u00e9tique \u00e0 la tremblante</h2>\n\n<ul>\n<li>Un \u00e9leveur dont le troupeau est ferm\u00e9 depuis longtemps et qui ne pr\u00e9sente aucun signe de tremblante.</li>\n<li>Un \u00e9leveur dont la race de moutons ou le troupeau compte peu ou pas d'animaux de g\u00e9notype 171<abbr title=\"glutamine\">Q</abbr><abbr title=\"arginine\">R</abbr> ou 171<abbr title=\"arginine\">R</abbr><abbr title=\"arginine\">R</abbr>.</li>\n<li>Un \u00e9leveur qui ne souhaite pas s'\u00e9carter de son plan d'am\u00e9lioration g\u00e9n\u00e9tique pour la s\u00e9lection d'autres caract\u00e8res de production.</li>\n</ul>\n\n<p>On recommande fortement \u00e0 un \u00e9leveur qui choisit de ne pas proc\u00e9der \u00e0 un \u00e9levage s\u00e9lectif visant la r\u00e9sistance \u00e0 la tremblante de garder son troupeau ferm\u00e9 ou d'acheter des brebis de reproduction provenant de troupeaux qui sont inscrits \u00e0 un <a href=\"https://scrapiecanada.ca/fr/certification/\">programme de certification \u00e0 l'\u00e9gard de la tremblante</a> et de commencer \u00e0 faire des tests de d\u00e9pistage de la tremblante chez ses animaux adultes qui meurent.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}