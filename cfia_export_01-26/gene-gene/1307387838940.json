{
    "dcr_id": "1307387838940",
    "title": {
        "en": "Canada's role in international plant protection",
        "fr": "R\u00f4le du Canada sur la sc\u00e8ne internationale pour la protection des v\u00e9g\u00e9taux"
    },
    "html_modified": "2024-01-26 2:20:34 PM",
    "modified": "2023-10-20",
    "issued": "2015-04-13 13:43:49",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_export_intlstandards_1307387838940_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_export_intlstandards_1307387838940_fra"
    },
    "ia_id": "1307388138219",
    "parent_ia_id": "1299165527958",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165527958",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Canada's role in international plant protection",
        "fr": "R\u00f4le du Canada sur la sc\u00e8ne internationale pour la protection des v\u00e9g\u00e9taux"
    },
    "label": {
        "en": "Canada's role in international plant protection",
        "fr": "R\u00f4le du Canada sur la sc\u00e8ne internationale pour la protection des v\u00e9g\u00e9taux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1307388138219",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299164931224",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/exports/international-plant-protection/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportation-des-vegetaux/protection-des-vegetaux-au-niveau-international/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Canada's role in international plant protection",
            "fr": "R\u00f4le du Canada sur la sc\u00e8ne internationale pour la protection des v\u00e9g\u00e9taux"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency is involved with the development of regional standards for phytosanitary measures and international standards for phytosanitary measures.",
            "fr": "L'Agence canadienne d'inspection des aliments participe \u00e0 l'\u00e9laboration des normes r\u00e9gionales pour les mesures phytosanitaires et les normes internationales pour les mesures phytosanitaires."
        },
        "keywords": {
            "en": "Canada's role, international plant protection, North American Plant Protection Organization, NAPPO, IPPC, International standards for phytosanitary measures, imports, exports, plants",
            "fr": "R\u00f4le du Canada, la sc\u00e8ne internationale pour la protection des v\u00e9g\u00e9taux, Organisation nord-am\u00e9ricaine pour la protection des v\u00e9g\u00e9taux, NAPPO, CIPV, Normes internationales pour la protection des v\u00e9g\u00e9taux, exportation, importation, v\u00e9g\u00e9taux"
        },
        "dcterms.subject": {
            "en": "exports,imports,inspection,standards",
            "fr": "exportation,importation,inspection,norme"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-13 13:43:49",
            "fr": "2015-04-13 13:43:49"
        },
        "modified": {
            "en": "2023-10-20",
            "fr": "2023-10-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Canada's role in international plant protection",
        "fr": "R\u00f4le du Canada sur la sc\u00e8ne internationale pour la protection des v\u00e9g\u00e9taux"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">International Plant Protection Convention</a></li>\n<li><a href=\"#a2\">North American Plant Protection Organization</a></li>\n<li><a href=\"#a3\">Consultations</a></li>\n<li><a href=\"#a4\">Call for topics</a></li>\n<li><a href=\"#a5\">Learn more about the IPPC and NAPPO</a></li>\n</ul>\n\n<h2 id=\"a1\">International Plant Protection Convention</h2>\n\n<p>The Canadian Food Inspection Agency (CFIA) is Canada's representative under the <a href=\"https://www.ippc.int/en/\">International Plant Protection Convention</a> (IPPC). Canada is a contracting party to the IPPC, an international treaty that establishes a common approach to preventing the introduction and spread of plant pests, and promotes appropriate measures for their control.</p>\n\n<p>The work of the IPPC is governed by its Commission on Phytosanitary Measures, which adopts <a href=\"https://www.ippc.int/en/core-activities/standards-setting/\">international standards for phytosanitary measures</a>. The IPPC's Secretariat coordinates the activities of the IPPC and is hosted by the Food and Agriculture Organization of the United Nations.</p>\n\n<h2 id=\"a2\">North American Plant Protection Organization</h2>\n\n<p>Canada is a member of <a href=\"https://www.nappo.org/\">North American Plant Protection Organization</a> (NAPPO), a trilateral regional plant protection organization established under the IPPC. NAPPO's mission is to provide a forum for public and private sectors in Canada, the United States and Mexico to collaborate in developing science-based standards to protect agricultural, forest and other plant resources against regulated plant pests, while facilitating trade. This mission includes developing regional standards for phytosanitary measures.</p>\n\n<h2 id=\"a3\">Consultations</h2>\n\n<p>Each year, the IPPC and NAPPO conduct consultations with contracting parties. The CFIA is responsible for obtaining and submitting comments on behalf of Canada.</p>\n\n<p>Learn more about <a href=\"/about-cfia/transparency/consultations-and-engagement/eng/1566414558282/1566414728048\">Consultations and engagement</a> at the CFIA.</p>\n\n<h3>IPPC</h3>\n\n<p>Canada has the opportunity to review and comment on the following draft documents. Many of these consultations are conducted on an annual basis:</p>\n\n<ul>\n<li><a href=\"https://www.ippc.int/en/core-activities/standards-setting/member-consultation-draft-ispms/\">International standards for phytosanitary measures, diagnostic protocols and phytosanitary treatments</a> <span class=\"nowrap\">(July 1 to September 30)</span></li>\n<li><a href=\"https://www.ippc.int/en/core-activities/standards-setting/member-consultation-draft-specifications-ispms/\">Specifications for international standards for phytosanitary measures</a> <span class=\"nowrap\">(July 1 to September 30)</span></li>\n<li><a href=\"https://www.ippc.int/en/core-activities/capacity-development/consultation-on-ic-sub-groups-draft-terms-of-reference-and-draft-rules-of-procedure/\">Implementation and capacity development documents</a>: specifications for guides, training materials and documents <span class=\"nowrap\">(July 1 to August 31)</span></li>\n<li><a href=\"https://www.ippc.int/en/core-activities/governance/cpm/current-consultations-for-cpm-recommendations/\">Commission on Phytosanitary Measures recommendations <span class=\"nowrap\">(July 1 to September 30)</span></a></li>\n</ul>\n\n<p>To participate, send your comments to <a href=\"mailto:cfia.ippc.acia@inspection.gc.ca\" class=\"nowrap\">cfia.ippc.acia@inspection.gc.ca</a> (your IPPC contact point) at least 2 weeks before the deadlines. This will give the CFIA sufficient time to collate and interpret your comments.</p>\n\n<h3>NAPPO</h3>\n\n<p>NAPPO consultation documents can be found on the <a href=\"https://nappo.org/english/country-consultation-2023\">Country Consultation Section of the NAPPO website</a> in English and Spanish (the official languages for NAPPO).</p>\n \n<p>Please ensure you follow the instructions and provide your comments to the Canadian representatives listed for the documents before the end of the consultation period.</p>\n\n<h2 id=\"a4\">Call for topics</h2>\n\n<p>The development of standards and implementation resources by the IPPC is determined based on topics submitted by contracting parties or regional plant protection organizations and adopted by the Commission on Phytosanitary Measures. There is a call for topics every\u00a02 years.</p>\n\n<h3>How to submit a topic proposal</h3>\n\n<p>The call for topics is usually issued in early May and ends in the middle of September.</p>\n\n<p>To submit a topic proposal, send your submission package to <a href=\"mailto:cfia.ippc.acia@inspection.gc.ca\" class=\"nowrap\">cfia.ippc.acia@inspection.gc.ca</a> (your IPPC contact point) at least\u00a02\u00a0weeks before the deadline. They will help you review and refine your submission package.</p>\n\n<p>Find more information on the <a href=\"https://www.ippc.int/en/core-activities/standards-and-implementation/call-for-topics-standards-and-implementation/\">submission process for the call to topics</a>, including the information package required.</p>\n\n<h2 id=\"a5\">Learn more about the IPPC and NAPPO</h2>\n\n<ul>\n<li><a href=\"https://www.ippc.int/en/core-activities/standards-setting/ispms/\">International standards for phytosanitary measures</a></li>\n<li><a href=\"https://www.ippc.int/en/core-activities/governance/convention-text/\">International Plant Protection Convention</a></li>\n<li><a href=\"https://www.ippc.int/en/countries/\">IPPC contracting parties and contact points for each national plant protection organization</a> (NPPO)</li>\n<li><a href=\"https://www.pestalerts.org/\">NAPPO's phytosanitary alert system</a></li>\n<li><a href=\"https://www.nappo.org/english/products\">NAPPO's regional standards for phytosanitary measures</a></li>\n<li><a href=\"https://www.ippc.int/en/external-cooperation/regional-plant-protection-organizations/\">Regional plant protection organizations</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">La Convention internationale pour la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"#a2\">L'Organisation nord-am\u00e9ricaine pour la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"#a3\">Consultations</a></li>\n<li><a href=\"#a4\">Appel \u00e0 la proposition des th\u00e8mes</a></li>\n<li><a href=\"#a5\">Apprenez-en davantage sur la CIPV et la NAPPO</a></li>\n</ul>\n\n<h2 id=\"a1\">La Convention internationale pour la protection des v\u00e9g\u00e9taux</h2>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) est la repr\u00e9sentante du Canada aupr\u00e8s de la <a href=\"https://www.ippc.int/fr/\">Convention internationale pour la protection des v\u00e9g\u00e9taux</a> (CIPV). Le Canada est signataire de la CIPV, un trait\u00e9 international qui \u00e9tablit une approche commune visant \u00e0 pr\u00e9venir la propagation des phytoravageurs et \u00e0 promouvoir des mesures de contr\u00f4le ad\u00e9quates contre ces organismes.</p>\n\n<p>Les travaux de la CIPV sont dirig\u00e9s par sa <a href=\"https://www.ippc.int/fr/core-activities/standards-setting/\">Commission des mesures phytosanitaires</a>, qui est charg\u00e9e d'adopter les normes internationales de mesures phytosanitaires. Le Secr\u00e9tariat de la CIPV coordonne ces travaux \u00e0 partir de bureaux situ\u00e9s au si\u00e8ge de l'Organisation des Nations Unies pour l'alimentation et l'agriculture.</p>\n\n<h2 id=\"a2\">L'Organisation nord-am\u00e9ricaine pour la protection des v\u00e9g\u00e9taux</h2>\n\n<p>Le Canada est membre de l'<a href=\"https://www.nappo.org/\">Organisation nord-am\u00e9ricaine pour la protection des v\u00e9g\u00e9taux (NAPPO) (en anglais seulement)</a>, une organisation trilat\u00e9rale constituant une organisation r\u00e9gionale de la protection des v\u00e9g\u00e9taux, cr\u00e9\u00e9e sous la CIPV. La NAPPO a pour mission d'offrir un forum aux secteurs public et priv\u00e9 du Canada, des \u00c9tats-Unis et du Mexique permettant une collaboration en vue de l'\u00e9laboration de normes bas\u00e9es sur la science visant \u00e0 prot\u00e9ger les ressources agricoles, foresti\u00e8res et v\u00e9g\u00e9tales contre les phytoravageurs r\u00e9glement\u00e9s, tout en facilitant le commerce. Cette mission comprend l'\u00e9laboration de normes r\u00e9gionales pour les mesures phytosanitaires.</p>\n\n<h2 id=\"a3\">Consultations</h2>\n\n<p>Chaque ann\u00e9e, la CIPV et la NAPPO m\u00e8nent des consultations avec les signataires. L'ACIA est responsable de l'obtention et de la soumission des commentaires au nom des Canadiens.</p>\n\n<p>Apprenez-en davantage sur les <a href=\"/a-propos-de-l-acia/transparence/consultations-et-participation/fra/1566414558282/1566414728048\">Consultations et participation</a> \u00e0 l'ACIA.</p>\n\n<h3>La CIPV</h3>\n\n<p>Le Canada a la possibilit\u00e9 d'examiner les \u00e9bauches de documents suivants et de formuler des commentaires \u00e0 leur sujet. Plusieurs de ces consultations sont men\u00e9es sur une base annuelle\u00a0:</p>\n\n<ul>\n<li><a href=\"https://www.ippc.int/en/core-activities/standards-setting/member-consultation-draft-ispms/\">Normes internationales de mesures phytosanitaires, protocoles de diagnostic et traitements phytosanitaires (en anglais seulement)</a> <span class=\"nowrap\">(du 1er juillet au 30 septembre)</span></li>\n<li><a href=\"https://www.ippc.int/en/core-activities/standards-setting/member-consultation-draft-specifications-ispms/\">Sp\u00e9cifications pour les normes internationales de mesures phytosanitaires (en anglais seulement)</a> <span class=\"nowrap\">(du 1er juillet au 31 ao\u00fbt)</span></li>\n<li><a href=\"https://www.ippc.int/en/core-activities/capacity-development/consultation-on-ic-sub-groups-draft-terms-of-reference-and-draft-rules-of-procedure/\">Documents concernant la mise en \u0153uvre et le d\u00e9veloppement des capacit\u00e9s (en anglais seulement)</a>\u00a0: sp\u00e9cifications pour les guides, le mat\u00e9riel de formation et les documents <span class=\"nowrap\">(du 1er juillet au 31 ao\u00fbt)</span></li>\n<li><a href=\"https://www.ippc.int/fr/core-activities/governance/cpm/current-consultations-for-cpm-recommendations/\">Recommandations de la Commission des mesures phytosanitaires (anglais seulement)</a> <span class=\"nowrap\">(du 1er juillet au 30 septembre)</span></li>\n</ul>\n\n<p>Afin de participer, envoyez vos commentaires \u00e0 <a href=\"mailto:cfia.ippc.acia@inspection.gc.ca\" class=\"nowrap\">cfia.ippc.acia@inspection.gc.ca</a> (votre point de contact pour la CIPV) au moins 2 semaines avant la fin de la consultation. Ceci fournira suffisamment de temps \u00e0 l'ACIA pour rassembler et interpr\u00e9ter vos commentaires.</p>\n\n<p>L'ensemble des documents de la CIPV ne sont pas tous disponibles en fran\u00e7ais. Pour recevoir une copie des documents en fran\u00e7ais, veuillez communiquer avec votre point de contact pour la CIPV\u00a0: <a href=\"mailto:cfia.ippc.acia@inspection.gc.ca\" class=\"nowrap\">cfia.ippc.acia@inspection.gc.ca</a>.</p>\n\n<h3>La NAPPO</h3>\n\n<p>Les documents de consultation de la NAPPO sont disponibles en anglais et en espagnol (les langues officielles de la NAPPO) sur le <a href=\"https://nappo.org/english/country-consultation-2023\">site Internet de la NAPPO, dans la section \u00ab\u00a0Consultationdes pays\u00a0\u00bb</a>.</p>\n\n<p>Veuillez-vous assurer de suivre les instructions et de fournir vos commentaires aux repr\u00e9sentants canadiens r\u00e9pertori\u00e9s pour les documents avant la fin de la p\u00e9riode de consultation.</p>\n\n<h2 id=\"a4\">Appel \u00e0 la proposition des th\u00e8mes</h2>\n\n<p>Le d\u00e9veloppement de normes et de ressources de mise en \u0153uvre de la CIPV est d\u00e9termin\u00e9 par l'entremises les sujets soumis par les signataires ou les organisations r\u00e9gionales de la protection des v\u00e9g\u00e9taux et adopt\u00e9s par la Commission des mesures phytosanitaires. L'appel \u00e0 la proposition des th\u00e8mes a lieu tous les\u00a02 ans.</p>\n\n<h3>Comment soumettre une proposition de th\u00e8me</h3>\n\n<p>L'appel \u00e0 la proposition des th\u00e8mes est normalement \u00e9mise au d\u00e9but du mois de mai et se termine \u00e0 la mi-septembre.</p>\n\n<p>Pour soumettre une proposition de th\u00e8me, envoyez votre dossier de soumission \u00e0 <a href=\"mailto:cfia.ippc.acia@inspection.gc.ca\" class=\"nowrap\">cfia.ippc.acia@inspection.gc.ca</a> (votre point de contact pour la CIPV) au moins\u00a02\u00a0semaines avant la date limite. Ils vous aideront \u00e0 r\u00e9viser et affiner votre dossier de soumission.</p>\n\n<p>Apprenez-en davantage sur la <a href=\"https://www.ippc.int/en/core-activities/standards-and-implementation/call-for-topics-standards-and-implementation/\">proc\u00e9dure de soumission pour l'appel \u00e0 la proposition des th\u00e8mes (en anglais seulement)</a>, y compris le dossier d'information requis.</p>\n\n\n<h2 id=\"a5\">Apprenez-en davantage sur la CIPV et la NAPPO</h2>\n\n<ul>\n<li><a href=\"https://www.ippc.int/fr/core-activities/standards-setting/ispms/\">Normes internationales pour la protection des v\u00e9g\u00e9taux (en anglais seulement)</a></li>\n<li><a href=\"https://www.ippc.int/fr/core-activities/governance/convention-text/\">Convention internationale pour la protection des v\u00e9g\u00e9taux</a></li>\n<li><a href=\"https://www.ippc.int/fr/countries/\">Liste des signataires de la CIPV et leurs points de contact officiels</a></li>\n<li><a href=\"https://www.pestalerts.org/\">Syst\u00e8me d'alerte phytosanitaire de la NAPPO (en anglais seulement)</a></li>\n<li><a href=\"https://www.nappo.org/english/products\">Normes r\u00e9gionales pour les mesures phytosanitaires de la NAPPO (en anglais seulement)</a></li>\n<li><a href=\"https://www.ippc.int/fr/external-cooperation/regional-plant-protection-organizations/\">Organisations r\u00e9gionales de la protection des v\u00e9g\u00e9taux</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}