{
    "dcr_id": "1392497209673",
    "title": {
        "en": "Questions and Answers - Containment Standards for Facilities Handling Plant Pests",
        "fr": "Questions et r\u00e9ponses - Normes relatives au confinement des installations manipulant des phytoravageurs"
    },
    "html_modified": "2014-02-15 15:46",
    "modified": "1-4-2016",
    "issued": "15-2-2014",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/plan_protect_biocon_questions_1392497209673_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/plan_protect_biocon_questions_1392497209673_fra"
    },
    "parent_ia_id": "1391707686040",
    "ia_id": "1392497407493",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and Answers - Containment Standards for Facilities Handling Plant Pests",
        "fr": "Questions et r\u00e9ponses - Normes relatives au confinement des installations manipulant des phytoravageurs"
    },
    "label": {
        "en": "Questions and Answers - Containment Standards for Facilities Handling Plant Pests",
        "fr": "Questions et r\u00e9ponses - Normes relatives au confinement des installations manipulant des phytoravageurs"
    },
    "templatetype": "content page 1 column",
    "node_id": "1392497407493",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1391707650055",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/biocontainment/questions-and-answers/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/bioconfinement/questions-et-reponses/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and Answers - Containment Standards for Facilities Handling Plant Pests",
            "fr": "Questions et r\u00e9ponses - Normes relatives au confinement des installations manipulant des phytoravageurs"
        },
        "description": {
            "en": "The Containment Standards for Facilities Handling Plant Pests (CSFHPP) describe the minimum acceptable physical and operational requirements for facilities working with plant pests (other than weeds).",
            "fr": "Les Normes relatives au confinement des installations manipulant des phytoravageurs (NCIMP) d\u00e9crivent les exigences physiques et op\u00e9rationnelles minimales visant les installations qui manipulent des phytoravageurs (autres que les mauvaises herbes)."
        },
        "keywords": {
            "en": "Frequently Asked Questions, FAQ,  answers, containment, standards, facilities, plant, pests",
            "fr": "Foire aux questions, FAQ, r\u00e9ponses, confinement, normes, installations, plantes, phytoravageurs"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-02-15 15:46:51",
            "fr": "2014-02-15 15:46:51"
        },
        "modified": {
            "en": "2016-04-01",
            "fr": "2016-04-01"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and Answers - Containment Standards for Facilities Handling Plant Pests",
        "fr": "Questions et r\u00e9ponses - Normes relatives au confinement des installations manipulant des phytoravageurs"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=3&amp;gc=24&amp;ga=58#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>1. What is the purpose of the  Containment Standards for Facilities Handling Plant Pests?</h2>\n<p>The  Containment Standards for Facilities Handling Plant Pests (CSFHPP) describe the minimum acceptable physical and operational requirements for facilities working with plant pests (other than weeds). The <abbr title=\"Containment Standards for Facilities Handling Plant Pests\">CSFHPP</abbr> do not apply to soil, genetically modified plants, and biological control insects. Four containment levels are described, including: Basic, Plant Pest Containment (PPC) Level 1, <abbr title=\"Plant Pest Containment\">PPC</abbr>-2, and <abbr title=\"Plant Pest Containment\">PPC</abbr>-3.</p>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>2. Why does my facility need to meet the physical and operational requirements of the  Containment Standards for Facilities Handling Plant Pests?</h2>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, under the authority of the  Plant Protection Act, strives to protect plant life and the agricultural and forestry sectors of the Canadian economy by preventing the importation, exportation and spread of pests, and by controlling or eradicating pests in Canada. The  Plant Protection Act and  Regulations authorize the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to prohibit or restrict the movement into, within, and out of Canada of any plant pest or other thing that is or could be infested with a pest, or is or could be a biological obstacle to the control of a plant pest. This effort includes the need to contain and control plant pests that are being used for research purposes.</p>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>3. What is the process to obtain a plant pest import permit?</h2>\n<p>To obtain these import permits, an <a href=\"/eng/1328823628115/1328823702784#c5256\">Application for Permit to Import Plants and Other Things under the  Plant Protection Act (CFIA/ACIA\u00a05256)</a> form must be completed and submitted to <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Plant Health Import Permit Office. For further information please consult:</p>\n<ul>\n<li><a href=\"/plant-health/invasive-species/plant-import/eng/1324569244509/1324569331710\">Import procedures</a></li>\n<li><a href=\"/plant-health/invasive-species/plant-import/before-you-apply-for-a-permit/eng/1343414402266/1343414628852\">Questions and Answers</a></li>\n</ul>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>4. What is the process to obtain certification?</h2>\n<p>First contact form (Form A-PP) will be available and required to be completed along with the application to import. Form A-PP is used to gather contact information as well as program objectives. Once completed and returned to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>, Biosafety Specialists will review the documentation to determine whether a self-certification checklist or a formal certification submission and inspection is required for the certification of your facility. The certification process varies with the <abbr title=\"Plant Pest Containment\">PPC</abbr> level deemed necessary for the plant pest in question.</p>\n<p>Certification will be valid for a period of two years. If a facility is not granted certification, or certification is revoked for any reason, the deficiency or deficiencies must be corrected before the facility can be certified or re-certified.</p>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>5. How is the required Plant Pest Containment determined?</h2>\n<p>The containment requirements for a particular organism are frequently project-specific, and are determined by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> after assessing pest risk factors as are listed in the <a href=\"/plant-health/invasive-species/biocontainment/containment-standards/eng/1412353866032/1412354048442\">standard</a>.</p>\n\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>6. I am building a new facility handling plant pests, and would like to know when I should contact the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>?</h2>\n<p>In order to avoid potential problems and confusion, <a href=\"mailto:cfia.biocontainment-bioconfinement.acia@canada.ca\">please contact us</a> early in the planning phase. Working collaboratively with the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> will help ensure compliance with the appropriate requirements, and should reduce any delays in the certification of your facility.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=3&amp;gc=24&amp;ga=58#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>1. \u00c0 quoi servent les  Normes relatives au confinement des installations manipulant des phytoravageurs?</h2>\n<p>Les  Normes relatives au confinement des installations manipulant des phytoravageurs (NCIMP) d\u00e9crivent les exigences physiques et op\u00e9rationnelles minimales visant les installations qui manipulent des phytoravageurs (autres que les mauvaises herbes). Les <abbr title=\"normes relatives au confinement des installations manipulant des phytoravageurs\">NCIMP</abbr> ne s'appliquent pas au sol, aux plantes g\u00e9n\u00e9tiquement modifi\u00e9es et aux arthropodes servant d'agents de lutte biologique. Elles pr\u00e9sentent quatre niveaux de confinement: niveau de base, niveau de confinement 1 des phytoravageurs (PPC-1), <abbr title=\"confinement phytoravageurs\">PPC</abbr>-2 et <abbr title=\"confinement phytoravageurs\">PPC</abbr>-3.</p>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>2. Pourquoi mon installation doit-elle respecter les exigences physiques et op\u00e9rationnelles des  Normes relatives au confinement des installations manipulant des phytoravageurs?</h2>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, en vertu des pouvoirs que lui conf\u00e8re la  Loi sur la protection des v\u00e9g\u00e9taux, s'emploie \u00e0 prot\u00e9ger la vie v\u00e9g\u00e9tale et les secteurs agricole et forestier de l'\u00e9conomie canadienne en pr\u00e9venant l'importation, l'exportation et la propagation de ravageurs, et en contr\u00f4lant et \u00e9radiquant les ravageurs au Canada. La  Loi sur la protection des v\u00e9g\u00e9taux et son  r\u00e8glement d'application autorise l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d'interdire ou de restreindre le d\u00e9placement \u00e0 travers, \u00e0 l'int\u00e9rieur et \u00e0 l'ext\u00e9rieur du Canada, de tout phytoravageur ou de toute chose qui est ou pourrait \u00eatre infest\u00e9e par un ravageur, ou qui est ou pourrait \u00eatre un obstacle biologique au contr\u00f4le d'un phytoravageur. Le besoin de confiner et de contr\u00f4ler les phytoravageurs utilis\u00e9s pour la recherche s'inscrit dans le cadre de ces efforts.</p>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>3. Quelle est la marche \u00e0 suivre pour obtenir un permis d'importation de phytoravageurs?</h2>\n<p>Pour obtenir un permis d'importation, il faut compl\u00e9ter et pr\u00e9senter un formulaire de <a href=\"/fra/1328823628115/1328823702784#c5256\">Demande de permis pour importer des v\u00e9g\u00e9taux et d'autre choses en vertu de la  Loi sur la protection des v\u00e9g\u00e9taux (CFIA/ACIA\u00a05256)</a> au Bureau des permis d'importation de la Protection des v\u00e9g\u00e9taux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>. Pour obtenir de plus amples renseignements consultez :</p>\n<ul>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/fra/1324569244509/1324569331710\">Proc\u00e9dure d'importation</a></li>\n<li><a href=\"/protection-des-vegetaux/especes-envahissantes/importations-de-vegetaux/avant-de-demander-un-permis-pour-importer/fra/1343414402266/1343414628852\">Questions et r\u00e9ponses</a></li>\n</ul>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>4. Quelle est la marche \u00e0 suivre pour obtenir une accr\u00e9ditation?</h2>\n<p>Le formulaire du premier contact (le formulaire A-PP) sera disponible et il faudra remplir ce formulaire en plus de la demande d'importation. Le formulaire A-PP sert \u00e0 obtenir les coordonn\u00e9es ainsi que les objectifs du programme. Une fois rempli et renvoy\u00e9 \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, des sp\u00e9cialistes de la bios\u00e9curit\u00e9 examineront la documentation pour d\u00e9terminer si une liste de contr\u00f4le d'auto-accr\u00e9ditation ou une demande officielle d'accr\u00e9ditation et une inspection s'av\u00e8rent n\u00e9cessaire pour l'accr\u00e9ditation de l'installation. Le processus d'accr\u00e9ditation varie selon le niveau <abbr title=\"confinement phytoravageurs\">PPC</abbr> jug\u00e9 n\u00e9cessaire pour un phytoravageur donn\u00e9.</p>\n<p>L'accr\u00e9ditation sera valide pour une p\u00e9riode de deux ans. Si l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> refuse d'accr\u00e9diter une installation ou si une accr\u00e9ditation est r\u00e9voqu\u00e9e pour une raison quelconque, la lacune ou les lacunes devront \u00eatre combl\u00e9es pour que l'installation puisse \u00eatre accr\u00e9dit\u00e9e.</p>\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>5. Comment d\u00e9termine-t-on le niveau de confinement des phytoravageurs requis?</h2>\n<p>Les exigences associ\u00e9es au confinement d'un organisme particulier sont souvent propres \u00e0 un projet et sont d\u00e9termin\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> apr\u00e8s avoir \u00e9valu\u00e9 les facteurs de risque associ\u00e9s au ravageur en question, lesquels figurent dans les <a href=\"/protection-des-vegetaux/especes-envahissantes/bioconfinement/normes-sur-le-confinement/fra/1412353866032/1412354048442\">normes</a>.</p>\n\n<h2 class=\"h5\"><abbr title=\"Question\">Q</abbr>6. Je suis en train de faire construire une nouvelle installation qui manipulera des phytoravageurs. \u00c0 quel moment devrais-je communiquer avec l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>?</h2>\n<p>Dans le but d'\u00e9viter tout probl\u00e8me \u00e9ventuel et toute confusion, <a href=\"mailto:cfia.biocontainment-bioconfinement.acia@canada.ca\">veuillez communiquer avec nous</a> d\u00e8s l'\u00e9tape de la planification. De cette mani\u00e8re, vous aurez la certitude de respecter les exigences appropri\u00e9es et le fait de collaborer avec l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> devrait r\u00e9duire les d\u00e9lais associ\u00e9s \u00e0 l'accr\u00e9ditation de votre installation.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": false,
    "chat_wizard": false
}