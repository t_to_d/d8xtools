{
    "dcr_id": "1472604373914",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Abutilon theophrasti</i> (Velvetleaf)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Abutilon theophrasti</i> (Abutilon)"
    },
    "html_modified": "16-10-2017",
    "modified": "11-10-2017",
    "issued": "12-10-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_abutilon_theophrasti_1472604373914_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_abutilon_theophrasti_1472604373914_fra"
    },
    "parent_ia_id": "1333136685768",
    "ia_id": "1472604374428",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Abutilon theophrasti</i> (Velvetleaf)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Abutilon theophrasti</i> (Abutilon)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Abutilon theophrasti</i> (Velvetleaf)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Abutilon theophrasti</i> (Abutilon)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1472604374428",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/abutilon-theophrasti/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/abutilon-theophrasti/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Abutilon theophrasti (Velvetleaf)",
            "fr": "Semence de mauvaises herbe : Abutilon theophrasti (Abutilon)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Abutilon theophrasti, Malvaceae, Velvetleaf",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Abutilon theophrasti, Malvaceae, Abutilon"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-10-16",
            "fr": "2017-10-16"
        },
        "modified": {
            "en": "2017-10-11",
            "fr": "2017-10-11"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,general public,government,scientists",
            "fr": "entreprises,grand public,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Abutilon theophrasti (Velvetleaf)",
        "fr": "Semence de mauvaises herbe : Abutilon theophrasti (Abutilon)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Malvaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Velvetleaf</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in all provinces except <abbr title=\"Newfoundland and Labrador\">NL</abbr>, <abbr title=\"Northwest Territories\">NT</abbr>, <abbr title=\"Nunavut\">NU</abbr> and <abbr title=\"Yukon\">YT</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). Prevalent in southern <abbr title=\"Ontario\">ON</abbr> and <abbr title=\"Quebec\">QC</abbr> (Warwick and Black 1988<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to Asia (India or China) and introduced to Europe, northern Africa and North America (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 3.0 - 4.0 <abbr title=\"millimetres\">mm</abbr></li> \n<li>Seed width: 2.3 - 3.6 <abbr title=\"millimetres\">mm</abbr></li>\n\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Heart-shaped seed with unequal lobes and a conspicuous notch at hilum end</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed dull with minute reticulations and scattered tubercles, concentrated at the hilum</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed greyish brown with a lighter-coloured pubescence</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Flap-like tissue covering the hilum may be present</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, field margins and fence rows, gardens, roadsides, vacant lots and other waste places. A troublesome weed, especially in corn, soybean, sorghum and cotton fields  (Warwick and Black 1988<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Velvetleaf was introduced into North America in the mid-1700s as a potential fibre crop (Warwick and Black 1988<sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n<p>This annual species is adapted to be a weed of cultivated land and waste places, and movement of feed grain is believed to have spread it throughout eastern Canada (Warwick and Black 1988<sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Similar species</h2>\n<h3>Flower-of-an-hour (<i lang=\"la\">Hibiscus trionum</i>)</h3>\n<ul><li>Flower-of-an-hour seeds are a similar heart shape, brownish colour, minutely reticulate surface and tissue covering the hilum as velvetleaf.</li>\n<li>Flower-of-an-hour seeds (length: 2.5 <abbr title=\"millimetres\">mm</abbr>; width: 2.3 <abbr title=\"millimetres\">mm</abbr>) are generally smaller than velvetleaf, densely covered with tubercles made up of brown hairs and resin glands, the surface is dark brown, and the lobes of the seed are equal or subequal. Velvetleaf seeds have fewer tubercles, the surface is light greyish-brown and the lobes are unequal.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_abutilon_theophrasti_05cnsh_1476382840261_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Velvetleaf (<i lang=\"la\">Abutilon theophrasti</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_abutilon_theophrasti_01cnsh_1476382781946_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Velvetleaf (<i lang=\"la\">Abutilon theophrasti</i>) seed</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_abutilon_theophrasti_03cnsh_1476382814598_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Velvetleaf (<i lang=\"la\">Abutilon theophrasti</i>) seed</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_hibiscus_trionum_01cnsh_1476383529959_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Flower-of-an-hour (<i lang=\"la\">Hibiscus trionum</i>) seed</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_hibiscus_trionum_02cnsh_1476383555450_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Flower-of-an-hour (<i lang=\"la\">Hibiscus trionum</i>) seeds</figcaption>\n</figure>\n\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong>Warwick, S. I. and Black, L. D. 1988</strong>. The Biology of Canadian Weeds 90. <i lang=\"la\">Abutilon theophrasti</i>. Canadian Journal of Plant Science 68 (4): 1069-1085.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Malvaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Abutilon</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente dans toutes les provinces, sauf \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr>, et absente des territoires (<abbr title=\"Territoires du Nord-Ouest\">T.N-O.</abbr>, <abbr title=\"Nunavut\">Nt</abbr>, <abbr title=\"Yukon\">Yn</abbr>) (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup>). Commune dans le sud de l'<abbr title=\"Ontario\">Ont.</abbr> et du <abbr title=\"Qu\u00e9bec\">Qc</abbr> (Warwick et Black, 1988<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne d'Asie (Inde ou Chine) et introduite en Europe, en Afrique septentrionale  et en Am\u00e9rique du Nord (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 3,0 \u00e0 4,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li> \n<li>Largeur de la graine\u00a0: 2,3 \u00e0 3,6 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine cordiforme \u00e0 lobes in\u00e9gaux, avec une encoche bien apparente \u00e0 l'extr\u00e9mit\u00e9 du hile</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine \u00e0 surface mate, orn\u00e9e de fines r\u00e9ticulations et de tubercules \u00e9pars et concentr\u00e9s autour du hile</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine brun gris\u00e2tre, recouverte d'une pubescence plus p\u00e2le</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Membrane de tissu ressemblant \u00e0 un rabat parfois pr\u00e9sente sur le hile</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, bords de champs et haies, jardins, bords de chemin, terrains vacants et autres terrains vagues. Mauvaise herbe probl\u00e9matique, en particulier dans les champs de ma\u00efs, de soja, de sorgho et de coton (Warwick et Black, 1988<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>L'abutilon a \u00e9t\u00e9 introduit en Am\u00e9rique du Nord au milieu des ann\u00e9es 1700 pour son potentiel textile (Warwick et Black, 1988<sup id=\"fn2c-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n<p>Cette esp\u00e8ce annuelle s'est adapt\u00e9e pour devenir une mauvaise herbe des terres cultiv\u00e9es et des terrains vagues, et on croit que le transport des c\u00e9r\u00e9ales fourrag\u00e8res  l'aurait propag\u00e9e dans l'Est du Canada (Warwick et Black, 1988<sup id=\"fn2d-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Ketmie trilob\u00e9e (<i lang=\"la\">Hibiscus trionum</i>)</h3>\n<ul><li>Les graines de la ketmie trilob\u00e9e ressemblent \u00e0 celles de l'abutilon par leur contour cordiforme, leur couleur brun\u00e2tre, leur surface finement r\u00e9ticul\u00e9e et la pr\u00e9sence d'une membrane de tissu semblable \u00e0 un rabat qui recouvre le hile.</li>\n<li>Les graines de la ketmie trilob\u00e9e (longueur : 2,5 <abbr title=\"millim\u00e8tre\">mm</abbr>; largeur : 2,3 <abbr title=\"millim\u00e8tre\">mm</abbr>) sont g\u00e9n\u00e9ralement plus petites que celles de l'abutilon, dens\u00e9ment recouvertes de tubercules constitu\u00e9s de poils bruns et de glandes r\u00e9sinif\u00e8res, ont une surface brun fonc\u00e9, et les lobes de la graine sont \u00e9gaux ou sub\u00e9gaux. Les graines de l'abutilon ont moins de tubercules, ont une surface brun gris\u00e2tre p\u00e2le et leurs lobes sont in\u00e9gaux.</li>\n\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_abutilon_theophrasti_05cnsh_1476382840261_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Abutilon (<i lang=\"la\">Abutilon theophrasti</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_abutilon_theophrasti_01cnsh_1476382781946_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Abutilon (<i lang=\"la\">Abutilon theophrasti</i>) graine</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_abutilon_theophrasti_03cnsh_1476382814598_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Abutilon (<i lang=\"la\">Abutilon theophrasti</i>) graine</figcaption>\n</figure>\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_hibiscus_trionum_01cnsh_1476383529959_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Flower-of-an-hour (<i lang=\"la\">Hibiscus trionum</i>) graine</figcaption>\n</figure>\n\n<figure>\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_hibiscus_trionum_02cnsh_1476383555450_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Flower-of-an-hour (<i lang=\"la\">Hibiscus trionum</i>) graines</figcaption>\n</figure>\n\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>Warwick, S. I. and Black, L. D. 1988</strong>. The Biology of Canadian Weeds 90. <i lang=\"la\">Abutilon theophrasti</i>. Canadian Journal of Plant Science 68 (4): 1069-1085.</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page</span>3</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}