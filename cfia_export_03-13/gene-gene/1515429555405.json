{
    "dcr_id": "1515429555405",
    "title": {
        "en": "Pascal Lemire: Farmer",
        "fr": "Pascal Lemire: Agriculteur"
    },
    "html_modified": "2024-03-12 3:57:52 PM",
    "modified": "2018-01-17",
    "issued": "2018-01-17",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/videos_farmer_1515429555405_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/videos_farmer_1515429555405_fra"
    },
    "ia_id": "1515429555828",
    "parent_ia_id": "1514575461640",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1514575461640",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Pascal Lemire: Farmer",
        "fr": "Pascal Lemire: Agriculteur"
    },
    "label": {
        "en": "Pascal Lemire: Farmer",
        "fr": "Pascal Lemire: Agriculteur"
    },
    "templatetype": "content page 1 column",
    "node_id": "1515429555828",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1514575460841",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/traceability/videos-livestock-and-poultry-traceability-in-canad/pascal-lemire/",
        "fr": "/sante-des-animaux/animaux-terrestres/tracabilite/videos-la-tracabilite-du-betail-et-de-la-volaille-/pascal-lemire/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Pascal Lemire: Farmer",
            "fr": "Pascal Lemire: Agriculteur"
        },
        "description": {
            "en": "Pascal Lemire's family has been farming for over 300 years. His goal as a farmer is to produce safe, high quality food for Canadians. Pascal believes that traceability does more for farmers than just trace back for disease; He also sees it as a management tool.",
            "fr": "La famille de Pascal Lemire pratique l'agriculture depuis plus de 300 ans. Son objectif en tant qu'agriculteur est de produire des aliments sains de grande qualit\u00e9 pour les Canadiens. Pascal croit que la tra\u00e7abilit\u00e9 n'aide pas uniquement les agriculteurs \u00e0 retracer les cas de maladie en amont; selon lui, il s'agit \u00e9galement d'un outil de gestion."
        },
        "keywords": {
            "en": "traceability, livestock, animal, animal identification, Videos, Livestock and Poultry Traceability in Canada, Pascal Lemire, farmer",
            "fr": "tra\u00e7abilit\u00e9, b\u00e9tail, animaux, v\u00e9g\u00e9taux, identification des animaux, Vid\u00e9os, Pascal Lemire, Agriculteur"
        },
        "dcterms.subject": {
            "en": "animal health,government information,inspection,livestock,veterinary medicine",
            "fr": "sant\u00e9 animale,information gouvernementale,inspection,b\u00e9tail,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2018-01-17",
            "fr": "2018-01-17"
        },
        "modified": {
            "en": "2018-01-17",
            "fr": "2018-01-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Pascal Lemire: Farmer",
        "fr": "Pascal Lemire: Agriculteur"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Pascal Lemire's family has been farming for over 300 years. His goal as a farmer is to produce safe, high quality food for Canadians. Pascal believes that traceability does more for farmers than just trace back for disease; he also sees it as a management tool.</p>\n\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://www.youtube.com/watch?v=qPSJWMcWpKo&amp;feature=youtu.be\"}'><video width=\"300\" height=\"150\" title=\"Pascal Lemire: Farmer\">\n<source type=\"video/youtube\" src=\"https://www.youtube.com/watch?v=qPSJWMcWpKo&amp;feature=youtu.be\">\n</source></video>\n<figcaption>\n<details id=\"inline-transcript\">\n<summary>Pascal Lemire: Farmer\u00a0\u2013\u00a0Transcript</summary>\n\n<p><strong>(Light background music)</strong></p>\n\n<p><strong>(The Canadian Food Inspection Agency corporate introduction plays. It shows images that represent the work of the Agency, including a petri dish, strawberries, a growing plant, a chicken and a maple leaf.)</strong></p>\n\n<p><strong>(Text: <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> - Safeguarding with Science)</strong></p>\n\n<p><strong>(Image of farmer inside a barn)</strong></p>\n\n<p>Pascal Lemire: My family has been...</p>\n\n<p><strong>(Close up of Pascal inside barn)</strong></p>\n\n<p>...farming for over 300 years. We are proud of our agricultural heritage and...</p>\n\n<p><strong>(Medium shot of Pascal inside barn with cows in background)</strong></p>\n\n<p>...producing safe, high quality food for Canadians.</p> \n\n<p><strong>(Text on screen: Pascal Lemire)</strong></p>\n\n<p><strong>(Close up of Pascal inside barn with cows in background)</strong></p>\n\n<p>That's why I feel traceability is so important.</p>\n\n<p><strong>(Close up of Pascal inside barn with cows in background)</strong></p>\n\n<p>Traceability is about identifying animals...</p>\n\n<p><strong>(Pascal in barn with cow)</strong></p>\n\n<p><strong>(Close up of cow inside barn)</strong></p>\n\n<p>...their location and where they move.</p>\n\n<p><strong>(Pascal in barn with cow)</strong></p>\n\n<p>As farmers, we don't just use traceability to trace back for disease; it's a management tool.</p> \n\n<p><strong>(Pascal inserting traceability tag on cow)</strong></p>\n\n<p>It helps us better manage our animals and...</p>\n\n<p><strong>(Close up of tag on cow's ear)</strong></p>\n\n<p>...bring more value to the market.</p>  \n\n<p><strong>(Close up of Pascal inside barn with cows in background)</strong></p>\n\n<p>We are working together, with all industry partners and with governments, federal and provincial in Canada...</p>\n\n<p><strong>(Close up baby chicks)</strong></p>\n\n<p>...to build this livestock and poultry...</p>\n\n<p><strong>(Close up of bison in field)</strong></p>\n\n<p>...traceability system.</p>\n\n<p><strong>(Close up of cow in field)</strong></p>\n\n<p><strong>(Medium shot of cows in field)</strong></p>\n\n<p><strong>(Medium shot of sheep interior barn)</strong></p>  \n\n<p>I'm Pascal Lemire and...</p>\n\n<p><strong>(Close up of Pascal inside barn with cows in background)</strong></p>\n\n<p>...traceability is key to the future of Canadian Agriculture.</p> \n\n<p><strong>(Medium shot, Pascal petting cow inside barn)</strong></p>\n\n<p><strong>(Cut to Canada wordmark.)</strong></p>\n\n<p><strong>(Text on screen:  Canada.  (c) Her Majesty the Queen in Right of Canada, represented by Canadian Food Inspection Agency (2017))</strong></p>\n\n</details>\n</figcaption>\n</figure>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La famille de Pascal Lemire pratique l'agriculture depuis plus de 300 ans. Son objectif en tant qu'agriculteur est de produire des aliments sains de grande qualit\u00e9 pour les Canadiens. Pascal croit que la tra\u00e7abilit\u00e9 n'aide pas uniquement les agriculteurs \u00e0 retracer les cas de maladie en amont; selon lui, il s'agit \u00e9galement d'un outil de gestion.</p>\n\n<figure class=\"wb-mltmd\" data-wb-mltmd='{\"shareUrl\": \"https://www.youtube.com/watch?v=pKH3TQZDT10&amp;feature=youtu.be\"}'><video width=\"300\" height=\"150\" title=\"Pascal Lemire: Agriculteur\u00a0: \u00c9leveur de moutons\">\n<source type=\"video/youtube\" src=\"https://www.youtube.com/watch?v=pKH3TQZDT10&amp;feature=youtu.be\">\n</source></video>\n<figcaption>\n<details id=\"inline-transcript\">\n<summary>Pascal Lemire\u00a0: Agriculteur\u00a0\u2013\u00a0Transcription</summary>\n\n<p><strong>(Musique de fond)</strong></p>\n\n<p><strong>(Mise en contexte de l'Agence canadienne d'inspection des aliments. Le travail de l'Agence est pr\u00e9sent\u00e9 en images, notamment une bo\u00eete de P\u00e9tri, des fraises, un plant en croissance, un poulet et une feuille d'\u00e9rable.)</strong></p>\n\n<p><strong>(Texte\u00a0: <abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> \u2013 Pr\u00e9server gr\u00e2ce \u00e0 la science)</strong></p>\n\n<p><strong>(Plan moyen d'un fermier \u00e0 l'int\u00e9rieur d'une \u00e9table)</strong></p>\n\n<p>Pascal Lemire: Ma famille s'adonne...</p>\n\n<p><strong>(Plan rapproch\u00e9 de Pascal \u00e0 l'int\u00e9rieur d'une \u00e9table)</strong></p>\n\n<p>...\u00e0 l'agriculture depuis plus de 300 ans. Nous sommes fiers de notre patrimoine agricole et...</p>\n\n<p><strong>(Plan moyen de Pascal \u00e0 l'int\u00e9rieur d'une \u00e9table avec des vaches au second plan)</strong></p>\n\n<p>...des aliments sains de qualit\u00e9 sup\u00e9rieure que nous produisons pour les Canadiens.</p>\n\n<p><strong>(Texte \u00e0 l'\u00e9cran\u00a0: Pascal Lemire)</strong></p>\n\n<p><strong>(Plan rapproch\u00e9 de Pascal \u00e0 l'int\u00e9rieur d'une \u00e9table avec des vaches au second plan)</strong></p>\n\n<p>Voil\u00e0 pourquoi j'estime que la tra\u00e7abilit\u00e9 est si importante.</p>\n\n<p><strong>(Plan rapproch\u00e9 d'une vache dans une \u00e9table)</strong></p>\n\n<p><strong>(Plan rapproch\u00e9 de Pascal \u00e0 l'int\u00e9rieur d'une \u00e9table avec des vaches au second plan)</strong></p>\n\n<p>La tra\u00e7abilit\u00e9 consiste \u00e0 identifier les animaux...</p>\n\n<p><strong>(Plan rapproch\u00e9 d'une vache dans une \u00e9table)</strong></p>\n\n<p>...leur emplacement et leurs d\u00e9placements.</p>\n\n<p><strong>(Pascal dans une \u00e9table avec des vaches)</strong></p>\n\n<p>Les agriculteurs utilisent la tra\u00e7abilit\u00e9 comme outil de gestion \u00e0 la ferme.</p>  \n\n<p><strong>(Pascal installant une \u00e9tiquette d'oreille \u00e0 un veau)</strong></p>\n\n<p>Elle nous aide \u00e0 mieux g\u00e9rer nos animaux et...</p>\n\n<p><strong>(Plan rapproch\u00e9 d'une \u00e9tiquette d'oreille sur une vache)</strong></p>\n\n<p>...ajoute de la valeur aux produits que nous commercialisons.</p>\n\n<p><strong>(Plan rapproch\u00e9 de Pascal \u00e0 l'int\u00e9rieur d'une \u00e9table avec des vaches au second plan)</strong></p>\n\n<p>Nous travaillons ensemble, les partenaires de l'industrie, avec tous les gouvernements au Canada...</p>\n\n<p><strong>(Plan rapproch\u00e9 de poussins)</strong></p>\n\n<p>...pour mettre sur pied notre syst\u00e8me de tra\u00e7abilit\u00e9...</p>\n\n<p><strong>(Plan rapproch\u00e9 de bison dans les champs)</strong></p>\n\n<p>...du b\u00e9tail et de la volaille.</p>\n\n<p><strong>(Plan rapproch\u00e9 d'une vache dans un champs)</strong></p>\n\n<p><strong>(Plan moyen de vaches dans un champs)</strong></p>\n\n<p><strong>(Plan moyen d'un mouton dans une \u00e9table)</strong></p>  \n\n<p>Je m'appelle Pascal Lemire...</p>\n\n<p><strong>(Plan rapproch\u00e9 de Pascal \u00e0 l'int\u00e9rieur d'une \u00e9table avec des vaches au second plan)</strong></p>\n\n<p>...et pour moi, la tra\u00e7abilit\u00e9 est essentiel au futur de notre agriculture canadienne.</p>\n\n<p><strong>(Plan moyen de Pascal carressant une vache dans une \u00e9table)</strong></p>\n\n<p><strong>(Mot-symbole \u00ab\u00a0Canada\u00a0\u00bb. Droit d'auteur Sa Majest\u00e9 la Reine du chef du Canada (Agence canadienne d'inspection des aliments), 2017.)</strong></p>\n\n</details>\n</figcaption>\n</figure>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}