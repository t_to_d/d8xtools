{
    "dcr_id": "1652284851276",
    "title": {
        "en": "Notice to industry\u00a0\u2013 Food misrepresentation",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013 La repr\u00e9sentation trompeuse des alimentaires"
    },
    "html_modified": "2024-02-13 9:55:48 AM",
    "modified": "2022-05-12",
    "issued": "2022-05-12",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/industry_notice_food_misrepresentation_1652284851276_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/industry_notice_food_misrepresentation_1652284851276_fra"
    },
    "ia_id": "1652284851588",
    "parent_ia_id": "1548444784124",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1548444784124",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Notice to industry\u00a0\u2013 Food misrepresentation",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013 La repr\u00e9sentation trompeuse des alimentaires"
    },
    "label": {
        "en": "Notice to industry\u00a0\u2013 Food misrepresentation",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013 La repr\u00e9sentation trompeuse des alimentaires"
    },
    "templatetype": "content page 1 column",
    "node_id": "1652284851588",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1548444760712",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/food-labels/food-fraud/cfia-s-role/notice-to-industry/",
        "fr": "/etiquetage-des-aliments/fraude-alimentaire/role-de-l-acia/avis-a-l-industrie/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Notice to industry\u00a0\u2013 Food misrepresentation",
            "fr": "Avis \u00e0 l'industrie\u00a0\u2013 La repr\u00e9sentation trompeuse des alimentaires"
        },
        "description": {
            "en": "Canadian laws prohibit the misrepresentation of food. Mislabeling, adulteration and substitution of food are forms of misrepresentation that may constitute food fraud.",
            "fr": "Les lois canadiennes interdisent la repr\u00e9sentation trompeuse des aliments. L'\u00e9tiquetage trompeuse, la falsification, et la substitution d'aliments sont des formes de repr\u00e9sentation trompeuse qui peuvent constituer une fraude alimentaire."
        },
        "keywords": {
            "en": "Food and Drugs Act, Food and Drugs Regulations, imports, exports, Food fraud, CFIA's role, combatting food fraud, Food misrepresentation",
            "fr": "Loi sur les aliments et drogues, R\u00e8glement sur les aliments et drogues,  importation, exportation, fraude alimentaire, R\u00f4le de l'ACIA, la lutte contre la fraude alimentaire, La repr\u00e9sentation trompeuse des alimentaires"
        },
        "dcterms.subject": {
            "en": "food,food labelling,food safety,food security",
            "fr": "aliment,\u00e9tiquetage des aliments,salubrit\u00e9 des aliments,s\u00e9curit\u00e9 alimentaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-05-12",
            "fr": "2022-05-12"
        },
        "modified": {
            "en": "2022-05-12",
            "fr": "2022-05-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Notice to industry\u00a0\u2013 Food misrepresentation",
        "fr": "Avis \u00e0 l'industrie\u00a0\u2013 La repr\u00e9sentation trompeuse des alimentaires"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<p>Canadian laws prohibit the misrepresentation of food. Mislabeling, adulteration and substitution of food are forms of misrepresentation that may constitute food fraud.</p>\n\n<p>Food fraud deceives consumers and food businesses. It also creates an unfair marketplace for businesses that follow food labelling rules. In some cases, food misrepresentation can even affect the health of consumers, such as in the case of undeclared allergens or additives, or inaccurate nutrition claims.</p>\n\n<p>The Canadian Food Inspection Agency\u00a0(CFIA) works at different levels of the food supply chain, including domestic manufacturers and importers, to verify that food is accurately labelled and not misrepresented. This includes targeted surveillance on commodities that are more susceptible to misrepresentation. During recent years, the CFIA has published <a href=\"/food-labels/food-fraud/cfia-s-role/eng/1548444760712/1548444784124#a4\" title=\"CFIA's role in combatting food fraud\">reports</a> summarizing these findings.</p>\n\n<p>Industry is responsible for ensuring compliance with regulatory requirements. The purpose of this notice is to:</p>\n\n<ul>\n<li>remind industry, including importers, distributors, and retailers, of the legal responsibility to ensure full compliance with the regulatory requirements, and</li>\n<li>outline the control and enforcement measures for non-compliance</li>\n</ul>\n\n<h2>Canada's regulatory requirements for food</h2>\n\n<p>All food sold in Canada must comply with the <a href=\"/english/reg/jredirect2.shtml?drga\"><i>Food and Drugs Act</i></a>\u00a0(FDA), the <a href=\"/english/reg/jredirect2.shtml?drgr\"><i>Food and Drug Regulations</i></a>\u00a0(FDR), the <a href=\"/english/reg/jredirect2.shtml?safefood\"><i>Safe Food for Canadians Act</i></a>\u00a0(SFCA) and the <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a>\u00a0(SFCR).</p>\n\n<p>As per these requirements, all food sold in Canada must be safe for consumption and shall not be sold, packaged, labelled or advertised in a manner that is false, misleading or <span class=\"nowrap\">deceptive [5(1), FDA; 6(1), SFCA].</span> Additional legislative and specific regulatory requirements also apply.</p>\n\n<p>Learn more about <a href=\"/food-labels/labelling/industry/eng/1383607266489/1383607344939\">labelling requirements</a> and other <a href=\"/food-guidance-by-commodity/eng/1521720158031/1521720158588\">regulatory requirements for food</a>.</p>\n\n<h2>Obligations of industry</h2>\n\n<p>Regulated parties are responsible for ensuring all applicable legislative and regulatory requirements are met. Misrepresentation, adulteration, dilution and substitution of a food commodity are generally considered contraventions of the FDA and SFCA.</p>\n\n<p>Under the SFCR, most Safe Food for Canadian <a href=\"/food-licences/licensing/eng/1523903020086/1523903065567\">licence holders</a> must maintain a <a href=\"/preventive-controls/preventive-control-plans/eng/1512152894577/1512152952810\">Preventive Control Plan</a>.</p>\n\n<p>The preventive control plan must include measures to ensure regulatory requirements are met, including:</p>\n\n<ul>\n<li>matters relating to the safety of the food</li>\n<li>assurance that the food is not represented in a manner that is false, misleading or deceptive; and</li>\n<li>compliance with applicable compositional standards</li>\n</ul>\n\n<p>Examples of preventive control measures may include supply chain controls such as obtaining verifiable assurance from suppliers of a product's authenticity, or conducting authenticity testing.</p>\n\n<h2>Control and enforcement measures for non-compliance</h2>\n\n<p>Industry is reminded that failure to comply with applicable legislative and regulatory \u00a0requirements with respect to the importation, distribution, or sale of food is a contravention of the FDA and/or the SFCA.</p>\n\n<p>In cases of non-compliance, the CFIA can use a wide range of control or enforcement actions including, but not limited to:</p>\n\n<ul>\n<li>control measures: seizure, detention, and removal of non-compliant goods from Canada; and</li>\n<li>enforcement actions: letters of non-compliance, formal meetings, <a href=\"/about-the-cfia/transparency/regulatory-transparency-and-openness/compliance-and-enforcement/amps/eng/1324319195211/1324319534243\">administrative monetary penalties</a>, <a href=\"/food-licences/licensing/eng/1523903020086/1523903065567#a5\">suspension or cancellation</a> of Safe Food for Canadian licences or other permissions, or prosecution</li>\n</ul>\n\n<p>Control and enforcement activities conducted by the CFIA are guided by the <a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/regulatory-response/overview/eng/1537322772198/1537322772432\">Standard Regulatory Response Process</a>. These activities are considered on a case-by-case basis, taking into consideration the harm caused by the non-compliance, the compliance history of the regulated party, and whether there is intent to violate federal requirements.</p>\n\n<h2>Ongoing surveillance activities of the CFIA</h2>\n\n<p>CFIA will continue risk-informed activities to protect Canadians from misrepresentation and contribute to a fair marketplace.</p>\n\n<p>The CFIA encourages all parties that are aware of or suspect deceptive or fraudulent practices to <a href=\"/food-safety-for-consumers/where-to-report-a-complaint/eng/1364500149016/1364500195684\">report these to the CFIA</a>.</p>\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>Les lois canadiennes interdisent la repr\u00e9sentation trompeuse des aliments. L'\u00e9tiquetage trompeuse, la falsification, et la substitution d'aliments sont des formes de repr\u00e9sentation trompeuse qui peuvent constituer une fraude alimentaire.</p>\n\n<p>La fraude alimentaire trompe les consommateurs et les entreprises alimentaires. Elle cr\u00e9e \u00e9galement un march\u00e9 injuste pour les entreprises qui respectent les r\u00e8gles d'\u00e9tiquetage des aliments. Dans certains cas, une repr\u00e9sentation trompeuse des aliments peut m\u00eame affecter la sant\u00e9 des consommateurs, comme dans le cas d'allerg\u00e8nes ou d'additifs non d\u00e9clar\u00e9s, ou d'all\u00e9gations inexactes concernant la valeur nutritive.</p>\n\n<p>L'Agence canadienne d'inspection des aliments\u00a0(ACIA) travaille \u00e0 diff\u00e9rents niveaux de la cha\u00eene d'approvisionnement alimentaire, y compris aupr\u00e8s des fabricants et des importateurs nationaux, pour v\u00e9rifier que les aliments sont correctement \u00e9tiquet\u00e9s et ne font pas l'objet d'une repr\u00e9sentation trompeuse. Cela comprend une surveillance cibl\u00e9e des produits qui sont plus susceptibles de faire l'objet d'une repr\u00e9sentation trompeuse. Au cours des derni\u00e8res ann\u00e9es, l'ACIA a publi\u00e9 des <a href=\"/etiquetage-des-aliments/fraude-alimentaire/role-de-l-acia/fra/1548444760712/1548444784124#a4\" title=\"R\u00f4le de l'ACIA dans la lutte contre la fraude alimentaire\">rapports</a> r\u00e9sumant ces constatations.</p>\n\n<p>L'industrie est responsable de veiller au respect des exigences r\u00e9glementaires. Le pr\u00e9sent avis a pour but\u00a0:</p>\n\n<ul>\n<li>de rappeler \u00e0 l'industrie, y compris aux importateurs, aux distributeurs et aux d\u00e9taillants, la responsabilit\u00e9 l\u00e9gale d'assurer l'enti\u00e8re conformit\u00e9 aux exigences r\u00e9glementaires;</li>\n<li>de d\u00e9crire les mesures de contr\u00f4le et d'application de la loi en cas de non-conformit\u00e9.</li>\n</ul>\n\n<h2>Exigences r\u00e9glementaires du Canada en mati\u00e8re d'aliments</h2>\n\n<p>Tous les aliments vendus au Canada doivent \u00eatre conformes \u00e0 la <a href=\"/francais/reg/jredirect2.shtml?drga\"><i>Loi sur les aliments et drogues</i></a>\u00a0(LAD), au <a href=\"/francais/reg/jredirect2.shtml?drgr\"><i>R\u00e8glement sur les aliments et drogues</i></a>\u00a0(RAD), \u00e0 la <a href=\"/francais/reg/jredirect2.shtml?safefood\"><i>Loi sur la salubrit\u00e9 des aliments au Canada</i></a>\u00a0(LSAC) et au <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a>\u00a0(RSAC).</p>\n\n<p>Conform\u00e9ment \u00e0 ces exigences, tous les aliments vendus au Canada doivent \u00eatre salubres pour la consommation et ne doivent pas \u00eatre vendus, emball\u00e9s, \u00e9tiquet\u00e9s ou annonc\u00e9s de mani\u00e8re fausse, trompeuse ou <span class=\"nowrap\">mensong\u00e8re [5(1), LAD]; 6(1), LSAC].</span> D'autres exigences l\u00e9gislatives et r\u00e9glementaires sp\u00e9cifiques s'appliquent \u00e9galement.</p>\n\n<p>Apprenez-en davantage sur les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/fra/1383607266489/1383607344939\">exigences en mati\u00e8re d'\u00e9tiquetage</a> et d'autres <a href=\"/exigences-et-documents-d-orientation-relatives-a-c/fra/1521720158031/1521720158588\">exigences r\u00e9glementaires relatives aux aliments</a>.</p>\n\n<h2>Obligations de l'industrie</h2>\n\n<p>Les parties r\u00e9glement\u00e9es sont responsables de veiller \u00e0 ce que toutes les exigences l\u00e9gislatives et r\u00e9glementaires applicables soient respect\u00e9es. La repr\u00e9sentation trompeuse, l'adult\u00e9ration, la dilution et la substitution d'un produit alimentaire sont g\u00e9n\u00e9ralement consid\u00e9r\u00e9es comme des infractions \u00e0 la LAD et \u00e0 la LSAC.</p>\n\n<p>En vertu du RSAC, la plupart des <a href=\"/licences-pour-aliments/licences/fra/1523903020086/1523903065567\">titulaires de licence</a> pour la salubrit\u00e9 des aliments au Canada doivent maintenir un <a href=\"/controles-preventifs/plans-de-controle-preventif/fra/1512152894577/1512152952810\">plan de contr\u00f4le pr\u00e9ventif</a>\u00a0(PCP).</p>\n\n<p>Le plan de contr\u00f4le pr\u00e9ventif doit comprendre des mesures visant \u00e0 assurer le respect des exigences r\u00e9glementaires, notamment\u00a0:</p>\n\n<ul>\n<li>Les affaires relatives \u00e0 la salubrit\u00e9 des aliments;</li>\n<li>L'assurance que les aliments ne sont pas repr\u00e9sent\u00e9s de mani\u00e8re fausse, trompeuse ou mensong\u00e8re;</li>\n<li>Le respect des normes de composition en vigueur.</li>\n</ul>\n\n<p>Des mesures de contr\u00f4le pr\u00e9ventif peuvent comprendre des contr\u00f4les de la cha\u00eene d'approvisionnement, comme l'obtention d'une assurance v\u00e9rifiable de la part des fournisseurs relativement \u00e0 l'authenticit\u00e9 d'un produit, ou la r\u00e9alisation d'analyses de l'authenticit\u00e9.</p>\n\n<h2>Mesures de contr\u00f4le et d'application de la loi en cas de non-conformit\u00e9</h2>\n\n<p>L'ACIA rappelle \u00e0 l'industrie que la non-conformit\u00e9 aux exigences l\u00e9gislatives et r\u00e9glementaires en vigueur en mati\u00e8re d'importation, de distribution ou de vente d'aliments, constitue une infraction \u00e0 la LAD ou \u00e0 la LSAC.</p>\n\n<p>En cas de non-conformit\u00e9, l'ACIA peut recourir \u00e0 un large \u00e9ventail de mesures de contr\u00f4le ou d'application de la loi, entre autres\u00a0:</p>\n\n<ul>\n<li>Mesures de contr\u00f4le\u00a0: saisie, d\u00e9tention et retrait de marchandises non conformes du Canada;</li>\n<li>Mesures d'application de la loi\u00a0: lettres de non-conformit\u00e9, r\u00e9unions officielles, <a href=\"/a-propos-de-l-acia/transparence/transparence-et-ouverture/conformite-et-application-de-la-loi/sap/fra/1324319195211/1324319534243\">sanctions administratives p\u00e9cuniaires</a>\u00a0(SAP), <a href=\"/licences-pour-aliments/licences/fra/1523903020086/1523903065567#a5\">suspension ou r\u00e9vocation</a> de licences relatives \u00e0 la salubrit\u00e9 des aliments au Canada ou d'autres autorisations, ou poursuites.</li>\n</ul>\n\n<p>Les mesures de contr\u00f4le et d'application de la loi men\u00e9es par l'ACIA sont guid\u00e9es par le <a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/intervention-reglementaire/apercu/fra/1537322772198/1537322772432\">Processus d'intervention r\u00e9glementaire normalis\u00e9</a>\u00a0(PIRN). Ces activit\u00e9s sont examin\u00e9es au cas par cas, en tenant compte du pr\u00e9judice caus\u00e9 par la non-conformit\u00e9, des ant\u00e9c\u00e9dents de conformit\u00e9 de la partie r\u00e9glement\u00e9e et de l'intention de contrevenir aux exigences f\u00e9d\u00e9rales.</p>\n\n<h2>Activit\u00e9s de surveillance continue de l'ACIA</h2>\n\n<p>L'ACIA poursuivra ses activit\u00e9s ax\u00e9es sur le risque afin de prot\u00e9ger les Canadiens contre les repr\u00e9sentations trompeuses et de contribuer \u00e0 un march\u00e9 \u00e9quitable.</p>\n\n<p>L'ACIA encourage toutes les parties qui sont au courant ou soup\u00e7onnent des pratiques trompeuses ou frauduleuses \u00e0 les <a href=\"/salubrite-alimentaire-pour-les-consommateurs/ou-signaler-une-plainte/fra/1364500149016/1364500195684\">signaler \u00e0 l'ACIA</a>.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}