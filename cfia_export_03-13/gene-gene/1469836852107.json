{
    "dcr_id": "1469836852107",
    "title": {
        "en": "Goat genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique caprine et tremblante"
    },
    "html_modified": "2024-03-12 3:57:26 PM",
    "modified": "2021-04-22",
    "issued": "2016-09-02",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_scrapie_goat_genetics_1469836852107_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_scrapie_goat_genetics_1469836852107_fra"
    },
    "ia_id": "1469836924249",
    "parent_ia_id": "1329723572482",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1329723572482",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Goat genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique caprine et tremblante"
    },
    "label": {
        "en": "Goat genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique caprine et tremblante"
    },
    "templatetype": "content page 1 column",
    "node_id": "1469836924249",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329723409732",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/scrapie/goat-genetics-and-scrapie/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/genetique-caprine-et-tremblante/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Goat genetics and scrapie",
            "fr": "G\u00e9n\u00e9tique caprine et tremblante"
        },
        "description": {
            "en": "Genotyping sheep for susceptibility to scrapie infection is an internationally recognized tool in scrapie disease control programs in many countries, including Canada's National Scrapie Eradication Program.",
            "fr": "Le g\u00e9notypage de la sensibilit\u00e9 \u00e0 la tremblante, qui vise \u00e0 d\u00e9pister une sensibilit\u00e9 \u00e0 l'infection par la tremblante est un outil reconnu internationalement dans les programmes de lutte contre la maladie de nombreux pays, dont le Programme national d'\u00e9radication de la tremblante du Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, scrapie, sheep, goats, Genotyping, National Scrapie Eradication Program",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, Tremblante, mouton, esp\u00e8ce caprine, g\u00e9notypage, Programme national d'\u00e9radication de la tremblante"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,livestock,regulation,veterinary medicine",
            "fr": "sant\u00e9 animale,inspection,b\u00e9tail,r\u00e9glementation,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-09-02",
            "fr": "2016-09-02"
        },
        "modified": {
            "en": "2021-04-22",
            "fr": "2021-04-22"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Goat genetics and scrapie",
        "fr": "G\u00e9n\u00e9tique caprine et tremblante"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"/animal-health/terrestrial-animals/diseases/reportable/scrapie/sheep-genetics-and-scrapie/eng/1356066354790/1356066497693\">Genotyping sheep</a> for susceptibility to scrapie infection is an internationally recognized tool in scrapie disease control programs in many countries, including <a href=\"/animal-health/terrestrial-animals/diseases/reportable/scrapie/eng/1329723409732/1329723572482\">Canada's National Scrapie Eradication Program (NSEP)</a>. More scientific information on goat genetics and scrapie susceptibility has become available both internationally and in Canada. The Canadian goat industry may now use genetics as a risk management tool for scrapie. Genotyping goats for susceptibility to scrapie infection will also be piloted as a disease control measure in the future, along with the other tools currently available in Canada's NSEP.</p>\n<h2>Genotyping</h2>\n<p>A genotype is an individual's collection of genes. Like all mammals, goats receive one allele for each gene from their dam (doe) and one allele from their sire (buck). Alleles are the different versions of a gene. Scrapie genotyping has been routinely used in sheep, and is now available to start using in goats. Genotype testing reveals the specific alleles inherited for the animals' prion gene that makes an animal more or less susceptible to scrapie.</p>\n<h2>Scrapie genotyping in goats</h2>\n<p>Results of a large amount of international and Canadian research on goat genotypes and susceptibility to scrapie are now available. Multiple variations in the goat prion gene have been studied and associated with different levels of scrapie resistance, depending on the breed of goat, the originating region of the animal and the individual study. Data from Canada, the United States and the European Union agree that goats having a single copy of either the S146 or K222 alleles have shown a strong degree of resistance to natural infection with scrapie.</p>\n<p>These two alleles naturally occur in Canadian goat breeds, and can be used by the Canadian goat industry to routinely start genotyping in their breeding programs. These alleles will likely continue to be found in additional goat breeds in Canada as more goats are genotyped.</p>\n<p>Producers who choose not to selectively breed for scrapie resistance are strongly urged to either close their herd or buy goats from herds in a <a href=\"https://scrapiecanada.ca/sfcp/\">scrapie flock certification program</a> and start scrapie testing in mature deadstock.</p>\n<h2>How scrapie genotyping is used in the National Scrapie Eradication Program</h2>\n<p>It is important to understand that scrapie genotyping is not disease testing. Scrapie genotyping is used in the NSEP in sheep as a tool used by the CFIA during disease control actions with an infected sheep flock, to try to decrease the number of sheep ordered destroyed in the flock. At this stage, the CFIA will be preparing to pilot the use of genotyping in goats as an added tool during disease control actions, in eligible positive goat herds. The CFIA continues to contribute to and evaluate new science as it applies to goat scrapie, and when possible, will continue to transfer it to the NSEP.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Le <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/genetique-du-mouton-et-tremblante/fra/1356066354790/1356066497693\">g\u00e9notypage du mouton</a>, pour d\u00e9terminer la sensibilit\u00e9 \u00e0 l'infection par la tremblante est un outil reconnu \u00e0 l'\u00e9chelle internationale dans les programmes de lutte contre la maladie dans de nombreux pays, y compris le <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/fra/1329723409732/1329723572482\">Programme national d'\u00e9radication de la tremblante (PNET) du Canada</a>. De plus en plus de renseignements scientifiques sur la g\u00e9n\u00e9tique de la ch\u00e8vre et la sensibilit\u00e9 \u00e0 la tremblante sont dor\u00e9navant disponibles, tant \u00e0 l'\u00e9chelle internationale qu'au Canada. L'industrie caprine canadienne peut maintenant utiliser la g\u00e9n\u00e9tique comme outil de gestion du risque pour la tremblante. Le g\u00e9notypage des ch\u00e8vres afin de d\u00e9terminer leur sensibilit\u00e9 \u00e0 l'infection par la tremblante fera \u00e9galement l'objet d'un projet pilote \u00e0 titre de mesure de lutte contre la maladie dans le futur, accompagn\u00e9 des autres outils actuellement disponibles dans le cadre du PNET du Canada.</p>\n<h2>G\u00e9notypage</h2>\n<p>Un g\u00e9notype est l'ensemble des g\u00e8nes d'un individu. Comme tous les mammif\u00e8res, la ch\u00e8vre re\u00e7oit un all\u00e8le de chacun des g\u00e8nes de sa m\u00e8re (femelle) et de son p\u00e8re (m\u00e2le). Les all\u00e8les sont les diff\u00e9rentes versions d'un g\u00e8ne. Le g\u00e9notypage pour la tremblante a \u00e9t\u00e9 r\u00e9guli\u00e8rement utilis\u00e9 chez le mouton, et il est d\u00e9sormais possible de commencer \u00e0 l'utiliser chez la ch\u00e8vre. L'analyse du g\u00e9notype r\u00e9v\u00e8le les all\u00e8les sp\u00e9cifiques h\u00e9rit\u00e9s du g\u00e8ne du prion des animaux qui rendent un individu plus ou moins sensible \u00e0 la tremblante.</p>\n<h2>G\u00e9notypage pour la tremblante chez la ch\u00e8vre</h2>\n<p>Les r\u00e9sultats d'un grand nombre de recherches internationales et canadiennes sur les g\u00e9notypes de la ch\u00e8vre et la sensibilit\u00e9 \u00e0 la tremblante sont maintenant disponibles. De multiples variations du g\u00e8ne du prion chez la ch\u00e8vre ont \u00e9t\u00e9 \u00e9tudi\u00e9es et associ\u00e9es \u00e0 divers degr\u00e9s de r\u00e9sistance \u00e0 la tremblante, selon la race de ch\u00e8vre, sa r\u00e9gion d'origine et l'\u00e9tude sur un individuindividu. Les donn\u00e9es provenant du Canada, des \u00c9tats-Unis et de l'Union europ\u00e9enne s'accordent pour dire que les ch\u00e8vres ayant une seule copie des all\u00e8les S146 ou K222 ont d\u00e9montr\u00e9 un fort degr\u00e9 de r\u00e9sistance \u00e0 l'infection naturelle par la tremblante.</p>\n<p>Ces deux all\u00e8les sont naturellement pr\u00e9sents chez les races caprines canadiennes, et peuvent \u00eatre utilis\u00e9s par l'industrie caprine canadienne afin de d\u00e9buter le g\u00e9notypage de routine dans leurs programmes de reproduction. Ces all\u00e8les continueront probablement \u00e0 \u00eatre retrouv\u00e9s chez d'autres races de ch\u00e8vres au Canada au fur et \u00e0 mesure que d'autres ch\u00e8vres seront g\u00e9notyp\u00e9es.</p>\n<p>Pour les producteurs qui choisissent de ne pas faire de reproduction s\u00e9lective pour la r\u00e9sistance \u00e0 la tremblante, il est fortement recommand\u00e9 de soitfermer leur troupeau ou bien d'acheter des ch\u00e8vres provenant de troupeaux qui participent \u00e0 un <a href=\"https://scrapiecanada.ca/fr/certification/\">Programme de certification des troupeaux \u00e0 l'\u00e9gard de la tremblante</a> et de commencer \u00e0 tester pour la tremblante les animaux matures morts \u00e0 la ferme.</p>\n<h2>Comment le g\u00e9notypage pour la tremblante est-il utilis\u00e9 dans le Programme national d'\u00e9radication de la tremblante</h2>\n<p>Il est important de comprendre que le g\u00e9notypage pour la tremblante n'est pas un test de d\u00e9pistage de la maladie. Le g\u00e9notypage pour la tremblante est utilis\u00e9 dans le PNET chez le mouton comme outil employ\u00e9 par l'ACIA lorsque des mesures de lutte contre la maladie sont prises dans un troupeau infect\u00e9 afin de tenter de minimiser le nombre de moutons qui feront l'objet d'une ordonnance de destruction. \u00c0 ce stade, l'ACIA se pr\u00e9pare \u00e0 lancer un projet pilote sur l'utilisation du g\u00e9notypage chez la ch\u00e8vre comme outil suppl\u00e9mentaire lorsque des mesures de lutte contre la maladie sont prises dans des troupeaux de ch\u00e8vres \u00e9ligibles et positifs \u00e0 la tremblante. L'ACIA continue \u00e0 contribuer \u00e0 la recherche et \u00e0 \u00e9valuer les nouvelles donn\u00e9es s'appliquant \u00e0 la tremblante chez la ch\u00e8vre et, lorsque c'est possible, continuera \u00e0 transf\u00e9rer les nouvelles connaissances au PNET.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}