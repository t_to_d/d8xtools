{
    "dcr_id": "1387305151050",
    "title": {
        "en": "Conditions for importing meat products from Thailand",
        "fr": "Conditions pour l'importation des produits de viande de la Tha\u00eflande"
    },
    "html_modified": "2024-03-12 3:56:34 PM",
    "modified": "2018-12-10",
    "issued": "2013-12-17 13:32:32",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexa_thailand_1387305151050_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/meavia_man_chapter10_annexa_thailand_1387305151050_fra"
    },
    "ia_id": "1387305321846",
    "parent_ia_id": "1336319720090",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food|Relevant to All",
        "fr": "Importation d\u2019aliments|Pertinents pour tous"
    },
    "commodity": {
        "en": "Meat products and food animals",
        "fr": "Animaux destin\u00e9s \u00e0 l\u2019alimentation et produits de viande"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1336319720090",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Conditions for importing meat products from Thailand",
        "fr": "Conditions pour l'importation des produits de viande de la Tha\u00eflande"
    },
    "label": {
        "en": "Conditions for importing meat products from Thailand",
        "fr": "Conditions pour l'importation des produits de viande de la Tha\u00eflande"
    },
    "templatetype": "content page 1 column",
    "node_id": "1387305321846",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1336318487908",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/approved-countries/thailand/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/pays-approuves/thailande/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Conditions for importing meat products from Thailand",
            "fr": "Conditions pour l'importation des produits de viande de la Tha\u00eflande"
        },
        "description": {
            "en": "Conditions for iimporting meat products from Thailand",
            "fr": "Conditions pour l'importation des produits de viande de Tha\u00eflande"
        },
        "keywords": {
            "en": "Meat Inspection Act, Meat Inspection Regulations, 1990, meat inspection, meat products, fresh meat, raw processed meat products, Thailand",
            "fr": "Loi sur l'inspection des viandes, R\u00e8glement de 1990 sur l'inspection des viandes, inspection des viandes, produits de viande crus transform\u00e9s, Tha\u00eflande"
        },
        "dcterms.subject": {
            "en": "agri-food products,food inspection,food safety,inspection,regulation",
            "fr": "produit agro-alimentaire,inspection des aliments,salubrit\u00e9 des aliments,inspection,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2013-12-17 13:32:32",
            "fr": "2013-12-17 13:32:32"
        },
        "modified": {
            "en": "2018-12-10",
            "fr": "2018-12-10"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Conditions for importing meat products from Thailand",
        "fr": "Conditions pour l'importation des produits de viande de la Tha\u00eflande"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul class=\"list-unstyled mrgn-bttm-md\">\n<li><a href=\"#a1\">1. Meat Inspection systems approved</a></li>\n<li><a href=\"#a2\">2. Types of meat products accepted for import (based on animal health restrictions)</a></li>\n<li><a href=\"#a3\">3. Additional certification statements/attestations required on the OMIC</a></li>\n<li><a href=\"#a4\">4. Additional certificates (documents) required</a></li>\n<li><a href=\"#a5\">5. Establishments eligible for export to Canada</a></li>\n<li><a href=\"#a6\">6. Specific import and final use conditions and restrictions</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Meat Inspection systems approved</h2>\n<p><strong>1.1</strong> Poultry (chicken):  slaughter, cutting, deboning and offal.</p>\n<p><strong>1.2</strong> Processing: comminuting, formulating, curing, cooking and canning.</p>\n<h2 id=\"a2\">2. Types of meat products accepted for import (based on animal health restrictions)</h2>\n<p><strong>2.1</strong> Fresh meat and raw processed meat products (chilled or frozen):</p>\n<p><strong>2.1</strong>.1 meat and meat products derived from poultry (chicken) \u2013 Not allowed.</p>\n<p><strong>2.2</strong> All processed meat products (heat treated and raw), other than shelf stable commercially sterile meat products packaged in hermetically sealed containers (cans and/or retortable pouches), and shelf stable dried soup\u2013mix products, bouillon cubes and meat extracts:</p>\n<p><strong>2.2.1</strong> meat and meat products derived from poultry (chicken) \u2013 see section 3.1 for additional certification statements required.</p>\n<p><strong>2.3</strong> Shelf stable commercially sterile meat products packaged in hermetically sealed containers (cans and/or retortable pouches) and shelf stable dried soup\u2013mix products, bouillon cubes, meat extracts:</p>\n<p><strong>2.3.1.</strong> meat products derived from poultry (chicken) \u2013 no animal health restrictions.</p>\n<h2 id=\"a3\">3. Additional certification statements/attestations required on the <abbr title=\"Official Meat inspection Certificate\">OMIC</abbr></h2>\n<div class=\"well well-sm\">\n<p class=\"mrgn-bttm-sm\">The additional certification statements in this section are official statements which must be provided in both official languages, in the order of English first, followed by French.</p>\n</div>\n<p><strong>3.1</strong> For cooked poultry meat products/<span lang=\"fr\">Pour les produits de poulet cuits\u00a0:</span></p>\n<p>I hereby certify that/<span lang=\"fr\">Je certifie par la pr\u00e9sente que\u00a0:</span></p>\n<p><strong>3.1.1</strong> The chicken meat has been cooked, reaching an internal temperature of at least 80\u00a0\u00b0<abbr title=\"Celcius\">C</abbr>/<span lang=\"fr\">La viande de poulet a \u00e9t\u00e9 cuite jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne d'au moins 80\u00a0\u00b0<abbr title=\"Celcius\">C</abbr>;</span></p>\n<p><strong>3.1.2</strong> There is a complete physical separation between the uncooked and cooked poultry meat product areas of the processing establishment/<span lang=\"fr\">Dans l'\u00e9tablissement de transformation, il y a une s\u00e9paration physique compl\u00e8te entre les aires de produits de volaille crus at les aires de produits cuits;</span></p>\n<p><strong>3.1.3</strong> The poultry meat was handled and prepared in accordance with Canadian requirements ensuring that there was no direct or indirect recontamination of the product after cooking and packaging/<span lang=\"fr\">La viande de poulet a \u00e9t\u00e9 manipul\u00e9e et pr\u00e9par\u00e9e de fa\u00e7on \u00e0 ce qu'il n'y ait aucun recontamination directe ou indirecte du produit de viande apr\u00e8s cuisson et apr\u00e8s emballage.</span></p>\n<h2 id=\"a4\">4. Additional certificates (documents) required</h2>\n<p><strong>4.1</strong> None.</p>\n<h2 id=\"a5\">5. Establishments eligible for export to Canada:</h2>\n<p><strong>5.1</strong> Refer to the <a href=\"/active/netapp/meatforeign-viandeetranger/forliste.aspx\">List of foreign countries establishments eligible to export meat products to Canada</a></p>\n<h2 id=\"a6\">6. Specific import and final use conditions and restrictions:</h2>\n<p><strong>6.1</strong> See the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/eng/1507329098491/1507329098850\">Export Requirements Library</a> for possible export restrictions.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul class=\"list-unstyled mrgn-bttm-md\">\n<li><a href=\"#a1\">1. Syst\u00e8mes d'inspection accept\u00e9s</a></li>\n<li><a href=\"#a2\">2. Types de produits de viande accept\u00e9s pour l'importation (en fonction des restrictions pour raisons de sant\u00e9 animale)</a></li>\n<li><a href=\"#a3\">3. Libell\u00e9s ou attestations de certification suppl\u00e9mentaires devant figurer sur le COIV</a></li>\n<li><a href=\"#a4\">4. Certificats (documents) suppl\u00e9mentaires requis</a></li>\n<li><a href=\"#a5\">5. Les \u00e9tablissements \u00e9ligibles pour exportation au Canada</a></li>\n<li><a href=\"#a6\">6. Conditions et restrictions pr\u00e9cises portent sur l'importation et l'utilisation finale</a></li>\n</ul>\n\n<h2 id=\"a1\">1. Syst\u00e8mes d'inspection accept\u00e9s</h2>\n<p><strong>1.1</strong> Volaille (poulet)\u00a0: abattage, d\u00e9coupage, d\u00e9sossage et les abats.</p>\n<p><strong>1.2</strong> Transformation\u00a0: hachage, formulation, salaison, cuisson, mise en conserve.</p>\n<h2 id=\"a2\">2. Types de produits de viande accept\u00e9s pour l'importation (en fonction des restrictions pour raisons de sant\u00e9 animale)</h2>\n<p><strong>2.1</strong> Viande fra\u00eeche et produits de viande transform\u00e9s crus (congel\u00e9s ou r\u00e9frig\u00e9r\u00e9s)\u00a0:</p>\n<p><strong>2.1</strong>.1 viande et produits de viande issus de volailles (poulet) \u2013 non autoris\u00e9.</p>\n<p><strong>2.2</strong> Produits de viande transform\u00e9s (trait\u00e9s \u00e0 la chaleur ou crus), autres que les produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilisables), et les m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, les cubes de bouillon et les extraits de viande de longue conservation\u00a0:</p>\n<p><strong>2.2.1</strong> viande et produits de viande issus de volailles (poulet) \u2013 consulter les sections 3.1 pour les attestations additionnelles requises.</p>\n<p><strong>2.3</strong> Produits de viande de longue conservation, commercialement st\u00e9riles et emball\u00e9s dans des contenants herm\u00e9tiques (conserves, sachets st\u00e9rilisables), et m\u00e9langes \u00e0 soupe d\u00e9shydrat\u00e9s, cubes de bouillon et extraits de viande de longue conservation\u00a0:</p>\n<p><strong>2.3.1</strong> dans le cas des produits de viande issus de volailles (poulet) \u2013 aucune restriction pour raisons de sant\u00e9 animale.</p>\n<h2 id=\"a3\">3. Libell\u00e9s ou attestations de certification suppl\u00e9mentaires devant figurer sur le <abbr title=\"Certificat officiel d'inspection des viandes\">COIV</abbr></h2>\n\n<div class=\"well well-sm\">\n<p class=\"mrgn-bttm-sm\">Les attestations de certification suppl\u00e9mentaires dans cette section sont des attestations officielles qui doivent \u00eatre fournies dans les deux langues officielles, soit l'anglais en premier, suivi du fran\u00e7ais.</p>\n</div>\n \n<p><strong>3.1</strong> <span lang=\"en\">For cooked poultry meat products</span>/Pour les produits de poulet cuits\u00a0:</p>\n<p><span lang=\"en\">I hereby certify that</span>/Je certifie par la pr\u00e9sente que\u00a0:</p>\n<p><strong>3.1.1</strong> <span lang=\"en\">The chicken meat has been cooked, reaching an internal temperature of at least 80\u00a0\u00b0<abbr title=\"Celcius\">C</abbr></span>/La viande de poulet a \u00e9t\u00e9 cuite jusqu'\u00e0 l'obtention d'une temp\u00e9rature interne d'au moins 80\u00a0\u00b0<abbr title=\"Celcius\">C</abbr>;</p>\n<p><strong>3.1.2</strong> <span lang=\"en\">There is a complete physical separation between the uncooked and cooked poultry meat product areas of the processing establishment</span>/Dans l'\u00e9tablissement de transformation, il y a une s\u00e9paration physique compl\u00e8te entre les aires de produits de volaille crus at les aires de produits cuits;</p>\n<p><strong>3.1.3</strong> <span lang=\"en\">The poultry meat was handled and prepared in accordance with Canadian requirements ensuring that there was no direct or indirect recontamination of the product after cooking and packaging</span>/La viande de poulet a \u00e9t\u00e9 manipul\u00e9e et pr\u00e9par\u00e9e de fa\u00e7on \u00e0 ce qu'il n'y ait aucun recontamination directe ou indirecte du produit de viande apr\u00e8s cuisson et apr\u00e8s emballage.</p>\n<h2 id=\"a4\">4. Certificats (documents) suppl\u00e9mentaires requis</h2>\n<p><strong>4.1</strong> Aucun.</p>\n<h2 id=\"a5\">5. Les \u00e9tablissements \u00e9ligibles pour exportation au Canada</h2>\n<p><strong>5.1</strong> Consultez la <a href=\"/active/netapp/meatforeign-viandeetranger/forlistf.aspx\">Liste des \u00e9tablissements des pays \u00e9trangers autoris\u00e9s \u00e0 exporter les produits de viande au Canada</a>.</p>\n<h2 id=\"a6\">6. Conditions et restrictions pr\u00e9cises portant sur l'importation et l'utilisation finale\u00a0:</h2>\n<p><strong>6.1</strong> Consultez la <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/fra/1507329098491/1507329098850\">biblioth\u00e8que des exigences en mati\u00e8re d'exportation</a> pour d'\u00e9ventuelles contraintes \u00e0 l'exportation.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}