{
    "dcr_id": "1305853043399",
    "title": {
        "en": "West Nile Virus Fact Sheet",
        "fr": "Fiche de renseignements sur le Virus du Nil occidental"
    },
    "html_modified": "2024-02-13 9:46:45 AM",
    "modified": "2018-01-10",
    "issued": "2011-05-19 20:57:26",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_westnile_fact_1305853043399_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_westnile_fact_1305853043399_fra"
    },
    "ia_id": "1305853235540",
    "parent_ia_id": "1305840912854",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1305840912854",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "West Nile Virus Fact Sheet",
        "fr": "Fiche de renseignements sur le Virus du Nil occidental"
    },
    "label": {
        "en": "West Nile Virus Fact Sheet",
        "fr": "Fiche de renseignements sur le Virus du Nil occidental"
    },
    "templatetype": "content page 1 column",
    "node_id": "1305853235540",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1305840783267",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/immediately-notifiable/west-nile-virus/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/virus-du-nil-occidental/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "West Nile Virus Fact Sheet",
            "fr": "Fiche de renseignements sur le Virus du Nil occidental"
        },
        "description": {
            "en": "West Nile virus (WNV) belongs to a family of viruses called Flaviviridae.",
            "fr": "Le virus du Nil occidental (VNO) est un flavivirus (famille des Flaviviridae)."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, immediately notifiable disease, West Nile Virus, humans, horses, crows, blue and grey jays, ravens, magpies, ataxia, depression, lethargy, fever, head pressing, tilt, impaired vision, inability to swallow, loss of appetite, muscle weakness, twitching, partial paralysis, coma",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, maladie \u00e0 notification imm\u00e9diate, Virus du Nil occidental, humains, cheavaux, orneilles, geais bleus ou gris, corbeaux, pies, ataxie, apathie. l\u00e9thargie, fi\u00e8vre, besoin de s'appuyer la t\u00eate, inclinaison de la t\u00eate, troubles de la vue, incapacit\u00e9 d'avaler, perte d'app\u00e9tit, la faiblesse ou des spasmes musculaires, paralysie partielle, coma"
        },
        "dcterms.subject": {
            "en": "imports,inspection,veterinary medicine,animal health",
            "fr": "importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-19 20:57:26",
            "fr": "2011-05-19 20:57:26"
        },
        "modified": {
            "en": "2018-01-10",
            "fr": "2018-01-10"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "West Nile Virus Fact Sheet",
        "fr": "Fiche de renseignements sur le Virus du Nil occidental"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is West Nile virus?</h2>\n\n<p>West Nile virus (WNV) belongs to a family of viruses called <span lang=\"la\">Flaviviridae</span>. The virus circulates among mosquitoes and wild birds, but can infect many other species.</p>\n\n<p>Horses and humans show signs of disease more than other mammals.</p>\n\n<p>Most wild birds show no clinical signs. However, birds such as crows, ravens, blue and grey jays are very susceptible to infection with <abbr title=\"West Nile virus\">WNV</abbr>. They usually die once infected.</p>\n\n<p>In the domestic bird population, chickens and turkeys usually show no signs of infection. Geese often show neurological signs.</p>\n\n<h2 class=\"h5\">Is West Nile virus a risk to human health?</h2>\n\n<p>An infected mosquito transmits the virus by biting a human. Most people infected with the virus either have no symptoms or flu-like symptoms. Rarely, people can become very ill resulting in hospitalization and even death.</p>\n\n<p>There is more information about <abbr title=\"West Nile virus\">WNV</abbr> in people on the <a href=\"https://www.canada.ca/en/public-health/services/diseases/west-nile-virus.html\" title=\"West Nile virus\">Health Canada</a> website.</p>\n\n<p>Almost every province of Canada has had human cases of <abbr title=\"West Nile virus\">WNV</abbr>.</p>\n\n<h2 class=\"h5\">What are the clinical signs of West Nile virus infection?</h2>\n\n<p>Animals (particularly horses) infected with the virus may show the following clinical signs:</p>\n\n<ul>\n<li>ataxia (lack of coordination);</li>\n<li>depression or lethargy;</li>\n<li>fever;</li>\n<li>head pressing;</li>\n<li>head tilt;</li>\n<li>impaired vision;</li>\n<li>inability to swallow;</li>\n<li>loss of appetite;</li>\n<li>muscle weakness or twitching;</li>\n<li>partial paralysis;</li>\n<li>coma;</li>\n<li>death</li>\n</ul>\n\n<p>The clinical signs of <abbr title=\"West Nile virus\">WNV</abbr> can be confused with rabies in mammals. It can look like Newcastle Disease and Avian Influenza in domestic birds.</p>\n\n<h2 class=\"h5\">Where is West Nile virus found?</h2>\n\n<p>Europe, Africa and Asia have reported cases of <abbr title=\"West Nile virus\">WNV</abbr>. In 1999, the virus was found in the <abbr title=\"United States\">U.S.</abbr> for the first time. It was found in Canada in 2001. In 2003, some South American countries reported their first <abbr title=\"West Nile virus\">WNV</abbr> cases.</p>\n\n<p>The <a href=\"http://www.cwhc-rcsf.ca/surveillance_data_wnv.php\">Canadian Wildlife Health Cooperative</a>\u00a0and the <a href=\"https://www.canada.ca/en/public-health/services/diseases/west-nile-virus/surveillance-west-nile-virus.html\" title=\"Surveillance of West Nile virus\">Public Health Agency of Canada</a> (PHAC) monitor the progression of <abbr title=\"West Nile virus\">WNV</abbr> in Canada. You can find reports of surveillance information on these websites.</p>\n\n<h2 class=\"h5\">How is West Nile virus transmitted and spread?</h2>\n\n<p>West Nile virus is usually spread by the bite of a mosquito that has fed on an infected bird. Mosquitoes transmit the virus from wild birds to mammals and domestic poultry.</p>\n\n<p>Very rarely, the virus can be spread through contact with infected animals, their blood, or other tissues.</p>\n\n<h2 class=\"h5\">How is West Nile virus infection diagnosed?</h2>\n\n<p>In horses and geese, neurological signs may suggest infection. Only laboratory tests can confirm the diagnosis of <abbr title=\"West Nile virus\">WNV</abbr>.</p>\n\n<p>During the outbreak in 2001, scientists used crows as an indicator of virus spread. Community members reported findings of dead crows. Scientists used this information to track the distribution of the disease in Canada.</p>\n\n<h2 class=\"h5\">How is West Nile virus infection treated?</h2>\n\n<p>There is no treatment available to kill the virus. Fluid therapy and anti-inflammatories can reduce the severity of clinical signs.</p>\n\n<p>Horses can be vaccinated to prevent infection. There are currently several <abbr title=\"West Nile virus\">WNV</abbr> vaccines for horses in Canada. The vaccines need to be given every year for continued protection. Contact your veterinarian for more information on <abbr title=\"West Nile virus\">WNV</abbr> vaccines for your horse.</p>\n\n<p>Vaccinated horses may test positive on certain blood tests. This may affect their eligibility for export to countries that require negative blood test results for the virus. Some other countries require that horses be certified as vaccinated against the virus prior to import. For information on specific import/export requirements, contact your <a href=\"/eng/1300462382369/1300462438912\" title=\"Animal Health Offices\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> district office</a>.</p>\n\n<h2 class=\"h5\">What is done to protect Canadian livestock from West Nile virus?</h2>\n\n<p><abbr title=\"West Nile virus\">WNV</abbr> is an immediately notifiable disease. This means that all laboratories must notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> when they suspect or diagnose this disease.</p>\n\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> considers <abbr title=\"West Nile virus\">WNV</abbr> a domestic disease, meaning that it is commonly found in Canada. Your vet will submit samples to a provincial or other lab for diagnosis. You will have to pay for <abbr title=\"West Nile virus\">WNV</abbr> testing if your vet suspects it.</p>\r\n<h2 class=\"font-medium black\">Additional information</h2>\n<ul>\n<li><a href=\"/eng/1300462382369/1300462438912\">Animal Health Offices</a></li>\n</ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que le virus du Nil occidental?</h2>\n\n<p>Le virus du Nil occidental (VNO) est un flavivirus (famille des <span lang=\"la\">Flaviviridae</span>). Il se propage par l'entremise des moustiques et des oiseaux sauvages mais peut infecter plusieurs autres esp\u00e8ces.</p>\n\n<p>Les chevaux et les humains pr\u00e9sentent d'avantages de signes de la maladie que les autres esp\u00e8ces de mammif\u00e8res.</p>\n\n<p>La plupart des oiseaux sauvages ne pr\u00e9sentent aucun signe de la maladie. Par contre, les corneilles, les geais bleus ou gris, les corbeaux et les pies sont particuli\u00e8rement vuln\u00e9rable \u00e0 une infection par le <abbr title=\"Virus du Nil occidental\">VNO</abbr>. Ils meurent habituellement une fois infect\u00e9s.</p>\n\n<p>La plupart des oiseaux domestiques infect\u00e9s ne pr\u00e9sentent pas de signes cliniques; seules les oies domestiques semblent particuli\u00e8rement susceptibles de contracter la maladie et d'en mourir.</p>\n\n<h2 class=\"h5\">Le virus du Nil occidental pose-t-il un risque pour la sant\u00e9 humaine?</h2>\n\n<p>Le <abbr title=\"Virus du Nil occidental\">VNO</abbr> peut \u00eatre transmis aux humains par l'entremise de piq\u00fbres de moustiques infect\u00e9s. En r\u00e8gle g\u00e9n\u00e9rale, les personnes atteintes ne manifestent soit aucun sympt\u00f4me ou ont des sympt\u00f4mes semblables \u00e0 ceux de la grippe. Rarement, le virus peut causer une grave maladie pouvant n\u00e9cessiter une hospitalisation ou entra\u00eener la mort.</p>\n\n<p>Plus de renseignements \u00e0 propos du <abbr title=\"Virus du Nil occidental\">VNO</abbr> chez l'humain peuvent \u00eatre obtenus sur le site de <a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/virus-nil-occidental.html\" title=\"Virus du Nil occidental\">Sant\u00e9 Canada</a>.</p>\n\n<p>Des cas d'infection par le <abbr title=\"Virus du Nil occidental\">VNO</abbr> chez l'humain ont \u00e9t\u00e9 observ\u00e9s dans presque toutes les provinces canadiennes.</p>\n\n<h2 class=\"h5\">Quels sont les signes cliniques d'une infection par le virus du Nil occidental?</h2>\n\n<p>Les animaux (en particulier les chevaux) infect\u00e9s par le <abbr title=\"Virus du Nil occidental\">VNO</abbr> pr\u00e9sentent les signes cliniques suivants\u00a0:</p>\n\n<ul>\n<li>l'ataxie (manque de coordination);</li>\n<li>l'apathie ou la l\u00e9thargie;</li>\n<li>la fi\u00e8vre;</li>\n<li>le besoin de s'appuyer la t\u00eate;</li>\n<li>une inclinaison de la t\u00eate;</li>\n<li>les troubles de la vue;</li>\n<li>l'incapacit\u00e9 d'avaler;</li>\n<li>la perte d'app\u00e9tit;</li>\n<li>la faiblesse ou des spasmes musculaires;</li>\n<li>la paralysie partielle;</li>\n<li>le coma;</li>\n<li>la mort</li>\n</ul>\n\n<p>Les signes cliniques du <abbr title=\"Virus du Nil occidental\">VNO</abbr> chez les mammif\u00e8res peuvent \u00eatre confondus avec ceux de la rage. Les signes cliniques du <abbr title=\"Virus du Nil occidental\">VNO</abbr> peuvent ressembler \u00e0 ceux de la maladie de Newcastle ou ceux de l'influenza aviaire chez les oiseaux domestiques.</p>\n\n<h2 class=\"h5\">O\u00f9 trouve-t-on des cas de virus du Nil occidental?</h2>\n\n<p>Des cas de <abbr title=\"Virus du Nil occidental\">VNO</abbr> ont \u00e9t\u00e9 rapport\u00e9s en Europe, en Afrique et en Asie. En 1999, ce virus a \u00e9t\u00e9 signal\u00e9 pour la premi\u00e8re fois aux \u00c9tats-Unis et en 2001, sa pr\u00e9sence a \u00e9t\u00e9 confirm\u00e9e au Canada. En 2003, certains pays d'Am\u00e9rique du Sud ont d\u00e9clar\u00e9 leurs premiers cas de <abbr title=\"Virus du Nil occidental\">VNO</abbr>.</p>\n\n<p>Le <a href=\"http://www.cwhc-rcsf.ca/data_products_wnv.php\">R\u00e9seau canadien de la sant\u00e9 de la faune</a> et l'<a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/virus-nil-occidental/surveillance-virus-nil-occidental.html\" title=\"Surveillance du virus du Nil occidental\">Agence de la sant\u00e9 publique du Canada</a> (ASPC) surveillent la progression du <abbr title=\"Virus du Nil occidental\">VNO</abbr> au pays. L'<abbr title=\"Agence de la sant\u00e9 publique du Canada\">ASPC</abbr> diffuse les r\u00e9sultats de ces activit\u00e9s de surveillance sur son site Web.</p>\n\n<h2 class=\"h5\">Quels sont les modes de transmission et de propagation du virus du Nil occidental?</h2>\n\n<p>Le <abbr title=\"Virus du Nil occidental\">VNO</abbr> se propage habituellement via les piq\u00fbres de moustiques se nourrissant du sang d'oiseaux infect\u00e9s. Les moustiques transmettent le virus de l'h\u00f4te principal (populations d'oiseaux sauvages) aux mammif\u00e8res et aux volailles domestiques.</p>\n\n<p>Tr\u00e8s rarement, le <abbr title=\"Virus du Nil occidental\">VNO</abbr> peut se r\u00e9pandre par contact entre animaux infect\u00e9s, leurs sangs ou autres tissus.</p>\n\n<h2 class=\"h5\">Comment diagnostique-t-on le virus du Nil occidental?</h2>\n\n<p>Chez les chevaux et les oies, un signe neurologique peut laisser pr\u00e9sumer la pr\u00e9sence du <abbr title=\"Virus du Nil occidental\">VNO</abbr>. La seule mani\u00e8re de diagnostiquer d\u00e9finitivement le <abbr title=\"Virus du Nil occidental\">VNO</abbr> est par l'analyse en laboratoire.</p>\n\n<p>Au cours de l'\u00e9closion de 2001, les chercheurs ont utilis\u00e9 les corbeaux comme un indicateur de la propagation du virus. Les membres de la communaut\u00e9 ont rapport\u00e9 avoir trouv\u00e9 des corbeaux morts. Les chercheurs ont utilis\u00e9s ces informations pour suivre la distribution de la maladie au Canada.</p>\n\n<h2 class=\"h5\">Comment traite-t-on le virus du Nil occidental?</h2>\n\n<p>Il n'existe actuellement aucun traitement contre ce virus. Toutefois, on a recours \u00e0 la fluidoth\u00e9rapie et \u00e0 un traitement anti-inflammatoire pour att\u00e9nuer la gravit\u00e9 des signes cliniques.</p>\n\n<p>La vaccination pourrait pr\u00e9venir le <abbr title=\"Virus du Nil occidental\">VNO</abbr> chez les chevaux. Actuellement, il existe quelques vaccins contre le <abbr title=\"Virus du Nil occidental\">VNO</abbr> chez les chevaux au Canada. Ces vaccins doivent \u00eatre suivis d'une injection de rappel annuelle pour que la protection soit maintenue. Communiquez avec votre v\u00e9t\u00e9rinaire pour en savoir plus sur les vaccins pour votre cheval.</p>\n\n<p>Les chevaux vaccin\u00e9s peuvent obtenir des r\u00e9sultats positifs \u00e0 certaines analyses sanguines. Cela peut nuire \u00e0 leur admissibilit\u00e9 \u00e0 l'exportation vers des pays qui exigent des r\u00e9sultats n\u00e9gatifs aux analyses sanguines de d\u00e9pistage du <abbr title=\"Virus du Nil occidental\">VNO</abbr>. Certains pays demandent en outre qu'on certifie que les chevaux ont \u00e9t\u00e9 vaccin\u00e9s contre le <abbr title=\"Virus du Nil occidental\">VNO</abbr> avant l'importation. Pour de plus amples renseignements sur les exigences pr\u00e9cises en mati\u00e8re d'importation et d'exportation, communiquer avec votre <a href=\"/fra/1300462382369/1300462438912\" title=\"Bureaux de sant\u00e9 animale\">bureau de district de l'Agence canadienne d'inspection des aliments (ACIA)</a>.</p>\n\n<h2 class=\"h5\">Que fait-on pour prot\u00e9ger le b\u00e9tail canadien contre le virus du Nil occidental?</h2>\n\n<p>Le <abbr title=\"Virus du Nil occidental\">VNO</abbr> est une maladie \u00e0 notification imm\u00e9diate, ce qui signifie que les laboratoires doivent aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de tous les cas pr\u00e9sum\u00e9s ou diagnostiqu\u00e9s.</p>\n\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> consid\u00e8re dor\u00e9navant le <abbr title=\"Virus du Nil occidental\">VNO</abbr> comme \u00e9tant un virus indig\u00e8ne au Canada, cela implique qu'on le retrouve couramment au Canada. Votre v\u00e9t\u00e9rinaire soumettra des \u00e9chantillons \u00e0 un laboratoire provincial ou \u00e0 d'autres laboratoires \u00e0 des fins diagnostics. Des frais sont assortis \u00e0 ces services d'analyse.</p>\r\n<h2 class=\"font-medium black\">Renseignements additionnels</h2>\n\n<ul>\n<li><a href=\"/fra/1300462382369/1300462438912\">Bureaux de sant\u00e9 animale</a></li>\n</ul> \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}