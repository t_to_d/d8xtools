{
    "dcr_id": "1668523851898",
    "title": {
        "en": "Annual Report on Travel, Hospitality and Conference Expenditures",
        "fr": "Rapport annuel sur les d\u00e9penses de voyages, d'accueil et de conf\u00e9rences"
    },
    "html_modified": "2024-02-13 9:56:01 AM",
    "modified": "2022-12-05",
    "issued": "2022-12-05",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/disclosure_report_annual_travel_2021-2022_1668523851898_eng",
        "fr": "/default/main/inspection/cont-cont/aboutcfia-sujetacia/STAGING/templatedata/comn-comn/gene-gene/data/disclosure_report_annual_travel_2021-2022_1668523851898_fra"
    },
    "ia_id": "1668524127057",
    "parent_ia_id": "1580428768952",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1580428768952",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Annual Report on Travel, Hospitality and Conference Expenditures",
        "fr": "Rapport annuel sur les d\u00e9penses de voyages, d'accueil et de conf\u00e9rences"
    },
    "label": {
        "en": "Annual Report on Travel, Hospitality and Conference Expenditures",
        "fr": "Rapport annuel sur les d\u00e9penses de voyages, d'accueil et de conf\u00e9rences"
    },
    "templatetype": "content page 1 column",
    "node_id": "1668524127057",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1580428768545",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-the-cfia/transparency/proactive-disclosure/expenditure-report-2021-2022-/",
        "fr": "/a-propos-de-l-acia/transparence/divulgation-proactive/rapport-sur-les-depenses-2021-2022-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Annual Report on Travel, Hospitality and Conference Expenditures",
            "fr": "Rapport annuel sur les d\u00e9penses de voyages, d'accueil et de conf\u00e9rences"
        },
        "description": {
            "en": "As required by the Treasury Board Directive on Travel, Hospitality, Conferences and Event Expenditures, this report provides information on the total annual expenditures for each of travel, hospitality and conferences for the Canadian Food Inspection Agency for the fiscal year ending March 31, 2022.",
            "fr": "Comme l'exige la Directive sur les d\u00e9penses de voyage, d'accueil, de conf\u00e9rences et d'\u00e9v\u00e8nements du Conseil du Tr\u00e9sor, le pr\u00e9sent rapport fournit de l'information sur les d\u00e9penses annuelles totales de voyage, d'accueil et de conf\u00e9rences de l'Agence canadienne d'inspection des aliments (ACIA) pour l'exercice se terminant le 31 mars 2022."
        },
        "keywords": {
            "en": "annual report, travel, hospitality, conferences, directive, expenditures, 2021-2022",
            "fr": "rapport annuel, voyage, accueil, conf&#233;rences, directive, d&#233;penses, 2021-2022"
        },
        "dcterms.subject": {
            "en": "policy,regulation,travel",
            "fr": "politique,r\u00e9glementation,voyage"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-12-05",
            "fr": "2022-12-05"
        },
        "modified": {
            "en": "2022-12-05",
            "fr": "2022-12-05"
        },
        "type": {
            "en": "financial report",
            "fr": "rapport financier"
        },
        "audience": {
            "en": "government",
            "fr": "gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Annual Report on Travel, Hospitality and Conference Expenditures",
        "fr": "Rapport annuel sur les d\u00e9penses de voyages, d'accueil et de conf\u00e9rences"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Canadian Food Inspection Agency 2021 to 2022</h2>\n<p>As required by the Treasury Board <a href=\"https://www.tbs-sct.gc.ca/pol/doc-eng.aspx?id=27228\">Directive on Travel, Hospitality, Conferences and Event Expenditures</a>, this report provides information on the total annual expenditures for each of travel, hospitality and conferences for the Canadian Food Inspection Agency for the fiscal year ending March 31, 2022.</p>\n<p>Travel, hospitality and conferences expenditures incurred by federal department or agency relate to activities that support the department or agency's mandate and the government's priorities.</p>\n<p>In particular, for the CFIA this includes the delivery of the following core programs and/or services to Canadians:</p>\n<ul>\n<li>The CFIA is dedicated to safeguarding food safety and animal and plant health, which enhances Canada's environment, economy, and the health and well-being of its residents</li>\n<li>The CFIA administers and enforces 11 federal statutes and 23 sets of regulations. In addition to supporting a sustainable plant and animal resource base, these statutes and regulations regulate the safety and quality of food and agricultural inputs sold in Canada, such as feed, seed, fertilizers, and veterinary biologics </li>\n<li>The CFIA develops program requirements, conducts laboratory testing, and delivers inspections and other services in order to\n<ul>\n<li>prevent and manage food safety risks</li>\n<li>protect plant resources from regulated pests, diseases and invasive species</li>\n<li>prevent and manage animal and zoonotic diseases</li>\n<li>contribute to consumer protection</li>\n<li>contribute to market access for Canada's food, plants, animals and their products</li>\n</ul>\n</li>\n</ul>\n<p>Travel is essential for the delivery of these services.</p>\n<h2>Canadian Food Inspection Agency travel, hospitality and conference expenditures for year ending March 31, 2022</h2>\n\n<div class=\"table-responsive\">    \n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th id=\"tbl5\">Expenditure category</th>\n<th id=\"tbl6\">Expenditures for the year ending March 31, 2022 (in thousands of dollars)</th>\n<th id=\"tbl7\">Expenditures for the year ending March 31, 2021 (in thousands of dollars)</th>\n<th id=\"tbl8\">Variance<br>(in thousands of dollars)</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td colspan=\"4\" headers=\"tbl5 tbl6 tbl7 tbl8\">Travel</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Operational activities</td>\n<td headers=\"tbl6\" class=\"text-right\">3,280</td>\n<td headers=\"tbl7\" class=\"text-right\">2,237</td>\n<td headers=\"tbl8\" class=\"text-right\">1,043</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Key stakeholders<sup id=\"fn1-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>1</a></sup></td>\n<td headers=\"tbl6\" class=\"text-right\">(3)</td>\n<td headers=\"tbl7\" class=\"text-right\">2</td>\n<td headers=\"tbl8\" class=\"text-right\">(5)</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Internal governance</td>\n<td headers=\"tbl6\" class=\"text-right\">0</td>\n<td headers=\"tbl7\" class=\"text-right\">8</td>\n<td headers=\"tbl8\" class=\"text-right\">(8)</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Training</td>\n<td headers=\"tbl6\" class=\"text-right\">48</td>\n<td headers=\"tbl7\" class=\"text-right\">26</td>\n<td headers=\"tbl8\" class=\"text-right\">22</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Other</td>\n<td headers=\"tbl6\" class=\"text-right\">61</td>\n<td headers=\"tbl7\" class=\"text-right\">84</td>\n<td headers=\"tbl8\" class=\"text-right\">(23)</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">A. Total travel</td>\n<td headers=\"tbl6\" class=\"text-right\">3,386</td>\n<td headers=\"tbl7\" class=\"text-right\">2,357</td>\n<td headers=\"tbl8\" class=\"text-right\">1,029</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">B. Hospitality</td>\n<td headers=\"tbl6\" class=\"text-right\">12</td>\n<td headers=\"tbl7\" class=\"text-right\">2</td>\n<td headers=\"tbl8\" class=\"text-right\">10</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">C. Conference fees</td>\n<td headers=\"tbl6\" class=\"text-right\">143</td>\n<td headers=\"tbl7\" class=\"text-right\">73</td>\n<td headers=\"tbl8\" class=\"text-right\">70</td>\n</tr>\n<tr class=\"active\">\n<td headers=\"tbl5\">Total [A+B+C]</td>\n<td headers=\"tbl6\" class=\"text-right\">3,541</td>\n<td headers=\"tbl7\" class=\"text-right\">2,432</td>\n<td headers=\"tbl8\" class=\"text-right\">1,109</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">International travel by minister and minister's staff (included in travel)</td>\n<td headers=\"tbl6\" class=\"text-right\">0</td>\n<td headers=\"tbl7\" class=\"text-right\">0</td>\n<td headers=\"tbl8\" class=\"text-right\">0</td>\n</tr>\n</tbody>\n</table>\n</div>    \n<aside class=\"wb-fnote\" role=\"note\">\n<p id=\"fn-tnotes\" class=\"wb-inv\">Table Notes</p>\n<dl>\n<dt>Table Note 1</dt>\n<dd id=\"fn1\">\n<p>The negative amount for key stakeholder expenditures is due to credits received from previous year travel plans.</p>\n<p class=\"fn-rtn\"> <a href=\"#fn1-rf\"> <span class=\"wb-inv\">Return to table note\u00a0</span>1 <span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>   \n</dl>\n</aside> \n\n    \n<h2>Explanation of significant variances compared with previous fiscal year</h2>\n<h3>Total travel</h3>\n<p>In 2021 to 2022, CFIA incurred increases year over year in travel, hospitality and conference expenditures due to reduced COVID-19 restrictions and increased travel for operational activities due to the avian influenza outbreak. Overall, the Agency's travel, hospitality and conference expenditures are lower than pre-COVID-19 pandemic levels.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Agence Canadienne d'inspection des aliments 2021 \u00e0 2022</h2>\n<p>Comme l'exige la <a href=\"https://www.tbs-sct.gc.ca/pol/doc-fra.aspx?id=27228\">Directive sur les d\u00e9penses de voyage, d'accueil, de conf\u00e9rences et d'\u00e9v\u00e8nements</a> du Conseil du Tr\u00e9sor, le pr\u00e9sent rapport fournit de l'information sur les d\u00e9penses annuelles totales de voyage, d'accueil et de conf\u00e9rences de l'Agence canadienne d'inspection des aliments (ACIA) pour l'exercice se terminant le 31 mars 2022.</p>\n<p>Les d\u00e9penses de voyage, d'accueil et de conf\u00e9rences engag\u00e9es par un minist\u00e8re ou un organisme f\u00e9d\u00e9ral sont li\u00e9es \u00e0 des activit\u00e9s qui appuient le mandat du minist\u00e8re ou de l'organisme et les priorit\u00e9s gouvernementales.</p>\n<p>En particulier, dans le cas de l'ACIA, elles comprennent l'ex\u00e9cution des programmes et/ou services de base suivants pour les Canadiens\u00a0:</p>\n<ul>\n<li>L'ACIA veille \u00e0 la protection de la sant\u00e9 et du bien-\u00eatre des habitants, de l'environnement et de l'\u00e9conomie du Canada en assurant la salubrit\u00e9 des aliments, la sant\u00e9 des animaux et la protection des v\u00e9g\u00e9taux.</li>\n<li>L'ACIA applique 11 lois f\u00e9d\u00e9rales et 23 r\u00e8glements. En plus de contribuer au maintien des ressources v\u00e9g\u00e9tales et animales, ces lois et r\u00e8glements encadrent la salubrit\u00e9 et la qualit\u00e9 des aliments et des produits agricoles vendus au Canada, comme les aliments du b\u00e9tail, les semences, les engrais et les produits biologiques v\u00e9t\u00e9rinaires.</li>\n<li>L'ACIA \u00e9labore des exigences de programme, r\u00e9alise des analyses en laboratoire et offre des services d'inspection et autres pour\u00a0:\n<ul>\n<li>pr\u00e9venir et g\u00e9rer les risques li\u00e9s \u00e0 la salubrit\u00e9 des aliments;</li>\n<li>prot\u00e9ger les ressources v\u00e9g\u00e9tales contre les ravageurs, les maladies et les esp\u00e8ces envahissantes r\u00e9glement\u00e9s;</li>\n<li>pr\u00e9venir et g\u00e9rer les maladies animales et les zoonoses; </li>\n<li>contribuer \u00e0 la protection des consommateurs; </li>\n<li>faciliter l'acc\u00e8s aux march\u00e9s des aliments, des v\u00e9g\u00e9taux, des animaux et des produits connexes du Canada.</li>\n</ul>\n</li>\n</ul>\n<p>Les voyages sont essentiels pour assurer la prestation de ces services.</p>\n<h2>Agence canadienne d'inspection des aliments d\u00e9penses de voyages, d'accueil et de conf\u00e9rences pour l'exercice se terminant le 31 mars 2022</h2>\n\n<div class=\"table-responsive\">  \n<table class=\"table table-bordered\">\n<thead>\n<tr class=\"active\">\n<th id=\"tbl5\">Cat\u00e9gorie de d\u00e9penses</th>\n<th id=\"tbl6\">D\u00e9penses pour l'exercice se terminant le 31 mars 2022 (en milliers de dollars)</th>\n<th id=\"tbl7\">D\u00e9penses pour l'exercice se terminant le 31 mars 2021 (en milliers de dollars)</th>\n<th id=\"tbl8\">\u00c9cart<br>\n</th>\n</tr>\n</thead>\n<tbody>\n<tr>\n<td colspan=\"4\" headers=\"tbl5 tbl6 tbl7 tbl8\">Voyages</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Activit\u00e9s op\u00e9rationnelles</td>\n<td headers=\"tbl6\" class=\"text-right\">3\u00a0280</td>\n<td headers=\"tbl7\" class=\"text-right\">2\u00a0237</td>\n<td headers=\"tbl8\" class=\"text-right\">1\u00a0043</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Principaux intervenants<sup id=\"fn1-rf\"> <a class=\"fn-lnk\" href=\"#fn1\"> <span class=\"wb-inv\">Table Note\u00a0</span>1</a></sup></td>\n<td headers=\"tbl6\" class=\"text-right\">(3)</td>\n<td headers=\"tbl7\" class=\"text-right\">2</td>\n<td headers=\"tbl8\" class=\"text-right\">(5)</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Gouvernance interne</td>\n<td headers=\"tbl6\" class=\"text-right\">0</td>\n<td headers=\"tbl7\" class=\"text-right\">8</td>\n<td headers=\"tbl8\" class=\"text-right\">(8)</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Formation</td>\n<td headers=\"tbl6\" class=\"text-right\">48</td>\n<td headers=\"tbl7\" class=\"text-right\">26</td>\n<td headers=\"tbl8\" class=\"text-right\">22</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Autre</td>\n<td headers=\"tbl6\" class=\"text-right\">61</td>\n<td headers=\"tbl7\" class=\"text-right\">84</td>\n<td headers=\"tbl8\" class=\"text-right\">(23)</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">A. Total des d\u00e9penses de voyages</td>\n<td headers=\"tbl6\" class=\"text-right\">3\u00a0386</td>\n<td headers=\"tbl7\" class=\"text-right\">2\u00a0357</td>\n<td headers=\"tbl8\" class=\"text-right\">1\u00a0029</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">B. Accueil</td>\n<td headers=\"tbl6\" class=\"text-right\">12</td>\n<td headers=\"tbl7\" class=\"text-right\">2</td>\n<td headers=\"tbl8\" class=\"text-right\">10</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">C. Frais de conf\u00e9rences</td>\n<td headers=\"tbl6\" class=\"text-right\">143</td>\n<td headers=\"tbl7\" class=\"text-right\">73</td>\n<td headers=\"tbl8\" class=\"text-right\">70</td>\n</tr>\n<tr class=\"active\">\n<td headers=\"tbl5\">Total [A, B, et C]</td>\n<td headers=\"tbl6\" class=\"text-right\">3\u00a0541</td>\n<td headers=\"tbl7\" class=\"text-right\">2\u00a0432</td>\n<td headers=\"tbl8\" class=\"text-right\">1\u00a0109</td>\n</tr>\n<tr>\n<td headers=\"tbl5\">Voyages internationaux du ministre et du personnel du ministre</td>\n<td headers=\"tbl6\" class=\"text-right\">0</td>\n<td headers=\"tbl7\" class=\"text-right\">0</td>\n<td headers=\"tbl8\" class=\"text-right\">0</td>\n</tr>\n</tbody>\n</table>\n</div>    \n<aside class=\"wb-fnote\" role=\"note\">\n<p id=\"fn-tnotes\" class=\"wb-inv\">Table Notes</p>\n<dl>\n<dt>Table Note 1</dt>\n<dd id=\"fn1\">\n<p>Le montant n\u00e9gatif pour les d\u00e9penses des principaux intervenants est d\u00fb aux cr\u00e9dits re\u00e7us des plans de voyage de l'ann\u00e9e pr\u00e9c\u00e9dente.</p>\n<p class=\"fn-rtn\"> <a href=\"#fn1-rf\"> <span class=\"wb-inv\">Return to table note\u00a0</span>1 <span class=\"wb-inv\">\u00a0referrer</span></a></p>\n</dd>  \n</dl>\n</aside>\n  \n<h2>\u00c9carts importants par rapport \u00e0 l'exercice pr\u00e9c\u00e9dent</h2>\n<h3>Total des d\u00e9penses de voyages</h3>\n<p>De 2021 \u00e0 2022, l'ACIA a subi des augmentations d'une ann\u00e9e \u00e0 l'autre des d\u00e9penses de voyage, d'accueil et de conf\u00e9rence en raison de la r\u00e9duction des restrictions li\u00e9es \u00e0 la COVID-19 et de l'augmentation des d\u00e9placements pour les activit\u00e9s op\u00e9rationnelles en raison de l'\u00e9closion de grippe aviaire. Dans l'ensemble, les d\u00e9penses de voyage, d'accueil et de conf\u00e9rence de l'Agence sont inf\u00e9rieures aux niveaux d'avant la pand\u00e9mie de COVID-19.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}