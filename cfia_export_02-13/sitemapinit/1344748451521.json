{
    "dcr_id": "1344748344710",
    "title": {
        "en": "Avian Biosecurity \u2013 Protect Poultry, Prevent Disease",
        "fr": "Bios\u00e9curit\u00e9 aviaire\u00a0\u2013 Protection de la volaille et pr\u00e9vention des maladies"
    },
    "html_modified": "2024-02-13 9:47:40 AM",
    "modified": "2023-08-02",
    "issued": "2015-04-20 17:49:58",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_poultry_1344748344710_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_biosec_principles_poultry_1344748344710_fra"
    },
    "ia_id": "1344748451521",
    "parent_ia_id": "1323991018946",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Biosecurity",
        "fr": "Bios\u00e9curit\u00e9"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1323991018946",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Avian Biosecurity \u2013 Protect Poultry, Prevent Disease",
        "fr": "Bios\u00e9curit\u00e9 aviaire\u00a0\u2013 Protection de la volaille et pr\u00e9vention des maladies"
    },
    "label": {
        "en": "Avian Biosecurity \u2013 Protect Poultry, Prevent Disease",
        "fr": "Bios\u00e9curit\u00e9 aviaire\u00a0\u2013 Protection de la volaille et pr\u00e9vention des maladies"
    },
    "templatetype": "content page 1 column",
    "node_id": "1344748451521",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1323990856863",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/biosecurite-aviaire/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Avian Biosecurity \u2013 Protect Poultry, Prevent Disease",
            "fr": "Bios\u00e9curit\u00e9 aviaire\u00a0\u2013 Protection de la volaille et pr\u00e9vention des maladies"
        },
        "description": {
            "en": "Anyone who has contact with birds though commercial farming, backyard flocks or hobby farms, and/or provides services to poultry producers (e.g. poultry transporters, feed providers, catching crews, etc.), is encouraged to practice enhanced biosecurity procedures.",
            "fr": "On encourage quiconque entre en contact avec des oiseaux dans les grands \u00e9levages commerciaux, dans les petits \u00e9levages non commerciaux ou dans les fermes d'agr\u00e9ment ou offre des services aux \u00e9leveurs de volaille (p. ex. transporteurs de volailles, fournisseurs d'aliments pour animaux, \u00e9quipes de capture, etc.) \u00e0 adopter des proc\u00e9dures de bios\u00e9curit\u00e9 renforc\u00e9es."
        },
        "keywords": {
            "en": "animals, animal health, avian, biosecurity, poultry, disease, birds",
            "fr": "animaux, sant\u00e9 des animaux, oiseaux, bios\u00e9curit\u00e9, aviaire, volaille, maladies"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,livestock,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,b\u00e9tail,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-20 17:49:58",
            "fr": "2015-04-20 17:49:58"
        },
        "modified": {
            "en": "2023-08-02",
            "fr": "2023-08-02"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Avian Biosecurity \u2013 Protect Poultry, Prevent Disease",
        "fr": "Bios\u00e9curit\u00e9 aviaire\u00a0\u2013 Protection de la volaille et pr\u00e9vention des maladies"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Anyone who has contact with birds though commercial farming, small flocks or hobby farms, and/or provides services to poultry producers (for example poultry transporters, feed providers, catching crews, <abbr title=\"et cetera\">etc.</abbr>), is encouraged to practice enhanced biosecurity procedures.</p>\n<h2>Producers</h2>\n\n<p>The National Avian On-Farm Biosecurity Standard and Producer Guide support the development of farm-specific biosecurity protocols for sectors that do not participate in a provincial association or On-Farm Food Safety (OFFS) program.</p>\n\n<ul>\n\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/national-avian-on-farm-biosecurity-standard/eng/1528732756921/1528732872665\">National Avian On-Farm Biosecurity Standard</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/general-producer-guide/eng/1398640321596/1398640379048\">General Producer Guide - National Avian On-Farm Biosecurity Standard</a></li>\n\n<li><details><summary>Basic biosecurity principles for poultry</summary>\n<p class=\"mrgn-tp-md\"><strong>Isolation:</strong></p>\n<ul>\n<li>Only obtain new birds from reputable sources.</li>\n<li>Isolate sick birds from the rest of the flock.</li>\n<li>Limit the frequency of introducing new birds to the flock.</li>\n<li>Isolate any new birds or birds returning from shows and exhibits.</li>\n<li>Use all-in-all-out flock movement where possible.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Sanitation:</strong></p>\n<ul>\n<li>Routinely clean and disinfect buildings, poultry houses, equipment, clothing and footwear.</li>\n<li>Designate a cleaning area for vehicles and equipment.</li>\n<li>Promptly dispose of mortalities and damaged eggs.</li>\n<li>Use plastic crates to transport birds (easier to clean).</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Traffic control:</strong></p>\n<ul>\n<li>Control visitors' access to the flock.</li>\n<li>Prevent birds, rodents, pets and other animals from coming into contact with the flock.</li>\n<li>Require all visitors to wear clean boots, clothing and gloves.</li>\n<li>Maintain records of the movement of people, animals and equipment on and off the premises.</li>\n<li>Make sure all suppliers and other farm visitors follow your biosecurity measures.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Flock health management:</strong></p>\n<ul>\n<li>Monitor flock health daily.</li>\n<li>Employ veterinary services to help implement flock health programs.</li>\n<li>Maintain daily health records on your flock, detailing production levels, health concerns and treatments applied.</li>\n<li>Immediately report any signs of illness to your veterinarian or the nearest <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Program maintenance:</strong></p>\n<ul>\n<li>Train all staff in the application of your biosecurity program.</li>\n<li>Regularly monitor the effectiveness of the program.</li>\n<li>Be aware of any avian diseases in your area and adjust your biosecurity program to meet specific needs, as  required. </li>\n</ul>\n</details></li>\n</ul>\n\n<h2>Poultry Service Industry</h2>\n\n<p>The goal of the Poultry Service Industry Biosecurity Guide is to provide service sector personnel with a set of guidelines to use, both within their own company's biosecurity protocols and in collaboration with the producer, to limit the opportunity for introducing and spreading disease.</p>\n\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/standards-and-principles/poultry-service-industry/eng/1405696997866/1405697055478\">Poultry  Service Industry Biosecurity Guide</a></li>\n</ul>\n\n<h2>Pet bird/small flock owners</h2>\n\n<p>Pet bird/small flock owners are urged to take an active role in protecting their flocks by employing strict biosecurity measures on their property. Small flocks are at risk of contracting viruses like avian influenza, in particular if they have access to the outdoors and ponds or bodies of water known to be used by wild birds.</p>\n\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/backyard-flocks-and-pet-birds/eng/1323643634523/1323644740109\">Biosecurity for small flock and pet bird owners</a></li>\n</ul>\n\n<h2>Sources of avian diseases</h2>\n<p>Disease in poultry and other avian species can be spread in a number of ways, including:</p>\n\n<ul>\n<li>through diseased birds or birds carrying disease;</li>\n<li>through animals other than birds (farm animals, pets, wild birds and other wildlife, vermin and insects);</li>\n<li>on the clothing and shoes of visitors and employees moving from flock-to-flock;</li>\n<li>in contaminated feed, water, bedding and litter;</li>\n<li>from the carcasses of dead birds;</li>\n<li>on contaminated farm equipment and vehicles;</li>\n<li>through contact with neighbouring flocks; or</li>\n<li>in airborne particles and dust blown by the wind.</li>\n</ul>\n\n<h2>General biosecurity measures for producers and veterinarians</h2>\n\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/tools/checklist/eng/1362944949857/1362945111651\">Assess your biosecurity practices</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/veterinarians/eng/1344822097335/1344822345700\">Biosecurity for veterinarians</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/observe-animals/eng/1363819905802/1363844081398\">Monitor your animals</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/reporting-animal-diseases/eng/1363844638670/1363844792607\">See something? Say something \u2013 Reporting animal diseases</a></li>\n</ul>\n\n<h2>Industry Notices</h2>\n\n<ul>\n\n\n<li>2018-07-27\u00a0- <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/2018-07-27/eng/1532715665365/1532715665896\">Notice to Industry\u00a0\u2013 Revised voluntary National Avian On-Farm Biosecurity Standard for poultry producers</a></li>\n\n\n\n<li>2016-04-29 - <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/2016-04-29/eng/1461802767517/1461802896621\">Avian Producers Urged to Practise Biosecurity in Wake of U.S. Disease Detections and Migration</a></li>\n\n<li>2015-10-22 - <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/2015-10-22/eng/1445366353495/1445366354714\">Farm Biosecurity During Wild Bird Migration</a></li>\n<li>2014-04-07 - <a href=\"/animal-health/terrestrial-animals/biosecurity/2014-04-17/eng/1429223102524/1429223103227\">Strengthen On-Farm Biosecurity During Wild Bird Migration</a></li>\n</ul>\n\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/tools/eng/1344790074044/1344790183249\">Biosecurity brochures and videos</a> </li>\n<li><a href=\"/animal-health/terrestrial-animals/biosecurity/general-public/eng/1344753505401/1344753729248\">Biosecurity information for the general public</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/protecting-your-flock-from-influenza/eng/1461799560842/1461801401264\">Protecting Your Flock From Influenza \u2013 Have You Got It Right?</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=7#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>On encourage quiconque entre en contact avec des oiseaux dans les grands \u00e9levages commerciaux, dans les petits \u00e9levages non commerciaux ou dans les fermes d'agr\u00e9ment ou offre des services aux \u00e9leveurs de volaille (par exemple transporteurs de volailles, fournisseurs d'aliments pour animaux, \u00e9quipes de capture, <abbr title=\"et cetera\">etc.</abbr>) \u00e0 adopter des proc\u00e9dures de bios\u00e9curit\u00e9 renforc\u00e9es.</p>\n\n<h2>Les producteurs</h2>\n\n<p>La Norme nationale de bios\u00e9curit\u00e9 pour les fermes avicoles et le guide du producteur connexe visent \u00e0 appuyer l'\u00e9laboration de protocoles de bios\u00e9curit\u00e9 propres aux exploitations pour les secteurs qui ne font pas d\u00e9j\u00e0 partie d'une association provinciale ou qui ne participent pas \u00e0 un programme provincial de salubrit\u00e9 des aliments \u00e0 la ferme.</p>\n\n<ul>\n\n\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/norme-nationale-de-biosecurite-pour-les-fermes-avi/fra/1528732756921/1528732872665\">Norme nationale de bios\u00e9curit\u00e9 pour les fermes avicoles</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/guide-general-du-producteur/fra/1398640321596/1398640379048\">Guide g\u00e9n\u00e9ral du producteur\u00a0- Norme nationale de bios\u00e9curit\u00e9 pour les fermes avicoles</a></li>\n\n<li>\n\n<details>\n<summary>Principes de bios\u00e9curit\u00e9 de base pour le secteur avicole</summary>\n<p class=\"mrgn-tp-md\"><strong>Isolement\u00a0:</strong></p>\n<ul>\n<li>Ne recevoir que des oiseaux de sources fiables.</li>\n<li>Isoler les oiseaux malades du reste du troupeau.</li>\n<li>Limiter la fr\u00e9quence d'introduction de nouveaux oiseaux dans le troupeau.</li>\n<li>Isoler les nouveaux oiseaux ou ceux qui reviennent de foires ou d'expositions.</li>\n<li>Autant que possible, d\u00e9placer les oiseaux d'un enclos \u00e0 l'autre selon le principe du renouvellement int\u00e9gral.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Mesures sanitaires\u00a0:</strong></p>\n<ul>\n<li>Nettoyer et d\u00e9sinfecter r\u00e9guli\u00e8rement les b\u00e2timents, les poulaillers, l'\u00e9quipement, les v\u00eatements et les chaussures.</li>\n<li>R\u00e9server une zone pour le nettoyage des v\u00e9hicules et de l'\u00e9quipement.</li>\n<li>\u00c9liminer rapidement les oiseaux morts et les oeufs endommag\u00e9s.</li>\n<li>Utiliser des cageots en plastique (plus faciles \u00e0 nettoyer) pour le transport des oiseaux.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Contr\u00f4le de la circulation\u00a0:</strong></p>\n<ul>\n<li>Limiter l'acc\u00e8s des visiteurs au troupeau.</li>\n<li>Pr\u00e9venir les contacts entre les oiseaux du troupeau et les autres oiseaux, les rongeurs, les animaux de compagnie ou d'autres animaux.</li>\n<li>Exiger que tous les visiteurs portent des gants, des bottes et des v\u00eatements propres.</li>\n<li>Tenir des registres sur les d\u00e9placements des personnes, des animaux et de l'\u00e9quipement qui arrivent sur les lieux ou les quittent.</li>\n<li>Veiller \u00e0 ce que tous les fournisseurs et autres visiteurs se conforment aux mesures de bios\u00e9curit\u00e9.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Gestion de la sant\u00e9 du troupeau\u00a0:</strong></p>\n<ul>\n<li>Surveiller quotidiennement la sant\u00e9 du troupeau.</li>\n<li>Avoir recours aux services d'un v\u00e9t\u00e9rinaire pour la mise en oeuvre des programmes sanitaires.</li>\n<li>Tenir des registres quotidiens sur la sant\u00e9 du troupeau, y indiquer les niveaux de production, les pr\u00e9occupations relatives \u00e0 la sant\u00e9 et les traitements administr\u00e9s.</li>\n<li>Signaler imm\u00e9diatement tout signe de maladie \u00e0 votre v\u00e9t\u00e9rinaire ou au bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> le plus pr\u00e8s.</li>\n</ul>\n<p class=\"mrgn-tp-md\"><strong>Mise \u00e0 jour du programme\u00a0:</strong></p>\n<ul>\n<li>Former tout le personnel relativement \u00e0 l'application du programme de bios\u00e9curit\u00e9.</li>\n<li>V\u00e9rifier r\u00e9guli\u00e8rement l'efficacit\u00e9 du programme.</li>\n<li>Se tenir au courant des maladies aviaires qui touchent la r\u00e9gion et adapter le programme de bios\u00e9curit\u00e9 selon les besoins.</li>\n</ul>\n</details>\n</li>\n</ul>\n\n<h2>Secteur des services avicoles</h2>\n<p>L'objectif du Guide de bios\u00e9curit\u00e9 pour les fournisseurs de services \u00e0 l'industrie de la volaille est d'offrir un ensemble de lignes directrices que les fournisseurs de services peuvent utiliser en combinaison avec les protocoles de bios\u00e9curit\u00e9 de leur entreprise et avec ceux de l'\u00e9leveur en vue de limiter les risques d'introduction et de propagation de maladies.</p>\n\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/normes-et-principes/fournisseurs-de-services-de-l-industrie-de-la-vola/fra/1405696997866/1405697055478\">Guide de bios\u00e9curit\u00e9 pour les fournisseurs de services \u00e0 l'industrie de la volaille</a></li>\n</ul>\n\n<h2>Propri\u00e9taires d'oiseaux de compagnie et de petits troupeaux</h2>\n\n<p>On conseille vivement aux propri\u00e9taires d'oiseaux de compagnie et de petits troupeaux de jouer un r\u00f4le actif dans la protection de leur \u00e9levage en appliquant des mesures de bios\u00e9curit\u00e9 tr\u00e8s strictes. Les petits troupeaux risquent de contracter des virus comme l'influenza aviaire, en particulier si les oiseaux ont acc\u00e8s \u00e0 l'ext\u00e9rieur ainsi qu'\u00e0 des \u00e9tangs ou \u00e0 des plans d'eau fr\u00e9quent\u00e9s par des oiseaux sauvages.</p>\n\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/petits-elevages-et-oiseaux-de-compagnie/fra/1323643634523/1323644740109\">R\u00e8gles de bios\u00e9curit\u00e9 pour les propri\u00e9taires de petits \u00e9levages et d'oiseaux de compagnie</a></li>\n</ul>\n\n<h2>Sources de maladies aviaires</h2>\n\n<p>Les maladies chez les volailles et autres esp\u00e8ces aviaires peuvent se propager de nombreuses fa\u00e7ons, notamment\u00a0</p>\n\n<ul>\n<li>par des oiseaux morts ou des oiseaux porteurs d'une maladie;</li>\n<li>par des animaux autres que les oiseaux (animaux d'\u00e9levage, animaux de compagnie, oiseaux sauvages et autres esp\u00e8ces fauniques, vermine et insectes);</li>\n<li>sur les v\u00eatements et les chaussures des visiteurs et des employ\u00e9s qui passent d'un \u00e9levage \u00e0 l'autre;</li>\n<li>par les aliments pour animaux, l'eau et la liti\u00e8re contamin\u00e9s;</li>\n<li>par les carcasses d'oiseaux morts;</li>\n<li>par du mat\u00e9riel et des v\u00e9hicules agricoles contamin\u00e9s;</li>\n<li>par contact avec des \u00e9levages voisins; </li>\n<li>par des particules en suspension dans l'air ou la poussi\u00e8re souffl\u00e9e par le vent.</li>\n</ul>\n\n<h2>Mesures de bios\u00e9curit\u00e9 g\u00e9n\u00e9rale \u00e0 l'intention des producteurs et des v\u00e9t\u00e9rinaires</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/outils/liste-de-verification/fra/1362944949857/1362945111651\">\u00c9valuez vos pratiques de bios\u00e9curit\u00e9</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/veterinaires/fra/1344822097335/1344822345700\">Bios\u00e9curit\u00e9 et v\u00e9t\u00e9rinaires</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/observez-les-animaux/fra/1363819905802/1363844081398\">Observez vos animaux</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/declarez-les-maladies-animales/fra/1363844638670/1363844792607\">Vous observez quelque chose? Dites-le \u2013 D\u00e9clarez les maladies animales</a></li>\n</ul>\n\n<h2>Avis \u00e0 l'industrie</h2>\n<ul>\n\n<li>2018-07-27 <a href=\"/animal-health/terrestrial-animals/diseases/reportable/avian-influenza/avian-biosecurity/2018-07-27/eng/1532715665365/1532715665896\">Avis \u00e0 l'industrie\u00a0\u2013 Norme nationale de bios\u00e9curit\u00e9 pour les fermes avicoles</a></li>\n\n<li>2016-04-29 <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/biosecurite-aviaire/2016-04-29/fra/1461802767517/1461802896621\">Avis \u00e0 l'industrie - Les aviculteurs sont vivement encourag\u00e9s \u00e0 appliquer des mesures de bios\u00e9curit\u00e9 \u00e9tant donn\u00e9 les cas de maladie d\u00e9tect\u00e9s aux \u00c9tats-Unis et la migration actuelle des oiseaux sauvages</a> </li>\n<li>2015-10-22 <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/biosecurite-aviaire/2015-10-22/fra/1445366353495/1445366354714\">Avis \u00e0 l'industrie - Bios\u00e9curit\u00e9 \u00e0 la ferme pendant la migration des oiseaux sauvages</a></li>\n<li>2014-04-07 <a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/2015-04-17/fra/1429223102524/1429223103227\">Avis \u00e0 l'industrie - Renforcer la bios\u00e9curit\u00e9 \u00e0 la ferme pendant la p\u00e9riode de migration des oiseaux sauvages</a></li>\n</ul>\n<h2>Information additionnelle</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/outils/fra/1344790074044/1344790183249\">Publications et vid\u00e9o sur la bios\u00e9curit\u00e9</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/biosecurite/grand-public/fra/1344753505401/1344753729248\">Information sur la bios\u00e9curit\u00e9 pour le grand public</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/influenza-aviaire/biosecurite-aviaire/avez-vous-fait-ce-qu-il-faut-/fra/1461799560842/1461801401264\">Prot\u00e9gez votre troupeau contre l'influenza\u00a0\u2013  avez-vous fait ce qu'il faut? </a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}