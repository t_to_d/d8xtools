{
    "dcr_id": "1548468448817",
    "title": {
        "en": "Open science",
        "fr": "Science ouverte"
    },
    "html_modified": "2024-01-26 2:24:31 PM",
    "modified": "2021-10-28",
    "issued": "2019-02-11",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/open_science_1548468448817_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/open_science_1548468448817_fra"
    },
    "ia_id": "1548468449082",
    "parent_ia_id": "1494875261582",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1494875261582",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Open science",
        "fr": "Science ouverte"
    },
    "label": {
        "en": "Open science",
        "fr": "Science ouverte"
    },
    "templatetype": "content page 1 column",
    "node_id": "1548468449082",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1494875229872",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/science-and-research/open-science/",
        "fr": "/les-sciences-et-les-recherches/science-ouverte/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Open science",
            "fr": "Science ouverte"
        },
        "description": {
            "en": "Access CFIA open science information and data found on Canada.ca, available to everyone for free.",
            "fr": "Acc\u00e9dez \u00e0 l'information scientifique et aux donn\u00e9es de l'ACIA trouv\u00e9 sur Canada.ca, accessibles \u00e0 tous."
        },
        "keywords": {
            "en": "Open, Science, food, animal, plant, laboratories, veterinarians, scientists, research",
            "fr": "Science, ouverte, aliments, animaux, v\u00e9g\u00e9taux, laboratoires, v\u00e9t\u00e9rinaires, scientistes, recherche,"
        },
        "dcterms.subject": {
            "en": "laboratories,sciences,scientific research,veterinary medicine",
            "fr": "laboratoire,sciences,recherche scientifique,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-02-11",
            "fr": "2019-02-11"
        },
        "modified": {
            "en": "2021-10-28",
            "fr": "2021-10-28"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government,scientists",
            "fr": "entreprises,grand public,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Open science",
        "fr": "Science ouverte"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><a href=\"https://science.gc.ca/eic/site/063.nsf/eng/h_98054.html\">Open science</a> makes scientific information (including data, processes and publications) available to all, while respecting privacy, security, ethics and intellectual property protection. </p>\n\n<p>In\u00a02021, the CFIA launched its first Open Science Action Plan, a 5-year phased approach to how the CFIA will achieve the overall objectives of open science. The CFIA has also named a <a href=\"/eng/1323267988971/1323268082556#csdo\">Chief Scientific Data Officer</a> (CSDO) to lead its open science activities. </p>\n\n<h2>Open Science Action Plan\u00a02021-2026</h2>\t\n\t\n\t\n<div class=\"col-sm-6 pull-right\">\t\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cFIA_open_science_action_plan_1628879023381_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-5 col-sm-5 col-md-4 col-lg-3\">\n<p><img src=\"/DAM/DAM-comn-comn/WORKAREA/DAM-comn-comn/images-images/download_pdf_1558729521562_eng.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-4 col-sm-4 col-md-6 col-lg-6\">\n<p class=\"gc-dwnld-txt\"><span>CFIA Open Science Action Plan 2021-2026</span> <span class=\"gc-dwnld-info nowrap\">(PDF \u2013 1.1\u00a0mb)</span></p>\n</div></div>\n</div></a>\n</div>\n</div>\n\n</div>\n<h3>Summary</h3>\n<p>The CFIA <a href=\"/science-and-research/open-science/open-science-action-plan/eng/1628881261668/1628881261996\">Open Science Action Plan</a> describes how the CFIA aims to achieve the goals of <a href=\"https://science.gc.ca/eic/site/063.nsf/eng/h_98054.html\">open science</a>.</p>\t\t\n<p><a href=\"https://open.canada.ca/en/content/canadas-2018-2020-national-action-plan-open-government\">Canada's\u00a02018-2020 National Action Plan on Open Government</a> outlines\u00a010 commitments to help make the Government of Canada more open and transparent. Open science is one of these commitments. The Open Science Action Plan is guided by the federal <a href=\"https://www.ic.gc.ca/eic/site/063.nsf/eng/h_97992.html\">Roadmap for Open Science</a>. </p>\t\n<p>There are\u00a04 directions the CFIA will use to promote open science:</p>\t\n<ol>\n<li>New ideas</li>\n<li>Exchanging knowledge</li>\n<li>Working with data</li>\n<li>Scientific publications </li>\n</ol>\n<p>Each direction has its own priorities and actions to advance open science at the CFIA. Open science helps the CFIA share its scientific work with Canadians and makes the CFIA stronger as an agency.</p>\n<h2>Open science information and data</h2>\n<p>Access CFIA open science information and data found on Canada.ca, available to everyone for free.</p>\n<ul>\n<li>Search our <a href=\"https://open.canada.ca/data/en/dataset/0f0e5884-9c1e-4171-bd33-42bcee1f2a01\">peer-reviewed scientific publications</a></li>\n<li>Search our <a href=\"https://open.canada.ca/data/en/dataset?portal_type=dataset&amp;_organization_limit=0&amp;organization=cfia-acia\">open data</a></li>\n<li>Search <a href=\"https://profils-profiles.science.gc.ca/en/search/profiles/?f%5B0%5D=field_organization%3A774\">profiles of our scientists and researchers</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La <a href=\"https://science.gc.ca/eic/site/063.nsf/fra/h_98054.html\">science ouverte</a> met les informations scientifiques (les intrants, les r\u00e9sultats et les processus) \u00e0 la disposition de tous, tout en respectant la vie priv\u00e9e, la s\u00e9curit\u00e9, l'\u00e9thique et la protection de la propri\u00e9t\u00e9 intellectuelle.</p>\n<p>En\u00a02021, l'ACIA a lanc\u00e9 son premier Plan d'action pour la science ouverte. Le Plan est une approche progressive sur cinq ans pour d\u00e9terminer la fa\u00e7on dont l'ACIA atteindra les objectifs g\u00e9n\u00e9raux de la science ouverte. L'ACIA a \u00e9galement nomm\u00e9 un <a href=\"/fra/1323267988971/1323268082556#csdo\">dirigeant principal des donn\u00e9es scientifiques</a> (DPDS) pour diriger ses activit\u00e9s de science ouverte.</p>\n\n<h2>Plan d'action pour la science ouverte 2021 \u00e0 2026</h2>\t\n<div class=\"col-sm-6 pull-right mrgn-tp-lg\">\t\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/text-texte/cFIA_open_science_action_plan_1628879023381_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-5 col-sm-5 col-md-4 col-lg-3\">\n<p><img src=\"/DAM/DAM-comn-comn/WORKAREA/DAM-comn-comn/images-images/download_pdf_1558729521562_fra.png\" alt=\"Download PDF\"></p>\n</div>\n<div class=\"col-xs-4 col-sm-4 col-md-6 col-lg-6\">\n<p class=\"gc-dwnld-txt\"><span>Plan d'action pour la science ouverte de l'ACIA 2021 \u00e0 2026</span> <span class=\"gc-dwnld-info nowrap\">(PDF \u2013 1.2\u00a0mo)</span></p>\n</div></div>\n</div></a>\n</div>\n</div>\n\n</div>\n<h3>R\u00e9sum\u00e9</h3>\n<p>Le <a href=\"/les-sciences-et-les-recherches/science-ouverte/plan-d-action-pour-la-science-ouverte/fra/1628881261668/1628881261996\">Plan d'action pour la science ouverte</a> de l'ACIA d\u00e9crit comment l'ACIA vise \u00e0 atteindre les objectifs de la <a href=\"https://science.gc.ca/eic/site/063.nsf/fra/h_98054.html\">science ouverte</a></p>\t\n<p>Le <a href=\"https://ouvert.canada.ca/fr/contenu/plan-daction-national-du-canada-pour-un-gouvernement-ouvert-de-2018-2020\">Plan d'action national du Canada pour un gouvernement ouvert de\u00a02018-2020</a> pr\u00e9sente\u00a010 engagements pour rendre le gouvernement du Canada plus ouvert et transparent. La science ouverte est l'un de ces engagements. Le Plan d'action pour la science ouverte de l'ACIA est guid\u00e9 par la <a href=\"https://www.ic.gc.ca/eic/site/063.nsf/fra/h_97992.html\">Feuille de route pour la science ouverte</a> du gouvernement f\u00e9d\u00e9ral. </p>\n<p>L'ACIA adoptera 4 orientations pour promouvoir la science ouverte\u00a0:</p>\n<ol>\n<li>Nouvelles id\u00e9es</li>\n<li>\u00c9change des connaissances </li>\n<li>\u0152uvrer avec des donn\u00e9es</li>\n<li>Science et les publications scientifiques</li>\n</ol>\n<p>Chaque orientation a ses propres priorit\u00e9s et actions pour faire progresser la science ouverte \u00e0 l'ACIA. La science ouverte aide l'ACIA \u00e0 partager son travail scientifique avec les Canadiens et rend l'ACIA plus forte en tant qu'agence.</p>\n<h2>Informations et donn\u00e9es scientifiques ouvertes</h2>\n<p>Acc\u00e9dez \u00e0 l'information et aux donn\u00e9es scientifique ouvertes de l'ACIA trouv\u00e9es sur Canada.ca, accessibles \u00e0 tous.</p>\n<ul>\n<li>Recherchez nos <a href=\"https://ouvert.canada.ca/data/fr/dataset/0f0e5884-9c1e-4171-bd33-42bcee1f2a01\">publications scientifiques \u00e9valu\u00e9s par les pairs</a></li>\n<li>Recherchez nos <a href=\"https://ouvert.canada.ca/data/fr/dataset?portal_type=dataset&amp;_organization_limit=0&amp;organization=cfia-acia\">donn\u00e9es ouvertes</a></li>\n<li>Recherchez les <a href=\"https://profils-profiles.science.gc.ca/fr/recherche/profils?f%5B0%5D=field_organization%3A774\">profils de nos scientifiques et chercheurs</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}