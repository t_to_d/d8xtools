{
    "dcr_id": "1336682711769",
    "title": {
        "en": "Koi Herpesvirus Disease - Fact Sheet",
        "fr": "Herp\u00e8s virose de la carpe ko\u00ef - Fiche de renseignements"
    },
    "html_modified": "2012-05-10 16:45",
    "modified": "20-11-2013",
    "issued": "10-5-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/aqua_dis_koiherpesvirus_factsheet_1336682711769_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/aqua_dis_koiherpesvirus_factsheet_1336682711769_fra"
    },
    "parent_ia_id": "1336681772583",
    "ia_id": "1336682801247",
    "parent_node_id": "1336681772583",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Koi Herpesvirus Disease - Fact Sheet",
        "fr": "Herp\u00e8s virose de la carpe ko\u00ef - Fiche de renseignements"
    },
    "label": {
        "en": "Koi Herpesvirus Disease - Fact Sheet",
        "fr": "Herp\u00e8s virose de la carpe ko\u00ef - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1336682801247",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1336681689001",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/koi-herpesvirus/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/l-herpesvirose-de-la-carpe-koi/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Koi Herpesvirus Disease - Fact Sheet",
            "fr": "Herp\u00e8s virose de la carpe ko\u00ef - Fiche de renseignements"
        },
        "description": {
            "en": "Koi herpesvirus disease has been found in common carp (Cyprinus carpio) and koi carp (Cyprinus carpio koi).",
            "fr": "L?herp\u00e8svirose de la carpe ko\u00ef a \u00e9t\u00e9 d\u00e9pist\u00e9e chez la carpe commune (Cyprinus carpio) et la carpe ko\u00ef (Cyprinus carpio koi)."
        },
        "keywords": {
            "en": "Koi herpesvirus, infectious disease, finfish, reportable disease, aquatic animals, factsheet, carp",
            "fr": "herp\u00e8svirose de la carpe ko\u00ef, maladie, touche les poissons, carp, animaux aquatiques, Maladies d\u00e9clarables, carpe, Fiche de renseignements"
        },
        "dcterms.subject": {
            "en": "inspection,infectious diseases,fish,fisheries products,animal health",
            "fr": "inspection,maladie infectieuse,poisson,produit de la peche,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Aquatic Animal Health Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux aquatiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-05-10 16:45:13",
            "fr": "2012-05-10 16:45:13"
        },
        "modified": {
            "en": "2013-11-20",
            "fr": "2013-11-20"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Koi Herpesvirus Disease - Fact Sheet",
        "fr": "Herp\u00e8s virose de la carpe ko\u00ef - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">What is koi herpesvirus disease?</h2>\n<p>Koi herpesvirus disease is an infectious disease of finfish. It is caused by the koi herpesvirus, which belongs to the family <span lang=\"la\"><i>Herpesviridae</i></span>.</p>\n<h2 class=\"h5\">What species of finfish can be infected by koi herpesvirus disease?</h2>\n<p>Koi herpesvirus disease has been found in common carp (<span lang=\"la\"><i>Cyprinus carpio</i></span>) and koi carp (<span lang=\"la\"><i>Cyprinus carpio koi</i></span>).</p>\n<h2 class=\"h5\">Is koi herpesvirus disease a risk to human health?</h2>\n<p>No. The causal agent of koi herpesvirus is not a risk to human health.</p>\n<h2 class=\"h5\">What are the signs of koi herpesvirus disease?</h2>\n<p>Koi herpesvirus disease is a cause of death in hatched life stages of common and koi carp. Infected animals can die within hours of the appearance of disease.</p>\n<p>Affected finfish may exhibit any of the following signs:</p>\n<ul>\n<li>behaviour \t\t\t \n<ul>\n<li>abnormal swimming patterns (slow swimming at surface, erratic swimming)</li>\n<li>gasping</li>\n</ul>\n</li>\n<li>appearance \t\t\t \n<ul>\n<li>pale gills or brown patches in the gills</li>\n<li>over- or under-production of mucus on the gills and skin</li>\n<li>areas of bleeding in the gills and skin</li>\n<li>sunken eyes</li>\n<li>swollen kidney</li>\n<li>swollen spleen</li>\n</ul>\n</li>\n</ul>\n<h2 class=\"h5\">Is koi herpesvirus disease found in Canada?</h2>\n<p>Yes. In Canada, koi herpesvirus disease has been found in Ontario and Manitoba.</p>\n<h2 class=\"h5\">How is koi herpesvirus spread?</h2>\n<p>Koi herpesvirus is spread between finfish by</p>\n<ul>\n<li>contaminated water, and</li>\n<li>contaminated equipment.</li>\n</ul>\n<p>People can spread koi herpesvirus by moving any of the following:</p>\n<ul>\n<li>infected live or dead finfish,</li>\n<li>contaminated equipment, or</li>\n<li>contaminated water.</li>\n</ul>\n<h2 class=\"h5\">How is koi herpesvirus disease diagnosed?</h2>\n<p>Diagnosing koi herpesvirus disease requires laboratory testing. Not all infected finfish show signs of disease.</p>\n<h2 class=\"h5\">How is koi herpesvirus disease treated?</h2>\n<p>There are no treatment options currently available for koi herpesvirus disease.</p>\n<h2 class=\"h5\">What measures can be taken to prevent the introduction and spread of koi herpesvirus disease?</h2>\n<p>If you frequently handle or work with finfish, be aware of the clinical signs of koi herpesvirus disease.</p>\n<p>Do not import live infected finfish into Canada.</p>\n<ul>\n<li>An import permit is required from the Canadian Food Inspection Agency (CFIA) for certain species of finfish as of December 2011.</li>\n<li>People bringing finfish into Canada should check other federal, provincial, and/or territorial requirements before entering the country.</li>\n</ul>\n<p>Do not introduce live finfish from another country into the natural waters of Canada.</p>\n<ul>\n<li>People releasing finfish into the natural waters or in rearing facilities within Canada should check if federal or provincial and/or territorial permits are required.</li>\n</ul>\n<p>If you frequently handle or work with finfish, be aware of where koi herpesvirus occurs in your area.</p>\n<ul>\n<li>A federal, provincial and/or territorial permit or licence may be required to relocate finfish within Canada.</li>\n</ul>\n<p>Do not use finfish bought in a grocery store as bait for catching finfish or other aquatic animals.</p>\n<p>When cleaning and gutting finfish, dispose of all finfish waste in your municipal garbage.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> recommends that you do not visit Canadian aquaculture sites, zoos or aquariums for 14 days if you have travelled to another country and</p>\n<ul>\n<li>visited an aquaculture site, or</li>\n<li>had contact with wild finfish.</li>\n</ul>\n<p>Wash and disinfect the footwear you wore to the site or when you had contact with wild finfish. Also wash your clothing thoroughly and dry it at a high temperature.</p>\n<h2 class=\"h5\">What is done to protect Canadian aquatic animals from koi herpesvirus disease?</h2>\n<p>Koi herpesvirus disease is a reportable disease in Canada. This means that anyone who owns or works with aquatic animals, who knows of or suspects koi herpesvirus disease in their fish, is required by law to notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n<p>If koi herpesvirus disease is found in Canada, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would control its spread by implementing disease response activities. These may include</p>\n<ul>\n<li>controlling the movements of infected animals that people own or work with</li>\n<li>humanely destroying infected animals</li>\n<li>cleaning and disinfecting</li>\n</ul>\n<p>The control measures chosen would depend on the situation.</p>\n<h2 class=\"h5\">What do I do if I think a finfish that I am raising or keeping have koi herpesvirus disease?</h2>\n<p>If you suspect a finfish that you are raising or keeping may have koi herpesvirus disease, you are required under the <i>Health of Animals Act</i> to immediately notify the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\r\n<h2 class=\"font-medium black\">How do I get more information</h2>\n<p>For more information about reportable diseases, visit the <a href=\"/animal-health/aquatic-animals/eng/1299155892122/1320536294234\">Aquatic Animal Health</a> page, contact your <a href=\"/eng/1300462382369/1365216058692\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Animal Health Office</a>,  or your <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area office:</p>\n\n<ul><li>Atlantic: 506-777-3939</li>\n<li>Quebec: 514-283-8888</li>\n<li>Ontario: 226-217-8555</li>\n<li>West: 587-230-2200</li></ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Qu'est-ce que l'herp\u00e8s virose de la carpe ko\u00ef?</h2>\n<p>L'herp\u00e8s virose de la carpe ko\u00ef est une maladie qui touche les poissons. Elle est caus\u00e9e par l'herp\u00e8s virose qui appartient \u00e0 la famille des <span lang=\"la\"><i>herpesviridae</i></span>.</p>\n<h2 class=\"h5\">Quelles esp\u00e8ces de poissons peuvent \u00eatre infect\u00e9es par l'herp\u00e8s virose de la carpe ko\u00ef?</h2>\n<p>L'herp\u00e8s virose de la carpe ko\u00ef a \u00e9t\u00e9 d\u00e9pist\u00e9e chez la carpe commune (<span lang=\"la\"><i>Cyprinus carpio</i></span>) et la carpe ko\u00ef (<span lang=\"la\"><i>Cyprinus carpio koi</i></span>).</p>\n<h2 class=\"h5\">L'herp\u00e8s virose de la carpe ko\u00ef pr\u00e9sente-t-elle un risque pour la sant\u00e9 humaine?</h2>\n<p>Non. L'herp\u00e8s virose de la carpe ko\u00ef ne pr\u00e9sente aucun risque pour la sant\u00e9 humaine.</p>\n<h2 class=\"h5\">Quels sont les signes de l'herp\u00e8s virose de la carpe ko\u00ef?</h2>\n<p>L'herp\u00e8s virose de la carpe ko\u00ef peut entra\u00eener la mort chez les carpes et les carpes ko\u00ef, apr\u00e8s \u00e9closion des \u0153ufs. Les sujets infect\u00e9s meurent dans les heures suivant l'apparition de la maladie.</p>\n<h2 class=\"h5\">Voici les signes que peuvent pr\u00e9senter les poissons infect\u00e9s.</h2>\n<ul>\n<li>Comportement \t\t\t \n<ul>\n<li>Anomalies du comportement natatoire (nage lente en surface, manque de coordination pendant la nage)</li>\n<li>Prise d'air</li>\n</ul>\n</li>\n<li>Apparence \t\t\t \n<ul>\n<li>P\u00e2leur des branchies ou taches blanches sur les branchies</li>\n<li>Surproduction ou sous-production de mucus au niveau des branchies et de la peau</li>\n<li>Zones d'h\u00e9morragie au niveau des branchies et de la peau</li>\n<li>Yeux enfonc\u00e9s dans les orbites</li>\n<li>Gonflement des reins</li>\n<li>Gonflement de la rate</li>\n</ul>\n</li>\n</ul>\n<h2 class=\"h5\">L'herp\u00e8s virose de la carpe ko\u00ef est-elle pr\u00e9sente au Canada?</h2>\n<p>Oui. Au Canada, on a recens\u00e9 des cas d'herp\u00e8s virose de la carpe ko\u00ef en Ontario et au Manitoba.</p>\n<h2 class=\"h5\">Comment l'herp\u00e8s virose de la carpe ko\u00ef se propage-t-elle?</h2>\n<p>L'herp\u00e8s virose de la carpe ko\u00ef est transmise d'un poisson \u00e0 l'autre par l'interm\u00e9diaire :</p>\n<ul>\n<li>d'eau contamin\u00e9e;</li>\n<li>d'\u00e9quipement contamin\u00e9.</li>\n</ul>\n<p>Les \u00eatres humains peuvent propager l'herp\u00e8s virose de la carpe ko\u00ef en d\u00e9pla\u00e7ant :</p>\n<ul>\n<li>des poissons infect\u00e9s, qu'ils soient vivants ou morts;</li>\n<li>de l'\u00e9quipement contamin\u00e9;</li>\n<li>de l'eau contamin\u00e9e.</li>\n</ul>\n<h2 class=\"h5\">De quelle fa\u00e7on l'herp\u00e8s virose de la carpe ko\u00ef est-elle diagnostiqu\u00e9e?</h2>\n<p>Pour diagnostiquer l'herp\u00e8s virose de la carpe ko\u00ef, il faut effectuer des \u00e9preuves de laboratoire. Les poissons infect\u00e9s ne pr\u00e9senteront pas tous des signes de la maladie.</p>\n<h2 class=\"h5\">De quelle fa\u00e7on l'herp\u00e8s virose de la carpe ko\u00ef est-elle trait\u00e9e?</h2>\n<p>\u00c0 l'heure actuelle, il n'existe pas de traitement pour l'herp\u00e8s virose de la carpe ko\u00ef.</p>\n<h2 class=\"h5\">Que peut-on faire pour pr\u00e9venir la propagation de l'herp\u00e8s virose de la carpe ko\u00ef?</h2>\n<p>Si vous travaillez souvent avec des poissons, ou si vous les manipulez, sachez reconna\u00eetre les sympt\u00f4mes de l'herp\u00e8s virose de la carpe ko\u00ef.</p>\n<p>N'importez pas de poissons infect\u00e9s vivants au Canada.</p>\n<ul>\n<li>\u00c0 compter de d\u00e9cembre 2011, un permis d'importation sera requis par l'Agence canadienne d'inspection des aliments (ACIA) pour certaines esp\u00e8ces de poissons.</li>\n<li>Les personnes qui rapportent des poissons au Canada devraient v\u00e9rifier les autres exigences f\u00e9d\u00e9rales, provinciales et territoriales avant d'importer des poissons au Canada ou d'entrer au Canada avec ces poissons.</li>\n</ul>\n<p>N'introduisez pas de poissons vivants provenant d'un autre pays ou d'une autre province dans les plans d'eau naturels du Canada.</p>\n<ul>\n<li>Les personnes qui rel\u00e2chent des poissons dans des plans d'eau naturels ou des \u00e9tablissements de grossissement du Canada devraient v\u00e9rifier si elles ont besoin d'un permis f\u00e9d\u00e9ral ou provincial ou territorial.</li>\n</ul>\n<p>Si vous travaillez souvent avec des poissons, ou si vous les manipulez, soyez inform\u00e9 des endroits o\u00f9 s\u00e9vit l'herp\u00e8s virose de la carpe ko\u00ef dans votre r\u00e9gion.</p>\n<ul>\n<li>Le d\u00e9placement de poissons d'un endroit \u00e0 l'autre au Canada peut n\u00e9cessiter une licence ou un permis f\u00e9d\u00e9ral, provincial ou territorial.</li>\n</ul>\n<p>N'utilisez pas de poissons achet\u00e9s au supermarch\u00e9 comme app\u00e2ts pour attraper du poisson ou d'autres animaux aquatiques.</p>\n<p>Lorsque vous \u00e9visc\u00e9rez et nettoyez un poisson, jetez tous les d\u00e9chets issus de l'animal aux ordures.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> vous recommande de ne pas visiter d'\u00e9tablissements aquacoles, de zoos ou d'aquariums canadiens pendant 14 jours si vous avez voyag\u00e9 dans un autre pays et si vous :</p>\n<ul>\n<li>avez visit\u00e9 un \u00e9tablissement aquacole;</li>\n<li>\u00eates entr\u00e9 en contact avec des poissons sauvages.</li>\n</ul>\n<p>De retour au Canada, lavez et d\u00e9sinfectez les chaussures que vous portiez pour vous rendre \u00e0 l'\u00e9tablissement ou lorsque vous \u00eates entr\u00e9 en contact avec des poissons sauvages. Lavez \u00e9galement soigneusement vos v\u00eatements et s\u00e9chez-les \u00e0 temp\u00e9rature \u00e9lev\u00e9e.</p>\n<h2 class=\"h5\">Quelles sont les mesures prises pour prot\u00e9ger les animaux aquatiques du Canada contre l'herp\u00e8s virose de la carpe ko\u00ef?</h2>\n<p>Au Canada, l'herp\u00e8s virose de la carpe ko\u00ef est une maladie d\u00e9clarable. Par cons\u00e9quent, quiconque poss\u00e8de des animaux aquatiques ou travaille avec de tels animaux et soup\u00e7onne ou d\u00e9c\u00e8le la pr\u00e9sence de l'herp\u00e8s virose de la carpe ko\u00ef chez les animaux qu'il poss\u00e8de ou avec lesquels il travaille est tenu par la loi d'en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n<p>Si on d\u00e9c\u00e8le la pr\u00e9sence de l'herp\u00e8s virose de la carpe ko\u00ef, l'ACIA contr\u00f4lera sa propagation en mettant en \u0153uvre des activit\u00e9s d'intervention en cas de maladie, par exemple :</p>\n<ul>\n<li>contr\u00f4ler les d\u00e9placements des animaux infect\u00e9s que les personnes poss\u00e8dent ou avec lesquels elles travaillent;</li>\n<li>d\u00e9truire sans cruaut\u00e9 les animaux infect\u00e9s;</li>\n<li>nettoyer et d\u00e9sinfecter.</li>\n</ul>\n<p>Les mesures de contr\u00f4le retenues d\u00e9pendront de la situation.</p>\n<h2 class=\"h5\">Que dois-je faire si je crois que les poissons que j'\u00e9l\u00e8ve ou que je garde sont atteints par l'herp\u00e8s virose de la carpe ko\u00ef?</h2>\n<p>Si vous avez des motifs de soup\u00e7onner qu'un poisson que vous \u00e9levez ou gardez est atteint par l'herp\u00e8s virose de la carpe ko\u00ef, vous devez en aviser l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> imm\u00e9diatement, conform\u00e9ment \u00e0 la <i>Loi sur la sant\u00e9 des animaux</i>.</p>\r\n<h2 class=\"font-medium black\">Comment puis-je obtenir davantage de renseignements</h2>\n<p>Pour de plus amples renseignements sur les maladies d\u00e9clarables, consultez la page <a href=\"/sante-des-animaux/animaux-aquatiques/fra/1299155892122/1320536294234\">Sant\u00e9 des animaux aquatiques</a>,  communiquez avec votre <a href=\"/fra/1300462382369/1365216058692\">bureau local de sant\u00e9 des animaux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou  le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de votre centre op\u00e9rationnel\u00a0: </p>\n<ul><li>Atlantique : 506-777-3939</li>\n<li>Qu\u00e9bec : 514-283-8888</li>\n<li>Ontario : 226-217-8555</li>\n<li>Ouest : 587-230-2200</li></ul>\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}