{
    "dcr_id": "1320834319536",
    "title": {
        "en": "Import policies: general",
        "fr": "Politiques pour <span class=\"nowrap\">l'importation :</span> g\u00e9n\u00e9ral"
    },
    "html_modified": "2024-03-12 3:55:27 PM",
    "modified": "2020-12-16",
    "issued": "2011-11-09 05:25:21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_import_pol_gen_1320834319536_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/terr_anima_import_pol_gen_1320834319536_fra"
    },
    "ia_id": "1321264109249",
    "parent_ia_id": "1320806678417",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Imports",
        "fr": "Importations"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1320806678417",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Import policies: general",
        "fr": "Politiques pour <span class=\"nowrap\">l'importation :</span> g\u00e9n\u00e9ral"
    },
    "label": {
        "en": "Import policies: general",
        "fr": "Politiques pour <span class=\"nowrap\">l'importation :</span> g\u00e9n\u00e9ral"
    },
    "templatetype": "content page 1 column",
    "node_id": "1321264109249",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1320806430753",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/imports/import-policies/general/",
        "fr": "/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/general/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Import policies: general",
            "fr": "Politiques pour l'importation : g\u00e9n\u00e9ral"
        },
        "description": {
            "en": "Returns and Permit Application Process for Canadian Animals, Semen, Embryos, Animal Products, Animal By-Products, and Finished Pet Food Return and Permit Application Process",
            "fr": "Processus de retour et de demande de permis visant les animaux, les semences, les embryons, les produits animaux, les sous-produits animaux et les aliments finis pour animaux de compagnie export\u00e9s du Canada."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, animal health, imports, policy, animal products, animal by-products, animal products, Health of Animals Act, import permit, animal or thing, TAHD National Headquarters, live animals, semen and embryos",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, sant\u00e9 des animaux, importation, politique, sant\u00e9 des animaux, produits d'origine animale, permis d'importation, vertu de l'article, agent v\u00e9t\u00e9rinaire, animaux vivants, semence, embryons"
        },
        "dcterms.subject": {
            "en": "animal health,imports,inspection,livestock,policy,veterinary medicine",
            "fr": "sant\u00e9 animale,importation,inspection,b\u00e9tail,politique,m\u00e9decine v\u00e9t\u00e9rinaire"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-11-09 05:25:21",
            "fr": "2011-11-09 05:25:21"
        },
        "modified": {
            "en": "2020-12-16",
            "fr": "2020-12-16"
        },
        "type": {
            "en": "policy,reference material",
            "fr": "politique,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Import policies: general",
        "fr": "Politiques pour l'importation : g\u00e9n\u00e9ral"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Specific import requirements, based on the applicable animal health policies, can be reviewed in the <a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System (AIRS)</a>.</p>\n<ul>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2005-9/eng/1321066760292/1426255335689\">Bovine Spongiform Encephalopathy Import Policy for Bovine Animals and Their Products and By-Products</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2002-3/eng/1321037138426/1577737753877\">Import Reference Document (As referenced in the <i>Health of Animals Regulations</i>)</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2002-17/eng/1321050654899/1323826743862\">International Waste Directive</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2003-3-9/eng/1321065624928/1323826579004\">Request to Import a New Commodity or Import from a New Country of Origin</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2010-9/eng/1321083659536/1434134370802\">Returns and Permit Application Process for Canadian Animals, Semen, Embryos, Animal Products, Animal By-Products, and Finished Pet Food</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=35#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Exigences sp\u00e9cifiques \u00e0 l'importation, sur la base des politiques de sant\u00e9 animale applicables, peuvent \u00eatre examin\u00e9s dansle <a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a>.</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/politiques-pour-l-importation/general/2003-3-9/fra/1321065624928/1323826579004\">Demande d'importation de nouveau produit ou d'importation d'un nouveau pays d'origine</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2002-17/eng/1321050654899/1323826743862\">Directive relative aux d\u00e9chets internationaux</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2002-3/eng/1321037138426/1577737753877\">Document de r\u00e9f\u00e9rence relatif \u00e0 l'importation (tel que mentionn\u00e9 dans le <i>R\u00e8glement sur la sant\u00e9 des animaux</i>)</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2005-9/eng/1321066760292/1426255335689\">Politiques d'importation sur l'enc\u00e9phalopathie spongiforme bovine et applicable aux bovins, \u00e0 leurs produits et sous-produits</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/import-policies/general/2010-9/eng/1321083659536/1434134370802\">Processus de retour et de demande de permis visant les animaux, les semences, les embryons, les produits animaux, les sous-produits animaux et les aliments finis pour animaux de compagnie export\u00e9s du Canada</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}