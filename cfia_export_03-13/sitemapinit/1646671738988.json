{
    "dcr_id": "1646671738613",
    "title": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Overview",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Aper\u00e7u"
    },
    "html_modified": "2024-03-12 3:55:19 PM",
    "modified": "2023-10-26",
    "issued": "2022-08-09",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/research_auth_fert_act_new_regs-overview_1646671738613_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/research_auth_fert_act_new_regs-overview_1646671738613_fra"
    },
    "ia_id": "1646671738988",
    "parent_ia_id": "1299165914316",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1299165914316",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Overview",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Aper\u00e7u"
    },
    "label": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Overview",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Aper\u00e7u"
    },
    "templatetype": "content page 1 column",
    "node_id": "1646671738988",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299165827648",
    "orig_type_name": "gene-gene",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/overview/",
        "fr": "/protection-des-vegetaux/engrais/apercu/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Research authorizations under the Fertilizers Act and regulations: Overview",
            "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Aper\u00e7u"
        },
        "description": {
            "en": "Most supplements and some fertilizers need a registration and pre-market assessment before importation or sale in Canada.",
            "fr": "La plupart des suppl\u00e9ments et certains engrais doivent faire l'objet d'un enregistrement et d'une \u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 avant de pouvoir \u00eatre import\u00e9s ou vendus au Canada."
        },
        "keywords": {
            "en": "Research, authorizations, New, Fertilizers Act, Regulations, Overview",
            "fr": "recherche, autorisations, nouveau, Loi sur les engrais, r\u00e8glement d'application, Aper\u00e7u"
        },
        "dcterms.subject": {
            "en": "crops,fertilizers,inspection,plants,policy",
            "fr": "cultures,engrais,inspection,plante,politique"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-08-09",
            "fr": "2022-08-09"
        },
        "modified": {
            "en": "2023-10-26",
            "fr": "2023-10-26"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Research authorizations under the Fertilizers Act and regulations: Overview",
        "fr": "Autorisations de recherche en vertu de la Loi sur les engrais et son r\u00e8glement d'application\u00a0: Aper\u00e7u"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">1. Overview</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646672002174/1646672002486\">2. Eligibility requirements</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/before-you-apply/eng/1646672025348/1646672025708\">3. Before you apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/how-to-apply/eng/1646672046841/1646672047216\">4. How to apply</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/after-you-apply/eng/1646672201606/1646672201950\">5. After you apply</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/plant-health/fertilizers/amending-and-renewing/eng/1646672246507/1646672246851\">6. Amending and renewing</a></li>\n</ul>\n</div>\n</div>\n\n<div class=\"pull-right col-sm-4 mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Fees</h2>\n</header>\n<div class=\"panel-body\">\n<p>Refer to <a href=\"/eng/1582641645528/1582641871296#c6\">Part\u00a05 of the Canadian Food Inspection Agency (CFIA) Fees Notice</a> for Fertilizers Fees</p>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Processing time</h2>\n</header>\n<div class=\"panel-body\">\n<p>90\u00a0days (not including industry response time).</p>\n</div>\n</section>\n</div>\n\n<h2>What are research authorizations</h2>\n\n<p>Most supplements and some fertilizers need a registration and pre-market assessment before importation or sale in Canada. Companies sometimes opt to conduct research trials on fertilizers or supplements to evaluate their field performance before applying to register a given product under the <i>Fertilizers Act</i>.</p>\n\n<p>The <i>Fertilizers Regulations</i> contain provisions that allow persons to conduct scientific trials of unregistered fertilizers and supplements that would otherwise require registration before importation or environmental release, as long as strict criteria are adhered to.</p>\n\n<p>The objective of the regulatory requirements in place for research is to:</p>\n\n<ul>\n<li>make sure that the environmental release of unregistered fertilizers (not exempt from registration) and novel supplements is safe</li>\n<li>define conditions for product use, handling and disposal to reduce the risks to human and animal health as well as the environment</li>\n</ul>\n\n<h2>Related links</h2>\n<ul>\n<li><a href=\"/plant-health/fertilizers/trade-memoranda/t-4-103/eng/1307855399697/1320243929540\">T-4-103\u00a0\u2013\u00a0Conducting research under the <i>Fertilizers Act</i></a></li>\n<li><a href=\"/about-the-cfia/find-a-form/form-cfia-acia-5475/eng/1565272872929/1565272873179\">Application for Research Authorization <span class=\"nowrap\">(CFIA/ACIA 5475)</span></a></li>\n<li><a href=\"/plant-health/fertilizers/overview/checklist/eng/1490995155973/1490995340847\">Submission Completeness Checklist for Research Authorizations</a></li>\n</ul>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Document navigation</h3>\n<ul class=\"pager\">\n<li class=\"next\"><a href=\"/plant-health/fertilizers/eligibility-requirements/eng/1646672002174/1646672002486\" rel=\"next\">Next</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"gc-stp-stp\">\n<div class=\"row\">\n<ul class=\"toc lst-spcd col-md-12\">\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item active\">1. Aper\u00e7u</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646672002174/1646672002486\">2. Conditions d'\u00e9ligibilit\u00e9</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/avant-de-presenter-une-demande/fra/1646672025348/1646672025708\">3. Avant de pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-md clr-lft-lg\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/comment-presenter-une-demande/fra/1646672046841/1646672047216\">4. Comment pr\u00e9senter une demande</a></li>\n<li class=\"col-md-4 col-sm-6 clr-lft-sm\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/apres-avoir-presente-une-demande/fra/1646672201606/1646672201950\">5. Apr\u00e8s avoir pr\u00e9sent\u00e9 une demande</a></li>\n<li class=\"col-md-4 col-sm-6\"><a class=\"list-group-item\" href=\"/protection-des-vegetaux/engrais/modifier-et-renouveler/fra/1646672246507/1646672246851\">6. Modifier et renouveler</a></li>\n</ul>\n</div>\n</div>\n\n<div class=\"pull-right col-sm-4 mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Frais</h2>\n</header>\n<div class=\"panel-body\">\n<p>Veuillez consulter la <a href=\"/fra/1582641645528/1582641871296#c6\">Partie\u00a05 de l'Avis sur les prix de l'Agence canadienne d'inspection des aliments</a> (ACIA) pour les prix applicables aux engrais</p>\n</div>\n</section>\n\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">D\u00e9lais de traitement</h2>\n</header>\n<div class=\"panel-body\">\n<p>90\u00a0jours (sans compter le temps de r\u00e9ponse de l'industrie).</p>\n</div>\n</section>\n</div>\n\n<h2>Que sont les autorisations de recherche</h2>\n\n<p>La plupart des suppl\u00e9ments et certains engrais doivent faire l'objet d'un enregistrement et d'une \u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 avant de pouvoir \u00eatre import\u00e9s ou vendus au Canada. Les entreprises choisissent parfois d'effectuer des essais \u00e0 des fins de recherche sur des engrais ou des suppl\u00e9ments afin d'\u00e9valuer leur efficacit\u00e9 sur le terrain avant de pr\u00e9senter une demande d'enregistrement d'un produit en vertu de la <i>Loi sur les engrais</i>.</p>\n\n<p>Le <i>R\u00e8glement sur les engrais</i> renferme des dispositions qui permettent \u00e0 des personnes de proc\u00e9der \u00e0 des essais scientifiques sur des engrais et des suppl\u00e9ments non enregistr\u00e9s qui devraient normalement \u00eatre enregistr\u00e9s avant d'\u00eatre import\u00e9s ou diss\u00e9min\u00e9s, si des crit\u00e8res stricts sont respect\u00e9s.</p>\n\n<p>Les exigences r\u00e9glementaires qui s'appliquent actuellement \u00e0 la recherche visent \u00e0\u00a0:</p>\n\n<ul>\n<li>assurer la diss\u00e9mination s\u00e9curitaire d'engrais non enregistr\u00e9e (non exempt\u00e9s de l'enregistrement) et de suppl\u00e9ments nouveaux </li>\n<li>d\u00e9finir les conditions li\u00e9es \u00e0 l'utilisation des produits, \u00e0 leur manipulation et \u00e0 leur \u00e9limination afin de r\u00e9duire les risques pour la sant\u00e9 humaine, animale et l'environnement.</li>\n</ul>\n\n<h2>Liens pertinents</h2>\n<ul>\n<li><a href=\"/protection-des-vegetaux/engrais/circulaires-a-la-profession/t-4-103/fra/1307855399697/1320243929540\">T-4-103\u00a0\u2013\u00a0Effectuer des recherches en vertu de la <i>Loi sur les engrais</i></a></li>\n<li><a href=\"/a-propos-de-l-acia/trouver-un-formulaire/formulaire-cfia-acia-5475/fra/1565272872929/1565272873179\">Demande d'autorisation de recherche (CFIA/ACIA 5475)</a></li>\n<li><a href=\"/protection-des-vegetaux/engrais/apercu/liste-de-controle/fra/1490995155973/1490995340847\">Liste de contr\u00f4le pour les autorisations de recherche</a></li>\n</ul>\n\n<div class=\"clearfix\"></div>\n\n<nav role=\"navigation\" class=\"mrgn-bttm-lg\">\n<h3 class=\"wb-inv\">Navigation dans le document</h3>\n<ul class=\"pager\">\n<li class=\"next\"><a href=\"/protection-des-vegetaux/engrais/conditions-d-eligibilite/fra/1646672002174/1646672002486\" rel=\"next\">Suivant</a></li>\n</ul>\n</nav>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}