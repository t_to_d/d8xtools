{
    "dcr_id": "1601391948941",
    "title": {
        "en": "Guide to submitting applications for registration under the Fertilizers Act",
        "fr": "Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la Loi sur les engrais"
    },
    "html_modified": "13-11-2020",
    "modified": "2-11-2023",
    "issued": "29-9-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/manuals-manuels/data/fert_registration_tab_submission_2020_1601391948941_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/manuals-manuels/data/fert_registration_tab_submission_2020_1601391948941_fra"
    },
    "parent_ia_id": "1646095693725",
    "ia_id": "1601392244498",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Guide to submitting applications for registration under the Fertilizers Act",
        "fr": "Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la Loi sur les engrais"
    },
    "label": {
        "en": "Guide to submitting applications for registration under the Fertilizers Act",
        "fr": "Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la Loi sur les engrais"
    },
    "templatetype": "content page 1 column",
    "node_id": "1601392244498",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "parent_dcr_id": "1646095692928",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/fertilizers/overview/guide/",
        "fr": "/protection-des-vegetaux/engrais/apercu/guide/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Guide to submitting applications for registration under the Fertilizers Act",
            "fr": "Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la Loi sur les engrais"
        },
        "description": {
            "en": "Fertilizers (essential plant nutrients) and supplements (products other than fertilizers that improve the physical condition of the soil or aid plant growth or crop yield) when imported into or sold in Canada are regulated under the authority of the federal Fertilizers Act and regulations administered by the Canadian Food Inspection Agency (CFIA)",
            "fr": "Les engrais (nutriments essentiels pour les plantes) et les suppl\u00e9ments (les produits autres que les engrais qui am\u00e9liorent l'\u00e9tat physique du sol ou favorisent la croissance des plantes ou le rendement des cultures) import\u00e9s ou vendus au Canada sont r\u00e9glement\u00e9s en vertu de la Loi sur les engrais et son r\u00e8glement d'application  et sont administr\u00e9s par l'Agence canadienne d'inspection des aliments (ACIA)."
        },
        "keywords": {
            "en": "tab submission format, TSF, applications for registration, Fertilizers Act, regulatory modernization, forms, fees, fertilizer, 2020",
            "fr": "format de soumission \u00e0 onglets, FSaO, demandes d'enregistrement, Loi sur les engrais, modernisation de la r\u00e9glementation, formulaires, frais, engrais"
        },
        "dcterms.subject": {
            "en": "crops,environment,fertilizers,inspection,plants",
            "fr": "cultures,environnement,engrais,inspection,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-11-13",
            "fr": "2020-11-13"
        },
        "modified": {
            "en": "2023-11-02",
            "fr": "2023-11-02"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Guide to submitting applications for registration under the Fertilizers Act",
        "fr": "Guide pour la pr\u00e9sentation des demandes d'enregistrement en vertu de la Loi sur les engrais"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=0\">Complete text</a></p>\r\n\r\n<div data-ajax-replace=\"/navi/eng/1604944506561\"></div>\n\n<p class=\"text-right\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/fert_registration_tab_submission_2020_1604950136666_eng.pdf\" title=\"Guide to Submitting Applications for Registration under the Fertilizers Act\">PDF (388 <abbr title=\"kilobyte\">kb</abbr>)</a></p>\n\n<h2>On this page</h2>\n\r\n<nav>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=1\">Abbreviations</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=2\">Glossary</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3\">1. Introduction</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s1c3\">1.1 Registrations under the <i>Fertilizers Act</i></a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s2c3\">1.2 Service Delivery Standards</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s3c3\">1.3 Purpose of the guide</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s4c3\">1.4 My CFIA: Electronic application platform</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s5c3\">1.5 Application format</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=3#s6c3\">1.6 Safety data requirements</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4\">2. Application structure and layout</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4#s7c4\">2.1 Tab\u00a01: Administrative forms and fees</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4#s8c4\">2.2 Tab\u00a02: Proposed marketplace label</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4#s9c4\">2.3 Tab\u00a03: Product specification</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4#s10c4\">2.4 Tab\u00a04: Results of analysis</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=4#s11c4\">2.5 Tab\u00a05: Safety rationale and supplemental data</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=5\">3.Contact information</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6\">Appendices</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s12c6\">Appendix\u00a01: List of mandatory tabs and sub-tabs</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s13c6\">Appendix\u00a02: Product ingredients and associated safety data requirements</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s14c6\">Appendix\u00a03: Metals, dioxins/furans standards and maximum acceptable level of indicator organisms in fertilizers and supplements</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s15c6\">Appendix\u00a04: Toxicological hazards characterization</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s16c6\">Appendix 5: Toxicological exposure and risk assessment</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s17c6\">Appendix\u00a06: Microbial hazard characterization (checklist)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s18c6\">Appendix\u00a07: Microbial exposure characterization - Factors to consider</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s19c6\">Appendix\u00a08: Considerations for classification of microbial hazard severity and exposure level</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s20c6\">Appendix\u00a09: Labelling requirements for fertilizer-pesticides permitted for home and garden uses</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s21c6\">Appendix 10: Information resources - Toxicology</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/plant-health/fertilizers/overview/guide/eng/1601391948941/1601392244498?chap=6#s22c6\">Appendix\u00a011: Information resources \u2013 Microbiology</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n</ul>\r\n</nav>\r\n\n\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=0\">Texte complet</a></p>\r\n\r\n<div data-ajax-replace=\"/navi/fra/1604944506561\"></div>\n\n<p class=\"text-right\"><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/fert_registration_tab_submission_2020_1604950136666_fra.pdf\" title=\"Guide pour la pr\u00e9sentation des demandes d\u2019enregistrement en vertu de la Loi sur les engrais\" class=\"nowrap\">PDF (455 ko)</a></p>\n<h2>Sur cette page</h2>\n\r\n<nav>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=1\">Abr\u00e9viations</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=2\">Glossaire</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3\">1. Introduction</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s1c3\">1.1 Enregistrement en vertu de la <i>Loi sur les engrais</i></a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s2c3\">1.2 Normes de prestation de services pour les demandes d'enregistrement</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s3c3\">1.3 Objectif du guide</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s4c3\">1.4 Mon ACIA\u00a0:\u00a0Plateforme de pr\u00e9sentation de demandes \u00e9lectroniques</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s5c3\">1.5 Format de la demande</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=3#s6c3\">1.6 Exigences concernant les donn\u00e9es d'innocuit\u00e9</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4\">2. Structure et organisation de la demande</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4#s7c4\">2.1 Onglet\u00a01 : Formulaires administratifs et frais</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4#s8c4\">2.2 Onglet\u00a02\u00a0: \u00c9tiquette propos\u00e9e pour le produit</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4#s9c4\">2.3 Onglet\u00a03\u00a0:\u00a0Caract\u00e9ristiques du produit</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4#s10c4\">2.4 Onglet\u00a04\u00a0: R\u00e9sultats d'analyses</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=4#s11c4\">2.5 Onglet\u00a05\u00a0: Justificatifs de l'innocuit\u00e9 et donn\u00e9es compl\u00e9mentaires</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=5\">3. Renseignements suppl\u00e9mentaires</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6\">Annexes</a>\r\n\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s12c6\">Annexe\u00a01\u00a0:\u00a0Liste des onglets et sous-onglets obligatoire</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s13c6\">Annexe\u00a02\u00a0:\u00a0Exigences propres aux ingr\u00e9dients pour d\u00e9montrer leur innocuit\u00e9</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s14c6\">Annexe\u00a03\u00a0:\u00a0Normes et seuils relatifs aux m\u00e9taux, aux dioxines et furanes et aux organismes indicateurs dans les engrais et les suppl\u00e9ments</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s15c6\">Annexe\u00a04\u00a0:\u00a0Caract\u00e9risation des dangers toxicologiques</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s16c6\">Annexe\u00a05\u00a0:\u00a0\u00c9valuation de l'exposition et du risque toxicologique</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s17c6\">Annexe\u00a06\u00a0:\u00a0Caract\u00e9risation du danger microbien (liste de v\u00e9rification)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s18c6\">Annexe\u00a07\u00a0:\u00a0Caract\u00e9risation de l'exposition \u00e0 un microorganisme \u2013 facteurs \u00e0 consid\u00e9rer</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s19c6\">Annexe\u00a08\u00a0:\u00a0Consid\u00e9rations concernant la classification de la gravit\u00e9 des dangers microbiens et du niveau d'exposition</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s20c6\">Annexe 9\u00a0:\u00a0\u00c9tiquetage des engrais-pesticides autoris\u00e9s pour des usages domestiques ou de jardinage</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s21c6\">Annexe\u00a010\u00a0:\u00a0Sources d'information \u2013 Toxicologie</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/protection-des-vegetaux/engrais/apercu/guide/fra/1601391948941/1601392244498?chap=6#s22c6\">Annexe\u00a011\u00a0:\u00a0Sources d'information \u2013 microbiologie</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n</li>\r\n\r\n</ul>\r\n</nav>\r\n\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}