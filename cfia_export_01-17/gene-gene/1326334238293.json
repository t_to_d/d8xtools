{
    "dcr_id": "1326334238293",
    "title": {
        "en": "<i lang=\"la\">Cornu aspersum</i> (European brown garden snail) - Fact sheet",
        "fr": "<i lang=\"la\">Cornu aspersum</i> (Escargot petit-gris) - Fiche de renseignements"
    },
    "html_modified": "2012-01-11 23:18",
    "modified": "14-1-2014",
    "issued": "11-1-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_helasp_factsheet_1326334238293_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pestrava_helasp_factsheet_1326334238293_fra"
    },
    "parent_ia_id": "1326332875537",
    "ia_id": "1326341285764",
    "parent_node_id": "1326332875537",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "<i lang=\"la\">Cornu aspersum</i> (European brown garden snail) - Fact sheet",
        "fr": "<i lang=\"la\">Cornu aspersum</i> (Escargot petit-gris) - Fiche de renseignements"
    },
    "label": {
        "en": "<i lang=\"la\">Cornu aspersum</i> (European brown garden snail) - Fact sheet",
        "fr": "<i lang=\"la\">Cornu aspersum</i> (Escargot petit-gris) - Fiche de renseignements"
    },
    "templatetype": "content page 1 column",
    "node_id": "1326341285764",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1326332798489",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/nematodes-snails-and-others/european-brown-garden-snail/fact-sheet/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/nematodes-escargots-et-autres/escargot-petit-gris/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Cornu aspersum (European brown garden snail) - Fact sheet",
            "fr": "Cornu aspersum (Escargot petit-gris) - Fiche de renseignements"
        },
        "description": {
            "en": "Cornu aspersum, like other snail species, is an hermaphroditic species (each individual having both male and female reproductive systems).",
            "fr": "Cornu aspersum se nourrit d'un large \u00e9ventail de l\u00e9gumes, fleurs, fruits, plantes ornementales et mauvaises herbes. Aussi, il ne d\u00e9daigne pas les tissus d'animaux morts et les produits de papier."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Cornu aspersum, European brown garden snail, brown garden snail",
            "fr": "phytoravageurs, enqu&#234;tes phytosanitaires, phytoparasites justiciables de quarantaine, pi&#233;geage, Cornu aspersum, Petit-gris, escargot"
        },
        "dcterms.subject": {
            "en": "insects,inspection,plant diseases,plants",
            "fr": "insecte,inspection,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-01-11 23:18:13",
            "fr": "2012-01-11 23:18:13"
        },
        "modified": {
            "en": "2014-01-14",
            "fr": "2014-01-14"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Cornu aspersum (European brown garden snail) - Fact sheet",
        "fr": "Cornu aspersum (Escargot petit-gris) - Fiche de renseignements"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><strong>Note:</strong> <i lang=\"la\">Cornu aspersum</i> was previously known as <i lang=\"la\">Helix aspersa</i>.</p>\n<h2>Hosts</h2>\n<p><i lang=\"la\">Cornu aspersum</i> feeds on a wide variety of vegetables, flowers, fruit, ornamental plants and weeds. Dead animal tissue and paper products are also eaten.</p>\n<h2>Distribution</h2>\n<ul>\n<li><strong>Asia:</strong> T\u00fcrkiye, borders of the Black Sea</li>\n<li><strong>Africa:</strong> Algeria and South Africa</li>\n<li><strong>Pacific:</strong> Australia (Queensland, Tasmania), New Zealand</li>\n<li><strong>Europe:</strong> Great Britain (mainly southern and coastal), Belgium, France, Germany, Greece, Ireland, Italy, Portugal, Spain</li>\n<li><strong>Islands:</strong> Canary Islands, Haiti</li>\n<li><strong>North America:</strong> Mexico, United States (Arizona, California, Hawaii, Idaho, Louisiana, Nevada, New Mexico, Oregon, Texas, Utah, Washington)</li>\n<li><strong>South America:</strong> Argentina, Chile</li>\n</ul>\n<h2>Biology</h2>\n<p><i lang=\"la\">Cornu aspersum</i>, like other snail species, is a hermaphroditic species (each individual having both male and female reproductive systems). Mating is nocturnal and lasts from 4-12 hours during which time mutual fertilization occurs via the exchange of sperm. Oviposition occurs 3 to 6 days after fertilization (both partners lay eggs).</p>\n<p>White spherical eggs are deposited into a nest excavated in soil by the snail. The nest is about 2.5 to 4.0 <abbr title=\"centimetres\">cm</abbr> deep and on average will contain about 86 eggs. The egg mass is concealed by a mixture of soil and mucus capped by a layer of excrement. Low temperature and low humidity reduce the frequency of oviposition.</p>\n<p>Warm, damp environments are ideal and under such conditions oviposition may occur once a month. Eggs hatch in about two weeks, with the young snails remaining in the nest for two to four days. The young snails then emerge from the nest and begin feeding on the first suitable vegetation they encounter. The snails may take up to two years to mature, the rate of maturation being determined by the concentration of calcium in the environment.</p>\n<p>In cold weather the snail buries itself in soil, seals the shell with a sheet of hardened mucus and becomes dormant. Under dry conditions the snail sticks down onto a harden surface and seal its shell to avoid desiccation. This species can survive -10\u00b0<abbr title=\"Celsius\">C</abbr> becoming active between 4.5\u00b0<abbr title=\"Celsius\">C</abbr> and 21.5\u00b0<abbr title=\"Celsius\">C</abbr>.</p>\n<h2>Detection and identification</h2>\n<h3>Detection</h3>\n<ul>\n<li>Ragged holes chewed in leaves, with large veins remaining</li>\n<li>Holes in fruit</li>\n<li>Slime trails and excrement on plant material</li>\n</ul>\n<h3>Identification</h3>\n<ul>\n<li><strong>Eggs:</strong> round, white, about 3\u00a0<abbr title=\"millimetres\">mm</abbr> in diameter.</li>\n<li><strong>Juveniles:</strong> Shell with one whorl only when newly hatched, smooth, light brown, speckled black, lacking any pattern of bands and flecks.</li>\n<li><strong>Adult shell:</strong> \n<ul>\n<li>4\u00bd to 5 whorls; thin, moderately glossy, sculptured with wrinkles the last whorl especially with coarse unequal wrinkles of growth and a network of smaller wrinkles running together. The last whorl descends in front and the opening of the shell, or lip, is turned back.</li>\n<li>32 to 38\u00a0<abbr title=\"millimetres\">mm</abbr> in diameter, 29 to 33\u00a0<abbr title=\"millimetres\">mm</abbr> in height.</li>\n<li>yellow to yellowish brown to greyish brown, with five brown bands, interrupted by yellow flecks or streaks.</li>\n</ul>\n</li>\n<li><strong>Head and foot:</strong> are 5 to 6\u00a0<abbr title=\"centimetres\">cm</abbr> long when extended, yellowish-grey to greenish-black, with a pale line along the back from the base of the tentacles to the shell.</li>\n</ul>\n<h3>Inspection method</h3>\n<p>Adults and larger sized juveniles are likely to be visible among the host material or attached to the transporting containers. They may also be hidden in protected locations, sealed into their shells, avoiding desiccation. Check the underside of containers and their rims, <abbr title=\"et cetera\">etc.</abbr> Small snails and eggs in soil could be difficult to find. <i lang=\"la\"><abbr title=\"Cornu\">C.</abbr> aspersum</i> likes to hide in crevices and will over winter in stony ground.</p>\n<h3>Snail surveys</h3>\n<p>Are best carried out under wet, warm and dark conditions. Under bright, dry conditions it is necessary to thoroughly search dark sheltered areas where the humidity is elevated, such as under low-growing plants or backyard debris. It should be realized that these snails may bury themselves in loose soil or other matter, so the only way we can be reasonably sure an area is not infested is to make repeated surveys over a long period of time.</p>\n<figure>\n<img alt=\"Empty snail shells\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_helasp_factsheet_image1_1326339708149_eng.jpg\" class=\"img-responsive\">\n<figcaption>Photo: Eric Odense, Canadian Food Inspection Agency.</figcaption>\n</figure>\n\n<h2>Synonyms</h2>\n<ul>\n<li><i lang=\"la\">Cantareus aspersus</i></li>\n<li><i lang=\"la\">Cryptomphalus aspersus</i></li>\n<li><i lang=\"la\">Helix aspersa</i></li>\n</ul>\n\n<h2>Related sites</h2>\n<ul>\n<li><a href=\"http://entomology.ifas.ufl.edu/creatures/misc/gastro/brown_garden_snail.htm\">University of Florida</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><strong>Remarque\u00a0:</strong> <i lang=\"la\">Cornu aspersum</i> \u00e9tait auparavant connu sous le nom de <i lang=\"la\">Helix  aspersa</i>.</p>\n<h2>H\u00f4tes</h2>\n<p><i lang=\"la\">Cornu aspersum</i> se nourrit d'un large \u00e9ventail de l\u00e9gumes, fleurs, fruits, plantes ornementales et mauvaises herbes. Aussi, il ne d\u00e9daigne pas les tissus d'animaux morts et les produits de papier.</p>\n<h2>Distribution</h2>\n<ul>\n<li><strong>Afrique\u00a0:</strong> Alg\u00e9rie et Afrique du Sud</li>\n<li><strong>Asie\u00a0:</strong> T\u00fcrkiye, rives de la Mer noire</li>\n<li><strong>Pacifique\u00a0:</strong> Australie (Queensland, Tasmanie), Nouvelle-Z\u00e9lande</li>\n<li><strong>Europe\u00a0:</strong> Grande-Bretagne (principalement les r\u00e9gions du sud et c\u00f4ti\u00e8res), Belgique, France, Allemagne, Gr\u00e8ce, Irlande, Italie, Portugal, Espagne</li>\n<li><strong>\u00celes\u00a0:</strong> \u00celes Canaries, Ha\u00efti</li>\n<li><strong>Am\u00e9rique du Nord\u00a0:</strong> Mexique, \u00c9tats-Unis (Arizona, Californie, Hawaii, Idaho, Louisiane, Nouveau-Mexique, Nevada, Oregon, Texas, Utah, Washington)</li>\n<li><strong>Am\u00e9rique du Sud\u00a0:</strong> Argentine, Chili</li>\n</ul>\n<h2>Cycle biologique</h2>\n<p><i lang=\"la\">Cornu aspersum</i>, comme d'autres esp\u00e8ces d'escargot, est une esp\u00e8ce hermaphrodite (chaque sp\u00e9cimen poss\u00e9dant les appareils reproducteurs m\u00e2le et femelle). L'accouplement est nocturne et dure de 4 \u00e0 12 heures, p\u00e9riode pendant laquelle la fertilisation mutuelle se produit par \u00e9change de sperme. L'oviposition survient 3 \u00e0 6 jours apr\u00e8s la fertilisation (les deux partenaires pondent des \u0153ufs).</p>\n<p>Des \u0153ufs blancs sph\u00e9riques sont d\u00e9pos\u00e9s dans un nid creus\u00e9 dans le sol par l'escargot. Le nid a environ 2,5 \u00e0 4,0\u00a0<abbr title=\"centim\u00e8tres\">cm</abbr> de profondeur et contient en moyenne environ 86 \u0153ufs. La masse d'\u0153ufs est masqu\u00e9e par un m\u00e9lange de sol et d'humus recouvert d'une couche d'excr\u00e9ments. La basse temp\u00e9rature et un faible degr\u00e9 d'humidit\u00e9 r\u00e9duisent la fr\u00e9quence de l'oviposition.</p>\n<p>Les milieux chauds et humides sont id\u00e9aux et, en pareilles conditions, l'oviposition peut avoir lieu une fois par mois. Les \u0153ufs \u00e9closent au bout d'environ deux semaines, les jeunes escargots demeurant dans le nid pendant deux \u00e0 quatre jours. Les jeunes sortent ensuite du nid et commencent \u00e0 se nourrir sur la premi\u00e8re v\u00e9g\u00e9tation acceptable qu'ils rencontrent. Ils peuvent prendre jusqu'\u00e0 deux ans pour atteindre la maturit\u00e9, le taux de maturation \u00e9tant d\u00e9termin\u00e9 par la concentration de calcium dans le milieu.</p>\n<p>Par temps froid, l'escargot s'enfouit dans le sol, scelle sa coquille par une couche de mucus durci et entre en dormance. Dans des conditions de s\u00e9cheresse, l'escargot adh\u00e8re \u00e0 une surface durcie et scelle sa coquille pour \u00e9viter la dessiccation. Cette esp\u00e8ce peut survivre \u00e0 -10\u00a0\u00b0<abbr title=\"Celsius\">C</abbr> et devient active entre 4,5\u00a0\u00b0<abbr title=\"Celsius\">C</abbr> et 21,5\u00a0\u00b0<abbr title=\"Celsius\">C</abbr>.</p>\n<h2>D\u00e9tection et identification</h2>\n<h3>D\u00e9tection</h3>\n<ul>\n<li>Pr\u00e9sence de trous d\u00e9chiquet\u00e9s dans les feuilles auxquelles les grandes nervures restent attach\u00e9es.</li>\n<li>Trous dans les fruits.</li>\n<li>Tra\u00een\u00e9es de mucus et pr\u00e9sence d'excr\u00e9ments sur le mat\u00e9riel v\u00e9g\u00e9tal.</li>\n</ul>\n<h3>Identification</h3>\n<ul>\n<li><strong>\u0152ufs\u00a0:</strong> ronds, blancs, environ 3\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> de diam\u00e8tre.</li>\n<li><strong>Stade juv\u00e9nile\u00a0:</strong> coquille comportant une spirale seulement lorsque les escargots sont fra\u00eechement \u00e9clos, lisse, brun clair, tachet\u00e9e de noir et d\u00e9pourvue de toute marque de bandes et de mouchetures.</li>\n<li><strong>Coquille des adultes\u00a0:</strong> \n<ul>\n<li>4,5 \u00e0 5 spirales; mince, mod\u00e9r\u00e9ment brillante, sculpt\u00e9e de rides (particuli\u00e8rement la derni\u00e8re spirale).</li>\n<li>de 32 \u00e0 38\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> de diam\u00e8tre et de 29 \u00e0 33\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr> de hauteur.</li>\n<li>brune \u00e0 brun jaun\u00e2tre \u00e0 brun gris\u00e2tre, marqu\u00e9e de cinq bandes brunes, interrompues par des mouchetures ou des stries jaunes.</li>\n</ul>\n</li>\n<li><strong>T\u00eate et pied\u00a0:</strong> de 5 \u00e0 6\u00a0<abbr title=\"centim\u00e8tres\">cm</abbr> de longueur en extension, gris jaun\u00e2tre \u00e0 noir verd\u00e2tre, marqu\u00e9s d'une ligne p\u00e2le le long du dos, de la base des tentacules jusqu'\u00e0 la coquille.</li>\n</ul>\n<h3>M\u00e9thode d'inspection</h3>\n<p>Il est possible d'apercevoir des adultes et des formes juv\u00e9niles avanc\u00e9es parmi le mat\u00e9riel h\u00f4te ou adh\u00e9rant \u00e0 des contenants de transport. Ils peuvent \u00e9galement \u00eatre cach\u00e9s dans des endroits prot\u00e9g\u00e9s, scell\u00e9s dans leurs coquilles pour \u00e9viter la dessiccation. Examinez le dessous des contenants et leurs rebords, <abbr title=\"et cetera\">etc.</abbr> Les petits escargots et les \u0153ufs dans un sol peuvent \u00eatre difficiles \u00e0 observer. <i lang=\"la\"><abbr title=\"Cornu\">C.</abbr> aspersum</i> aime se dissimuler dans les fissures et hiverne dans le sol pierreux.</p>\n<h3>Recherches d'escargots</h3>\n<p>Il est recommand\u00e9 de faire les recherches par temps pluvieux, chaud et sombre. Par temps clair et sec, il faut chercher minutieusement les endroits sombres et abrit\u00e9s o\u00f9 l'humidit\u00e9 est \u00e9lev\u00e9e, comme sous les plantes basses ou les d\u00e9bris d'arri\u00e8re-cour. Il faut comprendre que ces escargots peuvent s'enfouir dans le sol meuble ou d'autre mati\u00e8re; par cons\u00e9quent, la seule fa\u00e7on d'\u00eatre raisonnablement certain qu'une superficie de terrain n'est pas infest\u00e9e consiste \u00e0 faire des recherches r\u00e9p\u00e9t\u00e9es sur une longue p\u00e9riode de temps.</p>\n<figure>\n<img alt=\"coquilles vides d'escargots\" src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/pestrava_helasp_factsheet_image1_1326339708149_fra.jpg\" class=\"img-responsive\">\n<figcaption>Photo\u00a0: Eric Odense, Agence canadienne d'inspection des aliments.</figcaption>\n</figure>\n\n<h2>Synonymes</h2>\n<ul>\n<li><i lang=\"la\">Cantareus aspersus</i></li>\n<li><i lang=\"la\">Cryptomphalus aspersus</i></li>\n<li><i lang=\"la\">Helix aspersa</i></li>\n</ul>\n\n<h2>Sites connexes</h2>\n<ul>\n<li><a href=\"http://entomology.ifas.ufl.edu/creatures/misc/gastro/brown_garden_snail.htm\">Universit\u00e9 de la Floride</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}