{
    "dcr_id": "1475881649286",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Nicandra physalodes</i> (Apple of Peru)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Nicandra physalodes</i> (Nicandre faux-coqueret)"
    },
    "html_modified": "2024-02-13 9:51:46 AM",
    "modified": "2017-06-02",
    "issued": "2017-07-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_nicandra_physalodes_1475881649286_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_nicandra_physalodes_1475881649286_fra"
    },
    "ia_id": "1475881649799",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Nicandra physalodes</i> (Apple of Peru)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Nicandra physalodes</i> (Nicandre faux-coqueret)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Nicandra physalodes</i> (Apple of Peru)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Nicandra physalodes</i> (Nicandre faux-coqueret)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1475881649799",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/nicandra-physalodes/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/nicandra-physalodes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Nicandra physalodes (Apple of Peru)",
            "fr": "Semence de mauvaises herbe : Nicandra physalodes (Nicandre faux-coqueret)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Nicandra physalodes, Solanaceae, Apple of Peru",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Nicandra physalodes, Solanaceae, Nicandre faux-coqueret"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-07-14",
            "fr": "2017-07-14"
        },
        "modified": {
            "en": "2017-06-02",
            "fr": "2017-06-02"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Nicandra physalodes (Apple of Peru)",
        "fr": "Semence de mauvaises herbe : Nicandra physalodes (Nicandre faux-coqueret)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Family</h2>\n<p><i lang=\"la\">Solanaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Apple of Peru</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a>, 2016 under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr> and ephemeral in <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Nova Scotia\">NS</abbr>, <abbr title=\"Prince Edward Island\">PE</abbr> (Darbyshire 2003<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>, Brouillet <abbr title=\"and others\">et al.</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to South America and widely introduced elsewhere, including North America, Asia, Africa, eastern Europe, Australia and New Zealand (CABI 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, <abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). In the United States it occurs mainly in the east and midwestern regions, with a scattered occurrence in the central and western states (Kartesz 2011<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed diameter: <span class=\"nowrap\">1.0 - 1.3 <abbr title=\"millimetres\">mm</abbr></span></li>\n<li>Seed thickness: <span class=\"nowrap\">0.3 <abbr title=\"millimetre\">mm</abbr></span></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed is round to kidney-shaped, strongly compressed</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed is glossy; strongly reticulate with small wavy-edged cells</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed is orange to brownish-orange</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hilum is a small slit along the narrow edge of the seed.</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, old fields, old gardens, roadsides and disturbed areas (Scoggan 1979<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>, Gleason and Cronquist 1991<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Footnote\u00a0</span>7</a></sup>, Darbyshire 2003<sup id=\"fn1b-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>). Infests a wide range of crops, including cereals, soybeans, beans, corn and vegetables, as well as pastures, orchards and vineyards (CABI 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Apple of Peru is occasionally planted as an ornamental and may escape cultivation. It may also be dispersed as a contaminant in grain and birdseed (National Botanic Garden of Belgium 2013<sup id=\"fn8-rf\"><a class=\"fn-lnk\" href=\"#fn8\"><span class=\"wb-inv\">Footnote\u00a0</span>8</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Husk tomato (<i lang=\"la\">Physalis pubescens</i>)</h3>\n<ul><li> Husk tomato seeds are a similar size, round shape, with a glossy and wrinkled surface as apple of Peru.</li>\n<li>Husk tomato seeds differ from apple of Peru by: lighter colour and an indistinct surface pattern. The cells of husk tomato are wavy-edged, shallow, and are pulled at the hilum and along the narrow edge. The cells of apple of Peru are deep, well-defined and appear the same across the seed.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_nicandra_physalodes_03cnsh_1475604918039_eng.jpg\" alt=\"Figure 1 - Apple of Peru (Nicandra physalodes) seeds\" class=\"img-responsive\">\n<figcaption>Apple of Peru (<i lang=\"la\">Nicandra physalodes</i>) seeds</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_nicandra_physalodes_01cnsh_1475604876286_eng.jpg\" alt=\"Figure 2 - Apple of Peru (Nicandra physalodes) seed\" class=\"img-responsive\">\n<figcaption>Apple of Peru (<i lang=\"la\">Nicandra physalodes</i>) seed</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_nicandra_physalodes_02cnsh_1475604896628_eng.jpg\" alt=\"Figure 3 - Apple of Peru (Nicandra physalodes) seed, side view\" class=\"img-responsive\">\n<figcaption>Apple of Peru (<i lang=\"la\">Nicandra physalodes</i>) seed, side view</figcaption>\n</figure>\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_physalis_pubescens_02cnsh_1475605698450_eng.jpg\" alt=\"Figure 4 - Similar species: Husk tomato (Physalis pubescens) seeds\" class=\"img-responsive\">\n<figcaption>Similar species: Husk tomato (<i lang=\"la\">Physalis pubescens</i>) seeds</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_physalis_pubescens_01cnsh_1475605517938_eng.jpg\" alt=\"Figure 5 - Similar species: Husk tomato (Physalis pubescens) seed\" class=\"img-responsive\">\n<figcaption>Similar species: Husk tomato (<i lang=\"la\">Physalis pubescens</i>) seed</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>CABI. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, <abbr title=\"North Carolina\">N.C.</abbr>, www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Scoggan, H. J. 1979</strong>. Flora of Canada. National Museums of Canada, Ottawa.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 7</dt>\n<dd id=\"fn7\">\n<p><strong>Gleason, H. A. and Cronquist, A. 1991</strong>. Manual of vascular plants of northeastern United States and adjacent Canada. The New York Botanical Garden, Bronx, New York.</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>7<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 8</dt>\n<dd id=\"fn8\">\n<p><strong>National Botanic Garden of Belgium. 2013</strong>. <i lang=\"la\">Nicandra physalodes</i>. Manual of the alien plants of Belgium, http://alienplantsbelgium.be/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn8-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>8<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Famille</h2>\n<p><i lang=\"la\">Solanaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Nicandre faux-coqueret</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> L'esp\u00e8ce est pr\u00e9sente en <abbr title=\"Ontario\">Ont.</abbr> et au <abbr title=\"Qu\u00e9bec\">Qc</abbr> et est \u00e9ph\u00e9m\u00e8re en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr> et \u00e0 l'<abbr title=\"\u00cele-du-Prince-\u00c9douard\">\u00ce.-P.-\u00c9.</abbr> (Darbyshire, 2003<sup id=\"fn1a-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>; Brouillet <abbr title=\"et autres\">et al.</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Plante indig\u00e8ne d'Am\u00e9rique du Sud. Largement introduite ailleurs dans le monde, notamment en Am\u00e9rique du Nord, en Asie, en Afrique, dans l'est de l'Europe, en Australie et en Nouvelle-Z\u00e9lande (CABI, 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; <abbr title=\"United States Department of Agriculture\" lang=\"en\">USDA</abbr>-<abbr title=\"Agricultural Research Service\" lang=\"en\">ARS</abbr>, 2016<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>). Aux \u00c9tats-Unis, elle est pr\u00e9sente principalement dans les r\u00e9gions de l'est et du Midwest, et par endroits isol\u00e9s dans les \u00c9tats du centre et de l'Ouest (Kartesz, 2011<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine v\u00e9ritable</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Diam\u00e8tre de la graine\u00a0: <span class=\"nowrap\">1,0 - 1,3 <abbr title=\"millim\u00e8tre\">mm</abbr></span></li>\n<li>\u00c9paisseur de la graine\u00a0: <span class=\"nowrap\">0,3 <abbr title=\"millim\u00e8tre\">mm</abbr></span></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine au contour rond \u00e0 r\u00e9niforme; fortement comprim\u00e9e</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine \u00e0 la surface luisante, fortement r\u00e9ticul\u00e9e et affichant de petites cellules \u00e0 bords ondul\u00e9s</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine de couleur orange \u00e0 orange brun\u00e2tre</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Hile en forme de petite fente, situ\u00e9 le long du bord \u00e9troite de la graine.</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, champs abandonn\u00e9s, jardins abandonn\u00e9s, bords de chemin et terrains perturb\u00e9s (Scoggan, 1979<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>; Gleason et Cronquist, 1991<sup id=\"fn7-rf\"><a class=\"fn-lnk\" href=\"#fn7\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>7</a></sup>; Darbyshire, 2003<sup id=\"fn1b-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>). Infeste diverses cultures (c\u00e9r\u00e9ales, soja, haricot, ma\u00efs, l\u00e9gumes, p\u00e2turages, vergers, vignobles) (CABI, 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le nicandre faux-coqueret est parfois cultiv\u00e9 comme plante ornementale et peut s'\u00e9chapper des cultures. Comme il peut \u00e9galement contaminer le grain et les graines pour oiseaux, il peut \u00eatre dispers\u00e9 de cette fa\u00e7on (<span lang=\"en\">National Botanic Garden of Belgium</span>, 2013<sup id=\"fn8-rf\"><a class=\"fn-lnk\" href=\"#fn8\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>8</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Alk\u00e9kenge (<i lang=\"la\">Physalis pubescens</i>)</h3>\n<ul>\n<li>Les graines de l'alk\u00e9kenge ressemblent \u00e0 celles du nicandre faux-coqueret par leurs dimensions, leur forme arrondie et leur surface luisante et rid\u00e9e.</li>\n<li>Comparativement aux graines de nicandre faux-coqueret, celles de l'alk\u00e9kenge sont de couleur plus claire, et leur surface pr\u00e9sente un motif indistinct. Celles de l'alk\u00e9kenge ont des cellules aux bords ondul\u00e9s et peu profondes, et elles sont en saillie pr\u00e8s du hile et sur le bord \u00e9troit de la graine. Chez le nicandre faux-coqueret, les cellules sont profondes, bien d\u00e9finies et semblent uniformes  sur toute la surface de la graine.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_nicandra_physalodes_03cnsh_1475604918039_fra.jpg\" alt=\"Figure 1 - Nicandre faux-coqueret (Nicandra physalodes) graines\" class=\"img-responsive\">\n<figcaption>Nicandre faux-coqueret (<i lang=\"la\">Nicandra physalodes</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_nicandra_physalodes_01cnsh_1475604876286_fra.jpg\" alt=\"Figure 2 - Nicandre faux-coqueret (Nicandra physalodes) graine\" class=\"img-responsive\">\n<figcaption>Nicandre faux-coqueret (<i lang=\"la\">Nicandra physalodes</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_nicandra_physalodes_02cnsh_1475604896628_fra.jpg\" alt=\"Figure 3 - Nicandre faux-coqueret (Nicandra physalodes) graine, la vue de c\u00f4t\u00e9\" class=\"img-responsive\">\n<figcaption>Nicandre faux-coqueret (<i lang=\"la\">Nicandra physalodes</i>) graine, la vue de c\u00f4t\u00e9\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_physalis_pubescens_02cnsh_1475605698450_fra.jpg\" alt=\"Figure 4 - Esp\u00e8ce semblable : Alk\u00e9kenge (Physalis pubescens) graines\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Alk\u00e9kenge (<i lang=\"la\">Physalis pubescens</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_physalis_pubescens_01cnsh_1475605517938_fra.jpg\" alt=\"Figure 5 - Esp\u00e8ce semblable : Alk\u00e9kenge (Physalis pubescens) graine\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Alk\u00e9kenge (<i lang=\"la\">Physalis pubescens</i>) graine\n</figcaption>\n</figure>\n\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn1a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>CABI. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, <abbr title=\"North Carolina\">N.C.</abbr>, www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Scoggan, H. J. 1979</strong>. Flora of Canada. National Museums of Canada, Ottawa.</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n\n<dt>Note de bas de page 7</dt>\n<dd id=\"fn7\">\n<p lang=\"en\"><strong>Gleason, H. A. and Cronquist, A. 1991</strong>. Manual of vascular plants of northeastern United States and adjacent Canada. The New York Botanical Garden, Bronx, New York.</p>\n<p class=\"fn-rtn\"><a href=\"#fn7-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>7</a></p> \n</dd>\n\n<dt>Note de bas de page 8</dt>\n<dd id=\"fn8\">\n<p lang=\"en\"><strong>National Botanic Garden of Belgium. 2013</strong>. <i lang=\"la\">Nicandra physalodes</i>. Manual of the alien plants of Belgium, http://alienplantsbelgium.be/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn8-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>8</a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}