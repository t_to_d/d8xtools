{
    "dcr_id": "1675349873104",
    "title": {
        "en": "Transition and Guidelines for Incidental Feed Additive (IFA) Data Review Applications",
        "fr": "Avis \u00e0 l'industrie : Transition et directives pour les demandes d'\u00e9valuation des donn\u00e9es relatives aux additifs indirects d'aliments pour animaux (AIA)"
    },
    "html_modified": "2024-03-12 3:59:27 PM",
    "modified": "2023-02-15",
    "issued": "2023-02-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/transition_guidelines_for_ifa_data_review_1675349873104_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/transition_guidelines_for_ifa_data_review_1675349873104_fra"
    },
    "ia_id": "1675349997504",
    "parent_ia_id": "1320602705720",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1320602705720",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Transition and Guidelines for Incidental Feed Additive (IFA) Data Review Applications",
        "fr": "Avis \u00e0 l'industrie : Transition et directives pour les demandes d'\u00e9valuation des donn\u00e9es relatives aux additifs indirects d'aliments pour animaux (AIA)"
    },
    "label": {
        "en": "Transition and Guidelines for Incidental Feed Additive (IFA) Data Review Applications",
        "fr": "Avis \u00e0 l'industrie : Transition et directives pour les demandes d'\u00e9valuation des donn\u00e9es relatives aux additifs indirects d'aliments pour animaux (AIA)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1675349997504",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300212191074",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/feed-industry-notices/transition-and-guidelines/",
        "fr": "/sante-des-animaux/aliments-du-betail/avis-a-l-industrie-aliments-du-betail/transition-et-directives/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Transition and Guidelines for Incidental Feed Additive (IFA) Data Review Applications",
            "fr": "Avis \u00e0 l'industrie : Transition et directives pour les demandes d'\u00e9valuation des donn\u00e9es relatives aux additifs indirects d'aliments pour animaux (AIA)"
        },
        "description": {
            "en": "Incidental feed additives (IFAs) are substances used during feed manufacturing which may become residues in livestock feeds. They have no nutritional or technical function in single ingredient or mixed feeds. In the past, the Canadian Food Inspection Agency (CFIA) has registered IFA-type products.",
            "fr": "Les additifs indirects d'aliments pour animaux sont des substances utilis\u00e9es dans la fabrication d'aliments pour animaux qui peuvent se retrouver sous forme de r\u00e9sidus dans les aliments pour animaux de ferme."
        },
        "keywords": {
            "en": "Feed industry notices, Feeds Regulations, Transition and Guidelines, Incidental Feed Additive, IFA, Data Review Applications",
            "fr": "Avis \u00e0 l'industrie : Aliments du b\u00e9tail, R\u00e8glement sur les aliments du b\u00e9tail, Transition et directives pour les demandes d'\u00e9valuation des donn\u00e9es relatives aux additifs indirects d'aliments pour animaux (AIA)"
        },
        "dcterms.subject": {
            "en": "animal health,inspection,livestock,policy,regulation,standards",
            "fr": "sant\u00e9 animale,inspection,b\u00e9tail,politique,r\u00e9glementation,norme"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "modified": {
            "en": "2023-02-15",
            "fr": "2023-02-15"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Transition and Guidelines for Incidental Feed Additive (IFA) Data Review Applications",
        "fr": "Avis \u00e0 l'industrie : Transition et directives pour les demandes d'\u00e9valuation des donn\u00e9es relatives aux additifs indirects d'aliments pour animaux (AIA)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Incidental feed additives (IFAs) are substances used during feed manufacturing which may become residues in livestock feeds. They have no nutritional or technical function in single ingredient or mixed feeds. In the past, the Canadian Food Inspection Agency (CFIA) has registered IFA-type products. There are no sections of the <i>Feeds Act</i> and regulations, which specifically require the approval of IFAs for use during feed manufacturing. However, their misuse could result in the contamination of livestock feeds, thereby creating potential animal or human health risks. Such an action would be considered in violation of section 3(3) of the <i>Feeds Act</i> and sections 19(1)(j) and (k) of the <i>Feeds Regulations</i>.</p>\n<p>The CFIA is eliminating IFAs from the registration and renewal process for livestock feeds and has developed the following guidance for the evaluation, on a case-by-case basis, of the acceptability of IFA products voluntarily submitted by manufacturers wishing to supply these products to feed manufacturers. These evaluations, called applications for data review, will be conducted with the intent of assisting feed manufacturers in averting violations of the provisions of Section 3(3) of the <i>Feeds Act </i>and 19(1)(j) and (k) of the <i>Feeds Regulations</i>.</p>\n<p>Applications for IFA data reviews will include a product label with directions for use, product formulation, intended purpose in feed manufacturing, information on feed residue concentrations resulting from incidental contact, and information supporting the safety of these residues in livestock feeds.</p>\n<p>Letters expressing favourable opinions are called \"Letter Of No-Objection (LONO)\" will be issued for those IFAs submitted to the CFIA for a data review for which a favourable opinion is concluded after the review. A LONO does not constitute an approval of the product under the <i>Feeds Act</i> and regulations. It is simply an opinion expressed by the CFIA on the acceptability of the product, based on the information available at the time of its evaluation.</p>\n<p>There will be no fee for obtaining a LONO for an IFA and a LONO has no expiry date. It is considered valid as long as the composition, the intended use and the labelling content remain as described in the initial data review application. Applicants are responsible for notifying the CFIA of any changes affecting the validity of the no-objection status. The CFIA reserves the right to withdraw the no objection status of any given product should information come available that its use may potentially pose a risk to human health, animal health, or the environment.</p>\n<p>Products currently registered that are identified as IFAs will be transitioned to LONOs as their current registrations expire.</p>\n<h2>Appendix: Related Documents</h2>\n<ul>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-3/eng/1617909452465/1617909586070?chap=30\">RG-1 Chapter 3.30 Guidelines for Incidental Feed Additive (IFA) Data Review Applications</a></li>\n<li><a href=\"/animal-health/livestock-feeds/inspection-program/safe-use-of-incidental-feed-additives/eng/1675271749542/1675271861098\">Options to demonstrate the safe use of incidental feed additives in the production and manufacture of livestock feeds</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Les additifs indirects d'aliments pour animaux (AIA) sont des substances utilis\u00e9es dans la fabrication d'aliments pour animaux qui peuvent se retrouver sous forme de r\u00e9sidus dans les aliments pour animaux de ferme. Ils n'ont aucune fonction nutritionnelle ou technique dans les aliments pour animaux \u00e0 ingr\u00e9dient unique ou m\u00e9lang\u00e9s. Dans le pass\u00e9, l'Agence canadienne d'inspection des aliments (ACIA) a enregistr\u00e9 des produits de type AIA. Aucune section de la <i>Loi relative aux aliments du b\u00e9tail</i> et de son r\u00e8glement n'exige sp\u00e9cifiquement l'approbation des AIAs pour leur utilisation dans la fabrication des aliments pour animaux. Toutefois, leur mauvaise utilisation pourrait entra\u00eener la contamination des aliments pour animaux de ferme, cr\u00e9ant ainsi des risques potentiel pour la sant\u00e9 animale ou humaine. Une telle action serait consid\u00e9r\u00e9e comme une violation du paragraphe\u00a03(3) de la <i>Loi sur les aliments du b\u00e9tail</i> et des alin\u00e9as\u00a019(1)j) et k) du <i>R\u00e8glement sur les aliments du b\u00e9tail</i>.</p>\n<p>L'ACIA \u00e9limine les AIAs \u00a0du processus d'enregistrement et de renouvellement de l'enregistrement des aliments pour animaux et a \u00e9labor\u00e9 les directives suivantes pour l'\u00e9valuation, au cas par cas, de l'acceptabilit\u00e9 des produits AIAs soumis volontairement par les fabricants qui souhaitent fournir ces produits aux fabricants d'aliments pour animaux. Ces \u00e9valuations, appel\u00e9es demandes d'\u00e9valuation de donn\u00e9es, seront men\u00e9es dans le but d'aider les fabricants d'aliments pour animaux \u00e0 \u00e9viter les violations des dispositions du paragraphe\u00a03(3) de la <i>Loi sur les aliments du b\u00e9tail</i> et des alin\u00e9as\u00a019(1)j) et k) du <i>R\u00e8glement sur les aliments du b\u00e9tail</i>.</p>\n<p>Les demandes d'\u00e9valuation des donn\u00e9es relatives aux AIAs \u00a0comprendra l'\u00e9tiquette du produit avec le mode d'emploi, la composition du produit, l'objectif dans la fabrication d'aliments pour animaux, des renseignements sur les concentrations de r\u00e9sidus dans les aliments pour animaux r\u00e9sultant d'un contact indirect et des renseignements appuyant l'innocuit\u00e9 de ces r\u00e9sidus dans les aliments pour animaux.</p>\n<p>Les lettres exprimant un avis favorable sont appel\u00e9es \u00ab\u00a0lettres de non-objection\u00a0\u00bb (LNO) sera \u00e9mis pour les AIA soumises \u00e0 l'ACIA pour un \u00e9valuation des donn\u00e9es pour lesquelles un avis favorable est conclu apr\u00e8s l'\u00e9valuation. Une LNO ne constitue pas une approbation du produit en vertu de la <i>Loi relative aux aliments du b\u00e9tail</i> et de son r\u00e8glement. Il s'agit simplement d'une opinion exprim\u00e9e par l'ACIA sur l'acceptabilit\u00e9 du produit, en fonction des renseignements disponibles au moment de son \u00e9valuation.</p>\n<p>Il n'y aura pas de frais li\u00e9s \u00e0 l'obtention d'une LNO relative \u00e0 un AIA et une LNO n'a pas de date d'expiration. Elle est consid\u00e9r\u00e9e comme valable tant que la composition, l'utilisation pr\u00e9vue et le contenu de l'\u00e9tiquette restent tels qu'ils sont d\u00e9crits dans la demande d'\u00e9valuation des donn\u00e9es initiale. Il incombe aux demandeurs d'informer l'ACIA de tout changement susceptible de modifier la validit\u00e9 du statut de non-objection. Toutefois, l'ACIA se r\u00e9serve le droit de retirer le statut de non-objection d'un produit donn\u00e9 si des renseignements deviennent disponibles montrant que son utilisation peut potentiellement comporter un risque pour la sant\u00e9 humaine, la sant\u00e9 animale ou l'environnement.</p>\n<p>Les produits actuellement enregistr\u00e9s qui sont d\u00e9sign\u00e9s comme des AIAs seront transf\u00e9r\u00e9s en LNO au fur et \u00e0 mesure que leurs enregistrements actuels expireront.</p>\n<h2>Documents connexes</h2>\n<ul>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-3/fra/1617909452465/1617909586070?chap=30\">RG-1 Chapitre 3.30 Directives relatives aux demandes d'\u00e9valuation de donn\u00e9es concernant les additifs indirects dans les aliments pour animaux (AIA)</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/inspection-des-aliments-du-betail/utilisation-sure-des-additifs-indirects-dans-les-a/fra/1675271749542/1675271861098\">Options permettant de d\u00e9montrer l'utilisation s\u00fbre des additifs alimentaires indirects dans la production et la fabrication d'aliments pour animaux de ferme</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}