{
    "dcr_id": "1331745159167",
    "title": {
        "en": "Japanese stiltgrass",
        "fr": "Microst\u00e9gie en osier"
    },
    "html_modified": "2012-03-14 13:12",
    "modified": "28-8-2023",
    "issued": "14-3-2012",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_microstegium_1331745159167_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_microstegium_1331745159167_fra"
    },
    "parent_ia_id": "1331614823132",
    "ia_id": "1331745245097",
    "parent_node_id": "1331614823132",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Japanese stiltgrass",
        "fr": "Microst\u00e9gie en osier"
    },
    "label": {
        "en": "Japanese stiltgrass",
        "fr": "Microst\u00e9gie en osier"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331745245097",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331614724083",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/japanese-stiltgrass/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/microstegium-vimineum/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Japanese stiltgrass",
            "fr": "Microst\u00e9gie en osier"
        },
        "description": {
            "en": "Japanese stiltgrass is an invasive plant found in a variety of habitats, including forests, wetlands and disturbed areas.",
            "fr": "Microstegium vimineum est une plante envahissante qui pousse dans divers habitats, dont les for\u00eats, les zones humides et les zones perturb\u00e9es."
        },
        "keywords": {
            "en": "invasive plants, Microstegium vimineum, Plant health, Plant health act,",
            "fr": "plantes envahissantes, protection des v\u00e9g\u00e9taux, Loi sur la protection des v\u00e9g\u00e9taux, contre les mauvaises herbes, Microstegium vimineum"
        },
        "dcterms.subject": {
            "en": "crops,environment,environmental protection,inspection,plants,weeds",
            "fr": "cultures,environnement,protection de l'environnement,inspection,plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-14 13:12:44",
            "fr": "2012-03-14 13:12:44"
        },
        "modified": {
            "en": "2023-08-28",
            "fr": "2023-08-28"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "general public,government",
            "fr": "grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Japanese stiltgrass",
        "fr": "Microst\u00e9gie en osier"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"wb-lbx lbx-gal\">\n<div class=\"col-xs-4 pull-right mrgn-tp-lg\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_stiltgrass_1692891319829_eng.jpg\" title=\"Attribution: Steve Hurst, USDA NRCS PLANTS Database, Bugwood.org\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_stiltgrass_1692891319829_eng.jpg\" alt=\"\" class=\"img-responsive\">\n</a>\n</div>\n</div>\n\n<p class=\"mrgn-tp-lg\">Japanese stiltgrass (<i lang=\"la\">Microstegium vimineum</i>) is an invasive plant found in many habitats, including forests, wetlands and disturbed areas.</p>\n<p>Because of how fast it spreads, it can disturb ecosystems by out-competing native plants and affecting bird nests and other wildlife.</p>\n<p>Japanese stiltgrass spreads when its seeds attach to clothing or animal fur. Seeds can also be found on muddy car tires and shoes. Bird seed, soil, nursery stock and hay can also be contaminated with Japanese stiltgrass.</p>\n\n<p><a class=\"btn btn-call-to-action\" href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049#form-formulaire\">Report a Japanese stiltgrass sighting</a></p>\n<h2>Where it's found</h2>\n<p>Japanese stiltgrass has been confirmed in a small population under official control in southern Ontario, including:</p>\n<ul>\n<li>Waterloo Park</li>\n<li>Short Hills Provincial Park</li>\n</ul>\n<h2>What you can do</h2>\n<ul class=\"lst-spcd\">\n<li>When leaving a forested area in Southern Ontario, including national and provincial parks, help stop the spread of Japanese stiltgrass seeds by brushing off your: \n<ul>\n<li>clothes</li>\n<li>shoes</li> \n<li>bikes, and</li> \n<li>pets.</li>\n</ul></li>\n<li>Avoid planting invasive plants in your garden.</li>\n<li>Use clean, high-quality seeds that are certified for your garden to avoid accidentally planting an invasive specie.</li>\n<li>Ensure your machinery, vehicles and tools are free of soil and plant parts before moving from one area to another.</li>\n<li><a href=\"/about-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049\">Report sightings of Japanese stiltgrass</a> to the Canadian Food Inspection Agency.</li> \n</ul>\n\n<h2>How to spot it</h2>\n\n<div class=\"wb-lbx lbx-gal row\">\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumc_1331669405168_eng.jpg\" title=\"Attribution: David J. Moorhead, University of Georgia (Bugwood.org)\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumc_1331669405168_eng.jpg\" alt=\"Image 1\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiuma_1331669337748_eng.jpg\" title=\"Attribution: Chuck Bargeron, University of Georgia (Bugwood.org)\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiuma_1331669337748_eng.jpg\" alt=\"Image 2\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumb_1331669374232_eng.jpg\" title=\"Attribution: Leslie J. Mehrhoff, University of Connecticut (Bugwood.org)\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumb_1331669374232_eng.jpg\" alt=\"Image 3\" class=\"img-responsive\" width=\"148\">\n</a>\n</div>\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_MicrostegiumVimineum_img1_1397669814640_eng.jpg\" title=\"\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_MicrostegiumVimineum_img1_1397669814640_eng.jpg\" alt=\"Image 4\" class=\"img-responsive\" width=\"138\">\n</a>\n</div>\n<div class=\"clearfix\"></div>\n</div>\n\n<ul class=\"list-unstyled\">\n<li><strong>Image 1\u00a0:</strong> Leaves</li> \n<li><strong>Image 2\u00a0:</strong> Foliage in the spring and summer</li> \n<li><strong>Image 3\u00a0:</strong> Foliage in the fall</li>\n<li><strong>Image 4\u00a0:</strong> As a grain, spikelet and seed (from left to right)</li>\n</ul>\n\n<p>Japanese stiltgrass is an annual grass that sprawls across the floors of disturbed areas, forests and wetlands, as it prefers lower light. Stems can grow up to 1\u00a0m tall and have many branches. Leaf blades are thin, pale green and tapered at both ends. In late summer, slender flower spikes are found at the end of the stems. Plants develop a slightly purplish colour in autumn.</p>\n\n<h2>What we are doing</h2>\n<p>Japanese stiltgrass is <a href=\"/plant-health/invasive-species/regulated-pests/eng/1363317115207/1363317187811\">regulated as a pest</a> in Canada under the <a href=\"/english/reg/jredirect2.shtml?plavega\"><i>Plant Protection Act</i></a>. Importation and domestic movement of regulated plants and their propagative parts is prohibited.</p>\n\n<h2>More information</h2>\n<ul>\n<li>Learn more about <a href=\"/plant-health/invasive-species/eng/1299168913252/1299168989280\">invasive species</a></li>\n<li><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/plant_pest_cards_japanese_stiltgrass_1589497439502_eng.pdf\">Plant pest card\u00a0\u2013 PDF (1\u00a0000\u00a0ko)</a></li>\n<li><a href=\"/plant-health/seeds/seed-testing-and-grading/seeds-identification/microstegium-vimineum/eng/1397679674841/1397679719451\">Seed fact sheet</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"wb-lbx lbx-gal\">\n<div class=\"col-xs-4 pull-right mrgn-tp-lg\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_stiltgrass_1692891319829_fra.jpg\" title=\"Attribution: Steve Hurst, base de donn\u00e9es USDA NRCS PLANTS, Bugwood.org\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/japanese_stiltgrass_1692891319829_fra.jpg\" alt=\"\" class=\"img-responsive\">\n</a>\n</div>\n</div>\n\n<p class=\"mrgn-tp-lg\">La microst\u00e9gie en osier (<i lang=\"la\">Microstegium vimineum</i>) est une plante envahissante que l'on trouve dans de nombreux habitats, y compris les for\u00eats, les zones humides et les zones perturb\u00e9es.</p>\n<p>En raison de sa rapidit\u00e9 de propagation, elle peut perturber les \u00e9cosyst\u00e8mes en supplantant les plantes indig\u00e8nes et en affectant les nids d'oiseaux et d'autres esp\u00e8ces sauvages.</p>\n<p>La microst\u00e9gie en osier se propage lorsque ses graines s'accrochent aux v\u00eatements ou \u00e0 la fourrure des animaux. On peut \u00e9galement trouver des graines sur les pneus de voiture et les chaussures boueux. Les graines pour oiseaux, le sol, le mat\u00e9riel de p\u00e9pini\u00e8re et le foin peuvent \u00e9galement \u00eatre contamin\u00e9s par la microst\u00e9gie en osier.</p>\n\n<p><a class=\"btn btn-call-to-action\" href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049#form-formulaire\">Signaler une observation de Microst\u00e9gie en osier</a></p>\n<h2>O\u00f9 on la trouve </h2>\n<p>La microst\u00e9gie en osier a \u00e9t\u00e9 confirm\u00e9e dans une petite population sous contr\u00f4le officiel dans le sud de l'Ontario, y compris\u00a0:</p>\n<ul>\n<li>le Parc de Waterloo</li>\n<li>le Parc provincial de Short Hills</li>\n</ul>\n<h2>Ce que vous pouvez faire</h2>\n<ul class=\"lst-spcd\">\n<li>Lorsque vous quittez une zone foresti\u00e8re du sud de l'Ontario, y compris les parcs nationaux et provinciaux, aidez \u00e0 arr\u00eater la propagation des graines de microst\u00e9gie en osier en brossant vos\u00a0:\n<ul>\n<li>v\u00eatements;</li>\n<li>chaussures;</li> \n<li>v\u00e9los; et</li>\n<li>animaux de compagnie.</li></ul></li>\n<li>\u00c9vitez de planter des plantes envahissantes dans votre jardin.</li>\n<li>Utilisez des semences propres, de haute qualit\u00e9 et certifi\u00e9es pour votre jardin afin d'\u00e9viter de planter accidentellement une esp\u00e8ce envahissante.</li>\n<li>Assurez-vous que vos machines, v\u00e9hicules et outils sont exempts de terre et de parties de plantes avant de les d\u00e9placer d'un endroit \u00e0 l'autre.</li>\n<li><a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">Signalez les observations</a> de microst\u00e9gie en osier \u00e0 l'Agence canadienne d'inspection des aliments.</li> \n</ul>\n\n<h2>Comment la rep\u00e9rer</h2>\n\n<div class=\"wb-lbx lbx-gal row\">\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumc_1331669405168_fra.jpg\" title=\"Attribution : David J. Moorhead, Universit\u00e9 de G\u00e9orgie (Bugwood.org)\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumc_1331669405168_fra.jpg\" alt=\"Image 1\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiuma_1331669337748_fra.jpg\" title=\"Attribution : Chuck Bargeron, Universit\u00e9 de G\u00e9orgie (Bugwood.org)\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiuma_1331669337748_fra.jpg\" alt=\"Image 2\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumb_1331669374232_fra.jpg\" title=\"Attribution : Leslie J. Mehrhoff, Universit\u00e9 du Connecticut (Bugwood.org)\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_microstegiumb_1331669374232_fra.jpg\" alt=\"Image 3\" class=\"img-responsive\" width=\"148\">\n</a>\n</div>\n<div class=\"col-md-2 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_MicrostegiumVimineum_img1_1397669814640_fra.jpg\" title=\"\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_MicrostegiumVimineum_img1_1397669814640_fra.jpg\" alt=\"Image 4\" class=\"img-responsive\" width=\"138\">\n</a>\n</div>\n<div class=\"clearfix\"></div>\n</div>\n\n<ul class=\"list-unstyled\">\n<li><strong>Image 1\u00a0:</strong> Feuilles</li> \n<li><strong>Image 2\u00a0:</strong> Feuillage au printemps et en \u00e9t\u00e9</li> \n<li><strong>Image 3\u00a0:</strong> Feuillage en automne</li>\n<li><strong>Image 4\u00a0:</strong> Grain, \u00e9pillet et graine (de gauche \u00e0 droite)</li>\n</ul>\n\n<p>La microst\u00e9gie en osier est une gramin\u00e9e annuelle qui s'\u00e9tend sur les sols des zones perturb\u00e9es, des for\u00eats et des zones humides, car elle pr\u00e9f\u00e8re la lumi\u00e8re basse. Les tiges peuvent atteindre 1\u00a0m de haut et ont de nombreuses branches. Le limbe des feuilles est mince, vert p\u00e2le et effil\u00e9 aux deux extr\u00e9mit\u00e9s. \u00c0 la fin de l'\u00e9t\u00e9, des \u00e9pis de fleurs minces se trouvent \u00e0 l'extr\u00e9mit\u00e9 des tiges. Les plantes prennent une couleur l\u00e9g\u00e8rement violac\u00e9e en automne.</p>\n\n<h2>Ce que nous faisons</h2>\n<p>La microst\u00e9gie en osier est <a href=\"/protection-des-vegetaux/especes-envahissantes/organismes-nuisibles-reglementes/fra/1363317115207/1363317187811\">r\u00e9glement\u00e9e comme un organisme nuisible</a> au Canada en vertu de la <a href=\"/francais/reg/jredirect2.shtml?plavega\"><i>Loi sur la protection des v\u00e9g\u00e9taux</i></a>. L'importation et la circulation sur le territoire canadien de plantes r\u00e9glement\u00e9es et de leurs parties servant \u00e0 la multiplication sont interdites.</p>\n\n<h2>Plus d'informations</h2>\n<ul>\n<li>En savoir plus sur <a href=\"/protection-des-vegetaux/especes-envahissantes/fra/1299168913252/1299168989280\">les esp\u00e8ces envahissantes</a></li>\n<li><a href=\"/DAM/DAM-plants-vegetaux/STAGING/text-texte/plant_pest_cards_japanese_stiltgrass_1589497439502_fra.pdf\">Carte de phytoravageurs\u00a0\u2013 PDF (1,000\u00a0kb)</a></li>\n<li><a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/microstegium-vimineum/fra/1397679674841/1397679719451\">Fiche de renseignements</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}