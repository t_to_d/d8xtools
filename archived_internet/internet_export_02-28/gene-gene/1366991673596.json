{
    "dcr_id": "1366991673596",
    "title": {
        "en": "Asian Long-horned Beetle",
        "fr": "Longicorne asiatique"
    },
    "modified": "2015-08-07",
    "issued": "2014-06-10",
    "templatetype": "content page 1 column",
    "page_type": "0",
    "node_id": "1354137773743",
    "dc_date_created": "2014-06-10",
    "managing_branch": "PAB",
    "parent_node_id": "",
    "short_title": "",
    "exturl": "",
    "teamsite_location": {
        "en": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/alb-la_1366991673596_en",
        "fr": "/default/main/AAFC-AAC/Internet-Internet/Common-Commun/WORKAREA/Common-Commun/templatedata/comm-comm/gene-gene/data/04/alb-la_1366991673596_fr"
    },
    "type_name": "comm-comm/gene-gene",
    "has_dcr_id": true,
    "label": {
        "en": "Asian Long-horned Beetle",
        "fr": "Longicorne asiatique"
    },
    "breadcrumb": {
        "en": "Asian Long-horned Beetle",
        "fr": "Longicorne asiatique"
    },
    "layout_name": "content page 1 column",
    "lang": "en",
    "refaire": false,
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width, initial-scale=1"
        },
        "issued": {
            "en": "2014-06-10",
            "fr": "2014-06-10"
        },
        "modified": {
            "en": "2015-08-07",
            "fr": "2015-08-07"
        },
        "format": {
            "en": "text/html",
            "fr": "text/html"
        },
        "title": {
            "en": "Asian Long-horned Beetle",
            "fr": "Longicorne asiatique"
        },
        "subject": {
            "en": "plant pests;plant diseases",
            "fr": "plantes nuisibles;plantes-maladies et fl\u00e9aux"
        },
        "dcterms:subject": {
            "en": "plant diseases;trees",
            "fr": "maladie des plantes;arbre"
        },
        "description": {
            "en": "The adult Asian long-horned beetle emerges from the infested tree in May. Their emergence holes are about 10-15 mm in diameter. The adult beetle is 2.5-4 centimeters (cm) long. They are shiny black with white spots and have long, white and black banded antennas. Most of the time, the newly emerged adults will remain on the same host tree, but they can fly up to several hundred metres in search of another host tree. Adults are active for mating and egg-laying purposes from early summer to late fall.",
            "fr": "Le longicorne asiatique adulte \u00e9merge des arbres infest\u00e9s en mai. Les trous de sortie ont un diam\u00e8tre d'environ 10 \u00e0 15 millim\u00e8tres (mm). Le longicorne adulte mesure de 2,5 \u00e0 4 centim\u00e8tres (cm) de longueur. Cet insecte est noir lustr\u00e9 tachet\u00e9 de blanc et est muni de longues antennes \u00e0 bandes blanches et noires. La plupart du temps, les adultes nouvellement \u00e9merg\u00e9s demeurent sur le m\u00eame arbre h\u00f4te, mais ils peuvent voler sur des distances de plusieurs centaines de m\u00e8tres \u00e0 la recherche d'un nouvel h\u00f4te. Les adultes deviennent actifs pour l'accouplement et la ponte du d\u00e9but de l'\u00e9t\u00e9 \u00e0 la fin de l'automne."
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "keywords": {
            "en": " Poplar and Willow; Mountain Ash; Maple; Hackberry; Elm; common Horsechesnut; Birch; Aspen;Anoplophora glabripennis",
            "fr": " peuplier et saule; sorbier d'Am\u00e9rique; \u00e9rable; micocoulier; orme; marronnier d'Inde; bouleau; Tremble;Anoplophora glabripennis"
        },
        "audience": {
            "en": "gcaudience",
            "fr": "gcaudience"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "creator": {
            "en": "Agriculture and Agri-Food Canada;Government of Canada",
            "fr": "Agriculture et Agroalimentaire Canada;Gouvernement du Canada"
        },
        "spatial": {
            "en": "gcregions",
            "fr": "gcregions"
        },
        "service": {
            "en": "AAFC-AAC",
            "fr": "AAFC-AAC"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "legalName": {
            "en": "agriculture_and_agri_food_canada",
            "fr": "agriculture_et_agroalimentaire_canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Asian Long-horned Beetle",
        "fr": "Longicorne asiatique"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "\r\n    <p><i>Anoplophora glabripennis</i></p>\n \n<h2>Hosts</h2>\n\n<p>Aspen, Birch, common Horsechesnut, Elm, Hackberry, Maple, Mountain Ash, Poplar and Willow</p>\n \n<h2>Appearance and Life Cycle</h2>\n\n<div class=\"row\">\n\t<div class=\"col-md-4 pull-right\">\n            <figure>\n            <img class=\"mrgn-bttm-0 img-responsive\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/alb-la_1.jpg\" alt=\"Description of this image follows\">\n            <figcaption><i>Adult beetle.<br>Photo credit: Kenneth R. Law, United States Department of Agriculture Animal and Plant Health Inspection Service Plant Protection and Quarantine,</i></figcaption></figure>\n\t</div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n            <p>The adult Asian long-horned beetle emerges from the infested tree in May. Their emergence holes are about 10-15 mm in diameter. The adult beetle is 2.5-4 centimeters (cm) long. They are shiny black with white spots and have long, white and black banded antennas. Most of the time, the newly emerged adults will remain on the same host tree, but they can fly up to several hundred metres in search of another host tree. Adults are active for mating and egg-laying purposes from early summer to late fall.</p>\n            \n            <p>Adult females live approximately 40 days and lay about 25-40 eggs. The females will dig an oval groove about 15-20 millimeters (mm) long in the bark of the tree in which they will lay a single egg that is 5-7 mm long. Either the egg, larvae or pupae will overwinter in the tree. Usually the egg will hatch in about 2 weeks, after which the larvae will burrow into the tree. Larvae are creamy-white with a brown head, segmented and can reach a length of up to 50 mm. They create long, winding tunnels and feed in the innerwood of the tree. The pupae are orange and have a mature length of 32 mm.</p>\n             \n            <h2>Damage</h2>\n            \n            <p>Currently, the Asian long-horned beetle has only been found in eastern Canada and parts of the United States. The insect might become a threat to the prairie provinces in the future. For a distribution map, please visit www.inspection.gc.ca. Adult emergence holes leave a wound about 15-20 mm either on the trunk, branches or exposed roots. The oral grooves for the eggs leave a wound 15-20 mm long which can drip a frothy sap. Insects such as butterflies, bees and wasps will be attracted to the dripping sap. Sawdust and frass can be seen around the tree base, in cracks in the bark and in the branch crotches. Other signs of infestation include yellowing of foliage and pre-mature leaf drop. Injury caused by the Asian long-horned beetle also makes trees more susceptible to other insects and diseases. The damage caused by the Asian long-horned beetle and other factors can kill the tree.</p>\n\t</div>\n</div>\n\n<h2>Control</h2>\n             \n<div class=\"row\">\n\t<div class=\"col-md-4 pull-right\">\n            <figure>\n            <img class=\"mrgn-bttm-0 img-responsive\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/alb-la_2.jpg\" alt=\"Description of this image follows\">\n            <figcaption><i>Larva.<br>Photo credit: Steven Katovich, USDA Forest Service</i></figcaption></figure>\n\t</div>\n\t<div class=\"mrgn-lft-md mrgn-rght-md\">\n\n            \n            <p>There are no chemicals registered for control of the Asian long-horned beetle. Infested trees should be removed and burned before the adults emerge and spread to new areas. If you suspect that there is Asian long-horned beetle in your area, please notify the Canadian Food Inspection Agency.</p>\n\t</div>\n</div>\n\r\n    ",
        "fr": "\r\n     <p><i>Anoplophora glabripennis</i></p>\n \n<h2>H\u00f4tes</h2>\n\n<p>Tremble, bouleau, marronnier d'Inde, orme, micocoulier, \u00e9rable, sorbier d'Am\u00e9rique, peuplier et saule</p>\n \n<h2>Aspect et cycle de vie</h2>\n            \n<div class=\"row\">\n    <div class=\"col-md-4 pull-right\">\n        <figure>\n            <img class=\"mrgn-bttm-0 img-responsive\" alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/alb-la_1.jpg\">\n            <figcaption><i>Longicorne asiatique adulte.<br>\n            Source : Kenneth R. Law, <span lang=\"en\" xml:lang=\"en\">United States Department of Agriculture Animal and Plant Health Inspection Service Plant Protection and Quarantine</span>,</i></figcaption>\n        </figure>\n    </div>\n    <div class=\"mrgn-lft-md mrgn-rght-md\">\n        <p>Le longicorne asiatique adulte \u00e9merge des arbres infest\u00e9s en mai. Les trous de sortie ont un diam\u00e8tre d'environ 10 \u00e0 15\u00a0millim\u00e8tres (mm). Le longicorne adulte mesure de 2,5 \u00e0 4\u00a0centim\u00e8tres (cm) de longueur. Cet insecte est noir lustr\u00e9 tachet\u00e9 de blanc et est muni de longues antennes \u00e0 bandes blanches et noires. La plupart du temps, les adultes nouvellement \u00e9merg\u00e9s demeurent sur le m\u00eame arbre h\u00f4te, mais ils peuvent voler sur des distances de plusieurs centaines de m\u00e8tres \u00e0 la recherche d'un nouvel h\u00f4te. Les adultes deviennent actifs pour l'accouplement et la ponte du d\u00e9but de l'\u00e9t\u00e9 \u00e0 la fin de l'automne.</p>\n        \n        <p>La femelle adulte vit approximativement 40 jours et pond de 25 \u00e0 40 \u0153ufs. La femelle creuse un sillon ovale d'environ 15 \u00e0 20\u00a0mm de longueur dans l'\u00e9corce d'un arbre o\u00f9 elle pondra un seul \u0153uf de 5 \u00e0 7\u00a0mm de longueur. L'insecte hivernera dans l'arbre \u00e0 l'un ou l'autre des stades d'\u0153uf, de larve ou de nymphe. Normalement au bout de deux semaines, l'\u0153uf \u00e9clos et la larve qui en sort s'enfouit dans l'arbre. La larve est de couleur blanc cr\u00e8me et a une t\u00eate brune. Elle est form\u00e9e de segments et peut atteindre une longueur de 50\u00a0mm. La larve creuse de longues galeries sinueuses et se nourrit du bois de c\u0153ur. Quant \u00e0 la nymphe, elle est de couleur orange et atteint 32\u00a0mm \u00e0 maturit\u00e9.</p>\n\n        <h2>Dommages</h2>\n        \n        <p>\u00c0 l'heure actuelle, le longicorne asiatique n'a \u00e9t\u00e9 d\u00e9tect\u00e9 que dans l'est du Canada et dans certaines parties des \u00c9tats-Unis. L'insecte pourrait toutefois devenir une menace pour les provinces des Prairies dans l'avenir. Les trous d'\u00e9mergence des adultes laissent des blessures d'environ 15 \u00e0 20\u00a0mm sur le tronc, les branches ou les racines expos\u00e9es. Les sillons ovales servant \u00e0 accueillir les \u0153ufs forment une blessure de 15 \u00e0 20\u00a0mm de longueur laissant parfois s'\u00e9chapper une s\u00e8ve mousseuse. Ces \u00e9coulements de s\u00e8ve peuvent attirer des insectes comme les papillons, les abeilles et les gu\u00eapes. La pr\u00e9sence de l'insecte se manifeste \u00e9galement par la pr\u00e9sence de sciure et d'excr\u00e9ments au pied de l'arbre, dans les fissures de l'\u00e9corce et dans les fourches des branches. Parmi les autres signes d'infestation, mentionnons le jaunissement du feuillage et la chute pr\u00e9matur\u00e9e des feuilles. En outre, les blessures caus\u00e9es par le longicorne asiatique rendent les arbres plus vuln\u00e9rables aux autres insectes et maladies. Les dommages caus\u00e9s par le longicorne asiatique et autres facteurs peuvent causer la mort de l'arbre.</p>                    \n    </div>\n</div>\n            \n <h2>Lutte antiparasitaire</h2>\n                         \n<div class=\"row\">\n    <div class=\"col-md-4 pull-right\">\n\n\n         <figure>\n        <img class=\"mrgn-bttm-0 img-responsive\" alt=\"La description de cette image suit\" src=\"https://www.agr.gc.ca/resources/prod/img/pfra-arap/img/alb-la_2.jpg\">\n        <figcaption><i>Larve.<br>\n        Source : Steven Katovich, <span lang=\"en\" xml:lang=\"en\">United States Department of Agriculture Forest Service</span></i></figcaption>\n            </figure>\n    </div>\n    <div class=\"mrgn-lft-md mrgn-rght-md\">\n        <p>Aucun produit chimique n'est homologu\u00e9 pour la lutte contre le longicorne asiatique. Les arbres infest\u00e9s doivent \u00eatre abattus et br\u00fbl\u00e9s avant que les adultes \u00e9mergent et se propagent \u00e0 d'autres r\u00e9gions. Si vous soup\u00e7onnez la pr\u00e9sence de longicornes asiatiques dans votre r\u00e9gion, pri\u00e8re d'en aviser l'Agence canadienne d'inspection des aliments.</p>\n    </div>\n</div> \n\r\n    "
    }
}