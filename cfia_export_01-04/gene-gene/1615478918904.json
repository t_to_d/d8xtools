{
    "dcr_id": "1615478918904",
    "title": {
        "en": "Proposed document to be incorporated by reference \u2013 Compendium of Non-Feed Product Brochures",
        "fr": "Documents propos\u00e9s \u00e0 l'incorporation par renvoi \u2013 Recueil des notices sur les produits non alimentaires"
    },
    "html_modified": "11-6-2021",
    "modified": "11-6-2021",
    "issued": "15-2-2021",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/feed_reg_modernization_cnfpb_1615478918904_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/feed_reg_modernization_cnfpb_1615478918904_fra"
    },
    "parent_ia_id": "1612971995765",
    "ia_id": "1615478919255",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Proposed document to be incorporated by reference \u2013 Compendium of Non-Feed Product Brochures",
        "fr": "Documents propos\u00e9s \u00e0 l'incorporation par renvoi \u2013 Recueil des notices sur les produits non alimentaires"
    },
    "label": {
        "en": "Proposed document to be incorporated by reference \u2013 Compendium of Non-Feed Product Brochures",
        "fr": "Documents propos\u00e9s \u00e0 l'incorporation par renvoi \u2013 Recueil des notices sur les produits non alimentaires"
    },
    "templatetype": "content page 1 column",
    "node_id": "1615478919255",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1612969567098",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/regulatory-modernization/compendium-of-non-feed-product-brochures/",
        "fr": "/sante-des-animaux/aliments-du-betail/modernisation-du-reglement-sur-les-aliments-du-bet/recueil-des-notices-sur-les-produits-non-alimentai/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Proposed document to be incorporated by reference \u2013 Compendium of Non-Feed Product Brochures",
            "fr": "Documents propos\u00e9s \u00e0 l'incorporation par renvoi \u2013 Recueil des notices sur les produits non alimentaires"
        },
        "description": {
            "en": "The Canadian Food Inspection Agency (CFIA) is proposing the Compendium of Non-Feed Product Brochures (NFPB) to be incorporated by reference (IbR) into the proposed Feeds Regulations, 2022.",
            "fr": "L'Agence canadienne d'inspection des aliments (ACIA) propose que le Recueil de produits non alimentaires soit incorpor\u00e9 par renvoi (IPR) dans le projet de R\u00e8glement de 2022 sur les aliments du b\u00e9tail."
        },
        "keywords": {
            "en": "animals, disease, freedom, Compendium, Non-Feed Product Brochures",
            "fr": "animaux, maladies d\u00e9clarables, indemnes, produits non alimentaires, Recueil des notices"
        },
        "dcterms.subject": {
            "en": "imports,inspection,veterinary medicine,animal health",
            "fr": "importation,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2021-06-11",
            "fr": "2021-06-11"
        },
        "modified": {
            "en": "2021-06-11",
            "fr": "2021-06-11"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Proposed document to be incorporated by reference \u2013 Compendium of Non-Feed Product Brochures",
        "fr": "Documents propos\u00e9s \u00e0 l'incorporation par renvoi \u2013 Recueil des notices sur les produits non alimentaires"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"well\">\n<section>\n<p>This document was part of a consultation on proposed changes to the <i>Feeds Regulations</i>, which ran from <span class=\"nowrap\">June 12, 2021</span> to <span class=\"nowrap\">October 15, 2021.</span> This consultation is now closed.</p>\n</section>\n</div>\n\n<p>The Canadian Food Inspection Agency (CFIA) is proposing the Compendium of Non-Feed Product Brochures (NFPB) to be incorporated by reference (IbR) into the proposed <i>Feeds Regulations, 2022</i>. </p>\n<p>This compendium lists non-feed products such as veterinary biologics or veterinary health products that may be added to livestock feeds, and the conditions in which they are allowed to be added to feed. It specifies the following:</p>\n<ul>\n<li>species of livestock </li>\n<li>level of non-feed product </li>\n<li>directions for feeding</li>\n<li>purpose for which each non-feed product may be used </li>\n</ul>\n<p>In addition, it sets out the labelling requirements for the feed that contains the non-feed product to ensure compliance with labelling standards (for example, product level, approved claim, directions for use, caution and warning statements).</p>\n<p>This new IbR document is modelled after the Compendium of Medicating Ingredient Brochures and is designed to allow non-feed products which are approved by another Canadian competent authority (for example, Health Canada) to be mixed into livestock feeds. </p>\n<p class=\"mrgn-tp-lg\"><strong>NFPB-0001</strong>: <i lang=\"la\">Escherichia coli</i> polyclonal antibodies, chicken egg origin</p>\n<table class=\"table table-bordered\"> \n<caption id=\"t1\" class=\"text-left\">Table of NFPB Characteristics</caption>\n<thead> \n<tr> \n<th class=\"active\">Code</th> \n<th class=\"active\">Product type</th> \n<th class=\"active\">Approved claims</th> \n<th class=\"active\">Approved species</th>\n<th class=\"active\">Authorization number</th>\n<th class=\"active\">Authorizing department</th>\n<th class=\"active\">Approved brands</th>\n<th class=\"active\">Active ingredient(s)</th> \n</tr> \n</thead> \n<tbody> \n<tr>\n<td>NFPB-0001</td>\n<td>Licensed Veterinary Biologic</td>\n<td>Claim 1</td>\n<td>Swine</td>\n<td>CCVB# 880AY/E1.0/N17</td>\n<td>Canadian Center for Veterinary Biologics</td>\n<td>Hyper-Egg K-88</td>\n<td><i lang=\"la\">Escherichia coli</i> polyclonal antibodies</td>\n</tr>\n</tbody> \n</table>\n\t\n<p><strong>Approved for use in meal or pellet feed</strong></p>\n<h2>Claim 1 \u2013 for swine</h2>\n<p><strong>As an aid in the prevention of diarrhea due to infection with enterotoxigenic <i lang=\"la\">Escherichia coli</i> K-88.</strong></p>\n\n<h3>Approved inclusion level:</h3>\n<p>2 to 4\u00a0kg/tonne of complete feed.</p>\n<h4>Directions</h4>\n<p>Feed the complete feed/veterinary biologic mixture continuously as the sole ration to piglets during the\u00a02 week period following weaning.</p>\n\n<section class=\"alert alert-danger\">\n<h3>Warning</h3>\n<p>Not for human consumption</p>\n</section>\n<h3>Notes</h3>\n<ol>\n<li>Store in a cool and dry place</li>\n<li>Avoid humidity and direct sunlight</li>\n<li>Feed should not be pelleted at temperatures above 70\u00b0C</li>\n</ol>\n\t\n\n\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"well\">\n<section>\n<p>Ce document a fait partie de la consultation au sujet des modifications propos\u00e9es au <i>R\u00e8glement sur les aliments b\u00e9tail</i> qui \u00e9tait ouverte <span class=\"nowrap\">le 12 juin 2021</span> au <span class=\"nowrap\">15 octobre 2021.</span> Cette consultation est maintenant ferm\u00e9e.</p>\n</section>\n</div>\n\n<p>L'Agence canadienne d'inspection des aliments (ACIA) propose que le Recueil de produits non alimentaires  soit incorpor\u00e9 par renvoi (IPR) dans le projet de <i>R\u00e8glement de 2022 sur les aliments du b\u00e9tail</i>.</p>\n<p>Ce recueil dresse la liste des produits non alimentaires, tels que les produits biologiques v\u00e9t\u00e9rinaires ou les produits de sant\u00e9 v\u00e9t\u00e9rinaire, qui peuvent \u00eatre ajout\u00e9s aux aliments pour animaux de ferme, ainsi que les conditions dans lesquelles ils sont autoris\u00e9s \u00e0 \u00eatre ajout\u00e9s aux aliments. Il pr\u00e9cise notamment les \u00e9l\u00e9ments suivants\u00a0:</p>\n<ul>\n<li>esp\u00e8ce d'animaux de ferme</li>\n<li>concentration du produit non alimentaire</li>\n<li>mode d'emploi</li>\n<li>usage de chaque produit non alimentaire</li>\n</ul>\n<p>Il d\u00e9crit en outre les exigences d'\u00e9tiquetage pour les aliments qui contiennent de tels produits non alimentaires afin d'assurer la conformit\u00e9 avec les normes d'\u00e9tiquetage (par exemple, concentration du produit non alimentaire, all\u00e9gation approuv\u00e9e, directives d'utilisation, avertissements et pr\u00e9cautions).</p>\n<p>Ce nouveau document IPR s'inspire du Recueil des notices sur les substances m\u00e9dicatrices et vise \u00e0 permettre aux produits non destin\u00e9s \u00e0 l'alimentation animale qui sont approuv\u00e9s par une autre autorit\u00e9 comp\u00e9tente canadienne (par exemple, Sant\u00e9 Canada) d'\u00eatre m\u00e9lang\u00e9s aux aliments pour animaux. </p>\n<p class=\"mrgn-tp-lg\"><strong>PNAN-0001 </strong>\u00a0: Anticorps polyclonaux contre <i lang=\"la\">Escherichia coli</i>, dans l'\u0153uf de poules</p>\n<table class=\"table table-bordered\"> \n<caption id=\"t1\" class=\"text-left\">Table caract\u00e9ristiques de PNAN</caption>\n<thead> \n<tr> \n<th class=\"active\">Code</th> \n<th class=\"active\">Type de produit</th> \n<th class=\"active\">All\u00e9gations approuv\u00e9e</th> \n<th class=\"active\">Esp\u00e8ces approuv\u00e9e</th>\n<th class=\"active\">Num\u00e9ro d'autorisation</th>\n<th class=\"active\">Minist\u00e8res approuv\u00e9e</th>\n<th class=\"active\">Marques  approuv\u00e9es</th>\n<th class=\"active\">Ingr\u00e9dient(s) actif</th> \n</tr> \n</thead> \n<tbody> \n<tr>\n<td>PNAN-0001</td>\n<td>Produit biologique v\u00e9t\u00e9rinaire homologu\u00e9</td>\n<td>All\u00e9gation 1</td>\n<td>Porc</td>\n<td>CCPVB# 880AY/E1.0/N17</td>\n<td>Centre Canadien des produits biologiques v\u00e9terinaires</td>\n<td lang=\"en\">Hyper-Egg<br>K-88</td>\n<td>Anticorps polyclonaux contre <i lang=\"la\">Escherichia coli</i></td>\n</tr>\n</tbody> \n</table>\n\t\n<p><strong>Utilisation approuv\u00e9es dans les aliments sous forme de farine ou de granule</strong></p>\n<h2>All\u00e9gation 1 \u2013 pour Porcs</h2>\n<p><strong>Comme assistant dans la pr\u00e9vention de la diarrh\u00e9e caus\u00e9e par l'infection de l'<i lang=\"la\">Escherichia coli</i> K-88 ent\u00e9rotoxinog\u00e8ne</strong>.</p>\n\n<h3>Niveau d'inclusion approuv\u00e9\u00a0:</h3>\n<p>\u00c0 une dose de\u00a02 \u00e0\u00a04\u00a0kg par tonne d'aliments complet.</p>\n<h4>Mode d'emploi</h4>\n<p>Distribuer ce m\u00e9lange d'aliment complet/produit biologique v\u00e9t\u00e9rinaire sans interruption comme seule ration aux porcelets pendant la p\u00e9riode de deux semaines suivant le sevrage.</p>\n\n<section class=\"alert alert-danger\">\n<h3>Mise en garde\u00a0:</h3>\n<p>N'est pas pour la consommation humaine.</p>\n</section>\n\n<h3>Nota</h3>\n<ol>\n<li>Conserver dans un endroit frais et sec.</li>\n<li>\u00c9vitez l'humidit\u00e9 et l'ensoleillement direct.</li>\n<li>Les aliments ne devraient pas \u00eatre transform\u00e9s en granules \u00e0 des temp\u00e9ratures au-dessus de 70\u00a0\u00b0C.</li>\n</ol>\n\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": false,
    "chat_wizard": false
}