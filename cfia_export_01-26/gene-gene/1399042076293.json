{
    "dcr_id": "1399042076293",
    "title": {
        "en": "Bovine Surveillance System (BSS)",
        "fr": "Syst\u00e8me de surveillance des bovins (SSB)"
    },
    "html_modified": "2024-01-26 2:22:24 PM",
    "modified": "2016-10-06",
    "issued": "2014-05-02",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_surveillance_bss_1399042076293_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_surveillance_bss_1399042076293_fra"
    },
    "ia_id": "1399042275724",
    "parent_ia_id": "1313720675875",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1313720675875",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Bovine Surveillance System (BSS)",
        "fr": "Syst\u00e8me de surveillance des bovins (SSB)"
    },
    "label": {
        "en": "Bovine Surveillance System (BSS)",
        "fr": "Syst\u00e8me de surveillance des bovins (SSB)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1399042275724",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1313720601375",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/surveillance/bovine-surveillance-system-bss-/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/surveillance-des-maladies/systeme-de-surveillance-des-bovins-ssb-/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Bovine Surveillance System (BSS)",
            "fr": "Syst\u00e8me de surveillance des bovins (SSB)"
        },
        "description": {
            "en": "Animal health protection is vital to food safety and public health, the trade of Canadian livestock and livestock products, and the economic well-being of Canada's agriculture sector.",
            "fr": "Il est essentiel de prot\u00e9ger la sant\u00e9 des animaux pour assurer la salubrit\u00e9 des aliments et la sant\u00e9 publique, le maintien de l'acc\u00e8s aux march\u00e9s pour le b\u00e9tail et les produits d'\u00e9levage canadiens et la prosp\u00e9rit\u00e9 \u00e9conomique du secteur agricole."
        },
        "keywords": {
            "en": "animals, animal health, protection, food safety, public health, trade, livestock, products, agriculture sector, animal disease, surveillance",
            "fr": "animaux, sant\u00e9 des animaux, protection, salubrit\u00e9 des aliments, sant\u00e9 publique, march\u00e9, b\u00e9tail, produits, secteur agricole, maladies animales, surveillance"
        },
        "dcterms.subject": {
            "en": "agri-food products,animal health,food,food inspection,livestock,products,samples,testing",
            "fr": "produit agro-alimentaire,sant\u00e9 animale,aliment,inspection des aliments,b\u00e9tail,produit,\u00e9chantillon,test"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2014-05-02",
            "fr": "2014-05-02"
        },
        "modified": {
            "en": "2016-10-06",
            "fr": "2016-10-06"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Bovine Surveillance System (BSS)",
        "fr": "Syst\u00e8me de surveillance des bovins (SSB)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Animal health protection is vital to food safety and public health, the trade of Canadian livestock and livestock products, and the economic well-being of Canada's agriculture sector. The Canadian Food Inspection Agency (CFIA) works closely with provincial and territorial governments, and livestock and poultry industries to prevent animal disease and conduct regular surveillance activities. Surveillance activities are important because they help to demonstrate Canada's freedom from specific diseases.</p>\n<p>One such activity is the Bovine Surveillance System (BSS). Through the <abbr title=\"Bovine Surveillance System\">BSS</abbr>, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> collects and analyses information gathered from different sources to support Canada's claim that it is free from <a href=\"/animal-health/terrestrial-animals/diseases/reportable/brucellosis/eng/1305672974966/1305673091542\"><strong>brucellosis</strong></a> and <a href=\"/animal-health/terrestrial-animals/diseases/reportable/bluetongue/fact-sheet/eng/1306116803992/1306121522520\"><strong>bluetongue</strong></a> in cattle.</p>\n<h2 class=\"h5\">How does the <abbr title=\"Bovine Surveillance System\">BSS</abbr> work?</h2>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> uses both new and historical data in its <abbr title=\"Bovine Surveillance System\">BSS</abbr> activities. These sources include periodic surveys, abattoir surveillance, targeted surveillance, and testing through the import/export and artificial insemination programs. Similar surveillance systems exist for the detection of specific diseases in swine and poultry.</p>\n<p>Under the <abbr title=\"Bovine Surveillance System\">BSS</abbr>, information is regularly collected and analyzed which enables the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> to assess the status of brucellosis and bluetongue in Canadian cattle on a continuous basis. Large periodic surveys, conducted every three to five years, have been replaced by the abattoir component of the <abbr title=\"Bovine Surveillance System\">BSS</abbr>.</p>\n<h2 class=\"h5\">How does the abattoir component work?</h2>\n<p>Blood samples are randomly collected from cattle at slaughter on an ongoing basis and sent to a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> laboratory. There, they go through a series of screening tests known as \"serological tests.\" If one of the samples tests positive, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> conducts an investigation.</p>\n<p>Approximately 3,000 samples will be collected and tested every year under this component of the <abbr title=\"Bovine Surveillance System\">BSS</abbr>.</p>\n<h2 class=\"h5\">What does a positive serological test result mean?</h2>\n<p>A positive result from a serological test means that an animal may have been exposed to a virus (bluetongue) or bacteria (<i lang=\"la\">Brucella</i>) at some point previously. It does not mean that an animal is currently infected. A positive result provides an indication that further investigation is required.</p>\n<h2 class=\"h5\">What action does the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> take if a sample tests positive?</h2>\n<p>When serological testing turns up a positive result, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> locates the animal's farm of origin and traces the animal's movement history. Agency staff would visit the premises where the suspect animals lived to assess the health of the herds and to complete an epidemiological investigation. Additional samples would be taken from the herds for testing at a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> laboratory.</p>\n<p>If an animal is confirmed to be infected, the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> may initiate a variety of disease control measures, which may include quarantine, testing and possibly ordering destruction of infected/exposed animals, depending on the disease. Producers may be awarded compensation for animals that are ordered destroyed by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> for disease control purposes.</p>\n<h2 class=\"h5\">Does brucellosis or bluetongue pose a risk to human health?</h2>\n<p>While brucellosis can cause a disease in humans called \"undulant fever,\" human cases are rare in Canada. Sanitary practices in slaughterhouses and pasteurization of milk are effective in preventing the vast majority of human cases of brucellosis.</p>\n<p>There is no risk to human health associated with bluetongue.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>Il est essentiel de prot\u00e9ger la sant\u00e9 des animaux pour assurer la salubrit\u00e9 des aliments et la sant\u00e9 publique, le maintien de l'acc\u00e8s aux march\u00e9s pour le b\u00e9tail et les produits d'\u00e9levage canadiens et la prosp\u00e9rit\u00e9 \u00e9conomique du secteur agricole. L'Agence canadienne d'inspection des aliments (ACIA) collabore \u00e9troitement avec les gouvernements provinciaux et territoriaux et les industries du b\u00e9tail et de la volaille \u00e0 la pr\u00e9vention des maladies animales et \u00e0 l'ex\u00e9cution des activit\u00e9s r\u00e9guli\u00e8res de surveillance. Les activit\u00e9s de surveillance jouent un r\u00f4le important en aidant \u00e0 d\u00e9montrer que le Canada est indemne de certaines maladies.</p>\n<p>Parmi ces activit\u00e9s, mentionnons la mise en \u0153uvre d'un Syst\u00e8me de surveillance des bovins (SSB). Par le truchement du <abbr title=\"Syst\u00e8me de surveillance des bovins\">SSB</abbr>, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> recueille et analyse des renseignements aupr\u00e8s de diverses sources afin d'\u00e9tayer la d\u00e9claration du Canada \u00e0 l'effet que son b\u00e9tail est indemne de la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/brucellose/fra/1305672974966/1305673091542\"><strong>brucellose</strong></a> et de <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fievre-catarrhale-du-mouton/fra/1306107020373/1306117227621\"><strong>la fi\u00e8vre catarrhale du mouton</strong></a>.</p>\n<h2 class=\"h5\">En quoi consiste le <abbr title=\"Syst\u00e8me de surveillance des bovins\">SSB</abbr>?</h2>\n<p>Aux fins de ses activit\u00e9s du <abbr title=\"Syst\u00e8me de surveillance des bovins\">SSB</abbr>, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> utilise \u00e0 la fois des donn\u00e9es ant\u00e9rieures et de nouvelles donn\u00e9es. Ces donn\u00e9es proviennent des enqu\u00eates p\u00e9riodiques, de la surveillance des abattoirs, de la surveillance cibl\u00e9e et des analyses effectu\u00e9es \u00e0 l'appui des programmes d'importation et d'exportation et d'ins\u00e9mination artificielle. Des syst\u00e8mes de surveillance similaires existent pour le d\u00e9pistage de certaines maladies chez les porcins et la volaille.</p>\n<p>Dans le cadre du <abbr title=\"Syst\u00e8me de surveillance des bovins\">SSB</abbr>, des renseignements sont recueillis et analys\u00e9s p\u00e9riodiquement, permettant ainsi \u00e0 l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> d'\u00e9valuer de fa\u00e7on continue la situation \u00e0 l'\u00e9gard de la brucellose et de la fi\u00e8vre catarrhale du mouton chez le b\u00e9tail canadien. La tenue de vastes enqu\u00eates p\u00e9riodiques qui \u00e9taient men\u00e9es tous les trois ou cinq ans a \u00e9t\u00e9 remplac\u00e9e par un volet du <abbr title=\"Syst\u00e8me de surveillance des bovins\">SSB</abbr> portant sur les abattoirs.</p>\n<h2 class=\"h5\">En quoi consiste le volet \u00ab\u00a0abattoir\u00a0\u00bb?</h2>\n<p>Des \u00e9chantillons de sang sont r\u00e9guli\u00e8rement pr\u00e9lev\u00e9s au hasard sur du b\u00e9tail au moment de l'abattage. Ces \u00e9chantillons sont ensuite envoy\u00e9s \u00e0 un laboratoire de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>, o\u00f9 ils sont soumis \u00e0 une s\u00e9rie d'analyses de d\u00e9pistage (\u00ab\u00a0\u00e9preuves s\u00e9rologiques\u00a0\u00bb). Si l'un des \u00e9chantillons obtient un r\u00e9sultat positif, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> lance une enqu\u00eate.</p>\n<p>Environ 3 000 \u00e9chantillons seront pr\u00e9lev\u00e9s et analys\u00e9s chaque ann\u00e9e dans le cadre de ce volet du <abbr title=\"Syst\u00e8me de surveillance des bovins\">SSB</abbr>.</p>\n<h2 class=\"h5\">Que signifie un r\u00e9sultat d'analyse positif?</h2>\n<p>L'obtention d'un r\u00e9sultat positif montre que l'animal a pu \u00eatre expos\u00e9 \u00e0 un virus (fi\u00e8vre catarrhale du mouton) ou \u00e0 une bact\u00e9rie (<i lang=\"la\">Brucella</i>) \u00e0 un certain moment au cours de sa vie, mais cela ne signifie pas pour autant qu'il est actuellement infect\u00e9. Un r\u00e9sultat positif indique qu'il faut proc\u00e9der \u00e0 une enqu\u00eate plus approfondie.</p>\n<h2 class=\"h5\">Quelles mesures l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> prend-elle lorsque l'analyse d'un \u00e9chantillon donne un r\u00e9sultat positif?</h2>\n<p>Lorsque l'analyse d'un \u00e9chantillon donne un r\u00e9sultat positif, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> remonte \u00e0 l'\u00e9levage d'origine et retrace l'historique des d\u00e9placements de l'animal concern\u00e9. Les employ\u00e9s de l'Agence visitent ensuite les installations o\u00f9 ont v\u00e9cu les animaux suspects pour \u00e9valuer l'\u00e9tat de sant\u00e9 du troupeau et mener une enqu\u00eate \u00e9pid\u00e9miologique. D'autres \u00e9chantillons pourraient \u00eatre pr\u00e9lev\u00e9s au sein du troupeau et envoy\u00e9s aux laboratoires de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> pour analyse.</p>\n<p>Si, \u00e0 la suite des analyses, il est confirm\u00e9 qu'un animal est infect\u00e9, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut d\u00e9ployer diverses mesures de lutte contre les maladies pouvant comprendre la mise en quarantaine, l'analyse d'\u00e9chantillons, voire l'abattage des animaux infect\u00e9s ou expos\u00e9s, selon la maladie consid\u00e9r\u00e9e. Des indemnit\u00e9s pourraient \u00eatre vers\u00e9es aux producteurs \u00e0 qui l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> ordonne d'abattre des animaux aux fins de la lutte contre les maladies.</p>\n<h2 class=\"h5\">La brucellose ou la fi\u00e8vre catarrhale du mouton constituent-elles un risque pour la sant\u00e9 humaine?</h2>\n<p>La brucellose peut causer une maladie appel\u00e9e \u00ab\u00a0fi\u00e8vre ondulante\u00a0\u00bb chez les humains, mais de tels cas sont rares au Canada. Les pratiques sanitaires utilis\u00e9es dans les abattoirs ainsi que la pasteurisation du lait permettent g\u00e9n\u00e9ralement d'\u00e9viter les cas de brucellose chez les humains.</p>\n<p>Quant \u00e0 la fi\u00e8vre catarrhale du mouton, elle ne pr\u00e9sente aucun risque pour la sant\u00e9 humaine.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}