{
    "dcr_id": "1476287494739",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Lepidium campestre</i> (Field peppergrass)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Lepidium campestre</i> (L\u00e9pidie des champs)"
    },
    "html_modified": "2024-03-12 3:57:30 PM",
    "modified": "2017-11-06",
    "issued": "2017-11-07",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_lepidium_campestre_1476287494739_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_lepidium_campestre_1476287494739_fra"
    },
    "ia_id": "1476287495238",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Lepidium campestre</i> (Field peppergrass)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Lepidium campestre</i> (L\u00e9pidie des champs)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Lepidium campestre</i> (Field peppergrass)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Lepidium campestre</i> (L\u00e9pidie des champs)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476287495238",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/lepidium-campestre/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/lepidium-campestre/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Lepidium campestre (Field peppergrass)",
            "fr": "Semence de mauvaises herbe : Lepidium campestre (L\u00e9pidie des champs)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Lepidium campestre, Brassicaceae, Field peppergrass",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Lepidium campestre, Brassicaceae, L\u00e9pidie des champs"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-07",
            "fr": "2017-11-07"
        },
        "modified": {
            "en": "2017-11-06",
            "fr": "2017-11-06"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Lepidium campestre (Field peppergrass)",
        "fr": "Semence de mauvaises herbe : Lepidium campestre (L\u00e9pidie des champs)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Brassicaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Field peppergrass</p>\n\n<h2>Regulation</h2>\n<p>Secondary Noxious, Class 3 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"Alberta\">AB</abbr>, <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Newfoundland and Labrador\">NF</abbr>, <abbr title=\"Nova Scotia\">NS</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Prince Edward Island\">PE</abbr> and <abbr title=\"Quebec\">QC</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to parts of Europe and western parts of temperate Asia (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Naturalized in eastern temperate Asia, Europe, Australia, New Zealand, North and South America (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual or Biennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 2.0 - 3.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 1.5 - 2.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed thickness: 1.5 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed oval, compressed, tapers at hilum end</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed surface densely covered in small tubercles</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed dark reddish-brown</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>One end of seed is rounded and the other end is narrow with yellowish tissue over the hilum</li>\n<li>Pale line visible between the radicle and the cotyledons on the seed</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, pastures, gardens, old fields, meadows, open-flats, forests, rocky slopes, roadsides and disturbed areas (<abbr title=\"Flora of North America\">FNA</abbr> 1993+<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, Darbyshire 2003<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Field peppergrass is poisonous to livestock (Darbyshire 2003<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Globe-pod hoary cress (<i lang=\"la\">Lepidium appelianum</i>)</h3>\n<ul>\n<li>Globe-pod hoary cress has a similar oval shape and roughened surface as field peppergrass.</li>\n\n<li>Globe-pod hoary cress (length: 2.0 - 2.5 <abbr title=\"millimetres\">mm</abbr>; width: 1.3 - 1.8 <abbr title=\"millimetres\">mm</abbr>) is generally smaller than field peppergrass, reddish rather than dark reddish-brown, the hilum area is rounded and the surface is not tuberculate.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_04cnsh_1475603133688_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Field peppergrass (<i lang=\"la\">Lepidium campestre</i>) seeds and pod </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_02cnsh_1475603113463_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Field peppergrass (<i lang=\"la\">Lepidium campestre</i>) seed </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_01cnsh_1475603092319_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Field peppergrass (<i lang=\"la\">Lepidium campestre</i>) seed </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_copyright_1475603196909_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Field peppergrass (<i lang=\"la\">Lepidium campestre</i>) seed </figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_appelianum_05cnsh_1475603010855_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Globe-pod hoary cress (<i lang=\"la\">Lepidium appelianum</i>) seeds and pod\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_appelianum_02cnsh_1475602944981_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Globe-pod hoary cress (<i lang=\"la\">Lepidium appelianum</i>) seed\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_appelianum_03cnsh_1475602965290_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Globe-pod hoary cress (<i lang=\"la\">Lepidium appelianum</i>) seed, hilum view\n</figcaption>\n</figure>\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Flora of North America\">FNA</abbr>. 1993+</strong>. Flora of North America North of Mexico. 19+ vols. Flora of North America Editorial Committee, eds. New York and Oxford, http://www.fna.org/FNA/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Brassicaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>L\u00e9pidie des champs</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible secondaire, cat\u00e9gorie 3 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> L'esp\u00e8ce est pr\u00e9sente en <abbr title=\"Alberta\">Alb.</abbr>, en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr>, \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr>, en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr>, \u00e0 l'<abbr title=\"\u00cele-du-Prince-\u00c9douard\">\u00ce.-P.-\u00c9.</abbr> et au <abbr title=\"Qu\u00e9bec\">Qc</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de certaines parties de l'Europe et des r\u00e9gions occidentales de l'Asie temp\u00e9r\u00e9e (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2a-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Naturalis\u00e9e dans en Asie temp\u00e9r\u00e9e orientale, en Europe, en Australie, en   Nouvelle-Z\u00e9lande, en Am\u00e9rique du Nord et en Am\u00e9rique du Sud (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2b-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle ou bisannuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 2,0 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 1,5 \u00e0 2,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>\u00c9paisseur de la graine\u00a0: 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine elliptique, comprim\u00e9e, se r\u00e9tr\u00e9cissant \u00e0 l'extr\u00e9mit\u00e9 hilaire</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Surface de la graine dens\u00e9ment recouverte de petits tubercules</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine brun rouge\u00e2tre fonc\u00e9</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Une extr\u00e9mit\u00e9 de la graine est arrondie et l'autre est \u00e9troite et garnie de tissus jaun\u00e2tres; sur le hile</li>\n<li>P\u00e2le ligne visible entre la radicule et les cotyl\u00e9dons sur la graine</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, p\u00e2turages, jardins, champs abandonn\u00e9s, pr\u00e9s, plaines   d\u00e9gag\u00e9es, for\u00eats, pentes rocheuses, bords de chemin et terrains   perturb\u00e9s (<abbr lang=\"en\" title=\"Flora of North America\">FNA</abbr>, 1993+<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; Darbyshire, 2003<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La l\u00e9pidie des champs est toxique pour le b\u00e9tail (Darbyshire, 2003<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Cranson velu (<i lang=\"la\">Lepidium appelianum</i>)</h3>\n<ul>\n<li>La graine du cranson velu ressemble \u00e0 celle de la l\u00e9pidie des champs par sa forme elliptique et sa surface rugueuse.</li>\n\n<li>La graine du cranson velu (longueur\u00a0: 2,0 \u00e0 2,5 <abbr title=\"millim\u00e8tre\">mm</abbr>; largeur\u00a0: 1,3 \u00e0 1,8 <abbr title=\"millim\u00e8tre\">mm</abbr>) est g\u00e9n\u00e9ralement plus petite que celle de la l\u00e9pidie des champs, elle est rouge\u00e2tre plut\u00f4t que brun rouge\u00e2tre fonc\u00e9, sa zone hilaire est arrondie et sa surface n'est pas tubercul\u00e9e.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_04cnsh_1475603133688_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>L\u00e9pidie des champs (<i lang=\"la\">Lepidium campestre</i>) graines et silicule </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_02cnsh_1475603113463_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>L\u00e9pidie des champs (<i lang=\"la\">Lepidium campestre</i>) graine </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_01cnsh_1475603092319_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>L\u00e9pidie des champs (<i lang=\"la\">Lepidium campestre</i>) graine </figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_campestre_copyright_1475603196909_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>L\u00e9pidie des champs (<i lang=\"la\">Lepidium campestre</i>) graine</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_appelianum_05cnsh_1475603010855_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Cranson velu (<i lang=\"la\">Lepidium appelianum</i>) graines et silicule</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_appelianum_02cnsh_1475602944981_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Cranson velu (<i lang=\"la\">Lepidium appelianum</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_lepidium_appelianum_03cnsh_1475602965290_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Cranson velu (<i lang=\"la\">Lepidium appelianum</i>) graine, vue hile\n</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Flora of North America\">FNA</abbr>. 1993+</strong>. Flora of North America North of Mexico. 19+ vols. Flora of North America Editorial Committee, eds. New York and Oxford, http://www.fna.org/FNA/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}