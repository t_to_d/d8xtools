{
    "dcr_id": "1330099413455",
    "title": {
        "en": "Facts about infectious pancreatic necrosis",
        "fr": "Fiche de renseignements sur la n\u00e9crose pancr\u00e9atique infectieuse"
    },
    "html_modified": "2024-01-26 2:20:59 PM",
    "modified": "2020-09-21",
    "issued": "2012-02-24 11:03:35",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_pancreatic_factsheet_1330099413455_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_pancreatic_factsheet_1330099413455_fra"
    },
    "ia_id": "1330099555496",
    "parent_ia_id": "1330112080550",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1330112080550",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Facts about infectious pancreatic necrosis",
        "fr": "Fiche de renseignements sur la n\u00e9crose pancr\u00e9atique infectieuse"
    },
    "label": {
        "en": "Facts about infectious pancreatic necrosis",
        "fr": "Fiche de renseignements sur la n\u00e9crose pancr\u00e9atique infectieuse"
    },
    "templatetype": "content page 1 column",
    "node_id": "1330099555496",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1330111800962",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/reportable-diseases/infectious-pancreatic-necrosis/fact-sheet/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/necrose-pancreatique-infectieuse/fiche-de-renseignements/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Facts about infectious pancreatic necrosis",
            "fr": "Fiche de renseignements sur la n\u00e9crose pancr\u00e9atique infectieuse"
        },
        "description": {
            "en": "Infectious pancreatic necrosis is an infectious disease of some freshwater and saltwater finfish. It is caused by the infectious pancreatic necrosis virus, which belongs to the family Birnaviridae.",
            "fr": "La n\u00e9crose pancr\u00e9atique infectieuse est une maladie qui touche certains poissons d?eau douce et d?eau sal\u00e9e. Elle est caus\u00e9e par le virus de la n\u00e9crose pancr\u00e9atique infectieuse, qui appartient \u00e0 la famille des Birnaviridae."
        },
        "keywords": {
            "en": "infectious pancreatic necrosis, finfish, freshwater, saltwater",
            "fr": "n\u00e9crose pancr\u00e9atique infectieuse, poisson, eau douce, eau sal\u00e9e, maladie"
        },
        "dcterms.subject": {
            "en": "inspection,infectious diseases,fish,animal health",
            "fr": "inspection,maladie infectieuse,poisson,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Aquatic Animal Health Division",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Division de la sant\u00e9 des animaux aquatiques"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-02-24 11:03:35",
            "fr": "2012-02-24 11:03:35"
        },
        "modified": {
            "en": "2020-09-21",
            "fr": "2020-09-21"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Facts about infectious pancreatic necrosis",
        "fr": "Fiche de renseignements sur la n\u00e9crose pancr\u00e9atique infectieuse"
    },
    "body": {
        "en": "        \r\n        \n<div class=\"pull-right mrgn-lft-md col-sm-5\"> \n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">What we know about the disease</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>disease in cultured salmonids mainly occurs in the freshwater environment</li>\n<li>outbreaks are most severe in brook trout and rainbow trout</li>\n<li>disease is seen in younger finfish rather than older finfish, however, older populations remain infected</li>\n<li>mortality is higher when the water temperature is more than 10\u00b0C</li>\n<li>disease has been reported in Atlantic salmon within months after transfer to saltwater and in some species of marine finfish under culture conditions</li>\n<li>the virus is also found in wild salmonids where it is known to occur</li>\n</ul>\n</div>\n</section>\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">What we would like to know about the disease</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>do the IPN virus strains that infect marine species of finfish affect salmonids</li>\n</ul>\n</div>\n</section>\n</div>\n<p>Infectious pancreatic necrosis (IPN) is an infectious disease of some freshwater and marine finfish. It is caused by the infectious pancreatic necrosis virus, which belongs to the family <i lang=\"la\">Birnaviridae</i>.</p>\n<p>IPN does not infect humans and is not a risk to food safety.</p>\n<p>IPN is a reportable disease in Canada. If you own or work with finfish and suspect or detect IPN, you must let the Canadian Food Inspection Agency (CFIA) know. Here's how:</p>\n<ul>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/aquaculturists/eng/1450407254532/1450407255317\">Notification of reportable diseases by aquaculturists</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/veterinarians/eng/1450406407952/1450406408789\">Notification of reportable diseases by veterinarians</a></li>\n<li><a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/researchers/eng/1450405664371/1450405665318\">Notification of reportable and immediately notifiable diseases by researchers</a></li>\n</ul>\n<h2>Infectious pancreatic necrosis in Canada</h2>\n<p>The disease is found in most regions of Canada, including the Atlantic Ocean watershed.</p>\n<p>However, IPN is not established in finfish populations in Prince Edward Island or in the Pacific Ocean watershed of British Columbia.</p>\n<p>For information about where IPN occurs in Canada (infected areas, buffer areas and free areas), see <a href=\"/animal-health/aquatic-animals/diseases/finfish/eng/1450409829304/1450409830112\">declarations by province and marine areas</a>.</p>\n<p>For dates and locations (provinces/territories) of agency confirmed cases of the disease, see <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/federally-reportable-aquatic-animal-diseases/eng/1339174937153/1339175227861\">Federally reportable aquatic animal diseases in Canada</a>.</p>\n<h2>Species of finfish that can be infected</h2>\n<ul>\n<li><i lang=\"la\">Abramis brama</i> (common bream)</li>\n<li><i lang=\"la\">Anguilla anguilla</i> (European eel)</li>\n<li><i lang=\"la\">Anguilla japonica</i> (Japanese eel)</li>\n<li><i lang=\"la\">Carassius auratus</i> (goldfish)</li>\n<li><i lang=\"la\">Catostomus commersonii</i> (white sucker)</li>\n<li><i lang=\"la\">Cyprinus carpio</i> (common carp)</li>\n<li><i lang=\"la\">Gadus morhua</i> (Atlantic cod)</li>\n<li><i lang=\"la\">Hippoglossus hippoglossus</i> (Atlantic halibut)</li>\n<li><i lang=\"la\">Hucho hucho</i> (huchen)</li>\n<li><i lang=\"la\">Lampetra fluviatilis</i> (river lamprey)</li>\n<li><i lang=\"la\">Morone saxatilis</i> (striped bass)</li>\n<li><i lang=\"la\">Oncorhynchus clarkii</i> (cutthroat trout)</li>\n<li><i lang=\"la\">Oncorhynchus keta</i> (chum salmon)</li>\n<li><i lang=\"la\">Oncorhynchus kisutch </i>(coho salmon) </li>\n<li><i lang=\"la\">Oncorhynchus mykiss</i> (rainbow trout)</li>\n<li><i lang=\"la\">Perca fluviatilis</i> (European perch)</li>\n<li><i lang=\"la\">Phoxinus phoxinus</i> (Eurasian minnow)</li>\n<li><i lang=\"la\">Salmo salar</i> (Atlantic salmon)</li>\n<li><i lang=\"la\">Salmo trutta</i> (brown trout)</li>\n<li><i lang=\"la\">Salvelinus alpinus</i> (Arctic char)</li>\n<li><i lang=\"la\">Salvelinus fontinalis</i> (brook trout)</li>\n<li><i lang=\"la\">Salvelinus namaycush</i> (lake trout)</li>\n<li><i lang=\"la\">Thymallus thymallus</i> (grayling)</li>\n</ul>\n<h2>Signs of infectious pancreatic necrosis</h2>\n<p>The disease can cause significant death rates in fry and fingerlings from 1 to 4 months of age. Atlantic salmon smolts transferred to seawater are also at risk of death, with rates increasing in infected animals 7 to 12 weeks after their transfer.</p>\n<p>Affected finfish may exhibit any of the following external signs:</p>\n<ul>\n<li>loss of appetite</li>\n<li>spiral and corkscrew swimming patterns </li>\n<li>lying still on bottom of the holding unit</li>\n<li>white trailing feces </li>\n<li>swollen belly</li>\n<li>dark skin colour</li>\n<li>bulging eyes</li>\n<li>areas of bleeding on the bottom of the belly and fins</li>\n<li>pale gills</li>\n</ul>\n<p>Affected finfish may exhibit any of the following internal signs:</p>\n<ul>\n<li>areas of pinpoint bleeding in the fatty tissues surrounding the organs</li>\n<li>abdomen filled with fluid</li>\n<li>pale spleen, kidney, liver and heart</li>\n<li>empty stomach and intestines, or filled with clear or milky mucus</li>\n</ul>\n<h2>Managing infectious pancreatic necrosis</h2>\n<p>Currently no treatments are available for IPN.</p>\n<p>Finfish may contract the virus through contact with discharged bodily wastes or mucus secretions from infected finfish. It can also spread through water contaminated with the virus.</p>\n<p>People can spread the virus to other finfish by moving infected live or dead finfish, by using contaminated equipment, vehicles and vessels, or by using or moving contaminated water.</p>\n<p>You can prevent the introduction of the IPN virus into your facility and prevent the spread of the virus within your facility by using specific safety practices. You can find out more about what practices you need to consider by visiting <a href=\"/animal-health/aquatic-animals/aquatic-animal-biosecurity/eng/1320594187303/1320594268146\">Aquatic animal biosecurity</a>. Talk to your veterinarian about what practices are most effective for the management of this disease in your finfish.</p>\n<p>There is no vaccine approved in Canada against IPN. Talk to your veterinarian about whether a vaccine approved in another country can be an effective tool to manage this disease.</p>\n<p>In areas where IPN is known to occur, you may also need to consider additional practices that are required by your provincial IPN control program.</p>\n<h2>CFIA's initial response to a notification</h2>\n<p>When you notify a CFIA veterinary inspector of the possible presence of IPN in your finfish, we will launch an investigation, which could include an inspection of your facility.</p>\n<p>We will note all relevant information you can provide, such as species affected, life stage affected, signs of illness in the animals, presence of any other diseases and water temperature.</p>\n<p>At the same time, we will collect samples for testing at one of Canada's three National Aquatic Animal Health Laboratories.</p>\n<p>We may issue movement controls on the animals and equipment, including vehicles and vessels. Movement controls are always issued immediately if the farm is in an area where IPN is not known to occur.</p>\n<p>We evaluate all the collected information and test results to confirm the presence of IPN in your facility.</p>\n<p>The agency also confirms the presence of IPN in wild finfish. The agency receives notifications from provincial and federal staff who have investigated a wild fish death event.</p>\n<h2>CFIA's response once the disease is confirmed</h2>\n<p>Further disease response measures will either eradicate IPN from your facility or geographically contain the disease. Response measures are also determined by whether your facility is located in an area where the disease occurs or is not known to occur. <a href=\"/animal-health/aquatic-animals/diseases/investigation-and-response/eng/1338313604809/1338313669210\">Aquatic animal disease investigation and response</a> provides information about CFIA's response. Although each disease investigation and response situation is different, the steps involved are often similar.</p>\r\n<h2 class=\"font-medium black\">How do I get more information</h2>\n<p>For more information about reportable diseases, visit the <a href=\"/animal-health/aquatic-animals/eng/1299155892122/1320536294234\">Aquatic Animal Health</a> page, contact your <a href=\"/eng/1300462382369/1365216058692\">local <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Animal Health Office</a>,  or your <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> Area office:</p>\n\n<ul><li>Atlantic: 506-777-3939</li>\n<li>Quebec: 514-283-8888</li>\n<li>Ontario: 226-217-8555</li>\n<li>West: 587-230-2200</li></ul>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"pull-right mrgn-lft-md col-sm-5\"> \n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">Ce que nous savons sur la maladie</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>la maladie chez les salmonid\u00e9s d'\u00e9levage se manifeste principalement en milieu d'eau douce</li>\n<li>les \u00e9closions les plus graves sont observ\u00e9es chez l'omble de fontaine et la truite arc-en-ciel</li>\n<li>la maladie est observ\u00e9e chez les jeunes poissons plut\u00f4t que chez les poissons plus \u00e2g\u00e9s, mais les populations plus \u00e2g\u00e9es restent infect\u00e9es</li>\n<li>la mortalit\u00e9 est plus \u00e9lev\u00e9e lorsque la temp\u00e9rature de l'eau est sup\u00e9rieure \u00e0 10 \u00b0C</li>\n<li>la maladie a \u00e9t\u00e9 signal\u00e9e chez le saumon de l'Atlantique quelques mois apr\u00e8s son transfert en eau sal\u00e9e et chez certaines esp\u00e8ces de poissons marins dans des conditions d'\u00e9levage</li>\n<li>le virus est \u00e9galement d\u00e9tect\u00e9 chez les salmonid\u00e9s sauvages o\u00f9 la maladie est habituellement pr\u00e9sente</li>\n</ul>\n</div>\n</section>\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title h5 mrgn-tp-0\">Ce que nous aimerions savoir sur la maladie</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>les souches du virus de la NPI qui infectent les esp\u00e8ces marines de poissons affectent-elles les salmonid\u00e9s?</li>\n</ul>\n</div>\n</section>\n</div>\n<p>La n\u00e9crose pancr\u00e9atique infectieuse (NPI) est une maladie qui touche certains poissons d'eau douce et d'eau sal\u00e9e. Elle est caus\u00e9e par le virus de la n\u00e9crose pancr\u00e9atique infectieuse, qui appartient \u00e0 la famille des <i lang=\"la\">Birnaviridae</i>.</p>\n<p>La NPI n'infecte pas les humains et ne pose aucun risque pour la salubrit\u00e9 des aliments.</p>\n<p>La NPI est une maladie \u00e0 d\u00e9claration obligatoire au Canada. Si vous poss\u00e9dez ou manipulez des poissons dans le cadre de votre travail et que vous soup\u00e7onnez ou d\u00e9tectez la pr\u00e9sence de la NPI, vous devez informer l'Agence canadienne d'inspection des aliments (ACIA). Voici comment proc\u00e9der\u00a0:</p>\n<ul>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/aquaculteurs/fra/1450407254532/1450407255317\">D\u00e9claration obligatoire des maladies d\u00e9clarables d'animaux aquatiques par les aquaculteurs</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/veterinaires/fra/1450406407952/1450406408789\">D\u00e9claration obligatoire des maladies d\u00e9clarables d'animaux aquatiques par les v\u00e9t\u00e9rinaires</a></li>\n<li><a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/chercheurs/fra/1450405664371/1450405665318\">D\u00e9claration obligatoire des maladies d\u00e9clarables ou \u00e0 notification imm\u00e9diate d'animaux aquatiques par les chercheurs</a></li>\n</ul>\n<h2>La n\u00e9crose pancr\u00e9atique infectieuse au Canada </h2>\n<p>La maladie est pr\u00e9sente dans la plupart des r\u00e9gions du Canada, y compris dans le bassin versant de l'oc\u00e9an Atlantique.</p>\n<p>Cependant, la NPI n'est pas \u00e9tablie chez les populations de poissons de l'\u00cele-du-Prince-\u00c9douard ou dans le bassin versant de l'oc\u00e9an Pacifique en Colombie-Britannique.</p>\n<p>Pour savoir o\u00f9 la maladie est pr\u00e9sente au Canada (zones infect\u00e9es, zones tampons et zones exemptes), veuillez consulter les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/poissons/fra/1450409829304/1450409830112\">d\u00e9clarations par province et zones marines</a>.</p>\n<p>Pour conna\u00eetre les dates et les lieux (provinces et territoires) des cas de maladie confirm\u00e9s par l'Agence, veuillez consulter les <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/maladies-des-animaux-aquatiques-a-declaration-obli/fra/1339174937153/1339175227861\">maladies des animaux aquatiques \u00e0 d\u00e9claration obligatoire au Canada</a>.</p>\n<h2>Esp\u00e8ces de poissons qui peuvent \u00eatre infect\u00e9es</h2>\n<ul>\n<li><i lang=\"la\">Abramis brama</i> (br\u00e8me commune)</li>\n<li><i lang=\"la\">Anguilla anguilla</i> (anguille d'Europe)</li>\n<li><i lang=\"la\">Anguilla japonica</i> (anguille du Japon)</li>\n<li><i lang=\"la\">Carassius auratus</i> (carassin dor\u00e9)</li>\n<li><i lang=\"la\">Catostomus commersonii</i> (meunier noir)</li>\n<li><i lang=\"la\">Cyprinus carpio</i> (carpe commune)</li>\n<li><i lang=\"la\">Gadus morhua</i> (morue franche)</li>\n<li><i lang=\"la\">Hippoglossus hippoglossus</i> (fl\u00e9tan de l'Atlantique)</li>\n<li><i lang=\"la\">Hucho hucho</i> (huchon du Danube)</li>\n<li><i lang=\"la\">Lampetra fluviatilis</i> (lamproie fluviatale)</li>\n<li><i lang=\"la\">Morone saxatilis</i> (bar d'Am\u00e9rique)</li>\n<li><i lang=\"la\">Oncorhynchus clarkii</i> (truite fard\u00e9e)</li>\n<li><i lang=\"la\">Oncorhynchus keta</i> (saumon k\u00e9ta)</li>\n<li><i lang=\"la\">Oncorhynchus kisutch</i> (saumon coho)</li>\n<li><i lang=\"la\">Oncorhynchus mykiss</i> (truite arc-en-ciel)</li>\n<li><i lang=\"la\">Perca fluviatilis</i> (perche europ\u00e9enne)</li>\n<li><i lang=\"la\">Phoxinus phoxinus</i> (vairon)</li>\n<li><i lang=\"la\">Salmo salar</i> (saumon de l'Atlantique)</li>\n<li><i lang=\"la\">Salmo trutta</i> (truite brune)</li>\n<li><i lang=\"la\">Salvelinus alpinus</i> (omble chevalier)</li>\n<li><i lang=\"la\">Salvelinus fontinalis</i> (omble de fontaine)</li>\n<li><i lang=\"la\">Salvelinus namaycush</i> (touladi)</li>\n<li><i lang=\"la\">Thymallus thymallus</i> (ombre commun)</li>\n</ul>\n<h2>Signes de la n\u00e9crose pancr\u00e9atique infectieuse</h2>\n<p>La maladie peut entra\u00eener des taux de mortalit\u00e9 importants chez les sujets \u00e2g\u00e9s de 1 \u00e0 4 mois (alevins et juv\u00e9niles). Les saumoneaux de l'Atlantique transf\u00e9r\u00e9s dans l'eau sal\u00e9e sont \u00e9galement \u00e0 risque de mourir, les taux augmentant chez les animaux infect\u00e9s de 7 \u00e0 12 semaines apr\u00e8s leur transfert.</p>\n<p>Les poissons atteints peuvent pr\u00e9senter l'un des signes ext\u00e9rieurs suivants\u00a0:</p>\n<ul>\n<li>perte d'app\u00e9tit</li>\n<li>nage en spirales ou en tire-bouchon</li>\n<li>immobilit\u00e9 au fond du bassin</li>\n<li>tra\u00een\u00e9es blanches d'excr\u00e9ments</li>\n<li>abdomen distendu</li>\n<li>coloration fonc\u00e9e de la peau</li>\n<li>yeux exorbit\u00e9s</li>\n<li>zones d'h\u00e9morragie sur la partie inf\u00e9rieure de l'abdomen et des nageoires</li>\n<li>p\u00e2leur des branchies</li>\n</ul>\n<p>Les poissons atteints peuvent pr\u00e9senter l'un des signes internes suivants\u00a0:</p>\n<ul>\n<li>p\u00e9t\u00e9chies dans les tissus adipeux autour des organes</li>\n<li>abdomen rempli de liquide</li>\n<li>p\u00e2leur de la rate, des reins, du foie et du c\u0153ur</li>\n<li>estomac et intestins vides ou remplis de mucus incolore ou laiteux</li>\n</ul>\n<h2>Gestion de la n\u00e9crose pancr\u00e9atique infectieuse</h2>\n<p>\u00c0 l'heure actuelle, il n'existe pas de traitement pour la NPI.</p>\n<p>Les poissons peuvent contracter le virus \u00e0 la suite d'un contact avec des excr\u00e9ments ou des s\u00e9cr\u00e9tions de mucus provenant de poissons infect\u00e9s. Il peut \u00e9galement se propager par l'eau contamin\u00e9e par le virus.</p>\n<p>Les \u00eatres humains peuvent \u00e9galement transmettre le virus \u00e0 d'autres poissons en d\u00e9pla\u00e7ant des poissons infect\u00e9s, qu'ils soient vivants ou morts, en utilisant de l'\u00e9quipement, des v\u00e9hicules et des navires contamin\u00e9s, ou en utilisant ou d\u00e9pla\u00e7ant de l'eau contamin\u00e9e.</p>\n<p>Vous pouvez pr\u00e9venir l'introduction du virus de la NPI dans votre exploitation et emp\u00eacher la propagation du virus au sein de votre exploitation en adoptant certaines pratiques en mati\u00e8re de s\u00e9curit\u00e9. Pour en savoir plus sur les pratiques que vous devez prendre en consid\u00e9ration, veuillez consulter la page Web intitul\u00e9e <a href=\"/sante-des-animaux/animaux-aquatiques/biosecurite-relative-aux-animaux-aquatiques/fra/1320594187303/1320594268146\">Bios\u00e9curit\u00e9 relative aux animaux aquatiques</a>. Demandez \u00e0 votre v\u00e9t\u00e9rinaire quelles sont les pratiques les plus efficaces pour g\u00e9rer cette maladie chez vos poissons.</p>\n<p>Il n'y a aucun vaccin approuv\u00e9 au Canada contre la NPI. Demandez \u00e0 votre v\u00e9t\u00e9rinaire si un vaccin approuv\u00e9 dans un autre pays peut \u00eatre un outil efficace pour g\u00e9rer cette maladie.</p>\n<p>Dans les zones o\u00f9 la NPI est pr\u00e9sente, vous devrez peut-\u00eatre \u00e9galement suivre les pratiques suppl\u00e9mentaires exig\u00e9es par le programme de lutte contre la NPI de votre province.</p>\n<h2>Intervention initiale de l'ACIA suite \u00e0 une notification</h2>\n<p>Lorsque vous informez un v\u00e9t\u00e9rinaire-inspecteur de l'ACIA de la pr\u00e9sence possible de la NPI chez vos poissons, nous lan\u00e7ons une enqu\u00eate, qui peut inclure une inspection de l'\u00e9tablissement.</p>\n<p>Nous prendrons note de tous les renseignements pertinents que vous pourrez nous fournir, tels que les esp\u00e8ces touch\u00e9es, l'\u00e9tape du cycle de vie touch\u00e9e, les signes de maladie chez les animaux, les ant\u00e9c\u00e9dents de vaccination, la pr\u00e9sence de toute autre maladie et la temp\u00e9rature de l'eau.</p>\n<p>Nous pr\u00e9l\u00e8verons \u00e9galement des \u00e9chantillons pour les \u00e9preuves de laboratoire dans l'un des trois Laboratoires nationaux pour la sant\u00e9 des animaux aquatiques du Canada.</p>\n<p>Il se peut que nous imposions des mesures de contr\u00f4le des d\u00e9placements des animaux et de l'\u00e9quipement, y compris les v\u00e9hicules et les navires. Des mesures de contr\u00f4le des d\u00e9placements sont toujours imm\u00e9diatement mises en place lorsque l'exploitation se situe dans une zone o\u00f9 la NPI n'est r\u00e9put\u00e9e \u00eatre pas pr\u00e9sente.</p>\n<p>Nous \u00e9valuons l'ensemble des renseignements recueillis et des r\u00e9sultats des \u00e9preuves pour confirmer la pr\u00e9sence de la NPI \u00e0 l'exploitation.</p>\n<p>L'Agence confirme \u00e9galement la pr\u00e9sence de la NPI chez les poissons sauvages. L'Agence re\u00e7oit des notifications du personnel provincial ou f\u00e9d\u00e9rale qui a enqu\u00eat\u00e9 sur un cas de d\u00e9c\u00e8s de poissons sauvages.</p>\n<h2>Intervention de l'ACIA une fois que la pr\u00e9sence de la maladie est confirm\u00e9e</h2>\n<p>D'autres mesures d'intervention pour lutter contre la maladie permettront soit d'\u00e9radiquer la NPI de de l'\u00e9tablissement, soit de limiter la maladie sur le plan g\u00e9ographique. Les mesures d'intervention sont \u00e9galement d\u00e9termin\u00e9es en fonction du fait que votre installation est situ\u00e9e dans une zone o\u00f9 la maladie est pr\u00e9sente ou n'est pas r\u00e9put\u00e9e \u00eatre pr\u00e9sente. La page Web <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/enquete-et-intervention/fra/1338313604809/1338313669210\">Enqu\u00eate et intervention concernant les maladies d'animaux aquatiques</a> fournit des renseignements sur l'intervention de l'ACIA. Bien que chaque situation soit diff\u00e9rente, les \u00e9tapes de la d\u00e9marche \u00e0 suivre lors d'une enqu\u00eate et d'une intervention en cas de maladie sont souvent similaires.</p>\r\n<h2 class=\"font-medium black\">Comment puis-je obtenir davantage de renseignements</h2>\n<p>Pour de plus amples renseignements sur les maladies d\u00e9clarables, consultez la page <a href=\"/sante-des-animaux/animaux-aquatiques/fra/1299155892122/1320536294234\">Sant\u00e9 des animaux aquatiques</a>,  communiquez avec votre <a href=\"/fra/1300462382369/1365216058692\">bureau local de sant\u00e9 des animaux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> ou  le bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de votre centre op\u00e9rationnel\u00a0: </p>\n<ul><li>Atlantique : 506-777-3939</li>\n<li>Qu\u00e9bec : 514-283-8888</li>\n<li>Ontario : 226-217-8555</li>\n<li>Ouest : 587-230-2200</li></ul>\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}