{
    "dcr_id": "1304399296214",
    "title": {
        "en": "Japan - Export requirements for fish and seafood",
        "fr": "Japon -  Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "html_modified": "2024-03-12 3:55:15 PM",
    "modified": "2023-09-20",
    "issued": "2011-05-03 01:08:18",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/fispoi_export_japan_1304399296214_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/fispoi_export_japan_1304399296214_fra"
    },
    "ia_id": "1304441483630",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Fish and seafood",
        "fr": "Poissons et fruits de mer"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Japan - Export requirements for fish and seafood",
        "fr": "Japon -  Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "label": {
        "en": "Japan - Export requirements for fish and seafood",
        "fr": "Japon -  Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "templatetype": "content page 1 column",
    "node_id": "1304441483630",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/japan-fish-and-seafood/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/japon-le-poisson-et-les-produits-de-la-mer/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Japan - Export requirements for fish and seafood",
            "fr": "Japon -  Exigences d'exportation pour le poisson et les produits de la mer"
        },
        "description": {
            "en": "Countries to which exports are currently made - Japan",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Japon"
        },
        "keywords": {
            "en": "fish, seafood, safety, QMP, HACCP, crustaceans, exports, certification, Japan",
            "fr": "poisson, produits de la mer et de la production, poisson, produits de la mer, PGQ, HACCP, crustac\u00e9, exportation, certification, Japon"
        },
        "dcterms.subject": {
            "en": "crustaceans,exports,fish,fisheries products,food inspection,inspection,seafood",
            "fr": "crustac\u00e9,exportation,poisson,produit de la peche,inspection des aliments,inspection,fruits de mer"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-03 01:08:18",
            "fr": "2011-05-03 01:08:18"
        },
        "modified": {
            "en": "2023-09-20",
            "fr": "2023-09-20"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Japan - Export requirements for fish and seafood",
        "fr": "Japon - Exigences d'exportation pour le poisson et les produits de la mer"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Eligible/Ineligible product</h2>\n<h3>Eligible</h3>\n<ul>\n<li>All fish and seafood products</li>\n</ul>\n\n<h2>Pre-export approvals by competent authority of importing country</h2>\n<h3>Establishments</h3>\n<ol>\n<li class=\"mrgn-bttm-md\">For <strong>live lobster</strong> and <strong>lobster products containing tomalley:</strong>\n<p class=\"mrgn-tp-md\">Exporter must appear on the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/japan-fish-and-seafood/live-lobster-and-lobster-tomalley-products/eng/1304466360823/1304466626301\">List of Exporters of Live Lobster and Lobster Tomalley Products to Japan</a>.</p>\n</li>\n<li class=\"mrgn-bttm-md\">For <strong>live and raw oysters:</strong>\n<p class=\"mrgn-tp-md\">The product must originate from an establishment and harvest area/lease published on the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/japan-fish-and-seafood/raw-oyster-products/eng/1304457030793/1304457190421\">List of Canadian Certified Shellfish Dealers and Leases Approved to Export their Products to Japan</a>.</p></li>\n<li class=\"mrgn-bttm-md\">For all fish and seafood products exported to Japan that are further used in products destined to the European Union (EU):\n<p class=\"mrgn-tp-md\">Establishments must appear on the appropriate establishment list administered by Directorate-General for Health and Food Safety (DG-SANTE). See <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/european-union-fish-and-seafood/eng/1304221213916/1304221299574\">European Union</a> for more information.</p>\n<p class=\"mrgn-tp-md\">Canadian exporters are encouraged to communicate with the Japanese importer to confirm the accurate intended use of the fish and seafood products.</p></li>\n\n</ol>\n\n<h2>Product specifications</h2>\n<h3>Maximum levels for chemical contaminants</h3>\n<ul>\n<li>Total mercury\u00a0\u2013 0.4\u00a0ppm (does not apply to tuna or sturgeon, fish oils, gelatin or other fish by-products)</li>\n<li>Methyl mercury\u00a0\u2013 0.3\u00a0ppm (does not apply to tuna or sturgeon, fish oils, gelatin or other fish by-products)</li>\n<li>PCB\u00a0\u2013 0.5\u00a0ppm (does not apply to fish oils, gelatin or other fish by-products)</li>\n</ul>\n<h2>Production controls and inspection requirements</h2>\n<ul>\n<li>Live lobster and lobster products containing tomalley must be prepared using the reference standard for controls to export lobsters to countries with requirements for levels of Paralytic Shellfish Poison (PSP) toxin in the hepatopancreas as per the <a href=\"/exporting-food-plants-or-animals/food-exports/requirements/japan-fish-and-seafood/tomalley-guidance-document/eng/1304444268047/1304475517406\">Guidance Document\u00a0\u2013 Export of Live Lobsters and Lobster Products Containing Tomalley</a>.</li>\n</ul>\n<h2>Documentation requirements</h2>\n<h3>Certificate</h3>\n<ol>\n<li class=\"mrgn-bttm-md\">For\u00a0<strong>live lobster and lobster products containing tomalley</strong>:\n<p class=\"mrgn-tp-md\">Certificate of Origin and Hygiene (CFIA/ACIA\u00a05003)</p>\n<ul>\n<li>Before requesting a certificate, the exporter must provide the\u00a0CFIA\u00a0with a description of their controls that follow the\u00a0<a href=\"/exporting-food-plants-or-animals/food-exports/requirements/japan-fish-and-seafood/tomalley-guidance-document/eng/1304444268047/1304475517406\">Guidance Document\u00a0\u2013 Export of Live Lobsters and Lobster Products Containing Tomalley</a>.</li>\n<li>The exporter must include the results of tests performed on the lobsters being shipped to Japan with each certificate request to show that the levels of Paralytic Shellfish Poison (PSP) toxin in the hepatopancreas met Japan's standard of\u00a080\u00a0\u00b5g/100\u00a0g.</li>\n<li>The Certificate of Origin and Hygiene (CFIA/ACIA\u00a05003) should include the following statement:\n<ul>\n<li>\"The consignor listed below is controlling their shipments of lobsters in accordance with the Guidance Document\u00a0\u2013 Export of Live Lobsters and Lobster Products Containing Tomalley\"</li>\n</ul>\n</li></ul>\n<p><strong>Note:</strong> The exporter's physical address must appear on both the Certificate of Origin and Hygiene and the List of Exporters of Live Lobster and Lobster Tomalley Products to Japan.</p>\n<p><strong>Note:</strong> The above statement must be in both English and French for shipments destined to Japan.</p>\n    \n</li>\n<li class=\"mrgn-bttm-md\">For <strong>live and raw oysters</strong>:\n<p class=\"mrgn-tp-md\">Certificate of Origin and Hygiene (CFIA/ACIA\u00a05003)</p>\n<ul>\n<li>The following statements/information must be included on the certificate:\n<ul>\n<li>\"The product meets the requirements of the <i>Safe Food for Canadians Act</i>, and the <i>Safe Food for Canadians Regulations</i>, as amended from time to time and the Canadian Shellfish Sanitation Program. The product is fit for human consumption. The laws and regulations of Canada have been deemed to be equivalent to the <i>Food Sanitation Act</i> and relevant regulations of Japan.\"</li>\n<li>\"The product originates from oyster harvesting areas where the total coliform geometric mean MPN of the water samples does not exceed 70 per 100\u00a0ml and the product sample does not exceed a MPN of 230 per 100\u00a0grams as determined by regular coliform testing.\"</li>\n<li>\"The Paralytic Shellfish Poison (PSP) content is less than <span class=\"nowrap\">80 \u00b5g/100 g</span>\".</li>\n<li>Geographic and Harvest Area</li>\n<li>Harvest Date (DD/MM/YYYY)</li>\n<li>Growing Area/Lease No.</li>\n<li>Processing facility (name, address, registration number)</li>\n<li>Quantity and weight (under Lot Size field)</li>\n</ul>\n</li>\n<li>For information on the procedure to verify PSP content, please contact your\u00a0<a href=\"/about-the-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA Office</a>.</li></ul>\n<p><strong>Note</strong>: The exporter's physical address must appear on both the Certificate of Origin and Hygiene and the\u00a0<a href=\"/exporting-food-plants-or-animals/food-exports/requirements/japan-fish-and-seafood/raw-oyster-products/eng/1304457030793/1304457190421\">List of Canadian Certified Shellfish Dealers and Leases Approved to Export their Products to Japan</a></p>\n<p><strong>Note</strong>: The above statements must be in both English and French</p>\n    \n</li>\n<li>For <strong>all other products</strong>:\n<p class=\"mrgn-tp-md\">No certificate required but product must still comply with Japanese Food Safety standards</p>\n</li>\n</ol>\n<h2>Other information</h2>\n<p>Further information on import requirements may be obtained from Japan's <a href=\"https://www.mhlw.go.jp/english/\">Ministry of Health, Labour and Welfare</a>.</p>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Produits admissibles/non admissibles</h2>\n<h3>Admissibles</h3>\n<ul>\n<li>Tous les poissons et fruits de mer</li>\n</ul>\n\n<h2>Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n<h3>\u00c9tablissements</h3>\n<ol>\n<li class=\"mrgn-bttm-md\">Pour <strong>les homards vivants et produits du homard contenant du tomalli</strong>\u00a0:\n<p class=\"mrgn-tp-md\">L'exportateur doit figurer sur la <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/japon-le-poisson-et-les-produits-de-la-mer/homard-vivant-et-des-produits-de-homard-contenant-/fra/1304466360823/1304466626301\">Liste des exportateurs qui exp\u00e9dient du homard vivant et des produits du homard contenant du tomalli vers le Japon</a>.</p></li>\n<li class=\"mrgn-bttm-md\">Pour <strong>les hu\u00eetres vivantes et crues</strong>\u00a0:\n<p class=\"mrgn-tp-md\">Le produit doit provenir d'un \u00e9tablissement et d'une zone de r\u00e9colte/concession figurant sur la <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/japon-le-poisson-et-les-produits-de-la-mer/produits-d-huitres-crues/fra/1304457030793/1304457190421\">Liste des N\u00e9gociants canadiens certifi\u00e9s dans le secteur des coquillages et des exploitations ostr\u00e9icoles</a>.</p></li>\n<li class=\"mrgn-bttm-md\">Pour tous les produits de poisson et de fruits de mer export\u00e9s vers le Japon pour \u00eatre ensuite utilis\u00e9s dans des produits destin\u00e9s \u00e0 l'Union europ\u00e9enne (UE)\u00a0:\n<p class=\"mrgn-tp-md\">Les \u00e9tablissements doivent figurer sur la liste des \u00e9tablissements appropri\u00e9e g\u00e9r\u00e9e par la Direction g\u00e9n\u00e9rale de la sant\u00e9 et de la s\u00e9curit\u00e9 alimentaire (DG-SANTE). Voir <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/union-europeenne-le-poisson-et-les-produits-de-la-/fra/1304221213916/1304221299574\">Union europ\u00e9enne</a> pour plus d'informations.</p>\n<p class=\"mrgn-tp-md\">Les exportateurs canadiens sont encourag\u00e9s \u00e0 communiquer avec l'importateur japonais pour confirmer l'utilisation exacte pr\u00e9vue des produits de poisson et de fruits de mer.</p></li>\n</ol>\n\n<h2>Sp\u00e9cifications du produit</h2>\n<h3>Concentrations maximales des contaminants chimiques</h3>\n<ul>\n<li>Mercure total\u00a0\u2013 0,4\u00a0ppm (ne s'applique pas au thon et \u00e0 l'esturgeon, aux huiles de poisson, aux g\u00e9latines ou \u00e0 d'autres sous-produits du poisson)</li>\n<li>M\u00e9thylmercure\u00a0\u2013 0,3\u00a0ppm (ne s'applique pas au thon et \u00e0 l'esturgeon, aux huiles de poisson, aux g\u00e9latines ou \u00e0 d'autres sous-produits du poisson)</li>\n<li>BPC\u00a0\u2013 0,5\u00a0ppm (ne s'applique pas aux huiles de poisson, aux g\u00e9latines ou \u00e0 d'autres sous-produits du poisson)</li>\n</ul>\n\n<h2>Contr\u00f4les de production et exigences d'inspection</h2>\n<ul>\n<li>Les homards vivants et les produits du homard contenant du tomalli doivent \u00eatre pr\u00e9par\u00e9s selon la norme de r\u00e9f\u00e9rence concernant le contr\u00f4le des exportations de homards vers les pays ayant des exigences relatives \u00e0 la concentration de la toxine paralysante (PSP) dans l'h\u00e9patopancr\u00e9as des crustac\u00e9s selon le <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/japon-le-poisson-et-les-produits-de-la-mer/tomalli-document-d-orientation/fra/1304444268047/1304475517406\">Document d'orientation\u00a0\u2013 Exportation de homard vivants et de produits du homard contenant du tomalli</a>.</li>\n</ul>\n<h2>Documents requis</h2>\n<h3>Certificat</h3>\n<ol>\n<li class=\"mrgn-bttm-md\">Pour <strong>les homards vivants</strong> et <strong>les produits du homard contenant du tomalli</strong>\u00a0:\n<p class=\"mrgn-tp-md\">Certificat \u00ab\u00a0Origine et hygi\u00e8ne\u00a0\u00bb (CFIA/ACIA\u00a05003)</p>\n<ul>\n<li>Avant de demander un certificat, l'exportateur doit fournir \u00e0 l'ACIA\u00a0la description de ses contr\u00f4les qui permettent de respecter les exigences \u00e9nonc\u00e9es dans le <a href=\"/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/japon-le-poisson-et-les-produits-de-la-mer/tomalli-document-d-orientation/fra/1304444268047/1304475517406\">Document d'orientation\u00a0\u2013 Exportation de homard vivants et de produits du homard contenant du tomalli</a>.</li>\n<li>L'exportateur doit joindre, avec chaque demande de certificat, les r\u00e9sultats d'analyses effectu\u00e9s sur les homards destin\u00e9s \u00e0 \u00eatre export\u00e9s au Japon afin de montrer que les concentrations de toxine paralysante (PSP) dans l'h\u00e9patopancr\u00e9as des crustac\u00e9s respectent la norme japonaise de\u00a080\u00a0\u00b5g/100\u00a0g.</li>\n<li>Le certificat \u00ab\u00a0Origine et hygi\u00e8ne\u00a0\u00bb (CFIA/ACIA\u00a05003) doit comporter les d\u00e9clarations suivantes\u00a0:\n<ul>\n<li>\u00ab\u00a0L'exp\u00e9diteur, dont le nom figure ci-dessous, contr\u00f4le ses envois de homards conform\u00e9ment au Document d'orientation\u00a0\u2013 Exportation de homards vivants et de produits du homard contenant du tomalli\u00a0\u00bb</li>\n</ul>\n</li>    </ul>\n<p><strong>Remarque\u00a0:</strong>\u00a0L'adresse civique de l'exportateur doit figurer sur le \u00ab\u00a0certificat d'origine et d'hygi\u00e8ne\u00a0\u00bb et dans la Liste des exportateurs qui exp\u00e9dient du homard vivant et des produits de homard contenant du tomalli vers le Japon.</p>\n<p><strong>Remarque\u00a0:</strong> La d\u00e9claration ci-dessus doit \u00eatre en anglais et fran\u00e7ais.</p>\n\n</li>\n<li class=\"mrgn-bttm-md\">Pour <strong>les hu\u00eetres vivantes</strong> et <strong>crues</strong>\u00a0:\n<p class=\"mrgn-tp-md\">Certificat \u00ab\u00a0Origine et hygi\u00e8ne\u00a0\u00bb (CFIA/ACIA\u00a05003)</p>\n<ul>\n<li>Les d\u00e9clarations\u00a0/ informations suivantes doivent figurer sur le certificat\u00a0:\n<ul>\n<li>Le produit r\u00e9pond aux exigences de la <i>Loi sur la salubrit\u00e9 des aliments au Canada</i> et le <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i>, tel qu'il est modifi\u00e9 de temps \u00e0 autre, et du Programme canadien de contr\u00f4le de la salubrit\u00e9 des mollusques. Le produit est propre \u00e0 la consommation humaine. Les lois et r\u00e8glements du Canada ont \u00e9t\u00e9 jug\u00e9s \u00e9quivalents \u00e0 la <i lang=\"en\">Food Sanitation Act</i> et aux r\u00e8glements pertinents du Japon.</li>\n<li>Le produit provient d'un site ostr\u00e9icole o\u00f9 le\u00a0NPP\u00a0de la moyenne g\u00e9om\u00e9trique des coliformes totaux dans les \u00e9chantillons d'eau ne d\u00e9passe pas\u00a070 par 100\u00a0ml\u00a0et les \u00e9chantillons du produit ne d\u00e9passent pas un\u00a0NPP\u00a0de\u00a0230 par 100\u00a0g, d'apr\u00e8s les r\u00e9sultats des essais habituels sur les coliformes.</li>\n<li>La teneur en toxine paralysante (PSP) est inf\u00e9rieure \u00e0\u00a080\u00a0\u00b5g/100\u00a0g.</li>\n<li>Le lieu g\u00e9ographique et la zone de r\u00e9colte</li>\n<li>La date de la r\u00e9colte (JJ/MM/AAAA)</li>\n<li>Le secteur coquillier/le num\u00e9ro de concession ostr\u00e9icole</li>\n<li>Installation de traitement\u00a0: (nom, adresse, num\u00e9ro d'agr\u00e9ment)</li>\n<li>Quantit\u00e9 et Poids (sous la bo\u00eete de Taille du lot)</li>\n</ul>\n</li>\n<li>Pour de l'information sur la proc\u00e9dure \u00e0 suivre en vue de v\u00e9rifier contenu PSP, veuillez communiquer avec votre bureau local de l'ACIA.</li></ul>\n<p><strong>Remarque</strong>\u00a0: L'adresse civique de l'exportateur doit figurer sur le certificat \u00ab\u00a0Origine et hygi\u00e8ne\u00a0\u00bb et sur La liste des n\u00e9gociants canadiens certifi\u00e9s dans le secteur des coquillages et des exploitations ostr\u00e9icoles autoris\u00e9s \u00e0 exporter des produits d'hu\u00eetres crues au Japon.</p>\n<p><strong>Remarque\u00a0:</strong> Les d\u00e9clarations ci-dessus doivent \u00eatre en anglais et fran\u00e7ais.</p>\n    \n</li>\n<li>Pour tous <strong>les autres produits</strong>\u00a0:\n<p class=\"mrgn-tp-md\">Aucun certificat requis, mais les autres produits doivent \u00eatre conformes aux normes japonaises en mati\u00e8re de salubrit\u00e9 des aliments.</p>\n</li>\n</ol>\n<h2>Autre renseignements</h2>\n<p>D'autres renseignements sur les exigences en mati\u00e8re d'importation sont disponibles aupr\u00e8s du <a href=\"https://www.mhlw.go.jp/english/\"><span lang=\"en\">Ministry of Health, Labour and Welfare</span> (en anglais seulement)</a> japonaises.</p>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}