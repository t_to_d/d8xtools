{
    "dcr_id": "1458835515028",
    "title": {
        "en": "Questions and answers: Innate\u2122 potato",
        "fr": "Questions et r\u00e9ponses : La pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>"
    },
    "html_modified": "24-3-2016",
    "modified": "8-9-2017",
    "issued": "24-3-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pnts_public_innate_potato_faq_1458835515028_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/pnts_public_innate_potato_faq_1458835515028_fra"
    },
    "parent_ia_id": "1337384231869",
    "ia_id": "1458835687626",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and answers: Innate\u2122 potato",
        "fr": "Questions et r\u00e9ponses : La pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>"
    },
    "label": {
        "en": "Questions and answers: Innate\u2122 potato",
        "fr": "Questions et r\u00e9ponses : La pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>"
    },
    "templatetype": "content page 1 column",
    "node_id": "1458835687626",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1337380923340",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plants-with-novel-traits/general-public/questions-and-answers-innate-potato/",
        "fr": "/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/questions-et-reponses-la-pomme-de-terre-innatemc/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and answers: Innate\u2122 potato",
            "fr": "Questions et r\u00e9ponses : La pomme de terre InnateMC"
        },
        "keywords": {
            "en": "plants, plant health, Innate potato, potato, black spot bruising, acrylamide, environmental assessment, feed assessment",
            "fr": "v\u00e9g\u00e9taux, la sant\u00e9 des v\u00e9g\u00e9taux, pomme de terre Innate, pomme de terre, taches noires, acrylamide, \u00e9valuation environnementale, \u00e9valuation de l'innocuit\u00e9 \u00e0 titre d'aliment du b\u00e9tail"
        },
        "dcterms.subject": {
            "en": "labelling,food labeling,potatoes,plants,safety",
            "fr": "\u00e9tiquetage,\u00e9tiquetage des aliments,patates,plante,s\u00e9curit\u00e9"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Government of Canada,Canadian Food Inspection Agency"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-03-24",
            "fr": "2016-03-24"
        },
        "modified": {
            "en": "2017-09-08",
            "fr": "2017-09-08"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,media",
            "fr": "entreprises,gouvernement,grand public,m\u00e9dia"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and answers: Innate\u2122 potato",
        "fr": "Questions et r\u00e9ponses : La pomme de terre InnateMC"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2 class=\"h5\">How was the Innate\u2122 potato assessed?</h2>\n\n<p>Innate\u2122 potato required three separate assessments. Health Canada has assessed the safety and nutrition of Innate potato for use as food, while the Canadian Food Inspection Agency (CFIA) assessed its safety and nutrition for use as a livestock feed, as well as whether it is suitable to be released into the environment.</p>\n\n<h2 class=\"h5\">How does the Innate\u2122 potato have reduced black spot bruising and acrylamide?</h2>\n<p>Innate\u2122 Potato is genetically modified to be less susceptible to black spot from bruising and have lower levels of asparagine, which results in formation of less acrylamide when the potatoes are baked or fried. Acrylamide is a by-product of the cooking process that can be toxic at high levels.</p>\n<h2 class=\"h5\">What was considered in the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> livestock feed assessment of the Innate\u2122 potato?</h2>\n<p>The livestock feed safety assessment considered the potential impact of Innate\u2122 potato on:</p>\n<ul>\n<li>animal health and human safety as it relates to the potential transfer of residues into foods of animal origin, and worker/bystander exposure to the feed; and</li>\n<li>livestock nutrition.</li>\n</ul>\n<h2 class=\"h5\">What was considered in the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> environmental assessment of the Innate\u2122 potato?</h2>\n<p>The environmental safety assessment examined five broad categories of possible impacts:</p>\n<ul>\n<li>the potential for the plant to become a weed or invade natural habitats;</li>\n<li>the potential, and consequences of, gene flow to related plants;</li>\n<li>the potential for the plant to become a plant pest;</li>\n<li>the potential impact of the plant or its gene products on non-target species; and</li>\n<li>the potential impact on biodiversity.</li>\n</ul>\n<h2 class=\"h5\">Were effects on other potato growers considered?</h2>\n<p>The Government of Canada will only authorize a product for use as food, for use as livestock feed, and for release into the environment, if, after a thorough scientific assessment, it is determined to be as safe as its conventional counterparts. In order to protect the scientific integrity of the assessment process, socio-economic factors, such as potential market reaction, are not considered in the decision-making process with respect to novel products.</p>\n<p>In Canada, once a novel product, such as Innate\u2122 potato, has been approved for environmental release and for use as food and livestock feed, it is considered to be equivalent to its conventional counterparts. Producers are free to choose amongst products deemed to be safe and to implement the production methods and marketing strategies of their choice.</p>\n<h2 class=\"h5\">Does the Government of Canada endorse the Innate\u2122 potato?</h2>\n<p>The Government of Canada neither advocates for, nor opposes, specific products. Regulatory decisions are evidence-based and impartial.</p>\n<h2 class=\"h5\">Why is this product needed when there are other ways to stop a potato from browning?</h2>\n<p>The potential market demand for any new product is a matter of business judgement. In this case, it is up to J.R. Simplot Company to determine whether there is sufficient customer demand to merit commercializing Innate\u2122 potato.</p>\n<h2 class=\"h5\">Will foods containing Innate\u2122 potatoes be labelled?</h2>\n<p>Since Innate\u2122 potatoes have been determined to be as safe as conventional potatoes, Health Canada does not require special labelling. In Canada, voluntary labelling is permitted to provide consumers with information that is not related to the safety of the product. The national standard, \"<a href=\"http://www.tpsgc-pwgsc.gc.ca/ongc-cgsb/programme-program/normes-standards/internet/032-0315/index-eng.html\"><i>Voluntary Labelling and Advertising of Foods that Are and Are Not Products of Genetic Engineering</i></a>\", states that products can be voluntarily labelled as <abbr title=\"genetically-engineered\">GE</abbr> or non-<abbr title=\"genetically-engineered\">GE</abbr>, provided conditions are met and the claim is understandable, informative, accurate, and not misleading.</p>\n<p>The decision of whether or not to proceed with voluntary labelling is solely that of J.R. Simplot Company.</p>\n<h2 class=\"h5\">What data were used for the assessments?</h2>\n<p>As part of the assessment process, J.R. Simplot Company provided extensive data for review by regulators at the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> and Health Canada. The submitted data included a detailed description of how Innate\u2122 potato was developed and how the reduced bruising and acrylamide traits function. Regulators also accessed relevant peer-reviewed studies from the published literature.</p>\n<p>Canada's regulatory framework requires the product developer to generate the data necessary to fulfill the requirements of the safety and nutrition assessments. This approach to data generation is consistent with that of regulatory bodies around the world, and is the standard practice to ensure that the safety and nutrition of the product can be evaluated.</p>\n<p>A decision document that outlines the safety assessment and the regulatory decision will be posted on the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> website.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2 class=\"h5\">Comment la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> a-t-elle \u00e9t\u00e9 \u00e9valu\u00e9e?</h2>\n<p>La pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> a d\u00fb \u00eatre soumise \u00e0 trois \u00e9valuations distinctes. Sant\u00e9 Canada a \u00e9valu\u00e9 l'innocuit\u00e9 et la nutrition de la pomme de terre Innate pour la consommation, alors que l'Agence canadienne d'inspection des aliments (ACIA) a \u00e9valu\u00e9 son innocuit\u00e9 et sa nutrition en vue de son utilisation comme aliment du b\u00e9tail et pour v\u00e9rifier si elle peut \u00eatre diss\u00e9min\u00e9e dans l'environnement sans danger.</p>\n<h2 class=\"h5\">Qu'est-ce qui explique la r\u00e9duction des taches noires caus\u00e9es par des meurtrissures et de l'acrylamide dans la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>?</h2>\n<p>La pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> est g\u00e9n\u00e9tiquement modifi\u00e9e pour la rendre moins susceptible de former des taches noires dues aux meurtrissures et pour baisser son taux d'asparagine, ce qui r\u00e9duit la formation d'acrylamide lorsque les pommes de terre sont frites ou cuites au four. L'acrylamide est un sous-produit de la cuisson qui peut \u00eatre toxique \u00e0 des taux \u00e9lev\u00e9s.</p>\n<h2 class=\"h5\">Qu'a-t-on pris en compte lors de l'\u00e9valuation par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> \u00e0 titre d'aliment du b\u00e9tail?</h2>\n<p>L'\u00e9valuation de l'innocuit\u00e9 \u00e0 titre d'aliment du b\u00e9tail vise les r\u00e9percussions possibles de la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> sur\u00a0:</p>\n<ul>\n<li>la sant\u00e9 des animaux et des humains, en cas de transfert de r\u00e9sidus vers les aliments d'origine animale ou d'exposition de travailleurs ou de tiers aux aliments du b\u00e9tail;</li>\n<li>la nutrition du b\u00e9tail.</li>\n</ul>\n<h2 class=\"h5\">Qu'a-t-on pris en compte lors de l'\u00e9valuation environnementale par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> de la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>?</h2>\n<p>L'\u00e9valuation des risques pour l'environnement vise cinq cat\u00e9gories g\u00e9n\u00e9rales de r\u00e9percussions possibles\u00a0:</p>\n<ul>\n<li>la possibilit\u00e9 que le v\u00e9g\u00e9tal devienne une mauvaise herbe ou qu'il envahisse les milieux naturels;</li>\n<li>la possibilit\u00e9 de flux g\u00e9nique vers des v\u00e9g\u00e9taux apparent\u00e9s et les cons\u00e9quences de ce flux;</li>\n<li>la possibilit\u00e9 que le v\u00e9g\u00e9tal devienne nuisible aux autres v\u00e9g\u00e9taux;</li>\n<li>les effets possibles du v\u00e9g\u00e9tal ou de ses produits g\u00e9niques sur les esp\u00e8ces non vis\u00e9es;</li>\n<li>les effets possibles sur la biodiversit\u00e9.</li>\n</ul>\n<h2 class=\"h5\">Les r\u00e9percussions sur les autres producteurs de pommes de terre ont-elles \u00e9t\u00e9 prises en compte?</h2>\n<p>Le gouvernement du Canada autorise l'utilisation d'un produit pour l'alimentation humaine ou animale, et sa diss\u00e9mination dans l'environnement si, apr\u00e8s une \u00e9valuation scientifique rigoureuse, il est d\u00e9termin\u00e9 que ce produit est aussi s\u00fbr que ses \u00e9quivalents traditionnels. Afin de pr\u00e9server l'int\u00e9grit\u00e9 scientifique du processus d'\u00e9valuation, les facteurs socio-\u00e9conomiques, comme la r\u00e9action possible des march\u00e9s, ne sont pas pris en consid\u00e9ration dans le processus d\u00e9cisionnel en ce qui a trait aux produits nouveaux.</p>\n<p>Au Canada, lorsqu'un produit nouveau, comme la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>, est approuv\u00e9 aux fins de l'alimentation humaine et animale et de diss\u00e9mination dans l'environnement, il est consid\u00e9r\u00e9 comme \u00e9quivalant aux produits traditionnels. Les producteurs peuvent choisir parmi des produits jug\u00e9s sans danger et mettre en \u0153uvre la m\u00e9thode de production et les strat\u00e9gies de commercialisation de leur choix. </p>\n<h2 class=\"h5\">Le gouvernement du Canada appuie-t-il la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>?</h2>\n<p>Le gouvernement du Canada ne fait pas la promotion de produits donn\u00e9s, mais ne s'y oppose pas non plus. Les d\u00e9cisions r\u00e9glementaires sont fond\u00e9es sur des donn\u00e9es probantes et sont impartiales.</p>\n<h2 class=\"h5\">Pourquoi ce produit est-il n\u00e9cessaire alors qu'il existe d'autres fa\u00e7ons d'emp\u00eacher le brunissement des pommes de terre?</h2>\n<p>La demande possible sur le march\u00e9 de tout nouveau produit est une question d'appr\u00e9ciation commerciale. Dans ce cas-ci, il revient \u00e0 l'entreprise J.R. Simplot de d\u00e9terminer si la demande des consommateurs est suffisante pour justifier la commercialisation de la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup>.</p>\n<h2 class=\"h5\">Les aliments contenant des pommes de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> porteront-ils une mention sur l'\u00e9tiquette?</h2>\n<p>Puisque la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> a \u00e9t\u00e9 jug\u00e9e aussi salubre que les vari\u00e9t\u00e9s de pommes classiques, Sant\u00e9 Canada n'exige pas que ce produit soit \u00e9tiquet\u00e9 de fa\u00e7on particuli\u00e8re. Au Canada, il est permis d'\u00e9tiqueter les produits sur une base volontaire, pour offrir aux consommateurs des renseignements qui ne portent pas sur l'innocuit\u00e9 du produit. Selon la norme nationale \u00ab\u00a0<a href=\"http://www.tpsgc-pwgsc.gc.ca/ongc-cgsb/programme-program/normes-standards/internet/032-0315/index-fra.html\">\u00c9tiquetage volontaire et publicit\u00e9 visant les aliments issus ou non du g\u00e9nie g\u00e9n\u00e9tique</a>\u00a0\u00bb, les produits peuvent \u00eatre volontairement \u00e9tiquet\u00e9s comme \u00e9tant g\u00e9n\u00e9tiquement modifi\u00e9s ou non issus du g\u00e9nie g\u00e9n\u00e9tique pourvu que les conditions soient respect\u00e9es et que l'all\u00e9gation soit intelligible, informative, exacte et non trompeuse. La d\u00e9cision de proc\u00e9der \u00e0 un \u00e9tiquetage volontaire incombe uniquement \u00e0 l'entreprise J.R. Simplot.</p>\n<h2 class=\"h5\">Quelles donn\u00e9es ont \u00e9t\u00e9 utilis\u00e9es dans le cadre de ces \u00e9valuations?</h2>\n<p>Dans le cadre du processus d'\u00e9valuation, l'entreprise J.R. Simplot a fourni des donn\u00e9es exhaustives que les responsables de la r\u00e9glementation de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> et de Sant\u00e9 Canada ont pu examiner. Les donn\u00e9es pr\u00e9sent\u00e9es comprenaient notamment une description d\u00e9taill\u00e9e de la fa\u00e7on dont la pomme de terre <span lang=\"en\">Innate</span><sup><abbr title=\"marque de commerce\">MC</abbr></sup> a \u00e9t\u00e9 mise au point et comment ses caract\u00e9ristiques de r\u00e9duction du brunissement et de l'acrylamide fonctionnent. Les responsables de la r\u00e9glementation ont \u00e9galement consult\u00e9 des \u00e9tudes r\u00e9vis\u00e9es par des pairs provenant de publications scientifiques.</p>\n<p>Le cadre de r\u00e9glementation du Canada exige que le concepteur du produit pr\u00e9sente les donn\u00e9es n\u00e9cessaires pour r\u00e9pondre aux exigences des \u00e9valuations de l'innocuit\u00e9 et de la valeur nutritionnelle. Cette m\u00e9thode de collecte de donn\u00e9es est conforme \u00e0 celle des organismes de r\u00e9glementation internationaux et correspond \u00e0 la pratique standard pour faire en sorte que l'innocuit\u00e9 et la valeur nutritionnelle du produit puissent \u00eatre \u00e9valu\u00e9es.</p>\n<p>Un document de d\u00e9cision qui pr\u00e9sente l'\u00e9valuation de l'innocuit\u00e9 et la d\u00e9cision r\u00e9glementaire sera affich\u00e9 sur le site Web de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}