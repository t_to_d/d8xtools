{
    "dcr_id": "1601393370403",
    "title": {
        "en": "Chronic wasting disease: what hunters should know",
        "fr": "Maladie d\u00e9bilitante chronique\u00a0: ce que les chasseurs doivent savoir "
    },
    "html_modified": "30-9-2020",
    "modified": "17-11-2021",
    "issued": "30-9-2020",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/cwd_fctsht_hunters_1601393370403_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/cwd_fctsht_hunters_1601393370403_fra"
    },
    "parent_ia_id": "1330143991594",
    "ia_id": "1601393370841",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Chronic wasting disease: what hunters should know",
        "fr": "Maladie d\u00e9bilitante chronique\u00a0: ce que les chasseurs doivent savoir "
    },
    "label": {
        "en": "Chronic wasting disease: what hunters should know",
        "fr": "Maladie d\u00e9bilitante chronique\u00a0: ce que les chasseurs doivent savoir "
    },
    "templatetype": "content page 1 column",
    "node_id": "1601393370841",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1330143462380",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/prion-diseases/cwd/what-hunters-should-know/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/maladies-a-prions/mdc/ce-que-les-chasseurs-doivent-savoir/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Chronic wasting disease: what hunters should know",
            "fr": "Maladie d\u00e9bilitante chronique\u00a0: ce que les chasseurs doivent savoir"
        },
        "description": {
            "en": "If you hunt, handle or eat deer, elk, caribou or moose, you should know about Chronic Wasting Disease (CWD).",
            "fr": "Si vous chassez, manipulez ou mangez du cerf, du wapiti, du caribou ou de l'orignal, vous devez conna\u00eetre la maladie d\u00e9bilitante chronique (MDC)."
        },
        "keywords": {
            "en": "Health of Animals Act, reportable disease, disease, chronic wasting disease, CWD, hunters",
            "fr": "Loi sur la sant\u00e9 des animaux, maladie \u00e0 d\u00e9claration obligatoire, maladie, maladie d\u00e9bilitante chronique, chasseurs"
        },
        "dcterms.subject": {
            "en": "animal inspection,animal diseases,regulations,animal health",
            "fr": "inspection des animaux,maladie animale,r\u00e9glementations,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2020-09-30",
            "fr": "2020-09-30"
        },
        "modified": {
            "en": "2021-11-17",
            "fr": "2021-11-17"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Chronic wasting disease: what hunters should know",
        "fr": "Maladie d\u00e9bilitante chronique\u00a0: ce que les chasseurs doivent savoir"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n\n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/cpa-fsu-cwd_what_hunters_need_to_know_1601565827066_eng.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/WORKAREA/DAM-comn-comn/images-images/download_pdf_1558729521562_eng.png\" alt=\"\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>CWD\u00a0\u2013 What hunters should know</span> <span class=\"gc-dwnld-info nowrap\">(PDF - 179 kb)</span></p>\n</div>\n</div>\n</div>\n</a>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n\n<p class=\"mrgn-tp-lg\">If you hunt, handle or eat deer, elk, caribou or moose, you should know about chronic wasting disease (CWD).</p>\n\n<p>CWD is a progressive, fatal nervous system disease that affects these animals, which are all part of the deer family known as cervids. It is a transmissible spongiform encephalopathy, or prion disease. It is contagious among cervids, like scrapie in sheep.</p>\n\n<p>There has been no known transmission of CWD to humans; however, bovine spongiform encephalopathy (also known as mad cow disease) in cattle has been known to cause Creutzfeldt-Jakob disease in humans.</p>\n\n<p>As a precaution, the Government of Canada recommends that people not consume any part of an animal that has tested positive for CWD.</p>\n\n<p>In Canada, CWD has been predominantly found in wild and farmed deer and elk populations in Saskatchewan and Alberta. There have been 3 confirmed cases in wild moose. The disease has also been found in farmed red deer in Quebec and in a wild deer in Manitoba. It has not been detected in wild cervids in other provinces or territories in Canada. It has not been detected in wild caribou anywhere in North America.</p>\n\n<p>There are a number of actions people can take when hunting to reduce their potential risk of exposure to CWD when handling animals in the deer family.</p>\n\n<h2>How to protect yourself</h2>\n\n<ul class=\"lst-spcd\">\n<li>Before you hunt, check with your provincial or territorial wildlife management office to see whether CWD has been found in the area, and whether testing is available or required for CWD</li>\n<li>People in the area of Alberta, Saskatchewan or Quebec where CWD has been found, should have their animal tested for CWD before eating the meat, preparing trophies or harvesting hides</li>\n<li>Pay attention to the appearance and behaviour of the animals you are hunting</li>\n<li>Do not harvest an animal that appears to be sick and report sick or dead animals to your provincial or territorial wildlife authority</li>\n<li>Avoid handling or eating an animal or parts of an animal that has died from unknown causes or has tested positive for CWD</li>\n<li>When handling carcasses, wear latex or rubber gloves and avoid handling or eating the animal's brain, spinal cord, eyes, spleen, tonsils or lymph nodes. If you use parts of the carcass to tan the hide, minimize handling of the animal's head and brain</li>\n<li>Wash hands and tools thoroughly with soap and water after field dressing is completed. Tools should then be disinfected with bleach, as this substance can decontaminate surfaces contaminated with CWD prions</li>\n<li>Appropriate disposal is also important to reduce the risk of spread of CWD. Contact your provincial wildlife management office for information</li>\n<li>If you have the animal commercially processed, ask that your animal be processed individually, without meat from other cervids being added</li>\n</ul>\n\n<p>Material from known CWD-infected cervids is not permissible for use in natural health products (NHPs). Consumers are encouraged to use all the available label information when selecting health products to meet their needs. Canadians may wish to avoid consuming NHPs that contain materials from cervids, since the existence of a potential risk cannot be definitively excluded. To date, Health Canada is not aware of any documented cases of CWD infection in humans.</p>\n\n<h2>How is CWD diagnosed and what are the limitations of testing</h2>\n\n<p>Although animals infected with CWD sometimes show <a href=\"/eng/1330189947852/1330190096558#cscwd\" title=\"Clinical signs of CWD\">symptoms</a>, CWD can only be confirmed by testing specific tissues from an affected animal after it is dead. While a negative test result does not guarantee that an individual animal is not infected with CWD, it is considerably less likely and may reduce your potential risk of exposure to CWD.</p>\n\n<p>Currently, CWD tests officially approved by the Canadian Food Inspection Agency (CFIA) are designed for surveillance purposes and are not reliable enough to detect the disease in animals under 12 months of age. CFIA's surveillance testing aims to identify farmed animals over 12 months of age (who are more likely to be infected with CWD), in order to prevent contaminated meat or other consumable products from entering the market. There is currently no test available to certify that food or other consumable products are free from the CWD prion.</p>\n\n<h2>What we know about CWD and human health</h2>\n\n<p>There has been no known transmission of CWD to humans. Extensive surveillance of human prion diseases in Canada and elsewhere has not provided any direct evidence that CWD has infected humans. However, experts continue to study CWD and whether it has the potential to infect other animals and humans. As a precaution, measures are in place to prevent known infected animals from entering the food chain, including:</p>\n\n<ul class=\"lst-spcd\">\n<li>mandatory testing of all cervids sent for slaughter (over the age of 12 months) at all abattoirs in Saskatchewan, Alberta, Manitoba, Yukon and Quebec</li>\n<li>not allowing animals known to be positive for CWD to enter the commercial food chain</li>\n<li>reporting immediately to the CFIA all suspected cases, as CWD is a reportable disease under the <i>Health of Animals Act</i></li>\n</ul>\n\n<h2>Related links</h2>\n\n<ul>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/food-safety/food-related-illnesses/chronic-wasting-disease.html\">Canada.ca: Chronic Wasting Disease</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/reportable/prion-diseases/cwd/eng/1330143462380/1330143991594\">CFIA: Chronic wasting disease (CWD) of deer and elk</a></li>\n<li><a href=\"https://www.sac-isc.gc.ca/eng/1571687660394/1571687748442\">Tips on food safety when hunting</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"row\">\n<div class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n<a class=\"gc-dwnld-lnk\" href=\"/DAM/DAM-animals-animaux/STAGING/text-texte/cpa-fsu-cwd_what_hunters_need_to_know_1601565827066_fra.pdf\">\n<div class=\"well gc-dwnld\">\n<div class=\"row\">\n<div class=\"col-xs-3 col-sm-3 col-md-2 col-lg-2\">\n<p><img class=\"img-responsive thumbnail gc-dwnld-img\" src=\"/DAM/DAM-comn-comn/WORKAREA/DAM-comn-comn/images-images/download_pdf_1558729521562_fra.png\" alt=\"\"></p>\n</div>\n<div class=\"col-xs-9 col-sm-9 col-md-10 col-lg-10\">\n<p class=\"gc-dwnld-txt\"><span>MDC\u00a0\u2013 Ce que les chasseurs doivent savoir</span> <span class=\"gc-dwnld-info nowrap\">(PDF - 180 ko)</span></p>\n</div>\n</div>\n</div>\n</a>\n</div>\n</div>\n<div class=\"clearfix\"></div>\n\n<p class=\"mrgn-tp-lg\">Si vous chassez, manipulez ou mangez du cerf, du wapiti, du caribou ou de l'orignal, vous devez conna\u00eetre la maladie d\u00e9bilitante chronique (MDC).</p>\n\n<p>La MDC est une maladie progressive et mortelle du syst\u00e8me nerveux qui touche ces animaux qui font tous partie de la famille des cervid\u00e9s. C'est une enc\u00e9phalopathie spongiforme transmissible, aussi appel\u00e9e maladie \u00e0 prion. Elle est contagieuse chez les cervid\u00e9s, comme la tremblante du mouton.</p>\n\n<p>Il n'y a pas eu de transmission connue de la MDC aux humains, mais on sait que l'enc\u00e9phalopathie spongiforme bovine (aussi appel\u00e9e maladie de la vache folle) chez les bovins cause la maladie de Creutzfeldt-Jakob chez les humains.</p>\n\n<p>Par mesure de pr\u00e9caution, le gouvernement du Canada recommande que les gens ne consomment aucune partie d'un animal dont les tests de d\u00e9pistage de la MDC sont positifs.</p>\n\n<p>Au Canada, la MDC est surtout pr\u00e9sente dans les populations de cerfs et de wapitis sauvages et d'\u00e9levage en Saskatchewan et en Alberta. Il existe trois cas confirm\u00e9s chez des orignaux sauvages. La maladie a \u00e9galement \u00e9t\u00e9 d\u00e9cel\u00e9e chez le cerf rouge d'\u00e9levage au Qu\u00e9bec et dans un cerf sauvage au Manitoba. Elle n'a pas \u00e9t\u00e9 d\u00e9tect\u00e9e chez les cervid\u00e9s sauvages des autres provinces ou territoires du Canada ni chez les caribous sauvages en Am\u00e9rique du Nord.</p>\n\n<p>Il y a plusieurs mesures que les gens peuvent prendre lorsqu'ils chassent pour r\u00e9duire le risque d'exposition \u00e0 la MDC pendant la manipulation des animaux de la famille des cerfs.</p>\n\n<h2>Comment vous prot\u00e9ger</h2>\n\n<ul class=\"lst-spcd\">\n<li>Avant de chasser, v\u00e9rifiez aupr\u00e8s du bureau de gestion de la faune de votre province ou de votre territoire pour savoir si la MDC a \u00e9t\u00e9 d\u00e9tect\u00e9e dans la zone et si des tests sont disponibles ou requis pour d\u00e9tecter la maladie</li>\n<li>Les gens de la r\u00e9gion de l'Alberta, de la Saskatchewan ou du Qu\u00e9bec o\u00f9 la MDC a \u00e9t\u00e9 d\u00e9cel\u00e9e devraient faire analyser leur animal avant de manger la viande, de pr\u00e9parer des troph\u00e9es ou de pr\u00e9parer des peaux</li>\n<li>Vous devriez porter attention \u00e0 l'apparence et au comportement des animaux que vous chassez</li>\n<li>N'abattez pas un animal qui semble malade et signalez les animaux malades ou morts \u00e0 votre autorit\u00e9 provinciale ou territoriale responsable de la faune</li>\n<li>\u00c9vitez de manipuler ou de manger un animal ou des parties d'un animal qui est mort de causes inconnues ou dont le test de d\u00e9pistage de la MDC est positif</li>\n<li>Lorsque vous manipulez des carcasses, portez des gants en latex ou en caoutchouc et \u00e9vitez de manipuler ou de manger le cerveau, la moelle \u00e9pini\u00e8re, les yeux, la rate, les amygdales ou les ganglions lymphatiques de l'animal. Si vous utilisez des parties de la carcasse pour tanner la peau, r\u00e9duisez au minimum la manipulation de la t\u00eate et du cerveau de l'animal</li>\n<li>Lavez-vous soigneusement les mains et lavez les outils avec de l'eau et du savon une fois que vous avez termin\u00e9 la manipulation sur le terrain. Vous devez ensuite d\u00e9sinfecter les outils avec de l'eau de Javel, car cette substance peut d\u00e9contaminer les surfaces contamin\u00e9es par des prions de la MDC</li>\n<li>L'\u00e9limination appropri\u00e9e est \u00e9galement importante pour r\u00e9duire le risque de propagation de la MDC. Communiquez avec votre bureau provincial de gestion de la faune pour obtenir des renseignements \u00e0 ce sujet</li>\n<li>Si l'animal est transform\u00e9 commercialement, demandez qu'il soit transform\u00e9 individuellement, sans ajouter de viande d'autres cervid\u00e9s</li>\n</ul>\n\n<p>Les mati\u00e8res provenant de cervid\u00e9s infect\u00e9s par la MDC ne sont pas autoris\u00e9es dans les produits de sant\u00e9 naturels (PSN). On encourage les consommateurs \u00e0 utiliser toute l'information disponible sur l'\u00e9tiquette lorsqu'ils choisissent des produits de sant\u00e9 qui r\u00e9pondent \u00e0 leurs besoins. Les Canadiens et Canadiennes peuvent souhaiter \u00e9viter de consommer des PSN qui contiennent des mati\u00e8res provenant des cervid\u00e9s, car l'existence d'un risque ne peut \u00eatre d\u00e9finitivement exclue. \u00c0 ce jour, Sant\u00e9 Canada n'est au courant d'aucun cas document\u00e9 d'infection par la MDC chez les humains.</p>\n\n<h2>Comment la MDC est-elle diagnostiqu\u00e9e et quelles sont les limites des tests</h2>\n\n<p>Bien que les animaux infect\u00e9s par la MDC pr\u00e9sentent parfois des <a href=\"/fra/1330189947852/1330190096558#cscwd\" title=\"Les signes cliniques de la maladie d\u00e9bilitante chronique\">sympt\u00f4mes</a>, on ne peut confirmer la pr\u00e9sence de la maladie qu'en analysant des tissus pr\u00e9cis d'un animal touch\u00e9 apr\u00e8s sa mort. Un r\u00e9sultat de test n\u00e9gatif ne garantit pas qu'un animal n'est pas infect\u00e9 par la MDC, mais il est beaucoup moins probable que ce soit le cas, ainsi votre risque d'exposition \u00e0 la MDC est r\u00e9duit.</p>\n\n<p>\u00c0 l'heure actuelle, les analyses de la MDC officiellement approuv\u00e9es par l'Agence canadienne d'inspection des aliments (ACIA) sont con\u00e7ues \u00e0 des fins de surveillance et ne sont pas suffisamment fiables pour d\u00e9tecter la maladie chez les animaux de moins de 12 mois. Les tests de surveillance de l'ACIA visent \u00e0 identifier les animaux d'\u00e9levage de plus de 12 mois (qui sont plus susceptibles d'\u00eatre infect\u00e9 par la MDC) afin d'emp\u00eacher la viande ou d'autres produits de consommation contamin\u00e9s d'entrer sur le march\u00e9. Il n'existe actuellement aucun test permettant de certifier que les aliments ou d'autres produits consommables sont exempts de prions de la MDC.</p>\n\n<h2>Ce que nous savons de la MDC et de la sant\u00e9 humaine</h2>\n\n<p>Il n'y a pas eu de transmission connue de la MDC aux humains. La surveillance pouss\u00e9e des maladies humaines \u00e0 prions au Canada et ailleurs n'a fourni aucune preuve directe que la MDC a infect\u00e9 les humains. Cependant, les experts continuent d'\u00e9tudier la MDC et la possibilit\u00e9 qu'elle infecte d'autres animaux et humains. \u00c0 titre de pr\u00e9caution, des mesures sont en place pour emp\u00eacher les animaux infect\u00e9s connus d'entrer dans la cha\u00eene alimentaire, notamment\u00a0:</p>\n\n<ul class=\"lst-spcd\">\n<li>D\u00e9pistage obligatoire chez tous les cervid\u00e9s envoy\u00e9s \u00e0 l'abattage (de plus de 12 mois) dans tous les abattoirs de la Saskatchewan, de l'Alberta, du Manitoba, du Yukon et du Qu\u00e9bec</li>\n<li>Interdiction aux animaux dont on sait qu'ils sont porteurs de la MDC d'entrer dans la cha\u00eene alimentaire commerciale</li>\n<li>Signalisation imm\u00e9diatement \u00e0 l'ACIA de tous les cas soup\u00e7onn\u00e9s, car la MDC est une maladie \u00e0 d\u00e9claration obligatoire en vertu de la <i>Loi sur la sant\u00e9 des animaux</i></li>\n</ul>\n\n<h2>Liens connexes</h2>\n\n<ul>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/salubrite-aliments/maladies-origine-alimentaire/maladie-debilitante-chronique.html\">Canada.ca\u00a0: Maladie d\u00e9bilitante chronique</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/maladies-a-prions/mdc/fra/1330143462380/1330143991594\">Maladie d\u00e9bilitante chronique (MDC) des cerfs et des wapitis</a></li>\n<li><a href=\"https://www.sac-isc.gc.ca/fra/1571687660394/1571687748442\">Conseils sur la salubrit\u00e9 alimentaire pendant la chasse</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}