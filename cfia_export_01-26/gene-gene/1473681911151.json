{
    "dcr_id": "1473681911151",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Anthriscus sylvestris</i> (Cow parsley)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Anthriscus sylvestris</i> (Anthrisque des bois)"
    },
    "html_modified": "2024-01-26 2:23:23 PM",
    "modified": "2017-06-28",
    "issued": "2017-05-26",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_anthriscus_sylvestris_1473681911151_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_anthriscus_sylvestris_1473681911151_fra"
    },
    "ia_id": "1473681911507",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Anthriscus sylvestris</i> (Cow parsley)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Anthriscus sylvestris</i> (Anthrisque des bois)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Anthriscus sylvestris</i> (Cow parsley)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Anthriscus sylvestris</i> (Anthrisque des bois)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1473681911507",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/anthriscus-sylvestris/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/anthriscus-sylvestris/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Anthriscus sylvestris (Cow parsley)",
            "fr": "Semence de mauvaises herbe : Anthriscus sylvestris (Anthrisque des bois)"
        },
        "description": {
            "en": "Fact sheet for Anthriscus sylvestris, Apiaceae, Cow parsley",
            "fr": "Fiche de renseignements pour Anthriscus sylvestris, Apiaceae, Anthrisque des bois"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Anthriscus sylvestris, Apiaceae, Cow parsley",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Anthriscus sylvestris, Apiaceae, Anthrisque des bois"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-05-26",
            "fr": "2017-05-26"
        },
        "modified": {
            "en": "2017-06-28",
            "fr": "2017-06-28"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,general public,government,scientists",
            "fr": "entreprises,grand public,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Anthriscus sylvestris (Cow parsley)",
        "fr": "Semence de mauvaises herbe : Anthriscus sylvestris (Anthrisque des bois)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Family</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Cow parsley</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Newfoundland and Labrador\">NL</abbr>, <abbr title=\"Nova Scotia\">NS</abbr> (Brouillet <abbr title=\"and others\">et al.</abbr> <span class=\"nowrap\">2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup></span>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, Europe and Asia and introduced to southern Africa, North America and Chile (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> <span class=\"nowrap\">2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup></span>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Biennial or short-lived perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Schizocarp, divided into 2 mericarps</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Mericarp length: 6.0 - 7.0\u00a0<abbr title=\"millimetres\">mm</abbr></li> \n<li>Mericarp width: 1.2 - 1.6\u00a0<abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Mericarp is elongate, tapering to a remnant floral style at the top</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Mericarp is shiny, smooth</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Mericarp is medium brown</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Furrow along ventral side of mericarp.</li>\n<li>Teardrop-shaped depression at base of mericarp.</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Pastures, damp meadows, old fields, open forests, stream banks, ditches, roadsides and other disturbed areas (Darbyshire <abbr title=\"and others\">et al.</abbr> <span class=\"nowrap\">1999<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup></span>, Darbyshire <span class=\"nowrap\">2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup></span>). Weed of perennial forage crops and pastures in North America (Darbyshire <abbr title=\"and others\">et al.</abbr> <span class=\"nowrap\">1999<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup></span>). </p>\n\n<h2>General Information</h2>\n<p>Cow parsley produces 800 to 10,000 seeds per plant, which are often dispersed by agricultural equipment (Rew <abbr title=\"and others\">et al.</abbr> <span class=\"nowrap\">1996<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup></span>) and traffic corridors (Magn\u00fasson <span class=\"nowrap\">2006<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup></span>). This species prefers disturbed habitats and forest edges with moist or mesic soils, rich with organic matter (Darbyshire <abbr title=\"and others\">et al.</abbr> <span class=\"nowrap\">1999<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup></span>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Garden chervil (<i lang=\"la\">Anthriscus cerefolium</i>)</h3>\n<ul><li>Garden chervil is a similar elongated shape, size, brown colour and floral style to cow parsley.</li>\n<li>Garden chervil has a longer floral style (2.0 - 4.0\u00a0<abbr title=\"millimetres\">mm</abbr>), a bumpy surface texture and is thinner (1.0\u00a0<abbr title=\"millimetre\">mm</abbr>) compared to cow parsley.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_04cnsh_1475586917434_eng.jpg\" alt=\"Cow parsley (Anthriscus sylvestris) mericarps\" class=\"img-responsive\">\n<figcaption>Cow parsley (<i lang=\"la\">Anthriscus sylvestris</i>) mericarps</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_01cnsh_1475586856125_eng.jpg\" alt=\"Cow parsley (Anthriscus sylvestris) mericarp, interior\" class=\"img-responsive\">\n<figcaption>Cow parsley (<i lang=\"la\">Anthriscus sylvestris</i>) mericarp</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_03cnsh_1475586887913_eng.jpg\" alt=\"Cow parsley (Anthriscus sylvestris) mericarp, exterior\" class=\"img-responsive\">\n<figcaption>Cow parsley (<i lang=\"la\">Anthriscus sylvestris</i>) mericarp</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_copyright_1475586946577_eng.jpg\" alt=\"Cow parsley (Anthriscus sylvestris) mericarp\" class=\"img-responsive\">\n<figcaption>Cow parsley (<i lang=\"la\">Anthriscus sylvestris</i>) mericarp</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_cerefolium_04cnsh_1475586825973_eng.jpg\" alt=\"Similar species: Garden chervil (Anthriscus cerefolium) mericarps\" class=\"img-responsive\">\n<figcaption>Similar species: Garden chervil (<i lang=\"la\">Anthriscus cerefolium</i>) mericarp, exterior\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_cerefolium_01cnsh_1475586785967_eng.jpg\" alt=\"Similar species: Garden chervil (Anthriscus cerefolium) mericarp\" class=\"img-responsive\">\n<figcaption>Similar species: Garden chervil (<i lang=\"la\">Anthriscus cerefolium</i>) mericarp\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [<span class=\"nowrap\">2016, May 30</span>].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [<span class=\"nowrap\">2016, May 30</span>].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Darbyshire, S. J., Hoeg, R. and Haverkort, J. 1999</strong>. The biology of Canadian weeds. 111. <i lang=\"la\">Anthriscus sylvestris</i> (<abbr title=\"Linnaeus\">L.</abbr>) Hoffm. Canadian Journal of Plant Science 79: 671\u2013682.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Rew, L. J., Froud-Williams, R. J. and Boatman, N. D. 1996</strong>. Dispersal of <i lang=\"la\">Bromus sterilis</i> and <i lang=\"la\">Anthriscus sylvestris</i> seed within arable field margins. Department of Agricultural Botany, 2 Earley Gate, University of Reading, Reading, <abbr title=\"United Kingdom\">UK</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Magn\u00fasson, S. H. 2006</strong>. <abbr title=\"European Network on Invasive Alien Species\">NOBANIS</abbr> \u2013 Invasive Alien Species Fact Sheet \u2013 <i lang=\"la\">Anthriscus sylvestris</i>. Online Database of the North European and Baltic Network on Invasive Alien Species \u2013 <abbr title=\"European Network on Invasive Alien Species\">NOBANIS</abbr>, https://www.nobanis.org/species-info/?taxaId=1858 [<span class=\"nowrap\">2016, May 30</span>].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Famille</h2>\n<p><i lang=\"la\">Apiaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Anthrisque des bois</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> L'esp\u00e8ce est pr\u00e9sente en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr>, au <abbr title=\"Qu\u00e9bec\">Qc</abbr>, au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr>, \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr> et en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr> (Brouillet <abbr title=\"et divers collaborateurs\">et al.</abbr>, <span class=\"nowrap\">2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>1</a></sup></span>). </p>\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Plante indig\u00e8ne d'Afrique du Nord, d'Europe et d'Asie. Introduite en Afrique australe, en Am\u00e9rique du Nord et au Chili (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Bisannuelle, ou vivace \u00e0 courte dur\u00e9e de vie</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Schizocarpe, divis\u00e9 en deux m\u00e9ricarpes</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur du m\u00e9ricarpe\u00a0: 6,0 - 7,0\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr></li> \n<li>Largeur du m\u00e9ricarpe\u00a0: 1,2 - 1,6\u00a0<abbr title=\"millim\u00e8tres\">mm</abbr></li> \n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe allong\u00e9, \u00e0 sommet effil\u00e9, portant un bec persistant \u00e0 son sommet</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>M\u00e9ricarpe \u00e0 surface luisante, lisse</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>M\u00e9ricarpe brun moyen</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Sillon le long de la face ventrale du m\u00e9ricarpe.</li>\n<li>D\u00e9pression en forme de larme \u00e0 la base du m\u00e9ricarpe.</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>P\u00e2turages, pr\u00e9s humides, champs abandonn\u00e9s, for\u00eats clairsem\u00e9es, berges de cours d'eau, foss\u00e9s, bords de chemin et autres terrains perturb\u00e9s (Darbyshire <abbr title=\"et divers collaborateurs\">et al.</abbr>, <span class=\"nowrap\">1999<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup></span>, Darbyshire, <span class=\"nowrap\">2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>4</a></sup></span>). Mauvaise herbe des cultures fourrag\u00e8res p\u00e9rennes et des p\u00e2turages en Am\u00e9rique du Nord (Darbyshire <abbr title=\"et divers collaborateurs\">et al.</abbr>, <span class=\"nowrap\">1999<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup></span>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Un plant d'anthrisque des bois produit de 800 \u00e0 <span class=\"nowrap\">10 000 graines</span>, lesquelles sont souvent dispers\u00e9es par l'\u00e9quipement agricole  (Rew <abbr title=\"et divers collaborateurs\">et al.</abbr>, <span class=\"nowrap\">1996<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>5</a></sup></span>) et les corridors de circulation (Magn\u00fasson, <span class=\"nowrap\">2006<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>6</a></sup></span>). Cette esp\u00e8ce pr\u00e9f\u00e8re les milieux perturb\u00e9s et les bords de for\u00eat \u00e0 sols humide ou m\u00e9sique et riches en mati\u00e8re organique (Darbyshire <abbr title=\"et divers collaborateurs\">et al.</abbr>, <span class=\"nowrap\">1999<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Note de bas de page\u00a0</span>3</a></sup></span>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Cerfeuil cultiv\u00e9 (<i lang=\"la\">Anthriscus cerefolium</i>)</h3>\n<ul><li>Le m\u00e9ricarpe du cerfeuil cultiv\u00e9 ressemble \u00e0 celui de l'anthrisque des bois par sa forme allong\u00e9e, ses dimensions, sa couleur brune et son \u00ab bec \u00bb.</li>\n<li>Le m\u00e9ricarpe du cerfeuil cultiv\u00e9 est plus mince (1,0 <abbr title=\"millim\u00e8tre\">mm</abbr>) que celui de l'anthrisque des bois et pr\u00e9sente un \u00ab bec \u00bb plus long (2,0 - 4,0 <abbr title=\"millim\u00e8tre\">mm</abbr>), et sa surface est bossel\u00e9e.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_04cnsh_1475586917434_fra.jpg\" alt=\"Anthrisque des bois (Anthriscus sylvestris) m\u00e9ricarpes\" class=\"img-responsive\">\n<figcaption>Anthrisque des bois (<i lang=\"la\">Anthriscus sylvestris</i>) m\u00e9ricarpes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_01cnsh_1475586856125_fra.jpg\" alt=\"Anthrisque des bois (Anthriscus sylvestris) m\u00e9ricarpe, la face int\u00e9rieure\" class=\"img-responsive\">\n<figcaption>Anthrisque des bois (<i lang=\"la\">Anthriscus sylvestris</i>) m\u00e9ricarpe\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_03cnsh_1475586887913_fra.jpg\" alt=\"Anthrisque des bois (Anthriscus sylvestris) m\u00e9ricarpe, la face ext\u00e9rieure\" class=\"img-responsive\">\n<figcaption>Anthrisque des bois (<i lang=\"la\">Anthriscus sylvestris</i>) m\u00e9ricarpe</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_sylvestris_copyright_1475586946577_fra.jpg\" alt=\"Anthrisque des bois (Anthriscus sylvestris) m\u00e9ricarpe\" class=\"img-responsive\">\n<figcaption>Anthrisque des bois (<i lang=\"la\">Anthriscus sylvestris</i>) m\u00e9ricarpe</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_cerefolium_04cnsh_1475586825973_fra.jpg\" alt=\"Esp\u00e8ce semblable : Cerfeuil cultiv\u00e9 (Anthriscus cerefolium) m\u00e9ricarpes\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Cerfeuil cultiv\u00e9 (<i lang=\"la\">Anthriscus cerefolium</i>) m\u00e9ricarpes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_anthriscus_cerefolium_01cnsh_1475586785967_fra.jpg\" alt=\"Esp\u00e8ce semblable : Cerfeuil cultiv\u00e9 (Anthriscus cerefolium) m\u00e9ricarpe\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Cerfeuil cultiv\u00e9 (<i lang=\"la\">Anthriscus cerefolium</i>) m\u00e9ricarpe</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [<span class=\"nowrap\">2016, May 30</span>].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [<span class=\"nowrap\">2016, May 30</span>].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Darbyshire, S. J., Hoeg, R. and Haverkort, J. 1999</strong>. The biology of Canadian weeds. 111. <i lang=\"la\">Anthriscus sylvestris</i> (<abbr title=\"Linnaeus\">L.</abbr>) Hoffm. Canadian Journal of Plant Science 79: 671\u2013682.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Rew, L. J., Froud-Williams, R. J. and Boatman, N. D. 1996</strong>. Dispersal of <i lang=\"la\">Bromus sterilis</i> and <i lang=\"la\">Anthriscus sylvestris</i> seed within arable field margins. Department of Agricultural Botany, 2 Earley Gate, University of Reading, Reading, <abbr title=\"United Kingdom\">UK</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Magn\u00fasson, S. H. 2006</strong>. <abbr title=\"European Network on Invasive Alien Species\">NOBANIS</abbr> \u2013 Invasive Alien Species Fact Sheet \u2013 <i lang=\"la\">Anthriscus sylvestris</i>. Online Database of the North European and Baltic Network on Invasive Alien Species \u2013 <abbr title=\"European Network on Invasive Alien Species\">NOBANIS</abbr>, https://www.nobanis.org/species-info/?taxaId=1858 [<span class=\"nowrap\">2016, May 30</span>].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}