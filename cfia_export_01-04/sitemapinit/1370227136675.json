{
    "dcr_id": "1370227088259",
    "title": {
        "en": "Novel feeds",
        "fr": "Aliments nouveaux"
    },
    "html_modified": "2015-04-08 12:57",
    "modified": "24-2-2020",
    "issued": "8-4-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/anima_feed_novel_feeds_1370227088259_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/WORKAREA/cfiaadmin-adminacia/templatedata/comn-comn/gene-gene/data/anima_feed_novel_feeds_1370227088259_fra"
    },
    "parent_ia_id": "1320536661238",
    "ia_id": "1370227136675",
    "parent_node_id": "1320536661238",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Novel feeds",
        "fr": "Aliments nouveaux"
    },
    "label": {
        "en": "Novel feeds",
        "fr": "Aliments nouveaux"
    },
    "templatetype": "content page 1 column",
    "node_id": "1370227136675",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "parent_dcr_id": "1299157225486",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/livestock-feeds/novel-feeds/",
        "fr": "/sante-des-animaux/aliments-du-betail/aliments-nouveaux/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Novel feeds",
            "fr": "Aliments nouveaux"
        },
        "description": {
            "en": "Novel feeds include microbial products (e.g. forage inoculants, fermentation products) plants with novel traits, and plants with no history of use as feed, and products/by-products of biotechnology-derived animals.",
            "fr": "Les aliments nouveaux du b\u00e9tail comprennent les produits microbiens (p. ex. les inoculums pour le fourrage, les produits de fermentation) les v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux et les v\u00e9g\u00e9taux n'ayant jamais \u00e9t\u00e9 utilis\u00e9s auparavant comme aliments du b\u00e9tail, et les produits/sous-produits d'animaux issus de la biotechnologie."
        },
        "keywords": {
            "en": "novel feeds, microbial products, plants with novel traits, biotechnology",
            "fr": "aliments nouveaux du b&eacute;tail, produits microbiens, v&eacute;g&eacute;taux &agrave; caract&egrave;res nouveaux, biotechnologie"
        },
        "dcterms.subject": {
            "en": "animal,biotechnology,agri-food industry,plants",
            "fr": "animal,biotechnologie,industrie agro-alimentaire,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-08 12:57:58",
            "fr": "2015-04-08 12:57:58"
        },
        "modified": {
            "en": "2020-02-24",
            "fr": "2020-02-24"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Novel feeds",
        "fr": "Aliments nouveaux"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=6&amp;ga=54#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>Novel feeds are feeds composed of or derived from microorganisms, plants or animal sources that:</p>\n<ul>\n<li>are not approved as livestock feed in Canada (not listed in Schedule <abbr title=\"4\">IV</abbr> or <abbr title=\"5\">V</abbr> of the Feeds Regulations)</li>\n<li>and/or contain a novel trait</li>\n</ul>\n<p>Novel feeds include microbial products (<abbr title=\"for example\">e.g.</abbr> forage inoculants, fermentation products) plants with novel traits, and plants with no history of use as feed, and products/by-products of biotechnology-derived animals.</p>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/approved-under-review/decision-documents/eng/1303704378026/1303704484236\">Decision Documents - Determination of Environmental and Livestock Feed Safety</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/managing-cases-of-non-compliance/overview/eng/1338275008823/1338275093927\">Introduction to <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>'s Policy on Managing Cases of Non-compliance of Unauthorized Plant Products Derived through Biotechnology</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/pre-submission-consultation/eng/1368394145255/1368394206548\">Pre-submission consultation procedures for novel foods and feeds derived from plants with novel traits</a></li>\n<li>Specific requirements for novel feeds derived from                 \n<ul>\n<li><a href=\"/animal-health/livestock-feeds/novel-feeds/biotechnology-derived-animals/eng/1370246252019/1370246321528\">Animal sources</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-2/eng/1329298059609/1329298179464?chap=7\">Microbial sources</a></li>\n<li><a href=\"/animal-health/livestock-feeds/regulatory-guidance/rg-1/chapter-2/eng/1329298059609/1329298179464?chap=6\">Plant sources</a></li>\n</ul></li>\n<li><a href=\"/animal-health/livestock-feeds/novel-feeds/guidance/eng/1547154845895/1547154883652\">Guidance for submitting whole genome sequencing (WGS) data to support the pre-market assessment of novel foods, novel feeds, and plants with novel traits</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/gene-editing-techniques/eng/1541800629219/1541800629556\">Gene editing techniques</a></li>\n</ul>\n<h2>Additional information</h2>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/canadian-pre-market-regulatory-process/eng/1542135073648/1542135074107\">Canadian pre-market regulatory process</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/genetically-modified-foods-other-novel-foods/approved-products/frequently-asked-questions-aquadvantage-salmon.html\">Frequently Asked Questions: AquAdvantage Salmon</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/general-public/eng/1337380923340/1337384231869\">General Information on Biotechnology</a></li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/food-nutrition/genetically-modified-foods-other-novel-foods/canadian-food-inspection-agency-statement-seralini-2012-publication-2-year-rodent-feeding-study-glyphosate-formulations-maize-nk603.html\">Health Canada's and the Canadian Food Inspection Agency's statement on the <span lang=\"fr\">S\u00e9ralini</span> <abbr lang=\"la\" title=\"and others\">et al.</abbr> (2012) publication on a 2-year rodent feeding study with glyphosate formulations and <abbr title=\"genetically modified\">GM</abbr> maize NK603</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=6&amp;ga=54#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>Les aliments nouveaux du b\u00e9tail sont des aliments constitu\u00e9s ou d\u00e9riv\u00e9s de microorganismes, de v\u00e9g\u00e9taux ou d'animaux\u00a0:</p>\n<ul>\n<li>qui ne sont pas approuv\u00e9s comme aliments du b\u00e9tail au Canada (qui ne figurent pas \u00e0 l'annexe <abbr title=\"4\">IV</abbr> ou <abbr title=\"5\">V</abbr> du R\u00e8glement sur les aliments du b\u00e9tail)</li>\n<li>et/ou qui comportent un caract\u00e8re nouveau</li>\n</ul>\n<p>Les aliments nouveaux du b\u00e9tail comprennent les produits microbiens (<abbr title=\"par exemple\">p.\u00a0ex.</abbr> les inoculums pour le fourrage, les produits de fermentation) les v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux et les v\u00e9g\u00e9taux n'ayant jamais \u00e9t\u00e9 utilis\u00e9s auparavant comme aliments du b\u00e9tail, et les produits/sous-produits d'animaux issus de la biotechnologie.</p>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/approuves-cours-d-evaluation/documents-des-decisions/fra/1303704378026/1303704484236\">Documents des d\u00e9cisions - D\u00e9termination de la s\u00e9curit\u00e9 environnementale et de l'innocuit\u00e9 des aliments du b\u00e9tail</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/gestion-des-situations-de-non-conformite/apercu/fra/1338275008823/1338275093927\">Pr\u00e9sentation de la politique de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> sur la gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/procedures-de-consultation-prealable/fra/1368394145255/1368394206548\">Proc\u00e9dures de consultation pr\u00e9alable concernant les aliments nouveaux et les aliments du b\u00e9tail provenant de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a></li>\n<li>Exigences sp\u00e9ciales pour les sources d'aliments nouveaux   \n<ul>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/aliments-nouveaux/provenant-d-animaux-issus-de-la-biotechnologie/fra/1370246252019/1370246321528\">Origine animale</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-2/fra/1329298059609/1329298179464?chap=7\">Origine microbienne</a></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/directives-reglementaires/rg-1/chapitre-2/fra/1329298059609/1329298179464?chap=6\">Origine v\u00e9g\u00e9tale</a></li>\n</ul></li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/aliments-nouveaux/lignes-directrices/fra/1547154845895/1547154883652\">Lignes directrices sur la pr\u00e9sentation des donn\u00e9es issues du s\u00e9quen\u00e7age du g\u00e9nome entier (SGE) au soutien de l'\u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 des aliments nouveaux destin\u00e9s \u00e0 la consommation humaine et animale et aux v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/techniques-de-modification-genetique/fra/1541800629219/1541800629556\">Techniques de modification g\u00e9n\u00e9tique</a></li>\n</ul>\n<h2>Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/processus-reglementaire-canadien-prealable-a-la-mi/fra/1542135073648/1542135074107\">Processus r\u00e9glementaire canadien pr\u00e9alable \u00e0 la mise en march\u00e9</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/aliments-genetiquement-modifies-autres-aliments-nouveaux/produits-approuves/foire-questions-saumon-aquadvantage.html\">Foire aux questions\u00a0: Saumon AquAdvantage</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/grand-public/fra/1337380923340/1337384231869\">Renseignements g\u00e9n\u00e9raux sur la biotechnologie</a></li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/aliments-nutrition/aliments-genetiquement-modifies-autres-aliments-nouveaux/position-agence-canadienne-inspection-aliments-concernant-publication-etude-toxicologique-longue-duree-portant-mais-nk603-roundup-ready.html\">Position de Sant\u00e9 Canada et de l'Agence canadienne d'inspection des aliments concernant la publication d'une \u00e9tude toxicologique de longue dur\u00e9e portant sur le ma\u00efs NK603 <span lang=\"en\">Roundup Ready</span> et l'herbicide <span lang=\"en\">Roundup</span> par S\u00e9ralini <abbr title=\"et divers collaborateurs\">et coll.</abbr> (2012)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "chat_wizard": false,
    "success": true
}