{
    "dcr_id": "1676059194630",
    "title": {
        "en": "Export certification requirements of fresh peppers from British Columbia to Japan",
        "fr": "Exigences de certification des exportations de poivrons frais de la Colombie-Britannique vers le Japon"
    },
    "html_modified": "14-2-2023",
    "modified": "13-2-2023",
    "issued": "10-2-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/exp_cert_reqs_of_peppers_to_japan_1676059194630_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/exp_cert_reqs_of_peppers_to_japan_1676059194630_fra"
    },
    "parent_ia_id": "1300383922268",
    "ia_id": "1676059194974",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Export certification requirements of fresh peppers from British Columbia to Japan",
        "fr": "Exigences de certification des exportations de poivrons frais de la Colombie-Britannique vers le Japon"
    },
    "label": {
        "en": "Export certification requirements of fresh peppers from British Columbia to Japan",
        "fr": "Exigences de certification des exportations de poivrons frais de la Colombie-Britannique vers le Japon"
    },
    "templatetype": "content page 1 column",
    "node_id": "1676059194974",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1300383870830",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/horticulture/exports/peppers-to-japan/",
        "fr": "/protection-des-vegetaux/horticulture/exportation/poivrons-vers-le-japon/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Export certification requirements of fresh peppers from British Columbia to Japan",
            "fr": "Exigences de certification des exportations de poivrons frais de la Colombie-Britannique vers le Japon"
        },
        "description": {
            "en": "To be eligible to export fresh peppers produced in British Columbia (BC) to Japan propagation facilities must register with the Canadian Food Inspection Agency (CFIA) by completing and signing a compliance agreement.",
            "fr": "Pour pouvoir exporter des pommes produites au Canada vers l'Union europ\u00e9enne (UE) et le Royaume-Uni, les producteurs de toutes les provinces doivent s'inscrire aupr\u00e8s de l'Agence canadienne d'inspection des aliments (ACIA) en remplissant et en signant une entente de conformit\u00e9."
        },
        "keywords": {
            "en": "Plant Health Act, Plant Health Regulations, plant protection, Export, certification, requirements, peppers, Japan",
            "fr": "Loi sur la protection des v\u00e9g\u00e9taux, R\u00e8glement sur la protection des v\u00e9g\u00e9taux, protection des v\u00e9g\u00e9taux, Exigences, certification, Poivrons, Japon"
        },
        "dcterms.subject": {
            "en": "environment,inspection,guidelines,plant diseases,pests",
            "fr": "environnement,inspection,lignes directrices,maladie des plantes,organisme nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-02-14",
            "fr": "2023-02-14"
        },
        "modified": {
            "en": "2023-02-13",
            "fr": "2023-02-13"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Export certification requirements of fresh peppers from British Columbia to Japan",
        "fr": "Exigences de certification des exportations de poivrons frais de la Colombie-Britannique vers le Japon"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n<ul>\n<li><a href=\"#a1\">Requirements for propagation facilities to export fresh peppers from British Columbia to Japan</a></li>\n<li><a href=\"#a2\">Requirements for production facilities to export fresh peppers from British Columbia to Japan</a></li>\n<li><a href=\"#a3\">Requirements for packing facilities to export fresh peppers from British Columbia to Japan</a></li>\n</ul>\n<h2 id=\"a1\">Requirements for propagation facilities to export fresh peppers from British Columbia to Japan</h2>\n<p>To be eligible to export fresh peppers produced in British Columbia (BC) to Japan propagation facilities must register with the Canadian Food Inspection Agency (CFIA) by completing and signing a compliance agreement. The agreement can be obtained by contacting your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a>.</p>\n<p>The compliance agreement will include but is not limited to the following requirements:</p>\n<h3>General requirements</h3>\n<ul>\n<li>The propagation facility must be located in the Greater Vancouver Regional District or the Fraser Valley Regional District.</li>\n<li>The facility must be inspected by the CFIA on an annual basis.</li>\n</ul>\n<h3>Pest monitoring</h3>\n<ul>\n<li>The facility must have a pest monitoring program in place for the pest of concern to Japan.</li>\n<li><i span lang=\"la\">Nicotiana</i> spp. plants must not be transported into or grown within the facility.</li>\n<li>The pepper plants must be monitored through out the entire crop cycle.</li>\n</ul>\n<h3>Records</h3>\n<ul>\n<li>Registered propagation facilities must complete and maintain monitoring records during the growing season.</li>\n<li>The records must be made available to CFIA upon request.</li>\n</ul>\n\n<h3 id=\"a2\">Requirements for production facilities to export fresh peppers from British Columbia to Japan</h3>\n\n<p>To be eligible to export fresh peppers produced in British Columbia (BC) to Japan production facilities must register with the Canadian Food Inspection Agency (CFIA) by completing and signing a compliance agreement. The agreement can be obtained by contacting your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a>.</p>\n\n<p>The compliance agreement will include but is not limited to the following requirements:</p>\n\n<h3>Product Source</h3>\n<ul>\n<li>Pepper plants must be sourced from CFIA-registered propagation facilities.</li>\n</ul>\n<h3>Facility requirements</h3>\n<ul>\n<li>The propagation facility must be located in the Greater Vancouver Regional District or the Fraser Valley Regional District.</li>\n<li>Product storage areas must be cleaned and disinfected.</li>\n<li>The facility must be inspected by the CFIA on an annual basis.\u00a0</li>\n</ul>\n<h3>Pest monitoring</h3>\n<ul>\n<li>The facility must have a pest monitoring program in place for the pest of concern to Japan.</li>\n<li><i span lang=\"la\">Nicotiana</i> spp. plants must not be transported into or grown within the facility.</li>\n<li>The pepper plants must be monitored through out the entire crop cycle.</li>\n<li>Hosts of the pest of concern to Japan that are present in the environment surrounding the facility must also be monitored.</li>\n</ul>\n<h3>Records</h3>\n<ul>\n<li>Copies of invoices for the purchase of the pepper plants must be presented to the CFIA on request and must be kept on file until the end of the crop cycle.</li>\n<li>Registered production facilities must complete and maintain monitoring records during the growing season.</li>\n<li>Cleaning and disinfection written procedure must be available for CFIA.</li>\n<li>The records must be made available to CFIA upon request.</li>\n</ul>\n\n<h2 id=\"a3\">Requirements for packing facilities to export fresh peppers from British Columbia to Japan</h2>\n<p>To be eligible to export fresh peppers produced in British Columbia (BC) to Japan the packing facilities must register with the Canadian Food Inspection Agency (CFIA) by completing and signing a compliance agreement. The agreement can be obtained by contacting your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local CFIA office</a>.</p>\n<p>The compliance agreement will include but is not limited to the following requirements:</p>\n<h3>Product source</h3>\n<ul>\n<li>Pepper fruit must be only sourced from CFIA approved production facilities.</li>\n</ul>\n<h3>Facility Requirements</h3>\n<ul>\n<li>The propagation facility must be located in the Greater Vancouver Regional District or the Fraser Valley Regional District.</li>\n<li>The facility must have written procedures for cleaning and disinfecting the packing house and storage areas.</li>\n<li>The facility must be inspected by the CFIA on an annual basis.\u00a0</li>\n</ul>\n<h3>Records</h3>\n<ul>\n<li>Copies of purchase invoices or delivery slips for pepper fruit must be presented to the CFIA on request and be kept on file until the end of the crop cycle.</li>\n<li>Records demonstrating that the packing house and storage areas are cleaned and disinfected must be kept.</li>\n<li>Records must be supplied to the CFIA upon request.</li>\n</ul>\n<h3>Sorting</h3>\n<ul>\n<li>Pepper fruit that is eligible for export to Japan must be segregated from other pepper fruit during packing and once the fruit has been packed. All packing materials must be new and \u00a0free from the pest of concern to Japan.</li>\n</ul>\n<h3>Labelling</h3>\n<ul>\n<li>Each shipping unit of peppers must be sealed with pallet straps and labelled with 'INSPECTED' and 'FOR JAPAN'.</li>\n</ul>\n<h3>Export certification</h3>\n<ul>\n<li>The consignment will not be eligible for export if pests of concern are found during the CFIA export inspection.</li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n<ul>\n<li><a href=\"#a1\">Exigences relatives aux \u00e9tablissements de propagation pour l'exportation de poivrons frais de la Colombie-Britannique vers le Japon</a></li>\n<li><a href=\"#a2\">Exigences pour les \u00e9tablissements de production pour l'exportation de poivrons frais de la Colombie-Britannique vers le Japon</a></li>\n<li><a href=\"#a3\">Exigences pour les \u00e9tablissements d'emballage pour l'exportation de poivrons frais de la Colombie-Britannique vers le Japon</a></li>\n</ul>\n<h2 id=\"a1\">Exigences relatives aux \u00e9tablissements de propagation pour l'exportation de poivrons frais de la Colombie-Britannique vers le Japon</h2>\n<p>Pour pouvoir exporter des poivrons frais produits en Colombie-Britannique (C.-B.) vers le Japon, les \u00e9tablissements de propagation doivent s'enregistrer aupr\u00e8s de l'Agence canadienne d'inspection des aliments (ACIA) en remplissant et en signant une entente de conformit\u00e9. L'entente peut \u00eatre obtenue en communiquant avec votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'ACIA</a>.</p>\n<p>L'entente de conformit\u00e9 comprendra, mais sans s'y limiter, les exigences suivantes\u00a0:</p>\n<h3>Exigences g\u00e9n\u00e9rales</h3>\n<ul>\n<li>L'\u00e9tablissement de propagation doit \u00eatre situ\u00e9 dans le district r\u00e9gional du Grand Vancouver ou le district r\u00e9gional de la vall\u00e9e de Fraser.</li> \n<li>L'\u00e9tablissement doit \u00eatre inspect\u00e9 par l'ACIA \u00e0 chaque ann\u00e9e.</li>\n</ul>\n<h3>Surveillance des ravageurs</h3>\n<ul>\n<li>L'\u00e9tablissement doit disposer d'un programme de surveillance des ravageurs en place pour les ravageurs pr\u00e9occupants pour le Japon.</li> \n<li>Les plants de <i span lang=\"la\">Nicotiana</i> spp. ne doivent pas \u00eatre transport\u00e9s ou cultiv\u00e9s dans l'\u00e9tablissement.</li> \n<li>Les plants de poivrons doivent \u00eatre surveill\u00e9s tout au long du cycle de culture.</li> \n\n</ul>\n<h3>Registres</h3>\n<ul>\n<li>Les \u00e9tablissements de propagation enregistr\u00e9s doivent remplir et tenir \u00e0 jour des registres de surveillance pendant la saison de croissance.</li> \n<li>Les registres doivent \u00eatre mis \u00e0 la disposition de l'ACIA sur demande.</li>\n</ul>\n\n<h2 id=\"a2\">Exigences pour les \u00e9tablissements de production pour exporter des poivrons frais de la Colombie-Britannique vers le Japon</h2>\n\n<p>Pour pouvoir exporter des poivrons frais produits en Colombie-Britannique (C.-B.) vers le Japon, les \u00e9tablissements de production doivent s'enregistrer aupr\u00e8s de l'Agence canadienne d'inspection des aliments (ACIA) en remplissant et en signant une entente de conformit\u00e9. L'entente peut \u00eatre obtenue en communiquant avec votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'ACIA</a>.</p>\n\n<p>L'entente de conformit\u00e9 comprendra, mais sans s'y limiter, les exigences suivantes\u00a0:</p>\n\n<h3>Origine du produit</h3>\n<ul>\n<li>Les plants de poivron doivent provenir d'\u00e9tablissements de propagation enregistr\u00e9s par l'ACIA.</li>\n</ul>\n<h3>Exigences de l'\u00e9tablissement</h3>\n<ul>\n<li>L'\u00e9tablissement de propagation doit \u00eatre situ\u00e9 dans le district r\u00e9gional du Grand Vancouver ou le district r\u00e9gional de la vall\u00e9e de Fraser.</li> \n<li>Les zones d'entreposage des produits doivent \u00eatre nettoy\u00e9es et d\u00e9sinfect\u00e9es.</li> \n<li>L'\u00e9tablissement doit \u00eatre inspect\u00e9 par l'ACIA \u00e0 chaque ann\u00e9e.</li> \n</ul>\n<h3>Surveillance des ravageurs</h3>\n<ul>\n<li>L'\u00e9tablissement doit disposer d'un programme de surveillance des ravageurs en place pour les ravageurs pr\u00e9occupants pour le Japon.</li> \n<li>Les plants de <i span lang=\"la\">Nicotiana</i> spp. ne doivent pas \u00eatre transport\u00e9s ou cultiv\u00e9s dans l'\u00e9tablissement.</li> \n<li>Les plants de poivrons doivent \u00eatre surveill\u00e9s tout au long du cycle de culture.</li> \n<li>Les h\u00f4tes de l'organisme nuisible pr\u00e9occupant pour le Japon qui sont pr\u00e9sents dans l'environnement entourant l'\u00e9tablissement doivent \u00e9galement \u00eatre surveill\u00e9s.</li>\n</ul>\n<h3>Registres</h3>\n<ul>\n<li>Des copies des factures d'achat des plants de poivrons doivent \u00eatre pr\u00e9sent\u00e9es \u00e0 l'ACIA sur demande et doivent \u00eatre conserv\u00e9es jusqu'\u00e0 la fin du cycle de culture.</li>\n<li>Les \u00e9tablissements de production enregistr\u00e9s doivent remplir et conserver des registres de surveillance pendant la saison de croissance.</li> \n<li>La proc\u00e9dure \u00e9crite de nettoyage et de d\u00e9sinfection doit \u00eatre disponible pour l'ACIA.</li> \n<li>Les registres doivent \u00eatre mis \u00e0 la disposition de l'ACIA sur demande.</li>\n</ul>\n\n<h2 id=\"a3\">Exigences pour les \u00e9tablissements d'emballage pour l'exportation de poivrons frais de la Colombie-Britannique vers le Japon</h2>\n<p>Pour pouvoir exporter des poivrons frais produits en Colombie-Britannique (C.-B.) vers le Japon, les \u00e9tablissement d'emballage doivent s'enregistrer aupr\u00e8s de l'Agence canadienne d'inspection des aliments (ACIA) en remplissant et en signant une entente de conformit\u00e9. L'entente peut \u00eatre obtenue en communiquant avec votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'ACIA</a>.</p>\n<p>L'entente de conformit\u00e9 comprendra, mais sans s'y limiter, les exigences suivantes\u00a0:</p>\n<h3>Origine du produit</h3>\n<ul>\n<li>Les poivrons doivent provenir uniquement d'\u00e9tablissements de production approuv\u00e9s par l'ACIA.</li>\n</ul>\n<h3>Exigences de l'\u00e9tablissement</h3>\n<ul>\n<li>L'\u00e9tablissement de propagation doit \u00eatre situ\u00e9 dans le district r\u00e9gional du Grand Vancouver ou le district r\u00e9gional de la vall\u00e9e de Fraser.</li> \n<li>L'\u00e9tablissement doit disposer de proc\u00e9dures \u00e9crites pour le nettoyage et la d\u00e9sinfection de la zone d'emballage et des zones d'entreposage.</li> \n<li>L'\u00e9tablissement doit \u00eatre inspect\u00e9 par l'ACIA \u00e0 chaque ann\u00e9e.</li>\n</ul>\n<h3>Registres</h3>\n<ul>\n<li>Des copies des factures d'achat ou des bons de livraison pour les poivrons doivent \u00eatre pr\u00e9sent\u00e9es \u00e0 l'ACIA sur demande et conserv\u00e9es jusqu'\u00e0 la fin du cycle de culture.</li> \n<li>Des registres d\u00e9montrant que la zone d'emballage et les zones d'entreposage sont nettoy\u00e9es et d\u00e9sinfect\u00e9es doivent \u00eatre conserv\u00e9s.</li> \n<li>Les registres doivent \u00eatre fournis \u00e0 l'ACIA sur demande.</li>\n</ul>\n<h3>Triage</h3>\n<ul>\n<li>Les poivrons \u00e9ligibles \u00e0 l'exportation vers le Japon doivent \u00eatre s\u00e9par\u00e9s des autres poivrons lors de l'emballage et une fois que les fruits ont \u00e9t\u00e9 emball\u00e9s. Tous les mat\u00e9riaux d'emballage doivent \u00eatre neufs et exempts de l'organisme nuisible pr\u00e9occupant pour le Japon.</li>\n</ul>\n<h3>\u00c9tiquetage</h3>\n<ul>\n<li>Chaque unit\u00e9 d'exp\u00e9dition de poivrons doit \u00eatre scell\u00e9e avec des sangles de palette et \u00e9tiquet\u00e9e \u00ab\u00a0INSPECT\u00c9\u00a0\u00bb et \u00ab\u00a0POUR LE JAPON\u00a0\u00bb.</li>\n</ul>\n<h3>Certification des exportations</h3>\n<ul>\n<li>L'envoi ne sera pas admissible \u00e0 l'exportation si des organismes nuisibles pr\u00e9occupants sont d\u00e9couverts lors de l'inspection \u00e0 l'exportation de l'ACIA.</li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}