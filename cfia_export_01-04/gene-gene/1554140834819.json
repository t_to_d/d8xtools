{
    "dcr_id": "1554140834819",
    "title": {
        "en": "Questions and answers: New measures to reduce salmonella in frozen raw breaded chicken products",
        "fr": "Questions et r\u00e9ponses\u00a0: Nouvelles mesures pour r\u00e9duire la salmonelle dans les produits de poulet"
    },
    "html_modified": "1-4-2019",
    "modified": "1-4-2019",
    "issued": "1-4-2019",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/faq_1554140834819_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/WORKAREA/food-aliments/templatedata/comn-comn/gene-gene/data/faq_1554140834819_fra"
    },
    "parent_ia_id": "1531254524999",
    "ia_id": "1554140994648",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Questions and answers: New measures to reduce salmonella in frozen raw breaded chicken products",
        "fr": "Questions et r\u00e9ponses\u00a0: Nouvelles mesures pour r\u00e9duire la salmonelle dans les produits de poulet"
    },
    "label": {
        "en": "Questions and answers: New measures to reduce salmonella in frozen raw breaded chicken products",
        "fr": "Questions et r\u00e9ponses\u00a0: Nouvelles mesures pour r\u00e9duire la salmonelle dans les produits de poulet"
    },
    "templatetype": "content page 1 column",
    "node_id": "1554140994648",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1531254524193",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/meat/salmonella-in-frozen-raw-breaded-chicken/faq/",
        "fr": "/controles-preventifs/produits-de-viande/salmonella-dans-les-produits-de-poulet-crus-panes-/faq/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Questions and answers: New measures to reduce salmonella in frozen raw breaded chicken products",
            "fr": "Questions et r\u00e9ponses\u00a0: Nouvelles mesures pour r\u00e9duire la salmonelle dans les produits de poulet"
        },
        "description": {
            "en": "These new measures were prompted by the continued link between frozen raw breaded chicken products and outbreaks of food-borne illness.",
            "fr": "Ces nouvelles mesures font suite au lien constamment soulign\u00e9 entre les \u00e9closions de maladies d'origine alimentaire et les produits de poulet cru pan\u00e9s et congel\u00e9s."
        },
        "keywords": {
            "en": "industry, manufacturing, processing, salmonella, frozen, raw, breaded, chicken, chicken products, retail sale, Questions and answers",
            "fr": "industrie, fabrication, transformation, salmonelles, surgel&#233;s, crus, pan&#233;s, poulet, produits de poulet, vente au d&#233;tail, Questions et r\u00e9ponses"
        },
        "dcterms.subject": {
            "en": "inspection,food inspection,regulations,food safety,meat,poultry",
            "fr": "inspection,inspection des aliments,r\u00e9glementations,salubrit\u00e9 des aliments,viande,volaille"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-04-01",
            "fr": "2019-04-01"
        },
        "modified": {
            "en": "2019-04-01",
            "fr": "2019-04-01"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Questions and answers: New measures to reduce salmonella in frozen raw breaded chicken products",
        "fr": "Questions et r\u00e9ponses\u00a0: Nouvelles mesures pour r\u00e9duire la salmonelle dans les produits de poulet"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Why is the Canadian Food Inspection Agency requiring these new measures?</h2>\n<p>These new measures were prompted by the <a href=\"/preventive-controls/meat/salmonella-in-frozen-raw-breaded-chicken/backgrounder/eng/1554141522231/1554141574415\">continued link between frozen raw breaded chicken products and outbreaks of food-borne illness</a>. These new measures will reduce the risks associated with these products, but not eliminate the risk completely. The best way to prevent illnesses is to <a href=\"https://www.canada.ca/en/health-canada/services/meat-poultry-fish-seafood-safety/frozen-breaded-chicken.html\">store, prepare and cook</a> all poultry products properly. This includes following package directions and cooking chicken fully to kill bacteria like\u00a0<a href=\"https://www.canada.ca/en/public-health/services/diseases/salmonellosis-salmonella.html\"><i lang=\"la\">Salmonella</i></a>.</p>\n<h2>What options does the industry have to meet the requirements?</h2>\n<p>Manufacturers of frozen raw breaded chicken products must implement one of the following control measure options by April 1, 2019:</p>\n<ul>\n<li>implement a validated cooking process to reduce <i lang=\"la\">Salmonella</i>; or</li>\n<li>implement a testing program for the raw chicken mixture to demonstrate it has no detectable <i lang=\"la\">Salmonella</i>; or</li>\n<li>implement a testing program for finished frozen raw breaded chicken products to demonstrate they have no detectable <i lang=\"la\">Salmonella; or</i></li>\n<li>include a validated process or combination of processes to reduce <i lang=\"la\">Salmonella</i> and implement a <i lang=\"la\">Salmonella</i> sampling program for the raw chicken mixture.</li>\n</ul>\n<h2>Will these requirements be put in place for all poultry products across Canada?</h2>\n<p>The new requirements address the risks associated with frozen raw breaded chicken products (such as chicken nuggets, chicken strips, chicken burgers, popcorn chicken and chicken fries) because they appear ready-to-eat.</p>\n<p>These new measures will reduce the risks associated with these products, but not eliminate the risk completely. The best way to prevent illnesses is to <a href=\"https://www.canada.ca/en/health-canada/services/meat-poultry-fish-seafood-safety/frozen-breaded-chicken.html\">store, prepare and cook</a> all poultry products properly. This includes following package directions and cooking chicken fully to kill bacteria like\u00a0<a href=\"https://www.canada.ca/en/public-health/services/diseases/salmonellosis-salmonella.html\"><i lang=\"la\">Salmonella</i></a>.</p>\n<h2>Why are only certain products affected by the new requirements?</h2>\n<p>The new measures focus on products that have the greatest potential to make Canadians sick when they are consumed undercooked. This includes products that are made from ground, chopped or formed chicken meat and products that look like they are already cooked (par-fried). Breaded products that are made from whole pieces of chicken (such as chicken breasts) do not have the same high risk.</p>\n<h2>How long did the Canadian Food Inspection Agency give industry to make the changes?</h2>\n<p>The Canadian Food Inspection Agency provided industry with a 12 month implementation period. Facilities that manufacture these products must implement control measures by April 1, 2019.</p>\n<h2>Will recalls still happen once the measures are implemented?</h2>\n<p>If products are found to be contaminated with <i lang=\"la\">Salmonella</i>, they will be recalled and removed from shelves. Frozen raw breaded chicken products may remain in the marketplace or in freezers for up to two years. </p>\n<h2>Will the Canadian Food Inspection Agency monitor industry compliance?</h2>\n<p>The Canadian Food Inspection Agency has an active monitoring program which inspectors use to verify that industry is complying with the new requirements.</p>\n<h2>What are the labelling requirements for frozen raw breaded chicken products?</h2>\n<p>In 2004, mandatory labelling measures were implemented requiring descriptors such as \"must be cooked\", \"uncooked\" or \"raw\" be included near the common name. Comprehensive cooking instructions were also added on the outer-packaging.</p>\n<p>In 2015, the Government of Canada worked with industry to develop additional voluntary labelling, which included more prominent and consistent labels and messaging, as well as explicit instructions not to microwave the product. Industry also introduced cooking instructions on the inner-packaging.</p>\n<h2>What efforts has the Government of Canada made to protect the public from the risk of <i lang=\"la\">Salmonella</i> from frozen breaded chicken products?</h2>\n<p>The Canadian Food Inspection Agency, Health Canada, and the Public Health Agency of Canada work together to reduce the risk of illness associated with all frozen raw breaded chicken products packaged for retail sale. This includes an extensive communications campaign to reach as many Canadians as possible, through social media ads and posts, an overarching public health notice, a joint statement by the Council of Chief Medical Officers of Health (CCMOH), outreach to media and partner engagement, in addition to the public notices related to specific outbreaks and recalls.</p>\n<h2>What is the Government of Canada doing to protect Canadians with regards to products produced before April 1, 2019?</h2>\n<p>The new measures to reduce <i lang=\"la\">Salmonella</i> in frozen raw breaded chicken products must be implemented for products produced as of April 1, 2019. This means that frozen raw breaded chicken products may remain in the marketplace or in consumers' freezers for up to two years.</p>\n<p>The best way to prevent illnesses is to <a href=\"https://www.canada.ca/en/health-canada/services/meat-poultry-fish-seafood-safety/frozen-breaded-chicken.html\">store, prepare and cook</a> all poultry products properly. This includes following package directions and cooking chicken to kill bacteria like\u00a0<a href=\"https://www.canada.ca/en/public-health/services/diseases/salmonellosis-salmonella.html\"><i lang=\"la\">Salmonella</i></a>.</p>\n<h2>What can I do to make sure the products I eat are safe?</h2>\n<p>Even with the new measures in place, it is important to store, prepare and cook all breaded chicken products properly to prevent illness. You should always follow package directions and cook your chicken properly to kill bacteria like <a href=\"https://www.canada.ca/en/public-health/services/diseases/salmonellosis-salmonella.html\"><i lang=\"la\">Salmonella</i></a>.</p>\n<h2>What does \"below detectable levels of salmonella\" mean?</h2>\n<p>It is the lowest level of salmonella that our current technology allows us to detect.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Pourquoi l'Agence canadienne d'inspection des aliments exige-t-elle ces nouvelles mesures?</h2>\n<p>Ces nouvelles mesures font suite au <a href=\"/controles-preventifs/produits-de-viande/salmonella-dans-les-produits-de-poulet-crus-panes-/renseignements/fra/1554141522231/1554141574415\">lien constamment soulign\u00e9 entre les \u00e9closions de maladies d'origine alimentaire et les produits de poulet cru pan\u00e9s et congel\u00e9s</a>. Ces mesures r\u00e9duiront le risque associ\u00e9 avec ces produits, mais ne l'\u00e9limineront pas compl\u00e8tement. La meilleure fa\u00e7on de pr\u00e9venir le risque de maladie est <a href=\"https://www.canada.ca/fr/sante-canada/services/salubrite-viandes-volailles-poissons-et-fruits-mer/poulet-pane-surgele.html\">d'entreposer, de pr\u00e9parer ou de cuire</a> correctement tous les produits de volaille pan\u00e9s ad\u00e9quatement. Ceci comprend suivre les directives de cuisson sur l'emballage et cuire ad\u00e9quatement le poulet afin d'\u00e9liminer les bact\u00e9ries comme la <a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/salmonellose-salmonella.html\">salmonelle</a>.</p>\n<h2>Quelles sont les options de l'industrie pour r\u00e9pondre aux exigences?</h2>\n<p>Les transformateurs de produits de poulet cru pan\u00e9s et congel\u00e9s doivent mettre en \u0153uvre une des mesures de contr\u00f4le suivante en date du 1<sup>er</sup> avril 2019\u00a0:</p>\n<ul>\n<li>inclure dans la fabrication un proc\u00e9d\u00e9 de cuisson valid\u00e9 pour r\u00e9duire la salmonelle; ou</li>\n<li>mettre en \u0153uvre un programme d'\u00e9chantillonnage de <i lang=\"la\">Salmonella </i>pour le m\u00e9lange de poulet cru afin de d\u00e9montrer que la bact\u00e9rie n'a pas \u00e9t\u00e9 d\u00e9tect\u00e9e; ou </li>\n<li>mettre en \u0153uvre un programme d'analyse visant les produits finis de poulet crus pan\u00e9s et congel\u00e9s pour d\u00e9montrer qu'ils ne contiennent pas de <i lang=\"la\">Salmonella</i> d\u00e9tectable; ou</li>\n<li>inclure dans la fabrication des produits un proc\u00e9d\u00e9 ou une combinaison de proc\u00e9d\u00e9s qui ont \u00e9t\u00e9 valid\u00e9s pour r\u00e9duire la <i lang=\"la\">Salmonella</i> et mettre en \u0153uvre un programme d'\u00e9chantillonnage visant <i lang=\"la\">Salmonella</i> pour le m\u00e9lange de poulet cru.</li>\n</ul>\n<h2>Ces exigences visent-ils tous les produits de volaille partout au Canada?</h2>\n<p>Cette approche aborde d'abord les risques associ\u00e9s aux produits de poulet pan\u00e9s crus et congel\u00e9s (comme les p\u00e9pites de poulet, les lani\u00e8res de poulet, les hamburgers de poulet, les frites de poulet et le poulet pop-corn) puisque ces produits semblent pr\u00eats \u00e0 \u00eatre consomm\u00e9s.</p>\n<p>Ces mesures \u00a0r\u00e9duiront le risque associ\u00e9 avec ces produits, mais ne l'\u00e9limineront pas compl\u00e8tement. La meilleure fa\u00e7on de pr\u00e9venir le risque de maladie est <a href=\"https://www.canada.ca/fr/sante-canada/services/salubrite-viandes-volailles-poissons-et-fruits-mer/poulet-pane-surgele.html\">d'entreposer, de pr\u00e9parer ou de cuire</a> correctement tous les produits de volaille pan\u00e9s ad\u00e9quatement. Ceci comprend suivre les directives de cuisson sur l'emballage et cuire ad\u00e9quatement le poulet afin d'\u00e9liminer les bact\u00e9ries comme la <a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/salmonellose-salmonella.html\">salmonelle</a>.</p>\n<h2>Pourquoi les nouvelles mesures ne visent que certains produits?</h2>\n<p>Les nouvelles mesures sont centr\u00e9es sur les produits \u00e9tant les plus susceptibles de rendre les Canadiens malades s'ils ne sont pas bien cuit avant d'\u00eatre consomm\u00e9s. Ceci comprend les produits qui sont fait de poulet hach\u00e9, broy\u00e9 ou form\u00e9 et qui semble \u00eatre d\u00e9j\u00e0 cuit (partiellement frits). Les produits pan\u00e9s qui sont pr\u00e9par\u00e9s \u00e0 partir de morceaux de poulet entiers (comme les poitrines de poulet) n'ont pas le m\u00eame niveau de risque.</p>\n<h2>Combien de temps L'Agence canadienne d'inspection des aliments a-t-elle accord\u00e9 \u00e0 l'industrie pour apporter ces changements?</h2>\n<p>L'Agence canadienne d'inspection des aliments a accord\u00e9 \u00e0 l'industrie un d\u00e9lai de 12\u00a0mois pour mettre en \u0153uvre ces changements. Les transformateurs produisant ce type de produit doivent revoir leur processus et mettre en \u0153uvre des mesures de contr\u00f4le d'ici le 1er avril 2019.</p>\n<h2>Est-ce que des rappels se produiront une fois que les mesures sont mises en \u0153uvre?</h2>\n<p>Si des produits sont contamin\u00e9s par <i lang=\"la\">Salmonella</i>, ils seront rappel\u00e9s et retir\u00e9s des tablettes. On estime qu'il pourrait rester des produits de poulet cru pan\u00e9s et congel\u00e9s sur le march\u00e9 et dans les cong\u00e9lateurs jusqu'\u00e0 deux ans.</p>\n<h2>L'Agence canadienne d'inspection des aliments mettra-t-elle en \u0153uvre un programme surveillance active pour \u00e9valuer la conformit\u00e9 de l'industrie?</h2>\n<p>L'Agence canadienne d'inspection des aliments a en \u0153uvre un programme surveillance active utilis\u00e9 par les inspecteurs pour \u00e9valuer la conformit\u00e9 de l'industrie aux nouvelles exigences.</p>\n<h2>Quelles sont les exigences en mati\u00e8re d'\u00e9tiquetage des produits de poulet crus pan\u00e9s et congel\u00e9s?</h2>\n<p>En 2004, des mesures d'\u00e9tiquetage obligatoire ont \u00e9t\u00e9 mises en \u0153uvre pour exiger que les mentions telles que \u00ab\u00a0doit \u00eatre cuit\u00a0\u00bb, \u00ab\u00a0non cuit\u00a0\u00bb ou \u00ab\u00a0produit cru\u00a0\u00bb se trouvent pr\u00e8s du nom usuel sur les \u00e9tiquettes. Des instructions de cuisson d\u00e9taill\u00e9es ont aussi \u00e9t\u00e9 ajout\u00e9es sur l'emballage ext\u00e9rieur.</p>\n<p>En 2015, le Portefeuille a travaill\u00e9 avec l'industrie \u00e0 l'\u00e9laboration d'autres \u00e9tiquettes volontaires, qui \u00e9taient plus visibles et uniformes, ainsi que d'instructions claires indiquant de ne pas cuire les produits au four \u00e0 micro-ondes. L'industrie a \u00e9galement ajout\u00e9 des instructions de cuisson sur l'emballage int\u00e9rieur.</p>\n<h2>Qu'a fait le gouvernement du Canada pour prot\u00e9ger le public des risques de la salmonelle dans les produits de poulet cru pan\u00e9s et congel\u00e9s?</h2>\n<p>L'Agence canadienne d'inspection des aliments, Sant\u00e9 Canada et l'Agence de la sant\u00e9 publique du Canada ont travaill\u00e9 ensemble pour r\u00e9duire le risque de maladie associ\u00e9e avec les produits de poulet cru pan\u00e9s et congel\u00e9s qui sont emball\u00e9s pour la vente au d\u00e9tail. Ceci comprend une campagne de communication publique importante pour informer le plus de Canadiens possible, par le biais de publicit\u00e9s et de messages dans les m\u00e9dias sociaux, un Avis de sant\u00e9 publique majeur, une d\u00e9claration conjointe du Conseil des m\u00e9decins hygi\u00e9nistes en chef ainsi que de la sensibilisation aupr\u00e8s des m\u00e9dias et des partenaires, en plus des avis au public continus li\u00e9s aux \u00e9closions et aux rappels.</p>\n<h2>Que fait le gouvernement du Canada pour prot\u00e9ger les Canadiens en ce qui a trait aux produits pr\u00e9par\u00e9s avant le 1<sup>er</sup> avril 2019?</h2>\n<p>Ces nouvelles mesures de contr\u00f4le pour r\u00e9duire la quantit\u00e9 de <i lang=\"la\">Salmonella</i> dans les produits de poulet cru pan\u00e9s et congel\u00e9s doivent \u00eatre mises en \u0153uvre pour les produits pr\u00e9par\u00e9s en date du 1er avril 2019. Ceci veut dire qu'il pourrait rester des produits de poulet cru pan\u00e9s et congel\u00e9s sur le march\u00e9 et dans les cong\u00e9lateurs jusqu'\u00e0 deux ans.</p>\n<p>La meilleure fa\u00e7on de pr\u00e9venir le risque de maladie est <a href=\"https://www.canada.ca/fr/sante-canada/services/salubrite-viandes-volailles-poissons-et-fruits-mer/poulet-pane-surgele.html\">d'entreposer, de pr\u00e9parer ou de cuire</a> correctement tous les produits de volaille pan\u00e9s ad\u00e9quatement. Ceci comprend suivre les directives de cuisson sur l'emballage et cuire ad\u00e9quatement le poulet afin d'\u00e9liminer les bact\u00e9ries comme la <a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/salmonellose-salmonella.html\">salmonelle</a>. </p>\n<h2>Que puis-je faire pour m'assurer que les produits que je mange sont salubres?</h2>\n<p>Bien que ces mesures sont maintenant en place, nous tenons \u00e0 rappeler aux Canadiens que la meilleure fa\u00e7on de pr\u00e9venir le risque de maladie est <a href=\"https://www.canada.ca/fr/sante-canada/services/salubrite-viandes-volailles-poissons-et-fruits-mer/poulet-pane-surgele.html\">d'entreposer, de pr\u00e9parer ou de cuire</a> correctement tous les produits de volaille pan\u00e9s ad\u00e9quatement. Vous devriez toujours suivre les directives de cuisson sur l'emballage et cuire ad\u00e9quatement le poulet afin d'\u00e9liminer les bact\u00e9ries comme la <a href=\"https://www.canada.ca/fr/sante-publique/services/maladies/salmonellose-salmonella.html\">salmonelle.</a></p>\n<h2>Qu'entend-on par l'expression \u00ab\u00a0quantit\u00e9 de Salmonella inf\u00e9rieure aux quantit\u00e9s d\u00e9celables\u00a0\u00bb?</h2>\n<p>Il s'agit de la quantit\u00e9 de bact\u00e9ries Salmonella que notre technologie actuelle nous permet de d\u00e9tecter.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}