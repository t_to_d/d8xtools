{
    "dcr_id": "1475881428791",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Carduus acanthoides</i> (Spiny plumeless thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Carduus acanthoides</i> (Chardon \u00e9pineux)"
    },
    "html_modified": "2024-01-26 2:23:25 PM",
    "modified": "2017-06-28",
    "issued": "2017-07-14",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_carduus_acanthoides_1475881428791_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_carduus_acanthoides_1475881428791_fra"
    },
    "ia_id": "1475881429283",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Carduus acanthoides</i> (Spiny plumeless thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Carduus acanthoides</i> (Chardon \u00e9pineux)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Carduus acanthoides</i> (Spiny plumeless thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Carduus acanthoides</i> (Chardon \u00e9pineux)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1475881429283",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/carduus-acanthoides/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/carduus-acanthoides/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Carduus acanthoides (Spiny plumeless thistle)",
            "fr": "Semence de mauvaises herbe : Carduus acanthoides (Chardon \u00e9pineux)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Carduus acanthoides, Asteraceae, Spiny plumeless thistle",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Carduus acanthoides, Asteraceae, Chardon \u00e9pineux"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-07-14",
            "fr": "2017-07-14"
        },
        "modified": {
            "en": "2017-06-28",
            "fr": "2017-06-28"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Carduus acanthoides (Spiny plumeless thistle)",
        "fr": "Semence de mauvaises herbe : Carduus acanthoides (Chardon \u00e9pineux)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div id=\"remove-weed-seed\"></div>\n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Common Name</h2>\n<p>Spiny plumeless thistle</p>\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Nova Scotia\">NS</abbr>, <abbr title=\"Ontario\">ON</abbr>, and <abbr title=\"Quebec\">QC</abbr> and ephemeral in <abbr title=\"New Brunswick\">NB</abbr> and <abbr title=\"Newfoundland and Labrador\">NL</abbr> (Brouillet <abbr title=\"and others\">et al.</abbr> 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, temperate Asia, and most of Europe. Introduced to North America, Argentina, Uruguay, New Zealand, and beyond its native range in Europe (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Occurs across the continental United States, with the exception of some southwestern and southeastern states (Kartesz 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Annual or biennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Achene length: 2.5 - 3.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width : 1.5 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene is elongate, tapering at the base, compressed laterally; may have a small hump to one side of the collar</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Achene is smooth, with longitudinal lines and transverse wrinkles on the surface</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Achene is cream-coloured to light brown, often has a pink tint</li>\n<li>The collar at the top of the achene is often yellow</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>The style peg at the top of achene is usually short and thick.</li>\n<li>Immature achenes can have a filamentous white pappus <span class=\"nowrap\">1.3 - 2.0 <abbr title=\"centimeters\">cm</abbr></span> long.</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Cultivated fields, pastures, rangelands, roadsides and disturbed areas (Desrochers <abbr title=\"and others\">et al.</abbr> 1988<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, Darbyshire 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Spiny plumeless thistle was first recorded on the east coast of North America in the late 19th century, possibly arriving in ships' ballast (Desrochers <abbr title=\"and others\">et al.</abbr> 1988<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>). It may form dense stands on disturbed sites and invade native and restored grasslands (Ministry of Agriculture, Food, and Fisheries 2002<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Footnote\u00a0</span>6</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Nodding thistle (<i lang=\"la\">Carduus nutans</i>)</h3>\n<ul>\n<li>Nodding thistle achenes are a similar size and brown colour, elongate shape with a thick top peg, and longitudinal and transverse lines on the surface.</li>\n<li>Nodding thistle achenes have a yellow base and the achene surface is covered with a smooth, varnish-like coating.</li>\n<li>Immature nodding thistle achenes may have a wrinkled surface similar to spiny plumeless thistle, but they will have a yellow base.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_03cnsh_1475592513591_eng.jpg\" alt=\"Figure 1 - Spiny plumeless thistle (Carduus acanthoides) achenes\" class=\"img-responsive\">\n<figcaption>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) achenes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_01cnsh_1475592486335_eng.jpg\" alt=\"Figure 2 - Spiny plumeless thistle (Carduus acanthoides) achene\" class=\"img-responsive\">\n<figcaption>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_04cnsh_1475592537358_eng.jpg\" alt=\"Figure 3 - Spiny plumeless thistle (Carduus acanthoides) achenes\" class=\"img-responsive\">\n<figcaption>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) achenes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_06cnsh_1475592586932_eng.jpg\" alt=\"Figure 4 - Spiny plumeless thistle (Carduus acanthoides) top of achene\" class=\"img-responsive\">\n<figcaption>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) top of achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_07cnsh_1475592738665_eng.jpg\" alt=\"Figure 5 - Spiny plumeless thistle (Carduus acanthoides) bottom of achene\" class=\"img-responsive\">\n<figcaption>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) bottom of achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_2_copyright_1475592853024_eng.jpg\" alt=\"Figure 6 - Spiny plumeless thistle (Carduus acanthoides) achene\" class=\"img-responsive\">\n<figcaption>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) achene\n</figcaption>\n</figure>\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_05cnsh_1475592949337_eng.jpg\" alt=\"Figure 7 - Similar species: Nodding thistle (Carduus nutans) achenes\" class=\"img-responsive\">\n<figcaption>Similar species: Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achenes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_02cnsh_1475592825972_eng.jpg\" alt=\"Figure 8 - Similar species: Nodding thistle (Carduus nutans) achene\" class=\"img-responsive\">\n<figcaption>Similar species: Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achene</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, <abbr title=\"North Carolina\">N.C.</abbr>, www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Desrochers, A. M., Bain, J. F. and Warwick, S. I. 1988</strong>. The Biology of Canadian weeds. 89. <i lang=\"la\">Carduus</i> <i lang=\"la\">nutans</i> L. and <i lang=\"la\">Carduus acanthoides</i> L. Canadian Journal of Plant Science 68: 1053-1068.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 6</dt>\n<dd id=\"fn6\">\n<p><strong>Ministry of Agriculture, Food, and Fisheries. 2002</strong>. Guide to the Weeds in British Columbia, https://www.for.gov.bc.ca/hra/Plants/weedsbc/GuidetoWeeds.pdf [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>6<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div id=\"remove-weed-seed\"></div>\n\n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n<h2>Nom commun</h2>\n<p>Chardon \u00e9pineux</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada\u00a0:</strong> L'esp\u00e8ce est pr\u00e9sente en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr> et au <abbr title=\"Qu\u00e9bec\">Qc</abbr>, et est \u00e9ph\u00e9m\u00e8re au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr> et \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr> (Brouillet <abbr title=\"et autres\">et al.</abbr>, 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale\u00a0:</strong> Indig\u00e8ne d'Afrique du Nord, d'Asie temp\u00e9r\u00e9e et de la majeure partie de l'Europe. Introduite en Am\u00e9rique du Nord, en Argentine, en Uruguay, en Nouvelle-Z\u00e9lande et \u00e0 l'ext\u00e9rieur de son aire d'indig\u00e9nat en Europe (<abbr title=\"United States Department of Agriculture\" lang=\"en\">USDA</abbr>-<abbr title=\"Agricultural Research Service\" lang=\"en\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Pr\u00e9sente dans l'ensemble de la r\u00e9gion continentale des \u00c9tats-Unis, sauf dans certains \u00c9tats du sud-ouest et du sud-est (Kartesz, 2011<sup id=\"fn3-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Annuelle ou bisannuelle</p>\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne</p>\n<h2>Caract\u00e9ristiques d'identification</h2>\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'ak\u00e8ne\u00a0: <span class=\"nowrap\">2,5 - 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr></span></li>\n<li>Largeur de l'ak\u00e8ne\u00a0: <span class=\"nowrap\">1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></span></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne allong\u00e9, s'att\u00e9nuant \u00e0 la base, comprim\u00e9 lat\u00e9ralement, pr\u00e9sentant parfois une petite bosse sur le c\u00f4t\u00e9 du collet</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Surface de l'ak\u00e8ne lisse, avec des lignes longitudinales et des rides transversales</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne cr\u00e8me \u00e0 brun clair, souvent teint\u00e9 de rose</li>\n<li>Collet du sommet de l'ak\u00e8ne souvent jaune</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>La base stylaire, au sommet de l'ak\u00e8ne, est g\u00e9n\u00e9ralement courte et \u00e9paisse.</li>\n<li>Les ak\u00e8nes immatures pr\u00e9sentent parfois un pappus filamenteux blanc de 1,3 \u00e0 <span class=\"nowrap\">2,0 <abbr title=\"centim\u00e8tres\">cm</abbr></span> de longueur.</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs cultiv\u00e9s, p\u00e2turages, terres de parcours, bords de chemin et terrains perturb\u00e9s (Desrochers <abbr title=\"et autres\">et al.</abbr>, 1988<sup id=\"fn4a-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>; Darbyshire, 2003<sup id=\"fn5-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>). </p>\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le chardon \u00e9pineux a \u00e9t\u00e9 signal\u00e9 pour la premi\u00e8re fois sur la c\u00f4te est de l'Am\u00e9rique du Nord \u00e0 la fin du 19e si\u00e8cle et pourrait y avoir \u00e9t\u00e9 apport\u00e9 dans les eaux de ballast d'un navire (Desrochers <abbr title=\"et autres\">et al.</abbr>, 1988<sup id=\"fn4b-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>). Il peut former de denses peuplements dans les sites perturb\u00e9s et envahir les prairies indig\u00e8nes et restaur\u00e9es (Ministry of Agriculture, Food, and Fisheries, 2002<sup id=\"fn6-rf\"><a class=\"fn-lnk\" href=\"#fn6\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>6</a></sup>).</p>\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>)</h3>\n<ul>\n<li>Les ak\u00e8nes du chardon  pench\u00e9 ressemblent \u00e0 ceux du chardon \u00e9pineux par leurs dimensions, leur couleur  brune, leur forme allong\u00e9e, leur base stylaire \u00e9paisse et leur surface  pr\u00e9sentant des lignes longitudinales et transversales.</li>\n<li>Les ak\u00e8nes du chardon  pench\u00e9 sont jaune \u00e0 la base et leur surface est recouverte d'une couche  luisante qui ressemble \u00e0 du vernis.</li>\n<li>Les ak\u00e8nes immatures du  chardon pench\u00e9 peuvent pr\u00e9senter une surface rid\u00e9e semblable \u00e0 celle des ak\u00e8nes  du chardon \u00e9pineux, mais leur base est jaune.</li>\n</ul>\n<h2>Photos</h2>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_03cnsh_1475592513591_fra.jpg\" alt=\"Figure 1 - Chardon \u00e9pineux (Carduus acanthoides) ak\u00e8nes\" class=\"img-responsive\">\n<figcaption>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) ak\u00e8nes </figcaption>\n</figure>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_01cnsh_1475592486335_fra.jpg\" alt=\"Figure 2 - Chardon \u00e9pineux (Carduus acanthoides) ak\u00e8ne\" class=\"img-responsive\">\n<figcaption>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) ak\u00e8ne </figcaption>\n</figure>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_04cnsh_1475592537358_fra.jpg\" alt=\"Figure 3 - Chardon \u00e9pineux (Carduus acanthoides) ak\u00e8nes\" class=\"img-responsive\">\n<figcaption>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) ak\u00e8nes </figcaption>\n</figure>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_06cnsh_1475592586932_fra.jpg\" alt=\"Figure 4 - Chardon \u00e9pineux (Carduus acanthoides) sommet de l'ak\u00e8ne\" class=\"img-responsive\">\n<figcaption>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) sommet de l'ak\u00e8ne </figcaption>\n</figure>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_07cnsh_1475592738665_fra.jpg\" alt=\"Figure 4 - Chardon \u00e9pineux (Carduus acanthoides) sommet de l'ak\u00e8ne\" class=\"img-responsive\">\n<figcaption>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) base de l'ak\u00e8ne </figcaption>\n</figure>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_2_copyright_1475592853024_fra.jpg\" alt=\"Figure 5 - Chardon \u00e9pineux (Carduus acanthoides) ak\u00e8ne\" class=\"img-responsive\">\n<figcaption>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) ak\u00e8ne </figcaption>\n</figure>\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_05cnsh_1475592949337_fra.jpg\" alt=\"Figure 6 - Esp\u00e8ce semblable\u00a0: Chardon pench\u00e9 (Carduus nutans) ak\u00e8nes\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) ak\u00e8nes</figcaption>\n</figure>\n<figure class=\"mrgn-bttm-md\"> <img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_02cnsh_1475592825972_fra.jpg\" alt=\"Figure 7 - Esp\u00e8ce semblable\u00a0: Chardon pench\u00e9 (Carduus nutans) ak\u00e8ne\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable\u00a0: Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) ak\u00e8ne</figcaption>\n</figure>\n<aside class=\"wb-fnote\" role=\"note\">\n<h2 id=\"fn\">Notes de bas de page</h2>\n<dl>\n<dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p>\n</dd>\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p>\n</dd>\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Kartesz, J. T. 2011</strong>. The Biota of North America Program (BONAP). North American Plant Atlas. Chapel Hill, <abbr title=\"North Carolina\">N.C.</abbr>, www.bonap.org/MapSwitchboard.html [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p>\n</dd>\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Desrochers, A. M., Bain, J. F. and Warwick, S. I. 1988</strong>. The Biology of Canadian weeds. 89. <i lang=\"la\">Carduus</i> <i lang=\"la\">nutans</i> L. and <i lang=\"la\">Carduus acanthoides</i> L. Canadian Journal of Plant Science 68: 1053-1068.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p>\n</dd>\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p>\n</dd>\n<dt>Note de bas de page 6</dt>\n<dd id=\"fn6\">\n<p lang=\"en\"><strong>Ministry of Agriculture, Food, and Fisheries. 2002</strong>. Guide to the Weeds in British Columbia, https://www.for.gov.bc.ca/hra/Plants/weedsbc/GuidetoWeeds.pdf [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn6-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>6</a></p>\n</dd>\n</dl>\n</aside>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}