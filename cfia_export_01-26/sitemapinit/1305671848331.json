{
    "dcr_id": "1305670991321",
    "title": {
        "en": "Immediately notifiable diseases",
        "fr": "Maladies \u00e0 notification imm\u00e9diate"
    },
    "html_modified": "2024-01-26 2:20:27 PM",
    "modified": "2021-05-12",
    "issued": "2011-05-17 18:23:13",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_immed_notifiable_1305670991321_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/dis_immed_notifiable_1305670991321_fra"
    },
    "ia_id": "1305671848331",
    "parent_ia_id": "1300388449143",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Diseases",
        "fr": "Maladies"
    },
    "commodity": {
        "en": "Terrestrial Animal Health",
        "fr": "Sant\u00e9 des animaux terrestres"
    },
    "program": {
        "en": "Animals",
        "fr": "Animaux"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1300388449143",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Immediately notifiable diseases",
        "fr": "Maladies \u00e0 notification imm\u00e9diate"
    },
    "label": {
        "en": "Immediately notifiable diseases",
        "fr": "Maladies \u00e0 notification imm\u00e9diate"
    },
    "templatetype": "content page 1 column",
    "node_id": "1305671848331",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300388388234",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/immediately-notifiable/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Immediately notifiable diseases",
            "fr": "Maladies \u00e0 notification imm\u00e9diate"
        },
        "description": {
            "en": "Immediately notifiable diseases are diseases exotic to Canada for which there are no control or eradication programs.",
            "fr": "Maladies \u00e0 notification imm\u00e9diate sont des maladies exotiques au Canada pour lesquelles il n'existe aucun programme de lutte ni d'\u00e9radication."
        },
        "keywords": {
            "en": "Health of Animals Act, Health of Animals Regulations, immediately notifiable diseases, compensation, surveillance",
            "fr": "Loi sur la sant\u00e9 des animaux, R\u00e8glement sur la sant\u00e9 des animaux, maladies \u00e0 notification imm\u00e9diate, indemnisation, surveillance"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-05-17 18:23:13",
            "fr": "2011-05-17 18:23:13"
        },
        "modified": {
            "en": "2021-05-12",
            "fr": "2021-05-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Immediately notifiable diseases",
        "fr": "Maladies \u00e0 notification imm\u00e9diate"
    },
    "body": {
        "en": "        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section><p>In general, immediately notifiable diseases are diseases exotic to Canada for which there are no control or eradication programs.</p>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> can undertake control measures for such diseases when notified of their presence in Canada. This category also includes some rare indigenous diseases. A herd or flock of origin must be certified as being free from these diseases in order to meet import requirements of trading partners.</p>\n<p>Only laboratories are required to contact the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> regarding the suspicion or diagnosis of one of these diseases. Information must be forwarded by e-mail to the Epidemiology and Surveillance Section.<br> \n<a href=\"mailto:cfia.notification-notification.acia@inspection.gc.ca\">cfia.notification-notification.acia@inspection.gc.ca</a></p>\n<p>The notification must include the following information:</p>\n<ol class=\"lst-lwr-alph\">\n<li>the name, address and telephone number of the person who owns or has the possession, care or control of the animal;</li>\n<li>the location of the animal; and</li>\n<li>all other information that the laboratory has in relation to the animal.</li>\n</ol>\n<p>An electronic compendium of immediately notifiable disease factsheets is available upon request. These sheets contain information on the epidemiology of these diseases as well as diagnostic tests that could lead to notification. To obtain a copy of the compendium, please send your request to the Epidemiology and Surveillance Section at the email address mentioned above.</p>\n<p>The immediately notifiable diseases are:</p>\n<ul class=\"colcount-sm-2\">\n<li>Aino virus infection</li>\n<li>Akabane disease</li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/anaplasmosis/eng/1319738331963/1319810963903\">Anaplasmosis</a></li>\n<li>Avian chlamydiosis (C. pscittaci)</li>\n<li>Avian encephalomyelitis</li>\n<li>Avian infectious laryngotracheitis</li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/bluetongue/eng/1306107020373/1306117266230\">Bluetongue</a></li>\n<li>Besnoitiosis</li>\n<li>Borna disease</li>\n<li>Bovine babesiosis (B. bovis)</li>\n<li>Bovine ephemeral fever</li>\n<li>Bovine petechial fever</li>\n<li>Contagious agalactia</li>\n<li>Contagious caprine pleuropneumonia</li>\n<li>Dourine</li>\n<li>Duck hepatitis</li>\n<li>Egg drop syndrome (adenovirus)</li>\n<li>Enterovirus encephalomyelitis (Teschen disease)</li>\n<li>Epizootic haemorrhagic disease</li>\n<li>Epizootic lymphangitis</li>\n<li>Equine encephalomyelitis, western and eastern</li>\n<li>Fluvinate-resistant Varroa mite</li>\n<li>Fowl cholera</li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/fact-sheet/eng/1515109194349/1515109530734\" title=\"Equine Glanders Fact Sheet\">Glanders</a></li>\n<li>Goose parvovirus infection (Derzsy's disease)</li>\n<li>Heartwater (cowdriosis)</li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/hendra-virus/eng/1303446980487/1305773199876\">Hendra Virus</a></li>\n<li>Herpes virus of cervidae</li>\n<li>Ibaraki disease</li>\n<li>Japanese encephalitis</li>\n<li>Louping ill</li>\n<li>Nairobi sheep disease</li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/nipah-virus/eng/1303433426078/1306092780481\">Nipah Virus</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/rhd/eng/1530824799434/1530824799683\">Rabbit haemorrhagic disease (RHD) or viral haemorrhagic disease of rabbits)</a></li>\n<li>Screwworm (Cochliomyia hominivorax and Chrysomyia bezziana)</li>\n<li>Small hive beetle (Aethina tumida)</li>\n<li>Theileriasis</li>\n<li>Tick-borne fever (Cytoecetes phagocytophilia)</li>\n<li>Tissue worm (Elaphostrongylus cervi)</li>\n<li>Trypanosomiasis (exotic to Canada)</li>\n<li>Turkey viral rhinotracheitis or swollen head disease in chickens</li>\n<li>Wesselsbron's disease</li>\n<li><a href=\"/animal-health/terrestrial-animals/diseases/immediately-notifiable/west-nile-virus/eng/1305840783267/1305840912854\">West Nile Virus</a></li>\n</ul>\n\n<h2>Importation of animal pathogens</h2>\n<p>The importation and use of animal and zoonotic pathogens is regulated by the Health of Animals Act and Regulations. Import permits and facility certification can be obtained from the <a href=\"/animal-health/biohazard-containment-and-safety/eng/1300121579431/1315776600051\">Biohazard Containment and Safety Division</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=15#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section><p>En g\u00e9n\u00e9ral, maladies \u00e0 notification imm\u00e9diate sont des maladies exotiques au Canada pour lesquelles il n'existe aucun programme de lutte ni d'\u00e9radication.</p>\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut cependant d\u00e9cider de prendre des mesures de lutte contre celles-ci lorsqu'elle est avertie de leur pr\u00e9sence au Canada. Cette cat\u00e9gorie englobe \u00e9galement certaines maladies indig\u00e8nes rares dont l'absence \u00e0 l'int\u00e9rieur du troupeau d'origine doit \u00eatre certifi\u00e9e pour satisfaire aux exigences d'importation des partenaires commerciaux.</p>\n<p>Seuls les laboratoires sont tenus de contacter l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> s'ils soup\u00e7onnent une telle maladie ou ont obtenu un diagnostic. Ils doivent communiquer les renseignements par courriel \u00e0 la Section d'\u00e9pid\u00e9miologie et de surveillance.<br> \n<a href=\"mailto:cfia.notification-notification.acia@inspection.gc.ca\">cfia.notification-notification.acia@inspection.gc.ca</a></p>\n\n<p>L'avis doit \u00eatre accompagn\u00e9 des renseignements suivants\u00a0:</p>\n<ol class=\"lst-lwr-alph\">\n<li>les nom, adresse et num\u00e9ro de t\u00e9l\u00e9phone du propri\u00e9taire de l'animal ou de la personne qui en a la possession, la garde ou la charge des soins;</li>\n<li>l'endroit o\u00f9 se trouve l'animal;</li>\n<li>tout autre renseignement dont le laboratoire dispose sur l'animal.</li>\n</ol>\n\n<p>Un recueil \u00e9lectronique de fiches d\u2019information sur les maladies \u00e0 notification imm\u00e9diate est disponible sur demande. Ces fiches contiennent de l'information sur l'\u00e9pid\u00e9miologie de ces maladies ainsi que les \u00e9preuves diagnostiques qui pourraient mener \u00e0 une notification. Pour obtenir une copie du recueil, veuillez faire parvenir votre demande \u00e0 la Section d'\u00e9pid\u00e9miologie et de surveillance \u00e0 l'adresse courriel mentionn\u00e9e ci-haut.</p>\n<p>Les autres maladies \u00e0 notification immediate sont\u00a0:</p>\n<ul class=\"colcount-sm-2\">\n<li>Agalaxie contagieuse</li>\n<li><a href=\"fra/1319738331963/1319810963903\">Anaplasmose</a></li>\n<li>Arthrogrypose enzootique</li>\n<li>Bab\u00e9siose bovine (B. bovis)</li>\n<li>Besnoitiose</li>\n<li>Chlamydiose aviaire (C. pscittaci)</li>\n<li>Chol\u00e9ra aviaire</li>\n<li>Col\u00e9opt\u00e8re Aethina tumida</li>\n<li>Dourine</li>\n<li>Enc\u00e9phalite japonaise</li>\n<li>Enc\u00e9phalomy\u00e9lite \u00e0 ent\u00e9rovirus (maladie de Teschen)</li>\n<li>Enc\u00e9phalomy\u00e9lite aviaire</li>\n<li>Enc\u00e9phalomy\u00e9lite \u00e9quine de l'Est ou de l'Ouest</li>\n<li>Enc\u00e9phalomy\u00e9lite ovine (louping ill)</li>\n<li>Fi\u00e8vre \u00e0 tiques (Cytoecetes phagocytophila)</li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/fievre-catarrhale-du-mouton/fra/1306107020373/1306117266230\">Fi\u00e8vre catarrhale du mouton</a></li>\n<li>Fi\u00e8vre \u00e9ph\u00e9m\u00e8re bovine</li>\n<li>Fi\u00e8vre p\u00e9t\u00e9chiale bovine</li>\n<li>Heartwater (cowdriose) </li>\n<li>H\u00e9morragie \u00e9pizootique</li>\n<li>H\u00e9patite virale du canard</li>\n<li>Herp\u00e8svirus des cervid\u00e9s</li>\n<li>Infection \u00e0 parvovirus de l'oie (maladie de Derzsy)</li>\n<li>Infection virale d'Aino</li>\n<li>Larve du tissu musculaire (Elaphostrongylus cervi)</li>\n<li>Laryngotrach\u00e9ite infectieuse aviaire</li>\n<li>Lymphangite \u00e9pizootique</li>\n<li>Maladie de Borna</li>\n<li>Maladie de Nairobi</li>\n<li>Maladie de Wesselsbron</li>\n<li>Maladie d'Ibaraki</li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/mhl/fra/1530824799434/1530824799683\">Maladie h\u00e9morragique du lapin (MHL) ou maladie h\u00e9morragique virale du lapin</a></li>\n<li>Mite Varroa r\u00e9sistante au fluvalinate</li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/fiche-de-renseignements/fra/1515109194349/1515109530734\" title=\"Fiche de renseignements sur la morve \u00e9quine\">Morve</a></li>\n<li>Myiase (Cochliomyia hominivorax et Chrysomyia bezziana)</li>\n<li>Pleuropneumonie contagieuse caprine</li>\n<li>Rhinotrach\u00e9ite virale du dindon ou syndrome de la grosse t\u00eate des poulets</li>\n<li>Syndrome de la chute de ponte (ad\u00e9novirus)</li>\n<li>Theil\u00e9riose</li>\n<li>Trypanosomose (forme exotique)</li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/virus-hendra/fra/1303446980487/1305773199876\">Virus Hendra</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/virus-du-nil-occidental/fra/1305840783267/1305840912854\">Virus du Nil occidental</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/maladies/notification-immediate/virus-nipah/fra/1303433426078/1306092780481\">Virus Nipah</a></li>\n</ul>\n\n<h2>L'importation d'agents zoopathog\u00e8nes et zoonotiques</h2>\n<p>L'importation et l'utilisation d'agents zoopathog\u00e8nes et zoonotiques sont r\u00e9gis par la Loi sur la sant\u00e9 des animaux et ses r\u00e8glements d'application. La <a href=\"/sante-des-animaux/confinement-des-biorisques-et-securite/fra/1300121579431/1315776600051\">division  du Confinement des biorisques et s\u00e9curit\u00e9</a> peut d\u00e9livrer des permis d'importation et certifier des installations.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}