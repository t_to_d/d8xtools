{
    "dcr_id": "1300208718953",
    "title": {
        "en": "Applying for environmental release of plants with novel traits in Canada",
        "fr": "Pr\u00e9senter une demande de diss\u00e9mination de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "html_modified": "2024-01-26 2:20:26 PM",
    "modified": "2023-05-03",
    "issued": "2015-04-13 11:33:29",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_pnts_release_1300208718953_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/plan_pnts_release_1300208718953_fra"
    },
    "ia_id": "1300208874046",
    "parent_ia_id": "1300137939635",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1300137939635",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Applying for environmental release of plants with novel traits in Canada",
        "fr": "Pr\u00e9senter une demande de diss\u00e9mination de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "label": {
        "en": "Applying for environmental release of plants with novel traits in Canada",
        "fr": "Pr\u00e9senter une demande de diss\u00e9mination de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1300208874046",
    "managing_branch": "comn",
    "type_name": "sitemapinit",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1300137887237",
    "orig_type_name": "sitemapinit",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-varieties/plants-with-novel-traits/applicants/",
        "fr": "/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Applying for environmental release of plants with novel traits in Canada",
            "fr": "Pr\u00e9senter une demande de diss\u00e9mination de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
        },
        "description": {
            "en": "Applying for environmental release of plants with novel traits in Canada",
            "fr": "Pr\u00e9senter une demande de diss\u00e9mination de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
        },
        "keywords": {
            "en": "confined, unconfined, environmental release, PNT, applicants, plants with novel traits",
            "fr": "conditions confin\u00e9es, v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux, VCN"
        },
        "dcterms.subject": {
            "en": "agri-food products,assessment,biotechnology,plants",
            "fr": "produit agro-alimentaire,\u00e9valuation,biotechnologie,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Plant Health and Biosecurity Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la protection des v\u00e9g\u00e9taux et bios\u00e9curit\u00e9"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-13 11:33:29",
            "fr": "2015-04-13 11:33:29"
        },
        "modified": {
            "en": "2023-05-03",
            "fr": "2023-05-03"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Applying for environmental release of plants with novel traits in Canada",
        "fr": "Pr\u00e9senter une demande de diss\u00e9mination de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Determining novelty</h2>\n<p>Plants with novel traits (PNTs) require authorization before they can be released into the environment. The following documents provide guidance on determining if a plant is a PNT:</p>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/directive-2009-09/eng/1304466419931/1304466812439\">Directive 2009-09: Plants with Novel Traits Regulated under Part V of the <i>Seeds Regulations</i>: Guidelines for Determining when to Notify the CFIA</a>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/directive-2009-09/rationale-for-updated-guidelines/eng/1682425597052/1682425597973\">Rationale for updated guidance determining whether a plant is subject to Part V of the <i>Seeds Regulations</i></a></li>\n</ul>\n</li>\n</ul>\n<h2>Applying for a confined research field trial in Canada</h2>\n<p>The confined research field trial program provides developers with an opportunity to grow PNTs for research purposes under terms and conditions of confinement which are designed to minimize any impact the PNT may have on the environment. These research trials allow developers to evaluate the field performance of their PNT, to collect information to address the environmental safety criteria necessary for an application for unconfined release, or to undertake academic research.</p>\n<p>Instructions, information requirements, and terms and conditions can be found in the following links:</p>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/directive-dir-2000-07/eng/1304474667559/1304474738697\">Directive 2000-07: Conducting Confined Research Field Trials of Plants with Novel Traits in Canada</a>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/approved-under-review/field-trials/eng/1313872595333/1313873672306\">Crop-Specific Terms and Conditions</a></li>\n</ul>\n</li>\n</ul>\n<p>Applicants wishing to submit an application to conduct field trials of PNTs intended for plant molecular farming (PMF) are subject to information requirements found in Dir2000-07.</p>\n<h2>Applying for an unconfined release in Canada</h2>\n<p>The Plant Biosafety Office of the Canadian Food Inspection Agency is responsible for the regulation of PNTs in Canada in regards to environmental safety. Before a PNT can be authorized for unconfined environmental release, a determination on the risk to the environment, including to human health, is required. Unconfined release involves the release into the environment with limited or no restrictions, generally towards commercialization.</p>\n<p>The following documents define the criteria and information requirements for the environmental safety assessment of a PNT intended for unconfined release:</p>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/directive-94-08/eng/1512588596097/1512588596818\">Directive 94-08 (Dir 94-08) Assessment Criteria for Determining Environmental Safety of Plants With Novel Traits</a>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/directive-94-08/biology-documents/eng/1330723572623/1330723704097\">Biology Documents (Companion Documents for Dir94-08)</a></li>\n</ul>\n</li>\n<li><a href=\"/animal-health/livestock-feeds/novel-feeds/guidance/eng/1547154845895/1547154883652\">Guidance for Submitting Whole Genome Sequencing Data to Support the Pre-market Assessment of Novel Foods, Novel Feeds, and Plants with Novel Traits</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/pre-submission-consultation/eng/1368394145255/1368394206548\">Pre-submission consultation process</a></li>\n</ul>\n<h2 id=\"imp\">Additional information</h2>\n<ul>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/canadian-pre-market-regulatory-process/eng/1542135073648/1542135074107\">Canadian pre-market regulatory process</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/imports/eng/1521907164251/1521907164658\">Importing plants with novel traits to Canada</a></li>\n<li><a href=\"/plant-varieties/plants-with-novel-traits/applicants/managing-cases-of-non-compliance/eng/1521903887306/1521903888151\">Managing cases of non-compliance of unauthorized plant products derived through biotechnology</a></li>\n</ul>\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>D\u00e9termination de la nouveaut\u00e9 </h2>\n<p>Les  v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux (VCN) n\u00e9cessitent une autorisation avant d'\u00eatre  diss\u00e9min\u00e9s dans l'environnement. Les documents suivants fournissent des lignes  directrices pour d\u00e9terminer si une plante est consid\u00e9r\u00e9e comme un VCN\u00a0:</p>\n\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/directive-2009-09/fra/1304466419931/1304466812439\">Directive 2009-09\u00a0: V\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux r\u00e9glement\u00e9s par la Partie V du\u00a0<i>R\u00e8glement sur les semences</i>\u00a0: Notes d'orientation pour d\u00e9terminer quand aviser l'ACIA</a>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/directive-2009-09/justification-des-lignes-directrices/fra/1682425597052/1682425597973\">Justification des lignes directrices mises \u00e0 jour pour d\u00e9terminer si un v\u00e9g\u00e9tal est r\u00e9glement\u00e9 par la partie V du <i>R\u00e8glement sur les semences</i> (Directive 2009-09)</a></li>\n</ul>\n</li>\n</ul>\n<h2>Pr\u00e9senter une demande d'essai de recherche au champ en conditions confin\u00e9es au Canada</h2>\n<p>Le programme d'essais de recherche au champ en conditions confin\u00e9es fournit aux concepteurs une occasion de cultiver des VCN \u00e0 des fins de recherche sous des conditions particuli\u00e8res de confinement, lesquelles visent \u00e0 r\u00e9duire au minimum toute r\u00e9percussion que pourrait avoir un VCN sur l'environnement. Les essais de recherche permettent aux concepteurs d'\u00e9valuer le rendement au champ de leur VCN, de recueillir l'information n\u00e9cessaire pour r\u00e9pondre aux crit\u00e8res de s\u00e9curit\u00e9 environnementale en vue d'une demande de diss\u00e9mination en milieu ouvert, ou m\u00eame d'entreprendre des \u00e9tudes fondamentales.</p>\n<p>Voir les liens ci-dessous pour conna\u00eetre les directives, l'information requise et les conditions particuli\u00e8res\u00a0:</p>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/directive-dir-2000-07/fra/1304474667559/1304474738697\">Directive 2000-07\u00a0: La conduite d'essais au champ en conditions confin\u00e9es de v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada</a>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/approuves-cours-d-evaluation/essais-au-champ/fra/1313872595333/1313873672306\">Conditions particuli\u00e8res \u00e0 chaque esp\u00e8ce v\u00e9g\u00e9tale</a></li>\n</ul>\n</li>\n</ul>\n<p>Les demandeurs qui souhaitent pr\u00e9senter une demande d'essai au champ de VCN destin\u00e9s \u00e0 la agriculture mol\u00e9culaire v\u00e9g\u00e9tale (MV) sont assujettis aux prescriptions en mati\u00e8re d'information de la Dir2000-07.</p>\n<h2>Pr\u00e9senter une demande de diss\u00e9mination en milieu ouvert au Canada</h2>\n<p>Le Bureau de la bios\u00e9curit\u00e9 v\u00e9g\u00e9tale de l'Agence canadienne d'inspection des aliments est responsable de la r\u00e9glementation des VCN au Canada, en ce qui concerne la s\u00e9curit\u00e9 pour l'environnement. Avant que l'on puisse autoriser la diss\u00e9mination d'un VCN en milieu ouvert, une \u00e9tude du risque pour l'environnement, y compris la sant\u00e9 humaine, doit \u00eatre men\u00e9e. La diss\u00e9mination en milieu ouvert signifie que le VCN est diss\u00e9min\u00e9 dans l'environnement sans aucune restriction ou suivant des restrictions limit\u00e9es, en r\u00e8gle g\u00e9n\u00e9rale en vue d'en faire la commercialisation.</p>\n<p>Les documents suivants d\u00e9finissent les crit\u00e8res et exigences relatives \u00e0 l'information prise en compte dans l'\u00e9valuation de la s\u00fbret\u00e9 pour l'environnement des VCN destin\u00e9s \u00e0 \u00eatre diss\u00e9min\u00e9s en milieu ouvert\u00a0:</p>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/directive-94-08/fra/1512588596097/1512588596818\">Directive 94-08 (Dir 94-08) Crit\u00e8res d'\u00e9valuation du risque environnemental associ\u00e9 aux v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/directive-94-08/documents-sur-la-biologie/fra/1330723572623/1330723704097\">Documents sur la biologie (Cahiers allant de pair avec la Dir94-08)</a></li>\n</ul>\n</li>\n<li><a href=\"/sante-des-animaux/aliments-du-betail/aliments-nouveaux/lignes-directrices/fra/1547154845895/1547154883652\">Lignes directrices sur la pr\u00e9sentation des donn\u00e9es issues du s\u00e9quen\u00e7age du g\u00e9nome entier (SGE) au soutien de l'\u00e9valuation pr\u00e9alable \u00e0 la mise en march\u00e9 des aliments nouveaux destin\u00e9s \u00e0 la consommation humaine et animale et aux v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/procedures-de-consultation-prealable/fra/1368394145255/1368394206548\">Proc\u00e9dures de consultation pr\u00e9alable</a></li>\n</ul>\n<h2 id=\"info\">Information suppl\u00e9mentaire</h2>\n<ul>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/processus-reglementaire-canadien-prealable-a-la-mi/fra/1542135073648/1542135074107\">Processus r\u00e9glementaire canadien pr\u00e9alable \u00e0 la mise en march\u00e9</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/importation/fra/1521907164251/1521907164658\">Importer des v\u00e9g\u00e9taux \u00e0 caract\u00e8res nouveaux au Canada</a></li>\n<li><a href=\"/varietes-vegetales/vegetaux-a-caracteres-nouveaux/demandeurs/gestion-des-situations-de-non-conformite/fra/1521903887306/1521903888151\">Gestion des situations de non-conformit\u00e9 des produits v\u00e9g\u00e9taux non autoris\u00e9s issus de la biotechnologie</a></li>\n</ul>\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}