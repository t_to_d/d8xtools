{
    "dcr_id": "1660760988035",
    "title": {
        "en": "Report plant pests to help protect Canada's plant resources",
        "fr": "Signaler les phytoravageurs pour aider \u00e0 prot\u00e9ger les ressources v\u00e9g\u00e9tales du Canada"
    },
    "html_modified": "2024-03-12 3:59:21 PM",
    "modified": "2022-08-18",
    "issued": "2022-08-18",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_report_plant_pests_1660760988035_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_report_plant_pests_1660760988035_fra"
    },
    "ia_id": "1660760988613",
    "parent_ia_id": "1565298134171",
    "chronicletopic": {
        "en": "Science and Innovation|Plant Health",
        "fr": "Science et Innovation|Sant\u00e9 des plantes"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Industry|Canadians",
        "fr": "Industrie|Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565298134171",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Report plant pests to help protect Canada's plant resources",
        "fr": "Signaler les phytoravageurs pour aider \u00e0 prot\u00e9ger les ressources v\u00e9g\u00e9tales du Canada"
    },
    "label": {
        "en": "Report plant pests to help protect Canada's plant resources",
        "fr": "Signaler les phytoravageurs pour aider \u00e0 prot\u00e9ger les ressources v\u00e9g\u00e9tales du Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1660760988613",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565298133875",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/plant-health/plant-pests/",
        "fr": "/inspecter-et-proteger/sante-des-vegetaux/phytoravageurs/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Report plant pests to help protect Canada's plant resources",
            "fr": "Signaler les phytoravageurs pour aider \u00e0 prot\u00e9ger les ressources v\u00e9g\u00e9tales du Canada"
        },
        "description": {
            "en": "The quicker a pest is detected and reported, the quicker we can act to address the risks of it establishing and spreading. Here are some species to look out for.",
            "fr": "Le plus t\u00f4t un organisme nuisible est d\u00e9tect\u00e9 et signal\u00e9, le plus vite nous pouvons agir pour faire face aux risques qu'il s'\u00e9tablisse et se propage. Voici quelques esp\u00e8ces \u00e0 surveiller."
        },
        "keywords": {
            "en": "Plant pests, protect plant resources, cultivating science, CFIA",
            "fr": "Signaler les phytoravageurs, prot\u00e9ger les ressources v\u00e9g\u00e9tales, ACIA"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-08-18",
            "fr": "2022-08-18"
        },
        "modified": {
            "en": "2022-08-18",
            "fr": "2022-08-18"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Report plant pests to help protect Canada's plant resources",
        "fr": "Signaler les phytoravageurs pour aider \u00e0 prot\u00e9ger les ressources v\u00e9g\u00e9tales du Canada"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263148420\"></div>\n\n<p>By Fedaa Khirallah and Bruno Gallant</p>\n\t\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_report_pests_600x600_1660761353974_eng.jpg\" alt=\"Adult spotted lanternfly\" class=\"img-thumbnail img-responsive\">\n<figcaption><small>Adult spotted lanternfly.<br>Source: Lawrence Barringer, Pennsylvania Department of Agriculture (Bugwood.org).\n</small></figcaption>\n</figure>\n</div>\n\n\n\t\n<p>This blog post was originally published to <a href=\"https://science.gc.ca/eic/site/063.nsf/eng/h_97679.html\">Cultivating Science</a> on science.gc.ca.</p>\n\t\n<blockquote>\n\t\n<p>All Canadians have a role to play in protecting plants in Canada from invasive insects and plant species. This includes\u00a0reporting findings of plant pests\u00a0to the Canadian Food Inspection Agency (CFIA). Invasive plants, insects, snails, slugs and pathogens all have the potential to negatively impact Canada's agriculture, forestry and environmental resources. Quickly determining their impact is key to minimizing any potential damage to the Canadian environment and economy.</p>\n\t\n<p>The CFIA is a science-based regulator that sets plant health regulations, policies and programs that are consistent with international standards.</p>\n\t\n</blockquote>\n\t\n<p><a href=\"https://www.science.gc.ca/eic/site/063.nsf/eng/98423.html\">Read the full blog post</a>.</p>\n\t\n\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263148420\"></div>\n\n<p>Par Fedaa Khirallah et Bruno Gallant</p>\n\t\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_pro_report_pests_600x600_1660761353974_fra.jpg\" alt=\"Fulgore tachet\u00e9 adulte.\" class=\"img-thumbnail img-responsive\">\n<figcaption><small>Fulgore tachet\u00e9 adulte.<br>Source\u00a0: Lawrence Barringer, Pennsylvania Department of Agriculture (Bugwood.org).\n</small></figcaption>\n</figure>\n</div>\n\n<p>Ce billet de blogue est d'abord paru dans <a href=\"https://science.gc.ca/eic/site/063.nsf/fra/h_97679.html\">Cultiver la science</a> sur science.gc.ca.</p>\n\n<blockquote>\n\t\n<p>Tous les Canadiens ont un r\u00f4le \u00e0 jouer dans la protection des plantes contre les insectes et les esp\u00e8ces v\u00e9g\u00e9tales envahissantes au Canada. Cela comprend la d\u00e9claration de l'existence de phytoravageurs \u00e0 l'Agence canadienne d'inspection des aliments (ACIA). Les plantes envahissantes, les insectes, les escargots, les limaces et les agents pathog\u00e8nes peuvent entra\u00eener des r\u00e9percussions n\u00e9gatives sur les ressources agricoles, foresti\u00e8res et environnementales du Canada. Il est essentiel de d\u00e9terminer rapidement leur impact pour r\u00e9duire au minimum les dommages potentiels \u00e0 l'environnement et \u00e0 l'\u00e9conomie du Canada.</p>\n\t\n<p>L'ACIA est un organisme de r\u00e9glementation scientifique qui \u00e9tablit des r\u00e8glements, des politiques et des programmes sur la sant\u00e9 des v\u00e9g\u00e9taux qui sont conformes aux normes internationales.</p>\n\t\n</blockquote>\n\t\n<p><a href=\"https://www.science.gc.ca/eic/site/063.nsf/fra/98423.html\">Lisez le billet de blogue complet</a>.</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}