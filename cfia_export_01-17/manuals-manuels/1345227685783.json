{
    "dcr_id": "1345227685783",
    "title": {
        "en": "Chapter 1 - Overview",
        "fr": "Chapitre 1 - Aper\u00e7u"
    },
    "html_modified": "2015-03-19 15:48",
    "modified": "25-8-2021",
    "issued": "19-3-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/manuals-manuels/data/terr_disease_accreditvetmanual_chapter1_1345227685783_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/manuals-manuels/data/terr_disease_accreditvetmanual_chapter1_1345227685783_fra"
    },
    "parent_ia_id": "1343915703253",
    "ia_id": "1345227813629",
    "parent_node_id": "1343915703253",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Chapter 1 - Overview",
        "fr": "Chapitre 1 - Aper\u00e7u"
    },
    "label": {
        "en": "Chapter 1 - Overview",
        "fr": "Chapitre 1 - Aper\u00e7u"
    },
    "templatetype": "content page 1 column",
    "node_id": "1345227813629",
    "managing_branch": "comn",
    "type_name": "manuals-manuels",
    "dcr_type": "manuals-manuels",
    "parent_dcr_id": "1343915611518",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/accredited-veterinarian-s-manual/chapter-1/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/manuel-du-veterinaire-accredite/chapitre-1/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Chapter 1 - Overview",
            "fr": "Chapitre 1 - Aper\u00e7u"
        },
        "description": {
            "en": "This module describes the authority for activities and duties performed by accredited veterinarians.",
            "fr": "Ce module d\u00e9crit le fondement l\u00e9gislatif pour les activit\u00e9s et fonctions effectu\u00e9es par le v\u00e9t\u00e9rinaire accr\u00e9dit\u00e9."
        },
        "keywords": {
            "en": "animals, diseases, inspection, import, export, veterinary medicine, regulations, procedures, responsability, surveillance, certification",
            "fr": "animal, maladies, inspection, importation, exportation, m&#233;decine v&#233;t&#233;rinaire, politiques, proc&#233;dures, responsabilit&#233;s, surveillances, certification"
        },
        "dcterms.subject": {
            "en": "livestock,food inspection,infectious diseases,handbooks,veterinary medicine,standards,animal health",
            "fr": "b\u00e9tail,inspection des aliments,maladie infectieuse,manuel,m\u00e9decine v\u00e9t\u00e9rinaire,norme,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-03-19 15:48:08",
            "fr": "2015-03-19 15:48:08"
        },
        "modified": {
            "en": "2021-08-25",
            "fr": "2021-08-25"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Chapter 1 - Overview",
        "fr": "Chapitre 1 - Aper\u00e7u"
    },
    "body": {
        "en": "        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">This page is part of the Guidance Document Repository (GDR).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/industry-guidance/eng/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=14#gdr_results\" class=\"alert-link\">\r\n            Search for related documents in the Guidance Document Repository\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/animal-health/terrestrial-animals/diseases/accredited-veterinarian-s-manual/chapter-1/eng/1345227685783/1345227813629?chap=0\">Complete text</a></p>\r\n\r\n\r\n<h2>On this page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/diseases/accredited-veterinarian-s-manual/chapter-1/eng/1345227685783/1345227813629?chap=1\">1.1 Accredited Veterinarian's Mandate</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/diseases/accredited-veterinarian-s-manual/chapter-1/eng/1345227685783/1345227813629?chap=2\">1.2 Accreditation Agreement</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/diseases/accredited-veterinarian-s-manual/chapter-1/eng/1345227685783/1345227813629?chap=3\">1.3 Quality control (updated July 2021)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/animal-health/terrestrial-animals/diseases/accredited-veterinarian-s-manual/chapter-1/eng/1345227685783/1345227813629?chap=4\">1.4 Special provisions (updated July 2021)</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n\r\n\r\n<section class=\"alert alert-info\">\r\n<h2 class=\"h3\">Cette page fait partie du r\u00e9pertoire des documents d'orientation (RDO).</h2>\r\n<p>\r\n<br>\r\n<a href=\"/orientation-pour-l-industrie/fra/1374161650885/1374161737236?gp=1&amp;gc=30&amp;ga=14#gdr_results\" class=\"alert-link\">\r\n            Recherche de documents connexes dans le r\u00e9pertoire des documents d'orientation.\r\n</a>\r\n</p>\r\n</section>\r\n\r\n\r\n\r\n<p class=\"small text-right\"><a href=\"/sante-des-animaux/animaux-terrestres/maladies/manuel-du-veterinaire-accredite/chapitre-1/fra/1345227685783/1345227813629?chap=0\">Texte complet</a></p>\r\n\r\n\r\n<h2>Sur cette page</h2>\r\n<ul class=\"list-unstyled col-md-offset-1 mrgn-tp-md mrgn-bttm-md\">\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/manuel-du-veterinaire-accredite/chapitre-1/fra/1345227685783/1345227813629?chap=1\">1.1 Mandat du v\u00e9t\u00e9rinaire accr\u00e9dit\u00e9</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/manuel-du-veterinaire-accredite/chapitre-1/fra/1345227685783/1345227813629?chap=2\">1.2 Entente d'accr\u00e9ditation</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/manuel-du-veterinaire-accredite/chapitre-1/fra/1345227685783/1345227813629?chap=3\">1.3 Contr\u00f4le de la qualit\u00e9 (mise \u00e0 jour : juillet 2021)</a>\r\n\r\n</li>\r\n\r\n<li>\r\n<a href=\"/sante-des-animaux/animaux-terrestres/maladies/manuel-du-veterinaire-accredite/chapitre-1/fra/1345227685783/1345227813629?chap=4\">1.4 Dispositions sp\u00e9ciales (mis \u00e0 jour juillet 2021)</a>\r\n\r\n</li>\r\n\r\n</ul>\r\n\r\n\r\n<div class=\"clear\"></div>\r\n\n\n\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}