{
    "dcr_id": "1355965765063",
    "title": {
        "en": "Contribute to Scrapie Surveillance",
        "fr": "Participez \u00e0 la surveillance de la tremblante"
    },
    "html_modified": "2015-04-07 12:51",
    "modified": "23-9-2016",
    "issued": "7-4-2015",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_scrapie_surveillance_1355965765063_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/dis_scrapie_surveillance_1355965765063_fra"
    },
    "parent_ia_id": "1329723572482",
    "ia_id": "1355965901017",
    "parent_node_id": "1329723572482",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Contribute to Scrapie Surveillance",
        "fr": "Participez \u00e0 la surveillance de la tremblante"
    },
    "label": {
        "en": "Contribute to Scrapie Surveillance",
        "fr": "Participez \u00e0 la surveillance de la tremblante"
    },
    "templatetype": "content page 1 column",
    "node_id": "1355965901017",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1329723409732",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/scrapie/surveillance/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tremblante/surveillance/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Contribute to Scrapie Surveillance",
            "fr": "Participez \u00e0 la surveillance de la tremblante"
        },
        "description": {
            "en": "Animals, Animal Health",
            "fr": "Animaux, Sant\u00e9 des animaux"
        },
        "keywords": {
            "en": "Animals, Animal Health",
            "fr": "Animaux, Sant\u00e9 des animaux"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2015-04-07 12:51:16",
            "fr": "2015-04-07 12:51:16"
        },
        "modified": {
            "en": "2016-09-23",
            "fr": "2016-09-23"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Contribute to Scrapie Surveillance",
        "fr": "Participez \u00e0 la surveillance de la tremblante"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Producer participation is critical to the success of the scrapie surveillance program and, ultimately, to eradicating scrapie from Canada</h2>\n<p>To achieve scrapie sampling targets, the Canadian Food Inspection Agency (CFIA) works closely with the sheep and goat industries, veterinarians and provinces. Scrapie surveillance samples are collected at farms, abattoirs, auction markets, deadstock facilities and animal health laboratories.</p>\n<p>The goals of the scrapie surveillance program are to:</p>\n\n<ul><li>identify infected sheep flocks or goat herds so that proper disease control actions can be taken;</li>\n<li>measure the progress of Canada's eradication program;</li> \n<li>detect scrapie if introduced to Canada; and</li> \n<li>demonstrate low disease risk for trading purposes.</li></ul>\n\n<p>Scrapie surveillance is a shared responsibility. Sheep and goat producers, industry, veterinarians and governments all have a role to play.</p>\n\n<h2>Having an animal tested</h2>\n<p>The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> would like to test any mature animals (aged 12 months and older) that die on your farm or exhibit the following symptoms:</p>\n<ul>\n<li>unexplained weight loss</li>\n<li>problems standing or walking</li>\n<li>changes in behaviour</li>\n</ul>\n<p>You can arrange to drop the sample off yourself, or have a <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> inspector come to your farm to collect the sample.</p>\n<p>To make arrangements to have a sample taken for testing, call the nearest <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\"><abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> office</a>.</p>\n\n<h2>The facts about scrapie</h2>\n\n<p>Scrapie is a fatal disease that affects the central nervous system of sheep and goats. It is a transmissible spongiform encephalopathy (TSE). Other <abbr title=\"transmissible spongiform encephalopathy\">TSE</abbr>s include <a href=\"/animal-health/terrestrial-animals/diseases/reportable/bovine-spongiform-encephalopathy/eng/1323991831668/1323991912972\">bovine spongiform encephalopathy (BSE)</a> in cattle and <a href=\"/animal-health/terrestrial-animals/diseases/reportable/cwd/eng/1330143462380/1330143991594\">chronic wasting disease in deer and elk</a>.</p> \n\n<p>Scrapie is a \"<a href=\"/animal-health/terrestrial-animals/diseases/reportable/eng/1303768471142/1303768544412\">federally reportable disease</a>\" in Canada. That means all suspected cases must be reported to the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>. Compensation may be available for live animals suspected of having scrapie and ordered destroyed by the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>.</p>\n\n<p>Scrapie surveillance is an essential part of the National Scrapie Eradication Program. The goal of Canada's National Scrapie Eradication Program is to eradicate classical scrapie from the national sheep flock and goat herd. This protects the health of the national sheep flock and goat herd, and maintains confidence in Canada's sheep and goat industries locally and around the world.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>La participation des producteurs est essentielle au succ\u00e8s du programme de surveillance de la tremblante et, en d\u00e9finitive, \u00e0 l'\u00e9radication de cette maladie au Canada.</h2>\n\n<p>Pour atteindre ses objectifs d'\u00e9chantillonnage destin\u00e9s au d\u00e9pistage de la tremblante, l'Agence canadienne d'inspection des aliments (ACIA) collabore \u00e9troitement avec les industries ovine et caprine, les v\u00e9t\u00e9rinaires et les provinces. Dans le cadre de cette surveillance, des \u00e9chantillons sont pr\u00e9lev\u00e9s dans les exploitations, les abattoirs, les march\u00e9s aux ench\u00e8res, les centres de collecte de cadavres d'animaux et les laboratoires de sant\u00e9 animale.</p>\n\n<p>Les objectifs du programme de surveillance de la tremblante sont les suivants\u00a0:</p> \n<ul><li>rep\u00e9rer les troupeaux de moutons ou de ch\u00e8vres infect\u00e9s pour que les mesures appropri\u00e9es de lutte contre la maladie puissent \u00eatre prises;</li>\n<li>mesurer les avanc\u00e9es du programme d'\u00e9radication du Canada;</li> \n<li>d\u00e9tecter la tremblante si elle est introduite au Canada;</li>\n<li>d\u00e9montrer que le risque de maladie est faible aux fins des \u00e9changes commerciaux.</li></ul>\n\n<p>La surveillance de la tremblante est une responsabilit\u00e9 partag\u00e9e. Les \u00e9leveurs de moutons et de ch\u00e8vres, l'industrie, les v\u00e9t\u00e9rinaires et les gouvernements ont tous un r\u00f4le \u00e0 jouer.</p>\n\n<h2>Soumettre un animal \u00e0 des \u00e9preuves de d\u00e9pistage</h2>\n\n<p>L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> souhaite soumettre \u00e0 des \u00e9preuves de d\u00e9pistage tout animal adulte (\u00e2g\u00e9 de 12 mois et plus) qui meurt \u00e0 l'exploitation ou qui pr\u00e9sente les sympt\u00f4mes suivants\u00a0:</p>\n<ul>\n<li>perte de poids inexpliqu\u00e9e</li>\n<li>difficult\u00e9 \u00e0 se tenir debout ou \u00e0 marcher</li>\n<li>changements de comportement</li>\n</ul>\n\n<p>Vous pouvez vous charger de la livraison de l'\u00e9chantillon ou demander \u00e0 ce qu'un inspecteur de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> vienne \u00e0 votre exploitation pour le pr\u00e9lever.</p>\n\n<p>Pour prendre les dispositions n\u00e9cessaires pour faire pr\u00e9lever un \u00e9chantillon \u00e0 des fins d'analyse, communiquez avec le <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr></a> le plus pr\u00e8s de vous.</p>\n\n<h2>Faits concernant la tremblante</h2>\n\n<p>La tremblante est une maladie mortelle qui touche le syst\u00e8me nerveux central des moutons et des ch\u00e8vres. C'est une enc\u00e9phalopathie spongiforme transmissible (EST). Il existe d'autres <abbr title=\"enc\u00e9phalopathie spongiforme transmissible\">EST</abbr>, par exemple l'<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/encephalopathie-spongiforme-bovine/fra/1323991831668/1323991912972\">enc\u00e9phalopathie spongiforme bovine (ESB)</a> chez les bovins et la <a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/mdc/fra/1330143462380/1330143991594\">maladie d\u00e9bilitante chronique (MDC) chez les cerfs et les wapitis</a>.</p>\n \n<p>La tremblante est une \u00ab\u00a0<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/fra/1303768471142/1303768544412\">maladie \u00e0 d\u00e9claration obligatoire</a>\u00a0\u00bb au Canada. Cela signifie que l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> doit \u00eatre inform\u00e9e de tous les cas soup\u00e7onn\u00e9s. Une indemnisation pourrait \u00eatre accord\u00e9e pour les animaux vivants soup\u00e7onn\u00e9s d'\u00eatre infect\u00e9s par la maladie et faisant l'objet d'une ordonnance de destruction par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\n\n<p>La surveillance de la tremblante est un \u00e9l\u00e9ment essentiel du Programme national d'\u00e9radication de la tremblante. L'objectif du Programme national d'\u00e9radication de la tremblante est d'\u00e9radiquer cette maladie des troupeaux nationaux de moutons et de ch\u00e8vres afin de prot\u00e9ger la sant\u00e9 de ces troupeaux et de maintenir la confiance envers les industries ovine et caprine canadiennes \u00e0 l'\u00e9chelle locale et dans le monde.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}