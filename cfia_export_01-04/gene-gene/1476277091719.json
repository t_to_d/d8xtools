{
    "dcr_id": "1476277091719",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Carduus nutans</i> (Nodding thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Carduus nutans</i> (Chardon pench\u00e9)"
    },
    "html_modified": "16-10-2017",
    "modified": "11-10-2017",
    "issued": "12-10-2016",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_carduus_nutans_1476277091719_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/WORKAREA/plants-vegetaux/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_carduus_nutans_1476277091719_fra"
    },
    "parent_ia_id": "1333136685768",
    "ia_id": "1476277092202",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Carduus nutans</i> (Nodding thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Carduus nutans</i> (Chardon pench\u00e9)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Carduus nutans</i> (Nodding thistle)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Carduus nutans</i> (Chardon pench\u00e9)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476277092202",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/carduus-nutans/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/carduus-nutans/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Carduus nutans (Nodding thistle)",
            "fr": "Semence de mauvaises herbe : Carduus nutans (Chardon pench\u00e9)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Carduus nutans, Asteraceae, Nodding thistle",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Carduus nutans, Asteraceae, Chardon pench\u00e9"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-10-16",
            "fr": "2017-10-16"
        },
        "modified": {
            "en": "2017-10-11",
            "fr": "2017-10-11"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Carduus nutans (Nodding thistle)",
        "fr": "Semence de mauvaises herbe : Carduus nutans (Chardon pench\u00e9)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Common Name</h2>\n<p>Nodding thistle</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs across Canada with the exception of <abbr title=\"Northwest Territories\">NT</abbr>, <abbr title=\"Nunavut\">NU</abbr>, <abbr title=\"Prince Edward Island\">PE</abbr>, <abbr title=\"Yukon\">YT</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to northern Africa, temperate Asia, and Europe. Introduced in North America, Chile, South Africa, Australia, New Zealand and beyond its native range in Europe (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Biennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Achene</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Achene length: 2.4 - 3.8 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Achene width: 0.9 - 1.6 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene elongate, straight to slightly curved, tapering at the base and top is truncate</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Achene glossy surface, looks varnished</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Achene light to dark golden brown; the base is canary yellow and a yellow band may occur below the collar</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Both longitudinal lines and transverse waves may be seen under the achene's varnish-like surface coating</li>\n<li>The style peg at the top of achene is usually short and thick</li>\n<li>Immature achenes may have a thin white pappus</li>\n\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Old fields, pastures, rangelands, roadsides and disturbed areas (Desrochers et al. 1988<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>, Darbyshire 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Nodding thistle is believed to have arrived in eastern North America in ships' ballast, and introduced into Saskatchewan through rapeseed and dispersed along railway lines (Desrochers et al. 1988<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n<p>This species produces large numbers of seeds (11,000) per plant and may form dense stands in disturbed areas such as gravel pits, roadsides and overgrazed pasture (Desrochers et al. 1988<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>)</h3>\n<ul>\n<li>Spiny plumeless thistle achenes (length: 2.5\u00a0-\u00a03.0 <abbr title=\"millimetres\">mm</abbr>) are generally shorter and paler than nodding thistle achenes. The achenes have the same elongate shape with a thick top peg, and both longitudinal and transverse lines occur on both species' achenes.</li>\n\n<li>Spiny plumeless thistle does not have the canary yellow base of nodding thistle, and the lines on the surface stand out in relief, while the lines on the surface of nodding thistle are covered with a varnish-like coating.</li>\n\n<li>Immature nodding thistle achenes may have lines that stand out from the surface, but they will have a yellow base.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_05cnsh_1475592949337_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achenes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_02cnsh_1475592825972_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_04cnsh_1475592914725_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achene top peg\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_07cnsh_1475592996960_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Nodding thistle (<i lang=\"la\">Carduus nutans</i>) bottom of achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_looks_like_nutans_copyright_1475592769756_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achene\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_copyrifgt_1475593026210_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Nodding thistle (<i lang=\"la\">Carduus nutans</i>) achenes\n</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_03cnsh_1475592513591_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) achenes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_01cnsh_1475592486335_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Spiny plumeless thistle (<i lang=\"la\">Carduus acanthoides</i>) achene</figcaption>\n</figure>\n\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong>Desrochers, A. M., Bain, J. F. and Warwick, S. I. 1988</strong>. The Biology of Canadian weeds. 89. <i lang=\"la\">Carduus</i> <i lang=\"la\">nutans</i> L. and <i lang=\"la\">Carduus acanthoides</i> L. Canadian Journal of Plant Science 68: 1053-1068.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Asteraceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Chardon pench\u00e9</p>\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente partout au Canada, sauf dans les <abbr title=\"Territoires du Nord-Ouest\">T.N.-O.</abbr>, au <abbr title=\"Nunavut\">Nt</abbr>, \u00e0 l'<abbr title=\"\u00cele-du-Prince-\u00c9douard\">\u00ce.-P.-\u00c9.</abbr> Et au <abbr title=\"Yukon\">Yn</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Afrique septentrionale, de l'Asie temp\u00e9r\u00e9e et d'Europe. Introduite en Am\u00e9rique du Nord, au Chili, en Afrique du Sud, en Australie, en Nouvelle-Z\u00e9lande et \u00e0 l'ext\u00e9rieur de son aire d'indig\u00e9nat en Europe (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Bisannuelle</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Ak\u00e8ne</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de l'ak\u00e8ne\u00a0: 2,4 \u00e0 3,8 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeurde l'ak\u00e8ne\u00a0: 0,9 \u00e0 1,6 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Ak\u00e8ne allong\u00e9, droit \u00e0 l\u00e9g\u00e8rement courb\u00e9, se r\u00e9tr\u00e9cissant vers la base, sommet tronqu\u00e9</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>L'ak\u00e8ne a une surface luisante et verniss\u00e9e</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>L'ak\u00e8ne est de couleur brun dor\u00e9, p\u00e2le ou fonc\u00e9; la base est jaune canari et une bande jaune est parfois pr\u00e9sente sous le collet</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li> Des lignes longitudinales et des ondulations transversales sont parfois visibles sous le rev\u00eatement verniss\u00e9 de l'ak\u00e8ne</li>\n<li>La base du style au sommet de l'ak\u00e8ne est g\u00e9n\u00e9ralement courte et \u00e9paisse</li>\n<li>Les ak\u00e8nes immatures peuvent avoir un mince pappus blanc</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs abandonn\u00e9s, p\u00e2turages, parcours, bords de chemin et terrains perturb\u00e9s (Desrochers et al., 1988<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>; Darbyshire, 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>Le chardon pench\u00e9 aurait, croit-on, \u00e9t\u00e9 introduit dans l'Est de l'Am\u00e9rique du Nord par les eaux de ballast de navire, et en Saskatchewan dans du colza contamin\u00e9, puis il s'est dispers\u00e9 le long des voies ferr\u00e9es (Desrochers et al., 1988<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n<p>Cette esp\u00e8ce produit un grand nombre de graines (11\u00a0000) par plante, pouvant former de denses peuplements dans des sites perturb\u00e9s comme des carri\u00e8res de gravier, des bords de chemin et des p\u00e2turages surutilis\u00e9s (Desrochers et al., 1988<sup id=\"fn3c-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>)</h3>\n<ul>\n<li>Les ak\u00e8nes du chardon \u00e9pineux (longueur\u00a0: 2,5 \u00e0 3,0 <abbr title=\"millim\u00e8tre\">mm</abbr>) sont g\u00e9n\u00e9ralement plus courts et plus p\u00e2les que ceux du chardon pench\u00e9. Les ak\u00e8nes ont la m\u00eame forme allong\u00e9e avec une base stylaire \u00e9paisse, tandis que des lignes longitudinales et transversales sont pr\u00e9sentes chez les deux esp\u00e8ces.</li>\n\n<li>Les ak\u00e8nes du chardon \u00e9pineux n'ont pas une base jaune canari comme celle du chardon pench\u00e9, et les lignes \u00e0 la surface sont saillantes, alors que celles du chardon pench\u00e9 sont recouvertes d'une couche verniss\u00e9e.</li>\n\n<li>Les ak\u00e8nes immatures du chardon pench\u00e9 peuvent avoir des lignes saillantes \u00e0 la surface, mais leur base est jaune.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_05cnsh_1475592949337_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_02cnsh_1475592825972_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_04cnsh_1475592914725_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) sommet de l'ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_07cnsh_1475592996960_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) base de l'ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_looks_like_nutans_copyright_1475592769756_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) ak\u00e8ne\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_nutans_copyrifgt_1475593026210_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Chardon pench\u00e9 (<i lang=\"la\">Carduus nutans</i>) ak\u00e8nes\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_03cnsh_1475592513591_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) ak\u00e8nes</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_carduus_acanthoides_01cnsh_1475592486335_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Chardon \u00e9pineux (<i lang=\"la\">Carduus acanthoides</i>) ak\u00e8ne</figcaption>\n</figure>\n\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p>\n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong>Desrochers, A. M., Bain, J. F. and Warwick, S. I. 1988</strong>. The Biology of Canadian weeds. 89. <i lang=\"la\">Carduus</i> <i lang=\"la\">nutans</i> L. and <i lang=\"la\">Carduus acanthoides</i> L. Canadian Journal of Plant Science 68: 1053-1068.</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p>\n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p>\n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": false,
    "chat_wizard": false
}