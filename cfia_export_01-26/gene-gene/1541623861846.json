{
    "dcr_id": "1541623861846",
    "title": {
        "en": "Importing honey",
        "fr": "Importation de miel"
    },
    "html_modified": "2024-01-26 2:24:24 PM",
    "modified": "2022-12-08",
    "issued": "2022-12-08",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_imp_honey_1541623861846_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_sfcr_imp_honey_1541623861846_fra"
    },
    "ia_id": "1541623862045",
    "parent_ia_id": "1536170495320",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Importing food",
        "fr": "Importation d\u2019aliments"
    },
    "commodity": {
        "en": "Honey",
        "fr": "Miel"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1536170495320",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Importing honey",
        "fr": "Importation de miel"
    },
    "label": {
        "en": "Importing honey",
        "fr": "Importation de miel"
    },
    "templatetype": "content page 1 column",
    "node_id": "1541623862045",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1536170455757",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/food-specific-requirements/honey/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/exigences-propres-de-certaines-denrees/miel/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Importing honey",
            "fr": "Importation de miel"
        },
        "description": {
            "en": "Honey is the sweet, viscous liquid produced by bees from the nectar of a variety of plants as well as from secretions of sap-feeding insects. The bees collect the nectar and transform it by combining with specific substances of their own.",
            "fr": "Le miel est le liquide sucr\u00e9 et visqueux produit par les abeilles \u00e0 partir du nectar de diverses plantes, mais aussi de s\u00e9cr\u00e9tions d'insectes qui se nourrissent de s\u00e8ve. Les abeilles accumulent le nectar et le transforment en le combinant \u00e0 des substances particuli\u00e8res qu'elles s\u00e9cr\u00e8tent elles\u2011m\u00eames."
        },
        "keywords": {
            "en": "importing honey, specific requirements for imported honey, standards of identity and grades for honey, labelling requirements, organic honey",
            "fr": "importation de miel, exigences sp\u00e9cifiques pour le miel import\u00e9, normes d'identit\u00e9 et qualit\u00e9s pour le miel, exigences d'\u00e9tiquetage, miel biologique"
        },
        "dcterms.subject": {
            "en": "agri-food industry,honey,regulations,food safety",
            "fr": "industrie agro-alimentaire,miel,r\u00e9glementations,salubrit\u00e9 des aliments"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-12-08",
            "fr": "2022-12-08"
        },
        "modified": {
            "en": "2022-12-08",
            "fr": "2022-12-08"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Importing honey",
        "fr": "Importation de miel"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>On this page</h2>\n\n<ul>\n<li><a href=\"#a1\">Overview</a></li>\n<li><a href=\"#a2\">Standards of identity and grades</a></li>\n<li><a href=\"#a3\">Labelling requirements</a></li>\n<li><a href=\"#a4\">Antimicrobials and veterinary drugs residue levels</a></li>\n</ul>\n\n<h2 id=\"a1\">Overview</h2>\n\n<p>Honey is the sweet, viscous liquid produced by bees from the nectar of a variety of plants as well as from secretions of sap-feeding insects. The bees collect the nectar and transform it by combining with specific substances of their own. The nectar is stored in cells in the combs and the bees work to evaporate the moisture until it is between 16% and 18%. Honey, by definition cannot include added ingredients such as colour or sugar and still be called honey (Section B.18.025, B.18.026 and B.18.027 of the <a href=\"/english/reg/jredirect2.shtml?drgr\"><i>Food and Drug Regulations</i></a>, (FDR)).</p>\n\n<p>Importers are responsible for ensuring the honey they import is safe, truthfully represented, and meets Canadian requirements. Importers are also required to source from foreign suppliers who are manufacturing, preparing, storing, packaging, and labelling the food under conditions that provide the same level of protection as those outlined in the <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\"><i>Safe Food for Canadians Regulations</i></a> (SFCR), the <i>Food and Drugs Act</i> and FDR.</p>\n\n<p>Any person who imports organic honey or markets it in Canada as an organic product must be able to demonstrate, at all times, that the product meets the certification requirements and must retain documentation attesting that the product is organic as per Part 13 of the <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\" title=\"Safe Food for Canadians Regulations\">SFCR</a>. Information on organic product requirements can be found on the <a href=\"/organic-products/eng/1526652186199/1526652186496\">Organic products</a> web page.</p>\n\n<p>Honey imported for honey bee use could have additional requirements from other Canadian regulations such as the <i>Health of Animals Act</i> and <i>Health of Animals Regulations</i>, where a zoosanitary certification or an import permit may be required. Information on these requirements can be found on the Animal Health <a href=\"/eng/1616507117282/1616507282914#s11c10\">Terrestrial Animal Products and By-products: Import Policy Framework\u00a0\u2013\u00a0Bee Products</a> web page.</p>\n\n<p>Get additional guidance on honey from the following resources.</p>\n\n<section class=\"panel panel-primary mrgn-tp-lg\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Related resources</h3>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li><a href=\"/importing-food-plants-or-animals/food-imports/step-by-step-guide/eng/1523979839705/1523979840095\">Importing Food to Canada: A Step by Step Guide</a></li>\n<li><a href=\"/food-guidance-by-commodity/honey/eng/1526655030663/1526655030943\">Food-specific requirements and guidance\u00a0\u2013\u00a0Honey</a></li>\n<li><a href=\"/preventive-controls/honey-products/eng/1511460446016/1511460473502\">Preventive controls for honey products</a></li>\n<li><a href=\"/animal-health/terrestrial-animals/imports/airs/eng/1300127512994/1326599273148\">Automated Import Reference System (AIRS)</a></li>\n</ul>\n</div>\n</section>\n\n<h2 id=\"a2\">Standards of identity and grades</h2>\n\n<p>There are grades and standards referred to in the regulations for honey which have been combined into an <a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/eng/1518625951131/1518625952071\">Inventory of documents incorporated by reference</a>. Imported honey must meet the requirements set out in the following:</p>\n\n<ul>\n<li><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-standards-of-identity-volume-5/eng/1521129625548/1521129625892\">Canadian Standards of Identity Volume 5, Honey</a></li>\n<li><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-6/eng/1523388139064/1523388171017\">Canadian Grade Compendium Volume 6, Honey</a></li>\n<li><a href=\"/about-cfia/acts-and-regulations/list-of-acts-and-regulations/documents-incorporated-by-reference/canadian-grade-compendium-volume-9/eng/1520647701525/1520647702274?chap=1#s1c1\">Grade Names for Imported Food</a> (refer to item 1 in this table for information relevant to honey)</li>\n</ul>\n\n<h2 id=\"a3\">Labelling requirements</h2>\n\n<p>Imported honey must also meet the packaging and labelling requirements set out in the SFCR. The <a href=\"/food-labels/labelling/industry/eng/1383607266489/1383607344939\">Industry labelling tool</a> is a food labelling reference for all industry that outlines the requirement for food labelling and advertising.</p>\n\n<p><a href=\"/food-labels/labelling/industry/honey/eng/1625512297254/1625512297753\">Labelling requirements for honey products</a> summarize the labelling requirements specific for honey.</p>\n\n<h2 id=\"a4\">Antimicrobials and veterinary drugs residue levels</h2>\n\n<ul class=\"lst-spcd\">\n<li>Health Canada has both <a href=\"https://www.canada.ca/en/health-canada/services/drugs-health-products/veterinary-drugs/maximum-residue-limits-mrls.html\">Maximum Residue Limits (MRLs)</a> and recommended safe <a href=\"https://www.canada.ca/en/health-canada/services/drugs-health-products/veterinary-drugs/legislation-guidelines/policies/working-residue-levels-wrls-antimicrobial-residues-honey-policy-administrative-maximum-residue-limits-working-residue-levels-veterinary-drugs-food.html\">Working Residue Levels (WRLs)</a> for a number of veterinary drugs approved for use in other food-producing animals that may be detected in domestic or imported honey</li>\n<li><a href=\"https://www.canada.ca/en/health-canada/services/drugs-health-products/veterinary-drugs/legislation-guidelines/policies/working-residue-levels-wrls-antimicrobial-residues-honey-policy-administrative-maximum-residue-limits-working-residue-levels-veterinary-drugs-food.html\">Working Residue Levels (WRLs) For Antimicrobial Residues in Honey</a> provide guidance to honey producers on residue levels which are deemed not to pose undue risk to human health\n<ul class=\"list-unstyled\">\n<li>WRLs do not represent approval of additional drugs for use in beekeeping and must not be interpreted as an encouragement of their use.</li>\n</ul></li>\n<li>Health Canada's website provides a table of <a href=\"https://www.canada.ca/en/health-canada/services/drugs-health-products/veterinary-drugs/legislation-guidelines/policies/table-current-lowest-amrls-mrls-meats-recommended-wrls-honey-policy-administrative-maximum-residue-limits-working-residue-levels-veterinary-drugs-food.html\">Current Lowest Administrative Maximum Residue Limits (AMRLs)/MRLs in Meats and Recommended WRLs in Honey</a></li>\n</ul>\r\n<!--<div class=\"alert alert-info\">\n-->\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n\n<ul>\n<li><a href=\"#a1\">Aper\u00e7u</a></li>\n<li><a href=\"#a2\">Normes d'identit\u00e9 et classification</a></li>\n<li><a href=\"#a3\">Exigences en mati\u00e8re d'\u00e9tiquetage</a></li>\n<li><a href=\"#a4\">Limites de r\u00e9sidus d'antimicrobiens et de drogues pour usage v\u00e9t\u00e8rinaire</a></li>\n</ul>\n\n<h2 id=\"a1\">Aper\u00e7u</h2>\n\n<p>Le miel est le liquide sucr\u00e9 et visqueux produit par les abeilles \u00e0 partir du nectar de diverses plantes, mais aussi de s\u00e9cr\u00e9tions d'insectes qui se nourrissent de s\u00e8ve. Les abeilles accumulent le nectar et le transforment en le combinant \u00e0 des substances particuli\u00e8res qu'elles s\u00e9cr\u00e8tent elles\u2011m\u00eames. Le nectar est entrepos\u00e9 dans les alv\u00e9oles des rayons, et les abeilles travaillent \u00e0 faire \u00e9vaporer l'humidit\u00e9 jusqu'\u00e0 ce qu'elle atteigne une valeur entre 16\u00a0% et 18\u00a0%. Par d\u00e9finition, le miel ne peut pas contenir d'ingr\u00e9dients ajout\u00e9s, comme des colorants ou du sucre. Le cas \u00e9ch\u00e9ant, il ne pourrait plus porter le nom de miel (Section B.18.025, B.18.026 et B.18.027 du <a href=\"/francais/reg/jredirect2.shtml?drgr\"><i>R\u00e8glement sur les aliments et drogues</i></a>, (RAD)).</p>\n\n<p>Les importateurs sont responsable de s'assurer que le miel qu'ils importent est salubres, est pr\u00e9sent\u00e9 de mani\u00e8re v\u00e9ridique et qu'il satisfait aux exigences canadiennes. Les importateurs sont \u00e9galement tenus de s'approvisionner aupr\u00e8s de fournisseurs \u00e9trangers qui fabriquent, conditionnent, entreposent, emballent et \u00e9tiquettent les aliments dans des conditions qui offrent le m\u00eame niveau de protection que celles d\u00e9crites dans le <a href=\"/francais/reg/jredirect2.shtml?sfcrrsac\"><i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i></a> (RSAC) et ainsi que dans la <i>Loi sur les aliments et drogues</i> et le RAD.</p>\n\n<p>Quiconque importe du miel biologique ou le commercialise au Canada en tant que produit biologique doit \u00eatre en mesure de d\u00e9montrer, en tout temps, que le produit r\u00e9pond aux exigences de certification et doit d\u00e9tenir les documents attestant qu'il est biologique conform\u00e9ment \u00e0 la partie 13 du <a href=\"/english/reg/jredirect2.shtml?sfcrrsac\" title=\"R\u00e8glement sur la salubrit\u00e9 des aliments au Canada\">RSAC</a>. Vous trouverez des renseignements sur les exigences des produits biologiques sur la page Web <a href=\"/produits-biologiques/fra/1526652186199/1526652186496\">Produits biologiques</a>.</p>\n\n<p>Le miel import\u00e9 pour utilisation avec des abeilles domestiques pourrait \u00eatre assujetti \u00e0 des exigences suppl\u00e9mentaires \u00e9nonc\u00e9es dans d'autres r\u00e8glements canadiens, comme la <i>Loi sur la sant\u00e9 des animaux</i> et le <i>r\u00e8glement sur la sant\u00e9 des animaux</i>, o\u00f9 un certificat zoosanitaire ou un permis d'importation pourrait \u00eatre requis. Vous trouverez des renseignements sur ces exigences sur la page Web <a href=\"/fra/1616507117282/1616507282914#s11c10\">Produits et sous-produits d'animaux terrestres\u00a0: Cadre sur la politique d'importation\u00a0\u2013\u00a0Produits d'abeilles (apicoles)</a> de la Sant\u00e9 des animaux.</p>\n\n<p>Vous trouverez des conseils suppl\u00e9mentaires sur le miel dans les ressources suivantes.</p>\n\n<section class=\"panel panel-primary mrgn-tp-lg\">\n<header class=\"panel-heading\">\n<h3 class=\"panel-title\">Ressources connexes</h3>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li><a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/guide-par-etape/fra/1523979839705/1523979840095\">Importer des aliments au Canada\u00a0: un guide par \u00e9tape</a></li>\n<li><a href=\"/exigences-et-documents-d-orientation-relatives-a-c/miel/fra/1526655030663/1526655030943\">Exigences et documents d'orientation relatives \u00e0 certains aliments\u00a0\u2013\u00a0Miel</a></li>\n<li><a href=\"/controles-preventifs/produits-de-miel/fra/1511460446016/1511460473502\">Contr\u00f4les pr\u00e9ventifs pour les produits de miel</a></li>\n<li><a href=\"/sante-des-animaux/animaux-terrestres/importation/sari/fra/1300127512994/1326599273148\">Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (SARI)</a></li>\n</ul>\n</div>\n</section>\n\n<h2 id=\"a2\">Normes d'identit\u00e9 et classification</h2>\n\n<p>Il existe des cat\u00e9gories et des normes dans le r\u00e8glement s'appliquant au miel qui ont \u00e9t\u00e9 regroup\u00e9es dans un <a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/fra/1518625951131/1518625952071\">Inventaire des documents incorpor\u00e9s par renvoi</a>. Le miel import\u00e9 doit satisfaire aux exigences \u00e9tablies dans les documents suivants\u00a0:</p>\n\n<ul>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/normes-d-identite-canadiennes-volume-5/fra/1521129625548/1521129625892\">Normes d'identit\u00e9 canadiennes, Volume 5\u00a0\u2013\u00a0Miel</a></li>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1523388139064/1523388171017\">Recueil des normes canadiennes de classification, Volume 6\u00a0\u2013\u00a0Miel</a></li>\n<li><a href=\"/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/documents-incorpores-par-renvoi/recueil-des-normes-canadiennes-de-classification-v/fra/1520647701525/1520647702274?chap=1#s1c1\">Noms de cat\u00e9gories applicables aux aliments import\u00e9s</a> (se reporter au point 1 de ce tableau pour de plus amples renseignements concernant le miel)</li>\n</ul>\n\n<h2 id=\"a3\">Exigences en mati\u00e8re d'\u00e9tiquetage</h2>\n\n<p>Le miel import\u00e9 doit \u00e9galement satisfaire aux exigences en mati\u00e8re d'\u00e9tiquetage et d'emballage \u00e9tablies dans le RSAC. L'<a href=\"/etiquetage-des-aliments/etiquetage/industrie/fra/1383607266489/1383607344939\">Outil d'\u00e9tiquetage de l'industrie</a> est un outil de r\u00e9f\u00e9rence en mati\u00e8re d'\u00e9tiquetage des aliments pour l'industrie qui d\u00e9crit les exigences d'\u00e9tiquetage et de publicit\u00e9.</p>\n\n<p>Les <a href=\"/etiquetage-des-aliments/etiquetage/industrie/miel/fra/1625512297254/1625512297753\">Exigences en mati\u00e8re d'\u00e9tiquetage des produits de miel</a> r\u00e9sument les exigences en mati\u00e8re d'\u00e9tiquetage sp\u00e9cifiques au miel.</p>\n\n<h2 id=\"a4\">Limites de r\u00e9sidus d'antimicrobiens et de m\u00e9dicaments v\u00e9t\u00e9rinaires</h2>\n\n<ul class=\"lst-spcd\">\n<li>Sant\u00e9 Canada a \u00e9tabli des <a href=\"https://www.canada.ca/fr/sante-canada/services/medicaments-produits-sante/medicaments-veterinaires/limites-maximales-residus.html\">Limites maximales de r\u00e9sidus (LMR)</a> et a recommand\u00e9 des <a href=\"https://www.canada.ca/fr/sante-canada/services/medicaments-produits-sante/medicaments-veterinaires/legislation-lignes-directrices/politiques/limites-fonctionnelles-residus-antimicrobiens-miel-politique-aliments-concerne-limites-maximales-administratives-residus-limites-fonctionnelles.html\">Limites fonctionnelles de r\u00e9sidus (LFR)</a> pour un certain nombre de m\u00e9dicaments v\u00e9t\u00e9rinaires dont l'utilisation est approuv\u00e9e chez d'autres animaux destin\u00e9s \u00e0 la fabrication d'aliments pouvant \u00eatre d\u00e9tect\u00e9s dans le miel domestique ou import\u00e9. </li>\n<li><a href=\"https://www.canada.ca/fr/sante-canada/services/medicaments-produits-sante/medicaments-veterinaires/legislation-lignes-directrices/politiques/limites-fonctionnelles-residus-antimicrobiens-miel-politique-aliments-concerne-limites-maximales-administratives-residus-limites-fonctionnelles.html\">Limites fonctionnelles de r\u00e9sidus (LFR) d'antimicrobiens dans le miel</a> fournissent aux producteurs de miel des lignes directrices sur les niveaux de r\u00e9sidus qui sont consid\u00e9r\u00e9s ne poser aucun risque inacceptable pour la sant\u00e9 humaine.\n<ul class=\"list-unstyled\">\n<li>Les LFR ne constituent pas une approbation de m\u00e9dicaments additionnels pour l'utilisation en apiculture et elles ne doivent pas \u00eatre consid\u00e9r\u00e9es comme un encouragement \u00e0 les utiliser.</li>\n</ul></li>\n<li>Le site Web de Sant\u00e9 Canada pr\u00e9sente un tableau des <a href=\"https://www.canada.ca/fr/sante-canada/services/medicaments-produits-sante/medicaments-veterinaires/legislation-lignes-directrices/politiques/tableau-lmar-courantes-basses-viandes-recommandees-miel-politique-aliments-concerne-limites-maximales-administratives-residus-limites-fonctionnelles.html\">Limite maximale administrative de r\u00e9sidus (LMAR) et LMR courantes les plus basses dans les viandes et des LFR recommand\u00e9es dans le miel</a>.</li>\n</ul>\r\n<!--<div class=\"alert alert-info\">\n-->\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}