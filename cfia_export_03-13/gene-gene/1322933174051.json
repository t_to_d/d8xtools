{
    "dcr_id": "1322933174051",
    "title": {
        "en": "National Surveillance for Aquatic Animal Diseases",
        "fr": "Surveillance nationale des maladies des animaux aquatiques"
    },
    "html_modified": "2024-03-12 3:55:29 PM",
    "modified": "2019-09-13",
    "issued": "2011-12-03 12:26:21",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_surveilliance_1322933174051_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/STAGING/templatedata/comn-comn/gene-gene/data/aqua_dis_surveilliance_1322933174051_fra"
    },
    "ia_id": "1322933270922",
    "parent_ia_id": "1320599059508",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1320599059508",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "National Surveillance for Aquatic Animal Diseases",
        "fr": "Surveillance nationale des maladies des animaux aquatiques"
    },
    "label": {
        "en": "National Surveillance for Aquatic Animal Diseases",
        "fr": "Surveillance nationale des maladies des animaux aquatiques"
    },
    "templatetype": "content page 1 column",
    "node_id": "1322933270922",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1299156296625",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/aquatic-animals/diseases/surveillance/",
        "fr": "/sante-des-animaux/animaux-aquatiques/maladies/surveillance/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "National Surveillance for Aquatic Animal Diseases",
            "fr": "Surveillance nationale des maladies des animaux aquatiques"
        },
        "description": {
            "en": "This information facilitates access to domestic and international markets by providing the basis for health certification and domestic disease control measures.",
            "fr": "Cette information facilite l'acc\u00e8s aux march\u00e9s nationaux et internationaux en fournissant la base pour la certification sanitaire et les mesures de contr\u00f4le des maladies \u00e0 l'\u00e9chelle nationale."
        },
        "keywords": {
            "en": "surveillance, aquatic animal diseases, aquatic animals, aquatic",
            "fr": "suveillance, maladies des animaux aquatiques, animaux aquatique, aquatiques"
        },
        "dcterms.subject": {
            "en": "exports,seafood,imports,handbooks,fish,fisheries products",
            "fr": "exportation,fruits de mer,importation,manuel,poisson,produit de la peche"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Animal Health Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction sant\u00e9 des animaux"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2011-12-03 12:26:21",
            "fr": "2011-12-03 12:26:21"
        },
        "modified": {
            "en": "2019-09-13",
            "fr": "2019-09-13"
        },
        "type": {
            "en": "reference material,policy",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence,politique"
        },
        "audience": {
            "en": "business,government",
            "fr": "entreprises,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "National Surveillance for Aquatic Animal Diseases",
        "fr": "Surveillance nationale des maladies des animaux aquatiques"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p class=\"mrgn-tp-lg\">The Canadian Food Inspection Agency monitors for <a href=\"/animal-health/aquatic-animals/diseases/eng/1299156296625/1320599059508\">aquatic animal diseases</a> and functions as the focal point for the collection, analysis and dissemination of surveillance data. The CFIA:</p>\n\n<ul>\n<li>develops surveys and sampling plans to gather the required data used to evaluate the likelihood of disease freedom, or to detect a disease</li>\n<li>develops and maintains sample collection procedures</li>\n<li>analyzes the data in the context of disease freedom for the aquatic animal diseases at different levels\u00a0 (country, zone (part of the country), or the individual facility) that is consistent with international standards set by the <a href=\"https://www.woah.org/en/home/\">World Organisation for Animals Health (WOAH; founded as <span lang=\"fr\">Office International des \u00c9pizooties</span> (OIE))</a>.</li>\n</ul>\n\n<p>The CFIA gathers the required data by building relationships and fostering existing partnerships with</p>\n\n<ul>\n<li>aquatic animal industries and other private organizations,</li>\n<li>provincial and territorial governments,</li>\n<li>aboriginal groups such as First Nations, <span lang=\"fr\">M\u00e9tis</span>, Inuit, and</li>\n<li>researchers in Canada and other countries.</li>\n</ul>\n\n<p>To date, the CFIA conducts, in collaboration with other groups, surveillance for finfish diseases in British Columbia, Manitoba, Alberta, Ontario, Quebec and Atlantic Canada. The CFIA also conducts collaborative surveillance for molluscan diseases in British Columbia and in the Atlantic area.</p>\n\n<p>Samples collected under the surveillance program are tested by the <a href=\"http://www.dfo-mpo.gc.ca/science/aah-saa/naahls-slnsas-eng.html\">National Aquatic Animal Health Laboratory System</a> in Fisheries and Oceans Canada (DFO) using testing protocols validated using international standards set by the World Organisation for Animal Health (WOAH).</p>\n\n<p>Results of surveillance activities are shared through quarterly and annual reports. This information facilitates access to domestic and international markets by providing the basis for health certification and domestic disease control measures. It also provides the necessary information for international reporting purposes, as required by the <a href=\"https://www.woah.org/en/home/\">World Organisation for Animal Health</a> (WOAH). General inquiries about the program or request for reports should be directed the <a href=\"/about-the-cfia/contact-us/contact-cfia-online/eng/1299860523723/1299860643049??aquasurv\">CFIA</a>.</p>\n\n<p>The CFIA also updates confirmed cases of <a href=\"/animal-health/aquatic-animals/diseases/reportable-diseases/federally-reportable-aquatic-animal-diseases/eng/1339174937153/1339175227861\">Federally Reportable Aquatic Animal Diseases</a> monthly. In order to receive these monthly updates, you are encouraged to subscribe to the <a href=\"https://notification.inspection.canada.ca/\">Aquatic Animal Health Email Notification Services</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p class=\"mrgn-tp-lg\">L'Agence canadienne d'inspection des aliments (ACIA) exerce une surveillance \u00e0 l'\u00e9gard des <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/fra/1299156296625/1320599059508\">maladies d'animaux aquatiques</a> et sert de centre de collecte, d'analyse et de communication des donn\u00e9es de surveillance. L'ACIA\u00a0:</p>\n\n<ul>\n<li>\u00e9labore des enqu\u00eates et des plans d'\u00e9chantillonnage afin de recueillir les donn\u00e9es n\u00e9cessaires pour d\u00e9terminer la probabilit\u00e9 d'absence de maladies ou pour d\u00e9pister une maladie;</li>\n<li>\u00e9labore et tient \u00e0 jour des proc\u00e9dures de pr\u00e9l\u00e8vement d'\u00e9chantillons;</li>\n<li>analyse les donn\u00e9es dans le contexte de l'absence de maladies \u00e0 diff\u00e9rents niveaux (dans un pays, une zone [partie d'un pays], ou un \u00e9tablissement particulier) conform\u00e9ment aux normes internationales \u00e9tablies par l'<a href=\"https://www.woah.org/fr/accueil/\">Organisation mondiale de la sant\u00e9 animale (OMSA, fond\u00e9e en tant qu'Office international des \u00e9pizooties (OIE))</a>.</li>\n</ul>\n\n<p>L'ACIA recueille les donn\u00e9es requises en \u00e9tablissant des relations et en renfor\u00e7ant les partenariats existants avec\u00a0:</p>\n\n<ul>\n<li>l'industrie des animaux aquatiques et autres organisations priv\u00e9es,</li>\n<li>les gouvernements provinciaux et territoriaux,</li>\n<li>les groupes autochtones comme les Premi\u00e8res nations, les M\u00e9tis et les Inuits,</li>\n<li>les chercheurs canadiens et internationaux.</li>\n</ul>\n\n<p>A ce jour, L'ACIA effectue, en collaboration \u00e9troite avec plusieurs organisations, une surveillance des maladies touchant les poissons en Colombie-Britannique, au Manitoba, en Alberta, en Ontario, au Qu\u00e9bec et dans les provinces de l'Atlantique. L'ACIA effectue aussi une surveillance des maladies touchant les mollusques en Colombie-Britannique et dans les provinces de l'Atlantique.</p>\n\n<p>Les \u00e9chantillons pr\u00e9lev\u00e9s dans le cadre du programme de surveillance sont analys\u00e9s par le <a href=\"http://www.dfo-mpo.gc.ca/science/aah-saa/naahls-slnsas-fra.html\">Syst\u00e8me de laboratoire national pour la sant\u00e9 des animaux aquatiques</a> du minist\u00e8re des P\u00eaches et des Oc\u00e9ans (MPO) en utilisant des protocoles d'analyse valid\u00e9s au moyen des normes internationales \u00e9tablies par l'<a href=\"https://www.woah.org/fr/accueil/\">Organisation mondiale de la sant\u00e9 animale (OMSA)</a>.</p>\n\n<p>Les r\u00e9sultats des activit\u00e9s de surveillance sont publi\u00e9s dans des rapports trimestriels et annuels. Cette information facilite l'acc\u00e8s aux march\u00e9s nationaux et internationaux en fournissant la base pour la certification sanitaire et les mesures nationales de contr\u00f4le des maladies. Les activit\u00e9s de surveillance fournissent \u00e9galement l'information n\u00e9cessaire aux fins de production de rapports \u00e0 l'\u00e9chelle internationale, selon les exigences de l'<a href=\"https://www.woah.org/fr/accueil/\">Organisation mondiale de la sant\u00e9 animale (OMSA)</a>. Les demandes de renseignements g\u00e9n\u00e9raux concernant le programme et les demandes de rapports devraient \u00eatre achemin\u00e9es \u00e0 l'<a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049?aquasurv\">ACIA</a>.</p>\n\n<p>L'ACIA fournit \u00e9galement des donn\u00e9es \u00e0 jour sur les cas confirm\u00e9s de <a href=\"/sante-des-animaux/animaux-aquatiques/maladies/maladies-declarables/maladies-des-animaux-aquatiques-a-declaration-obli/fra/1339174937153/1339175227861\">maladies des animaux aquatiques d\u00e9clarables</a> tous les mois. Afin de recevoir ces mises \u00e0 jour mensuelles, vous pouvez vous abonner aux <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">Services d'avis par courriel concernant les animaux aquatiques</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}