{
    "dcr_id": "1331749027765",
    "title": {
        "en": "Jointed goatgrass",
        "fr": "\u00c9gilope cylindrique"
    },
    "html_modified": "2024-01-26 2:21:04 PM",
    "modified": "2023-12-12",
    "issued": "2012-03-14 14:17:10",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_aegilops_1331749027765_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_factsheet_aegilops_1331749027765_fra"
    },
    "ia_id": "1331749093789",
    "parent_ia_id": "1331614823132",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1331614823132",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Jointed goatgrass",
        "fr": "\u00c9gilope cylindrique"
    },
    "label": {
        "en": "Jointed goatgrass",
        "fr": "\u00c9gilope cylindrique"
    },
    "templatetype": "content page 1 column",
    "node_id": "1331749093789",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1331614724083",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/invasive-plants/invasive-plants/jointed-goatgrass/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/plantes-envahissantes/plantes-envahissantes/egilope-cylindrique/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Jointed goatgrass",
            "fr": "\u00c9gilope cylindrique"
        },
        "description": {
            "en": "Jointed goatgrass is an invasive plant that competes with crops for water and nutrients, thus reducing the quality and yield of wheat and other crops.",
            "fr": "L?\u00e9gilope cylindrique est une plante envahissante qui rivalise avec les cultures pour acc\u00e9der \u00e0 l?eau et aux \u00e9l\u00e9ments nutritifs, r\u00e9duisant ainsi la qualit\u00e9 et le rendement des r\u00e9coltes de bl\u00e9 et d?autres cultures."
        },
        "keywords": {
            "en": "invasive plants, plant protection, Plant Protection Act, weed control, Jointed Goatgrass, aegilops cylindrica",
            "fr": "plantes envahissantes, protection des v\u00e9g\u00e9taux, Loi sur la protection des v\u00e9g\u00e9taux, contre les mauvaises herbes, \u00c9gilope cylindrique, aegilops cylindrica"
        },
        "dcterms.subject": {
            "en": "crops,environment,inspection,plants,weeds,environmental protection",
            "fr": "cultures,environnement,inspection,plante,plante nuisible,protection de l'environnement"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2012-03-14 14:17:10",
            "fr": "2012-03-14 14:17:10"
        },
        "modified": {
            "en": "2023-12-12",
            "fr": "2023-12-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "government,general public",
            "fr": "gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Jointed goatgrass",
        "fr": "\u00c9gilope cylindrique"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<div class=\"wb-lbx lbx-gal\">\n<div class=\"col-xs-4 pull-right mrgn-tp-lg\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img1_1700841971238_eng.jpg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img1_1700841971238_eng.jpg\" alt=\"\" class=\"img-responsive\">\n</a>\n</div>\n</div>\n\n<p class=\"mrgn-tp-lg\">Jointed goatgrass (<i lang=\"la\">Aegilops cylindrica</i>) is an invasive plant that competes with crops for water and nutrients, thus reducing the quality and yield of wheat and other crops. It is difficult to control in wheat because jointed goatgrass and wheat are genetically related. Both species have similar growth habits and are known to cross-pollinate with each other.</p>\n<p><a class=\"btn btn-call-to-action\" href=\"/eng/1299860523723/1299860643049#form-formulaire\">Report a jointed goatgrass sighting</a></p>\n<h2>How to spot it</h2>\n<p>To the untrained eye, jointed goatgrass resembles common grasses that grow in Canada. It is an annual grass, 40\u201360 cm tall, resembling wheat. Unlike wheat, however, jointed goatgrass has narrow cylindrical spikes and evenly spaced hairs extending from its leaf blades. The spikes are composed of a series of spikelets, each containing an average of two seeds. Uppermost spikelets usually have longer awns than the lower spikelets.</p>\n\n<div class=\"wb-lbx lbx-gal row wb-eqht\">\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img2_1700842019248_eng.jpg\" title=\"Jointed goatgrass spikelets and seed\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img2_1700842019248_eng.jpg\" alt=\"Image 1\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img3_1700842058753_eng.png\" title=\"Jointed goatgrass seedling\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img3_1700842058753_eng.png\" alt=\"Image 2\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img4_1700842093973_eng.jpg\" title=\"Attribution: Sam Brinker, OMNR-NHIC\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img4_1700842093973_eng.jpg\" alt=\"Attribution: Sam Brinker, OMNR-NHIC\" class=\"img-responsive\">\n</a>\n\t\t\n</div>\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilopsa_1331669454980_eng.jpg\" title=\"Attribution: Steve Dewey, Utah state University\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilopsa_1331669454980_eng.jpg\" alt=\"Attribution: Steve Dewey, Utah state University\" class=\"img-responsive\">\n</a>\n\t\t\n</div>\n\n<div class=\"clearfix\"></div>\n</div>\n\n<ul class=\"list-unstyled\">\n<li><strong>Image 1:</strong> Jointed goatgrass spikelets and seed</li> \n<li><strong>Image 2:</strong> Jointed goatgrass seedling</li> \n<li><strong>Image 3:</strong> Jointed goatgrass spike</li>\n<li><strong>Image 4:</strong> Jointed goatgrass hairs along leaf margins</li>\n</ul>\n<h2>How it spreads</h2>\n<p>Jointed goatgrass seeds spread primarily as contaminants in wheat seed. They can also spread on footwear worn through infested areas or by farm machinery, vehicles and when mixed in with grain, seed or straw.</p>\n \n<h2>Where it's found</h2>\n<p>In Canada, jointed goatgrass has been found in various locations in southern Ontario and British Columbia to date. Detections have been eradicated or are currently undergoing treatment. It is native to western Asia and southeastern Europe. It was introduced into the United States in contaminated seed in the 1880s, and has since become one of the most difficult weeds to control in the western states. Jointed goatgrass grows in cultivated fields, pastures and disturbed areas along fences, ditches and roadsides.</p>\n<h2>Legislation</h2>\n<p>Jointed goatgrass is regulated as a pest in Canada under the\u00a0<i>Plant Protection Act</i>. It is also listed as a prohibited noxious weed in the\u00a0<i>Weed Seeds Order</i>,\u00a02016 under the\u00a0<i>Seeds Act</i>. Importation and domestic movement of regulated plants and their propagative parts is prohibited.</p>\n<p>Various provinces including <a href=\"https://kings-printer.alberta.ca/1266.cfm?page=2010_019.cfm&amp;leg_type=Regs&amp;isbncln=9780779792474&amp;display=html\">Alberta</a>, <a href=\"https://www.bclaws.gov.bc.ca/civix/document/id/complete/statreg/66_85\">British Columbia</a>, <a href=\"https://www.saskinvasives.ca/_files/ugd/045093_b9184ad95aad49269af854f80324fe58.pdf\">Saskatchewan</a>, <a href=\"https://www.gov.mb.ca/agriculture/crops/weeds/declaration-of-noxious-weeds-in-mb.html\">Manitoba</a> and <a href=\"https://www.ontario.ca/laws/regulation/901096\">Ontario</a> also regulate the plant as an invasive species or noxious weed.</p>\n<h2>What you can do</h2>\n<p>If you think you've spotted jointed goatgrass, <a href=\"/plant-health/invasive-species/stop-the-spread/eng/1655945133110/1655945931559\">report it</a> to the Canadian Food Inspection Agency. They will follow up and determine if further action is needed.</p>\n<p>If you are in the agriculture sector:</p>\n<ul>\n<li>use clean grain, hay and straw</li>\n<li>use clean, high-quality seed that is certified if possible</li>\n<li>ensure machinery, vehicles and tools are free of soil and plant parts before moving them from one area to another</li>\n</ul>\n<h2>More information</h2>\n<ul>\n<li>Learn more about\u00a0<a href=\"/eng/1328325263410/1328325333845\">invasive species</a></li>\n<li><a href=\"/plant-health/seeds/seed-testing-and-grading/seeds-identification/aegilops-cylindrica/eng/1396452201582/1396452202582\">Weed seed fact sheet: jointed goatgrass (<i lang=\"la\">Aegilops cylindrica</i>)</a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div class=\"wb-lbx lbx-gal\">\n<div class=\"col-xs-4 pull-right mrgn-tp-lg\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img1_1700841971238_fra.jpg\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img1_1700841971238_fra.jpg\" alt=\"\" class=\"img-responsive\">\n</a>\n</div>\n</div>\n\n<p class=\"mrgn-tp-lg\">L'\u00e9gilope cylindrique (<i lang=\"la\">Aegilops cylindrica</i>) est une plante envahissante qui rivalise avec les cultures pour acc\u00e9der \u00e0 l'eau et aux \u00e9l\u00e9ments nutritifs, r\u00e9duisant ainsi la qualit\u00e9 et le rendement des r\u00e9coltes de bl\u00e9 et d'autres cultures. Il est difficile de l'\u00e9radiquer des champs de bl\u00e9, avec lequel il est g\u00e9n\u00e9tiquement apparent\u00e9. Les 2 esp\u00e8ces ont des types de croissance similaires et sont connues pour se croiser entre elles.</p>\n<p><a class=\"btn btn-call-to-action\" href=\"/eng/1299860523723/1299860643049#form-formulaire\">Signaler la pr\u00e9sence de l'\u00e9gilope cylindrique</a></p>\n  \n<h2>Comment la rep\u00e9rer</h2>\n<p>Pour un observateur non averti, l'\u00e9gilope cylindrique ressemble aux gramin\u00e9es indig\u00e8nes au Canada. Cette plante annuelle qui atteint de 40 \u00e0 60 cm de hauteur a l'apparence du bl\u00e9. Contrairement \u00e0 celui-ci, par contre, l'\u00e9gilope cylindrique porte des \u00e9pis \u00e9troits de forme cylindrique et des poils qui sont \u00e9galement r\u00e9partis le long du limbe. Les \u00e9pis sont compos\u00e9s d'une s\u00e9rie d'\u00e9pillets, contenant chacun en moyenne deux graines. Les \u00e9pillets terminaux ont habituellement des ar\u00eates plus longues que les \u00e9pillets inf\u00e9rieurs.</p>\n<div class=\"wb-lbx lbx-gal row wb-eqht\">\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img2_1700842019248_fra.jpg\" title=\"Graine et \u00e9pillet d'herbe \u00e0 \u00e9gilope cylindrique\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img2_1700842019248_fra.jpg\" alt=\"Image 1\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img3_1700842058753_fra.png\" title=\"Germe d'herbe de \u00e9gilope cylindrique\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img3_1700842058753_fra.png\" alt=\"Image 2\" class=\"img-responsive\">\n</a>\n</div>\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img4_1700842093973_fra.jpg\" title=\"Source: Sam Brinker, OMNR-NHIC\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilops_img4_1700842093973_fra.jpg\" alt=\"Image 3\" class=\"img-responsive\">\n</a>\n\t\t\n</div>\n<div class=\"col-md-3 col-sm-4 col-xs-6\">\n<a href=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilopsa_1331669454980_fra.jpg\" title=\"Source: Steve Dewy, Universit\u00e9 de l'Utah\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_factsheet_aegilopsa_1331669454980_fra.jpg\" alt=\"Image 4\" class=\"img-responsive\">\n</a>\n\t\t\n</div>\n<div class=\"clearfix\"></div>\n</div>\n\n<ul class=\"list-unstyled\">\n<li><strong>Image 1\u00a0:</strong> Graine et \u00e9pillet d'herbe \u00e0 \u00e9gilope cylindrique</li> \n<li><strong>Image 2\u00a0:</strong> Germe d'herbe de \u00e9gilope cylindrique</li> \n<li><strong>Image 3\u00a0:</strong> \u00c9pi de l'\u00e9gilope cylindrique</li>\n<li><strong>Image 4\u00a0:</strong> Poils bordant la feuille de l'\u00e9gilope cylindrique</li>\n</ul>\n<h2>Comment elle se propage</h2>\n<p>Les graines de l'\u00e9gilope cylindrique sont principalement dispers\u00e9es par contamination des semences de bl\u00e9. Elles peuvent \u00e9galement se propager par les chaussures port\u00e9es dans les zones infest\u00e9es ou \u00e0 l'aide de la machinerie agricole et apr\u00e8s avoir \u00e9t\u00e9 m\u00e9lang\u00e9es aux grains, aux semences ou \u00e0 la paille.</p>\n<h2>O\u00f9 on la trouve</h2>\n<p>Au Canada, l'\u00e9gilope cylindrique a \u00e9t\u00e9 observ\u00e9e \u00e0 ce jour en divers endroits au sud de l'Ontario et de la Colombie-Britannique. Les sites d\u00e9tect\u00e9s ont \u00e9t\u00e9 \u00e9radiqu\u00e9s ou font actuellement l'objet d'un traitement. Indig\u00e8ne de l'Ouest de l'Asie et du Sud-Est de l'Europe, cette plante a \u00e9t\u00e9 introduite aux \u00c9tats-Unis dans des semences contamin\u00e9es au cours des ann\u00e9es 1880. Depuis, elle est devenue l'une des mauvaises herbes les plus difficiles \u00e0 \u00e9liminer dans les \u00c9tats de l'Ouest. L'\u00e9gilope cylindrique pousse dans les champs cultiv\u00e9s, les p\u00e2turages et les endroits perturb\u00e9s le long des cl\u00f4tures, des foss\u00e9s et des routes.</p>\n<h2>Lois</h2>\n<p>L'\u00e9gilope cylindrique est r\u00e9 glement\u00e9e comme \u00e9tant un  organisme  nuisible  en vertu de la\u00a0<i>Loi sur la  protection des v\u00e9g\u00e9taux</i>. Il figure aussi \u00e0 la liste des mauvaises herbes  nuisibles interdites de l'Arr\u00eat\u00e9 de\u00a02016\u00a0sur les graines de mauvaises  herbes, qui s'inscrit dans la\u00a0<i>Loi sur les semences</i>. L'importation  et la circulation sur le territoire canadien de plantes r\u00e9glement\u00e9s et de leurs  parties servant \u00e0 la multiplication sont interdites.</p>\n<p>Plusieurs provinces, dont <a href=\"https://kings-printer.alberta.ca/1266.cfm?page=2010_019.cfm&amp;leg_type=Regs&amp;isbncln=9780779792474&amp;display=html\">l'Alberta (anglais seulement)</a>, <a href=\"https://www.bclaws.gov.bc.ca/civix/document/id/complete/statreg/66_85\">la Colombie-Britannique (anglais seulement)</a>, <a href=\"https://www.saskinvasives.ca/_files/ugd/045093_b9184ad95aad49269af854f80324fe58.pdf\">la Saskatchewan (anglais seulement)</a>, <a href=\"https://www.gov.mb.ca/agriculture/crops/weeds/declaration-of-noxious-weeds-in-mb.html\">le Manitoba (anglais seulement)</a>, et <a href=\"https://www.ontario.ca/laws/regulation/901096\">l'Ontario</a> r\u00e9glementent \u00e9galement la plante en tant qu'esp\u00e8ce envahissante ou mauvaise herbe nuisible.</p>\n<h2>Ce que vous pouvez faire</h2>\n<p>Si vous croyez avoir trouv\u00e9 de l'\u00e9gilope cylindrique, <a href=\"/a-propos-de-l-acia/contactez-nous/contactez-l-acia/fra/1299860523723/1299860643049\">signalez votre d\u00e9couverte</a> \u00e0 l'Agence canadienne d'inspection des aliments. L'Agence fera un suivi et d\u00e9terminera si d'autres mesures s'imposent.</p>\n<p>Si vous travaillez dans le secteur agricole\u00a0:</p>\n<ul>\n<li>utilisez des grains, du foin et de la paille propres</li>\n<li>utilisez des semences propres, de haute qualit\u00e9 et si possible certifi\u00e9es</li>\n<li>assurez-vous que la machinerie, les v\u00e9hicules et les outils sont exempts de terre et de mat\u00e9riel v\u00e9g\u00e9tal avant de les d\u00e9placer d'un endroit \u00e0 l'autre</li>\n</ul>\n<h2>Plus d'informations</h2>\n<ul>\n<li>En savoir plus sur les\u00a0<a href=\"/fra/1328325263410/1328325333845\">esp\u00e8ces envahissantes</a></li>\n<li><a href=\"/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/aegilops-cylindrica/fra/1396452201582/1396452202582\">Fiche de renseignements de la semence de l'\u00e9gilope cylindrique</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}