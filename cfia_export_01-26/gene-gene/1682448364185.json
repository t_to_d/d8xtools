{
    "dcr_id": "1682448364185",
    "title": {
        "en": "Potato varieties susceptible to potato wart",
        "fr": "Vari\u00e9t\u00e9s de pommes de terre sensibles \u00e0 la galle verruqueuse"
    },
    "html_modified": "2024-01-26 2:26:10 PM",
    "modified": "2023-05-31",
    "issued": "2023-04-27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pw_susceptible_varieties_1682448364185_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/pw_susceptible_varieties_1682448364185_fra"
    },
    "ia_id": "1682448364935",
    "parent_ia_id": "1327933793006",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1327933793006",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Potato varieties susceptible to potato wart",
        "fr": "Vari\u00e9t\u00e9s de pommes de terre sensibles \u00e0 la galle verruqueuse"
    },
    "label": {
        "en": "Potato varieties susceptible to potato wart",
        "fr": "Vari\u00e9t\u00e9s de pommes de terre sensibles \u00e0 la galle verruqueuse"
    },
    "templatetype": "content page 1 column",
    "node_id": "1682448364935",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1327933703431",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/potato-varieties/",
        "fr": "/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/varietes-de-pommes-de-terre/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Potato varieties susceptible to potato wart",
            "fr": "Vari\u00e9t\u00e9s de pommes de terre sensibles \u00e0 la galle verruqueuse"
        },
        "description": {
            "en": "Synchytrium endobioticum is an obligate parasite of certain members of the order Solanaceae with potato (Solanum tuberosum) being the only cultivated host. In Mexico some wild Solanum species are also known as hosts.",
            "fr": "Synchytrium endobioticum est un parasite oblig\u00e9 de certains membres de l'ordre des solanac\u00e9es, dont la pomme de terre (Solanum tuberosum) est le seul h\u00f4te cultiv\u00e9. Au Mexique, certaines esp\u00e8ces sauvages de Solanum sont r\u00e9put\u00e9es abriter ce champignon."
        },
        "keywords": {
            "en": "plant pest, pest surveys, quarantine pests, trapping, Synchytrium endobioticum, Potato Wart, Potato Canker",
            "fr": "phytoravageurs, enqu&#234;tes phytosanitaires, phytoparasites justiciables de quarantaine, pi&#233;geage, Synchytrium endobioticum, Galle verruqueuse de la pomme de terre, Tumeur verruqueuse de la pomme de terre"
        },
        "dcterms.subject": {
            "en": "insects,plant diseases,plants",
            "fr": "insecte,maladie des plantes,plante"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-04-27",
            "fr": "2023-04-27"
        },
        "modified": {
            "en": "2023-05-31",
            "fr": "2023-05-31"
        },
        "type": {
            "en": "fact sheet,reference material",
            "fr": "fiche de renseignements,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,scientists",
            "fr": "entreprises,gouvernement,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Potato varieties susceptible to potato wart",
        "fr": "Vari\u00e9t\u00e9s de pommes de terre sensibles \u00e0 la galle verruqueuse"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>Potato wart is extremely persistent. When present, it may reduce the harvest and quality of potato crops. It can spread through the movement of soil, farm equipment and potatoes from fields that have potato wart. Effective biosecurity programs are essential to helping contain, control and prevent spread. Careful consideration of which potato varieties to plant is also very important.</p>\n\n<p>To help manage potato wart, susceptible varieties are being proactively identified for use as specified in the <a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/pw-domestic-long-term-management-plan/eng/1645046929049/1645046929783\">Potato Wart Domestic Long Term Management Plan.</a> These are varieties that are considered susceptible to <a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/eng/1327933703431/1327933793006\"><i lang=\"la\">Synchytrium endobioticum</i></a>, the soil-borne fungus that causes potato wart.</p>\n\n<p>Growers using fields that are restricted due to detections of potato wart are encouraged to plant potato varieties that are not susceptible to this fungus, otherwise known as resistant varieties. Resistant varieties help suppress potato wart and reduce the risk of spread. The varieties currently considered resistant for fields associated with potato wart detections in Prince Edward Island (PEI) are:</p>\n\n<ul>\n<li>Althea</li>\n<li>Frontier Russet</li>\n<li>Goldrush</li>\n<li>HiLite Russet</li>\n<li>Prospect</li>\n</ul>\n\n<p>Other varieties have also been assessed for resistance. Please contact your <a href=\"/about-cfia/contact-us/eng/1546627816321/1546627838025\">local CFIA office</a> for details.</p>\n\n<p>It is recommended that growers who choose to plant susceptible potato varieties in potato wart restricted fields as specified under the Potato Wart Domestic Long Term Management Plan should:</p>\n\n<ul>\n<li>notify the CFIA, and</li>\n<li>implement on-farm surveillance activities to monitor the harvested crop for symptoms</li>\n</ul>\n\n<p>Below is a list of potato varieties susceptible to potato wart that we will update annually based on available information.</p>\n\n<ul class=\"colcount-sm-2 colcount-md-3 colcount-lg-4\">\n<li>Accent</li>\n<li>AAC Griffin Russet (V05060)</li>\n<li>AC Nova Chip</li>\n<li>Almera</li>\n<li>Andover</li>\n<li>Annabelle</li>\n<li>Atlantic</li>\n<li>Baby Boomer</li>\n<li>Bintje</li>\n<li>Calwhite</li>\n<li>Chieftain</li>\n<li>Classic Russet</li>\n<li>Clearwater Russet</li>\n<li>Coastal Russet</li>\n<li>Dakota Pearl</li>\n<li>Dakota Russet</li>\n<li>Diamant</li>\n<li>Electra</li>\n<li>Estima</li>\n<li>Eva</li>\n<li>Gemstar Russet</li>\n<li>Highland Russet</li>\n<li>HO2000 / Blushing Belle</li>\n<li>Innovator</li>\n<li>Irish Cobbler</li>\n<li>Kennebec</li>\n<li>Lady Rosetta</li>\n<li>Maritiema</li>\n<li>Markies</li>\n<li>Milva</li>\n<li>Mimi</li>\n<li>Mountain Gem Russet</li>\n<li>Norland</li>\n<li>Norvalley</li>\n<li>Pomerelle Russet</li>\n<li>Premiere</li>\n<li>Ranger Russet</li>\n<li>Rooster</li>\n<li>Russet Burbank</li>\n<li>Russet Norkotah</li>\n<li>Sangre</li>\n<li>Saxon</li>\n<li>Shepody</li>\n<li>Smart</li>\n<li>Snowdon</li>\n<li>Sundance</li>\n<li>Superior</li>\n<li>Targhee Russet</li>\n<li>Umatilla Russet</li>\n<li>Valor</li>\n<li>Victoria</li>\n<li>Vivaldi</li>\n<li>Yukon Gold</li>\n</ul>\n\n<p>Please contact your <a href=\"/about-cfia/contact-us/eng/1546627816321/1546627838025\">local CFIA office</a> if you have any questions about the use of specific potato varieties for PEI.</p>\n\n<p><a href=\"/plant-health/invasive-species/plant-diseases/potato-wart-or-potato-canker/potato-wart-in-pei/eng/1644011638795/1644012033480\">Stay up to date on the ongoing investigations of potato wart in PEI</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p>La galle verruqueuse est extr\u00eamement persistante. Lorsqu'elle est pr\u00e9sente, elle peut r\u00e9duire la r\u00e9colte et la qualit\u00e9 des cultures de pommes de terre. Elle peut se propager par les d\u00e9placements de terre, de mat\u00e9riel agricole et de pommes de terre provenant de champs atteints de galle verruqueuse. Des programmes de bios\u00e9curit\u00e9 efficaces sont essentiels pour aider \u00e0 contenir, contr\u00f4ler et pr\u00e9venir la propagation. Il est \u00e9galement tr\u00e8s important de bien choisir les vari\u00e9t\u00e9s de pommes de terre \u00e0 planter.</p>\n\n<p>Pour aider \u00e0 lutter contre la galle verruqueuse, les vari\u00e9t\u00e9s sensibles sont identifi\u00e9es de mani\u00e8re proactive en vue de leur utilisation dans le cadre du <a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/plan-canadien-de-lutte-a-long-terme-contre-la-gvpt/fra/1645046929049/1645046929783\">Plan canadien de lutte \u00e0 long terme contre la galle verruqueuse de la pomme de terre.</a> Il s'agit de vari\u00e9t\u00e9s consid\u00e9r\u00e9es comme sensibles au <a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/fra/1327933703431/1327933793006\"><i>Synchytrium endobioticum</i></a>, le champignon du sol responsable de la galle verruqueuse.</p>\n\n<p>Les producteurs qui utilisent des champs soumis \u00e0 des restrictions en raison de la d\u00e9tection de la galle verruqueuse sont encourag\u00e9s \u00e0 planter des vari\u00e9t\u00e9s de pommes de terre qui ne sont pas sensibles \u00e0 ce champignon, c'est-\u00e0-dire des vari\u00e9t\u00e9s r\u00e9sistantes. Les vari\u00e9t\u00e9s r\u00e9sistantes contribuent \u00e0 supprimer la galle verruqueuse et \u00e0 r\u00e9duire le risque de propagation. Les vari\u00e9t\u00e9s actuellement consid\u00e9r\u00e9es comme r\u00e9sistantes pour les champs associ\u00e9s \u00e0 des d\u00e9tections de galle verruqueuse dans l'\u00cele-du-Prince-\u00c9douard (\u00ce.-P.-\u00c9.) sont les suivantes :</p>\n\n<ul><li>Althea</li>\n<li>Frontier Russet</li>\n<li>Goldrush</li>\n<li>Hilite Russet</li>\n<li>Prospect</li>\n</ul>\n\n<p>D'autres vari\u00e9t\u00e9s ont \u00e9galement \u00e9t\u00e9 \u00e9valu\u00e9es pour leur r\u00e9sistance. Veuillez contacter votre <a href=\"/a-propos-de-l-acia/contactez-nous/fra/1546627816321/1546627838025\">bureau local de l'ACIA</a> pour plus de d\u00e9tails.</p>\n\n<p>Il est recommand\u00e9 aux producteurs qui choisissent de planter des vari\u00e9t\u00e9s de pommes de terre sensibles dans des champs soumis \u00e0 des restrictions concernant la galle verruqueuse dans le cadre du Plan canadien de lutte \u00e0 long terme contre la galle verruqueuse de la pomme de terre\u00a0:</p>\n\n<ul>\n<li>d'en aviser l'ACIA, et</li>\n<li>de mettre en \u0153uvre des activit\u00e9s de surveillance \u00e0 la ferme afin de d\u00e9pister les sympt\u00f4mes sur la culture r\u00e9colt\u00e9e</li>\n</ul>\n\n<p>Vous trouverez ci-dessous une liste des vari\u00e9t\u00e9s de pommes de terre sensibles \u00e0 la galle verruqueuse que nous mettrons \u00e0 jour chaque ann\u00e9e en fonction des informations disponibles.</p>\n\n<ul class=\"colcount-sm-2 colcount-md-3 colcount-lg-4\">\n<li>Accent</li>\n<li>AAC Griffin Russet (V05060)</li>\n<li>AC Nova Chip</li>\n<li>Almera</li>\n<li>Andover</li>\n<li>Annabelle</li>\n<li>Atlantic</li>\n<li>Baby Boomer</li>\n<li>Bintje</li>\n<li>Calwhite</li>\n<li>Chieftain</li>\n<li>Classic Russet</li>\n<li>Clearwater Russet</li>\n<li>Coastal Russet</li>\n<li>Dakota Pearl</li>\n<li>Dakota Russet</li>\n<li>Diamant</li>\n<li>Electra</li>\n<li>Estima</li>\n<li>Eva</li>\n<li>Gemstar Russet</li>\n<li>Highland Russet</li>\n<li>HO2000 / Blushing Belle</li>\n<li>Innovator</li>\n<li>Irish Cobbler</li>\n<li>Kennebec</li>\n<li>Lady Rosetta</li>\n<li>Maritiema</li>\n<li>Markies</li>\n<li>Milva</li>\n<li>Mimi</li>\n<li>Mountain Gem Russet</li>\n<li>Norland</li>\n<li>Norvalley</li>\n<li>Pomerelle Russet</li>\n<li>Premiere</li>\n<li>Ranger Russet</li>\n<li>Rooster</li>\n<li>Russet Burbank</li>\n<li>Russet Norkotah</li>\n<li>Sangre</li>\n<li>Saxon</li>\n<li>Shepody</li>\n<li>Smart</li>\n<li>Snowdon</li>\n<li>Sundance</li>\n<li>Superior</li>\n<li>Targhee Russet</li>\n<li>Umatilla Russet</li>\n<li>Valor</li>\n<li>Victoria</li>\n<li>Vivaldi</li>\n<li>Yukon Gold</li>\n</ul>\n\t\n<p>Veuillez contacter <a href=\"/a-propos-de-l-acia/contactez-nous/fra/1546627816321/1546627838025\">votre bureau local de l'ACIA</a> si vous avez des questions sur l'utilisation de certaines vari\u00e9t\u00e9s de pommes de terre pour l'\u00ce.-P.-\u00c9.</p>\n\n<p><a href=\"/protection-des-vegetaux/especes-envahissantes/maladies/galle-verruqueuse-de-la-pomme-de-terre/la-galle-verruqueuse-de-la-pomme-de-terre-a-ipe/fra/1644011638795/1644012033480\">Tenez-vous au courant des enqu\u00eates en cours sur la galle verruqueuse de la pomme de terre \u00e0 l'\u00ce.-P.-\u00c9.</a></p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}