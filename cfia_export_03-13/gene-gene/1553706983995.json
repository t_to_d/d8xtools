{
    "dcr_id": "1553706983995",
    "title": {
        "en": "Declaring regulated goods returning to Canada",
        "fr": "D\u00e9clarer des aliments r\u00e9glement\u00e9s qui sont retourn\u00e9s au Canada"
    },
    "html_modified": "2024-03-12 3:58:23 PM",
    "modified": "2019-04-02",
    "issued": "2019-03-28",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_imports_canadian_goods_returning_1553706983995_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/food_imports_canadian_goods_returning_1553706983995_fra"
    },
    "ia_id": "1553706984250",
    "parent_ia_id": "1364059265637",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1364059265637",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Declaring regulated goods returning to Canada",
        "fr": "D\u00e9clarer des aliments r\u00e9glement\u00e9s qui sont retourn\u00e9s au Canada"
    },
    "label": {
        "en": "Declaring regulated goods returning to Canada",
        "fr": "D\u00e9clarer des aliments r\u00e9glement\u00e9s qui sont retourn\u00e9s au Canada"
    },
    "templatetype": "content page 1 column",
    "node_id": "1553706984250",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1364059150360",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/importing-food-plants-or-animals/food-imports/nisc/canadian-goods-returning/",
        "fr": "/importation-d-aliments-de-vegetaux-ou-d-animaux/importation-d-aliments/csni/marchandises-canadiennes-de-retour/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Declaring regulated goods returning to Canada",
            "fr": "D\u00e9clarer des aliments r\u00e9glement\u00e9s qui sont retourn\u00e9s au Canada"
        },
        "description": {
            "en": "Declaring regulated goods returning to Canada",
            "fr": "D\u00e9clarer des aliments r\u00e9glement\u00e9s qui sont retourn\u00e9s au Canada"
        },
        "keywords": {
            "en": "imports, commercial imports, declaring, CFIA, regulated goods, Canadian Goods Returning",
            "fr": "importation, importateurs commerciaux, d\u00e9claration, marchandises r\u00e9glement\u00e9es, Agence canadienne d'inspection des aliments, marchandises, canadiennes, retour"
        },
        "dcterms.subject": {
            "en": "imports,inspection",
            "fr": "importation,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Operations Strategy and Delivery",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Strat\u00e9gie et prestation des Op\u00e9rations"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2019-03-28",
            "fr": "2019-03-28"
        },
        "modified": {
            "en": "2019-04-02",
            "fr": "2019-04-02"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Declaring regulated goods returning to Canada",
        "fr": "D\u00e9clarer des aliments r\u00e9glement\u00e9s qui sont retourn\u00e9s au Canada"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>As an importer or customs broker, it is your responsibility to include the correct information in your import declaration for CFIA regulated goods returning to Canada.</p>\n\n<p>An example of a Canadian good returning is a Canadian good that has gone across the border into another country and is being shipped back to Canada.</p>\n\n<h2>Code\u00a098.13 and\u00a098.14</h2>\n\n<p>If you are declaring CFIA regulated goods under Harmonized System (HS) Code 98.13 or 98.14, this must be done as a paper declaration to the Canada Border Services Agency (CBSA). See CBSA <a href=\"https://www.cbsa-asfc.gc.ca/publications/dm-md/d17/d17-1-4-eng.html#_a17\">D17-1-4</a>\u00a0\u2013 Exceptions to mandatory Electronic Data Interchange (EDI).</p>\n\n<p>All other declaration for CFIA regulated goods returning to Canada can be submitted electronically.</p>\n\n<h2>Electronic declaration using the Integrated Import Declaration (IID)</h2>\n\n<p>There are 3 main items that a commercial importer or customs broker must include on their declaration for CFIA regulated goods returning to Canada.</p>\n\n<p>When declaring CFIA regulated goods returning to Canada using the EDI <a href=\"https://www.cbsa-asfc.gc.ca/prog/sw-gu/menu-eng.html\">Integrated Import Declaration</a> (IID) message, make sure to:</p>\n\n<ol class=\"lst-spcd\">\n\n<li><strong>Declare the</strong> <a href=\"https://www.cbsa-asfc.gc.ca/import/origin-origine-eng.html\">Country of Origin</a> as Canada. This will be used for tariff purposes. This can be done by providing the Country of Origin at the Government Agency Goods Item Details (GAGI) level (SG104.LOC) and/or commodity level (SG118.LOC) using qualifier\u00a027 in element\u00a03227.</li>\n\n\n<li><strong>Declare the country the product is being exported from</strong> in the Country of Source field. This can be done by providing the Country of Source at the GAGI level (SG104.LOC) and/or commodity level (SG118.LOC) using qualifier\u00a030 in element\u00a03227.</li>\n\n\n<li><p><strong>Declare using</strong> Canadian Animals Returning to Canada, Canadian Goods Returning to Canada or Canadian Horses Returning to Canada <strong>End Use</strong> (if available) that is identified in Automated Import Reference System (<a href=\"/importing-food-plants-or-animals/plant-and-plant-product-imports/airs/eng/1300127512994/1300127627409\">AIRS</a>) for the HS code and AIRS Extension combination being declared. This can be done by providing the AIRS End Use at the commodity level (SG118.GIN) using qualifier A02 in element 7402.</p>\n\n<p><strong>Note:</strong> if Canadian Animals Returning to Canada, Canadian Goods Returning to Canada or Canadian Horses Returning to Canada is not available, please select the most applicable end use for the HS code and AIRS Extension combination being declared.</p></li>\n</ol>\n\n<p>For questions related to declaring CFIA regulated goods, please call the CFIA's National Import Service Centres at\u00a01-800-835-4486 (Mississauga) or\u00a01-877-493-0468 (Montreal).</p>\n\n<p>To stay updated on Agency news or changes that affect the import community, subscribe to our <a href=\"https://notification.inspection.canada.ca/\">Imports\u00a0\u2013 AIRS, EDI and Import Service Centre (ISC) updates and notices</a>.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>En tant qu'importateur ou de courtier en douane, il est votre responsabilit\u00e9 d'inclure les renseignements exacts dans votre d\u00e9claration d'importation pour vos marchandises r\u00e9glement\u00e9es de retour au Canada.</p>\n\n<p>Un exemple d'une marchandise canadienne de retour au Canada serait une marchandise canadienne qui a travers\u00e9 une fronti\u00e8re internationale pour ensuite \u00eatre r\u00e9exp\u00e9di\u00e9e au Canada.</p>\n\n<h2>Classement tarifaire\u00a098.13 et\u00a098.14</h2>\n\n<p>Si vous d\u00e9clarez une marchandise r\u00e9glement\u00e9e par l'Agence canadienne d'inspection des aliments (ACIA) admissibles au classement tarifaire\u00a098.13 ou\u00a098.14 du Syst\u00e8me harmonis\u00e9 de d\u00e9signation et de codification des marchandises, cette d\u00e9claration doit \u00eatre effectu\u00e9e par \u00e9crit \u00e0 l'Agence des services frontaliers du Canada (ASFC). Veuillez consulter le M\u00e9morandum\u00a0<a href=\"https://www.cbsa-asfc.gc.ca/publications/dm-md/d17/d17-1-4-fra.html#_a17\" class=\"nowrap\">D17-1-4</a> de l'ASFC, Exceptions de l'\u00c9change de donn\u00e9es informatis\u00e9 (EDI).</p>\n\n<p>Toutes les autres d\u00e9clarations pour des marchandises r\u00e9glement\u00e9es par l'ACIA de retour au Canada peuvent \u00eatre soumises \u00e9lectroniquement.</p>\n\n<h2>D\u00e9claration \u00e9lectronique \u00e0 l'aide du syst\u00e8me de D\u00e9claration int\u00e9gr\u00e9e des importations (DII)</h2>\n\n<p>Il existe trois \u00e9l\u00e9ments principaux qu'un importateur commercial ou un courtier en douane doit inclure dans leurs d\u00e9clarations d'importation de marchandises r\u00e9glement\u00e9es par l'ACIA de retour au Canada.</p>\n\n<p>Lorsque vous effectuez une d\u00e9claration pour des marchandises r\u00e9glement\u00e9es par l'ACIA de retour au Canada \u00e0 l'aide du syst\u00e8me de <a href=\"https://www.cbsa-asfc.gc.ca/prog/sw-gu/menu-fra.html\">D\u00e9claration int\u00e9gr\u00e9e des importations</a> (DII) de l'\u00c9change de donn\u00e9es informatis\u00e9 (EDI), veuillez vous assurer que\u00a0:</p>\n\n<ol class=\"lst-spcd\">\n\n<li><strong>Vous avez d\u00e9clar\u00e9 le Canada comme</strong> <a href=\"https://www.cbsa-asfc.gc.ca/import/origin-origine-fra.html\">Pays d'origine</a>. Ce renseignement sera utilis\u00e9 \u00e0 des fins de tarification. Cette d\u00e9claration peut \u00eatre effectu\u00e9e en fournissant le Pays d'origine au niveau des D\u00e9tails d'article de marchandise pour organisme gouvernemental (DAMOG) (SG104.LOC) ou au niveau des marchandises (SG118.LOC) en utilisant la valeur\u00a027 de l'\u00e9l\u00e9ment\u00a03227.</li>\n\n\n<li><strong>Vous d\u00e9clarez le pays \u00e0 compter duquel le produit est export\u00e9</strong> dans le champ du Pays de provenance. Cette d\u00e9claration peut \u00eatre effectu\u00e9e en fournissant le Pays de provenance au niveau des D\u00e9tails d'article de marchandise pour organisme gouvernemental (DAMOG) (SG104.LOC) ou au niveau des marchandises (SG118.LOC) en utilisant la valeur\u00a030 de l'\u00e9l\u00e9ment\u00a03227.</li>\n\n\n<li><p><strong>D\u00e9clarez en utilisant l'usage final</strong> Animaux canadiens revenant au Canada, Produits canadiens revenant au Canada ou Chevaux canadiens revenant au Canada (si disponible) tel qu'identifi\u00e9 dans le Syst\u00e8me automatis\u00e9 de r\u00e9f\u00e9rence \u00e0 l'importation (<a href=\"/importation-d-aliments-de-vegetaux-ou-d-animaux/importations-de-vegetaux-et-de-produits-vegetaux/sari/fra/1300127512994/1300127627409\">SARI</a>) en utilisant le Syst\u00e8me harmonis\u00e9 (SH) et la combinaison de prolongement du SARI qui sont d\u00e9clar\u00e9s. Cela peut \u00eatre r\u00e9alis\u00e9 en fournissant l'usage final dans le SARI au niveau des marchandises (SG118.LOC) en utilisant la valeur A02 de l'\u00e9l\u00e9ment 7402.</p>\n<p><strong>Note\u00a0:</strong> Si les usages finaux Animaux canadiens revenant au Canada, Produits canadiens revenant au Canada ou Chevaux canadiens revenant au Canada ne sont pas disponibles, veuillez s\u00e9lectionner l'usage final s'appliquant au code SH et la combinaison de prolongement du SARI qui sont d\u00e9clar\u00e9s.</p></li>\n</ol>\n\n<p>Pour les questions associ\u00e9es \u00e0 la d\u00e9claration de marchandises r\u00e9glement\u00e9es par l'ACIA, veuillez communiquer avec le Centre de service national \u00e0 l'importation (CSNI) au 1-800-835-4486 (Mississauga) ou au 1-877-493-0468 (Montr\u00e9al).</p>\n\n<p>Pour demeurer \u00e0 jour au sujet des nouvelles de l'Agence ou de modifications qui affectent la communaut\u00e9 des importations, veuillez vous abonner \u00e0 notre service d'avis par courriel\u00a0: <a href=\"https://notification.inspection.canada.ca/CfiaListserv/Index/fr-CA\">Importations\u00a0: mises \u00e0 jour et avis sur le SARI, l'EDI et le CSI</a>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}