{
    "dcr_id": "1687969580006",
    "title": {
        "en": "Industry Notice \u2013 Detection of bovine tuberculosis in Saskatchewan",
        "fr": "Avis \u00e0 l'industrie \u2013 D\u00e9tection de la tuberculose bovine en Saskatchewan"
    },
    "html_modified": "28-6-2023",
    "modified": "12-7-2023",
    "issued": "28-6-2023",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/industry_notice_detection_bovine_tuberculosis_saskatchewan_1687969580006_eng",
        "fr": "/default/main/inspection/cont-cont/animals-animaux/WORKAREA/animals-animaux/templatedata/comn-comn/gene-gene/data/industry_notice_detection_bovine_tuberculosis_saskatchewan_1687969580006_fra"
    },
    "parent_ia_id": "1687968532994",
    "ia_id": "1687969846737",
    "parent_node_id": null,
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Industry Notice \u2013 Detection of bovine tuberculosis in Saskatchewan",
        "fr": "Avis \u00e0 l'industrie \u2013 D\u00e9tection de la tuberculose bovine en Saskatchewan"
    },
    "label": {
        "en": "Industry Notice \u2013 Detection of bovine tuberculosis in Saskatchewan",
        "fr": "Avis \u00e0 l'industrie \u2013 D\u00e9tection de la tuberculose bovine en Saskatchewan"
    },
    "templatetype": "content page 1 column",
    "node_id": "1687969846737",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "parent_dcr_id": "1687968532228",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/animal-health/terrestrial-animals/diseases/reportable/bovine-tuberculosis/saskatchewan-2023/detection/",
        "fr": "/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tuberculose-bovine/saskatchewan-2023/detection/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Industry Notice \u2013 Detection of bovine tuberculosis in Saskatchewan",
            "fr": "Avis \u00e0 l'industrie \u2013 D\u00e9tection de la tuberculose bovine en Saskatchewan"
        },
        "description": {
            "en": "The CFIA is working closely with the producers, industry associations, and provincial and federal agricultural and health authorities throughout the investigation in Saskatchewan.",
            "fr": "L'ACIA travaille en \u00e9troite collaboration avec les producteurs, les associations de l'industrie et les autorit\u00e9s agricoles et sanitaires provinciales et f\u00e9d\u00e9rales tout au long de l'enqu\u00eate en Saskatchewan."
        },
        "keywords": {
            "en": "Bovine tuberculosis, animal health, animal disease, Saskatchewan, reportable disease",
            "fr": "Tuberculose bovine, sante animale, maladies des animaux, Saskatchewan, d\u00e9claration obligatoire"
        },
        "dcterms.subject": {
            "en": "livestock,inspection,animal diseases,veterinary medicine,regulation,animal health",
            "fr": "b\u00e9tail,inspection,maladie animale,m\u00e9decine v\u00e9t\u00e9rinaire,r\u00e9glementation,sant\u00e9 animale"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-06-28",
            "fr": "2023-06-28"
        },
        "modified": {
            "en": "2023-07-12",
            "fr": "2023-07-12"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Industry Notice \u2013 Detection of bovine tuberculosis in Saskatchewan",
        "fr": "Avis \u00e0 l'industrie \u2013 D\u00e9tection de la tuberculose bovine en Saskatchewan"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p><strong>June 28, 2023 \u2013 Ottawa, Ontario</strong></p>\n<p>The Canadian Food Inspection Agency (CFIA) has launched an investigation into cases of bovine tuberculosis (bovine TB) in a single cattle herd in Saskatchewan.</p>\n<p>Bovine TB is a reportable disease in Canada and has been subject to a mandatory national eradication program since 1923. While Canada is considered to be officially free of bovine TB today, isolated cases in cattle may occur.</p>\n<p>Human cases of bovine TB are very rare. Exposure can occur through the passage of fluids from an infected animal to an open skin sore, extended close contact with an animal with active respiratory TB or consuming raw or unpasteurized animal products (such as unpasteurized milk) from an infected animal. Generally, bovine TB does not pose a threat to public health in Canada because of the extremely low prevalence of the disease, the abattoir surveillance and testing programs in place, and practices such as pasteurization of milk.</p>\n<h2>Overview</h2>\n<p>On February 23, 2023, the United States Department of Agriculture (USDA) notified the CFIA that tissues collected at slaughter from a heifer originating from Canada had a positive polymerase chain reaction (PCR) test for bovine tuberculosis. The animal was exported from Saskatchewan in September 2022 and was in a US feedlot until its slaughter.</p>\n<p>The CFIA traced the movements of the animal in Canada and determined the herd of origin, which was placed under quarantine until testing could be completed after the spring calving season. The CFIA determined that the animal had not spent time on other farm premises in Canada. The animal spent 5 months in a feedlot before being exported. All of the Canadian feedlot contact animals were destined for slaughter and were not moved to other farms.</p>\n<p>In May 2023, all animals over 6 months of age in the herd of origin were tested for bovine TB and the reactor animals were removed for slaughter and post-mortem examination for signs of the disease. Tissues from the suspect animals were shipped to the\u00a0<a href=\"/science-and-research/our-laboratories/ottawa-fallowfield-/eng/1549570125940/1549570207759\">CFIA's Ottawa Laboratory\u2013Fallowfield</a> where PCR testing confirmed 2 cases of bovine TB on June 19.</p>\n<p>The CFIA is continuing to work closely with the producers, industry associations, and provincial and federal agricultural and health authorities throughout the investigation.</p>\n<p>This finding should not affect Canada's current international status in which all provinces are considered bovine TB-free. This status supports international trade for Canada's beef industry.</p>\n<h2>Process and next steps</h2>\n<p>In all cases where federally-regulated diseases are suspected or confirmed, the goal is to take appropriate and prudent control measures while minimizing disruptions to producers.</p>\n<p>During a bovine TB investigation, quarantines and movement restrictions are placed on any implicated animals. Testing, humane destruction, and disposal are carried out as required. All of the animals in the infected herd will be humanely destroyed.</p>\n<p>The CFIA is in the very early stages of its investigation. This involves identifying all herds that have come in contact with the infected animal during its life. The CFIA has also begun testing to identify the strain of the bacterium as this may inform if there are connections to previous cases.</p>\n<p>As the investigation proceeds, the CFIA will trace the movement of animals to and from the infected herd during the past 5 years to identify and eliminate the source and any potential spread of the disease. Because the investigation is in the early stages, the exact number of herds involved and the time to complete the investigation are not yet known.</p>\n<p>Detailed information on\u00a0<a href=\"/animal-health/terrestrial-animals/diseases/reportable/bovine-tuberculosis/eng/1330205978967/1330206128556\">bovine tuberculosis</a>\u00a0and what can be expected during an investigation is available on the CFIA website. Investigation updates will be posted as more information becomes available.</p>\n<h2>Compensation</h2>\n<p>Producers are eligible for compensation for any animals ordered destroyed by the CFIA as part of this investigation.</p>\n<p>In addition, under the Sustainable Canadian Agricultural Partnership, there is a suite of business risk management (BRM) programs available, including AgriInvest and AgriStability, to help farmers manage risk due to severe market volatility and disaster situations that are largely beyond their capacity to manage. Agriculture and Agri-Food Canada is the lead federal department for these programs.</p>\n<h2>A collaborative approach</h2>\n<p>Disease investigations require a great deal of cooperation and collaboration.</p>\n<p>Affected producers and industry associations, as well as federal and provincial departments, are cooperating in the ongoing investigation. The common goal is to protect human health, protect the health of Canadian livestock and, in the process, maintain market access.</p>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<p><strong>28 juin 2023 \u2013 Ottawa, Ontario</strong></p>\n<p>L'Agence canadienne d'inspection des aliments (ACIA) a ouvert une enqu\u00eate sur des cas de tuberculose bovine (TB bovine) dans un seul troupeau bovin en Saskatchewan.</p>\n<p>Au Canada, la tuberculose bovine est une maladie \u00e0 d\u00e9claration obligatoire qui fait l'objet d'un programme national d'\u00e9radication obligatoire depuis 1923. Bien que le Canada soit consid\u00e9r\u00e9 comme officiellement exempt de tuberculose bovine aujourd'hui, des cas isol\u00e9s peuvent survenir chez les bovins.</p>\n<p>Les cas humains de tuberculose bovine sont tr\u00e8s rares. L'exposition peut se produire par le passage de liquides d'un animal infect\u00e9 \u00e0 une plaie cutan\u00e9e ouverte, par un contact \u00e9troit prolong\u00e9 avec un animal atteint de tuberculose respiratoire active ou par la consommation de produits animaux crus ou non pasteuris\u00e9s (comme le lait non pasteuris\u00e9) d'un animal infect\u00e9. En g\u00e9n\u00e9ral, la tuberculose bovine ne constitue pas une menace pour la sant\u00e9 publique au Canada en raison de la pr\u00e9valence extr\u00eamement faible de la maladie, des programmes de surveillance et d'analyse en abattoir en place et de pratiques telles que la pasteurisation du lait.</p>\n<h2>Aper\u00e7u</h2>\n<p>Le 23 f\u00e9vrier 2023, le d\u00e9partement de l'Agriculture des \u00c9tats-Unis (USDA) a avis\u00e9 l'ACIA que des tissus pr\u00e9lev\u00e9s \u00e0 l'abattage sur une g\u00e9nisse originaire du Canada avaient fait l'objet d'un test de r\u00e9action en cha\u00eene par polym\u00e9rase (PCR) positif pour la tuberculose bovine.\u00a0L'animal a \u00e9t\u00e9 export\u00e9 de la Saskatchewan en septembre 2022 et \u00e9tait dans un parc d'engraissement am\u00e9ricain jusqu'\u00e0 son abattage.</p>\n<p>L'ACIA a retrac\u00e9 les d\u00e9placements de l'animal au Canada et d\u00e9termin\u00e9 le troupeau d'origine, qui a \u00e9t\u00e9 plac\u00e9 en quarantaine jusqu'\u00e0 ce que les tests puissent \u00eatre termin\u00e9s apr\u00e8s la saison de v\u00ealage du printemps. L'ACIA a d\u00e9termin\u00e9 que l'animal n'avait pas pass\u00e9 de temps dans d'autres exploitations agricoles au Canada. L'animal a pass\u00e9 5 mois dans un parc d'engraissement avant d'\u00eatre export\u00e9. Tous les animaux en contact dans le parc d'engraissement canadien \u00e9taient destin\u00e9s \u00e0 l'abattage et n'ont pas \u00e9t\u00e9 d\u00e9plac\u00e9s vers d'autres fermes.</p>\n<p>En mai 2023, tous les animaux du troupeau d'origine \u00e2g\u00e9s de plus de 6 mois ont \u00e9t\u00e9 test\u00e9s pour la tuberculose bovine et les animaux r\u00e9acteurs ont \u00e9t\u00e9 retir\u00e9s pour abattage et examen post-mortem pour d\u00e9celer des\u00a0 signes de la maladie.\u00a0 Les tissus des animaux suspects ont \u00e9t\u00e9 exp\u00e9di\u00e9s au <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/ottawa-fallowfield-/fra/1549570125940/1549570207759\">Laboratoire d'Ottawa de l'ACIA, \u00e0 Fallowfield</a>, o\u00f9 les tests PCR ont confirm\u00e9 2 cas de tuberculose bovine le 19 juin.</p>\n<p>L'ACIA continue de travailler en \u00e9troite collaboration avec les producteurs, les associations de l'industrie et les autorit\u00e9s agricoles et sanitaires provinciales et f\u00e9d\u00e9rales tout au long de l'enqu\u00eate.</p>\n<p>Cette d\u00e9couverte ne devrait pas avoir d'incidence sur le statut international actuel du Canada, o\u00f9 toutes les provinces sont consid\u00e9r\u00e9es comme indemnes de tuberculose bovine. Ce statut appuie les activit\u00e9s commerciales internationales de l'industrie canadienne du b\u0153uf.</p>\n<h2>Processus et prochaines \u00e9tapes</h2>\n<p>Chaque fois qu'un cas de maladie \u00e0 d\u00e9claration obligatoire au Canada est soup\u00e7onn\u00e9 ou confirm\u00e9, l'objectif est de prendre des mesures de lutte ad\u00e9quates et prudentes, tout en veillant \u00e0 ce que les activit\u00e9s du producteur vis\u00e9 soient perturb\u00e9es le moins possible.</p>\n<p>Pendant une enqu\u00eate sur la tuberculose bovine, les animaux touch\u00e9s sont plac\u00e9s en quarantaine et leurs d\u00e9placements font l'objet de restrictions. Selon les besoins, on proc\u00e8de \u00e0 des tests, \u00e0 l'abattage sans cruaut\u00e9 et \u00e0 l'\u00e9limination des carcasses. Tous les animaux du troupeau infect\u00e9 seront d\u00e9truits sans cruaut\u00e9.</p>\n<p>L'ACIA en est encore aux premi\u00e8res \u00e9tapes de son enqu\u00eate. Elle doit trouver tous les troupeaux qui ont \u00e9t\u00e9 en contact avec l'animal infect\u00e9 au cours de sa vie. L'ACIA a aussi commenc\u00e9 \u00e0 effectuer des analyses pour identifier la souche de la bact\u00e9rie, car cela pourrait indiquer s'il existe des liens avec des cas ant\u00e9rieurs.</p>\n<p>Avec la progression de l'enqu\u00eate, l'ACIA proc\u00e9dera \u00e0 des activit\u00e9s de tra\u00e7age des d\u00e9placements des animaux \u00e0 destination et en provenance du troupeau infect\u00e9 au cours des 5 derni\u00e8res ann\u00e9es, ce qui lui permettra d'identifier et d'\u00e9liminer la source, et d'emp\u00eacher toute propagation possible de la maladie. Comme l'enqu\u00eate n'en est qu'\u00e0 ses d\u00e9buts, on ne conna\u00eet pas encore le nombre exact de troupeaux touch\u00e9s ni le temps qu'il faudra pour mener \u00e0 bien l'enqu\u00eate.</p>\n<p>Des renseignements d\u00e9taill\u00e9s sur la\u00a0<a href=\"/sante-des-animaux/animaux-terrestres/maladies/declaration-obligatoire/tuberculose-bovine/fra/1330205978967/1330206128556\">tuberculose bovine</a>\u00a0et sur ce \u00e0 quoi on peut s'attendre au cours d'une enqu\u00eate sont disponibles sur le site Web de l'ACIA. Des mises \u00e0 jour sur l'enqu\u00eate seront publi\u00e9es au fur et \u00e0 mesure que de nouveaux renseignements seront disponibles.</p>\n<h2>Indemnisation</h2>\n<p>Les producteurs sont admissibles \u00e0 une indemnisation pour tout animal dont la destruction a \u00e9t\u00e9 ordonn\u00e9e par l'ACIA dans le cadre de cette enqu\u00eate.</p>\n<p>De plus, dans le cadre du Partenariat canadien pour une agriculture durable, il existe une s\u00e9rie de programmes de gestion des risques de l'entreprise (GRE), tels qu'Agri-investissement et Agri-stabilit\u00e9, pour aider les producteurs \u00e0 g\u00e9rer les risques li\u00e9s \u00e0 la grande volatilit\u00e9 des march\u00e9s et aux catastrophes qui d\u00e9passent largement leur capacit\u00e9 de gestion. Agriculture et Agroalimentaire Canada est le minist\u00e8re f\u00e9d\u00e9ral responsable de ces programmes.</p>\n<h2>Une approche ax\u00e9e sur la collaboration</h2>\n<p>Les enqu\u00eates sur les maladies n\u00e9cessitent beaucoup de coop\u00e9ration et de collaboration.</p>\n<p>Les producteurs et les associations industrielles touch\u00e9s, ainsi que les minist\u00e8res f\u00e9d\u00e9raux et provinciaux, collaborent \u00e0 l'enqu\u00eate en cours. L'objectif commun est de prot\u00e9ger la sant\u00e9 humaine et de prot\u00e9ger la sant\u00e9 du b\u00e9tail canadien, tout en maintenant l'acc\u00e8s aux march\u00e9s.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "chat_wizard": false,
    "success": true
}