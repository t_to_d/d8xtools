{
    "dcr_id": "1476285096491",
    "title": {
        "en": "Weed Seed: <i lang=\"la\">Linaria dalmatica</i> (Dalmatian toadflax)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria dalmatica</i> (Linaire \u00e0 feuilles larges)"
    },
    "html_modified": "2024-03-12 3:57:30 PM",
    "modified": "2017-11-01",
    "issued": "2017-11-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_linaria_dalmatica_1476285096491_eng",
        "fr": "/default/main/inspection/cont-cont/plants-vegetaux/STAGING/templatedata/comn-comn/gene-gene/data/invasive_plants_seed_factsheet_linaria_dalmatica_1476285096491_fra"
    },
    "ia_id": "1476285096959",
    "parent_ia_id": "1333136685768",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1333136685768",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Weed Seed: <i lang=\"la\">Linaria dalmatica</i> (Dalmatian toadflax)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria dalmatica</i> (Linaire \u00e0 feuilles larges)"
    },
    "label": {
        "en": "Weed Seed: <i lang=\"la\">Linaria dalmatica</i> (Dalmatian toadflax)",
        "fr": "Semence de mauvaises herbe : <i lang=\"la\">Linaria dalmatica</i> (Linaire \u00e0 feuilles larges)"
    },
    "templatetype": "content page 1 column",
    "node_id": "1476285096959",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1333136604307",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/plant-health/seeds/seed-testing-and-grading/seeds-identification/linaria-dalmatica/",
        "fr": "/protection-des-vegetaux/semences/analyse-des-semences-et-designation-de-categorie/identification-de-semences/linaria-dalmatica/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Weed Seed: Linaria dalmatica (Dalmatian toadflax)",
            "fr": "Semence de mauvaises herbe : Linaria dalmatica (Linaire \u00e0 feuilles larges)"
        },
        "description": {
            "en": "xxxxxxxx",
            "fr": "xxxxxxxxxxx"
        },
        "keywords": {
            "en": "factsheet, invasive, invasive seeds, seed, Linaria dalmatica, Plantaginaceae, Dalmatian toadflax",
            "fr": "fiche de renseignements, envahissantes, semence, envahissante graines, Linaria dalmatica, Plantaginaceae, Linaire \u00e0 feuilles larges"
        },
        "dcterms.subject": {
            "en": "plants,weeds",
            "fr": "plante,plante nuisible"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "modified": {
            "en": "2017-11-01",
            "fr": "2017-11-01"
        },
        "type": {
            "en": "fact sheet",
            "fr": "fiche de renseignements"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Weed Seed: Linaria dalmatica (Dalmatian toadflax)",
        "fr": "Semence de mauvaises herbe : Linaria dalmatica (Linaire \u00e0 feuilles larges)"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Family</h2>\n<p><i lang=\"la\">Plantaginaceae</i></p>\n\n<h2>Common Name</h2>\n<p>Dalmatian toadflax</p>\n\n<h2>Regulation</h2>\n<p>Primary Noxious, Class 2 in the Canadian <a href=\"/english/reg/jredirect2.shtml?wsoagm2016\"><i>Weed Seeds Order</i>, 2016</a> under the <a href=\"/english/reg/jredirect2.shtml?seesema\"><i>Seeds Act</i></a>.</p>\n\n<h2>Distribution</h2>\n<p><strong>Canadian:</strong> Occurs in <abbr title=\"Alberta\">AB</abbr>, <abbr title=\"British Columbia\">BC</abbr>, <abbr title=\"Manitoba\">MB</abbr>, <abbr title=\"New Brunswick\">NB</abbr>, <abbr title=\"Nova Scotia\">NS</abbr>, <abbr title=\"Ontario\">ON</abbr>, <abbr title=\"Quebec\">QC</abbr>, <abbr title=\"Saskatchewan\">SK</abbr> and ephemeral in <abbr title=\"Newfoundland and Labrador\">NL</abbr> and <abbr title=\"Yukon\">YT</abbr> (Brouillet et al. 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Footnote\u00a0</span>1</a></sup>).</p>\n<p><strong>Worldwide:</strong> Native to southeastern Europe and western Asia (<abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Footnote\u00a0</span>2</a></sup>). Introduced in North America, Argentina, South Africa, India, Australia, and beyond its native range in Europe (Germany, Hungary, Italy, Switzerland and the United Kingdom) (<abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr> 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>).</p>\n\n<h2>Duration of life cycle</h2>\n<p>Perennial</p>\n\n<h2>Seed or fruit type</h2>\n<p>Seed</p>\n\n<h2>Identification features</h2>\n\n<h3 class=\"mrgn-lft-lg\">Size</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Seed length: 1.0 - 2.0 <abbr title=\"millimetres\">mm</abbr></li>\n<li>Seed width: 1.0 -1.5 <abbr title=\"millimetres\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Shape</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed rectangular-blocky</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Surface Texture</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Seed covered in ridges that form a wavy-wrinkled pattern</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Colour</h3> <ul class=\"mrgn-lft-lg\">\n<li>Seed black with bronze highlights</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Other Features</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>The seed edges are winged, approximately 0.2 <abbr title=\"millimetres\">mm</abbr> wide</li>\n</ul>\n<h2>Habitat and Crop Association</h2>\n<p>Old fields, pastures, gardens, vineyards, forest plantations, roadsides, railway lines and disturbed areas (Darbyshire 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Footnote\u00a0</span>4</a></sup>, CABI 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Footnote\u00a0</span>3</a></sup>). Persists in alfalfa and summerfallow in North America (Vujnovic and Wein 1997<sup id=\"fn5a-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n<h2>General Information</h2>\n<p>Dalmatian toadflax was introduced into North America in the late 19th century as an ornamental (Vujnovic and Wein 1997<sup id=\"fn5b-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). It is adaptable to a wide range of environmental conditions and soil   types, but is mostly found on coarse soils in sparsely vegetated sites (Vujnovic and Wein 1997<sup id=\"fn5c-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n<p>Dalmatian toadflax is a perennial species that produces large numbers of   seed every year (up to 400,000 per plant) and can also regrow from root   pieces (Vujnovic and Wein 1997<sup id=\"fn5d-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>). This combination of characteristics makes eradication difficult; some success has been found with biological control (Vujnovic and Wein 1997<sup id=\"fn5e-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Footnote\u00a0</span>5</a></sup>).</p>\n\n\n<h2>Similar species</h2>\n<h3>Broomleaf toadflax (<i lang=\"la\">Linaria genistifolia</i>)</h3>\n<ul>\n<li>Broomleaf toadflax seeds are a similar size, blackish colour, rough surface and sharp winged edges as Dalmatian toadflax.</li>\n\n<li>Broomleaf toadflax seeds have a silvery-blue sheen, have one tapered end and the winged edges have a microscopic reticulate pattern compared to Dalmatian toadflax.</li>\n</ul>\n\n<h3>Striped toadflax (<i lang=\"la\">Linaria repens</i>)</h3>\n<ul>\n<li>Striped toadflax seeds are a similar size, dark colour and rough surface  as Dalmatian toadflax.</li>\n\n<li>Striped toadflax seeds lack winged edges, have a dull brownish surface and granular edges compared to Dalmatian toadflax.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_04cnsh_1475604190946_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_03cnsh_1475604170033_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seed\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_01cnsh_1475604153275_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seed showing bronze highlights\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_copyright_1475604226749_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seed\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_x_section_copyright_1475604246326_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Dalmatian toadflax (<i lang=\"la\">Linaria dalmatica</i>) seed, cross-section\n</figcaption>\n</figure>\n\n\n\n<h3>Similar species</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_04cnsh_1475604320280_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Broomleaf toadflax (<i lang=\"la\">Linaria genistifolia</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_01cnsh_1475604274431_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Broomleaf toadflax (<i lang=\"la\">Linaria genistifolia</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_09cnsh_1475604458341_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Striped toadflax (<i lang=\"la\">Linaria repens</i>) seeds\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_07cnsh_1475604431517_eng.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Similar species: Striped toadflax (<i lang=\"la\">Linaria repens</i>) seeds\n</figcaption>\n</figure>\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Footnotes</h2> \n<dl>\n<dt>Footnote 1</dt>\n<dd id=\"fn1\">\n<p><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>1<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 2</dt>\n<dd id=\"fn2\">\n<p><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>2<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 3</dt>\n<dd id=\"fn3\">\n<p><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>3<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 4</dt>\n<dd id=\"fn4\">\n<p><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>4<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n\n<dt>Footnote 5</dt>\n<dd id=\"fn5\">\n<p><strong>Vujnovic, K. and Wein, R. W. 1997</strong>. The biology of Canadian weeds. 106. <i lang=\"la\">Linaria dalmatica</i> (L.) Mill. Canadian Journal of Plant Science 77: 483\u2013491.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5a-rf\"><span class=\"wb-inv\">Return to footnote\u00a0</span>5<span class=\"wb-inv\">\u00a0referrer</span></a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Famille</h2>\n<p><i lang=\"la\">Plantaginaceae</i></p>\n\n<h2>Nom commun</h2>\n<p>Linaire \u00e0 feuilles larges</p>\n\n<h2>R\u00e9glementation</h2>\n<p>Mauvaise herbe nuisible principale, cat\u00e9gorie 2 de l'<a href=\"/francais/reg/jredirect2.shtml?wsoagm2016\"><i>Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises herbes</i></a> de la <a href=\"/francais/reg/jredirect2.shtml?seesema\"><i>Loi sur les semences</i></a>.</p>\n\n<h2>R\u00e9partition</h2>\n<p><strong>R\u00e9partition au Canada :</strong> Pr\u00e9sente en <abbr title=\"Alberta\">Alb.</abbr>, en <abbr title=\"Colombie-Britannique\">C.-B.</abbr>, au <abbr title=\"Manitoba\">Man.</abbr>, au <abbr title=\"Nouveau-Brunswick\">N.-B.</abbr>, en <abbr title=\"Nouvelle-\u00c9cosse\">N.-\u00c9.</abbr>, en <abbr title=\"Ontario\">Ont.</abbr>, au <abbr title=\"Qu\u00e9bec\">Qc</abbr> et en <abbr title=\"Saskatchewan\">Sask</abbr> et \u00e9ph\u00e9m\u00e8re \u00e0 <abbr title=\"Terre-Neuve-et-Labrador\">T.-N.-L.</abbr> et au <abbr title=\"Yukon\">Yn</abbr> (Brouillet et al., 2016<sup id=\"fn1-rf\"><a class=\"fn-lnk\" href=\"#fn1\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>1</a></sup>).</p>\n<p><strong>R\u00e9partition mondiale :</strong> Indig\u00e8ne de l'Europe du Sud-Est et de l'Asie occidentale (<abbr lang=\"en\" title=\"United States Department of Agriculture\">USDA</abbr>-<abbr lang=\"en\" title=\"Agricultural Research Service\">ARS</abbr>, 2016<sup id=\"fn2-rf\"><a class=\"fn-lnk\" href=\"#fn2\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>2</a></sup>). Introduite en Am\u00e9rique du Nord, en Argentine, en Afrique du Sud, en   Inde, en Australie et \u00e0 l'ext\u00e9rieur de son aire d'indig\u00e9nat en Europe   (Allemagne, Hongrie, Italie, Suisse et Royaume-Uni)  (<abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3a-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>).</p>\n\n<h2>Dur\u00e9e du cycle vital</h2>\n<p>Vivace</p>\n\n<h2>Type de graine ou de fruit</h2>\n<p>Graine</p>\n\n<h2>Caract\u00e9ristiques d'identification</h2>\n\n<h3 class=\"mrgn-lft-lg\">Dimensions</h3>\n<ul class=\"mrgn-lft-lg\">\n<li>Longueur de la graine\u00a0: 1,0 \u00e0 2,0 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n<li>Largeur de la graine\u00a0: 1,0 \u00e0 1,5 <abbr title=\"millim\u00e8tre\">mm</abbr></li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Forme</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine cubo\u00efde-rectangulaire</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Texture de la surface</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Graine recouverte de cr\u00eates qui forment un motif pliss\u00e9 ondul\u00e9</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Couleur</h3> \n<ul class=\"mrgn-lft-lg\">\n<li>Graine noire avec des reflets bronz\u00e9s</li>\n</ul>\n<h3 class=\"mrgn-lft-lg\">Autres structures</h3>\n<ul class=\"mrgn-lft-lg\"> \n<li>Les bords de la graine sont ail\u00e9s et mesurent environ 0,2 <abbr title=\"millim\u00e8tre\">mm</abbr> de largeur</li>\n</ul>\n<h2>Habitat et cultures associ\u00e9es</h2>\n<p>Champs abandonn\u00e9s, p\u00e2turages, jardins, vignobles, plantations foresti\u00e8res, bords de chemin, voies ferr\u00e9es et terrains perturb\u00e9s (Darbyshire, 2003<sup id=\"fn4-rf\"><a class=\"fn-lnk\" href=\"#fn4\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>4</a></sup>; <abbr lang=\"en\" title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>, 2016<sup id=\"fn3b-rf\"><a class=\"fn-lnk\" href=\"#fn3\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>3</a></sup>). Elle survit dans les luzerni\u00e8res et les jach\u00e8res en Am\u00e9rique du Nord (Vujnovic et Wein, 1997<sup id=\"fn5a-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>).</p>\n\n<h2>Renseignements g\u00e9n\u00e9raux</h2>\n<p>La linaire \u00e0 feuilles larges a \u00e9t\u00e9 introduite en Am\u00e9rique du Nord \u00e0 la fin du 19e\u00a0si\u00e8cle comme plante ornementale (Vujnovic et Wein, 1997<sup id=\"fn5b-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>). Elle s'adapte \u00e0 divers milieux et types de sol, mais elle est surtout   trouv\u00e9e dans des milieux o\u00f9 la v\u00e9g\u00e9tation est clairsem\u00e9e et o\u00f9 les sols   ont une texture grossi\u00e8re (Vujnovic et Wein, 1997<sup id=\"fn5c-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>).</p>\n<p>La linaire \u00e0 feuilles larges est une vivace produisant annuellement un   grand nombre de graines (jusqu'\u00e0 400\u00a0000\u00a0graines par plante) et pouvant   aussi repousser \u00e0 partir de fragments racinaires (Vujnovic et Wein, 1997<sup id=\"fn5d-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>). C'est cette combinaison de caract\u00e9ristiques qui rend son \u00e9radication   difficile; des pratiques de lutte biologique connaissent un certain   succ\u00e8s (Vujnovic et Wein, 1997<sup id=\"fn5e-rf\"><a class=\"fn-lnk\" href=\"#fn5\"><span class=\"wb-inv\">Notes de bas de page\u00a0</span>5</a></sup>).</p>\n\n<h2>Esp\u00e8ce semblable</h2>\n<h3>Linaire \u00e0 feuilles de gen\u00eat (<i lang=\"la\">Linaria genistifolia</i>)</h3>\n<ul>\n<li>Les graines de la linaire \u00e0 feuilles de gen\u00eat ressemblent \u00e0 celles de la linaire \u00e0 feuilles larges par leurs dimensions, leur couleur noir\u00e2tre, leur surface rugueuse et leurs bords nettement ail\u00e9s.</li>\n\n<li>Les graines de la linaire \u00e0 feuilles de gen\u00eat ont des reflets bleu argent\u00e9, une extr\u00e9mit\u00e9 effil\u00e9e et leurs bords ail\u00e9s pr\u00e9sentent un microscopique motif r\u00e9ticul\u00e9.</li>\n</ul>\n\n<h3>Linarie stri\u00e9e (<i lang=\"la\">Linaria repens</i>)</h3>\n<ul>\n<li>Les graines de la linaire stri\u00e9e ressemblent \u00e0 celles de la linaire \u00e0 feuilles larges par leurs dimensions, leur couleur fonc\u00e9e et leur surface rugueuse.</li>\n\n<li>Les graines de la linaire stri\u00e9e sont d\u00e9pourvues de bords ail\u00e9s, leur surface est brun\u00e2tre terne et leurs bords sont granuleux comparativement \u00e0 la linaire \u00e0 feuilles larges.</li>\n</ul>\n\n<h2>Photos</h2>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_04cnsh_1475604190946_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_03cnsh_1475604170033_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_01cnsh_1475604153275_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graine avec lustre bronz\u00e9\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_copyright_1475604226749_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graine\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_dalmatica_x_section_copyright_1475604246326_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Linaire \u00e0 feuilles larges (<i lang=\"la\">Linaria dalmatica</i>) graine, en section transversale\n</figcaption>\n</figure>\n\n\n\n<h3>Esp\u00e8ce semblable</h3>\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_04cnsh_1475604320280_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linarie \u00e0 feuilles de gen\u00eat (<i lang=\"la\">Linaria genistifolia</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_genistifolia_spp_genistifolia_01cnsh_1475604274431_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linarie \u00e0 feuilles de gen\u00eat (<i lang=\"la\">Linaria genistifolia</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_09cnsh_1475604458341_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linarie stri\u00e9e (<i lang=\"la\">Linaria repens</i>) graines\n</figcaption>\n</figure>\n\n<figure class=\"mrgn-bttm-md\">\n<img src=\"/DAM/DAM-plants-vegetaux/STAGING/images-images/invasive_plants_seed_factsheet_linaria_repens_07cnsh_1475604431517_fra.jpg\" alt=\"\" class=\"img-responsive\">\n<figcaption>Esp\u00e8ce semblable : Linarie stri\u00e9e (<i lang=\"la\">Linaria repens</i>) graines\n</figcaption>\n</figure>\n\n\n\n<aside class=\"wb-fnote\" role=\"note\"> \n<h2 id=\"fn\">Notes de bas de page</h2> \n<dl><dt>Note de bas de page 1</dt>\n<dd id=\"fn1\">\n<p lang=\"en\"><strong>Brouillet, L., Coursol, F., Favreau, M. and Anions, M. 2016</strong>. VASCAN, the database vascular plants of Canada, http://data.canadensys.net/vascan/ [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn1-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>1</a></p> \n</dd>\n\n<dt>Note de bas de page 2</dt>\n<dd id=\"fn2\">\n<p lang=\"en\"><strong><abbr title=\"United States Department of Agriculture\">USDA</abbr>-<abbr title=\"Agricultural Research Service\">ARS</abbr> 2016</strong>. Germplasm Resources Information Network - (GRIN), https://npgsweb.ars-grin.gov/gringlobal/taxon/taxonomysimple.aspx [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn2-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>2</a></p> \n</dd>\n\n<dt>Note de bas de page 3</dt>\n<dd id=\"fn3\">\n<p lang=\"en\"><strong><abbr title=\"Centre for Agriculture and Biosciences International\">CABI</abbr>. 2016</strong>. Invasive Species Compendium, www.cabi.org/isc [2016, May 30].</p>\n<p class=\"fn-rtn\"><a href=\"#fn3a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>3</a></p> \n</dd>\n\n<dt>Note de bas de page 4</dt>\n<dd id=\"fn4\">\n<p lang=\"en\"><strong>Darbyshire, S. J. 2003</strong>. Inventory of Canadian Agricultural Weeds. Agriculture and Agri-Food Canada, Research Branch. Ottawa, <abbr title=\"Ontario\">ON</abbr>.</p>\n<p class=\"fn-rtn\"><a href=\"#fn4-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>4</a></p> \n</dd>\n\n<dt>Note de bas de page 5</dt>\n<dd id=\"fn5\">\n<p lang=\"en\"><strong>Vujnovic, K. and Wein, R. W. 1997</strong>. The biology of Canadian weeds. 106. <i lang=\"la\">Linaria dalmatica</i> (L.) Mill. Canadian Journal of Plant Science 77: 483\u2013491.</p>\n<p class=\"fn-rtn\"><a href=\"#fn5a-rf\"><span class=\"wb-inv\">Retour \u00e0 la r\u00e9f\u00e9rence de la note de bas de page\u00a0</span>5</a></p> \n</dd>\n</dl>\n</aside>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}