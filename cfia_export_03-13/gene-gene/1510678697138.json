{
    "dcr_id": "1510678697138",
    "title": {
        "en": "Preventive controls for maple products - Lead residues",
        "fr": "Contr\u00f4les pr\u00e9ventif pour les produits de l'\u00e9rable\u00a0- R\u00e9sidus de plomb"
    },
    "html_modified": "2024-03-12 3:57:49 PM",
    "modified": "2018-06-13",
    "issued": "2017-11-14 11:57:08",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_prev_control_maple_prod_1510678697138_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/sfcr_prev_control_maple_prod_1510678697138_fra"
    },
    "ia_id": "1510678888805",
    "parent_ia_id": "1526472290070",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Industry",
        "fr": "Industrie"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Applying preventive controls",
        "fr": "Application de mesures de contr\u00f4le pr\u00e9ventif"
    },
    "commodity": {
        "en": "Maple products",
        "fr": "Produits d'\u00e9rable"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1526472290070",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Preventive controls for maple products - Lead residues",
        "fr": "Contr\u00f4les pr\u00e9ventif pour les produits de l'\u00e9rable\u00a0- R\u00e9sidus de plomb"
    },
    "label": {
        "en": "Preventive controls for maple products - Lead residues",
        "fr": "Contr\u00f4les pr\u00e9ventif pour les produits de l'\u00e9rable\u00a0- R\u00e9sidus de plomb"
    },
    "templatetype": "content page 1 column",
    "node_id": "1510678888805",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1526472289805",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/preventive-controls/maple-products/",
        "fr": "/controles-preventifs/produits-de-l-erable/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Preventive controls for maple products - Lead residues",
            "fr": "Contr\u00f4les pr\u00e9ventif pour les produits de l'\u00e9rable\u00a0- R\u00e9sidus de plomb"
        },
        "description": {
            "en": "Preventive controls to help identify, reduce and eliminate possible sources of lead (Pb) contamination in maple syrup.",
            "fr": "Les contr\u00f4les pr\u00e9ventif pour aider \u00e0 identifier et \u00e9liminer les sources possibles de contamination de plomb du sirop d'\u00e9rable."
        },
        "keywords": {
            "en": "maple products, Lead residues, food safety, SFCR, Safe Food for Canadian Regulation",
            "fr": "produits de l'\u00e9rable, R\u00e9sidus de plomb, salubrit\u00e9 des aliments, RSAC, R\u00e8glement sur la salubrit\u00e9 des aliments au Canada"
        },
        "dcterms.subject": {
            "en": "food,food safety,inspection",
            "fr": "aliment,salubrit\u00e9 des aliments,inspection"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency,Food Safety and Consumer Protection Directorate",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments,Direction de la salubrit\u00e9 des aliments et de la protection des consommateurs"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2017-11-14 11:57:08",
            "fr": "2017-11-14 11:57:08"
        },
        "modified": {
            "en": "2018-06-13",
            "fr": "2018-06-13"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Preventive controls for maple products - Lead residues",
        "fr": "Contr\u00f4les pr\u00e9ventif pour les produits de l'\u00e9rable\u00a0- R\u00e9sidus de plomb"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n<h2>Introduction</h2>\n<p>The presence of lead (Pb) in foods can be hazardous to human health. It can produce subtle but adverse long term health effects, particularly in children. Lead residues can be found in maple products, especially when produced with lead containing equipment.</p>\n<p>According to Health Canada, the presence of lead in maple syrup, at levels of 0.5 parts per million (ppm) or more, suggests that avoidable contamination has taken place. Lead levels for other maple products have not yet been established.</p>\n<h2>Avoid, reduce or eliminate lead in maple products</h2>\n<p>Sources of lead include equipment:</p>\n<ul>\n<li>welded with lead solder (soldered before 1995, especially in storage tanks and homemade equipment)</li>\n<li>made from galvanized steel before 1994</li>\n<li>with brass and bronze joints and connections</li>\n<li>with terne plate, a lead containing alloy</li>\n</ul>\n<p>It has been reported that a significant portion of the lead is concentrated in the sugar sand suspended in the syrup, even if a large percentage is in a dissolved form. Therefore, filtration systems such as gravity filtration and press filtration may reduce lead levels in maple syrup. A laboratory analysis would confirm whether the residue level was successfully reduced below 0.5 <abbr title=\"parts per million\">ppm</abbr>.</p>\n<p>To help identify, reduce and eliminate possible sources of lead contamination, numerous resources can be used, including:</p>\n<ul>\n<li><a href=\"https://www.agrireseau.net/erable/documents/63661/a-few-rules-to-reduce-the-risks-of-contamination-of-maple-syrup-products-by-lead-residues?r=A+few+rules\">Maple Syrup Fact sheet <abbr title=\"number\">No.</abbr> 211b1094(E): A few rules to reduce the risks of contamination of maple syrup products by lead residues</a>, Centre <abbr title=\"Centre de recherche, de d\u00e9veloppement et de transfert technologique ac\u00e9ricole\" lang=\"fr\">ACER</abbr>, December 1999.</li>\n<li><a href=\"http://www.omafra.gov.on.ca/english/food/inspection/maple/mapleproduction.htm\">Best Production Practices for Safe, Quality Maple Syrup</a>, Ontario Ministry of Agriculture and Food (OMAFRA), January 2016.</li>\n<li><a href=\"http://www.uvm.edu/~pmrc/?Page=publications.html\">Keeping Lead Out of Maple Syrup</a>, Proctor Maple Research Center, University of Vermont, with the assistance of the <abbr title=\"United States\">US</abbr> Department of Agriculture (USDA) and the North American Maple Syrup Council (NAMSC).</li>\n<li>Local maple producers' associations.</li>\n</ul>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<!--<div class=\"alert alert-info\">\n-->\n\n<h2>Introduction</h2>\n<p>La pr\u00e9sence de plomb dans les aliments peut poser un risque pour la sant\u00e9. Elle peut causer un effet subtil mais nocif \u00e0 long terme pour la sant\u00e9, surtout chez les enfants. Des r\u00e9sidus de plomb peuvent se retrouver dans les produits de l'\u00e9rable, surtout s'ils sont fabriqu\u00e9s avec un \u00e9quipement dont les mat\u00e9riaux ou les soudures contiennent du plomb.</p>\n<p>Sant\u00e9 Canada a d\u00e9clar\u00e9 qu'une teneur en plomb \u00e9gale ou sup\u00e9rieure \u00e0 0,5 <abbr title=\"partie par million\">ppm</abbr> r\u00e9v\u00e8le qu'une contamination \u00e9vitable est survenue. Jusqu'\u00e0 date, la limite de teneur en plomb pour les autres produits de l'\u00e9rable n'a pas \u00e9t\u00e9 \u00e9tablie.</p>\n<h2>\u00c9viter, r\u00e9duire ou \u00e9liminer le plomb dans les produits de l'\u00e9rable</h2>\n<p>Les sources de plomb comprennent l'\u00e9quipement\u00a0:</p>\n<ul>\n<li>soud\u00e9 au plomb (soud\u00e9 avant 1995, surtout l'entreposage, l'extracteur et l'\u00e9quipement maison);</li>\n<li>galvanis\u00e9 (fabriqu\u00e9 avant 1994);</li>\n<li>fait avec des raccords en bronze ou en laiton;</li>\n<li>fait de fer terne (un alliage m\u00e9tallique au plomb).</li>\n</ul>\n<p>On rapporte qu'une partie importante du plomb se concentre dans la r\u00e2che en suspension dans le sirop d'\u00e9rable, m\u00eame si un grand pourcentage du plomb est dissous. Dans certains cas, le recours \u00e0 des syst\u00e8mes de filtration \u00e0 gravit\u00e9 et \u00e0 pression peut contribuer \u00e0 r\u00e9duire la teneur en plomb du sirop d'\u00e9rable. Une analyse en laboratoire confirmerait si la teneur en r\u00e9sidus de plomb a \u00e9t\u00e9 r\u00e9duite au-dessous de 0,5 <abbr title=\"partie par million\">ppm</abbr>.</p>\n<p>Pour aider \u00e0 identifier et \u00e9liminer les sources possibles de contamination de plomb, plusieurs\u00a0 ressources peuvent \u00eatre consult\u00e9es, telles que\u00a0:</p>\n<ul>\n<li><a href=\"http://gestion.centreacer.qc.ca/fr/UserFiles/publications/77_Fr.pdf\">Info-fiche ac\u00e9ricole no 211b1094\u00a0: Quelques r\u00e8gles permettant de r\u00e9duire les risques de contamination des produits ac\u00e9ricoles par des r\u00e9sidus de plomb</a>, Centre <abbr title=\"Centre de recherche, de d\u00e9veloppement et de transfert technologique ac\u00e9ricole\">ACER</abbr>, d\u00e9cembre 1999.</li>\n<li><a href=\"http://www.omafra.gov.on.ca/french/food/inspection/maple/mapleproduction.htm\">Pratiques exemplaires pour la production d'un sirop d'\u00e9rable sans danger et de qualit\u00e9</a>, Minist\u00e8re de l'Agriculture, de l'alimentation et des Affaires rurales, en Ontario, janvier 2016.</li>\n<li>Fiche d'information <span lang=\"en\">\"<a href=\"http://www.uvm.edu/~pmrc/?Page=publications.html\">Keeping Lead Out of Maple Syrup</a>\", Proctor Maple Research Center</span>, Universit\u00e9 du Vermont, avec la contribution du <span lang=\"en\">United States Department of Agriculture (USDA)</span> et du <span lang=\"en\">North American Maple Syrup Council (NAMSC)</span>] (en anglais seulement).</li>\n<li>Les associations de producteurs d'\u00e9rable locales.</li>\n</ul>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}