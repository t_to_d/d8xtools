{
    "dcr_id": "1664202313922",
    "title": {
        "en": "Streamlining food testing at the Dartmouth Laboratory",
        "fr": "Rationalisation de l'analyse des aliments au laboratoire de Dartmouth"
    },
    "html_modified": "2024-03-12 3:59:22 PM",
    "modified": "2022-09-27",
    "issued": "2022-09-27",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_food_tsting_drtmouth_lab_1664202313922_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/ins_food_tsting_drtmouth_lab_1664202313922_fra"
    },
    "ia_id": "1664202314254",
    "parent_ia_id": "1565297581210",
    "chronicletopic": {
        "en": "Science and Innovation|Food Safety",
        "fr": "Science et Innovation|Salubrit\u00e9 des Aliments"
    },
    "chroniclecontent": {
        "en": "Article",
        "fr": "Article"
    },
    "chronicleaudience": {
        "en": "Industry|Canadians",
        "fr": "Industrie|Canadiens"
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "inspect",
    "parent_node_id": "1565297581210",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Streamlining food testing at the Dartmouth Laboratory",
        "fr": "Rationalisation de l'analyse des aliments au laboratoire de Dartmouth"
    },
    "label": {
        "en": "Streamlining food testing at the Dartmouth Laboratory",
        "fr": "Rationalisation de l'analyse des aliments au laboratoire de Dartmouth"
    },
    "templatetype": "content page 1 column",
    "node_id": "1664202314254",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1565297580961",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/inspect-and-protect/food-safety/food-testing/",
        "fr": "/inspecter-et-proteger/salubrite-des-aliments/analyse-des-aliments/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Streamlining food testing at the Dartmouth Laboratory",
            "fr": "Rationalisation de l'analyse des aliments au laboratoire de Dartmouth"
        },
        "description": {
            "en": "Our scientists have been working to make routine tests more efficient, which can help free up resources for important research activities and potential food safety investigations.",
            "fr": "Nos scientifiques s'efforcent de rendre les tests de routine plus efficaces, ce qui permet de lib\u00e9rer des ressources pour d'importantes activit\u00e9s de recherche et d'\u00e9ventuelles enqu\u00eates sur la salubrit\u00e9 alimentaire."
        },
        "keywords": {
            "en": "Plant pests, protect plant resources, cultivating science, CFIA, scientists, potential food safety investigations.",
            "fr": "Signaler les phytoravageurs, prot\u00e9ger les ressources v\u00e9g\u00e9tales, ACIA, scientifiques, \u00e9ventuelles enqu\u00eates sur la salubrit\u00e9 alimentaire"
        },
        "dcterms.subject": {
            "en": "laboratories,veterinary medicine,scientific research,sciences",
            "fr": "laboratoire,m\u00e9decine v\u00e9t\u00e9rinaire,recherche scientifique,sciences"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2022-09-27",
            "fr": "2022-09-27"
        },
        "modified": {
            "en": "2022-09-27",
            "fr": "2022-09-27"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public,scientists",
            "fr": "entreprises,gouvernement,grand public,Scientifiques"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Streamlining food testing at the Dartmouth Laboratory",
        "fr": "Rationalisation de l'analyse des aliments au laboratoire de Dartmouth"
    },
    "body": {
        "en": "        \r\n        \n<div data-ajax-replace=\"/navi/eng/1646263148420\"></div>\n\n<p>By Jolette MacAulay, Gina Benedict and Kelly Duong</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_fd_tstng_dartmouth_lab_600x600_1663873933354_eng.jpg\" alt=\"\" class=\"img-thumbnail img-responsive\">\n</figure>\n</div>\n\n<p>This blog post was originally published to <a href=\"https://science.gc.ca/eic/site/063.nsf/eng/h_97679.html\">Cultivating Science</a> on science.gc.ca.</p>\n\n<blockquote>\n<p>The Chemistry Section at the Canadian Food Inspection Agency (CFIA) <a href=\"/science-and-research/our-laboratories/dartmouth/eng/1549486704359/1549486756922\">Dartmouth Laboratory</a> in Nova Scotia is responsible for testing fish and other foods for a variety of trace elements and veterinary drug residues. Testing for a long list of compounds involves a variety of methods and instruments, and their effective operation, maintenance and troubleshooting requires specialized skills and expertise.</p>\n\n<p>Streamlining these routine tests can help free up more resources for important research activities and potential food safety investigations. Luckily, the Dartmouth Lab has a great team that is always looking for ways to improve its processes. The team includes two of the CFIA's mighty heroes: Vaughn Arthur and Gina Benedict. <a href=\"/eng/1568731957675/1568743720700#a3\">Check out their CFIA superhero trading cards!</a></p>\n</blockquote>\n\n<p><a href=\"https://www.science.gc.ca/eic/site/063.nsf/eng/98431.html\" title=\"Streamlining food testing at the Dartmouth Laboratory\">Read the full blog post</a>.</p>\n\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading panel-title-bg\">\n<h2 class=\"panel-title\">Get more Inspect and Protect</h2>\n</header>\n<div class=\"panel-body\">\n\n<ul>\n<li>Want to learn more about what we do? Explore <strong><a href=\"/inspect-and-protect/articles/eng/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspect-and-protect/videos/eng/1646855870439/1646855870736\">videos</a></strong> and <strong><a href=\"/inspect-and-protect/podcast/eng/1616162488695/1616162631392\">podcasts</a></strong>.</li>\n<li>Interested in reporting on a story? Contact <strong><a href=\"/about-the-cfia/media-relations/eng/1299073792503/1299076004509\">CFIA Media Relations</a></strong> to arrange an interview with one of our experts.</li>\n<li>Have an idea or feedback to share? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Get in touch</a></strong>!</li>\n</ul>\n</div>\n\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<div data-ajax-replace=\"/navi/fra/1646263148420\"></div>\n\n<p>Par Jolette MacAulay, Gina Benedict and Kelly Duong</p>\n\n<div class=\"col-sm-4 pull-right mrgn-lft-md mrgn-bttm-md\">\n<figure>\n<img src=\"/DAM/DAM-aboutcfia-sujetacia/STAGING/images-images/ins_fd_tstng_dartmouth_lab_600x600_1663873933354_fra.jpg\" alt=\"\" class=\"img-thumbnail img-responsive\">\n</figure>\n</div>\n\n<p>Ce billet de blogue est d'abord paru dans <a href=\"https://science.gc.ca/eic/site/063.nsf/fra/h_97679.html\">Cultiver la science</a> sur science.gc.ca.</p>\n\n<blockquote>\n<p>La section Chimie du <a href=\"/les-sciences-et-les-recherches/nos-laboratoires/dartmouth/fra/1549486704359/1549486756922\">laboratoire de Dartmouth</a> de l'Agence canadienne d'inspection des aliments (ACIA), en Nouvelle-\u00c9cosse, analyse le poisson et d'autres aliments \u00e0 la recherche de diff\u00e9rents \u00e9l\u00e9ments traces et r\u00e9sidus de m\u00e9dicaments v\u00e9t\u00e9rinaires. La recherche d'une longue liste de compos\u00e9s requiert diff\u00e9rentes m\u00e9thodes et plusieurs appareils, dont le fonctionnement, l'entretien et le d\u00e9pannage efficaces exigent des comp\u00e9tences et une expertise sp\u00e9cialis\u00e9es.</p>\n\n<p>La rationalisation de ces analyses de routine peut contribuer \u00e0 lib\u00e9rer davantage de ressources pour d'importantes activit\u00e9s de recherche et d'\u00e9ventuelles enqu\u00eates sur la salubrit\u00e9 alimentaire. Heureusement, le laboratoire de Dartmouth dispose d'une \u00e9quipe formidable qui est toujours \u00e0 la recherche de moyens d'am\u00e9liorer ses processus. Cette \u00e9quipe comprend deux des h\u00e9ros \u00e9piques de l'ACIA\u00a0: Vaughn Arthur et Gina Benedict. <a href=\"/fra/1568731957675/1568743720700#a3\">D\u00e9couvrez les cartes \u00e0 collectionner de superh\u00e9ros de l'ACIA!</a></p>\n</blockquote>\n\n<p><a href=\"https://www.science.gc.ca/eic/site/063.nsf/fra/98431.html\" title=\"Rationalisation de l'analyse des aliments au laboratoire de Dartmouth\">Lisez le billet de blogue complet</a>.</p>\n\n<div class=\"clearfix\"></div>\n<div class=\"mrgn-tp-md\">\n<section class=\"panel panel-primary\">\n<header class=\"panel-heading\">\n<h2 class=\"panel-title\">Plus d'Inspecter et prot\u00e9ger</h2>\n</header>\n<div class=\"panel-body\">\n<ul>\n<li>Vous voulez en savoir davantage sur notre travail? Explorez des <strong><a href=\"/inspecter-et-proteger/articles/fra/1646853383507/1646853384007\">articles</a></strong>, <strong><a href=\"/inspecter-et-proteger/videos/fra/1646855870439/1646855870736\">vid\u00e9os</a></strong> et <strong><a href=\"/inspecter-et-proteger/balado-inspecter-et-proteger/fra/1616162488695/1616162631392\">balados</a></strong>.</li>\n\n\n<li>D\u00e9sirez-vous faire un reportage sur une histoire?  Communiquez avec les <strong><a href=\"/a-propos-de-l-acia/relations-avec-les-medias/fra/1299073792503/1299076004509\">relations avec les m\u00e9dias de l'ACIA</a></strong> pour obtenir une entrevue avec l'un de nos experts.</li>\n\n<li>Vous avez une id\u00e9e ou une r\u00e9troaction \u00e0 partager? <strong><a href=\"mailto:cfia.InspectProtect-InspecterProteger.acia@inspection.gc.ca\">Communiquez avec nous</a></strong>!</li>\n</ul>\n\n</div>\n</section>\n</div>\n\n<style>\n.panel-heading{background-color: #36404b !important;}\n.panel {border-color: #36404b !important;}\n\n</style>\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "success": true,
    "chat_wizard": false
}