{
    "dcr_id": "1678212356423",
    "title": {
        "en": "India \u2013 Export requirements for Infant food",
        "fr": "Inde \u2013 Exigences d'exportation pour les aliments pour nourrissons"
    },
    "html_modified": "2024-01-26 2:26:08 PM",
    "modified": "2023-03-15",
    "issued": "2023-03-15",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_food_infant_food_india_1678212356423_eng",
        "fr": "/default/main/inspection/cont-cont/food-aliments/STAGING/templatedata/comn-comn/gene-gene/data/food_export_food_infant_food_india_1678212356423_fra"
    },
    "ia_id": "1678212357016",
    "parent_ia_id": "1507329098850",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "Guidance",
        "fr": "Document d'orientation"
    },
    "sfcraudience": {
        "en": "Agency personnel|Industry",
        "fr": "Industrie|Personnel de l'agence"
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "Exporting food|Inspecting and investigating - Performing export certification activities",
        "fr": "Exportation d\u2019aliments|Inspecter et v\u00e9rifier - Ex\u00e9cuter des activit\u00e9s pour la certification d'exportation"
    },
    "commodity": {
        "en": "Dairy products",
        "fr": "Produits laitiers"
    },
    "program": {
        "en": "Food",
        "fr": "Aliments"
    },
    "search_type": "guidancefinder",
    "parent_node_id": "1507329098850",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "India \u2013 Export requirements for Infant food",
        "fr": "Inde \u2013 Exigences d'exportation pour les aliments pour nourrissons"
    },
    "label": {
        "en": "India \u2013 Export requirements for Infant food",
        "fr": "Inde \u2013 Exigences d'exportation pour les aliments pour nourrissons"
    },
    "templatetype": "content page 1 column",
    "node_id": "1678212357016",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1507329098491",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/exporting-food-plants-or-animals/food-exports/requirements/india-infant-food/",
        "fr": "/exportation-d-aliments-de-plantes-ou-d-animaux/exportations-d-aliments/exigences/inde-aliments-pour-nourrissons/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "India \u2013 Export requirements for Infant food",
            "fr": "Inde \u2013 Exigences d'exportation pour les aliments pour nourrissons"
        },
        "description": {
            "en": "Countries to which exports are currently made - India",
            "fr": "Pays pour lesquels l'exportation est autoris\u00e9e \u2013 Inde"
        },
        "keywords": {
            "en": "India, Export requirements, supplements, Infant food",
            "fr": "Inde, Exigences d'exportation, aliments, nourrissons"
        },
        "dcterms.subject": {
            "en": "agri-food industry,eggs,exports",
            "fr": "industrie agro-alimentaire,oeuf,exportation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2023-03-15",
            "fr": "2023-03-15"
        },
        "modified": {
            "en": "2023-03-15",
            "fr": "2023-03-15"
        },
        "type": {
            "en": "reference material",
            "fr": "mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,general public,government",
            "fr": "entreprises,grand public,gouvernement"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "India \u2013 Export requirements for Infant food",
        "fr": "Inde \u2013 Exigences d'exportation pour les aliments pour nourrissons"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<h2>Sur cette page</h2>\n<ol>\n<li><a href=\"#a1\">Eligible/ineligible product</a></li>\n<li><a href=\"#a2\">Pre-export approvals by the competent authority of the importing country</a></li>\n<li><a href=\"#a3\">Production controls and inspection requirements</a></li>\n<li><a href=\"#a4\">Labelling, packaging and marking requirements</a></li>\n<li><a href=\"#a5\">Required documents</a></li>\n<li><a href=\"#a6\">Other information</a></li>\n</ol>\n\n<h2 id=\"a1\">1. Eligible/ineligible product</h2>\n\n<h3>Eligible</h3>\n<p>Infant food is defined in The Infant Milk Substitutes, Feeding Bottles and Infant Foods (Regulation of Production, Supply and Distribution) Act 1992 as any food (by whatever name called) being marketed or otherwise represented as a complement to mother\u2019s milk to meet the growing nutritional needs of the infant [after the age of six months and up to the age of two years].</p>\n\n<p>It is recommended that you work with your importer to confirm that the products being exported are eligible for entry into India.</p>\n\n<h3>Ineligible</h3>\n<ul>\n<li>Information not available</li>\n</ul>\n\n<h2 id=\"a2\">2. Pre-export approvals by the competent authority of the importing country</h2>\n\n<h3>Establishments</h3>\n\n<p>In order to export food supplements to India, establishments must appear on <a href=\"https://sites.fssai.gov.in/refom/index.php\">India's registration list of food manufacturers</a> approved to export to India. This list is administered by the Food Safety and Standards Authority of India (FSSAI).</p>\n\n<p>To add, update or remove a processing establishment on the list of Canadian establishments approved for export food supplements to India, please contact your <a href=\"/about-cfia/contact-a-cfia-office-by-telephone/eng/1313255382836/1313256130232\">local Canadian Food Inspection Agency (CFIA) office</a> to initiate the request in writing and include the following information:</p>\n\n<ul>\n<li>unique establishment identifier</li>\n<li>Safe Food for Canadians licence number</li>\n<li>full legal name as it appears on the Safe Food for Canadians licence</li>\n<li>physical site address associated with the unique establishment identifier</li>\n<li>company contact name and telephone number</li>\n<li>product details:\n<ul>\n<li>product name/description</li>\n<li>6\u00a0digit Harmonized System (HS) code of products</li>\n</ul></li>\n</ul>\n\n<p>The inspector will submit the request following the\u00a0<a href=\"/inspection-and-enforcement/guidance-for-food-inspection-activities/permission-issuance/food-export-eligibility-lists/eng/1540923903657/1540923903887\">Operational procedure: Procedure for maintaining food export eligibility lists</a>.</p>\n \n<p><strong>Note:</strong> by submitting a written request to the CFIA for addition of an establishment to an export eligibility list, the licence holder acknowledges that the products intended for export meet the requirements of the importing country. The licence holder consents to the publication of establishment information on the CFIA's and/or the foreign competent authority's public website.</p>\n\n<h3>Products</h3>\n\n<p>Manufacturers wishing to export to India must also register their products by providing the name\u00a0 and HS Code of products. See above the section related to the registration of establishments. Furthermore, India Food Safety and Standards (Health supplements, Nutraceuticals, Food for special dietary use, Food for special medical purpose, Functional food and Novel food) Regulation 2016 provides information on the type of products.</p>\n\n<h2 id=\"a3\">3. Production controls and inspection requirements</h2>\n\n<p>The manufacturer of food supplements must be licensed under the\u00a0<i>Safe Food for Canadian Regulations</i>\u00a0(SFCR).</p>\n \n<p>The establishment must be aware of the standards and requirements of the importing country and has a specific export procedure in place. Please consult the link to <a href=\"https://fssai.gov.in/cms/food-safety-and-standards-regulations.php\"><i>Food Safety and Standards Regulations</i></a>.</p>\n\n<h2 id=\"a4\">4. Labelling, packaging and marking requirements</h2>\n\n<p>It is the exporter's responsibility to meet all the requirements for labelling, packaging and marking requirements as per the importing country.</p>\n\n<h2 id=\"a5\">5. Required documents</h2>\n\n<h3>Certificate</h3>\n\n<p>For the moment, India has not  provided any information regarding the need to have a certificate that accompanies  a shipment of food supplements.</p>\n  \n<p>An import permit issued by the  competent authority in India could be required and may contain specific import  conditions. If this is the case, please provide the import permit to your local  CFIA office who will forward it to the Food Import and Export Division (FIED)  for review and action if necessary.</p>\n  \n<p><strong>Note:</strong> export certificates cannot be issued for products  that have left Canada. It is the responsibility of the exporter to ensure  whether the exported products must be accompanied by a health certificate.</p>\n\n<h2 id=\"a6\">6. Other information</h2>\n\n<p>Samples (personal or commercial) may be subject to the same requirements as a regular shipment. It is strongly recommended that the exporter verify these requirements with their importer. Furthermore, ports of entry into the Indian territory have been identified by the competent authority according to the risk associated with the imported products.</p>\n \n<h3>Relevant links</h3>\n\n<ul>\n<li><a href=\"https://fssai.gov.in/\">Food Safety and Standards Authority of India</a></li>\n<li><a href=\"https://fssai.gov.in/upload/advisories/2023/02/63dd205dc1304Port_Restriction.pdf\">List of ports of entry into Indian territory</a></li>\n<li><a href=\"https://www.indiacode.nic.in/\">India Code Digital Repository of All Central and State Acts </a></li>\n</ul>\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n<h2>Sur cette page</h2>\n<ol>\n<li><a href=\"#a1\">Produits admissibles/non admissibles</a></li>\n<li><a href=\"#a2\">Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</a></li>\n<li><a href=\"#a3\">Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</a></li>\n<li><a href=\"#a4\">Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</a></li>\n<li><a href=\"#a5\">Documents requis</a></li>\n<li><a href=\"#a6\">Autres renseignements</a></li>\n</ol>\n\n<h2 id=\"a1\">1. Produits admissibles/Non admissibles</h2>\n\n<h3>Admissibles</h3>\n\n<p>Les aliments pour nourrissons sont d\u00e9finis dans la loi de 1992 sur les substituts du lait pour nourrissons, les biberons et les aliments pour nourrissons (<span lang=\"en\">Regulation of Production, Supply and Distribution</span>) comme tout aliment (quel que soit son nom) commercialis\u00e9 ou autrement pr\u00e9sent\u00e9 comme un compl\u00e9ment au lait maternel pour r\u00e9pondre aux besoins croissants besoins nutritionnels du nourrisson [apr\u00e8s l'\u00e2ge de six mois et jusqu'\u00e0 l'\u00e2ge de deux ans].</p>\n\n<p>Il est recommand\u00e9 de travailler avec son importateur afin de confirmer afin de confirmer que les produits export\u00e9s sont \u00e9ligibles pour entrer en Inde.</p>\n\n<h3>Non Admissibles</h3>\n<ul>\n<li>Information non disponible. </li>\n</ul>\n\n<h2 id=\"a2\">2. Approbations pr\u00e9alables \u00e0 l'exportation par l'autorit\u00e9 comp\u00e9tente du pays importateur</h2>\n\n<h3>\u00c9tablissements</h3>\n\n<p>Pour exporter des suppl\u00e9ments alimentaires vers l'Inde, les \u00e9tablissements doivent figurer sur la <a href=\"https://sites.fssai.gov.in/refom/index.php\">Liste des fabricants \u00e9trangers des produits alimentaires (en anglais seulement)</a> autoris\u00e9s \u00e0 exporter vers l'Inde. Cette liste est administr\u00e9e par la <span lang=\"en\">Food Safety and Standards Authority of India</span> (FSSAI).</p>\n\n<p>Pour ajouter, mettre \u00e0 jour ou retirer un \u00e9tablissement sur la liste des \u00e9tablissements canadiens autoris\u00e9s pour l'exportation de suppl\u00e9ments alimentaires vers l'Inde, veuillez contacter votre <a href=\"/a-propos-de-l-acia/contactez-l-acia-par-telephone/fra/1313255382836/1313256130232\">bureau local de l'Agence canadienne d'inspection des aliments (ACIA</a>) pour initier une demande \u00e9crite en fournissant les informations suivantes\u00a0:</p>\n\n<ul>\n<li>identifiant unique de l'\u00e9tablissement</li>\n<li>num\u00e9ro de licence pour la salubrit\u00e9 des aliments au Canada</li>\n<li>nom l\u00e9gal complet tel qu'il appara\u00eet sur la licence de la salubrit\u00e9 des aliments au Canada</li>\n<li>adresse physique du site associ\u00e9e \u00e0 l'identifiant unique de l'\u00e9tablissement</li>\n<li>nom et num\u00e9ro de t\u00e9l\u00e9phone de la personne-ressource de l'entreprise</li>\n<li>d\u00e9tails du produit\u00a0:\n<ul>\n<li>nom/description du produit</li>\n<li>code du Syst\u00e8me harmonis\u00e9 (SH) \u00e0 6 chiffres des produits</li>\n</ul></li>\n</ul>\n\n<p>L'inspecteur soumettra la demande \u00e0 la Division d'importation et d'exportation des aliments (DIEA) selon la\u00a0<a href=\"/inspection-et-application-de-la-loi/documents-d-orientation-pour-les-activites-d-inspe/delivrance-d-autorisations/les-listes-d-admissibilite-a-l-exportation-d-alime/fra/1540923903657/1540923903887\">Proc\u00e9dure op\u00e9rationnelle\u00a0: Proc\u00e9dure pour tenir \u00e0 jour les listes d'admissibilit\u00e9 \u00e0 l'exportation des aliments</a>.</p>\n \n<p><strong>Remarque\u00a0:</strong> en soumettant une demande \u00e9crite \u00e0 l'ACIA pour l'ajout d'un \u00e9tablissement \u00e0 une liste d'admissibilit\u00e9 \u00e0 l'exportation, le titulaire de licence reconna\u00eet que les produits destin\u00e9s \u00e0 l'exportation satisfont aux exigences du pays importateur. Le titulaire de licence consent \u00e0 la publication de renseignements sur l'\u00e9tablissement sur le site Web public de l'ACIA et/ou de l'autorit\u00e9 comp\u00e9tente \u00e9trang\u00e8re.</p>\n\n<h3>Produits</h3>\n\n<p>Les fabricants d\u00e9sirant exporter vers l'Inde doivent aussi enregistrer leurs produits en fournissant le nom et le Code SH des produits. Voir ci-dessus la section en rapport avec l'enregistrement des \u00e9tablissements. Par ailleurs, le <span lang=\"en\">Food Safety and Standards (Health supplements, Nutraceuticals, Food for special dietary use, Food for special medical purpose, Functional food and Novel food) Regulation</span> 2016 informe sur le type de produits.</p>\n\n<h2 id=\"a3\">3. Mesures de contr\u00f4le de production et exigences en mati\u00e8re d'inspection</h2>\n\n<p>Le fabricant de suppl\u00e9ments alimentaires doit d\u00e9tenir une licence en vertu du <i>R\u00e8glement sur la salubrit\u00e9 des aliments au Canada</i> (RSAC).</p>\n\n<p>L'\u00e9tablissement doit \u00eatre au courant des normes et exigences du pays importateur. Une proc\u00e9dure d'exportation sp\u00e9cifique doit \u00eatre en place. Veuillez consulter le lien vers les <a href=\"https://fssai.gov.in/cms/food-safety-and-standards-regulations.php\"><i lang=\"en\">Food Safety and Standards Regulations</i> (en anglais seulement)</a></p>\n\n<h2 id=\"a4\">4. Exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage</h2>\n\n<p>Il incombe \u00e0 l'exportateur de satisfaire \u00e0 toutes les exigences en mati\u00e8re d'\u00e9tiquetage, d'emballage et de marquage selon le pays importateur.</p>\n\n<h2 id=\"a5\">5. Documents requis</h2>\n\n<h3>Certificat</h3>\n\n<p>Pour l'instant, l'Inde n'a pas fourni d'information concernant la n\u00e9cessit\u00e9 d'avoir un certificat qui accompagne une exp\u00e9dition de suppl\u00e9ments alimentaires.</p>\n \n<p>Un permis d'importation \u00e9mis par l'autorit\u00e9 comp\u00e9tente de l'Inde pourrait \u00eatre requis et des conditions sp\u00e9cifiques d'importation pourraient \u00eatre exig\u00e9es. Si tel est le cas, veuillez fournir le permis d'importation \u00e0 votre bureau local qui le transmettra \u00e0 la Division d'importation et d'exportation des aliments (FIED) pour r\u00e9vision et action si n\u00e9cessaire.</p>\n \n<p><strong>Remarque\u00a0:</strong> les certificats d'exportation ne peuvent pas \u00eatre \u00e9mis pour des produits qui ont quitt\u00e9 le Canada. C'est la responsabilit\u00e9 de l'exportateur de v\u00e9rifier si les produits export\u00e9s doivent \u00eatre accompagn\u00e9s d'un certificat sanitaire.</p>\n\n<h2 id=\"a6\">6. Autres renseignements</h2>\n\n<p>Les \u00e9chantillons (personnels ou commerciaux) pourraient \u00eatre soumis aux m\u00eames exigences qu'une exp\u00e9dition r\u00e9guli\u00e8re. Il est fortement recommand\u00e9 \u00e0 l'exportateur de v\u00e9rifier ces exigences aupr\u00e8s de son importateur. Par ailleurs, des ports d'entr\u00e9e selon le risque associ\u00e9 aux produits import\u00e9s sur le territoire indien ont \u00e9t\u00e9 identifi\u00e9s par l'autorit\u00e9 comp\u00e9tente. Voir le lien ci-dessous.</p>\n \n<h3>Liens pertinents</h3>\n\n<ul>\n<li><a href=\"https://fssai.gov.in/\"><span lang=\"en\">Food Safety and Standards Authority of India</span> (en anglais seulement)</a></li>\n<li><a href=\"https://fssai.gov.in/upload/advisories/2023/02/63dd205dc1304Port_Restriction.pdf\">Liste des ports d'entr\u00e9e sur le territoire indien (en anglais seulement)</a></li>\n<li><a href=\"https://www.indiacode.nic.in/\"><span lang=\"en\">India Code Digital Repository of All Central and State Acts</span> (en anglais seulement)</a></li>\n</ul>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}