{
    "dcr_id": "1463457256836",
    "title": {
        "en": "Frequently Asked Questions: Amendments  to the Weed Seeds Order",
        "fr": "Foire aux questions - Modifications \u00e0 l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes"
    },
    "html_modified": "2024-01-26 2:23:15 PM",
    "modified": "2016-06-01",
    "issued": "2016-06-01",
    "teamsite_location": {
        "en": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_weed_regs_1463457256836_eng",
        "fr": "/default/main/inspection/cont-cont/cfiaadmin-adminacia/STAGING/templatedata/comn-comn/gene-gene/data/cfia_regu_faq_weed_regs_1463457256836_fra"
    },
    "ia_id": "1463457257335",
    "parent_ia_id": "1419029097256",
    "chronicletopic": {
        "en": "",
        "fr": ""
    },
    "chroniclecontent": {
        "en": "",
        "fr": ""
    },
    "chronicleaudience": {
        "en": "",
        "fr": ""
    },
    "sfcrdocumenttype": {
        "en": "",
        "fr": ""
    },
    "sfcraudience": {
        "en": "",
        "fr": ""
    },
    "sfcrmethodofproduction": {
        "en": "",
        "fr": ""
    },
    "activity": {
        "en": "",
        "fr": ""
    },
    "commodity": {
        "en": "",
        "fr": ""
    },
    "program": {
        "en": "",
        "fr": ""
    },
    "search_type": "other",
    "parent_node_id": "1419029097256",
    "layout_name": "content page 1 column",
    "breadcrumb": {
        "en": "Frequently Asked Questions: Amendments  to the Weed Seeds Order",
        "fr": "Foire aux questions - Modifications \u00e0 l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes"
    },
    "label": {
        "en": "Frequently Asked Questions: Amendments  to the Weed Seeds Order",
        "fr": "Foire aux questions - Modifications \u00e0 l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes"
    },
    "templatetype": "content page 1 column",
    "node_id": "1463457257335",
    "managing_branch": "comn",
    "type_name": "gene-gene",
    "dcr_type": "gene-gene",
    "parent_dcr_id": "1419029096537",
    "has_dcr_id": true,
    "lang": "en",
    "could_not_load": 0,
    "refaire": false,
    "interwoven_url": {
        "en": "/about-cfia/acts-and-regulations/list-of-acts-and-regulations/faq-amendments-to-the-weed-seeds-order/",
        "fr": "/a-propos-de-l-acia/lois-et-reglements/liste-des-lois-et-reglements/faq-arrete-sur-les-graines-de-mauvaises-herbes/"
    },
    "meta": {
        "viewport": {
            "en": "width=device-width,initial-scale=1",
            "fr": "width=device-width,initial-scale=1"
        },
        "service": {
            "en": "CFIA-ACIA",
            "fr": "CFIA-ACIA"
        },
        "accessRights": {
            "en": "2",
            "fr": "2"
        },
        "title": {
            "en": "Frequently Asked Questions: Amendments  to the Weed Seeds Order",
            "fr": "Foire aux questions - Modifications \u00e0 l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes"
        },
        "description": {
            "en": "The frequently asked questions below are meant to provide Canadians and businesses with general information about the amendments to the Weed Seeds Order.",
            "fr": "La Foire aux questions qui suit vise \u00e0 informer les Canadiens et les entreprises des modifications apport\u00e9es \u00e0 l\u2019Arr\u00eat\u00e9 sur les graines de mauvaises herbes."
        },
        "keywords": {
            "en": "frequently asked questions, regulations, Weeds Seeds Order, amendments",
            "fr": "foire aux questions, r\u00e8glements, Arr\u00eat\u00e9 sur les graines de mauvaises herbes"
        },
        "dcterms.subject": {
            "en": "exports,imports,products,agri-food products,consumer protection,regulation",
            "fr": "exportation,importation,produit,produit agro-alimentaire,protection du consommateur,r\u00e9glementation"
        },
        "creator": {
            "en": "Government of Canada,Canadian Food Inspection Agency",
            "fr": "Gouvernement du Canada,Agence canadienne d'inspection des aliments"
        },
        "language": {
            "en": "eng",
            "fr": "fra"
        },
        "issued": {
            "en": "2016-06-01",
            "fr": "2016-06-01"
        },
        "modified": {
            "en": "2016-06-01",
            "fr": "2016-06-01"
        },
        "type": {
            "en": "frequently asked questions,reference material",
            "fr": "foire aux questions\u00a0,mat\u00e9riel de r\u00e9f\u00e9rence"
        },
        "audience": {
            "en": "business,government,general public",
            "fr": "entreprises,gouvernement,grand public"
        },
        "name": {
            "en": "Government of Canada",
            "fr": "Gouvernement du Canada"
        },
        "areaServed": {
            "en": "Canada",
            "fr": "Canada"
        }
    },
    "h1": {
        "en": "Frequently Asked Questions: Amendments to the Weed Seeds Order",
        "fr": "Foire aux questions - Modifications \u00e0 l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes"
    },
    "js": {
        "en": "",
        "fr": ""
    },
    "body": {
        "en": "        \r\n        \n<p>The frequently asked questions below are meant to  provide Canadians and businesses with general information about the amendments  to the Weed Seeds Order.</p>\n<h2 class=\"h5\">What  is the purpose of the Weed Seeds Order and  these amendments?</h2>\n<p>The <abbr title=\"Weed Seeds Order\">WSO</abbr>, a ministerial order made pursuant to  subsection 4(2) of the Seeds Act,  plays a critical role in preventing the introduction of new weed species into  Canada by regulating the presence of weed species in seed sold in, or imported  into, Canada.  </p>\n<p>The <abbr title=\"Weed Seeds Order\">WSO</abbr> groups weed seeds into six classes according  to their level of risk.  The assessment  of risk includes considerations such as economic reasons, such as crop yield  and market access, and environmental impacts. The most restrictive class, Class  1 prohibited noxious weed seeds, is prohibited in seed.  The level of weed species listed in Classes 2  to 6 and that are permitted in a seed sample are specified in Schedule I of the Seeds Regulations.</p>\n<p>The new Weed  Seeds Order, 2016 will be more effective at prohibiting  species of concern and controlling the spread of weed species through seed.  The amendments help reduce the number of  introduced and established weeds in Canada, preserve biodiversity, improve the  efficiency of agricultural production, and reduce the cost to control weeds. </p>\n<h2 class=\"h5\">What  are the key elements of these amendments?</h2>\n<p>The amendments include: </p>\n<ul>\n<li>adding new and emerging weed species to the <abbr title=\"Weed Seeds Order\">WSO</abbr>;</li>\n<li>reclassifying some species in order to reflect  the current distribution of weed species in Canada;</li>\n<li>removing some species which are now considered agricultural  crops;</li>\n<li>revisions to the regulatory  text with the wording in the Seeds Act to ensure consistency in language  and interpretation; and, </li>\n<li>alignment with current international best  practices. </li>\n</ul>\n\n<h2 class=\"h5\">How  do these amendments affect Canadian businesses?</h2>\n<p>Reducing the number of harmful weed species will help  preserve the long-term prosperity of Canada's agricultural sector and enhance  its value in domestic and international markets.  Updated weed species classifications in the  <abbr title=\"Weed Seeds Order\">WSO</abbr> is based on current information on species distribution in Canada and will  reduce undue burden on industry resulting from outdated regulation. </p>\n<p>We anticipate that there may be additional, though  minor, compliance costs to some small businesses in the seed industry with weed  species being added or reclassified. </p>\n<h2 class=\"h5\">Some  of the species listed as Prohibited Noxious on the Weed Seeds Order are also regulated by <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> under the Plant Protection Act.  What are the related implications for  stakeholders?</h2>\n<p>When a Prohibited Noxious species is detected as part  of the normal function of the Seeds Act,  the <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> may take action under the Plant  Protection Act (PPA), if the species is also listed on the List of Pest  Regulated by Canada.  The <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr> may invoke  the <abbr title=\"Plant  Protection Act\">PPA</abbr> to mitigate the risk associated with the weed species. </p>\n<p>As part of this consultation, stakeholders raised  concern about compensation for those affected by the implications of  enforcement action taken under the <abbr title=\"Plant  Protection Act\">PPA</abbr>.</p>\n\n<h2 class=\"h5\">When  are these regulations expected to come into force?</h2>\n<p>The amendments to the <abbr title=\"Weed Seeds Order\">WSO</abbr> come into force on November  1, 2016.</p>\n<h2 class=\"h5\">Where  can I get more information about the Seeds  Regulations and the Weed Seeds Order?</h2>\n<p>For  more information about <a href=\"/plant-health/seeds/eng/1299173228771/1299173306579\">seeds</a>,  please go to our website or <a href=\"mailto:david.bailey@inspection.gc.ca\">contact David R. Bailey</a>, Director, <abbr title=\"Canadian Food Inspection Agency\">CFIA</abbr>  Plant Production Division. </p>\n\n\r\n        \r\n\r\n\r\n\r\n<div class=\"row\">\r\n \r\n\t\r\n\t\t\t\r\n\r\n<div class=\"col-xm-12 pagedetails container\">\r\n\t\t\t\t\r\n\r\n</div>\r\n\t\r\n</div>",
        "fr": "\r\n        \r\n        \n\n<p>La Foire aux questions qui suit vise \u00e0 informer les Canadiens et les entreprises des modifications apport\u00e9es \u00e0 l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes. </p>\n<h2 class=\"h5\">Quelle est la finalit\u00e9 de l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes et de ces\u00a0modifications?</h2>\n<p>L'Arr\u00eat\u00e9 sur les graines de mauvaises herbes (AGMH), un arr\u00eat\u00e9 minist\u00e9riel publi\u00e9 en vertu du paragraphe\u00a04(2) de la Loi sur les semences, joue un r\u00f4le essentiel dans la pr\u00e9vention de l'introduction d'esp\u00e8ces nouvelles de mauvaises herbes au Canada en r\u00e9glementant la pr\u00e9sence d'esp\u00e8ces de mauvaises herbes sous forme de graines commercialis\u00e9es ou import\u00e9es au Canada. </p>\n<p>L'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> regroupe les graines de mauvaises herbes en six cat\u00e9gories, et ce, en fonction du risque qu'elles comportent. L'\u00e9valuation du risque prend en consid\u00e9ration divers facteurs, dont des facteurs d'ordre \u00e9conomique comme le rendement des r\u00e9coltes et l'acc\u00e8s aux march\u00e9s, de m\u00eame que les impacts sur l'environnement. La cat\u00e9gorie la plus contraignante est la cat\u00e9gorie\u00a01, celle des graines de mauvaises herbes nuisibles interdites dont la pr\u00e9sence dans des semences est strictement prohib\u00e9e. La teneur en esp\u00e8ces de mauvaises herbes inscrites dans les cat\u00e9gories 2 \u00e0 6 qui est autoris\u00e9e dans un \u00e9chantillon de semences est indiqu\u00e9e \u00e0 l'annexe\u00a0<abbr title=\"1\">I</abbr> du R\u00e8glement sur les\u00a0semences.</p>\n<p>Le nouvel Arr\u00eat\u00e9 de 2016 sur les graines de mauvaises permettra d'obtenir de meilleurs r\u00e9sultats quant \u00e0 l'interdiction d'esp\u00e8ces pr\u00e9occupantes et au contr\u00f4le de la diss\u00e9mination d'esp\u00e8ces de mauvaises herbes lors des semis. Les modifications contribueront \u00e0 diminuer le nombre des mauvaises herbes introduites et naturalis\u00e9es au Canada, \u00e0 pr\u00e9server la diversit\u00e9 biologique, \u00e0 hausser les rendements de la production agricole et \u00e0 abaisser les co\u00fbts associ\u00e9s \u00e0 la lutte contre les mauvaises herbes.</p>\n<h2 class=\"h5\">Quels sont les \u00e9l\u00e9ments cl\u00e9s de ces modifications?</h2>\n<p>Les modifications portent, entre autres, sur ce qui suit\u00a0: </p>\n<ul>\n<li>l'ajout \u00e0 l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> d'esp\u00e8ces de mauvaises herbes nouvelles et \u00e9mergentes; </li>\n<li>le reclassement de certaines esp\u00e8ces de mauvaises herbes afin de tenir compte de leur r\u00e9partition actuelle sur le territoire canadien; </li>\n<li>le retrait de certaines esp\u00e8ces qui sont dor\u00e9navant cultiv\u00e9es pour leur int\u00e9r\u00eat agricole; </li>\n<li>des r\u00e9visions au texte de la r\u00e9glementation reprenant le libell\u00e9 de la Loi sur les semences afin d'assurer une certaine uniformit\u00e9 quant \u00e0 la formulation et \u00e0\u00a0l'interpr\u00e9tation; </li>\n<li>la prise en compte des pratiques exemplaires actuellement en vogue dans le monde. </li>\n</ul>\n\n<h2 class=\"h5\">En quoi ces modifications ont-elles des incidences sur les entreprises canadiennes?</h2>\n<p>La diminution du nombre des esp\u00e8ces de mauvaises herbes nuisibles contribuera \u00e0 pr\u00e9server la prosp\u00e9rit\u00e9 \u00e0 long terme de la fili\u00e8re agricole du Canada et \u00e0 appr\u00e9cier sa valeur sur les march\u00e9s nationaux et internationaux. La mise \u00e0 jour des classifications des esp\u00e8ces de mauvaises herbes dans l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> repose sur des renseignements actuels sur leur r\u00e9partition au Canada; cela permettra d'all\u00e9ger le fardeau d'une r\u00e9glementation d\u00e9su\u00e8te que l'on impose inutilement \u00e0 l'industrie. </p>\n<p>\u00c0 la suite de l'ajout ou du reclassement d'esp\u00e8ces de mauvaises herbes, nous pr\u00e9voyons qu'un certain nombre de petites entreprises du secteur des semences devront peut-\u00eatre assumer des co\u00fbts suppl\u00e9mentaires, bien que n\u00e9gligeables, li\u00e9s \u00e0 la conformit\u00e9. </p>\n<h2 class=\"h5\">Certaines des esp\u00e8ces inscrites dans l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes en tant que graines de mauvaises herbes nuisibles interdites sont \u00e9galement r\u00e9glement\u00e9es par l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> en vertu de la Loi sur la protection des v\u00e9g\u00e9taux. Quelles en sont les r\u00e9percussions pour les intervenants?</h2>\n<p>Lorsqu'une esp\u00e8ce nuisible interdite est d\u00e9cel\u00e9e comme faisant partie de la fonction normale de la Loi sur les semences, l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut prendre une mesure pr\u00e9vue par la Loi sur la protection des v\u00e9g\u00e9taux (LPV), \u00e0 la condition que l'esp\u00e8ce soit \u00e9galement inscrite \u00e0 la Liste des parasites r\u00e9glement\u00e9s par le Canada. L'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr> peut se pr\u00e9valoir de la <abbr title=\"Loi sur la protection des v\u00e9g\u00e9taux\">LPV</abbr> pour att\u00e9nuer le risque associ\u00e9 \u00e0 la pr\u00e9sence de l'esp\u00e8ce de mauvaise herbe. </p>\n<p>Dans le cadre de cette consultation, certains intervenants ont manifest\u00e9 leur pr\u00e9occupation quant \u00e0 une indemnisation de ceux qui sont touch\u00e9s par les r\u00e9percussions de la mesure d'application de la loi prise en vertu de la <abbr title=\"Loi sur la protection des v\u00e9g\u00e9taux\">LPV</abbr>. </p>\n<h2 class=\"h5\">\u00c0 quelle date pr\u00e9voit-on que ce r\u00e8glement entrera en vigueur?</h2>\n<p>Les modifications \u00e0 l'<abbr title=\"Arr\u00eat\u00e9 sur les graines de mauvaises herbes\">AGMH</abbr> entrent en vigueur le 1er\u00a0novembre\u00a02016. </p>\n<h2 class=\"h5\">\u00c0 qui dois-je m'adresser pour obtenir de plus amples renseignements sur le R\u00e8glement sur les semences et sur l'Arr\u00eat\u00e9 sur les graines de mauvaises herbes?</h2>\n<p>Pour obtenir de plus amples renseignements \u00e0 propos des <a href=\"/protection-des-vegetaux/semences/fra/1299173228771/1299173306579\">semences</a>, veuillez consulter notre site Web ou <a href=\"mailto:david.bailey@inspection.gc.ca\">communiquer avec David R. Bailey</a>, directeur, Division des produits v\u00e9g\u00e9taux de l'<abbr title=\"Agence canadienne d'inspection des aliments\">ACIA</abbr>.</p>\r\n        \r\n\r\n<div class=\"row\">\r\n \r\n\t\t\t\r\n\t\t\t\r\n\r\n\t\t\r\n\t\r\n</div>"
    },
    "success": true,
    "chat_wizard": false
}